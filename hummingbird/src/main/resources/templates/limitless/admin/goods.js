import request from '@/utils/request'
import qs from 'qs'

// 查询采购订单商品 列表
export function listGoods(query) {
  return request({
    url: '/admin/goods/melist',
    method: 'get',
    params: query
  })
}

// 查询采购订单商品 详细
export function getGoods(id) {
  return request({
    url: '/admin/goods/single/' + id,
    method: 'get'
  })
}

// 新增采购订单商品 
export function addGoods(data) {
  return request({
    url: '/admin/goods/save',
    method: 'post',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    data: qs.stringify(data)
  })
}

// 修改采购订单商品 
export function updateGoods(data) {
  return request({
    url: '/admin/goods/modify',
    method: 'put',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    data: qs.stringify(data)
  })
}

// 删除采购订单商品 
export function delGoods(id) {
  return request({
    url: '/admin/goods/del/' + id,
    method: 'delete'
  })
}

// 导出采购订单商品 
export function exportGoods(query) {
  return request({
    url: '/admin/goods/export',
    method: 'get',
    params: query
  })
}