import request from '@/utils/request'
import qs from 'qs'

// 查询采购订单 列表
export function listOrder(query) {
  return request({
    url: '/admin/order/melist',
    method: 'get',
    params: query
  })
}

// 查询采购订单 详细
export function getOrder(id) {
  return request({
    url: '/admin/order/single/' + id,
    method: 'get'
  })
}

// 新增采购订单 
export function addOrder(data) {
  return request({
    url: '/admin/order/save',
    method: 'post',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    data: qs.stringify(data)
  })
}

// 修改采购订单 
export function updateOrder(data) {
  return request({
    url: '/admin/order/modify',
    method: 'put',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    data: qs.stringify(data)
  })
}

// 删除采购订单 
export function delOrder(id) {
  return request({
    url: '/admin/order/del/' + id,
    method: 'delete'
  })
}

// 导出采购订单 
export function exportOrder(query) {
  return request({
    url: '/admin/order/export',
    method: 'get',
    params: query
  })
}