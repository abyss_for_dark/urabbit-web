package com.graphai.mq.client;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.aliyun.openservices.ons.api.MessageListener;
import com.aliyun.openservices.ons.api.PropertyKeyConst;
import com.aliyun.openservices.ons.api.bean.ConsumerBean;
import com.aliyun.openservices.ons.api.bean.Subscription;
import com.graphai.mq.alibaba.rocketmq.config.RocketMqConfig;
import com.graphai.mq.listener.RedPacketMessageListener;

@Configuration
public class RocketConsumerClient {
    @Autowired
    private RocketMqConfig mqConfig;
    @Autowired
    private RedPacketMessageListener messageListener;

    @Bean(initMethod = "start", destroyMethod = "shutdown")
    public ConsumerBean buildConsumer() {
        ConsumerBean consumerBean = new ConsumerBean();
        System.out.println("初始化Mq获取配置数据：topic----" + mqConfig.getTopic() + "---groupId---" + mqConfig.getGroupId());
        // 配置文件
        Properties properties = mqConfig.getMqPropertie();
        properties.setProperty(PropertyKeyConst.GROUP_ID, mqConfig.getGroupId());
        // 将消费者线程数固定为20个 20为默认值
        properties.setProperty(PropertyKeyConst.ConsumeThreadNums, mqConfig.getConsumeThreadNums());
        consumerBean.setProperties(properties);
        // 订阅关系
        Map<Subscription, MessageListener> subscriptionTable = new HashMap<Subscription, MessageListener>();
        Subscription subscription = new Subscription();
        subscription.setTopic(mqConfig.getTopic());
        // subscription.setExpression(mqConfig.getTag());
        subscription.setExpression("*");
        subscriptionTable.put(subscription, messageListener);
        // 订阅多个topic如上面设置
        consumerBean.setSubscriptionTable(subscriptionTable);
        return consumerBean;
    }
}
