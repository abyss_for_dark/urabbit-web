package com.graphai.mq.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.aliyun.openservices.ons.api.Action;
import com.aliyun.openservices.ons.api.ConsumeContext;
import com.aliyun.openservices.ons.api.Message;
import com.aliyun.openservices.ons.api.MessageListener;
import com.graphai.api.mq.BasedMQConsumerHandle;
import com.graphai.spi.mq.TsfMessage;

@Component
public class RedPacketMessageListener implements MessageListener {
    // private final Log logger = LogFactory.getLog(RedPacketMessageListener.class);

    @Autowired
    private BasedMQConsumerHandle consumerHandle;

    /**
     * @note 统一监听红包消息topic
     */
    @Override
    public Action consume(Message message, ConsumeContext context) {
        try {
            TsfMessage tsfMessage = new TsfMessage();
            tsfMessage.setBody(message.getBody());
            tsfMessage.setMsgId(message.getMsgID());
            tsfMessage.setKey(message.getKey());
            tsfMessage.setTag(message.getTag());
            tsfMessage.setTopic(message.getTopic());
            tsfMessage.setShardingKey(message.getShardingKey());
            if ("success".equals(consumerHandle.consume(tsfMessage))) {
                return Action.CommitMessage;
            } else {
                return Action.ReconsumeLater;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Action.ReconsumeLater;
        }
    }



}
