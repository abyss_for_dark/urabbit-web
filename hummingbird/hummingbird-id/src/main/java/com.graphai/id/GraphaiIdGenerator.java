package com.graphai.id;

import com.graphai.uid.UidGenerator;

/**
 * ID生成器,这里有两种用法，第一种自己使用静态方法获取ID自己设置如entity， 第二种通过mybatis plus自动注入，并且会自动将Long类型转换成String
 * 
 * @author biteam
 */
public final class GraphaiIdGenerator {
    // implements IdentifierGenerator {

    private static GraphaiIdGenerator me;

    public GraphaiIdGenerator(UidGenerator uidGenerator) {
        me = this;
        me.uidGenerator = uidGenerator;
    }

    /**
     * 百度uid生成器
     */
    private UidGenerator uidGenerator;


    /**
     * 返回字符串 Snow 算法
     * 
     * @param tableName
     * @return
     */
    public static String nextId(String tableName) {
        /** (2)百度uid生成器 **/
        Long myId = me.uidGenerator.getUID();
        return myId.toString();
    }

    /**
     * 提供long类型返回值
     * 
     * @param entity
     * @return
     */
    public static Long nextId(Object entity) {
        return me.uidGenerator.getUID();
    }

    // /**
    // * mybatis plus自定义实现
    // */
    // @Override
    // public Long nextId(Object entity) {
    // return null;
    // // return me.uidGenerator.getUID();
    // }

}
