package com.graphai.id;

import com.baomidou.mybatisplus.core.incrementer.IdentifierGenerator;
import org.springframework.stereotype.Component;

@Component
public class MyBatisPlusCustomIdGenerator implements IdentifierGenerator {

    @Override
    public Long nextId(Object entity) {
        return GraphaiIdGenerator.nextId(entity);
    }
}
