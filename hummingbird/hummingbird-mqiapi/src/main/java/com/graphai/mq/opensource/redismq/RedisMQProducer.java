package com.graphai.mq.opensource.redismq;

import java.util.Map;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.graphai.spi.mq.BasedMQProducer;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RedisMQProducer implements BasedMQProducer {

    private final Log logger = LogFactory.getLog(RedisMQProducer.class);

    Properties properties = new Properties();

    @Override
    public String getType() {
        return "redis";
    }


    @Override
    public String send(Map<String, Object> msgmp) {
        return null;
    }

}
