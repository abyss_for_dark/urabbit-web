package com.graphai.mq.alibaba.rocketmq;

import java.util.Date;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.aliyun.openservices.ons.api.Message;
import com.aliyun.openservices.ons.api.SendResult;
import com.aliyun.openservices.ons.api.bean.ProducerBean;
import com.graphai.mq.alibaba.rocketmq.config.RocketMqConfig;
import com.graphai.spi.mq.BasedMQProducer;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AlibabaRocketMQProducer implements BasedMQProducer {
    private final Log logger = LogFactory.getLog(AlibabaRocketMQProducer.class);

    Properties properties = new Properties();
    @Autowired
    private RocketMqConfig rocketMqConfig;
    @Autowired
    private ProducerBean producer;

    @Override
    public String getType() {
        return "rocketmq";
    }

    @Override
    public String send(Map<String, Object> msgmp) {
        String param = MapUtils.getString(msgmp, "param");
        String bizKey = MapUtils.getString(msgmp, "bizKey");
        String tags = MapUtils.getString(msgmp, "tags");

        if (!StringUtils.isEmpty(param)) {
            Message msg = new Message(
                            // 在控制台创建的 Topic，即该消息所属的 Topic 名称
                            rocketMqConfig.getTopic(),
                            // Message Tag,
                            // 可理解为 Gmail 中的标签，对消息进行再归类，方便 Consumer 指定过滤条件在消息队列 RocketMQ 版服务器过滤
                            tags,
                            // Message Body
                            // 任何二进制形式的数据，消息队列 RocketMQ 版不做任何干预，
                            // 需要 Producer 与 Consumer 协商好一致的序列化和反序列化方式
                            param.getBytes());
            // 设置代表消息的业务关键属性，请尽可能全局唯一，以方便您在无法正常收到消息情况下，可通过控制台查询消息并补发
            // 注意：不设置也不会影响消息正常收发
            msg.setKey(bizKey);
            // 发送消息，只要不抛异常就是成功
            try {
                SendResult sendResult = producer.send(msg);
                // 同步发送消息，只要不抛异常就是成功
                if (sendResult != null) {
                    // 打印 Message ID，以便用于消息发送状态查询
                    logger.info(new Date() + " 发送消息成功,主题是:" + msg.getTopic() + " msgId是: " + sendResult.getMessageId());
                }
            } catch (Exception e) {
                // 消息发送失败，需要进行重试处理，可重新发送这条消息或持久化这条数据进行补偿处理
                logger.info(new Date() + " 发送消息失败,主题是:" + msg.getTopic());
                e.printStackTrace();
            }
        }
        // 在应用退出前，可以销毁 Producer 对象
        // 注意：如果不销毁也没有问题
        // producer.shutdown();


        return "success";
    }

}
