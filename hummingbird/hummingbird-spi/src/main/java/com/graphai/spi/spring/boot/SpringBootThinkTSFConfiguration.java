package com.graphai.spi.spring.boot;


import java.sql.SQLException;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.graphai.spi.config.MQProducerConfiguration;
import com.graphai.spi.config.MQProperties;
import com.graphai.spi.mq.BasedMQProducer;
import com.graphai.spi.mq.MQFactory;
import com.graphai.spi.mq.lmax.disruptor.DisruptorEventFactory;
import com.graphai.spi.mq.lmax.disruptor.DisruptorEventHandler;
import com.graphai.spi.mq.lmax.disruptor.DisruptorMessage;
import com.lmax.disruptor.BlockingWaitStrategy;
import com.lmax.disruptor.RingBuffer;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.dsl.ProducerType;

import lombok.RequiredArgsConstructor;

@ConditionalOnProperty(prefix = MQProperties.MQ_PREFIX, name = "auto", havingValue = "true", matchIfMissing = true)
@RequiredArgsConstructor
@Configuration
public class SpringBootThinkTSFConfiguration {

    @Autowired
    private MQProperties mqProperties;

    @Autowired
    private DisruptorEventHandler eventHandler;

    /**
     * 初始化MQ生产者
     * 
     * @return
     * @throws SQLException
     */
    @Bean
    public BasedMQProducer basedMQProducer() {
        return MQFactory.createMQProducer(new MQProducerConfiguration(mqProperties.getType(), mqProperties.getParamType()));
    }

    /**
     * 按条件装配
     * 
     * @return
     */
    @Bean
    @ConditionalOnBean(DisruptorEventHandler.class)
    @ConditionalOnProperty(prefix = MQProperties.MQ_PREFIX, name = MQProperties.MQ_TYPE, havingValue = MQProperties.MQ_DISRUPTOR, matchIfMissing = true)
    public RingBuffer<DisruptorMessage> messageRingBuffer() {
        // 定义用于事件处理的线程池， Disruptor通过java.util.concurrent.ExecutorSerivce提供的线程来触发consumer的事件处理
        // ExecutorService executor = Executors.newFixedThreadPool(2);
        // 指定事件工厂
        DisruptorEventFactory factory = new DisruptorEventFactory();
        // 指定ringbuffer字节大小，必须为2的N次方（能将求模运算转为位运算提高效率），否则将影响效率
        int bufferSize = 1024 * 256;
        ThreadFactory threadFactory = new ThreadFactory() {
            private final AtomicInteger index = new AtomicInteger(0);

            @Override
            public Thread newThread(Runnable r) {
                return new Thread(null, r, "disruptor-thread-" + index.incrementAndGet());
            }
        };

        // 单线程模式，获取额外的性能
        Disruptor<DisruptorMessage> disruptor = new Disruptor<>(factory, bufferSize, threadFactory, ProducerType.SINGLE, new BlockingWaitStrategy());

        // 设置事件业务处理器---消费者
        disruptor.handleEventsWith(eventHandler);

        // 启动disruptor线程
        disruptor.start();
        // 获取ringbuffer环，用于接取生产者生产的事件
        RingBuffer<DisruptorMessage> ringBuffer = disruptor.getRingBuffer();
        return ringBuffer;
    }

}
