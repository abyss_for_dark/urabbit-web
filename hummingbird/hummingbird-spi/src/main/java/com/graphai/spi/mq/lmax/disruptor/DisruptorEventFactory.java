package com.graphai.spi.mq.lmax.disruptor;

import com.lmax.disruptor.EventFactory;

/**
 * 消息工厂
 * 
 * @author biteam
 *
 */
public class DisruptorEventFactory implements EventFactory<DisruptorMessage> {

    @Override
    public DisruptorMessage newInstance() {
        return new DisruptorMessage();
    }

}
