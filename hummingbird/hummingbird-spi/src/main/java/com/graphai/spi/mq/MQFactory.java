package com.graphai.spi.mq;

import com.google.common.base.Strings;
import com.graphai.spi.config.MQProducerConfiguration;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * MQ 生产者工厂类
 * 
 * @author biteam
 *
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class MQFactory {

    /**
     * 创建MQ生产者
     * 
     * @param producerConfiguration
     * @return
     */
    public static BasedMQProducer createMQProducer(final MQProducerConfiguration producerConfiguration) {
        MQProducerServiceLoader serviceLoader = new MQProducerServiceLoader();
        return containsMQProducerConfiguration(producerConfiguration) ? serviceLoader.newService(producerConfiguration.getType(), producerConfiguration.getProperties()) : serviceLoader.newService();
    }

    private static boolean containsMQProducerConfiguration(final MQProducerConfiguration producerConfiguration) {
        return null != producerConfiguration && !Strings.isNullOrEmpty(producerConfiguration.getType());
    }

}
