package com.graphai.spi;

import java.util.Properties;

public interface TypeBasedSPI {
    /**
     * Get algorithm type.
     * 
     * @return type
     */
    String getType();

    /**
     * Get properties.
     * 
     * @return properties of algorithm
     */
    Properties getProperties();

    /**
     * Set properties.
     * 
     * @param properties properties of algorithm
     */
    void setProperties(Properties properties);

}
