package com.graphai.spi.config;

import java.util.Properties;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.graphai.spi.TypeBasedSPIConfiguration;

import lombok.Getter;

/**
 * MQ生产者配置
 * 
 * @author biteam
 *
 */
@Getter
public class MQProducerConfiguration extends TypeBasedSPIConfiguration {

    private String paramType;

    public MQProducerConfiguration(final String type, final String paramType) {
        super(type);
        Preconditions.checkArgument(!Strings.isNullOrEmpty(paramType), "paramType is required.");
        this.paramType = paramType;
    }


    public MQProducerConfiguration(final String type, final String paramType, final Properties properties) {
        super(type, properties);
        Preconditions.checkArgument(!Strings.isNullOrEmpty(paramType), "paramType is required.");
        this.paramType = paramType;
    }

}
