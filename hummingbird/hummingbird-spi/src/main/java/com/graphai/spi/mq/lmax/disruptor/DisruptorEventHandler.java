package com.graphai.spi.mq.lmax.disruptor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.graphai.api.mq.BasedMQConsumerHandle;
import com.graphai.spi.mq.TsfMessage;
import com.lmax.disruptor.EventHandler;

/**
 * disruptor消费者
 * 
 * @author biteam
 *
 */

@Component
public class DisruptorEventHandler implements EventHandler<TsfMessage> {

    @Autowired
    private BasedMQConsumerHandle consumerHandle;

    @Override
    public void onEvent(TsfMessage event, long sequence, boolean endOfBatch) throws Exception {
        consumerHandle.consume(event);
    }
}
