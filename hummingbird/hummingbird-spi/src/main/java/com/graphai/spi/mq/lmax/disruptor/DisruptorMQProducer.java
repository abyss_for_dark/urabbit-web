package com.graphai.spi.mq.lmax.disruptor;

import java.util.Map;
import java.util.Properties;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.graphai.spi.mq.BasedMQProducer;
import com.graphai.spi.mq.TsfMessage;
import com.lmax.disruptor.RingBuffer;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DisruptorMQProducer implements BasedMQProducer {
    private final Log logger = LogFactory.getLog(DisruptorMQProducer.class);

    @Autowired
    private RingBuffer<TsfMessage> messageRingBuffer;

    Properties properties = new Properties();

    @Override
    public String getType() {
        return "disruptor";
    }


    @Override
    public String send(Map<String, Object> msgmp) {
        String param = MapUtils.getString(msgmp, "param");
        String bizKey = MapUtils.getString(msgmp, "bizKey");
        String tags = MapUtils.getString(msgmp, "tags");

        long sequence = messageRingBuffer.next();
        try {
            // 给Event填充数据
            TsfMessage event = messageRingBuffer.get(sequence);
            if (event != null) {
                event.setBody(param.getBytes());
                event.setKey(bizKey);
                event.setTag(tags);
            }
        } catch (Exception e) {
            logger.error("failed to add event to messageRingBuffer for : " + e.getMessage(), e);
        } finally {
            // 发布Event，激活观察者去消费，将sequence传递给改消费者
            // 注意最后的publish方法必须放在finally中以确保必须得到调用；如果某个请求的sequence未被提交将会堵塞后续的发布操作或者其他的producer
            messageRingBuffer.publish(sequence);
        }

        return "succes";
    }

}
