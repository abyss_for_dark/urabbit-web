package com.graphai.spi.mq;

import java.util.Properties;

import com.aliyun.openservices.ons.api.TopicPartition;

public class TsfMessage {

    Properties systemProperties;

    private String msgId;

    private String topic;

    private byte[] body;

    private Properties userProperties;

    public TsfMessage() {
        this(null, null, "", null);
    }

    public TsfMessage(String topic, String tag, String key, byte[] body) {
        this.topic = topic;
        this.body = body;

        this.putSystemProperties(SystemPropKey.TAG, tag);
        this.putSystemProperties(SystemPropKey.KEY, key);
    }

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    void putSystemProperties(final String key, final String value) {
        if (null == this.systemProperties) {
            this.systemProperties = new Properties();
        }

        if (key != null && value != null) {
            this.systemProperties.put(key, value);
        }
    }

    public TsfMessage(String topic, String tags, byte[] body) {
        this(topic, tags, "", body);
    }

    public void putUserProperties(final String key, final String value) {
        if (null == this.userProperties) {
            this.userProperties = new Properties();
        }

        if (key != null && value != null) {
            this.userProperties.put(key, value);
        }
    }

    public String getUserProperties(final String key) {
        if (null != this.userProperties) {
            return (String) this.userProperties.get(key);
        }

        return null;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getTag() {
        return this.getSystemProperties(SystemPropKey.TAG);
    }

    String getSystemProperties(final String key) {
        if (null != this.systemProperties) {
            return this.systemProperties.getProperty(key);
        }

        return null;
    }

    public void setTag(String tag) {
        this.putSystemProperties(SystemPropKey.TAG, tag);
    }

    public String getKey() {
        return this.getSystemProperties(SystemPropKey.KEY);
    }

    public void setKey(String key) {
        this.putSystemProperties(SystemPropKey.KEY, key);
    }

    public String getMsgID() {
        return this.getSystemProperties(SystemPropKey.MSGID);
    }

    public void setMsgID(String msgid) {
        this.putSystemProperties(SystemPropKey.MSGID, msgid);
    }

    Properties getSystemProperties() {
        return systemProperties;
    }

    void setSystemProperties(Properties systemProperties) {
        this.systemProperties = systemProperties;
    }

    public Properties getUserProperties() {
        return userProperties;
    }

    public void setUserProperties(Properties userProperties) {
        this.userProperties = userProperties;
    }

    public byte[] getBody() {
        return body;
    }

    public void setBody(byte[] body) {
        this.body = body;
    }

    public int getReconsumeTimes() {
        String pro = this.getSystemProperties(SystemPropKey.RECONSUMETIMES);
        if (pro != null) {
            return Integer.parseInt(pro);
        }

        return 0;
    }

    public void setReconsumeTimes(final int value) {
        putSystemProperties(SystemPropKey.RECONSUMETIMES, String.valueOf(value));
    }

    public long getBornTimestamp() {
        String pro = this.getSystemProperties(SystemPropKey.BORNTIMESTAMP);
        if (pro != null) {
            return Long.parseLong(pro);
        }

        return 0L;
    }

    public void setBornTimestamp(final long value) {
        putSystemProperties(SystemPropKey.BORNTIMESTAMP, String.valueOf(value));
    }

    public String getBornHost() {
        String pro = this.getSystemProperties(SystemPropKey.BORNHOST);
        return pro == null ? "" : pro;
    }

    public void setBornHost(final String value) {
        putSystemProperties(SystemPropKey.BORNHOST, value);
    }

    public long getStartDeliverTime() {
        String pro = this.getSystemProperties(SystemPropKey.STARTDELIVERTIME);
        if (pro != null) {
            return Long.parseLong(pro);
        }

        return 0L;
    }

    public String getShardingKey() {
        String pro = this.getSystemProperties(SystemPropKey.SHARDINGKEY);
        return pro == null ? "" : pro;
    }

    public void setShardingKey(final String value) {
        putSystemProperties(SystemPropKey.SHARDINGKEY, value);
    }

    public void setStartDeliverTime(final long value) {
        putSystemProperties(SystemPropKey.STARTDELIVERTIME, String.valueOf(value));
    }

    public long getOffset() {
        String v = getSystemProperties(SystemPropKey.CONSUMEOFFSET);
        if (v != null) {
            return Long.parseLong(v);
        }
        return 0L;
    }

    public TopicPartition getTopicPartition() {
        return new TopicPartition(topic, getSystemProperties(SystemPropKey.PARTITION));
    }

    @Override
    public String toString() {
        return "Message [topic=" + topic + ", systemProperties=" + systemProperties + ", userProperties=" + userProperties + ", body=" + (body != null ? body.length : 0) + "]";
    }

    static public class SystemPropKey {
        public static final String TAG = "__TAG";
        public static final String KEY = "__KEY";
        public static final String MSGID = "__MSGID";
        public static final String SHARDINGKEY = "__SHARDINGKEY";
        public static final String RECONSUMETIMES = "__RECONSUMETIMES";
        public static final String BORNTIMESTAMP = "__BORNTIMESTAMP";
        public static final String BORNHOST = "__BORNHOST";
        public static final String STARTDELIVERTIME = "__STARTDELIVERTIME";

        public static final String CONSUMEOFFSET = "__CONSUMEOFFSET";

        public static final String PARTITION = "__PARTITION";
    }


}
