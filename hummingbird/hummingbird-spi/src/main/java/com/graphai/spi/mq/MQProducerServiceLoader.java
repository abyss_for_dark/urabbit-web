package com.graphai.spi.mq;

import com.graphai.spi.NewInstanceServiceLoader;
import com.graphai.spi.TypeBasedSPIServiceLoader;


/**
 * MQ消费者实例化加载类
 * 
 * @author biteam
 *
 */
public class MQProducerServiceLoader extends TypeBasedSPIServiceLoader<BasedMQProducer> {
    static {
        NewInstanceServiceLoader.register(BasedMQProducer.class);
    }

    public MQProducerServiceLoader() {
        super(BasedMQProducer.class);
    }
}
