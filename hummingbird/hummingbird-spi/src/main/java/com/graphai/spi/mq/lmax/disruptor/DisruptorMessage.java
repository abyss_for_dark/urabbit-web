package com.graphai.spi.mq.lmax.disruptor;

import com.graphai.spi.mq.TsfMessage;

/**
 * disruptor消息体
 * 
 * @author biteam
 *
 */
public class DisruptorMessage extends TsfMessage {

    public DisruptorMessage() {
        this(null, null, "", null);
    }

    public DisruptorMessage(String topic, String tag, String key, byte[] body) {
        super(topic, tag, key, body);
    }
}
