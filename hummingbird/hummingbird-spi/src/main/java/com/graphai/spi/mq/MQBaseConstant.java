package com.graphai.spi.mq;

public class MQBaseConstant {


    public static String MQ_HANDLE_SUCCESS = "success";

    public static String MQ_HANDLE_FAIL = "fail";

    /** 现金红包TAG **/
    public static final String RED_CASH_ENVELOPE_TAG = "RED_CASH_ENVELOPE";
    /** 整点红包TAG **/
    public static final String RED_WHOLE_ENVELOPE_TAG = "RED_WHOLE_ENVELOPE_TAG";
    /** 大额红包TAG **/
    public static final String RED_BIG_ENVELOPE_TAG = "RED_BIG_ENVELOPE_TAG";
    /** 抖货红包TAG **/
    public static final String RED_DY_ENVELOPE_TAG = "RED_DY_ENVELOPE_TAG";
    /** 新春拼团红包TAG (初始化红包)**/
    public static final String RED_GROUP_ENVELOPE_TAG = "RED_GROUP_ENVELOPE_TAG";
    /** 新春拼团红包TAG (初始化用户领取红包金额)**/
    public static final String RED_GROUP_100_ENVELOPE_TAG = "RED_GROUP_100_ENVELOPE_TAG";
    /** 新春拼团红包TAG (领取)**/
    public static final String RECEIVE_RED_GROUP_ENVELOPE_TAG = "RECEIVE_RED_RED_GROUP_ENVELOPE_TAG";
    /** 大额红包key前缀 **/
    public static final String RED_BIG_KEY_PREFIX = "redbig";
    /** 通用tag 记录页面PV访问量标签**/
    public static final String COMMON_PAGE_VIEW_TAG = "COMMON_PAGE_VIEW_TAG";
    /** 团购多人退款TAG**/
    public static final String GROUP_REFUND_TAG = "GROUP_REFUND_TAG";
    /** 任务竞价多人退款 **/
    public static final String TASK_DRAWBACK_MONEY_BY_DIDDING = "TASK_DRAWBACK_MONEY_BY_DIDDING";




}
