package com.graphai.spi;


import java.util.Properties;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

import lombok.Getter;

/**
 * Type based SPI configuration.
 */
@Getter
public abstract class TypeBasedSPIConfiguration {

    private final String type;

    private final Properties properties;

    public TypeBasedSPIConfiguration(final String type) {
        this(type, null);
    }

    public TypeBasedSPIConfiguration(final String type, final Properties properties) {
        Preconditions.checkArgument(!Strings.isNullOrEmpty(type), "Type is required.");
        this.type = type;
        this.properties = null == properties ? new Properties() : properties;
    }
}
