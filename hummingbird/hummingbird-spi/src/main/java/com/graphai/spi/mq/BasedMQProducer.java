package com.graphai.spi.mq;

import java.util.Map;

import com.graphai.spi.TypeBasedSPI;

/**
 * 定义消息发送的SPI主接口
 * 
 * @author biteam
 *
 */
public interface BasedMQProducer extends TypeBasedSPI {

    /**
     * 消息发送
     * 
     * @param msg
     * @return
     */
    String send(Map<String, Object> msgmp);
}
