package com.graphai.spi.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = MQProperties.MQ_PREFIX)
public class MQProperties {
    public static final String MQ_PREFIX = "gsf.mq";

    public static final String MQ_TYPE = "type";

    public static final String MQ_DISRUPTOR = "disruptor";
    public static final String MQ_ROCKETMQ = "rocketmq";

    private String type;

    private String paramType;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getParamType() {
        return paramType;
    }

    public void setParamType(String paramType) {
        this.paramType = paramType;
    }


}
