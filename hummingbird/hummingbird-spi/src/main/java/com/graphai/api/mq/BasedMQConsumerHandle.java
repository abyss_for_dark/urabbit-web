package com.graphai.api.mq;

import com.graphai.spi.mq.TsfMessage;

public interface BasedMQConsumerHandle {

    /**
     * 统一消费业务方法封装
     * 
     * @param tsfMessage
     * @return
     */
    public String consume(TsfMessage tsfMessage) throws Exception;
}
