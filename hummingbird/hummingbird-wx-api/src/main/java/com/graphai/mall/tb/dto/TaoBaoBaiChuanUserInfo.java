package com.graphai.mall.tb.dto;

public class TaoBaoBaiChuanUserInfo {

	private String topExpireTime;
	private String openSid;
	private String avatar;
	private String type;
	private String topAccessToken;
	private String nick;
	private String topAuthCode;
	private String openId;
	public String getTopExpireTime() {
		return topExpireTime;
	}
	public void setTopExpireTime(String topExpireTime) {
		this.topExpireTime = topExpireTime;
	}
	public String getOpenSid() {
		return openSid;
	}
	public void setOpenSid(String openSid) {
		this.openSid = openSid;
	}
	
	
	public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getTopAccessToken() {
		return topAccessToken;
	}
	public void setTopAccessToken(String topAccessToken) {
		this.topAccessToken = topAccessToken;
	}
	public String getNick() {
		return nick;
	}
	public void setNick(String nick) {
		this.nick = nick;
	}
	public String getTopAuthCode() {
		return topAuthCode;
	}
	public void setTopAuthCode(String topAuthCode) {
		this.topAuthCode = topAuthCode;
	}
	public String getOpenId() {
		return openId;
	}
	public void setOpenId(String openId) {
		this.openId = openId;
	}
	
	
    	
}
