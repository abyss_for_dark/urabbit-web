package com.graphai.mall.wx.web.game;



import com.graphai.commons.util.ResponseUtil;
import com.graphai.mall.db.domain.*;
import com.graphai.mall.db.constant.account.AccountStatus;
import com.graphai.mall.db.service.account.AccountService;
import com.graphai.mall.db.service.bmdesign.BmdesignBusinessFormService;
import com.graphai.mall.db.service.order.MallOrderService;
import com.graphai.mall.db.service.promotion.MarketingSignRecordService;
import com.graphai.mall.db.service.sysconfig.MallSystemConfigService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.service.user.behavior.MallUserCardService;
import com.graphai.mall.db.util.account.AccountUtils;
import com.graphai.mall.db.constant.CommonConstant;
import com.graphai.mall.wx.annotation.LoginUser;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.internal.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.*;

import java.util.*;
import java.util.stream.Collectors;

import static java.time.DayOfWeek.MONDAY;


@RestController
@RequestMapping("/wx/sign")
@Validated
public class WxMarketingSignRecordController {

    private final Log log = LogFactory.getLog(WxMarketingSignRecordController.class);

    @Autowired
    public MarketingSignRecordService marketingSignRecordService;
    @Autowired
    public MallUserService mallUserService;
    @Autowired
    public BmdesignBusinessFormService bmdesignBusinessFormService;
    @Autowired
    private MallSystemConfigService mallSystemConfigService;
    @Autowired
    private MallUserCardService mallUserCardService;
    @Autowired
    private MallOrderService MallOrderService;
    @Autowired
    private AccountService accountService;
    @Value("${public.moreIntegral}")
    private Integer moreIntegral;


    /**
     * 查询签到记录
     * @param userId
     * @return
     */
     @RequestMapping("/list")
   public Object querySignList(@LoginUser String userId){
         if(null == userId){
             return  ResponseUtil.unlogin();
         }
         Map data = new HashMap();
         LocalDate localdate = LocalDate.now();

         // 账户上的积分
         List<Map>  usermap = marketingSignRecordService.queryUserAccount(userId);
         if(null != usermap && usermap.size()>0 ){
             FcAccount fcAccount = accountService.getUserAccount(userId);
             data.put("totalIntegral",fcAccount.getIntegralAmount());
         }else{
             data.put("totalIntegral",0);
         }

         // 今日是否签到
         MarketingSignRecord record =  marketingSignRecordService.querySignRecord(userId,localdate);
         data.put("isSign",record == null?false:true);

         // 连续签到天数
         Integer signDay = 0;
         // 今日签到获得积分
         Integer integer = 0;
         int reduceDay = 0;
         while(true){
             // 判断之前签到了几天
             LocalDate templocaldate = LocalDate.now().plusDays(-reduceDay);
             MarketingSignRecord marketingSignRecord = marketingSignRecordService.querySignRecord(userId, templocaldate);
             if(marketingSignRecord != null){
                 signDay += 1;
             }else{
                 if(reduceDay != 0){
                     break;
                 }
             }
             reduceDay++;
         }
         integer = (signDay) * moreIntegral;
         data.put("signDay",signDay);
         data.put("integral",integer);

         // 之后能获得的积分
         LocalDate lastMonday = localdate.minusDays(localdate.getDayOfWeek().getValue() - 1);//算出当前周的周一
         Map<Integer, Object> recordList = new HashMap<>();
         for(int i = 0; i <= 6; i++){
             LocalDate tempLocaldate = lastMonday.plusDays(i);
             MarketingSignRecord marketingSignRecord = marketingSignRecordService.querySignRecord(userId, tempLocaldate);

             if(marketingSignRecord == null){
                 if(localdate.isBefore(tempLocaldate) || localdate.equals(tempLocaldate)){
                     // 当前周的天数大于今天或今天没有签到；需要预算后面多少积分
                     int day = getDifferenceDayCount(localdate,tempLocaldate);
                     Integer preIntegral;

                     MarketingSignRecord item = marketingSignRecordService.querySignRecord(userId, localdate);
                     if(item == null){
                         preIntegral = (signDay + day + 1) * moreIntegral;
                     }else{
                         preIntegral = (signDay + day) * moreIntegral;
                     }
                     if(preIntegral > 14){
                         preIntegral = 14;
                     }
                     recordList.put(i + 1, preIntegral);
                 }else{
                     // 之前未签到不用管
                     recordList.put(i + 1, 0);
                 }
             }else{
                 recordList.put(i + 1, marketingSignRecord.getReward().intValue());
             }
         }
         data.put("record",recordList);
         data.put("localDate",localdate);

         return ResponseUtil.ok(data);
     }
    /**
     * 两个日期间相差的天数  这里不包含起始日期，如果想要结果包含起始日期，则需要再结果上+1
     * @return
     */
    private Integer getDifferenceDayCount(LocalDate startDateStr,LocalDate endDateStr) {
        //取正数
        return Math.abs((int) (endDateStr.toEpochDay() - startDateStr.toEpochDay()));
    }

    /**
     * 添加签到记录
     * @param
     * @return
     */
    @RequestMapping("/addSign")
    public Object addSignRecord( @LoginUser String userId ) throws Exception  {

        if(null ==userId){
            return  ResponseUtil.unlogin();
        }
        MarketingSignRecord marketingSignRecord = new MarketingSignRecord();

        Object error = validateSign(userId);
        if(null != error){
           return  error;
        }

        LocalDate localdate = LocalDate.now();
        LocalDate lastMonday = localdate.minusDays(localdate.getDayOfWeek().getValue()-1);//算出当前周的周一
        LocalDate lastday = localdate.with(DayOfWeek.SUNDAY);//算出当前周的周日
        List<MarketingSignRecord> marketingSignRecordlist =  marketingSignRecordService.querySignRecordList(userId,lastMonday,lastday);
        Map paramsMap = new HashMap();
        paramsMap.put("dType",AccountStatus.DISTRIBUTION_TYPE_00);  //直接分润
        paramsMap.put("capitalTrend",AccountStatus.CAPITAL_TREND_01);
        paramsMap.put("changeType",AccountStatus.CHANGE_TYPE_01);
        paramsMap.put("changeAccountType",AccountStatus.CHANGE_ACCOUNT_TYPE_2);
        Map<String,Object> datamap = new HashMap<>();

        if(marketingSignRecordlist.size()>=4&&localdate.getDayOfWeek().toString().equals("SUNDAY")){
            //周六前累计签到4天，周日瓜分金币
            // todo 暂时写死后面从业务形态表获取
            marketingSignRecord.setReward(BigDecimal.valueOf(600));
            marketingSignRecord.setRemarks("累计签到7天额加500");
            datamap.put("gold",600);
            paramsMap.put("message","签到加100金币");
            AccountUtils.accountChange(userId, BigDecimal.valueOf(100), CommonConstant.TASK_REWARD_GOLD,AccountStatus.BID_10,paramsMap);
            paramsMap.put("message","累计签到7天额外加500金币");
            AccountUtils.accountChange(userId, BigDecimal.valueOf(500), CommonConstant.TASK_REWARD_GOLD,AccountStatus.BID_10,paramsMap);

        }else {
            // todo 暂时写死后面从业务形态表获取
            marketingSignRecord.setReward(BigDecimal.valueOf(100));
            marketingSignRecord.setRemarks("签到奖励");
            paramsMap.put("message","签到奖励金币");
            AccountUtils.accountChange(userId, BigDecimal.valueOf(100), CommonConstant.TASK_REWARD_GOLD,AccountStatus.BID_10,paramsMap);

        }
        marketingSignRecord.setRewardType(CommonConstant.TASK_REWARD_GOLD);// REWARD_GOLD 金币奖励  REWARD_CASH 现金奖励
        marketingSignRecord.setUserId(userId);
        marketingSignRecord.setIsCalculate(false);
        marketingSignRecord.setStatus("1");

        marketingSignRecordService.addSignRecord(marketingSignRecord);


        return ResponseUtil.ok();
    }

    /**
     * 添加签到记录-积分
     * 签到规则：
     * 1.每周为一个签到周期，首日签到可领2个积分，连续签到每日可递增2个积分
     * 2.连续签到每日可领积分上限为14个积分，即连续签到7天后，每天可领14个积分
     * 3.连续签到中断后，重新累计连续签到天数
     * @param
     * @return
     */
    @RequestMapping("/addIntegralSign")
    public Object addIntegralSignRecord( @LoginUser String userId ) throws Exception  {
        if(null == userId){
            return  ResponseUtil.unlogin();
        }

        Object error = validateSign(userId);
        if(null != error){
            return  error;
        }

        // 统计签到了几天
        Integer signDay = 1;
        Integer integer = 0;
        for(int i = 1; i <= 6; i++){
            // 判断之前签到了几天
            LocalDate localdate = LocalDate.now().plusDays(-i);
            // todo:
            MarketingSignRecord marketingSignRecord = marketingSignRecordService.querySignRecord(userId, localdate);
            if(marketingSignRecord == null){
                // 前i天没有签到
                break;
            }
            signDay += 1;
        }
        // 本次签到获得的积分
        integer = signDay * moreIntegral;

        // 账户添加积分
        Map paramsMap = new HashMap();
        paramsMap.put("dType",AccountStatus.DISTRIBUTION_TYPE_00);  //直接分润
        paramsMap.put("capitalTrend",AccountStatus.CAPITAL_TREND_01);
        paramsMap.put("changeType",AccountStatus.CHANGE_TYPE_01);
        paramsMap.put("changeAccountType",AccountStatus.CHANGE_ACCOUNT_TYPE_2);
        paramsMap.put("message","签到奖励积分");
        AccountUtils.accountChange(userId, BigDecimal.valueOf(integer), CommonConstant.TASK_REWARD_INTEGRAL,AccountStatus.BID_10,paramsMap);

        // 添加签到记录
        MarketingSignRecord marketingSignRecord = new MarketingSignRecord();
        marketingSignRecord.setReward(BigDecimal.valueOf(integer));
        marketingSignRecord.setRemarks("签到奖励");
        marketingSignRecord.setRewardType(CommonConstant.TASK_REWARD_INTEGRAL);// 积分奖励
        marketingSignRecord.setUserId(userId);
        marketingSignRecord.setIsCalculate(false);
        marketingSignRecord.setStatus("1");
        marketingSignRecordService.addSignRecord(marketingSignRecord);
        return ResponseUtil.ok(integer);
    }

    /**
     * 签到中心看广告
     */
    @PostMapping("/signTask")
    public Object signTask(@LoginUser String userId,String taskStatus) throws Exception {
        if(null ==userId){
            return  ResponseUtil.unlogin();
        }
        Map paramsMap = new HashMap();
        taskStatus ="T0";
        paramsMap.put("dType",AccountStatus.DISTRIBUTION_TYPE_00);  //直接分润
        paramsMap.put("message","签到中心看广告奖励金币");
        paramsMap.put("capitalTrend",AccountStatus.CAPITAL_TREND_01);
        paramsMap.put("changeType",AccountStatus.CHANGE_TYPE_01);
        paramsMap.put("changeAccountType",AccountStatus.CHANGE_ACCOUNT_TYPE_2);
        if(org.apache.commons.lang3.StringUtils.isBlank(taskStatus)){
            switch (taskStatus){
                case CommonConstant.TASKSTATUS:
//                    BmdesignBusinessForm bmd = bmdesignBusinessFormService.selectBmdesignBusinessFormById("21");
                    // todo 暂时写死后面从业务形态表获取
                    AccountUtils.accountChange(userId, new BigDecimal(100), CommonConstant.TASK_REWARD_GOLD,AccountStatus.BID_10,paramsMap);
                    break;
                case CommonConstant.TASKSTATUS_FINISH:
                    BmdesignBusinessForm bmd1 = bmdesignBusinessFormService.selectBmdesignBusinessFormById("22");
                    if(bmd1.getReward().compareTo(new BigDecimal(0.00)) == 1){
                        AccountUtils.accountChange(userId, bmd1.getReward(), CommonConstant.TASK_REWARD_GOLD,AccountStatus.BID_10,paramsMap);
                    }
                    break;
                case CommonConstant.TASKSTATUS_DOWN:
                    BmdesignBusinessForm bmd2 = bmdesignBusinessFormService.selectBmdesignBusinessFormById("23");
                    if(bmd2.getReward().compareTo(new BigDecimal(0.00)) == 1){
                        AccountUtils.accountChange(userId, bmd2.getReward(), CommonConstant.TASK_REWARD_GOLD,AccountStatus.BID_10,paramsMap);
                    }
                    break;

            }
        }



        return ResponseUtil.ok();
    }





    /**
     * 校验签到数据
     */
    private  Object validateSign(String userId){
        if(StringUtils.isEmpty(userId)){
            return ResponseUtil.badArgument();
        }
        LocalDate localdate = LocalDate.now();
        List<MarketingSignRecord> marketingSignRecordlist =
               marketingSignRecordService.querySignRecordList(userId,localdate,localdate);

        if(marketingSignRecordlist.size()>0){
            Map<String, Object> obj = new HashMap<String, Object>();
            obj.put("errno", 403);
            obj.put("errmsg", "多次签到");
            return obj;
        }
        return null;
    }

    /**
     * 查询达人卡信息
     * @return
     */
    @RequestMapping("/queryTalent")
    public  Object getTalentPrice(@LoginUser String userId){
        // 查询卡信息
        Map<String, String> stringStringMap = mallSystemConfigService.queryTalent();
        Map<String, Object> stringMap = new HashMap<>();
        //月卡
        stringMap.put("monthCard",stringStringMap.get("mall_talent_month_card"));
        //季卡
        stringMap.put("seasonCard",stringStringMap.get("mall_talent_season_card"));
        //年卡
        stringMap.put("yearCard",stringStringMap.get("mall_talent_year_card"));
        //月卡原价
        stringMap.put("monthCardY",stringStringMap.get("mall_talent_month_card_y"));
        //季卡原价
        stringMap.put("seasonCardY",stringStringMap.get("mall_talent_season_card_y"));
        //年卡原价
        stringMap.put("yearCardY",stringStringMap.get("mall_talent_year_card_y"));

        if(null != userId ){
            Map<String ,Object> dataMap = marketingSignRecordService.queryUserTalentCard(userId);
            Map<String ,Object> data = new HashMap<>();
            if(null != dataMap ){
                long start = ((Timestamp)dataMap.get("startTime")).getTime();
                long end = ((Timestamp)dataMap.get("endTime")).getTime();
                LocalDate startTime = Instant.ofEpochMilli(start).atZone(ZoneOffset.ofHours(8)).toLocalDate();
                LocalDate endTime = Instant.ofEpochMilli(end).atZone(ZoneOffset.ofHours(8)).toLocalDate();
                Integer cardType = (Integer)dataMap.get("cardType");
                data.put("startTime",startTime);
                data.put("endTime",endTime);
                data.put("cardType",cardType);
                stringMap.put("card",data);
            }else{
                stringMap.put("card","");
            }
        }
        return ResponseUtil.ok(stringMap);
    }


    /**
     * 达人
     */
   @RequestMapping("/queryTalentList")
    public Object queryTalent(@LoginUser String userId){
       if(null == userId){
           return  ResponseUtil.unlogin();
       }
       /**推荐列表*/
       List<MallUser> byUserId = mallUserService.findByUserId(userId);
       List<String> list = new ArrayList<>();
       list.add("0");
       List<Map<String,Object>> listCard = new ArrayList<>();
       for(MallUser vo:byUserId){
           list.add(vo.getId());
       }
       Set<MallUser> users = new HashSet<>();
       /**购买达人订单**/
       List<MallOrder> talentOrderByUserId = MallOrderService.getTalentOrderByUserId(list);
       for(MallUser vo:byUserId){
           /**拿到这个人所有的订单*/
           List<MallOrder> collect = talentOrderByUserId.stream().filter(order -> order.getUserId().equals(vo.getId())).collect(Collectors.toList());
           // 拿到最早订单判断是否分享购买，是分享购买出现在列表 否则不出现
           Optional<MallOrder> older3 = collect.stream().collect(Collectors.minBy(Comparator.comparing(MallOrder::getAddTime)));
           for(MallOrder order : talentOrderByUserId){
            if(order.getUserId().equals(vo.getId())){
                if(!older3.get().getBecomeVip()){
                    users.add(vo);
                }
            }
         }
       }

       /**用户购买卡*/
       List<MallUserCard> byIdsCard = mallUserCardService.findByIdsCard(list);
       for(MallUser vo:users){
           Map<String,Object> cardMap = new HashMap<>();
           for(int i=0;i<byIdsCard.size();i++){
               if(vo.getId().equals(byIdsCard.get(i).getUserId())){
                   cardMap.put("avatar",vo.getAvatar());
                   if(StringUtil.isBlank(vo.getNickname()) && !StringUtil.isBlank(vo.getUsername())){
                       cardMap.put("nickname",vo.getUseNumber());
                   }else if(StringUtil.isBlank(vo.getNickname()) && StringUtil.isBlank(vo.getUsername())){
                       cardMap.put("nickname",vo.getMobile());
                   }else{
                       cardMap.put("nickname",vo.getNickname());
                   }
                   cardMap.put("mobile",vo.getMobile());
                   cardMap.put("createTime",byIdsCard.get(i).getCreateTime());
               }
           }
           listCard.add(cardMap);
       }
       /**累计奖励*/
       BigDecimal amt = marketingSignRecordService.queryTalent(userId);
       Map<String,Object> map = new HashMap<>();
       map.put("amt",amt);
       map.put("userList",listCard);
       if(null != byUserId){
           map.put("total",users.size());
       }else{
           map.put("total","0");
       }
       return ResponseUtil.ok(map);
   }


 }
