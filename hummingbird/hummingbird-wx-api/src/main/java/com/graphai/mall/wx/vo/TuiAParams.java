package com.graphai.mall.wx.vo;

import lombok.Data;

/**
 * @Author: jw
 * @Date: 2021/1/30 13:37
 */

 @Data
public class TuiAParams {

        private String imei ;
        private String idfa ;
        private String oaid ;
        private String device_id ;
        private String api_version ;

}
