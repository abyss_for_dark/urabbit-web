package com.graphai.mall.wx.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HttpClientUtils {
	
	private static final String IMAGE_TYPE = ".*\\.((jpg)|(jpeg)|(png)|(gif)|(bmp)|(png)|(dxf)|(tiff)|(pcx))";
	
	public static String doPost(String path, Map<String, String> paramMap) throws Exception {
		return doPost(path, paramMap, "UTF-8");
	}
	
	public static String doPost(String path, Map<String, String> paramMap, String charset) throws Exception {
		charset = StringUtils.isBlank(charset) ? "UTF-8" : charset;
		
		List<NameValuePair> paramsList = new ArrayList<NameValuePair>();
		if(paramMap != null) {
			for(String key : paramMap.keySet()) {
				paramsList.add(new BasicNameValuePair(key, paramMap.get(key)));
			}
		}
		
		HttpPost httpPost = new HttpPost(path);
		HttpEntity entity = new UrlEncodedFormEntity(paramsList, charset);
		httpPost.setEntity(entity);
		
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpResponse httpResponse = httpclient.execute(httpPost);
		entity = httpResponse.getEntity();
		
		String result = "";
		if(entity != null) result = EntityUtils.toString(entity, charset);
		
		httpclient.close();
		
		return result.replace("\r\n", "");
	}
	
	public static String doGet(String path, Map<String, String> paramMap) throws Exception {
		return doGet(path, paramMap, "UTF-8");
	}
	
	public static String doGet(String path, Map<String, String> paramMap,String charset) throws Exception {
		if(paramMap != null) {
			StringBuilder sb = new StringBuilder();
			for(String key : paramMap.keySet()) {
				sb.append("&").append(key).append("=").append(paramMap.get(key));
			}
			if(sb.length()>0){
				if(path.indexOf("?")==-1){
					sb.replace(0, 1, "?");
				}
				path+=sb.toString();
				System.out.println(path);
			}
		}
		return doGet(path,charset);
	}
	public static String doGet(String path) throws Exception{
		return doGet(path, "UTF-8");
	}
	public static String doGet(String path, String charset) throws Exception {
		
		charset = StringUtils.isBlank(charset) ? "UTF-8" : charset;
		
		HttpGet httpGet = new HttpGet(path);
		
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpResponse httpResponse = httpclient.execute(httpGet);
		HttpEntity entity = httpResponse.getEntity();
		
		String result = "";
		if(entity != null) result = EntityUtils.toString(entity, charset);
		
		httpclient.close();
		
		return result.replace("\r\n", "");
	}
	
	/**
	 * URL调用data端相应的服务
	 * @param path
	 * @param paramMap
	 * @param resultType
	 * @return
	 * @throws Exception
	 */
	public static <T> T doPost(String path, Map<String, String> paramMap, final Class<T> resultType) throws Exception {
		return doPost(path, paramMap, resultType, "UTF-8");
	}
	
	/**
	 * URL调用data端相应的服务
	 * @param path
	 * @param paramMap
	 * @param resultType
	 * @param charset 默认UTF-8
	 * @return
	 * @throws Exception
	 */
	public static <T> T doPost(String path, Map<String, String> paramMap, final Class<T> resultType, String charset) throws Exception {
		charset = StringUtils.isBlank(charset) ? "UTF-8" : charset;
		
		ResponseHandler<T> responseHandler = getResponseHandler(resultType, charset);
		
		List<NameValuePair> paramsList = new ArrayList<NameValuePair>();
		for(String key : paramMap.keySet()) {
			paramsList.add(new BasicNameValuePair(key, paramMap.get(key)));
		}
		
		HttpPost httpPost = new HttpPost(path);
		HttpEntity entity = new UrlEncodedFormEntity(paramsList, charset);
		httpPost.setEntity(entity);
		
		CloseableHttpClient httpclient = HttpClients.createDefault();
		T result = httpclient.execute(httpPost, responseHandler);
		httpclient.close();
		
		return result;
	}
	
	public static <T> ResponseHandler<T> getResponseHandler(final Class<T> clazz, final String charset) {
		ResponseHandler<T> responseHandler = new ResponseHandler<T>() {
			@Override
			@SuppressWarnings("unchecked")
		    public T handleResponse(final HttpResponse response) throws IOException {
		        StatusLine statusLine = response.getStatusLine();
		        if(statusLine.getStatusCode() >= 300) 
		            throw new HttpResponseException(statusLine.getStatusCode(), statusLine.getReasonPhrase());
		        
		        HttpEntity httpEntity = response.getEntity();
		        if(httpEntity == null) 
		            throw new ClientProtocolException("Response contains no content");
		        
		        Reader reader = new InputStreamReader(httpEntity.getContent(), Charset.forName(charset));
		        if(clazz.getAnnotation(XmlRootElement.class) != null) {
		        	try {
						JAXBContext ctx = JAXBContext.newInstance(clazz);
						Unmarshaller unmarshaller = ctx.createUnmarshaller();
						return (T) unmarshaller.unmarshal(new StringReader(EntityUtils.toString(httpEntity, charset).trim()));
					} catch(JAXBException e) {
						return null;
					}
		        }
		        
		        Gson gson = new GsonBuilder().create();
		        return gson.fromJson(reader, clazz);
		    }
		};
		
		return responseHandler;
	}
	
	public static String doPostText(String url,String text){
		return doPostText(url, text, "UTF-8");
	}
	/**
	 * 使用post方式发生文本信息
	 * @param url
	 * @param text
	 * @param charset
	 * @return
	 */
	public static String doPostText(String url,String text,String charset){
		if(charset==null){
			charset = "UTF-8";
		}
		HttpPost httpPost = new HttpPost(url);
		CloseableHttpClient httpclient = HttpClients.createDefault();
		try {
			StringEntity entity = new StringEntity(text,charset);
			httpPost.setEntity(entity);
			HttpResponse httpResponse = httpclient.execute(httpPost);
			HttpEntity responseEntity = httpResponse.getEntity();
			String result = "";
			if(entity != null) result = EntityUtils.toString(responseEntity, charset);
			httpclient.close();
			return result;
		} catch (ClientProtocolException e) {
			System.out.println(e.getMessage());
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		return null;
	}
	
	public static void main(String[] args) throws Exception {
		Map map = new HashMap();
		map.put("grant_type", "client_credential");
		map.put("appid", "wx170b27eeff318918");
		map.put("secret", "00f014535259edebe3057d6d838c8aac");
		/*String s = doGet("https://api.weixin.qq.com/cgi-bin/token",map);
		System.out.println(s);*/
		String but = "{\"button\": [{\"type\": \"click\", \"name\": \"首页\", \"key\": \"aaaa\" },{\"type\": \"click\", \"name\": \"首页b\", \"key\": \"aaaab\" }]}";
		System.out.println(but);
		String s = doPostText("https://api.weixin.qq.com/cgi-bin/menu/create?access_token=rfkAdX5V-e2vt2dsKvusIADy4SUfr1W7Eel77Mkjhnzt5KU6aaUxFGk6pnoRG3P8E9nm9K8xCDOq7THR6GRJ-FmWWB_4N5y-t0afgk9Us3Q", but, "UTF-8");
		System.out.println(s);
	}
}