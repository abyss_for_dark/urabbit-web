package com.graphai.mall.wx.web.goods;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.github.pagehelper.PageInfo;
import com.graphai.commons.constant.MallConstant;
import com.graphai.commons.util.AppIdAlgorithm;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.dataokutil.HttpUtils;
import com.graphai.commons.util.dataokutil.SignMD5Util;
import com.graphai.commons.util.httprequest.HttpUtil;
import com.graphai.commons.util.image.ImageUtils;
import com.graphai.commons.util.ip.IpUtils;
import com.graphai.commons.util.json.JacksonUtil;
import com.graphai.commons.util.json.JacksonUtils;
import com.graphai.framework.storage.StorageService;
import com.graphai.id.GraphaiIdGenerator;
import com.graphai.mall.admin.domain.*;
import com.graphai.mall.admin.service.*;
import com.graphai.mall.core.system.SystemConfig;
import com.graphai.mall.db.constant.PlateFromConstant;
import com.graphai.mall.db.constant.account.AccountStatus;
import com.graphai.mall.db.constant.account.MallUserProfitNewEnum;
import com.graphai.mall.db.constant.user.MallUserEnum;
import com.graphai.mall.db.domain.*;
import com.graphai.mall.db.domain.mall.GoodsAllinone;
import com.graphai.mall.db.dto.MallGroupGoodsDTO;
import com.graphai.mall.db.service.advertisement.MallBrandService;
import com.graphai.mall.db.service.category.MallCategoryService;
import com.graphai.mall.db.service.common.AppVersionService;
import com.graphai.mall.db.service.coupon.MallCouponService;
import com.graphai.mall.db.service.distribution.FenXiaoService;
import com.graphai.mall.db.service.distribution.MallDistributionService;
import com.graphai.mall.db.service.game.GameRedpacketRainService;
import com.graphai.mall.db.service.goods.*;
import com.graphai.mall.db.service.groupon.MallGrouponRulesService;
import com.graphai.mall.db.service.index.IndexGoodsComponent;
import com.graphai.mall.db.service.order.MallOrderService;
import com.graphai.mall.db.service.promotion.MallSelectionService;
import com.graphai.mall.db.service.red.RedStatus;
import com.graphai.mall.db.service.red.WxRedRankService;
import com.graphai.mall.db.service.red.WxRedService;
import com.graphai.mall.db.service.search.MallSearchHistoryService;
import com.graphai.mall.db.service.sysconfig.MallSystemConfigService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.service.user.behavior.MallCollectService;
import com.graphai.mall.db.service.user.behavior.MallCommentService;
import com.graphai.mall.db.service.user.behavior.MallFootprintService;
import com.graphai.mall.db.service.user.behavior.MallIssueService;
import com.graphai.mall.db.util.*;
import com.graphai.mall.db.util.account.bobbi.AccountSFUtils;
import com.graphai.mall.db.util.account.bobbi.AccountZYUtils;
import com.graphai.mall.db.vo.MallGoodsDTO;
import com.graphai.mall.setting.domain.FreightTemplate;
import com.graphai.mall.setting.service.FreightTemplateService;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.mall.wx.util.GoodsUtils;
import com.graphai.mall.wx.util.Md5Utils;
import com.graphai.mall.wx.web.common.MyBaseController;
import com.graphai.open.api.getway.HttpRequestUtils;
import com.graphai.open.mallgoodsproduct.mapper.IMallGoodsProductMapper;
import com.graphai.properties.JdProperty;
import com.graphai.properties.PlatFormServerproperty;
import com.graphai.properties.TaoBaoProperties;
import com.graphai.validator.Order;
import com.graphai.validator.Sort;
import com.jd.open.api.sdk.DefaultJdClient;
import com.jd.open.api.sdk.JdClient;
import com.jd.open.api.sdk.JdException;
import com.pdd.pop.sdk.common.util.JsonUtil;
import com.pdd.pop.sdk.http.PopClient;
import com.pdd.pop.sdk.http.PopHttpClient;
import com.pdd.pop.sdk.http.api.pop.request.PddDdkGoodsDetailRequest;
import com.pdd.pop.sdk.http.api.pop.request.PddDdkGoodsZsUnitUrlGenRequest;
import com.pdd.pop.sdk.http.api.pop.response.PddDdkGoodsDetailResponse;
import com.pdd.pop.sdk.http.api.pop.response.PddDdkGoodsZsUnitUrlGenResponse;
import com.taobao.api.DefaultTaobaoClient;
import com.taobao.api.TaobaoClient;
import com.taobao.api.request.TbkActivityInfoGetRequest;
import com.taobao.api.response.TbkActivityInfoGetResponse;
import com.xxl.job.core.util.UrlAnalysisUtil;
import jd.union.open.goods.query.request.GoodsReq;
import jd.union.open.goods.query.request.UnionOpenGoodsQueryRequest;
import jd.union.open.goods.query.response.*;
import jd.union.open.promotion.bysubunionid.get.request.UnionOpenPromotionBysubunionidGetRequest;
import jd.union.open.promotion.bysubunionid.get.response.UnionOpenPromotionBysubunionidGetResponse;
import jd.union.open.promotion.common.get.request.PromotionCodeReq;
import jd.union.open.promotion.common.get.request.UnionOpenPromotionCommonGetRequest;
import jd.union.open.promotion.common.get.response.UnionOpenPromotionCommonGetResponse;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Header;
import org.apache.http.message.BasicHeader;
import org.apache.shiro.SecurityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.time.LocalDateTime;
import java.util.List;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;
import java.util.stream.Collectors;

import static com.graphai.commons.util.servlet.ServletUtils.getRequest;

/**
 * 商品服务
 */
@RestController
@RequestMapping("/wx/goods")
@Validated
public class WxGoodsController extends MyBaseController {
    private final Log logger = LogFactory.getLog(WxGoodsController.class);
    //房淘搜索
    private static final String URL = "http://web.51fangtao.com/wx/all/search/createOrUpdateIndexById?id=";
    private static final String DELETE_URL = "http://web.51fangtao.com/wx/all/search/deleteIndexById?id=";

    @Autowired
    private IMallUserMerchantService mallUserMerchantService;
    @Autowired
    private MallGoodsAttributeService mallGoodsAttributeService;
    @Autowired
    private IMallGoodsCategoryService mallGoodsCategoryService;
    @Autowired
    private MallGoodsProductService mallGoodsProductService;
    @Autowired
    private MallGoodsSpecificationService mallGoodsSpecificationService;
    @Autowired
    private FenXiaoService fenXiaoService;
    @Autowired
    private MallGoodsService goodsService;

    @Autowired
    private MallGoodsProductService productService;

    @Autowired
    private MallIssueService goodsIssueService;

    @Autowired
    private MallGoodsAttributeService goodsAttributeService;

    @Autowired
    private MallBrandService brandService;

    @Autowired
    private MallCommentService commentService;

    @Autowired
    private MallUserService userService;

    @Autowired
    private MallCollectService collectService;

    @Autowired
    private MallFootprintService footprintService;

    @Autowired
    private MallCategoryService categoryService;

    @Autowired
    private MallSearchHistoryService searchHistoryService;

    @Autowired
    private MallGoodsSpecificationService goodsSpecificationService;

    @Autowired
    private MallGrouponRulesService rulesService;

    @Autowired
    private StorageService storageService;

    @Autowired
    private MallGoodsCodeSeqService goodsCodeSeqService;

    @Autowired
    private MallCouponService couponService;

    @Autowired
    private MallDistributionService distributionService;


    @Autowired
    private TaoBaoProperties taoBaoProperties;

    @Autowired
    private PlatFormServerproperty platFormServerproperty;

    @Autowired
    private MallGoodsCouponCodeService MallGoodsCouponCodeService;

    @Autowired
    private MallOrderService MallOrderService;

    @Autowired
    private AppVersionService appVersionService;

    @Autowired
    private MallSystemConfigService mallSystemConfigService;

    @Autowired
    private JdProperty jdProperty;

    @Autowired
    private IMallGoodsProductMapper mallGoodsProductMapper;

    @Autowired
    private IMallUserProfitNewService iMallUserProfitNewService;

    @Autowired
    private GameRedpacketRainService gameRedpacketRainService;

    @Autowired
    private IMallPageViewService mallPageViewService;

    @Autowired
    private WxRedService wxRedService;

    @Resource
    private MallSelectionService mallSelectionService;

    @Autowired
    private WxRedRankService wxRedRankService;

    @Autowired
    private IMallGoodsUserService mallGoodsUserService;

    @Autowired
    private IndexGoodsComponent indexGoodsComponent;

    /**
     * 商品详情
     * <p>
     * 用户可以不登录。 如果用户登录，则记录用户足迹以及返回用户收藏信息。
     *
     * @param userId 用户ID
     * @param id     商品ID
     * @return 商品详情
     * @throws IOException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    @GetMapping("detailV2")
    public Object detailV2(@LoginUser String userId, HttpServletRequest mrequest, String id, String shId, String idClass, String virtualGood, String srcGoodsId, String industryId,
                           @RequestParam(value = "version", required = false) String version) throws Exception {


        MallGoods info = null;
        MallGoodsDTO mallGoodsDTO = null;
        JSONObject jsonCoupon = null;
        Map<String, Object> data = new HashMap<>();
        BigDecimal profit = null;

        if (org.apache.commons.lang.StringUtils.isNotBlank(id)) {
            String url = platFormServerproperty.getUrl(MallConstant.TAOBAO);
            Map<String, Object> mappMap = new HashMap<String, Object>();
            mappMap.put("plateform", "dtk");
            mappMap.put("id", id);

            Header[] headers = new BasicHeader[]{new BasicHeader("method", "getGoodsInfoById"), new BasicHeader("version", "v1.0")};

            String result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);

            JSONObject object = JSONObject.parseObject(result);

            if (!object.getString("errno").equals("0")) {
                return ResponseUtil.fail();
            }
            result = object.get("data").toString();
            JSONObject jsonObject = object.getJSONObject("data");
            System.out.println("-----" + jsonObject.get("coupon"));
            if (ObjectUtil.isNotNull(jsonObject.get("coupon"))) {
                jsonCoupon = jsonObject.parseObject(jsonObject.get("coupon").toString());
                data.put("coupon", jsonCoupon);
            }

            if (ObjectUtil.isNotNull(jsonCoupon) && org.apache.commons.lang3.StringUtils.isNotBlank(jsonCoupon.getString("srcCouponUrl"))) {

            }

            mallGoodsDTO = JSONObject.parseObject(jsonObject.get("goods").toString(), MallGoodsDTO.class);


        }

        if (org.apache.commons.lang3.StringUtils.isNotBlank(srcGoodsId)) {

            // if (com.graphai.commons.util.ObjectUtils.isNotNull(user)) {
            // 淘宝
            if (Objects.equals(industryId, "4")) {

                // String token = mrequest.getHeader("X-Mall-Token");
                // if (org.apache.commons.lang3.StringUtils.isBlank(token)) {
                // return ResponseUtil.unlogin();
                // }

                JSONObject goodObject = goodsService.insertGoodsByOpen(srcGoodsId);
                if (goodObject.isEmpty()) {
                    return ResponseUtil.fail(502, "抱歉,您查看的商品尚未被收录");
                }
                System.out.println("结果" + goodObject);
                JSONObject coupJsonObject = goodObject.parseObject(goodObject.get("coupon").toString());
                if (ObjectUtil.isNotNull(coupJsonObject)) {
                    data.put("coupon", coupJsonObject);
                }
                mallGoodsDTO = JSONObject.parseObject(goodObject.get("goods").toString(), MallGoodsDTO.class);
                if (org.apache.commons.lang3.StringUtils.isNotBlank(coupJsonObject.getString("discount"))) {
                    mallGoodsDTO.setCouponDiscount(new BigDecimal(coupJsonObject.getString("discount")));
                }

                if (org.apache.commons.lang3.StringUtils.isNotBlank(coupJsonObject.getString("min"))) {
                    mallGoodsDTO.setMin(new BigDecimal(coupJsonObject.getString("min")));
                }

                id = mallGoodsDTO.getId();

            }

            // 拼多多 苏宁 京东
            else if (Objects.equals(industryId, "8") || Objects.equals(industryId, "19") || Objects.equals(industryId, "5")) {
                JSONObject coupJsonObject = null;
                JSONObject goodObject = goodsService.saveOpenGoodsRds(industryId, srcGoodsId);
                System.out.println("结果" + goodObject);
                if (ObjectUtil.isNotNull(goodObject.get("coupon"))) {
                    coupJsonObject = goodObject.parseObject(goodObject.get("coupon").toString());
                }
                mallGoodsDTO = JSONObject.parseObject(goodObject.get("goods").toString(), MallGoodsDTO.class);
                if (ObjectUtil.isNotNull(coupJsonObject)) {
                    data.put("coupon", coupJsonObject);
                    mallGoodsDTO.setCouponDiscount(new BigDecimal("0"));
                    if (org.apache.commons.lang3.StringUtils.isNotBlank(coupJsonObject.getString("discount"))) {
                        mallGoodsDTO.setCouponDiscount(new BigDecimal(coupJsonObject.getString("discount")));
                    }

                    if (org.apache.commons.lang3.StringUtils.isNotBlank(coupJsonObject.getString("min"))) {
                        mallGoodsDTO.setMin(new BigDecimal(coupJsonObject.getString("min")));
                    }

                    if (ObjectUtil.isNotNull(coupJsonObject) && org.apache.commons.lang3.StringUtils.isNotBlank(coupJsonObject.getString("srcCouponUrl"))) {


                    }
                    id = mallGoodsDTO.getId();
                }

            }

            // 算出返佣金额
            if (ObjectUtil.isNotNull(mallGoodsDTO) && ObjectUtil.isNotNull(mallGoodsDTO.getCommissionRate()) && ObjectUtil.isNotNull(mallGoodsDTO.getRetailPrice())) {
                BigDecimal num1 = new BigDecimal(mallGoodsDTO.getCommissionRate());
                BigDecimal rate = num1.divide(new BigDecimal(100));

                profit = mallGoodsDTO.getRetailPrice().multiply(rate).multiply(new BigDecimal("0.9")).setScale(2, BigDecimal.ROUND_DOWN);
                // 淘宝需要额外扣除10%的技术服务费
                // if (Objects.equals("4", mallGoodsDTO.getBussinessId())) {
                // profit = profit.multiply(new BigDecimal("0.9"));
                // }
            }

            if (profit != null && profit.doubleValue() >= 0 && org.apache.commons.lang3.StringUtils.isNotBlank(mallGoodsDTO.getBussinessId())) {

                Map<String, Object> map = null;
                // 查询分润计算方式(key_value 0:服务商、区代、市代方式 1:用户firstLeader、secondLeader、thirderLeader方式 )
                Map<String, String> profitTypeMap = mallSystemConfigService.listProfitType();
                String profitType = profitTypeMap.get("Mall_profit_type");
                if (Objects.equals(profitType, "0")) {
                    map = (Map<String, Object>) AccountSFUtils.getProfit1(userId, mallGoodsDTO.getBussinessId(), profit, "JL20002", MallUserProfitNewEnum.PROFIT_TYPE_1.strCodeInt());
                } else {
                    /** 方式二：旧模式（firstLeader、secondLeader、thirderLeader方式）--【第三方购物和权益商品】 **/
                    map = (Map<String, Object>) AccountSFUtils.getProfit2(userId, mallGoodsDTO.getBussinessId(), profit, "JL20002");
                }

                if (ObjectUtil.isNotNull(map) && map.containsKey("commissionReward")) {
                    BigDecimal commissionfee = new BigDecimal(0.00);
                    if (ObjectUtil.isNotNull(map.get("commissionReward"))) {
                        commissionfee = new BigDecimal(map.get("commissionReward").toString());
                    }
                    mallGoodsDTO.setRebateAmount(commissionfee);
                } else {
                    mallGoodsDTO.setRebateAmount(new BigDecimal(0.0));
                }

            }
            // }

        }

        info = new MallGoods();

        try {
            BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
            BeanUtils.copyProperties(info, mallGoodsDTO);
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }


        String oldJumpUrl = info.getJumpUrl();


        // 拼多多
        if (6 == info.getPlatform() && StringHelper.isNotNullAndEmpty(oldJumpUrl)) {
            profit = info.getRebateAmount();


        }
        if (org.apache.commons.lang3.StringUtils.isEmpty(srcGoodsId)) {
            // else {
            // 算出返佣金额
            if (ObjectUtil.isNotNull(info.getCommissionRate()) && ObjectUtil.isNotNull(info.getRetailPrice())) {
                BigDecimal num1 = new BigDecimal(info.getCommissionRate());
                BigDecimal rate = num1.divide(new BigDecimal(100));
                profit = info.getRetailPrice().multiply(rate).multiply(new BigDecimal("0.9")).setScale(2, BigDecimal.ROUND_DOWN);

            }
            // }

            if (profit != null && profit.doubleValue() > 0 && org.apache.commons.lang3.StringUtils.isNotBlank(info.getBussinessId())) {

                // Map<String, Object> map = (Map<String, Object>) AccountUtils.getProfit2(userId,
                // info.getBussinessId(), profit, "JL20002");
                Map<String, Object> map = null;
                // 查询分润计算方式(key_value 0:服务商、区代、市代方式 1:用户firstLeader、secondLeader、thirderLeader方式 )
                Map<String, String> profitTypeMap = mallSystemConfigService.listProfitType();
                String profitType = profitTypeMap.get("Mall_profit_type");
                if (Objects.equals(profitType, "0")) {
                    map = (Map<String, Object>) AccountSFUtils.getProfit1(userId, mallGoodsDTO.getBussinessId(), profit, "JL20002", MallUserProfitNewEnum.PROFIT_TYPE_1.strCodeInt());
                } else {
                    /** 方式二：旧模式（firstLeader、secondLeader、thirderLeader方式）--【第三方购物和权益商品】 **/
                    map = (Map<String, Object>) AccountSFUtils.getProfit2(userId, mallGoodsDTO.getBussinessId(), profit, "JL20002");
                }

                if (ObjectUtil.isNotNull(map) && map.containsKey("commissionReward")) {
                    BigDecimal commissionfee = new BigDecimal(0.00);
                    if (ObjectUtil.isNotNull(map.get("commissionReward"))) {
                        commissionfee = new BigDecimal(map.get("commissionReward").toString());
                    }
                    info.setRebateAmount(commissionfee);
                } else {
                    info.setRebateAmount(new BigDecimal(0.0));
                }

            }
        }

        // }

        if (ObjectUtil.isNotNull(jsonCoupon)) {

            // 添加优惠金额和满减金额
            if (org.apache.commons.lang3.StringUtils.isNotBlank(jsonCoupon.getString("discount"))) {
                info.setCouponDiscount(new BigDecimal(jsonCoupon.getString("discount")));
            } else {
                info.setCouponDiscount(new BigDecimal("0.0"));
            }

            if (org.apache.commons.lang3.StringUtils.isNotBlank(jsonCoupon.getString("min"))) {
                info.setMin(new BigDecimal(jsonCoupon.getString("min")));
            }
        } else if (com.graphai.commons.util.obj.ObjectUtils.isNull(jsonCoupon) && org.apache.commons.lang.StringUtils.isEmpty(srcGoodsId)) {
            info.setCouponDiscount(new BigDecimal("0.0"));
        }

        // info.setCouponDiscount(
        // info.getRebateAmount().add(info.getCouponDiscount()).setScale(2, BigDecimal.ROUND_HALF_UP));

        // }

        // this.syncGoodsImg(info); // 判断是否需要合成图片 added by sami 190627
        this.syncGoodsImg(info);
        // try {
        // BeanUtils.copyProperties(info, mallGoodsDTO);
        // } catch (IllegalAccessException | InvocationTargetException e) {
        // e.printStackTrace();
        // }

        List<String> myDList = new ArrayList<String>();
        try {

        } catch (Exception e1) {
            logger.error("商品详情解析json异常" + e1.getMessage());
        }

        // 记录用户的足迹 异步处理
        String goodsId = id;
        if (userId != null) {
            executorService.execute(() -> {
                MallFootprint footprint = new MallFootprint();
                footprint.setUserId(userId);
                footprint.setGoodsId(goodsId);
                footprintService.add(footprint);

                MallUser uUser = new MallUser();
                uUser.setId(userId);
                uUser.setUpdateTime(LocalDateTime.now());
                uUser.setFootprintId(footprint.getId());
                userService.updateById(uUser);

            });
        }

        try {

            /*** 小花咪电商：需要计算返利金额，并且是根据不同的代理基本进行计算 */
            Byte userLevl = 0;
            List<Map<String, Object>> list = new LinkedList<>();
            Map<String, Object> map = new HashMap<>();
            map.put("question", "除了优惠券外还有其他优惠吗？");
            map.put("answer", "有的，您每购买一次，均可获得商品页面所显示的返佣金额，办理未莱生活特权卡（限期免费）可获得更多权益优惠。");
            Map<String, Object> map1 = new HashMap<>();
            map1.put("question", "为什么领券提示优惠券已失效？");
            map1.put("answer", "原因有两个：1、优惠券已被领完；2、商家已取消宝贝优惠券");
            Map<String, Object> map2 = new HashMap<>();
            map2.put("question", "领券下单一直无法跳转怎么解决？");
            map2.put("answer", "这是应用平台接口问题引起的卡顿，先返回重试多点几次或重启手机");
            Map<String, Object> map3 = new HashMap<>();
            map3.put("question", "优惠券是永久有效吗？");
            map3.put("answer", "优惠券是商家为推广商品设置，限时限量，领完为止，过期无效。");
            list.add(map);
            list.add(map1);
            list.add(map2);
            list.add(map3);
            data.put("info", info);
            data.put("issue", list);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return ResponseUtil.ok(data);
    }


    /**
     * 其他平台转链
     *
     * @throws Exception
     */
    /**
     * @param userId           用户id
     * @param mrequest
     * @param param-jumpUrl    购买链接
     * @param param-couponUrl  优惠券链接
     * @param param-industryId 业务形态id
     * @return
     * @throws Exception
     */
    @PostMapping("/getLink")
    public Object getLink(@LoginUser String userId, HttpServletRequest mrequest, @RequestBody Map<String, Object> param) throws Exception {

        String version = org.apache.commons.lang3.StringUtils.isNotBlank(mrequest.getHeader("version")) ? mrequest.getHeader("version") : "1.6.6";

        String jdAppSecret = "";
        String jdAppKey = "";
        String jdAppId = "";

        if (appVersionService.getTopOneAppVersion(version)) {
            jdAppSecret = jdProperty.getNewAppSecret();
            jdAppKey = jdProperty.getNewAppKey();
            jdAppId = jdProperty.getNewAppId();
        } else {
            jdAppSecret = jdProperty.getOldAppSecret();
            jdAppKey = jdProperty.getOldAppKey();
            jdAppId = jdProperty.getOldAppId();
        }

        String jumpUrl = String.valueOf(param.get("jumpUrl"));
        String couponUrl = String.valueOf(param.get("couponUrl"));
        String bussinessId = String.valueOf(param.get("bussinessId"));
        logger.info("转链入参" + cn.hutool.json.JSONUtil.toJsonStr(param));
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        Map<String, Object> map = new HashMap<String, Object>();
        try {

            String newJumpUrl = "";
            String shareUrl = "";

            MallUser user = userService.findById(userId);

            if (Objects.equals(user, null)) {
                return ResponseUtil.badArgumentValue("未查询到用户信息！");
            }
            // 苏宁
            if (Objects.equals("19", bussinessId)) {
                newJumpUrl = jumpUrl + "?sub_user=" + user.getId();

            }

            // 京东
            if (Objects.equals("5", bussinessId) && StringHelper.isNotNullAndEmpty(jumpUrl)) {
                // 推广长连接
                JdClient client = new DefaultJdClient(PlateFromConstant.JDGoodsList, "", jdAppKey, jdAppSecret);
                UnionOpenPromotionCommonGetRequest request = new UnionOpenPromotionCommonGetRequest();
                PromotionCodeReq promotionCodeReq = new PromotionCodeReq();
                promotionCodeReq.setExt1(user.getId());
                promotionCodeReq.setPositionId(Long.valueOf(user.getId()));
                promotionCodeReq.setMaterialId(jumpUrl);
                promotionCodeReq.setSiteId(jdAppId);
                if (org.apache.commons.lang3.StringUtils.isNotBlank(couponUrl)) {
                    promotionCodeReq.setCouponUrl(couponUrl);
                }
                promotionCodeReq.setSubUnionId(user.getId());
                request.setPromotionCodeReq(promotionCodeReq);
                try {
                    UnionOpenPromotionCommonGetResponse response = client.execute(request);
                    logger.info("京东转链结果" + response.getMessage());
                    if (response.getCode() == 200) {
                        String clickURL = response.getData().getClickURL();
                        // 京东比较特殊，有插件，需要去掉http
                        if (clickURL.contains("http") || clickURL.contains("https")) {
                            clickURL = clickURL.substring(clickURL.indexOf("//") + 2);
                        }
                        newJumpUrl = clickURL;
                    }
                    // 若优惠券过期或优惠券消费限额必须小于商品最低价格则优惠券链接传空
                    if (response.getCode() == 2001906 || response.getCode() == 2001905 || response.getCode() == 2001911) {
                        promotionCodeReq.setCouponUrl(null);
                        request.setPromotionCodeReq(promotionCodeReq);
                        UnionOpenPromotionCommonGetResponse response2 = client.execute(request);
                        if (response2.getCode() == 200) {
                            String clickURL = response2.getData().getClickURL();
                            // 京东比较特殊，有插件，需要去掉http
                            if (clickURL.contains("http") || clickURL.contains("https")) {
                                clickURL = clickURL.substring(clickURL.indexOf("//") + 2);
                            }
                            newJumpUrl = clickURL;
                        }

                    }
                } catch (JdException e) {
                    e.printStackTrace();
                }

                // 京东分享链接(短连接)
                JdClient shareClient = new DefaultJdClient(PlateFromConstant.JDGoodsList, "", jdAppKey, jdAppSecret);
                UnionOpenPromotionBysubunionidGetRequest shareRequest = new UnionOpenPromotionBysubunionidGetRequest();
                jd.union.open.promotion.bysubunionid.get.request.PromotionCodeReq promotionCodeReq2 = new jd.union.open.promotion.bysubunionid.get.request.PromotionCodeReq();
                promotionCodeReq2.setPositionId(Long.valueOf(user.getId()));
                promotionCodeReq2.setMaterialId(jumpUrl);
                promotionCodeReq2.setSubUnionId(user.getId());
                promotionCodeReq2.setChainType(2);
                if (org.apache.commons.lang3.StringUtils.isNotBlank(couponUrl)) {
                    promotionCodeReq2.setCouponUrl(couponUrl);
                }
                shareRequest.setPromotionCodeReq(promotionCodeReq2);
                // 若优惠券过期或优惠券消费限额必须小于商品最低价格则优惠券链接传空
                try {
                    UnionOpenPromotionBysubunionidGetResponse shareResponse = shareClient.execute(shareRequest);
                    logger.info("京东分享链接结果" + shareResponse.getMessage());
                    if (shareResponse.getCode() == 200) {
                        String shortUrl = shareResponse.getData().getShortURL();
                        if (org.apache.commons.lang3.StringUtils.isNotEmpty(shortUrl)) {
                            shareUrl = shortUrl;
                        }
                    }

                    if (shareResponse.getCode() == 2001906 || shareResponse.getCode() == 2001905 || shareResponse.getCode() == 2001911) {
                        promotionCodeReq2.setCouponUrl(null);
                        shareRequest.setPromotionCodeReq(promotionCodeReq2);
                        UnionOpenPromotionBysubunionidGetResponse shareResponse2 = shareClient.execute(shareRequest);
                        if (shareResponse2.getCode() == 200) {
                            String shortUrl = shareResponse2.getData().getShortURL();
                            if (org.apache.commons.lang3.StringUtils.isNotEmpty(shortUrl)) {
                                shareUrl = shortUrl;
                            }
                        }

                    }
                } catch (JdException e) {
                    e.printStackTrace();
                }

            }

            // 拼多多
            if (Objects.equals("8", bussinessId) && StringHelper.isNotNullAndEmpty(jumpUrl)) {

                // 查询是否备案授权
                PopClient client = new PopHttpClient(PlateFromConstant.PDD_CLIENT_ID, PlateFromConstant.PDD_CLIENT_SECRET);
                // PddDdkMemberAuthorityQueryRequest memberAuthorityRequest = new
                // PddDdkMemberAuthorityQueryRequest();
                // memberAuthorityRequest.setPid(PlateFromConstant.PDD_PID);
                // Map<String, Object> uidMap=new HashMap<String, Object>();
                // uidMap.put("uid",userId);
                // memberAuthorityRequest.setCustomParameters(JsonUtil.transferToJson(uidMap));
                // PddDdkMemberAuthorityQueryResponse memberAuthorityresponse =
                // client.syncInvoke(memberAuthorityRequest);
                // logger.info("查询拼多多备案授权"+JsonUtil.transferToJson(memberAuthorityresponse));
                // //若没有备案则需要生成备案链接点击备案
                // if (ObjectUtils.allNotNull(memberAuthorityresponse.getAuthorityQueryResponse(),
                // memberAuthorityresponse.getAuthorityQueryResponse().getBind())) {
                // if (memberAuthorityresponse.getAuthorityQueryResponse().getBind()==0) {
                // PddDdkRpPromUrlGenerateRequest promUrlRequest = new PddDdkRpPromUrlGenerateRequest();
                // promUrlRequest.setChannelType(10);
                // Map<String, Object> paramMap=new HashMap<String, Object>();
                // paramMap.put("uid",userId);
                // promUrlRequest.setCustomParameters(JsonUtil.transferToJson(paramMap));
                // List<String> pIdList = new ArrayList<String>();
                // pIdList.add(PlateFromConstant.PDD_PID);
                // promUrlRequest.setPIdList(pIdList);
                // PddDdkRpPromUrlGenerateResponse promUrlResponse = client.syncInvoke(promUrlRequest);
                // logger.info("拼多多备案授权链接"+JsonUtil.transferToJson(promUrlResponse));
                // List<RpPromotionUrlGenerateResponseUrlListItem> objects=
                // promUrlResponse.getRpPromotionUrlGenerateResponse().getUrlList();
                // if (CollectionUtils.isNotEmpty(objects)) {
                // map.put("promUrl", objects.get(0).getMobileUrl());
                // return ResponseUtil.ok(map);
                // }
                //
                // }
                // }

                // 跳转到备案链接


                // 进行转链接
                // PopClient client = new PopHttpClient(PlateFromConstant.PDD_CLIENT_ID,
                // PlateFromConstant.PDD_CLIENT_SECRET);

                PddDdkGoodsZsUnitUrlGenRequest request = new PddDdkGoodsZsUnitUrlGenRequest();
                request.setPid(PlateFromConstant.PDD_PID);
                request.setSourceUrl(jumpUrl);
                JSONObject uid = new JSONObject();
                uid.put("uid", user.getId());
                request.setCustomParameters(uid.toString());
                PddDdkGoodsZsUnitUrlGenResponse response = null;
                try {
                    response = client.syncInvoke(request);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                JSONObject generateResponse = JSONUtil.getObj(JsonUtil.transferToJson(response), "goods_zs_unit_generate_response");
                if (generateResponse != null) {
                    // String mobile_short_url = generateResponse.getString("url");
                    // String mobile_short_url = generateResponse.getString("short_url");
                    if (org.apache.commons.lang3.StringUtils.isNotEmpty(generateResponse.getString("short_url"))) {
                        shareUrl = generateResponse.getString("short_url");
                        newJumpUrl = generateResponse.getString("short_url");
                    }

                }
            }

            logger.info("推广链接-------------" + newJumpUrl);
            logger.info("分享短链接---------------" + shareUrl);


            if (StringHelper.isNotNullAndEmpty(shareUrl)) {
                map.put("shareUrl", shareUrl);
            }

            if (StringHelper.isNotNullAndEmpty(newJumpUrl)) {
                map.put("newJumpUrl", newJumpUrl);
            }


            return ResponseUtil.ok(map);


        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseUtil.fail();
    }

    /**
     * 唯品会跳转转链接
     *
     * @param userId
     * @return
     * @throws Exception
     */
    @GetMapping("/wph/detail")
    public Object wphDetail(@LoginUser String userId, String link) throws Exception {
        if (org.apache.commons.lang.StringUtils.isBlank(userId)) {
            return ResponseUtil.unlogin();
        }
        if (StringHelper.isNullOrEmpty(link)) {
            return ResponseUtil.badArgumentValue();
        }
        // 转链方式1
        String newJumpUrl = link + "?chanTag=" + userId;
        // 转链方式2
        /*
         * try { com.vip.adp.api.open.service.UnionUrlServiceHelper.UnionUrlServiceClient client = new
         * com.vip.adp.api.open.service.UnionUrlServiceHelper.UnionUrlServiceClient();
         * com.vip.osp.sdk.context.InvocationContext invocationContext =
         * com.vip.osp.sdk.context.InvocationContext.Factory.getInstance();
         * invocationContext.setAppKey(PlateFromConstant.WPH_APPKEY);
         * invocationContext.setAppSecret(PlateFromConstant.WPH_APPSECRET);
         * invocationContext.setAppURL(PlateFromConstant.WPH_APP_URL); invocationContext.setLanguage("zh");
         * java.util.ArrayList<String> urlList = new java.util.ArrayList<String>(); urlList.add(link);
         * UrlGenResponse urlGenResponse = client.genByVIPUrl(urlList, userId,
         * UUID.randomUUID().toString());
         *
         * List<UrlInfo> urlInfoList = urlGenResponse.getUrlInfoList(); for (UrlInfo urlInfo : urlInfoList)
         * { System.out.println(urlInfo.getUrl() + " " + urlInfo.getDeeplinkUrl() + " " +
         * urlInfo.getLongUrl()+ " " + urlInfo.getVipWxUrl()); } } catch
         * (com.vip.osp.sdk.exception.OspException e) { e.printStackTrace(); }
         */
        return ResponseUtil.ok(newJumpUrl);
    }


    /**
     * 活动转链
     *
     * @param link       活动链接、类目链接、广告链接 、等
     * @param userId     用户ID
     * @param industryId 类目ID
     * @return
     */
    @GetMapping("/three/activityLink")
    public Object threeCategoryLink(@LoginUser String userId, @NotNull String industryId, @NotNull String link
            /* String activityId, String id, @NotNull String type */) throws Exception {
        logger.info("开始执行活动转链=-------------------------------");

        String resultLink = "";
        if (org.apache.commons.lang.StringUtils.isBlank(userId)) {
            return ResponseUtil.unlogin();
        }
        if (StringHelper.isNullOrEmpty(link)) {
            return ResponseUtil.badArgumentValue();
        }
        if (StringHelper.isNullOrEmpty(industryId)) {
            return ResponseUtil.badArgumentValue();
        }

        if (Objects.equals("13", industryId)) {
            MallUser user = userService.findById(userId);
            if (com.graphai.commons.util.obj.ObjectUtils.isNotNull(user)) {
                if (org.apache.commons.lang3.StringUtils.isBlank(user.getTbkRelationId()) || org.apache.commons.lang3.StringUtils.isBlank(user.getTbkPubId())) {
                    return ResponseUtil.unTaobaoAuth();
                }
            }
        }

        resultLink = (String) getNewLinkByType(userId, industryId, link);

        if (Objects.equals("-1", resultLink)) {
            return ResponseUtil.fail();
        }
        if (Objects.equals("6", industryId) || Objects.equals("13", industryId)) {
            return ResponseUtil.ok(URLEncoder.encode(resultLink, "UTF-8"));

        }
        return ResponseUtil.ok("/pages/web/webview?link=" + URLEncoder.encode(resultLink, "UTF-8"));


    }


    /**
     * 进行活动链接转链
     *
     * @return
     */
    private Object getNewLinkByType(String userId, String industry_id, String link) {
        logger.info("进行转链开始------------------------------------------------------ " + link);

        // 因之前的格式都加了前缀，所以要兼容去掉
        link = link.replace("/pages/web/webview?link=", "");


        // 苏宁
        if (Objects.equals(industry_id, "11")) {
            if (link.contains("sub_user")) {
                return "-1";
            }
            return link + "?sub_user=" + userId;
        }

        // 唯品会
        if (Objects.equals(industry_id, "12")) {
            if (link.contains("sub_user")) {
                return "-1";
            }
            return link + "?chanTag=" + userId;
        }
        // 京东
        else if (Objects.equals(industry_id, "6")) {
            JdClient client = new DefaultJdClient(PlateFromConstant.JDGoodsList, "", jdProperty.getNewAppKey(), jdProperty.getNewAppSecret());
            UnionOpenPromotionCommonGetRequest request = new UnionOpenPromotionCommonGetRequest();
            PromotionCodeReq promotionCodeReq = new PromotionCodeReq();
            promotionCodeReq.setExt1(userId);
            promotionCodeReq.setPositionId(Long.valueOf(userId));
            promotionCodeReq.setMaterialId(link);
            promotionCodeReq.setSiteId(jdProperty.getNewAppId());

            promotionCodeReq.setSubUnionId(userId);
            request.setPromotionCodeReq(promotionCodeReq);
            try {
                UnionOpenPromotionCommonGetResponse response = client.execute(request);
                logger.info("京东转链结果" + response.getMessage());
                if (response.getCode() == 200) {
                    String clickURL = response.getData().getClickURL();
                    // 京东比较特殊，有插件，需要去掉http
                    return clickURL;
                }
                return "-1";

            } catch (JdException e) {
                e.printStackTrace();
            }


        }
        // 拼多多
        else if (Objects.equals("7", industry_id)) {
            // 进行转链接
            // PopClient client = new PopHttpClient(PlateFromConstant.PDD_CLIENT_ID,
            // PlateFromConstant.PDD_CLIENT_SECRET);
            // PddDdkResourceUrlGenRequest request = new PddDdkResourceUrlGenRequest();
            // request.setPid(PlateFromConstant.PDD_PID);
            // request.setUrl(link);
            // request.setResourceType(null);
            // JSONObject uid = new JSONObject();
            // uid.put("uid", userId);
            // request.setCustomParameters(uid.toString());
            // PddDdkResourceUrlGenResponse response = null;
            // try {
            // response = client.syncInvoke(request);
            // } catch (Exception e) {
            // e.printStackTrace();
            // }
            // if (response == null || response.getResourceUrlResponse() == null) {
            // return "-1";
            // }
            // return response.getResourceUrlResponse().getSingleUrlList().getMobileShortUrl();

            // todo暂时先返回链接
            return link;
        }

        // 淘宝 活动转链
        else if (Objects.equals("13", industry_id)) {
            try {
                MallUser user = userService.findById(userId);
                TaobaoClient client = new DefaultTaobaoClient(PlateFromConstant.DTK_ZL_URL, taoBaoProperties.getLianmengAppKey(), taoBaoProperties.getLianmengAppSecret());
                TbkActivityInfoGetRequest req = new TbkActivityInfoGetRequest();
                req.setAdzoneId(110282750499L);
                req.setActivityMaterialId(link);
                req.setRelationId(Long.parseLong(user.getTbkRelationId()));
                TbkActivityInfoGetResponse rsp = client.execute(req);

                logger.info("获取活动转链结果------------" + JacksonUtils.bean2Jsn(rsp));
                if (rsp.getData() != null) {
                    // String encoderUrl = URLEncoder.encode(rsp.getData().getClickUrl(), "UTF-8");
                    return rsp.getData().getClickUrl();
                }

            } catch (Exception e) {
                e.printStackTrace();
                logger.error(e.getMessage(), e);
                return ResponseUtil.fail();
            }
        }

        return "-1";
    }


    /**
     * 商品详情
     * <p>
     * 用户可以不登录。 如果用户登录，则记录用户足迹以及返回用户收藏信息。
     *
     * @param userId 用户ID
     * @param id     商品ID
     * @return 商品详情
     */
    @GetMapping("detailbak")
    public Object detailBak(@LoginUser String userId, @NotNull String id, String shId) {
        // 商品信息
        MallGoods info = goodsService.findById(id);
        this.syncGoodsImg(info); // 判断是否需要合成图片 added by sami 190627

        List<String> myDList = new ArrayList<String>();
        try {

            // 不是唯品仓同步过来的数据就要做处理 !"WPC".equals(info.getChannelCode())
            if ((org.apache.commons.lang.StringUtils.isEmpty(info.getChannelCode()) || "JW".equalsIgnoreCase(info.getChannelCode())) && info != null && !StringUtils.isEmpty(info.getDetail())) {
                Element doc = Jsoup.parseBodyFragment(info.getDetail()).body();
                Elements pngs = doc.select("img[src]");
                for (Element element : pngs) {
                    String imgUrl = element.attr("src");
                    myDList.add(imgUrl);
                }
                info.setDetail(JacksonUtils.bean2Jsn(myDList));
            }

        } catch (Exception e1) {
            logger.error("商品详情解析json异常" + e1.getMessage());
        }
        // 商品属性
        Callable<List> goodsAttributeListCallable = () -> goodsAttributeService.queryByGid(id);

        // 商品规格 返回的是定制的GoodsSpecificationVo
        Callable<Object> objectCallable = () -> goodsSpecificationService.getSpecificationVoList(id);

        // 商品规格对应的数量和价格
        Callable<List<MallGoodsProduct>> productListCallable = () -> productService.queryByGid(id);

        List<String> fpids = new ArrayList<String>();
        fpids.add("0");
        // 商品问题，这里是一些通用问题
        Callable<List> issueCallable = () -> goodsIssueService.querySelective(MallConstant.ISSUE_TYPE_PRODUCT, fpids, "", 1, 4, "", "");

        // 商品品牌商
        Callable<MallBrand> brandCallable = () -> {
            String brandId = info.getBrandId();
            MallBrand brand;
            if (org.apache.commons.lang3.StringUtils.isBlank(brandId)) {
                brand = new MallBrand();
            } else {
                brand = brandService.findById(info.getBrandId());
            }
            return brand;
        };

        // 评论
        Callable<Map> commentsCallable = () -> {
            List<MallComment> comments = commentService.queryGoodsByGid(id, 1, 2);
            List<Map<String, Object>> commentsVo = new ArrayList<>(comments.size());
            long commentCount = PageInfo.of(comments).getTotal();
            for (MallComment comment : comments) {
                Map<String, Object> c = new HashMap<>();
                c.put("id", comment.getId());
                c.put("addTime", comment.getAddTime());
                c.put("content", comment.getContent());
                MallUser user = userService.findById(comment.getUserId());
                c.put("nickname", user == null ? "" : user.getNickname());
                c.put("avatar", user == null ? "" : user.getAvatar());
                c.put("picList", comment.getPicUrls());
                commentsVo.add(c);
            }
            Map<String, Object> commentList = new HashMap<>();
            commentList.put("count", commentCount);
            commentList.put("data", commentsVo);
            return commentList;
        };

        // 团购信息
        Callable<List> grouponRulesCallable = () -> rulesService.queryByGoodsId(id);

        // 用户收藏
        int userHasCollect = 0;
        if (userId != null) {
            userHasCollect = collectService.count(userId, id);
        }

        // 记录用户的足迹 异步处理
        if (userId != null) {
            executorService.execute(() -> {
                MallFootprint footprint = new MallFootprint();
                footprint.setUserId(userId);
                footprint.setGoodsId(id);
                footprintService.add(footprint);

                MallUser uUser = new MallUser();
                uUser.setId(userId);
                uUser.setUpdateTime(LocalDateTime.now());
                uUser.setFootprintId(footprint.getId());
                userService.updateById(uUser);

            });
        }
        FutureTask<List> goodsAttributeListTask = new FutureTask<>(goodsAttributeListCallable);
        FutureTask<Object> objectCallableTask = new FutureTask<>(objectCallable);
        FutureTask<List<MallGoodsProduct>> productListCallableTask = new FutureTask<>(productListCallable);
        FutureTask<List> issueCallableTask = new FutureTask<>(issueCallable);
        FutureTask<Map> commentsCallableTsk = new FutureTask<>(commentsCallable);
        FutureTask<MallBrand> brandCallableTask = new FutureTask<>(brandCallable);
        FutureTask<List> grouponRulesCallableTask = new FutureTask<>(grouponRulesCallable);

        executorService.submit(goodsAttributeListTask);
        executorService.submit(objectCallableTask);
        executorService.submit(productListCallableTask);
        executorService.submit(issueCallableTask);
        executorService.submit(commentsCallableTsk);
        executorService.submit(brandCallableTask);
        executorService.submit(grouponRulesCallableTask);

        Map<String, Object> data = new HashMap<>();

        try {

            List<MallGoodsProduct> productList = productListCallableTask.get();
            if (shId != null) {
                MallRebateRecord rebate = fenXiaoService.getMallRebateRecordById(shId);

                if (CollectionUtils.isNotEmpty(productList)) {
                    logger.info("获取的产品数据 :" + JacksonUtils.bean2Jsn(productList));
                    for (int i = 0; i < productList.size(); i++) {
                        MallGoodsProduct myProduct = productList.get(i);
                        BigDecimal nowAmount = myProduct.getPrice();
                        BigDecimal getAmount = GoodsUtils.getAddAmount(rebate, nowAmount);
                        logger.info("获取到getAmount ： " + getAmount);
                        myProduct.setPrice(nowAmount.add(getAmount));
                    }
                }
                logger.info("计算玩后的获取的产品数据 :" + JacksonUtils.bean2Jsn(productList));
                // 设置加价金额
                GoodsUtils.setRetailPrice(rebate, info);
            } else {

                // 计算省赚金额
                info.setSpreads(GoodsUtils.getSpreads(info));
            }

            data.put("info", info);
            data.put("userHasCollect", userHasCollect);
            data.put("issue", issueCallableTask.get());
            data.put("comment", commentsCallableTsk.get());
            data.put("specificationList", objectCallableTask.get());
            data.put("productList", productList);
            data.put("attribute", goodsAttributeListTask.get());
            data.put("brand", brandCallableTask.get());
            data.put("groupon", grouponRulesCallableTask.get());
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (info != null) {
            // 商品分享图片地址
            data.put("shareImage", info.getShareUrl());
        }
        return ResponseUtil.ok(data);
    }

    /**
     * 商品分类类目
     *
     * @param id 分类类目ID
     * @return 商品分类类目
     * @throws IOException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    @GetMapping("category")
    public Object category(@NotNull String id) throws Exception {
        if (org.apache.commons.lang.StringUtils.isBlank(id)) {
            // return ResponseUtil.fail(-1, "产品详情id为空");
            return ResponseUtil.badArgument();
        }

        // MallCategory cur = categoryService.findById(id);
        // MallCategory parent = null;
        // List<MallCategory> children = null;
        //
        // if (com.graphai.commons.util.ObjectUtils.isNotNull(cur)) {
        // if ("0".equals(cur.getPid())) {
        // parent = cur;
        // children = categoryService.queryByPid(cur.getId());
        // cur = children.size() > 0 ? children.get(0) : cur;
        // } else {
        // parent = categoryService.findById(cur.getPid());
        // children = categoryService.queryByPid(cur.getPid());
        // }
        // }
        //
        // Map<String, Object> data = new HashMap<>();
        // data.put("currentCategory", cur);
        // data.put("parentCategory", parent);
        // data.put("brotherCategory", children);

        String url = platFormServerproperty.getUrl(MallConstant.TAOBAO);
        Map<String, Object> mappMap = new HashMap<String, Object>();
        mappMap.put("plateform", "dtk");
        mappMap.put("cid", id);

        Header[] headers = new BasicHeader[]{new BasicHeader("method", "getCategory"), new BasicHeader("version", "v1.0")};
        String result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);


        JSONObject object = JSONObject.parseObject(result);

        if (!object.getString("errno").equals("0")) {
            return ResponseUtil.fail();
        }

        Map<String, Object> map = JSONArray.parseObject(object.get("data").toString(), Map.class);
        return ResponseUtil.ok(map);
    }

    /**
     * 根据条件搜素商品
     * <p>
     * 1. 这里的前五个参数都是可选的，甚至都是空 2. 用户是可选登录，如果登录，则记录用户的搜索关键字
     *
     * @param categoryId 分类类目ID，可选
     * @param brandId    品牌商ID，可选
     * @param keyword    关键字，可选
     * @param isNew      是否新品，可选
     * @param isHot      是否热买，可选
     * @param userId     用户ID
     * @param page       分页页数
     * @param limit      分页大小
     * @param sort       排序方式，支持"add_time", "retail_price"或"name"
     * @param order      排序类型，顺序或者降序 shId : 分享ID,用来记录加价问题
     * @return 根据条件搜素的商品详情
     */
    @GetMapping("list")
    public Object list(@RequestParam(defaultValue = "children") String catLevel, String activityId, String categoryId, String brandId, String keyword, Boolean isNew, Boolean isHot, String shId, @LoginUser String userId,
                       @RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit, @Sort(accepts = {"id", "retail_price", "name"}) @RequestParam(defaultValue = "add_time") String sort,
                       @Order @RequestParam(defaultValue = "desc") String order) {

        // 添加到搜索历史
        if (userId != null && !StringUtils.isEmpty(keyword)) {
            MallSearchHistory searchHistoryVo = new MallSearchHistory();
            searchHistoryVo.setKeyword(keyword);
            searchHistoryVo.setUserId(userId);
            searchHistoryVo.setFrom("wx");
            searchHistoryService.save(searchHistoryVo);
        }

        MallRebateRecord rebate = null;
        /*** 说明是分销员分享出来的 ***/
        if (shId != null) {
            rebate = fenXiaoService.getMallRebateRecordById(shId);
        }

        if (org.apache.commons.lang.StringUtils.isBlank(categoryId) || "null".equals(categoryId)) {
            categoryId = "0";
        }
        /*** 2019年6月5号改造，如果传入的目录是父目录ID将子目录下所有的商品全部查询出来 ***/
        // 查询列表数据
        List<MallGoods> goodsList = goodsService.querySelectiveV2(activityId, categoryId, catLevel, brandId, keyword, isHot, isNew, page, limit, sort, order);
        if (CollectionUtils.isNotEmpty(goodsList)) {
            // 2B的时候需要计算省赚金额(分销所得金额)
            if (shId == null) {
                // 计算省赚金额
                for (int i = 0; i < goodsList.size(); i++) {
                    MallGoods myGoods = goodsList.get(i);
                    myGoods.setSpreads(GoodsUtils.getSpreads(myGoods));
                }
            } else {
                GoodsUtils.setRetailPrice(rebate, goodsList);
            }

        }

        // 查询商品所属类目列表。
        List<String> goodsCatIds = goodsService.getCatIds(brandId, keyword, isHot, isNew);
        List<MallCategory> categoryList = null;
        if (goodsCatIds.size() != 0) {
            categoryList = categoryService.queryL2ByIds(goodsCatIds);
        } else {
            categoryList = new ArrayList<>(0);
        }

        Map<String, Object> data = new HashMap<>();
        data.put("goodsList", goodsList);
        data.put("count", PageInfo.of(goodsList).getTotal());
        data.put("filterCategoryList", categoryList);

        return ResponseUtil.ok(data);
    }

    @GetMapping("/bussiness/list")
    public Object listv2() {
        String url = platFormServerproperty.getUrl(MallConstant.TAOBAO);
        Map<String, Object> mappMap = new HashMap<String, Object>();
        mappMap.put("plateform", "dtk");

        Header[] headers = new BasicHeader[]{new BasicHeader("method", "getSearchBussinessList"), new BasicHeader("version", "v1.0")};
        return HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);

    }

    /**
     * 商城首页下拉搜索
     *
     * @param catLevel
     * @param activityId
     * @param categoryId
     * @param brandId
     * @param keyword
     * @param isNew
     * @param isHot
     * @param shId
     * @param userId
     * @param page
     * @param size
     * @param sort
     * @param order
     * @param industryId
     * @return
     * @throws Exception
     */
    @GetMapping("v2/list")
    public Object listv2(@RequestParam(defaultValue = "children") String catLevel, HttpServletRequest request, String activityId, String categoryId, String brandId, String keyword, Boolean isNew, Boolean isHot,
                         String shId, @LoginUser String userId, @RequestParam(defaultValue = "1") Integer page, @RequestParam(value = "size", required = true) Integer size, @RequestParam(defaultValue = "0") String sort,
                         @Order @RequestParam(defaultValue = "desc") String order, @RequestParam(defaultValue = "0") String industryId, @RequestParam(value = "gids", required = false) List<String> gids,
                         @RequestParam(value = "isCoupon", required = false) String isCoupon, @RequestParam(defaultValue = "1.6.6") String version) throws Exception {


        // 添加到搜索历史
        if (userId != null && !StringUtils.isEmpty(keyword)) {
            MallSearchHistory searchHistoryVo = new MallSearchHistory();
            searchHistoryVo.setKeyword(keyword);
            searchHistoryVo.setUserId(userId);
            searchHistoryVo.setFrom("wx");
            searchHistoryService.save(searchHistoryVo);
        }


        // 查询分润计算方式(key_value 0:服务商、区代、市代方式 1:用户firstLeader、secondLeader、thirderLeader方式 )
        Map<String, String> profitTypeMap = mallSystemConfigService.listProfitType();
        String profitType = profitTypeMap.get("Mall_profit_type");

        // （2）查询第三方购物平台波比
        QueryWrapper<MallUserProfitNew> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("profit_type", MallUserProfitNewEnum.PROFIT_TYPE_1.getCode());
        queryWrapper.eq("deleted", 0);
        List<MallUserProfitNew> profitNewList = iMallUserProfitNewService.list(queryWrapper);
        MallUserProfitNew mallUserProfitNew = null;
        if (CollectionUtils.isNotEmpty(profitNewList)) {
            mallUserProfitNew = profitNewList.get(0);
        }
        MallUser mallUser = userService.findById(userId);


        boolean flag = false;
        boolean retrial = false;
        if (appVersionService.getTopOneAppVersion(version)) {
            flag = true;
        }

        if (RetrialUtils.examine(request, version)) {
            retrial = true;
        }

        List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
        List<VO> specificationVoList = null;
        MallRebateRecord rebate = null;
        /*** 说明是分销员分享出来的 ***/
        if (shId != null) {
            rebate = fenXiaoService.getMallRebateRecordById(shId);
        }


        if (org.apache.commons.lang.StringUtils.isBlank(categoryId) || "null".equals(categoryId)) {
            categoryId = "0";
        }

        String sortField = "sort_order";
        if ("0".equals(sort)) {
            sortField = "retail_price ,id ,itemsale";
        } else if ("1".equals(sort)) {
            sortField = "id";
        } else if ("2".equals(sort)) {
            sortField = "itemsale";
        } else if ("3".equals(sort)) {
            sortField = "retail_price";
        }

        // 查询列表数据
        List<MallGoods> goodsList = goodsService.querySelectiveV3(activityId, categoryId, catLevel, brandId, keyword, isHot, isNew, page, size, sortField, order, industryId, gids, isCoupon, flag, retrial);
        // 打乱顺序 如果需要用到排序，则此行代码必须删掉
        // Collections.shuffle(goodsList);

        if (CollectionUtils.isNotEmpty(goodsList)) {
            for (MallGoods mallGoodsDTO : goodsList) {
                BigDecimal profit = null;
                // 算出返佣金额
                if (ObjectUtil.isNotNull(mallGoodsDTO.getCommissionRate()) && ObjectUtil.isNotNull(mallGoodsDTO.getRetailPrice())) {
                    BigDecimal num1 = new BigDecimal(mallGoodsDTO.getCommissionRate());
                    BigDecimal rate = num1.divide(new BigDecimal(100));
                    profit = mallGoodsDTO.getRetailPrice().multiply(rate).multiply(new BigDecimal("0.9")).setScale(2, BigDecimal.ROUND_DOWN);

                }

                if (profit != null && profit.doubleValue() >= 0 && org.apache.commons.lang3.StringUtils.isNotBlank(mallGoodsDTO.getBussinessId())) {

                    Map<String, Object> map = null;
                    // 查询分润计算方式(key_value 0:服务商、区代、市代方式 1:用户firstLeader、secondLeader、thirderLeader方式 )
                    // Map<String, String> profitTypeMap = mallSystemConfigService.listProfitType();
                    // String profitType = profitTypeMap.get("Mall_profit_type");
                    // if (Objects.equals(profitType, "0")) {
                    // map = (Map<String, Object>) AccountSFUtils.getProfit1(userId, mallGoodsDTO.getBussinessId(),
                    // profit, "JL20002", AccountStatus.PROFIT_TYPE_1);
                    // } else {
                    // /** 方式二：旧模式（firstLeader、secondLeader、thirderLeader方式）--【第三方购物和权益商品】 **/
                    // map = (Map<String, Object>) AccountSFUtils.getProfit2(userId, mallGoodsDTO.getBussinessId(),
                    // profit, "JL20002");
                    // }

                    if (Objects.equals(profitType, "0")) {
                        map = (Map<String, Object>) AccountSFUtils.getProfit(mallUser, mallGoodsDTO.getBussinessId(), profit, "JL20002", MallUserProfitNewEnum.PROFIT_TYPE_1.strCodeInt(), mallUserProfitNew);
                    } else {
                        /** 方式二：旧模式（firstLeader、secondLeader、thirderLeader方式）--【第三方购物和权益商品】 **/
                        map = (Map<String, Object>) AccountSFUtils.getProfit2(userId, mallGoodsDTO.getBussinessId(), profit, "JL20002");
                    }

                    if (ObjectUtil.isNotNull(map) && map.containsKey("commissionReward")) {
                        BigDecimal commissionfee = new BigDecimal(0.00);
                        if (ObjectUtil.isNotNull(map.get("commissionReward"))) {
                            commissionfee = new BigDecimal(map.get("commissionReward").toString());
                        }
                        mallGoodsDTO.setRebateAmount(commissionfee);
                    } else {
                        mallGoodsDTO.setRebateAmount(new BigDecimal(0.0));
                    }

                }
            }
        }

        Map<String, Object> data = new HashMap<>();
        data.put("goodsList", goodsList);
        data.put("count", PageInfo.of(goodsList).getTotal());

        return ResponseUtil.ok(data);
    }

    /**
     * 查询产品并且查询出产品下的具体优惠券数据
     *
     * @param catLevel
     * @param activityId
     * @param categoryId
     * @param brandId
     * @param keyword
     * @param isNew
     * @param isHot
     * @param shId
     * @param userId
     * @param page
     * @param limit
     * @param sort
     * @param order
     * @param industryId
     * @return
     * @throws Exception
     */
    @GetMapping("v3/list")
    public Object listv3(@RequestParam(defaultValue = "children") String catLevel, String activityId, String categoryId, String brandId, String keyword, Boolean isNew, Boolean isHot, String shId,
                         @LoginUser String userId, @RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit, @RequestParam(defaultValue = "id") String sort,
                         @Order @RequestParam(defaultValue = "desc") String order, @RequestParam(value = "industryId", required = false) String industryId,
                         @RequestParam(value = "isGetDetail", required = false) String isGetDetail) throws Exception {

        List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
        List<VO> specificationVoList = null;

        if (org.apache.commons.lang.StringUtils.isBlank(categoryId) || "null".equals(categoryId)) {
            categoryId = "0";
        }

        String sortField = "sortorder";
        if ("0".equals(sort)) {
            sortField = "retail_price ,id ,itemsale";
        } else if ("1".equals(sort)) {
            sortField = "id";
        } else if ("2".equals(sort)) {
            sortField = "itemsale";
        } else if ("3".equals(sort)) {
            sortField = "retail_price";
        }

        /*** 2019年6月5号改造，如果传入的目录是父目录ID将子目录下所有的商品全部查询出来 ***/
        // 查询列表数据
        // List<MallGoods> goodsList = goodsService.querySelectiveV3(activityId, categoryId, catLevel,
        // brandId, keyword, isHot, isNew, page, limit, sortField,
        // order, industryId);


        // 根据大淘客父类目id查询商品列表

        String url = platFormServerproperty.getUrl(MallConstant.TAOBAO);
        Map<String, Object> mappMap = new HashMap<String, Object>();
        mappMap.put("plateform", "dtk");
        mappMap.put("page", page);
        mappMap.put("size", limit);
        mappMap.put("categoryId", categoryId);
        mappMap.put("sort", sort);
        mappMap.put("columnType", " ");

        Header[] headers = new BasicHeader[]{new BasicHeader("method", "selectDtkGoodsByPage"), new BasicHeader("version", "v1.0")};
        String result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);


        JSONObject object = JSONObject.parseObject(result);

        if (!object.getString("errno").equals("0")) {
            return ResponseUtil.fail();
        }

        List<MallGoods> goodsList = JSONArray.parseArray(object.get("data").toString(), MallGoods.class);


        List<String> goodIds = new ArrayList<String>();

        if (isGetDetail.equals("0")) {
            if (CollectionUtils.isNotEmpty(goodsList)) {
                /*** 界网部落:由代购分享给C端用户时候，代购类电商产品需要计算加价部分金额 */
                if (shId != null) {
                    MallRebateRecord rebate = fenXiaoService.getMallRebateRecordById(shId);
                    GoodsUtils.setRetailPrice(rebate, goodsList);
                } else {
                    GoodsUtils.setSpreads(goodsList);
                }

                /*** 小花咪电商：需要计算返利金额，并且是根据不同的代理基本进行计算 */
                Byte userLevl = 0;
                if (userId != null) {
                    MallUser user = userService.findById(userId);
                    userLevl = user.getUserLevel();
                }

                MallDistributionSettings setting = distributionService.getlDistributionSetting(userLevl, "D00");
                GoodsUtils.setRebateAmount(setting, goodsList);

                // 小花咪电商，计算返利多少金额

                for (int i = 0; i < goodsList.size(); i++) {
                    goodIds.add(goodsList.get(i).getId());
                }

                // 查询属性集合
                List<MallGoodsAttribute> attributeList = goodsAttributeService.queryListByGids(goodIds);
                // 查询规格数据集合
                List<MallGoodsSpecification> specificationList = goodsSpecificationService.queryListByGids(goodIds);
                // 查询sku集合
                List<MallGoodsProduct> productList = productService.queryListByGid(goodIds);
                // 优惠券列表
                List<Map<String, Object>> couponList = couponService.getProductUnderCoupons(goodIds);

                for (int i = 0; i < goodsList.size(); i++) {
                    List<String> finalImgList = new ArrayList<String>();
                    List<String> myDList = new ArrayList<String>();
                    MallGoods myGoods = goodsList.get(i);
                    String goodsId = myGoods.getId();

                    try {

                        if (myGoods != null && (org.apache.commons.lang.StringUtils.isEmpty(myGoods.getChannelCode()) || "JW".equalsIgnoreCase(myGoods.getChannelCode())) && !StringUtils.isEmpty(myGoods.getDetail())) {
                            Element doc = Jsoup.parseBodyFragment(myGoods.getDetail()).body();
                            Elements pngs = doc.select("img[src]");
                            for (Element element : pngs) {
                                String imgUrl = element.attr("src");
                                myDList.add(imgUrl);
                            }
                            myGoods.setDetail(JacksonUtils.bean2Jsn(myDList));
                        } else {
                            myDList = JacksonUtils.jsn2List(myGoods.getDetail(), String.class);
                        }
                    } catch (Exception e1) {
                        logger.error("商品详情解析json异常" + e1.getMessage());
                    }

                    // finalImgList.addAll(Arrays.asList(myGoods.getGallery()));
                    finalImgList.addAll(myDList);

                    String[] str = new String[finalImgList.size()];

                    // myGoods.setGallery(finalImgList.toArray(str));
                    // 筛选响应产品的属性数据
                    List<MallGoodsAttribute> atCollect = new ArrayList<MallGoodsAttribute>();
                    CollectionUtils.select(attributeList, new Predicate() {
                        @Override
                        public boolean evaluate(Object object) {
                            MallGoodsAttribute attribute = (MallGoodsAttribute) object;
                            return (goodsId != null && goodsId.equals(attribute.getGoodsId())) ? true : false;
                        }
                    }, atCollect);

                    // 筛选规格数据
                    List<MallGoodsSpecification> spCollect = new ArrayList<MallGoodsSpecification>();
                    CollectionUtils.select(specificationList, new Predicate() {
                        @Override
                        public boolean evaluate(Object object) {
                            MallGoodsSpecification sp = (MallGoodsSpecification) object;
                            return (goodsId != null && goodsId.equals(sp.getGoodsId())) ? true : false;
                        }
                    }, spCollect);

                    List<Map<String, Object>> couponCollect = new ArrayList<Map<String, Object>>();
                    CollectionUtils.select(couponList, new Predicate() {
                        @Override
                        public boolean evaluate(Object object) {
                            @SuppressWarnings("unchecked")
                            Map<String, Object> coupon = (Map<String, Object>) object;
                            return (goodsId != null && goodsId.equals(MapUtils.getInteger(coupon, "goodsId"))) ? true : false;
                        }
                    }, couponCollect);

                    Map<String, VO> map = new HashMap<>();
                    specificationVoList = new ArrayList<VO>();

                    for (MallGoodsSpecification goodsSpecification : spCollect) {
                        String specification = goodsSpecification.getSpecification();
                        VO goodsSpecificationVo = map.get(specification);
                        if (goodsSpecificationVo == null) {
                            goodsSpecificationVo = new VO();
                            goodsSpecificationVo.setName(specification);
                            List<MallGoodsSpecification> valueList = new ArrayList<>();
                            valueList.add(goodsSpecification);
                            goodsSpecificationVo.setValueList(valueList);
                            map.put(specification, goodsSpecificationVo);
                            specificationVoList.add(goodsSpecificationVo);
                        } else {
                            List<MallGoodsSpecification> valueList = goodsSpecificationVo.getValueList();
                            valueList.add(goodsSpecification);
                        }
                    }

                    // 产品筛选
                    List<MallGoodsProduct> pdCollect = new ArrayList<MallGoodsProduct>();
                    CollectionUtils.select(productList, new Predicate() {
                        @Override
                        public boolean evaluate(Object object) {
                            MallGoodsProduct pd = (MallGoodsProduct) object;
                            return (goodsId != null && goodsId.equals(pd.getGoodsId())) ? true : false;
                        }
                    }, pdCollect);

                    Map<String, Object> goodsData = new HashMap<String, Object>();
                    goodsData.put("info", myGoods);
                    goodsData.put("specificationList", specificationVoList);
                    goodsData.put("productList", pdCollect);
                    goodsData.put("attribute", atCollect);
                    // 优惠券列表
                    goodsData.put("couponCollect", couponCollect);
                    dataList.add(goodsData);
                }
            }
        } else if (isGetDetail.equals("1")) {
            if (CollectionUtils.isNotEmpty(goodsList)) {
                for (int i = 0; i < goodsList.size(); i++) {
                    MallGoods myGoods = goodsList.get(i);
                    Map<String, Object> goodsData = new HashMap<String, Object>();
                    goodsData.put("info", myGoods);
                    dataList.add(goodsData);
                }
            }
        }


        Map<String, Object> data = new HashMap<>();
        data.put("goodsList", dataList);
        data.put("count", PageInfo.of(goodsList).getTotal());

        return ResponseUtil.ok(data);
    }

    /**
     * 根据大淘客类目查看商品列表
     */
    @GetMapping("v3/dtkGoodsList")
    public Object dtkGoodsList(@RequestParam(value = "categoryId") String categoryId, @LoginUser String userId, @RequestParam(defaultValue = "0") Integer page, @RequestParam(defaultValue = "10") Integer limit,
                               @RequestParam(defaultValue = "id") String sort) {

        if (org.apache.commons.lang.StringUtils.isBlank(categoryId) || "null".equals(categoryId)) {
            categoryId = "0";
        }
        Map<String, Object> data = new HashMap<>();
        // String url = platFormServerproperty.getUrl(MallConstant.TAOBAO) +
        // "?method=selectDtkGoodsByPage&version=v1.0.1&plateform=dtk&page=" + page + "&size=" + limit +
        // "&categoryId=" + categoryId + "&sort=" + sort + "&columnType=hot";
        try {
            String url = platFormServerproperty.getUrl(MallConstant.TAOBAO);
            Map<String, Object> mappMap = new HashMap<String, Object>();
            mappMap.put("plateform", "dtk");
            mappMap.put("page", page);
            mappMap.put("size", limit);
            mappMap.put("categoryId", categoryId);
            mappMap.put("sort", sort);
            mappMap.put("columnType", "hot");

            Header[] headers = new BasicHeader[]{new BasicHeader("method", "selectDtkGoodsByPage"), new BasicHeader("version", "v1.0")};
            String result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);


            JSONObject object = JSONObject.parseObject(result);

            if (!object.getString("errno").equals("0")) {
                return ResponseUtil.fail();
            }

            List<MallGoods> goodsList = JSONArray.parseArray(object.get("data").toString(), MallGoods.class);


            data.put("goodsList", goodsList);
        } catch (Exception e) {
            logger.error("商品查询异常");
        }


        return ResponseUtil.ok(data);
    }

    private Object findDetail(MallGoods info, String shId) {
        String id = info.getId();
        List<String> myDList = new ArrayList<String>();
        try {
            // 不是唯品仓同步过来的数据就要做处理 !"WPC".equals(info.getChannelCode())
            if ((org.apache.commons.lang.StringUtils.isEmpty(info.getChannelCode()) || "JW".equalsIgnoreCase(info.getChannelCode())) && info != null && !StringUtils.isEmpty(info.getDetail())) {
                Element doc = Jsoup.parseBodyFragment(info.getDetail()).body();
                Elements pngs = doc.select("img[src]");
                for (Element element : pngs) {
                    String imgUrl = element.attr("src");
                    myDList.add(imgUrl);
                }
                info.setDetail(JacksonUtils.bean2Jsn(myDList));
            }

        } catch (Exception e1) {
            logger.error("商品详情解析json异常" + e1.getMessage());
        }
        // 商品属性
        List<MallGoodsAttribute> goodsAttributeList = goodsAttributeService.queryByGid(id);
        // 商品规格
        Object goodsSpecification = goodsSpecificationService.getSpecificationVoList(id);
        // 商品规格对应的数量和价格
        List<MallGoodsProduct> productList = productService.queryByGid(id);
        Map<String, Object> data = new HashMap<>();
        if (shId != null) {
            MallRebateRecord rebate = fenXiaoService.getMallRebateRecordById(shId);

            if (CollectionUtils.isNotEmpty(productList)) {
                // logger.info("获取的产品数据 :" +
                // JacksonUtils.bean2Jsn(productList));
                for (int i = 0; i < productList.size(); i++) {
                    MallGoodsProduct myProduct = productList.get(i);
                    BigDecimal nowAmount = myProduct.getPrice();
                    BigDecimal getAmount = GoodsUtils.getAddAmount(rebate, nowAmount);
                    myProduct.setPrice(nowAmount.add(getAmount));
                }
            }
            // 设置加价金额
            GoodsUtils.setRetailPrice(rebate, info);
        } else {
            // 计算省赚金额
            info.setSpreads(GoodsUtils.getSpreads(info));
        }

        data.put("info", info);
        data.put("specificationList", goodsSpecification);
        data.put("productList", productList);
        data.put("attribute", goodsAttributeList);

        return data;
    }

    /**
     * 商品详情页面“大家都在看”推荐商品
     *
     * @param id , 商品ID
     * @return 商品详情页面推荐商品
     */
    @GetMapping("related")
    public Object related(@NotNull String id) {
        MallGoods goods = goodsService.findById(id);
        if (goods == null) {
            return ResponseUtil.badArgumentValue();
        }

        // 目前的商品推荐算法仅仅是推荐同类目的其他商品
        String cid = goods.getCategoryId();

        // 查找六个相关商品
        int related = 6;
        List<MallGoods> goodsList = goodsService.queryByCategory(cid, 0, related);
        Map<String, Object> data = new HashMap<>();
        data.put("goodsList", goodsList);
        return ResponseUtil.ok(data);
    }

    /**
     * 在售的商品总数
     *
     * @return 在售的商品总数
     */
    @GetMapping("count")
    public Object count() {
        Integer goodsCount = goodsService.queryOnSale();
        Map<String, Object> data = new HashMap<>();
        data.put("goodsCount", goodsCount);
        return ResponseUtil.ok(data);
    }


    private void syncGoodsImg(MallGoods goods) {
        try {
            if (goods == null || goods.getId() == null || goods.getName() == null || goods.getPicUrl() == null && !goods.getName().contains(" ")) {
                return;
            }
            if (!goods.getPicUrl().contains("a.vpimg2.com") && !goods.getPicUrl().contains("akmer.aikucun.com")) {
                return;
            }
            String[] goodsNameSplitArr = goods.getName().split(" ");
            if (goodsNameSplitArr == null || goodsNameSplitArr.length < 1) {
                return;
            }

            String goodsCodeSeq = goodsNameSplitArr[goodsNameSplitArr.length - 1];
            MallGoodsCodeSeq codeSeq = goodsCodeSeqService.selectByPrimaryKey(goodsCodeSeq);
            if (codeSeq == null) {
                return;
            }

            String content = goodsCodeSeq;
            String urlPath = goods.getPicUrl();

            String newUrlPath = this.uploadToAliyun(urlPath, content);

            MallGoods syncGoodsImgDto = new MallGoods();
            syncGoodsImgDto.setId(goods.getId());
            syncGoodsImgDto.setPicUrl(newUrlPath);

            String[] newGallery = null;
            if (goods.getGallery() != null && goods.getGallery().length > 0) {
                newGallery = new String[goods.getGallery().length];
                for (int i = 0; i < goods.getGallery().length; i++) {
                    String url = goods.getGallery()[i];
                    if (url.contains("a.vpimg2.com") || url.contains("akmer.aikucun.com")) {
                        newGallery[i] = this.uploadToAliyun(url, content);
                    } else {
                        newGallery[i] = url;
                    }
                }
                syncGoodsImgDto.setGallery(newGallery);
            }
            goodsService.updateById(syncGoodsImgDto);

            goods.setPicUrl(newUrlPath);
            if (newGallery != null) {
                goods.setGallery(newGallery);
            }
        } catch (NumberFormatException e1) {
            logger.error("add goodsCode2Img fail:" + goods.getId());
        } catch (Exception e) {
            logger.error("", e);
        }
    }


    private String uploadToAliyun(String urlPath, String content) throws Exception {

        ByteArrayOutputStream os = new ByteArrayOutputStream();
        int fontSize = 16;
        BufferedImage bg = ImageUtils.drawStringForNetImage(urlPath, content, Color.white, 6, 20, "Microsoft YaHei", fontSize, new Color(220, 99, 119), 0, 0, 95, 30);

        ImageIO.write(bg, "jpg", os);

        InputStream ips = new ByteArrayInputStream(os.toByteArray());

        String format = "jpg";
        String fileName = AppIdAlgorithm.generateCode(6).toUpperCase() + System.currentTimeMillis() + ".jpg";
        MallStorage storage = storageService.store(ips, os.toByteArray().length, format, fileName);
        return storage.getUrl();
    }

    private class VO {
        private String name;
        private List<MallGoodsSpecification> valueList;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<MallGoodsSpecification> getValueList() {
            return valueList;
        }

        public void setValueList(List<MallGoodsSpecification> valueList) {
            this.valueList = valueList;
        }
    }


    /**
     * 根据福权益禄类目查看商品列表
     */
    @GetMapping("v3/fuluGoodsList")
    public Object fuluGoodsList(@RequestParam(value = "categoryId") String categoryId, @LoginUser String userId, @RequestParam(defaultValue = "0") Integer page, @RequestParam(defaultValue = "100") Integer limit) {

        if (org.apache.commons.lang.StringUtils.isBlank(categoryId) || "null".equals(categoryId)) {
            return ResponseUtil.fail();
        }
        Map<String, Object> data = new HashMap<>();
        try {
            String url = platFormServerproperty.getUrl(MallConstant.TAOBAO);
            Map<String, Object> mappMap = new HashMap<String, Object>();
            mappMap.put("plateform", "fulu");
            mappMap.put("page", page);
            mappMap.put("size", limit);
            mappMap.put("categoryId", categoryId);
            mappMap.put("position_code", "categoryRights");

            Header[] headers = new BasicHeader[]{new BasicHeader("method", "getQYMallCategoryByIdByFuLu"), new BasicHeader("version", "v1.0")};
            String result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);

            JSONObject object = JSONObject.parseObject(result);

            if (!object.getString("errno").equals("0")) {
                return ResponseUtil.fail();
            }
            return ResponseUtil.ok(JSONObject.parseObject(object.get("data").toString()));

        } catch (Exception e) {
            logger.error("商品查询异常");
        }

        return ResponseUtil.ok(data);
    }


    /**
     * 品牌列表
     *
     * @return 秒杀商品列表
     */
    @GetMapping("/timeSpikeGoodsList")
    public Object list(String industryId) throws Exception {


        if (industryId == null) {
            industryId = "0";
        }


        // 限时秒杀
        JSONArray timeSpikeGoodsJson = goodsService.queryTimeSpike(0, SystemConfig.getHotLimit(), industryId, "timeSpHome");
        Map<String, Object> map = new HashMap<String, Object>();

        if (com.graphai.commons.util.obj.ObjectUtils.isNotNull(timeSpikeGoodsJson)) {
            map.put("now", new Date());
            map.put("List", timeSpikeGoodsJson);
            return ResponseUtil.ok(map);
        }
        return ResponseUtil.ok();
    }


    /**
     * 拼多多分页列表
     *
     * @param userId
     * @param curPage
     * @param industryId 业务心态
     */
    @GetMapping("/pddGoodsPageList")
    public Object pddGoodsPageList(@LoginUser String userId, String curPage, String curPageSize, String industryId) {


        // 业务心态表的industryId 对应商品表的platform
        String platfrom = "6";
        if (Objects.equals(industryId, "8")) {
            platfrom = "6";
        } else if (Objects.equals(industryId, "5")) {
            platfrom = "5";
        } else if (Objects.equals(industryId, "4")) {
            platfrom = "1";
        } else if (Objects.equals(industryId, "18")) {
            platfrom = "11";
        } else if (Objects.equals(industryId, "17")) {
            platfrom = "7";
        } else if (Objects.equals(industryId, "16")) {
            platfrom = "8";
        } else if (Objects.equals(industryId, "19")) {
            platfrom = "9";
        } else if (Objects.equals(industryId, "20")) {
            platfrom = "10";
        } else if (Objects.equals(industryId, "39")) {
            platfrom = "0";
        }


        if (org.apache.commons.lang3.StringUtils.isBlank(curPage)) {
            return ResponseUtil.badArgumentValue();
        }
        if (org.apache.commons.lang3.StringUtils.isBlank(curPageSize)) {
            return ResponseUtil.badArgumentValue();
        }


        String url = platFormServerproperty.getUrl(industryId);

        Map<String, Object> mappMap = new HashMap<String, Object>();
        mappMap.put("plateform", "pdd");
        mappMap.put("curPage", curPage);
        mappMap.put("curPageSize", curPageSize);
        mappMap.put("platform", platfrom);
        Header[] headers = new BasicHeader[]{new BasicHeader("method", "pdd.get.home.pagelist"), new BasicHeader("version", "v1.0")};

        MallUser mallUser = userService.findById(userId);

        // 查询分润计算方式(key_value 0:服务商、区代、市代方式 1:用户firstLeader、secondLeader、thirderLeader方式 )
        Map<String, String> profitTypeMap = mallSystemConfigService.listProfitType();
        String profitType = profitTypeMap.get("Mall_profit_type");

        // （2）查询第三方购物平台波比
        QueryWrapper<MallUserProfitNew> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("profit_type", MallUserProfitNewEnum.PROFIT_TYPE_1.getCode());
        queryWrapper.eq("deleted", 0);
        List<MallUserProfitNew> profitNewList = iMallUserProfitNewService.list(queryWrapper);
        MallUserProfitNew mallUserProfitNew = null;
        if (CollectionUtils.isNotEmpty(profitNewList)) {
            mallUserProfitNew = profitNewList.get(0);
        }

        String result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);
        ;

        JSONObject object = JSONObject.parseObject(result);

        List<MallGoodsDTO> goodsList = new ArrayList<>(20);

        if (object.getString("errno").equals("0")) {
            goodsList = JSONArray.parseArray(object.get("data").toString(), MallGoodsDTO.class);

            List<String> goodsIdList = new ArrayList<String>();
            for (MallGoodsDTO mallGoodsDTO : goodsList) {
                if (platfrom.equals("0")) {
                    goodsIdList.add(mallGoodsDTO.getId());
                } else {
                    BigDecimal rebateAmount = mallGoodsDTO.getRebateAmount();
                    if (StringHelper.isNullOrEmpty(mallGoodsDTO.getRebateAmount()) && ObjectUtil.isNotNull(mallGoodsDTO.getCommissionRate()) && ObjectUtil.isNotNull(mallGoodsDTO.getRetailPrice())) {

                        // 算出返佣金额
                        BigDecimal num1 = new BigDecimal(mallGoodsDTO.getCommissionRate());
                        BigDecimal rate = num1.divide(new BigDecimal(100));
                        rebateAmount = mallGoodsDTO.getRetailPrice().multiply(rate).multiply(new BigDecimal("0.9")).setScale(2, BigDecimal.ROUND_DOWN);

                    }
                    if (rebateAmount == null || rebateAmount.doubleValue() <= 0) {
                        continue;
                    }

                    Map<String, Object> map = null;
                    // 查询分润计算方式(key_value 0:服务商、区代、市代方式 1:用户firstLeader、secondLeader、thirderLeader方式 )
                    // Map<String, String> profitTypeMap = mallSystemConfigService.listProfitType();
                    // String profitType = profitTypeMap.get("Mall_profit_type");
                    // if (Objects.equals(profitType, "0")) {
                    // map = (Map<String, Object>) AccountSFUtils.getProfit1(userId, mallGoodsDTO.getBussinessId(),
                    // rebateAmount, "JL20002", AccountStatus.PROFIT_TYPE_1);
                    // } else {
                    // /** 方式二：旧模式（firstLeader、secondLeader、thirderLeader方式）--【第三方购物】 **/
                    // map = (Map<String, Object>) AccountSFUtils.getProfit2(userId, mallGoodsDTO.getBussinessId(),
                    // rebateAmount, "JL20002");
                    // }


                    if (Objects.equals(profitType, "0")) {
                        map = (Map<String, Object>) AccountSFUtils.getProfit(mallUser, mallGoodsDTO.getBussinessId(), rebateAmount, "JL20002", MallUserProfitNewEnum.PROFIT_TYPE_1.strCodeInt(), mallUserProfitNew);
                    } else {
                        /** 方式二：旧模式（firstLeader、secondLeader、thirderLeader方式）--【第三方购物和权益商品】 **/
                        map = (Map<String, Object>) AccountSFUtils.getProfit2(userId, mallGoodsDTO.getBussinessId(), rebateAmount, "JL20002");
                    }

                    if (ObjectUtil.isNotNull(map)) {
                        BigDecimal commissionfee = new BigDecimal("0.00");
                        if (ObjectUtil.isNotNull(map.get("commissionReward"))) {
                            commissionfee = new BigDecimal(map.get("commissionReward").toString());
                        }
                        mallGoodsDTO.setRebateAmount(commissionfee);
                    }

                }
            }
            if (!goodsIdList.isEmpty()) {
                List<Map<String, Object>> mallGoodsProductList = mallGoodsProductMapper.queryGoodsProductInGoodsId(goodsIdList);
                for (Map<String, Object> goodsId : mallGoodsProductList) {
                    for (MallGoodsDTO mallGoodsDTO : goodsList) {
                        if (goodsId.get("goodsId").equals(mallGoodsDTO.getId()) && goodsId.containsKey("pubSharePreFee")) {
                            // mallGoodsDTO.setRebateAmount(new BigDecimal(goodsId.get("pubSharePreFee").toString()));
                            String commissionReward = AccountZYUtils.getProfitZy1OnlyUser(userId, AccountStatus.BID_27, new BigDecimal(goodsId.get("pubSharePreFee").toString()), MallUserProfitNewEnum.PROFIT_TYPE_2.strCodeInt())
                                    .get("commissionReward");
                            mallGoodsDTO.setRebateAmount(new BigDecimal(commissionReward.toString()));
                        }
                    }
                }
            }
        }

        return ResponseUtil.ok(goodsList);
    }


    /**
     * 行业商品分页列表
     */
    @SuppressWarnings("unchecked")
    @GetMapping("/getGoodsByIndustry")
    public Object pddGoodsPageList(@LoginUser String userId, @RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit, @NotNull String cateId) {

        if (page <= 0) {
            page = 1;
        }

        BigDecimal profit = null;
        try {

            // 查询分润计算方式(key_value 0:服务商、区代、市代方式 1:用户firstLeader、secondLeader、thirderLeader方式 )
            Map<String, String> profitTypeMap = mallSystemConfigService.listProfitType();
            String profitType = profitTypeMap.get("Mall_profit_type");

            // （2）查询第三方购物平台波比
            QueryWrapper<MallUserProfitNew> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("profit_type", MallUserProfitNewEnum.PROFIT_TYPE_1.getCode());
            queryWrapper.eq("deleted", 0);
            List<MallUserProfitNew> profitNewList = iMallUserProfitNewService.list(queryWrapper);
            MallUserProfitNew mallUserProfitNew = null;
            if (CollectionUtils.isNotEmpty(profitNewList)) {
                mallUserProfitNew = profitNewList.get(0);
            }

            MallUser mallUser = userService.findById(userId);
            String url = platFormServerproperty.getUrl(MallConstant.TAOBAO);
            Map<String, Object> mappMap = new HashMap<String, Object>();
            mappMap.put("plateform", "dtk");
            mappMap.put("page", page);
            mappMap.put("size", limit);
            mappMap.put("categoryId", cateId);
            mappMap.put("sort", "id");

            Header[] headers = new BasicHeader[]{new BasicHeader("method", "selectDtkGoodsByPage"), new BasicHeader("version", "v1.0")};
            String result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);
            JSONObject object = JSONObject.parseObject(result);
            if (object == null) {
                return ResponseUtil.fail();
            }

            List<MallGoodsDTO> goodsList = new ArrayList<>(50);
            if (Objects.equals(object.getString("errno"), "0")) {
                goodsList = JSONArray.parseArray(object.get("data").toString(), MallGoodsDTO.class);
                if (CollectionUtils.isNotEmpty(goodsList)) {
                    for (MallGoodsDTO mallGoodsDTO : goodsList) {
                        if (!mallGoodsDTO.getPicUrl().contains("https") && !mallGoodsDTO.getPicUrl().contains("http")) {
                            mallGoodsDTO.setPicUrl("https:" + mallGoodsDTO.getPicUrl());
                        }

                        /** CommissionRate<三方返佣金额比例> RetailPrice<三方商品实际销售价格> **/
                        if (ObjectUtil.isNotNull(mallGoodsDTO.getCommissionRate()) && ObjectUtil.isNotNull(mallGoodsDTO.getRetailPrice())) {
                            BigDecimal num1 = new BigDecimal(mallGoodsDTO.getCommissionRate());
                            BigDecimal rate = num1.divide(new BigDecimal(100));
                            /** 扣除掉平台的10% 技术服务费 **/
                            profit = mallGoodsDTO.getRetailPrice().multiply(rate).multiply(new BigDecimal("0.9")).setScale(2, BigDecimal.ROUND_DOWN);
                        }

                        if (profit != null && profit.doubleValue() >= 0 && org.apache.commons.lang3.StringUtils.isNotBlank(mallGoodsDTO.getBussinessId())) {
                            Map<String, Object> map = null;

                            if (Objects.equals(profitType, "0")) {
                                map = (Map<String, Object>) AccountSFUtils.getProfit(mallUser, mallGoodsDTO.getBussinessId(), profit, "JL20002", MallUserProfitNewEnum.PROFIT_TYPE_1.strCodeInt(), mallUserProfitNew);
                            } else {
                                /** 方式二：旧模式（firstLeader、secondLeader、thirderLeader方式）--【第三方购物和权益商品】 **/
                                map = (Map<String, Object>) AccountSFUtils.getProfit2(userId, mallGoodsDTO.getBussinessId(), profit, "JL20002");
                            }

                            if (ObjectUtil.isNotNull(map) && map.containsKey("commissionReward")) {
                                BigDecimal commissionfee = new BigDecimal(0.00);
                                if (ObjectUtil.isNotNull(map.get("commissionReward"))) {
                                    commissionfee = new BigDecimal(map.get("commissionReward").toString());
                                }
                                mallGoodsDTO.setRebateAmount(commissionfee);
                            } else {
                                mallGoodsDTO.setRebateAmount(new BigDecimal(0.0));
                            }

                        }

                    }
                }

            }
            return ResponseUtil.ok(goodsList);

        } catch (Exception e) {
            logger.error("商品查询异常 ：" + e.getMessage(), e);
        }

        return ResponseUtil.ok();
    }


    /**
     * 大淘客9.9包邮
     */
    @GetMapping("/getDtkNineGoods")
    public Object getDtkNineGoods(@RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit, @NotNull String cateId) {

        if (page <= 0) {
            page = 1;
        }

        try {

            String url = platFormServerproperty.getUrl(MallConstant.TAOBAO);
            Map<String, Object> mappMap = new HashMap<String, Object>();
            mappMap.put("plateform", "dtk");
            mappMap.put("page", page);
            mappMap.put("size", limit);
            mappMap.put("tbChoiceId", cateId);
            mappMap.put("sort", "add_time");

            Header[] headers = new BasicHeader[]{new BasicHeader("method", "selectDtkGoodsByPage"), new BasicHeader("version", "v1.0")};
            String result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);

            JSONObject object = JSONObject.parseObject(result);

            List<MallGoodsDTO> goodsList = new ArrayList<>(200);

            if ("0".equals(object.getString("errno"))) {

                goodsList = JSONArray.parseArray(object.get("data").toString(), MallGoodsDTO.class);

            }
            return ResponseUtil.ok(goodsList);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return ResponseUtil.ok();
    }


    /**
     * 大淘客疯抢排行商品
     */
    @GetMapping("/getDtkRankGoods")
    public Object getDtkNineGoods(@RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit, @NotNull String type, @NotNull String cateId) {

        if (page <= 0) {
            page = 1;
        }

        try {

            String url = platFormServerproperty.getUrl(MallConstant.TAOBAO);
            Map<String, Object> mappMap = new HashMap<String, Object>();
            mappMap.put("plateform", "dtk");
            mappMap.put("page", page);
            mappMap.put("size", limit);
            if (Objects.equals(type, "cateType")) {
                mappMap.put("cateId", cateId);
            } else if (Objects.equals(type, "rankType")) {
                mappMap.put("rankType", cateId);
            }

            Header[] headers = new BasicHeader[]{new BasicHeader("method", "queryRankGoods"), new BasicHeader("version", "v1.0")};
            String result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);

            JSONObject object = JSONObject.parseObject(result);

            List<MallGoodsDTO> goodsList = new ArrayList<>(200);

            if ("0".equals(object.getString("errno"))) {
                goodsList = JSONArray.parseArray(object.get("data").toString(), MallGoodsDTO.class);
            }
            return ResponseUtil.ok(goodsList);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return ResponseUtil.ok();
    }


    /**
     * 查询0元购商品
     *
     * @throws Exception
     */
    @GetMapping("/getZeroGoodsList")
    public Object getZeroGoodsList(/* @RequestParam(defaultValue = "110") String categoryId, */
            @LoginUser String userId, @RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit) throws Exception {

        if (page <= 0) {
            page = 1;
        }
        try {
            List<MallGoods> goodsList = goodsService.queryZeroGoods(page, limit);
            return ResponseUtil.ok(goodsList);

        } catch (Exception e) {
            logger.error("商品查询异常");
            throw e;
        }
    }


    /**
     * 根据类目id查看优惠卷商品
     *
     * @throws Exception
     */
    @GetMapping("/getCouponGoodsList")
    public Object getZeroGoodsList(@RequestParam(defaultValue = "1275019386604589058") String categoryId, @RequestParam(defaultValue = "1") Long page, @RequestParam(defaultValue = "10") Long limit) throws Exception {


        try {
            String url = platFormServerproperty.getUrl(MallConstant.TAOBAO);
            Map<String, Object> mappMap = new HashMap<String, Object>();
            mappMap.put("plateform", "dtk");
            mappMap.put("page", page);
            mappMap.put("size", limit);
            mappMap.put("categoryId", categoryId);
            // mappMap.put("columnType", "hot");

            Header[] headers = new BasicHeader[]{new BasicHeader("method", "selectCouponGoodsByPage"), new BasicHeader("version", "v1.0")};
            String result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);


            JSONObject object = JSONObject.parseObject(result);

            if (!"0".equals(object.getString("errno"))) {
                return ResponseUtil.fail();
            }

            List<MallGoodsDTO> goodsList = JSONArray.parseArray(object.get("data").toString(), MallGoodsDTO.class);

            if (CollectionUtils.isNotEmpty(goodsList)) {
                for (int i = 0; i < goodsList.size(); i++) {
                    Integer count = MallGoodsCouponCodeService.getGoodsCouponStock(goodsList.get(i).getCouponId());
                    goodsList.get(i).setStock(count);
                }
            }
            return ResponseUtil.ok(goodsList);
        } catch (Exception e) {
            logger.error("商品查询异常");
            throw e;
        }
    }


    /**
     * 商品详情
     * <p>
     * 用户可以不登录。 如果用户登录，则记录用户足迹以及返回用户收藏信息。
     *
     * @param userId 用户ID
     * @param id     商品ID
     * @return 商品详情
     * @throws IOException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    @GetMapping("goodDetail")
    public Object detail(@LoginUser String userId, HttpServletRequest mrequest, @NotNull String id) throws Exception {
        if (org.apache.commons.lang.StringUtils.isBlank(userId)) {
            return ResponseUtil.unlogin();
        }


        try {
            String url = platFormServerproperty.getUrl(MallConstant.TAOBAO);
            Map<String, Object> mappMap = new HashMap<String, Object>();
            mappMap.put("plateform", "dtk");
            mappMap.put("id", id);


            Header[] headers = new BasicHeader[]{new BasicHeader("method", "getCouponGoodsDetail"), new BasicHeader("version", "v1.0")};
            String result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);


            JSONObject object = JSONObject.parseObject(result);

            if (!"0".equals(object.getString("errno"))) {
                return ResponseUtil.fail();
            }
            if (ObjectUtil.isNull(object.get("data"))) {
                return ResponseUtil.fail();
            }

            Map<String, Object> map = (Map<String, Object>) JSONArray.parseObject(object.get("data").toString(), Map.class);

            return ResponseUtil.ok(map);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseUtil.fail();
    }


    /**
     * 生成分享二维码，淘宝令
     *
     * @throws Exception
     */
    @GetMapping("/getShareLink")
    public Object getShareLink(@LoginUser String userId, HttpServletRequest mrequest, @NotNull String srcGoodsId, @RequestParam(required = false) String type) throws Exception {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }

        // 淘宝转链
        String token = mrequest.getHeader("X-Mall-Token");
        if (org.apache.commons.lang3.StringUtils.isBlank(token)) {
            return ResponseUtil.unlogin();
        }

        if (Objects.equals("0", type)) {

            List<MallOrder> mallOrders = MallOrderService.queryOrder(userId, "zero", "dtk");
            if (CollectionUtils.isNotEmpty(mallOrders)) {
                return ResponseUtil.fail(502, "您已购买过0元购商品!");
            }


            JSONObject object = goodsService.queryGoodsDetailBySrcId(srcGoodsId);
            if (ObjectUtil.isNotNull(object)) {
                if (ObjectUtil.isNotNull(object.getInteger("stock"))) {
                    if (object.getInteger("stock") <= 0) {
                        return ResponseUtil.fail(503, "该商品太火爆,当日已售罄");
                    }
                }
            }


        }


        MallUser user = userService.findById(userId);


        // String relationId =user.getTbkRelationId();
        // System.out.println("~~~~~relationId ::::::" + relationId);
        if (org.apache.commons.lang3.StringUtils.isBlank(user.getTbkRelationId()) || org.apache.commons.lang3.StringUtils.isBlank(user.getTbkPubId())) {
            return ResponseUtil.unTaobaoAuth();
        }


        try {

            if (com.graphai.commons.util.obj.ObjectUtils.isNotNull(user)) {
                if (org.apache.commons.lang3.StringUtils.isNotBlank(user.getTbkPubId()) && org.apache.commons.lang3.StringUtils.isNotBlank(user.getTbkRelationId())) {
                    String url = platFormServerproperty.getUrl(MallConstant.TAOBAO);
                    Map<String, Object> mappMap = new HashMap<String, Object>();
                    mappMap.put("plateform", "dtk");
                    mappMap.put("id", srcGoodsId);
                    mappMap.put("pid", user.getTbkPubId());
                    mappMap.put("channelId", user.getTbkRelationId());

                    Header[] headers = new BasicHeader[]{new BasicHeader("method", "getSpreadLink"), new BasicHeader("version", "v1.0")};

                    String result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);

                    JSONObject object = JSONObject.parseObject(result);

                    if (Objects.equals(object.getString("errno"), "0")) {

                        JSONObject coupon2 = (JSONObject) object.get("data");

                        return ResponseUtil.ok(coupon2);
                    }
                }
                return ResponseUtil.fail(510, "此商品被淘宝下架，请选择其他商品查看!");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseUtil.fail();
    }


    /**
     * 解析淘口令
     *
     * @throws Exception
     */
    @GetMapping("/parseTkl")
    public Object parseTkl(HttpServletRequest mrequest, @RequestParam(value = "tkl", required = true) String tkl) throws Exception {


        BigDecimal actualPrice = BigDecimal.ZERO;

        try {

            // 新版
            // TreeMap<String, String> paraMap = new TreeMap<>();
            // paraMap.put("version", "v1.0.0");
            // paraMap.put("appKey", PlateFromConstant.DaTaoKeAppKey);
            // paraMap.put("content", tkl);
            // String result = ApiClient.sendReq(PlateFromConstant.newParseTkl,
            // PlateFromConstant.DaTaoKeAppSecret,
            // paraMap);

            // JSONObject object = JSONObject.parseObject(result);
            //
            // if (object.getString("code").equals("0")) {
            // JSONObject resultObj = JSONObject.parseObject(object.get("data").toString());
            // if (!Objects.equals(null, resultObj.getString("goodsId"))) {
            //
            // JSONObject orJsonObject = JSONObject.parseObject(resultObj.get("originInfo").toString());
            // if (!Objects.equals(null, orJsonObject.getString("price"))) {
            //
            // if (!Objects.equals(null, orJsonObject.getString("price"))
            // && !Objects.equals(null, orJsonObject.getString("amount"))
            // && (new BigDecimal(orJsonObject.getString("price"))
            // .compareTo(new BigDecimal(orJsonObject.getString("amount"))) == 1)) {
            // actualPrice = new BigDecimal(orJsonObject.getString("price"))
            // .subtract(new BigDecimal(orJsonObject.getString("amount")));
            // } else {
            // actualPrice = new BigDecimal(orJsonObject.getString("price"));
            // }
            // orJsonObject.put("discountPrice", actualPrice);
            // resultObj.put("originInfo", orJsonObject);
            // } else {
            // JSONObject originInfo = new JSONObject();
            //
            // TreeMap<String, String> paraMapGoods = new TreeMap<>();
            // paraMapGoods.put("version", "v1.2.2");
            // paraMapGoods.put("appKey", PlateFromConstant.DaTaoKeAppKey);
            // paraMapGoods.put("goodsId", resultObj.getString("goodsId"));
            // paraMapGoods.put("sign",
            // SignMD5Util.getSignStr(paraMapGoods, PlateFromConstant.DaTaoKeAppSecret));
            // String goodResult = HttpUtils.sendGet(PlateFromConstant.DaTaoKeGoodsDetail, paraMapGoods);
            //
            // JSONObject goodObject = JSONObject.parseObject(goodResult);
            // if ("0".equals(goodObject.getString("code"))) {
            // JSONObject ob = JSONObject.parseObject(goodObject.get("data").toString());
            //
            // String title = ob.getString("title");
            // originInfo.put("title", title);
            // String pic = ob.getString("mainPic");
            // originInfo.put("image", pic);
            //
            // originInfo.put("price", new BigDecimal(ob.getString("originalPrice")));
            // actualPrice = new BigDecimal(ob.getString("actualPrice"));
            // originInfo.put("discountPrice", actualPrice);
            //
            // resultObj.put("originInfo", originInfo);
            // }
            //
            // }
            // resultObj.put("industryId", "4");
            // return ResponseUtil.ok(resultObj);
            // }
            //
            // } else if (Objects.equals(object.getString("code"), "20001")) {
            // return ResponseUtil.fail(502, "淘口令过期或已失效");
            // } else if (Objects.equals(object.getString("code"), "20002")) {
            // return ResponseUtil.fail(504, "您粘贴的不是淘口令");
            // } else if (Objects.equals(object.getString("code"), "-1")) {
            // return ResponseUtil.fail(503, "第三方服务器错误");
            // } else if (Objects.equals(object.getString("code"), "20003")) {
            // return ResponseUtil.fail(505, "未找到有效淘口令或链接");
            // } else if (Objects.equals(object.getString("code"), "25003")) {
            // Map<String, Object> map = new HashMap<String, Object>();
            // if (tkl.contains("item.m.jd.com")||tkl.contains("u.jd.com")) {
            // map.put("industryId", "5");
            // }
            //// else if (tkl.contains("https://mobile.yangkeduo.com")) {
            //// map.put("industryId", "8");
            //// } else if (tkl.contains("https://m.suning.com")) {
            //// map.put("industryId", "19");
            //// }
            // return ResponseUtil.ok(map);
            // }

            Map<String, String> paramMap = new LinkedHashMap<String, String>();
            paramMap.put("apikey", "V9WgLR85kJDHCLsoE0prjRzGKnpvAdd4");
            paramMap.put("tkl", tkl);
            String result = HttpUtil.sendPost("http://api.91tool.com/tkl/query", paramMap);
            JSONObject object = JSONObject.parseObject(result);

            if (Objects.equals(object.getInteger("code"), 200)) {
                JSONObject resultObj = JSONObject.parseObject(object.get("data").toString());
                if (!Objects.equals(null, resultObj.getString("num_iid"))) {

                    resultObj.put("goodsId", resultObj.getString("num_iid"));
                    JSONObject originInfo = new JSONObject();


                    originInfo.put("title", resultObj.getString("content"));

                    originInfo.put("image", resultObj.getString("pic_url"));

                    originInfo.put("price", resultObj.getString("price"));

                    // originInfo.put("discountPrice", resultObj.getString("content"));

                    resultObj.put("originInfo", originInfo);

                    resultObj.put("industryId", "4");


                    return ResponseUtil.ok(resultObj);
                }

            } else if (Objects.equals(object.getInteger("code"), 4005)) {
                return ResponseUtil.fail(502, "积分不足");
            } else if (Objects.equals(object.getInteger("code"), 4006)) {
                return ResponseUtil.fail(504, "请求拒绝");
            } else if (Objects.equals(object.getInteger("code"), 4003)) {
                return ResponseUtil.fail(504, "参数不正确");
            } else {
                Map<String, Object> map = new HashMap<String, Object>();
                if (tkl.contains("item.m.jd.com") || tkl.contains("u.jd.com")) {
                    map.put("industryId", "5");
                }
                // else if (tkl.contains("https://mobile.yangkeduo.com")) {
                // map.put("industryId", "8");
                // } else if (tkl.contains("https://m.suning.com")) {
                // map.put("industryId", "19");
                // }
                return ResponseUtil.ok(map);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseUtil.fail();
    }


    /**
     * 解析淘口令
     *
     * @throws Exception
     */
    @GetMapping("/parseTklV2")
    public Object parseTklV2(@LoginUser String userId, HttpServletRequest mrequest, @RequestParam(value = "tkl", required = true) String tkl) throws Exception {

        logger.info("解析口令请求参数，tkl=" + tkl);

        BigDecimal actualPrice = BigDecimal.ZERO;
        BigDecimal profit = BigDecimal.ZERO;
        BigDecimal rate = BigDecimal.ZERO; // 佣金小数
        BigDecimal commissionFee = BigDecimal.ZERO; // 返佣金额
        BigDecimal amount = BigDecimal.ZERO; // 卷金额
        boolean isSuccess = true;
        String industryId = null;
        try {

            // Todo 2021-01-24 判断口令红包
            if (StringUtils.isNotBlank(tkl)) {
                int number = tkl.length() - tkl.replaceAll("K#", "").length();
                logger.info("number=" + number);
                if (number >= 4) {
                    String resultSub = RedpacketUtil.subString(tkl, "K#", "K#", 2);
                    logger.info("结果：" + resultSub);
                    if (StringUtils.isNotBlank(resultSub)) {
                        GameRedpacketRain grainValidateRepeat = gameRedpacketRainService.selectGameRedpacketByPassword(resultSub);
                        if (null != grainValidateRepeat) {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("id", grainValidateRepeat.getId());
                            jsonObject.put("industryId", "89");
                            // 广告渠道点击明细 PV和UV
                            try {
                                MallPageView mallPageView = new MallPageView();
                                mallPageView.setPvName("口令红包");
                                mallPageView.setPvUrl(grainValidateRepeat.getShortUrl());
                                mallPageView.setMobileType(IpUtils.getDevice(getRequest()));
                                mallPageView.setClientIp(IpUtils.getIpAddress(getRequest()));
                                mallPageView.setOutUserId(userId);
                                mallPageView.setType(RedStatus.MPV_TYPE_03);
                                mallPageView.setPlatform(RedStatus.MRQR_PLATFORM_RESOURCE_APP);
                                mallPageView.setCreateTime(LocalDateTime.now());
                                mallPageViewService.save(mallPageView);
                            } catch (Exception e) {
                                e.printStackTrace();
                                logger.error("插入失败，", e);
                            }
                            logger.info("首页广告渠道点击明细 PV和UV 操作成功" + grainValidateRepeat.getId());
                            return ResponseUtil.ok(jsonObject);
                        }
                    }
                }
            }

            // Todo 2021-01-27 判断整点红包口令
            if (StringUtils.isNotBlank(tkl)) {
                int number = tkl.length() - tkl.replaceAll("Z#", "").length();
                logger.info("number=" + number);
                if (number >= 4) {
                    if (StringUtils.isBlank(userId)) {
                        return ResponseUtil.unlogin();
                    }
                    String resultSub = RedpacketUtil.subString(tkl, "Z#", "Z#", 2);
                    logger.info("结果：" + resultSub);
                    if (StringUtils.isNotBlank(resultSub)) {
                        MallUser byId = userService.findById(userId);
                        // 没有直推人锁定关系
                        if (null != byId && StringUtils.isBlank(byId.getDirectLeader())) {
                            if (!resultSub.equals(userId)) {
                                byId.setDirectLeader(resultSub);
                                // set绑定直推人时间
                                byId.setLockTime(LocalDateTime.now());
                                userService.updateById(byId);

                                // 这里是 注册完了，如果用户有直推人，就找上六级的单位
                                C2cUtils.c2cUtilsInsert(userId, resultSub, 6);
                            }
                        }
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("industryId", "90");
                        return ResponseUtil.ok(jsonObject);
                    }
                }
            }

            // Todo 2021-02-05 判断拼团红包
            if (StringUtils.isNotBlank(tkl)) {
                int number = tkl.length() - tkl.replaceAll("P#", "").length();
                logger.info("number=" + number);
                if (number >= 4) {
                    String resultSub = RedpacketUtil.subString(tkl, "P#", "P#", 2);
                    logger.info("结果：" + resultSub);
                    if (StringUtils.isNotBlank(resultSub)) {
                        GameRedpacketRain grainValidateRepeat = gameRedpacketRainService.selectGameRedpacketByPassword(resultSub);
                        if (null != grainValidateRepeat) {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("id", grainValidateRepeat.getId());
                            jsonObject.put("industryId", "91");
                            jsonObject.put("nickName", wxRedRankService.indexParseTklV2(grainValidateRepeat));
                            return ResponseUtil.ok(jsonObject);
                        }
                    }
                }
            }

            // Todo 2021-03-01 判断商圈线下红包,等下面新需求上线，这里可以移除掉
            if (StringUtils.isNotBlank(tkl)) {
                int number = tkl.length() - tkl.replaceAll("C#", "").length();
                logger.info("number=" + number);
                if (number >= 4) {
                    String resultSub = RedpacketUtil.subString(tkl, "C#", "C#", 2);
                    logger.info("结果：" + resultSub);
                    if (StringUtils.isNotBlank(resultSub)) {
                        GameRedpacketRain grainValidateRepeat = gameRedpacketRainService.selectGameRedpacketByPassword(resultSub);
                        if (null != grainValidateRepeat) {
                            if (userId == null) {
                                return ResponseUtil.unlogin();
                            }
                            MallUser usr = userService.findById(userId);
                            // 没有直推人锁定关系
                            if (usr != null) {
                                boolean isUpdate = false;
                                if (StringUtils.isBlank(usr.getDirectLeader()) && !grainValidateRepeat.getUserId().equals(userId)) {
                                    usr.setDirectLeader(grainValidateRepeat.getUserId());
                                    // set绑定直推人时间
                                    usr.setLockTime(LocalDateTime.now());
                                    isUpdate = true;
                                    usr.setChannelId(MallUserEnum.CHANNEL_ID_3.getCode());
                                }
                                if (isUpdate) {
                                    userService.updateById(usr);

                                    // 这里是 注册完了，如果用户有直推人，就找上六级的单位
                                    C2cUtils.c2cUtilsInsert(userId, resultSub, 6);
                                }
                            }
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("id", grainValidateRepeat.getId());
                            jsonObject.put("industryId", "90");
                            jsonObject.put("nickName", wxRedRankService.indexParseTklV2(grainValidateRepeat));
                            return ResponseUtil.ok(jsonObject);
                        }
                    }
                }
            }

            // Todo 2021-03-10 判断商圈线下红包(新需求)
            if (StringUtils.isNotBlank(tkl)) {
                int number = tkl.length() - tkl.replaceAll("A#", "").length();
                if (number >= 4) {
                    String resultSub = RedpacketUtil.subString(tkl, "A#", "A#", 2);
                    logger.info("结果：" + resultSub);
                    if (StringUtils.isNotBlank(resultSub)) {
                        if (userId == null) {
                            return ResponseUtil.unlogin();
                        }
                        JSONObject jsonObject = wxRedRankService.downRed(userId, resultSub);
                        if ("0000".equals(String.valueOf(jsonObject.get("code")))) {
                            return ResponseUtil.ok(jsonObject);
                        }
                    }
                }
            }

            // Todo 2021-07-02 判断团购分享
            if (StringUtils.isNotBlank(tkl)) {
                int number = tkl.length() - tkl.replaceAll("T#", "").length();
                logger.info("判断团购分享number=" + number);
                if (number >= 4) {
                    if (StringUtils.isBlank(userId)) {
                        return ResponseUtil.unlogin();
                    }
                    String resultSub = RedpacketUtil.subString(tkl, "T#", "T#", 2);
                    logger.info("判断团购分享结果：" + resultSub);
                    if (StringUtils.isNotBlank(resultSub)) {
                        return indexGoodsComponent.groupPasswordAnalysisV2(resultSub, userId);
                    }
                }
            }

            if (tkl.contains("m.tb.cn")) {
                industryId = "4";
                Map<String, String> paramMap = new LinkedHashMap<String, String>();
                paramMap.put("apikey", "V9WgLR85kJDHCLsoE0prjRzGKnpvAdd4");
                paramMap.put("tkl", tkl);
                String result = HttpUtil.sendPost("http://api.91tool.com/tkl/query", paramMap);
                JSONObject object = JSONObject.parseObject(result);

                if (Objects.equals(object.getInteger("code"), 200)) {
                    JSONObject resultObj = JSONObject.parseObject(object.get("data").toString());
                    if (!Objects.equals(null, resultObj.getString("num_iid"))) {

                        resultObj.put("goodsId", resultObj.getString("num_iid"));
                        JSONObject originInfo = new JSONObject();

                        originInfo.put("title", resultObj.getString("content"));

                        originInfo.put("image", resultObj.getString("pic_url"));

                        originInfo.put("price", resultObj.getString("price"));

                        // originInfo.put("discountPrice", resultObj.getString("content"));

                        resultObj.put("originInfo", originInfo);

                        resultObj.put("industryId", industryId);

                        // 再解析出优惠价，优惠金额和佣金比例
                        TreeMap<String, String> paraMapGoods = new TreeMap<>();
                        paraMapGoods.put("version", "v1.2.2");
                        paraMapGoods.put("appKey", PlateFromConstant.DaTaoKeAppKey);
                        paraMapGoods.put("goodsId", resultObj.getString("num_iid"));
                        paraMapGoods.put("sign", SignMD5Util.getSignStr(paraMapGoods, PlateFromConstant.DaTaoKeAppSecret));
                        String goodResult = HttpUtils.sendGet(PlateFromConstant.DaTaoKeGoodsDetail, paraMapGoods);

                        JSONObject goodObject = JSONObject.parseObject(goodResult);
                        if ("0".equals(goodObject.getString("code"))) {
                            JSONObject ob = JSONObject.parseObject(goodObject.get("data").toString());

                            String title = ob.getString("title");
                            originInfo.put("title", title);
                            String pic = ob.getString("mainPic");
                            originInfo.put("image", pic);

                            originInfo.put("price", new BigDecimal(ob.getString("originalPrice")));
                            actualPrice = new BigDecimal(ob.getString("actualPrice"));
                            originInfo.put("discountPrice", actualPrice);

                            if (StringHelper.isNotNullAndEmpty(ob.getInteger("couponPrice"))) {
                                amount = new BigDecimal(ob.getString("couponPrice"));
                            }

                            if (StringHelper.isNotNullAndEmpty(ob.getString("commissionRate"))) {
                                rate = new BigDecimal(ob.getString("commissionRate")).divide(new BigDecimal(100));
                            }

                            originInfo.put("amount", amount);
                            resultObj.put("originInfo", originInfo);
                        } else {
                            isSuccess = false;
                        }

                        if (actualPrice.compareTo(new BigDecimal(0)) == 1 && rate.compareTo(new BigDecimal(0)) == 1) {
                            profit = actualPrice.multiply(rate).multiply(new BigDecimal("0.9")).setScale(2, BigDecimal.ROUND_DOWN);

                            if (profit != null && profit.doubleValue() >= 0) {

                                // Map<String, Object> map = (Map<String, Object>) AccountUtils.getProfit2(userId,
                                // PlateFromConstant.BUSSINESSID_DTK, profit, "JL20002");
                                Map<String, Object> map = null;
                                // 查询分润计算方式(key_value 0:服务商、区代、市代方式 1:用户firstLeader、secondLeader、thirderLeader方式 )
                                Map<String, String> profitTypeMap = mallSystemConfigService.listProfitType();
                                String profitType = profitTypeMap.get("Mall_profit_type");
                                if (Objects.equals(profitType, "0")) {
                                    map = (Map<String, Object>) AccountSFUtils.getProfit1(userId, PlateFromConstant.BUSSINESSID_DTK, profit, "JL20002", MallUserProfitNewEnum.PROFIT_TYPE_1.strCodeInt());
                                } else {
                                    /** 方式二：旧模式（firstLeader、secondLeader、thirderLeader方式）--【第三方购物和权益商品】 **/
                                    map = (Map<String, Object>) AccountSFUtils.getProfit2(userId, PlateFromConstant.BUSSINESSID_DTK, profit, "JL20002");
                                }

                                if (ObjectUtil.isNotNull(map) && map.containsKey("commissionReward")) {
                                    if (ObjectUtil.isNotNull(map.get("commissionReward"))) {
                                        commissionFee = new BigDecimal(map.get("commissionReward").toString());
                                    }
                                }

                            }
                        }

                        resultObj.put("commissionFee", commissionFee);
                        resultObj.put("findDetail", isSuccess);

                        return ResponseUtil.ok(resultObj);
                    }

                } else if (Objects.equals(object.getInteger("code"), 4005)) {
                    return ResponseUtil.fail(502, "积分不足");
                } else if (Objects.equals(object.getInteger("code"), 4006)) {
                    return ResponseUtil.fail(504, "请求拒绝");
                } else if (Objects.equals(object.getInteger("code"), 4003)) {
                    return ResponseUtil.fail(504, "参数不正确");
                }
            } else {
                Map<String, Object> map = new HashMap<String, Object>();
                if (tkl.contains("item.m.jd.com") || tkl.contains("u.jd.com")) {
                    industryId = "5";
                    map.put("industryId", industryId);

                    logger.info("------------------------------解析京东商品-------------------------------------");
                    JdClient client = new DefaultJdClient(PlateFromConstant.JDGoodsList, "", PlateFromConstant.JDNewAppKey, PlateFromConstant.JDNewAppSecret);

                    UnionOpenGoodsQueryRequest request = new UnionOpenGoodsQueryRequest();
                    GoodsReq goodsReqDTO = new GoodsReq();
                    goodsReqDTO.setKeyword(tkl);
                    request.setGoodsReqDTO(goodsReqDTO);

                    UnionOpenGoodsQueryResponse response = client.execute(request);

                    if ("success".equals(response.getMessage())) {
                        GoodsResp[] list = response.getData();
                        List goods_list = Arrays.asList(list);
                        if (CollectionUtils.isNotEmpty(goods_list)) {
                            for (int i = 0; i < goods_list.size(); i++) {

                                JSONObject originInfo = new JSONObject();

                                map.put("originInfo", originInfo);

                                // 获取商品信息
                                GoodsResp goodsResp = (GoodsResp) goods_list.get(i);
                                ImageInfo imageInfo = goodsResp.getImageInfo();
                                PriceInfo priceInfo = goodsResp.getPriceInfo();
                                CommissionInfo commissionInfo = goodsResp.getCommissionInfo();

                                map.put("goodsId", String.valueOf(goodsResp.getSkuId()));
                                UrlInfo[] imageInfos = imageInfo.getImageList();
                                UrlInfo imageList = imageInfos[0];
                                if (ObjectUtils.anyNotNull(priceInfo.getLowestCouponPrice())) {
                                    actualPrice = new BigDecimal(priceInfo.getLowestCouponPrice());
                                } else {
                                    actualPrice = new BigDecimal(priceInfo.getPrice());
                                }

                                originInfo.put("discountPrice", actualPrice);

                                originInfo.put("title", goodsResp.getSkuName());

                                originInfo.put("image", imageList.getUrl());

                                originInfo.put("price", new BigDecimal(priceInfo.getPrice()));

                                if (StringHelper.isNotNullAndEmpty(commissionInfo.getCommissionShare())) {
                                    rate = new BigDecimal(commissionInfo.getCommissionShare()).divide(new BigDecimal(100));
                                }

                                map.put("originInfo", originInfo);
                            }

                        }
                    } else {
                        isSuccess = false;
                    }

                } else if (tkl.contains("https://mobile.yangkeduo.com")) {
                    industryId = "8";
                    map.put("industryId", industryId);
                    String goodsId = null;
                    // 解析出商品id
                    Map<String, String> goodsMap = UrlAnalysisUtil.URLRequest(tkl);
                    if (!goodsMap.isEmpty()) {
                        goodsId = goodsMap.get("goods_id");
                    }
                    logger.info("拼多多商品id" + goodsId);
                    // 获取详情
                    PddDdkGoodsDetailRequest request = new PddDdkGoodsDetailRequest();
//                    List<Long> goodsIdList = new ArrayList<Long>();
//                    goodsIdList.add(Long.parseLong(goodsId));
                    request.setGoodsSign(goodsId);
                    PddDdkGoodsDetailResponse response = null;
                    PopClient client = new PopHttpClient(PlateFromConstant.PDD_CLIENT_ID, PlateFromConstant.PDD_CLIENT_SECRET);
                    try {
                        response = client.syncInvoke(request);
                        if (!Objects.equals(null, response.getErrorResponse())) {
                            isSuccess = false;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (!Objects.equals(null, response.getGoodsDetailResponse())) {
                        JSONArray productDetailJSON = JSONArray.parseArray(JSONObject.parseObject(JSONObject.parseObject(JsonUtil.transferToJson(response)).getString("goods_detail_response")).getString("goods_details"));
                        if (CollectionUtils.isNotEmpty(productDetailJSON)) {
                            for (int i = 0; i < productDetailJSON.size(); i++) {
                                JSONObject object = (JSONObject) productDetailJSON.get(i);
                                map.put("goodsId", object.getString("goods_id"));
                                JSONObject originInfo = new JSONObject();

                                originInfo.put("title", object.getString("goods_name"));

                                originInfo.put("image", object.getString("goods_image_url"));

                                originInfo.put("price", new BigDecimal(object.getString("min_group_price")).divide(new BigDecimal(100)));

                                originInfo.put("discountPrice",
                                        new BigDecimal(object.getString("min_group_price")).divide(new BigDecimal(100)).subtract(new BigDecimal(object.getString("coupon_discount")).divide(new BigDecimal(100))));

                                actualPrice = new BigDecimal(object.getString("min_group_price")).divide(new BigDecimal(100)).subtract(new BigDecimal(object.getString("coupon_discount")).divide(new BigDecimal(100)));

                                rate = new BigDecimal(object.getString("promotion_rate")).divide(new BigDecimal(1000));

                                map.put("originInfo", originInfo);

                            }
                        }
                    }

                }

                if (actualPrice.compareTo(new BigDecimal(0)) == 1 && rate.compareTo(new BigDecimal(0)) == 1) {
                    profit = actualPrice.multiply(rate).multiply(new BigDecimal("0.9")).setScale(2, BigDecimal.ROUND_DOWN);

                    if (profit != null && profit.doubleValue() >= 0) {

                        // Map<String, Object> ratemap = (Map<String, Object>) AccountUtils.getProfit2(userId,
                        // industryId, profit, "JL20002");
                        Map<String, Object> ratemap = null;
                        // 查询分润计算方式(key_value 0:服务商、区代、市代方式 1:用户firstLeader、secondLeader、thirderLeader方式 )
                        Map<String, String> profitTypeMap = mallSystemConfigService.listProfitType();
                        String profitType = profitTypeMap.get("Mall_profit_type");
                        if (Objects.equals(profitType, "0")) {
                            ratemap = (Map<String, Object>) AccountSFUtils.getProfit1(userId, PlateFromConstant.BUSSINESSID_DTK, profit, "JL20002", MallUserProfitNewEnum.PROFIT_TYPE_1.strCodeInt());
                        } else {
                            /** 方式二：旧模式（firstLeader、secondLeader、thirderLeader方式）--【第三方购物和权益商品】 **/
                            ratemap = (Map<String, Object>) AccountSFUtils.getProfit2(userId, PlateFromConstant.BUSSINESSID_DTK, profit, "JL20002");
                        }

                        if (ObjectUtil.isNotNull(ratemap) && ratemap.containsKey("commissionReward")) {
                            if (ObjectUtil.isNotNull(ratemap.get("commissionReward"))) {
                                commissionFee = new BigDecimal(ratemap.get("commissionReward").toString());
                            }
                        }

                    }
                }
                map.put("findDetail", isSuccess);
                map.put("commissionFee", commissionFee);
                return ResponseUtil.ok(map);
            }

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("解析口令失败++++++", e);
        }
        return ResponseUtil.fail();

    }


    /**
     * 未莱卡优惠券商品列表
     *
     * @param data id类目id
     * @param data page第几页
     * @param data size每页数据
     * @return
     * @throws Exception
     */
    @PostMapping("/getGoodsHSCard")
    public Object getCouponGoodsHSCard(@RequestBody String data, HttpServletRequest request) throws Exception {

//        String sign = JacksonUtil.parseString(data, "sign");

        String id = JacksonUtil.parseString(data, "id");
        Long page = Long.parseLong(JacksonUtil.parseString(data, "page"));
        Long size = Long.parseLong(JacksonUtil.parseString(data, "size"));

        if (page <= 0) {
            page = (long) 1;
        }

        Map<String, Object> paramMap = new HashMap<>();

        paramMap.put("id", id);
        paramMap.put("page", page);
        paramMap.put("size", size);
        String string = JSONObject.toJSONString(paramMap);// 请求参数转换成字符串进行加密处理
//        boolean flag = Md5Utils.verifySignature(string, sign);
//        if (!flag) {
//            return ResponseUtil.fail(403, "无效签名");
//        }

        try {

            List<String> couponIds = MallGoodsCouponCodeService.getCouponIds();

            String url = platFormServerproperty.getUrl(MallConstant.TAOBAO);
            Map<String, Object> mappMap = new HashMap<String, Object>();
            mappMap.put("plateform", "dtk");
            mappMap.put("id", id);
            mappMap.put("page", page);
            mappMap.put("size", size);
            if (CollectionUtils.isNotEmpty(couponIds)) {
                mappMap.put("couponIds", couponIds);
            }


            Header[] headers = new BasicHeader[]{new BasicHeader("method", "getCouponAndQuanyiGoods"), new BasicHeader("version", "v1.0")};
            String result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);

            JSONObject object = JSONObject.parseObject(result);

            if (!"0".equals(object.getString("errno"))) {
                return ResponseUtil.fail();
            }
            JSONObject obj = JSONObject.parseObject(object.get("data").toString());
            if (Objects.equals("coupon", obj.getString("type"))) {

                List<MallGoodsDTO> goodsList = JSONArray.parseArray(obj.get("goods").toString(), MallGoodsDTO.class);
                // List<String> couponIds=new ArrayList<String>();
                // List<Integer> counts=new ArrayList<Integer>();
                // if (CollectionUtils.isNotEmpty(goodsList)) {
                // for (int i = 0; i < goodsList.size(); i++) {
                // Integer count = MallGoodsCouponCodeService.getGoodsCouponStock(goodsList.get(i).getCouponId());
                // if (count==0) {
                // goodsList.remove(i);
                // i--;
                // }else {
                // goodsList.get(i).setStock(count);
                // }
                // //couponIds.add(goodsList.get(i).getCouponId());
                // goodsList.get(i).setStock(1);
                // }
                // if (CollectionUtils.isNotEmpty(couponIds)) {
                // counts=MallGoodsCouponCodeService.getGoodsCouponCounts(couponIds);
                // }
                //
                // for (int i = 0; i < goodsList.size(); i++) {
                // if (counts.get(i)==0) {
                // goodsList.remove(i);
                // i--;
                // }else {
                // goodsList.get(i).setStock(counts.get(i));
                // }
                // }

                // }
                obj.put("goods", goodsList);
            }
            return ResponseUtil.ok(obj);
        } catch (Exception e) {
            logger.error("商品查询异常");
            throw e;
        }
    }


    /**
     * 未莱卡优惠券或权益商品详情
     *
     * @param data
     * @param request
     * @return
     * @throws Exception
     */
    @PostMapping("/getGoodsDetailHSCard")
    public Object getGoodsDetailHSCard(@RequestBody String data, HttpServletRequest request) throws Exception {

        String sign = JacksonUtil.parseString(data, "sign");
        String id = JacksonUtil.parseString(data, "id");
        String type = JacksonUtil.parseString(data, "type");
        Map<String, Object> paramMap = new HashMap<>();

        paramMap.put("id", id);
        paramMap.put("type", type);

        String string = JSONObject.toJSONString(paramMap);// 请求参数转换成字符串进行加密处理
        boolean flag = Md5Utils.verifySignature(string, sign);
        if (!flag) {
            return ResponseUtil.fail(403, "无效签名");
        }

        String result = "";
        try {
            String url = platFormServerproperty.getUrl(MallConstant.TAOBAO);
            Map<String, Object> mappMap = new HashMap<String, Object>();
            if (Objects.equals("quanyi", type)) {
                mappMap.put("plateform", "fulu");
                mappMap.put("categoryId", id);

                Header[] headers = new BasicHeader[]{new BasicHeader("method", "getQYMallCategoryByIdByFuLu"), new BasicHeader("version", "v1.0")};
                result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);
            } else if (Objects.equals("coupon", type)) {
                mappMap.put("plateform", "dtk");
                mappMap.put("id", id);
                Header[] headers = new BasicHeader[]{new BasicHeader("method", "getCouponGoodsDetail"), new BasicHeader("version", "v1.0")};
                result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);
            }

            JSONObject object = JSONObject.parseObject(result);

            if (!"0".equals(object.getString("errno"))) {
                return ResponseUtil.fail();
            }

            return ResponseUtil.ok(JSONObject.parseObject(object.get("data").toString()));
        } catch (Exception e) {
            logger.error("商品查询异常");
            throw e;
        }
    }


    /**
     * 商品详情
     * <p>
     * 用户可以不登录。 如果用户登录，则记录用户足迹以及返回用户收藏信息。
     *
     * @param mrequest
     * @param id       商品ID
     * @return 商品详情
     * @throws IOException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    @GetMapping("shareDetail")
    public Object shareDetail(HttpServletRequest mrequest, String id) throws Exception {

        MallGoods info = null;
        MallGoodsDTO mallGoodsDTO = null;
        JSONObject jsonCoupon = null;
        Map<String, Object> data = new HashMap<>();

        if (org.apache.commons.lang.StringUtils.isNotBlank(id)) {
            String url = platFormServerproperty.getUrl(MallConstant.TAOBAO);
            Map<String, Object> mappMap = new HashMap<String, Object>();
            mappMap.put("plateform", "dtk");
            mappMap.put("id", id);

            Header[] headers = new BasicHeader[]{new BasicHeader("method", "getGoodsInfoById"), new BasicHeader("version", "v1.0")};

            String result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);

            JSONObject object = JSONObject.parseObject(result);

            if (!object.getString("errno").equals("0")) {
                return ResponseUtil.fail();
            }
            result = object.get("data").toString();
            JSONObject jsonObject = object.getJSONObject("data");
            System.out.println("-----" + jsonObject.get("coupon"));
            if (ObjectUtil.isNotNull(jsonObject.get("coupon"))) {
                jsonCoupon = jsonObject.parseObject(jsonObject.get("coupon").toString());
                data.put("coupon", jsonCoupon);
            }

            mallGoodsDTO = JSONObject.parseObject(jsonObject.get("goods").toString(), MallGoodsDTO.class);

        }

        info = new MallGoods();

        try {
            BeanUtils.copyProperties(info, mallGoodsDTO);
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }

        if (ObjectUtil.isNotNull(jsonCoupon)) {

            // 添加优惠金额和满减金额
            if (org.apache.commons.lang3.StringUtils.isNotBlank(jsonCoupon.getString("discount"))) {
                info.setCouponDiscount(new BigDecimal(jsonCoupon.getString("discount")));
            } else {
                info.setCouponDiscount(new BigDecimal("0.0"));
            }

            if (org.apache.commons.lang3.StringUtils.isNotBlank(jsonCoupon.getString("min"))) {
                info.setMin(new BigDecimal(jsonCoupon.getString("min")));
            }
        }

        try {

        } catch (Exception e1) {
            logger.error("商品详情解析json异常" + e1.getMessage());
        }

        data.put("info", info);

        return ResponseUtil.ok(data);
    }


    @GetMapping("/getPrivilegeGoods")
    public Object qyIndexBak(@LoginUser String userId, HttpServletRequest request, @RequestParam(value = "version", required = false) String version) {
        logger.info("--------------------获取特权卡数据--------------------------------------------");


        JSONArray array = null;
        try {
            String url = platFormServerproperty.getUrl(MallConstant.SUNING);


            Map<String, Object> mappMap = new HashMap<String, Object>();
            mappMap.put("plateform", "fulu");
            mappMap.put("type", "privilege");

            Header[] headers = new BasicHeader[]{new BasicHeader("method", "getQYAllMallCategoryByFuLu"), new BasicHeader("version", "v1.0")};

            String result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);

            JSONObject object = JSONObject.parseObject(result);

            if (!object.getString("errno").equals("0")) {
                return ResponseUtil.fail();
            }

            array = JSONArray.parseArray(object.get("data").toString());


            if (RetrialUtils.examine(request, version)) {


                if (CollectionUtils.isNotEmpty(array)) {
                    for (int i = 0; i < array.size(); i++) {
                        JSONObject obj = (JSONObject) array.get(i);
                        if (Objects.equals(obj.getString("name"), "热门推荐")) {
                            array.remove(obj);
                            i--;
                        }

                        if (Objects.equals(obj.getString("name"), "优享充值")) {
                            array.remove(obj);
                            i--;
                        }

                        if (Objects.equals(obj.getString("name"), "网游互娱")) {
                            array.remove(obj);
                            i--;
                        }

                        if (Objects.equals(obj.getString("name"), "游戏专区")) {
                            array.remove(obj);
                            i--;
                        }

                        if (Objects.equals(obj.getString("name"), "在线阅读")) {
                            array.remove(obj);
                            i--;
                        }

                        if (Objects.equals(obj.getString("name"), "礼品专区")) {
                            array.remove(obj);
                            i--;
                        }

                        if (Objects.equals(obj.getString("name"), "生活服务")) {
                            array.remove(obj);
                            i--;
                        }

                        if (Objects.equals(obj.getString("name"), "美食餐饮")) {
                            array.remove(obj);
                            i--;
                        }

                        if (Objects.equals(obj.getString("name"), "旅游出行")) {
                            array.remove(obj);
                            i--;
                        }

                    }
                }


            }


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // executorService.shutdown();
        }
        return ResponseUtil.ok(array);
    }


    /**
     * 优享充值商品
     */
    @GetMapping("/getPrivilegeAllGoods")
    public Object getPrivilegeAllGoods(@RequestParam(defaultValue = "1") Integer page, HttpServletRequest request, @RequestParam(defaultValue = "10") Integer limit,
                                       @RequestParam(value = "version", required = false) String version) {

        if (page <= 0) {
            page = 1;
        }


        try {

            String url = platFormServerproperty.getUrl(MallConstant.TAOBAO);
            Map<String, Object> mappMap = new HashMap<String, Object>();
            mappMap.put("plateform", "dtk");
            mappMap.put("page", page);
            mappMap.put("size", limit);


            if (RetrialUtils.examine(request, version)) {
                mappMap.put("flag", true);
            }


            Header[] headers = new BasicHeader[]{new BasicHeader("method", "getPrivilegeGoodsByPage"), new BasicHeader("version", "v1.0")};
            String result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);

            JSONObject object = JSONObject.parseObject(result);

            return ResponseUtil.ok(JSONObject.parse(String.valueOf(object.get("data"))));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return ResponseUtil.fail();
    }


    /**
     * 下午茶商品
     */
    @GetMapping("/getAfternoonTeaGoods")
    public Object getAfternoonTeaGoods(@RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit) {

        if (page <= 0) {
            page = 1;
        }

        try {
            List<String> couponIds = MallGoodsCouponCodeService.getCouponIds();
            String url = platFormServerproperty.getUrl(MallConstant.TAOBAO);
            Map<String, Object> mappMap = new HashMap<String, Object>();
            mappMap.put("plateform", "dtk");
            mappMap.put("page", page);
            mappMap.put("size", limit);
            if (CollectionUtils.isNotEmpty(couponIds)) {
                mappMap.put("couponIds", couponIds);
            }

            Header[] headers = new BasicHeader[]{new BasicHeader("method", "queryAfternoonTeaGoods"), new BasicHeader("version", "v1.0")};
            String result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);

            JSONObject object = JSONObject.parseObject(result);

            return ResponseUtil.ok(JSONObject.parse(object.get("data").toString()));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return ResponseUtil.fail();
    }

    /**
     * 根据分类id和筛选条件获取商品列表
     *
     * @param categoryId 类目id
     * @param name       商品名称
     * @param material   材质
     * @param minSize    最小尺寸
     * @param maxSize    最大尺寸
     * @param technology 工艺
     * @param minPrice   最低价格
     * @param maxPrice   最高价格
     * @param place      产地
     * @param page
     * @param limit
     * @param sort       排序
     * @param sortType   排序类型
     * @return
     */
    @GetMapping("/queryByScreeningConditions")
    public Object queryByScreeningConditions(@LoginUser String userId, String categoryId, String name, String material, Integer minSize, Integer maxSize, String technology, String minPrice
            , String maxPrice, String place, @RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit, String sort, String sortType) {

        // 添加到搜索历史
        if (userId != null && !StringUtils.isEmpty(name)) {
            MallSearchHistory searchHistoryVo = new MallSearchHistory();
            searchHistoryVo.setKeyword(name);
            searchHistoryVo.setUserId(userId);
            searchHistoryVo.setFrom("wx");
            searchHistoryService.save(searchHistoryVo);
        }
        Object result = goodsService.queryByScreeningConditions(userId, categoryId, name, material, minSize, maxSize, technology, minPrice, maxPrice, place, page, limit, sort, sortType);
        return ResponseUtil.ok(result);
    }


    /**
     * 商品添加
     */
    @PostMapping("/create")
    public Object create(@RequestBody GoodsAllinone goodsAllinone) {
        String code =  goodsService.create(goodsAllinone).toString();
        if("1".equals(code)){
            MallGoods mallGoods = goodsAllinone.getGoods();
            if(mallGoods.getIsOnSale()) {
                String url = URL + goodsAllinone.getGoods().getId();
                com.graphai.open.utils.HttpRequestUtils.get(url, null, null);
            }else{
                String url = DELETE_URL + goodsAllinone.getGoods().getId();
                com.graphai.open.utils.HttpRequestUtils.get(url, null, null);
            }
        }
        return ResponseUtil.ok();
    }


    /**
     * 商品修改
     */
    @PostMapping("/update")
    public Object update(@RequestBody GoodsAllinone goodsAllinone) {
        String code =  goodsService.update(goodsAllinone).toString();
        if("1".equals(code)){
            MallGoods mallGoods = goodsAllinone.getGoods();
            if(mallGoods.getIsOnSale()) {
                String url = URL + goodsAllinone.getGoods().getId();
                com.graphai.open.utils.HttpRequestUtils.get(url, null, null);
            }else{
                String url = DELETE_URL + goodsAllinone.getGoods().getId();
                com.graphai.open.utils.HttpRequestUtils.get(url, null, null);
            }
        }
        return ResponseUtil.ok();
    }


    /**
     * 普通商品详情
     */
    @GetMapping("/groupDetail")
    public Object groupDetail(@LoginUser String userId, @NotNull String id) {
        // 记录用户的足迹 异步处理
        String goodsId = id;
        MallGoods mallGoods = goodsService.findById(goodsId);
        if (userId != null) {
            executorService.execute(() -> {
                MallFootprint footprint = new MallFootprint();
                footprint.setUserId(userId);
                footprint.setGoodsId(goodsId);
                footprint.setMerchantId(mallGoods.getMerchantId());
                footprintService.add(footprint);

                MallUser uUser = new MallUser();
                uUser.setId(userId);
                uUser.setUpdateTime(LocalDateTime.now());
                uUser.setFootprintId(footprint.getId());
                userService.updateById(uUser);
            });
        }

        return goodsService.detail(userId, mallGoods, id);
    }

    /**
     * 活动商品详情
     *
     * @param userId
     * @param id           商品id
     * @param activityType 活动类型(0:秒杀；1：直播；2：团购)
     * @return
     */
    @GetMapping("/activityGoodsDetail")
    public Object liveGoodsDetail(@LoginUser String userId, @NotNull String id, Integer activityType) {
        // 记录用户的足迹 异步处理
        String goodsId = id;
        MallGoods mallGoods = goodsService.findById(goodsId);
        if (userId != null) {
            executorService.execute(() -> {
                MallFootprint footprint = new MallFootprint();
                footprint.setUserId(userId);
                footprint.setGoodsId(goodsId);
                footprint.setMerchantId(mallGoods.getMerchantId());
                footprintService.add(footprint);

                MallUser uUser = new MallUser();
                uUser.setId(userId);
                uUser.setUpdateTime(LocalDateTime.now());
                uUser.setFootprintId(footprint.getId());
                userService.updateById(uUser);
            });
        }

        return goodsService.activityGoodsDetail(userId, mallGoods, id, activityType);
    }


    /**
     * 聚合合伙人商品列表
     */
    @GetMapping("/groupList")
    public Object groupList(@RequestParam(value = "categoryId", required = false) String categoryId,
                            @RequestParam(defaultValue = "1") Integer page,
                            @RequestParam(defaultValue = "10") Integer limit,
                            @RequestParam(value = "mobile", required = true) String mobile,
                            @RequestParam(value = "storeId", required = false) String storeId) {
        List<MallGroupGoodsDTO> grouList = null;
        List<String> gids = null;
        MallGoodsUser goodsUser = new MallGoodsUser();
        if (StringHelper.isNotNullAndEmpty(storeId)) {
            goodsUser.setMerchantId(storeId);
        }
        if (StringHelper.isNotNullAndEmpty(mobile)) {
            goodsUser.setMobile(mobile);
        }
        try {
            List<MallGoodsUser> mallGoodsUsers = mallGoodsUserService.query(goodsUser);
            if (CollectionUtils.isNotEmpty(mallGoodsUsers)) {
                gids = mallGoodsUsers.stream().map(MallGoodsUser::getGoodsId).collect(Collectors.toList());
                grouList = goodsService.queryByPhoneAndCateId(gids, categoryId, (page - 1) * limit, limit);
            } else {
                return ResponseUtil.ok(grouList);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseUtil.ok(grouList);
    }


    @GetMapping("/delete")
    public Object delete(@NotNull String id) {
        return goodsService.delete(id);
    }


    /**
     * 每日更新
     *
     * @param userId
     * @param page
     * @param limit
     * @return
     */
    @GetMapping("/dailyUpdate")
    public Object dailyUpdate(@LoginUser String userId, @RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit) {
        Object result = goodsService.dailyUpdate(userId, page, limit, "add_time");
        return ResponseUtil.ok(result);
    }

    @Autowired
    private IMallMerchantService mallMerchantService;

    /**
     * 工厂优选
     *
     * @param userId
     * @param page
     * @param limit
     * @return
     */
    @GetMapping("/excellent")
    public Object excellent(@LoginUser String userId, @RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit) {
        Object result = goodsService.dailyUpdate(userId, page, limit, "sales_tip");
        return ResponseUtil.ok(result);
    }

    @PostMapping("/getNewcomerGroupOrder")
    public Object getNewcomerGroupOrder(@LoginUser String userId) {
        if (StrUtil.isEmpty(userId)) {
            return ResponseUtil.unlogin();
        }
        return MallOrderService.getNewcomerGroupOrder(userId);
    }

    /**
     * 获取推荐商品
     *
     * @param page
     * @param limit
     * @return
     */
    @GetMapping("recommendGoods")
    public Object recommendGoods(@LoginUser String userId, @RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit) {
        Object obj = goodsService.recommendGoods(userId, page, limit);
        return ResponseUtil.ok(obj);
    }
    @Autowired
    private FreightTemplateService freightTemplateService;

    @PostMapping("/copy")
    public Object copy(@LoginUser String userId, @RequestBody GoodsAllinone goodsAllinone) {


        QueryWrapper<MallUserMerchant> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", userId);
        MallUserMerchant mallUserMerchant = mallUserMerchantService.getOne(queryWrapper);

        //判断商品是否导入过了，
        String goodId = goodsAllinone.getGoods().getId();
        MallGoodsExample goodsExample = new MallGoodsExample();
        goodsExample.or().andMerchantIdEqualTo(mallUserMerchant.getMerchantId()).andSrcGoodIdEqualTo(goodId).andDeletedEqualTo(false);
        List<MallGoods> list = goodsService.selectByExample(goodsExample);
        if(list.size() > 0){
            return ResponseUtil.fail("商品已经导入过了哟");
        }

        List<FreightTemplate> freightTemplates = freightTemplateService.list(new QueryWrapper<FreightTemplate>().lambda().eq(FreightTemplate::getMerchantId,mallUserMerchant.getMerchantId()));
        try {
            MallGoods mallGoods = goodsAllinone.getGoods();
            String oldGoodsId = mallGoods.getId();
            String goodsId = GraphaiIdGenerator.nextId("MallGoods");
            mallGoods.setId(goodsId);
            mallGoods.setSrcGoodId(oldGoodsId);
            mallGoods.setSellerId(mallGoods.getMerchantId());
            mallGoods.setMerchantId(mallUserMerchant.getMerchantId());
            mallGoods.setItemsale(0);
            mallGoods.setItemsale2(0);
            mallGoods.setTodaysale(0);

            if(freightTemplates.size() != 0){
                mallGoods.setTemplateId(freightTemplates.get(0).getId());
            }else{
                mallGoods.setTemplateId(null);
            }
            //复制商品参数
            List<MallGoodsAttribute> mallGoodsAttributes = mallGoodsAttributeService.queryByGid(oldGoodsId);
            for (MallGoodsAttribute item : mallGoodsAttributes) {
                item.setId(GraphaiIdGenerator.nextId("MallGoodsAttribute"));
                item.setGoodsId(goodsId);
                mallGoodsAttributeService.add(item);
            }
            //复制商品类目
            List<MallGoodsCategory> mallGoodsCategories = mallGoodsCategoryService.queryByGid(oldGoodsId);
            for (MallGoodsCategory item : mallGoodsCategories) {
                item.setId(GraphaiIdGenerator.nextId("MallGoodsCategory"));
                item.setGoodsId(goodsId);
                mallGoodsCategoryService.add(item);
            }
            //复制商品货品
            List<MallGoodsProduct> mallGoodsProducts = mallGoodsProductService.queryByGid(oldGoodsId);
            for (MallGoodsProduct item : mallGoodsProducts) {
                MallGoodsProduct[] mallGoodsProductsOld = goodsAllinone.getProducts();
                for(MallGoodsProduct oldProduct : mallGoodsProductsOld){
                    if(item.getId().equals(oldProduct.getId())){
                        item.setPrice(oldProduct.getPrice());
                        item.setFactoryPrice(oldProduct.getFactoryPrice());
                    }
                }

                item.setId(GraphaiIdGenerator.nextId("MallGoodsProduct"));
                item.setGoodsId(goodsId);
                mallGoodsProductService.add(item);
            }
            //复制商品规格
            List<MallGoodsSpecification> mallGoodsSpecifications = mallGoodsSpecificationService.queryByGid(oldGoodsId);
            for (MallGoodsSpecification item : mallGoodsSpecifications) {
                item.setId(GraphaiIdGenerator.nextId("MallGoodsSpecification"));
                item.setGoodsId(goodsId);
                mallGoodsSpecificationService.add(item);
            }
            goodsService.save(mallGoods);

            String url = URL + mallGoods.getId();
            com.graphai.open.utils.HttpRequestUtils.get(url, null, null);
            return ResponseUtil.ok();
        } catch (Exception e) {
            return ResponseUtil.fail(-1, e.getMessage());
        }
    }

}
