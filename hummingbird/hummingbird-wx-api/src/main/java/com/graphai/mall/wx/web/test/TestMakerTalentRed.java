package com.graphai.mall.wx.web.test;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.json.JacksonUtil;
import com.graphai.id.GraphaiIdGenerator;
import com.graphai.mall.admin.domain.MallPageView;
import com.graphai.mall.admin.domain.MallRedQrcodeRecord;
import com.graphai.mall.admin.domain.MallSystemRule;
import com.graphai.mall.admin.service.IGameRedHourRuleService;
import com.graphai.mall.admin.service.IMallPageViewService;
import com.graphai.mall.admin.service.IMallRedQrcodeRecordService;
import com.graphai.mall.db.domain.GameRedpacketRain;
import com.graphai.mall.db.domain.MallOrder;
import com.graphai.mall.db.domain.MallSystem;
import com.graphai.mall.db.domain.MallUser;
import com.graphai.mall.db.constant.account.AccountStatus;
import com.graphai.mall.db.service.game.GameRedpacketRainService;
import com.graphai.mall.db.service.order.MallOrderService;
import com.graphai.mall.db.service.pay.DoPayService;
import com.graphai.mall.db.service.red.RedStatus;
import com.graphai.mall.db.service.sysconfig.MallSystemConfigService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.constant.CommonConstant;
import com.graphai.mall.db.util.OrderUtil;
import com.graphai.mall.db.util.account.AccountUtils;

/**创客，达人，整点红包测试类*/

@RequestMapping("/test/makerTalentRed")
@RestController
public class TestMakerTalentRed {
    private final Log logger = LogFactory.getLog(TestMakerTalentRed.class);


    @Autowired
    private DoPayService doPayService;
    @Autowired
    private MallOrderService orderService;
    @Autowired
    private MallUserService userService;
    @Autowired
    private IMallRedQrcodeRecordService recordService;
    @Autowired
    private IGameRedHourRuleService redHourRuleService;
    @Autowired
    private MallSystemConfigService configService;
    @Autowired
    private IMallPageViewService pageViewService;
    @Autowired
    private GameRedpacketRainService gameRedpacketRainService;

    /**
     * 创客订单
     */
    @PostMapping("/maker")
    public void setMakerOrder(@RequestBody String body) throws IOException, InterruptedException, ExecutionException {
        String  pageNum = JacksonUtil.parseString(body,"pageNum");
        //1 创客 2 达人  3 整点红包
        String  type = JacksonUtil.parseString(body,"type");
        int num =Integer.parseInt(pageNum) ;
//        CompletableFuture[] futures = new CompletableFuture[num];
        ExecutorService threadPool= Executors.newFixedThreadPool(100);
        List<String> userIds = userService.findUserId(1000000);
        try {
            for (int i = 0; i < userIds.size(); i++) {
                String userId = userIds.get(i);
            threadPool.execute(new Runnable() {
                @Override
                public void run() {
                        runTask(type, userId);
                }
            });
            }
//            for (int i = 1; i < userIds.size(); i++) {
//                int j= i;
//                futures[i] = CompletableFuture.runAsync(() -> {
//                    // 升级创客
//                    try {
//                            runTask(type,userIds.get(j));
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }, JDiworkerConfig.CUSTOMER_COMMON_POOL);
//
//            }
//            CompletableFuture.allOf(futures).get();
            // 主线程必须在启动其它线程后立即调用CountDownLatch.await()方法，
            // 这样主线程的操作就会在这个方法上阻塞，直到其它线程完成各自的任务。
            // 计数器的值等于0时，主线程就能通过await()方法恢复执行自己的任务。
//            countDownLatch.await();
            logger.info("数据操作完成!可以在此开始其它业务");
        } finally {
            // 关闭线程池，释放资源
//            threadPool.shutdown();
        }

    }
    /**
     * 创客订单
     */
    @GetMapping("/maker/count")
    public Object getRedPacker(@RequestParam String bid) {
        Map<String,Object> map = new HashMap<>();
        map.put("total",orderService.countOrderSum(bid));
        return ResponseUtil.ok(map);
    }
    /**
     * 整点红包
     */
    @RequestMapping("/red")
    public void setRedPacker() {

    }





    public  void runTask(String type,String userId) {
           if ( type.equals("1")) {
                  // 业务逻辑，例如批量insert或者update
               logger.info("现在操作的是创客订单入库------");
               logger.info("线程名称------"+Thread.currentThread().getName());

                   //创建订单
                   MallOrder order = new MallOrder();
                   double amount = Math.random()*10;
                   order.setUserId(userId);
                   order.setOrderSn(GraphaiIdGenerator.nextId("OrderSN"));
                   order.setOrderStatus(OrderUtil.STATUS_PAY);
                   order.setGoodsPrice(BigDecimal.valueOf(amount));
                   order.setOrderPrice(BigDecimal.valueOf(amount));
                   order.setActualPrice(BigDecimal.valueOf(amount));
                   // 推荐人id后续用来修改锁定关系
                   // order.setTpid(firstLeaderId);
                   order.setSource("分库分表升级创客");
//                   order.setClientIp(clientIp);
                   // 订单类型 升级达人
                   order.setBid(AccountStatus.BID_38);
                   // 添加订单表
                   orderService.add(order);
                   //修改用户身份
                    MallUser user = new MallUser();
                     user.setIsMaker("1");
                    user.setId(userId);
                   userService.updateById(user);

             }else if(type.equals("2")){
               // 业务逻辑，例如批量insert或者update
               logger.info("现在操作的是达人订单入库------");
//               List<MallUser> users = userService.findUsers(page,limit);
//               for(int i =0;i<users.size();i++) {
                   MallOrder order = new MallOrder();
                   //随机金额
                   double amount = Math.random();
                   order.setId(GraphaiIdGenerator.nextId("OrderSN"));
                   order.setUserId(userId);
                   order.setOrderSn(GraphaiIdGenerator.nextId("OrderSN"));
                   order.setOrderStatus(OrderUtil.STATUS_PAY);
//                   order.setConsignee(users.get(i).getNickname());
//                   order.setMobile(users.get(i).getMobile());
                   order.setGoodsPrice(BigDecimal.valueOf(amount));
                   order.setOrderPrice(BigDecimal.valueOf(amount));
                   order.setActualPrice(BigDecimal.valueOf(amount));
                   // 使用该字段标识购买哪种类型的卡
                   order.setActivityType("2");
                   // 推荐人id后续用来修改锁定关系
                   // order.setTpid(firstLeaderId);
                   order.setSource("分库分表升级达人");

                   // 订单类型 升级达人
                   order.setBid(AccountStatus.BID_44);
                   // 添加订单表
                   orderService.add(order);
                   // 升级达人

                 //修改用户身份user.setIsMaker("1");
                   MallUser user = new MallUser();
                    user.setId(userId);
                    user.setIsTalent("1");
                   userService.updateById(user);
//               }
             }else if(type.equals("3")){
               // 业务逻辑，例如批量insert或者update
               logger.info("现在操作的是领取整点红包------");
               Map<String, Object> recordmap = new HashMap<>();
               /** 通过红包规则段获取最终领取金额 **/
               Random random = new Random();
               DecimalFormat df1 = new DecimalFormat("0.00");
               BigDecimal talResiduer  = new BigDecimal(df1.format(random.nextDouble() * (Double.parseDouble("0.09") - Double.parseDouble("0.02")) + Double.parseDouble("0.02")));
               recordmap.put("amount", talResiduer);
               recordmap.put("create_time", LocalDateTime.now());
               recordmap.put("userId", userId);
               recordmap.put("nickname", userId);
               recordmap.put("makerId", userId);
               recordmap.put("residuerStr", talResiduer);
               recordmap.put("talResiduer", talResiduer);
               try {
                   receive(recordmap);
               } catch (Exception e) {
                   e.printStackTrace();
               }
           }

      }
    public   String receive(Map<String, Object> bizmap) throws Exception {
        try {
            String msgId = MapUtils.getString(bizmap, "msgId");
            String device = MapUtils.getString(bizmap, "device");
            String rule = MapUtils.getString(bizmap, "mall_red_rule");
            String version = MapUtils.getString(bizmap, "version");
            String ip = MapUtils.getString(bizmap, "ip");
            // 原金额
            BigDecimal residuer = new BigDecimal(MapUtils.getString(bizmap, "residuerStr"));
            // 乘达人倍率后金额
            BigDecimal talResiduer = new BigDecimal(MapUtils.getString(bizmap, "talResiduer"));
            String userId = MapUtils.getString(bizmap, "userId");
            String makerId = MapUtils.getString(bizmap, "makerId");
            String grainId = MapUtils.getString(bizmap, "grainId");
            String ruleId = MapUtils.getString(bizmap, "ruleId");
            String xMallAd = MapUtils.getString(bizmap, "xMallAd");

            List<GameRedpacketRain> grain = gameRedpacketRainService.selectRedpacketList("11");
            MallSystemRule mallSystemRule = redHourRuleService.getById("1");
             Random random1= new Random();
            int i = random1.nextInt(24);
            /** 当前已经领取的人数 **/
//            int anum = Integer.parseInt(grain.get(i).getAlreadyNumber());// 已领取人数
            /** 当前时间段发放的总金额 **/
//            BigDecimal amount = grain.getAmount();
//
//            anum = anum + 1;
//            grain.setAlreadyNumber(anum + "");
//            // 累加金额
//            grain.setAmount(amount.add(talResiduer));
//            gameRedpacketRainService.editGameRedpacket(grain);
            // String rule = ruleMap.get("mall_red_rule");
            /** 用户收益 订单分润 **/
            Map<String, Object> paramsMap = new HashMap<>();
            paramsMap.put("dType", AccountStatus.DISTRIBUTION_TYPE_00);
            paramsMap.put("message", "整点红包奖励");
            paramsMap.put("capitalTrend", AccountStatus.CAPITAL_TREND_01);
            paramsMap.put("changeType", AccountStatus.CHANGE_TYPE_01);
            paramsMap.put("changeAccountType", AccountStatus.CHANGE_ACCOUNT_TYPE_2);
            paramsMap.put("tradeType", AccountStatus.TRADE_TYPE_01);
            /** 锁客者分润 直接分润 **/
            Map<String, Object> paramsMapMaker = new HashMap<>();
            paramsMapMaker.put("dType", AccountStatus.DISTRIBUTION_TYPE_10);
            paramsMapMaker.put("message", "整点红包分享收益");
            paramsMapMaker.put("capitalTrend", AccountStatus.CAPITAL_TREND_01);
            paramsMapMaker.put("changeType", AccountStatus.CHANGE_TYPE_01);
            paramsMapMaker.put("changeAccountType", AccountStatus.CHANGE_ACCOUNT_TYPE_2);
            paramsMapMaker.put("tradeType", AccountStatus.TRADE_TYPE_18);

            // 用户领取记录
            MallRedQrcodeRecord mallRedQrcodeRecord = new MallRedQrcodeRecord();
            mallRedQrcodeRecord.setCreateTime(LocalDateTime.now());
            mallRedQrcodeRecord.setUpdateTime(LocalDateTime.now());
            mallRedQrcodeRecord.setDeleted(false);
            mallRedQrcodeRecord.setRedpacketId(grain.get(i).getId());
            mallRedQrcodeRecord.setToUserId(userId);
            mallRedQrcodeRecord.setAmountType(RedStatus.MRQR_AMOUNT_TYPE_03);// 业务类型：01-红包码 02-app红包 03-整点红包
            mallRedQrcodeRecord.setType(RedStatus.MRQR_TYPE_02);// 分类 01：现金红包 02：整点红包
            mallRedQrcodeRecord.setTypeOfClaim(RedStatus.MRQR_TYPE_OF_CLAIM_01);// 领取类型 01：自领 02：推荐
//            mallRedQrcodeRecord.setClientIp(ip);
//            mallRedQrcodeRecord.setDevice(device);
//            mallRedQrcodeRecord.setMsgId(msgId);

            // 整点红包分享（推荐）收益记录
            MallRedQrcodeRecord recordMaker = new MallRedQrcodeRecord();
            recordMaker.setCreateTime(LocalDateTime.now());
            recordMaker.setUpdateTime(LocalDateTime.now());
            recordMaker.setDeleted(false);
            recordMaker.setRedpacketId(grain.get(i).getId());
            recordMaker.setRemark("整点红包分享（推荐）收益记录");
            recordMaker.setAmountType(RedStatus.MRQR_AMOUNT_TYPE_03);// 业务类型：01-红包码 02-app红包 03-整点红包
            recordMaker.setType(RedStatus.MRQR_TYPE_02);// 分类 01：现金红包 02：整点红包
            recordMaker.setTypeOfClaim(RedStatus.MRQR_TYPE_OF_CLAIM_02);// 领取类型 01：自领 02：推荐
//            recordMaker.setClientIp(ip);
//            recordMaker.setDevice(device);
//            recordMaker.setMsgId(msgId);

            MallPageView mallPageView = new MallPageView();
            if (org.apache.commons.lang3.StringUtils.isNotEmpty(version)) {
                mallPageView.setPlatform("app");
                mallRedQrcodeRecord.setPlatformResource("app");
                recordMaker.setPlatformResource("app");
            } else {
                recordMaker.setPlatformResource("xcx");
                mallRedQrcodeRecord.setPlatformResource("xcx");
                mallPageView.setPlatform("mini");
            }
//            recordMaker.setAdId(xMallAd);
//            mallRedQrcodeRecord.setAdId(xMallAd);

            MallSystem mallSystem = new MallSystem();
            mallSystem.setKeyName("mall_app_ad%");
            List<MallSystem> appAd = configService.getSystemList(mallSystem);// 查询整点红包app广告
            if (CollectionUtils.isNotEmpty(appAd)) {
                recordMaker.setAdName(appAd.get(0).getKeyValue());
                mallRedQrcodeRecord.setAdName(appAd.get(0).getKeyValue());
                // gameRedpacketRecord.setAdName(appAd.get(0).getKeyValue());
                mallPageView.setPvName(appAd.get(0).getKeyValue());
            }

            /** 当前时间段有没有领取过 **/
//            int list = recordService.count(new QueryWrapper<MallRedQrcodeRecord>().eq("to_user_id", userId).eq("type_of_claim", "01").eq("type", "02")
//                    .eq("redpacket_id", grain.get(i).getId()).between("create_time", LocalDateTime.of(LocalDateTime.now().toLocalDate(), LocalTime.MIN),
//                            LocalDateTime.of(LocalDateTime.now().toLocalDate(), LocalTime.MAX)));
            // List<GameRedpacketRecord> list =
            // gameRedPacketRecordService.selectGameRedpacketRecord(userId, grain.getId());
            Map<String, String> stringStringMap = configService.queryMaker();// 查询创客信息
//            if (list == 0) {

                // gameRedPacketRecordService.insertRedPacketRecode(gameRedpacketRecord);
                // todo广告生成记录
//                mallPageView.setMobileType(device);
//                mallPageView.setClientIp(ip);
                mallPageView.setOutUserId(userId);
                mallPageView.setType("02");

                mallPageView.setCreateTime(LocalDateTime.now());
                pageViewService.save(mallPageView);
                // 插入资金变动，充值流水表（账户表（金币兑换，领取红包）专用字段）
                if (residuer.compareTo(new BigDecimal(0.00)) == 1) {
                    MallUser user = userService.findById(userId);

                    // 是否达人 达人收益比例增高 金额已经计算好了
                    mallRedQrcodeRecord.setAmount(talResiduer);
                    if (null != user.getIsTalent() && user.getIsTalent().equals("1")) {
                        AccountUtils.accountChangeByExchange(userId, talResiduer, CommonConstant.TASK_REWARD_CASH, AccountStatus.BID_1, paramsMap);
                        mallRedQrcodeRecord.setRemark("用户身份（达人）领取整点红包+" + (Double.parseDouble(mallSystemRule.getReserveTwo()) * 100) + "%");
                        mallRedQrcodeRecord.setUserIdentity("1");

                    }
                    // 不是达人
                    else {
                        // （新：新的规则表）
                        AccountUtils.accountChangeByExchange(userId, talResiduer, CommonConstant.TASK_REWARD_CASH, AccountStatus.BID_1, paramsMap);
                        mallRedQrcodeRecord.setRemark("用户身份（普通）领取整点红包记录");
                    }
                    //创客
                    if (null != user && user.getIsTalent().equals("1")) {
                        mallRedQrcodeRecord.setIsMaker("1");
                    }
                    mallRedQrcodeRecord.setMsgId(msgId);
                    // 插入红包记录表（用户自己领）
                    mallRedQrcodeRecord.setMobile(user.getMobile());
                    mallRedQrcodeRecord.setNickname(user.getNickname());
                    recordService.save(mallRedQrcodeRecord);
                    // 直推人逻辑
                    MallUser user1 = new MallUser();
                    // 有直推人
                    if (null != user && !org.apache.commons.lang3.StringUtils.isEmpty(user.getDirectLeader())) {
                        user1 = userService.findById(user.getDirectLeader());
                    }
                    // 没有直推人，直接把分享人set直推人 一起绑定上级
                    else {
                        if (!StringUtils.isBlank(makerId)) {
                            user1 = userService.findById(makerId);
                            user.setDirectLeader(makerId);
                            userService.updateById(user);
                        }
                    }
                    // 直推人收益 1 是比例
                    if ("1".equals(stringStringMap.get("mall_maker_profit_type"))) {
                        BigDecimal makerRes = new BigDecimal("0.01");
                        // 通过配置表中的配置来决定
                        if ("1".equals(rule)) {
                            // 直推人收益 （旧：配置表中规则）
                            makerRes = new BigDecimal(stringStringMap.get("mall_maker_profit_proportion")).multiply(residuer);
                        } else {
                            if (null != user1) {
                                // 判断直推人是达人
                                if (null != user1.getIsTalent() && user1.getIsTalent().equals("1")) {
                                    // 直推人收益，（新：新的规则表）
                                    makerRes = mallSystemRule.getShareProportion().multiply(residuer);
                                    recordMaker.setUserIdentity("1");
                                }// 不是达人
                                else {
                                    // 直推人收益，（新：新的规则表）
                                    makerRes = new BigDecimal(mallSystemRule.getReserveOne()).multiply(residuer);
                                }
                                recordMaker.setNickname(user1.getNickname());
                                recordMaker.setMobile(user1.getMobile());
                                recordMaker.setToUserId(user1.getId());
                                recordMaker.setAmount(makerRes);
                                // 插入分享者整点红包记录
                                AccountUtils.accountChangeByExchange(user1.getId(), makerRes, CommonConstant.TASK_REWARD_CASH, AccountStatus.BID_1, paramsMapMaker);
                                //todo md
//                                String fansName = org.apache.commons.lang3.StringUtils.isNotEmpty(user.getNickname()) ? user.getNickname() : user.getMobile();
//                                String content = "您的粉丝" + fansName + "成功领取整点红包，您所得收益¥" + makerRes.setScale(6, BigDecimal.ROUND_DOWN).toString() + "已转至可提现账户，可点击“有料-零钱提现-钱包流水”进行查看。";
//                                PushUtils.pushMessage(user1.getId(), "佣金通知", content, 3, 0);
                                recordService.save(recordMaker);
                            }
                        }
                    }
                    // todo 2 是固定金额 暂时不用扩展用
                    else {
                        // 直推人收益
                        AccountUtils.accountChangeByExchange(user1.getId(), new BigDecimal(stringStringMap.get("mall_maker_profit_fixed")), CommonConstant.TASK_REWARD_CASH,
                                AccountStatus.BID_1, paramsMapMaker);
                        //todo md
//                        String fansName = org.apache.commons.lang3.StringUtils.isNotEmpty(user.getNickname()) ? user.getNickname() : user.getMobile();
//                        String content = "您的粉丝" + fansName + "成功领取整点红包，您所得收益¥" + new BigDecimal(stringStringMap.get("mall_maker_profit_fixed")).setScale(6, BigDecimal.ROUND_DOWN).toString() + "已转至可提现账户，可点击“有料-零钱提现-钱包流水”进行查看。";
//                        PushUtils.pushMessage(user1.getId(), "佣金通知", content, 3, 0);

                    }



                }
//            }
        } catch (Exception e) {
            logger.error("红包入库异常：" + e.getMessage(), e);
            throw e;
        }

        return "success";
    }

}


