package com.graphai.mall.wx.web.finance;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.json.JacksonUtil;
import com.graphai.commons.util.json.JacksonUtils;
import com.graphai.mall.db.domain.*;
import com.graphai.mall.db.service.account.AccountService;
import com.graphai.mall.db.service.common.MallPhoneValidationService;
import com.graphai.mall.db.service.order.MallOrderGoodsService;
import com.graphai.mall.db.service.order.MallOrderService;
import com.graphai.mall.db.service.user.MallUserService;

//import com.graphai.mall.db.util.OrderUtil;
//import com.graphai.mall.wx.annotation.LoginUser;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

//import static com.graphai.mall.wx.util.WxResponseCode.AUTH_NAME_REGISTERED;
//import static com.graphai.mall.wx.util.WxResponseCode.ORDER_UNKNOWN;
/**
 * 账户-积分商城
 * @author Qxian
 * @date 2020-10-15
 * @version V0.0.1
 */
@RestController
// @RequestMapping(value = "/api_gateway", consumes = {
@RequestMapping(value = "/wx/capital/integral", consumes = { MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.APPLICATION_ATOM_XML_VALUE,
		MediaType.APPLICATION_FORM_URLENCODED_VALUE, MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE, MediaType.TEXT_HTML_VALUE,
		MediaType.TEXT_XML_VALUE, MediaType.APPLICATION_OCTET_STREAM_VALUE })
// html直接请求
public class AccountIntegralController {

	private final Log log = LogFactory.getLog(AccountIntegralController.class);

	@Autowired
	private AccountService accountManager;
	
//	@Autowired
//	private MallOrderService orderService;
//	@Autowired
//	private MallOrderGoodsService orderGoodsService;
//	@Autowired
//	private MallPhoneValidationService phoneService;
	@Autowired
	private AccountService accountService;
	@Autowired
	private MallUserService userService;

	/**
	 * 开户接口
	 * 
	 * @param response
	 * @param request
	 * @param paramJson
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/account", method = RequestMethod.POST)
	public Object createNewCapitalAccount(HttpServletResponse response, HttpServletRequest request, @RequestBody String paramJson) throws Exception {
		Map<String, Object> paramsMap = JacksonUtils.jsn2map(paramJson, String.class, Object.class);
		String apsKey = MapUtils.getString(paramsMap, "apsKey");
		log.info("~~开户参数apsKey:" + apsKey);
		FcAccount customerAccount = accountManager.getAccountByKey(apsKey);

		if (customerAccount != null) {
			return ResponseUtil.ok("您已经开户", customerAccount);
		}

		// 开户
		customerAccount = accountManager.accountTradeCreateAccount(apsKey);

		return ResponseUtil.ok("开户成功", customerAccount);
	}

	/**
	 * 9.获取当前用户的账号信息
	 * @param body-mobile
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/account/info", method = RequestMethod.POST)
//	public Object getCustomerAccount(HttpServletResponse response, HttpServletRequest request,@RequestBody String body) throws Exception {
	public Object getCustomerAccount(@RequestBody String body) throws Exception {

		//判断用户信息存在
		String mobile = JacksonUtil.parseString(body, "mobile");
		if (mobile == null || "".equals(mobile)) {
			return ResponseUtil.fail(501,"手机号码不能为空!");
		}
		String userId="";
		List<MallUser> users = userService.queryByMobile(mobile);
		if (CollectionUtils.isNotEmpty(users)) {
			if (org.apache.commons.lang3.StringUtils.isNotEmpty(users.get(0).getId())) {
				userId = users.get(0).getId();
			}
		}else {
			return ResponseUtil.fail(501,"用户不存在!");
		}
//		log.info("~~accountType:" + fcAccount.getAccountType());

		List<FcAccount> fcAccountList = accountManager.getAccountBycustomerId(userId);
		FcAccount fcAccount = new FcAccount();
		if (0 == fcAccountList.size()){
			//特殊情况，未来卡要是没绑定没有注册账户，这里注册一下
			//fcAccount = accountService.accountTradeCreateAccountIntegral(userId);
			return ResponseUtil.fail(601,"用户的账号不存在，请联系客服处理");
		}else{
			fcAccount = fcAccountList.get(0);
		}

		return ResponseUtil.ok("账户查询成功", fcAccount);
	}

	/**
	 * 获取分销分润充值记录
	 * 
	 * @param response
	 * @param request
	 * @param userId
	 * @return
	 * @throws Exception
	 */
//	@RequestMapping(value = "/recharge/list")
//	public Object getCustomerAccount(HttpServletResponse response, HttpServletRequest request, @LoginUser Integer userId,
//			@RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit) throws Exception {
//		if (userId == null) {
//			return ResponseUtil.unlogin();
//		}
//		Integer start = (page - 1) * limit;
//		Map<String, Object> result = new HashMap<String, Object>();
//		List<FcAccountRechargeBill> list = accountManager.getAccountRechargeBillList(userId, start, limit);
//
//		List<Object> resultList = new ArrayList<Object>();
//
//		for (int i = 0; i < list.size(); i++) {
//			FcAccountRechargeBill bill = list.get(i);
//			Integer orderId = bill.getOrderId();
//
//			// 订单信息
//			MallOrder order = orderService.findById(orderId);
//			if (null == order) {
//				return ResponseUtil.fail(ORDER_UNKNOWN, "订单不存在");
//			}
//			if (!order.getUserId().equals(userId)) {
//				//return ResponseUtil.fail(ORDER_INVALID, "不是当前用户的订单");
//			}
//			Map<String, Object> orderVo = new HashMap<String, Object>();
//			orderVo.put("id", order.getId());
//			orderVo.put("orderSn", order.getOrderSn());
//			orderVo.put("addTime", order.getAddTime());
//			orderVo.put("consignee", order.getConsignee());
//			orderVo.put("mobile", order.getMobile());
//			orderVo.put("address", order.getAddress());
//			orderVo.put("goodsPrice", order.getGoodsPrice());
//			orderVo.put("couponPrice", order.getCouponPrice());
//			orderVo.put("freightPrice", order.getFreightPrice());
//			orderVo.put("actualPrice", order.getActualPrice());
//			orderVo.put("orderStatusText", OrderUtil.orderStatusText(order));
//			orderVo.put("handleOption", OrderUtil.build(order));
//			orderVo.put("expCode", order.getShipChannel());
//			orderVo.put("expNo", order.getShipSn());
//
//			List<MallOrderGoods> orderGoodsList = orderGoodsService.queryByOid(order.getId());
//
//			Map<String, Object> results = new HashMap<>();
//			results.put("orderInfo", orderVo);
//			results.put("orderGoods", orderGoodsList);
//			results.put("bill", bill);
//
//			resultList.add(results);
//		}
//
//
//		Long total = accountManager.getAccountRechargeBillCount(userId);
//		int totalPages = (int) Math.ceil((double) Integer.parseInt(total.toString()) / limit);
//
//		result.put("billList", resultList);
//		result.put("totalPage", totalPages);
//		return ResponseUtil.ok("分润数据查询成功", result);
//	}

	/**
	 * 获取绩效
	 * 
	 * @param response
	 * @param request
	 * @param paramJson
	 * @param userId
	 * @return
	 * @throws Exception
	 */
//	@RequestMapping(value = "/achievement", method = RequestMethod.POST)
//	public Object getCustomerAchievement(HttpServletResponse response, HttpServletRequest request, @RequestBody String paramJson, @LoginUser Integer userId)
//			throws Exception {
//		if (userId == null) {
//			return ResponseUtil.unlogin();
//		}
//		log.info("接口调用过来了....." + userId);
//		Map<String, Object> map = accountManager.getCustomerAchievement(userId.toString());
//
//		return ResponseUtil.ok("收益查询成功", map);
//	}
	
	/**
	 * 发起提现
	 * @param response
	 * @param request
	 * @param bill
	 * @param userId
	 * @return
	 * @throws Exception
	 */
//	@RequestMapping(value = "/apply/cashwithdrawal", method = RequestMethod.POST)
//	public Object applyCashWithdrawal(HttpServletResponse response, HttpServletRequest request,
//                                      @RequestBody FcAccountWithdrawBill bill, @LoginUser Integer userId)
//			throws Exception {
//		if (userId == null) {
//			return ResponseUtil.unlogin();
//		}
//		log.info("接口调用过来了....." + userId);
//		FcAccount account = accountManager.getAccountByKey(userId.toString());
//
//		//获取是当前的几号
//		int dayCal = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
//
//		if(dayCal!=10 && dayCal!=20 && dayCal!=30){
//			return ResponseUtil.fail(-1, "当前不是提现日期");
//		}
//
//		String smsType = "WITHDRAW";
//		try {
//			if(account!=null && bill!=null){
//				MallPhoneValidation val = phoneService.getPhoneValidationByCode(bill.getMobile(), bill.getValCode(),smsType);
//
//				if(val==null){
//					return ResponseUtil.fail(AUTH_NAME_REGISTERED, "验证码有误");
//				}
//
//				bill.setCustomerId(userId.toString());
//				bill.setAccountId(account.getAccountId());
//				//返回值 -1 小于 0 等于 1 大于
//				if(bill.getAmt().compareTo(account.getAmount()) == -1){
//					log.info("提现金额和账户有的金额 getAmt： " +bill.getAmt().toString() );
//					log.info("提现金额和账户有的金额getAmount ： " +account.getAmount().toString() );
//
//					accountManager.addWithdraw(bill,account);
//
//					MallPhoneValidation updateVal = new MallPhoneValidation();
//					updateVal.setId(val.getId());
//					updateVal.setValState("00");
//					phoneService.update(updateVal);
//
//					return ResponseUtil.ok();
//				}else{
//					return ResponseUtil.fail(-1,"账户金额少于提现金额");
//				}
//			}else{
//				return ResponseUtil.fail(-1,"账户不存在");
//			}
//		} catch (Exception e) {
//			log.error("发起提现异常:" + e.getMessage(),e);
//		}
//
//		return ResponseUtil.fail(-1,"账户不存在");
//
//	}
	
	 
}
