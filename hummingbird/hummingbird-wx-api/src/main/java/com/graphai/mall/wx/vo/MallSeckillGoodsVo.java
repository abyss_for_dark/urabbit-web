package com.graphai.mall.wx.vo;

import com.graphai.mall.admin.domain.MallSeckillGoods;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;


@Data
public class MallSeckillGoodsVo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    private String id;

    /**
     * 秒杀定义id
     */
    private String seckillId;

    /**
     * 库存数量
     */
    private Integer number;

    /**
     * 商品名称
     */
    private String goodsName;

    /**
     * 起拍件数
     */
    private Integer startsNumber;

    /**
     * 秒杀价格
     */
    private BigDecimal seckillPrice;

    /**
     * 商品ID
     */
    private String goodsId;

    /**
     * 审核状态0审核中1审核通过2审核失败
     */
    private Integer auditStatus;

    /**
     * 审核原因
     */
    private String reason;

    /**
     * 1上架0下架
     */
    private Integer isOnSale;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;
    /**
     * 商品宣传图片列表，采用JSON数组格式
     */
    private String gallery;
    /**
     * 商品简介
     */
    private String brief;

    public MallSeckillGoodsVo(MallSeckillGoods mallSeckillGoods) {
        this.id = mallSeckillGoods.getId();
        this.seckillId = mallSeckillGoods.getSeckillId();
        this.number = mallSeckillGoods.getNumber();
        this.goodsName = mallSeckillGoods.getGoodsName();
        this.startsNumber = mallSeckillGoods.getStartsNumber();
        this.seckillPrice = mallSeckillGoods.getSeckillPrice();
        this.goodsId = mallSeckillGoods.getGoodsId();
        this.auditStatus = mallSeckillGoods.getAuditStatus();
        this.reason = mallSeckillGoods.getReason();
        this.isOnSale = mallSeckillGoods.getIsOnSale();
        this.createTime = mallSeckillGoods.getCreateTime();
        this.updateTime = mallSeckillGoods.getUpdateTime();
    }

}
