package com.graphai.mall.wx.web.coupon;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.bcrypt.BCryptPasswordEncoder;
import com.graphai.commons.util.json.JacksonUtil;
import com.graphai.mall.admin.domain.MallUserMerchant;
import com.graphai.mall.admin.service.impl.MallUserMerchantServiceImpl;
import com.graphai.mall.db.constant.coupon.CouponConstant;
import com.graphai.mall.db.domain.MallCoupon;
import com.graphai.mall.db.domain.MallCouponUser;
import com.graphai.mall.db.domain.MallUser;
import com.graphai.mall.db.service.coupon.CouponVerifyService;
import com.graphai.mall.db.service.coupon.MallCouponService;
import com.graphai.mall.db.service.coupon.MallCouponUserService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.mall.wx.util.WxResponseCode;
import com.graphai.mall.wx.vo.CouponVo;
import com.graphai.validator.Order;
import com.graphai.validator.Sort;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 优惠券服务
 */
@RestController
@RequestMapping("/wx/coupon")
@Validated
public class WxCouponController {
    private final Log logger = LogFactory.getLog(WxCouponController.class);

    @Autowired
    private MallCouponService couponService;
    @Autowired
    private MallCouponUserService couponUserService;

    @Autowired
    private CouponVerifyService couponVerifyService;
    @Autowired
    private MallUserService mallUserService;

    @Autowired
    private MallUserMerchantServiceImpl userMerchantService;

    /**
     * 优惠券列表
     *
     * @param page
     * @param limit
     * @return
     */
    @GetMapping("list")
    public Object list(@LoginUser String userId, @RequestParam("method") String method,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit,
                       @RequestParam(required = false) String merchantId, String type) {
        MallUser user = mallUserService.queryByUserId(userId);
        String levelId = user.getLevelId();
        // 0 普通用户 9/10/11 商户 98工厂
        List<MallCoupon> couponList = null;
        //是否是领劵中心请求
        if ("center".equals(method)) {
            if ("0".equals(levelId) || "98".equals(levelId)) {
                couponList = couponService.queryByMerchantId(page, limit, merchantId, "2");
            } else {
                couponList = couponService.queryByMerchantId(page, limit, merchantId, "1");
            }
        } else {
            MallUserMerchant userMerchant = userMerchantService.getOne(new QueryWrapper<MallUserMerchant>().lambda()
                .eq(MallUserMerchant::getUserId, user.getId()));
            // 进入门店领劵请求
            if ("0".equals(levelId)) {
                if ("0".equals(type)){
                    couponList = couponService.queryByMerchantId(page, limit, null, "2");
                }else{
                    couponList = couponService.queryByMerchantId(page, limit, merchantId, "2");
                }
            } else if ("98".equals(levelId)) {
                //0 工厂门店 1商户门店
                if ("1".equals(type) || userMerchant.getMerchantId().equals(merchantId)){
                    couponList = couponService.queryByMerchantId(page, limit, merchantId, "2");
                }else{
                    couponList = couponService.queryByMerchantId(page, limit, null, "2");
                }
            } else {
                if ("0".equals(type) || userMerchant.getMerchantId().equals(merchantId)) {
                    couponList = couponService.queryByMerchantId(page, limit, merchantId, "1");
                }else {
                        couponList = couponService.queryByMerchantId(page, limit, null, "1");
                }
            }
        }
        for (MallCoupon coupon : couponList) {
            List<MallCouponUser> mallCouponUsers = couponUserService.queryList(userId, coupon.getId(), null, page, 1, "id", "desc");
            if (mallCouponUsers.size() == 1) {
                coupon.isGet = true;
                coupon.isUse = mallCouponUsers.get(0).getStatus();

            }
        }

        Map<String, Object> data = new HashMap<>();
        long total = PageInfo.of(couponList).getTotal();
        data.put("data", couponList);
        data.put("total", total);
        return ResponseUtil.ok(data);
    }

    /**
     * 个人优惠券列表
     *
     * @param userId
     * @param status
     * @param page
     * @param limit
     * @param sort
     * @param order
     * @return
     */
    @GetMapping("mylist")
    public Object mylist(@LoginUser String userId,
                         @NotNull Short status,
                         @RequestParam(defaultValue = "1") Integer page,
                         @RequestParam(defaultValue = "10") Integer limit,
                         @Sort @RequestParam(defaultValue = "id") String sort,
                         @Order @RequestParam(defaultValue = "desc") String order) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        List<MallCouponUser> couponUserList = couponUserService.queryList(userId, null, status, page, limit, sort, order);
        long total = PageInfo.of(couponUserList).getTotal();
        List<CouponVo> couponVoList = change(couponUserList);

        Map<String, Object> data = new HashMap<String, Object>();
        data.put("data", couponVoList);
        data.put("count", total);
        return ResponseUtil.ok(data);
    }

    private List<CouponVo> change(List<MallCouponUser> couponList) {
        List<CouponVo> couponVoList = new ArrayList<>(couponList.size());
        for (MallCouponUser couponUser : couponList) {
            String couponId = couponUser.getCouponId();
            MallCoupon coupon = couponService.findById(couponId);
            CouponVo couponVo = new CouponVo();
            couponVo.setCouponId(coupon.getId());
            couponVo.setCouponUserId(couponUser.getId());
            couponVo.setCouponType(coupon.getCouponType());
            couponVo.setName(coupon.getName());
            couponVo.setDesc(coupon.getDesc());
            couponVo.setTag(coupon.getTag());
            couponVo.setMin(coupon.getMin().toPlainString());
            couponVo.setDiscount(coupon.getDiscount());
            couponVo.setStartTime(couponUser.getStartTime());
            couponVo.setEndTime(couponUser.getEndTime());
            couponVo.setMerchantId(coupon.getMerchantId());
            couponVoList.add(couponVo);
        }

        return couponVoList;
    }


    /**
     * 当前购物车下单商品订单可用优惠券
     *
     * @param userId
     * @param
     * @return
     */
    @GetMapping("selectlist")
    public Object selectlist(@LoginUser String userId, @RequestParam("checkedGoodsPrice") BigDecimal checkedGoodsPrice,
                             @RequestParam("type") String type, @RequestParam("merchantId") String merchantId) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        // 计算优惠券可用情况
        String levelId = mallUserService.queryByUserId(userId).getLevelId();
        List<MallCouponUser> couponUserList = null;
        // 0 普通用户 9、10、11、 98 商户和工厂
        if ("0".equals(levelId)) {
            couponUserList = couponUserService.getUnusedCoupons(userId, merchantId, "2");
        } else if ("9".equals(levelId) || "10".equals(levelId) || "11".equals(levelId) || "98".equals(levelId)) {
            // 0 工厂门店 2商户门店
            if ("0".equals(type)) {
                couponUserList = couponUserService.getUnusedCoupons(userId, merchantId, "1");
            } else {
                couponUserList = couponUserService.getUnusedCoupons(userId, merchantId, "2");
            }
        }
        List<MallCouponUser> availableCouponUserList = new ArrayList<>(couponUserList.size());
        for (MallCouponUser couponUser : couponUserList) {
            MallCoupon coupon = couponVerifyService.checkCoupon(userId, couponUser.getCouponId(), checkedGoodsPrice);
            if (coupon == null) {
                continue;
            }
            availableCouponUserList.add(couponUser);
        }

        List<CouponVo> couponVoList = change(availableCouponUserList);

        return ResponseUtil.ok(couponVoList);
    }

    /**
     * 优惠券领取
     *
     * @param userId 用户ID
     * @param body   请求内容， { couponId: xxx }
     * @return 操作结果
     */
    @PostMapping("receive")
    public Object receive(@LoginUser String userId, @RequestBody String body) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }

        String couponId = JacksonUtil.parseString(body, "couponId");
        String merchantId = JacksonUtil.parseString(body, "merchantId");

        if (couponId == null) {
            return ResponseUtil.badArgument();
        }

        MallCoupon coupon = couponService.findById(couponId);
        if (coupon == null) {
            return ResponseUtil.badArgumentValue();
        }

        // 当前已领取数量和总数量比较
        Integer total = coupon.getTotal();
        if (total == 0) {
            return ResponseUtil.fail(WxResponseCode.COUPON_EXCEED_LIMIT, "优惠券已领完");
        }

        // 当前用户已领取数量和用户限领数量比较
        Integer limit = coupon.getLimit().intValue();
        Integer userCounpons = couponUserService.countUserAndCoupon(userId, couponId);
        if ((limit != 0) && (userCounpons >= limit)) {
            return ResponseUtil.fail(WxResponseCode.COUPON_EXCEED_LIMIT, "优惠券已经领取过");
        }

        // 优惠券分发类型
        // 例如注册赠券类型的优惠券不能领取
        Short type = coupon.getType();
        if (type.equals(CouponConstant.TYPE_REGISTER)) {
            return ResponseUtil.fail(WxResponseCode.COUPON_RECEIVE_FAIL, "新用户优惠券自动发送");
        } else if (type.equals(CouponConstant.TYPE_CODE)) {
            return ResponseUtil.fail(WxResponseCode.COUPON_RECEIVE_FAIL, "优惠券只能兑换");
        } else if (type.equals(CouponConstant.TYPE_COMMON)) {
            return ResponseUtil.fail(WxResponseCode.COUPON_RECEIVE_FAIL, "优惠券类型不支持");
        }

        // 优惠券状态，已下架或者过期不能领取
        Short status = coupon.getStatus();
        if (status.equals(CouponConstant.STATUS_OUT)) {
            return ResponseUtil.fail(WxResponseCode.COUPON_EXCEED_LIMIT, "优惠券已领完");
        } else if (status.equals(CouponConstant.STATUS_EXPIRED)) {
            return ResponseUtil.fail(WxResponseCode.COUPON_RECEIVE_FAIL, "优惠券已经过期");
        }

        // 用户领券记录
        MallCouponUser couponUser = new MallCouponUser();
        if (StrUtil.isNotBlank(merchantId)){
            couponUser.setMerchantId(merchantId);
        }
        couponUser.setCouponId(couponId);
        couponUser.setUserId(userId);
/*        couponUser.setDiscount(coupon.getDiscount());
        couponUser.setMin(coupon.getMin());
        couponUser.setType(coupon.getType());
        couponUser.setCouponType(coupon.getCouponType());
        couponUser.setCouponName(coupon.getName());
        couponUser.setTag(coupon.getTag());*/
        Short timeType = coupon.getTimeType();
        if (timeType.equals(CouponConstant.TIME_TYPE_TIME)) {
            couponUser.setStartTime(coupon.getStartTime());
            couponUser.setEndTime(coupon.getEndTime());
        } else {
            LocalDateTime now = LocalDateTime.now().withNano(0);
            couponUser.setStartTime(now);
            couponUser.setEndTime(now.plusDays(coupon.getDays()).withHour(23).withMinute(59).withSecond(59));
        }
        //领取量加一
        coupon.setReceive(coupon.getReceive() + 1);
        if (coupon.getTotal() > 0) {
            coupon.setTotal(coupon.getTotal() - 1);
        }
        couponService.updateById(coupon);
        return couponUserService.add(couponUser) > 0 ? ResponseUtil.ok() : ResponseUtil.fail();
    }

    /**
     * 优惠券兑换
     *
     * @param userId 用户ID
     * @param body   请求内容， { code: xxx }
     * @return 操作结果
     */
    @PostMapping("exchange")
    public Object exchange(@LoginUser String userId, @RequestBody String body) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }

        String code = JacksonUtil.parseString(body, "code");
        if (code == null) {
            return ResponseUtil.badArgument();
        }

        MallCoupon coupon = couponService.findByCode(code);
        if (coupon == null) {
            return ResponseUtil.fail(WxResponseCode.COUPON_CODE_INVALID, "优惠券不正确");
        }
        String couponId = coupon.getId();

        // 当前已领取数量和总数量比较
        Integer total = coupon.getTotal();
        Integer totalCoupons = couponUserService.countCoupon(couponId);
        if ((total != 0) && (totalCoupons >= total)) {
            return ResponseUtil.fail(WxResponseCode.COUPON_EXCEED_LIMIT, "优惠券已兑换");
        }

        // 当前用户已领取数量和用户限领数量比较
        Integer limit = coupon.getLimit().intValue();
        Integer userCounpons = couponUserService.countUserAndCoupon(userId, couponId);
        if ((limit != 0) && (userCounpons >= limit)) {
            return ResponseUtil.fail(WxResponseCode.COUPON_EXCEED_LIMIT, "优惠券已兑换");
        }

        // 优惠券分发类型
        // 例如注册赠券类型的优惠券不能领取
        Short type = coupon.getType();
        if (type.equals(CouponConstant.TYPE_REGISTER)) {
            return ResponseUtil.fail(WxResponseCode.COUPON_RECEIVE_FAIL, "新用户优惠券自动发送");
        } else if (type.equals(CouponConstant.TYPE_COMMON)) {
            return ResponseUtil.fail(WxResponseCode.COUPON_RECEIVE_FAIL, "优惠券只能领取，不能兑换");
        } else if (!type.equals(CouponConstant.TYPE_CODE)) {
            return ResponseUtil.fail(WxResponseCode.COUPON_RECEIVE_FAIL, "优惠券类型不支持");
        }

        // 优惠券状态，已下架或者过期不能领取
        Short status = coupon.getStatus();
        if (status.equals(CouponConstant.STATUS_OUT)) {
            return ResponseUtil.fail(WxResponseCode.COUPON_EXCEED_LIMIT, "优惠券已兑换");
        } else if (status.equals(CouponConstant.STATUS_EXPIRED)) {
            return ResponseUtil.fail(WxResponseCode.COUPON_RECEIVE_FAIL, "优惠券已经过期");
        }

        // 用户领券记录
        MallCouponUser couponUser = new MallCouponUser();
        couponUser.setCouponId(couponId);
        couponUser.setUserId(userId);
        Short timeType = coupon.getTimeType();
        if (timeType.equals(CouponConstant.TIME_TYPE_TIME)) {
            couponUser.setStartTime(coupon.getStartTime());
            couponUser.setEndTime(coupon.getEndTime());
        } else {
            LocalDateTime now = LocalDateTime.now().withNano(0);
            couponUser.setStartTime(now);
            LocalDateTime endTime = now.plusDays(coupon.getDays());
            endTime = endTime.withHour(23).withMinute(59).withSecond(59);
            couponUser.setEndTime(endTime);
        }
        couponUserService.add(couponUser);
        return ResponseUtil.ok();
    }
}