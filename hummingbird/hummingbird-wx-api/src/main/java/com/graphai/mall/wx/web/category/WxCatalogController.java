package com.graphai.mall.wx.web.category;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;

import com.graphai.mall.wx.vo.CategoryVo;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Header;
import org.apache.http.message.BasicHeader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.graphai.commons.constant.MallConstant;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.json.JacksonUtil;
import com.graphai.commons.util.json.JacksonUtils;
import com.graphai.mall.db.constant.PlateFromConstant;
import com.graphai.mall.db.domain.MallCategory;
import com.graphai.mall.db.domain.MallUser;
import com.graphai.mall.db.service.category.MallCategoryService;
import com.graphai.mall.db.service.common.AppVersionService;
import com.graphai.mall.db.service.sysconfig.MallSystemConfigService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.util.RetrialUtils;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.mall.wx.service.HomeCacheManager;
import com.graphai.mall.wx.util.Md5Utils;
import com.graphai.open.api.getway.HttpRequestUtils;
import com.graphai.properties.PlatFormServerproperty;
import com.graphai.properties.TaoBaoProperties;
import com.taobao.api.DefaultTaobaoClient;
import com.taobao.api.TaobaoClient;
import com.taobao.api.request.TbkActivityInfoGetRequest;
import com.taobao.api.response.TbkActivityInfoGetResponse;

import cn.hutool.core.util.ObjectUtil;

/**
 * 类目服务
 */
@RestController
@RequestMapping("/wx/catalog")
@Validated
public class WxCatalogController {
    private final Log logger = LogFactory.getLog(WxCatalogController.class);

    @Autowired
    private MallCategoryService categoryService;
    @Autowired
    private PlatFormServerproperty platFormServerproperty;

    @Autowired
    private TaoBaoProperties taoBaoProperties;

    @Autowired
    private MallUserService userService;

    @Autowired
    private MallSystemConfigService mallSystemConfigService;

    @Autowired
    private AppVersionService appVersionService;

    /**
     * 分类详情
     *
     * @param id 分类类目ID。 如果分类类目ID是空，则选择第一个分类类目。 需要注意，这里分类类目是一级类目
     * @return 分类详情
     * @throws IOException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    @GetMapping("index")
    public Object index(String id) throws Exception {

        // 所有一级分类目录
        // List<MallCategory> l1CatList = categoryService.queryL1("categoryHome");
        //
        // // 当前一级分类目录
        // MallCategory currentCategory = null;
        // if (id != null) {
        // currentCategory = categoryService.findById(id);
        // } else {
        // currentCategory = l1CatList.get(0);
        // }
        //
        // // 当前一级分类目录对应的二级分类目录
        // List<MallCategory> currentSubCategory = null;
        // if (null != currentCategory) {
        // currentSubCategory = categoryService.queryByPid(currentCategory.getId());
        // }

        String url = platFormServerproperty.getUrl(MallConstant.TAOBAO);
        Map<String, Object> mappMap = new HashMap<String, Object>();
        mappMap.put("plateform", "dtk");
        mappMap.put("id", id);

        Header[] headers = new BasicHeader[] {new BasicHeader("method", "getCurrentCategory"),
                new BasicHeader("version", "v1.0")};

        String result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);

        JSONObject object = JSONObject.parseObject(result);

        if (object.getString("errno").equals("0")) {

        }


        Map<String, Object> data = new HashMap<String, Object>();
        // data.put("categoryList", l1CatList);
        // data.put("currentCategory", currentCategory);
        // data.put("currentSubCategory", currentSubCategory);
        return ResponseUtil.ok(data);
    }

    /**
     * 所有分类数据
     *
     * @return 所有分类数据
     */
    @GetMapping("all")
    public Object queryAll() {
        // 优先从缓存中读取
        if (HomeCacheManager.hasData(HomeCacheManager.CATALOG)) {
            return ResponseUtil.ok(HomeCacheManager.getCacheData(HomeCacheManager.CATALOG));
        }


        // 所有一级分类目录
        List<MallCategory> l1CatList = categoryService.queryL1("categoryHome");

        // 所有子分类列表
        Map<String, List<MallCategory>> allList = new HashMap<>();
        List<MallCategory> sub;
        for (MallCategory category : l1CatList) {
            sub = categoryService.queryByPid(category.getId());
            allList.put(category.getId(), sub);
        }

        // 当前一级分类目录
        MallCategory currentCategory = l1CatList.get(0);

        // 当前一级分类目录对应的二级分类目录
        List<MallCategory> currentSubCategory = null;
        if (null != currentCategory) {
            currentSubCategory = categoryService.queryByPid(currentCategory.getId());
        }

        Map<String, Object> data = new HashMap<String, Object>();
        data.put("categoryList", l1CatList);
        data.put("allList", allList);
        data.put("currentCategory", currentCategory);
        data.put("currentSubCategory", currentSubCategory);

        // 缓存数据
        HomeCacheManager.loadData(HomeCacheManager.CATALOG, data);
        return ResponseUtil.ok(data);
    }

    /**
     * 当前分类栏目
     *
     * @param id 分类类目ID
     * @return 当前分类栏目
     */
    @GetMapping("current")
    public Object current(@NotNull String id) {
        // 当前分类
        MallCategory currentCategory = categoryService.findById(id);
        List<MallCategory> currentSubCategory = categoryService.queryByPid(currentCategory.getId());

        Map<String, Object> data = new HashMap<String, Object>();
        data.put("currentCategory", currentCategory);
        data.put("currentSubCategory", currentSubCategory);
        return ResponseUtil.ok(data);
    }


    /**
     * 大淘客分类栏目
     *
     * @param id 分类类目ID
     * @return 当前分类栏目
     */
    @GetMapping("dtkCurrent")
    public Object dtkCurrent(@RequestParam @NotNull String id) {

        if (StringUtils.isEmpty(id)) {
            return ResponseUtil.fail(401, "参数有误!");
        }
        // 调用开放平台数据
        String result = HttpRequestUtils.post(
                        "http://localhost:8082/api/getaway?method=getCurrentCategory&version=v1.0.1&plateform=dtk&id="
                                        + id,
                        "", null);

        JSONObject object = JSONObject.parseObject(result);
        if (!object.getString("errno").equals("0")) {
            return ResponseUtil.fail();
        }
        return ResponseUtil.ok(object);
    }

    /**
     * 首页分类栏目
     *
     * @param id 分类类目ID
     * @return 当前分类栏目
     */
    @GetMapping("indexCurrent")
    public Object indexCurrent(String id, String position_code, String offset, String pageSize, String categoryId)
                    throws Exception {
        if (StringUtils.isBlank(categoryId)) {
            ResponseUtil.fail(401, "categoryId不能为空");
        }
        // String url =
        // "http://localhost:8083/api/getaway?plateform=dtk&method=getHomeCategory&position_code=" +
        // position_code + "&offset=" + offset + "&pageSize=" + pageSize + "&categoryId="+categoryId;

        String url = platFormServerproperty.getUrl(MallConstant.TAOBAO);
        Map<String, Object> mappMap = new HashMap<String, Object>();
        mappMap.put("plateform", "dtk");
        mappMap.put("position_code", position_code);
        mappMap.put("offset", offset);
        mappMap.put("pageSize", pageSize);
        mappMap.put("categoryId", categoryId);

        Header[] headers = new BasicHeader[] {new BasicHeader("method", "getHomeCategory"),
                new BasicHeader("version", "v1.0")};


        if (StringUtils.isNotBlank(id)) {
            mappMap.put("id", id);

        }
        String result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);
        if (StringUtils.isBlank(result)) {
            return ResponseUtil.fail(401, "获取数据为空");
        }
        JSON json = (JSON) JSON.parse(result);
        return ResponseUtil.ok(json);
    }

    /**
     * 礼品卡列表
     *
     * @return 礼品卡列表
     */
    @GetMapping("/getAllCardList")
    public Object getAllCardList() throws Exception {

        String url = platFormServerproperty.getUrl(MallConstant.SUNING);
        Map<String, Object> mappMap = new HashMap<String, Object>();
        mappMap.put("plateform", "lsxd");

        Header[] headers = new BasicHeader[] {new BasicHeader("method", "getAllCardList"),
                new BasicHeader("version", "v1.0")};

        return HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);
    }

    /**
     * 礼品卡详情
     *
     * @return 礼品卡详情
     */
    @GetMapping("/getAllCardDetailInfo")
    public Object getAllCardDetailInfo(@RequestParam String goodsId, @RequestParam String specifications)
                    throws Exception {
        if (StringUtils.isBlank(goodsId)) {
            return ResponseUtil.fail(401, "goodsId 不能为空");
        }
        if (StringUtils.isBlank(specifications)) {
            return ResponseUtil.fail(401, "specifications 不能为空");
        }
        String url = platFormServerproperty.getUrl(MallConstant.TAOBAO);
        Map<String, Object> mappMap = new HashMap<String, Object>();
        mappMap.put("plateform", "lsxd");
        mappMap.put("goodsId", goodsId);
        mappMap.put("specifications", specifications);

        Header[] headers = new BasicHeader[] {new BasicHeader("method", "getAllCardDetailInfo"),
                new BasicHeader("version", "v1.0")};

        return HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);
    }


    /**
     * 大淘客品牌团类目
     * 
     * @throws IOException
     * @throws JsonMappingException
     * @throws JsonParseException
     *
     */
    @GetMapping("/getDtkCategorysByBrand")
    public Object getDtkCategorysByBrand() throws Exception {
        String url = platFormServerproperty.getUrl(MallConstant.TAOBAO);
        Map<String, Object> mappMap = new HashMap<String, Object>();
        mappMap.put("plateform", "dtk");
        mappMap.put("categoryHome", "dtkCategory");

        Header[] headers = new BasicHeader[] {new BasicHeader("method", "getDtkCategory"),
                new BasicHeader("version", "v1.0")};

        String result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);

        // 首页目录配置 KM商城分类
        JSONObject object = JSONObject.parseObject(result);

        if ("0".equals(object.getString("errno"))) {

            List<MallCategory> kmMallCategoryList = JSON.parseObject(object.get("data").toString(), List.class);


            return ResponseUtil.ok(kmMallCategoryList);
        }

        return ResponseUtil.ok();
    }


    /**
     * 大淘客9.9精选类目
     * 
     * @throws IOException
     * @throws JsonMappingException
     * @throws JsonParseException
     *
     */
    @GetMapping("/getNineCategory")
    public Object getNineCategory() throws Exception {
        String url = platFormServerproperty.getUrl(MallConstant.TAOBAO);
        Map<String, Object> mappMap = new HashMap<String, Object>();
        mappMap.put("plateform", "dtk");
        mappMap.put("categoryHome", "dtkNineCategory");

        Header[] headers = new BasicHeader[] {new BasicHeader("method", "getDtkCategory"),
                new BasicHeader("version", "v1.0")};

        String result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);

        JSONObject object = JSONObject.parseObject(result);

        if ("0".equals(object.getString("errno"))) {

            List<MallCategory> kmMallCategoryList = JSON.parseObject(object.get("data").toString(), List.class);

            return ResponseUtil.ok(kmMallCategoryList);
        }

        return ResponseUtil.ok();
    }


    /**
     * 大淘客疯抢排行类目
     * 
     * @throws IOException
     * @throws JsonMappingException
     * @throws JsonParseException
     *
     */
    @GetMapping("/getDtkRankCategory")
    public Object getDtkRankCategory() throws Exception {
        String url = platFormServerproperty.getUrl(MallConstant.TAOBAO);
        Map<String, Object> mappMap = new HashMap<String, Object>();
        mappMap.put("plateform", "dtk");
        mappMap.put("categoryHome", "dtkCategory");

        Header[] headers = new BasicHeader[] {new BasicHeader("method", "getDtkCategory"),
                new BasicHeader("version", "v1.0")};
        String result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);
        JSONObject object = JSONObject.parseObject(result);

        List<LinkedHashMap<String, Object>> cateHashMaps = new ArrayList<LinkedHashMap<String, Object>>();
        if ("0".equals(object.getString("errno"))) {
            List<LinkedHashMap<String, Object>> CategoryList =
                            JSON.parseObject(object.get("data").toString(), List.class);
            CategoryList.remove(0);
            for (int i = 0; i < CategoryList.size(); i++) {

                LinkedHashMap<String, Object> objMap = new LinkedHashMap<String, Object>(CategoryList.get(i));
                objMap.put("type", "cateType");
                cateHashMaps.add(objMap);


            }
            LinkedHashMap<String, Object> map = new LinkedHashMap<String, Object>();
            map.put("name", "实时榜");
            map.put("infCateId", "1");
            map.put("type", "rankType");
            cateHashMaps.add(0, map);
            LinkedHashMap<String, Object> map1 = new LinkedHashMap<String, Object>();
            map1.put("name", "全天榜");
            map1.put("infCateId", "2");
            map1.put("type", "rankType");
            cateHashMaps.add(1, map1);
            return ResponseUtil.ok(cateHashMaps);
        }

        return ResponseUtil.ok();
    }


    /**
     * 获取美食特惠行业的类目
     * 
     * @throws IOException
     *
     */
    @GetMapping("/getcouponGoodsCate")
    public Object getcouponGoodsCate() throws Exception {
        String url = platFormServerproperty.getUrl(MallConstant.TAOBAO);
        Map<String, Object> mappMap = new HashMap<String, Object>();
        mappMap.put("plateform", "dtk");
        mappMap.put("cateRelaType", "couponGoodsCategory");

        Header[] headers = new BasicHeader[] {new BasicHeader("method", "getCategorysByIndustry"),
                new BasicHeader("version", "v1.0")};
        String result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);
        JSONObject object = JSONObject.parseObject(result);

        List<LinkedHashMap<String, Object>> cateHashMaps = new ArrayList<LinkedHashMap<String, Object>>();
        if ("0".equals(object.getString("errno"))) {
            List<MallCategory> CategoryList = JSON.parseObject(object.get("data").toString(), List.class);
            return ResponseUtil.ok(CategoryList);
        }

        return ResponseUtil.ok();
    }



    /**
     * 类目链接(淘宝活动转链)
     */
    @GetMapping("/getActivityLink")
    public Object getActivityLink(@LoginUser String userId, HttpServletRequest mrequest, @NotNull String activityId) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }

        if (com.graphai.mall.db.util.StringHelper.isNullOrEmpty(activityId)) {
            return ResponseUtil.badArgument();
        }

        // 淘宝转链
        String token = mrequest.getHeader("X-Mall-Token");
        if (org.apache.commons.lang3.StringUtils.isBlank(token)) {
            return ResponseUtil.unlogin();
        }

        MallUser user = userService.findById(userId);
        if (com.graphai.commons.util.obj.ObjectUtils.isNotNull(user)) {
            if (org.apache.commons.lang3.StringUtils.isBlank(user.getTbkRelationId())
                            || org.apache.commons.lang3.StringUtils.isBlank(user.getTbkPubId())) {
                return ResponseUtil.unTaobaoAuth();
            }
        }

        try {

            TaobaoClient client = new DefaultTaobaoClient(PlateFromConstant.DTK_ZL_URL,
                            taoBaoProperties.getLianmengAppKey(), taoBaoProperties.getLianmengAppSecret());
            TbkActivityInfoGetRequest req = new TbkActivityInfoGetRequest();
            req.setAdzoneId(110282750499L);
            req.setActivityMaterialId(activityId);
            req.setRelationId(Long.parseLong(user.getTbkRelationId()));
            TbkActivityInfoGetResponse rsp = client.execute(req);
            System.out.println(rsp.getBody());

            logger.info("获取活动转链结果------------" + JacksonUtils.bean2Jsn(rsp));
            if (rsp.getData() != null) {
                logger.info("推广链接" + rsp.getData());
                String encoderUrl = URLEncoder.encode(rsp.getData().getClickUrl(), "UTF-8");
                return ResponseUtil.ok("/pages/web/webview?link=" + encoderUrl);
            }

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            e.printStackTrace();
        }
        return ResponseUtil.fail(502, "请再点击一次!");
    }



    /**
     * 大牌返现
     * 
     * @throws Exception
     */
    @GetMapping("/getBrandCategory")
    public Object getBrandCategory(@LoginUser String userId, HttpServletRequest mrequest, @NotNull String position)
                    throws Exception {

        try {
            Object obj = categoryService.queryBrandCategory(position);

            if (ObjectUtil.isNotNull(obj)) {
                return ResponseUtil.ok(obj);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseUtil.ok();
    }

    
    
    
    /**
     * 获取分类数据
     * 
     * @throws Exception
     */
    @GetMapping("/queryByPositionCode")
    public Object queryByPositionCode(HttpServletRequest mrequest, @NotNull String positionCode) throws Exception {

        try {
            // 所有一级分类目录
            List<MallCategory> CatList = categoryService.queryByPositionCode(null, null, positionCode,null);
            List<Map<String, Object>> data = new ArrayList<>(CatList.size());
            for (MallCategory category : CatList) {
                Map<String, Object> d = new HashMap<>(2);
                d.put("categoryId", category.getId());
                d.put("name", category.getName());
                data.add(d);
            }
            return ResponseUtil.ok(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseUtil.ok();

    }

    /**
     * 获取不同位置的类目列表
     * 
     * @throws Exception
     */
    @GetMapping("/getCategoryByPositionCode/V2")
    public Object getCategoryByPositionCodeV2(HttpServletRequest mrequest, @NotNull String icode,
                    @RequestParam(value = "version", required = false) String version) throws Exception {

        try {
            // MallIndustry mallIndustry = categoryService.selectIndustryIcode(icode);
            Map<String, Object> mappMap = new HashMap<String, Object>();
            mappMap.put("plateform", "pub");
            mappMap.put("icode", icode);
            String url = platFormServerproperty.getUrl(MallConstant.TAOBAO);
            Header[] headers = new BasicHeader[] {new BasicHeader("method", "pub.singlePage.category"),
                    new BasicHeader("version", "v1.0")};
            String result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);

            JSONObject object = JSONObject.parseObject(result);

            String mobileTye = mrequest.getHeader("User-Agent");

            Map data = new HashMap();
            if (object.getString("errno").equals("0")) {
                List<Map<String, Object>> cateList = JSON.parseObject(object.get("data").toString(), List.class);

                // data.put("categoryIndustryList",
                // JSON.parseObject(object.get("data").toString(), List.class));

                if (Objects.equals(icode, "privilege")) {
                    if (RetrialUtils.examine(mrequest, version)) {


                        for (int i = 0; i < cateList.size(); i++) {
                            if (Objects.equals("在线游戏", cateList.get(i).get("cname"))) {
                                cateList.get(i).put("cname", "在线影音");
                                cateList.get(i).put("linkUrl", "/pages/kmi/kmi");
                            }
                        }
                    }

                }
                data.put("categoryIndustryList", cateList);
            }
            return ResponseUtil.ok(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseUtil.ok();

    }


    /**
     * 获取优惠券或权益类目
     * 
     * @throws IOException
     *
     */
    @PostMapping("/getCateHSCard")
    public Object getcouponGoodsCateHSCard(@RequestBody String data, HttpServletRequest request) throws Exception {

        String sign = JacksonUtil.parseString(data, "sign");

        boolean flag = Md5Utils.verifySignature(null, sign);
        if (!flag) {
            return ResponseUtil.fail(403, "无效签名");
        }
        String url = platFormServerproperty.getUrl(MallConstant.TAOBAO);
        Map<String, Object> mappMap = new HashMap<String, Object>();
        mappMap.put("plateform", "dtk");

        Header[] headers = new BasicHeader[] {new BasicHeader("method", "getCouponCategory"),
                new BasicHeader("version", "v1.0")};
        String result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);
        JSONObject object = JSONObject.parseObject(result);

        if ("0".equals(object.getString("errno"))) {
            List<MallCategory> CategoryList = JSON.parseObject(object.get("data").toString(), List.class);
            return ResponseUtil.ok(CategoryList);
        }

        return ResponseUtil.ok();
    }



    /**
     * 商城首页频道
     * 
     * @throws IOException
     *
     */
    @GetMapping("/getIndustryCategory")
    public Object getIndustryCategory(String industryId, @NotNull String positionCode) throws Exception {
        return ResponseUtil.ok(categoryService.getCategoryByIndustry(industryId, positionCode));
    }

    /**
     * 获取分类栏目信息
     * @return
     */
    @GetMapping("/getCategoryIndex")
    public Object getCategoryIndex(){
        // 所有一级自定义分类目录
        List<MallCategory> l1Category = categoryService.queryL1("zyCategory");
        if(l1Category == null){
            return  ResponseUtil.fail("未配置类目");
        }

        // 获取子项
        List<CategoryVo> categoryVos = getChilds(l1Category.get(0).getId());

        Map<String,Object> map = new HashMap<>();
        map.put("l1Category",l1Category);
        map.put("catelogVos", categoryVos);

        return ResponseUtil.ok(map);
    }

    /**
     * 根据1级类型获取子项
     * @param pid
     * @return
     */
    @GetMapping("/getCategoryByL1")
    public Object getCategoryByL1(@RequestParam String pid){
        return ResponseUtil.ok(getChilds(pid));
    }

    /**
     * 获取二三级项
     * @param pid
     * @return
     */
    private List<CategoryVo> getChilds(String pid){
        List<CategoryVo> categoryVos = new ArrayList<>();
        List<MallCategory> l2Category = categoryService.queryByPid(pid);
        // 二级
        for (MallCategory temp: l2Category) {
            CategoryVo categoryVo = new CategoryVo();
            categoryVo.setMallCategory(temp);
            categoryVo.setChildCategory(new ArrayList<>());
            categoryVos.add(categoryVo);
        }

        // 3级
        for (CategoryVo temp: categoryVos) {
            List<MallCategory> itemList = categoryService.queryByPid(temp.getMallCategory().getId());
            temp.getChildCategory().addAll(itemList);
        }
        return categoryVos;
    }
}
