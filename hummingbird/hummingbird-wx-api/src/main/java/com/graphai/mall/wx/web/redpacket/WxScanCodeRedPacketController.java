package com.graphai.mall.wx.web.redpacket;

import static com.graphai.commons.util.servlet.ServletUtils.getRequest;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.security.KeyFactory;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

import javax.crypto.Cipher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.csp.sentinel.SphO;
import com.alibaba.csp.sentinel.slots.block.RuleConstant;
import com.alibaba.csp.sentinel.slots.block.flow.FlowRule;
import com.alibaba.csp.sentinel.slots.block.flow.FlowRuleManager;
import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.Cached;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.CertAlipayRequest;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayFundTransUniTransferRequest;
import com.alipay.api.request.AlipaySystemOauthTokenRequest;
import com.alipay.api.response.AlipayFundTransUniTransferResponse;
import com.alipay.api.response.AlipaySystemOauthTokenResponse;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.graphai.commons.util.RedUtils;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.ip.IpUtils;
import com.graphai.id.GraphaiIdGenerator;
import com.graphai.mall.admin.domain.MallPageView;
import com.graphai.mall.admin.domain.MallRedQrcode;
import com.graphai.mall.admin.domain.MallRedQrcodeRecord;
import com.graphai.mall.admin.service.IMallPageViewService;
import com.graphai.mall.admin.service.IMallRedQrcodeRecordService;
import com.graphai.mall.admin.service.IMallRedQrcodeService;
import com.graphai.mall.db.domain.FcAccount;
import com.graphai.mall.db.domain.MallAd;
import com.graphai.mall.db.domain.MallOrderNotifyLog;
import com.graphai.mall.db.domain.MallUser;
import com.graphai.mall.db.service.account.AccountService;
import com.graphai.mall.db.constant.account.AccountStatus;
import com.graphai.mall.db.service.advertisement.MallAdService;
import com.graphai.mall.db.service.order.MallOrderNotifyLogService;
import com.graphai.mall.db.service.sysconfig.MallSystemConfigService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.constant.CommonConstant;
import com.graphai.mall.db.util.RandomUtils;
import com.graphai.mall.db.util.account.AccountUtils;
import com.graphai.mall.db.util.phone.PushUtils;
import com.graphai.nosql.redis.NewCacheInstanceServiceLoader;
import com.graphai.spi.mq.BasedMQProducer;
import com.graphai.spi.mq.MQBaseConstant;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;

@RestController
@RequestMapping("/wx/gamejackpot")
@Validated
public class WxScanCodeRedPacketController {

    private final Log logger = LogFactory.getLog(WxScanCodeRedPacketController.class);

    @Autowired
    private IMallRedQrcodeRecordService iMallRedQrcodeRecordService;
    @Autowired
    private MallSystemConfigService mallSystemConfigService;
    @Autowired
    private AccountService accountService;
    @Autowired
    private MallUserService mallUserService;
    @Autowired
    private IMallPageViewService mallPageViewService;
    @Autowired
    private BasedMQProducer __basedMQProducer;
    @Autowired
    private IMallRedQrcodeService serviceImpl;
    @Autowired
    private MallAdService mallAdService;
    @Autowired
    private MallOrderNotifyLogService logService;


    @Value("${mall.wx.mpappid}")
    private String appid;
    @Value("${weiRed.app}")
    private String app;
    @Value("${weiRed.privateKey}")
    private String privateKey;
    @Value("${weiRed.publicKey}")
    private String publicKey;
    @Value("${weiRed.qrcodeUrl}")
    private String requestUrl;

    /**
     * 扫红包码领取红包
     *
     * @return
     */
    @GetMapping("/receiveRedPacket")
    public Object receiveRedPacket(@RequestParam String userId, @RequestParam String random, HttpServletRequest httpServerRequest, @RequestParam String adId) throws Exception {


        Map<String, String> map = mallSystemConfigService.redPacketRule();// 红包规则
        // 配置限流规则
        List<FlowRule> rules = new ArrayList<>();
        FlowRule rule = new FlowRule();
        rule.setResource("tutorial");
        // QPS 不得超出 xx
        Integer qpsNum = Integer.parseInt(map.get("mall_red_qps_number"));
        rule.setCount(qpsNum);
        rule.setGrade(RuleConstant.FLOW_GRADE_QPS);
        rule.setLimitApp("default");
        rules.add(rule);
        // 加载规则
        FlowRuleManager.loadRules(rules);

        // 下面开始运行被限流作用域保护的代码
        if (SphO.entry("tutorial")) {
            try {
                // logger.info("hello world");

                String userAgent = getRequest().getHeader("user-agent");
                String device = "";// 设备类型 iPhone(苹果) android（安卓）
                if (null != userAgent && userAgent.contains("Android")) {
                    device = "Android";
                } else if (null != userAgent && userAgent.contains("iPhone")) {
                    device = "iPhone";
                }
                String IP = IpUtils.getIpAddress(getRequest());
                // todo 判断是否公司域名领取红包
                // StringBuffer url = getRequest().getRequestURL();
                // String contextPath = getRequest().getServletContext().getContextPath();
                // String uri = url.delete(url.length() -
                // getRequest().getRequestURI().length(),url.length()).append(contextPath).toString();
                // String uri = httpServerRequest.getHeader("Referer");
                //// String uri = url.delete(url.length() - getRequest().getRequestURI().length(),
                // url.length()).append("/").toString();
                //
                // logger.info("-----领取uri-----"+uri);
                // logger.info("-----本机uri-----"+requestUrl);
                //
                // if(!uri.equals(requestUrl)){
                // return ResponseUtil.fail(402,"领取失败，域名非法！");
                // }

                MallOrderNotifyLog log = new MallOrderNotifyLog();

                // 64位解码加密后的字符串
                byte[] inputByte = org.apache.commons.codec.binary.Base64.decodeBase64(random.getBytes("UTF-8"));
                // base64编码的私钥
                byte[] decoded1 = Base64.decodeBase64(privateKey);
                RSAPrivateKey priKey = (RSAPrivateKey) KeyFactory.getInstance("RSA").generatePrivate(new PKCS8EncodedKeySpec(decoded1));
                // RSA解密
                Cipher cipher1 = Cipher.getInstance("RSA");
                cipher1.init(Cipher.DECRYPT_MODE, priKey);
                // 解密后红包code
                String code = new String(cipher1.doFinal(inputByte));
                Map<String, String> mapRequest = new HashMap<String, String>();
                String[] arrSplit = code.split("[&]");
                for (String strSplit : arrSplit) {
                    String[] arrSplitEqual = null;
                    arrSplitEqual = strSplit.split("[=]");
                    // 解析出键值
                    if (arrSplitEqual.length > 1) {
                        // 正确解析
                        mapRequest.put(arrSplitEqual[0], arrSplitEqual[1]);
                    } else {
                        if (arrSplitEqual[0] != "") {
                            // 只有参数没有值，不加入
                            mapRequest.put(arrSplitEqual[0], "");
                        }
                    }
                }


                String makerCode = mapRequest.get("makerCode");
                String randomCode1 = mapRequest.get("randomCode");
                String[] strings = randomCode1.split(",");
                String randomCode = (String) NewCacheInstanceServiceLoader.objectCache().get(strings[1]);// redis中随机串key
                String signa = strings[0];// 随机串
                long time = Long.parseLong(mapRequest.get("time"));// 时间
                // 获取毫秒数
                Long milliSecond = LocalDateTime.now().toInstant(ZoneOffset.of("+8")).toEpochMilli();// 当前时间
                long timeNow = Long.parseLong(map.get("mall_red_packet_time"));// 允许领取时间长，超了不给领
                String sum = map.get("mall_red_packet_number");// 个人最高领取次数
                String enableMq = map.get("mall_red_enable_mq");// 是否启用Mq
                Map data = new HashMap();
                List<MallAd> listTad = mallAdService.queryIndex((byte) 107);// 第三方广告
                int nu = RandomUtil.randomInt(0, listTad.size());

                if ((milliSecond - time) / 1000 > timeNow) {
                    data.put("red", 0);
                    data.put("userNum", 0);
                    data.put("ad", listTad.get(nu).getUrl());
                    data.put("msg", "领取超时，请重新扫码！");
                    return ResponseUtil.ok(data);
                }


                if (signa.equals(randomCode)) {

                    MallRedQrcode one = getQrcode(makerCode);
                    String sevenNum = one.getSevenNum();// 创客分润比例
                    String oneNum = one.getOneNum();// 红包额度区间第一次
                    String fiveNum = one.getFiveNum();// 红包额度区间第五次
                    String redpacketId = one.getId();// 红包id
                    String[] str = oneNum.split(",");// 第一次
                    String[] str2 = one.getTwoNum().split(",");// 第二次
                    String[] strt = one.getThreeNum().split(",");// 第三次
                    String[] strf = one.getFourNum().split(",");// 第四次
                    String[] str1 = fiveNum.split(",");// KA第五次( 新用户)
                    String[] strSix = one.getSixNum().split(",");// KM第五次( 旧用户)
                    Integer userNumber = one.getUserNumber();// 个人红包领取次数
                    Integer surplusNumber = one.getSurplusNumber();


                    double ed1 = Double.parseDouble(str[0]);// 区间1
                    double ed2 = Double.parseDouble(str[1]);// 区间1
                    double edt1 = Double.parseDouble(str2[0]);// 区间2
                    double edt2 = Double.parseDouble(str2[1]);// 区间2
                    double edth1 = Double.parseDouble(strt[0]);// 区间3
                    double edth2 = Double.parseDouble(strt[1]);// 区间3
                    double edf1 = Double.parseDouble(strf[0]);// 区间4
                    double edf2 = Double.parseDouble(strf[1]);// 区间4
                    double edf4 = Double.parseDouble(str1[0]);// 区间5(ka)
                    double edf5 = Double.parseDouble(str1[1]);// 区间5(ka)
                    double edKm1 = Double.parseDouble(strSix[0]);// 区间5(km)
                    double edKm2 = Double.parseDouble(strSix[1]);// 区间5(km)
                    Random random1 = new Random();
                    // Math.round(Math.random()*(y-x))+x;

                    DecimalFormat df = new DecimalFormat("0.00");
                    BigDecimal red = new BigDecimal(df.format(random1.nextDouble() * (ed2 - ed1) + ed1));// 第一次
                    BigDecimal red1 = new BigDecimal(df.format(random1.nextDouble() * (edt2 - edt1) + edt1));// 第二次
                    BigDecimal red2 = new BigDecimal(df.format(random1.nextDouble() * (edth2 - edth1) + edth1));// 第三次
                    BigDecimal red3 = new BigDecimal(df.format(random1.nextDouble() * (edf2 - edf1) + edf1));// 第四次
                    BigDecimal red4 = new BigDecimal(df.format(random1.nextDouble() * (edf5 - edf4) + edf4));// 第五次
                    BigDecimal redKm = new BigDecimal(df.format(random1.nextDouble() * (edKm2 - edKm1) + edKm1));// 第五次
                    BigDecimal maker1 = new BigDecimal("0");
                    BigDecimal maker2 = new BigDecimal("0");
                    BigDecimal maker3 = new BigDecimal("0");
                    BigDecimal maker4 = new BigDecimal("0");
                    BigDecimal maker5 = new BigDecimal("0");
                    BigDecimal makerKm = new BigDecimal("0");


                    if (surplusNumber <= 0) {
                        data.put("red", 0);
                        data.put("userNum", userNumber);
                        data.put("ad", listTad.get(nu).getUrl());
                        data.put("msg", "您来晚了，红包已领完！");
                        return ResponseUtil.ok(data);
                    } else {
                        surplusNumber = surplusNumber - 1;// 剩余次数
                        one.setSurplusNumber(surplusNumber);
                    }

                    String isNew = "";
                    // 查询用户信息
                    List<MallUser> mallUserList = mallUserService.queryByAlipayId(userId, null);

                    if (mallUserList.size() > 0) {
                        if (LocalDate.now().isEqual(mallUserList.get(0).getAddTime().toLocalDate())) {
                            // 是否新用户
                            isNew = "1";
                        } else {
                            isNew = "0";
                        }
                    }


                    logger.info("isNew---" + isNew);

                    String statr = LocalDateTime.of(LocalDate.now(), LocalTime.of(00, 00, 00)).toString();
                    String end = LocalDateTime.of(LocalDate.now(), LocalTime.of(23, 59, 59)).toString();
                    // 所有码领取次数
                    int cnt1 = iMallRedQrcodeRecordService.count(new QueryWrapper<MallRedQrcodeRecord>().eq("to_user_id", userId).eq("remark", "支付宝用户红包扫码领取记录").between("create_time", statr, end));

                    // 单个码领取次数
                    String statr1 = LocalDateTime.of(LocalDate.now(), LocalTime.of(00, 00, 00)).toString();
                    String end1 = LocalDateTime.of(LocalDate.now(), LocalTime.of(23, 59, 59)).toString();
                    int cnt = iMallRedQrcodeRecordService
                                    .count(new QueryWrapper<MallRedQrcodeRecord>().eq("redpacket_id", redpacketId).eq("to_user_id", userId).eq("remark", "支付宝用户红包扫码领取记录").between("create_time", statr1, end1));


                    BigDecimal amount = new BigDecimal("0.00");
                    BigDecimal makerAmount = new BigDecimal("0.00");
                    String adUrl = "//app.weilaylife.com/common/h5/#/pages/download/index";
                    int bCode = 0;

                    if (cnt1 < Integer.parseInt(sum)) {
                        if (cnt < userNumber) {
                            serviceImpl.updateById(one);// 更新红包信息
                            // 用户领取记录
                            MallRedQrcodeRecord mallRedQrcodeRecord = new MallRedQrcodeRecord();
                            mallRedQrcodeRecord.setCreateTime(LocalDateTime.now());
                            mallRedQrcodeRecord.setUpdateTime(LocalDateTime.now());
                            mallRedQrcodeRecord.setDeleted(false);
                            mallRedQrcodeRecord.setRedpacketId(one.getId());
                            mallRedQrcodeRecord.setToUserId(userId);
                            mallRedQrcodeRecord.setUsersId(one.getUsersId());
                            mallRedQrcodeRecord.setRemark("支付宝用户红包扫码领取记录");
                            mallRedQrcodeRecord.setAmountType("01");// 业务类型：01-红包码 02-app红包 03-整点红包
                            mallRedQrcodeRecord.setType("01");// 分类 01：现金红包 02：整点红包
                            mallRedQrcodeRecord.setTypeOfClaim("01");// 领取类型 01：自领 02：推荐
                            mallRedQrcodeRecord.setClientIp(IP);
                            mallRedQrcodeRecord.setDevice(device);
                            if (!Objects.equals(null, listTad.get(nu))) {
                                mallRedQrcodeRecord.setAdName(listTad.get(nu).getName());
                            }
                            // 创客记录
                            MallRedQrcodeRecord recordMaker = new MallRedQrcodeRecord();
                            recordMaker.setCreateTime(LocalDateTime.now());
                            recordMaker.setUpdateTime(LocalDateTime.now());
                            recordMaker.setDeleted(false);
                            recordMaker.setRedpacketId(one.getId());
                            recordMaker.setToUserId(userId);
                            recordMaker.setUsersId(one.getUsersId());
                            recordMaker.setRemark("创客分润记录");
                            recordMaker.setAmountType("01");// 业务类型：01-红包码 02-app红包 03-整点红包
                            recordMaker.setType("01");// 分类 01：现金红包 02：整点红包
                            recordMaker.setTypeOfClaim("02");// 领取类型 01：自领 02：推荐
                            recordMaker.setClientIp(IP);
                            recordMaker.setDevice(device);
                            if (!Objects.equals(null, listTad.get(nu))) {
                                recordMaker.setAdName(listTad.get(nu).getName());
                            }

                            Map<String, Object> paramsMap = new HashMap();
                            paramsMap.put("dType", AccountStatus.DISTRIBUTION_TYPE_00); // 直接分润
                            paramsMap.put("message", "支付宝红包扫码用户充值");
                            paramsMap.put("capitalTrend", AccountStatus.CAPITAL_TREND_01);
                            paramsMap.put("changeType", AccountStatus.CHANGE_TYPE_01);
                            paramsMap.put("changeAccountType", AccountStatus.CHANGE_ACCOUNT_TYPE_1);
                            paramsMap.put("tradeType", AccountStatus.TRADE_TYPE_17);
                            Map<String, Object> paramsMap1 = new HashMap();
                            paramsMap1.put("dType", AccountStatus.DISTRIBUTION_TYPE_00); // 直接分润
                            paramsMap1.put("message", "红包码推广收益");
                            paramsMap1.put("capitalTrend", AccountStatus.CAPITAL_TREND_01);
                            paramsMap1.put("changeType", AccountStatus.CHANGE_TYPE_01);
                            paramsMap1.put("changeAccountType", AccountStatus.CHANGE_ACCOUNT_TYPE_1);
                            paramsMap1.put("tradeType", AccountStatus.TRADE_TYPE_17);


                            if (cnt == 0) {
                                mallRedQrcodeRecord.setAmount(red);// 小额红包第一次
                                if ("percent".equals(one.getType())) {
                                    // 比例计算
                                    maker1 = new BigDecimal(sevenNum).multiply(red);// 创客分润金额
                                    recordMaker.setAmount(maker1);
                                } else {
                                    maker1 = new BigDecimal(sevenNum);// 固定金额
                                    recordMaker.setAmount(maker1);
                                }
                                amount = red;
                                makerAmount = maker1;
                            } else if (cnt == 1) {
                                mallRedQrcodeRecord.setAmount(red1);// 小额红包第二次
                                if ("percent".equals(one.getType())) {
                                    // 比例计算
                                    maker2 = new BigDecimal(sevenNum).multiply(red1);// 创客分润金额
                                    recordMaker.setAmount(maker2);
                                } else {
                                    maker2 = new BigDecimal(sevenNum);// 固定金额
                                    recordMaker.setAmount(maker2);
                                }
                                amount = red1;
                                makerAmount = maker2;
                            } else if (cnt == 2) {
                                mallRedQrcodeRecord.setAmount(red2);// 小额红包第三次
                                if ("percent".equals(one.getType())) {
                                    // 比例计算
                                    maker3 = new BigDecimal(sevenNum).multiply(red2);// 创客分润金额
                                    recordMaker.setAmount(maker3);
                                } else {
                                    maker3 = new BigDecimal(sevenNum);// 固定金额
                                    recordMaker.setAmount(maker3);
                                }
                                amount = red2;
                                makerAmount = maker3;
                            } else if (cnt == 3) {
                                mallRedQrcodeRecord.setAmount(red3);// 小额红包第四次
                                if ("percent".equals(one.getType())) {
                                    // 比例计算
                                    maker4 = new BigDecimal(sevenNum).multiply(red3);// 创客分润金额
                                    recordMaker.setAmount(maker4);
                                } else {
                                    maker4 = new BigDecimal(sevenNum);// 固定金额
                                    recordMaker.setAmount(maker4);
                                }
                                amount = red3;
                                makerAmount = maker4;
                            } else if ("1".equals(isNew) && cnt == 4) {
                                // 新用户第五次(KA)
                                mallRedQrcodeRecord.setAmount(red4);// 第五次大额红包
                                recordMaker.setAmount(maker5);

                                if ("percent".equals(one.getType())) {
                                    // 比例计算
                                    maker5 = new BigDecimal(sevenNum).multiply(red4);// 创客分润金额
                                    recordMaker.setAmount(maker5);
                                } else {
                                    maker5 = new BigDecimal(sevenNum);// 固定金额
                                    recordMaker.setAmount(maker5);
                                }
                                amount = red4;
                                makerAmount = maker5;
                                adUrl = "//app.weilaylife.com/common/h5/#/pages/download/index";
                                bCode = 303;
                                data.put("loadUrl", adUrl);
                            } else if (cnt == 4) {
                                // 老用户第五次(KM)
                                mallRedQrcodeRecord.setAmount(redKm);// 第五次大额红包
                                if ("percent".equals(one.getType())) {
                                    // 比例计算
                                    makerKm = new BigDecimal(sevenNum).multiply(redKm);// 创客分润金额
                                    recordMaker.setAmount(makerKm);
                                } else {
                                    makerKm = new BigDecimal(sevenNum);// 固定金额
                                    recordMaker.setAmount(makerKm);
                                }
                                amount = redKm;
                                makerAmount = makerKm;
                                adUrl = "//app.weilaylife.com/common/h5/#/pages/download/index";
                                bCode = 303;
                                data.put("loadUrl", adUrl);
                            } else {
                                mallRedQrcodeRecord.setAmount(red);// 小额红包第一次
                                amount = red;
                                maker1 = new BigDecimal(sevenNum).multiply(red);// 创客分润金额
                                recordMaker.setAmount(maker1);
                            }

                            data.put("ad", listTad.get(nu).getUrl());

                            // 启用MQ
                            if ("1".equals(enableMq)) {
                                JSONObject obj = JSONUtil.createObj();
                                obj.append("recordMaker", recordMaker);
                                obj.append("mallRedQrcodeRecord", mallRedQrcodeRecord);
                                obj.append("userParamsMap", paramsMap);
                                obj.append("makerParamsMap", paramsMap1);
                                obj.append("user", mallUserList.get(0));
                                logger.info("MQ--------");
                                // 异步发消息给mq
                                ExecutorService executor = Executors.newFixedThreadPool(1);
                                CompletableFuture.supplyAsync(new Supplier<Integer>() {
                                    @Override
                                    public Integer get() {
                                        Map<String, Object> mqParams = new HashMap<>();
                                        mqParams.put("param", obj.toString());
                                        mqParams.put("bizKey", "");
                                        mqParams.put("tags", MQBaseConstant.RED_CASH_ENVELOPE_TAG);
                                        
                                        __basedMQProducer.send(mqParams);
                                        //wxProducerService.produceMsg(obj.toString(), "", RocketMQConstant.RED_CASH_ENVELOPE_TAG);
                                        return 1;
                                    }
                                }, executor);
                                data.put("red", amount);
                                data.put("userNum", userNumber);
                                FcAccount userAccount1 = accountService.getUserAccount(mallUserList.get(0).getId());
                                data.put("balance", userAccount1.getAmount());// 余额
                                return ResponseUtil.ok(303, data);
                            }
                            // 不启用MQ
                            else {
                                String orderId = GraphaiIdGenerator.nextId("mallRed");
                                iMallRedQrcodeRecordService.save(recordMaker);
                                if (iMallRedQrcodeRecordService.save(mallRedQrcodeRecord)) {
                                    // todo广告生成记录
                                    MallPageView mallPageView = new MallPageView();
                                    if (!Objects.equals(null, listTad.get(nu))) {
                                        mallPageView.setPvName(listTad.get(nu).getName());
                                    }


                                    if (StringUtils.isNotEmpty(device)) {
                                        mallPageView.setMobileType(device);
                                    }

                                    mallPageView.setClientIp(IP);
                                    mallPageView.setOutUserId(userId);
                                    mallPageView.setType("01");
                                    if (StringUtils.isNotEmpty(getRequest().getHeader("version"))) {
                                        mallPageView.setPlatform("app");
                                    }
                                    mallPageView.setCreateTime(LocalDateTime.now());
                                    mallPageViewService.saveOrUpdate(mallPageView);
                                    if (mallUserList.size() > 0) {
                                        // 充值
                                        AccountUtils.accountChange(mallUserList.get(0).getId(), amount, CommonConstant.TASK_REWARD_CASH, AccountStatus.BID_37, paramsMap);
                                        AccountUtils.accountChangeByExchange(one.getUsersId(), makerAmount, CommonConstant.TASK_REWARD_CASH, AccountStatus.BID_37, paramsMap1);

                                        // todo md
                                        // 消息推送
                                        String fansName = StringUtils.isNotEmpty(mallUserList.get(0).getNickname()) ? mallUserList.get(0).getNickname() : mallUserList.get(0).getMobile();
                                        String content = "您的粉丝" + fansName + "成功领取扫码红包，您所得收益¥" + makerAmount.setScale(6, BigDecimal.ROUND_DOWN).toString() + "已转至可提现账户，可点击“有料-零钱提现-钱包流水”进行查看。";
                                        PushUtils.pushMessage(one.getUsersId(), "佣金通知", content, 3, 0);


                                        String appUserId = mallUserList.get(0).getId();
                                        FcAccount userAccount = accountService.getUserAccount(appUserId);
                                        // 满0.1自动打款
                                        if (null != userAccount && userAccount.getAmount().compareTo(new BigDecimal("0.1")) > 0 && userAccount.getAmount().compareTo(new BigDecimal("1")) < 0) {
                                            CertAlipayRequest certAlipayRequest = RedUtils.redCode("1");
                                            logger.info("打款certAlipayRequest-------" + certAlipayRequest.getAppId());
                                            AlipayClient alipayClient = null;
                                            try {
                                                alipayClient = new DefaultAlipayClient(certAlipayRequest);
                                            } catch (AlipayApiException e) {
                                                e.printStackTrace();
                                            }
                                            // 构造API请求
                                            AlipayFundTransUniTransferRequest request = new AlipayFundTransUniTransferRequest();
                                            request.setBizContent("{" + "\"out_biz_no\":\"" + orderId + "\"," + "\"trans_amount\":" + userAccount.getAmount().setScale(2, BigDecimal.ROUND_DOWN) + ","
                                                            + "\"product_code\":\"TRANS_ACCOUNT_NO_PWD\"," + "\"biz_scene\":\"DIRECT_TRANSFER\"," + "\"order_title\":\"转账\"," + "\"payee_info\":{" + "\"identity\":\""
                                                            + userId + "\"," + "\"identity_type\":\"ALIPAY_USER_ID\"," + "    }," + "\"remark\":\"单笔转账\","
                                                            + "\"business_params\":\"{\\\"sub_biz_scene\\\":\\\"REDPACKET\\\"}\"" + "  }");
                                            AlipayFundTransUniTransferResponse response = null;
                                            try {
                                                response = alipayClient.certificateExecute(request);
                                            } catch (AlipayApiException e) {
                                                e.printStackTrace();
                                                log.setErrMsg("支付宝参数解析异常：" + e.getMessage());
                                                logService.add(log);
                                            }
                                            if (response.isSuccess()) {
                                                log.setErrMsg("支付宝打款成功：" + response.isSuccess() + ",打款支付宝Id为：" + userId + ",打款金额为：" + userAccount.getAmount());
                                                log.setWxParams("支付宝");
                                                log.setIsSuccess("成功");

                                                paramsMap.put("dType", AccountStatus.DISTRIBUTION_TYPE_00); // 直接分润
                                                paramsMap.put("message", "支付宝红包打款");
                                                paramsMap.put("capitalTrend", AccountStatus.CAPITAL_TREND_02);
                                                paramsMap.put("changeType", AccountStatus.CHANGE_TYPE_09);
                                                if (userAccount.getAmount().compareTo(new BigDecimal(0.00)) == 1) {
                                                    AccountUtils.accountChange(appUserId, userAccount.getAmount(), CommonConstant.TASK_REWARD_CASH, AccountStatus.BID_37, paramsMap);
                                                }

                                                logService.add(log);

                                                data.put("red", amount);
                                                data.put("userNum", userNumber);
                                                FcAccount userAccount1 = accountService.getUserAccount(appUserId);
                                                data.put("balance", userAccount1.getAmount());// 余额

                                                return ResponseUtil.ok(bCode, data);
                                            } else {

                                                return ResponseUtil.fail("网络繁忙请稍后重试!");

                                            }


                                        } else {
                                            data.put("red", amount);
                                            data.put("userNum", userNumber);
                                            FcAccount userAccount1 = accountService.getUserAccount(mallUserList.get(0).getId());
                                            data.put("balance", userAccount1.getAmount());// 余额
                                            return ResponseUtil.ok(bCode, data);
                                        }

                                    } else {
                                        return ResponseUtil.fail("未查到用户!");
                                    }
                                }
                            }


                        } else {
                            data.put("red", 0);
                            data.put("userNum", userNumber);
                            data.put("ad", listTad.get(nu).getUrl());
                            data.put("msg", "今日次数已用完，请明日再来！");
                            data.put("loadUrl", adUrl);
                            return ResponseUtil.ok(303, data);
                        }
                    } else {
                        data.put("red", 0);
                        data.put("userNum", userNumber);
                        data.put("ad", listTad.get(nu).getUrl());
                        data.put("msg", "今日次数已用完，请明日再来！");
                        data.put("loadUrl", adUrl);
                        return ResponseUtil.ok(303, data);
                    }
                    return ResponseUtil.ok(data);
                } else {
                    data.put("red", 0);
                    data.put("userNum", 1);
                    data.put("ad", listTad.get(nu).getUrl());
                    data.put("msg", "今日次数已用完，请明日再来！");
                    return ResponseUtil.ok(data);
                }
            } finally {
                SphO.exit();
            }
        } else {
            // 触发限流
            return ResponseUtil.fail("当前活动太火爆请稍后重试!");
        }
    }

    @Cached(key = "'getQrcode'", expire = 1, timeUnit = TimeUnit.DAYS, cacheType = CacheType.LOCAL)
    public MallRedQrcode getQrcode(String code) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("mall_red_qrcode.code", code);
        return serviceImpl.getOne(queryWrapper);
    }


    /**
     * 生成通用二维码
     */
    @GetMapping("/qrCode")
    public Object createQrCode(HttpServletResponse response, @RequestParam String makerCode) throws IOException {
        logger.info("进入红包------");

        String userAgent = getRequest().getHeader("user-agent");
        logger.info("userAgent--------" + userAgent.contains("MicroMessenger"));
        if (userAgent == null || userAgent.contains("MicroMessenger")) {
            String strU = URLEncoder.encode("请用支付宝扫码!", "utf-8");
            String redirectUrl1 = app + "/#/pages/code/white?code=500&msg=" + strU;
            response.sendRedirect(redirectUrl1);
            return null;
        }
        try {
            // 查看二维码是否被绑定
            QueryWrapper queryWrapper = new QueryWrapper();
            queryWrapper.eq("mall_red_qrcode.code", makerCode);
            MallRedQrcode one = serviceImpl.getOne(queryWrapper);
            LocalDateTime end = one.getEndTime();
            LocalDateTime now = LocalDateTime.now();
            String status = one.getStatus();
            if ("1".equals(status) || "2".equals(status)) {
                String strU = URLEncoder.encode("二维码已过期!", "utf-8");
                String redirectUrl1 = app + "/#/pages/code/white?code=402&msg=" + strU;
                response.sendRedirect(redirectUrl1);
                return null;
            }
            if (null == end && now.isAfter(end)) {
                String strU = URLEncoder.encode("二维码已过期!", "utf-8");
                String redirectUrl1 = app + "/#/pages/code/white?code=402&msg=" + strU;
                response.sendRedirect(redirectUrl1);
                return null;
            }
            if (null == one.getUsersId() && StringUtils.isEmpty(one.getUsersId())) {
                String strU = URLEncoder.encode("二维码未绑定创客!", "utf-8");
                String redirectUrl1 = app + "/#/pages/code/white?code=402&msg=" + strU;
                response.sendRedirect(redirectUrl1);
                return null;
            }

            // 获取毫秒数
            Long milliSecond = LocalDateTime.now().toInstant(ZoneOffset.of("+8")).toEpochMilli();
            // 生成随机串
            String randomCode = RandomUtils.genRandomNum(6);
            NewCacheInstanceServiceLoader.objectCache().PUT("randomCode" + milliSecond, randomCode);
            String conent = "makerId=" + one.getUsersId() + "&makerCode=" + makerCode + "&time=" + milliSecond + "&randomCode=" + randomCode + ",randomCode" + milliSecond;
            // logger.info("明文------"+conent);
            // base64编码的公钥
            byte[] decoded = org.apache.commons.codec.binary.Base64.decodeBase64(publicKey);
            RSAPublicKey pubKey = (RSAPublicKey) KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(decoded));
            // RSA加密
            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.ENCRYPT_MODE, pubKey);
            String outStr = org.apache.commons.codec.binary.Base64.encodeBase64String(cipher.doFinal(conent.getBytes("UTF-8")));
            Map map = mallSystemConfigService.listAdRed();
            // logger.info("进入红包------"+map.get("Mall_ad_red_funeng"));
            String outStr2 = URLEncoder.encode(outStr, "utf-8");
            String str = URLEncoder.encode((String) map.get("Mall_ad_red_funeng"), "utf-8");
            String redirectUrl = URLEncoder.encode(app + "/#/pages/code/white?c=" + str + "&position=73&m=" + outStr2, "utf-8");
            logger.info("redirectUrl------" + app);

            String redLocation = "https://openauth.alipay.com/oauth2/publicAppAuthorize.htm?app_id=2021002114615168&scope=auth_base&redirect_uri=" + redirectUrl;
            logger.info(redLocation);
            response.sendRedirect(redLocation);
        } catch (Exception e) {
            logger.error("扫码异常 : " + e.getMessage(), e);
            return ResponseUtil.fail(402, "二维码已失效！");
        }

        return null;
    }

    /**
     * 海报
     */
    @GetMapping("/queryAd")
    public Object queryAd(@RequestParam String userId) throws Exception {
        List<MallAd> listAd = mallAdService.queryByPosition("73");// 海报
        List<MallAd> listTad = mallAdService.queryByPosition("107");// 第三方广告豆盟

        return ResponseUtil.ok(listAd);
    }

    /**
     * 红包码余额提现
     */
    @GetMapping("/reflect")
    public Object reflect(@RequestParam String aliUserId, @RequestParam String type) throws Exception {

        List<MallUser> mallUserList = mallUserService.queryByAlipayId(aliUserId, null);
        String userId = mallUserList.get(0).getId();
        Map<String, Object> paramsMap = new HashMap();
        String orderId = GraphaiIdGenerator.nextId("mallRed");
        FcAccount userAccount = accountService.getUserAccount(userId);
        // 满0.1自动打款
        if (null != userAccount && userAccount.getAmount().compareTo(new BigDecimal("0.1")) > 0) {
            CertAlipayRequest certAlipayRequest = RedUtils.redCode("1");
            logger.info("打款certAlipayRequest-------" + certAlipayRequest.getAppId());
            AlipayClient alipayClient = null;
            try {
                alipayClient = new DefaultAlipayClient(certAlipayRequest);
            } catch (AlipayApiException e) {
                e.printStackTrace();
            }
            // 构造API请求
            AlipayFundTransUniTransferRequest request = new AlipayFundTransUniTransferRequest();
            request.setBizContent("{" + "\"out_biz_no\":\"" + orderId + "\"," + "\"trans_amount\":" + userAccount.getAmount().setScale(2, BigDecimal.ROUND_DOWN) + "," + "\"product_code\":\"TRANS_ACCOUNT_NO_PWD\","
                            + "\"biz_scene\":\"DIRECT_TRANSFER\"," + "\"order_title\":\"转账\"," + "\"payee_info\":{" + "\"identity\":\"" + aliUserId + "\"," + "\"identity_type\":\"ALIPAY_USER_ID\"," + "    },"
                            + "\"remark\":\"单笔转账\"," + "\"business_params\":\"{\\\"sub_biz_scene\\\":\\\"REDPACKET\\\"}\"" + "  }");
            AlipayFundTransUniTransferResponse response = null;
            try {
                response = alipayClient.certificateExecute(request);
            } catch (AlipayApiException e) {
                e.printStackTrace();
            }
            if (response.isSuccess()) {

                paramsMap.put("dType", AccountStatus.DISTRIBUTION_TYPE_00); // 直接分润
                paramsMap.put("message", "支付宝红包打款");
                paramsMap.put("capitalTrend", AccountStatus.CAPITAL_TREND_02);
                paramsMap.put("changeType", AccountStatus.CHANGE_TYPE_09);
                if (userAccount.getAmount().compareTo(new BigDecimal(0.00)) == 1) {
                    AccountUtils.accountChange(userId, userAccount.getAmount(), CommonConstant.TASK_REWARD_CASH, AccountStatus.BID_37, paramsMap);
                }

                logger.info("调用成功");
                return ResponseUtil.fail(402, "提现成功！");
            } else {
                logger.info("调用失败");
                return ResponseUtil.fail("网络繁忙请稍后重试!");

            }
        } else {

            return ResponseUtil.fail(402, "金额不足0.1！");
        }


    }


    /**
     * 查询支付宝userId
     *
     * @param authCode
     * @return
     */
    @GetMapping("/queryUserId")
    public Object queryUserId(@RequestParam String authCode) throws Exception {


        CertAlipayRequest certAlipayRequest = RedUtils.redCode("1");
        logger.info("certAlipayRequest-----" + certAlipayRequest.getAppId());
        // 构造client
        AlipayClient alipayClient = null;
        try {
            alipayClient = new DefaultAlipayClient(certAlipayRequest);
            logger.info("alipayClient-----" + alipayClient.toString());
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        // 构造API请求
        AlipaySystemOauthTokenRequest request = new AlipaySystemOauthTokenRequest();
        request.setCode(authCode);// 这个就是第一步获取的auth_code
        request.setGrantType("authorization_code");
        logger.info("AlipaySystemOauthTokenRequest-----" + request.getCode());
        AlipaySystemOauthTokenResponse response = null;
        try {
            response = alipayClient.certificateExecute(request);
            logger.info("AlipaySystemOauthTokenResponse-----" + response.getUserId());
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }

        List<MallAd> listTad = mallAdService.queryIndex((byte) 107);// 第三方广告
        int nu = RandomUtil.randomInt(0, listTad.size());
        String userId = response.getUserId();
        // 查询用户信息
        List<MallUser> mallUserList = mallUserService.queryByAlipayId(userId, null);
        MallUser mallUser = new MallUser();
        Map<String, Object> dataMap = new HashMap<>();
        if (mallUserList.size() == 0) {
            // 新用户
            mallUser.setNickname("扫码红包用户");
            mallUser.setId(GraphaiIdGenerator.nextId("malluser"));
            mallUser.setAddTime(LocalDateTime.now());
            mallUser.setSource("支付宝扫红包码创建");
            mallUser.setDeleted(false);
            mallUser.setUpdateTime(LocalDateTime.now());
            mallUser.setLevelId("0");
            mallUser.setAlipayUserId(userId);
            mallUserService.addAlipayUser(mallUser);
            dataMap.put("balance", "0");// 余额
            // 是否新用户
            dataMap.put("isNew", "1");

        } else {
            // 用户已存在
            FcAccount userAccount = accountService.getUserAccount(mallUserList.get(0).getId());
            dataMap.put("balance", userAccount.getAmount());// 余额
            if (LocalDate.now().isEqual(mallUserList.get(0).getAddTime().toLocalDate())) {
                logger.info("新用户-----" + mallUserList.get(0));
                // 是否新用户
                dataMap.put("isNew", "1");
            } else {
                dataMap.put("isNew", "0");
            }
        }
        String statr = LocalDateTime.of(LocalDate.now(), LocalTime.of(00, 00, 00)).toString();
        String end = LocalDateTime.of(LocalDate.now(), LocalTime.of(23, 59, 59)).toString();

        // 领取次数
        int cnt = iMallRedQrcodeRecordService.count(new QueryWrapper<MallRedQrcodeRecord>().eq("to_user_id", userId).eq("remark", "支付宝用户红包扫码领取记录").between("create_time", statr, end));
        logger.info("支付宝用户红包扫码领取记录-----" + cnt);

        if (response.isSuccess()) {
            dataMap.put("userId", userId);
            dataMap.put("number", cnt);
            dataMap.put("ad", listTad.get(nu));
            return ResponseUtil.ok(dataMap);
        } else {
            return ResponseUtil.fail("网络繁忙请稍后重试!");
        }
    }

}
