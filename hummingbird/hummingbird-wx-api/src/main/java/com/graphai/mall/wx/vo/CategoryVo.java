package com.graphai.mall.wx.vo;

import com.graphai.mall.db.domain.MallCategory;
import lombok.Data;

import java.util.List;

/**
 * @author longx
 * @date 2021/7/1311:53
 */
@Data
public class CategoryVo {
    // 二级
    private MallCategory mallCategory;
    // 三级
    private List<MallCategory> childCategory;
}
