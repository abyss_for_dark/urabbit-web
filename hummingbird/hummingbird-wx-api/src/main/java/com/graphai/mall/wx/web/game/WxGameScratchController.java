package com.graphai.mall.wx.web.game;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.mall.db.domain.FcAccount;
import com.graphai.mall.db.domain.FcAccountRechargeBill;
import com.graphai.mall.db.domain.GameScratchHappy;
import com.graphai.mall.db.domain.UserVo;
import com.graphai.mall.db.service.account.AccountService;
import com.graphai.mall.db.constant.account.AccountStatus;
import com.graphai.mall.db.service.account.FcAccountRechargeBillService;
import com.graphai.mall.db.service.game.GameMotionMakeMoneyService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.constant.CommonConstant;
import com.graphai.mall.db.util.account.AccountUtils;
import com.graphai.mall.wx.annotation.LoginUser;

@RestController
@RequestMapping("/wx/gamejackpot")
@Validated
public class WxGameScratchController {

    @Autowired
    private FcAccountRechargeBillService fcAccountRechargeBillService;
    @Autowired
    private MallUserService mallUserService;
    @Autowired
    private AccountService accountService;
    @Autowired
    private GameMotionMakeMoneyService gameMotionMakeMoneyService;

    /**
     * 3.1 查询商品列表 刮刮乐游戏
     */
    @RequestMapping("/scratchGame")
    public Object getGameScratchDetail(@LoginUser String userId) {
        if (null == userId) {
            return ResponseUtil.unlogin();
        }

        FcAccount fc = accountService.getUserAccount(userId);
        List<GameScratchHappy> list = gameMotionMakeMoneyService.selectScratchHappy();
        Map<String, Object> dataMap = new HashMap();
        if (null != fc) {
            dataMap.put("gold", fc.getGold());
        } else {
            dataMap.put("gold", 0);
        }
        int sum = 0;
        if (null != list && list.size() > 0) {
            for (GameScratchHappy record : list) {
                sum += record.getNumber();
            }
            dataMap.put("sum", sum);
        } else {
            dataMap.put("sum", 0);
        }

        dataMap.put("goods", list);
        List<FcAccountRechargeBill> billList = fcAccountRechargeBillService.listFcAccountBillList(userId);
        List<Map> list1 = new ArrayList<>();
        if (billList.size() > 0) {
            for (FcAccountRechargeBill bill : billList) {
                Map map = new HashMap();
                UserVo user = mallUserService.findUserVoById(bill.getCustomerId());
                if (null != user.getNickname() && null != user.getAvatar()) {
                    String myName = null;
                    char[] chars1 = user.getNickname().toCharArray();
                    // if(chars1.length>=11){
                    //// char[] chars = nick.toCharArray();
                    // for(int i=0;i<chars1.length;i++){
                    // if(i>2 && i< chars1.length-3){
                    // chars1[i]='*';
                    // }
                    // }
                    // String myCustNo = String.valueOf(chars1);
                    // map.put("name",myCustNo);
                    // }else if(chars1.length>=3 && chars1.length<=5){
                    // myName=user.getNickname().replaceAll(user.getNickname().substring(1, chars1.length-1), "*");
                    // map.put("name",myName);
                    // }else if(chars1.length>=6 && chars1.length<=11){
                    // myName=user.getNickname().replaceAll(user.getNickname().substring(2, chars1.length-1), "*");
                    // map.put("name",myName);
                    // }else if(chars1.length == 2){
                    // myName=user.getNickname().replaceAll(user.getNickname().substring(1, chars1.length), "*");
                    // map.put("name",myName);
                    // }
                    if (user.getNickname().length() >= 11) {
                        String result = user.getNickname().substring(user.getNickname().length() - 3, user.getNickname().length());
                        String result1 = user.getNickname().substring(0, 3);
                        String myCustNo = result1 + "****" + result;
                        map.put("name", myCustNo);
                    } else if (user.getNickname().length() >= 3 && user.getNickname().length() <= 5) {
                        String result = user.getNickname().substring(user.getNickname().length() - 1, user.getNickname().length());
                        String result1 = user.getNickname().substring(0, 1);
                        myName = result1 + "**" + result;
                        map.put("name", myName);
                    } else if (user.getNickname().length() >= 6 && user.getNickname().length() <= 11) {
                        String result = user.getNickname().substring(user.getNickname().length() - 2, user.getNickname().length());
                        String result1 = user.getNickname().substring(0, 2);
                        myName = result1 + "***" + result;
                        map.put("name", myName);
                    } else if (user.getNickname().length() == 2) {
                        myName = user.getNickname().substring(0, 1) + "*";
                        map.put("name", myName);
                    }
                    map.put("avatar", user.getAvatar());
                    map.put("gold", bill.getAmt());
                } else {
                    final double d = Math.random();
                    final int i = (int) (d * 100);
                    map.put("name", "136****45" + i);
                    map.put("avatar", "http://gxfacepay.oss-cn-shenzhen.aliyuncs.com/hummingbird/mipmap/she.png");
                    map.put("gold", i);
                }
                list1.add(map);
            }
        } else {
            for (int i = 0; i < 20; i++) {
                Map map = new HashMap();
                final double a = Math.random();
                final int b = (int) (a * 100);
                map.put("name", "136*****5" + b);
                map.put("avatar", "http://gxfacepay.oss-cn-shenzhen.aliyuncs.com/hummingbird/mipmap/she.png");
                map.put("gold", b);
                list1.add(map);
            }
        }
        dataMap.put("record", list1);
        return ResponseUtil.ok(dataMap);
    }

    /**
     * 3.1 点击游戏 刮刮乐游戏
     */
    @RequestMapping("/clickScratch")
    public Object postGameDetail(@LoginUser String userId, @RequestParam String gameId, @RequestParam String number) throws Exception {
        if (null == userId) {
            return ResponseUtil.unlogin();
        }
        GameScratchHappy record = gameMotionMakeMoneyService.selectScratchHappyGameById(gameId);
        if (null != record) {
            if (record.getNumber() < 0) {
                return ResponseUtil.fail(403, "可刮次数已经使用完！");
            } else {
                int sum = record.getNumber() - 1;
                record.setNumber(sum);
            }
        }

        int i = gameMotionMakeMoneyService.updateScratchHappyGame(record);

        FcAccount fc = accountService.getUserAccount(userId);
        BigDecimal goldj = new BigDecimal(50);
        if (null != fc && fc.getGold().compareTo(goldj) > 0) {

            Map<String, Object> paramsMap = new HashMap();
            paramsMap.put("dType", AccountStatus.DISTRIBUTION_TYPE_00); // 直接分润
            paramsMap.put("message", "刮刮乐游戏金币消耗");
            paramsMap.put("capitalTrend", AccountStatus.CAPITAL_TREND_02);
            paramsMap.put("changeType", AccountStatus.CHANGE_TYPE_09);
            if (goldj.compareTo(new BigDecimal(0.00)) == 1) {
                AccountUtils.accountChange(userId, goldj, CommonConstant.TASK_REWARD_GOLD, AccountStatus.BID_7, paramsMap);
            }

        } else {
            return ResponseUtil.fail(403, "账户金币不足！");
        }


        BigDecimal gold = new BigDecimal(0.00);
        switch (number) {
            case "1":
                gold = new BigDecimal(26);
                break;
            case "2":
                gold = new BigDecimal(36);
                break;
            case "3":
                gold = new BigDecimal(46);
                break;
            case "4":
                gold = new BigDecimal(56);
                break;
            case "5":
                gold = new BigDecimal(66);
                break;
            case "6":
                gold = new BigDecimal(666);
                break;
        }

        Map<String, Object> paramsMap = new HashMap();
        paramsMap.put("dType", AccountStatus.DISTRIBUTION_TYPE_00); // 直接分润
        paramsMap.put("message", "刮刮乐游戏金币充值");
        paramsMap.put("capitalTrend", AccountStatus.CAPITAL_TREND_01);
        paramsMap.put("changeType", AccountStatus.CHANGE_TYPE_01);
        paramsMap.put("changeAccountType", AccountStatus.CHANGE_ACCOUNT_TYPE_2);

        if (gold.compareTo(new BigDecimal(0.00)) == 1) {
            AccountUtils.accountChange(userId, gold, CommonConstant.TASK_REWARD_GOLD, AccountStatus.BID_7, paramsMap);
        }

        Map<String, Object> dataMap = new HashMap();

        dataMap.put("gold", gold);

        return ResponseUtil.ok(dataMap);

    }

}
