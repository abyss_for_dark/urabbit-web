package com.graphai.mall.wx.web.integral;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.json.JacksonUtil;
import com.graphai.id.GraphaiIdGenerator;
import com.graphai.mall.admin.domain.MallSystemRule;
import com.graphai.mall.admin.service.IGameRedHourRuleService;
import com.graphai.mall.db.domain.MallOrder;
import com.graphai.mall.db.service.account.AccountStatus;
import com.graphai.mall.db.service.order.MallOrderService;
import com.graphai.mall.db.util.OrderUtil;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.mall.wx.service.WxRechargeIntegralService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;


/**
 * 积分充值
 * @author Qxian
 * @date 2020-10-14
 * @version V0.0.1
 */
@RestController
@RequestMapping("/wx/recharge/integral")
@Validated
public class WxRechargeIntegralController {
	@Autowired
	private WxRechargeIntegralService wxRechargeIntegralService;
	@Autowired
	private IGameRedHourRuleService redHourRuleService;
	@Autowired
	private MallOrderService mallOrderService;

	/**
	 * 充值规则列表
	 *
	 * @param userId
	 *            用户ID
	 * @return 订单列表
	 */
	@GetMapping("list")
	public Object list(@LoginUser String userId) {
		return wxRechargeIntegralService.list(userId);
	}

	/**
	 * 积分充值流水列表
	 * @param userId
	 * @param page
	 * @param limit
	 * @param sort
	 * @param order
	 * @return
	 */
	@GetMapping("rechargeList")
	public Object rechargeList(@LoginUser String userId,@RequestParam(defaultValue = "1") Integer page,
							   @RequestParam(defaultValue = "10") Integer limit,
							   @RequestParam(defaultValue = "create_date") String sort,
							   @RequestParam(defaultValue = "desc") String order) {
		return wxRechargeIntegralService.rechargeList(userId,page,limit,sort,order);
	}


	/**
	 * 生成积分支付订单
	 * @param userId
	 * @return
	 */
	@PostMapping("createOrder")
	public Object createOrder(@LoginUser String userId, @RequestBody String body) {
		if (body == null) {
			return ResponseUtil.badArgument();
		}
		// 手机号
		String ruleId = JacksonUtil.parseString(body, "ruleId");
		// 获取积分套餐数据
		MallSystemRule mallSystemRule = redHourRuleService.getById(ruleId);
		if (mallSystemRule == null) {
			return ResponseUtil.badArgumentValue();
		}

		// 生成订单
		String sn = GraphaiIdGenerator.nextId("ORDER_SN");
		MallOrder mallOrder = new MallOrder();
		mallOrder.setId(GraphaiIdGenerator.nextId("MallOrder"));
		mallOrder.setHuid("jfcz"); // 积分充值
		mallOrder.setOrderSn(sn);
		mallOrder.setUserId(userId);
		mallOrder.setOrderStatus(Short.parseShort(String.valueOf(OrderUtil.STATUS_CREATE)));
		mallOrder.setMessage("充值积分支付订单");
		mallOrder.setDeleted(false);
		mallOrder.setOrderPrice(mallSystemRule.getProportion()); // Proportion：充值金额
		mallOrder.setActualPrice(mallSystemRule.getProportion());
		mallOrder.setIntegralPrice(mallSystemRule.getShareProportion());// ShareProportion：充值积分
		mallOrder.setPubSharePreFee(mallSystemRule.getProportion().toString());
		mallOrder.setBid(AccountStatus.BID_51);
		mallOrder.setAddTime(LocalDateTime.now());
		mallOrderService.add(mallOrder);
		return ResponseUtil.ok(mallOrder);
	}
//
//	/**
//	 * 充值积分-预支付
//	 * @param body
//	 * @param request
//	 * @return
//	 */
//	@PostMapping("setMealPrepay")
//	public Object setMealPrepay(@RequestBody String body, HttpServletRequest request) {
//		return wxRechargeIntegralService.setMealPrepay(body, request);
//	}
//
//	/**
//	 * 充值积分回调接口
//	 *
//	 * @param request
//	 *            请求内容
//	 * @param response
//	 *            响应内容
//	 * @return 操作结果
//	 */
//	@PostMapping("setMealPay-notify")
//	public Object setMealPayNotify(HttpServletRequest request, HttpServletResponse response) {
//		return wxRechargeIntegralService.setMealPayNotify(request, response);
//	}

}