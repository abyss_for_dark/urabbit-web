package com.graphai.mall.wx.web.test;

import com.graphai.mall.core.lucene.service.CreateIndexAboutModelService;
import com.graphai.mall.db.dao.MallGoodsMapper;
import com.graphai.mall.db.domain.MallGoodsExample;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@RestController
@RequestMapping("/wx/cwj")
@Validated
public class CwjTestController {
    private final Log logger = LogFactory.getLog(CwjTestController.class);
    @Autowired
    private CreateIndexAboutModelService createIndexAboutModelService;
    @Autowired
    private MallGoodsMapper mallGoodsMapper;
    @Value("${lucene.config.limit}")
    private Integer limit;

    @GetMapping("/hello")
    public Object easyTest(Integer num) {
        logger.info("->>>>>>>>>>>>>>>>>>>>>> - 开启生成全局搜索所需的索引文件！");
        Long total = mallGoodsMapper.countByExample(new MallGoodsExample());
        logger.info("-----------------total:" + total);
        ExecutorService threadPool = Executors.newFixedThreadPool(10);
        int i = 0;
        for (; i < total; ) {
            int finalI = i;
            threadPool.execute(new Runnable() {
                @Override
                public void run() {
                    if (finalI + limit < total) {
                        if (createIndexAboutModelService.createGoodsIndex(limit, finalI).equals("success")) {
                            logger.info("已生成索引文件，序列为：limit:" + limit + "-------offset:" + finalI);
                        }
                    } else {
                        Integer b = total.intValue();
                        Integer lastLimit = b - finalI;
                        createIndexAboutModelService.createGoodsIndex(lastLimit, finalI);
                        logger.info("已生成索引文件，序列为：limit:" + lastLimit + "-------offset:" + finalI);
                    }
                }
            });
            i += limit;
            threadPool.shutdown();
        }
        return null;
    }


}
