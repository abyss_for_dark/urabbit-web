package com.graphai.mall.wx.web.statistics;

import com.github.houbb.sensitive.core.api.SensitiveUtil;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.mall.db.domain.MallRankConfigure;
import com.graphai.mall.db.domain.MallUser;
import com.graphai.mall.db.service.MallRankConfigureService;
import com.graphai.mall.db.constant.account.AccountStatus;
import com.graphai.mall.db.service.account.FcAccountRechargeBillService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.constant.CommonConstant;
import com.graphai.mall.db.vo.MallUserRankVo;
import com.graphai.mall.wx.annotation.LoginUser;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * 分享（用户数据统计）
 */

@RestController
@RequestMapping("/wx/userStatistics")
@Validated
public class WxUserStatisticsController {
    private final Log logger = LogFactory.getLog(WxUserStatisticsController.class);

    @Autowired
    private MallUserService mallUserService;
    @Autowired
    private MallRankConfigureService mallRankConfigureService;
    @Autowired
    private FcAccountRechargeBillService fcAccountRechargeBillService;




    /**
     * 上周排行榜 (邀请人数、现金奖励、当前总业绩奖励、上周排行榜)
     */
    @GetMapping("/rankList")
    public Object rankList(@LoginUser String userId) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        //1.邀请人数
        List<MallUser> mallUserList = mallUserService.invitationCount(userId);
        int invitationCount = 0;
        if (mallUserList.size() > 0) {
            invitationCount = mallUserList.size();
        }
        //2.现金奖励
        List<String> bidList = new ArrayList<>();
        bidList.add(AccountStatus.BID_11);
        BigDecimal cashAward = fcAccountRechargeBillService.selectRechargeAmountByCondition(userId,true, bidList, AccountStatus.DISTRIBUTION_TYPE_00, CommonConstant.TASK_REWARD_CASH, null, null).setScale(2, BigDecimal.ROUND_HALF_UP);
        //排行奖励
        List<String> bidList2 = new ArrayList<>();
        bidList.add(AccountStatus.BID_13);
        BigDecimal rankAward = fcAccountRechargeBillService.selectRechargeAmountByCondition(userId,true, bidList2, AccountStatus.DISTRIBUTION_TYPE_00, CommonConstant.TASK_REWARD_CASH, null, null).setScale(2, BigDecimal.ROUND_HALF_UP);
        //3.当前总业绩奖励
        BigDecimal totalPerformanceAward = cashAward.add(rankAward);

        //4.上周排行
        LocalDate now = LocalDate.now();
        int value = now.getDayOfWeek().getValue();
        LocalDate endDate = now.minusDays(value);   //结束时间
        LocalDate beginDate = endDate.minusDays(6); //开始时间
        logger.info("开始时间： " + beginDate + ",结束时间： " + endDate);
        List<MallUserRankVo> lastWeekRankList = mallUserService.rankList(null, beginDate, endDate);

        //5.本场排行
        List<MallUserRankVo> currentRankList = mallUserService.rankList(null, null, null);
        //我的排行——本周
        Integer lastWeekRank = 1;
        //我的排行——当前
        Integer currentRank = 1;
        if(lastWeekRankList.size() > 0){
            lastWeekRankList = SensitiveUtil.desCopyCollection(lastWeekRankList);
            for (int i = 0; i < lastWeekRankList.size(); i++) {
                if (lastWeekRankList.get(i).getId().equals(userId)) {
                    lastWeekRank = lastWeekRankList.get(i).getOrderNum();
                }
            }
        }
        if(currentRankList.size() > 0){
            currentRankList = SensitiveUtil.desCopyCollection(currentRankList);
            for (int i = 0; i < currentRankList.size(); i++) {
                if (currentRankList.get(i).getId().equals(userId)) {
                    currentRank = currentRankList.get(i).getOrderNum();
                }
            }
        }
        if(lastWeekRankList.size() > 10){
            lastWeekRankList = lastWeekRankList.subList(0, 10);  //默认10行
        }
        if(currentRankList.size() > 10){
            currentRankList = currentRankList.subList(0, 10);
        }

        //6.排行奖励金额
        MallRankConfigure configure = new MallRankConfigure();
        List<MallRankConfigure> mallRankConfigureList = mallRankConfigureService.selectMallRankConfigureList(configure, null, null, "order_num", "asc");
        //7.奖金发放时间  //todo 暂时按照上周结束时间的 后面一天
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy年MM月dd日");
        String grantDate = endDate.plusDays(1).format(dateTimeFormatter);
        //8.活动结束时间
        String activityEndDate = endDate.plusDays(8) + " 00:00:00";

        //9.我的收益
        Map<String, Object> myProfitMap = new HashMap<>();
        myProfitMap.put("cashAward", cashAward); //现金奖励
        myProfitMap.put("rankAward", rankAward); //排行奖金

        //todo -----暂时注释，后面放开-----
        Map<String, Object> map = new HashMap<>();
//        map.put("invitationCount", invitationCount);  //邀请人数
//        map.put("cashAward", cashAward);         //现金奖励
//        map.put("totalPerformanceAward", totalPerformanceAward); //当前总业绩奖励  现金奖励+排行奖金
//        map.put("lastWeekRankList", lastWeekRankList);  //上周排行榜
//        map.put("currentRankList", currentRankList);  //本场排行榜
//        map.put("lastWeekRank", lastWeekRank); //本周排行
//        map.put("currentRank", currentRank); //当前排行
//        map.put("mallRankConfigureList", mallRankConfigureList);  //排行奖励金额
//        map.put("myProfit", myProfitMap);  //我的收益
//        map.put("grantDate", grantDate);  //奖金发放时间
//        String grantDesc = "活动结束，奖金将于" + grantDate + "前发放。下一期活动敬请期待！";
//        map.put("grantDesc",grantDesc);   //奖金发放时间描述
//        map.put("activityEndDate", activityEndDate);  //活动结束时间



        //todo -----暂时使用这个空数据，后面删除-----
        map.put("invitationCount", invitationCount);  //邀请人数
        map.put("cashAward", cashAward);         //现金奖励
        map.put("totalPerformanceAward", '0'); //当前总业绩奖励   现金奖励+排行奖金

        lastWeekRankList = new ArrayList<>();
        map.put("lastWeekRankList", lastWeekRankList);  //上周排行榜
        map.put("currentRankList", lastWeekRankList);  //本场排行榜
        map.put("lastWeekRank", "999+"); //本周排行
        map.put("currentRank", "999+"); //当前排行
        map.put("mallRankConfigureList", mallRankConfigureList);  //排行奖励金额
        map.put("myProfit", myProfitMap);  //我的收益
        map.put("grantDate", grantDate);  //奖金发放时间
        String grantDesc = "活动结束,奖金将于" + grantDate + "前发放。下一期活动敬请期待！";
        map.put("grantDesc",grantDesc);   //奖金发放时间描述
        map.put("activityEndDate", activityEndDate);  //活动结束时间

        return ResponseUtil.ok(map);
    }


}
