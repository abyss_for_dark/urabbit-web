package com.graphai.mall.wx.web.agent;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.lang.StringUtils;
import com.graphai.mall.admin.domain.*;
import com.graphai.mall.admin.service.*;
import com.graphai.mall.admin.vo.AgentCommentVo;
import com.graphai.mall.admin.vo.AgentUserVo;
import com.graphai.mall.admin.vo.AgentFootprintVo;
import com.graphai.mall.db.dao.MallUserMapper;
import com.graphai.mall.db.domain.*;
import com.graphai.mall.db.service.account.AccountService;
import com.graphai.mall.db.service.common.ShipService;
import com.graphai.mall.db.service.goods.MallGoodsProductService;
import com.graphai.mall.db.service.goods.MallGoodsService;
import com.graphai.mall.db.service.notice.MallNoticeService;
import com.graphai.mall.db.service.order.MallAftersaleService;
import com.graphai.mall.db.service.order.MallOrderGoodsService;
import com.graphai.mall.db.service.order.MallOrderService;
import com.graphai.mall.db.service.sysconfig.MallSystemConfigService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.service.user.behavior.MallCollectService;
import com.graphai.mall.db.service.user.behavior.MallCommentService;
import com.graphai.mall.db.service.user.behavior.MallFootprintService;
import com.graphai.mall.db.util.OrderUtil;
import com.graphai.mall.db.vo.MallZYOrderVo;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.mall.wx.service.WxOrderService;
import com.graphai.open.mallorder.entity.IMallOrder;
import com.graphai.open.mallorder.service.IMallOrderZYService;
import com.graphai.open.mallorder.service.impl.MallOrderZYServiceImpl;
import com.graphai.open.mallordergoods.entity.IMallOrderGoods;
import com.graphai.open.mallordergoods.service.IMallOrderGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 代理端接口
 *
 * @author longx
 * @date 2021/7/2811:24
 */

@RestController
@RequestMapping("/wx/agent")
@Validated
public class WxAgentController {
    @Autowired
    private IMallMerchantService mallMerchantService;
    @Autowired
    private IMallUserMerchantService mallUserMerchantService;
    @Autowired
    private MallCollectService mallCollectService;
    @Autowired
    private MallOrderService mallOrderService;
    @Autowired
    private IMallPurchaseOrderService mallPurchaseOrderService;
    @Autowired
    private MallGoodsService mallGoodsService;
    @Autowired
    private WxOrderService wxOrderService;
    @Autowired
    private MallOrderGoodsService mallOrderGoodsService;
    @Autowired
    private IMallPurchaseOrderGoodsService mallPurchaseOrderGoodsService;
    @Autowired
    private ShipService shipService;
    @Autowired
    private MallFootprintService mallFootprintService;
    @Autowired
    private IMallLiveRoomProductService mallLiveRoomProductService;
    @Autowired
    private IMallActivityGoodsProductService mallActivityGoodsProductService;
    @Autowired
    private MallCommentService mallCommentService;
    @Autowired
    private MallUserService mallUserService;
    @Autowired
    private AccountService accountService;
    @Autowired
    private MallNoticeService mallNoticeService;
    @Autowired
    private MallAftersaleService mallAftersaleService;
    @Autowired
    private IMallOrderZYService orderZYService;
    @Autowired
    private IMallShipCompanyService shipCompanyService;
    @Autowired
    private IMallFootprintService iMallFootprintService;


    /**
     * 获取首页
     *
     * @param userId
     * @return
     */
    @GetMapping("index")
    public Object index(@LoginUser String userId) {
        FcAccount fcAccount = accountService.getUserAccount(userId);
        return ResponseUtil.ok(fcAccount);
    }

    /**
     * 获取商铺详情
     *
     * @param userId
     * @return
     * @throws Exception
     */
    @GetMapping("getMerchangtDetail")
    public Object getMerchangtDetail(@LoginUser String userId) throws Exception {
        MallMerchant mallMerchant = this.getMerchantByUserId(userId);
        return ResponseUtil.ok(mallMerchant);
    }

    /**
     * 修改商户信息
     *
     * @param mallMerchant
     * @return
     */
    @PostMapping("updateMerchantDetail")
    public Object updateMerchantDetail(@RequestBody MallMerchant mallMerchant) {
        mallMerchantService.updateById(mallMerchant);
        return ResponseUtil.ok();
    }

    /**
     * 获取粉丝列表
     *
     * @param userId
     * @return
     */
    @GetMapping("getFans")
    public Object getFans(@LoginUser String userId, @RequestParam(defaultValue = "1") Integer page
            , @RequestParam(defaultValue = "10") Integer limit) throws Exception {
        List<MallUser> mallUsers = mallUserService.getUserListFans(userId, page, limit);
        List<AgentUserVo> agentUserVos = new ArrayList<>();
        for (MallUser temp : mallUsers) {
            List<String> ids = new ArrayList<>();
            ids.add(temp.getId());
            List<MallOrder> mallOrders = mallOrderService.findByUserIds(ids);

            BigDecimal price = BigDecimal.ZERO;
            for (MallOrder orderTemp : mallOrders) {
                price = price.add(orderTemp.getActualPrice());
            }
            AgentUserVo agentUserVo = new AgentUserVo();
            agentUserVo.setId(temp.getId());
            agentUserVo.setNickName(temp.getNickname());
            agentUserVo.setDate(temp.getAddTime());
            agentUserVo.setImg(temp.getAvatar());
            agentUserVo.setCnt(mallOrders.size());
            agentUserVo.setPrice(price);
            agentUserVo.setMobile(temp.getMobile());
            agentUserVos.add(agentUserVo);
        }

        Map<String, Object> map = new HashMap<>();
        map.put("total", agentUserVos.size());
        map.put("agentUserVos", agentUserVos);
        return ResponseUtil.ok(map);
    }

    /**
     * 获取销售订单列表
     *
     * @param userId
     * @param showType
     * @param page
     * @param limit
     * @return
     * @throws Exception
     */
    @GetMapping("getOrderList")
    public Object getOrderList(@LoginUser String userId, String order, Integer showType, @RequestParam(defaultValue = "1") Integer page
            , @RequestParam(defaultValue = "10") Integer limit) throws Exception {
        // 销售订单
        Object mallOrders = wxOrderService.list(userId,order, showType, null, page, limit,"agent");

        return ResponseUtil.ok(mallOrders);
    }

    /**
     * 获取采购订单列表
     *
     * @param userId
     * @param showType
     * @param page
     * @param limit
     * @return
     * @throws Exception
     */
    @GetMapping("getPurchaseOrderList")
    public Object getPurchaseOrderList(@LoginUser String userId,String order, Integer showType, @RequestParam(defaultValue = "1") Integer page
            , @RequestParam(defaultValue = "10") Integer limit) throws Exception {
        // 采购订单
        Object mallPurchaseOrders = mallPurchaseOrderService.list(userId,order, showType,null, page, limit,"agent");
        return ResponseUtil.ok(mallPurchaseOrders);
    }

    /**
     * 获取订单详情
     *
     * @param userId
     * @param type   0：普通订单；1：采购订单
     * @return
     */
    @GetMapping("getOrderDetail")
    public Object getOrderDetail(@LoginUser String userId, @RequestParam(defaultValue = "0") Integer type, String orderId) {
        Map<String, Object> map = new HashMap<>();
        if (type == 0) {
            MallOrder mallOrder = mallOrderService.findById(orderId);
            List<MallOrderGoods> mallOrderGoods = mallOrderGoodsService.queryByOid(orderId);
            MallUser mallUser = mallUserService.queryByUserId(mallOrder.getUserId());
            if (mallOrder.getOrderStatus().equals(OrderUtil.STATUS_SHIP)) {
                // ExpressInfo ei = expressService.getExpressInfo(order.getShipChannel(), order.getShipSn());
                Object object = shipService.getLogisticsInfo(mallOrder.getShipSn(), mallOrder.getShipCode());
                map.put("expressInfo", object);
            }

            // 售后信息
            List<Map<String, Object>> goodsMap = new ArrayList<>();
            for (MallOrderGoods temp : mallOrderGoods) {
                Map<String, Object> tempMap = new HashMap<>();
                MallAftersale mallAftersale = mallAftersaleService.findByOrderIdAndOrderGoodsId(temp.getOrderId(),temp.getGoodsId());
                tempMap.put("orderGoods", temp);
                tempMap.put("mallAftersale", mallAftersale);
                goodsMap.add(tempMap);
            }
            MallMerchant mallMerchant = mallMerchantService.getById(mallOrder.getMerchantId());

            map.put("mallUser", mallUser);
            map.put("mallOrder", mallOrder);
            map.put("mallMerchant", mallMerchant);
            map.put("goodsMap", goodsMap);
        } else {
            MallPurchaseOrder mallOrder = mallPurchaseOrderService.getById(orderId);
            QueryWrapper<MallPurchaseOrderGoods> mallPurchaseOrderGoodsQueryWrapper = new QueryWrapper<MallPurchaseOrderGoods>();
            mallPurchaseOrderGoodsQueryWrapper.eq("order_id", orderId);
            List<MallPurchaseOrderGoods> mallOrderGoods = mallPurchaseOrderGoodsService.list(mallPurchaseOrderGoodsQueryWrapper);
            MallUser mallUser = mallUserService.queryByUserId(mallOrder.getUserId());
            if (mallOrder.getOrderStatus().equals(OrderUtil.STATUS_SHIP.intValue())) {
                // ExpressInfo ei = expressService.getExpressInfo(order.getShipChannel(), order.getShipSn());
                Object object = shipService.getLogisticsInfo(mallOrder.getShipSn(), mallOrder.getShipCode());
                map.put("expressInfo", object);
            }

            // 售后信息
            List<Map<String, Object>> goodsMap = new ArrayList<>();
            for (MallPurchaseOrderGoods temp : mallOrderGoods) {
                Map<String, Object> tempMap = new HashMap<>();
                MallAftersale mallAftersale = mallAftersaleService.findByOrderIdAndOrderGoodsId(temp.getOrderId(),temp.getGoodsId());
                tempMap.put("orderGoods", temp);
                tempMap.put("mallAftersale", mallAftersale);
                goodsMap.add(tempMap);
            }
            MallMerchant mallMerchant = mallMerchantService.getById(mallOrder.getMerchantId());

            map.put("mallUser", mallUser);
            map.put("mallMerchant", mallMerchant);
            map.put("mallOrder", mallOrder);
            map.put("goodsMap", goodsMap);
        }

        return ResponseUtil.ok(map);
    }

    /**
     * 获取浏览记录
     *
     * @param userId
     * @param page
     * @param limit
     * @return
     * @throws Exception
     */
    @GetMapping("/getFootprint")
    public Object getFootprint(@LoginUser String userId, @RequestParam(defaultValue = "1") Integer page
            , @RequestParam(defaultValue = "10") Integer limit, String name) throws Exception {
        MallMerchant mallMerchant = this.getMerchantByUserId(userId);


        List<MallFootprint> mallFootprints = new ArrayList<>();
        List<AgentFootprintVo> agentFootprintVos = new ArrayList<>();
        List<MallGoods> goods = null;

        String level = mallUserService.findById(userId).getLevelId();

        if("98".equals(level)) {
            mallFootprints = mallFootprintService.findByMerchantIdAndName(mallMerchant.getId(),userId,null, name, page, limit);
        }else{
            mallFootprints = mallFootprintService.findByFactoryIdAndName(mallMerchant.getId(),userId,null, name, page, limit);
        }

        for (MallFootprint temp : mallFootprints) {
            MallGoods mallGoods = mallGoodsService.findById2(temp.getGoodsId());
            MallUser mallUser = mallUserService.queryByUserId(temp.getUserId());
            if(ObjectUtil.isNull(mallUser) || ObjectUtil.isEmpty(mallUser)){
                break;
            }
            if("0".equals(mallUser.getLevelId()) && "98".equals(level)){
                //如果是普通用户则过滤
                continue;
            }
            if(mallGoods == null){
                continue;
            }
            AgentFootprintVo agentFootprintVo = new AgentFootprintVo();
            MallUserMerchant mallUserMerchant = mallUserMerchantService.getByUserId(temp.getUserId());
            if(ObjectUtil.isNotEmpty(mallUserMerchant)){
                MallMerchant mallMerchant1 = mallMerchantService.getById(mallUserMerchant.getMerchantId());
                agentFootprintVo.setMallMerchant(mallMerchant1);
            }
            agentFootprintVo.setMallFootprint(temp);
            agentFootprintVo.setMallGoods(mallGoods);

            agentFootprintVo.setMallUser(mallUser);
            agentFootprintVos.add(agentFootprintVo);

        }

        return ResponseUtil.ok(agentFootprintVos);
    }

    @Autowired
    private MallGoodsProductService productService;

    /**
     * 获取商品
     *
     * @param userId
     * @param categoryId
     * @param name
     * @return
     * @throws Exception
     */
    @GetMapping("getGoods")
    public Object getGoods(@LoginUser String userId, String categoryId, String name) throws Exception {
        MallMerchant mallMerchant = this.getMerchantByUserId(userId);
        List<MallGoods> mallGoods = mallGoodsService.findByMerchantId(mallMerchant.getId(), categoryId, name);
        for(MallGoods item : mallGoods){
            item.setProduct(productService.queryByGid(item.getId()));
        }
        return ResponseUtil.ok(mallGoods);
    }

    /**
     * 修改商品状态
     *
     * @param mallGoods
     * @return
     */
    @PostMapping("updateGoodsStatus")
    public Object UpdateGoodsStatus(@RequestBody MallGoods mallGoods) {
        if (mallGoods.getIsOnSale() == false) {
            // 下架
            mallGoodsService.updateById(mallGoods);
            // 如果活动商品存在商品则清除
            // 直播间商品
            QueryWrapper<MallLiveRoomProduct> mallLiveRoomProductQueryWrapper = new QueryWrapper<>();
            mallLiveRoomProductQueryWrapper.eq("goods_id", mallGoods.getId());
            mallLiveRoomProductService.remove(mallLiveRoomProductQueryWrapper);

            // 活动关联产品规格
            QueryWrapper<MallActivityGoodsProduct> mallActivityGoodsProductQueryWrapper = new QueryWrapper<>();
            mallActivityGoodsProductQueryWrapper.eq("goods_id", mallGoods.getId());
            mallActivityGoodsProductService.remove(mallActivityGoodsProductQueryWrapper);
        } else {
            MallGoods tempMallGoods = mallGoodsService.findById(mallGoods.getId());

            // 管理员已通过审核
            if (tempMallGoods.getMerchantNoSale() == 3) {
                tempMallGoods.setIsOnSale(true);
            } else {
                tempMallGoods.setMerchantNoSale(1);
            }
            mallGoodsService.updateById(tempMallGoods);
        }
        return ResponseUtil.ok();
    }

    /**
     * 删除商品
     *
     * @param mallGoods
     * @return
     */
    @PostMapping("removeGoods")
    public Object removeGoods(@RequestBody MallGoods mallGoods) {
        mallGoodsService.delete(mallGoods.getId());
        return ResponseUtil.ok();
    }

    /**
     * 获取评论
     *
     * @param userId
     * @param name
     * @param type   0:全部；1：好评；2：差评
     * @param page
     * @param limit
     * @return
     * @throws Exception
     */
    @GetMapping("getComment")
    public Object getComment(@LoginUser String userId, String name, Integer type, @RequestParam(defaultValue = "1") Integer page
            , @RequestParam(defaultValue = "10") Integer limit) throws Exception {
        MallMerchant mallMerchant = this.getMerchantByUserId(userId);

        List<String> ids = null;
        if (StringUtils.isNotEmpty(name)) {
            List<MallGoods> mallGoods = mallGoodsService.findByParams(mallMerchant.getId(), name);
            if (mallGoods.size() <= 0) {
                return ResponseUtil.ok("商品不存在");
            }
            ids = mallGoods.stream().map(MallGoods::getId).collect(Collectors.toList());
        }

        List<AgentCommentVo> agentCommentVos = new ArrayList<>();

        MallComment mallComment = new MallComment();
        mallComment.setMerchantId(mallMerchant.getId());
        List<MallComment> mallComments = mallCommentService.selectByAgent(ids, mallComment, type, page, limit);
        for (MallComment temp : mallComments) {
            MallGoods mallGoods = mallGoodsService.findById(temp.getValueId());
            List<MallComment> reply = mallCommentService.getReply(mallMerchant.getId(), temp.getId());

            AgentCommentVo agentCommentVo = new AgentCommentVo();
            agentCommentVo.setMallComment(temp);
            agentCommentVo.setMallGoods(mallGoods);
            agentCommentVo.setReplyMallComment(reply);
            agentCommentVos.add(agentCommentVo);
        }

        Map<String, Object> map = new HashMap<>();
        long allCnt = mallCommentService.getCnt(ids, mallMerchant.getId(), 0);
        long goodCnt = mallCommentService.getCnt(ids, mallMerchant.getId(), 1);
        long badCnt = mallCommentService.getCnt(ids, mallMerchant.getId(), 2);
        map.put("allCnt", allCnt);
        map.put("goodCnt", goodCnt);
        map.put("badCnt", badCnt);
        map.put("agentCommentVos", agentCommentVos);
        return ResponseUtil.ok(map);
    }

    /**
     * 删除评论
     *
     * @param mallComment
     * @return
     */
    @PostMapping("deleteComment")
    public Object deleteComment(@RequestBody MallComment mallComment) {
        mallCommentService.deleteById(mallComment.getId());
        return ResponseUtil.ok();
    }

    /**
     * 回复评论
     *
     * @param params
     * @return
     */
    @PostMapping("replyComment")
    public Object replyComment(@LoginUser String userId, @RequestBody MallComment params) throws Exception {
        MallMerchant mallMerchant = this.getMerchantByUserId(userId);
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        if (StringUtils.isEmpty(params.getValueId()) || StringUtils.isEmpty(params.getContent())) {
            return ResponseUtil.badArgument();
        }

        MallComment comment = new MallComment();
        comment.setValueId(params.getValueId());
        comment.setContent(params.getContent());
        comment.setUserId(userId);
        comment.setType((byte) 4);
        comment.setMerchantId(mallMerchant.getId());

        MallUser user = mallUserService.findById(userId);
        comment.setUserAvatar(user.getAvatar());

        // nickname -> mobile -> username 按次序拿到非空值, 然后判断值是否是1开头11位数字的手机号, 如果是则脱敏
        String userName = mallMerchant.getName();
        comment.setUserName(userName);

        MallComment reply = mallCommentService.selectOneById(params.getValueId());
        if (reply != null) {
            MallComment reply2 = mallCommentService.selectOneById(reply.getValueId());
            if (reply2 != null) {
                // 目前是做成只有两阶评论, a评论下回复的回复, 回复的回复的回复...都挂在a下面
                comment.setValueId(reply2.getId());
            }
        }
        return mallCommentService.save(comment) > 0 ? ResponseUtil.ok() : ResponseUtil.fail();
    }

    /**
     * 获取消息通知
     *
     * @param userId
     * @return
     */
    @GetMapping("getNotify")
    public Object getNotify(@LoginUser String userId) {
        List<MallNotice> mallNotices = mallNoticeService.queryNoticesByUserIdType(userId, null);
        return ResponseUtil.ok(mallNotices);
    }

    /**
     * 根据用户id获取商户
     *
     * @param userId
     * @return
     */
    private MallMerchant getMerchantByUserId(String userId) throws Exception {
        if (StringUtils.isEmpty(userId)) {
            throw new Exception("用户未登录");
        }
        // 用户商户关联表
        QueryWrapper<MallUserMerchant> mallUserMerchantWrapper = new QueryWrapper<MallUserMerchant>();
        mallUserMerchantWrapper.eq("user_id", userId);
        List<MallUserMerchant> mallUserMerchant = mallUserMerchantService.list(mallUserMerchantWrapper);

        MallMerchant mallMerchant = null;
        for (MallUserMerchant temp : mallUserMerchant) {
            // 商户信息
            QueryWrapper<MallMerchant> merchantWrapper = new QueryWrapper<MallMerchant>();
            merchantWrapper.eq("status", 0);// 正常营业
            merchantWrapper.eq("audit_status", 1);// 审核通过
            merchantWrapper.eq("id", temp.getMerchantId());
            mallMerchant = mallMerchantService.getOne(merchantWrapper);

            if (mallMerchant != null) {
                break;
            }
        }

        if (mallMerchant == null) {
            throw new Exception("没该用户商户信息");
        }
        return mallMerchant;
    }

    /**
     * 发货
     */
    @GetMapping("/shipments")
    public Object shipments(@RequestParam String orderId, @RequestParam String type,
                            @RequestParam String shipSn, @RequestParam String shipCode, @RequestParam String shipChannel) {
        LocalDateTime currentTime = LocalDateTime.now();
        //发货
        if ("0".equals(type)) {
            //采购
            MallPurchaseOrder purchaseOrder = mallPurchaseOrderService.getById(orderId);
            if (ObjectUtil.isNotEmpty(purchaseOrder)) {
                if (OrderUtil.STATUS_PAY.equals(purchaseOrder.getOrderStatus().shortValue())) {
                    purchaseOrder.setOrderStatus(OrderUtil.STATUS_SHIP.intValue());
                }
                purchaseOrder.setShipSn(shipSn);
                purchaseOrder.setShipCode(shipCode);
                purchaseOrder.setShipChannel(shipChannel);
                purchaseOrder.setShipTime(currentTime);
                mallPurchaseOrderService.updateById(purchaseOrder);
                return ResponseUtil.ok();
            }
            return ResponseUtil.orderArgument();
        }
        IMallOrder order = orderZYService.getById(orderId);
        if (ObjectUtil.isNotEmpty(order)) {
            if (OrderUtil.STATUS_PAY.equals(order.getOrderStatus().shortValue())) {
                order.setOrderStatus(OrderUtil.STATUS_SHIP.intValue());
            }
            //发货时间
            order.setShipTime(currentTime);
            order.setShipSn(shipSn);
            order.setShipCode(shipCode);
            order.setShipChannel(shipChannel);
            orderZYService.updateById(order);
            return ResponseUtil.ok();
        }
        return ResponseUtil.orderArgument();
    }

    /**
     * 收货
     */
    @GetMapping("/receipt")
    public Object receipt(@RequestParam String orderId, @RequestParam String type) {
        LocalDateTime currentTime = LocalDateTime.now();
        // 采购订单
        if ("0".equals(type)) {
            MallPurchaseOrder purchaseOrder = mallPurchaseOrderService.getById(orderId);
            // 如果是待收货状态
            if (OrderUtil.STATUS_SHIP.equals(purchaseOrder.getOrderStatus().shortValue())) {
                purchaseOrder.setOrderStatus(OrderUtil.STATUS_CONFIRM.intValue());
            }
            purchaseOrder.setConfirmTime(currentTime);
            purchaseOrder.setUpdateTime(currentTime);
            mallPurchaseOrderService.updateById(purchaseOrder);
        }else{
            //销售订单
            IMallOrder order = orderZYService.getById(orderId);
            //未申请过售后
            if (order.getOrderStatus().equals(OrderUtil.STATUS_SHIP.intValue())) {
                order.setOrderStatus(OrderUtil.STATUS_CONFIRM.intValue());
            }
            order.setConfirmTime(currentTime);
            order.setUpdateTime(currentTime);
            orderZYService.updateById(order);
        }
        return ResponseUtil.ok();
    }

    //修改价格
    @GetMapping("/modifyPrice")
    public Object modifyPrice(@RequestParam String orderId, @RequestParam String type, @RequestParam BigDecimal orderPrice) {
        LocalDateTime currentTime = LocalDateTime.now();
        if ("0".equals(type)) {
            MallPurchaseOrder purchaseOrder = mallPurchaseOrderService.getById(orderId);
            purchaseOrder.setOrderPrice(orderPrice);
            // 重新计算支付金额
            BigDecimal actualPrice = orderPrice.subtract(purchaseOrder.getCouponPrice()).subtract(purchaseOrder.getIntegralPrice());
            purchaseOrder.setActualPrice(actualPrice);
            purchaseOrder.setRemainingRefundAmount(actualPrice);

            purchaseOrder.setUpdateTime(currentTime);
            purchaseOrder.setModifyPrice(true);

//            QueryWrapper<MallPurchaseOrderGoods> queryWrapper = new QueryWrapper<>();
//            queryWrapper.eq("order_id",purchaseOrder.getId());
//            MallPurchaseOrderGoods mallPurchaseOrderGoods = mallPurchaseOrderGoodsService.list(queryWrapper).get(0);
//            MallGoods mallGoods = mallGoodsService.findById(mallPurchaseOrderGoods.getGoodsId());
//            //重新计算分润金额
//            BigDecimal pubSharePreFee = purchaseOrder.getActualPrice().multiply(mallGoods.getFirstRatio().divide(BigDecimal.valueOf(100)));

            mallPurchaseOrderService.updateById(purchaseOrder);
        } else {
            IMallOrder order = orderZYService.getById(orderId);
            order.setOrderPrice(orderPrice);
            // 重新计算支付金额
            BigDecimal actualPrice = orderPrice.subtract(order.getCouponPrice()).subtract(order.getIntegralPrice());
            order.setActualPrice(actualPrice);
            order.setRemainingRefundAmount(actualPrice);

            order.setTicket("(商家改价)");
            order.setUpdateTime(currentTime);

            MallOrderGoods mallPurchaseOrderGoods = mallOrderGoodsService.queryByOid(orderId).get(0);
            MallGoods mallGoods = mallGoodsService.findById(mallPurchaseOrderGoods.getGoodsId());
            //重新计算分润金额
            BigDecimal pubSharePreFee = order.getActualPrice().multiply(mallGoods.getFirstRatio().divide(BigDecimal.valueOf(100)));
            order.setPubSharePreFee(pubSharePreFee.toString());

            orderZYService.updateById(order);
        }
        return ResponseUtil.ok();
    }

    /**
     * 修改收货地址
     *
     * @param mallOrder
     * @return
     */
    @PostMapping("/updateAddress")
    public Object updateAddress(@RequestBody MallOrder mallOrder) {
        MallOrder tempMallOrder = mallOrderService.findById(mallOrder.getId());
        if (tempMallOrder == null) {
            MallPurchaseOrder mallPurchaseOrder = mallPurchaseOrderService.getById(mallOrder.getId());
            mallPurchaseOrder.setConsignee(mallOrder.getConsignee());
            mallPurchaseOrder.setMobile(mallOrder.getMobile());
            mallPurchaseOrder.setAddress(mallOrder.getAddress());
            mallPurchaseOrderService.updateById(mallPurchaseOrder);
        } else {
            tempMallOrder.setConsignee(mallOrder.getConsignee());
            tempMallOrder.setMobile(mallOrder.getMobile());
            tempMallOrder.setAddress(mallOrder.getAddress());
            mallOrderService.updateByExample(tempMallOrder);
        }
        return ResponseUtil.ok();
    }

    /**
     * 取消订单
     *
     * @param userId
     * @param mallOrder
     * @return
     */
    @PostMapping("cancelOrder")
    public Object cancelOrder(@LoginUser String userId, @RequestBody MallOrder mallOrder) {
        if (mallOrder.getOrderType() == 0) {
            MallOrder tempMallOrder = mallOrderService.findById(mallOrder.getId());
            tempMallOrder.setOrderStatus(OrderUtil.STATUS_CANCEL);
            mallOrderService.updateByExample(tempMallOrder);
        } else {
            MallPurchaseOrder tempMallOrder = mallPurchaseOrderService.getById(mallOrder.getId());
            tempMallOrder.setOrderStatus(OrderUtil.STATUS_CANCEL.intValue());
            mallPurchaseOrderService.updateById(tempMallOrder);
        }
        return ResponseUtil.ok();
    }

    /**
     * 获取物流公司
     */
    @GetMapping("/getLogisticsCompany")
    public Object getLogisticsCompany(){
        return ResponseUtil.ok(shipCompanyService.list());
    }
}
