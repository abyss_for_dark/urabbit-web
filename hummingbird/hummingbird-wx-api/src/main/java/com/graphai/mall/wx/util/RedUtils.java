package com.graphai.mall.wx.util;

import com.alipay.api.CertAlipayRequest;
import lombok.extern.slf4j.Slf4j;


@Slf4j(topic = "redUtil")
public class RedUtils extends Thread{

    public static CertAlipayRequest redCode(String type){
        //构造client
        CertAlipayRequest certAlipayRequest = new CertAlipayRequest();
        //设置网关地址
        certAlipayRequest.setServerUrl("https://openapi.alipay.com/gateway.do");
        //设置应用Id
        if("1".equals(type)){
            certAlipayRequest.setAppId("2021002114615168");//红包应用
            //设置应用私钥
            certAlipayRequest.setPrivateKey("MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCt97m6vJAOjGBnhHtyMh7r7IMFP0h69ckqoY9iYZaPOfZMvcQZyP4rOO2JQFyY0rgPXIPz7NLQLRIWIBkokc39pELHWcq5CmkeodjL4cB0y05592brkP29bmR3/DW1bnwiDnqyQyDdYagapKpPBOPGPBi+hiUg4c5SN/jnvCt6KiCqGXAMTekvvWiPKy3O3M9hBs3coTOEl1JlkKlgke49OiOqoAkC6j8K6hZ2o181jypVp13UPL4hmJfTTw2GCF85Vo/yFfKf/n39SAVTSwb0c1Z7BvR1EoW01Wc6avBPijQ5R1bP4ry2Klrq6BgRVU9/rU8X2dIxnm5CQD0EopWfAgMBAAECggEAGlR3ENM/PmRyptg/Us5RLcXzIpUsZCfVFZW0POCmR/Eu3PiPuJNaHLKWCQLrwNmLsA+BjSaX1Zgst5qOFDzVH4F/9eoaK51uwYsfY+Q8G7E1pHIUaqzcKpKNtDdA7b/WG9VzLo3IaJlCNx4bBLCpxLQNzrBA6FQkxP7htjkdn3SGtwwEvJ1ocmQ4C6RT8rQubhisSdqYszwH3rRkdbZOgTmMLjL8nC8qXX0eb9XKkcQfR/zz8OSJVI3pehMv26NPkkNkUi9FIlTgzcsKoMWWRGlI5ZfcfCXhKGzO17fnYzzW7nPYFSf9yRtyy38DVS+zMw9SVnfcW/s93vN0HJqGsQKBgQDlp4kwZkvFXqet1vPxT4ILYsNhpHLDLwGLH8CDT+2UODpSyFJEWjs4VnQv4qIg1WMLLFeXhevG8TrufQUgO6iJtCXlR+p6EXvFnXVOVegIayUk1kOku6N+kgtpC5vfnb9XVRaf3AvOb4h3cubjl7+OFZFa8d+2Uo20cbC0zECsVwKBgQDB7MmaF8VbpZrxxh4Bcg8+rqpkE3VFNjVNLUmnScoNldAB0xJ2w0XXEgGA+BYjd99+UQaU/SJtFRRWT0cDIScm6D8G+AU9TzNhMybQzbzVp6SEaph3ZIMz4YTRVsSO5amrBU8hNVdOdrVnGKvIC8RA5D//qiPYuGNK9bejREOT+QKBgDeq6rhOs6M+FIsxBaTCzxUPdajfgeoKJOeiZLnvz/kTwB9862QiMySu4C8TUpV+5Ck7MGhMUCDDLLPOnkV38JygjGcNjarVPhli16KLdFlGD5TkUzVFPpZZtGZcKAaGaHE08O0DG+GIa534vjsA96/rTnarFr1NOYg53smUkUC1AoGANXpwKi9t/JURwqTo92sV6ypN0OVQzLJjRpI7ooeF0fNDdo1XpSq6otHTWlaP/F39Yx9DGmkoSj6q6hZYtfM5kdAwOWLCRyMFP0PU63YhIiZmByJCQ4KTcAY8E2LDN8j73i7LoJPOLNQtuG2kSbl8+Iw5Xe70jbpjn3NdngegPzkCgYBaJ4SvCgUIR3m7fVhnAPAfHlrBinrW5b6kXwHAvira3jzVSc/KGT9wcc7IfqValWCyRPA/xRiNCMWJCG3InirxVvdNj3Lf9Tfa40wj0NUuA/M5ykMqQ3ZrBnpYFBPVLOJicV2+7+BIr8tLDC43HqKWFTxqWblUIG2ikZFgei1Qiw==");
            //设置应用公钥证书路径
//            String file_path = "usr"+ File.separator+"local"+File.separator+"nginx"+File.separator+"conf"+File.separator+"cert"+File.separator+"zfb1"+File.separator+"appCertPublicKey_2021002114615168.crt";
            certAlipayRequest.setCertPath("/usr/local/nginx/conf/cert/zfb1/appCertPublicKey_2021002114615168.crt");
            //设置支付宝根证书路径
           certAlipayRequest.setRootCertPath("/usr/local/nginx/conf/cert/zfb1/alipayRootCert.crt");
            //设置支付宝公钥证书路径
           certAlipayRequest.setAlipayPublicCertPath("/usr/local/nginx/conf/cert/zfb1/alipayCertPublicKey_RSA2.crt");
//            certAlipayRequest.setCertPath("C:\\Users\\Administrator.SC-202011302240\\Documents\\支付宝开放平台开发助手\\未来红包应用证书\\应用公钥证书\\appCertPublicKey_2021002114615168.crt");
//            certAlipayRequest.setRootCertPath("C:\\Users\\Administrator.SC-202011302240\\Documents\\支付宝开放平台开发助手\\未来红包应用证书\\支付宝根证书\\alipayRootCert.crt");
//            certAlipayRequest.setAlipayPublicCertPath("C:\\Users\\Administrator.SC-202011302240\\Documents\\支付宝开放平台开发助手\\未来红包应用证书\\支付宝公钥证书\\alipayCertPublicKey_RSA2.crt");
        }else{
            certAlipayRequest.setAppId("2019103168815177");//酒店应用
            ////设置应用私钥
             certAlipayRequest.setPrivateKey("MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCFUb/Xvk1E59ThAK6BEBc6j+b3p/iH0ouCislF92l/6YvbAJ+WeeezeQm3yKxTuJMBc8XZjhUEM7Y80Vb3ki23doBth52KJ/xvayaKvUvHzM12VC1VQbA0ZPgCOCoUAMKR57pAc8cg8+2Srfbq0lSsN3pt239+sYHNBpv+Zd0lUmwLCBp1WMb4QQmvOyi6qCIZXihKAwFEmXpQGiX6vwFIRieWJ0/TxmTWUcnQN81TRYYOYGmHLCMx8isfgq79LWXkh+kjzAuMWw0kOI6tgGFBpO0Hz6zlfmYEd5Ws3ShAmBNDpsuSXg+TDNIwnWh4sXxMEmEbnZcjtQJTImtrnS4TAgMBAAECggEAOMMQmguVHHRUR6QhqCOwsCdaoDDs+O8kpOTwQxDO4z4NZKYETG4Ev34WLCCNdmvMcz7WiwUaX5qgyY4bp/ad7lHO5lNqOMtDiDCnzURBQoUpKWs3jegU3Insqg09pRyZusNdollo+9e/W8cjrOD2WgiY4LMmuktbR2679flqyZuJ/l7/b9TunGfow+ignlZZfaIxyifTBRJOhSos3ihE/4YQvXZtuEW7W8SnJK8qKTDDeM03wI92xPcgqQTwXayzUu/u3dfbGucWnsvsqgxK/hMxyLiwtC/jf+fFbDD3HS/oDR3xnd3KrxFH74PqpXcKYcGvYkSFMMwd7F4qeS4tsQKBgQDWkjDlIIej9tZdLP6DRRSdeeXxo590BAk5dvmB2nQfWK5OgMVuXVLPX7lJSqXBy0YFUMVZmAc758IHM7gLr2P6nHqsYu/SJe77j7QHuloidRq6xZG2rVF3LgAa30O8Im8VxM8eaT3WZMu4V6UMPKpK6w0AFKrz0uC3Fc67/5b0SQKBgQCfD3OejoywmpUh7W4HUPqYwuxcTKoYDmlyxbBxswMZR3xSVwQkSMtaEjv1T9TwUz6b8ovWIHSzk9yB+ma49/5+j4nEOmiPXAjgeCdkId33lQEHDl+Z7s9FnQfucM2tpEZbJmYkhkNZF2Qz/0Q2+QdQRZju9+ut6zCVjLkx5olXewKBgAV1NH8vg6O9DJjUIyktoaebySiLU+v8ikuG5jPSXa/Kbqyag+sZvph9w9XEa1DxnbdW5Z4wysZQT9QlEaJidLWUM16dKaJ7yodlkVu89rv3pJVpnxo5cGAKsH/+e9eWPKMeHXKVlLgWWkQ3HoCgLmi3b943qF2JWKPvjxGjH7DZAoGALM4wdoCXNwREcCsurFbuUGvQIuvhmictIwx2+IBBpjRR2+vxGbt7tOKgCykyxYPdhhZbt7Sqyw8itWZw2zfYc5RBbYo6e++j/H88MWOs4BoUEQXGflkb//MNEeiqt3gn7XMyoKVFgWoqas9ESuv5WG5SdzZVKEexBt9+J4VWONMCgYAx6/z9jt8O/iPotruGTUBNvdzBPQXuGEuhijux1XK0dLPsjGEIq+2/RrE77YAfJUIqk2pXZo73QW2BKRfiT8N0Gkqy/yh/SIU/3ScyQ16+54GunRjJC/EPPTl9ojyLm2TgKHrw0ypeVxS/JxD7n6K19GHQLzSZs4sqX7HSRMwrrA==");
            ////设置应用公钥证书路径
            certAlipayRequest.setCertPath("/usr/local/nginx/conf/cert/zfb2/appCertPublicKey_2019103168815177.crt");
        //设置支付宝根证书路径
            certAlipayRequest.setRootCertPath("/usr/local/nginx/conf/cert/zfb2/alipayRootCert.crt");
//        //设置支付宝公钥证书路径
            certAlipayRequest.setAlipayPublicCertPath("/usr/local/nginx/conf/cert/zfb2/alipayCertPublicKey_RSA2.crt");
//            certAlipayRequest.setCertPath("D:\\cert\\appCertPublicKey_2019103168815177.crt");
//            certAlipayRequest.setRootCertPath("D:\\cert\\alipayRootCert.crt");
//            certAlipayRequest.setAlipayPublicCertPath("D:\\cert\\alipayCertPublicKey_RSA2.crt");
        }
        //设置请求格式，固定值json
        certAlipayRequest.setFormat("json");
        //设置字符集
        certAlipayRequest.setCharset("utf-8");
        //设置签名类型
        certAlipayRequest.setSignType("RSA2");

        return certAlipayRequest;
    }






}
