package com.graphai.mall.wx.web.common;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.imageio.stream.ImageOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;

import cn.hutool.extra.qrcode.QrCodeUtil;
import cn.hutool.json.JSONNull;
import cn.hutool.json.JSONUtil;
import com.graphai.commons.util.json.JacksonUtil;
import com.graphai.mall.db.constant.CommonConstant;
import com.graphai.mall.db.util.RedpacketUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.graphai.commons.util.AppIdAlgorithm;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.image.GoodsInfoVo;
import com.graphai.commons.util.image.ImageUtils;
import com.graphai.commons.util.json.JacksonUtils;
import com.graphai.framework.storage.StorageService;
import com.graphai.mall.db.domain.MallAppVersion;
import com.graphai.mall.db.domain.MallSelection;
import com.graphai.mall.db.domain.MallStorage;
import com.graphai.mall.db.service.MallInvitationCodeService;
import com.graphai.mall.db.service.common.AppVersionService;
import com.graphai.mall.db.service.promotion.MallSelectionService;
import com.graphai.mall.wx.web.user.behavior.WxAddressController;

import cn.hutool.core.util.ObjectUtil;


@RestController
@RequestMapping("/wx/common")
public class WxCommonController {
    private final Log logger = LogFactory.getLog(WxAddressController.class);

    @Autowired
    private AppVersionService versionService;

    @Autowired
    private StorageService storageService;

    @Autowired
    private MallInvitationCodeService codeService;

    @Resource
    private MallSelectionService mallSelectionService;

    /**
     * 获取APP当前的版本
     * 
     * @param appCode
     * @return
     */
    @GetMapping("/appversion")
    public Object getAppVersion(@RequestParam("appCode") String appCode) {
        if (StringUtils.isEmpty(appCode)) {
            return ResponseUtil.badArgument();
        }
        MallAppVersion androidAppVersion = versionService.getNowAppVersion(appCode, "android");

        MallAppVersion iosAppVersion = versionService.getNowAppVersion(appCode, "ios");
        try {
            logger.info("查询appVersion ：" + JSONUtil.toJsonStr(androidAppVersion));
            logger.info("查询appVersion ：" + JSONUtil.toJsonStr(iosAppVersion));
        } catch (Exception e) {
            logger.error("查询appVersion：" + e.getMessage(), e);
        }

        Map<String, Object> map = new HashMap<String, Object>();
        if (ObjectUtil.isNotNull(androidAppVersion)) {
            map.put("android", androidAppVersion);
        }

        if (ObjectUtil.isNotNull(iosAppVersion)) {
            map.put("ios", iosAppVersion);
        }

        return ResponseUtil.ok(map);
    }

    @RequestMapping(value = "/qrcode")
    public Object getQrcode(String content, HttpServletResponse resp) throws Exception {
        // ServletOutputStream stream = null;

        ByteArrayOutputStream os = new ByteArrayOutputStream();

        try {

            int width = 300;
            int height = 300;
            String format = "png";
            HashMap<Object, Object> map = new HashMap<Object, Object>();
            map.put(EncodeHintType.CHARACTER_SET, "utf-8");
            map.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M);
            map.put(EncodeHintType.MARGIN, 0);
            BitMatrix bm = new MultiFormatWriter().encode(content, BarcodeFormat.QR_CODE, width, height);
            MatrixToImageWriter.writeToStream(bm, format, os);
            InputStream ips = new ByteArrayInputStream(os.toByteArray());
            // 上传到OSS是上传字节的形式
            String fileName = AppIdAlgorithm.generateCode(6).toUpperCase() + System.currentTimeMillis() + ".png";
            MallStorage storage = storageService.store(ips, os.toByteArray().length, format, fileName);

            return ResponseUtil.ok(storage);

        } catch (WriterException e) {
            logger.error("二维码生成失败：" + e.getMessage(), e);

        } finally {
            if (os != null) {
                os.flush();
                os.close();
            }
        }

        return ResponseUtil.ok();

    }

    @RequestMapping(value = "/poster")
    public Object genPoster(String content, HttpServletResponse resp) throws Exception {
        // ServletOutputStream stream = null;

        ByteArrayOutputStream os = new ByteArrayOutputStream();
        try {

            GoodsInfoVo vo = new GoodsInfoVo();
            vo.setPicUrl("http://a.vpimg2.com/upload/merchandise/pdcvis/106864/2019/0603/15/2b67903b-2622-415c-bae3-824f9eeda63b_420_531.jpg");
            vo.setGoodsId("111");
            vo.setHasCoupon("false");
            vo.setRebatePrice(new BigDecimal(1000));
            vo.setOrgPrice(new BigDecimal(1900));
            vo.setTitle("帕丝特 夏装新款妈妈T恤简约纯色短袖套头女士T恤妈妈装中老年女装上衣");
            vo.setPrice(new BigDecimal(1900));

            String filePath = "C:\\02工作文件\\01编程软件\\qweqweqwe.png";

            // return getImageStream(bg);
            BufferedImage bg = ImageUtils.drawCouponPosterImagev2("", 111l, vo, filePath);

            ImageIO.write(bg, "png", os);
            InputStream ips = new ByteArrayInputStream(os.toByteArray());

            String format = "png";
            String fileName = AppIdAlgorithm.generateCode(6).toUpperCase() + System.currentTimeMillis() + ".png";
            MallStorage storage = storageService.store(ips, os.toByteArray().length, format, fileName);

            return ResponseUtil.ok(storage);

        } catch (WriterException e) {
            logger.error("二维码生成失败：" + e.getMessage(), e);

        } finally {
            if (os != null) {
                os.flush();
                os.close();
            }
        }

        return ResponseUtil.ok();

    }

    public static InputStream getImageStream(BufferedImage bimage) {
        InputStream is = null;
        ByteArrayOutputStream bs = new ByteArrayOutputStream();
        ImageOutputStream imOut = null;
        try {
            imOut = ImageIO.createImageOutputStream(bs);
            ImageIO.write(bimage, "png", imOut);
            is = new ByteArrayInputStream(bs.toByteArray());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (imOut != null) {
                    imOut.flush();
                    imOut.close();
                }

                if (bs != null) {
                    bs.flush();
                    bs.close();
                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return is;
    }

    /**
     * 选品活动-共用-配置信息
     * 
     * @param type 01-零元购
     * @return Object
     */
    @GetMapping("/getSelectCommonConf")
    public Object getSelectCommonConf(@NotNull String type) {
        MallSelection mallSelection = mallSelectionService.queryBybak1(type);
        if (mallSelection == null) {
            return ResponseUtil.badArgument();
        } else {
            return ResponseUtil.ok(mallSelection);
        }
    }

    /**
     * 长短链转短链
     *
     * @param data longUrl
     * @return Object
     */
    @PostMapping("/longUrlToShorUrl")
    public Object longUrlToShorUrl(@RequestBody String data) {
        String longUrl = JacksonUtil.parseString(data, "longUrl");
        String type = JacksonUtil.parseString(data, "type");
        if (org.apache.commons.lang.StringUtils.isBlank(longUrl)) {
            type = "weibo";
        }
        Map<String,Object> resultMap = new HashMap<>();
        try {
            if (org.apache.commons.lang.StringUtils.isNotBlank(longUrl)) {
                logger.info("longUrl="+longUrl);
                String shortUrl = RedpacketUtil.getLongToShortUrl(longUrl,type);
                logger.info("shortUrl="+shortUrl);
                if (org.apache.commons.lang.StringUtils.isNotBlank(shortUrl)) {
                    longUrl = shortUrl;
                    resultMap.put("msg","转链成功");
                } else {
                    resultMap.put("shortUrl",longUrl);
                    resultMap.put("msg","转链频繁，请一分钟后再试。");
                }
            }
        } catch (Exception ex) {
            logger.error("转链失败，请一分钟后再试， msg=" + ex.getMessage(), ex); // jsonObject={"msg":"请一分钟后再试","status":500}
            ex.printStackTrace();
        } finally {
            resultMap.put("shortUrl",longUrl);
            return ResponseUtil.ok(resultMap);
        }
    }

    /**
     * 生成二维码图片
     */
    @GetMapping("/getOrcode")
    public Object getQrcode(@RequestParam String url,@RequestParam(defaultValue = "300") Integer width,
                            @RequestParam(defaultValue = "300") Integer height){
        String qrcodImg = CommonConstant.BASE64_PREFIX + Base64.getEncoder().encodeToString(QrCodeUtil.generatePng(url,width, height));
        return ResponseUtil.ok(qrcodImg);
    }

}
