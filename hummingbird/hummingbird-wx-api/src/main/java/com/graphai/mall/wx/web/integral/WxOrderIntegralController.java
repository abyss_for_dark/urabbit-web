package com.graphai.mall.wx.web.integral;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.mall.core.system.SystemConfig;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.mall.wx.service.WxOrderIntegralService;
import com.graphai.mall.wx.web.order.WxOrderController;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;


/**
 * 订单-积分商城
 * @author Qxian
 * @date 2020-10-14
 * @version V0.0.1
 */
@RestController
@RequestMapping("/wx/order/integral")
@Validated
public class WxOrderIntegralController {
	private final Log logger = LogFactory.getLog(WxOrderController.class);

	@Autowired
	private WxOrderIntegralService wxOrderService;

	/**
	 * 订单列表
	 *
	 * @param userId
	 *            用户ID
	 * @param showType
	 *            订单信息
	 * @param page
	 *            分页页数
	 * @param limit
	 *            分页大小
	 * @return 订单列表
	 */
	@GetMapping("list")
	public Object list(@LoginUser String userId, @RequestParam(defaultValue = "0") Integer showType, @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit) {
		return wxOrderService.list(userId, showType, page, limit);
	}

	/**
	 * 订单列表-积分商城
	 * @param mobile【由原来的userId改为mobile，通过手机号码查询用户信息】
	 * @param showBelong
	 *            0看全部，1看分销，2看本人，3屏蔽已取消
	 * @param showType 订单状态
	 * @param page
	 * @param limit
	 * @return
	 */
	@GetMapping("/fenxiao/list")
	public Object fenxiaoList(@RequestParam String mobile, @RequestParam(defaultValue = "0") Integer showBelong,
                              @RequestParam(defaultValue = "0") Integer showType, @RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit) {
		return wxOrderService.fenxiaoList(showBelong, mobile, showType, page, limit);
	}

	/**
	 * 订单详情
	 *
	 * @param mobile
	 *            用户手机号码
	 * @param orderId
	 *            订单ID
	 * @return 订单详情
	 */
	@GetMapping("detail")
	public Object detail(@NotNull String mobile,@NotNull String orderId) {
		return wxOrderService.detail(mobile,orderId);
	}

	/**
	 * 提交订单
	 *
	 * @param body
	 *            订单信息，{ userId，xxx，cartId：xxx, addressId: xxx, couponId: xxx, message:xxx, grouponRulesId: xxx, grouponLinkId: xxx}
	 * @return 提交订单操作结果
	 */
	@PostMapping("submit")
	public Object submit(@RequestBody String body, HttpServletRequest request) {
		return wxOrderService.submit(body,getIpAddress(request));
	}

	/**
	 * 取消订单
	 *
	 * @param body-mobile
	 *            用户手机号码
	 * @param body-orderId
	 *            订单信息，{ orderId：xxx }
	 * @return 取消订单操作结果
	 */
	@PostMapping("cancel")
	public Object cancel(@RequestBody String body) {
		return wxOrderService.cancel(body);
	}

	/**
	 * 付款订单的预支付会话标识
	 *
	 * @param body-userId
	 *            用户ID
	 * @param body
	 *            订单信息，{ orderId：xxx }
	 * @return 支付订单ID
	 */
	@PostMapping("prepay")
	public Object prepay(@RequestBody String body, HttpServletRequest request) {
		return wxOrderService.prepay(body, request);
	}

	/**
	 * 微信付款成功或失败回调接口
	 * <p>
	 * TODO 注意，这里pay-notify是示例地址，建议开发者应该设立一个隐蔽的回调地址
	 *
	 * @param request
	 *            请求内容
	 * @param response
	 *            响应内容
	 * @return 操作结果
	 */
	@PostMapping("pay-notify")
	public Object payNotify(HttpServletRequest request, HttpServletResponse response) {
		return wxOrderService.payNotify(request, response);
	}

	/**
	 * 支付成功充值商城余额和积分接口
	 * <p>
	 * TODO 注意，这里points-recharge-notify是示例地址，建议开发者应该设立一个隐蔽的回调地址
	 *
	 * @param body
	 *            请求内容
	 * @return 操作结果
	 */
	@PostMapping("pointsRechargeNotify")
	public Object pointsRechargeNotify(@RequestBody String body) {

		return wxOrderService.pointsRechargeNotify(body);
	}

	/**
	 * 订单申请退款
	 *
	 * @param userId
	 *            用户ID
	 * @param body
	 *            订单信息，{ orderId：xxx }
	 * @return 订单退款操作结果
	 */
	@PostMapping("refund")
	public Object refund(@LoginUser String userId, @RequestBody String body) {
		try {
			return wxOrderService.refund(userId, body);
		} catch (Exception e) {
			logger.error("订单退单异常 :" + e.getMessage(), e);
		}

		return ResponseUtil.fail();
	}

	/**
	 * 手动退订单退账
	 * 
	 * @param body
	 * @return
	 */
//	@PostMapping("mrefund")
//	public Object mrefund(@RequestBody String body) {
//		try {
//			String orderId = JacksonUtil.parseString(body, "orderId");
//			if (orderId == null) {
//				return ResponseUtil.badArgument();
//			}
//
//			return wxOrderService.manualRefund(orderId);
//		} catch (Exception e) {
//			logger.error("订单退单异常 :" + e.getMessage(), e);
//		}
//		return ResponseUtil.fail();
//	}

	/**
	 * 确认收货
	 *
	 * @param body
	 *            订单信息，{ orderId：xxx，mobile:xxx }
	 * @return 订单操作结果
	 */
	@PostMapping("confirm")
	public Object confirm(@RequestBody String body) {
		return wxOrderService.confirm(body);
	}

	/**
	 * 删除订单
	 *
	 * @param userId
	 *            用户ID
	 * @param body
	 *            订单信息，{ orderId：xxx }
	 * @return 订单操作结果
	 */
	@PostMapping("delete")
	public Object delete(@LoginUser String userId, @RequestBody String body) {
		return wxOrderService.delete(userId, body);
	}

	/**
	 * 待评价订单商品信息
	 *
	 * @param userId
	 *            用户ID
	 * @param orderId
	 *            订单ID
	 * @param goodsId
	 *            商品ID
	 * @return 待评价订单商品信息
	 */
	@GetMapping("goods")
	public Object goods(@LoginUser String userId, @NotNull String orderId, @NotNull String goodsId) {
		return wxOrderService.goods(userId, orderId, goodsId);
	}

	/**
	 * 评价订单商品
	 *
	 * @param userId
	 *            用户ID
	 * @param body
	 *            订单信息，{ orderId：xxx }
	 * @return 订单操作结果
	 */
	@PostMapping("comment")
	public Object comment(@LoginUser String userId, @RequestBody String body) {
		return wxOrderService.comment(userId, body);
	}

//	@PostMapping("supplement-sheet")
//	public Object supplementSheet(@RequestBody String body) {
//		wxOrderService.supplementSheet(body);
//		return ResponseUtil.ok("计算成功");
//	}

//	@PostMapping("supplement-sheet22")
//	public Object supplementSheet2(@RequestBody String body) {
//		String query =
//				"198234019627180034,";
//
//		List<String> list = Arrays.asList(query.split(","));
//
//		try {
//			InternalProtocolMsg<Object> msg = wxOrderService.sheet(list);
//			System.out.println(JacksonUtils.bean2Jsn(msg));
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return ResponseUtil.ok("计算成功");
//	}

	/**
	 * 支付返积分比例
	 * @return 支付返积分比例
	 */
	@GetMapping("orderBonusPoints")
	public Object orderBonusPoints() {
		BigDecimal orderBonusPoints = SystemConfig.getLitemallOrderBonusPoints();
		if(orderBonusPoints == null){
			orderBonusPoints = new BigDecimal("1");
		}
		return ResponseUtil.ok(orderBonusPoints);
	}

	/**
	 * 获取用户真实IP地址，不使用request.getRemoteAddr();的原因是有可能用户使用了代理软件方式避免真实IP地址,
	 * 
	 * 可是，如果通过了多级反向代理的话，X-Forwarded-For的值并不止一个，而是一串IP值，究竟哪个才是真正的用户端的真实IP呢？
	 * 答案是取X-Forwarded-For中第一个非unknown的有效IP字符串。
	 * 
	 * 如：X-Forwarded-For：192.168.1.110, 192.168.1.120, 192.168.1.130,
	 * 192.168.1.100
	 * 
	 * 用户真实IP为： 192.168.1.110
	 * 
	 * @param request
	 * @return
	 */
	public static String getIpAddress(HttpServletRequest request) {
		String ip = request.getHeader("x-forwarded-for");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_CLIENT_IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_X_FORWARDED_FOR");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		return ip;
	}

}