package com.graphai.mall.wx.web.auth;

import static com.graphai.mall.wx.util.WxResponseCode.AUTH_CAPTCHA_FREQUENCY;
import static com.graphai.mall.wx.util.WxResponseCode.AUTH_CAPTCHA_UNMATCH;
import static com.graphai.mall.wx.util.WxResponseCode.AUTH_CAPTCHA_UNSUPPORT;
import static com.graphai.mall.wx.util.WxResponseCode.AUTH_INVALID_ACCOUNT;
import static com.graphai.mall.wx.util.WxResponseCode.AUTH_INVALID_MOBILE;
import static com.graphai.mall.wx.util.WxResponseCode.AUTH_MOBILE_REGISTERED;
import static com.graphai.mall.wx.util.WxResponseCode.AUTH_MOBILE_UNREGISTERED;
import static com.graphai.mall.wx.util.WxResponseCode.AUTH_NAME_REGISTERED;
import static com.graphai.mall.wx.util.WxResponseCode.AUTH_OPENID_BINDED;
import static com.graphai.mall.wx.util.WxResponseCode.AUTH_OPENID_UNACCESS;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.CompletableFuture;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import cn.binarywang.wx.miniapp.api.impl.WxMaServiceImpl;
import cn.binarywang.wx.miniapp.bean.WxMaUserInfo;
import cn.binarywang.wx.miniapp.config.impl.WxMaDefaultConfigImpl;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.graphai.commons.constant.MallConstant;
import com.graphai.commons.util.*;
import com.graphai.mall.admin.domain.MallMerchant;
import com.graphai.mall.admin.domain.MallUserMerchant;
import com.graphai.mall.admin.service.IMallMerchantService;
import com.graphai.mall.admin.service.IMallUserMerchantService;
import com.graphai.mall.db.constant.user.MallUserEnum;
import com.graphai.mall.db.domain.*;
import com.graphai.mall.db.util.C2cUtils;
import com.graphai.mall.wx.dto.*;
import com.graphai.mall.wx.util.RSAUtils;
import com.graphai.properties.TaoBaoProperties;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.github.houbb.heaven.util.id.impl.UUID32;
import com.graphai.commons.util.bcrypt.BCryptPasswordEncoder;
import com.graphai.commons.util.httprequest.HttpRequestUtils;
import com.graphai.commons.util.json.JacksonUtil;
import com.graphai.commons.util.json.JacksonUtils;
import com.graphai.commons.util.lang.StringHelper;
import com.graphai.config.JDiworkerConfig;
import com.graphai.id.GraphaiIdGenerator;
import com.graphai.mall.core.notify.NotifyService;
import com.graphai.mall.core.notify.NotifyType;
import com.graphai.mall.db.service.MallInvitationCodeService;
import com.graphai.mall.db.service.account.AccountService;
import com.graphai.mall.db.constant.account.AccountStatus;
import com.graphai.mall.db.service.bmdesign.BmdesignUserLevelService;
import com.graphai.mall.db.service.common.MallPhoneValidationService;
import com.graphai.mall.db.service.coupon.CouponAssignService;
import com.graphai.mall.db.service.order.MallOrderService;
import com.graphai.mall.db.service.sysconfig.MallSystemConfigService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.service.user.behavior.MallUserCardService;
import com.graphai.mall.db.util.OrderUtil;
import com.graphai.mall.db.util.RandomUtils;
import com.graphai.mall.db.util.RetrialUtils;
import com.graphai.mall.db.util.phone.EncryptionUtil;
import com.graphai.mall.db.util.phone.HttpUtil;
import com.graphai.mall.db.util.phone.PushUtils;
import com.graphai.mall.db.vo.MallUserAgentVo;
import com.graphai.mall.tb.dto.TaoBaoBaiChuanUserInfo;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.mall.wx.service.CaptchaCodeManager;
import com.graphai.mall.wx.service.UserTokenManager;
import com.taobao.api.ApiException;
import com.taobao.api.DefaultTaobaoClient;
import com.taobao.api.TaobaoClient;
import com.taobao.api.request.TbkScPublisherInfoSaveRequest;
import com.taobao.api.request.TopAuthTokenCreateRequest;
import com.taobao.api.response.TbkScPublisherInfoSaveResponse;
import com.taobao.api.response.TbkScPublisherInfoSaveResponse.Data;
import com.taobao.api.response.TopAuthTokenCreateResponse;
import com.vdurmont.emoji.EmojiParser;

import cn.binarywang.wx.miniapp.api.WxMaService;
import cn.binarywang.wx.miniapp.bean.WxMaJscode2SessionResult;
import cn.binarywang.wx.miniapp.bean.WxMaPhoneNumberInfo;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.common.bean.WxJsapiSignature;
import me.chanjar.weixin.common.bean.WxOAuth2UserInfo;
import me.chanjar.weixin.common.bean.oauth2.WxOAuth2AccessToken;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;

/**
 * 鉴权服务
 */
@RestController
@RequestMapping("/wx/auth")
@Validated
public class WxAuthController {
    private final Log logger = LogFactory.getLog(WxAuthController.class);

    @Value("${mall.china-mobile.appId}")
    private String appId;
    @Value("${mall.china-mobile.appKey}")
    private String appKey;
    @Value("${mall.china-mobile.appSecret}")
    private String appSecret;
    @Value("${rsa.payPassword.privateKey}")
    private String privateKey;

    @Autowired
    private IMallUserMerchantService mallUserMerchantService;
    @Autowired
    private MallOrderService orderService;

    @Resource
    private com.graphai.mall.db.dao.MallOrderMapper MallOrderMapper;


    @Autowired
    private MallUserService userService;
    @Autowired
    private IMallMerchantService mallMerchantService;

    @Autowired
    private WxMaService wxService;
    // 微信公众号的实现
    @Autowired
    private WxMpService wxMpService;

    @Autowired
    private TaoBaoProperties taoBaoProperties;

    @Autowired
    private NotifyService notifyService;

    @Autowired
    private CouponAssignService couponAssignService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private MallPhoneValidationService phoneService;
    @Autowired
    private MallInvitationCodeService codeSercice;
    @Autowired
    private BmdesignUserLevelService bmdesignUserLevelService;
    @Autowired
    private MallSystemConfigService systemConfigService;
    @Autowired
    private MallUserCardService mallUserCardService;

    /**
     * 云函数登陆请求
     */
    private static final String APP_YHS_LOGIN_URL =
            "https://7f2c004a-4500-4397-9620-685a6117ba2b.bspapp.com/http/authLogin";

    /**
     * 账号登录
     *
     * @param registerInfo 请求内容，{ username: xxx, password: xxx }
     * @param request      请求对象
     * @return 登录结果
     */
    @PostMapping("login")
    // public Object login(@RequestBody String body, HttpServletRequest request) {
    public Object login(@RequestBody RegisterInfo registerInfo, HttpServletRequest request) {
        // String username = JacksonUtil.parseString(body, "username");
        // String password = JacksonUtil.parseString(body, "password");

        String username = registerInfo.getUsername();
        String password = registerInfo.getPassword();
        String loginChannel = registerInfo.getRegisterChannel();
        String wxMpCode = registerInfo.getWxCode();
        String sessionKey = "";
        String openId = "";
        MiniProgramUserInfo miniProgramUserInfo = null;
        Map<Object, Object> result = new HashMap<Object, Object>();
        try {
            if (username == null || password == null) {
                return ResponseUtil.badArgument();
            }

            List<MallUser> userList = userService.queryByUsername(username);
            MallUser user = null;
            if (userList.size() > 1) {
                return ResponseUtil.serious();
            } else if (userList.size() == 0) {
                return ResponseUtil.fail(AUTH_INVALID_ACCOUNT, "账号不存在");
            } else {
                user = userList.get(0);
            }

            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
            if (!encoder.matches(password, user.getPassword())) {
                return ResponseUtil.fail(AUTH_INVALID_ACCOUNT, "账号密码不对");
            }

            if ("0".equals(user.getLevelId())){
                return ResponseUtil.fail("该用户无权限登录!");
            }
            // 小程序登录的时候如果没有微信用户信息那么需要更新
            if ("miniProgram".equalsIgnoreCase(loginChannel) && !StringUtils.isEmpty(wxMpCode)) {
                miniProgramUserInfo = registerInfo.getMiniProgramUserInfo();
                WxMaJscode2SessionResult getWxResult = this.wxService.getUserService().getSessionInfo(wxMpCode);
                sessionKey = getWxResult.getSessionKey();
                openId = getWxResult.getOpenid();
                if (StringUtils.isEmpty(user.getMiniProgramOpenid())) {

                    MallUser UUser = new MallUser();
                    UUser.setId(user.getId());
                    UUser.setMiniProgramOpenid(openId);
                    UUser.setNickname(miniProgramUserInfo.getNickName());
                    UUser.setAvatar(miniProgramUserInfo.getAvatarUrl());
                    UUser.setGender(miniProgramUserInfo.getGender().toString());
                    userService.updateById(UUser);


                    user.setNickname(miniProgramUserInfo.getNickName());
                    user.setAvatar(miniProgramUserInfo.getAvatarUrl());
                    user.setGender(miniProgramUserInfo.getGender().toString());
                }
            }


            // token
            String token = UserTokenManager.generateToken(user.getId(), user.getTbkRelationId(),
                    user.getTbkSpecialId());

            result.put("token", token);
            result.put("userInfo", user);
        } catch (Exception e) {
            logger.error("微信登录异常 : " + e.getMessage(), e);
        }
        return ResponseUtil.ok(result);
    }

    @PostMapping("wx_config")
    public Object getWxConfig(@RequestBody String body, HttpServletRequest request) {
        // window.location.href.split('#')[0]
        String url = JacksonUtil.parseString(body, "url");
        try {
            WxJsapiSignature wxJsapiSignature = wxMpService.createJsapiSignature(url);
            return ResponseUtil.ok(wxJsapiSignature);
        } catch (Exception e) {
            logger.error("获取微信签名异常 ：" + e.getMessage(), e);
        }

        return ResponseUtil.fail();
    }

    /**
     * 微信公众号登录(通过授权获取微信公众号openId_用户购买骑士卡授权)
     *
     * @param wxLoginInfo
     * @return
     */
    @PostMapping("login_by_weixinmp")
    public Object loginByWeixinMp(@RequestBody WxLoginInfo wxLoginInfo) throws IOException {
        logger.info("参数：" + JacksonUtils.bean2Jsn(wxLoginInfo));

        String code = wxLoginInfo.getCode();
        String url = wxLoginInfo.getRedirectURI();
        String mobile = wxLoginInfo.getMobile(); // 用户手机号
        // code为空的情况下返回跳转链接
        if (StringUtils.isEmpty(code)) {
            // code为空的情况下说明还没有经过授权链接
            // String retUrl = wxMpService.oauth2buildAuthorizationUrl(url,WxConsts.OAuth2Scope.SNSAPI_USERINFO,
            // null);

            // 授权方法改写_weixin-pay.version_3.9.0
            String retUrl = wxMpService.getOAuth2Service().buildAuthorizationUrl(url,
                    WxConsts.OAuth2Scope.SNSAPI_USERINFO, null);
            Map<Object, Object> result = new HashMap<Object, Object>();
            result.put("retUrl", retUrl);
            return ResponseUtil.ok(result);
            // return ResponseUtil.badArgument();
        }
        WxOAuth2UserInfo wxMpUser = null;
        String openId = null;
        try {
            // WxMpOAuth2AccessToken wxMpOAuth2AccessToken = wxMpService.oauth2getAccessToken(code);
            // wxMpUser = wxMpService.oauth2getUserInfo(wxMpOAuth2AccessToken,null);

            // 授权方法改写_weixin-pay.version_3.9.0
            // WxMpOAuth2AccessToken wxMpOAuth2AccessToken =
            // wxMpService.getOAuth2Service().getAccessToken(code);
            // 授权方法改写_weixin-pay.version_4.0.0
            WxOAuth2AccessToken wxMpOAuth2AccessToken = wxMpService.getOAuth2Service().getAccessToken(code);
            wxMpUser = wxMpService.getOAuth2Service().getUserInfo(wxMpOAuth2AccessToken, null);
            // WxMaJscode2SessionResult result =
            // this.wxService.getUserService().getSessionInfo(code);
            logger.info("微信公众号对象转换数据 ：" + JacksonUtils.bean2Jsn(wxMpUser));
            // sessionKey = result.getSessionKey();
            openId = wxMpUser.getOpenid();

            if (openId == null) {
                return ResponseUtil.fail();
            }

            // 若存在userId则修改用户的openId
            if (StringUtils.isEmpty(mobile)) {
                return ResponseUtil.badArgumentValue("未获取到手机号!");
            }
            List<MallUser> mallUserList = userService.queryByMobile(mobile);
            MallUser mallUser = new MallUser();
            if (mallUserList.size() == 0) {
                // 若用户不存在，则注册成新用户
                mallUser.setMobile(mobile);
                mallUser.setNickname(mobile);
                // mallUser.setLastLoginTime(LocalDateTime.now());
                mallUser.setLevelId("0");
                mallUser.setWeixinMpOpenid(openId);
                mallUser.setAvatar(wxMpUser.getHeadImgUrl());
                mallUser.setGender(wxMpUser.getSex().toString());
                userService.add(mallUser);
                // 开户
                accountService.accountTradeCreateAccount(mallUser.getId());
            } else if (mallUserList.size() == 1) {
                mallUser = mallUserList.get(0);
                MallUser updateUser = new MallUser();
                updateUser.setId(mallUser.getId());
                updateUser.setWeixinMpOpenid(openId);
                logger.info("修改的用户信息：" + updateUser);
                userService.updateById(updateUser);
            } else {
                return ResponseUtil.badArgumentValue("存在多个相同用户！");
            }

            Map<Object, Object> result = new HashMap<Object, Object>();
            result.put("wxMpUser", wxMpUser);
            logger.info("返回数据 : " + JacksonUtils.bean2Jsn(ResponseUtil.ok(result)));

            return ResponseUtil.ok(result);

        } catch (Exception e) {
            logger.error("微信公众号登录异常 :" + e.getMessage());
            // 如果是调用微信发生的异常
            if (e instanceof WxErrorException) {
                WxErrorException ex = (WxErrorException) e;
                // 如果发生code已经使用过的错误
                if (ex.getError().getErrorCode() == 40163) {
                    // code为空的情况下说明还没有经过授权链接
                    // String retUrl = wxMpService.oauth2buildAuthorizationUrl(url,WxConsts.OAuth2Scope.SNSAPI_USERINFO,
                    // null);

                    // 授权方法改写_weixin-pay.version_3.9.0
                    String retUrl = wxMpService.getOAuth2Service().buildAuthorizationUrl(url,
                            WxConsts.OAuth2Scope.SNSAPI_USERINFO, null);
                    Map<Object, Object> result = new HashMap<Object, Object>();
                    result.put("retUrl", retUrl);
                    return ResponseUtil.ok(result);
                }

            }

            try {
                e.printStackTrace();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }

        return ResponseUtil.fail();
    }


    /**
     * 活动支付微信授权
     */
    @PostMapping("login_by_weixinmpv3")
    public Object loginByWeixinMpv3(@RequestBody WxLoginInfo wxLoginInfo) throws IOException {
        logger.info("参数：" + JacksonUtils.bean2Jsn(wxLoginInfo));

        String code = wxLoginInfo.getCode();
        String url = wxLoginInfo.getRedirectURI();
        String mobile = wxLoginInfo.getMobile(); // 用户手机号

        // code为空的情况下返回跳转链接
        if (StringUtils.isEmpty(code)) {
            // code为空的情况下说明还没有经过授权链接
            // String retUrl = wxMpService.oauth2buildAuthorizationUrl(url,WxConsts.OAuth2Scope.SNSAPI_USERINFO,
            // null);

            // 授权方法改写_weixin-pay.version_3.9.0
            String retUrl = wxMpService.getOAuth2Service().buildAuthorizationUrl(url,
                    WxConsts.OAuth2Scope.SNSAPI_USERINFO, null);
            Map<Object, Object> result = new HashMap<Object, Object>();
            result.put("retUrl", retUrl);
            return ResponseUtil.ok(result);
        }
        WxOAuth2UserInfo wxMpUser = null;
        String openId = null;
        try {
            // WxMpOAuth2AccessToken wxMpOAuth2AccessToken = wxMpService.oauth2getAccessToken(code);
            // wxMpUser = wxMpService.oauth2getUserInfo(wxMpOAuth2AccessToken,null);

            // 授权方法改写_weixin-pay.version_3.9.0
            // WxMpOAuth2AccessToken wxMpOAuth2AccessToken =
            // wxMpService.getOAuth2Service().getAccessToken(code);
            // 授权方法改写 wx-java.version_4.0.0
            WxOAuth2AccessToken wxMpOAuth2AccessToken = wxMpService.getOAuth2Service().getAccessToken(code);
            wxMpUser = wxMpService.getOAuth2Service().getUserInfo(wxMpOAuth2AccessToken, null);
            // WxMaJscode2SessionResult result =
            // this.wxService.getUserService().getSessionInfo(code);
            logger.info("微信公众号对象转换数据 ：" + JacksonUtils.bean2Jsn(wxMpUser));
            // sessionKey = result.getSessionKey();
            openId = wxMpUser.getOpenid();

            if (openId == null) {
                return ResponseUtil.fail();
            }

            List<MallUser> mallUserList = userService.queryByOpenid(openId);
            MallUser mallUser = new MallUser();
            if (mallUserList.size() == 0) {
                // 若用户不存在，则注册成新用户
                mallUser.setMobile(mobile);
                mallUser.setNickname(mobile);
                // mallUser.setLastLoginTime(LocalDateTime.now());
                mallUser.setLevelId("0");
                mallUser.setWeixinMpOpenid(openId);
                mallUser.setAvatar(wxMpUser.getHeadImgUrl());
                mallUser.setGender(wxMpUser.getSex().toString());
                mallUser.setSource("用户通过H5页面购买998特权活动创建！");
                mallUser.setChannelId(AccountStatus.CHANNEL_ID_2); // 活动注册
                userService.add(mallUser);
                // 开户
                accountService.accountTradeCreateAccount(mallUser.getId());
            } else if (mallUserList.size() == 1) {
                mallUser = mallUserList.get(0);
            } else {
                return ResponseUtil.badArgumentValue("存在多个相同用户！");
            }

            /** 生成token返回给前端 **/
            String token = UserTokenManager.generateToken(mallUser.getId(), mallUser.getTbkRelationId(),
                    mallUser.getTbkSpecialId());
            Map<Object, Object> result = new HashMap<Object, Object>();
            if (!StringUtils.isEmpty(mallUser.getNickname())) {
                mallUser.setNickname(EmojiParser.parseToUnicode(mallUser.getNickname()));
            }
            result.put("token", token);
            result.put("wxMpUser", mallUser);
            logger.info("返回数据 : " + JacksonUtils.bean2Jsn(ResponseUtil.ok(result)));
            return ResponseUtil.ok(result);

        } catch (Exception e) {
            logger.error("微信公众号登录异常 :" + e.getMessage());
            // 如果是调用微信发生的异常
            if (e instanceof WxErrorException) {
                WxErrorException ex = (WxErrorException) e;
                // 如果发生code已经使用过的错误
                if (ex.getError().getErrorCode() == 40163) {
                    // code为空的情况下说明还没有经过授权链接
                    // String retUrl = wxMpService.oauth2buildAuthorizationUrl(url,WxConsts.OAuth2Scope.SNSAPI_USERINFO,
                    // null);

                    // 授权方法改写_weixin-pay.version_3.9.0
                    String retUrl = wxMpService.getOAuth2Service().buildAuthorizationUrl(url,
                            WxConsts.OAuth2Scope.SNSAPI_USERINFO, null);
                    Map<Object, Object> result = new HashMap<Object, Object>();
                    result.put("retUrl", retUrl);
                    return ResponseUtil.ok(result);
                }

            }

            try {
                e.printStackTrace();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }

        return ResponseUtil.fail();
    }

    /**
     * 创客微信授权
     */
    @PostMapping("login_by_weixinmpv4")
    public Object loginByWeixinMpv4(@RequestBody WxLoginInfo wxLoginInfo) throws IOException {
        logger.info("参数：" + JacksonUtils.bean2Jsn(wxLoginInfo));

        String code = wxLoginInfo.getCode();
        String url = wxLoginInfo.getRedirectURI();
        String mobile = wxLoginInfo.getMobile(); // 用户手机号

        // code为空的情况下返回跳转链接
        if (StringUtils.isEmpty(code)) {
            // code为空的情况下说明还没有经过授权链接
            // String retUrl = wxMpService.oauth2buildAuthorizationUrl(url,WxConsts.OAuth2Scope.SNSAPI_USERINFO,
            // null);

            // 授权方法改写_weixin-pay.version_3.9.0
            String retUrl = wxMpService.getOAuth2Service().buildAuthorizationUrl(url,
                    WxConsts.OAuth2Scope.SNSAPI_USERINFO, null);
            Map<Object, Object> result = new HashMap<Object, Object>();
            result.put("retUrl", retUrl);
            return ResponseUtil.ok(result);
        }
        WxOAuth2UserInfo wxMpUser = null;
        String openId = null;
        try {
            // WxMpOAuth2AccessToken wxMpOAuth2AccessToken = wxMpService.oauth2getAccessToken(code);
            // wxMpUser = wxMpService.oauth2getUserInfo(wxMpOAuth2AccessToken,null);

            // 授权方法改写_weixin-pay.version_3.9.0
            // WxMpOAuth2AccessToken wxMpOAuth2AccessToken =
            // wxMpService.getOAuth2Service().getAccessToken(code);
            // 授权方法改写 wx-java.version_4.0.0
            WxOAuth2AccessToken wxMpOAuth2AccessToken = wxMpService.getOAuth2Service().getAccessToken(code);
            wxMpUser = wxMpService.getOAuth2Service().getUserInfo(wxMpOAuth2AccessToken, null);
            // WxMaJscode2SessionResult result =
            // this.wxService.getUserService().getSessionInfo(code);
            logger.info("微信公众号对象转换数据 ：" + JacksonUtils.bean2Jsn(wxMpUser));
            // sessionKey = result.getSessionKey();
            openId = wxMpUser.getOpenid();

            if (openId == null) {
                return ResponseUtil.fail();
            }

            Map<Object, Object> result = new HashMap<Object, Object>();
            result.put("wxOpenid", openId);
            logger.info("返回数据 : " + JacksonUtils.bean2Jsn(ResponseUtil.ok(result)));
            return ResponseUtil.ok(result);

        } catch (Exception e) {
            logger.error("微信公众号登录异常 :" + e.getMessage());
            // 如果是调用微信发生的异常
            if (e instanceof WxErrorException) {
                WxErrorException ex = (WxErrorException) e;
                // 如果发生code已经使用过的错误
                if (ex.getError().getErrorCode() == 40163) {
                    // code为空的情况下说明还没有经过授权链接
                    // String retUrl = wxMpService.oauth2buildAuthorizationUrl(url,WxConsts.OAuth2Scope.SNSAPI_USERINFO,
                    // null);

                    // 授权方法改写_weixin-pay.version_3.9.0
                    String retUrl = wxMpService.getOAuth2Service().buildAuthorizationUrl(url,
                            WxConsts.OAuth2Scope.SNSAPI_USERINFO, null);
                    Map<Object, Object> result = new HashMap<Object, Object>();
                    result.put("retUrl", retUrl);
                    return ResponseUtil.ok(result);
                }

            }

            try {
                e.printStackTrace();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }

        return ResponseUtil.fail();
    }


    /**
     * 微信公众号登录(通过授权获取微信公众号openId_用户购买骑士卡授权)
     *
     * @param wxLoginInfo
     * @return
     */
    @PostMapping("login_by_weixinmp/v2")
    public Object loginByWeixinMpV2(@RequestBody WxLoginInfo wxLoginInfo) throws IOException {
        logger.info("参数：" + JacksonUtils.bean2Jsn(wxLoginInfo));

        String code = wxLoginInfo.getCode();
        String url = wxLoginInfo.getRedirectURI();
        // code为空的情况下返回跳转链接
        if (StringUtils.isEmpty(code)) {
            // code为空的情况下说明还没有经过授权链接
            // String retUrl = wxMpService.oauth2buildAuthorizationUrl(url,WxConsts.OAuth2Scope.SNSAPI_USERINFO,
            // null);

            // 授权方法改写_weixin-pay.version_3.9.0
            String retUrl = wxMpService.getOAuth2Service().buildAuthorizationUrl(url,
                    WxConsts.OAuth2Scope.SNSAPI_USERINFO, null);
            Map<Object, Object> result = new HashMap<Object, Object>();
            result.put("retUrl", retUrl);
            return ResponseUtil.ok(result);
            // return ResponseUtil.badArgument();
        }
        WxOAuth2UserInfo wxMpUser = null;
        String openId = null;
        try {
            // WxMpOAuth2AccessToken wxMpOAuth2AccessToken = wxMpService.oauth2getAccessToken(code);
            // wxMpUser = wxMpService.oauth2getUserInfo(wxMpOAuth2AccessToken,null);

            // 授权方法改写_weixin-pay.version_3.9.0
            // WxMpOAuth2AccessToken wxMpOAuth2AccessToken =
            // wxMpService.getOAuth2Service().getAccessToken(code);
            WxOAuth2AccessToken wxMpOAuth2AccessToken = wxMpService.getOAuth2Service().getAccessToken(code);
            wxMpUser = wxMpService.getOAuth2Service().getUserInfo(wxMpOAuth2AccessToken, null);
            // WxMaJscode2SessionResult result =
            // this.wxService.getUserService().getSessionInfo(code);
            logger.info("微信公众号对象转换数据 ：" + JacksonUtils.bean2Jsn(wxMpUser));
            // sessionKey = result.getSessionKey();
            openId = wxMpUser.getOpenid();

            if (openId == null) {
                return ResponseUtil.fail();
            }

            // 若存在userId则修改用户的openId
            // if(StringUtils.isEmpty(mobile)){
            // return ResponseUtil.badArgumentValue("未获取到手机号!");
            // }
            MallUser getmallUser = userService.queryByMpOid(openId);
            if (getmallUser != null) {
                // MallUser mallUser = mallUserList.get(0);
                MallUser updateUser = new MallUser();
                updateUser.setId(getmallUser.getId());
                updateUser.setWeixinMpOpenid(openId);
                logger.info("修改的用户信息：" + updateUser);
                userService.updateById(updateUser);
            } else {
                MallUser user = new MallUser();
                user.setUsername(openId);
                user.setPassword(openId);
                user.setWeixinMpOpenid(openId);
                user.setAvatar(wxMpUser.getHeadImgUrl());
                user.setNickname(wxMpUser.getNickname());
                user.setGender(wxMpUser.getSex().toString());
                user.setStatus((byte) 0);
                // user.setLastLoginTime(LocalDateTime.now());
                user.setLevelId("0");

                userService.add(user);
                // return ResponseUtil.badArgumentValue("用户不存在或存在多个相同用户！");
                String token = UserTokenManager.generateToken(user.getId(), user.getTbkRelationId(),
                        user.getTbkSpecialId());
                Map<Object, Object> result = new HashMap<Object, Object>();
                result.put("token", token);
                result.put("userInfo", user);

                return ResponseUtil.ok(result);

            }

            // token
            String token = UserTokenManager.generateToken(getmallUser.getId(), getmallUser.getTbkRelationId(),
                    getmallUser.getTbkSpecialId());
            Map<Object, Object> result2 = new HashMap<Object, Object>();
            result2.put("token", token);
            result2.put("userInfo", getmallUser);

            return ResponseUtil.ok(result2);

            // Map<Object, Object> result = new HashMap<Object, Object>();
            // result.put("wxMpUser", wxMpUser);
            // logger.info("返回数据 : "
            // + JacksonUtils.bean2Jsn(ResponseUtil.ok(result)));
            //
            // return ResponseUtil.ok(result);

        } catch (Exception e) {
            logger.error("微信公众号登录异常 :" + e.getMessage());
            // 如果是调用微信发生的异常
            if (e instanceof WxErrorException) {
                WxErrorException ex = (WxErrorException) e;
                // 如果发生code已经使用过的错误
                if (ex.getError().getErrorCode() == 40163) {
                    // code为空的情况下说明还没有经过授权链接
                    // String retUrl = wxMpService.oauth2buildAuthorizationUrl(url,WxConsts.OAuth2Scope.SNSAPI_USERINFO,
                    // null);

                    // 授权方法改写_weixin-pay.version_3.9.0
                    String retUrl = wxMpService.getOAuth2Service().buildAuthorizationUrl(url,
                            WxConsts.OAuth2Scope.SNSAPI_USERINFO, null);
                    Map<Object, Object> result = new HashMap<Object, Object>();
                    result.put("retUrl", retUrl);
                    return ResponseUtil.ok(result);
                }

            }

            try {
                e.printStackTrace();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }

        return ResponseUtil.fail();
    }

    /**
     * 微信小程序登录
     *
     * @param wxLoginInfo 请求内容，{ code: xxx, userInfo: xxx }
     * @param request     请求对象
     * @return 登录结果
     */
    @PostMapping("login_by_weixin")
    public Object loginByWeixin(@RequestBody WxLoginInfo wxLoginInfo, HttpServletRequest request) throws IOException {
        String code = wxLoginInfo.getCode();
        UserInfo userInfo = wxLoginInfo.getUserInfo();
        if (code == null || userInfo == null) {
            return ResponseUtil.badArgument();
        }

        String sessionKey = null;
        String openId = null;
        try {
            WxMaJscode2SessionResult result = this.wxService.getUserService().getSessionInfo(code);
            sessionKey = result.getSessionKey();
            openId = result.getOpenid();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (sessionKey == null || openId == null) {
            return ResponseUtil.fail();
        }

        MallUser user = userService.queryByOid(openId);
        if (user == null) {
            user = new MallUser();
            user.setUsername(openId);
            user.setPassword(openId);
            user.setWeixinOpenid(openId);
            user.setAvatar(userInfo.getAvatarUrl());
            user.setNickname(userInfo.getNickName());
            user.setGender(userInfo.getGender().toString());
            user.setStatus((byte) 0);
            // user.setLastLoginTime(LocalDateTime.now());
            user.setLastLoginIp(IpUtil.getIpAddr(request));
            user.setSessionKey(sessionKey);
            user.setLevelId("0");

            userService.add(user);

            // 新用户发送注册优惠券
            couponAssignService.assignForRegister(user.getId());
        } else {
            // user.setLastLoginTime(LocalDateTime.now());
            user.setLastLoginIp(IpUtil.getIpAddr(request));
            user.setSessionKey(sessionKey);
            if (userService.updateById(user) == 0) {
                return ResponseUtil.updatedDataFailed();
            }
        }

        // token
        String token = UserTokenManager.generateToken(user.getId(), user.getTbkRelationId(), user.getTbkSpecialId());

        Map<Object, Object> result = new HashMap<Object, Object>();
        result.put("token", token);
        result.put("userInfo", userInfo);
        return ResponseUtil.ok(result);
    }

    /**
     * 请求注册验证码
     * <p>
     * TODO 这里需要一定机制防止短信验证码被滥用
     *
     * @param body 手机号码 { mobile }
     * @return
     */
    @PostMapping("regCaptcha")
    public Object registerCaptcha(@RequestBody String body) {
        String phoneNumber = JacksonUtil.parseString(body, "mobile");
        if (StringUtils.isEmpty(phoneNumber)) {
            return ResponseUtil.badArgument();
        }
        if (!RegexUtil.isMobileExact(phoneNumber)) {
            return ResponseUtil.badArgumentValue();
        }

        if (!notifyService.isSmsEnable()) {
            return ResponseUtil.fail(AUTH_CAPTCHA_UNSUPPORT, "小程序后台验证码服务不支持");
        }
        String code = CharUtil.getRandomNum(6);
        notifyService.notifySmsTemplate(phoneNumber, NotifyType.CAPTCHA, new String[]{code});

        boolean successful = CaptchaCodeManager.addToCache(phoneNumber, code);
        if (!successful) {
            return ResponseUtil.fail(AUTH_CAPTCHA_FREQUENCY, "验证码未超时1分钟，不能发送");
        }

        return ResponseUtil.ok();
    }


    @PostMapping("/register/sms")
    public Object getRegisterShortSms(@RequestBody String body) {
        String phoneNumber = JacksonUtil.parseString(body, "mobile");

        String smsType = JacksonUtil.parseString(body, "smsType");

        if (StringUtils.isEmpty(phoneNumber)) {
            return ResponseUtil.badArgument();
        }
        if (!RegexUtil.isMobileExact(phoneNumber)) {
            return ResponseUtil.badArgumentValue("请输入正确的手机号!");
        }

        String code = CharUtil.getRandomNum(6);


        // notifyService.notifySmsTemplate(phoneNumber, NotifyType.CAPTCHA,
        // new String[] { code });
        //

        String res = phoneService.sendSms(phoneNumber, code, smsType);
        // boolean successful = CaptchaCodeManager.addToCache(phoneNumber, code);
        if ("001".equals(res)) {
            return ResponseUtil.fail(AUTH_CAPTCHA_FREQUENCY, "验证码未超时1分钟，不能发送");
        }

        return ResponseUtil.ok(res);
    }

    /**
     * 发送阿里云验证码 { "mobile":"15871729300","smsType":"APP_LOGIN" }
     */
    @PostMapping("/register/sendAliSms")
    public Object sendAliSms(@RequestBody String body) {
        String phoneNumber = JacksonUtil.parseString(body, "mobile");
        String smsType = JacksonUtil.parseString(body, "smsType");

        if (StringUtils.isEmpty(phoneNumber)) {
            return ResponseUtil.badArgumentValue("手机号不能为空！");
        }
        if (!RegexUtil.isMobileExact(phoneNumber)) {
            return ResponseUtil.badArgumentValue("请输入正确的手机号!");
        }

        String code = CharUtil.getRandomNum(6);

        String res = phoneService.sendAliSms(phoneNumber, code, smsType);

        if ("001".equals(res)) {
            return ResponseUtil.fail(AUTH_CAPTCHA_FREQUENCY, "验证码未超时1分钟，不能发送");
        }
        phoneNumber = NameUtil.mobileEncrypt(phoneNumber);
        return ResponseUtil.ok(phoneNumber);
    }


    /**
     * 账号注册
     *
     * @param body    请求内容 { username: xxx, password: xxx, mobile: xxx code: xxx }
     *                其中code是手机验证码，目前还不支持手机短信验证码
     * @param request 请求对象
     * @return 登录结果 成功则 { errno: 0, errmsg: '成功', data: { token: xxx, tokenExpire: xxx, userInfo: xxx }
     * } 失败则 { errno: XXX, errmsg: XXX }
     */
    @PostMapping("register")
    public Object register(@RequestBody String body, HttpServletRequest request) throws IOException {
        String username = JacksonUtil.parseString(body, "username");
        String password = JacksonUtil.parseString(body, "password");
        String mobile = JacksonUtil.parseString(body, "mobile");
        String code = JacksonUtil.parseString(body, "code");
        String wxCode = JacksonUtil.parseString(body, "wxCode");

        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password) || StringUtils.isEmpty(mobile)
                || StringUtils.isEmpty(wxCode) || StringUtils.isEmpty(code)) {
            return ResponseUtil.badArgument();
        }

        List<MallUser> userList = userService.queryByUsername(username);
        if (userList.size() > 0) {
            return ResponseUtil.fail(AUTH_NAME_REGISTERED, "用户名已注册");
        }

        userList = userService.queryByMobile(mobile);
        if (userList.size() > 0) {
            return ResponseUtil.fail(AUTH_MOBILE_REGISTERED, "手机号已注册");
        }
        if (!RegexUtil.isMobileExact(mobile)) {
            return ResponseUtil.fail(AUTH_INVALID_MOBILE, "手机号格式不正确");
        }
        // 判断验证码是否正确
        String cacheCode = CaptchaCodeManager.getCachedCaptcha(mobile);
        if (cacheCode == null || cacheCode.isEmpty() || !cacheCode.equals(code)) {
            return ResponseUtil.fail(AUTH_CAPTCHA_UNMATCH, "验证码错误");
        }

        String openId = null;
        try {
            WxMaJscode2SessionResult result = this.wxService.getUserService().getSessionInfo(wxCode);
            openId = result.getOpenid();
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseUtil.fail(AUTH_OPENID_UNACCESS, "openid 获取失败");
        }
        userList = userService.queryByOpenid(openId);
        if (userList.size() > 1) {
            return ResponseUtil.serious();
        }
        if (userList.size() == 1) {
            MallUser checkUser = userList.get(0);
            String checkUsername = checkUser.getUsername();
            String checkPassword = checkUser.getPassword();
            if (!checkUsername.equals(openId) || !checkPassword.equals(openId)) {
                return ResponseUtil.fail(AUTH_OPENID_BINDED, "openid已绑定账号");
            }
        }

        MallUser user = null;
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String encodedPassword = encoder.encode(password);
        user = new MallUser();
        user.setUsername(username);
        user.setPassword(encodedPassword);
        user.setMobile(mobile);
        user.setWeixinOpenid(openId);
        user.setAvatar("https://yanxuan.nosdn.127.net/80841d741d7fa3073e0ae27bf487339f.jpg?imageView&quality=90&thumbnail=64x64");
        user.setNickname(username);
        user.setGender("0");
        user.setUserLevel((byte) 0);
        user.setStatus((byte) 0);
        user.setLastLoginTime(LocalDateTime.now());
        user.setLastLoginIp(IpUtil.getIpAddr(request));
        user.setLevelId("0");
        userService.add(user);

        // 给新用户发送注册优惠券
        couponAssignService.assignForRegister(user.getId());

        // userInfo
        UserInfo userInfo = new UserInfo();
        userInfo.setNickName(username);
        userInfo.setAvatarUrl(user.getAvatar());

        // token
        String token = UserTokenManager.generateToken(user.getId(), user.getTbkRelationId(), user.getTbkSpecialId());

        Map<Object, Object> result = new HashMap<Object, Object>();
        result.put("token", token);
        result.put("userInfo", userInfo);
        return ResponseUtil.ok(result);
    }

    /**
     * 分销商注册
     *
     * @param registerInfo
     * @param request
     * @return
     */
    @PostMapping("register_merchant")
    public Object registerMerchant(@RequestBody RegisterInfo registerInfo, HttpServletRequest request) {


        // logger.info("注册的参数body ： " + body);
        // String username = JacksonUtil.parseString(body, "username");
        // String password = JacksonUtil.parseString(body, "password");
        // // 推荐码
        // String invitationCode = JacksonUtil.parseString(body, "invitationCode");
        // // 微信授权的code
        // String wxMpCode = JacksonUtil.parseString(body, "wxCode");
        //
        // String valCode = JacksonUtil.parseString(body, "valCode");
        //
        // /**
        // * 注册渠道
        // */
        // String registerChannel = JacksonUtil.parseString(body, "registerChannel");
        //

        String username = registerInfo.getUsername();
        String password = registerInfo.getPassword();
        // 推荐码
        String invitationCode = registerInfo.getInvitationCode();
        // 微信授权的code
        String wxMpCode = registerInfo.getWxCode();

        String valCode = registerInfo.getValCode();

        String smsType = "APP_REGISTER";
        /**
         * 注册渠道
         */
        String registerChannel = registerInfo.getRegisterChannel();
        // 用户注册参数校验
        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password) || StringUtils.isEmpty(valCode)) {
            return ResponseUtil.badArgument();
        }

        MallPhoneValidation val = phoneService.getPhoneValidationByCode(username, valCode, smsType);

        if (val == null) {
            return ResponseUtil.fail(AUTH_NAME_REGISTERED, "验证码有误");
        }

        // List<MallUser> myUserList = userService.queryByInvitationCode(invitationCode);

        MallInvitationCode codeDto = codeSercice.getCode(invitationCode);
        if (codeDto == null) {
            return ResponseUtil.fail(AUTH_NAME_REGISTERED, "邀请码不存在");
        }

        List<MallUser> userList = userService.queryByUsername(username);
        if (userList.size() > 0) {
            return ResponseUtil.fail(AUTH_NAME_REGISTERED, "用户名已注册");
        }

        userList = userService.queryByMobile(username);
        if (userList.size() > 0) {
            return ResponseUtil.fail(AUTH_MOBILE_REGISTERED, "手机号已注册");
        }

        if (!RegexUtil.isMobileExact(username)) {
            return ResponseUtil.fail(AUTH_INVALID_MOBILE, "手机号格式不正确");
        }

        WxOAuth2UserInfo wxMpUser = null;
        String openId = null;

        try {
            MiniProgramUserInfo miniProgramUserInfo = null;
            String sessionKey = "";
            if ("miniProgram".equalsIgnoreCase(registerChannel) && !StringUtils.isEmpty(wxMpCode)) {

                miniProgramUserInfo = registerInfo.getMiniProgramUserInfo();
                WxMaJscode2SessionResult result = this.wxService.getUserService().getSessionInfo(wxMpCode);
                sessionKey = result.getSessionKey();
                openId = result.getOpenid();

            } else if ("wx".equalsIgnoreCase(registerChannel)) {

            } else if ("wxMp".equalsIgnoreCase(registerChannel) && !StringUtils.isEmpty(wxMpCode)) {
                // WxMpOAuth2AccessToken wxMpOAuth2AccessToken = wxMpService.oauth2getAccessToken(wxMpCode);
                // wxMpUser = wxMpService.oauth2getUserInfo(wxMpOAuth2AccessToken, null);

                // 授权方法改写_weixin-pay.version_3.9.0
                // WxMpOAuth2AccessToken wxMpOAuth2AccessToken =
                // wxMpService.getOAuth2Service().getAccessToken(wxMpCode);
                // 授权方法改写wx-java.version_4.0.0
                WxOAuth2AccessToken wxMpOAuth2AccessToken = wxMpService.getOAuth2Service().getAccessToken(wxMpCode);
                wxMpUser = wxMpService.getOAuth2Service().getUserInfo(wxMpOAuth2AccessToken, null);
                logger.info("微信数据获取 ：" + JacksonUtils.bean2Jsn(wxMpUser));
                // sessionKey = result.getSessionKey();
                openId = wxMpUser.getOpenid();

                if (openId == null) {
                    return ResponseUtil.fail(-1, "微信登录信息获取异常");
                }

            }


            MallUser user = null;
            // 记录推荐码
            String myInvitationCode = invitationCode;// AppIdAlgorithm.generateCode(4).toUpperCase();

            String parentId = codeDto.getUserId();
            // String parentId = fenxiaoUser.getId();
            String myI = parentId + myInvitationCode;
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
            String encodedPassword = encoder.encode(password);
            user = new MallUser();
            user.setUsername(username);
            user.setPassword(encodedPassword);
            user.setMobile(username);
            user.setIsDistribut("1");
            if (wxMpUser != null) {
                user.setWeixinOpenid(openId);
                user.setAvatar(wxMpUser.getHeadImgUrl());
                user.setNickname(wxMpUser.getNickname());
                user.setGender(wxMpUser.getSex().toString());
            } else {
                if ("miniProgram".equalsIgnoreCase(registerChannel) && miniProgramUserInfo != null) {
                    // user.setWeixinOpenid(openId);
                    user.setMiniProgramOpenid(openId);
                    user.setAvatar(miniProgramUserInfo.getAvatarUrl());
                    user.setNickname(miniProgramUserInfo.getNickName());
                    user.setGender(miniProgramUserInfo.getGender().toString());
                } else {
                    user.setNickname(username);
                }
            }

            user.setUserLevel((byte) 0);
            user.setInvitationCode(myI);
            // 记录上下级关系
            user.setFirstLeader(parentId.toString());
            user.setStatus((byte) 0);
            user.setLastLoginTime(LocalDateTime.now());
            user.setLastLoginIp(IpUtil.getIpAddr(request));
            user.setSessionKey(sessionKey);
            user.setLevelId("0");
            userService.add(user);

            // 开户
            if (user.getId() != null) {
                logger.info("开户成功..............");
                accountService.accountTradeCreateAccount(user.getId().toString());
            }
            // 给新用户发送注册优惠券 2C 用户需要这个逻辑
            // couponAssignService.assignForRegister(user.getId());

            // userInfo
            // UserInfo userInfo = new UserInfo();
            // userInfo.setNickName(username);
            // userInfo.setAvatarUrl(user.getAvatar());
            MallPhoneValidation updateVal = new MallPhoneValidation();
            updateVal.setId(val.getId());
            updateVal.setValState("00");
            phoneService.update(updateVal);

            // 修改邀请码使用状态
            MallInvitationCode uCode = new MallInvitationCode();
            uCode.setId(codeDto.getId());
            uCode.setUseStatus("111");
            codeSercice.updateMallInvitationCode(uCode);
            // token
            String token = UserTokenManager.generateToken(user.getId(), user.getTbkRelationId(),
                    user.getTbkSpecialId());

            Map<Object, Object> result = new HashMap<Object, Object>();
            result.put("token", token);
            result.put("userInfo", user);
            return ResponseUtil.ok(result);
        } catch (Exception e) {
            logger.error("分销商注册异常: " + e.getMessage(), e);
        }

        return ResponseUtil.fail();
    }

    /**
     * 一般适用于微信授权登录后将淘宝的数据修改进用户信息里面 淘宝授权登录修改用户信息
     *
     * @param registerInfo
     * @param request
     * @return
     */
    @PostMapping("/taobao")
    public Object taobaoAuthUpdateUserInfo(@RequestBody RegisterInfo registerInfo, HttpServletRequest request) {
        String userId = registerInfo.getUserId();
        String accessToken = null;
        MallUser user = userService.findById(userId);
        TaoBaoBaiChuanUserInfo taoBaoBaiChuanUserInfo = registerInfo.getTaoBaoBaiChuanUserInfo();
        logger.info("调用淘宝taoBaoBaiChuanUserInfo接口： " + JacksonUtils.bean2Jsn(taoBaoBaiChuanUserInfo));
        if (Objects.equals(user, null)) {
            return ResponseUtil.fail(AUTH_NAME_REGISTERED, "用户不存在请先注冊");
        }
        // {
        // "top_auth_token_create_response": {
        // "token_result":
        // "{\"w1_expires_in\":1800,\"refresh_token_valid_time\":1570338706126,\"taobao_user_nick\":\"%E4%BD%A0%E4%B8%AB%E7%9A%84%E4%BD%A0\",\"re_expires_in\":2592000,\"expire_time\":1570338706126,\"token_type\":\"Bearer\",\"access_token\":\"620190507b5eegie967a8f2799e7edfcad1bdca3a3c26fa1908045153\",\"taobao_open_uid\":\"AAE0lW1DAJZAyHpplmaQb8pT\",\"w1_valid\":1567748506126,\"refresh_token\":\"6201705fdc32dfh67b4c0c683b1ca60a5dcb6fb06a800b51908045153\",\"w2_expires_in\":0,\"w2_valid\":1567746706126,\"r1_expires_in\":1800,\"r2_expires_in\":0,\"r2_valid\":1567746706126,\"r1_valid\":1567748506126,\"taobao_user_id\":\"1908045153\",\"expires_in\":2592000}",
        // "request_id": "47qfvksf37am"
        // }
        // }
        MallUser uUser = new MallUser();
        //原生ios可以直接获取到accessToken，而uniapp是通过code获取accessToken
        try {

            String url = "https://eco.taobao.com/router/rest";// ( 获取Access Token )
            TaobaoClient client = new DefaultTaobaoClient(url, taoBaoProperties.getLianmengAppKey(),
                    taoBaoProperties.getLianmengAppSecret());
            TopAuthTokenCreateRequest req = new TopAuthTokenCreateRequest();
            req.setCode(registerInfo.getWxCode());
            TopAuthTokenCreateResponse rsp = client.execute(req);

            logger.info("调用淘宝AccessToken接口： " + JacksonUtils.bean2Jsn(rsp));

            // TopAuthToken topAuthToken = JacksonUtils.jsn2beanUnderscores(rsp.getBody(), TopAuthToken.class);
            String tokenResult = rsp.getTokenResult();
            if (tokenResult == null) {
                return ResponseUtil.fail();
            }

            Map<String, String> tokenResultMap = JacksonUtils.jsn2map(tokenResult, String.class, String.class);
            accessToken = MapUtils.getString(tokenResultMap, "access_token");

            if (accessToken != null) {
                uUser.setTbkAccessToken(accessToken);
                String saveurl = "https://eco.taobao.com/router/rest";
                TaobaoClient client1 = new DefaultTaobaoClient(saveurl, taoBaoProperties.getLianmengAppKey(),
                        taoBaoProperties.getLianmengAppSecret());
                TbkScPublisherInfoSaveRequest req1 = new TbkScPublisherInfoSaveRequest();
                req1.setRelationFrom("1");
                req1.setOfflineScene("4");
                req1.setOnlineScene("3");
                req1.setInviterCode("MTCQ32");
                req1.setInfoType(1L);
                // req1.setNote(taoBaoBaiChuanUserInfo.getNick());
                // req1.setRegisterInfo("{\"phoneNumber\":\"18801088599\",\"city\":\"江苏省\",\"province\":\"南京市\",\"location\":\"玄武区花园小区\",\"detailAddress\":\"5号楼3单元101室\",\"shopType\":\"社区店\",\"shopName\":\"全家便利店\",\"shopCertifyType\":\"营业执照\",\"certifyNumber\":\"111100299001\"}");
                TbkScPublisherInfoSaveResponse rspSave = client1.execute(req1, accessToken);
                Data data = rspSave.getData();
                if (!Objects.equals(data, null)) {
                    Long relationId = data.getRelationId();
                    Long specialId = data.getSpecialId();
                    if (relationId != null) {

                        MallUser tbUser = userService.queryByTbkRelationId(String.valueOf(relationId));
                        if (ObjectUtil.isNotNull(tbUser)) {
                            return ResponseUtil.badArgumentValue("此淘宝账号已授权过，请解绑对应账号或授权其他淘宝账号!");
                        }


                        logger.info("源渠道id： " + user.getTbkRelationId());
                        logger.info("现渠道id： " + relationId);
                        if (StringHelper.isNullOrEmpty(user.getTbkRelationId())) {
                            uUser.setTbkRelationId(relationId.toString());
                            user.setTbkRelationId(relationId.toString());
                        }

                    }
                    if (specialId != null) {
                        uUser.setTbkSpecialId(specialId.toString());
                        user.setTbkSpecialId(specialId.toString());
                    }
                    if (null != taoBaoBaiChuanUserInfo.getNick()) {
                        uUser.setTbNickname(taoBaoBaiChuanUserInfo.getNick());
                        user.setTbNickname(taoBaoBaiChuanUserInfo.getNick());
                    }
                } else {
                    return ResponseUtil.fail(602, rspSave.getSubMsg());
                }


                // 淘宝会员备案获取会员id
                taobaoMemberSave(accessToken, uUser, user, taoBaoBaiChuanUserInfo);


                uUser.setId(user.getId());
                // 存入pid
                user.setTbkPubId("mm_131454905_1564300091_110282750499");


                /** 修改用户数据 **/
                if (userService.updateById(uUser) > 0) {
                    Map<Object, Object> result = new HashMap<Object, Object>();
                    if (userService.updateById(user) > 0) {
                        /** 生成token返回给前端 **/
                        String token = UserTokenManager.generateToken(user.getId(), uUser.getTbkRelationId(),
                                uUser.getTbkSpecialId());
                        result.put("userInfo", user);
                        result.put("token", token);
                        return ResponseUtil.ok(result);
                    } else {
                        return ResponseUtil.fail(AUTH_NAME_REGISTERED, "用户数据修改失败");
                    }
                } else {
                    return ResponseUtil.fail(AUTH_NAME_REGISTERED, "用户数据修改失败");
                }
            } else {
                return ResponseUtil.fail(AUTH_NAME_REGISTERED, "淘宝授权异常");
            }
        } catch (Exception e) {
            logger.error("淘宝授权异常:" + e.getMessage(), e);
        }


        return ResponseUtil.ok();
    }

    /**
     * 淘宝授权登录注册用户信息
     *
     * @param registerInfo
     * @param request
     * @return
     */
    @PostMapping("register_taobao")
    public Object registerTaoBao(@RequestBody RegisterInfo registerInfo, HttpServletRequest request) {


        String username = registerInfo.getUsername();
        String password = registerInfo.getPassword();
        // 推荐码
        String invitationCode = registerInfo.getInvitationCode();
        // 微信授权的code
        String wxMpCode = registerInfo.getWxCode();

        String valCode = registerInfo.getValCode();

        String smsType = "APP_REGISTER";
        /**
         * 注册渠道
         */
        String registerChannel = registerInfo.getRegisterChannel();
        // 用户注册参数校验
        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password) || StringUtils.isEmpty(valCode)) {
            return ResponseUtil.badArgument();
        }

        MallPhoneValidation val = phoneService.getPhoneValidationByCode(username, valCode, smsType);

        if (val == null) {
            return ResponseUtil.fail(AUTH_NAME_REGISTERED, "验证码有误");
        }

        // 上级信息
        List<MallUser> parentUserList = userService.queryByInvitationCode(invitationCode);
        if (CollectionUtils.isEmpty(parentUserList)) {
            return ResponseUtil.fail(AUTH_NAME_REGISTERED, "邀请码不存在");
        }
        // MallInvitationCode codeDto = codeSercice.getCode(invitationCode);
        // if (codeDto==null) {
        // return ResponseUtil.fail(AUTH_NAME_REGISTERED, "邀请码不存在");
        // }


        List<MallUser> userList = userService.queryByUsername(username);
        if (userList.size() > 0) {
            return ResponseUtil.fail(AUTH_NAME_REGISTERED, "用户名已注册");
        }

        userList = userService.queryByMobile(username);
        if (userList.size() > 0) {
            return ResponseUtil.fail(AUTH_MOBILE_REGISTERED, "手机号已注册");
        }

        if (!RegexUtil.isMobileExact(username)) {
            return ResponseUtil.fail(AUTH_INVALID_MOBILE, "手机号格式不正确");
        }

        WxOAuth2UserInfo wxMpUser = null;
        String openId = null;

        try {
            MiniProgramUserInfo miniProgramUserInfo = null;
            TaoBaoBaiChuanUserInfo taoBaoBaiChuanUserInfo = null;

            MallUser user = null;
            user = new MallUser();

            String sessionKey = "";
            if ("miniProgram".equalsIgnoreCase(registerChannel) && !StringUtils.isEmpty(wxMpCode)) {

                miniProgramUserInfo = registerInfo.getMiniProgramUserInfo();
                WxMaJscode2SessionResult result = this.wxService.getUserService().getSessionInfo(wxMpCode);
                sessionKey = result.getSessionKey();
                openId = result.getOpenid();

            } else if ("wx".equalsIgnoreCase(registerChannel)) {

            } else if ("wxMp".equalsIgnoreCase(registerChannel) && !StringUtils.isEmpty(wxMpCode)) {

                // WxMpOAuth2AccessToken wxMpOAuth2AccessToken = wxMpService.oauth2getAccessToken(wxMpCode);
                // wxMpUser = wxMpService.oauth2getUserInfo(wxMpOAuth2AccessToken, null);

                // 授权方法改写_weixin-pay.version_3.9.0
                // WxMpOAuth2AccessToken wxMpOAuth2AccessToken =
                // wxMpService.getOAuth2Service().getAccessToken(wxMpCode);
                // 授权方法改写wx-java.version_4.0.0
                WxOAuth2AccessToken wxMpOAuth2AccessToken = wxMpService.getOAuth2Service().getAccessToken(wxMpCode);
                wxMpUser = wxMpService.getOAuth2Service().getUserInfo(wxMpOAuth2AccessToken, null);
                logger.info("微信数据获取 ：" + JacksonUtils.bean2Jsn(wxMpUser));
                // sessionKey = result.getSessionKey();
                openId = wxMpUser.getOpenid();

                if (openId == null) {
                    return ResponseUtil.fail(-1, "微信登录信息获取异常");
                }

            } else if ("taobao".equalsIgnoreCase(registerChannel)) {
                taoBaoBaiChuanUserInfo = registerInfo.getTaoBaoBaiChuanUserInfo();
                // {
                // "top_auth_token_create_response": {
                // "token_result":
                // "{\"w1_expires_in\":1800,\"refresh_token_valid_time\":1570338706126,\"taobao_user_nick\":\"%E4%BD%A0%E4%B8%AB%E7%9A%84%E4%BD%A0\",\"re_expires_in\":2592000,\"expire_time\":1570338706126,\"token_type\":\"Bearer\",\"access_token\":\"620190507b5eegie967a8f2799e7edfcad1bdca3a3c26fa1908045153\",\"taobao_open_uid\":\"AAE0lW1DAJZAyHpplmaQb8pT\",\"w1_valid\":1567748506126,\"refresh_token\":\"6201705fdc32dfh67b4c0c683b1ca60a5dcb6fb06a800b51908045153\",\"w2_expires_in\":0,\"w2_valid\":1567746706126,\"r1_expires_in\":1800,\"r2_expires_in\":0,\"r2_valid\":1567746706126,\"r1_valid\":1567748506126,\"taobao_user_id\":\"1908045153\",\"expires_in\":2592000}",
                // "request_id": "47qfvksf37am"
                // }
                // }

                String url = "https://eco.taobao.com/router/rest";// ( 获取Access Token )
                TaobaoClient client = new DefaultTaobaoClient(url, taoBaoProperties.getLianmengAppKey(),
                        taoBaoProperties.getLianmengAppSecret());
                TopAuthTokenCreateRequest req = new TopAuthTokenCreateRequest();
                req.setCode(registerInfo.getWxCode());
                TopAuthTokenCreateResponse rsp = client.execute(req);

                logger.info("调用淘宝AccessToken接口： " + JacksonUtils.bean2Jsn(rsp));

                // TopAuthToken topAuthToken = JacksonUtils.jsn2beanUnderscores(rsp.getBody(), TopAuthToken.class);
                String tokenResult = rsp.getTokenResult();
                if (tokenResult == null) {
                    return ResponseUtil.fail();
                }

                // String tokenRes = topAuthToken.getTopAuthTokenCreateResponse().getTokenResult();
                // if(tokenRes==null){
                // return ResponseUtil.fail();
                // }
                //
                // String newRes = tokenRes.replaceAll("\\\"","");

                Map<String, String> tokenResultMap = JacksonUtils.jsn2map(tokenResult, String.class, String.class);
                String accessToken = MapUtils.getString(tokenResultMap, "access_token");

                if (accessToken != null) {
                    user.setTbkAccessToken(accessToken);
                    String saveurl = "https://eco.taobao.com/router/rest";
                    TaobaoClient client1 = new DefaultTaobaoClient(saveurl, taoBaoProperties.getLianmengAppKey(),
                            taoBaoProperties.getLianmengAppSecret());
                    TbkScPublisherInfoSaveRequest req1 = new TbkScPublisherInfoSaveRequest();
                    req1.setRelationFrom("1");
                    req1.setOfflineScene("4");
                    req1.setOnlineScene("1");
                    req1.setInviterCode("VEMJP2");
                    req1.setInfoType(1L);
                    req1.setNote(taoBaoBaiChuanUserInfo.getNick());
                    // req1.setRegisterInfo("{\"phoneNumber\":\"18801088599\",\"city\":\"江苏省\",\"province\":\"南京市\",\"location\":\"玄武区花园小区\",\"detailAddress\":\"5号楼3单元101室\",\"shopType\":\"社区店\",\"shopName\":\"全家便利店\",\"shopCertifyType\":\"营业执照\",\"certifyNumber\":\"111100299001\"}");
                    TbkScPublisherInfoSaveResponse rspSave = client1.execute(req1, accessToken);
                    Data data = rspSave.getData();

                    Long relationId = data.getRelationId();
                    Long specialId = data.getSpecialId();
                    if (relationId != null) {
                        user.setTbkRelationId(relationId.toString());
                    }
                    if (specialId != null) {
                        user.setTbkSpecialId(specialId.toString());
                    }
                }
            }
            // 记录推荐码

            String parentId = parentUserList.get(0).getId();// codeDto.getUserId();
            // String parentId = fenxiaoUser.getId();
            // String myI = parentId + myInvitationCode;

            MallInvitationCode code = codeSercice.generateCode();
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
            String encodedPassword = encoder.encode(password);

            user.setUsername(username);
            user.setPassword(encodedPassword);
            user.setMobile(username);
            user.setIsDistribut("1");
            if (wxMpUser != null) {
                user.setWeixinOpenid(openId);
                user.setAvatar(wxMpUser.getHeadImgUrl());
                user.setNickname(wxMpUser.getNickname());
                user.setGender(wxMpUser.getSex().toString());
            } else {
                if ("miniProgram".equalsIgnoreCase(registerChannel) && miniProgramUserInfo != null) {
                    // user.setWeixinOpenid(openId);
                    user.setMiniProgramOpenid(openId);
                    user.setAvatar(miniProgramUserInfo.getAvatarUrl());
                    user.setNickname(miniProgramUserInfo.getNickName());
                    user.setGender(miniProgramUserInfo.getGender().toString());
                } else if ("taobao".equalsIgnoreCase(registerChannel) && taoBaoBaiChuanUserInfo != null) {
                    user.setAvatar(taoBaoBaiChuanUserInfo.getAvatar());
                    user.setNickname(taoBaoBaiChuanUserInfo.getNick());
                } else {
                    user.setNickname(username);
                }
            }

            user.setUserLevel((byte) 0);
            user.setInvitationCode(code.getCode());
            // 记录上下级关系
            user.setFirstLeader(parentId.toString());
            user.setStatus((byte) 0);
            user.setLastLoginTime(LocalDateTime.now());
            user.setLastLoginIp(IpUtil.getIpAddr(request));
            user.setSessionKey(sessionKey);
            user.setLevelId("0");
            userService.add(user);

            // 开户
            if (user.getId() != null) {
                logger.info("开户成功..............");
                accountService.accountTradeCreateAccount(user.getId().toString());
            }
            // 给新用户发送注册优惠券 2C 用户需要这个逻辑
            // couponAssignService.assignForRegister(user.getId());

            // userInfo
            // UserInfo userInfo = new UserInfo();
            // userInfo.setNickName(username);
            // userInfo.setAvatarUrl(user.getAvatar());
            MallPhoneValidation updateVal = new MallPhoneValidation();
            updateVal.setId(val.getId());
            updateVal.setValState("00");
            phoneService.update(updateVal);

            // 修改邀请码使用状态
            MallInvitationCode uCode = new MallInvitationCode();
            uCode.setId(code.getId());
            uCode.setUseStatus("111");
            codeSercice.updateMallInvitationCode(uCode);
            // token
            String token = UserTokenManager.generateToken(user.getId(), user.getTbkRelationId(),
                    user.getTbkSpecialId());

            Map<Object, Object> result = new HashMap<Object, Object>();
            result.put("token", token);
            result.put("userInfo", user);
            return ResponseUtil.ok(result);
        } catch (Exception e) {
            logger.error("分销商注册异常: " + e.getMessage(), e);
        }

        return ResponseUtil.fail();
    }

    /**
     * 请求验证码
     * <p>
     * TODO 这里需要一定机制防止短信验证码被滥用
     *
     * @param body 手机号码 { mobile: xxx, type: xxx }
     * @return
     */
    @PostMapping("captcha")
    public Object captcha(@LoginUser String userId, @RequestBody String body) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        String phoneNumber = JacksonUtil.parseString(body, "mobile");
        String captchaType = JacksonUtil.parseString(body, "type");
        if (StringUtils.isEmpty(phoneNumber)) {
            return ResponseUtil.badArgument();
        }
        if (!RegexUtil.isMobileExact(phoneNumber)) {
            return ResponseUtil.badArgumentValue();
        }
        if (StringUtils.isEmpty(captchaType)) {
            return ResponseUtil.badArgument();
        }

        if (!notifyService.isSmsEnable()) {
            return ResponseUtil.fail(AUTH_CAPTCHA_UNSUPPORT, "小程序后台验证码服务不支持");
        }
        String code = CharUtil.getRandomNum(6);
        // TODO
        // 根据type发送不同的验证码
        notifyService.notifySmsTemplate(phoneNumber, NotifyType.CAPTCHA, new String[]{code});

        boolean successful = CaptchaCodeManager.addToCache(phoneNumber, code);
        if (!successful) {
            return ResponseUtil.fail(AUTH_CAPTCHA_FREQUENCY, "验证码未超时1分钟，不能发送");
        }

        return ResponseUtil.ok();
    }

    /**
     * 账号密码重置
     *
     * @param body    请求内容 { password: xxx, mobile: xxx code: xxx } 其中code是手机验证码，目前还不支持手机短信验证码
     * @param request 请求对象
     * @return 登录结果 成功则 { errno: 0, errmsg: '成功' } 失败则 { errno: XXX, errmsg: XXX }
     */
    @PostMapping("reset")
    public Object reset(@RequestBody String body, HttpServletRequest request) {

        String password = JacksonUtil.parseString(body, "password");
        String mobile = JacksonUtil.parseString(body, "mobile");
        String code = JacksonUtil.parseString(body, "valCode");

        if (mobile == null || code == null || password == null) {
            return ResponseUtil.badArgument();
        }

        // 判断验证码是否正确
        // String cacheCode = CaptchaCodeManager.getCachedCaptcha(mobile);

        String smsType = "FORGET";
        MallPhoneValidation val = phoneService.getPhoneValidationByCode(mobile, code, smsType);

        if (val == null) {
            return ResponseUtil.fail(AUTH_NAME_REGISTERED, "验证码有误");
        }

        List<MallUser> userList = userService.queryByMobile(mobile);
        MallUser user = null;
        if (userList.size() > 1) {
            return ResponseUtil.serious();
        } else if (userList.size() == 0) {
            return ResponseUtil.fail(AUTH_MOBILE_UNREGISTERED, "手机号未注册");
        } else {
            user = userList.get(0);
        }

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String encodedPassword = encoder.encode(password);
        user.setPassword(encodedPassword);

        if (userService.updateById(user) == 0) {
            return ResponseUtil.updatedDataFailed();
        }

        return ResponseUtil.ok();
    }

    /**
     * 账号手机号码重置
     *
     * @param body    请求内容 { password: xxx, mobile: xxx code: xxx } 其中code是手机验证码，目前还不支持手机短信验证码
     * @param request 请求对象
     * @return 登录结果 成功则 { errno: 0, errmsg: '成功' } 失败则 { errno: XXX, errmsg: XXX }
     */
    @PostMapping("resetPhone")
    public Object resetPhone(@LoginUser String userId, @RequestBody String body, HttpServletRequest request) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        String password = JacksonUtil.parseString(body, "password");
        String mobile = JacksonUtil.parseString(body, "mobile");
        String code = JacksonUtil.parseString(body, "code");

        if (mobile == null || code == null || password == null) {
            return ResponseUtil.badArgument();
        }

        // 判断验证码是否正确
        String cacheCode = CaptchaCodeManager.getCachedCaptcha(mobile);
        if (cacheCode == null || cacheCode.isEmpty() || !cacheCode.equals(code))
            return ResponseUtil.fail(AUTH_CAPTCHA_UNMATCH, "验证码错误");

        List<MallUser> userList = userService.queryByMobile(mobile);
        MallUser user = null;
        if (userList.size() > 1) {
            return ResponseUtil.fail(AUTH_MOBILE_REGISTERED, "手机号已注册");
        }
        user = userService.findById(userId);

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        if (!encoder.matches(password, user.getPassword())) {
            return ResponseUtil.fail(AUTH_INVALID_ACCOUNT, "账号密码不对");
        }

        user.setMobile(mobile);
        if (userService.updateById(user) == 0) {
            return ResponseUtil.updatedDataFailed();
        }

        return ResponseUtil.ok();
    }

    /**
     * 账号信息更新
     *
     * @param body    请求内容 { password: xxx, mobile: xxx code: xxx } 其中code是手机验证码，目前还不支持手机短信验证码
     * @param request 请求对象
     * @return 登录结果 成功则 { errno: 0, errmsg: '成功' } 失败则 { errno: XXX, errmsg: XXX }
     */
    @PostMapping("profile")
    public Object profile(@LoginUser String userId, @RequestBody String body, HttpServletRequest request) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        String avatar = JacksonUtil.parseString(body, "avatar");
        Byte gender = JacksonUtil.parseByte(body, "gender");
        String nickname = JacksonUtil.parseString(body, "nickname");

        MallUser user = userService.findById(userId);
        if (!StringUtils.isEmpty(avatar)) {
            user.setAvatar(avatar);
        }
        if (gender != null) {
            user.setGender(gender.toString());
        }
        if (!StringUtils.isEmpty(nickname)) {
            user.setNickname(nickname);
        }

        if (userService.updateById(user) == 0) {
            return ResponseUtil.updatedDataFailed();
        }

        return ResponseUtil.ok();
    }

    /**
     * 微信手机号码绑定
     *
     * @param userId
     * @param body
     * @return
     */
    @PostMapping("bindPhone")
    public Object bindPhone(@LoginUser String userId, @RequestBody String body) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        MallUser user = userService.findById(userId);
        String encryptedData = JacksonUtil.parseString(body, "encryptedData");
        String iv = JacksonUtil.parseString(body, "iv");
        WxMaPhoneNumberInfo phoneNumberInfo =
                this.wxService.getUserService().getPhoneNoInfo(user.getSessionKey(), encryptedData, iv);
        String phone = phoneNumberInfo.getPhoneNumber();
        user.setMobile(phone);
        if (userService.updateById(user) == 0) {
            return ResponseUtil.updatedDataFailed();
        }
        return ResponseUtil.ok();
    }

    @PostMapping("logout")
    public Object logout(@LoginUser String userId) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        return ResponseUtil.ok();
    }

    @GetMapping("info")
    public Object info(@LoginUser String userId) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }

        Map<Object, Object> data = new HashMap<Object, Object>();
        MallUser user = userService.findById(userId);

        if(!StringUtils.isEmpty(user.getDirectLeader())){
            // 用户商户关联表
            QueryWrapper<MallUserMerchant> mallUserMerchantWrapper = new QueryWrapper<MallUserMerchant>();
            mallUserMerchantWrapper.eq("user_id", userId);
            List<MallUserMerchant> mallUserMerchant = mallUserMerchantService.list(mallUserMerchantWrapper);

            MallMerchant mallMerchant = null;
            for (MallUserMerchant temp: mallUserMerchant) {
                // 商户信息
                QueryWrapper<MallMerchant> merchantWrapper = new QueryWrapper<MallMerchant>();
                merchantWrapper.eq("status", 0);// 正常营业
                merchantWrapper.eq("audit_status", 1);// 审核通过
                merchantWrapper.eq("id",temp.getMerchantId());
                mallMerchant = mallMerchantService.getOne(merchantWrapper);

                if(mallMerchant != null){
                    break;
                }
            }

            data.put("directMerchant", mallMerchant);
        }
        data.put("nickName", user.getNickname());
        data.put("avatar", user.getAvatar());
        data.put("gender", user.getGender());
        data.put("mobile", user.getMobile());

        return ResponseUtil.ok(data);
    }


    @GetMapping("getRegisterNumber")
    public Object getRegisterNumber(@LoginUser String userId) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }

        int count = userService.count();

        return ResponseUtil.ok(count);
    }


    @GetMapping("getShardingNumber")
    public Object getShardingNumber(@LoginUser String userId) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        MallOrderExample example = new MallOrderExample();
        MallOrderExample.Criteria criteria = example.createCriteria();
        criteria.andHuidEqualTo("sharding");
//        int count = MallOrderMapper.selectCount(new QueryWrapper<MallOrder>().eq("huid", "sharding"));
        int count = (int) MallOrderMapper.countByExample(example);
        return ResponseUtil.ok(count);
    }

    @PostMapping("batchInsertOrder")
    public Object batchInsertOrder(@LoginUser String userId, @RequestBody Map<String, String> body) throws Exception {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }

        Integer number = Integer.valueOf(body.get("body"));

        List<String> userIds = userService.findUserId(number);


        CompletableFuture[] futures = new CompletableFuture[number];

        for (int i = 0; i < userIds.size(); i++) {

            int finalI = i;
            futures[i] = CompletableFuture.runAsync(() -> {

                MallOrder order = new MallOrder();
                order.setUserId(userIds.get(finalI));
                order.setOrderSn(GraphaiIdGenerator.nextId("OrderSN"));
                order.setOrderStatus(OrderUtil.STATUS_CREATE);
                order.setMessage("sharding测试商品");
                // 面值
                order.setGoodsPrice(new BigDecimal(33));
                order.setFreightPrice(new BigDecimal(0.00));
                order.setCouponPrice(new BigDecimal(0.00));
                order.setIntegralPrice(new BigDecimal(0.00));
                // 订单总费用
                BigDecimal actualPrice = new BigDecimal(33).multiply(new BigDecimal(0.5));
                order.setActualPrice(actualPrice);
                order.setOrderPrice(actualPrice);
                order.setHuid("sharding");
                order.setGrouponPrice(new BigDecimal(0.00)); // 团购价格
                order.setBid(AccountStatus.BID_5);

                // 添加订单表
                orderService.add(order);
                logger.info("插入完成" + userIds.get(finalI));

            }, JDiworkerConfig.CUSTOMER_COMMON_POOL);
        }

        CompletableFuture.allOf(futures).get();

        return ResponseUtil.ok();
    }


    /**
     * 分库分表批量插入测试
     *
     * @param userId
     * @param body
     * @return
     * @throws Exception
     */
    @PostMapping("batchRegister")
    public Object batchRegister(@LoginUser String userId, @RequestBody Map<String, String> body) throws Exception {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }

        Integer number = Integer.valueOf(body.get("body"));
//        int count = userService.count();
//        logger.info("--------------------总量为： "+count);
//
        CompletableFuture[] futures = new CompletableFuture[number];

        for (int i = 0; i < number; i++) {
            futures[i] = CompletableFuture.runAsync(() -> {
                try {
                    this.registerTest();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }, JDiworkerConfig.CUSTOMER_COMMON_POOL);
        }

        CompletableFuture.allOf(futures).get();
//
//        count = userService.count();
//        logger.info("--------------------总量为： "+count);

        return ResponseUtil.ok();
    }


    /**
     * (1)多線程插入用戶
     */
    public Object registerTest() throws Exception {
        StringBuffer mobileBuffer = new StringBuffer(10);
        mobileBuffer.append(1);
        for (int i = 0; i < 10; i++) {
            mobileBuffer.append((int) (Math.random() * (10)));
        }
        String mobile = mobileBuffer.toString(); // 手机号
        String clientId = UUID32.getInstance().genId();// 个推用户标识

        List<MallUser> userList = userService.queryByMobile(mobile);
        MallUser user = new MallUser();
        if (userList.size() == 0) {
            // 若用户不存在，则注册成新用户
            user.setMobile(mobile);
            user.setNickname(mobile);
            user.setLastLoginTime(LocalDateTime.now());
            user.setDirectLeader("4963561144116576256");

            StringBuffer ip = new StringBuffer(15);
            ip.append(1);
            for (int i = 0; i < 11; i++) {
                ip.append((int) (Math.random() * (10)));
                if (i == 1 || i == 4 || i == 7) {
                    ip.append(".");
                }
            }

            user.setLastLoginIp(ip.toString());
            user.setLevelId("0");
            user.setIsDistribut("0");
            user.setStatus(new Byte("0"));
            user.setSource("0");
            user.setClientId(clientId);
            user.setRegisterDevice((byte) 2); // 注册设备
            user.setChannelId(MallUserEnum.CHANNEL_ID_4.getCode());
            user.setAvatar(NameUtil.getUserUrl());
            userService.add(user);
            // 开户
            if (user.getId() != null) {
                logger.info("开户成功..............");
                accountService.accountTradeCreateAccount(user.getId());
            }
            return ResponseUtil.ok();

        } else {
            return ResponseUtil.badArgumentValue("存在多条记录！");
        }


    }

    /**
     * (1)通过手机验证码登录/注册
     *
     * @param body 请求内容 { mobile: xxx, code: xxx, clientId: xxx } ,其中code为手机验证码
     */
    @PostMapping("/loginByPhone")
    public Object loginByPhone(@RequestBody String body, HttpServletRequest request) throws Exception {
        try {
            // 设备类型
            String mobileTye = request.getHeader("User-Agent");
            String mobile = JacksonUtil.parseString(body, "mobile"); // 手机号
            String code = JacksonUtil.parseString(body, "code"); // 验证码
            String clientId = JacksonUtil.parseString(body, "clientId");// 个推用户标识
            String version = JacksonUtil.parseString(body, "version"); // 版本号
            if (mobile == null || code == null) {
                return ResponseUtil.badArgument();
            }
            boolean isDevice = false;
//            判断导购机身份
            if (mobileTye.contains("ZC-328")) {
                isDevice =true;
            }
//        if (mobile == null) {
//            return ResponseUtil.badArgument();
//        }

            // 通过手机号、手机验证码 判断验证码是否正确
            String smsType = "APP_LOGIN";
            MallPhoneValidation mallPhoneValidation = phoneService.getPhoneValidationByCode(mobile, code,
                    smsType);

            if (mallPhoneValidation == null) {
                return ResponseUtil.fail(AUTH_CAPTCHA_UNMATCH, "验证码错误");
            }
            MallUser mallUser = null;
            if (isDevice) {
                List<MallUser> mallUserList = userService.queryByMobile(mobile);
                if (null == mallUserList || mallUserList.size() < 1
                        || (!AccountStatus.LEVEL_9.equalsIgnoreCase(mallUserList.get(0).getLevelId())
                        && !AccountStatus.LEVEL_10.equalsIgnoreCase(mallUserList.get(0).getLevelId())
                        && !AccountStatus.LEVEL_11.equalsIgnoreCase(mallUserList.get(0).getLevelId()))) {
                    return ResponseUtil.fail(AUTH_CAPTCHA_UNMATCH, "经销商账户不存在");
                }
                mallUser = mallUserList.get(0);
                mobile += "device";
            }
            Byte registerDevice = null;
            if (mobileTye.contains("iPhone")) {
                registerDevice = (byte) 2;
            } else if (mobileTye.contains("Android")) {
                registerDevice = (byte) 1;
            }
            List<MallUser> userList = userService.queryByMobile(mobile);
            MallUser user = new MallUser();
            if (userList.size() == 0) {
                // 若用户不存在，则注册成新用户
                user.setMobile(mobile);
                user.setUsername(mobile);
                BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
                String encodedPassword = encoder.encode(mobile);
                user.setPassword(encodedPassword);
                user.setGender("0");
                user.setSource("H5");
                user.setNickname(NameUtil.mobileEncrypt(user.getMobile()));
                user.setAvatar("https://51fangtao.oss-cn-shenzhen.aliyuncs.com/logo.png");
                user.setLastLoginTime(LocalDateTime.now());
                user.setLastLoginIp(IpUtil.getIpAddr(request));
                user.setLevelId("0");
                if (isDevice && null != mallUser) {
                    user.setDirectLeader(mallUser.getId());
                    user.setFirstLeader(mallUser.getId());
                    user.setLockTime(LocalDateTime.now());
                    user.setUsername("导购机用户");
                }
                // 是否为分销商,0否1是
                user.setIsDistribut("0");
                if (!StringUtils.isEmpty(clientId)) {
                    //清空其他用户所持有的相同clientId,避免设备接收到不同用户的消息推送
                    userService.updateUserClient(clientId);
                    user.setClientId(clientId);
                }
                user.setRegisterDevice(registerDevice); // 注册设备
                userService.add(user);
                // 开户
                if (user.getId() != null) {
                    logger.info("开户成功..............");
                    accountService.accountTradeCreateAccount(user.getId().toString());
                }
            } else if (userList.size() == 1) {
                user = userList.get(0);
                user.setNickname(NameUtil.mobileEncrypt(user.getMobile()));
                user.setLastLoginTime(LocalDateTime.now());
                user.setLastLoginIp(IpUtil.getIpAddr(request));
                if (!StringUtils.isEmpty(clientId)) {
                    userService.updateUserClient(clientId);
                    user.setClientId(clientId);
                }
                if (StringUtils.isEmpty(user.getRegisterDevice())) {
                    user.setRegisterDevice(registerDevice); // 注册设备
                }
                if (userService.updateById(user) == 0) {
                    return ResponseUtil.updatedDataFailed();
                }
            } else {
                return ResponseUtil.badArgumentValue("存在多条记录！");
            }

            // 使验证码失效
            MallPhoneValidation mallPhoneValidation1 = new MallPhoneValidation();
            mallPhoneValidation1.setId(mallPhoneValidation.getId());
            mallPhoneValidation1.setValState("00");
            phoneService.update(mallPhoneValidation1);


            /** 修改用户达标状态 **/
            RetrialUtils.updateUserEligible(user);
            logger.info("修改用户达标状态已完成");

            BmdesignUserLevel bmdesignUserLevel = bmdesignUserLevelService.selectBmdesignUserLevelById(user.getLevelId());
            logger.info("获取用户等级完成");

            // token
            String token = UserTokenManager.generateToken(user.getId(), user.getTbkRelationId(), user.getTbkSpecialId());
            Map<Object, Object> result = new HashMap<Object, Object>();
            result.put("token", token);
            logger.info("用户token=" + token);

            // 查询是否是审核状态
            boolean isExamine = RetrialUtils.examine(request, version);
            if (isExamine) {
                user.setIsActivate((byte) 1);
            }
            /**禁用提示语*/
            Map<String, String> stringStringMap = systemConfigService.listDis();
            String mallTips = stringStringMap.get("mall_disable_tips");
            if (Objects.nonNull(user)) {
                if (Objects.nonNull(user.getStatus())) {
                    if (MallUserEnum.STATUS_1.strCode2Byte().equals(user.getStatus()))
                        return ResponseUtil.fail(mallTips);
                }
            }
            result.put("userInfo", user);
            result.put("userLevel", bmdesignUserLevel);
            if (!isExamine) {
                // if (StringUtils.isEmpty(user.getDirectLeader())) {
                // return ResponseUtil.other(1000, "用户未被邀请！", result);
                // }
            }
            return ResponseUtil.ok(result);
        } catch (Exception ex) {
            logger.info("登录异常：" + ex.getMessage(), ex);
            ex.printStackTrace();
            return ResponseUtil.fail();
        }
    }

    // todo 废弃
    @PostMapping("/login_by_phone")
    public Object login_by_phone(@RequestBody String body, HttpServletRequest request) throws Exception {
        String mobile = JacksonUtil.parseString(body, "mobile"); // 手机号
        String code = JacksonUtil.parseString(body, "code"); // 验证码
        String clientId = JacksonUtil.parseString(body, "clientId");// 个推用户标识
        String version = JacksonUtil.parseString(body, "version"); // 版本号
        if (mobile == null || code == null) {
            return ResponseUtil.badArgument();
        }
        // 通过手机号、手机验证码 判断验证码是否正确
        String smsType = "APP_LOGIN";
        MallPhoneValidation mallPhoneValidation = phoneService.getPhoneValidationByCode(mobile, code, smsType);
        if (mallPhoneValidation == null) {
            return ResponseUtil.fail(AUTH_CAPTCHA_UNMATCH, "验证码错误");
        }


        // 设备类型
        String mobileTye = request.getHeader("User-Agent");
        Byte registerDevice = null;
        if (mobileTye.contains("iPhone")) {
            registerDevice = (byte) 2;
        } else if (mobileTye.contains("Android")) {
            registerDevice = (byte) 1;
        }
        List<MallUser> userList = userService.queryByMobile(mobile);
        MallUser user = new MallUser();
        if (userList.size() == 0) {
            // 若用户不存在，则注册成新用户
            user.setMobile(mobile);
            user.setNickname(mobile);
            user.setLastLoginTime(LocalDateTime.now());
            user.setLastLoginIp(IpUtil.getIpAddr(request));
            user.setLevelId("0");
            user.setIsDistribut("0");
            if (!StringUtils.isEmpty(clientId)) {
                user.setClientId(clientId);
            }
            user.setRegisterDevice(registerDevice); // 注册设备
            userService.add(user);
            // 开户
            if (user.getId() != null) {
                logger.info("开户成功..............");
                accountService.accountTradeCreateAccount(user.getId().toString());
            }
            // 消息推送
            PushUtils.pushMessage(user.getId(), AccountStatus.PUSH_TITLE_REGISTER, AccountStatus.PUSH_CONTENT_REGISTER,
                    2, null);

        } else if (userList.size() == 1) {
            user = userList.get(0);
            user.setLastLoginTime(LocalDateTime.now());
            user.setLastLoginIp(IpUtil.getIpAddr(request));
            if (!StringUtils.isEmpty(clientId)) {
                user.setClientId(clientId);
            }
            if (StringUtils.isEmpty(user.getRegisterDevice())) {
                user.setRegisterDevice(registerDevice); // 注册设备
            }
            if (userService.updateById(user) == 0) {
                return ResponseUtil.updatedDataFailed();
            }
        } else {
            return ResponseUtil.badArgumentValue("存在多条记录！");
        }

        // 使验证码失效
        MallPhoneValidation mallPhoneValidation1 = new MallPhoneValidation();
        mallPhoneValidation1.setId(mallPhoneValidation.getId());
        mallPhoneValidation1.setValState("00");
        phoneService.update(mallPhoneValidation1);


        /** 修改用户达标状态 **/
        RetrialUtils.updateUserEligible(user);

        BmdesignUserLevel bmdesignUserLevel = bmdesignUserLevelService.selectBmdesignUserLevelById(user.getLevelId());
        // token
        String token = UserTokenManager.generateToken(user.getId(), user.getTbkRelationId(), user.getTbkSpecialId());
        Map<Object, Object> result = new HashMap<Object, Object>();
        result.put("token", token);


        // 查询是否是审核状态
        boolean isExamine = RetrialUtils.examine(request, version);
        if (isExamine) {
            user.setIsActivate((byte) 1);
        }
        result.put("userInfo", user);
        result.put("userLevel", bmdesignUserLevel);
        if (!isExamine) {
            if (StringUtils.isEmpty(user.getDirectLeader())) {
                return ResponseUtil.other(1000, "用户未被邀请！", result);
            }
        }
        return ResponseUtil.ok(result);
    }

    /**
     * (2)手机一键登录/注册
     */
    @PostMapping("/accessPhone")
    public Object accessPhone(@RequestBody String body, HttpServletRequest request) {
        String token = JacksonUtil.parseString(body, "token");
        String clientId = JacksonUtil.parseString(body, "clientId");// 个推用户标识
        String appVersion = JacksonUtil.parseString(body, "version"); // 版本号
        if (StringUtils.isEmpty(token)) {
            return ResponseUtil.badArgumentValue("未获取到token！");
        }
        Map<String, Object> resultMap = new HashMap<>();
        try {
            /** 获取本机手机号码 **/
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmssSSS");
            String appid = appId;
            String appsecret = appSecret;
            String version = "2.0";
            String msgid = RandomUtils.genRandomNum(16);
            String systemtime = dateFormat.format(new Date());
            String strictcheck = "1";
            String param = appid + version + msgid + systemtime + strictcheck + token + appsecret;
            String sign = EncryptionUtil.encryptMD5(param);
            logger.info("签名1：" + sign);
            String encryptionalgorithm = "";
            String expandparams = "";


            Map<String, String> paramMap = new HashMap<>();
            paramMap.put("encryptionalgorithm", encryptionalgorithm);
            paramMap.put("expandparams", expandparams);
            paramMap.put("appid", appid);
            paramMap.put("sign", sign);
            paramMap.put("strictcheck", strictcheck);
            paramMap.put("msgid", msgid);
            paramMap.put("systemtime", systemtime);
            paramMap.put("version", version);
            paramMap.put("token", token);
            String jsonStr = JacksonUtils.bean2Jsn(paramMap);
            logger.info("请求参数：" + jsonStr);

            // 参数封装请求接口
            String postResult = HttpUtil.doPost("https://www.cmpassport.com/unisdk/rsapi/loginTokenValidate", jsonStr);
            logger.info("返回参数：" + postResult);
            resultMap = JacksonUtils.jsn2map(postResult, String.class, Object.class);

            String resultCode = String.valueOf(resultMap.get("resultCode"));
            if (resultCode.equals("103000")) {
                // 若请求成功,则直接通过手机号登录/注册
                String msisdn = String.valueOf(resultMap.get("msisdn"));
                logger.info("手机号获取成功,手机号码：" + msisdn);

                // 设备类型
                String mobileTye = request.getHeader("User-Agent");
                Byte registerDevice = null;
                if (mobileTye.contains("iPhone")) {
                    registerDevice = (byte) 2;
                } else if (mobileTye.contains("Android")) {
                    registerDevice = (byte) 1;
                }
                // 登陆用户
                return getLoginUser(msisdn, clientId, registerDevice, mobileTye, appVersion, request);
            }

        } catch (Exception e) {
            logger.error("生成签名失败", e);
        }

        return resultMap;
    }


    /**
     * uni云函数一键登陆
     */
    @PostMapping("/appYhsLogin")
    public Object appYhsLogin(@RequestBody String body, HttpServletRequest request) {
        String access_token = JacksonUtil.parseString(body, "access_token");
        String openid = JacksonUtil.parseString(body, "openid");
        String clientId = JacksonUtil.parseString(body, "clientId");// 个推用户标识
        String appVersion = JacksonUtil.parseString(body, "version"); // 版本号
        if (StringUtils.isEmpty(access_token)) {
            return ResponseUtil.badArgumentValue("未获取到token！");
        }
        JSONObject jsonObject = null;
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("access_token", access_token);
        paramsMap.put("openid", openid);
        String paramsStr = JSONUtil.toJsonStr(paramsMap);
        try {
            // 参数封装请求接口
            logger.info("uni云函数登陆请求参数：" + paramsStr);
            String postResult = com.graphai.commons.util.httprequest.HttpUtil.sendPostJson(APP_YHS_LOGIN_URL, paramsStr);
            logger.info("uni云函数登陆返回结果：" + postResult);
            jsonObject = JSONUtil.parseObj(postResult);
            Object res = jsonObject.getObj("res");
            if (res != null) {
                JSONObject resObj = JSONUtil.parseObj(res);
                Integer code = resObj.getInt("code");
                if (null != code && 0 == code) {
                    String phoneNumber = resObj.getStr("phoneNumber");
                    // 若请求成功,则直接通过手机号登录/注册
                    logger.info("手机号获取成功,手机号码：" + phoneNumber);
                    // 设备类型
                    String mobileTye = request.getHeader("User-Agent");
                    Byte registerDevice = null;
                    if (mobileTye.contains("iPhone")) {
                        registerDevice = (byte) 2;
                    } else if (mobileTye.contains("Android")) {
                        registerDevice = (byte) 1;
                    }
                    return getLoginUser(phoneNumber, clientId, registerDevice, mobileTye, appVersion, request);
                }
            } else {
                Object error = jsonObject.getObj("error");
                if (null != error) {
                    JSONObject object = JSONUtil.parseObj(error);
                    return ResponseUtil.fail(502, object.getStr("message"));
                }
            }
        } catch (Exception e) {
            logger.error("生成签名失败", e);
        }
        return ResponseUtil.other(502, "登陆失败", jsonObject);
    }

    /**
     * 登陆用户
     *
     * @param phoneNumber    手机号
     * @param clientId
     * @param registerDevice 设备ID
     * @param mobileTye      手机类型
     * @param appVersion     App版本
     * @param request        请求req
     * @return
     * @throws Exception
     */
    private Object getLoginUser(String phoneNumber, String clientId, Byte registerDevice, String mobileTye,
                                String appVersion, HttpServletRequest request) throws Exception {
        MallUser user = new MallUser();
        List<MallUser> userList = userService.queryByMobile(phoneNumber);
        if (userList.size() == 0) {
            // 若用户不存在，则注册成新用户
            user.setMobile(phoneNumber);
            user.setNickname(phoneNumber);
            user.setLastLoginTime(LocalDateTime.now());
            user.setLastLoginIp(IpUtil.getIpAddr(request));
            user.setLevelId("0");
            user.setIsDistribut("0");
            if (!StringUtils.isEmpty(clientId)) {
                userService.updateUserClient(clientId);
                user.setClientId(clientId);
            }
            user.setRegisterDevice(registerDevice); // 注册设备
            userService.add(user);
            // 开户
            if (user.getId() != null) {
                logger.info("开户成功..............");
                accountService.accountTradeCreateAccount(user.getId().toString());
            }
            // 消息推送
            // PushUtils.pushMessage(user.getId(), AccountStatus.PUSH_TITLE_REGISTER,
            // AccountStatus.PUSH_CONTENT_REGISTER, 2, null);

        } else if (userList.size() == 1) {
            user = userList.get(0);
            user.setLastLoginTime(LocalDateTime.now());
            user.setLastLoginIp(IpUtil.getIpAddr(request));
            if (!StringUtils.isEmpty(clientId)) {
                userService.updateUserClient(clientId);
                user.setClientId(clientId);
            }
            if (StringUtils.isEmpty(user.getRegisterDevice())) {
                user.setRegisterDevice(registerDevice); // 注册设备
            }
            if (userService.updateById(user) == 0) {
                return ResponseUtil.updatedDataFailed();
            }
        } else {
            return ResponseUtil.badArgumentValue("存在多条记录！");
        }


        /** 修改用户达标状态 **/
        RetrialUtils.updateUserEligible(user);

        BmdesignUserLevel bmdesignUserLevel = bmdesignUserLevelService.selectBmdesignUserLevelById(user.getLevelId());
        // token
        String userToken =
                UserTokenManager.generateToken(user.getId(), user.getTbkRelationId(), user.getTbkSpecialId());
        Map<Object, Object> result = new HashMap<Object, Object>();
        result.put("token", userToken);


        // 查询是否是审核状态
        boolean isExamine = RetrialUtils.examine(request, appVersion);
        if (isExamine) {
            user.setIsActivate((byte) 1);
        }

        /**禁用提示语*/
        Map<String, String> stringStringMap = systemConfigService.listDis();
        String mallTips = stringStringMap.get("mall_disable_tips");
        if (null != user && user.getStatus() == 1) {
            return ResponseUtil.fail(mallTips);
        }

        result.put("userInfo", user);
        result.put("userLevel", bmdesignUserLevel);
        // if (!isExamine) {
        // if (StringUtils.isEmpty(user.getDirectLeader())) {
        // return ResponseUtil.other(1000, "用户未被邀请！", result);
        // }
        // }
        return ResponseUtil.ok(result);
    }

    /**
     * (3)通过手机号注册（H5页面注册）
     *
     * @param body 请求内容 { username: xxx, mobile: xxx, code: xxx, firstLeaderId:xxx } ,其中code为手机验证码
     */
    @PostMapping("/registerByPhone")
    public Object registerByPhone(@RequestBody String body, HttpServletRequest request) throws Exception {
        String username = JacksonUtil.parseString(body, "username"); // 用户名
        String mobile = JacksonUtil.parseString(body, "mobile"); // 手机号
        String code = JacksonUtil.parseString(body, "code"); // 验证码
        String directLeaderId = JacksonUtil.parseString(body, "firstLeaderId"); // 推荐人ID
        String channelId = JacksonUtil.parseString(body, "channelId"); // 渠道ID

        if (StringUtils.isEmpty(username)) {
            return ResponseUtil.badArgumentValue("用户名不能为空！");
        }
        if (StringUtils.isEmpty(mobile)) {
            return ResponseUtil.badArgumentValue("手机号不能为空！");
        }
        if (StringUtils.isEmpty(code)) {
            return ResponseUtil.badArgumentValue("验证码不能为空！");
        }
        if (StringUtils.isEmpty(directLeaderId)) {
            return ResponseUtil.badArgumentValue("未获取到推荐人Id！");
        }

        // 通过手机号、手机验证码 判断验证码是否正确
        String smsType = "APP_REGISTER";
        MallPhoneValidation mallPhoneValidation = phoneService.getPhoneValidationByCode(mobile, code, smsType);
        if (mallPhoneValidation == null) {
            return ResponseUtil.fail(AUTH_CAPTCHA_UNMATCH, "验证码错误");
        }
        List<MallUser> userList = userService.queryByMobile(mobile);
        if (userList.size() > 0) {
            return ResponseUtil.badArgumentValue("该手机号已经注册过！");
        }

        // 设备类型
        String mobileTye = request.getHeader("User-Agent");
        Byte registerDevice = null;
        if (mobileTye.contains("iPhone")) {
            registerDevice = (byte) 2;
        } else if (mobileTye.contains("Android")) {
            registerDevice = (byte) 1;
        }
        // 注册新用户
        MallUser user = new MallUser();
        user.setMobile(mobile);
        user.setNickname(mobile);
        // user.setLastLoginTime(LocalDateTime.now());
        user.setLastLoginIp(IpUtil.getIpAddr(request));
        user.setLevelId("0");
        user.setIsDistribut("0");
        user.setRegisterDevice(registerDevice); // 注册设备
        if (!StringUtils.isEmpty(directLeaderId)) {
            MallUser directUser = userService.findById(directLeaderId); // 直推人
            if (null != directUser) {
                // 直推人id
                user.setDirectLeader(directLeaderId);
                //set绑定直推人时间
                user.setLockTime(LocalDateTime.now());
                if (directUser.getLevelId().equals(AccountStatus.LEVEL_0)
                        || directUser.getLevelId().equals(AccountStatus.LEVEL_1)) {
                    // 普通用户、会员 直接绑定直推人的三个上级
                    user.setFirstLeader(directUser.getFirstLeader());
                    user.setSecondLeader(directUser.getSecondLeader());
                    user.setThirdLeader(directUser.getThirdLeader());
                } else if (directUser.getLevelId().equals(AccountStatus.LEVEL_4)) {
                    // 服务商
                    user.setFirstLeader(directUser.getId());
                    user.setSecondLeader(directUser.getSecondLeader());
                    user.setThirdLeader(directUser.getThirdLeader());
                } else if (directUser.getLevelId().equals(AccountStatus.LEVEL_5)) {
                    // 区代
                    user.setFirstLeader(directUser.getFirstLeader());
                    user.setSecondLeader(directUser.getId());
                    user.setThirdLeader(directUser.getThirdLeader());
                } else if (directUser.getLevelId().equals(AccountStatus.LEVEL_6)) {
                    // 市代
                    user.setFirstLeader(directUser.getFirstLeader());
                    user.setSecondLeader(directUser.getSecondLeader());
                    user.setThirdLeader(directUser.getId());
                }
            } else {
                return ResponseUtil.badArgumentValue("邀请用户不存在！");
            }
        }
        // 用户注册渠道
        if (!StringUtils.isEmpty(channelId)) {
            user.setChannelId(channelId);
        }
        userService.add(user);
        // 开户
        if (user.getId() != null) {
            logger.info("开户成功..............");
            accountService.accountTradeCreateAccount(user.getId().toString());
        }
        // 消息推送
        PushUtils.pushMessage(user.getId(), AccountStatus.PUSH_TITLE_REGISTER, AccountStatus.PUSH_CONTENT_REGISTER, 2,
                null);

        // 使验证码失效
        MallPhoneValidation mallPhoneValidation1 = new MallPhoneValidation();
        mallPhoneValidation1.setId(mallPhoneValidation.getId());
        mallPhoneValidation1.setValState("00");
        phoneService.update(mallPhoneValidation1);
        return ResponseUtil.ok();
    }

    /**
     * 通过邀请码绑定上下级关系
     */
    @PostMapping("/authByCode")
    public Object authByCode(@LoginUser String userId, @RequestBody String body) throws Exception {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        String code = JacksonUtil.parseString(body, "code"); // 邀请码 或 激活码
        if (StringUtils.isEmpty(code)) {
            return ResponseUtil.badArgumentValue("未获取到邀请码！");
        }
        // 当前用户信息
        MallUser user = userService.findById(userId);
        if (user == null) {
            return ResponseUtil.badArgumentValue("用户不存在！");
        }
        if (!StringUtils.isEmpty(user.getDirectLeader())) {
            return ResponseUtil.badArgumentValue("你已经填写过邀请码！");
        }


        // 判断code类型
        int length = code.length();
        if (length == 8) {
            /** 激活码（8位） **/
            // todo================= 目前只判断普通用户激活
            // todo================= 目前只判断普通用户激活
            if (user.getIsActivate() == 1) {
                return ResponseUtil.badArgumentValue("你已经激活了,无需再次激活！");
            }
            /** 查询激活码信息 **/
            MallInvitationCode mallInvitationCode = new MallInvitationCode();
            mallInvitationCode.setCode(code);
            mallInvitationCode.setUseStatus("000");
            List<MallInvitationCode> mallInvitationCodeList =
                    codeSercice.selectMallInvitationCodeList(mallInvitationCode, null, null, null, null);
            if (mallInvitationCodeList.size() == 0) {
                return ResponseUtil.badArgumentValue("邀请码不存在或已经失效！");
            }
            mallInvitationCode = mallInvitationCodeList.get(0);
            /** 判断激活码是否符合条件 **/
            if (StringUtils.isEmpty(mallInvitationCode.getUserId())) {
                logger.info("激活码拥有者不存在！");
                return ResponseUtil.badArgumentValue("激活码使用条件不符合！");
            }
            MallUser codeUser = userService.findById(mallInvitationCode.getUserId());
            if (codeUser == null) {
                logger.info("激活码拥有者不存在！");
                return ResponseUtil.badArgumentValue("激活码使用条件不符合！");
            }
            if (!(AccountStatus.LEVEL_4.equals(codeUser.getLevelId())
                    || AccountStatus.LEVEL_5.equals(codeUser.getLevelId())
                    || AccountStatus.LEVEL_6.equals(codeUser.getLevelId()))) {
                logger.info("激活码拥有者不为代理！");
                return ResponseUtil.badArgumentValue("激活码使用条件不符合！");
            }

            /** 激活码失效 **/
            MallInvitationCode updateCode = new MallInvitationCode();
            updateCode.setId(mallInvitationCode.getId());
            updateCode.setUseStatus("111");
            updateCode.setUseTime(LocalDateTime.now());
            updateCode.setSpendUserId(user.getId());
            codeSercice.updateMallInvitationCode(updateCode);
            /** 用户卡激活 **/
            /** 通过userId、是否激活 查询可激活的卡号信息 **/
            MallUserCard userCard = new MallUserCard();
            userCard.setUserId(user.getId());
            List<MallUserCard> mallUserCardList =
                    mallUserCardService.selectMallUserCardList(userCard, null, null, null, null);
            if (mallUserCardList.size() > 0) {
                userCard = mallUserCardList.get(0);
                userCard.setActivationCode(code);
                userCard.setActivateTime(LocalDateTime.now());
                userCard.setSpendUserId(user.getId());
                mallUserCardService.updateMallUserCard(userCard);
            }
            /** 用户激活,同时成为会员 **/
            MallUser updateUser = new MallUser();
            updateUser.setId(user.getId());
            updateUser.setLevelId(AccountStatus.LEVEL_1); // 激活卡成为 'vip用户'
            updateUser.setIsActivate((byte) 1);
            updateUser.setDirectLeader(codeUser.getId()); // 直推人
            //set绑定直推人时间
            updateUser.setLockTime(LocalDateTime.now());
            // 直接绑定直推人的三个上级
            updateUser.setFirstLeader(codeUser.getFirstLeader());
            updateUser.setSecondLeader(codeUser.getSecondLeader());
            updateUser.setThirdLeader(codeUser.getThirdLeader());
            userService.updateById(updateUser);
            // 这里是 注册完了，如果用户有直推人，就找上六级的单位
            C2cUtils.c2cUtilsInsert(user.getId(), codeUser.getId(), 6);
            // 消息推送
            PushUtils.pushMessage(user.getId(), AccountStatus.PUSH_TITLE_ACTIVATE, AccountStatus.PUSH_CONTENT_ACTIVATE,
                    2, null);

        } else {
            /** 邀请码(6位) **/
            /** 通过注册码查询邀请人信息 **/
            List<MallUser> mallUserList = userService.queryByInvitationCode(code);
            MallUser mallUser = new MallUser();
            if (mallUserList.size() == 1) {
                mallUser = mallUserList.get(0);
            } else {
                return ResponseUtil.badArgumentValue("请输入正确的邀请码！");
            }
            if (userId.equals(mallUser.getId())) {
                return ResponseUtil.badArgumentValue("无法使用自己的邀请码！");
            }
            user.setDirectLeader(mallUser.getId());
            //set绑定直推人时间
            user.setLockTime(LocalDateTime.now());
            if (mallUser.getLevelId().equals(AccountStatus.LEVEL_0)
                    || mallUser.getLevelId().equals(AccountStatus.LEVEL_1)) {
                // 普通用户、会员 直接绑定直推人的三个上级
                user.setFirstLeader(mallUser.getFirstLeader());
                user.setSecondLeader(mallUser.getSecondLeader());
                user.setThirdLeader(mallUser.getThirdLeader());
            } else if (mallUser.getLevelId().equals(AccountStatus.LEVEL_4)) {
                // 服务商
                user.setFirstLeader(mallUser.getId());
                user.setSecondLeader(mallUser.getSecondLeader());
                user.setThirdLeader(mallUser.getThirdLeader());
            } else if (mallUser.getLevelId().equals(AccountStatus.LEVEL_5)) {
                // 区代
                user.setFirstLeader(mallUser.getFirstLeader());
                user.setSecondLeader(mallUser.getId());
                user.setThirdLeader(mallUser.getThirdLeader());
            } else if (mallUser.getLevelId().equals(AccountStatus.LEVEL_6)) {
                // 市代
                user.setFirstLeader(mallUser.getFirstLeader());
                user.setSecondLeader(mallUser.getSecondLeader());
                user.setThirdLeader(mallUser.getId());
            }
            // 确定用户注册渠道,若直推用户为 系统用户则为自主注册,否则为分享注册
            if (!mallUser.getId().equals(AccountStatus.PLATFORM_USER_ID)) {
                // 分享注册
                user.setChannelId(MallUserEnum.CHANNEL_ID_1.getCode());
            }
            userService.updateById(user);
        }
        return ResponseUtil.ok();
    }

    /**
     * (4)代理端小程序_登录接口 {"username":"","password":""}
     */
    @PostMapping("/loginByAgent")
    public Object loginByAgent(@RequestBody String body) {
        String username = JacksonUtil.parseString(body, "username");
        String password = JacksonUtil.parseString(body, "password");
        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password)) {
            return ResponseUtil.badArgumentValue("账号或密码不能为空！");
        }
        // 查询代理身份list
        BmdesignUserLevel bmdesignUserLevel = new BmdesignUserLevel();
        bmdesignUserLevel.setType("identity");
        List<BmdesignUserLevel> levelList = bmdesignUserLevelService.selectBmdesignUserLevelList(bmdesignUserLevel,
                null, null, "level_num", "asc");
        List<String> levelIds = new ArrayList<>();
        MallUser mallUser = null;
        if (levelList.size() > 0) {
            for (BmdesignUserLevel level : levelList) {
                levelIds.add(level.getId());
            }
            List<MallUser> mallUserList = userService.selectUser(username, null, levelIds);
            if (mallUserList.size() == 1) {
                mallUser = mallUserList.get(0);
            }
        }
        if (mallUser == null) {
            return ResponseUtil.badArgumentValue("账号或密码错误！");
        }
        if (mallUser.getStatus() == 1) {
            return ResponseUtil.badArgumentValue("您的账号已被冻结，请联系您的上级！");
        } else if (mallUser.getStatus() == 2) {
            return ResponseUtil.badArgumentValue("您的账号已注销！");
        }
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        if (!encoder.matches(password, mallUser.getPassword())) {
            return ResponseUtil.badArgumentValue("账号或密码错误！");
        }
        // 用户特殊昵称处理
        if (!StringUtils.isEmpty(mallUser.getNickname())) {
            mallUser.setNickname(EmojiParser.parseToUnicode(mallUser.getNickname()));
        }
        // 用户上级信息
        String levelId = mallUser.getLevelId();
        MallUserAgentVo leaderInfo = null;

        // 查询分润计算方式(key_value 0:服务商、区代、市代方式 1:用户firstLeader、secondLeader、thirderLeader方式 )
        Map<String, String> profitTypeMap = systemConfigService.listProfitType();
        String profitType = profitTypeMap.get("Mall_profit_type");
        if (Objects.equals(profitType, "0")) {
            /** 方式一(根据省市区) **/
            if (levelId.equals(AccountStatus.LEVEL_4)) {
                // 当前用户为服务商
                // 查询区级代理
                List<MallUserAgentVo> qlist = userService.getCountByCondition(false, null, mallUser.getProvince(),
                        mallUser.getCity(), mallUser.getCounty(), AccountStatus.LEVEL_5);
                if (qlist.size() == 1) {
                    leaderInfo = qlist.get(0);
                } else {
                    // 查询市级代理
                    List<MallUserAgentVo> slist = userService.getCountByCondition(false, null, mallUser.getProvince(),
                            mallUser.getCity(), null, AccountStatus.LEVEL_6);
                    if (slist.size() == 1) {
                        leaderInfo = slist.get(0);
                    }
                }
            } else if (levelId.equals(AccountStatus.LEVEL_5)) {
                // 当前用户为区级代理
                // 查询市级代理
                List<MallUserAgentVo> slist = userService.getCountByCondition(false, null, mallUser.getProvince(),
                        mallUser.getCity(), null, AccountStatus.LEVEL_6);
                if (slist.size() == 1) {
                    leaderInfo = slist.get(0);
                }
            }
        } else {
            /** 方式二(根据userId) **/
            MallUser user = null;
            if (levelId.equals(AccountStatus.LEVEL_0) || levelId.equals(AccountStatus.LEVEL_1)) {
                // 普通、VIP用户
                if (!StringUtils.isEmpty(mallUser.getDirectLeader())) {
                    user = userService.findById(mallUser.getDirectLeader());
                } else {
                    if (!StringUtils.isEmpty(mallUser.getFirstLeader())) {
                        user = userService.findById(mallUser.getFirstLeader());
                    } else {
                        if (!StringUtils.isEmpty(mallUser.getSecondLeader())) {
                            user = userService.findById(mallUser.getSecondLeader());
                        } else {
                            if (!StringUtils.isEmpty(mallUser.getThirdLeader())) {
                                user = userService.findById(mallUser.getThirdLeader());
                            }
                        }
                    }
                }

            } else if (levelId.equals(AccountStatus.LEVEL_4)) {
                // 当前用户为服务商
                if (!StringUtils.isEmpty(mallUser.getSecondLeader())) {
                    user = userService.findById(mallUser.getSecondLeader());
                } else {
                    if (!StringUtils.isEmpty(mallUser.getThirdLeader())) {
                        user = userService.findById(mallUser.getThirdLeader());
                    }
                }
            } else if (levelId.equals(AccountStatus.LEVEL_5)) {
                // 当前用户为区级代理
                if (!StringUtils.isEmpty(mallUser.getThirdLeader())) {
                    user = userService.findById(mallUser.getThirdLeader());
                }
            }
            if (user != null) {
                leaderInfo = new MallUserAgentVo();
                BeanUtils.copyProperties(user, leaderInfo);
            }
        }

        // 设置默认上级信息
        if (leaderInfo == null) {
            leaderInfo = new MallUserAgentVo();
            Map<String, String> data = systemConfigService.listAgent();
            leaderInfo.setNickname(data.get("Mall_agent_nickname"));
            leaderInfo.setMobile(data.get("Mall_agent_mobile"));
        }


        /** 用户身份信息 **/
        BmdesignUserLevel userLevel = bmdesignUserLevelService.selectBmdesignUserLevelById(levelId);

        /** 生成token返回给前端 **/
        String token = UserTokenManager.generateToken(mallUser.getId(), mallUser.getTbkRelationId(),
                mallUser.getTbkSpecialId());
        Map<Object, Object> result = new HashMap<Object, Object>();

        result.put("token", token);
        result.put("userInfo", mallUser);
        result.put("userLevel", userLevel);
        result.put("leaderInfo", leaderInfo); // 上级信息
        return ResponseUtil.ok(result);
    }

    /**
     * (5)998活动小程序_登录接口 {"username":"","password":""}
     */
    @PostMapping("/loginByActivity")
    public Object loginByActivity(@RequestBody String body) {
        String username = JacksonUtil.parseString(body, "username");
        String password = JacksonUtil.parseString(body, "password");
        String registerCode = JacksonUtil.parseString(body, "registerCode");// 注册码
        String type = JacksonUtil.parseString(body, "type");
        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password)) {
            return ResponseUtil.badArgumentValue("账号或密码不能为空！");
        }
        MallMarketingActivity mallUser = null;
        List<MallMarketingActivity> mallUserList = userService.selectUserActivity(username, password, null);
        if (mallUserList.size() == 1) {
            mallUser = mallUserList.get(0);
        }
        Map<String, String> data = systemConfigService.listCode();
        List<MallMarketingActivity> mallUserList1 = userService.selectUserActivity(null, null, null);
        if (mallUser == null && !"register".equals(type)) {
            return ResponseUtil.badArgumentValue("账号或密码错误！");
        } else if ("register".equals(type) && data.get("Mall_register_code").equals(registerCode)) {
            for (MallMarketingActivity malluser : mallUserList1) {
                if (malluser.getUsername().equals(username)) {
                    return ResponseUtil.badArgumentValue("账号已存在！");
                }
            }
            mallUser = new MallMarketingActivity();
            mallUser.setId(GraphaiIdGenerator.nextId("MallMarketingActivity"));
            mallUser.setUsername(username);
            mallUser.setMobile(username);
            mallUser.setPassword(password);
            mallUser.setDeleted(false);
            mallUser.setAmount(mallUserList1.get(0).getAmount());
            mallUser.setQuota(mallUserList1.get(0).getQuota());
            mallUser.setSurplusQuota(mallUserList1.get(0).getSurplusQuota());
            mallUser.setAddTime(LocalDateTime.now());
            mallUser.setUpdateTime(LocalDateTime.now());
            mallUser.setStartTime(LocalDate.now());
            mallUser.setEndTime(mallUserList1.get(0).getEndTime());
            mallUser.setPicUrls(mallUserList1.get(0).getPicUrls());
            mallUser.setBtnPic(mallUserList1.get(0).getBtnPic());
            userService.addUserActivity(mallUser);

        }


        /** 生成token返回给前端 **/
        String token = UserTokenManager.generateToken(mallUser.getId(), null, null);
        Map<Object, Object> result = new HashMap<Object, Object>();

        result.put("token", token);
        result.put("userInfo", mallUser);
        return ResponseUtil.ok(result);
    }

    /**
     * 微信授权
     */
    @PostMapping("/authByWx")
    public Object authByWx(@LoginUser String userId, @RequestBody String param) throws IOException {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }

        if (StringUtils.isEmpty(param)) {
            return ResponseUtil.badArgumentValue("未获取到微信授权参数！");
        }
        logger.info("微信授权信息： " + JacksonUtils.bean2Jsn(param));
        String accessToken = JacksonUtil.parseString(param, "access_token");
        String openId = JacksonUtil.parseString(param, "openid");
        if (StringUtils.isEmpty(accessToken) || StringUtils.isEmpty(openId)) {
            return ResponseUtil.badArgumentValue("微信授权参数有误！");
        }

        /** 请求开放平台接口获取用户信息 **/
        String jsn = HttpRequestUtils.get(
                "https://api.weixin.qq.com/sns/userinfo?access_token=" + accessToken + "&openid=" + openId,
                null, null);
        logger.info("微信数据获取 ：" + jsn);

        Map<String, Object> usermap = JacksonUtils.jsn2map(jsn, String.class, Object.class);
        String openid = MapUtils.getString(usermap, "openid");

        //判断此微信有无被绑定
        int status = userService.getBindingStatus(openid);
        if (status > 0) {
            return ResponseUtil.badArgumentValue("此微信号已被授权其他账号，请先解绑原账号再进行授权！");
        }

        String nickname = MapUtils.getString(usermap, "nickname");
        String sex = MapUtils.getString(usermap, "sex");
        // String province = MapUtils.getString(usermap, "province");
        // String city = MapUtils.getString(usermap, "city");
        // String country = MapUtils.getString(usermap, "country");
        String headimgurl = MapUtils.getString(usermap, "headimgurl");
        String unionid = MapUtils.getString(usermap, "unionid");
        if (!StringUtils.isEmpty(nickname)) {
            nickname = EmojiParser.parseToAliases(nickname);
        }
        // 判断用户身份,代理身份无法修改昵称
        MallUser userInfo = userService.findById(userId);

        MallUser user = new MallUser();
        user.setId(userId);
        user.setWeixinOpenid(openid);
        user.setAvatar(headimgurl);
        if (userInfo.getLevelId().equals(AccountStatus.LEVEL_0)
                || userInfo.getLevelId().equals(AccountStatus.LEVEL_1)) {
            if (!StringUtils.isEmpty(nickname)) {
                user.setNickname(nickname);
                user.setWxNickname(nickname);
            }
        } else {
            if (!StringUtils.isEmpty(nickname)) {
                user.setWxNickname(nickname);
            }
        }

        user.setGender(sex);
        user.setUnionid(unionid);
        // user.setProvince(province);
        // user.setCity(city);
        // user.setCountry(country);
        userService.updateById(user);


        /** 修改用户达标状态 **/
        userInfo.setWeixinOpenid(openid);
        RetrialUtils.updateUserEligible(userInfo);
        return ResponseUtil.ok();
    }


    /**
     * 用户设置密码
     *
     * @param userId 用户id
     * @param params {"code":"手机收到的验证码","mobile":"用户的电话","payPassword":"加密后的密码"}
     * @return
     */
    @PostMapping("/setPayPasswordByMobileCode")
    public Object addPayPassword(@LoginUser String userId, @RequestBody String params) throws Exception {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        String mobile = JacksonUtil.parseString(params, "mobile"); // 手机号
        String code = JacksonUtil.parseString(params, "code"); // 验证码
        String payPassword = JacksonUtil.parseString(params, "payPassword");// 密码
        if (mobile == null || code == null) {
            return ResponseUtil.badArgument();
        }
        // 通过手机号、手机验证码 判断验证码是否正确
        String smsType = "INIT_PAY_PWD";
        MallPhoneValidation mallPhoneValidation = phoneService.getPhoneValidationByCode(mobile, code,
                smsType);

        if (mallPhoneValidation == null) {
            return ResponseUtil.fail(AUTH_CAPTCHA_UNMATCH, "验证码错误");
        }
        // 利用 RSA 私钥解密，完了，得到 明文密码 ，再用 BCrypt 对 密码进行加密 ，完了，再放到数据库
        byte[] payPasswordByte = RSAUtils.decryptByPrivateKey(payPassword, privateKey);
        String payPasswordUserSend = new String(payPasswordByte);
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String encodedPassword = encoder.encode(payPasswordUserSend);
        // 将密码直接保存到mall_user 表里面
        MallUser user = new MallUser();
        user.setId(userId);
        user.setPayPassword(encodedPassword);
        int num = userService.updateById(user);
        if (num > 0) {
            return ResponseUtil.ok();
        } else {
            return ResponseUtil.fail("保存失败！");
        }
    }

    @PostMapping("/setIsNoPayPasswordByMobileCodeOrPayPassword")
    public Object addIsNoPayPassword(@LoginUser String userId, @RequestBody String params) throws Exception {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        String code = JacksonUtil.parseString(params, "code"); // 验证码
        String payPassword = JacksonUtil.parseString(params, "payPassword");// 支付密码
        if (StringUtils.isEmpty(code) && StringUtils.isEmpty(payPassword)) {
            return ResponseUtil.fail("请输入验证码或者支付密码！进行校验");
        }
        if (!StringUtils.isEmpty(code)) {
            String mobile = JacksonUtil.parseString(params, "mobile"); // 手机号
            if (StringUtils.isEmpty(mobile)) {
                return ResponseUtil.fail("请输入手机号码");
            }
            String smsType = "INIT_PAY_PWD";
            MallPhoneValidation mallPhoneValidation = phoneService.getPhoneValidationByCode(mobile, code,
                    smsType);
            if (mallPhoneValidation == null) {
                return ResponseUtil.fail(AUTH_CAPTCHA_UNMATCH, "验证码错误");
            }
        }
        if (!StringUtils.isEmpty(payPassword)) {
            byte[] payPasswordByte = RSAUtils.decryptByPrivateKey(payPassword, privateKey);
            logger.info(payPasswordByte);
            String payPasswordUserSend = new String(payPasswordByte);
            MallUser mallUser = userService.findById(userId);
            if (ObjectUtils.isEmpty(mallUser)) {
                return ResponseUtil.fail("找不到对应的用户！");
            }
            if (com.graphai.commons.util.lang.StringUtils.isEmpty(mallUser.getPayPassword())) {
                return ResponseUtil.fail("该用户没有设置支付密码，请先设置支付密码！");
            }
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
            if (!encoder.matches(payPasswordUserSend, mallUser.getPayPassword())) {
                return ResponseUtil.fail("支付密码错误！");
            }
        }
        String isNoPayPassword = JacksonUtil.parseString(params, "isNoPayPassword");
        if (StringUtils.isEmpty(isNoPayPassword)) {
            isNoPayPassword = "0";
        }
        // 为用户开启免密支付模式
        MallUser user = new MallUser();
        user.setId(userId);
        Byte b = isNoPayPassword.getBytes()[0];
        user.setIsPayNoPassword(b);
        int num = userService.updateById(user);
        if (num > 0) {
            return ResponseUtil.ok();
        } else {
            return ResponseUtil.fail("保存失败！");
        }
    }


    /**
     * 淘宝会员备案
     *
     * @param accessToken
     * @param uUser
     * @param user
     * @param taoBaoBaiChuanUserInfo
     */
    public void taobaoMemberSave(String accessToken, MallUser uUser, MallUser user,
                                 TaoBaoBaiChuanUserInfo taoBaoBaiChuanUserInfo) {
        String saveurl = "https://eco.taobao.com/router/rest";
        TaobaoClient client1 = new DefaultTaobaoClient(saveurl, taoBaoProperties.getLianmengAppKey(),
                taoBaoProperties.getLianmengAppSecret());
        TbkScPublisherInfoSaveRequest req1 = new TbkScPublisherInfoSaveRequest();
        req1.setOfflineScene("4");
        req1.setOnlineScene("3");
        req1.setInviterCode("H8JEJG");
        req1.setInfoType(1L);
        TbkScPublisherInfoSaveResponse rspSave;
        try {
            rspSave = client1.execute(req1, accessToken);
            Data data = rspSave.getData();
            if (!Objects.equals(data, null)) {
                Long specialId = data.getSpecialId();
                if (specialId != null) {
                    uUser.setTbkSpecialId(String.valueOf(specialId));
                    user.setTbkSpecialId(String.valueOf(specialId));
                }
                if (null != taoBaoBaiChuanUserInfo.getNick()) {
                    uUser.setTbNickname(taoBaoBaiChuanUserInfo.getNick());
                    user.setTbNickname(taoBaoBaiChuanUserInfo.getNick());
                }
            }
        } catch (ApiException e) {
            e.printStackTrace();
        }

    }

    /**
     * App和小程序使用微信登录
     *
     * @param body    请求参数
     * @param request
     * @return
     */
    @PostMapping("/loginByWx")
    public Object authLogin(@RequestBody String body, HttpServletRequest request) {
        String accessToken = JacksonUtil.parseString(body, "access_token");
        String openId = JacksonUtil.parseString(body, "openid");
        // 版本号
        String version = JacksonUtil.parseString(body, "version");

        /** 微信授权的code 小程序使用到 **/
        String wxMpCode = JacksonUtil.parseString(body, "wxMpCode");

        /** 登录渠道 **/
        String loginChannel = JacksonUtil.parseString(body, "loginChannel");

        String nickname = null;
        String gender = null;
        String province = null;
        String city = null;
        String country = null;
        String unionid = null;
        String avatarUrl = null;
        MallUser user = new MallUser();
        try {
            if (MallConstant.REGISTER_TYPE_WXAPP.equalsIgnoreCase(loginChannel)) {
                /** 请求开放平台接口获取用户信息 **/
                String jsn = HttpRequestUtils.get("https://api.weixin.qq.com/sns/userinfo?access_token="
                        + accessToken + "&openid=" + openId, null, null);
                Map<String, Object> usermap = JacksonUtils.jsn2map(jsn, String.class, Object.class);
                gender = MapUtils.getString(usermap, "sex");
                province = MapUtils.getString(usermap, "province");
                city = MapUtils.getString(usermap, "city");
                country = MapUtils.getString(usermap, "country");
                avatarUrl = MapUtils.getString(usermap, "headimgurl");
                unionid = MapUtils.getString(usermap, "unionid");
                if (StrUtil.isNotBlank(MapUtils.getString(usermap, "nickname"))) {
                    nickname = EmojiParser.parseToAliases(MapUtils.getString(usermap, "nickname"));
                } else {
                    String uuId = UUID.randomUUID().toString();
                    nickname = uuId.substring(uuId.lastIndexOf("-")) + "用户";
                }
                user.setWeixinOpenid(MapUtils.getString(usermap, "openid"));
                logger.info("微信数据获取 ：" + jsn);
            } else if (MallConstant.REGISTER_TYPE_WXMINI.equalsIgnoreCase(loginChannel) && StrUtil.isNotBlank(wxMpCode)) {
                WxMaJscode2SessionResult result = this.wxService.getUserService().getSessionInfo(wxMpCode);
                user.setWeixinMiniOpenid(result.getOpenid());
                user.setSessionKey(result.getSessionKey());
            }
            // 设备类型
            String mobileTye = request.getHeader("User-Agent");
            Byte registerDevice = null;
            if (mobileTye.contains("iPhone")) {
                registerDevice = (byte) 2;
            } else if (mobileTye.contains("Android")) {
                registerDevice = (byte) 1;
            }

            user.setAvatar(avatarUrl);
            user.setNickname(nickname);
            user.setGender(gender);
            user.setUnionid(unionid);
            user.setProvince(province);
            user.setCity(city);
            user.setCountry(country);
            //注册聚道
            user.setSource(loginChannel);
            // 注册设备
            user.setRegisterDevice(registerDevice);
            List<MallUser> userList = null;
            if (MallConstant.REGISTER_TYPE_WXAPP.equalsIgnoreCase(loginChannel)){
                userList = userService.queryByOpenid(user.getWeixinOpenid());
            }else if (MallConstant.REGISTER_TYPE_WXMINI.equalsIgnoreCase(loginChannel)){
                userList = userService.queryByWeixinMiniOpenid(user.getWeixinMiniOpenid());
            }
            if ( ObjectUtil.isNotEmpty(userList) && userList.size() == 1) {
                user = userList.get(0);
                user.setLastLoginTime(LocalDateTime.now());
                user.setLastLoginIp(IpUtil.getIpAddr(request));
                if (StrUtil.isEmpty(user.getRegisterDevice().toString())) {
                    user.setRegisterDevice(registerDevice); // 注册设备
                }
                if (userService.updateById(user) == 0) {
                    return ResponseUtil.updatedDataFailed();
                }
            } else if (userList.size() > 1) {
                return ResponseUtil.badArgumentValue("存在多条记录！");
            }

            Map<Object, Object> result = new HashMap<Object, Object>();

            // 用户存在时才返回的数据
            if (userList.size() == 1) {
                /** 修改用户达标状态 **/
                RetrialUtils.updateUserEligible(user);
                logger.info("修改用户达标状态已完成");

                BmdesignUserLevel bmdesignUserLevel = bmdesignUserLevelService.selectBmdesignUserLevelById(user.getLevelId());
                logger.info("获取用户等级完成");
                result.put("userLevel", bmdesignUserLevel);

                String token = UserTokenManager.generateToken(user.getId(), user.getTbkRelationId(), user.getTbkSpecialId());
                result.put("token", token);
                logger.info("用户token=" + token);
            }
            // 查询是否是审核状态
            boolean isExamine = RetrialUtils.examine(request, version);
            if (isExamine) {
                user.setIsActivate((byte) 1);
            }
            /**禁用提示语*/
            Map<String, String> stringStringMap = systemConfigService.listDis();
            String mallTips = stringStringMap.get("mall_disable_tips");
            if (ObjectUtil.isNotEmpty(user)) {
                if (ObjectUtil.isNotNull(user.getStatus())) {
                    if (MallUserEnum.STATUS_1.strCode2Byte().equals(user.getStatus()))
                        return ResponseUtil.fail(mallTips);
                }
            }
            result.put("userInfo", user);

            return ResponseUtil.ok(result);
        } catch (Exception ex) {
            logger.info("登录异常：" + ex.getMessage(), ex);
            ex.printStackTrace();
            return ResponseUtil.fail();
        }

    }

    /**
     * App使用短信方式进行手机绑定同时也是注册用户
     *
     * @param userRegisterInfo 用户信息
     * @return
     */
    @PostMapping("/appBindPhone")
    public Object appBindPhone(@RequestBody UserRegisterInfo userRegisterInfo, HttpServletRequest request) {

        String phone = userRegisterInfo.getMobile();
        String code = userRegisterInfo.getCode();

        // 通过手机号、手机验证码 判断验证码是否正确
        String smsType = "APP_LOGIN";
        MallPhoneValidation mallPhoneValidation = phoneService.getPhoneValidationByCode(phone, code, smsType);
        if (mallPhoneValidation == null) {
            return ResponseUtil.fail(AUTH_CAPTCHA_UNMATCH, "验证码错误");
        }
        // 使验证码失效
        MallPhoneValidation mallPhoneValidation1 = new MallPhoneValidation();
        mallPhoneValidation1.setId(mallPhoneValidation.getId());
        mallPhoneValidation1.setValState("00");
        phoneService.update(mallPhoneValidation1);
        return setUserInfo(userRegisterInfo,request);
    }

    /**
     * 小程序微信手机号码绑定同时也是注册用户
     *
     * @param userRegisterInfo
     * @return
     */
    @PostMapping("/applet/authPhone")
    public Object bindPhone(@RequestBody UserRegisterInfo userRegisterInfo, HttpServletRequest request) {
        String encryptedData = userRegisterInfo.getEncryptedData();
        System.out.println(appId);
        System.out.println(appSecret);
        System.out.println(appKey);
        String sessionKey = null;
        sessionKey = userRegisterInfo.getSessionKey();
//        String iv = userRegisterInfo.getIv();
//        if (StrUtil.isEmpty(sessionKey)){
//            WxMaJscode2SessionResult result = null;
//            try {
//                result = this.wxService.getUserService().getSessionInfo(userRegisterInfo.getWxMpCode());
//                sessionKey = result.getSessionKey();/

//            } catch (WxErrorException e) {
//                e.printStackTrace();
//            }
//        }
//        WxMaPhoneNumberInfo phoneNumberInfo = this.wxService.getUserService().getPhoneNoInfo(sessionKey, encryptedData, iv);
//        String phone = phoneNumberInfo.getPhoneNumber();
        userRegisterInfo.setMobile("13800138000");
        return setUserInfo(userRegisterInfo,request);
    }

    /**
     * 锁客
     * @param userId
     * @param body
     * @return
     */
    @PostMapping("/lockCustomer")
    public Object LockCustomer(@LoginUser String userId,@RequestBody String body){
        // 邀请人userId
        String invitationUserId = JacksonUtil.parseString(body, "invitationUserId");

        if(invitationUserId.equals("00")){
            // 平台锁定
            MallUser user = userService.queryByUserId(userId);
            if(StringUtils.isEmpty(user.getDirectLeader())){
                user.setLockTime(LocalDateTime.now());
                user.setDirectLeader(invitationUserId);
                userService.updateById(user);
            }
        }else{
            //邀请人
            MallUser invitationUser = userService.queryByUserId(invitationUserId);
            if(invitationUser == null){
                return ResponseUtil.fail("邀请人信息错误");
            }if(invitationUser.getLevelId().equals("0") || invitationUser.getLevelId().equals("98")){
                // 普通用户或工厂邀请
                MallUser user = userService.queryByUserId(userId);
                if(StringUtils.isEmpty(user.getDirectLeader()) && StringUtils.isEmpty(user.getFirstLeader())){
                    if(!StringUtils.isEmpty(invitationUser.getFirstLeader())
                            && invitationUser.getFirstLeader().equals(invitationUser.getDirectLeader())){
                        user.setFirstLeader(invitationUser.getFirstLeader());
                    }

                    user.setLockTime(LocalDateTime.now());
                    user.setDirectLeader(invitationUser.getId());
                    userService.updateById(user);
                }
            }else if(invitationUser.getLevelId().equals("9") || invitationUser.getLevelId().equals("10")
                || invitationUser.getLevelId().equals("11")){
                // 商户邀请
                MallUser user = userService.queryByUserId(userId);
                if(StringUtils.isEmpty(user.getDirectLeader()) && StringUtils.isEmpty(user.getFirstLeader())){
                    user.setDirectLeader(invitationUser.getId());
                    user.setFirstLeader(invitationUser.getId());
                    user.setLockTime(LocalDateTime.now());
                    userService.updateById(user);
                }
            }
        }

        // 用户商户关联表
        QueryWrapper<MallUserMerchant> mallUserMerchantWrapper = new QueryWrapper<MallUserMerchant>();
        mallUserMerchantWrapper.eq("user_id", invitationUserId);
        List<MallUserMerchant> mallUserMerchant = mallUserMerchantService.list(mallUserMerchantWrapper);
        MallMerchant mallMerchant = null;
        for (MallUserMerchant temp: mallUserMerchant) {
            // 商户信息
            QueryWrapper<MallMerchant> merchantWrapper = new QueryWrapper<MallMerchant>();
            merchantWrapper.eq("status", 0);// 正常营业
            merchantWrapper.eq("audit_status", 1);// 审核通过
            merchantWrapper.eq("id",temp.getMerchantId());
            mallMerchant = mallMerchantService.getOne(merchantWrapper);

            if(mallMerchant != null){
                break;
            }
        }
        return ResponseUtil.ok(mallMerchant);
    }

    private Object setUserInfo(UserRegisterInfo userRegisterInfo, HttpServletRequest request){
        MallUser user = new MallUser();
        //设置用户信息

        if (StrUtil.isNotBlank(userRegisterInfo.getMobile())){
            user.setMobile(userRegisterInfo.getMobile());
        }

        if (StrUtil.isNotBlank(userRegisterInfo.getAvatar())){
            user.setAvatar(userRegisterInfo.getAvatar());
        }else{
            user.setAvatar("https://51fangtao.oss-cn-shenzhen.aliyuncs.com/logo.png");
        }

        if (StrUtil.isNotBlank(userRegisterInfo.getNickname())){
            user.setNickname(userRegisterInfo.getNickname());
        }else {
            user.setNickname(NameUtil.mobileEncrypt(user.getMobile()));
        }

        if (userRegisterInfo.getGender() != null){
            user.setGender(userRegisterInfo.getGender().toString());
        }else{
            user.setGender("0");
        }

        if (StrUtil.isNotBlank(userRegisterInfo.getUnionid())){
            user.setUnionid(userRegisterInfo.getUnionid());
        }

        if (StrUtil.isNotBlank(userRegisterInfo.getProvince())){
            user.setProvince(userRegisterInfo.getProvince());
        }

        if (StrUtil.isNotBlank(userRegisterInfo.getCity())){
            user.setCity(userRegisterInfo.getCity());
        }

        if (StrUtil.isNotBlank(userRegisterInfo.getCountry())){
            user.setCountry(userRegisterInfo.getCountry());
        }

        if (StrUtil.isNotBlank(userRegisterInfo.getWeixinOpenid())){
            user.setWeixinOpenid(userRegisterInfo.getWeixinOpenid());
        }

        if (StrUtil.isNotBlank(userRegisterInfo.getWeixinMiniOpenid())){
            user.setWeixinMiniOpenid(userRegisterInfo.getWeixinMiniOpenid());
        }

        if (StrUtil.isNotBlank(userRegisterInfo.getSessionKey())){
            user.setSessionKey(userRegisterInfo.getSessionKey());
        }

        if (StrUtil.isNotBlank(userRegisterInfo.getSource())){
            user.setSource(userRegisterInfo.getSource());
        }

        user.setLastLoginTime(LocalDateTime.now());
        user.setLastLoginIp(IpUtil.getIpAddr(request));
        user.setLevelId("0");

        // 是否为分销商,0否1是
        user.setIsDistribut("0");
        // 设备类型
        String mobileTye = request.getHeader("User-Agent");
        Byte registerDevice = null;
        if (mobileTye.contains("iPhone")) {
            registerDevice = (byte) 2;
        } else if (mobileTye.contains("Android")) {
            registerDevice = (byte) 1;
        }
        // 注册设备
        user.setRegisterDevice(registerDevice);
        String token = null;
        Map<Object, Object> result = new HashMap<Object, Object>();
        try {
            List<MallUser> mallUsers = userService.queryByMobile(user.getMobile());
            //兼容H5
            if (mallUsers.size() == 1) {
                MallUser mallUser = mallUsers.get(0);
                user.setLevelId(mallUser.getLevelId());
                BeanUtil.copyProperties(user,mallUser,CopyOptions.create().setIgnoreNullValue(true));
                if (StrUtil.isEmpty(mallUser.getUsername())){
                    mallUser.setUsername(user.getMobile());
                    BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
                    String encodedPassword = encoder.encode(user.getMobile());
                    mallUser.setPassword(encodedPassword);
                }
                userService.updateById(mallUser);
                token = UserTokenManager.generateToken(mallUser.getId(), mallUser.getTbkRelationId(), mallUser.getTbkSpecialId());
                result.put("userInfo", mallUser);
                BmdesignUserLevel bmdesignUserLevel = bmdesignUserLevelService.selectBmdesignUserLevelById(mallUser.getLevelId());
                result.put("userLevel", bmdesignUserLevel);
            }else if (mallUsers.size() <=0){
                user.setUsername(user.getMobile());
                BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
                String encodedPassword = encoder.encode(user.getMobile());
                user.setPassword(encodedPassword);
                userService.add(user);
                token = UserTokenManager.generateToken(user.getId(), user.getTbkRelationId(), user.getTbkSpecialId());
                // 开户
                if (user.getId() != null) {
                    logger.info("开户成功..............");
                    accountService.accountTradeCreateAccount(user.getId());
                }
                BmdesignUserLevel bmdesignUserLevel = bmdesignUserLevelService.selectBmdesignUserLevelById(user.getLevelId());
                result.put("userLevel", bmdesignUserLevel);
                result.put("userInfo", user);
            }else{
                return ResponseUtil.fail("存在多个相同用户无法注册");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        result.put("token", token);
        logger.info("用户token=" + token);

        return ResponseUtil.ok(result);
    }
}
