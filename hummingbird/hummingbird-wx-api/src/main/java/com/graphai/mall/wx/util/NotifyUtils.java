package com.graphai.mall.wx.util;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class NotifyUtils {
    static final ExecutorService executor = Executors.newFixedThreadPool(4);

    /**
     * 個推接口
     * @param clientId 终端id
     */
    public static void SingleSend(String clientId) {
        PushUtil thread = new PushUtil(clientId);
        executor.execute(thread);
    }

    public static void main(String[] args) {
        //NotifyUtils.SingleSend("1fsdadczxaa");
    }
}
