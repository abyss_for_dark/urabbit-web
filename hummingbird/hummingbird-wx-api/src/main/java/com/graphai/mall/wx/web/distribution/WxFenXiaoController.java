package com.graphai.mall.wx.web.distribution;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.graphai.commons.util.AppIdAlgorithm;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.image.ActivityVo;
import com.graphai.commons.util.image.GoodsInfoVo;
import com.graphai.commons.util.image.ImageUtils;
import com.graphai.commons.util.json.JacksonUtils;
import com.graphai.framework.storage.StorageService;
import com.graphai.mall.db.domain.MallAd;
import com.graphai.mall.db.domain.MallGoods;
import com.graphai.mall.db.domain.MallGoodsProduct;
import com.graphai.mall.db.domain.MallRebateRecord;
import com.graphai.mall.db.domain.MallStorage;
import com.graphai.mall.db.service.distribution.FenXiaoService;
import com.graphai.mall.db.service.goods.MallGoodsAttributeService;
import com.graphai.mall.db.service.goods.MallGoodsProductService;
import com.graphai.mall.db.service.goods.MallGoodsService;
import com.graphai.mall.db.service.goods.MallGoodsSpecificationService;
import com.graphai.mall.db.service.promotion.ActivityService;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.mall.wx.web.common.MyBaseController;
import com.graphai.mall.wx.web.user.behavior.WxFootprintController;

@RestController
@RequestMapping("/wx/fenxiao")
public class WxFenXiaoController  extends MyBaseController {
	private final Log logger = LogFactory.getLog(WxFootprintController.class);
	@Autowired
	private FenXiaoService fenxiaoService;
	@Autowired
	private StorageService storageService;
	@Autowired
	private MallGoodsService goodsService;
	@Autowired
	private MallGoodsAttributeService goodsAttributeService;
	@Autowired
	private MallGoodsSpecificationService goodsSpecificationService;
	@Autowired
	private ActivityService activityService;
	@Autowired
	private MallGoodsProductService productService;

	String b2cDomain = "http://pdwx.gzxwsoft.com/";
	
	
	@PostMapping("poster.hechengtianxia")
	public Object posterHechengtianxia(@RequestBody Map<String,Object> requestMap, HttpServletRequest request) {
		
		ByteArrayOutputStream os1 = new ByteArrayOutputStream();
		InputStream ips = null;
		MallStorage storage = null;
		try {
			BufferedImage bg = ImageUtils.drawHeChengTianXiaPoster(requestMap, null);
			ImageIO.write(bg, "png", os1);
			ips = new ByteArrayInputStream(os1.toByteArray());
			String format = "png";
			String fileName = AppIdAlgorithm.generateCode(6).toUpperCase() + System.currentTimeMillis() + ".png";
			storage= storageService.store(ips, os1.toByteArray().length, format, fileName);
		} catch (IOException e) {
			e.printStackTrace();
		} finally{
			try {
				os1.flush();
				os1.close();
				if(ips!=null){
					ips.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return ResponseUtil.ok(storage);
	}
	
	/**
	 * 生成vip活动海报
	 * @param id
	 * @param shId
	 * @return
	 */
	@PostMapping("vip-poster")
	public Object vipPostGenerator(@LoginUser String userId,@RequestBody MallRebateRecord body, HttpServletRequest request) {
		MallRebateRecord myBody = null;
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		Map<String, Object> result = new HashMap<String, Object>();
		InputStream ips = null;
		try {
			if (body == null) {
				return ResponseUtil.fail();
			}
			
			MallRebateRecord  getRecord  = fenxiaoService.getMallRebateRecordVipByUser(userId, "vip");
			if(getRecord!=null){
				result.put("record", getRecord);
				return ResponseUtil.ok(result);
			}
			
			
			List<String> picUrls = new ArrayList<String>();
			
			ActivityVo avo = new ActivityVo();
			
			String activityId = body.getItemId();
			Map<String, Object> activitymap = activityService.getActivity(activityId);
			String content = MapUtils.getString(activitymap, "content");
			String activityPicUrl = MapUtils.getString(activitymap, "activityPicUrl");
			String activityName = MapUtils.getString(activitymap, "activityName");
			// 创建分销分享记录
			body.setStatus("vip");
			myBody = fenxiaoService.createNewFenXiao(body);

			
			avo.setActivityName(activityName);
			avo.setContent(content);
			avo.setPicUrl(activityPicUrl);
			String qrCodeContent = b2cDomain + "c5/#/pages/brand/activityDetail?id=" + activityId + "&tpid=" + userId + "&shId=" + myBody.getId();
			avo.setQrCodeContent(qrCodeContent);
			
			BufferedImage bg = ImageUtils.drawVipPoster(avo, null);

			ImageIO.write(bg, "png", os);
			ips = new ByteArrayInputStream(os.toByteArray());

			String format = "png";
			String fileName = AppIdAlgorithm.generateCode(6).toUpperCase() + System.currentTimeMillis() + ".png";
			MallStorage storage = storageService.store(ips, os.toByteArray().length, format, fileName);

			String url = "https://images.jiewangbl.com/" + storage.getKey();
			picUrls.add(url);
			
			MallRebateRecord record = new MallRebateRecord();
			record.setId(myBody.getId());
			String[] strings = new String[picUrls.size()];
			
			String[] sharePics = picUrls.toArray(strings);
			record.setSharePics(sharePics);
			record.setRemark(qrCodeContent);
			fenxiaoService.updateFenXiao(record);
			
			myBody.setRemark(qrCodeContent);
			myBody.setSharePics(sharePics);
			
			result.put("record", myBody);
			result.put("storage", storage);
		} catch (Exception e) {
			logger.error("vip海报生成异常....");
		} finally {
			try {
				if (os != null) {
					os.flush();
					os.close();
				}
				
				if(ips!=null){
					ips.close();
				}
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return ResponseUtil.ok(result);
		
	}

		/**
		 * 生成分享海报
		 * @param userId
		 * @param map
		 * @return
		 */
		@PostMapping("share-poster")
		public Object sharePostGenerator(@LoginUser String userId,@RequestBody Map<String,String> map, HttpServletRequest request) {

			ByteArrayOutputStream os = new ByteArrayOutputStream();
			Map<String, Object> result = new HashMap<String, Object>();
			MallRebateRecord myBody = null;
			InputStream ips = null;
			String picurl = map.get("picurl");
			String url = map.get("url");
			String gid = map.get("gid");
			String price = map.get("price");
			String orgPrice = map.get("orgPrice");
			String title = map.get("title");
			try {

				List<String> picUrls = new ArrayList<String>();

				ActivityVo avo = new ActivityVo();
				GoodsInfoVo gv = new GoodsInfoVo();
				gv.setGoodsId(gid);
				gv.setPicUrl(picurl);
				gv.setPrice(new BigDecimal(price));
				gv.setOrgPrice(new BigDecimal(orgPrice));
				gv.setTitle(title);
				BufferedImage bg = ImageUtils.drawSharePosterImage(gv,url);

				ImageIO.write(bg, "png", os);
				ips = new ByteArrayInputStream(os.toByteArray());

				String format = "png";
				String fileName = AppIdAlgorithm.generateCode(6).toUpperCase() + System.currentTimeMillis() + ".png";
				MallStorage storage = storageService.store(ips, os.toByteArray().length, format, fileName);

				String url1 = "https://images.jiewangbl.com/" + storage.getKey();
				picUrls.add(url1);

				MallRebateRecord record = new MallRebateRecord();
				//record.setId(myBody.getId());
				String[] strings = new String[picUrls.size()];

				String[] sharePics = picUrls.toArray(strings);
				record.setSharePics(sharePics);

				//myBody.setSharePics(sharePics);

				//result.put("record", myBody);
				result.put("storage", url1);

				System.out.println(result);

	//			result.put("share",bg.getData());
			} catch (Exception e) {
				logger.error("分享海报生成异常....");
			} finally {
				try {
					if (os != null) {
						os.flush();
						os.close();
					}

					if(ips!=null){
						ips.close();
					}

				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			return ResponseUtil.ok(result);

		}


	
	@PostMapping("requesv2")
	public Object createNewFenXiaoV2(@LoginUser String userId,@RequestBody MallRebateRecord body, HttpServletRequest request) {
		
		if(userId==null){
			ResponseUtil.unlogin();
		}
		MallRebateRecord myBody = null;
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		Map<String, Object> result = new HashMap<String, Object>();
		if (body == null) {
			return ResponseUtil.fail();
		}
		//记录分销人的用户ID
		body.setUserId(userId);
		try {
			if ("goods".equalsIgnoreCase(body.getItemType())) {
				GoodsInfoVo vo = new GoodsInfoVo();
				List<String> pics = new ArrayList<String>();
				
				String id = body.getItemId();
				
				List<MallGoodsProduct> productList = productService.queryByGid(id);
				
				//Callable<List<MallGoodsProduct>> productListCallable = () -> 

				MallGoods goods = goodsService.findById(body.getItemId());
				//FutureTask<List<MallGoodsProduct>> productListCallableTask = new FutureTask<>(productListCallable);
				//executorService.submit(productListCallableTask);

				//List<MallGoodsProduct> productList = productListCallableTask.get();
				String specStr = "";
				String spec = goods.getGoodsSn();

				// 产品非空
				if (CollectionUtils.isNotEmpty(productList)) {
					MallGoodsProduct goodsProduct = productList.get(0);
					String[] spes = goodsProduct.getSpecifications();

					if (spes != null && spes.length > 1) {
						for (int i = 0; i < spes.length; i++) {
							specStr += spes[i] + "/";
						}
					} else if (spes != null && spes.length == 1) {
						specStr += spes[0];
					}
				}

				body.setGoodsPrice(goods.getRetailPrice());
				BigDecimal nowAmount = goods.getRetailPrice();
				BigDecimal afterAmount = goods.getRetailPrice();
				if ("1".equals(body.getMarkupType())) {

				} else if ("2".equals(body.getMarkupType())) {
					BigDecimal amount = body.getMarkupRange();
					afterAmount = nowAmount.add(amount);
				} else if ("3".equals(body.getMarkupType())) {
					BigDecimal proportion = body.getMarkupRange();
					BigDecimal mydev = new BigDecimal(String.valueOf(100));
					BigDecimal bili = proportion.divide(mydev.setScale(2, BigDecimal.ROUND_HALF_UP)).setScale(2, BigDecimal.ROUND_HALF_UP);
					BigDecimal addAmount = nowAmount.multiply(bili.setScale(2, BigDecimal.ROUND_HALF_UP)).setScale(2, BigDecimal.ROUND_HALF_UP);
					afterAmount = nowAmount.add(addAmount);
				}
				body.setAfterGoodsPrice(afterAmount);
				// 创建分销分享记录
				myBody = fenxiaoService.createNewFenXiao(body);
				
				String[] gallery = goods.getGallery();

				List<String> picUrls = new ArrayList<String>();

				List<String> myDList = new ArrayList<String>();
				// 不是唯品仓同步过来的数据就要做处理 !"WPC".equals(info.getChannelCode())
				if ((org.apache.commons.lang.StringUtils.isEmpty(goods.getChannelCode()) 
						|| "JW".equalsIgnoreCase(goods.getChannelCode())) 
						&& goods != null
						&& StringUtils.isNotBlank(goods.getDetail())) {
					Element doc = Jsoup.parseBodyFragment(goods.getDetail()).body();
					Elements pngs = doc.select("img[src]");
					for (Element element : pngs) {
						String imgUrl = element.attr("src");
						myDList.add(imgUrl);
					}
				} else if (StringUtils.isNotBlank(goods.getDetail())) {
					myDList = JacksonUtils.jsn2List(goods.getDetail(), String.class);
				}

				if (CollectionUtils.isNotEmpty(myDList)) {
					if (myDList.size() >= 2) {
						picUrls.add(myDList.get(0));
						picUrls.add(myDList.get(1));
					} else if (myDList.size() == 1) {
						picUrls.add(myDList.get(0));
						if (ArrayUtils.isNotEmpty(gallery)) {
							if (gallery.length > 1) {
								picUrls.add(gallery[0]);
							} else {
								picUrls.add(goods.getPicUrl());
							}
						}
					} else {
						if (ArrayUtils.isNotEmpty(gallery)) {
							if (gallery.length >= 2) {
								picUrls.add(gallery[0]);
								picUrls.add(gallery[1]);
							} else if (gallery.length == 1) {
								picUrls.add(gallery[0]);
								picUrls.add(goods.getPicUrl());
							}
						}
					}
				}else{
					if (ArrayUtils.isNotEmpty(gallery)) {
						if (gallery.length >= 2) {
							picUrls.add(gallery[0]);
							picUrls.add(gallery[1]);
						} else if (gallery.length == 1) {
							picUrls.add(gallery[0]);
							picUrls.add(goods.getPicUrl());
						}
					}else{
						picUrls.add(goods.getPicUrl());
						picUrls.add(goods.getPicUrl());
					}
				}

				vo.setPicUrls(picUrls);
				
				String qrCodeContent = b2cDomain + "c5/#/pages/product/product?id=" + id + "&tpid=" + userId + "&shId=" + myBody.getId();
				
				vo.setGoodsId(id);
				vo.setPicUrl(goods.getPicUrl());
				vo.setHasCoupon("false");
				vo.setOrgPrice(goods.getCounterPrice());
				vo.setRebatePrice(afterAmount);
				vo.setTitle(goods.getName());
				vo.setSpecStr(specStr);
				vo.setSpec(spec);
				vo.setQrCodeContent(qrCodeContent);

				BufferedImage bg = ImageUtils.drawGoodsPoster(vo, "");

				ImageIO.write(bg, "png", os);
				InputStream ips = new ByteArrayInputStream(os.toByteArray());

				String format = "png";
				String fileName = AppIdAlgorithm.generateCode(6).toUpperCase() + System.currentTimeMillis() + ".png";
				MallStorage storage = storageService.store(ips, os.toByteArray().length, format, fileName);
				
				String url = "https://images.jiewangbl.com/" + storage.getKey();
				pics.add(url);
				
				MallRebateRecord record = new MallRebateRecord();
				record.setId(myBody.getId());
				String[] strings = new String[pics.size()];
				
				String[] sharePics = pics.toArray(strings);
				record.setSharePics(sharePics);
				record.setRemark(qrCodeContent);
				fenxiaoService.updateFenXiao(record);
				
				myBody.setRemark(qrCodeContent);
				myBody.setSharePics(sharePics);
				result.put("record", myBody);
				result.put("storage", storage);
				if(ips!=null){
					ips.close();
				}
				
			} else if ("activity".equalsIgnoreCase(body.getItemType())) {
				List<String> picUrls = new ArrayList<String>();
				
				ActivityVo avo = new ActivityVo();
				
				String activityId = body.getItemId();
				Map<String, Object> activitymap = activityService.getActivity(activityId);
				String content = MapUtils.getString(activitymap, "content");
				String activityPicUrl = MapUtils.getString(activitymap, "activityPicUrl");
				String activityName = MapUtils.getString(activitymap, "activityName");
				// 创建分销分享记录
				myBody = fenxiaoService.createNewFenXiao(body);

				
				avo.setActivityName(activityName);
				avo.setContent(content);
				avo.setPicUrl(activityPicUrl);
				String qrCodeContent = b2cDomain + "c5/#/pages/brand/activityDetail?id=" + activityId + "&tpid=" + userId + "&shId=" + myBody.getId();
				avo.setQrCodeContent(qrCodeContent);
				
				BufferedImage bg = ImageUtils.drawActivityPoster(avo, null);

				ImageIO.write(bg, "png", os);
				InputStream ips = new ByteArrayInputStream(os.toByteArray());

				String format = "png";
				String fileName = AppIdAlgorithm.generateCode(6).toUpperCase() + System.currentTimeMillis() + ".png";
				MallStorage storage = storageService.store(ips, os.toByteArray().length, format, fileName);

				String url = "https://images.jiewangbl.com/" + storage.getKey();
				picUrls.add(url);
				
				MallRebateRecord record = new MallRebateRecord();
				record.setId(myBody.getId());
				String[] strings = new String[picUrls.size()];
				
				String[] sharePics = picUrls.toArray(strings);
				record.setSharePics(sharePics);
				record.setRemark(qrCodeContent);
				fenxiaoService.updateFenXiao(record);
				
				myBody.setRemark(qrCodeContent);
				myBody.setSharePics(sharePics);
				
				result.put("record", myBody);
				result.put("storage", storage);
				
				if(ips!=null){
					ips.close();
				}
				
			} else if ("urlgoods".equalsIgnoreCase(body.getItemType())) {
				String id = body.getItemId();
				//Callable<List<MallGoodsProduct>> productListCallable = () -> productService.queryByGid(id);

				MallGoods goods = goodsService.findById(body.getItemId());
				//FutureTask<List<MallGoodsProduct>> productListCallableTask = new FutureTask<>(productListCallable);
				//executorService.submit(productListCallableTask);
				List<MallGoodsProduct> productList = productService.queryByGid(id);
				//List<MallGoodsProduct> productList = productListCallableTask.get();
				String specStr = "";
				String spec = goods.getGoodsSn();

				// 产品非空
				if (CollectionUtils.isNotEmpty(productList)) {
					MallGoodsProduct goodsProduct = productList.get(0);
					String[] spes = goodsProduct.getSpecifications();

					if (spes != null && spes.length > 1) {
						for (int i = 0; i < spes.length; i++) {
							specStr += spes[i] + "/";
						}
					} else if (spes != null && spes.length == 1) {
						specStr += spes[0];
					}
				}

				body.setGoodsPrice(goods.getRetailPrice());
				BigDecimal nowAmount = goods.getRetailPrice();
				BigDecimal afterAmount = goods.getRetailPrice();
				if ("1".equals(body.getMarkupType())) {

				} else if ("2".equals(body.getMarkupType())) {
					BigDecimal amount = body.getMarkupRange();
					afterAmount = nowAmount.add(amount);
				} else if ("3".equals(body.getMarkupType())) {
					BigDecimal proportion = body.getMarkupRange();
					BigDecimal mydev = new BigDecimal(String.valueOf(100));
					BigDecimal bili = proportion.divide(mydev.setScale(2, BigDecimal.ROUND_HALF_UP)).setScale(2, BigDecimal.ROUND_HALF_UP);
					BigDecimal addAmount = nowAmount.multiply(bili.setScale(2, BigDecimal.ROUND_HALF_UP)).setScale(2, BigDecimal.ROUND_HALF_UP);
					afterAmount = nowAmount.add(addAmount);
				}
				body.setAfterGoodsPrice(afterAmount);
				// 创建分销分享记录
				myBody = fenxiaoService.createNewFenXiao(body);

				String getUrl = b2cDomain + "c5/#/pages/product/product?id=" + id + "&tpid=" + userId + "&shId=" + myBody.getId();
				
				MallRebateRecord record = new MallRebateRecord();
				record.setId(myBody.getId());
				record.setRemark(getUrl);
				fenxiaoService.updateFenXiao(record);
				
				result.put("record", myBody);
				result.put("storage", getUrl);
			} else if ("urlactivity".equalsIgnoreCase(body.getItemType())) {
				// 创建分销分享记录
				myBody = fenxiaoService.createNewFenXiao(body);
				String activityId = body.getItemId();
				String qrCodeContent = b2cDomain + "c5/#/pages/brand/activityDetail?id=" + activityId + "&tpid=" + userId + "&shId=" + myBody.getId();
				MallRebateRecord record = new MallRebateRecord();
				record.setId(myBody.getId());
				record.setRemark(qrCodeContent);
				
				result.put("record", myBody);
				result.put("storage", qrCodeContent);
			} else if ("activityBatch".equalsIgnoreCase(body.getItemType())) {
				// 批量转发生成图片
				myBody = fenxiaoService.createNewFenXiao(body);

				String[] itemIds = body.getItemIds();
				List<String> activityIds = new ArrayList<String>();
				if (ArrayUtils.isNotEmpty(itemIds)) {
					for (int i = 0; i < itemIds.length; i++) {
						if (StringUtils.isNotBlank(itemIds[i])) {
							activityIds.add(itemIds[i]);
						}
					}
				}

				List<MallStorage> storages = new ArrayList<MallStorage>();

				List<MallAd> ads = activityService.getActivityList(activityIds);

				List<String> pics = new ArrayList<String>();
				// String qrCodeContent = body.getRemark() + "&shId=" +
				// myBody.getId();
				for (int i = 0; i < ads.size(); i++) {
					ByteArrayOutputStream os1 = new ByteArrayOutputStream();
					MallAd ad = ads.get(i);
					//String qrCodeContent = b2cDomain + "c5/#/pages/brand/activityListv2?tpid=" + body.getUserId() + "&shId=" + myBody.getId();
					String qrCodeContent = b2cDomain + "c5/#/pages/brand/activityDetail?id=" + ad.getId() + "&tpid=" + userId + "&shId=" + myBody.getId();
					
					ActivityVo avo = new ActivityVo();
					avo.setActivityName(ad.getName());
					avo.setContent(ad.getContent());
					avo.setPicUrl(ad.getUrl());
					avo.setQrCodeContent(qrCodeContent);
					BufferedImage bg = ImageUtils.drawActivityPoster(avo, null);

					ImageIO.write(bg, "png", os1);
					InputStream ips = new ByteArrayInputStream(os1.toByteArray());

					String format = "png";
					String fileName = AppIdAlgorithm.generateCode(6).toUpperCase() + System.currentTimeMillis() + ".png";
					MallStorage storage = storageService.store(ips, os1.toByteArray().length, format, fileName);
					storages.add(storage);

					String url = "https://images.jiewangbl.com/" + storage.getKey();
					pics.add(url);
					os1.flush();
					os1.close();
					if(ips!=null){
						ips.close();
					}
					
				}

				MallRebateRecord record = new MallRebateRecord();
				record.setId(myBody.getId());
				String[] strings = new String[pics.size()];
				
				String[] sharePics = pics.toArray(strings);
				record.setSharePics(sharePics);
				
				fenxiaoService.updateFenXiao(record);
				
				myBody.setSharePics(sharePics);
				result.put("record", myBody);
				result.put("storage", storages);
			} else if ("goodsBatch".equalsIgnoreCase(body.getItemType())) {
				// 批量转发生成图片
				myBody = fenxiaoService.createNewFenXiao(body);

				String[] itemIds = body.getItemIds();
				List<String> goodsIds = new ArrayList<String>();
				if (ArrayUtils.isNotEmpty(itemIds)) {
					for (int i = 0; i < itemIds.length; i++) {
						if (StringUtils.isNotBlank(itemIds[i])) {
							goodsIds.add(itemIds[i]);
						}
					}
				}

				List<MallStorage> storages = new ArrayList<MallStorage>();

				List<MallGoods> goodsList = goodsService.findByIdList(goodsIds);

				List<String> pics = new ArrayList<String>();
				// String qrCodeContent = body.getRemark() + "&shId=" +
				// myBody.getId();
				for (int i = 0; i < goodsList.size(); i++) {
					ByteArrayOutputStream os1 = new ByteArrayOutputStream();
					MallGoods goods = goodsList.get(i);
					//String qrCodeContent = b2cDomain + "c5/#/pages/brand/activityListv2?tpid=" + body.getUserId() + "&shId=" + myBody.getId();
					String qrCodeContent = b2cDomain + "c5/#/pages/product/product?id=" + goods.getId() + "&tpid=" + userId + "&shId=" + myBody.getId();
					
					GoodsInfoVo avo = new GoodsInfoVo();
					avo.setTitle(goods.getName());
					String[] gallery = goods.getGallery();

					List<String> picUrls = new ArrayList<String>();

					List<String> myDList = new ArrayList<String>();
					// 不是唯品仓同步过来的数据就要做处理 !"WPC".equals(info.getChannelCode())
					if ((org.apache.commons.lang.StringUtils.isEmpty(goods.getChannelCode()) || "JW".equalsIgnoreCase(goods.getChannelCode())) && goods != null
							&& StringUtils.isNotBlank(goods.getDetail())) {
						Element doc = Jsoup.parseBodyFragment(goods.getDetail()).body();
						Elements pngs = doc.select("img[src]");
						for (Element element : pngs) {
							String imgUrl = element.attr("src");
							myDList.add(imgUrl);
						}
					} else if (StringUtils.isNotBlank(goods.getDetail())) {
						myDList = JacksonUtils.jsn2List(goods.getDetail(), String.class);
					}

					if (CollectionUtils.isNotEmpty(myDList)) {
						if (myDList.size() >= 2) {
							picUrls.add(myDList.get(0));
							picUrls.add(myDList.get(1));
						} else if (myDList.size() == 1) {
							picUrls.add(myDList.get(0));
							if (ArrayUtils.isNotEmpty(gallery)) {
								if (gallery.length > 1) {
									picUrls.add(gallery[0]);
								} else {
									picUrls.add(goods.getPicUrl());
								}
							}
						} else {
							if (ArrayUtils.isNotEmpty(gallery)) {
								if (gallery.length >= 2) {
									picUrls.add(gallery[0]);
									picUrls.add(gallery[1]);
								} else if (gallery.length == 1) {
									picUrls.add(gallery[0]);
									picUrls.add(goods.getPicUrl());
								}
							}
						}
					}else{
						if (ArrayUtils.isNotEmpty(gallery)) {
							if (gallery.length >= 2) {
								picUrls.add(gallery[0]);
								picUrls.add(gallery[1]);
							} else if (gallery.length == 1) {
								picUrls.add(gallery[0]);
								picUrls.add(goods.getPicUrl());
							}
						}else{
							picUrls.add(goods.getPicUrl());
							picUrls.add(goods.getPicUrl());
						}
					}

					avo.setPicUrls(picUrls);

					/*** 加价的情况下要计算加价金额 ***/
					BigDecimal nowAmount = goods.getRetailPrice();
					BigDecimal afterAmount = goods.getRetailPrice();
					if ("1".equals(body.getMarkupType())) {

					} else if ("2".equals(body.getMarkupType())) {
						BigDecimal amount = body.getMarkupRange();
						afterAmount = nowAmount.add(amount);
					} else if ("3".equals(body.getMarkupType())) {
						BigDecimal proportion = body.getMarkupRange();
						BigDecimal mydev = new BigDecimal(String.valueOf(100));
						BigDecimal bili = proportion.divide(mydev.setScale(2, BigDecimal.ROUND_HALF_UP)).setScale(2, BigDecimal.ROUND_HALF_UP);
						BigDecimal addAmount = nowAmount.multiply(bili.setScale(2, BigDecimal.ROUND_HALF_UP)).setScale(2, BigDecimal.ROUND_HALF_UP);
						afterAmount = nowAmount.add(addAmount);
					}

					avo.setPicUrl(goods.getPicUrl());
					avo.setRebatePrice(afterAmount);
					avo.setQrCodeContent(qrCodeContent);
					BufferedImage bg = ImageUtils.drawGoodsPoster(avo, null);

					ImageIO.write(bg, "png", os1);
					InputStream ips = new ByteArrayInputStream(os1.toByteArray());

					String format = "png";
					String fileName = AppIdAlgorithm.generateCode(6).toUpperCase() + System.currentTimeMillis() + ".png";
					MallStorage storage = storageService.store(ips, os1.toByteArray().length, format, fileName);
					storages.add(storage);

					String url = "https://images.jiewangbl.com/" + storage.getKey();
					pics.add(url);
					os1.flush();
					os1.close();
					if(ips!=null){
						ips.close();
					}
				}

				MallRebateRecord record = new MallRebateRecord();
				record.setId(myBody.getId());
				String[] strings = new String[pics.size()];
				record.setSharePics(pics.toArray(strings));
				fenxiaoService.updateFenXiao(record);
				String[] sharePics = pics.toArray(strings);
				record.setSharePics(sharePics);
				myBody.setSharePics(sharePics);
				
				result.put("record", myBody);
				result.put("storage", storages);
			}

		} catch (Exception e) {
			logger.error("分销记录创建异常 :" + e.getMessage(), e);
		} finally {
			try {
				if (os != null) {
					os.flush();
					os.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return ResponseUtil.ok(result);
	}

	@GetMapping("single/rebate")
	public Object getFenXiaoRebate(@RequestParam String id) {
		MallRebateRecord record = fenxiaoService.getMallRebateRecordById(id);
		return ResponseUtil.ok(record);
	}

	@PostMapping
	public Object createNewFenXiao(@RequestBody MallRebateRecord body, HttpServletRequest request) {
		MallRebateRecord myBody = null;
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		InputStream ips = null;
		try {
			myBody = fenxiaoService.createNewFenXiao(body);
			logger.info("数据交互 ：" + JacksonUtils.bean2Jsn(body));
			if (StringUtils.isNotBlank(body.getRemark())) {
				String content = myBody.getRemark() + "&shId=" + myBody.getId();
				int width = 300;
				int height = 300;
				String format = "png";
				HashMap<Object, Object> map = new HashMap<Object, Object>();
				map.put(EncodeHintType.CHARACTER_SET, "utf-8");
				map.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M);
				map.put(EncodeHintType.MARGIN, 0);
				BitMatrix bm = new MultiFormatWriter().encode(content, BarcodeFormat.QR_CODE, width, height);
				MatrixToImageWriter.writeToStream(bm, format, os);
				ips = new ByteArrayInputStream(os.toByteArray());
				// 上传到OSS是上传字节的形式
				String fileName = AppIdAlgorithm.generateCode(6).toUpperCase() + System.currentTimeMillis() + ".png";
				MallStorage storage = storageService.store(ips, os.toByteArray().length, format, fileName);
				String qrCodeUrl = storage.getUrl();
				myBody.setRemark(qrCodeUrl);
			}

		} catch (Exception e) {
			logger.error("分销记录创建异常 :" + e.getMessage(), e);
		} finally {
			try {
				if (os != null) {
					os.flush();
					os.close();
				}
				
				if (ips != null) {
					ips.close();
				}
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return ResponseUtil.ok(myBody);
	}
}
