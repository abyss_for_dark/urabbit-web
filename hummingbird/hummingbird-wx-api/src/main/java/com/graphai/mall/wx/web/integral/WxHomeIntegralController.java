package com.graphai.mall.wx.web.integral;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.json.JacksonUtils;
import com.graphai.mall.core.system.SystemConfig;
import com.graphai.mall.db.domain.MallAd;
import com.graphai.mall.db.domain.MallGoods;
import com.graphai.mall.db.service.advertisement.MallAdService;
import com.graphai.mall.db.service.advertisement.MallBrandService;
import com.graphai.mall.db.service.category.MallCategoryService;
import com.graphai.mall.db.service.coupon.MallCouponService;
import com.graphai.mall.db.service.goods.MallGoodsService;
import com.graphai.mall.db.service.groupon.MallGrouponRulesService;
import com.graphai.mall.db.service.integral.MallGoodsIntegralService;
import com.graphai.mall.db.service.promotion.MallTopicService;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.mall.wx.service.HomeCacheManager;
import com.graphai.mall.wx.web.common.MyBaseController;
import com.graphai.mall.wx.web.home.WxHomeController;

/**
 * 首页服务-积分商城
 * 
 * @author Qxian
 * @date 2020-10-14
 * @version V0.0.1
 */
@RestController
@RequestMapping("/wx/home/integral")
@Validated
public class WxHomeIntegralController extends MyBaseController {
    private final Log logger = LogFactory.getLog(WxHomeController.class);

    @Autowired
    private MallAdService adService;

    @Autowired
    private MallGoodsService goodsService;

    @Autowired
    private MallGoodsIntegralService mallGoodsIntegralService;

    @Autowired
    private MallBrandService brandService;

    @Autowired
    private MallTopicService topicService;

    @Autowired
    private MallCategoryService categoryService;

    @Autowired
    private MallGrouponRulesService grouponRulesService;

    @Autowired
    private MallCouponService couponService;

    @GetMapping("/cache")
    public Object cache(@NotNull String key) {
        if (!key.equals("Mall_cache")) {
            return ResponseUtil.fail();
        }

        // 清除缓存
        HomeCacheManager.clearAll();
        return ResponseUtil.ok("缓存已清除");
    }

    /**
     * 首页广告-未莱卡
     * 
     * @return 首页数据
     */
    @GetMapping("/indexWeilayCard")
    public Object indexWeilayCard(@RequestParam(value = "type", required = false, defaultValue = "25") Integer type) {
        // List<MallAd> adList = adService.queryIndexWeilayCard(type);
        // 首页广告-未莱卡
        List<MallAd> adList = adService.queryIndex(Byte.parseByte("25"));
        Map<String, Object> data = new HashMap<>();
        data.put("banner", adList);
        return ResponseUtil.ok(data);
    }

    /**
     * 首页数据
     * 
     * @param //userId 当用户已经登录时，非空。为登录状态为null
     * @return 首页数据
     */
    @GetMapping("/index")
    public Object index(String industryId) throws Exception {

        if (industryId == null || "".equals(industryId)) {
            industryId = "0";
        }
        Map<String, Object> data = new HashMap<>();
        // 获取轮播广告
        List<MallAd> adList = adService.queryIndex(Byte.parseByte("24"));
        // 热卖产品
        List<MallGoods> hotGoodsList = mallGoodsIntegralService.queryByHot(0, SystemConfig.getHotLimit(), industryId);
        try {
            data.put("banner", adList);
            logger.info("获取到广告列表的数据 ： " + JacksonUtils.bean2Jsn(adList));

            // 热卖产品
            data.put("hotGoodsList", hotGoodsList);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // executorService.shutdown();
        }
        return ResponseUtil.ok(data);
    }


    @GetMapping("/indexBak")
    public Object indexBak(@LoginUser Integer userId, Integer industryId) {
        /*
         * if (userId == null) { return ResponseUtil.unlogin(); } //优先从缓存中读取
         *//**
            * if (HomeCacheManager.hasData(HomeCacheManager.INDEX)) { return
            * ResponseUtil.ok(HomeCacheManager.getCacheData(HomeCacheManager.INDEX)); }
            **//*
                * 
                * Map<String, Object> data = new HashMap<>();
                * 
                * //广告列表 Callable<List<MallAd>> bannerListCallable = () -> adService.queryIndex();
                * 
                * //查询产品目录 Callable<List<MallCategory>> channelListCallable = () ->
                * categoryService.queryChannel(industryId,"L1"); //查询优惠券 Callable<List<MallCoupon>>
                * couponListCallable; // if(userId == null){ // couponListCallable = () ->
                * couponService.queryList(0, 3); // } else { couponListCallable = () ->
                * couponService.queryAvailableList(userId,0, 3); // }
                * 
                * //新品上市 Callable<List<MallGoods>> newGoodsListCallable = () -> goodsService.queryByNew(0,
                * SystemConfig.getNewLimit(),industryId); //热卖产品 Callable<List<MallGoods>> hotGoodsListCallable =
                * () -> goodsService.queryByHot(0, SystemConfig.getHotLimit(),industryId);
                * 
                * Callable<List<MallBrand>> brandListCallable = () -> brandService.queryVO(0,
                * SystemConfig.getBrandLimit());
                * 
                * Callable<List<MallTopic>> topicListCallable = () -> topicService.queryList(0,
                * SystemConfig.getTopicLimit());
                * 
                * //团购专区 Callable<List<Map<String, Object>>> grouponListCallable = () ->
                * grouponRulesService.queryList(0, 5);
                * 
                * Callable<List<Map<String,Object>>> floorGoodsListCallable = this::getCategoryList;
                * 
                * FutureTask<List<MallAd>> bannerTask = new FutureTask<>(bannerListCallable);
                * FutureTask<List<MallCategory>> channelTask = new FutureTask<>(channelListCallable);
                * FutureTask<List<MallCoupon>> couponListTask = new FutureTask<>(couponListCallable);
                * FutureTask<List<MallGoods>> newGoodsListTask = new FutureTask<>(newGoodsListCallable);
                * FutureTask<List<MallGoods>> hotGoodsListTask = new FutureTask<>(hotGoodsListCallable);
                * FutureTask<List<MallBrand>> brandListTask = new FutureTask<>(brandListCallable);
                * FutureTask<List<MallTopic>> topicListTask = new FutureTask<>(topicListCallable);
                * FutureTask<List<Map<String, Object>>> grouponListTask = new FutureTask<>(grouponListCallable);
                * FutureTask<List<Map<String,Object>>> floorGoodsListTask = new
                * FutureTask<>(floorGoodsListCallable);
                * 
                * executorService.submit(bannerTask); executorService.submit(channelTask);
                * executorService.submit(couponListTask); executorService.submit(newGoodsListTask);
                * executorService.submit(hotGoodsListTask); executorService.submit(brandListTask);
                * executorService.submit(topicListTask); executorService.submit(grouponListTask);
                * executorService.submit(floorGoodsListTask);
                * 
                * try { data.put("banner", bannerTask.get());
                * 
                * logger.info("获取到广告列表的数据 ： "+ JacksonUtils.bean2Jsn(bannerTask.get()) ); //目录分类
                * data.put("channel", channelTask.get()); //优惠券列表 data.put("couponList", couponListTask.get());
                * //新品上市 List<MallGoods> newGoodsList = newGoodsListTask.get();
                * if(CollectionUtils.isNotEmpty(newGoodsList)){ for (int i = 0; i < newGoodsList.size(); i++) {
                * MallGoods myGoods = newGoodsList.get(i); myGoods.setSpreads(GoodsUtils.getSpreads(myGoods)); } }
                * 
                * data.put("newGoodsList",newGoodsList );
                * 
                * List<MallGoods> hotGoodsList = hotGoodsListTask.get();
                * if(CollectionUtils.isNotEmpty(hotGoodsList)){ for (int i = 0; i < hotGoodsList.size(); i++) {
                * MallGoods myGoods = hotGoodsList.get(i); myGoods.setSpreads(GoodsUtils.getSpreads(myGoods)); } }
                * 
                * //热卖产品 data.put("hotGoodsList", hotGoodsList); //品牌制造商 data.put("brandList",
                * brandListTask.get()); //精选商品 data.put("topicList", topicListTask.get()); //团购专区
                * data.put("grouponList", grouponListTask.get()); data.put("floorGoodsList",
                * floorGoodsListTask.get()); //缓存数据 //HomeCacheManager.loadData(HomeCacheManager.INDEX, data); }
                * catch (Exception e) { e.printStackTrace(); }finally { //executorService.shutdown(); } return
                * ResponseUtil.ok(data);
                */
        return ResponseUtil.ok();
    }


    private List<Map<String, Object>> getCategoryList() {
        /*
         * List<Map<String,Object>> categoryList = new ArrayList<>(); List<MallCategory> catL1List =
         * categoryService.queryL1WithoutRecommend(0, SystemConfig.getCatlogListLimit()); for (MallCategory
         * catL1 : catL1List) { List<MallCategory> catL2List = categoryService.queryByPid(catL1.getId());
         * List<Integer> l2List = new ArrayList<>(); for (MallCategory catL2 : catL2List) {
         * l2List.add(catL2.getId()); }
         * 
         * List<MallGoods> categoryGoods; if (l2List.size() == 0) { categoryGoods = new ArrayList<>(); }
         * else { categoryGoods = goodsService.queryByCategory(l2List, 0,
         * SystemConfig.getCatlogMoreLimit()); }
         * 
         * Map<String, Object> catGoods = new HashMap<>(); catGoods.put("id", catL1.getId());
         * catGoods.put("name", catL1.getName()); catGoods.put("goodsList", categoryGoods);
         * categoryList.add(catGoods); } return categoryList;
         */
        List<Map<String, Object>> categoryList = new ArrayList<>();
        return categoryList;
    }
}
