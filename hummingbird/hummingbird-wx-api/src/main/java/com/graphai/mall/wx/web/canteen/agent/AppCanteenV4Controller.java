package com.graphai.mall.wx.web.canteen.agent;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.id.GraphaiIdGenerator;
import com.graphai.mall.admin.domain.MallUserMerchant;
import com.graphai.mall.admin.service.IMallUserMerchantService;
import com.graphai.mall.canteen.domain.*;
import com.graphai.mall.canteen.form.AppCanteenCartFoodForm;
import com.graphai.mall.canteen.service.*;
import com.graphai.mall.db.constant.Constants;
import com.graphai.mall.db.vo.KndCanteenFoodSpecificationVo;
import com.graphai.mall.db.vo.QueryeForm;
import com.graphai.mall.wx.annotation.LoginUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 小程序-堂食
 *
 * @author Administrator
 */
@RestController
@RequestMapping("/wx/b/canteen")
public class AppCanteenV4Controller {

    protected final Logger log = LoggerFactory.getLogger(AppCanteenV4Controller.class);

    //    @Autowired
//    private DoPayServiceUtil doPayServiceUtil;
    @Autowired
    private IKndCanteenTableService kndCanteenTableService;
    @Autowired
    private IKndCanteenFoodService kndCanteenFoodService;
    @Autowired
    private IKndCanteenCategoryService kndCanteenCategoryService;
    @Autowired
    private IKndCanteenFoodSpecificationService kndCanteenFoodSpecificationService;
    @Autowired
    private IKndCanteenFoodProductService kndCanteenFoodProductService;
    @Autowired
    private IKndCanteenCartService kndCanteenCartService;
    @Autowired
    private IKndCanteenOrderService kndCanteenOrderService;

    @Autowired
    private IKndCanteenOrderFoodService kndCanteenOrderFoodService;

    @Autowired
    private IKndCanteenBaseCostService kndCanteenBaseCostService;

    @Autowired
    private IKndCanteenLabelService kndCanteenLabelService;

    @Autowired
    private IMallUserMerchantService userMerchantService;
    /** ------------------------------ 菜品列表 （KndCanteenFood） ------------------------------ **/
    /**
     * 查询菜品分类列表、商户名称、店铺名称
     */
    @CrossOrigin
    @PostMapping("/selectCanteenCategoryList")
    public Object selectCanteenCategoryList(@LoginUser String userId, @RequestParam(value = "tableId", required = false) String tableId) {
        //通过userId获取商户信息
        MallUserMerchant userMerchant = userMerchantService.getOne(new QueryWrapper<MallUserMerchant>()
                .lambda().eq(MallUserMerchant::getUserId, userId));
        if (Objects.isNull(userMerchant)) {
            return ResponseUtil.badArgumentValue("商户信息不存在");
        }

        KndCanteenCategory kndCanteenCategory = new KndCanteenCategory();
        kndCanteenCategory.setMerchantId(userMerchant.getMerchantId());
        List<KndCanteenCategory> kndCanteenCategoryList = kndCanteenCategoryService.getKndCanteenCategoryList(kndCanteenCategory);  //分类信息
        Map map = new HashMap<String, Object>();
        map.put("tableId", tableId);
        if (StrUtil.isNotEmpty(tableId)) {
            KndCanteenTable kndCanteenTable = kndCanteenTableService.selectKndCanteenTableById(tableId);
            if (null != kndCanteenTable) {
                map.put("tableSn", kndCanteenTable.getTableSn());
            }
        }
        map.put("kndCanteenCategoryList", kndCanteenCategoryList);
        return ResponseUtil.ok(map);
    }

    /**
     * 查询菜品列表
     */
    @PostMapping("/selectCanteenFoodList")
    public Object selectCanteenFoodList(@LoginUser String userId, @RequestParam(value = "categoryId") String categoryId,
                                        String foodName, String storeId, Integer isOnSale) {
        //通过userId获取商户信息
        MallUserMerchant userMerchant = userMerchantService.getOne(new QueryWrapper<MallUserMerchant>()
                .lambda().eq(MallUserMerchant::getUserId, userId));
        if (Objects.isNull(userMerchant)) {
            return ResponseUtil.badArgumentValue("商户信息不存在");
        }

        KndCanteenFood kndCanteenFood = new KndCanteenFood();
        kndCanteenFood.setMerchantId(userMerchant.getMerchantId());
        kndCanteenFood.setIsOnSale(isOnSale == 2 ? null : 0); // 兼容处理, 可查全部
        if (StrUtil.isNotBlank(categoryId)) {
            kndCanteenFood.setCategoryId(categoryId);
        }
        if (StrUtil.isNotBlank(foodName)) {
            kndCanteenFood.setFoodName(foodName);
        }
        List<KndCanteenFood> kndCanteenFoodList = kndCanteenFoodService.selectKndCanteenFoodList(kndCanteenFood);
        return ResponseUtil.ok(kndCanteenFoodList);
    }

    /**
     * 选择菜品产品
     */
    @PostMapping("/selectCanteenFoodProductList")
    public Object selectCanteenFoodProductList(@LoginUser String userId, @RequestParam(value = "foodId") String foodId) {
        if (StrUtil.isEmpty(foodId)) {
            return ResponseUtil.fail("未获取到菜品Id！");
        }

        //通过userId获取商户信息
        MallUserMerchant userMerchant = userMerchantService.getOne(new QueryWrapper<MallUserMerchant>()
                .lambda().eq(MallUserMerchant::getUserId, userId));
        if (Objects.isNull(userMerchant)) {
            return ResponseUtil.badArgumentValue("商户信息不存在");
        }

        //查询菜品规格list
        KndCanteenFoodSpecification kndCanteenFoodSpecification = new KndCanteenFoodSpecification();
        kndCanteenFoodSpecification.setMerchantId(userMerchant.getMerchantId());
        kndCanteenFoodSpecification.setFoodId(foodId);
        kndCanteenFoodSpecification.setDeleted(0);
        List<KndCanteenFoodSpecification> canteenFoodSpecificationList = kndCanteenFoodSpecificationService.selectKndCanteenFoodSpecificationList(kndCanteenFoodSpecification);
        List<KndCanteenFoodSpecificationVo> kndCanteenFoodSpecificationList = new ArrayList<>();
        if (canteenFoodSpecificationList.size() > 0) {
            //同 Specification的菜品产品分类
            Map<String, List<KndCanteenFoodSpecification>> collectList = canteenFoodSpecificationList.stream().collect(Collectors.groupingBy(KndCanteenFoodSpecification::getSpecification));
            //获取所有 Specification
            List<String> tableNames = canteenFoodSpecificationList.stream().map(KndCanteenFoodSpecification::getSpecification).collect(Collectors.toList());
            //同 Specification放入一个list
            List<String> ketList = tableNames.stream().distinct().collect(Collectors.toList());
            //将 Specification放入list中重组
            for (int i = 0; i < ketList.size(); i++) {
                KndCanteenFoodSpecificationVo kndCanteenFoodSpecificationVo = new KndCanteenFoodSpecificationVo();
                kndCanteenFoodSpecificationVo.setName(ketList.get(i));
                kndCanteenFoodSpecificationVo.setValueList(collectList.get(ketList.get(i)));
                kndCanteenFoodSpecificationList.add(kndCanteenFoodSpecificationVo);
            }
        }

        //菜品规格
        KndCanteenFoodProduct kndCanteenFoodProduct = new KndCanteenFoodProduct();
        kndCanteenFoodProduct.setFoodId(foodId);
        kndCanteenFoodProduct.setDeleted(0);
        List<KndCanteenFoodProduct> kndCanteenFoodProductList = kndCanteenFoodProductService.selectKndCanteenFoodProductList(kndCanteenFoodProduct);

        Map map = new HashMap();
        map.put("kndCanteenFoodSpecificationList", kndCanteenFoodSpecificationList);
        map.put("kndCanteenFoodProductList", kndCanteenFoodProductList);
        return ResponseUtil.ok(map);
    }


    /** ------------------------------ 购物车 （KndCanteenCart） ------------------------------ **/
    /**
     * 查询用户购物车的菜品信息
     */
    @PostMapping("/selectCanteenCart")
    public Object selectCanteenCart(@LoginUser String userId) {
        //通过userId获取商户信息
        MallUserMerchant userMerchant = userMerchantService.getOne(new QueryWrapper<MallUserMerchant>()
                .lambda().eq(MallUserMerchant::getUserId, userId));
        if (Objects.isNull(userMerchant)) {
            return ResponseUtil.badArgumentValue("商户信息不存在");
        }
        KndCanteenCart kndCanteenCart = new KndCanteenCart();
        kndCanteenCart.setMerchantId(userMerchant.getMerchantId());
        kndCanteenCart.setUserOpenId(userId);
        List<KndCanteenCart> kndCanteenCartList = kndCanteenCartService.selectKndCanteenCartListByUser(kndCanteenCart);
        //返回购物车的菜品产品信息是否失效
        if (kndCanteenCartList.size() > 0) {
            for (int i = 0; i < kndCanteenCartList.size(); i++) {
                if (StrUtil.isNotEmpty(kndCanteenCartList.get(i).getFoodProductId())) {
                    kndCanteenCartList.get(i).setInvalid(false);
                } else {
                    kndCanteenCartList.get(i).setInvalid(true);
                }
            }
        }
        return ResponseUtil.ok("操作成功！", kndCanteenCartList);
    }

    /**
     * 添加菜品到购物车
     */
    @PostMapping("/updateCanteenCart")
    public Object updateCanteenCart(@LoginUser String userId, @RequestBody @Validated AppCanteenCartFoodForm appCanteenCartsForm) {

        List<KndCanteenFoodProduct> foodProductList = appCanteenCartsForm.getFoodProductList();
        if (foodProductList.size() == 0) {
            return ResponseUtil.fail("请选择菜品！");
        }

        //通过userId获取商户信息
        MallUserMerchant userMerchant = userMerchantService.getOne(new QueryWrapper<MallUserMerchant>()
                .lambda().eq(MallUserMerchant::getUserId, userId));
        if (Objects.isNull(userMerchant)) {
            return ResponseUtil.badArgumentValue("商户信息不存在");
        }

        //循环菜品产品信息
        for (int i = 0; i < foodProductList.size(); i++) {
            //判断购物车里面是否已经存在 指定prodtcuId 的菜品产品信息
            KndCanteenFoodProduct kndCanteenFoodProduct = foodProductList.get(i);
            //判断菜品是否下架
            KndCanteenFoodProduct foodProduct = kndCanteenFoodProductService.selectKndCanteenFoodProductById(kndCanteenFoodProduct.getId());    //菜品产品
            if (ObjectUtil.isEmpty(foodProduct)) {
                return ResponseUtil.fail("存在下架菜品，请先清空下架菜品！");
            }
            KndCanteenFood food = kndCanteenFoodService.selectKndCanteenFoodById(foodProduct.getFoodId());      //菜品

            if (kndCanteenFoodProduct.getNumber() > food.getSurplusNumber() || kndCanteenFoodProduct.getNumber() < 0) {
                return ResponseUtil.fail("当日库存不足！");
            }

            //若购物车已存在菜品产品信息 则修改，若不存在则添加
            KndCanteenCart cart = new KndCanteenCart();
            cart.setMerchantId(userMerchant.getMerchantId());
            cart.setStoreId(food.getStoreId());
            cart.setUserOpenId(userId.toString());
            cart.setProductId(foodProduct.getId());     //菜品产品id主键
            List<KndCanteenCart> kndCanteenCartList = kndCanteenCartService.selectKndCanteenCartList(cart);
            if (kndCanteenCartList.size() == 0) {
                if (kndCanteenFoodProduct.getNumber() > 0) {
                    KndCanteenCart kndCanteenCart = new KndCanteenCart();
                    kndCanteenCart.setMerchantId(userMerchant.getMerchantId());
                    kndCanteenCart.setStoreId(food.getStoreId());
                    kndCanteenCart.setUserOpenId(userId.toString());

                    kndCanteenCart.setFoodId(foodProduct.getFoodId());
                    kndCanteenCart.setFoodName(food.getFoodName());
                    kndCanteenCart.setProductId(foodProduct.getId());
                    kndCanteenCart.setPrice(foodProduct.getPrice());
                    kndCanteenCart.setNumber(kndCanteenFoodProduct.getNumber());
                    kndCanteenCart.setSpecifications(foodProduct.getSpecifications());
                    kndCanteenCart.setPicUrl(food.getPicUrl());

                    kndCanteenCartService.insertKndCanteenCart(kndCanteenCart);
                }
            } else {
                if (kndCanteenCartList.size() > 1) {
                    return ResponseUtil.fail("购物车添加失败，找到多条数据！");
                }
                KndCanteenCart kndCanteenCart = kndCanteenCartList.get(0);

                if (kndCanteenFoodProduct.getNumber() != 0) {
                    KndCanteenCart cartUpdate = new KndCanteenCart();
                    cartUpdate.setId(kndCanteenCart.getId());
                    cartUpdate.setNumber(kndCanteenFoodProduct.getNumber());
                    kndCanteenCartService.updateKndCanteenCart(cartUpdate);
                } else {
                    //删除购物车中的 数量为 0的菜品信息
                    kndCanteenCartService.deleteKndCanteenCartById(kndCanteenCart.getId());
                }
            }

        }

        return ResponseUtil.ok("操作成功！");
    }

    /**
     * 清空购物车
     */
    @PostMapping("/clearCanteenCart")
    public Object clearCanteenCart(@LoginUser String userId, @RequestParam("ids") String ids) {
        return kndCanteenCartService.deleteKndCanteenCartByIds(ids) > 0 ? ResponseUtil.ok() : ResponseUtil.fail();
    }


    /** ------------------------------ 菜品订单 （KndCanteenOrder） ------------------------------ **/
    /**
     * 查询订单信息
     */
    @PostMapping("/selectCanteenOrderList")
    public Object selectCanteenOrderList(@LoginUser String userId, @RequestBody QueryeForm queryeForm) {
        //通过userId获取商户信息
        MallUserMerchant userMerchant = userMerchantService.getOne(new QueryWrapper<MallUserMerchant>()
                .lambda().eq(MallUserMerchant::getUserId, userId));
        if (Objects.isNull(userMerchant)) {
            return ResponseUtil.badArgumentValue("商户信息不存在");
        }

        KndCanteenOrder kndCanteenOrder = new KndCanteenOrder();
        kndCanteenOrder.setMerchantId(userMerchant.getMerchantId());
        Map<String, Object> paramsMap = new HashMap<>();
        if (StrUtil.isNotEmpty(queryeForm.getStatus())) {
            paramsMap.put("orderStatus", Convert.toStrArray(queryeForm.getStatus()));
        }
        paramsMap.put("beginTime", queryeForm.getStartTime());
        paramsMap.put("endTime", queryeForm.getEndTime());
        paramsMap.put("name", queryeForm.getName());
        kndCanteenOrder.setParams(paramsMap);
        kndCanteenOrder.setIsShip(queryeForm.getIsShip());
        kndCanteenOrder.setPeisongStatus(queryeForm.getPeisongStatus());
        kndCanteenOrder.setDeliveryId(queryeForm.getDeliveryId());
        List<KndCanteenOrder> kndCanteenOrderList = kndCanteenOrderService.selectKndCanteenOrderListInfo(kndCanteenOrder);
        return ResponseUtil.ok("操作成功！", kndCanteenOrderList);
    }

    /**
     * 下单（添加 knd_canteen_order、knd_canteen_order_food 表数据），同时删除购物车里面的数据(删除knd_canteen_cart表数据)
     */
    @PostMapping("/submitOrUpdateCanteenOrder")
    @Transactional
    public Object submitOrUpdateCanteenOrder(@LoginUser String userId, @RequestBody @Validated AppCanteenCartFoodForm appCanteenCartsForm) {
        MallUserMerchant userMerchant = userMerchantService.getOne(new QueryWrapper<MallUserMerchant>()
                .lambda().eq(MallUserMerchant::getUserId, userId));
        if (Objects.isNull(userMerchant)) {
            return ResponseUtil.badArgumentValue("商户信息不存在");
        }

        if (appCanteenCartsForm.getOrderStatus() != 4 && !Constants.NO.equals(appCanteenCartsForm.getOrderStatus())) {
            return ResponseUtil.fail("下单失败，请选择下单或挂单");
        }
        boolean isUpdateOrder = !StrUtil.isEmpty(appCanteenCartsForm.getOrderId());
        if (isUpdateOrder) {
            KndCanteenOrder order = kndCanteenOrderService.selectKndCanteenOrderById(appCanteenCartsForm.getOrderId());
            if (order == null || !Constants.NO.equals(order.getOrderStatus()) && appCanteenCartsForm.getOrderStatus() != 4) {
                return ResponseUtil.fail("订单状态非下单状态，修改失败");
            }
        }

        List<KndCanteenFoodProduct> foodProductList = appCanteenCartsForm.getFoodProductList();
        if (foodProductList.isEmpty()) {
            return ResponseUtil.fail("请选择菜品！");
        }

        // 用户支付金额
        BigDecimal payAmount = new BigDecimal(0);
        // 订单总优惠价
        BigDecimal totalCouponPrice = new BigDecimal(0);

        for (KndCanteenFoodProduct kndCanteenFoodProduct : foodProductList) {

            KndCanteenFoodProduct foodProduct = kndCanteenFoodProductService.selectKndCanteenFoodProductById(kndCanteenFoodProduct.getId());
            if (ObjectUtil.isEmpty(foodProduct)) {
                return ResponseUtil.fail("存在下架菜品，请先清空下架菜品！");
            }
            KndCanteenFood food = kndCanteenFoodService.selectKndCanteenFoodById(kndCanteenFoodProduct.getFoodId());
            if (ObjectUtil.isEmpty(food)) {
                return ResponseUtil.fail("未查询到菜品信息！");
            }

            BigDecimal price = kndCanteenFoodProduct.getPrice();
            Integer number = kndCanteenFoodProduct.getNumber();

            // 菜品是否有打折
            if (kndCanteenFoodProduct.getDiscount() != null) {
                // 优惠后金额
                BigDecimal couponPrice = price.multiply(kndCanteenFoodProduct.getDiscount())
                        .divide(new BigDecimal(10), 2, BigDecimal.ROUND_HALF_UP);
                price = couponPrice;
                totalCouponPrice = totalCouponPrice.add(foodProduct.getPrice()).subtract(couponPrice).multiply(new BigDecimal(number));
            }

            List<KndCanteenOrderFood> addFoods = kndCanteenFoodProduct.getAddFoods();
            for (KndCanteenOrderFood addFood : addFoods) {
                payAmount = payAmount.add(addFood.getPrice());
            }

            payAmount = payAmount.add(price.multiply(new BigDecimal(number)));
        }

        KndCanteenOrder kndCanteenOrder = new KndCanteenOrder();
        kndCanteenOrder.setFoodPrice(payAmount);

        KndCanteenTable table = null;
        if (!StrUtil.isEmpty(appCanteenCartsForm.getTableId())) {
            // 茶位费
            BigDecimal tableFee = new BigDecimal(0);
            table = kndCanteenTableService.selectKndCanteenTableById(appCanteenCartsForm.getTableId());
            if (table.getTableFee() != null) {
                tableFee = table.getTableFee();
            }
            payAmount = payAmount.add(tableFee);
            // 开台必点费
            BigDecimal baseCostPrice = new BigDecimal(0);

            KndCanteenBaseCost baseCost = new KndCanteenBaseCost();
            baseCost.setMerchantId(userMerchant.getMerchantId());
            baseCost.setChecked(1);
            List<KndCanteenBaseCost> costs = kndCanteenBaseCostService.selectKndCanteenBaseCostList(baseCost);
            for (KndCanteenBaseCost cost : costs) {
                if (cost.getPrice() != null) {
                    // unit: 0(人/份)
                    if (cost.getUnit() == 0) {
                        baseCostPrice = baseCostPrice.add(cost.getPrice().multiply(new BigDecimal(appCanteenCartsForm.getPeopleNumber())));
                    }
                }
            }
            payAmount = payAmount.add(baseCostPrice);

            kndCanteenOrder.setBaseCostPrice(baseCostPrice);
            kndCanteenOrder.setFreightPrice(tableFee);
        }

        kndCanteenOrder.setMerchantId(userMerchant.getMerchantId());
        kndCanteenOrder.setUserOpenId(userId);
        kndCanteenOrder.setTableId(appCanteenCartsForm.getTableId());
        kndCanteenOrder.setMessage(appCanteenCartsForm.getMessage());
        kndCanteenOrder.setPeopleNumber(appCanteenCartsForm.getPeopleNumber());
        kndCanteenOrder.setIsShip(appCanteenCartsForm.getIsShip());
        kndCanteenOrder.setCouponPrice(totalCouponPrice);
        kndCanteenOrder.setOrderPrice(payAmount.add(totalCouponPrice));
        kndCanteenOrder.setActualPrice(payAmount);

        if (isUpdateOrder) {
            kndCanteenOrder.setId(appCanteenCartsForm.getOrderId());
            kndCanteenOrderService.updateKndCanteenOrder(kndCanteenOrder);
        } else {
            kndCanteenOrder.setId(GraphaiIdGenerator.nextId("kndCanteenOrder"));
            kndCanteenOrder.setOrderSn(kndCanteenOrderService.selectUniqueNumber());
            kndCanteenOrder.setOrderStatus(appCanteenCartsForm.getOrderStatus());
            kndCanteenOrderService.insertKndCanteenOrder(kndCanteenOrder);
        }

        List<KndCanteenOrderFood> oldOrderFoods;
        if (isUpdateOrder) {
            KndCanteenOrderFood obj = new KndCanteenOrderFood();
            obj.setOrderId(appCanteenCartsForm.getOrderId());
            oldOrderFoods = kndCanteenOrderFoodService.selectKndCanteenOrderFoodList(obj);
        } else {
            oldOrderFoods = new ArrayList<>();
        }

        for (KndCanteenFoodProduct kndCanteenFoodProduct : foodProductList) {

            KndCanteenFoodProduct foodProduct = kndCanteenFoodProductService.selectKndCanteenFoodProductById(kndCanteenFoodProduct.getId()); //菜品产品
            KndCanteenFood food = kndCanteenFoodService.selectKndCanteenFoodById(foodProduct.getFoodId()); //菜品

            BigDecimal couponPrice;
            if (kndCanteenFoodProduct.getDiscount() != null && kndCanteenFoodProduct.getType() == 0) {
                // 优惠后金额
                couponPrice = foodProduct.getPrice().multiply(kndCanteenFoodProduct.getDiscount()).divide(new BigDecimal(10), 2, BigDecimal.ROUND_HALF_UP);
            } else {
                couponPrice = kndCanteenFoodProduct.getPrice();
            }
            // 加料列表
            List<KndCanteenOrderFood> addFoods = kndCanteenFoodProduct.getAddFoods();
            boolean isAddFood = addFoods != null && !addFoods.isEmpty();

            // 修改旧菜品列表数据
            boolean isExist = false;
            if (!oldOrderFoods.isEmpty() && kndCanteenFoodProduct.getId() != null) {
                for (int i = 0; i < oldOrderFoods.size(); i++) {
                    KndCanteenOrderFood old = oldOrderFoods.get(i);
                    if (kndCanteenFoodProduct.getOrderFoodId() != null && kndCanteenFoodProduct.getOrderFoodId().equals(old.getId())) {
                        old.setPrice(couponPrice);
                        old.setOriginalPrice(foodProduct.getPrice());
                        old.setDiscount(kndCanteenFoodProduct.getDiscount());
                        old.setDiscountRemark(kndCanteenFoodProduct.getDiscountRemark());
                        old.setNumber(kndCanteenFoodProduct.getNumber());
                        old.setRemark(kndCanteenFoodProduct.getRemark());
                        kndCanteenOrderFoodService.updateKndCanteenOrderFood(old);
                        // 新增加料, 后面的代码会把之前所有的加料删除
                        if (isAddFood) {
                            String oldId = old.getId();
                            // copy菜品对象给加料
                            old.setId(null);
                            for (KndCanteenOrderFood addFood : addFoods) {
                                old.setPrice(addFood.getPrice());
                                old.setRemark(addFood.getRemark());
                                old.setAddFoodId(oldId);
                                old.setType(1);
                                old.setNumber(1);
                                kndCanteenOrderFoodService.insertKndCanteenOrderFood(old);
                            }
                        }
                        isExist = true;
                        oldOrderFoods.remove(i);
                    }
                }
            }

            if (!isExist) {
                // 添加数据
                KndCanteenOrderFood kndCanteenOrderFood = new KndCanteenOrderFood();
                kndCanteenOrderFood.setMerchantId(foodProduct.getMerchantId());
                kndCanteenOrderFood.setOrderId(kndCanteenOrder.getId());
                kndCanteenOrderFood.setFoodId(food.getId());
                kndCanteenOrderFood.setFoodSn(food.getFoodSn());
                kndCanteenOrderFood.setSpecifications(kndCanteenFoodProduct.getSpecifications());
                kndCanteenOrderFood.setProductId(foodProduct.getId());
                kndCanteenOrderFood.setPrice(couponPrice);
                kndCanteenOrderFood.setPicUrl(food.getPicUrl());
                kndCanteenOrderFood.setType(0);
                kndCanteenOrderFood.setOriginalPrice(foodProduct.getPrice());
                kndCanteenOrderFood.setDiscount(kndCanteenFoodProduct.getDiscount());
                kndCanteenOrderFood.setDiscountRemark(kndCanteenFoodProduct.getDiscountRemark());
                kndCanteenOrderFood.setNumber(kndCanteenFoodProduct.getNumber());
                kndCanteenOrderFood.setRemark(kndCanteenFoodProduct.getRemark());

                kndCanteenOrderFoodService.insertKndCanteenOrderFood(kndCanteenOrderFood);
                // 插入加料数据
                if (isAddFood) {
                    for (KndCanteenOrderFood addFood : addFoods) {
                        kndCanteenOrderFood.setNumber(1);
                        kndCanteenOrderFood.setAddFoodId(kndCanteenOrderFood.getId());
                        kndCanteenOrderFood.setType(1);
                        kndCanteenOrderFood.setPrice(addFood.getPrice());
                        kndCanteenOrderFood.setRemark(addFood.getRemark());
                        kndCanteenOrderFood.setId(null);
                        kndCanteenOrderFoodService.insertKndCanteenOrderFood(kndCanteenOrderFood);
                    }
                }
            }
        }

        if (table != null) {
            // 桌台绑定订单
            table.setOrderId(kndCanteenOrder.getId());
            table.setPeopleNum(appCanteenCartsForm.getPeopleNumber());
            kndCanteenTableService.updateKndCanteenTable(table);
        }

        // 如果库里的菜品不在前端传的列表里, 那么删除这个菜品, 以及删除所有加料数据
        if (!oldOrderFoods.isEmpty()) {
            StringBuilder ids = new StringBuilder();
            for (KndCanteenOrderFood orderFood : oldOrderFoods) {
                ids.append(orderFood.getId()).append(",");
            }

            kndCanteenOrderFoodService.deleteKndCanteenOrderFoodByIds(ids.toString().substring(0, ids.toString().length() - 1));
        }

        //餐饮打印 异步进行 调用即结束
/*
        if (appCanteenCartsForm.getOrderStatus() == 4) {
            doPayServiceUtil.printCanteenOrderByTenantId((String) kndMerchants.getParams().get("cashierId"), storeId, kndCanteenOrder.getId(), 1, kndMerchants.getTenantId(), 1, null);
        }
*/

        Map<String, Object> map = new HashMap<>();
        map.put("orderId", kndCanteenOrder.getId());
        map.put("orderPrice", kndCanteenOrder.getActualPrice());

        return ResponseUtil.ok("操作成功", map);
    }


    @Transactional
    @PostMapping("/returnFood")
    public Object returnFood(@RequestBody KndCanteenOrderFood orderFood) {

        if (orderFood.getNumber() == null || orderFood.getId() == null) {
            return ResponseUtil.fail("参数错误");
        }

        KndCanteenOrderFood existFood = kndCanteenOrderFoodService.selectKndCanteenOrderFoodById(orderFood.getId());
        if (existFood == null) {
            return ResponseUtil.fail("订单不存在此菜品");
        }

        Integer refundNum = orderFood.getNumber();
        if (refundNum > existFood.getRefundableNumber()) {
            return ResponseUtil.fail("超出最大可退数量");
        }
        existFood.setRefundableNumber(existFood.getRefundableNumber() - orderFood.getNumber());
        existFood.setNumber(existFood.getNumber() - refundNum);

        Integer returnedNum = existFood.getReturnedFoodNum();
        BigDecimal returnedMoney = existFood.getReturnedMoney();
        if (returnedNum == null) returnedNum = 0;
        if (returnedMoney == null) returnedMoney = new BigDecimal(0);

        BigDecimal returnPrice = existFood.getPrice().multiply(new BigDecimal(orderFood.getNumber()));
        BigDecimal returnOriPrice = existFood.getOriginalPrice().multiply(new BigDecimal(orderFood.getNumber()));

        existFood.setReturnedFoodNum(returnedNum + orderFood.getNumber());
        existFood.setReturnedMoney(returnedMoney.add(returnPrice));
        existFood.setReturnRemark(orderFood.getReturnRemark());

        // 修改订单价格
        KndCanteenOrder order = kndCanteenOrderService.selectKndCanteenOrderById(existFood.getOrderId());

        // 菜品金额: 原菜品金额 - 菜品折后价 * 退菜数量
        order.setFoodPrice(order.getFoodPrice().subtract(returnPrice));
        // 优惠金额: 原优惠金额 - (菜品原价 - 菜品折后价) * 退菜数量
        order.setCouponPrice(order.getCouponPrice().subtract(existFood.getOriginalPrice().subtract(existFood.getPrice().multiply(new BigDecimal(existFood.getNumber())))));
        // 订单原价: 原订单价 - 菜品原价 * 菜品数量
        order.setOrderPrice(order.getOrderPrice().subtract(returnOriPrice));
        // 实付金额: 原实付 - 菜品折后价 * 退菜数量
        order.setActualPrice(order.getActualPrice().subtract(returnPrice));

        kndCanteenOrderService.updateKndCanteenOrder(order);

        // 如果菜全退了, 把加料数据同步删除
        if (existFood.getNumber() == 0) {
            KndCanteenOrderFood food = new KndCanteenOrderFood();
            food.setAddFoodId(existFood.getId());
            List<KndCanteenOrderFood> addFoods = kndCanteenOrderFoodService.selectKndCanteenOrderFoodList(food);
            if (addFoods != null && !addFoods.isEmpty()) {
                StringBuilder ids = new StringBuilder();
                for (KndCanteenOrderFood addFood : addFoods) {
                    ids.append(addFood.getId()).append(",");
                }
                kndCanteenOrderFoodService.deleteKndCanteenOrderFoodByIds(ids.toString());
            }
        }

        int i = kndCanteenOrderFoodService.updateKndCanteenOrderFood(existFood);
        return i > 0 ? ResponseUtil.ok() : ResponseUtil.fail();
    }

    /**
     * 下单（添加 knd_canteen_order、knd_canteen_order_food 表数据），同时删除购物车里面的数据(删除knd_canteen_cart表数据)
     */
    @PostMapping("/submitCanteenOrder")
    public Object submitCanteenOrder(@LoginUser String userId, @RequestBody @Validated AppCanteenCartFoodForm appCanteenCartsForm) {
        if (null == appCanteenCartsForm.getOrderStatus()) {
            appCanteenCartsForm.setOrderStatus(Constants.NO);
        } else if (4 != appCanteenCartsForm.getOrderStatus() && Constants.NO != appCanteenCartsForm.getOrderStatus()) {
            return ResponseUtil.fail("下单失败，请选择下单还是挂单");
        }
        //通过userId获取商户信息
        MallUserMerchant userMerchant = userMerchantService.getOne(new QueryWrapper<MallUserMerchant>()
                .lambda().eq(MallUserMerchant::getUserId, userId));
        if (Objects.isNull(userMerchant)) {
            return ResponseUtil.badArgumentValue("商户信息不存在");
        }
        //计算菜品总费用
        List<KndCanteenFoodProduct> foodProductList = appCanteenCartsForm.getFoodProductList();
        if (foodProductList.size() == 0) {
            return ResponseUtil.fail("请选择菜品！");
        }
        BigDecimal totalMoney = new BigDecimal(0.00);
        for (int i = 0; i < foodProductList.size(); i++) {
            KndCanteenFoodProduct kndCanteenFoodProduct = foodProductList.get(i);

            KndCanteenFoodProduct foodProduct = kndCanteenFoodProductService.selectKndCanteenFoodProductById(kndCanteenFoodProduct.getId());    //菜品产品
            if (ObjectUtil.isEmpty(foodProduct)) {
                return ResponseUtil.fail("存在下架菜品，请先清空下架菜品！");
            }
            KndCanteenFood food = kndCanteenFoodService.selectKndCanteenFoodById(foodProduct.getFoodId());  //菜品
            //判断菜品产品数量 是否大于 菜品剩余数量
            if (kndCanteenFoodProduct.getNumber() == null || kndCanteenFoodProduct.getNumber() > food.getSurplusNumber()) {
                return ResponseUtil.fail("当日库存不足！");
            }
            totalMoney = totalMoney.add(foodProduct.getPrice().multiply(new BigDecimal(kndCanteenFoodProduct.getNumber())));
        }

        KndCanteenOrder kndCanteenOrder = new KndCanteenOrder();
        kndCanteenOrder.setId(GraphaiIdGenerator.nextId("kndCanteenOrder"));
        kndCanteenOrder.setMerchantId(userMerchant.getMerchantId());
        kndCanteenOrder.setUserOpenId(userId.toString());
        kndCanteenOrder.setTableId(appCanteenCartsForm.getTableId());
        //通过桌台id查询 店铺id
       /* KndCanteenTable kndCanteenTable = kndCanteenTableService.selectKndCanteenTableById(appCanteenCartsForm.getTableId());
        if (ObjectUtil.isEmpty(kndCanteenTable)) {
            return error("未查询到桌台信息！");
        }
        kndCanteenOrder.setStoreId(kndCanteenTable.getStoreId());*/
        //生成8位 订单编码orderSn
        String orderSn = kndCanteenOrderService.selectUniqueNumber();
        kndCanteenOrder.setOrderSn(orderSn);
        kndCanteenOrder.setOrderStatus(appCanteenCartsForm.getOrderStatus());  //订单状态为 挂单
        kndCanteenOrder.setMessage(appCanteenCartsForm.getMessage());
        kndCanteenOrder.setPeopleNumber(appCanteenCartsForm.getPeopleNumber());
        kndCanteenOrder.setIsShip(appCanteenCartsForm.getIsShip());
        //todo 金额方面 暂时不计算 配送费用，所以未支付订单目前只存在 菜品总费用
        kndCanteenOrder.setFoodPrice(totalMoney);
        kndCanteenOrder.setFreightPrice(new BigDecimal(0.00));
//        kndCanteenOrder.setCouponPrice(new BigDecimal(0.00));
        kndCanteenOrder.setIntegralPrice(new BigDecimal(0.00));
        kndCanteenOrder.setOrderPrice(totalMoney);
//        kndCanteenOrder.setActualPrice(totalMoney);

        //创建未支付订单
        kndCanteenOrderService.insertKndCanteenOrder(kndCanteenOrder);
        //添加 knd_canteen_order_food 表数据
        for (int i = 0; i < foodProductList.size(); i++) {
            KndCanteenFoodProduct kndCanteenFoodProduct = foodProductList.get(i);

            KndCanteenFoodProduct foodProduct = kndCanteenFoodProductService.selectKndCanteenFoodProductById(kndCanteenFoodProduct.getId());    //菜品产品
            if (ObjectUtil.isEmpty(foodProduct)) {
                return ResponseUtil.fail("未查询到上架的菜品产品信息！");
            }
            KndCanteenFood food = kndCanteenFoodService.selectKndCanteenFoodById(foodProduct.getFoodId());  //菜品
            if (ObjectUtil.isEmpty(food)) {
                return ResponseUtil.fail("未查询到菜品信息！");
            }
            //判断菜品产品数量 是否大于 菜品剩余数量
            if (kndCanteenFoodProduct.getNumber() != null && kndCanteenFoodProduct.getNumber() > food.getSurplusNumber()) {
                return ResponseUtil.fail("请选择正确的数量！");
            }
            //添加数据
            KndCanteenOrderFood kndCanteenOrderFood = new KndCanteenOrderFood();
            kndCanteenOrderFood.setMerchantId(foodProduct.getMerchantId());
            kndCanteenOrderFood.setOrderId(kndCanteenOrder.getId());
            kndCanteenOrderFood.setFoodId(foodProduct.getFoodId());
            kndCanteenOrderFood.setFoodSn(food.getFoodSn());
            kndCanteenOrderFood.setProductId(foodProduct.getId());
            kndCanteenOrderFood.setNumber(kndCanteenFoodProduct.getNumber());
            kndCanteenOrderFood.setPrice(foodProduct.getPrice());
            kndCanteenOrderFood.setSpecifications(foodProduct.getSpecifications());
            kndCanteenOrderFood.setPicUrl(food.getPicUrl());
            kndCanteenOrderFoodService.insertKndCanteenOrderFood(kndCanteenOrderFood);

            //todo 创建未支付订单之后，菜品数量减少
            KndCanteenFood foodUpdate = new KndCanteenFood();
            foodUpdate.setId(food.getId());
            foodUpdate.setSurplusNumber(food.getSurplusNumber() - kndCanteenFoodProduct.getNumber());   //菜品剩余数量减少
            kndCanteenFoodService.updateKndCanteenFood(foodUpdate);


            //todo 删除购物车里面的商品信息
            KndCanteenCart kndCanteenCart = new KndCanteenCart();
            kndCanteenCart.setMerchantId(userMerchant.getMerchantId());
            kndCanteenCart.setUserOpenId(userId.toString());
            kndCanteenCart.setProductId(foodProduct.getId());
            List<KndCanteenCart> kndCanteenCartList = kndCanteenCartService.selectKndCanteenCartList(kndCanteenCart);
            if (kndCanteenCartList.size() > 0) {
                KndCanteenCart cart = kndCanteenCartList.get(0);
                kndCanteenCartService.deleteKndCanteenCartById(cart.getId());
            }
        }
        //提交订单之后，返回前端 订单id，后续操作可支付订单、取消
        Map map = new HashMap();
        map.put("orderId", kndCanteenOrder.getId());
        map.put("orderPrice", kndCanteenOrder.getOrderPrice());
/*        if (4 == appCanteenCartsForm.getOrderStatus()) {
            //餐饮打印 异步进行 调用即结束
            doPayServiceUtil.printCanteenOrderByTenantId((String) kndMerchants.getParams().get("cashierId"), storeId, kndCanteenOrder.getId(), 1, kndMerchants.getTenantId());
        }*/
        return ResponseUtil.ok("操作成功！", map);
    }

    /**
     * (2)修改订单状态: 修改订单状态          ——订单状态（0:未支付 1:已支付 2:已取消 3:已完成 4:挂单）
     */
    @PostMapping("/cancelCanteenOrder")
    public Object cancelCanteenOrder(@LoginUser String userId, @RequestParam("orderId") String orderId,
                                     @RequestParam(value = "orderStatus", defaultValue = "2") Integer orderStatus) {
        if (StrUtil.isEmpty(orderId)) {
            return ResponseUtil.fail("未获取到订单号！");
        }

        if (2 != orderStatus && 4 != orderStatus) {
            return ResponseUtil.fail("修改失败，订单状态参数异常");
        }

        //判断是否存在订单
        KndCanteenOrder order = kndCanteenOrderService.selectKndCanteenOrderById(orderId);
        //通过userId获取商户信息
        MallUserMerchant userMerchant = userMerchantService.getOne(new QueryWrapper<MallUserMerchant>()
                .lambda().eq(MallUserMerchant::getUserId, userId));
        if (Objects.isNull(userMerchant)) {
            return ResponseUtil.badArgumentValue("商户信息不存在");
        }
        order.setMerchantId(userMerchant.getMerchantId());
        if (ObjectUtil.isEmpty(order)) {
            return ResponseUtil.fail("未查询到订单信息！");
        }
       /* if (order.getOrderStatus() != 0 && 4 != order.getOrderStatus()) {
            return error("订单状态错误！");
        }*/
        KndCanteenOrder kndCanteenOrder = new KndCanteenOrder();
        kndCanteenOrder.setId(orderId);
        kndCanteenOrder.setOrderStatus(orderStatus);
/*        if (2 == orderStatus && "MTPS".equalsIgnoreCase(order.getDeliveryId())) {
            meituanPeisongopenUtil.cancelOrder(order);
        }*/
        kndCanteenOrderService.updateKndCanteenOrder(kndCanteenOrder);
/*        if (4 == orderStatus) {
            //餐饮打印 异步进行 调用即结束
            doPayServiceUtil.printCanteenOrderByTenantId((String) kndMerchants.getParams().get("cashierId"), storeId, kndCanteenOrder.getId(), 1, kndMerchants.getTenantId(), 1, null);
        }*/
        return ResponseUtil.ok();
    }

    /**
     * 通过订单id查询 订单食品信息
     */
    @PostMapping("/selectCanteenOrderById")
    public Object selectCanteenOrderById(@RequestParam("orderId") String orderId) {

        //订单信息
        KndCanteenOrder kndCanteenOrder = kndCanteenOrderService.selectKndCanteenOrderById(orderId);
        if (kndCanteenOrder == null) {
            return ResponseUtil.fail("订单不存在");
        }
        //订单食品list信息
        KndCanteenOrderFood kndCanteenOrderFood = new KndCanteenOrderFood();
        kndCanteenOrderFood.setOrderId(orderId);
        List<KndCanteenOrderFood> kndCanteenOrderFoodList = kndCanteenOrderFoodService.selectKndCanteenOrderFoodListInfo(kndCanteenOrderFood);

        Map map = new HashMap();
        String storeId = kndCanteenOrder.getStoreId();
        Map<String, Object> reqmap = new HashMap<>();
/*        if (kndCanteenOrder.getIsShip() == 2 && kndCanteenOrder.getOrderStatus() != 0 &&
                null != kndCanteenOrder.getPeisongStatus() && ("TEST".equalsIgnoreCase(kndCanteenOrder.getDeliveryId())
                || "MTPS".equalsIgnoreCase(kndCanteenOrder.getDeliveryId()))) {
            String longitude = kndCanteenOrder.getLongitude();
            String latitude = kndCanteenOrder.getLatitude();

            //获取骑手信息
            cn.hutool.json.JSONObject order = meituanPeisongopenUtil.getOrder(kndCanteenOrder);
            String rider_lng = order.getStr("rider_lng");//骑手位置经度, 配送中时返回
            String rider_lat = order.getStr("rider_lat");//骑手位置纬度, 配送中时返回
            if (StrUtil.isNotEmpty(rider_lat) && StrUtil.isNotEmpty(rider_lat) && StrUtil.isNotEmpty(longitude) && StrUtil.isNotEmpty(latitude)) {
                map.put("distance", LatLngUtil.getDistance(longitude + "," + latitude, rider_lng + "," + rider_lat));//配送距离KM
            } else {
                map.put("distance", 0);//配送距离KM
            }
            map.put("mtpsInfo", order);
        }
        map.put("kndCanteenOrder", kndCanteenOrder);
        map.put("kndCanteenOrderFoodList", kndCanteenOrderFoodList);
        if (null != kndCanteenOrderFoodList) {
            for (KndCanteenOrderFood canteenOrderFood : kndCanteenOrderFoodList) {
                String[] specifications = canteenOrderFood.getSpecifications();
                String specification = "";
                if (!StringUtils.isEmpty(specifications)) {
                    for (String s : specifications) {
                        specification += "," + s;
                    }
                    canteenOrderFood.setSpecificationsStr(specification.substring(1));
                }
            }
        }*/
        if (0 == kndCanteenOrder.getOrderStatus()) {
            map.put("packPrice", 0);
            map.put("peisongTime", DateUtil.offsetMinute(new Date(), 60));
        }
        map.put("peisongPrice", kndCanteenOrder.getPeisongPrice());
            return ResponseUtil.ok("操作成功！", map);
        }

        /**
         * 通过桌台id查询 茶位费
         */
        @PostMapping("/selectCanteenTableFeeByTableId")
        public Object selectCanteenTableFeeByTableId (@LoginUser String userId, @RequestParam("tableId") String tableId)
        {
            if (StrUtil.isEmpty(tableId)) {
                return ResponseUtil.fail("没有查询到桌台号信息");
            }
            //桌台信息
            KndCanteenTable kndCanteenTable = kndCanteenTableService.selectKndCanteenTableById(tableId);
            return ResponseUtil.ok("查询成功！", kndCanteenTable);
        }

        /**
         * 统计挂单数量，已支付数量
         */
/*        @PostMapping("/selectStatisticsByOrder")
        public Object selectStatisticsByOrder (@LoginUser String userId){
            //通过userId获取商户信息
            MallUserMerchant userMerchant = userMerchantService.getOne(new QueryWrapper<MallUserMerchant>()
                    .lambda().eq(MallUserMerchant::getUserId, userId));
            if (Objects.isNull(userMerchant)) {
                return ResponseUtil.badArgumentValue("商户信息不存在");
            }
            String storeId = (String) kndMerchants.getParams().get("storeId");
            Map<String, String> kndCanteenOrderStatistics = kndCanteenOrderService.selectStatisticsByOrder(storeId);
            return ResponseUtil.ok("查询成功", kndCanteenOrderStatistics);
        }*/

        /**
         * 修改选择堂食或外带
         */
        @PostMapping("/updateIsShip")
        public Object updateIsShip (@LoginUser String userId,@RequestParam("orderId") String orderId, @RequestParam("isShip") Integer isShip){
            //通过userId获取商户信息
            KndCanteenOrder kndCanteenOrder = kndCanteenOrderService.selectKndCanteenOrderById(orderId);
            if (kndCanteenOrder == null) {
                return ResponseUtil.fail("请输入正确的订单号");
            }
            KndCanteenOrder canteenOrder = new KndCanteenOrder();
            canteenOrder.setIsShip(isShip);
            canteenOrder.setId(orderId);
            kndCanteenOrderService.updateKndCanteenOrder(canteenOrder);
            return ResponseUtil.ok("操作成功", orderId);
        }

        @PostMapping("/delivery")
        public Object delivery (@RequestParam("orderId") String
        orderId, @RequestParam(value = "deliveryId", defaultValue = "SJPS") String deliveryId){
            /*if (deliveryId.equalsIgnoreCase("MTPS")) {
                return meituanPeisongopenUtil.addOrder(orderId);
            }*/
            KndCanteenOrder kndCanteenOrder = kndCanteenOrderService.selectKndCanteenOrderById(orderId);
            if (kndCanteenOrder == null) return ResponseUtil.fail("订单不存在");

            if (kndCanteenOrder.getOrderStatus() != 1 || kndCanteenOrder.getIsShip() != 2) {
                return ResponseUtil.fail();
            }
            kndCanteenOrder.setDeliveryId(deliveryId);
            kndCanteenOrder.setPeisongStatus("101");
            int i = kndCanteenOrderService.updateKndCanteenOrder(kndCanteenOrder);

            return i > 0 ? ResponseUtil.ok() : ResponseUtil.fail();
        }
        @PostMapping("/getDeliveryCount")
        public Object getDeliveryCount (@LoginUser String userId){
            //通过userId获取商户信息
            MallUserMerchant userMerchant = userMerchantService.getOne(new QueryWrapper<MallUserMerchant>()
                    .lambda().eq(MallUserMerchant::getUserId, userId));
            if (Objects.isNull(userMerchant)) {
                return ResponseUtil.badArgumentValue("商户信息不存在");
            }
            KndCanteenOrder kndCanteenOrder = new KndCanteenOrder();
            kndCanteenOrder.setMerchantId(userMerchant.getMerchantId());
            kndCanteenOrder.setOrderStatus(1);
            kndCanteenOrder.setPeisongStatus("0");
            kndCanteenOrder.setIsShip(2);
            return ResponseUtil.ok("获取成功", kndCanteenOrderService.getDeliveryCount(kndCanteenOrder));
        }

        @GetMapping("/getBaseCosts")
        public Object getBaseCosts (@LoginUser String userId){
            MallUserMerchant userMerchant = userMerchantService.getOne(new QueryWrapper<MallUserMerchant>()
                    .lambda().eq(MallUserMerchant::getUserId, userId));
            if (Objects.isNull(userMerchant)) {
                return ResponseUtil.badArgumentValue("商户信息不存在");
            }
            KndCanteenBaseCost cost = new KndCanteenBaseCost();
            cost.setMerchantId(userMerchant.getMerchantId());

            return ResponseUtil.ok(kndCanteenBaseCostService.selectKndCanteenBaseCostVoList(cost));
        }

        @PostMapping("/updateBaseCost")
        public Object updateBaseCost (@LoginUser String userId,@RequestBody List < KndCanteenBaseCost > baseCosts){
            MallUserMerchant userMerchant = userMerchantService.getOne(new QueryWrapper<MallUserMerchant>()
                    .lambda().eq(MallUserMerchant::getUserId, userId));
            if (Objects.isNull(userMerchant)) {
                return ResponseUtil.badArgumentValue("商户信息不存在");
            }
            int i = kndCanteenBaseCostService.updateKndCanteenBaseCostVo(baseCosts,userMerchant.getMerchantId());
            return i > 0 ? ResponseUtil.ok() : ResponseUtil.fail();
        }

        @GetMapping("/getLabel")
        public Object getLabel (@LoginUser String userId,@RequestParam Integer type, String foodId){
            KndCanteenLabel label = new KndCanteenLabel();
            label.setType(type);
            label.setFoodId(foodId);
            return ResponseUtil.ok(kndCanteenLabelService.selectKndCanteenLabelList(label));
        }

        @PostMapping("/addLabel")
        public Object addLabel (@LoginUser String userId,@RequestBody KndCanteenLabel label){
            MallUserMerchant userMerchant = userMerchantService.getOne(new QueryWrapper<MallUserMerchant>()
                    .lambda().eq(MallUserMerchant::getUserId, userId));
            if (Objects.isNull(userMerchant)) {
                return ResponseUtil.badArgumentValue("商户信息不存在");
            }
            label.setMerchantId(userMerchant.getMerchantId());
            label.setCreateTime(LocalDateTime.now());

            int i = kndCanteenLabelService.insertKndCanteenLabel(label);
            return i > 0 ? ResponseUtil.ok() : ResponseUtil.fail();
        }

        @PostMapping("/delLabel")
        public Object delLabel (@RequestParam String id){
            int i = kndCanteenLabelService.deleteKndCanteenLabelById(id);
            return i > 0 ? ResponseUtil.ok() : ResponseUtil.fail();
        }

        @GetMapping("/confirmOrder")
        public Object confirmOrder (@RequestParam String orderId){
            KndCanteenOrder order = kndCanteenOrderService.selectKndCanteenOrderById(orderId);
            if (StrUtil.isEmpty(order.getPeisongStatus())) {
                order.setPeisongStatus("0");
            }
            if (ObjectUtil.isNull(order))
                return ResponseUtil.fail("订单不存在");

            if (order.getOrderStatus() != 1 || order.getIsShip() != 2 || !"0".equals(order.getPeisongStatus())) {
                return ResponseUtil.fail();
            }
            order.setPeisongStatus("101");
            int i = kndCanteenOrderService.updateKndCanteenOrder(order);

            return i > 0 ? ResponseUtil.ok() : ResponseUtil.fail();
        }

    }
