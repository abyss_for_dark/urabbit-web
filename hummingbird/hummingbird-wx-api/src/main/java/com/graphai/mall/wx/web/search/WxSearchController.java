package com.graphai.mall.wx.web.search;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotEmpty;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.mall.db.domain.MallKeyword;
import com.graphai.mall.db.domain.MallSearchHistory;
import com.graphai.mall.db.dto.SearchEngineKeywordDTO;
import com.graphai.mall.db.service.search.MallKeywordService;
import com.graphai.mall.db.service.search.MallSearchHistoryService;
import com.graphai.mall.wx.annotation.LoginUser;

/**
 * 商品搜索服务
 * <p>
 * 注意：目前搜索功能非常简单，只是基于关键字匹配。
 */
@RestController
@RequestMapping("/wx/search")
@Validated
public class WxSearchController {
    private final Log logger = LogFactory.getLog(WxSearchController.class);

    @Autowired
    private MallKeywordService keywordsService;
    @Autowired
    private MallSearchHistoryService searchHistoryService;

    /**
     * 搜索页面信息
     * <p>
     * 如果用户已登录，则给出用户历史搜索记录；
     * 如果没有登录，则给出空历史搜索记录。
     *
     * @param userId 用户ID，可选
     * @return 搜索页面信息
     */
    @GetMapping("index")
    public Object index(@LoginUser String userId) {
        //取出输入框默认的关键词
        MallKeyword defaultKeyword = keywordsService.queryDefault();
        //取出热闹关键词
        //List<MallKeyword> hotKeywordList = keywordsService.queryHots();

        List<MallSearchHistory> historyList = null;
        if (userId != null) {
            //取出用户历史关键字
            historyList = searchHistoryService.queryByUid(userId);
            if (CollectionUtils.isNotEmpty(historyList)) {
            	if (historyList.size()>20) {
            		historyList=historyList.subList(0, 20);
				}
			}
        } else {
            return ResponseUtil.unlogin();
            // historyList = new ArrayList<>(0);
        }

        Map<String, Object> data = new HashMap<String, Object>();
        data.put("defaultKeyword", defaultKeyword);
        data.put("historyKeywordList", historyList);
        //data.put("hotKeywordList", hotKeywordList);
        return ResponseUtil.ok(data);
    }

    /**
     * 关键字提醒
     * <p>
     * 当用户输入关键字一部分时，可以推荐系统中合适的关键字。
     *
     * @param keyword 关键字
     * @return 合适的关键字
     */
    @GetMapping("helper")
    public Object helper(@NotEmpty String keyword,
                         @RequestParam(defaultValue = "1") Integer page,
                         @RequestParam(defaultValue = "10") Integer limit) {
        List<MallKeyword> keywordsList = keywordsService.queryByKeyword(keyword, page, limit);
        String[] keys = new String[keywordsList.size()];
        int index = 0;
        for (MallKeyword key : keywordsList) {
            keys[index++] = key.getKeyword();
        }
        return ResponseUtil.ok(keys);
    }

    /**
     * 商品查询搜索引擎
     * @param searchDTO
     * @return
     */
    @PostMapping("goods/engine")
    public Object goodsSearchEngine(@RequestBody SearchEngineKeywordDTO searchDTO) {

        return ResponseUtil.ok();
    }


    /**
     * 清除用户搜索历史
     *
     * @param userId 用户ID
     * @return 清理是否成功
     */
    @PostMapping("clearhistory")
    public Object clearhistory(@LoginUser String userId) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        searchHistoryService.deleteByUid(userId);
        return ResponseUtil.ok();
    }
}
