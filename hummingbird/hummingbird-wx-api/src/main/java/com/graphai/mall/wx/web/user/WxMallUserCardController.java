package com.graphai.mall.wx.web.user;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.graphai.commons.constant.MallConstant;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.json.JacksonUtils;
import com.graphai.mall.db.domain.*;
import com.graphai.mall.db.service.MallInvitationCodeService;
import com.graphai.mall.db.service.order.MallOrderService;
import com.graphai.mall.db.service.user.behavior.MallUserCardService;
import com.graphai.open.api.getway.HttpRequestUtils;
import com.graphai.properties.PlatFormServerproperty;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Header;
import org.apache.http.message.BasicHeader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 用户卡号
 */
@RestController
@RequestMapping("/wx/mallUserCard")
@Validated
public class WxMallUserCardController {
    private final Log logger = LogFactory.getLog(WxMallUserCardController.class);

    @Autowired
    private MallUserCardService mallUserCardService;
    @Autowired
    private MallInvitationCodeService mallInvitationCodeService;
    @Autowired
    private MallOrderService mallOrderService;
    @Autowired
    private PlatFormServerproperty platFormServerproperty;

    /**
     * 通过订单id 查询 电子卡信息、激活码信息
     */
    @GetMapping("/getCard")
    public Object getCard(String orderId) {
        //根据订单id查询订单
        MallOrder mallOrder = mallOrderService.findById(orderId);
        if(mallOrder == null){
            return ResponseUtil.badArgumentValue("订单不存在！");
        }

        //根据订单id查询激活码
        MallInvitationCode mallInvitationCode = new MallInvitationCode();
        mallInvitationCode.setOrderId(orderId);
        List<MallInvitationCode> mallInvitationCodeList = mallInvitationCodeService.selectMallInvitationCodeList(mallInvitationCode, null, null, null, null);
        MallInvitationCode code = null;
        if(mallInvitationCodeList.size() == 1){
            code = mallInvitationCodeList.get(0);
        }
        if(code == null){
            return ResponseUtil.badArgumentValue("激活码不存在！");
        }

        //根据用户id查询卡号
        String userId = code.getUserId();   //拥有者
        MallUserCard mallUserCard = new MallUserCard();
        mallUserCard.setUserId(userId);
        List<MallUserCard> mallUserCardList = mallUserCardService.selectMallUserCardList(mallUserCard, null, null, null, null);
        if(mallUserCardList.size() == 1){
            mallUserCard = mallUserCardList.get(0);
        }
        Map<String,String> map = new HashMap<>();

        map.put("cardCode",mallUserCard.getCardCode());  //卡号
        map.put("name",mallOrder.getConsignee());  //购卡人名字
        map.put("activationCode",code.getCode());  //激活码
        map.put("mobile",mallOrder.getMobile());  //手机号
        return ResponseUtil.ok(map);
    }




    /**
     * 给指定用户生成实体卡
     */
    @PostMapping("/createCard")
    public Object createCard(String userId) {
        MallUserCard userCard = new MallUserCard();
        userCard.setUserId(userId);
        userCard.setCardType((byte)0);    //电子卡
        mallUserCardService.insertMallUserCard(userCard);
        return ResponseUtil.ok();
    }

    /**
     * 批量生成激活码
     */
    @PostMapping("/createCode")
    public Object createCode(Integer num) {
        for (int i = 0; i < num; i++) {
            MallInvitationCode mallInvitationCode = new MallInvitationCode();
            mallInvitationCode.setIsPrint(true);
            mallInvitationCodeService.insertMallInvitationCode(mallInvitationCode);
        }
        return ResponseUtil.ok();
    }

//    @PostMapping("/createCode2")
//    public Object createCode2(String code) {
//        MallInvitationCode mallInvitationCode = mallInvitationCodeService.selectMallInvitationCodeByCode(code);
//        return ResponseUtil.ok(mallInvitationCode);
//    }


    @PostMapping("/getGoodsList")
    public Object getGoodsList(String goodId){
        Object goodsProductData = null;
        try {
            String url = platFormServerproperty.getUrl(MallConstant.TAOBAO);
            Map<String,Object> map = new HashMap<>();
            map.put("plateform", "dtk");
            map.put("goodsId", goodId);
            Header[] headers = new BasicHeader[]{new BasicHeader("method", "getGoodsProductByGoodsId"), new BasicHeader("version", "v1.0")};
            String result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(map), headers);
            JSONObject object = JSONObject.parseObject(result);
            if ("0".equals(object.getString("errno"))) {
                goodsProductData = object.get("data");
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.info("失败！");
        }

        return ResponseUtil.ok(goodsProductData);
    }


    @PostMapping("/getGoodsById")
    public Object getGoodsById(String id){
        Map<String, Object> responseMap2 = null;
        try {
            String url = platFormServerproperty.getUrl(MallConstant.TAOBAO);
            Map<String,Object> map = new HashMap<>();
            map.put("plateform", "dtk");
            map.put("id", id);
            Header[] headers = new BasicHeader[]{new BasicHeader("method", "getGoodsProductById"), new BasicHeader("version", "v1.0")};
            String result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(map), headers);
            JSONObject couponObject = JSONObject.parseObject(result);
            if ("0".equals(couponObject.getString("errno"))) {
                Object couponData = couponObject.get("data");
                if(ObjectUtil.isNotNull(couponData)){
                    responseMap2 = (Map<String, Object>) JSONArray.parseObject(couponData.toString(), Map.class);
                }
            }
        } catch (Exception e) {
            //            e.printStackTrace();
            logger.info("失败！");
        }
        return ResponseUtil.ok(responseMap2);
    }

}
