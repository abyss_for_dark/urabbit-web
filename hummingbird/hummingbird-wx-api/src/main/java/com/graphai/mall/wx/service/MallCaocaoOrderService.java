package com.graphai.mall.wx.service;


import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.graphai.mall.db.dao.MallOrderMapper;
import com.graphai.mall.db.domain.MallOrder;
import com.graphai.mall.db.domain.MallOrderExample;
import com.graphai.mall.db.domain.MallUser;
import com.graphai.mall.db.constant.account.AccountStatus;
import com.graphai.mall.db.service.order.MallOrderService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.constant.CommonConstant;
import com.graphai.mall.db.util.account.AccountUtils;
import com.graphai.mall.wx.util.HttpClientUtils;

@Service
public class MallCaocaoOrderService {

    //测试环境
//    private static final String CLENT_ID = "ce69d6823d8f4844";
//    private static final String SIGN_KEY = "02ea85edd959adb57d4923c7760a6781";
//    private static final String INDEX_PATH = "https://sandbox-mobile.caocaokeji.cn/pay-travel/home?";
//    private static final String ORDER_DETAIL_PATH =  "https://sandbox-cop.caocaokeji.cn/v2/common/queryOrderDetailV2?";

    //正式环境
    private static final String CLENT_ID = "4df9a9dcf2ff47a2";
    private static final String SIGN_KEY = "e1bfc7c5b7a29822200558bbada84ec4";
    private static final String INDEX_PATH = "https://mobile.caocaokeji.cn/pay-travel/home?";
    private static final String ORDER_DETAIL_PATH =  "https://cop.caocaokeji.cn/v2/common/queryOrderDetailV2?";

    //曹操订单状态
    private static final String CODE_1 = "1";//司机接单,生成订单开始行程
    private static final String[] CANCEL_ORDER_CODES = {"20","21","22","26"};//取消订单
    private static final String PAID_CODE = "11";//用户支付

    @Resource
    private MallOrderMapper mallOrderMapper;
    @Resource
    private MallUserService userService;
    @Resource
    private MallOrderService mallOrderService;



    public int orderHandler(String orderId,String eventCode) {
        HashMap<String, String> map = new HashMap<>();
        map.put("client_id",CLENT_ID);
        map.put("order_id",orderId);
        map.put("sign_key",SIGN_KEY);
        map.put("timestamp",System.currentTimeMillis()+"");
        try{
            //拉取订单
            String data = HttpClientUtils.doGet(ORDER_DETAIL_PATH + generateParams(map, null));
            JSONObject retMap = JSONObject.parseObject(data);
            if (retMap.getBoolean("success")) {
                //司机接单后,生成订单
                if (CODE_1.equalsIgnoreCase(eventCode)) {
                    return createOrder(retMap);
                }
                //取消订单
                if (isCancel(eventCode)) {
                    MallOrderExample ex = new MallOrderExample();
                    ex.createCriteria().andOrderSnEqualTo(orderId);
                    MallOrder order = new MallOrder();
                    order.setOrderStatus(Short.parseShort(eventCode));
                    order.setActualPrice(new BigDecimal(0));
                    order.setOrderStatusDesc("已取消");
                    mallOrderMapper.updateByExampleSelective(order,ex);
                }
                //行程结束,用户支付
                if (PAID_CODE.equalsIgnoreCase(eventCode)) {
                    MallOrderExample ex = new MallOrderExample();
                    ex.createCriteria().andOrderSnEqualTo(orderId);
                    MallOrder order = mallOrderMapper.selectOneByExample(ex);
                    HashMap<String, String> params = new HashMap<>();
                    params.put("userId", order.getUserId());
                    params.put("bid", AccountStatus.BID_26);
                    params.put("totalMoney", order.getActualPrice().toString());
                    params.put("rechargeType", CommonConstant.TASK_REWARD_CASH);
                    params.put("orderId", order.getId());
                    params.put("orderSn", order.getOrderSn());
                    //创建预充值
                    AccountUtils.preCharge(params);
                    //修改订单状态
                    JSONObject orderObj = retMap.getJSONObject("data");
                    JSONObject orderFee = orderObj.getJSONObject("orderFeeVo");
                    order.setOrderPrice(new BigDecimal(orderFee.getInteger("originTotalFee")).divide(new BigDecimal(100)));
                    order.setActualPrice(new BigDecimal(orderFee.getInteger("totalFee")).divide(new BigDecimal(100)));
                    order.setOrderStatus(Short.parseShort(eventCode));
                    order.setOrderStatusDesc("已完成");
                    mallOrderMapper.updateByPrimaryKeySelective(order);
                }
                return 1;
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    private int createOrder(JSONObject retMap) {
        MallOrder order = new MallOrder();

        JSONObject data = retMap.getJSONObject("data");
        JSONObject basicOrder = data.getJSONObject("basicOrderVO");
        JSONObject orderFee = data.getJSONObject("orderFeeVo");

        if (null != basicOrder && null != orderFee) {
            order.setOrderSn(basicOrder.getString("orderId"));
            order.setUserId(basicOrder.getString("accid"));
            order.setMobile(basicOrder.getString("callerPhone"));
            order.setOrderStatus(basicOrder.getShort("status"));
            order.setAddress(basicOrder.getString("startAddress"));
            order.setAddressDetail(basicOrder.getString("endAddress"));
            order.setBid(AccountStatus.BID_26);
            order.setHuid("caocao");
            order.setDeleted(false);
            order.setOrderStatusDesc("行程中");
            order.setOrderPrice(new BigDecimal(orderFee.getInteger("originTotalFee")).divide(new BigDecimal(100)));
            order.setActualPrice(new BigDecimal(orderFee.getInteger("totalFee")).divide(new BigDecimal(100)));

            return mallOrderService.add(order);
        }
        return -1;
    }

    public String getLinkUrl(String userId) throws UnsupportedEncodingException {
        MallUser user = userService.findById(userId);
        HashMap<String, String> map = new HashMap<>();
        map.put("client_id",CLENT_ID);
        map.put("ext_user_id",userId);
        map.put("sign_key",SIGN_KEY);
        map.put("timestamp",System.currentTimeMillis()+"");
        return INDEX_PATH + URLEncoder.encode(generateParams(map, user.getMobile()),"UTF-8");
    }

    private String generateParams(HashMap<String, String> map, String phone) {
        List<String> keys = new ArrayList<>(map.size());
        keys.addAll(map.keySet());
        Collections.sort(keys);
        StringBuilder signStr = new StringBuilder();
        StringBuilder params = new StringBuilder();
        for (String key : keys) {
            signStr.append(key).append(map.get(key));
            if (key.equalsIgnoreCase("sign_key")) continue;
            params.append(key).append("=").append(map.get(key)).append("&");
        }

        try {
            MessageDigest sha = MessageDigest.getInstance("SHA-1");
            sha.update(signStr.toString().getBytes());
            byte[] digest = sha.digest();
            String sign = byte2hex(digest);
            StringBuilder path = params.append("sign=").append(sign);
            if (phone != null) {
                path.append("&phone=").append(phone);
            }
            return path.toString();
        }catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String byte2hex(byte[] bytes) {
        StringBuilder sign = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(bytes[i] & 0xFF);
            if (hex.length() == 1) {
                sign.append("0");
            }
            sign.append(hex.toLowerCase());
        }
        return sign.toString();
    }

    private Boolean isCancel(String code) {
        for (String cancelCode : CANCEL_ORDER_CODES) {
            if (cancelCode.equalsIgnoreCase(code)) {
                return true;
            }
        }
        return false;
    }
}
