package com.graphai.mall.wx.dto;

import lombok.Data;

@Data
public class MiniProgramUserInfo {

	private String nickName;
	
	private Byte gender;
	
	private String language;
	
	private String city;
	
	private String province;
	
	private String country;
	
	private String avatarUrl;
	
}
