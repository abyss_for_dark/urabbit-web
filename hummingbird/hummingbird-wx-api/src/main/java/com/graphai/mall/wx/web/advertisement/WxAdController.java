package com.graphai.mall.wx.web.advertisement;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.json.JacksonUtil;
import com.graphai.mall.db.domain.MallAd;
import com.graphai.mall.db.service.advertisement.MallAdService;
import com.graphai.mall.db.util.StringHelper;


@RestController
@RequestMapping(value = "/wx/ad", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.APPLICATION_ATOM_XML_VALUE,
        MediaType.APPLICATION_FORM_URLENCODED_VALUE, MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE,
        MediaType.TEXT_HTML_VALUE, MediaType.TEXT_XML_VALUE, MediaType.APPLICATION_OCTET_STREAM_VALUE})
// html直接请求
public class WxAdController {
    @Autowired
    private MallAdService adService;

    private final Log logger = LogFactory.getLog(WxAdController.class);

    /**
     * 首页数据
     * 
     * @param userId 当用户已经登录时，非空。为登录状态为null
     * @return 首页数据
     */
    @GetMapping("/list")
    // public Object index(@LoginUser String userId,String industryId) {
    public Object index(@RequestParam(defaultValue = "1") Byte position) {
        List<MallAd> adList = adService.queryIndex(position);
        if (CollectionUtils.isNotEmpty(adList)) {
            for (int i = 0; i < adList.size(); i++) {
                if (StringHelper.isNotNullAndEmpty(adList.get(i).getChannelCode())) {
                    adList.get(i).setBusinessId(adList.get(i).getChannelCode());
                }
            }
        }
        return ResponseUtil.ok(adList);
    }


    /**
     * 未莱商城各平台广告
     * 
     * @param
     * @return 广告数据
     */
    @GetMapping("/listByPosition")
    public Object listByPosition(@RequestParam(defaultValue = "4") String industryId) {
        List<MallAd> adList = adService.queryShop(industryId);
        if (CollectionUtils.isNotEmpty(adList)) {
            for (int i = 0; i < adList.size(); i++) {
                if (StringHelper.isNotNullAndEmpty(adList.get(i).getChannelCode())) {
                    adList.get(i).setBusinessId(adList.get(i).getChannelCode());
                }
            }
        }
        return ResponseUtil.ok(adList);
    }



    /**
     * 各位置广告
     *
     * @param
     * @return 广告数据
     * @throws Exception
     */
    @GetMapping("/getListByPosition")
    public Object getListByPosition(@RequestParam(defaultValue = "6") String position) throws Exception {
        List<MallAd> adList = adService.queryByPosition(position);
        return ResponseUtil.ok(adList);
    }

    /**
     * 工厂套系广告
     *
     * @param
     * @return 广告数据
     * @throws Exception
     */
    @GetMapping("/getFactoryList")
    public Object getFactoryList(@RequestParam(defaultValue = "99") Byte position,@RequestParam() String firstPosition) throws Exception {
        List<MallAd> adList = adService.querySelective(null,firstPosition, null, position, null, 1, 10000, "sort", "asc");
        return ResponseUtil.ok(adList);
    }



    /**
     * 未莱商圈广告
     * 
     * @param position
     * @return 广告轮播
     * @throws Exception
     */
    @PostMapping("/getAdsByWeiLai")
    public Object getGoodsDetailHSCard(@RequestBody String data, HttpServletRequest request) throws Exception {

        Byte adPosition = JacksonUtil.parseByte(data, "adPosition");
        Byte backPosition = JacksonUtil.parseByte(data, "backPosition");

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("adPosition", adPosition);
        paramMap.put("backPosition", backPosition);



        try {
            // 广告
            List<MallAd> adList = adService.queryIndex(adPosition);

            // 背景
            List<MallAd> back = adService.queryIndex(backPosition);

            Map<String, Object> map = new HashMap<String, Object>();

            if (CollectionUtils.isNotEmpty(adList)) {
                map.put("adList", adList);
            }
            if (CollectionUtils.isNotEmpty(back)) {
                map.put("back", back.get(0));
            }

            return ResponseUtil.ok(map);
        } catch (Exception e) {
            logger.error("商品查询异常");
            throw e;
        }
    }

}
