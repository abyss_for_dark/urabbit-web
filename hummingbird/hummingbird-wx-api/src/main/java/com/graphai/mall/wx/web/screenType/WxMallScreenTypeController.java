package com.graphai.mall.wx.web.screenType;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.mall.admin.domain.MallScreenType;
import com.graphai.mall.admin.service.IMallScreenTypeService;
import com.graphai.mall.db.constant.screenType.MallScreenTypeEnum;
import com.graphai.mall.db.domain.MallAdmin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.apache.shiro.SecurityUtils.getSubject;

@RestController
@RequestMapping("/wx/screentype")
@Validated
public class WxMallScreenTypeController {

    @Autowired
    private IMallScreenTypeService serviceImpl;


    @GetMapping("/materialTypeList")
    public Object getMaterialTypeByCategoryId(String  CategoryId) throws Exception{
        List<MallScreenType> mallScreenTypes  = serviceImpl.selectbyCategoryId(CategoryId);
        List<MallScreenType> materialType = new ArrayList();
        List<MallScreenType> sizeType = new ArrayList();
        List<MallScreenType> craftType = new ArrayList();
        List<MallScreenType> priceType = new ArrayList();
        List<MallScreenType> placeType = new ArrayList();
        Map<String,Object> map = new HashMap();
        // 若CategoryId为空,设置默认筛选
        if (CategoryId == null || CategoryId.equals("") || CategoryId.equals("null") || mallScreenTypes == null || mallScreenTypes.size() == 0 ) {
            CategoryId = "5606235792321712128";
            mallScreenTypes  = serviceImpl.selectbyCategoryId(CategoryId);
        }
        for (MallScreenType mallScreenType : mallScreenTypes) {
            if (mallScreenType.getCategoryId().equals(CategoryId) && mallScreenType.getDisabled().equals(MallScreenTypeEnum.Disabled_0.strCode2Int())) {
                if (mallScreenType.getType().equals(MallScreenTypeEnum.TYPE_0.strCode2Int()) && mallScreenType.getDisabled().equals(MallScreenTypeEnum.Disabled_0.strCode2Int())) {
                    materialType.add(mallScreenType);
                }
                if (mallScreenType.getType().equals(MallScreenTypeEnum.TYPE_1.strCode2Int()) && mallScreenType.getDisabled().equals(MallScreenTypeEnum.Disabled_0.strCode2Int())) {
                    sizeType.add(mallScreenType);
                }
                if (mallScreenType.getType().equals(MallScreenTypeEnum.TYPE_2.strCode2Int()) && mallScreenType.getDisabled().equals(MallScreenTypeEnum.Disabled_0.strCode2Int())) {
                    craftType.add(mallScreenType);
                }
                if (mallScreenType.getType().equals(MallScreenTypeEnum.TYPE_3.strCode2Int()) && mallScreenType.getDisabled().equals(MallScreenTypeEnum.Disabled_0.strCode2Int())) {
                    priceType.add(mallScreenType);
                }
                if (mallScreenType.getType().equals(MallScreenTypeEnum.TYPE_4.strCode2Int()) && mallScreenType.getDisabled().equals(MallScreenTypeEnum.Disabled_0.strCode2Int())) {
                    placeType.add(mallScreenType);
                }
            }
        }
        map.put("materialType",materialType);
        map.put("sizeType",sizeType);
        map.put("craftType",craftType);
        map.put("priceType",priceType);
        map.put("placeType",placeType);
        return ResponseUtil.ok(map);
    }
}
