package com.graphai.mall.wx.web.tripartite;


import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.mall.core.qianzhu.kfc.boot.client.KfcExecuteClient;
import com.graphai.mall.core.qianzhu.kfc.boot.model.KfcOrderModel;
import com.graphai.mall.core.qianzhu.kfc.boot.request.GetKfcOrderByOrderNoRequest;
import com.graphai.mall.core.qianzhu.kfc.boot.request.GetUserTokenRequest;
import com.graphai.mall.core.qianzhu.kfc.boot.response.GetKfcOrderByOrderNoResponse;
import com.graphai.mall.core.qianzhu.kfc.boot.response.GetUserTokenResponse;
import com.graphai.mall.core.qianzhu.kfc.boot.util.SignUtils;
import com.graphai.mall.db.dao.MallOrderMapper;
import com.graphai.mall.db.domain.MallUser;
import com.graphai.mall.db.domain.qianzhu.model.OrderEventModel;
import com.graphai.mall.db.domain.qianzhu.model.UserModel;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.mall.wx.util.PushUtil;
import com.graphai.properties.KfcProperty;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;


@Slf4j(topic = "KfcQianzhuController")
@RestController
@RequestMapping("/wx/kfc")
public class KfcQianzhuController {
	

	
	@Autowired
    private MallUserService userService;
	
	
	@Autowired
    private KfcExecuteClient kfcExecuteClient;

    @Autowired
    private KfcProperty kfcProperty;
	
	//private final Log log = LogFactory.getLog(KfcQianzhuController.class);
	
	/**
     * 跳转肯德基登录地址
     *
     * @param userId 用户id
     * @return 登录地址
     */
    @GetMapping("/loginUrl")
    public Object loginUrl(@LoginUser String userId) {

		if (userId == null) {
			return ResponseUtil.unlogin();
		}
		
		

		try {

			MallUser user = userService.findById(userId);
			if (com.graphai.commons.util.obj.ObjectUtils.isNotNull(user)) {
				
				UserModel userModel = new UserModel();
				userModel.setId(userId);
				userModel.setNickname(user.getNickname());
				if (StrUtil.isBlank(user.getMobile())) {
					return ResponseUtil.fail(400, "手机号不能为空");
				}
				userModel.setMobile(user.getMobile());

				GetUserTokenRequest getUserTokenRequest = new GetUserTokenRequest();
				getUserTokenRequest.setPlatformUniqueId(userModel.getId()).setNickname(userModel.getNickname())
						.setMobile(userModel.getMobile());

				GetUserTokenResponse userTokenResponse = kfcExecuteClient.execute(getUserTokenRequest);
				Assert.isTrue(Boolean.TRUE.equals(userTokenResponse.getSuccess()), userTokenResponse.getMessage());

				return ResponseUtil.ok(String.format("https://kfc.qianzhu8.com/index?token=%s&platformId=%s",
						userTokenResponse.getData().getAccessToken(), kfcProperty.getPlatformId()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ResponseUtil.fail();
    }
    
    
    @PostMapping("/notify")
    public Object onOrderEvent(@RequestBody OrderEventModel orderEventModel) {
    	log.info("received kfc order message from qianzhu, message={}", JSON.toJSONString(orderEventModel));

        if (orderEventModel == null || orderEventModel.getEventType() == null) {
        	log.error("kfc order message is illegal。[C101]");
            return ResponseUtil.fail(400, "参数不能为空!");
        }
        
       
        // 校验签名
        checkSign(orderEventModel);

        final KfcOrderModel kfcOrderModel = getByOrderNo(orderEventModel.getOrderNo());
        
        if (kfcOrderModel == null) {
        	log.info("-------------查询不到订单------------");
            return ResponseUtil.fail(506,"查询不到订单!");
        }
        
		  if (StringUtils.isBlank(kfcOrderModel.getPlatformUniqueId())) {
			  return ResponseUtil.fail(508, "订单没有返回用户标识id");		
	    }
        
        MallUser user = userService.findById(kfcOrderModel.getPlatformUniqueId());
        
        if (ObjectUtil.isNull(user)||StringUtils.isBlank(user.getClientId())) {
        	return ResponseUtil.fail(508, "获取client失败!");	
		}
        
        
        switch (orderEventModel.getEventType()) {
        // 部分取消
        case -3:
        	PushUtil.pushToSingle(user.getClientId(),orderEventModel.getEventType());
            // todo 发送部分取消的通知给用户
        	return "success";
        // 全部取消
        case -5:
            // todo 发送取消的通知给用户
        	PushUtil.pushToSingle(user.getClientId(),orderEventModel.getEventType());
        	return "success";
        case -10:
        	// todo 发送取消的通知给用户
        	PushUtil.pushToSingle(user.getClientId(),orderEventModel.getEventType());
        	return "success";
        // 完成
        case 10:
        PushUtil.pushToSingle(user.getClientId(),orderEventModel.getEventType());
        // todo 发送已出票的通知给用户
            return "success";
        // 非法事件
        default:
        	log.error("kfc order message is illegal。[C999]");
            return ResponseUtil.fail(506, "订单事件类型有误!");
    }
        
    }

    /**
     * 校验签名
     *
     * @param orderEventModel orderEventModel
     */
    private void checkSign(OrderEventModel orderEventModel) {
        Map<String, String> requestMap = new HashMap<>(8);
        requestMap.put("orderNo", orderEventModel.getOrderNo());
        requestMap.put("eventType", orderEventModel.getEventType().toString());
        requestMap.put("platformId", orderEventModel.getPlatformId().toString());
        requestMap.put("platform", orderEventModel.getPlatform());
     
        final String sign = SignUtils.generateSign(requestMap, kfcProperty.getSecret());
        orderEventModel.setSign(sign);
        Assert.isTrue(sign.equalsIgnoreCase(orderEventModel.getSign()), "签名校验失败。");
    }

    /**
     * 根据订单号获取订单
     *
     * @param orderNo 订单号
     * @return 订单
     */
    private KfcOrderModel getByOrderNo(String orderNo) {
        GetKfcOrderByOrderNoRequest request = new GetKfcOrderByOrderNoRequest();
        request.setOrderNo(orderNo);

        final GetKfcOrderByOrderNoResponse kfcOrderByOrderNoResponse = kfcExecuteClient.execute(request);
        log.info("get kfc order,order={}", JSON.toJSONString(kfcOrderByOrderNoResponse));

        if (kfcOrderByOrderNoResponse == null) {
        	log.warn("get kfc order error,orderNo={} [C104]", orderNo);
            return null;
        }
        if (!Boolean.TRUE.equals(kfcOrderByOrderNoResponse.getSuccess())) {
            log.warn("get kfc order error,orderNo={} [C105]", orderNo);
            return null;
        }

        return kfcOrderByOrderNoResponse.getData();
    }
    

  

    
    
   
    
	

}
