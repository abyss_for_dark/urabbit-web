package com.graphai.mall.wx.web.merchant;


import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageException;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.dataokutil.HttpUtils;
import com.graphai.id.GraphaiIdGenerator;
import com.graphai.mall.admin.domain.MallMerchant;
import com.graphai.mall.admin.domain.MallUserMerchant;
import com.graphai.mall.admin.service.IMallMerchantService;
import com.graphai.mall.admin.service.IMallUserMerchantService;
import com.graphai.mall.db.domain.*;
import com.graphai.mall.db.service.MallAdminService;
import com.graphai.mall.db.service.category.MallCategoryService;
import com.graphai.mall.db.service.common.MallPhoneValidationService;
import com.graphai.mall.db.service.goods.MallGoodsService;
import com.graphai.mall.db.service.order.MallOrderService;
import com.graphai.mall.db.service.sysconfig.MallSystemConfigService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.service.user.behavior.MallCollectService;
import com.graphai.mall.db.vo.MallMerchantVo;
import com.graphai.mall.live.domain.MallLiveRoom;
import com.graphai.mall.live.service.IMallLiveRoomService;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.mall.wx.util.StringUtils;
import com.xxl.job.admin.core.alarm.JobAlarmer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@RequestMapping("/wx/merchant")
@Validated
public class MallMerchantController {
    private static Logger logger = LoggerFactory.getLogger(JobAlarmer.class);

    private static final String key = "0adaff1dc4e2e4193d6403a64cfce19e";
    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
    @Autowired
    private IMallMerchantService serviceImpl;
    @Autowired
    private MallCategoryService mallCategoryService;
    @Resource
    private MallGoodsService goodsService;
    @Autowired
    private MallCollectService mallCollectService;
    @Autowired
    private MallUserService mallUserService;
    @Autowired
    private MallAdminService mallAdminService;
    @Autowired
    private IMallUserMerchantService mallUserMerchantService;
    @Autowired
    private MallOrderService mallOrderService;
    @Autowired
    private MallSystemConfigService mallSystemConfigService;
    @Resource
    private MallPhoneValidationService phoneService;
    @Autowired
    private IMallLiveRoomService mallLiveRoomService;



    @PostMapping("/save")
    public Object save(@LoginUser String userId, @RequestBody MallMerchant mallMerchant) {
        if(StrUtil.isBlank(userId)){
            return ResponseUtil.unlogin();
        }

        try {
            logger.info("userId:" + userId);
            String merchantId = GraphaiIdGenerator.nextId("MallMerchant");
            //如果入驻的是工厂,则补充订单中的商户id
            if (mallMerchant.getType().equals(0)) {
                if (ObjectUtil.isNotNull(mallMerchant.getOrderId()) && ObjectUtil.isNotEmpty(mallMerchant.getOrderId())) {
                    MallOrder mallOrder = mallOrderService.findById(mallMerchant.getOrderId());
                    mallOrder.setMerchantId(merchantId);
                    mallOrderService.updateByExample(mallOrder);
                } else {
                    return ResponseUtil.fail("工厂入驻订单查询失败");
                }
            }
            mallMerchant.setId(merchantId);
            mallMerchant.setStatus(1);
            mallMerchant.setAuditStatus(0);
            mallMerchant.setCreateTime(simpleDateFormat.format(new Date()));
            mallMerchant.setUpdateTime(simpleDateFormat.format(new Date()));
            mallMerchant.setName(mallMerchant.getTitle());//商户名称当作简称
            mallMerchant.setContacts(mallMerchant.getLegal());//法人名称当作联系人
            serviceImpl.save(mallMerchant);

            MallUserMerchant mallUserMerchant = new MallUserMerchant();
            mallUserMerchant.setId(GraphaiIdGenerator.nextId("MallUserMerchant"));
            mallUserMerchant.setUserId(userId);
            mallUserMerchant.setMerchantId(mallMerchant.getId());
            mallUserMerchantService.save(mallUserMerchant);

            return ResponseUtil.ok("商户入驻申请成功");
        } catch (Exception e) {
            logger.info("错误", e);
            e.printStackTrace();
            return ResponseUtil.fail("商户入驻申请失败！！！");
        }
    }


    @GetMapping("/single")
    public Object single(@LoginUser String userId) {
        if(StrUtil.isBlank(userId)){
            return ResponseUtil.unlogin();
        }

        try {
            QueryWrapper<MallUserMerchant> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("user_id", userId);
            MallUserMerchant mallUserMerchant = mallUserMerchantService.getOne(queryWrapper);
            //如果用户申请了商户，则入驻费用肯定是商户的入驻费用
            if (ObjectUtil.isNotNull(mallUserMerchant) && ObjectUtil.isNotEmpty(mallUserMerchant)) {
                MallMerchant mallMerchant = serviceImpl.getById(mallUserMerchant.getMerchantId());
                String money = null;
                if (mallMerchant.getAuditStatus() == 1 || mallMerchant.getAuditStatus() == 3) {
                    if (mallMerchant.getLevel() != null && mallMerchant.getType() == 1) {
                        switch (mallMerchant.getLevel()) {
                            case 0:
                                money = mallSystemConfigService.getMallSystemCommonKeyValue("fangtao_low_merchant_money");
                                break;
                            case 1:
                                money = mallSystemConfigService.getMallSystemCommonKeyValue("fangtao_middle_merchant_money");
                                break;
                            case 2:
                                money = mallSystemConfigService.getMallSystemCommonKeyValue("fangtao_high_merchant_money");
                                break;
                        }
                    } else {
                        money = mallSystemConfigService.getMallSystemCommonKeyValue("fangtao_plant_money");
                    }
                }
                Map<String, Object> result = new HashMap<>();
                result.put("money", money);
                result.put("merchant", mallMerchant);
                return ResponseUtil.ok(result);
            } else {
                //如果用户没有申请，则入住费用肯定是工厂的
                String money = mallSystemConfigService.getMallSystemCommonKeyValue("fangtao_plant_money");
                Map<String, Object> result = new HashMap<>();
                result.put("money", money);
                result.put("merchant", null);
                return ResponseUtil.ok(result);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            return ResponseUtil.fail("商户入驻查询失败！！！");
        }
    }


    @GetMapping("/phone")
    public Object phone(@RequestParam String phone, @RequestParam String code) {
        String smsType = "APP_LOGIN";
        MallPhoneValidation mallPhoneValidation = phoneService.getPhoneValidationByCode(phone, code,
                smsType);

        if (mallPhoneValidation == null) {
            return ResponseUtil.fail("验证码错误");
        } else {
            QueryWrapper<MallMerchant> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("phone", phone);
            MallMerchant mallMerchant = serviceImpl.getOne(queryWrapper);
            if (ObjectUtil.isNotNull(mallMerchant) && ObjectUtil.isNotEmpty(mallMerchant)) {
                return ResponseUtil.fail("已入驻平台");
            }
            return ResponseUtil.ok();
        }
    }

    @PostMapping("/modify")
    public Object modify(@RequestBody MallMerchant mallMerchant) {
        mallMerchant.setAuditStatus(0);
        serviceImpl.updateById(mallMerchant);
        return ResponseUtil.ok();
    }

    @GetMapping("/fangtao/query")
    public Object fangtaoQuery(@RequestParam(required = false,defaultValue = "zyCategory")String positionCode) {
        List<MallCategory> mallCategories = mallCategoryService.getFangtaoCategory(positionCode);

        return ResponseUtil.ok(mallCategories);
    }


    /**
     * 第一个接口，根据查询条件查询商家，并查询3个商品
     * <p>
     * type:
     * 0：公域用户；1：私域用户，2：商家用户，3：工厂用户
     *
     * @return
     */
    @GetMapping("/list")
    public Object list(@LoginUser String userId, @RequestParam(required = false) String name, @RequestParam(required = false) String type,
                       @RequestParam(required = false) String order, @RequestParam(required = false) String category,
                       @RequestParam(defaultValue = "113.27") String lng, @RequestParam(defaultValue = "23.13") String lat,
                       @RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit) {

        if (StringUtils.isBlank(userId)) {
            return ResponseUtil.unlogin();
        }

        MallUser mallUser = mallUserService.findById(userId);

        if ("0".equals(mallUser.getLevelId())) {
            if (ObjectUtil.isNotEmpty(mallUser.getFirstLeader()) && ObjectUtil.isNotNull(mallUser.getFirstLeader())) {
                //私域用户
                QueryWrapper<MallUserMerchant> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("user_id", mallUser.getFirstLeader());
                String merchantId = mallUserMerchantService.getOne(queryWrapper).getMerchantId();
                MallMerchant mallMerchant = serviceImpl.getById(merchantId);
                Map<String, Object> result = new HashMap<>();
                result.put("type", 1);
                result.put("merchant", mallMerchant);
                return ResponseUtil.ok(result);
            } else {
                //公域用户
                return serviceImpl.publicList(name, type, order, category, lng, lat, page, limit);
            }
        } else if ("9".equals(mallUser.getLevelId()) || "10".equals(mallUser.getLevelId()) || "11".equals(mallUser.getLevelId())) {
            //商家用户
            return serviceImpl.merchantList(userId, name, type, order, category, lng, lat, page, limit);
        } else if ("98".equals(mallUser.getLevelId())) {
            //工厂用户
            return serviceImpl.plantList(userId, name, type, order, category, lng, lat, page, limit);
        }

        return ResponseUtil.ok();
    }


    /**
     * 第二个接口，根据商家id获取商家详情
     *
     * @return
     */
    @GetMapping("/detail")
    public Object detail(@LoginUser String userId, @RequestParam String merchantId) {
        if(StrUtil.isBlank(userId)){
            return ResponseUtil.unlogin();
        }

        MallMerchant mallMerchant = serviceImpl.getById(merchantId);
        QueryWrapper<MallUserMerchant> queryWrapper1 = new QueryWrapper<>();
        queryWrapper1.eq("merchant_id",mallMerchant.getId());
        MallUserMerchant mallUserMerchant = mallUserMerchantService.getOne(queryWrapper1);
        Boolean self = false;
        if(userId.equals(mallUserMerchant.getUserId())){
            self = true;
        }

        MallUser mallUser = mallUserService.findById(userId);
        String firstLeader = mallUser.getFirstLeader();
        Boolean lock = false;
        MallMerchant mallMerchant1 = null;
        if(ObjectUtil.isNotNull(firstLeader) && ObjectUtil.isNotEmpty(firstLeader)){
            lock = true;
            MallUserMerchant first = mallUserMerchantService.getByUserId(firstLeader);
            mallMerchant1 = serviceImpl.getById(first.getMerchantId());

        }

        if (ObjectUtil.isNull(mallMerchant)){
            return ResponseUtil.fail(600,"该店铺不存在");
        }
        mallMerchant.setBrowseNum(mallMerchant.getBrowseNum()==null?0:mallMerchant.getBrowseNum()+1);
        serviceImpl.updateById(mallMerchant);

        MallCollect mallCollect = mallCollectService.queryByTypeAndValue(userId, (byte) 3, merchantId);
        Boolean flag = false;

        if (ObjectUtil.isNotNull(mallCollect) && ObjectUtil.isNotEmpty(mallCollect)) {
            flag = true;
        }

        String categoryName = mallCategoryService.findByCategory(mallMerchant.getCategory()).getName();
        mallMerchant.setCategoryName(categoryName);

        //获取当前商户的直播间（只有正在直播的才会展示）
        QueryWrapper<MallLiveRoom> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("add_id",mallUserMerchant.getUserId());
        queryWrapper.eq("room_status",1);//开播状态
        MallLiveRoom mallLiveRoom = mallLiveRoomService.getOne(queryWrapper);
        String roomId = "";
        if(ObjectUtil.isNotNull(mallLiveRoom) && ObjectUtil.isNotEmpty(mallLiveRoom)){
            roomId = mallLiveRoom.getId();
        }
        try {
            String enterTime = mallMerchant.getEnterTime();
            Date date = simpleDateFormat.parse(enterTime);
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            cal.add(Calendar.YEAR, 1);
            mallMerchant.setUpdateTime(simpleDateFormat.format(cal.getTime()));
        }catch (ParseException e){
            logger.info("入驻时间转化错误");
        }


        Map<String, Object> result = new HashMap<>();
        result.put("merchant", mallMerchant);
        result.put("flag", flag);
        result.put("self", self);
        result.put("lock", lock);
        result.put("mallMerchant", mallMerchant1);
        result.put("roomId", roomId);
        return ResponseUtil.ok(result);
    }


    /**
     * 第二个接口，根据商家id获取商家详情
     *
     * @return
     */
    @GetMapping("/detail/goods")
    public Object detailGoods(@LoginUser String userId, @RequestParam String merchantId,@RequestParam(defaultValue = "") String factoryId,@RequestParam(defaultValue = "") String category, @RequestParam(required = false, defaultValue = "1") String type,
                              @RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit) {

        List<String> categoryIds = new ArrayList<>();
        if(StrUtil.isNotEmpty(category)){
            List<MallCategory> categories = mallCategoryService.queryByPid(category);
            for(MallCategory item: categories){
                categoryIds.add(item.getId());
            }
            if(categoryIds.size() > 0) {
                List<MallCategory> mallCategories = mallCategoryService.queryByPids(categoryIds);
                for (MallCategory item : mallCategories) {
                    categoryIds.add(item.getId());
                }
            }
            categoryIds.add(category);
        }

        String first = mallUserService.findById(userId).getFirstLeader();

        if(ObjectUtil.isEmpty(first) || ObjectUtil.isNull(first)){
            factoryId = null;
        }
//        MallGoodsExample example = new MallGoodsExample();
//        MallGoodsExample.Criteria criteria = example.or();
//        if(StrUtil.isNotEmpty(category)){
//            criteria.andCategoryIdIn(categoryIds);
//        }
//        criteria.andMerchantIdEqualTo(merchantId).andIsOnSaleEqualTo(true).andDeletedEqualTo(false);
//        if ("2".equals(type)) {
//            example.setOrderByClause("sort_order DESC");
//        } else if ("1".equals(type)) {
//            example.setOrderByClause("sales_tip+0 DESC");
//        }
//
//
//        PageHelper.startPage(page,limit);
//        List<MallGoods> goods = goodsService.selectByExample(example);
//        long total = PageInfo.of(goods).getTotal();
//
//
//
//
//        Map<String, Object> result = new HashMap<>();
//        result.put("goods",goods);
//        result.put("total",total);
        return goodsService.detailGoods(categoryIds,merchantId,factoryId,type,page,limit);
    }


    /**
     * 第二个接口，根据商家id获取商家详情
     *
     * @return
     */
    @GetMapping("/gaode/address")
    public Object address(@RequestParam String longitude, @RequestParam String latitude) {
        String url = "https://restapi.amap.com/v3/geocode/regeo?output=JSON&location=" + longitude + "," + latitude + "&key=" + key + "&radius=1000&extensions=all";
        String result = HttpUtils.sendGet(url, null);
        logger.info(result);
        JSONObject jsonObject = new JSONObject(result);
        logger.info(jsonObject.toString());
        Object str = jsonObject.get("status");
        String status = null;
        if (ObjectUtil.isNotNull(str)) {
            status = str.toString();
        }
        if ("1".equals(status)) {
            JSONObject regeocode = new JSONObject(jsonObject.get("regeocode"));
            Object addressStr = regeocode.get("addressComponent").toString();
            if (ObjectUtil.isNotNull(addressStr)) {
                JSONObject address = new JSONObject(addressStr);
                return ResponseUtil.ok(address);
            }
        } else {
            logger.info(jsonObject.toString());
            return ResponseUtil.fail("查询失败");
        }
        return ResponseUtil.fail("查询失败");
    }


}
