package com.graphai.mall.wx.web.test.set;

/**
 * 整点红包排行榜金额
 *
 * @author Qxian
 * @date 2021-02-20
 *
 * 3.Set集合的主要特点及用法
 * 3.1 不允许重复对象
 * 3.2 只允许一个 null 元素
 * 3.3 无序容器，你无法保证每个元素的存储顺序，TreeSet通过 Comparator 或者 Comparable 维护了一个排序顺序
 * 3.4 Set 接口最流行的几个实现类是 HashSet、LinkedHashSet 以及 TreeSet。最流行的是基于 HashMap 实现的 HashSet；TreeSet 还实现了 SortedSet 接口，因此 TreeSet 是一个根据其 compare() 和 compareTo() 的定义进行排序的有序容器
 */
public class Person implements Comparable<Person> {

    private String id;
    private String name;
    private String address;

    public Person(String id, String name, String address) {
        this.id = id;
        this.name = name;
        this.address = address;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                '}';
    }

    @Override
    public int compareTo(Person o) {
        if (o instanceof Person) {
            Person o1 = (Person) o;
            return new Double(o1.getId()).compareTo(new Double(this.getId()));
        }
        throw new ClassCastException("不能转换为Person类型的对象...");
    }
}
