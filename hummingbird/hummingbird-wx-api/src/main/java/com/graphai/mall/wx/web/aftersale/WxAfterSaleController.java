package com.graphai.mall.wx.web.aftersale;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.graphai.commons.constant.AftersaleConstant;
import com.graphai.commons.constant.ApiCodeEnum;
import com.graphai.commons.constant.UserConstants;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.json.JacksonUtil;
import com.graphai.mall.admin.domain.MallPurchaseOrder;
import com.graphai.mall.admin.domain.MallPurchaseOrderGoods;
import com.graphai.mall.admin.service.IMallPurchaseOrderGoodsService;
import com.graphai.mall.admin.service.IMallPurchaseOrderService;
import com.graphai.mall.db.domain.MallAftersale;
import com.graphai.mall.db.domain.MallOrderGoods;
import com.graphai.mall.db.domain.MallUser;
import com.graphai.mall.db.service.MallAdminService;
import com.graphai.mall.db.service.order.MallAftersaleService;
import com.graphai.mall.db.service.sysconfig.MallSystemConfigService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.util.OrderUtil;
import com.graphai.mall.db.util.UserUtils;
import com.graphai.mall.setting.service.FreightTemplateService;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.mall.wx.dto.AfterSaleInfo;
import com.graphai.open.mallorder.entity.IMallOrder;
import com.graphai.open.mallorder.service.IMallOrderZYService;
import com.graphai.open.mallordergoods.entity.IMallOrderGoods;
import com.graphai.open.mallordergoods.service.IMallOrderGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/wx/afterSale")
public class WxAfterSaleController {

    @Autowired
    private IMallPurchaseOrderService purchaseOrderService;

    @Autowired
    private IMallPurchaseOrderGoodsService purchaseOrderGoodsService;

    @Autowired
    private MallAftersaleService aftersaleService;

    @Autowired
    private IMallOrderZYService orderZYService;

    @Autowired
    private IMallOrderGoodsService orderGoodsService;

    @Autowired
    private MallUserService userService;

    @Autowired
    private IMallPurchaseOrderGoodsService mallPurchaseOrderGoodsService;

    @Autowired
    private MallSystemConfigService systemConfigService;

    @PostMapping("/orderGoodsInfo")
    public Object orderGoodsInfo(@RequestBody String body) {
        String orderId = JacksonUtil.parseString(body, "orderId");
        String goodsId = JacksonUtil.parseString(body, "goodsId");
        String type = JacksonUtil.parseString(body, "type");
        if ("0".equals(type)) {
            IMallOrderGoods orderGoods = orderGoodsService.getOne(new QueryWrapper<IMallOrderGoods>().lambda()
                    .eq(IMallOrderGoods::getOrderId, orderId)
                    .eq(IMallOrderGoods::getGoodsId, goodsId));
            IMallOrder mallOrder = orderZYService.getById(orderId);
            if (ObjectUtil.isNotEmpty(orderGoods)) {
                HashMap<String, Object> map = new HashMap<>();
                map.put("order", mallOrder);
                map.put("orderGoods", orderGoods);
                return ResponseUtil.ok(map);
            }
        } else {
            MallPurchaseOrderGoods purchaseOrderGoods = purchaseOrderGoodsService.getOne(new QueryWrapper<MallPurchaseOrderGoods>().lambda()
                    .eq(MallPurchaseOrderGoods::getOrderId, orderId)
                    .eq(MallPurchaseOrderGoods::getOrderId, goodsId));
            MallPurchaseOrder purchaseOrder = purchaseOrderService.getById(orderId);
            if (ObjectUtil.isNotEmpty(purchaseOrderGoods)) {
                HashMap<String, Object> map = new HashMap<>();
                map.put("purchaseOrder", purchaseOrder);
                map.put("purchaseOrderGoods", purchaseOrderGoods);
                return ResponseUtil.ok(map);
            }
        }
        return ResponseUtil.fail("订单商品不存在！");
    }

    //申请售后
    @PostMapping("/applyAfterSale")
    public Object ApplyAfterSale(@LoginUser String userId, @RequestBody AfterSaleInfo afterSaleInfo) {
        //申请方式

        String applicationMethod = afterSaleInfo.getApplicationMethod();
        MallAftersale mallAftersale = afterSaleInfo.getMallAftersale();
        mallAftersale.setUserId(userId);
        String orderId = mallAftersale.getOrderId();
        MallUser user = userService.queryByUserId(userId);
        Map<String, String> map = systemConfigService.getMallSystemCommon("afterSale");
        String  applyAfterSaleValid = map.get("apply_aftersale_valid");
        Integer validTime = 7;
        if (StrUtil.isNotBlank(applyAfterSaleValid)){
             validTime= Integer.parseInt(applyAfterSaleValid);
        }
        //c端 工厂和普通用户 申请售后 是销售表
        if ("0".equals(user.getLevelId()) || "98".equals(user.getLevelId())) {
            IMallOrder order = orderZYService.getOne(new QueryWrapper<IMallOrder>().lambda().eq(IMallOrder::getId, orderId));
            //售后
            if (OrderUtil.STATUS_AFTERSALE_REFUND.equals(order.getOrderStatus().shortValue())) {
                //判断用户是否已收货
                if (ObjectUtil.isNotNull(order.getConfirmTime())) {
                    if (!order.getConfirmTime().plusDays(validTime).isAfter(LocalDateTime.now())) {
                        return ResponseUtil.fail(ApiCodeEnum.AFTERSALE_REFUSE);
                    }
                }
            } else if (OrderUtil.STATUS_CONFIRM.equals(order.getOrderStatus().shortValue())){
                //用户已收货
                if (!order.getConfirmTime().plusDays(validTime).isAfter(LocalDateTime.now())) {
                    return ResponseUtil.fail(ApiCodeEnum.AFTERSALE_REFUSE);
                }
                order.setOrderStatus(OrderUtil.STATUS_AFTERSALE_REFUND.intValue());
            }else{
                order.setOrderStatus(OrderUtil.STATUS_AFTERSALE_REFUND.intValue());
            }
            order.setUpdateTime(LocalDateTime.now());
            orderZYService.updateById(order);
            mallAftersale.setOrderType(AftersaleConstant.ORDER_TYPE_SALE);
        } else {
            //商户是采购表
            MallPurchaseOrder purchaseOrder = purchaseOrderService.getOne(new QueryWrapper<MallPurchaseOrder>()
                    .lambda().eq(MallPurchaseOrder::getId, orderId));
            //售后
            if (OrderUtil.STATUS_AFTERSALE_REFUND.equals(purchaseOrder.getOrderStatus().shortValue())) {
                //判断用户是否已收货
                if (ObjectUtil.isNotNull(purchaseOrder.getConfirmTime())) {
                    if (!purchaseOrder.getConfirmTime().plusDays(validTime).isAfter(LocalDateTime.now())) {
                        return ResponseUtil.fail(ApiCodeEnum.AFTERSALE_REFUSE);
                    }
                }

            } else if (OrderUtil.STATUS_CONFIRM.equals(purchaseOrder.getOrderStatus().shortValue())) {
                //用户已收货
                if (!purchaseOrder.getConfirmTime().plusDays(validTime).isAfter(LocalDateTime.now())) {
                    return ResponseUtil.fail(ApiCodeEnum.AFTERSALE_REFUSE);
                }
                purchaseOrder.setOrderStatus(OrderUtil.STATUS_AFTERSALE_REFUND.intValue());
            }else{
                purchaseOrder.setOrderStatus(OrderUtil.STATUS_AFTERSALE_REFUND.intValue());
            }
            purchaseOrder.setUpdateTime(LocalDateTime.now());
            purchaseOrderService.updateById(purchaseOrder);
        }
        aftersaleService.add(mallAftersale);
        return ResponseUtil.ok();
    }

    //校验销售参数
    private boolean checkDelivery(IMallOrder currentOrder, IMallOrder oldOrder, String flag) {
        String shipSn = currentOrder.getShipSn();
        String shipChannel = currentOrder.getShipChannel();
        String shipCode = currentOrder.getShipCode();
        String consignee = currentOrder.getConsignee();
        String mobile = currentOrder.getMobile();
        String address = currentOrder.getAddress();
        boolean tag = true;
        if ("ship".equals(flag)) {
            if (StrUtil.isNotBlank(oldOrder.getShipSn()) || StrUtil.isEmpty(shipSn) || StrUtil.isEmpty(shipChannel) || StrUtil.isEmpty(shipCode)) {
                tag = false;
            }
        } else if ("modifyAddress".equals(flag)) {
            if (StrUtil.isEmpty(consignee) || StrUtil.isEmpty(mobile) || StrUtil.isEmpty(address)) {
                tag = false;
            }
        }
        return tag;
    }

    private boolean checkPurchase(IMallOrder currentOrder, String oldOrderShipSn) {
        String shipSn = currentOrder.getShipSn();
        String shipChannel = currentOrder.getShipChannel();
        String shipCode = currentOrder.getShipCode();
        boolean tag = false;
        if (StrUtil.isNotBlank(oldOrderShipSn) || StrUtil.isEmpty(shipSn) || StrUtil.isEmpty(shipChannel) || StrUtil.isEmpty(shipCode)) {
            tag = true;
        }

        return tag;
    }

    private boolean checkPurchaseAfterSale(String orderId) {
        List<MallPurchaseOrderGoods> orderGoodsList = mallPurchaseOrderGoodsService.list(new QueryWrapper<MallPurchaseOrderGoods>().lambda().eq(MallPurchaseOrderGoods::getOrderId, orderId));
        List<MallAftersale> aftersaleList = aftersaleService.findByOrderId(orderId);
        boolean flag = false;
        if (ObjectUtil.isNotEmpty(aftersaleList) && aftersaleList.size() >= orderGoodsList.size() ) {
            flag = true;
        }
        return flag;
    }

    private boolean checkAfterSale(String orderId) {
        List<IMallOrderGoods> orderGoodsList = orderGoodsService.list(new QueryWrapper<IMallOrderGoods>().lambda().eq(IMallOrderGoods::getOrderId, orderId));
        List<MallAftersale> aftersaleList = aftersaleService.findByOrderId(orderId);
        boolean flag = false;
        if (ObjectUtil.isNotEmpty(aftersaleList) && aftersaleList.size() >= orderGoodsList.size()) {
            flag = true;
        }
        return flag;
    }

    @GetMapping("/getAfterSaleInfo")
    public Object getAfterSaleInfo(@RequestParam("orderId") String orderId, @RequestParam("goodsId") String goodsId) {
        MallAftersale aftersale = aftersaleService.findByOrderIdAndOrderGoodsId(orderId, goodsId);
        if (ObjectUtil.isEmpty(aftersale)) {
            return ResponseUtil.ok();
        }
        return ResponseUtil.ok(aftersale);
    }

    /**
     * 代理端处理售后
     * @param aftersale
     * @return
     */
    @PostMapping("/handleAfterSale")
    public Object handleAfterSale(@RequestBody MallAftersale aftersale){
        return aftersaleService.handleRefund(aftersale);
    }
}
