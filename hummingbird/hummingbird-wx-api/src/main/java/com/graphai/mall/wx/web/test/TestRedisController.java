package com.graphai.mall.wx.web.test;

import com.alicp.jetcache.Cache;
import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.CreateCache;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.date.DateUtilTwo;
import com.graphai.commons.util.json.JacksonUtil;
import com.graphai.commons.util.lang.StringUtils;
import com.graphai.commons.util.redpacket.RandomUtils;
import com.graphai.mall.db.service.red.WxRedRankService;
import com.graphai.mall.db.vo.MallUserSortRedisVo;
import com.graphai.mall.wx.web.common.MyBaseController;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * 缓存测试类
 * @author Qxian
 * @date 2021-02-20
 */
@RestController
@RequestMapping("/wx/test/redis")
@Validated
public class TestRedisController extends MyBaseController {

    private final Log log = LogFactory.getLog(TestRedisController.class);

    @Autowired
    private WxRedRankService wxRedRankService;

    // 1.注入redis，将key声明为常量SCORE_RANK
    public static final String SCORE_RANK = "score_rank";
    private String scoreRankKey = SCORE_RANK+ DateUtilTwo.getNowDateShortYmd(); // score_rank20210220

    @CreateCache(name = "TestRedisController.redCache", expire = 24, timeUnit = TimeUnit.HOURS, cacheType = CacheType.REMOTE)
    private Cache<String, Object> sortCache;// Object是对象要实例化才可以存

    /**
     * 测试map最大压力
     *
     * @return
     */
    @GetMapping("cachemp")
    public void testcCachemp() {
        int i = 0;
        while (true) {
            cachemp.put(scoreRankKey + (i++), SCORE_RANK);
        }
    }

    /**
     * 1.1、设置redis红包排行榜-存放100条数据
     * @return
     */
    @PostMapping("setRedRankingTest")
    public Object setRedRankingTest() {
        Set<MallUserSortRedisVo> onTheHourSet = getOnTheHourSet();
        sortCache.put(scoreRankKey,onTheHourSet);
        return onTheHourSet;
    }

    /**
     * 1.2、设置redis红包排行榜-应用
     * @return
     */
    @PostMapping("setRedRanking")
    public Object setRedRanking(@RequestBody String body) {

        String amountParam = JacksonUtil.parseString(body,"amountParam");
        String userId = JacksonUtil.parseString(body,"userId");
        BigDecimal compareAmount = new BigDecimal(amountParam);
        String nickName = "小霸王";
        String avatar = "https://gxfacepay.oss-cn-shenzhen.aliyuncs.com/hummingbird/app/static/img/config/she.png";
        String isMaker = "1";
        String userIdentity = "0";
        String mobile = "18878280468";
        BigDecimal newAmount = compareAmount;

        // （1）根据当天日期获取缓存里面数据
        Object object = sortCache.get(scoreRankKey);
        Set<MallUserSortRedisVo> redSet = (Set<MallUserSortRedisVo>)object;
        // 如果redis为空直接赋值
        if (CollectionUtils.isEmpty(redSet)) {
            redSet = new TreeSet<MallUserSortRedisVo>(); // 要先初始值对象，不然设置不了
            redSet.add(new MallUserSortRedisVo(userId, nickName, avatar, isMaker, compareAmount, newAmount, userIdentity,mobile));
            sortCache.put(scoreRankKey,redSet);
            return redSet;
        }
        log.info("redSet大小="+redSet.size());

        // （2）set集合转换list集合
        List<MallUserSortRedisVo> setList = new ArrayList<MallUserSortRedisVo>(redSet);
        if (CollectionUtils.isEmpty(setList)) {
            return setList;
        }
        log.info("setList大小="+setList.size());

        // （3）获取前100排行中最后一位数据
        int lastCount = 100;
        if (setList.size() < lastCount)
            lastCount = setList.size();
        MallUserSortRedisVo lastVo = setList.get(lastCount-1);
        log.info("获取前100排行中最后一位数据："+lastVo);

        // （4）对比前100排行中最后一位数据金额，新增设置redis值
        boolean flag = false; // 是否可以往redis排行榜里面塞值，默认否
        if (lastVo != null) {
            BigDecimal amount = lastVo.getAmount();
            if (amount == null) {
                amount = new BigDecimal("0");
            }
            if (compareAmount == null) {
                compareAmount = new BigDecimal("0");
            }
            if (compareAmount.compareTo(amount) >= 0)
                flag = true;
        }
        if (flag || setList.size() < lastCount) {
            // 循环判断当前用户ID存在，移除掉，等下重新插入
            for (MallUserSortRedisVo MallUserSortRedisVo : redSet) {
                if (StringUtils.isNotEmpty(MallUserSortRedisVo.getUserId())) {
                    if (userId.equals(MallUserSortRedisVo.getUserId())) {
                        redSet.remove(MallUserSortRedisVo);
                    }
                }
                // 如果金额一样，设置对比金额随机底一点（金额相同，根据时间先到排在前）
                if (StringUtils.isNotNull(MallUserSortRedisVo.getAmount())) {
                    if (compareAmount.compareTo(MallUserSortRedisVo.getAmount()) == 0)
                        newAmount = newAmount.subtract(RandomUtils.getGenerateRandom());
                }
            }
            // 重新设置redis缓存
            redSet.add(new MallUserSortRedisVo(userId, nickName, avatar, isMaker, compareAmount, newAmount, userIdentity,mobile));
            sortCache.put(scoreRankKey,redSet);
        }
        log.info("设置后set大小="+redSet.size());
        for (Object o : redSet) {
            log.info(o);
        }

        return redSet;
    }

    /**
     * 1.3、设置redis红包排行榜-应用-prod
     * @return
     */
    @PostMapping("setRedRankingProd")
    public Object setRedRankingProd(@RequestBody String body) {
        String amountParam = JacksonUtil.parseString(body,"amountParam");
        String userId = JacksonUtil.parseString(body,"userId");
        String type = JacksonUtil.parseString(body,"type");
        // 整点红包排行榜-设置redis
        Map<String,String> resMap = new HashMap<>();
        resMap.put("userId",userId);
        resMap.put("nickName","寄托一个时间");
        resMap.put("avatar","");
        resMap.put("isMaker","0");
        resMap.put("isTalent","1");
        resMap.put("mobile","13128572588");
        resMap.put("compareAmount",amountParam);
        resMap.put("type",type);
        wxRedRankService.setRedRankingCommon(resMap);
        return ResponseUtil.ok();
    }

    /**
     * 2.1、获取redis红包排行榜
     * @return
     */
    @GetMapping("getRedRanking")
    public Object getRedRanking() {

        // （1）整点红包排行榜
        Map<String, Set<MallUserSortRedisVo>> redMap = new HashMap<>();
        List<MallUserSortRedisVo> resulttList = new ArrayList<MallUserSortRedisVo>();
        Object object = sortCache.get(scoreRankKey);
        Set<MallUserSortRedisVo> redSet = (Set<MallUserSortRedisVo>)object;
        if (CollectionUtils.isEmpty(redSet)) {
            return redSet;
        }

        // （2）只获取前10名数据
        List<MallUserSortRedisVo> setList = new ArrayList<MallUserSortRedisVo>(redSet);
        log.info("setList大小="+setList.size());
        int count = 10; // 放到配置表中
        if (setList.size() < count)
            count = setList.size();

        // （3）返回结果
        for (int i=0; i<count; i++) {
            resulttList.add(setList.get(i));
        }
        return resulttList;
    }

    /**
     * 2.2、获取redis红包排行榜-prod
     * @return
     */
    @GetMapping("getRedRankingProd")
    public Object getRedRankingProd(@RequestParam String type) {
        return wxRedRankService.getRedRankingCommon(type);
    }

    // 设置redis红包排行榜
    private Set<MallUserSortRedisVo> getOnTheHourSet(){
        Set<MallUserSortRedisVo> set = new TreeSet<MallUserSortRedisVo>();
        String nickName = "小霸王";
        String avatar = "https://gxfacepay.oss-cn-shenzhen.aliyuncs.com/hummingbird/app/static/img/config/she.png";
        String isMaker = "1";
        String userIdentity = "0";
        String mobile = "18878280468";
        for(int i=0; i<100; i++){
            String userId = ""+i;
            BigDecimal amount = new BigDecimal("5.20").add(new BigDecimal(String.valueOf(i)));
            set.add(new MallUserSortRedisVo(userId, nickName, avatar, isMaker, amount, amount,userIdentity,mobile));
        }
        set.add(new MallUserSortRedisVo("5027831790648164351", nickName, avatar, isMaker, new BigDecimal("216.20"), new BigDecimal("216.20"),userIdentity,mobile));
        set.add(new MallUserSortRedisVo("5027831790648164351", nickName, avatar, isMaker, new BigDecimal("16.20"), new BigDecimal("16.20"),userIdentity,mobile));
        set.add(new MallUserSortRedisVo("5027831790648164351", nickName, avatar, isMaker, new BigDecimal("116.20"), new BigDecimal("116.20"),userIdentity,mobile));
        set.add(new MallUserSortRedisVo("5027831790648164353", nickName, avatar, isMaker, new BigDecimal("1.20"), new BigDecimal("1.20"),userIdentity,mobile));
        set.add(new MallUserSortRedisVo("5027831790648164352", nickName, avatar, isMaker, new BigDecimal("2.20"), new BigDecimal("2.20"),userIdentity,mobile));
        set.add(new MallUserSortRedisVo("5027831790648164352", nickName, avatar, isMaker, new BigDecimal("119.20"), new BigDecimal("119.20"),userIdentity,mobile));

        log.info("大小="+set.size());
        for(Object o : set){
            log.info(o);
        }
        return set;
    }

    // 测试类
    public static void main(String[] args) {

        Set<MallUserSortRedisVo> set = new TreeSet<MallUserSortRedisVo>();
        String nickName = "小霸王";
        String avatar = "https://gxfacepay.oss-cn-shenzhen.aliyuncs.com/hummingbird/app/static/img/config/she.png";
        String isMaker = "1";
        String userIdentity = "0";
        String mobile = "18878280468";
        for (int i=0; i<100; i++) {
            String userId = ""+i;
            BigDecimal amount = new BigDecimal("5.20").add(new BigDecimal(String.valueOf(i)));
            set.add(new MallUserSortRedisVo(userId, nickName, avatar, isMaker, amount,amount, userIdentity,mobile));
        }
        set.add(new MallUserSortRedisVo("5027831790648164351", nickName, avatar, isMaker, new BigDecimal("216.20"), new BigDecimal("216.20"),userIdentity,mobile));
        set.add(new MallUserSortRedisVo("5027831790648164351", nickName, avatar, isMaker, new BigDecimal("16.20"), new BigDecimal("16.20"),userIdentity,mobile));
        set.add(new MallUserSortRedisVo("5027831790648164351", nickName, avatar, isMaker, new BigDecimal("116.20"), new BigDecimal("116.20"),userIdentity,mobile));
        set.add(new MallUserSortRedisVo("5027831790648164353", nickName, avatar, isMaker, new BigDecimal("1.20"), new BigDecimal("1.20"),userIdentity,mobile));
        set.add(new MallUserSortRedisVo("5027831790648164352", nickName, avatar, isMaker, new BigDecimal("2.20"), new BigDecimal("2.20"),userIdentity,mobile));
        set.add(new MallUserSortRedisVo("5027831790648164352", nickName, avatar, isMaker, new BigDecimal("119.20"), new BigDecimal("119.20"),userIdentity,mobile));

        // （1）获取所有数据
        System.out.println("set大小="+set.size());
        for (Object o : set) {
            System.out.println(o);
        }

        // （2）只获取前10名数据
        List<MallUserSortRedisVo> setList = new ArrayList<MallUserSortRedisVo>(set);
        System.out.println("setList大小="+set.size());
        int count = 10;
        if (setList.size() < count)
            count = setList.size();
        for (int i=0; i<count; i++) {
            System.out.println(setList.get(i));
        }
        // 拼装List<Map<String, Object>>返回结果
        List<Map<String, Object>> listMap =new ArrayList<Map<String,Object>>();
        for (int i=0; i<count; i++) {
            Map<String, Object> map = new HashMap<String, Object>();
            MallUserSortRedisVo musrVo = setList.get(i);
            map.put("userId",musrVo.getUserId());
            map.put("nickName", musrVo.getNickName());
            map.put("avatar", musrVo.getAvatar());
            map.put("mobile", musrVo.getMobile());
            map.put("isMaker", musrVo.getIsMaker());
            map.put("isTalent", musrVo.getIsTalent());
            map.put("amount", musrVo.getAmount());
            listMap.add(map);
        }
        System.out.println("--------------------拼装List<Map<String, Object>>返回结果---------------------");
        System.out.println(listMap.toString());

        // （3）获取前100排行中最后一位数据
        int lastCount = 100;
        if (setList.size() < lastCount)
            lastCount = setList.size();
        MallUserSortRedisVo lastVo = setList.get(lastCount-1);
        System.out.println("获取前100排行中最后一位数据："+lastVo);

        // （4）对比前100排行中最后一位数据金额，新增设置redis值
        boolean flag = false; // 是否可以往redis排行榜里面塞值，默认否
        BigDecimal compareAmount = new BigDecimal("8.21");
        if (lastVo != null) {
            BigDecimal amount = lastVo.getAmount();
            if (amount == null) {
                amount = new BigDecimal("0");
            }
            if (compareAmount.compareTo(amount) == 1)
                flag = true;
        }
        if (flag || setList.size() < lastCount) {
            set.add(new MallUserSortRedisVo("5027831790648164388", nickName, avatar, isMaker, compareAmount,compareAmount, userIdentity,mobile));
        }
        System.out.println("设置后set大小="+set.size());
        for (Object o : set) {
            System.out.println(o);
        }

    }

}
