package com.graphai.mall.wx.web.auth;

import static com.graphai.mall.wx.util.WxResponseCode.AUTH_INVALID_MOBILE;
import static com.graphai.mall.wx.util.WxResponseCode.AUTH_MOBILE_REGISTERED;
import static com.graphai.mall.wx.util.WxResponseCode.AUTH_NAME_REGISTERED;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.graphai.commons.constant.MallConstant;
import com.graphai.commons.util.IpUtil;
import com.graphai.commons.util.RegexUtil;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.bcrypt.BCryptPasswordEncoder;
import com.graphai.commons.util.httprequest.HttpRequestUtils;
import com.graphai.commons.util.json.JacksonUtils;
import com.graphai.mall.core.notify.NotifyService;
import com.graphai.mall.db.domain.FcAccount;
import com.graphai.mall.db.domain.MallAddress;
import com.graphai.mall.db.domain.MallPhoneValidation;
import com.graphai.mall.db.domain.MallUser;
import com.graphai.mall.db.service.account.AccountService;
import com.graphai.mall.db.constant.account.AccountStatus;
import com.graphai.mall.db.service.bmdesign.BmdesignTeamService;
import com.graphai.mall.db.service.common.MallPhoneValidationService;
import com.graphai.mall.db.service.coupon.CouponAssignService;
import com.graphai.mall.db.service.sysconfig.MallSystemConfigService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.service.user.behavior.MallAddressService;
import com.graphai.mall.db.util.phone.PushUtils;
import com.graphai.mall.tb.dto.TaoBaoBaiChuanUserInfo;
import com.graphai.mall.wx.dto.MiniProgramUserInfo;
import com.graphai.mall.wx.dto.RegisterInfo;
import com.graphai.mall.wx.dto.WxAppAuthInfo;
import com.graphai.mall.wx.service.UserTokenManager;
import com.graphai.mall.wx.service.WxOrderService;
import com.graphai.properties.TaoBaoProperties;
import com.taobao.api.DefaultTaobaoClient;
import com.taobao.api.TaobaoClient;
import com.taobao.api.request.TbkScPublisherInfoSaveRequest;
import com.taobao.api.request.TopAuthTokenCreateRequest;
import com.taobao.api.response.TbkScPublisherInfoSaveResponse;
import com.taobao.api.response.TbkScPublisherInfoSaveResponse.Data;
import com.taobao.api.response.TopAuthTokenCreateResponse;
import com.vdurmont.emoji.EmojiParser;

import cn.binarywang.wx.miniapp.api.WxMaService;
import cn.binarywang.wx.miniapp.bean.WxMaJscode2SessionResult;
import cn.hutool.core.util.ObjectUtil;
import me.chanjar.weixin.common.bean.WxOAuth2UserInfo;
import me.chanjar.weixin.common.bean.oauth2.WxOAuth2AccessToken;
import me.chanjar.weixin.mp.api.WxMpService;


/**
 * 重写登录服务
 *
 * @author biteam
 */
@RestController
@RequestMapping("/wx/login")
@Validated
public class AppLoginController {
    private final Log logger = LogFactory.getLog(AppLoginController.class);

    @Autowired
    private MallUserService userService;

    @Autowired
    private WxMaService wxService;
    /**
     * 微信公众号的实现
     **/
    @Autowired
    private WxMpService wxMpService;

    @Autowired
    private TaoBaoProperties taoBaoProperties;

    @Autowired
    private NotifyService notifyService;

    @Autowired
    private CouponAssignService couponAssignService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private MallPhoneValidationService phoneService;
    @Autowired
    private MallAddressService mallAddressService;
    @Autowired
    private BmdesignTeamService bmdesignTeamService;
    @Autowired
    private WxOrderService wxOrderService;
    @Autowired
    private MallSystemConfigService mallSystemConfigService;

    /**
     * 登录和注册统一使用同一个方法
     *
     * @param registerInfo
     * @param request
     * @return
     */
    @PostMapping("auth")
    public Object authLogin(@RequestBody RegisterInfo registerInfo, HttpServletRequest request) {
        /** 注册渠道 **/
        String registerChannel = registerInfo.getRegisterChannel();
        // WxMpUser wxMpUser = null;
        WxOAuth2UserInfo wxMpUser = null;
        String openId = null;
        /** 推荐码 **/
        String invitationCode = registerInfo.getInvitationCode();
        /** 微信授权的code **/
        String wxMpCode = registerInfo.getWxCode();
        /** 用户名 **/
        String username = registerInfo.getUsername();
        /** 密码 **/
        String password = registerInfo.getPassword();

        try {
            /** 如果是自定义注册或者是自定义注册附带邀请码 **/
            if (MallConstant.REGISTER_TYPE_CUSTOM.equalsIgnoreCase(registerChannel)
                            || MallConstant.REGISTER_TYPE_INVITATION.equalsIgnoreCase(registerChannel)) {
                /** 验证参数 **/
                Object obj = needValidation(registerInfo);
                if (obj != null) {
                    return obj;
                }
            }

            MallUser user = new MallUser();
            String sessionKey = "";
            /** 微信小程序授权登录 **/
            if (MallConstant.REGISTER_TYPE_WXMINI.equalsIgnoreCase(registerChannel) && !StringUtils.isEmpty(wxMpCode)) {
                MiniProgramUserInfo miniProgramUserInfo = registerInfo.getMiniProgramUserInfo();
                WxMaJscode2SessionResult result = this.wxService.getUserService().getSessionInfo(wxMpCode);
                sessionKey = result.getSessionKey();
                openId = result.getOpenid();
                user.setMiniProgramOpenid(openId);
                user.setAvatar(miniProgramUserInfo.getAvatarUrl());
                user.setNickname(miniProgramUserInfo.getNickName());
                user.setGender(miniProgramUserInfo.getGender().toString());
                user.setProvince(miniProgramUserInfo.getProvince());
                user.setCity(miniProgramUserInfo.getCity());
                user.setCountry(miniProgramUserInfo.getCountry());
            }
            /** 微信APP授权登录注册 **/
            else if (MallConstant.REGISTER_TYPE_WXAPP.equalsIgnoreCase(registerChannel)) {
                WxAppAuthInfo wxAppAuthInfo = registerInfo.getWxAppAuthInfo();
                /** 请求开放平台接口获取用户信息 **/
                String jsn = HttpRequestUtils.get("https://api.weixin.qq.com/sns/userinfo?access_token="
                                + wxAppAuthInfo.getAccessToken() + "&openid=" + wxAppAuthInfo.getOpenid(), null, null);
                Map<String, Object> usermap = JacksonUtils.jsn2map(jsn, String.class, Object.class);
                String openid = MapUtils.getString(usermap, "openid");
                String nickname = MapUtils.getString(usermap, "nickname");
                String sex = MapUtils.getString(usermap, "sex");
                String province = MapUtils.getString(usermap, "province");
                String city = MapUtils.getString(usermap, "city");
                String country = MapUtils.getString(usermap, "country");
                String headimgurl = MapUtils.getString(usermap, "headimgurl");
                String unionid = MapUtils.getString(usermap, "unionid");
                nickname = EmojiParser.parseToAliases(nickname);
                logger.info("微信数据获取 ：" + jsn);
                user.setWeixinOpenid(openid);
                user.setAvatar(headimgurl);
                user.setNickname(nickname);
                user.setGender(sex);
                user.setUnionid(unionid);
                user.setProvince(province);
                user.setCity(city);
                user.setCountry(country);

            }
            /** 微信公众号授权登录 **/
            else if (MallConstant.REGISTER_TYPE_WXMP.equalsIgnoreCase(registerChannel)
                            && !StringUtils.isEmpty(wxMpCode)) {
                // WxMpOAuth2AccessToken wxMpOAuth2AccessToken = wxMpService.oauth2getAccessToken(wxMpCode);
                // wxMpUser = wxMpService.oauth2getUserInfo(wxMpOAuth2AccessToken, null);

                // 授权方法改写wx-java.version_3.9.0
                // WxMpOAuth2AccessToken wxMpOAuth2AccessToken =
                // wxMpService.getOAuth2Service().getAccessToken(wxMpCode);

                // 授权方法改写wx-java.version_4.0.0
                WxOAuth2AccessToken wxMpOAuth2AccessToken = wxMpService.getOAuth2Service().getAccessToken(wxMpCode);
                wxMpUser = wxMpService.getOAuth2Service().getUserInfo(wxMpOAuth2AccessToken, null);
                logger.info("微信数据获取 ：" + JacksonUtils.bean2Jsn(wxMpUser));
                // sessionKey = result.getSessionKey();
                openId = wxMpUser.getOpenid();// getOpenId();
                if (openId == null) {
                    return ResponseUtil.fail(-1, "微信登录信息获取异常");
                }

                user.setWeixinOpenid(openId);
                user.setAvatar(wxMpUser.getHeadImgUrl());
                user.setNickname(wxMpUser.getNickname());
                user.setGender(wxMpUser.getSex().toString());
                user.setProvince(wxMpUser.getProvince());
                user.setCity(wxMpUser.getCity());
                user.setCountry(wxMpUser.getCountry());
            }
            /** 淘宝授权登录 **/
            else if (MallConstant.REGISTER_TYPE_TAOBAO.equalsIgnoreCase(registerChannel)) {
                TaoBaoBaiChuanUserInfo taoBaoBaiChuanUserInfo = registerInfo.getTaoBaoBaiChuanUserInfo();
                String url = "https://eco.taobao.com/router/rest";// ( 获取Access Token )
                TaobaoClient client = new DefaultTaobaoClient(url, taoBaoProperties.getLianmengAppKey(),
                                taoBaoProperties.getLianmengAppSecret());
                TopAuthTokenCreateRequest req = new TopAuthTokenCreateRequest();
                req.setCode(registerInfo.getWxCode());
                TopAuthTokenCreateResponse rsp = client.execute(req);

                logger.info("调用淘宝AccessToken接口： " + JacksonUtils.bean2Jsn(rsp));

                // TopAuthToken topAuthToken = JacksonUtils.jsn2beanUnderscores(rsp.getBody(), TopAuthToken.class);
                String tokenResult = rsp.getTokenResult();
                if (tokenResult == null) {
                    return ResponseUtil.fail();
                }

                Map<String, String> tokenResultMap = JacksonUtils.jsn2map(tokenResult, String.class, String.class);
                String accessToken = MapUtils.getString(tokenResultMap, "access_token");

                if (accessToken != null) {
                    // 渠道备案获取渠道id或会员id(写定是邀请渠道模式)
                    user.setTbkAccessToken(accessToken);
                    String saveurl = "https://eco.taobao.com/router/rest";
                    TaobaoClient client1 = new DefaultTaobaoClient(saveurl, taoBaoProperties.getLianmengAppKey(),
                                    taoBaoProperties.getLianmengAppSecret());
                    TbkScPublisherInfoSaveRequest req1 = new TbkScPublisherInfoSaveRequest();
                    req1.setRelationFrom("1");
                    req1.setOfflineScene("4");
                    req1.setOnlineScene("3");
                    req1.setInviterCode("MTCQ32");
                    req1.setInfoType(1L);
                    req1.setNote(taoBaoBaiChuanUserInfo.getNick());
                    TbkScPublisherInfoSaveResponse rspSave = client1.execute(req1, accessToken);
                    Data data = rspSave.getData();
                    logger.info("淘宝联盟备案接口返回： " + JacksonUtils.bean2Jsn(rspSave));
                    Long relationId = data.getRelationId();
                    Long specialId = data.getSpecialId();
                    if (relationId != null) {
                        user.setTbkRelationId(relationId.toString());
                    }
                    if (specialId != null) {
                        user.setTbkSpecialId(specialId.toString());
                    }

                    user.setAvatar(taoBaoBaiChuanUserInfo.getAvatar());
                    user.setNickname(taoBaoBaiChuanUserInfo.getNick());
                    user.setTbkPubId("mm_131454905_1564300091_110282750499");

                }
            }
            /** 自定义登录模式 **/
            else if (MallConstant.REGISTER_TYPE_CUSTOM.equals(registerChannel)) {
                if (!StringUtils.isEmpty(password)) {
                    BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
                    String encodedPassword = encoder.encode(password);
                    user.setPassword(encodedPassword);
                }
                user.setNickname(username);
                user.setMobile(registerInfo.getPhone());
                user.setProvince(registerInfo.getProvince());
                user.setCity(registerInfo.getCity());


            }
            /** 自定义登录模式附带邀请码模式 ----- 一般用于分销模式下 **/
            else if (MallConstant.REGISTER_TYPE_INVITATION.equals(registerChannel)) {
                // 上级信息
                List<MallUser> parentUserList = userService.queryByInvitationCode(invitationCode);
                if (CollectionUtils.isEmpty(parentUserList)) {
                    return ResponseUtil.fail(AUTH_NAME_REGISTERED, "邀请码不存在");
                }
                // 记录推荐码
                String parentId = parentUserList.get(0).getId();
                // 记录上下级关系
                user.setFirstLeader(parentId.toString());
            }

            user.setIsDistribut("1");
            user.setUserLevel((byte) 0);
            user.setStatus((byte) 0);
            user.setLastLoginTime(LocalDateTime.now());
            user.setLastLoginIp(IpUtil.getIpAddr(request));
            user.setSessionKey(sessionKey);
            user.setLevelId(AccountStatus.LEVEL_0);
            System.out.println("openid-----" + user.getWeixinOpenid());
            List<MallUser> mallUserList = userService.queryByOpenid(user.getWeixinOpenid());
            if (mallUserList.size() > 0) {
                user = mallUserList.get(0);
            } else {
                userService.add(user);
            }



            /** 先判断是否开户 **/
            if (MallConstant.OPEN_ACCOUNT_YES.equals(registerInfo.getOpenAccount()) && user.getId() != null) {
                FcAccount fc = accountService.accountTradeCreateAccount(user.getId().toString());
                logger.info("开户成功..............");
            }

            /** 生成token返回给前端 **/
            String token = UserTokenManager.generateToken(user.getId(), user.getTbkRelationId(),
                            user.getTbkSpecialId());
            Map<Object, Object> result = new HashMap<Object, Object>();
            if (!StringUtils.isEmpty(user.getNickname())) {
                user.setNickname(EmojiParser.parseToUnicode(user.getNickname()));
            }
            result.put("token", token);
            result.put("userInfo", user);
            return ResponseUtil.ok(result);
        } catch (Exception e) {
            logger.error("分销商注册异常: " + e.getMessage(), e);
        }

        return ResponseUtil.fail();
    }

    /**
     * 通过分享链接注册
     * 
     * @param registerInfo
     * @param request
     * @return
     */
    @PostMapping("authByShare")
    public Object authByShare(@RequestBody RegisterInfo registerInfo, HttpServletRequest request) {
        /** 注册渠道 **/
        String registerChannel = registerInfo.getRegisterChannel();
        /** 推荐人id **/
        String firstLeaderId = registerInfo.getFirstLeaderId();
        /** 渠道商id **/
        String channelId = registerInfo.getChannelId();
        if (StringUtils.isEmpty(firstLeaderId) && StringUtils.isEmpty(channelId)) {
            return ResponseUtil.badArgumentValue("未获取到推荐人id或渠道商id！");
        }

        try {
            MallUser user = new MallUser();
            MallAddress address = new MallAddress();
            String sessionKey = "";

            /** 自定义登录模式 **/
            if (MallConstant.REGISTER_TYPE_CUSTOM.equals(registerChannel)) {
                /** 校验参数 **/
                Object obj = needValidationByShare(registerInfo);
                if (obj != null) {
                    return obj;
                }
                // 设备类型
                String mobileTye = request.getHeader("User-Agent");
                Byte registerDevice = null;
                if (mobileTye.contains("iPhone")) {
                    registerDevice = (byte) 2;
                } else if (mobileTye.contains("Android")) {
                    registerDevice = (byte) 1;
                }
                /**
                 * 判断手机号是否已经注册： 1.若没有注册，则先注册用户，然后创建订单 2.如果已经注册，则直接创建订单
                 **/
                List<MallUser> userList = userService.queryByMobile(registerInfo.getPhone());
                if (userList.size() > 0) {
                    // 若用户存在,返回修改之后的用户信息
                    user = userList.get(0);
                    // 修改用户信息
                    // user.setNickname(registerInfo.getUsername());
                    // user.setProvince(registerInfo.getProvince());
                    // user.setCity(registerInfo.getCity());
                    // user.setLastLoginTime(LocalDateTime.now());
                    // user.setLastLoginIp(IpUtil.getIpAddr(request));
                    // user.setSessionKey(sessionKey);
                    // user.setWeixinMpOpenid(registerInfo.getWeixinMpOpenid()); //用户微信公众号授权openId
                    // userService.updateById(user);

                    // address = mallAddressService.findDefault(user.getId());
                } else {
                    // 创建用户
                    user.setNickname(registerInfo.getUsername());
                    user.setMobile(registerInfo.getPhone());
                    user.setProvince(registerInfo.getProvince());
                    user.setCity(registerInfo.getCity());
                    user.setIsDistribut("1");
                    user.setLevelId(AccountStatus.LEVEL_0);
                    user.setStatus((byte) 0);
                    user.setLastLoginTime(LocalDateTime.now());
                    user.setLastLoginIp(IpUtil.getIpAddr(request));
                    user.setSessionKey(sessionKey);
                    user.setWeixinMpOpenid(registerInfo.getWeixinMpOpenid()); // 用户微信公众号授权openId

                    /** 通过分享链接注册, firstLeaderId分享人 id **/
                    if (!StringUtils.isEmpty(firstLeaderId)) {
                        MallUser firstUser = userService.findById(firstLeaderId); // 推荐人
                        if (null != firstUser) {
                            user.setDirectLeader(firstLeaderId);
                            //set绑定直推人时间
                            user.setLockTime(LocalDateTime.now());
                            if (firstUser.getLevelId().equals(AccountStatus.LEVEL_0)
                                            || firstUser.getLevelId().equals(AccountStatus.LEVEL_1)) {
                                // 普通用户、会员 直接绑定直推人的三个上级
                                user.setFirstLeader(firstUser.getFirstLeader());
                                user.setSecondLeader(firstUser.getSecondLeader());
                                user.setThirdLeader(firstUser.getThirdLeader());
                            } else if (firstUser.getLevelId().equals(AccountStatus.LEVEL_4)) {
                                // 服务商
                                user.setFirstLeader(firstUser.getId());
                                user.setSecondLeader(firstUser.getSecondLeader());
                                user.setThirdLeader(firstUser.getThirdLeader());
                            } else if (firstUser.getLevelId().equals(AccountStatus.LEVEL_5)) {
                                // 区代
                                user.setFirstLeader(firstUser.getFirstLeader());
                                user.setSecondLeader(firstUser.getId());
                                user.setThirdLeader(firstUser.getThirdLeader());
                            } else if (firstUser.getLevelId().equals(AccountStatus.LEVEL_6)) {
                                // 市代
                                user.setFirstLeader(firstUser.getFirstLeader());
                                user.setSecondLeader(firstUser.getSecondLeader());
                                user.setThirdLeader(firstUser.getId());
                            }

                        } else {
                            return ResponseUtil.badArgumentValue("邀请用户不存在！");
                        }

                    }
                    /** 通过渠道商url注册 **/
                    if (!StringUtils.isEmpty(channelId)) {
                        user.setChannelId(channelId);
                    }
                    user.setRegisterDevice(registerDevice); // 注册设备
                    userService.add(user);
                    // 开户
                    if (user.getId() != null) {
                        logger.info("开户成功..............");
                        accountService.accountTradeCreateAccount(user.getId().toString());
                    }
                    // 消息推送
                    PushUtils.pushMessage(user.getId(), AccountStatus.PUSH_TITLE_REGISTER,
                                    AccountStatus.PUSH_CONTENT_REGISTER, 2, null);
                }


                // 添加用户地址信息
                if (!StringUtils.isEmpty(registerInfo.getDetail())) {
                    address.setUserId(user.getId());
                    address.setName(registerInfo.getUsername());
                    address.setProvince(registerInfo.getProvince());
                    address.setCity(registerInfo.getCity());
                    address.setCounty(registerInfo.getDistrict());
                    address.setAddressDetail(registerInfo.getDetail());
                    address.setTel(registerInfo.getPhone());
                    address.setIsDefault(true);
                    mallAddressService.add(address);
                }
                // 创建订单参数
                Map<String, Object> paramMap = new HashMap<>();
                paramMap.put("addressId", address.getId());
                paramMap.put("submitClient", "Customer");
                paramMap.put("message", "用户购买骑士卡");
                paramMap.put("tpid", firstLeaderId);
                paramMap.put("channelId", channelId);
                // 参数
                String token = UserTokenManager.generateToken(user.getId(), user.getTbkRelationId(),
                                user.getTbkSpecialId());
                Map<String, Object> map = new HashMap<>();
                map.put("token", token); // 用户token
                map.put("userInfo", user); // 用户信息
                map.put("addressInfo", address); // 用户地址信息
                map.put("paramMap", paramMap); // 创建订单参数信息
                Map<String, String> cardPriceMap = mallSystemConfigService.listCard();// 查询购卡价格
                map.put("cardPriceMap", cardPriceMap);

                // /**生成购卡订单**/
                // String body = JacksonUtils.bean2Jsn(map); //封装请求参数
                // Object orderInfo = wxOrderService.buyCardSubmit(user.getId(), body, IpUtil.getIpAddr(request));
                return ResponseUtil.ok(map);
            }


        } catch (Exception e) {
            logger.error("用户注册异常: " + e.getMessage(), e);
        }
        return ResponseUtil.fail();
    }

    /**
     * 创客推创客
     *
     * @param registerInfo
     * @param request
     * @return
     */
    @PostMapping("authByMaker")
    public Object authByMaker(@RequestBody RegisterInfo registerInfo, HttpServletRequest request) {
        /** 推荐人id **/
//        String firstLeaderId = registerInfo.getFirstLeaderId();
        if (StringUtils.isEmpty(registerInfo.getUsername()) ) {
            return ResponseUtil.badArgumentValue("用户名不能为空！");
        }
//        if (StringUtils.isEmpty(firstLeaderId) ) {
//            return ResponseUtil.badArgumentValue("未获取到推荐人id！");
//        }
        if (StringUtils.isEmpty(registerInfo.getPhone())) {
            return ResponseUtil.badArgumentValue("手机号码不能为空！");
        }
        try {
              MallUser user = new MallUser();
              MallAddress address = new MallAddress();
                /** 校验参数 **/
                Object obj = needValidation1(registerInfo);
                if (obj != null) {
                    return obj;
                }

                // 设备类型
                String mobileTye = request.getHeader("User-Agent");
                Byte registerDevice = null;
                if (mobileTye.contains("iPhone")) {
                    registerDevice = (byte) 2;
                } else if (mobileTye.contains("Android")) {
                    registerDevice = (byte) 1;
                }
                /**
                 * 判断手机号是否已经注册： 1.若没有注册，则先注册用户，然后创建订单 2.如果已经注册，则直接创建订单
                 **/
                List<MallUser> userList = userService.queryByMobile(registerInfo.getPhone());
                if (userList.size() > 0) {
                    // 若用户存在,返回修改之后的用户信息
                    user = userList.get(0);
                } else {
                    // 创建用户
                    user.setMobile(registerInfo.getPhone());
                    user.setIsDistribut("1");
                    user.setLevelId(AccountStatus.LEVEL_0);
                    user.setStatus((byte) 0);
                    user.setLastLoginTime(LocalDateTime.now());
                    user.setLastLoginIp(IpUtil.getIpAddr(request));
                    user.setRegisterDevice(registerDevice); // 注册设备
                    user.setSource("创客分享注册");
                    userService.add(user);
                    // 开户
                    if (user.getId() != null) {
                        logger.info("开户成功..............");
                        accountService.accountTradeCreateAccount(user.getId().toString());
                    }

                }
                // 添加用户地址信息
                if (!StringUtils.isEmpty(registerInfo.getDetail())) {
                    address.setUserId(user.getId());
                    address.setName(registerInfo.getUsername());
                    address.setProvince(registerInfo.getProvince());
                    address.setCity(registerInfo.getCity());
                    address.setCounty(registerInfo.getDistrict());
                    address.setAddressDetail(registerInfo.getDetail());
                    address.setTel(registerInfo.getPhone());
//                    address.setIsDefault(true);
                    mallAddressService.add(address);
                }
                // 参数
                String token = UserTokenManager.generateToken(user.getId(), user.getTbkRelationId(),
                        user.getTbkSpecialId());
                Map<String, Object> map = new HashMap<>();
                map.put("token", token); // 用户token
                map.put("userInfo", user); // 用户信息
                map.put("addressId", address.getId()); // 用户地址信息
                return ResponseUtil.ok(map);

        } catch (Exception e) {
            logger.error("购买创客异常: " + e.getMessage(), e);
        }
        return ResponseUtil.fail();
    }

    /**
     * 需要校验的参数
     *
     * @param registerInfo
     * @return
     */
    private Object needValidation1(RegisterInfo registerInfo) {
        String username = registerInfo.getUsername();
        String valCode = registerInfo.getValCode();
        String smsType = "APP_REGISTER";
        String mobile = registerInfo.getPhone();

            /** 需要校验用户注册参数校验 **/
            if (StringUtils.isEmpty(username)) {
                return ResponseUtil.badArgument();
            }
            MallPhoneValidation mallPhoneValidation = phoneService.getPhoneValidationByCode(mobile, valCode, smsType);
            if (mallPhoneValidation == null) {
                return ResponseUtil.fail(AUTH_NAME_REGISTERED, "验证码有误");
            }
            List<MallUser> userList = userService.queryByUsername(username);

            userList = userService.queryByMobile(registerInfo.getPhone());
            if (userList.size() > 0) {
                if(ObjectUtil.equals("1",userList.get(0).getIsMaker())) {
                    return ResponseUtil.fail(AUTH_MOBILE_REGISTERED, "已经购买创客，请勿重复购买！");
                }
            }

            if (!RegexUtil.isMobileExact(registerInfo.getPhone())) {
                return ResponseUtil.fail(AUTH_INVALID_MOBILE, "手机号格式不正确");
            }
            /** 修改手机验证码状态 **/
            MallPhoneValidation updateVal = new MallPhoneValidation();
            updateVal.setId(mallPhoneValidation.getId());
            updateVal.setValState("00");
            phoneService.update(updateVal);

            return null;

    }

    /**
     * 需要校验的参数
     * 
     * @param registerInfo
     * @return
     */
    private Object needValidation(RegisterInfo registerInfo) {
        String username = registerInfo.getPhone();
        String password = registerInfo.getPassword();
        String valCode = registerInfo.getValCode();
        String smsType = "APP_REGISTER";

        if (MallConstant.REGISTER_TYPE_CUSTOM.equalsIgnoreCase(registerInfo.getRegisterChannel())) {
            /** 需要校验用户注册参数校验 **/
            if (StringUtils.isEmpty(username)) {
                return ResponseUtil.badArgument();
            }
            List<MallUser> userList = userService.queryByUsername(username);
            if (userList.size() > 0) {
                return ResponseUtil.fail(AUTH_NAME_REGISTERED, "用户名已注册");
            }

            userList = userService.queryByMobile(registerInfo.getPhone());
            if (userList.size() > 0) {
                return ResponseUtil.fail(AUTH_MOBILE_REGISTERED, "手机号已注册");
            }

            if (!RegexUtil.isMobileExact(registerInfo.getPhone())) {
                return ResponseUtil.fail(AUTH_INVALID_MOBILE, "手机号格式不正确");
            }

        } else {
            /** 需要校验用户注册参数校验 **/
            if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password) || StringUtils.isEmpty(valCode)) {
                return ResponseUtil.badArgument();
            }

            MallPhoneValidation mallPhoneValidation = phoneService.getPhoneValidationByCode(username, valCode, smsType);
            if (mallPhoneValidation == null) {
                return ResponseUtil.fail(AUTH_NAME_REGISTERED, "验证码有误");
            }

            List<MallUser> userList = userService.queryByUsername(username);
            if (userList.size() > 0) {
                return ResponseUtil.fail(AUTH_NAME_REGISTERED, "用户名已注册");
            }

            userList = userService.queryByMobile(username);
            if (userList.size() > 0) {
                return ResponseUtil.fail(AUTH_MOBILE_REGISTERED, "手机号已注册");
            }

            if (!RegexUtil.isMobileExact(username)) {
                return ResponseUtil.fail(AUTH_INVALID_MOBILE, "手机号格式不正确");
            }

            /** 修改手机验证码状态 **/
            MallPhoneValidation updateVal = new MallPhoneValidation();
            updateVal.setId(mallPhoneValidation.getId());
            updateVal.setValState("00");
            phoneService.update(updateVal);

            return null;

        }
        return null;
    }

    /**
     * 分享链接注册需要校验非空的参数
     */
    private Object needValidationByShare(RegisterInfo registerInfo) {

        if (StringUtils.isEmpty(registerInfo.getUsername())) {
            return ResponseUtil.badArgumentValue("用户名不能为空！");
        }
        if (StringUtils.isEmpty(registerInfo.getPhone())) {
            return ResponseUtil.badArgumentValue("手机号码不能为空！");
        }
        if (StringUtils.isEmpty(registerInfo.getRephone())) {
            return ResponseUtil.badArgumentValue("确认手机号码不能为空！");
        }
        if (!registerInfo.getPhone().equals(registerInfo.getRephone())) {
            return ResponseUtil.badArgumentValue("输入的手机号码不一致！");
        }
        if (!RegexUtil.isMobileExact(registerInfo.getPhone())) {
            return ResponseUtil.fail(AUTH_INVALID_MOBILE, "手机号格式不正确");
        }
        if (StringUtils.isEmpty(registerInfo.getProvince()) || StringUtils.isEmpty(registerInfo.getCity())
                        || StringUtils.isEmpty(registerInfo.getDistrict())) {
            return ResponseUtil.badArgumentValue("收件地址不能为空！");
        }
        if (StringUtils.isEmpty(registerInfo.getDetail())) {
            return ResponseUtil.badArgumentValue("详细地址不能为空！");
        }
        if (registerInfo.getDetail().length() < 5) {
            return ResponseUtil.badArgumentValue("详细地址不少于5个字！");
        }
        return null;
    }


}
