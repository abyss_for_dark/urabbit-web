package com.graphai.mall.wx.dto;

import com.graphai.mall.admin.domain.MallUserProfitNew;
import com.graphai.mall.db.domain.*;
import lombok.Data;

import java.util.List;

@Data
public class GoodsAllinone {
    MallGoods goods;
    MallGoodsSpecification[] specifications;
    MallGoodsAttribute[] attributes;
    MallGoodsProduct[] products;
    List<String> categoryIds;

    MallCoupon coupon;
    MallGrouponRules grouponRules;
    MallUserProfitNew userProfitNew;
//    List<String> placement;
    int [] placement ;

    public int[] getPlacement() {
        return placement;
    }

    public void setPlacement(int[] placement) {
        this.placement = placement;
    }

    public MallGrouponRules getGrouponRules() {
        return grouponRules;
    }

    public void setGrouponRules(MallGrouponRules grouponRules) {
        this.grouponRules = grouponRules;
    }

    public MallUserProfitNew getUserProfitNew() {
        return userProfitNew;
    }

    public void setUserProfitNew(MallUserProfitNew userProfitNew) {
        this.userProfitNew = userProfitNew;
    }

    public MallGoods getGoods() {
        return goods;
    }

    public void setGoods(MallGoods goods) {
        this.goods = goods;
    }

    public MallGoodsProduct[] getProducts() {
        return products;
    }

    public void setProducts(MallGoodsProduct[] products) {
        this.products = products;
    }

    public MallGoodsSpecification[] getSpecifications() {
        return specifications;
    }

    public void setSpecifications(MallGoodsSpecification[] specifications) {
        this.specifications = specifications;
    }

    public MallGoodsAttribute[] getAttributes() {
        return attributes;
    }

    public void setAttributes(MallGoodsAttribute[] attributes) {
        this.attributes = attributes;
    }

    public MallCoupon getCoupon() {
        return coupon;
    }

    public void setCoupon(MallCoupon coupon) {
        this.coupon = coupon;
    }
}
