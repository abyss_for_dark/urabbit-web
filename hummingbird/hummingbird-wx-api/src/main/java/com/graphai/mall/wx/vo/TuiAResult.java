package com.graphai.mall.wx.vo;

import lombok.Data;

/**
 * @Author: jw
 * @Date: 2021/1/30 16:05
 */
 @Data
public class TuiAResult {
   private String code;
   private String desc;
   private String success;
   private TuiAResultParams data;

}
