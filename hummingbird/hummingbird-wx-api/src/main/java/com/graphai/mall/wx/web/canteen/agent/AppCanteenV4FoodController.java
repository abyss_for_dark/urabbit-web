package com.graphai.mall.wx.web.canteen.agent;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.id.GraphaiIdGenerator;
import com.graphai.mall.admin.domain.MallUserMerchant;
import com.graphai.mall.admin.service.IMallUserMerchantService;
import com.graphai.mall.canteen.domain.KndCanteenCategory;
import com.graphai.mall.canteen.domain.KndCanteenFood;
import com.graphai.mall.canteen.domain.KndCanteenFoodProduct;
import com.graphai.mall.canteen.domain.KndCanteenFoodSpecification;
import com.graphai.mall.canteen.form.AmKndCanteenFoodFrom;
import com.graphai.mall.canteen.service.IKndCanteenCategoryService;
import com.graphai.mall.canteen.service.IKndCanteenFoodProductService;
import com.graphai.mall.canteen.service.IKndCanteenFoodService;
import com.graphai.mall.canteen.service.IKndCanteenFoodSpecificationService;
import com.graphai.mall.wx.annotation.LoginUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * 餐饮菜品Controller
 *
 * @author author
 * @date 2020-03-26
 */
@RestController
@RequestMapping("/wx/b/canteenFood")
public class AppCanteenV4FoodController {

    @Autowired
    private IKndCanteenFoodService kndCanteenFoodService;
    @Autowired
    private IKndCanteenCategoryService kndCanteenCategoryService;
    @Autowired
    private IKndCanteenFoodSpecificationService kndCanteenFoodSpecificationService;
    @Autowired
    private IKndCanteenFoodProductService kndCanteenFoodProductService;

    @Autowired
    private IMallUserMerchantService userMerchantService;

    /**
     * 查询餐饮菜品列表
     */
    @CrossOrigin
    @PostMapping("/list")
    @ResponseBody
    public Object list(@LoginUser String userId,KndCanteenFood kndCanteenFood) {
        //通过userId获取商户信息
        MallUserMerchant userMerchant = userMerchantService.getOne(new QueryWrapper<MallUserMerchant>()
                .lambda().eq(MallUserMerchant::getUserId, userId));
        if (Objects.isNull(userMerchant)) {
            return ResponseUtil.badArgumentValue("商户信息不存在");
        }
        kndCanteenFood.setMerchantId(userMerchant.getMerchantId());
        List<KndCanteenFood> list = kndCanteenFoodService.selectKndCanteenFoodList(kndCanteenFood);
        return ResponseUtil.ok(list);
    }

    /**
     * 新增保存餐饮菜品
     */
    @CrossOrigin
    @PostMapping("/add")
    @ResponseBody
    public Object addSave(@LoginUser String userId,@RequestBody AmKndCanteenFoodFrom amKndCanteenFoodFrom, String storeId) {

        //通过userId获取商户信息
        MallUserMerchant userMerchant = userMerchantService.getOne(new QueryWrapper<MallUserMerchant>()
                .lambda().eq(MallUserMerchant::getUserId, userId));
        if (Objects.isNull(userMerchant)) {
            return ResponseUtil.badArgumentValue("商户信息不存在");
        }

        KndCanteenFood kndCanteenFood = amKndCanteenFoodFrom.getKndCanteenFood();

        KndCanteenFood food = new KndCanteenFood();
        food.setMerchantId(userMerchant.getMerchantId());
        food.setStoreId(storeId);
        food.setFoodName(kndCanteenFood.getFoodName());
        List<KndCanteenFood> list = kndCanteenFoodService.getKndCanteenFoodList(food);
        if (null != list && !list.isEmpty()) {
            return ResponseUtil.fail("添加失败，已存在相同菜品名！");
        }
        kndCanteenFood.setMerchantId(userMerchant.getMerchantId());
        kndCanteenFood.setStoreId(storeId);
        kndCanteenFood.setId(GraphaiIdGenerator.nextId("kndCanteenFood"));
        //判断原价是否为空
        if (kndCanteenFood.getCounterPrice() == null) {
            kndCanteenFood.setCounterPrice(kndCanteenFood.getRetailPrice());
        }


        List<KndCanteenFoodSpecification> foodSpecificationList = amKndCanteenFoodFrom.getFoodSpecificationList();
        List<KndCanteenFoodProduct> foodProductList = amKndCanteenFoodFrom.getFoodProductList();
        if (ObjectUtil.isNotEmpty(foodSpecificationList) && foodSpecificationList.size() > 0) {
            if (ObjectUtil.isEmpty(foodProductList) || foodProductList.size() == 0) {
                return ResponseUtil.fail("餐饮产品规格组合错误！");
            }
            //todo 添加产品规格表
            for (int i = 0; i < foodSpecificationList.size(); i++) {
//                KndCanteenFoodSpecification kndCanteenFoodSpecification = new KndCanteenFoodSpecification();
                KndCanteenFoodSpecification kndCanteenFoodSpecification = foodSpecificationList.get(i);
                kndCanteenFoodSpecification.setFoodId(kndCanteenFood.getId());
                kndCanteenFoodSpecification.setMerchantId(userMerchant.getMerchantId());
                kndCanteenFoodSpecification.setStoreId(storeId);
                kndCanteenFoodSpecificationService.insertKndCanteenFoodSpecification(kndCanteenFoodSpecification);
            }

            //todo 添加产品表
            for (int i = 0; i < foodProductList.size(); i++) {
//                KndCanteenFoodProduct kndCanteenFoodProduct = new KndCanteenFoodProduct();
                KndCanteenFoodProduct kndCanteenFoodProduct = foodProductList.get(i);
                kndCanteenFoodProduct.setFoodId(kndCanteenFood.getId());
                kndCanteenFoodProduct.setMerchantId(userMerchant.getMerchantId());
                kndCanteenFoodProduct.setStoreId(storeId);
                kndCanteenFoodProduct.setUrl(kndCanteenFood.getPicUrl());
                kndCanteenFoodProductService.insertKndCanteenFoodProduct(kndCanteenFoodProduct);
            }
        } else {
            //若没有填写 产品规格，则添加默认菜品规格(空) 至 产品表
            KndCanteenFoodProduct kndCanteenFoodProduct = new KndCanteenFoodProduct();
            kndCanteenFoodProduct.setFoodId(kndCanteenFood.getId());
            kndCanteenFoodProduct.setMerchantId(userMerchant.getMerchantId());
            kndCanteenFoodProduct.setStoreId(storeId);
            kndCanteenFoodProduct.setPrice(kndCanteenFood.getRetailPrice());
            kndCanteenFoodProduct.setUrl(kndCanteenFood.getPicUrl());
            kndCanteenFoodProduct.setNumber(amKndCanteenFoodFrom.getDefaultProductNum());
            kndCanteenFoodProductService.insertKndCanteenFoodProduct(kndCanteenFoodProduct);
        }
        return kndCanteenFoodService.insertKndCanteenFood(kndCanteenFood) > 0 ? ResponseUtil.ok() : ResponseUtil.fail();
    }

    /**
     * 修改餐饮菜品
     */
    @CrossOrigin
    @GetMapping("/edit/{id}")
    @ResponseBody
    public Object edit(@LoginUser String userId,@PathVariable("id") String id) {
        KndCanteenFood kndCanteenFood = kndCanteenFoodService.selectKndCanteenFoodById(id);

        //查询产品规格
        KndCanteenFoodSpecification kndCanteenFoodSpecification = new KndCanteenFoodSpecification();
        kndCanteenFoodSpecification.setFoodId(id);
        kndCanteenFoodSpecification.setDeleted(0);
        List<KndCanteenFoodSpecification> foodSpecificationList = kndCanteenFoodSpecificationService.selectKndCanteenFoodSpecificationList(kndCanteenFoodSpecification);
        //查询产品
        KndCanteenFoodProduct kndCanteenFoodProduct = new KndCanteenFoodProduct();
        kndCanteenFoodProduct.setFoodId(id);
        kndCanteenFoodProduct.setDeleted(0);
        List<KndCanteenFoodProduct> foodProductList = kndCanteenFoodProductService.selectKndCanteenFoodProductList(kndCanteenFoodProduct);

        Map map = new HashMap();
        map.put("kndCanteenFood", kndCanteenFood);
        map.put("foodSpecificationList", foodSpecificationList);
        map.put("foodProductList", foodProductList);
        return ResponseUtil.ok(map);
    }

    /**
     * 修改保存餐饮菜品
     */
    @Transactional
    @PostMapping("/edit")
    public Object editSave(@LoginUser String userId, @RequestBody AmKndCanteenFoodFrom amKndCanteenFoodFrom) {

        //通过userId获取商户信息
        MallUserMerchant userMerchant = userMerchantService.getOne(new QueryWrapper<MallUserMerchant>()
                .lambda().eq(MallUserMerchant::getUserId, userId));
        if (Objects.isNull(userMerchant)) {
            return ResponseUtil.badArgumentValue("商户信息不存在");
        }

        KndCanteenFood kndCanteenFood = amKndCanteenFoodFrom.getKndCanteenFood();

        KndCanteenFood food = new KndCanteenFood();
        food.setMerchantId(userMerchant.getMerchantId());
        food.setFoodName(kndCanteenFood.getFoodName());
        List<KndCanteenFood> list = kndCanteenFoodService.getKndCanteenFoodList(food);
        if (null != list && list.size() > 1) {
            return ResponseUtil.fail("修改失败，已存在相同菜品名！");
        }
        //判断原价是否为空
        if (kndCanteenFood.getCounterPrice() == null) {
            kndCanteenFood.setCounterPrice(kndCanteenFood.getRetailPrice());
        }

        KndCanteenFoodSpecification foodSpecification = new KndCanteenFoodSpecification();
        foodSpecification.setFoodId(kndCanteenFood.getId());
        List<KndCanteenFoodSpecification> existSpecs = kndCanteenFoodSpecificationService.selectKndCanteenFoodSpecificationList(foodSpecification);
        List<KndCanteenFoodSpecification> updateSpecs = amKndCanteenFoodFrom.getFoodSpecificationList();

        for (KndCanteenFoodSpecification spec : updateSpecs) {
            if (spec.getId() != null) {
                kndCanteenFoodSpecificationService.updateKndCanteenFoodSpecification(spec);
                for (int i = 0; i < existSpecs.size(); i++) {
                    if (existSpecs.get(i).getId().equals(spec.getId())) {
                        existSpecs.remove(i);
                        break;
                    }
                }
            } else {
                spec.setFoodId(kndCanteenFood.getId());
                spec.setMerchantId(userMerchant.getMerchantId());
                kndCanteenFoodSpecificationService.insertKndCanteenFoodSpecification(spec);
            }
        }

        KndCanteenFoodProduct foodProduct = new KndCanteenFoodProduct();
        foodProduct.setFoodId(kndCanteenFood.getId());
        List<KndCanteenFoodProduct> existProducts = kndCanteenFoodProductService.selectKndCanteenFoodProductList(foodProduct);
        List<KndCanteenFoodProduct> updateProducts = amKndCanteenFoodFrom.getFoodProductList();
        // 默认规格
        if (updateProducts.isEmpty()) {
            KndCanteenFoodProduct defProduct = new KndCanteenFoodProduct();
            defProduct.setFoodId(kndCanteenFood.getId());
            defProduct.setMerchantId(userMerchant.getMerchantId());
            defProduct.setPrice(kndCanteenFood.getRetailPrice());
            defProduct.setUrl(kndCanteenFood.getPicUrl());
            updateProducts.add(defProduct);
        }

        for (KndCanteenFoodProduct product : updateProducts) {
            if (product.getId() != null) {
                if(ObjectUtil.isEmpty(product.getSpecifications()) || ObjectUtil.isNull(product.getSpecifications())){
                    product.setPrice(kndCanteenFood.getRetailPrice());
                }

                if(ObjectUtil.isNull(product.getNumber())){
                    product.setNumber(-1);
                }

                kndCanteenFoodProductService.updateKndCanteenFoodProduct(product);
                for (int i = 0; i < existProducts.size(); i++) {
                    if (existProducts.get(i).getId().equals(product.getId())) {
                        existProducts.remove(i);
                        break;
                    }
                }
            } else {
                product.setFoodId(kndCanteenFood.getId());
                product.setMerchantId(userMerchant.getMerchantId());
                product.setUrl(kndCanteenFood.getPicUrl());
                kndCanteenFoodProductService.insertKndCanteenFoodProduct(product);
            }
        }

        if (!existSpecs.isEmpty()) {
            for (KndCanteenFoodSpecification spec : existSpecs) {
                spec.setDeleted(1);
                kndCanteenFoodSpecificationService.updateKndCanteenFoodSpecification(spec);
            }
        }
        if (!existProducts.isEmpty()) {
            for (KndCanteenFoodProduct product : existProducts) {
                product.setDeleted(1);
                kndCanteenFoodProductService.updateKndCanteenFoodProduct(product);
            }
        }

        return kndCanteenFoodService.updateKndCanteenFood(kndCanteenFood) > 0 ? ResponseUtil.ok() : ResponseUtil.fail();
    }

    /**
     * 删除餐饮菜品
     */
    @CrossOrigin
    @PostMapping("/remove")
    @ResponseBody
    public Object remove(String ids) {
        //删除 产品规格表、产品表数据
        kndCanteenFoodSpecificationService.deleteKndCanteenFoodSpecificationByFoodId(ids);
        kndCanteenFoodProductService.deleteKndCanteenFoodProductByFoodId(ids);
        return kndCanteenFoodService.deleteKndCanteenFoodByIds(ids) > 0 ? ResponseUtil.ok() : ResponseUtil.fail();
    }


    /**
     * 查询菜品分类
     */
    @CrossOrigin
    @PostMapping("/getCategoryList")
    @ResponseBody
    public Object getCategoryList(@LoginUser String userId,
                                  @RequestParam(defaultValue = "1") Integer page,@RequestParam(defaultValue = "17") Integer size) {
        MallUserMerchant userMerchant = userMerchantService.getOne(new QueryWrapper<MallUserMerchant>()
                .lambda().eq(MallUserMerchant::getUserId, userId));
        if (Objects.isNull(userMerchant)) {
            return ResponseUtil.badArgumentValue("商户信息不存在");
        }
        KndCanteenCategory kndCanteenCategory = new KndCanteenCategory();
        kndCanteenCategory.setMerchantId(userMerchant.getMerchantId());
        return ResponseUtil.ok( kndCanteenCategoryService.selectKndCanteenCategoryList(new Page<>(page,size),kndCanteenCategory));
    }

    /**
     * 修改状态、库存
     */
    @CrossOrigin
    @PostMapping("/editCanteenFood")
    @ResponseBody
    public Object editCanteenFood(@LoginUser String userId,KndCanteenFood kndCanteenFood) {
        return kndCanteenFoodService.updateKndCanteenFood(kndCanteenFood) > 0 ? ResponseUtil.ok():ResponseUtil.fail();
    }
}
