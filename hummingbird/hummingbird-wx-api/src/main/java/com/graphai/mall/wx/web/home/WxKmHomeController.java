package com.graphai.mall.wx.web.home;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.apache.http.message.BasicHeader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.json.JacksonUtils;
import com.graphai.mall.db.domain.BmdesignBusinessForm;
import com.graphai.mall.db.domain.MallGoodsSpecification;
import com.graphai.mall.db.service.bmdesign.BmdesignBusinessFormService;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.mall.wx.web.common.MyBaseController;
import com.graphai.open.api.getway.HttpRequestUtils;
import com.graphai.properties.PlatFormServerproperty;
import com.graphai.validator.Order;

import lombok.extern.slf4j.Slf4j;

/**
 * 首页服务
 */
@RestController
@RequestMapping("/wx/kmhome")
@Validated
@Slf4j
public class WxKmHomeController extends MyBaseController {
	
	@Autowired
    private PlatFormServerproperty platFormServerproperty;
	@Autowired
	private BmdesignBusinessFormService BmdesignBusinessFormServie;

    /**
     * 首页数据
     */
    @GetMapping("/index")
    public Object index(String industryId, @LoginUser String userId) {
        return ResponseUtil.ok();
    }

    private class VO {
        private String name;
        private List<MallGoodsSpecification> valueList;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<MallGoodsSpecification> getValueList() {
            return valueList;
        }

        public void setValueList(List<MallGoodsSpecification> valueList) {
            this.valueList = valueList;
        }
    }


    /**
     * 查询产品并且查询出产品下的具体优惠券数据
     *
     * @param catLevel
     * @param activityId
     * @param categoryId 类目ID
     * @param brandId
     * @param keyword
     * @param isNew
     * @param isHot
     * @param shId
     * @param userId
     * @param page       页数
     * @param limit      页面大小
     * @param sort
     * @param order
     * @param industryId 行业类目
     * @return
     * @throws Exception
     */
    @GetMapping("/list")
    public Object listv3(@RequestParam String categoryId,
                         String brandId,
                         String keyword,
                         @RequestParam(defaultValue = "0") String is_select_category,
                         String name,
                         @LoginUser String userId,
                         @RequestParam(defaultValue = "1") Integer page,
                         @RequestParam(defaultValue = "10") Integer limit,
                         @RequestParam(defaultValue = "0") String sort,
                         @RequestParam(defaultValue = "6") String platform,
                         @RequestParam(defaultValue = "categoryPDD") String position_code,
                         @Order @RequestParam(defaultValue = "desc") String order,
                         @RequestParam(value = "industryId", required = true) String industryId) throws Exception {

        if (org.apache.commons.lang.StringUtils.isBlank(categoryId) || "null".equals(categoryId)) {
            return ResponseUtil.badArgumentValue();
        }

        String sortField = "sortorder";
        if ("0".equals(sort)) {
            sortField = "retail_price ,id ,itemsale";
        } else if ("1".equals(sort)) {
            sortField = "id";
        } else if ("2".equals(sort)) {
            sortField = "itemsale";
        } else if ("3".equals(sort)) {
            sortField = "retail_price";
        }


        //根据大淘客父类目id查询商品列表

        String url = platFormServerproperty.getUrl(industryId);
        Map<String, Object> mappMap = new HashMap<String, Object>();
        mappMap.put("plateform", "pdd");
        mappMap.put("platform", platform);
        mappMap.put("curPage", page);
        mappMap.put("curPageSize", limit);
        mappMap.put("categoryId", categoryId);
        mappMap.put("sort", sortField);
        mappMap.put("order", order);
        mappMap.put("industryId", industryId);
        mappMap.put("position_code", position_code);
        mappMap.put("is_select_category", is_select_category);
        if (StringUtils.isNotBlank(name)) {
            mappMap.put("name", name);
        }

        Header[] headers = new BasicHeader[]{new BasicHeader("method", "search.goods.by.id"), new BasicHeader("version", "v1.0")};
         return  HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);
    }

    @GetMapping("/kmbussinesslist")
    public Object kmBussinessList(@LoginUser String userId) throws Exception {
//        if (userId == null) {
//            return ResponseUtil.unlogin();
//        }
        //根据大淘客父类目id查询商品列表
//        String url = platFormServerproperty.getUrl(MallConstant.PINDUODUO);
//
//        Map<String, Object> mappMap = new HashMap<String, Object>();
//
//        mappMap.put("plateform", "pdd");
//
//        Header[] headers = new BasicHeader[]{new BasicHeader("method", "mall.bussiness.list"), new BasicHeader("version", "v1.0")};
//
//        return HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);
        
        List<BmdesignBusinessForm> businessForms=BmdesignBusinessFormServie.selectBmdesignBusinessFormListByType("BF10002");
        
        return ResponseUtil.ok(businessForms);
        		
        
        
    }

}
