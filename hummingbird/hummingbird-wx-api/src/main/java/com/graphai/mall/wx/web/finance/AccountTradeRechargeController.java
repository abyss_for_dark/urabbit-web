package com.graphai.mall.wx.web.finance;

import java.math.BigDecimal;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.lock.MutexLock;
import com.graphai.commons.util.lock.MutexLockUtils;
import com.graphai.commons.util.json.JacksonUtils;
import com.graphai.mall.db.domain.FcAccount;
import com.graphai.mall.db.domain.FcAccountRechargeBill;
import com.graphai.mall.db.service.account.AccountService;
import com.graphai.mall.db.service.account.CapitalAccountUtils;
import com.graphai.mall.wx.annotation.LoginUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
//@RequestMapping(value = "/api_gateway", consumes = { MediaType.MULTIPART_FORM_DATA_VALUE,
@RequestMapping(value = "/capital", consumes = { MediaType.MULTIPART_FORM_DATA_VALUE,
		MediaType.APPLICATION_ATOM_XML_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE,
		MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE, MediaType.TEXT_HTML_VALUE,
		MediaType.TEXT_XML_VALUE, MediaType.APPLICATION_OCTET_STREAM_VALUE })
public class AccountTradeRechargeController {
	private final Log log = LogFactory.getLog(AccountTradeRechargeController.class);

	@Autowired
	private AccountService accountManager;
	
	/**
	 * 充值接口
	 * @param response
	 * @param request
	 * @param paramJson
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/account/recharge", method = RequestMethod.POST)
	public Object accountRecharge(HttpServletResponse response, HttpServletRequest request,
			@RequestBody String paramJson) throws Exception {
		
		Map<String, Object> paramsMap = JacksonUtils.jsn2map(paramJson, String.class, Object.class);
		String apsKey = MapUtils.getString(paramsMap, "apsKey");
		log.info("~~~充值者apsKey: " + apsKey);
		// 传过来的单位是元,需要转换成分
		String amountY = MapUtils.getString(paramsMap, "amount");
		log.info("~~~充值者amount: " + amountY);
		MutexLock mutexLock = MutexLockUtils.getMutexLock(apsKey);
		synchronized (mutexLock) {
			FcAccount oldCustomerAccount = accountManager.getAccountByKey(apsKey);
			Object obj = CapitalAccountUtils.validationAccount(oldCustomerAccount);
			if (obj != null) {
				return obj;
			}
			 
			BigDecimal rechargeAmount = new BigDecimal(amountY);
			
			FcAccount rechargeAccount = new FcAccount();
			rechargeAccount.setAccountId(oldCustomerAccount.getAccountId());
			rechargeAccount.setAmount(rechargeAmount);
			accountManager.accountTradeRecharge(oldCustomerAccount, rechargeAccount, paramsMap, "");
			MutexLockUtils.remove(apsKey);
		}
		
		return ResponseUtil.ok("账户充值成功!",null);
	}
	
	@RequestMapping(value = "/account/rechargev2", method = RequestMethod.POST)
	public Object accountRechargev2(HttpServletResponse response, HttpServletRequest request,
			@RequestBody String paramJson,@LoginUser String userId) throws Exception {
		Map<String, Object> paramsMap = JacksonUtils.jsn2map(paramJson, String.class, Object.class);
		if (userId == null) {
			return ResponseUtil.unlogin();
		}
		
		
		log.info("~~~充值者apsKey: " + userId);
		// 传过来的单位是元,需要转换成分
		String amountY = MapUtils.getString(paramsMap, "amount");
		log.info("~~~充值者amount: " + amountY);
		MutexLock mutexLock = MutexLockUtils.getMutexLock(userId.toString());
		synchronized (mutexLock) {
			FcAccount oldCustomerAccount = accountManager.getAccountByKey(userId.toString());
			Object obj = CapitalAccountUtils.validationAccount(oldCustomerAccount);
			if (obj != null) {
				return obj;
			}
			
			BigDecimal rechargeAmount = new BigDecimal(amountY);
			FcAccount rechargeAccount = new FcAccount();
			rechargeAccount.setAccountId(oldCustomerAccount.getAccountId());
			rechargeAccount.setCustomerId(userId.toString());
			rechargeAccount.setAmount(rechargeAmount);
			FcAccountRechargeBill myBill = accountManager.crateNewRechargeBill(oldCustomerAccount, rechargeAccount, paramsMap);
			MutexLockUtils.remove(userId.toString());
			return ResponseUtil.ok("账户充值成功",myBill.getBillId());
			
		}
	}
	
}
