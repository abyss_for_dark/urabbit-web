package com.graphai.mall.wx.web.tripartite;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.mall.db.domain.MallUser;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.util.MTUtil;
import com.graphai.mall.wx.annotation.LoginUser;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 美团CPS
 */
@RestController
@RequestMapping("/wx/mt/yiqifa")
@Validated
public class WxMtController {
    private final Log logger = LogFactory.getLog(WxMtController.class);

    @Resource
    MallUserService mallUserService;

    /**
     * 美团外卖取链接口
     * @param userId  用户ID
     * @return
     */
    @GetMapping("/link")
    public Object create(@LoginUser String userId) throws Exception {
//        Object error = validate(userId);
//        if (error != null) {
//            return error;
//        }
    	
		if (userId == null) {
		return ResponseUtil.unlogin();
	}
        MallUser mallUser = mallUserService.findById(userId);
        if(mallUser== null){
            logger.error("该用户编号系统不存在");
            return ResponseUtil.fail(-1,"该用户编号系统不存在");
        }
        String rspStr = MTUtil.requestMTLink(userId,19896,1030389);
//        String rspStr = "{\"msg\":\"接口调用次数超限\",\"code\":\"success\",\"result\":{\"clearLink\":\"www.baidu.com\",\"cipherLink\":\"www.baidu.com\",\"shortLink\":\"https://s.click.taobao.com/C0aA4zu\"}}";
        logger.info("渠道返回内容="+rspStr);
        Map<String, String> res = MTUtil.json2Map(rspStr);
        String code = String.valueOf(res.get("code"));
        String msg = String.valueOf(res.get("msg"));
        if(!"0".equals(code)){
            logger.error("请求有误。code不等于0");
            return ResponseUtil.fail(-1,msg);
        }
        String result = String.valueOf(res.get("result"));
        System.out.println("result="+result);
        if(result== null){
            logger.error("请求没有获到数据。result返回空。");
            return ResponseUtil.fail(-1,msg);
        }
        JSONObject jsonObject = (JSONObject) JSON.parseObject(result, Object.class);
        //拼接链接返回给前端
        String clearLink = "/pages/web/webview?link="+jsonObject.getString("clearLink");      //清除链接
        String cipherLink = "/pages/web/webview?link="+jsonObject.getString("cipherLink");    //密码链接
        String shortLink = "/pages/web/webview?link="+jsonObject.getString("shortLink");      //短链接
        JSONObject json = new JSONObject();
        json.put("clearLink",clearLink);
        json.put("cipherLink",cipherLink);
        json.put("shortLink",shortLink);

        return ResponseUtil.ok(json);

    }

    /**
     * 取链接口验证：用户ID不能为空。
     */
    private Object validate(String userId) {
        if (StringUtils.isEmpty(userId)) {
            return ResponseUtil.badArgument();
        }
        return null;
    }
}