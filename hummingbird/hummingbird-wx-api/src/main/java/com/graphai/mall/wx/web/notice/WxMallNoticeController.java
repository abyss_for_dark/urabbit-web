package com.graphai.mall.wx.web.notice;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.graphai.mall.db.service.common.AppVersionService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.mall.db.domain.MallNotice;
import com.graphai.mall.db.domain.MallUser;
import com.graphai.mall.db.service.notice.MallNoticeService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.util.phone.PushUtils;
import com.graphai.mall.wx.annotation.LoginUser;

import cn.hutool.core.util.ObjectUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * 首页服务
 */
@RestController
@RequestMapping("/wx/mallNotice")
@Validated
@Slf4j
public class WxMallNoticeController {
	
	
	@Autowired
    private MallUserService userService;
	
	@Autowired
	private MallNoticeService mallNoticeService;

	@Autowired
	private AppVersionService appVersionService;
	
	
	
	/**
	 * 展示通知分类列表
	 * 
	 * @throws Exception
	 */
	@GetMapping("/noticeTypeList")
	public Object list(@LoginUser String userId,@RequestParam(defaultValue = "1.6.6") String version) throws Exception {

		if (org.apache.commons.lang.StringUtils.isBlank(userId)) {
			return ResponseUtil.unlogin();
		}

		List<Map<String, Object>> maps = new ArrayList<Map<String, Object>>(5);
		//判断用户的版本是否小于当前版本 false:不小于  true:小于
		boolean appVersion = appVersionService.isLessNewAppVersion(version,"2.1.1");
		// PushUtils.pushMessage("317373169827516416", "test3", "test3", 3, 0);

		MallUser user = userService.findById(userId);
		String level = "";
		List<MallNotice> notices = null;
		try {
			List<MallNotice> mallNotices = mallNoticeService.queryNoticesGroupByType();
			List<String> list= null;
			if (appVersion){
				 list = Arrays.asList("每日好货", "活动公告", "系统通知", "佣金通知");
			}else{
				 list = Arrays.asList("每日好货", "活动公告", "系统通知", "佣金通知","申诉通知");
			}
			for (int i = 0; i < list.size(); i++) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("name", list.get(i));
				map.put("type", i);
				for (MallNotice mallNotice : mallNotices) {
					if (i == mallNotice.getType()) {

						if (Integer.parseInt(user.getLevelId()) <= 3) {
							level = "1";
							notices = mallNoticeService.selectByUserLevel(level, null, user.getId(), String.valueOf(i),
									null, 0, 1);
						} else {
							level = user.getLevelId();
							notices = mallNoticeService.selectByUserLevel(level, "1", user.getId(), String.valueOf(i),
									null, 0, 1);
						}

						if (CollectionUtils.isNotEmpty(notices)) {
							map.put("content", notices.get(0).getContent()==null?"":notices.get(0).getContent());
						}else{
							map.put("content", "");
						}
					}
				}
				if (i == 0) {
					map.put("icon", "https://gxfacepay.oss-cn-shenzhen.aliyuncs.com/hummingbird/message/haohuo.png");
				}

				if (i == 1) {
					map.put("icon", "https://gxfacepay.oss-cn-shenzhen.aliyuncs.com/hummingbird/message/activity.png");
				}

				if (i == 2) {
					map.put("icon", "https://gxfacepay.oss-cn-shenzhen.aliyuncs.com/hummingbird/message/system.png");
				}

				if (i == 3) {
					map.put("icon", "https://gxfacepay.oss-cn-shenzhen.aliyuncs.com/hummingbird/message/yongjin.png");
				}
				if (i == 4){
					map.put("icon", "https://gxfacepay.oss-cn-shenzhen.aliyuncs.com/hummingbird/message/complain.png");
				}
				maps.add(map);
			}
			return ResponseUtil.ok(maps);
		} catch (Exception e) {
			log.error("公告类型查询异常");
			throw e;
		}

	}
	
	
	
	
	
	
	/**
     * App展示公告消息
     * @throws Exception
     */
    @GetMapping("/list")
    public Object list(@LoginUser String userId,@RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer size,
    		@RequestParam(required = true) String type,String secondType) throws Exception {

    	  if (org.apache.commons.lang.StringUtils.isBlank(userId)) {
              return ResponseUtil.unlogin();
          }
    	  
    	  if (page <= 0) {
              page = 1;
          }
    	  
    	  String level="";
    	  List<MallNotice> notices=null;
        try {
        	MallUser user = userService.findById(userId);
            
        	if (ObjectUtil.isNotNull(user)) {
        		
        		
        		if (Integer.parseInt(user.getLevelId())<=3) {
        			level="1";
        			notices=mallNoticeService.selectByUserLevel(level,null,user.getId(),type,secondType,(page - 1) * size, size);
				}else {
					level=user.getLevelId();
					notices=mallNoticeService.selectByUserLevel(level,"1",user.getId(),type,secondType,(page - 1) * size, size);
				}
        		 
        		return ResponseUtil.ok(notices);
			}
        } catch (Exception e) {
            log.error("公告查询异常");
            throw e;
        }
        return ResponseUtil.fail();
    }

}
