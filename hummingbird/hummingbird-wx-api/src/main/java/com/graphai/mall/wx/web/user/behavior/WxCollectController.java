package com.graphai.mall.wx.web.user.behavior;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.validation.constraints.NotNull;

import cn.hutool.core.util.ObjectUtil;
import com.graphai.mall.admin.domain.MallActivityGoodsProduct;
import com.graphai.mall.admin.domain.MallMerchant;
import com.graphai.mall.admin.service.IMallActivityGoodsProductService;
import com.graphai.mall.admin.service.IMallMerchantService;
import com.graphai.mall.db.domain.MallCategory;
import com.graphai.mall.db.domain.MallHelpTeachSupport;
import com.graphai.mall.db.service.category.MallCategoryService;
import com.graphai.mall.db.service.groupon.MallGrouponRulesService;
import com.graphai.mall.db.service.helper.MallHelpTeachSupportService;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.json.JacksonUtil;
import com.graphai.mall.db.domain.MallCollect;
import com.graphai.mall.db.domain.MallGoods;
import com.graphai.mall.db.service.goods.MallGoodsService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.service.user.behavior.MallCollectService;
import com.graphai.mall.db.util.UserUtils;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.open.mallgrouponrules.mapper.IMallGrouponRulesMapper;

/**
 * 用户收藏服务
 */
@RestController
@RequestMapping("/wx/collect")
@Validated
public class WxCollectController {
    private final Log logger = LogFactory.getLog(WxCollectController.class);

    @Autowired
    private MallCollectService collectService;
    @Autowired
    private MallGoodsService goodsService;

    @Autowired
    private MallUserService userService;
    
    @Autowired
    private IMallGrouponRulesMapper mallGrouponRulesMapper;
    
    @Autowired
    private MallGrouponRulesService mallGrouponRulesService;

    @Autowired
    private MallCategoryService mallCategoryService;
    @Autowired
    private MallHelpTeachSupportService mallHelpTeachSupportService;
    @Autowired
    private IMallMerchantService mallMerchantService;
    @Autowired
    private IMallActivityGoodsProductService mallActivityGoodsProductService;

    /**
     * 用户收藏列表
     *
     * @param userId 用户ID
     * @param type 类型，如果是0则是商品收藏，如果是1则是专题收藏
     * @param page 分页页数
     * @param limit 分页大小
     * @return 用户收藏列表
     */
    @GetMapping("list")
    public Object list(@LoginUser String userId, @NotNull Byte type, @RequestParam(defaultValue = "1") Integer page,
                    @RequestParam(defaultValue = "10") Integer limit) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }

        List<MallCollect> collectList = collectService.queryByType(userId, type, page, limit);
        int count = collectService.countByType(userId, type);
        int totalPages = (int) Math.ceil((double) count / limit);

        List<Object> collects = new ArrayList<>(collectList.size());
        for (MallCollect collect : collectList) {
            Map<String, Object> c = new HashMap<String, Object>();
            c.put("id", collect.getId());
            c.put("type", collect.getType());
            c.put("valueId", collect.getValueId());

            MallGoods goods = goodsService.findById(collect.getValueId());
            if (goods != null) {
                c.put("name", goods.getName());
                c.put("brief", goods.getBrief());
                c.put("picUrl", goods.getPicUrl());
                c.put("retailPrice", goods.getRetailPrice());
                collects.add(c);
            }
        }

        Map<String, Object> result = new HashMap<String, Object>();
        result.put("collectList", collects);
        result.put("totalPages", totalPages);
        return ResponseUtil.ok(result);
    }

    /**
     * 用户收藏列表
     * @param userId
     * @param type 0：商品收藏；2：咨询收藏；3：商铺收藏；
     * @return
     */
    @GetMapping("collectList")
    public Object collectList(@LoginUser String userId, Byte type,@RequestParam(defaultValue = "1") Integer page,
                              @RequestParam(defaultValue = "10") Integer limit){
        // 收藏列表
        List<Map<String, Object>> collectList =
                collectService.queryListByPage(userId, type, (page - 1) * limit, limit);

        List<Object> collects = new ArrayList<>(collectList.size());
        if(collectList.size() <= 0){
            return ResponseUtil.ok(collects);
        }
        try {

            List<String> ids = new ArrayList<String>(collectList.size());
            for (int i = 0; i < collectList.size(); i++) {
                ids.add(String.valueOf(collectList.get(i).get("valueId")));
            }
            if(type == 0){
                // 商品
                for (int i = 0; i < collectList.size(); i++) {
                    String id = String.valueOf(collectList.get(i).get("id"));
                    String valueId = String.valueOf(collectList.get(i).get("valueId"));
                    MallGoods mallGoods = goodsService.findById(valueId);
                    MallCategory mallCategory = mallCategoryService.findById(mallGoods.getCategoryId());

                    List<MallActivityGoodsProduct> mallActivityGoodsProducts = mallActivityGoodsProductService.getListByActivityGoodsId(2, valueId);

                    Map<String,Object> map = new HashMap<>();
                    if(mallActivityGoodsProducts != null && mallActivityGoodsProducts.size() > 0){
                        map.put("isGroup",true);
                    }else {
                        map.put("isGroup", false);
                    }
                    map.put("goods",mallGoods);
                    map.put("mallCategory",mallCategory);
                    map.put("id",id);
                    collects.add(map);
                }
            }else if(type == 2){
                // 咨询收藏
                for (int i = 0; i < collectList.size(); i++) {
                    String id = String.valueOf(collectList.get(i).get("id"));
                    String valueId = String.valueOf(collectList.get(i).get("valueId"));
                    MallHelpTeachSupport mallHelpTeachSupport = mallHelpTeachSupportService.getById(valueId);

                    if(ObjectUtil.isNull(mallHelpTeachSupport) || ObjectUtil.isEmpty(mallHelpTeachSupport)){
                        continue;
                    }
                    Map<String,Object> map = new HashMap<>();
                    map.put("mallHelpTeachSupport",mallHelpTeachSupport);
                    map.put("id",id);
                    collects.add(map);
                }
            }else if(type == 3){
                // 商铺收藏
                for (int i = 0; i < collectList.size(); i++) {
                    String id = String.valueOf(collectList.get(i).get("id"));
                    String valueId = String.valueOf(collectList.get(i).get("valueId"));
                    MallMerchant mallMerchant = mallMerchantService.getById(valueId);

                    if(mallMerchant != null){
                        Map<String,Object> map = new HashMap<>();
                        map.put("mallMerchant",mallMerchant);
                        map.put("id",id);
                        collects.add(map);
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return ResponseUtil.ok(collects);
    }

    /**
     * 用户收藏列表
     *
     * @param userId 用户ID
     * @param type 类型，如果是0则是商品收藏，如果是1则是专题收藏
     * @param page 分页页数
     * @param limit 分页大小
     * @return 用户收藏列表
     */
    @PostMapping("wlkList")
    public Object wlkList(@LoginUser String userId, @RequestBody String body) {
        String sign = JacksonUtil.parseString(body, "sign");
        // 手机号
        String mobile = JacksonUtil.parseString(body, "mobile");
        Byte type = JacksonUtil.parseByte(body, "type");
        Integer page = JacksonUtil.parseInteger(body, "page");
        Integer limit = JacksonUtil.parseInteger(body, "limit");
        if (null == userId) {
            Object result = UserUtils.getTokenUserId(mobile);
            if (!Objects.equals(result, null)) {
                userId = String.valueOf(result);
            }
        }
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            if (page <= 0) {
                page = 1;
            }
            List<Map<String, Object>> collectList =
                            collectService.queryListByPage(userId, type, (page - 1) * limit, limit);
            List<String> gidList = new ArrayList<String>(collectList.size());
            for (int i = 0; i < collectList.size(); i++) {
                gidList.add(String.valueOf(collectList.get(i).get("valueId")));
            }
            JSONArray collectGoodsList = goodsService.queryCollectList(gidList);
            //求出团购商品id集合
            List<String> gids=mallGrouponRulesService.queryGoodsId();
            //求出每个商品的收藏人数
            List<Map<String, Object>> countList=collectService.queryCountByValueId(userId, type);
            List<Object> collects = new ArrayList<>(collectList.size());
            for (int k = 0; k < collectList.size(); k++) {
                Map<String, Object> c = new HashMap<String, Object>();
                c.put("id", collectList.get(k).get("id"));
                c.put("type", collectList.get(k).get("type"));
                c.put("valueId", collectList.get(k).get("valueId"));
                for (int i = 0; i < collectGoodsList.size(); i++) {
                    JSONObject object = (JSONObject) collectGoodsList.get(i);
                    System.out.println("id" + object.getString("id"));
                    if (Objects.equals(collectList.get(k).get("valueId"), object.getString("id"))) {
                        c.put("name", object.getString("name"));
                        c.put("brief", object.getString("brief"));
                        c.put("picUrl", object.getString("picUrl"));
                        c.put("retailPrice", object.getString("retailPrice"));
                        
                        Integer virtualGood=object.getInteger("virtualGood");
                        if (Objects.equals(virtualGood, 0)) {
                            if (CollectionUtils.isNotEmpty(gids)) {
                                if (gids.contains(collectList.get(k).get("valueId"))) {
                                    //拼团线上
                                    virtualGood=1; 
                                }
                            }else {
                                if (!Objects.equals(null, object.getString("cashbackType"))) {
                                    //返现
                                    virtualGood=2; 
                                }
                            } 
                        }
                        c.put("virtualGood", virtualGood);   
                    }
                    if (CollectionUtils.isNotEmpty(countList)) {
                        for (int j = 0; j < countList.size(); j++) {
                            for (Map<String, Object> object2 : countList) {
                                if (Objects.equals(collectList.get(k).get("valueId"),String.valueOf(object2.get("valueId")))) {
                                    c.put("num", object2.get("count")); 
                                }
                            }
                        }
                    } 
                }
                    collects.add(c);
            }
            result.put("collectList", collects);
            // result.put("totalPages", totalPages);
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }


        return ResponseUtil.ok(result);
    }

    /**
     * 用户收藏添加或删除
     * <p>
     * 如果商品没有收藏，则添加收藏；如果商品已经收藏，则删除收藏状态。
     *
     * @param userId 用户ID
     * @param body 请求内容，{ type: xxx, valueId: xxx }
     * @return 操作结果
     */
    @PostMapping("addordelete")
    public Object addordelete(@LoginUser String userId, @RequestBody String body) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        //String userId = "5199692295660396544";

        Byte type = JacksonUtil.parseByte(body, "type");
        String valueId = JacksonUtil.parseString(body, "valueId");
        String merchantId = JacksonUtil.parseString(body, "merchantId");
//        if (!ObjectUtils.allNotNull(type, valueId, merchantId)) {
//            return ResponseUtil.badArgument();
//        }

        MallCollect collect = collectService.queryByTypeAndValue(userId, type, valueId);

        String handleType = null;
        if (collect != null) {
            handleType = "delete";
            collectService.deleteById(collect.getId());
        } else {
            handleType = "add";
            collect = new MallCollect();
            collect.setUserId(userId);
            collect.setValueId(valueId);
            collect.setType(type);
            collect.setMerchantId(merchantId);
            collectService.add(collect);
        }

        Map<String, Object> data = new HashMap<String, Object>();
        data.put("type", handleType);
        return ResponseUtil.ok(data);
    }

    /**
     * 批量删除收藏
     * @param body
     * @return
     */
    @PostMapping("deleteMore")
    public Object deleteMore(@RequestBody String body){
        List<String> ids = JacksonUtil.parseStringList(body, "ids");
        if(ids.size() <= 0){
            return ResponseUtil.fail("未选择收藏");
        }
        collectService.deleteMore(ids);
        return ResponseUtil.ok();
    }

    /**
     * 用户收藏添加或删除
     * <p>
     * 如果商品没有收藏，则添加收藏；如果商品已经收藏，则删除收藏状态。
     *
     * @param userId 用户ID
     * @param body 请求内容，{ type: xxx, valueId: xxx }
     * @return 操作结果
     */
    @PostMapping("wlkAddorDelete")
    public Object wlkAddorDelete(@LoginUser String userId, @RequestBody String body) {

        String sign = JacksonUtil.parseString(body, "sign");
        // 手机号
        String mobile = JacksonUtil.parseString(body, "mobile");
        Byte type = JacksonUtil.parseByte(body, "type");
        String valueId = JacksonUtil.parseString(body, "valueId");
        String findGoodsCollect = JacksonUtil.parseString(body, "findGoodsCollect");
        if (!ObjectUtils.allNotNull(type, valueId)) {
            return ResponseUtil.badArgument();
        }
        
        
        

        Map<String, Object> data = new HashMap<String, Object>();

        if (null == userId) {
            Object result = UserUtils.getTokenUserId(mobile);
            if (!Objects.equals(result, null)) {
                userId = String.valueOf(result);
            }
        }
        boolean handleType = false;
        boolean isCollect = false;
        try {
            MallCollect collect = collectService.queryByTypeAndValue(userId, type, valueId);

            if (Objects.equals(findGoodsCollect, "1")) {
                if (collect != null) {
                    isCollect = true;
                }
                data.put("isCollect", isCollect);
            } else {
                if (collect != null) {
                    handleType = false;
                    collectService.deleteById(collect.getId());
                } else {
                    handleType = true;
                    collect = new MallCollect();
                    collect.setUserId(userId);
                    collect.setValueId(valueId);
                    collect.setType(type);
                    collectService.add(collect);
                }
                data.put("type", handleType);
            }

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }

        return ResponseUtil.ok(data);
    }



}
