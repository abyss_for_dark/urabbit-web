package com.graphai.mall.wx.service;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.bcrypt.BCryptPasswordEncoder;
import com.graphai.commons.util.lang.StringUtils;
import com.graphai.mall.db.dao.MallOrderMapper;
import com.graphai.mall.db.dao.MallUserMapper;
import com.graphai.mall.db.domain.*;
import com.graphai.mall.db.service.account.AccountService;
import com.graphai.mall.db.constant.account.AccountStatus;
import com.graphai.mall.db.service.order.MallOrderService;
import com.graphai.mall.db.service.pay.DoPayService;
import com.graphai.mall.db.service.wangzhuan.WangzhuanTaskService;
import com.graphai.mall.db.service.wangzhuan.WangzhuanTaskServiceV2;
import com.graphai.mall.db.constant.CommonConstant;
import com.graphai.mall.db.util.OrderUtil;
import com.graphai.mall.db.util.account.AccountUtils;
import com.graphai.mall.wx.util.RSAUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class WxOrderByMyWalletService {
    private final Log logger = LogFactory.getLog(WxOrderByMyWalletService.class);

    @Resource
    private MallOrderMapper mallOrderMapper;
    @Resource
    private MallUserMapper mallUserMapper;
    @Resource
    private AccountService accountService;
    @Resource
    private WangzhuanTaskService wangzhuanTaskService;
    @Resource
    private WangzhuanTaskServiceV2 wangzhuanTaskServiceV2;
    @Resource
    private MallOrderService mallOrderService;
    @Autowired
    private DoPayService doPayService;

    // 判断用户的钱包，是否足以支付订单！
    public Object checkUserWalletIsFull(String userId,String orderId,String type){
        logger.info("判断用户 userId:"+userId+"的  "+type+"  余额，是否足以支付订单 orderId："+orderId);
        // 根据用户id 找到 改用户 的 账户数据
        FcAccount fcAccount = accountService.getUserAccount(userId);
        if(ObjectUtils.isEmpty(fcAccount)){
            return ResponseUtil.fail("未找到该用户的账户！");
        }
        BigDecimal amount = BigDecimal.ZERO;
        // 判断用户要从 那里扣钱
        switch (type){
            case "task":
                // 任务钱包
                amount = fcAccount.getExchangeAmount();
                break;
            case "coins":
                // 零钱钱包
                amount = fcAccount.getAmount();
                break;
            case "gold":
                // 金币
                amount = fcAccount.getGold();
                break;
            case "score":
                // 信用分
                amount = fcAccount.getCreditScore();
                break;
            default:
                break;
        }
        // 根据订单id  找到对应的订单数据
        MallOrder mallOrder = mallOrderMapper.selectByPrimaryKey(orderId);
        List<String> list = AccountStatus.BID_TASK_BID;
        // 顺带判断一下， 这个订单所属的业务，是否支持使用任务钱包。 -- 这个判断，之后需要重新拉取到一个公用的方法。
        if(!list.contains(mallOrder.getBid())){
           return ResponseUtil.fail("该业务，暂不支持使用任务钱包");
        }

        if(ObjectUtils.isEmpty(mallOrder)){
            return ResponseUtil.fail("未找到对应的订单");
        }
        BigDecimal beAmount = mallOrder.getActualPrice();
        if(beAmount.compareTo(amount) == 1){
            return ResponseUtil.fail("账户余额不足，请选择其他支付方式");
        }
        Boolean bool = false;
        MallUser mallUser = mallUserMapper.selectByPrimaryKey(userId);
        if(!ObjectUtils.isEmpty(mallUser.getIsPayNoPassword())){
            if(mallUser.getIsPayNoPassword().toString().equals("1")){
                bool = true;
            }
        }
        Map<String,Object> map = new HashMap<>();
        map.put("orderId",orderId);
        map.put("isPayNoPassword",bool);
        map.put("type",type);
        return ResponseUtil.ok(map);
    }

    @Transactional
    public Object goToUpdateAboutTaskPayAfter(String userId,String orderId){
        MallOrder mallOrder = mallOrderMapper.selectByPrimaryKey(orderId);
        // 先给我扣款， 扣完了，生成流水， 完了再算
        Map<String, String> map = dealMoneyForPay(userId,mallOrder);
        if(!ObjectUtils.isEmpty(map)){
            if(map.get("code").equals("0001")){
                return ResponseUtil.fail(map.get("msg"));
            }
        }
        // 给我滚去处理订单！
        Boolean bool = checkOrderAboutTask(mallOrder);
        if(bool){
             String msg = dealModelForAlreadyPay(mallOrder);
             if("fail".equals(msg)){
                 return ResponseUtil.fail("处理业务逻辑出现异常！");
             }else{
                 return ResponseUtil.ok();
             }
        }else{
            logger.info("订单处理失败！");
        }
        return ResponseUtil.ok();
    }
    // 这个方法是用来扣钱的！
    public Map<String, String> dealMoneyForPay(String userId,MallOrder order){
        logger.info("扣款参数：-------------------userId:"+userId+"-----order:"+order.toString());
        Map<String, String> resultMap = new HashMap<>();
        resultMap.put("code","0001");
        resultMap.put("msg","网赚任务-充值失败");
        Map<String, Object> paramsMap = new HashMap<>();
        String rechargeType = CommonConstant.TASK_REWARD_CASH;
        paramsMap.put("buyerId", userId);
        paramsMap.put("capitalTrend", AccountStatus.CAPITAL_TREND_02);
        paramsMap.put("changeType", AccountStatus.CHANGE_TYPE_02);
        paramsMap.put("message", "支付");
        paramsMap.put("changeAccountType", AccountStatus.CHANGE_ACCOUNT_TYPE_2);
        paramsMap.put("tradeType",AccountStatus.TRADE_TYPE_38);
        try {
            AccountUtils.accountChangeByExchange(userId, order.getActualPrice(), CommonConstant.TASK_REWARD_CASH,
                    order.getBid(), paramsMap);
            resultMap.put("code","0000");
            resultMap.put("msg","网赚任务-扣款成功！");
            return  resultMap;
        }catch (Exception e){
            e.printStackTrace();
            logger.info(e.getMessage()+"扣款失败！");
            return resultMap;
        }
    }
    // 这个方法是用来更改订单的支付状态的，大致为，这个订单已经支付过了！即，扣款完成之后的回调方法
    public boolean checkOrderAboutTask(MallOrder order){
        order.setPayId("taskWalletPay");
        order.setPayTime(LocalDateTime.now());
        order.setOrderStatus(OrderUtil.STATUS_PAY);
        order.setUserId(order.getUserId());
        int updated = mallOrderService.updateWithOptimisticLocker(order);
        if(updated>0){
            return true;
        }else{
            return false;
        }
    }
    // 这个方法是用来 处理对应的业务逻辑的！
    public String dealModelForAlreadyPay(MallOrder order){
        // 判断是-网赚任务-订单就拦截处理，不往下执行了
        if (AccountStatus.BID_47.equals(order.getBid())) {
            try {
                return wangzhuanTaskService.orderProcess(order, order.getOrderSn());
            } catch (Exception ex) {
                logger.info("三方下--网赚任务--单支付出错：" + ex.getMessage(), ex);
            }
        }

        // 判断是-网赚任务-上调数量/上调单价-订单就拦截处理，不往下执行了
        if (AccountStatus.BID_48.equals(order.getBid())) {
            try {
                return wangzhuanTaskService.orderProcessAddNumOrAmount(order,order.getOrderSn());
            }catch (Exception e){
                logger.info("三方下-- 网赚任务-上调任务单价/任务数量--订单支付出错："+e.getMessage(),e);
            }
        }
        // 判断是-网赚任务-购买推荐、置顶 / 竞价 -订单就拦截处理，不往下执行了
        if (AccountStatus.BID_49.equals(order.getBid())) {
            try {
                return wangzhuanTaskServiceV2.orderProcessTBR(order,order.getOrderSn());
            }catch (Exception e){
                logger.info("三方下-- 网赚任务-上调任务单价/任务数量--订单支付出错："+e.getMessage(),e);
            }
        }
        if (AccountStatus.BID_36.equals(order.getBid())) {
            try {
                order.setSource("taskWallet");
//                doPayService.doGroupV3(order);
                doPayService.doNewGroupV3(order);
                return "";
            } catch (Exception e) {
                logger.info(" 团购--订单余额支付出错：" + e.getMessage(), e);
            }
        }
        return "fail";
    }
    // 校验支付密码是否正确
    public Object checkUserPayPassword(String userId,String payPassword,String privateKey,String orderId){
        try {
            byte[] payPasswordByte =RSAUtils.decryptByPrivateKey(payPassword, privateKey);
            logger.info(payPasswordByte);
            String payPasswordUserSend = new String(payPasswordByte);
            MallUser mallUser = mallUserMapper.selectByPrimaryKey(userId);
            if(ObjectUtils.isEmpty(mallUser)){
                return ResponseUtil.fail("找不到对应的用户！");
            }
            if(StringUtils.isEmpty(mallUser.getPayPassword())){
                return ResponseUtil.fail("该用户没有设置支付密码，请先设置支付密码！");
            }
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
            if (!encoder.matches(payPasswordUserSend, mallUser.getPayPassword())) {
                return ResponseUtil.fail("支付密码错误！");
            }
            return ResponseUtil.ok(orderId);
        }catch (Exception e){
            logger.info("校验密码出现异常！");
            e.printStackTrace();
            return ResponseUtil.fail("校验密码出现异常！");
        }
    }



}
