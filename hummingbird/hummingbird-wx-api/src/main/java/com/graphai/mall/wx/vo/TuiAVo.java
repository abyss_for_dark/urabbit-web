package com.graphai.mall.wx.vo;

import lombok.Data;

/**
 * @Author: jw
 * @Date: 2021/1/30 13:37
 */
 @Data
public class TuiAVo {
    private String gameName ;
    private TuiAParams tuiAParams ;
}
