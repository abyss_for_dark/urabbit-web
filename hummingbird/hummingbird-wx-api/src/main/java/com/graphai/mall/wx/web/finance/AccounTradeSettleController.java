package com.graphai.mall.wx.web.finance;

import java.math.BigDecimal;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.lock.MutexLock;
import com.graphai.commons.util.lock.MutexLockUtils;
import com.graphai.commons.util.json.JacksonUtils;
import com.graphai.mall.db.domain.FcAccount;
import com.graphai.mall.db.domain.FcAccountFreezeBill;
import com.graphai.mall.db.service.account.AccountService;
import com.graphai.mall.db.service.account.CapitalAccountUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/capital", consumes = { MediaType.MULTIPART_FORM_DATA_VALUE,
		MediaType.APPLICATION_ATOM_XML_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE,
		MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE, MediaType.TEXT_HTML_VALUE,
		MediaType.TEXT_XML_VALUE, MediaType.APPLICATION_OCTET_STREAM_VALUE })
public class AccounTradeSettleController {
	private final Log log = LogFactory.getLog(AccounTradeSettleController.class);
	@Autowired
	private AccountService accountManager;

	@RequestMapping(value = "/account/consume", method = RequestMethod.POST)
	public Object accountTradeSettle(HttpServletResponse response, HttpServletRequest request,
			@RequestBody String paramJson) throws Exception {

		Map<String, Object> paramsMap = JacksonUtils.jsn2map(paramJson, String.class, Object.class);
		String apsKey = MapUtils.getString(paramsMap, "apsKey");
		// 传过来的单位是元,需要转换成分
		// String amountY = MapUtils.getString(paramsMap, "amount");
		String freezeAcBillId = MapUtils.getString(paramsMap, "freezeAcBillId");
		// 传过来的单位是元,需要转换成分
		// String sn = MapUtils.getString(paramsMap, "sn");

		log.info("消费账户金额接口接收参数paramJson ： " + paramJson);
		MutexLock mutexLock = MutexLockUtils.getMutexLock(apsKey);
		synchronized (mutexLock) {
			// 账户不存在
			FcAccount oldCustomerAccount = accountManager.getAccountByKey(apsKey);
			// 根据冻结单来消费金额
			FcAccountFreezeBill freezeBill = accountManager.getFcAccountFreezeBillById(freezeAcBillId);
			if (freezeBill == null) {
				return ResponseUtil.fail(-13, "预消费单不存在,请检查");
			}

			/***
			 * 01 冻结 02 冻结释放 03 已结算
			 **/
			String freezeStatus = freezeBill.getFreezeStatus();
			if ("02".equals(freezeStatus) || "03".equals(freezeStatus)) {
				return ResponseUtil.fail(-14, "编号为:" + freezeAcBillId + "预消费单已经失效,请检查");
			}

			Object obj = CapitalAccountUtils.validationAccount(oldCustomerAccount);
			if (obj != null) {
				return obj;
			}
		 
			/******账户结算******/
			BigDecimal unCashAmt = freezeBill.getFreezeUncashAmt();
			FcAccount freezeAccount = new FcAccount();
			freezeAccount.setAccountId(oldCustomerAccount.getAccountId());
			// freezeAccount.setAmount(unCashAmt);
			freezeAccount.setFreezeUncashAmount(unCashAmt);
			accountManager.accountTradeSettle(oldCustomerAccount, freezeAccount, paramsMap);
 
			MutexLockUtils.remove(apsKey);
		}

	 	return ResponseUtil.ok("账户结算成功",null);
	}
}
