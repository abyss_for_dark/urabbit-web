package com.graphai.mall.wx.web.other;

import com.graphai.commons.constant.MallConstant;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.mall.db.domain.BmdesignTeam;
import com.graphai.mall.db.domain.BmdesignUserLevel;
import com.graphai.mall.db.domain.MallInvitationCode;
import com.graphai.mall.db.domain.MallUser;
import com.graphai.mall.db.service.*;
import com.graphai.mall.db.service.bmdesign.BmdesignTeamService;
import com.graphai.mall.db.service.bmdesign.BmdesignUserLevelService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.vo.MallInvitationCodeVo;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.validator.Order;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 激活码
 */
@RestController
@RequestMapping("/wx/mallInvitationCode")
@Validated
public class WxMallInvitationCodeController {
    private final Log logger = LogFactory.getLog(WxMallInvitationCodeController.class);

    @Autowired
    private BmdesignUserLevelService bmdesignUserLevelService;
    @Autowired
    private MallUserService mallUserService;
    @Autowired
    private BmdesignTeamService bmdesignTeamService;
    @Autowired
    private MallInvitationCodeService mallInvitationCodeService;

    /**
     * 查询激活码
     */
    @GetMapping("/list")
    public Object list(@LoginUser String userId,
//                       @RequestParam(required = false) String useStatus,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit,
                       @RequestParam(defaultValue = "id") String sort,
                       @Order @RequestParam(defaultValue = "desc") String order) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        Integer start = (page - 1) * limit;

        List<MallInvitationCodeVo> list = mallInvitationCodeService.selectMallInvitationCodeListByUserId(userId, null, null, null, sort, order);
        List<MallInvitationCodeVo> allUnusedCodeList = mallInvitationCodeService.selectMallInvitationCodeListByUserId(userId, MallConstant.INVOCATION_STATUS_NOTUSED, null, null, sort, order);
        List<MallInvitationCodeVo> allUsedCodeList = mallInvitationCodeService.selectMallInvitationCodeListByUserId(userId, MallConstant.INVOCATION_STATUS_USED, null, null, sort, order);
        List<MallInvitationCodeVo> unusedcodeList = mallInvitationCodeService.selectMallInvitationCodeListByUserId(userId, MallConstant.INVOCATION_STATUS_NOTUSED, start, limit, sort, order);
        List<MallInvitationCodeVo> usedCodeList = mallInvitationCodeService.selectMallInvitationCodeListByUserId(userId, MallConstant.INVOCATION_STATUS_USED, start, limit, sort, order);

        Map<String, Object> data = new HashMap<>();
        data.put("total", list.size());
        data.put("unusedtotal", allUnusedCodeList.size());
        data.put("usedTotal", allUsedCodeList.size());
        data.put("unusedCodeList", unusedcodeList);
        data.put("usedCodeList", usedCodeList);
        return ResponseUtil.ok(data);
    }

    /**
     * 添加激活码
     */
    @PostMapping("/add")
    public Object add(@LoginUser String userId) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        MallInvitationCode mallInvitationCode = new MallInvitationCode();
        mallInvitationCode.setUserId(userId);
        mallInvitationCodeService.insertMallInvitationCode(mallInvitationCode);
        return ResponseUtil.ok();
    }

    /**
     * 通过id删除激活码(逻辑删除)
     */
    @PostMapping("/edit/{id}")
    public Object edit(@LoginUser String userId,
                       @PathVariable("id") String id) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        MallInvitationCode mallInvitationCode = mallInvitationCodeService.selectMallInvitationCodeById(id);
        if (null == mallInvitationCode) {
            return ResponseUtil.badArgumentValue("不存在该注册邀请码！");
        }
        if (!userId.equals(mallInvitationCode.getUserId())) {
            return ResponseUtil.badArgumentValue("无权限操作该注册邀请码！");
        }
        MallInvitationCode code = new MallInvitationCode();
        code.setId(id);
        code.setDeleted(MallInvitationCode.IS_DELETED);
        mallInvitationCodeService.updateMallInvitationCode(code);

        return ResponseUtil.ok();
    }

    /**
     * 使用激活码
     */
    @PostMapping("/useCode/{code}")
    public Object useCode(@LoginUser String userId,
                          @PathVariable("code") String code) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        //通过code 分销注册邀请码
        MallInvitationCode mallInvitationCode = mallInvitationCodeService.selectMallInvitationCodeByCode(code);
        if (null == mallInvitationCode) {
            return ResponseUtil.badArgumentValue("不存在该注册邀请码！");
        }
        MallUser user = mallUserService.findById(userId);
        if (StringUtils.isNotEmpty(user.getDirectLeader())) {
            return ResponseUtil.badArgumentValue("您已经被邀请过,无法再使用其他注册邀请码！");
        }

        String firstId = mallInvitationCode.getUserId();    //推荐人
        if (userId.equals(firstId)) {
            return ResponseUtil.badArgumentValue("无法使用自己的注册邀请码！");
        }
        String secondId = null;
        String thirdId = null;
        MallUser firstUser = mallUserService.findById(firstId);
        if (null != firstUser) {
            if (StringUtils.isNotEmpty(firstUser.getFirstLeader())) {
                secondId = firstUser.getFirstLeader();
            }
            if (StringUtils.isNotEmpty(firstUser.getSecondLeader())) {
                thirdId = firstUser.getSecondLeader();
            }
        } else {
            return ResponseUtil.badArgumentValue("邀请用户不存在！");
        }
        //1.修改mall_user 信息
        MallUser mallUser = new MallUser();
        mallUser.setId(userId);
        mallUser.setFirstLeader(firstId);
        mallUser.setSecondLeader(secondId);
        mallUser.setThirdLeader(thirdId);
        mallUserService.updateById(mallUser);

        //2.创建bmdesign_team 表关系数据
        if (StringUtils.isNotEmpty(user.getLevelId()) && !"0".equals(user.getLevelId())) {
            //todo 用户有等级,则判断符合条件的上级
            BmdesignUserLevel level = bmdesignUserLevelService.selectBmdesignUserLevelById(user.getLevelId());
            if (null == level) {
                return ResponseUtil.badArgumentValue("系统错误，用户等级不存在！");
            }
            //通过条件查询 个人团体表
            List<BmdesignTeam> bmdesignTeamList = bmdesignTeamService.selectBmdesignTeamListByCondition(firstId, level.getLevelNum(), false);

            if (bmdesignTeamList.size() > 0) {
                for (BmdesignTeam team2 : bmdesignTeamList) {
                    BmdesignTeam team = new BmdesignTeam();
                    team.setTeamLeaderId(team2.getTeamLeaderId());
                    team.setFirstLeaderId(firstId);
                    team.setUserId(userId);
                    team.setLevelId(user.getLevelId());
                    team.setTeamLeaderLevelId(team2.getTeamLeaderLevelId());
                    team.setOldLevelName(level.getLevelName());
                    team.setNickname(user.getNickname());
                    team.setAvatar(user.getAvatar());
//                    team.setDeleted(BmdesignTeam.NOT_DELETED);
                    bmdesignTeamService.insertBmdesignTeam(team);
                }
            }

        } else {
            //todo 用户没等级（至少创建一条直推数据）,则不需要判断符合条件的上级
            BmdesignTeam bmdesignTeam = new BmdesignTeam();
            bmdesignTeam.setUserId(firstId);
            List<BmdesignTeam> bmdesignTeamList = bmdesignTeamService.selectBmdesignTeamList(bmdesignTeam);
            if (bmdesignTeamList.size() > 0) {
                for (BmdesignTeam team2 : bmdesignTeamList) {
                    BmdesignTeam team = new BmdesignTeam();
                    team.setTeamLeaderId(team2.getTeamLeaderId());
                    team.setFirstLeaderId(firstId);
                    team.setUserId(userId);
                    team.setTeamLeaderLevelId(team2.getTeamLeaderLevelId());
                    team.setNickname(user.getNickname());
                    team.setAvatar(user.getAvatar());
//                    team.setDeleted(BmdesignTeam.NOT_DELETED);
                    bmdesignTeamService.insertBmdesignTeam(team);
                }
            }
            //直推数据
            BmdesignTeam team = new BmdesignTeam();
            team.setTeamLeaderId(firstId);
            team.setFirstLeaderId(firstId);
            team.setUserId(userId);
            team.setTeamLeaderLevelId(firstUser.getLevelId());
            team.setNickname(user.getNickname());
            team.setAvatar(user.getAvatar());
//            team.setDeleted(BmdesignTeam.NOT_DELETED);
            bmdesignTeamService.insertBmdesignTeam(team);
        }


        //3.修改 分销注册邀请码状态(已使用)
        MallInvitationCode updateCode = new MallInvitationCode();
        updateCode.setId(mallInvitationCode.getId());
        updateCode.setSpendUserId(userId);  //使用人
        updateCode.setUseStatus(MallConstant.INVOCATION_STATUS_USED);
        updateCode.setUseTime(LocalDateTime.now());
        mallInvitationCodeService.updateMallInvitationCode(updateCode);

        return ResponseUtil.ok();
    }


}
