package com.graphai.mall.wx.web.goods;

import cn.hutool.core.util.ObjectUtil;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.mall.core.lucene.service.DeleteIndexAboutService;
import com.graphai.mall.db.domain.MallGoods;
import com.graphai.mall.db.service.goods.MallGoodsService;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.mall.wx.dto.GoodsAllinone;
import com.graphai.mall.wx.service.WxGoodsService;
import com.graphai.open.utils.HttpRequestUtils;
import com.graphai.validator.Order;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.Optional;

/**
 * 房淘自营商品 agent端服务
 */
@RestController
@Slf4j
@RequestMapping("/wx/zYGoods")
public class WxZYGoodsController {

    @Autowired
    private WxGoodsService wxGoodsService;

    @Autowired
    private DeleteIndexAboutService deleteIndexAboutService;

    private static final String URL = "http://web.51fangtao.com/wx/all/search/createOrUpdateIndexById?id=";

    /**
     * 查询商品(goodsType: 0 自营商品、1:第三方商品 2:优惠券商品 3:权益商品)
     */
    @GetMapping("/list")
    public Object list(String goodsSn, String name, Integer goodsType,
                       String brandName, Integer priceMin, Integer priceMax,
                       Boolean isHot, Boolean isNew, Integer isOnSale,String merchantNoSale,
                       Integer categorypid, Integer industryId, Boolean isRightGoods,
                       Integer platform,
                       Integer placement,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit,
                       @RequestParam(defaultValue = "id") String sort,
                       @Order @RequestParam(defaultValue = "desc") String order) {
        //isOnSale：0:下架，1:上架，2:审核中（审核中也就是下架）
        if(ObjectUtil.isNull(isOnSale)){
            return wxGoodsService.list(merchantNoSale,goodsSn, name, page, limit, sort, order,
                    brandName, priceMin, platform, priceMax, isHot, isNew, null, categorypid, industryId, goodsType, isRightGoods, placement);
        }

        if(isOnSale == 2 || isOnSale == 0){
            return wxGoodsService.list(merchantNoSale,goodsSn, name, page, limit, sort, order,
                    brandName, priceMin, platform, priceMax, isHot, isNew, false, categorypid, industryId, goodsType, isRightGoods, placement);
        }else if(isOnSale == 1){
            return wxGoodsService.list(merchantNoSale,goodsSn, name, page, limit, sort, order,
                    brandName, priceMin, platform, priceMax, isHot, isNew, true, categorypid, industryId, goodsType, isRightGoods, placement);
        }
        return wxGoodsService.list(merchantNoSale,goodsSn, name, page, limit, sort, order,
                brandName, priceMin, platform, priceMax, isHot, isNew, true, categorypid, industryId, goodsType, isRightGoods, placement);
    }

    /**
     * 商品添加
     */
    @PostMapping("/create")
    public Object create(@LoginUser String userId, @RequestBody GoodsAllinone goodsAllinone) {
        try {
            wxGoodsService.create(userId,goodsAllinone);
            MallGoods mallGoods = goodsAllinone.getGoods();
            if(mallGoods.getIsOnSale()) {
                String url = URL + goodsAllinone.getGoods().getId();
                HttpRequestUtils.get(url,null,null);
            }
            return ResponseUtil.ok();
        } catch (Exception e) {
            return ResponseUtil.fail(-1, e.getMessage());
        }
    }

    /**
     * 商品编辑
     */
    @PostMapping("/update")
    public Object update(@RequestBody GoodsAllinone goodsAllinone) {
        wxGoodsService.update(goodsAllinone);
        String url = URL + goodsAllinone.getGoods().getId();
        HttpRequestUtils.get(url,null,null);
        return ResponseUtil.ok();
    }

    /**
     * 商品详情
     */
    @GetMapping("/detail")
    public Object detail(@NotNull String id) {
        return wxGoodsService.detail(id);
    }

    /**
     * 商品删除
     */
    @PostMapping("/delete")
    public Object delete(@RequestBody MallGoods goods) {
        wxGoodsService.delete(goods);
        try {
            deleteIndexAboutService.deleteIndexById(goods.getId());
            return ResponseUtil.ok();
        }catch (IOException e){
            log.info(e.toString());
            return ResponseUtil.fail();
        }
    }
}
