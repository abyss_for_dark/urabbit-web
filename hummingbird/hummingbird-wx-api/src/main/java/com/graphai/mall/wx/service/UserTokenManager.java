package com.graphai.mall.wx.service;

import com.graphai.mall.wx.util.JwtHelper;

/**
 * 维护用户token
 */
public class UserTokenManager {
	public static String generateToken(String id,String relationId,String specialId) {
        JwtHelper jwtHelper = new JwtHelper();
        return jwtHelper.createToken(id,relationId,specialId);
    }
    public static String getUserId(String token) {
    	JwtHelper jwtHelper = new JwtHelper();
		String userId = jwtHelper.verifyTokenAndGetUserId(token);
//    	String userId = "5627854115072020480";
    	if(userId == null || "0".equals(userId)){
    		return null;
    	}
        return userId;
    }
    
    public static String getRelationId(String token) {
    	JwtHelper jwtHelper = new JwtHelper();
    	String relationId = jwtHelper.getRelationId(token);
    	if(relationId == null || "0".equals(relationId)){
    		return null;
    	}
        return relationId;
    }
    
    
    public static String getSpecialId(String token) {
    	JwtHelper jwtHelper = new JwtHelper();
    	String userId = jwtHelper.getSpecialId(token);
    	if(userId == null || "0".equals(userId)){
    		return null;
    	}
        return userId;
    }
}
