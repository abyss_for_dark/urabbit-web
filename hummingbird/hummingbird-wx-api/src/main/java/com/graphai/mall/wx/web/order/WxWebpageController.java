package com.graphai.mall.wx.web.order;

import cn.hutool.extra.qrcode.QrCodeUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipaySystemOauthTokenRequest;
import com.alipay.api.response.AlipaySystemOauthTokenResponse;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.json.JacksonUtils;
import com.graphai.mall.db.domain.MallOrder;
import com.graphai.mall.db.service.order.MallOrderService;
import com.graphai.mall.db.util.OrderHandleOption;
import com.graphai.mall.db.util.OrderUtil;
import com.graphai.properties.AliProperties;
import com.graphai.properties.WxProperties;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 网页授权支付
 * @author longx
 * @date 2021/8/109:39
 */
@RestController
@RequestMapping("/wx/webpage")
@Validated
public class WxWebpageController {
    private final Log logger = LogFactory.getLog(WxWebpageController.class);
    public static final String BASE64_PREFIX = "data:image/png;base64,";

    @Autowired
    private MallOrderService mallOrderService;
    @Value("${public.urlPrefix}")
    private String urlPrefix;
    @Autowired
    private WxProperties properties;
    @Autowired
    private AliProperties aliProperties;


    /**
     * 生成扫码图片
     * @return
     */
    @GetMapping("createUrl")
    public Object createUrl(String orderId){
        String url = urlPrefix + "wx/webpage/authorization?orderId=" + orderId;
        String qrcodImg = BASE64_PREFIX + Base64.getEncoder().encodeToString(QrCodeUtil.generatePng(url,250,250));
        return ResponseUtil.ok(qrcodImg);
    }

    /**
     * 授权
     * @param orderId
     * @return
     * @throws Exception
     */
    @GetMapping("authorization")
    public ModelAndView authorization(HttpServletRequest request, String orderId) throws Exception {
        logger.info("-------->h5支付授权：" + JSONObject.toJSONString(orderId));
        MallOrder mallOrder = mallOrderService.findById(orderId);
        if(mallOrder == null){
            throw new Exception("订单错误");
        }
        // 检测是否能够取消
        OrderHandleOption handleOption = OrderUtil.build(mallOrder);
        if (!handleOption.isPay()) {
            throw new Exception("订单不能支付");
        }

        String userAgent = request.getHeader("user-agent");
        // h5回调地址
        String userAppAuthUrl = urlPrefix + "pay/?orderId="+mallOrder.getId();
        String redirectUrl = URLEncoder.encode(userAppAuthUrl, "utf-8");
        String redLocation = "";
        if (userAgent != null && userAgent.contains("AlipayClient")) {
            // 支付宝
            redLocation = "https://openauth.alipay.com/oauth2/publicAppAuthorize.htm?app_id=" + aliProperties.getHsAppId() + "&state=" + orderId + "&scope=auth_base&redirect_uri=" + redirectUrl;
        } else if (userAgent != null && userAgent.contains("MicroMessenger")) {
            // 微信
            redLocation = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=" + properties.getHsAppId() + "&redirect_uri=" + redirectUrl + "&response_type=code&scope=snsapi_base&state=" + orderId + "#wechat_redirect";
        }else{
            redLocation = urlPrefix + "pay/#/pages/pay/error";
        }

        logger.info("--------->/wx/qrcode/store: " + redLocation);
        return new ModelAndView(new RedirectView(redLocation));
    }

    /**
     * 获取微信用户的openId
     * @param code
     * @return
     */
    @GetMapping("getWxOpenId")
    public Object getWxOpenId(String code) {
        logger.info("-------->获取微信用户的openId" + JSONObject.toJSONString(code));

        String appid = properties.getHsAppId();
        String secret = properties.getHsAppSecret();

        String url = "https://api.weixin.qq.com/sns/oauth2/access_token"
                + "?appid=AppId"
                + "&secret=AppSecret"
                + "&code=CODE"
                + "&grant_type=authorization_code";
        url = url.replace("AppId", appid)
                .replace("AppSecret", secret)
                .replace("CODE", code);
        // 根据地址获取请求
        HttpGet request = new HttpGet(url);//这里发送get请求
        // 获取当前客户端对象
        HttpClient httpClient = HttpClientBuilder.create().build();
        // 通过请求对象获取响应对象
        Map<String,Object> map = new HashMap<>();
        try {
            HttpResponse response = httpClient.execute(request);

            String jsonStr = EntityUtils.toString(response.getEntity(), "utf-8");

            logger.info(jsonStr);

            JSONObject jsonTexts = (JSONObject) JSON.parse(jsonStr);
            if (jsonTexts.get("openid") == null) {
                return ResponseUtil.fail(jsonStr);
            }

            map.put("accessToken",jsonTexts.get("access_token"));
            map.put("expiresIn",jsonTexts.get("expires_in"));
            map.put("refreshToken",jsonTexts.get("refresh_token"));
            map.put("openid",jsonTexts.get("openid"));
            map.put("scope",jsonTexts.get("scope"));
        } catch (IOException e) {
            e.printStackTrace();
            logger.error("获取微信openid异常：" + e.getMessage());
        }
        return ResponseUtil.ok(map);
    }

    /**
     * 获取支付宝用户的userId
     * @param code
     * @return
     */
    @GetMapping("getAliPayUserId")
    public Object getAliPayUserId(String code) {
        String userId = "";
        String appid = aliProperties.getHsAppId();
        String privateKey = aliProperties.getHsRsaPrivateKey();
        String publicKey = aliProperties.getHsRsaPublicKey();
        String serverUrl = "https://openapi.alipay.com/gateway.do";
        AlipayClient alipayClient = new DefaultAlipayClient(serverUrl, appid, privateKey, "json", "UTF-8", publicKey, "RSA2");

        AlipaySystemOauthTokenRequest request = new AlipaySystemOauthTokenRequest();
        request.setCode(code);//这个就是第一步获取的auth_code
        request.setGrantType("authorization_code");//这个固定值,参考https://docs.open.alipay.com/api_9/alipay.system.oauth.token
        try {
            AlipaySystemOauthTokenResponse oauthTokenResponse = alipayClient.execute(request);
            userId = oauthTokenResponse.getUserId();
        } catch (AlipayApiException e) {
            logger.error("获取支付宝用户userId异常：" + e.getErrCode() + e.getErrMsg());
        }
        return ResponseUtil.ok(userId);
    }
}
