package com.graphai.mall.wx.dto;


import com.graphai.mall.db.domain.MarketingSignRecord;
import lombok.Data;

@Data
public class MarketingSignRecordDto {

    private MarketingSignRecord  marketingSignRecord;

}
