package com.graphai.mall.wx.web.notice;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.mall.db.service.notice.MallNoticeRelationService;
import com.graphai.mall.wx.annotation.LoginUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/wx/notice", consumes = { MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.APPLICATION_ATOM_XML_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE,
		MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE, MediaType.TEXT_HTML_VALUE, MediaType.TEXT_XML_VALUE, MediaType.APPLICATION_OCTET_STREAM_VALUE })
public class WxNoticeRelationController {
	
	@Autowired
	private MallNoticeRelationService noticeService;
	
	/**
	 * 开户接口
	 * 
	 * @param response
	 * @param request
	 * @param paramJson
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public Object createNewCapitalAccount(HttpServletResponse response, HttpServletRequest request, @LoginUser String userId,
			@RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "30") Integer limit) throws Exception {
		
		Map<String,Object> result = new HashMap<String, Object>();
		
		if (userId == null) {
			return ResponseUtil.unlogin();
		}
		
		List<Map<String,Object>> data =  noticeService.getMyNotice(userId, page, limit);
		Long total = noticeService.getMyNoticeCount(userId);

		Integer totalPages = (int) Math.ceil((double) Integer.parseInt(total.toString()) / limit);
		
		result.put("noticeList", data);
		result.put("totalPages", totalPages);
		
		return ResponseUtil.ok("消息查询成功", result);
	}
}
