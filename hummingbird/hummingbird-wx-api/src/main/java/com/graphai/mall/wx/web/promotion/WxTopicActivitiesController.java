package com.graphai.mall.wx.web.promotion;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.mall.db.service.promotion.MallTopicActivitiesService;

@RestController
@RequestMapping("/wx/topicActivity")
@Validated
public class WxTopicActivitiesController {

    @Autowired
    MallTopicActivitiesService topicActivitiesService;

    @GetMapping("/list")
    public Object getActivitiesByPosition(@NotNull String position) {
        return ResponseUtil.ok(topicActivitiesService.getActivitiesByShowPlace(position));
    }

    @GetMapping("/listV2")
    public Object getActivitiesListByPosition(@NotNull String position) {
        return ResponseUtil.ok(topicActivitiesService.getActivitiesListByShowPlace(position));
    }

}
