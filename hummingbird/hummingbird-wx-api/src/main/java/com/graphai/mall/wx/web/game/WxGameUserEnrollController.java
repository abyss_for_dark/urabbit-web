package com.graphai.mall.wx.web.game;

import com.graphai.commons.util.IpUtil;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.json.JacksonUtil;
import com.graphai.commons.util.json.JacksonUtils;
import com.graphai.mall.db.domain.*;
import com.graphai.mall.db.service.account.AccountService;
import com.graphai.mall.db.constant.account.AccountStatus;
import com.graphai.mall.db.service.game.GameDefinitionService;
import com.graphai.mall.db.service.game.GameJackpotService;
import com.graphai.mall.db.service.game.GameRuleConfigService;
import com.graphai.mall.db.service.game.GameUserEnrollService;
import com.graphai.mall.db.service.order.MallOrderService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.util.account.AccountUtils;
import com.graphai.mall.db.constant.CommonConstant;
import com.graphai.mall.db.vo.GameUserEnrollVo;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.mall.wx.service.WxOrderService;
import com.graphai.payment.enumerate.Channel;
import com.graphai.validator.Order;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * 用户报名
 */
@RestController
@RequestMapping("/wx/gameUserEnroll")
@Validated
public class WxGameUserEnrollController {
    private static final Log logger = LogFactory.getLog(WxGameUserEnrollController.class);

    @Autowired
    private GameUserEnrollService gameUserEnrollService;
    @Autowired
    private GameDefinitionService gameDefinitionService;
    @Autowired
    private GameRuleConfigService gameRuleConfigService;
    @Autowired
    private GameJackpotService gameJackpotService;
    @Autowired
    private MallUserService mallUserService;
    @Autowired
    private WxOrderService wxOrderService;
    @Autowired
    private AccountService accountService;
    @Autowired
    private MallOrderService orderService;


    /**
     * 查询用户报名list
     */
    @GetMapping("/list")
    public Object list(@LoginUser String userId,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit,
                       @RequestParam(defaultValue = "id") String sort,
                       @Order @RequestParam(defaultValue = "desc") String order) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        GameUserEnroll gameUserEnroll = new GameUserEnroll();
        gameUserEnroll.setUserId(userId);
        List<GameUserEnroll> list = gameUserEnrollService.selectGameUserEnrollList(gameUserEnroll, page, limit, sort, order);
        Map<String, Object> data = new HashMap<>();
        data.put("items", list);
        return ResponseUtil.ok(data);
    }

    /**
     * 查询用户报名状态(是否已经报名当天游戏场次)
     */
    @PostMapping("/gameEnrollStatus")
    public Object gameEnrollStatus(@LoginUser String userId,
                             @RequestParam String gameRuleId){
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        if(StringUtils.isEmpty(gameRuleId)){
            return ResponseUtil.badArgumentValue("游戏规则id不能为空!");
        }
        GameRuleConfig gameRuleConfig = gameRuleConfigService.selectGameRuleConfigById(gameRuleId);
        if(gameRuleConfig == null){
            return ResponseUtil.badArgumentValue("请选择正确的游戏!");
        }
        //判断游戏状态
        GameDefinition gameDefinition = gameDefinitionService.selectGameDefinitionById(gameRuleConfig.getGameId());
        if(gameDefinition.getGameStatus()){
            return ResponseUtil.badArgumentValue("出现错误,游戏已经下架!");
        }

        String gameId = gameRuleConfig.getGameId(); //游戏id
        //判断用户明天场次是否已经报名(用户只能当天报名第二天的场次)
        LocalDate tomorrowDate = LocalDate.now().plusDays(1);

        GameUserEnroll gameUserEnroll = new GameUserEnroll();
        gameUserEnroll.setUserId(userId);
        gameUserEnroll.setGameId(gameId);
        gameUserEnroll.setGameRuleId(gameRuleId);
        gameUserEnroll.setEnrollTime(tomorrowDate);  //报名场次
        gameUserEnroll.setEstatus("0");   //未结算
        List<GameUserEnroll> gameUserEnrollList = gameUserEnrollService.selectGameUserEnrollList(gameUserEnroll, null, null, null, null);
        if(gameUserEnrollList.size() > 0){
            return ResponseUtil.ok("你已经报名了明天的场次！");
        }
        return ResponseUtil.ok("你还暂未报名明天的场次！");
    }


    /**
     * 用户报名游戏   {"gameRuleId":"289725241360801792","number":"1"}
     */
    @PostMapping("/gameEnroll")
    @Transactional(rollbackFor = { Exception.class })
    public Object gameEnroll(@LoginUser String userId,
                             @RequestBody String param,
                             HttpServletRequest request) throws Exception {
        String gameRuleId = JacksonUtil.parseString(param, "gameRuleId");
        Integer number = Integer.valueOf(JacksonUtil.parseString(param, "number"));

        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        //查询游戏费用
        if(StringUtils.isEmpty(gameRuleId)){
            return ResponseUtil.badArgumentValue("游戏规则id不能为空!");
        }
        GameRuleConfig gameRuleConfig = gameRuleConfigService.selectGameRuleConfigById(gameRuleId);
        if(gameRuleConfig == null){
            return ResponseUtil.badArgumentValue("请选择正确的游戏!");
        }
        //判断游戏状态
        GameDefinition gameDefinition = gameDefinitionService.selectGameDefinitionById(gameRuleConfig.getGameId());
        if(gameDefinition.getGameStatus()){
            return ResponseUtil.badArgumentValue("出现错误,游戏已经下架!");
        }
        if(gameDefinition.getGameType().equals(CommonConstant.GAME_TYPE_CLOCK)){
            if(number != null){
                if(number <= 0){
                    return ResponseUtil.badArgumentValue("请选择正确的份数！");
                }
            }else{
                return ResponseUtil.badArgumentValue("请选择份数！");
            }
        }
        String gameId = gameRuleConfig.getGameId(); //游戏id
        String amountType = gameRuleConfig.getAmountType(); //费用类型 ( JL20001 金币奖励、JL20002 现金奖励)
        BigDecimal price = gameRuleConfig.getSignAmount().multiply(new BigDecimal(number));  //报名总费用 = 报名费用 * 份数
        //判断用户明天场次是否已经报名(用户只能当天报名第二天的场次)
        LocalDate tomorrowDate = LocalDate.now().plusDays(1);

        GameUserEnroll gameUserEnroll = new GameUserEnroll();
        gameUserEnroll.setUserId(userId);
        gameUserEnroll.setGameId(gameId);
        gameUserEnroll.setGameRuleId(gameRuleId);
        gameUserEnroll.setEnrollTime(tomorrowDate);  //报名场次
        gameUserEnroll.setEstatus("0");   //未结算
        List<GameUserEnroll> gameUserEnrollList = gameUserEnrollService.selectGameUserEnrollList(gameUserEnroll, null, null, null, null);
        if(gameUserEnrollList.size() > 0){
            return ResponseUtil.badArgumentValue("你已经报名了明天的场次!");
        }
        //todo 若没有报名，则扣除用户金额进行报名
        //创建订单
        if(amountType.equals(CommonConstant.TASK_REWARD_GOLD)){
            //金钱(直接创建支付订单)
            FcAccount fcAccount = accountService.getUserAccount(userId);
            if(fcAccount == null){
                return ResponseUtil.badArgumentValue("不存在账户!");
            }
            BigDecimal gold = new BigDecimal(0.00);
            if(fcAccount.getGold() != null){
                gold = fcAccount.getGold();
            }
            if(price.compareTo(gold) == 1){
                return ResponseUtil.badArgumentValue("账户金币不足!");
            }

            //todo 用户账号gold变动记录
            Map<String, Object> paramsMap = new HashMap<>();
            paramsMap.put("message","用户报名游戏");
            paramsMap.put("capitalTrend",AccountStatus.CAPITAL_TREND_02);
            paramsMap.put("changeType",AccountStatus.CHANGE_TYPE_02);
            paramsMap.put("changeAccountType",AccountStatus.CHANGE_ACCOUNT_TYPE_2);
            if(price.compareTo(new BigDecimal(0.00)) == 1){
                AccountUtils.accountChange(userId,price,amountType,AccountStatus.BID_7,paramsMap);
            }

        }else{
            //现金(需要调用微信支付)
            Map<String,Object> map = new HashMap<>();
            map.put("message","用户报名游戏");
            map.put("submitClient","Customer");
            map.put("price",price);
            map.put("bid",AccountStatus.BID_7);
            map.put("gameRuleId",gameRuleId);
            map.put("signNumber",number);
            map.put("enrollTime",tomorrowDate.toString());
            map.put("amountType",amountType);
            String body = JacksonUtils.bean2Jsn(map);
            Object result = wxOrderService.paySubmit(userId, body, IpUtil.getIpAddr(request));
            Map<String, Object> resultMap = JacksonUtils.jsn2map(JacksonUtils.bean2Jsn(result), String.class, Object.class);
            //创建预支付订单
            if(resultMap.get("errno").equals(0) || "0".equals(resultMap.get("errno"))){
                Object data = resultMap.get("data");
                String orderId = JacksonUtils.jsn2map(JacksonUtils.bean2Jsn(data), String.class, Object.class).get("orderId").toString();

                //返回预支付订单
                Map<String,Object> paramMap = new HashMap<>();
                paramMap.put("orderId",orderId);
                paramMap.put("channel", Channel.wx);    //公众号支付
                return wxOrderService.prepay(userId, JacksonUtils.bean2Jsn(paramMap), request);
            }
        }


        //判断明天的场次是否已经存在 奖池
        GameJackpot gameJackpot = new GameJackpot();
        gameJackpot.setGameId(gameId);
        gameJackpot.setGameRuleId(gameUserEnroll.getGameRuleId());
        gameJackpot.setEnrollTime(tomorrowDate);
        List<GameJackpot> gameJackpotList = gameJackpotService.selectGameJackpotList(gameJackpot, null, null, null, null);
        if(gameJackpotList.size() > 1){
            return ResponseUtil.badArgumentValue("存在多条记录!");
        }
        //结算时间
        LocalDateTime settlementTime = LocalDateTime.of(tomorrowDate, LocalTime.of(12,00,00));
        if(gameJackpotList.size() == 0){
            //奖池不存在
            gameJackpot.setJackpotName(tomorrowDate + "期" + gameRuleConfig.getRuleName());
            gameJackpot.setAmountType(amountType);
            gameJackpot.setParticipantsNumber(0);
            gameJackpot.setCopiesNumber(0);
            gameJackpot.setSettlementNumber(0);
            gameJackpot.setJvalue(new BigDecimal(0.00));
            gameJackpot.setRemarks(tomorrowDate + "期"+ gameRuleConfig.getRuleName() + "游戏奖池备注");
            gameJackpot.setSettlementTime(settlementTime);
//            gameJackpot.setSettlementCountdown();     //结算倒计时长
            gameJackpot.setJackpotStartTime(LocalDateTime.now());   //奖池开始时间
            gameJackpotService.insertGameJackpot(gameJackpot);
        }else{
            //奖池存在
            gameJackpot = gameJackpotList.get(0);
        }

        gameUserEnroll.setJackpotId(gameJackpot.getId());   //奖池
        gameUserEnroll.setSettlementTime(settlementTime); //结算时间(第二天 12点)
        gameUserEnroll.setAmountType(amountType);
        gameUserEnroll.setSignAmount(price);
        gameUserEnroll.setSignNumber(number);
        gameUserEnroll.setRemarks(tomorrowDate + "期游戏 用户报名游戏备注");
        if(gameDefinition.getGameType().equals(CommonConstant.GAME_TYPE_STEP)){
            //若报名走路，则默认步数为0
            gameUserEnroll.setGameStep(0);
        }
        gameUserEnrollService.insertGameUserEnroll(gameUserEnroll);

        //游戏奖池变化(报名人数、金额变化)
        GameJackpot gameJackpot1 = new GameJackpot();
        gameJackpot1.setId(gameJackpot.getId());
        gameJackpot1.setParticipantsNumber(gameJackpot.getParticipantsNumber() + 1);    //每报名一人增加一次
        gameJackpot1.setCopiesNumber(gameJackpot.getCopiesNumber() + number);
        gameJackpot1.setJvalue(gameJackpot.getJvalue().add(price));
        gameJackpotService.updateGameJackpot(gameJackpot1);
        return ResponseUtil.ok();
    }

    /**
     * 用户完成游戏(打卡、走路)
     */
    @PostMapping("/gameComplete")
    public Object gameComplete(@LoginUser String userId,
                               @RequestBody String param) {
        String gameRuleId = JacksonUtil.parseString(param, "gameRuleId");
        Integer step = Integer.valueOf(JacksonUtil.parseString(param, "step"));

        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        //判断游戏规则id是否存在
        if(StringUtils.isEmpty(gameRuleId)){
            return ResponseUtil.badArgumentValue("游戏规则id不能为空!");
        }
        GameRuleConfig gameRuleConfig = gameRuleConfigService.selectGameRuleConfigById(gameRuleId);
        if(gameRuleConfig == null){
            return ResponseUtil.badArgumentValue("请选择正确的游戏!");
        }
        //游戏期数
        LocalDate todayDate = LocalDate.now();  //当前日期
        LocalDateTime todayDateTime = LocalDateTime.now();  //当前时间
        //判断用户当天场次是否已经报名
        GameUserEnroll gameUserEnroll = new GameUserEnroll();
        gameUserEnroll.setUserId(userId);
        gameUserEnroll.setGameId(gameRuleConfig.getGameId());
        gameUserEnroll.setGameRuleId(gameRuleId);
        gameUserEnroll.setEnrollTime(todayDate);
        gameUserEnroll.setEstatus("0");   //未结算
        List<GameUserEnroll> gameUserEnrollList = gameUserEnrollService.selectGameUserEnrollList(gameUserEnroll, null, null, null, null);
        if(gameUserEnrollList.size() == 0){
            return ResponseUtil.badArgumentValue("你未报名目前场次的游戏!");
        }

        //判断用户打卡时间是否符合游戏规则的时间段
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String beginTime = todayDate + " " + gameRuleConfig.getRuleBeginTime();//开始时间
        String endTime = todayDate + " " + gameRuleConfig.getRuleEndTime() ;//结束时间
        boolean before = todayDateTime.isBefore(LocalDateTime.parse(beginTime, formatter));
        boolean after = todayDateTime.isAfter(LocalDateTime.parse(endTime, formatter));
        if(before){
            return ResponseUtil.badArgumentValue("打卡失败,未到打卡时间！");
        }
        if(after){
            return ResponseUtil.badArgumentValue("打卡失败,打卡时间已过！");
        }

        //查询游戏类型(打卡、走路)
        gameUserEnroll = gameUserEnrollList.get(0);
        //修改用户数据
        GameUserEnroll updateGameUserEnroll = new GameUserEnroll();
        updateGameUserEnroll.setId(gameUserEnroll.getId());
        GameDefinition gameDefinition = gameDefinitionService.selectGameDefinitionById(gameRuleConfig.getGameId());
        if(gameDefinition.getGameType().equals(CommonConstant.GAME_TYPE_CLOCK)){
            if(!StringUtils.isEmpty(gameUserEnroll.getClockTime())){
                return ResponseUtil.badArgumentValue("打卡失败,你已经打过卡了！");
            }
            updateGameUserEnroll.setClockTime(todayDateTime);
        }else if(gameDefinition.getGameType().equals(CommonConstant.GAME_TYPE_STEP)){
            if(step == null){
                return ResponseUtil.badArgumentValue("未获取到步数！");
            }
            if(step < gameUserEnroll.getGameStep()){
                return ResponseUtil.badArgumentValue("出现错误,发现步数变少了！");
            }
            updateGameUserEnroll.setGameStep(step);
        }

        gameUserEnrollService.updateGameUserEnroll(updateGameUserEnroll);
        return ResponseUtil.ok();
    }

    /**
     * 用户报名游戏——统计页面
     */
    @GetMapping("/getRankByClock")
    public Object getRankByClock(@LoginUser String loginUserId,
                                 @RequestParam("gameRuleId") String gameRuleId,
                                 @RequestParam(defaultValue = "1") Integer page,
                                 @RequestParam(defaultValue = "10") Integer limit){
        if (loginUserId == null) {
            return ResponseUtil.unlogin();
        }
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        //当前场次
        LocalDate today = LocalDate.now();
        //明天场次
        LocalDate tomorrow = today.plusDays(1);
        //昨天场次
        LocalDate yesterday = today.minusDays(1);
        //当前时间
        LocalDateTime now = LocalDateTime.now();
        logger.info("昨天：" + yesterday +",今天：" + today + ",明天：" + tomorrow);

        /** 查询明日打卡奖池情况(奖池未结算) **/
        Integer tomorrowNumber = 0;
        BigDecimal tomorrowJvalue = new BigDecimal(0.00);
        String tomorrowJtype = CommonConstant.TASK_REWARD_CASH;
        Integer userStatus = 1;
        Integer completeStatus = 1;
        GameRuleConfig gameRuleConfig = gameRuleConfigService.selectGameRuleConfigById(gameRuleId);

        String beginTime = today + " " + gameRuleConfig.getRuleBeginTime(); //若用户今日未报名，beginTime显示今日游戏的开始时间,否则显示明日游戏的开始时间
        String endTime = today + " " + gameRuleConfig.getRuleEndTime();
        if(now.isAfter(LocalDateTime.parse(beginTime, formatter)) && now.isBefore(LocalDateTime.parse(endTime, formatter))){
            //待打卡
            completeStatus = 2;
        }


        //查询明日场次报名列表(明日场次报名人list的信息)
        List<MallUser> userList = null;
        GameUserEnroll userEnroll = new GameUserEnroll();
        userEnroll.setGameRuleId(gameRuleId);
        userEnroll.setEnrollTime(tomorrow);
        List<GameUserEnroll> tomorrowGameUserEnrollList = gameUserEnrollService.selectGameUserEnrollList(userEnroll, null, null, "id", "desc");
        List<String> userIds = new ArrayList<>();

        if(tomorrowGameUserEnrollList.size() > 0){
//            GameUserEnroll tomorrowGameUserEnroll = tomorrowGameUserEnrollList.get(0);
//            String userId = tomorrowGameUserEnroll.getUserId();
//            userInfo = mallUserService.findById(userId);
            for (GameUserEnroll gameUserEnroll: tomorrowGameUserEnrollList){
                userIds.add(gameUserEnroll.getUserId());
            }
            userList = mallUserService.findByIds(userIds);
            if(userList.size() > 10){
                //默认10条数据
                userList = userList.subList(0,10);
            }
        }

        /** 查询今日打卡情况(奖池已经结算) **/
        Integer todayCopies = 0;
        Integer todaySettlement = 0;
        Integer todayFail = 0;
        MallUser mostUser = null;
        MallUser firstUser = null;
        MallUser continueUser = null;
        BigDecimal mostReward = new BigDecimal(0.00);
        LocalDateTime firstDateTime = null;
        Integer continueTimes = 0;

        /** 查询今日奖池场次 **/
        GameJackpot gameJackpot = new GameJackpot();
        gameJackpot.setGameRuleId(gameRuleId);
        gameJackpot.setEnrollTime(today);
        List<GameJackpot> todayList = gameJackpotService.selectGameJackpotList(gameJackpot, null, null, null, null);
        Boolean flag = false;
        int status = 3; // 1:游戏已经结算 2:游戏未结算 3:游戏不存在
        LocalDateTime settlementTime = null;    //结算时间
        if(todayList.size() > 0){
            GameJackpot jackpot = todayList.get(0);
            if(jackpot.getJstatus().equals(CommonConstant.GAME_JSSTATUS_2)){
                //当天场次 奖池已经结算
                flag = true;
                status = 1; //游戏已经结算

                todayCopies = jackpot.getCopiesNumber();
                todaySettlement = jackpot.getSettlementNumber();
                todayFail = jackpot.getParticipantsNumber() - todaySettlement;
                //查询今日战况
                GameUserEnroll gameUserEnroll = new GameUserEnroll();
                gameUserEnroll.setJackpotId(jackpot.getId());
                //最多奖励金额用户
                List<GameUserEnroll> gameUserEnrollList = gameUserEnrollService.selectGameUserEnrollListByComplete(gameUserEnroll, CommonConstant.GAME_TYPE_CLOCK, "reward","desc");
                if(gameUserEnrollList.size() > 0){
                    GameUserEnroll enroll = gameUserEnrollList.get(0);
                    String userId = enroll.getUserId();
                    mostUser = mallUserService.findById(userId);
                    mostReward = enroll.getReward();
                }
                //最早打卡用户
                List<GameUserEnroll> enrollList = gameUserEnrollService.selectGameUserEnrollListByComplete(gameUserEnroll, CommonConstant.GAME_TYPE_CLOCK, "clock_time","asc");
                if(enrollList.size() > 0){
                    GameUserEnroll enroll = enrollList.get(0);
                    String userId = enroll.getUserId();
                    firstUser = mallUserService.findById(userId);
                    firstDateTime = enroll.getClockTime();
                }
            }else{
                status = 2; //游戏未结算
                //当天场次 奖池未结算
                settlementTime = jackpot.getSettlementTime();   //结算时间
            }

            /** 判断用户今日场次是否已经报名 **/
            GameUserEnroll gameUserEnroll = new GameUserEnroll();
            gameUserEnroll.setUserId(loginUserId);
            gameUserEnroll.setJackpotId(jackpot.getId());
            List<GameUserEnroll> gameUserEnrollList = gameUserEnrollService.selectGameUserEnrollList(gameUserEnroll, null, null, null, null);
            if(gameUserEnrollList.size() > 0){
                //若用户今日已经报名,userStatus状态为2;若奖池结算时间已过userStatus改为1
                if(now.isBefore(jackpot.getSettlementTime()) ){
                    userStatus = 2;
                }
                GameUserEnroll gameUserEnroll1 = gameUserEnrollList.get(0);
                //ClockTime不为空则显示已经打卡
                if(!StringUtils.isEmpty(gameUserEnroll1.getClockTime())){
                    completeStatus = 3;
                }
                //今日奖池结算时间已过，completeStatus初始化
                if(now.isAfter(jackpot.getSettlementTime()) ){
                    completeStatus = 1;
                }
            }
        }

        /** 查询明日奖池场次 **/
        gameJackpot.setEnrollTime(tomorrow);
        List<GameJackpot> tomorrowList = gameJackpotService.selectGameJackpotList(gameJackpot, null, null, null, null);
        if(tomorrowList.size() > 0){
            GameJackpot jackpot = tomorrowList.get(0);
            tomorrowNumber = jackpot.getParticipantsNumber();
            tomorrowJvalue = jackpot.getJvalue();
            tomorrowJtype = jackpot.getAmountType();

            /** 判断用户明日场次是否已经报名 **/
            GameUserEnroll gameUserEnroll = new GameUserEnroll();
            gameUserEnroll.setUserId(loginUserId);
            gameUserEnroll.setJackpotId(jackpot.getId());
            List<GameUserEnroll> gameUserEnrollList = gameUserEnrollService.selectGameUserEnrollList(gameUserEnroll, null, null, null, null);
            if(gameUserEnrollList.size() > 0){
                beginTime = tomorrow + " " + gameRuleConfig.getRuleBeginTime();
                endTime = tomorrow + " " + gameRuleConfig.getRuleEndTime();
                userStatus = 2;
                completeStatus = 2;
            }
        }


        //打卡次数最多的用户（毅力榜）
        List<GameUserEnrollVo> gameUserEnrollVoList = gameUserEnrollService.selectCompleteTimes(gameRuleId);
        if(gameUserEnrollVoList.size() > 0){
            GameUserEnrollVo gameUserEnrollVo = gameUserEnrollVoList.get(0);
            String userId = gameUserEnrollVo.getUserId();
            continueUser = mallUserService.findById(userId);
            continueTimes = gameUserEnrollVo.getTimes();
        }

        /** 查询昨天打卡情况(奖池已经结算) **/
        Integer yesterdayNumber = 0;
        BigDecimal yesterdayJvalue = new BigDecimal(0.00);
        BigDecimal rate = new BigDecimal(0.00);
        gameJackpot.setEnrollTime(yesterday);
        //上期结果
        List<GameJackpot> yesterdayJackpotList = gameJackpotService.selectGameJackpotList(gameJackpot, null, null, null, null);
        List<GameUserEnrollVo> yesterdayLuckList = null;
        List<GameUserEnrollVo> yesterdayEarlyList = null;
        if(yesterdayJackpotList.size() > 0){
            GameJackpot jackpot = yesterdayJackpotList.get(0);
            yesterdayNumber = jackpot.getParticipantsNumber();
            yesterdayJvalue = jackpot.getJvalue();

            //好运榜
            yesterdayLuckList = gameUserEnrollService.selectLuckList(jackpot.getId());
            //早起榜
            yesterdayEarlyList = gameUserEnrollService.selectClockList(jackpot.getId());
            if(yesterdayEarlyList.size() > 0){
                GameUserEnrollVo gameUserEnrollVo = yesterdayEarlyList.get(0);
                BigDecimal costMoney = gameUserEnrollVo.getCostMoney();
                BigDecimal rewardMoney = gameUserEnrollVo.getRewardMoney();
                rate = rewardMoney.divide(costMoney,2,BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100));
            }

        }


        Map<String,Object> map = new HashMap<>();
        Map<String,Object> topMap = new HashMap<>();
        Map<String,Object> middleMap = new HashMap<>();
        Map<String,Object> bottomMap = new HashMap<>();
        //上
        topMap.put("enrollTime",tomorrow); //场次
        topMap.put("tomorrowJtype",tomorrowJtype); //明日奖池金额类型
        topMap.put("tomorrowJvalue",tomorrowJvalue); //明日奖池金额
        topMap.put("tomorrowNumber",tomorrowNumber); //明日参与人数(当前参与人数)
        topMap.put("yesterdayCopies",todayCopies); //今日场次参与挑战份数(昨天参与挑战份数)
        topMap.put("userList",userList);    //最新报名的用户信息
        topMap.put("nowTime", now.format(formatter));    //服务器当前时间

        topMap.put("beginTime", beginTime);    //游戏开始时间
        topMap.put("endTime", endTime);    //游戏结束时间
        //判断用户是否报名今日场次
        topMap.put("userStatus",userStatus);    //用户是否报名游戏(1:未报名 2:已报名; 结算时间过了userStatus变为1)
        //判断用户是否已经打卡
        topMap.put("completeStatus",completeStatus);    //用户是否已经打卡(1:未打卡 2:待打卡 3:已打卡)

        //中(今日战况)
        if(flag){
            Map<String,Object> mostUserMap = new HashMap<>();
            Map<String,Object> firstUserMap = new HashMap<>();
            Map<String,Object> continueUserMap = new HashMap<>();
            mostUserMap.put("mostUser",mostUser);
            mostUserMap.put("mostReward",mostReward);   //最多奖励
            firstUserMap.put("firstUser",firstUser);
            firstUserMap.put("firstDateTime",firstDateTime);    //最早打卡时间
            continueUserMap.put("continueUser",continueUser);
            continueUserMap.put("continueTimes",continueTimes); //签到最多的次数

            middleMap.put("todaySuccess",todaySettlement);   //今日打卡成功人数
            middleMap.put("todayFail",todayFail);   //今日打卡失败人数

            middleMap.put("mostUserMap",mostUserMap);   //今日奖励最多人信息
            middleMap.put("firstUserMap",firstUserMap);   //今日最早打卡人信息
            middleMap.put("continueUserMap",continueUserMap);   //打卡次数最多人信息
        }else{
            middleMap.put("settlementTime",settlementTime);
        }
        middleMap.put("status",status); // 游戏状态： 1:游戏已经结算 2:游戏未结算 3:游戏不存在
        middleMap.put("gameRuleConfig",gameRuleConfig); //游戏规则配置

        //下
        //下(上期结果)
        bottomMap.put("yesterdayNumber",yesterdayNumber);   //昨天参与人数
        bottomMap.put("yesterdayJvalue",yesterdayJvalue);   //昨天奖池金额
        bottomMap.put("rate",rate); //回报率
        bottomMap.put("luckList",yesterdayLuckList);   //好运榜(上一期)
        bottomMap.put("earlyList",yesterdayEarlyList);   //早起榜(上一期)
        bottomMap.put("continueList",gameUserEnrollVoList);   //毅力榜(总场次)

        map.put("topMap",topMap);
        map.put("middleMap",middleMap);
        map.put("bottomMap",bottomMap);
        return ResponseUtil.ok(map);
    }

    /**
     * 查询我的打卡记录
     */
    @GetMapping("/getRecordByClock")
    public Object getRecordByClock(@LoginUser String userId,
                                   @RequestParam("gameRuleId") String gameRuleId,
                                   @RequestParam(defaultValue = "1") Integer page,
                                   @RequestParam(defaultValue = "10") Integer limit){
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        MallUser user = mallUserService.findById(userId);
        //查询游戏规则配置
        GameRuleConfig gameRuleConfig = gameRuleConfigService.selectGameRuleConfigById(gameRuleId);
        if(gameRuleConfig == null){
            return ResponseUtil.badArgumentValue("游戏不存在！");
        }

        GameUserEnroll gameUserEnroll = new GameUserEnroll();
        gameUserEnroll.setGameRuleId(gameRuleId);
        gameUserEnroll.setUserId(userId);
        List<GameUserEnroll> list = gameUserEnrollService.selectGameUserEnrollList(gameUserEnroll, page, limit, "enroll_time", "desc");
        BigDecimal costMoney = new BigDecimal(0.00);
        BigDecimal costGold = new BigDecimal(0.00);
        BigDecimal earnMoney = new BigDecimal(0.00);
        BigDecimal earnGold = new BigDecimal(0.00);

        for (GameUserEnroll enroll:list){
            if(enroll.getAmountType().equals(CommonConstant.TASK_REWARD_CASH)){
                //金额
                if(enroll.getSignAmount() != null){
                    costMoney = costMoney.add(enroll.getSignAmount());
                }
                if(enroll.getReward() != null){
                    earnMoney = earnMoney.add(enroll.getReward());
                }
            }else{
                //金币
                if(enroll.getSignAmount() != null){
                    costGold = costGold.add(enroll.getSignAmount());
                }
                if(enroll.getReward() != null){
                    earnGold = earnGold.add(enroll.getReward());
                }
            }
        }

        Map<String,Object> map = new HashMap<>();
        map.put("costMoney",costMoney);   //累计投入金额
        map.put("costGold",costGold);   //累计投入金币
        map.put("earnMoney",earnMoney);   //累计赚取金额
        map.put("earnGold",earnGold);   //累计赚取金币

        map.put("list",list);   //打卡记录
        map.put("userInfo",user);   //用户信息
        map.put("endTime",LocalDate.now() + " " + gameRuleConfig.getRuleEndTime());   //今日奖池结算时间(到了结算时间显示‘打卡成功’或‘打卡失败’，未到结算时间显示‘待打卡’)
        return ResponseUtil.ok(map);
    }


//    /**
//     * 系统用户定时报名游戏,同时完成游戏 (定时6点执行)
//     */
//    @Scheduled(cron = "0 0 6 * * ?")
//    @PostMapping("/autoGameEnroll")
//    public Object autoGameEnroll() throws Exception {
//        logger.info("执行了--------");
//        //默认gameRuleId （    289725241360801792(6-8点打卡) 、289726498100109312(1000米走路) ）
//        List<String> gameRuleIdList = new ArrayList<>();
//        gameRuleIdList.add("289725241360801792");
//        gameRuleIdList.add("289726498100109312");
//        List<String> userIdList = new ArrayList<>();
//        userIdList.add("01");    //机器人1
//        userIdList.add("02");    //机器人2
//        userIdList.add("03");    //机器人3
//
//        Integer number = 1; //默认一份
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
//
//        /** 游戏循环 **/
//        for (String gameRuleId: gameRuleIdList){
//            GameRuleConfig gameRuleConfig = gameRuleConfigService.selectGameRuleConfigById(gameRuleId);
//            if(gameRuleConfig == null){
//                return ResponseUtil.badArgumentValue("请选择正确的游戏!");
//            }
//            //判断游戏状态
//            GameDefinition gameDefinition = gameDefinitionService.selectGameDefinitionById(gameRuleConfig.getGameId());
//            if(gameDefinition.getGameStatus()){
//                return ResponseUtil.badArgumentValue("出现错误,游戏已经下架!");
//            }
//
//            String gameId = gameRuleConfig.getGameId(); //游戏id
//            String amountType = gameRuleConfig.getAmountType(); //费用类型 ( JL20001 金币奖励、JL20002 现金奖励)
//            BigDecimal price = gameRuleConfig.getSignAmount().multiply(new BigDecimal(number));  //报名总费用 = 报名费用 * 份数
//            //判断用户明天场次是否已经报名(用户只能当天报名第二天的场次)
//            LocalDate tomorrowDate = LocalDate.now().plusDays(1);
//
//
//            /** 用户循环 **/
//            for (String userId:userIdList){
//                GameUserEnroll gameUserEnroll = new GameUserEnroll();
//                gameUserEnroll.setUserId(userId);
//                gameUserEnroll.setGameId(gameId);
//                gameUserEnroll.setGameRuleId(gameRuleId);
//                gameUserEnroll.setEnrollTime(tomorrowDate);  //报名场次
//
//                List<GameUserEnroll> gameUserEnrollList = gameUserEnrollService.selectGameUserEnrollList(gameUserEnroll, null, null, null, null);
//                if(gameUserEnrollList.size() == 0){
//                    FcAccount fcAccount = accountService.getUserAccount(userId);
//                    if(fcAccount == null){
//                        return ResponseUtil.badArgumentValue("不存在账户!");
//                    }
//
//                    //todo 若没有报名，则扣除用户金额进行报名
//                    //创建订单
//                    if(amountType.equals(CommonConstant.TASK_REWARD_GOLD)){
//                        //金钱(直接创建支付订单)
//                        BigDecimal gold = fcAccount.getGold();
//                        if(price.compareTo(gold) == 1){
//                            return ResponseUtil.badArgumentValue("账户金币不足!");
//                        }
//
//                        //todo 用户账号gold变动记录
//                        Map<String, Object> paramsMap = new HashMap<>();
//                        paramsMap.put("message","用户报名游戏");
//                        paramsMap.put("capitalTrend",AccountStatus.CAPITAL_TREND_02);
//                        paramsMap.put("changeType",AccountStatus.CHANGE_TYPE_02);
//                        AccountUtils.accountChange(userId,price,amountType,AccountStatus.BID_7,paramsMap);
//
//
//                    }else{
//                        //金钱(直接创建支付订单、预支付订单)
//                        BigDecimal amount = fcAccount.getAmount();
//                        if(price.compareTo(amount) == 1){
//                            return ResponseUtil.badArgumentValue("账户余额不足!");
//                        }
//
//                        //现金(需要调用微信支付)
//                        Map<String,Object> map = new HashMap<>();
//                        map.put("message","系统用户报名游戏");
//                        map.put("submitClient","Customer");
//                        map.put("price",price);
//                        map.put("bid",AccountStatus.BID_7);
//                        map.put("gameRuleId",gameRuleId);
//                        map.put("signNumber",number);
//                        map.put("enrollTime",tomorrowDate.toString());
//                        map.put("amountType",amountType);
//                        String body = JacksonUtils.bean2Jsn(map);
//                        Object result = wxOrderService.paySubmit(userId, body, "127.0.0.1");
//                        Map<String, Object> resultMap = JacksonUtils.jsn2map(JacksonUtils.bean2Jsn(result), String.class, Object.class);
//
//                        if(resultMap.get("errno").equals(0) || "0".equals(resultMap.get("errno"))){
//                            //自动支付,不需要创建预支付订单
//                            Object data = resultMap.get("data");
//                            String orderId = JacksonUtils.jsn2map(JacksonUtils.bean2Jsn(data), String.class, Object.class).get("orderId").toString();
//
//                            Map<String, Object> paramsMap = new HashMap<>();
//                            MallOrder mallOrder = orderService.findById(orderId);
//                            paramsMap.put("orderId",orderId);
//                            paramsMap.put("orderSn",mallOrder.getOrderSn());
//                            paramsMap.put("message","用户报名游戏");
//                            paramsMap.put("capitalTrend",AccountStatus.CAPITAL_TREND_02);
//                            paramsMap.put("changeType",AccountStatus.CHANGE_TYPE_02);
//                            AccountUtils.accountChange(userId,price,amountType,AccountStatus.BID_7,paramsMap);
//
//
//                            //修改订单状态
//                            MallOrder order = new MallOrder();
//                            order.setId(orderId);
//                            order.setPayTime(LocalDateTime.now());
//                            order.setOrderStatus(OrderUtil.STATUS_PAY);
//                            orderService.updateByPrimaryKeySelective(order);
//                        }
//                    }
//
//
//                    //判断明天的场次是否已经存在 奖池
//                    GameJackpot gameJackpot = new GameJackpot();
//                    gameJackpot.setGameId(gameId);
//                    gameJackpot.setGameRuleId(gameUserEnroll.getGameRuleId());
//                    gameJackpot.setEnrollTime(tomorrowDate);
//                    List<GameJackpot> gameJackpotList = gameJackpotService.selectGameJackpotList(gameJackpot, null, null, null, null);
//                    if(gameJackpotList.size() > 1){
//                        return ResponseUtil.badArgumentValue("存在多条记录!");
//                    }
//                    //结算时间
//                    LocalDateTime settlementTime = LocalDateTime.of(tomorrowDate, LocalTime.of(12,00,00));
//                    if(gameJackpotList.size() == 0){
//                        //奖池不存在
//                        gameJackpot.setJackpotName(tomorrowDate + "期" + gameRuleConfig.getRuleName());
//                        gameJackpot.setAmountType(amountType);
//                        gameJackpot.setParticipantsNumber(0);
//                        gameJackpot.setCopiesNumber(0);
//                        gameJackpot.setSettlementNumber(0);
//                        gameJackpot.setJvalue(new BigDecimal(0.00));
//                        gameJackpot.setRemarks(tomorrowDate + "期"+ gameRuleConfig.getRuleName() + "游戏奖池备注");
//                        gameJackpot.setSettlementTime(settlementTime);
////            gameJackpot.setSettlementCountdown();     //结算倒计时长
//                        gameJackpot.setJackpotStartTime(LocalDateTime.now());   //奖池开始时间
//                        gameJackpotService.insertGameJackpot(gameJackpot);
//                    }else{
//                        //奖池存在
//                        gameJackpot = gameJackpotList.get(0);
//                    }
//
//                    gameUserEnroll.setJackpotId(gameJackpot.getId());   //奖池
//                    gameUserEnroll.setSettlementTime(settlementTime); //结算时间(第二天 12点)
//                    gameUserEnroll.setAmountType(amountType);
//                    gameUserEnroll.setSignAmount(price);
//                    gameUserEnroll.setSignNumber(number);
//                    gameUserEnroll.setRemarks(tomorrowDate + "期游戏 系统用户报名游戏备注");
//                    if(gameDefinition.getGameType().equals(CommonConstant.GAME_TYPE_STEP)){
//                        //todo 后面改为随机步数
//
//                        gameUserEnroll.setGameStep(gameRuleConfig.getStepCount());
//                    }else{
//                        //todo 后面改为随机打卡时间
//
//                        String clockTime = tomorrowDate + " " + gameRuleConfig.getRuleBeginTime();
//                        LocalDateTime newClockTime = LocalDateTime.parse(clockTime, formatter);
//                        gameUserEnroll.setClockTime(newClockTime);
//                    }
//                    gameUserEnrollService.insertGameUserEnroll(gameUserEnroll);
//
//                    //游戏奖池变化(报名人数、金额变化)
//                    GameJackpot gameJackpot1 = new GameJackpot();
//                    gameJackpot1.setId(gameJackpot.getId());
//                    gameJackpot1.setParticipantsNumber(gameJackpot.getParticipantsNumber() + 1);    //每报名一人增加一次
//                    gameJackpot1.setCopiesNumber(gameJackpot.getCopiesNumber() + number);
//                    gameJackpot1.setJvalue(gameJackpot.getJvalue().add(price));
//                    gameJackpotService.updateGameJackpot(gameJackpot1);
//                }
//
//            }
//        }
//
//
//        return ResponseUtil.ok();
//    }
}
