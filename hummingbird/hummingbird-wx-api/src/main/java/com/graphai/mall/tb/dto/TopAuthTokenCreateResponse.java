package com.graphai.mall.tb.dto;

public class TopAuthTokenCreateResponse {

	
	private String tokenResult;
	private String requestId;
	public String getTokenResult() {
		return tokenResult;
	}
	public void setTokenResult(String tokenResult) {
		this.tokenResult = tokenResult;
	}
	public String getRequestId() {
		return requestId;
	}
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	
	
}
