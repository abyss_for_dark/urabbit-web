package com.graphai.mall.wx.web.other;

import static com.graphai.mall.wx.util.WxResponseCode.GOODS_UNKNOWN;
import static com.graphai.mall.wx.util.WxResponseCode.GROUPON_EXPIRED;
import static com.graphai.mall.wx.util.WxResponseCode.ORDER_INVALID;
import static com.graphai.mall.wx.util.WxResponseCode.ORDER_UNKNOWN;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import javax.validation.constraints.NotNull;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.json.JSONObject;
import com.graphai.mall.db.constant.group.GroupEnum;
import com.graphai.mall.db.vo.GroupFloatingWindowVo;
import com.graphai.mall.db.vo.GroupVo;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.houbb.sensitive.core.api.SensitiveUtil;
import com.github.pagehelper.PageInfo;
import com.graphai.commons.constant.JetcacheConstant;
import com.graphai.commons.util.NameUtil;
import com.graphai.commons.util.PushUtil;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.json.JacksonUtil;
import com.graphai.express.ExpressService;
import com.graphai.express.dao.ExpressInfo;
import com.graphai.mall.admin.domain.MallRedQrcodeRecord;
import com.graphai.mall.admin.service.IMallRedQrcodeRecordService;
import com.graphai.commons.util.json.JacksonUtils;
import com.graphai.mall.db.domain.MallGoods;
import com.graphai.mall.db.domain.MallGroupon;
import com.graphai.mall.db.domain.MallGrouponRules;
import com.graphai.mall.db.domain.MallOrder;
import com.graphai.mall.db.domain.MallOrderGoods;
import com.graphai.mall.db.domain.MallUser;
import com.graphai.mall.db.domain.UserVo;
import com.graphai.mall.db.service.goods.MallGoodsService;
import com.graphai.mall.db.service.groupon.MallGrouponRulesService;
import com.graphai.mall.db.service.groupon.MallGrouponService;
import com.graphai.mall.db.service.order.MallOrderGoodsService;
import com.graphai.mall.db.service.order.MallOrderService;
import com.graphai.mall.db.service.sysconfig.MallSystemConfigService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.util.OrderUtil;
import com.graphai.mall.db.util.StringHelper;
import com.graphai.mall.db.util.UserUtils;
import com.graphai.mall.db.vo.MallGroupVo;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.nosql.redis.NewCacheInstanceServiceLoader;
import com.graphai.spi.mq.BasedMQProducer;
import com.graphai.spi.mq.MQBaseConstant;
import com.graphai.validator.Order;
import com.graphai.validator.Sort;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;

/**
 * 团购服务
 * <p>
 * 需要注意这里团购规则和团购活动的关系和区别。
 */
@RestController
@RequestMapping("/wx/groupon")
@Validated
public class WxGrouponController {
    private final Log logger = LogFactory.getLog(WxGrouponController.class);

    @Autowired
    private MallSystemConfigService mallSystemConfigService;
    @Autowired
    private MallGrouponRulesService rulesService;
    @Autowired
    private MallGrouponService grouponService;
    @Autowired
    private MallGoodsService goodsService;
    @Autowired
    private MallOrderService orderService;
    @Autowired
    private MallOrderGoodsService orderGoodsService;
    @Autowired
    private MallUserService userService;
    @Autowired
    private ExpressService expressService;
    private DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    @Autowired
    private BasedMQProducer __basedMQProducer;

    @Autowired
    private IMallRedQrcodeRecordService iMallRedQrcodeRecordService;





    /**
     * 团购规则列表
     *
     * @param page 分页页数
     * @param limit 分页大小
     * @return 团购规则列表
     */
    @GetMapping("list")
    public Object list(@RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit, @Sort @RequestParam(defaultValue = "id") String sort,
                    @Order @RequestParam(defaultValue = "desc") String order) {
        List<Map<String, Object>> topicList = rulesService.queryList(page, limit, sort, order);
        long total = PageInfo.of(topicList).getTotal();
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("data", topicList);
        data.put("count", total);
        return ResponseUtil.ok(data);
    }

    /**
     * 团购活动详情
     *
     * @param userId 用户ID
     * @param grouponId 团购活动ID
     * @return 团购活动详情
     */
    @GetMapping("detail")
    public Object detail(@LoginUser String userId, @NotNull String grouponId) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }

        MallGroupon groupon = grouponService.queryById(grouponId);
        if (groupon == null) {
            return ResponseUtil.badArgumentValue();
        }

        MallGrouponRules rules = rulesService.queryById(groupon.getRulesId());
        if (rules == null) {
            return ResponseUtil.badArgumentValue();
        }

        // 订单信息
        MallOrder order = orderService.findById(groupon.getOrderId());
        if (null == order) {
            return ResponseUtil.fail(ORDER_UNKNOWN, "订单不存在");
        }
        if (!order.getUserId().equals(userId)) {
            return ResponseUtil.fail(ORDER_INVALID, "不是当前用户的订单");
        }
        Map<String, Object> orderVo = new HashMap<String, Object>();
        orderVo.put("id", order.getId());
        orderVo.put("orderSn", order.getOrderSn());
        orderVo.put("addTime", order.getAddTime());
        orderVo.put("consignee", order.getConsignee());
        orderVo.put("mobile", order.getMobile());
        orderVo.put("address", order.getAddress());
        orderVo.put("goodsPrice", order.getGoodsPrice());
        orderVo.put("freightPrice", order.getFreightPrice());
        orderVo.put("actualPrice", order.getActualPrice());
        orderVo.put("orderStatusText", OrderUtil.orderStatusText(order));
        orderVo.put("handleOption", OrderUtil.build(order));
        orderVo.put("expCode", order.getShipChannel());
        orderVo.put("expNo", order.getShipSn());

        List<MallOrderGoods> orderGoodsList = orderGoodsService.queryByOid(order.getId());
        List<Map<String, Object>> orderGoodsVoList = new ArrayList<>(orderGoodsList.size());
        for (MallOrderGoods orderGoods : orderGoodsList) {
            Map<String, Object> orderGoodsVo = new HashMap<>();
            orderGoodsVo.put("id", orderGoods.getId());
            orderGoodsVo.put("orderId", orderGoods.getOrderId());
            orderGoodsVo.put("goodsId", orderGoods.getGoodsId());
            orderGoodsVo.put("goodsName", orderGoods.getGoodsName());
            orderGoodsVo.put("number", orderGoods.getNumber());
            orderGoodsVo.put("retailPrice", orderGoods.getPrice());
            orderGoodsVo.put("picUrl", orderGoods.getPicUrl());
            orderGoodsVo.put("goodsSpecificationValues", orderGoods.getSpecifications());
            orderGoodsVoList.add(orderGoodsVo);
        }

        Map<String, Object> result = new HashMap<>();
        result.put("orderInfo", orderVo);
        result.put("orderGoods", orderGoodsVoList);

        // 订单状态为已发货且物流信息不为空
        // "YTO", "800669400640887922"
        if (order.getOrderStatus().equals(OrderUtil.STATUS_SHIP)) {
            ExpressInfo ei = expressService.getExpressInfo(order.getShipChannel(), order.getShipSn());
            result.put("expressInfo", ei);
        }

        UserVo creator = userService.findUserVoById(groupon.getCreatorUserId());
        List<UserVo> joiners = new ArrayList<>();
        joiners.add(creator);
        String linkGrouponId;
        // 这是一个团购发起记录
        if ("0".equals(groupon.getGrouponId())) {
            linkGrouponId = groupon.getId();
        } else {
            linkGrouponId = groupon.getGrouponId();

        }
        List<MallGroupon> groupons = grouponService.queryJoinRecord(linkGrouponId);

        UserVo joiner;
        for (MallGroupon grouponItem : groupons) {
            joiner = userService.findUserVoById(grouponItem.getUserId());
            joiners.add(joiner);
        }

        result.put("linkGrouponId", linkGrouponId);
        result.put("creator", creator);
        result.put("joiners", joiners);
        result.put("groupon", groupon);
        result.put("rules", rules);
        return ResponseUtil.ok(result);
    }

    /**
     * 参加团购
     *
     * @param grouponId 团购活动ID
     * @return 操作结果
     */
    @GetMapping("join")
    public Object join(@NotNull String grouponId) {
        MallGroupon groupon = grouponService.queryById(grouponId);
        if (groupon == null) {
            return ResponseUtil.badArgumentValue();
        }

        MallGrouponRules rules = rulesService.queryById(groupon.getRulesId());
        if (rules == null) {
            return ResponseUtil.badArgumentValue();
        }

        MallGoods goods = goodsService.findById(rules.getGoodsId());
        if (goods == null) {
            return ResponseUtil.badArgumentValue();
        }

        Map<String, Object> result = new HashMap<>();
        result.put("groupon", groupon);
        result.put("goods", goods);

        return ResponseUtil.ok(result);
    }

    /**
     * 用户开团或入团情况
     *
     * @param userId 用户ID
     * @param showType 显示类型，如果是0，则是当前用户开的团购；否则，则是当前用户参加的团购
     * @return 用户开团或入团情况
     */
    @GetMapping("my")
    public Object my(@LoginUser String userId, @RequestParam(defaultValue = "0") Integer showType) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }

        List<MallGroupon> myGroupons;
        if (showType == 0) {
            myGroupons = grouponService.queryMyGroupon(userId);
        } else {
            myGroupons = grouponService.queryMyJoinGroupon(userId);
        }

        List<Map<String, Object>> grouponVoList = new ArrayList<>(myGroupons.size());

        MallOrder order;
        MallGrouponRules rules;
        MallUser creator;
        for (MallGroupon groupon : myGroupons) {
            order = orderService.findById(groupon.getOrderId());
            rules = rulesService.queryById(groupon.getRulesId());
            creator = userService.findById(groupon.getCreatorUserId());

            Map<String, Object> grouponVo = new HashMap<>();
            // 填充团购信息
            grouponVo.put("id", groupon.getId());
            grouponVo.put("groupon", groupon);
            grouponVo.put("rules", rules);
            grouponVo.put("creator", creator.getNickname());

            String linkGrouponId;
            // 这是一个团购发起记录
            if ("0".equals(groupon.getGrouponId())) {
                linkGrouponId = groupon.getId();
                grouponVo.put("isCreator", creator.getId() == userId);
            } else {
                linkGrouponId = groupon.getGrouponId();
                grouponVo.put("isCreator", false);
            }
            int joinerCount = grouponService.countGroupon(linkGrouponId);
            grouponVo.put("joinerCount", joinerCount + 1);

            // 填充订单信息
            grouponVo.put("orderId", order.getId());
            grouponVo.put("orderSn", order.getOrderSn());
            grouponVo.put("actualPrice", order.getActualPrice());
            grouponVo.put("orderStatusText", OrderUtil.orderStatusText(order));
            grouponVo.put("handleOption", OrderUtil.build(order));

            List<MallOrderGoods> orderGoodsList = orderGoodsService.queryByOid(order.getId());
            List<Map<String, Object>> orderGoodsVoList = new ArrayList<>(orderGoodsList.size());
            for (MallOrderGoods orderGoods : orderGoodsList) {
                Map<String, Object> orderGoodsVo = new HashMap<>();
                orderGoodsVo.put("id", orderGoods.getId());
                orderGoodsVo.put("goodsName", orderGoods.getGoodsName());
                orderGoodsVo.put("number", orderGoods.getNumber());
                orderGoodsVo.put("picUrl", orderGoods.getPicUrl());
                orderGoodsVoList.add(orderGoodsVo);
            }
            grouponVo.put("goodsList", orderGoodsVoList);
            grouponVoList.add(grouponVo);
        }

        Map<String, Object> result = new HashMap<>();
        result.put("count", grouponVoList.size());
        result.put("data", grouponVoList);

        return ResponseUtil.ok(result);
    }



    /**
     * 参团或邀请进团
     *
     * @param data 团购ID
     * @return 参团或邀请进团
     */
    @PostMapping("joinOrInvite")
    public Object joinOrInvite(@LoginUser String userId, @RequestBody String data) {
        // 手机号
        String mobile = JacksonUtil.parseString(data, "mobile");

        String groupId = JacksonUtil.parseString(data, "groupId");

        boolean group = false;
        if (null == userId) {
            Object result = UserUtils.getTokenUserId(mobile);
            if (!Objects.equals(result, null)) {
                userId = String.valueOf(result);
            }
        }

        if (Objects.equals(groupId, null)) {
            return ResponseUtil.fail(401, "团购id不能为空");
        }
        // 查看是否是开团
        MallGroupon baseGroupon = grouponService.queryById(groupId);
        if (baseGroupon == null){
            return ResponseUtil.fail(-1, "客官请稍等!");
        }
        List<MallOrderGoods> orderGoods = null;
        MallGroupon mallGroupon = grouponService.queryById(groupId);
        String orderId = mallGroupon.getOrderId();
        //如果是团长订单
        if ("0".equalsIgnoreCase(mallGroupon.getGrouponId())) {
            List<MallGroupVo> mallGroupVos = grouponService.queryGroupByIdV2(groupId, null ,null);
            for (MallGroupVo mallGroupVo : mallGroupVos) {
                if (! "00".equalsIgnoreCase(mallGroupVo.getOrderId())) {
                    orderId = mallGroupVo.getOrderId();
                    break;
                }
            }
        }
        List<MallOrder> mallOrders = orderService.findByUserIdAndGroupId(orderId);
        if (! Objects.isNull(mallOrders) && mallOrders.size() > 0) {
            orderGoods = orderGoodsService.queryByOid(mallOrders.get(0).getId());
        }

        String userJoinOrderId="";
        Map<String, Object> map = new HashMap<String, Object>();

        MallGrouponRules rules = rulesService.queryByIdV2(baseGroupon.getRulesId());
        if (rules == null) {
            return ResponseUtil.badArgumentValue();
        }

        // 团购活动已经过期
        if (rulesService.isExpired(rules)) {
            return ResponseUtil.fail(GROUPON_EXPIRED, "团购活动已过期!");
        }

        if (null != baseGroupon) {
            if (GroupEnum.GROUP_COMMANDER.getCode().equalsIgnoreCase(baseGroupon.getGrouponId())) {
                //机器人开团 ，订单随机数
                logger.info("团长进入/joinOrInvite ，baseGroupon.getGrouponId()=" + baseGroupon.getGrouponId());
//                orderId = null;
            } else {
                groupId = baseGroupon.getGrouponId();
            }
        }
        // 查看参团成员
        List<MallGroupVo> groupList = grouponService.queryGroupByIdV2(groupId, null, null);
        // 是邀请进来还是参团成功
        boolean flag = false;
        for (MallGroupVo mapList : groupList) {
           if (!mapList.getOrderId().equalsIgnoreCase("00")){
               List<MallOrder> mallOrders1 = orderService.selectList(mapList.getOrderId(), 2);
               mapList.setOrderId(mallOrders1.get(0).getId());
              /* if (Objects.equals(mapList.getGroupId(), "0")) {
                   expireTime = mapList.getAddTime();
               }*/
               if (StringHelper.isNotNullAndEmpty(userId)) {
                   if (Objects.equals(mapList.getUserId(), userId)) {
                       flag = true;
                       userJoinOrderId= mallOrders1.get(0).getId();
                   }
               }
           }
        }
        if (orderGoods != null && orderGoods.size()>0){
            map.put("order", orderGoods.get(0));
        }
        //分享者的参团
        MallOrder order = orderService.findById(mallOrders.get(0).getId());
        map.put("orderThreeStatus", order.getOrderThreeStatus());
        if (500==order.getOrderStatus()) {
            map.put("orderThreeStatus", order.getOrderStatus());
        }
        if (Objects.equals(199, order.getOrderThreeStatus()) || Objects.equals(200, order.getOrderThreeStatus())
                        || Objects.equals(500, order.getOrderThreeStatus())) {
            MallGroupon groupon = grouponService.queryById(groupId);
            if (!Objects.equals(null, groupon.getUpdateTime())
                            && groupon.getUpdateTime().isAfter(groupon.getAddTime())) {
                map.put("openGroupTime", groupon.getUpdateTime());
            }
        }
        if (flag) {
            //当前进去此页面的用户拼团状态
            MallOrder ownOrder = orderService.findById(userJoinOrderId);
            map.put("orderThreeStatus", ownOrder.getOrderThreeStatus());
            if (500==ownOrder.getOrderStatus()) {
                map.put("orderThreeStatus", ownOrder.getOrderStatus());
            }

        }
        // 当前用户参团下单信息
        if (!Objects.equals(null, orderId)) {
            List<MallOrderGoods> mallOrderGoods = orderGoodsService.queryByOid(orderId);
            if (CollectionUtils.isNotEmpty(mallOrderGoods)) {
                // 团长购买商品的规格
                map.put("specifications", mallOrderGoods.get(0).getSpecifications());
            }
        }

        map.put("groups", groupList);
        if (rules.getDiscountMember() == groupList.size()) {
            group = true;
            map.put("group", group);
        } else {
            map.put("surplusNum", rules.getDiscountMember() - groupList.size() < 0 ? 0
                            : rules.getDiscountMember() - groupList.size());
        }

        map.put("expireTime", baseGroupon.getExpirationTime());
        map.put("isJoin", flag);
        return ResponseUtil.ok(map);
    }



    /**
     * 时间戳转换为日期字符串
     *
     * @param time
     * @return
     */
    private static String convertTimeToFormat(Long time) {
        Assert.notNull(time, "time is null");
        DateTimeFormatter ftf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return ftf.format(LocalDateTime.ofInstant(Instant.ofEpochMilli(time), ZoneId.systemDefault()));
    }

    /**
     * 团购列表
     */
    @GetMapping("getGrouponList")
    public Object getGroupon(@LoginUser String userId, @RequestParam String rulesId, @RequestParam(defaultValue = "1") Integer page,
                             @RequestParam(defaultValue = "10") Integer limit) {
        MallGrouponRules rules = rulesService.queryByIdV2(rulesId);
        List<GroupVo> groupList = grouponService.queryListByPage(rulesId, (page - 1) * 10, limit);
        List<String> groupIds = new ArrayList<String>();
        for (GroupVo groupVo : groupList) {
            groupIds.add((groupVo.getGroupId()));
        }
        //查看此商品参团的全部记录
        List<MallGroupon> joinList = grouponService.queryJoinNum(groupIds);
        for (GroupVo groupVo : groupList) {
            if (com.baomidou.mybatisplus.core.toolkit.CollectionUtils.isNotEmpty(joinList)) {
                //参团任务
                int joinNum = 0;
                //累计算出每个团参团总人数
                for (MallGroupon mallGroupon : joinList) {
                    if (Objects.equals((groupVo.getGroupId()), mallGroupon.getId()) || Objects.equals((groupVo.getGroupId()), mallGroupon.getGrouponId())) {
                        joinNum++;
                    }
                    groupVo.setSurplusNum(rules.getDiscountMember() - joinNum);
                }
            }
        }
        Map<String, Object> map = new HashMap<>();
        map.put("groupList", groupList);
        map.put("rules", rules);
        return ResponseUtil.ok(map);
    }

    /**
     * 团购用户列表
     */
    @GetMapping("getGrouponUserList")
    public Object getGrouponUserList(@LoginUser String userId, @RequestParam String groupId) {
        return ResponseUtil.ok(grouponService.queryGroupByIdV2(groupId, null, null));
    }

    /**
     * 团购列表
     *
     */
    @PostMapping("wlkQueryList")
    public Object wlkQueryList(@LoginUser String userId, @RequestBody String data) {
        List<GroupVo> groups;
        Integer num = null;
        Integer hour = null;
        Map<String, Object> resulMap = new HashMap<String, Object>();
        try {
            String rulesId = JacksonUtil.parseString(data, "rulesId");
            String mobile = JacksonUtil.parseString(data, "mobile");

            MallGrouponRules rules = rulesService.queryByIdV2(rulesId);
            Integer groupNum = rules.getGroupNum();
            if (rules == null) {
                return ResponseUtil.badArgumentValue();
            }
            if (!Objects.equals(rules.getDiscountMember(), null)) {
                num = rules.getDiscountMember();
            }

            if (!Objects.equals(rules.getDuration(), null)) {
                hour = rules.getDuration();
            }
            if (StrUtil.isEmpty(userId)) {
                Object result = UserUtils.getTokenUserId(mobile);
                if (!Objects.equals(result, null)) {
                    userId = String.valueOf(result);
                }
            }
            groups = grouponService.queryListByPage(rulesId, hour, 0, 1);
            List<GroupVo> temp = new ArrayList<>();
            List<MallGroupon> joinList=null;
            List<String> groupIds=new ArrayList<String>();
            if (CollectionUtils.isNotEmpty(groups) && groups.size() > 0) {
                groupIds.add(groups.get(0).getGroupId());
                //查看此商品参团的全部记录
                joinList = grouponService.queryJoinNum(groupIds);
                groups.get(0).setSurplusNum(num - joinList.size());
            }

            for (int i = 0; i < groups.size(); i++) {
                boolean own = false;
                if (StrUtil.isNotEmpty(userId)) {
                    Boolean isV = false;
                    List<MallGroupon> groupId = grouponService.queryIsGroup(String.valueOf(groups.get(i).getGroupId()));
                    if (null != groupNum && groupNum == 1) {
                        List<String> list = new ArrayList<>();
                        MallGroupon grouponUser = null;
                        for (MallGroupon mallGroupon : groupId) {
                            list.add(mallGroupon.getUserId());
                            grouponUser = mallGroupon;
                        }
                        List<MallUser> mallUserList = userService.findByIds(list);
                        for (MallUser mallUser : mallUserList) {
                            if (mallUser.getId().equalsIgnoreCase(grouponUser.getUserId())) {
                                Map<String, Object> map = new HashMap<>();
                                map.put("userId", mallUser.getId());
                                map.put("avatar", mallUser.getAvatar());
                                map.put("nickname", NameUtil.nameDesensitizationDef(mallUser.getNickname()));
                                map.put("addGrouponTime", grouponUser.getAddTime());
                                resulMap.put("newGrouponUser", map);
                            }
                        }
                        resulMap.put("mallUserList", mallUserList);
                    }
                    for (MallGroupon mallGroupon : groupId) {
                        if (mallGroupon.getUserId().equals(userId)) {
                            isV = true;
                        }
                    }
                    if (isV) {
                        own = true;
                        groups.get(i).setOwn(own);
                        groups.get(i).setNickName(NameUtil.nameDesensitizationDef((String) groups.get(i).getNickName()));
                        temp.add(0, groups.get(i));
                        continue;
                    }
                }
                temp.add(groups.get(i));
            }
            resulMap.put("groupNum", groupNum);
            resulMap.put("groupList", temp);
            resulMap.put("totalJoinNum", grouponService.countGroupByRules(rulesId));
            resulMap.put("grouponRules", rules);

            List<Map<String, Object>> maps = grouponService.queryGroupNotOpenInfo(rulesId, userId);
            if (maps != null && maps.size() > 0) {
                resulMap.put("groupNotOpenInfo", maps.get(0));
            } else {
                resulMap.put("groupNotOpenInfo", null);
            }
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return ResponseUtil.ok(resulMap);

    }


    /**
     * 获取开团id
     *
     * @return
     */
    @PostMapping("getGroupId")
    public Object getGroupId(@RequestBody String data) {
        try {
            String orderId = JacksonUtil.parseString(data, "orderId");
            if (Objects.equals(orderId, null)) {
                return ResponseUtil.fail(401, "订单不能为空");
            }
            MallGroupon groupon = grouponService.queryByPayOrderId(orderId);
            String groupId = "";
            if (null != groupon) {
                if (Objects.equals("0", groupon.getGrouponId())) {
                    groupId = groupon.getId();
                } else {
                    groupId = groupon.getGrouponId();
                }

            }
            return ResponseUtil.ok(groupId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseUtil.fail();
    }



    /**
     * 一键预约
     *
     */
    @PostMapping("appointmentGroup")
    public Object appointmentGroup(@LoginUser String userId) {


        if (userId == null) {
            return ResponseUtil.unlogin();
        }


        try {


            MallGrouponRules rules = rulesService.queryGroupRulesByAsc("0,1");


            if (Objects.equals(rules, null)) {
                return ResponseUtil.fail(401, "团购规则为空!");
            }
            if (1 == rules.getStatus()) {
                return ResponseUtil.ok();
            }

            // 参与者
            MallGroupon invitedgroup = grouponService.queryJoinGrouponByUserId(userId, rules.getId());


            if (!Objects.equals(invitedgroup, null)) {
                return ResponseUtil.fail(402, "您已参团!");
            }


            Integer max = rules.getDiscountMember(); // 满团人数
            Integer preNum = (int) Math.rint((max * 0.7)); // 预成团人数


            // 参团总人数
            Long joinNum = grouponService.queryGroupCountByRulesId(rules.getId());
            Integer joInteger = joinNum.intValue();

            Map<String, Object> map = new HashMap<String, Object>(5);
            if (null != joInteger && joInteger.intValue() == preNum.intValue() && !"1".equals(rules.getPicUrl())) {
                map.put("groupId", rules.getId());
                map.put("ruleId", rules.getRuleId());
                rules.setPicUrl("1");
                rulesService.updateById(rules);

                Map<String, Object> mqParams = new HashMap<>();
                mqParams.put("param", JacksonUtils.bean2Jsn(map));
                mqParams.put("bizKey", "");
                mqParams.put("tags", MQBaseConstant.RED_GROUP_ENVELOPE_TAG);
                __basedMQProducer.send(mqParams);
                //wxProducerService.produceMsg(JacksonUtils.bean2Jsn(map), null, MQBaseConstant.RED_GROUP_ENVELOPE_TAG);
            } else if (null != joInteger && joInteger.intValue() == max.intValue()) {
                map.put("rulesId", rules.getId());
                rules.setStatus(1);
                rulesService.updateById(rules);

                Map<String, Object> mqParams = new HashMap<>();
                mqParams.put("param", JacksonUtils.bean2Jsn(map));
                mqParams.put("bizKey", "");
                mqParams.put("tags", MQBaseConstant.RED_GROUP_100_ENVELOPE_TAG);
                __basedMQProducer.send(mqParams);

                //wxProducerService.produceMsg(JacksonUtils.bean2Jsn(map), null, MQBaseConstant.RED_GROUP_100_ENVELOPE_TAG);
                return ResponseUtil.fail(401, "参团人数已满!");
            }



            MallGroupon groupon = new MallGroupon();
            groupon.setUserId(userId);
            groupon.setRulesId(rules.getId());
            groupon.setCreatorUserId(userId);
            groupon.setGrouponId("0");
            groupon.setJoinNum(1);
            grouponService.createGroupon(groupon);


            return ResponseUtil.ok(groupon);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return ResponseUtil.fail();
    }



    /**
     * 参团
     *
     * @return
     */
    @PostMapping("joinGroup")
    public Object joinGroup(@LoginUser String userId) {


        if (userId == null) {
            return ResponseUtil.unlogin();
        }



        try {



            MallGrouponRules rules = rulesService.queryGroupRulesByAsc("0,1");


            if (Objects.equals(rules, null)) {
                return ResponseUtil.fail(401, "团购规则为空!");
            }
            if (null == rules.getStatus() || 1 == rules.getStatus()) {
                return ResponseUtil.ok();
            }

            // 参与者
            MallGroupon invitedgroup = grouponService.queryJoinGrouponByUserId(userId, rules.getId());


            if (!Objects.equals(invitedgroup, null)) {
                logger.info("用户已参团");
                return ResponseUtil.fail("用户已参团");
            }


            MallGroupon groupon = new MallGroupon();
            groupon.setUserId(userId);
            groupon.setRulesId(rules.getId());
            groupon.setCreatorUserId(userId);
            groupon.setGrouponId("0");
            groupon.setJoinNum(1);
            grouponService.createGroupon(groupon);


            return ResponseUtil.ok(groupon);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return ResponseUtil.fail();
    }



    /**
     * 显示最新被邀请人
     *
     * @return
     */
    @PostMapping("showInvited")
    public Object showInvited(@LoginUser String userId) {


        if (userId == null) {
            return ResponseUtil.unlogin();
        }



        try {



            MallGrouponRules rules = rulesService.queryGroupRulesByAsc("0,1");


            if (Objects.equals(rules, null)) {
                return ResponseUtil.fail(401, "团购规则为空!");
            }


            // 参与者
            MallGroupon invitedgroup = grouponService.queryJoinGrouponByUserId(userId, rules.getId());


            if (null != invitedgroup && !Objects.equals(invitedgroup.getCreatorUserId(), invitedgroup.getUserId())) {
                if (Objects.equals(invitedgroup.getIsShow(), false)) {
                    invitedgroup.setIsShow(true);
                    grouponService.updateById(invitedgroup);
                    String data = null;

                    MallUser user = userService.findById(invitedgroup.getUserId());
                    if (!Objects.equals(user.getNickname(), null)) {

                        data = NameUtil.nameDesensitizationDef(user.getNickname());


                    } else {
                        data = NameUtil.mobileEncrypt(user.getMobile());
                    }

                    return ResponseUtil.ok(data);

                }
            }



        } catch (Exception e) {
            e.printStackTrace();
        }

        return ResponseUtil.ok();
    }



    /**
     * 显示被邀请人列表
     *
     * @return
     */
    @PostMapping("showInvitedList")
    public Object showInvitedList(@LoginUser String userId) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        Map<String, Object> data = new HashMap<String, Object>(3);
        boolean flag = false;
        List<Map<String, Object>> temp = new ArrayList<Map<String, Object>>();
        try {
            MallGrouponRules rules = rulesService.queryGroupRulesByAsc("0,1");
            if (Objects.equals(rules, null)) {
                return ResponseUtil.fail(401, "团购规则为空!");
            }

            // 是否参团
            MallGroupon invitedgroup = grouponService.queryJoinGrouponByUserId(userId, rules.getId());
            if (!Objects.equals(invitedgroup, null)) {
                flag = true;
            }
            data.put("isJoin", flag);

            // 邀请者列表
            List<Map<String, Object>> invitedList = grouponService.queryGroupbyRulesIdAndUserId(rules.getId(), userId);
            data.put("count", invitedList.size() == 0 ? 0 : invitedList.size() - 1);
            if (CollectionUtils.isNotEmpty(invitedList)) {
                for (int i = 0; i < invitedList.size(); i++) {
                    if (Objects.equals(invitedList.get(i).get("userId"), userId)) {
                        invitedList.remove(invitedList.get(i));
                        i--;
                        continue;
                    }
                    if (!Objects.equals(invitedList.get(i).get("nickName"), null)) {
                        invitedList.get(i).put("nickName", NameUtil.nameDesensitizationDef(String.valueOf(invitedList.get(i).get("nickName"))));
                    } else {
                        invitedList.get(i).put("nickName", NameUtil.mobileEncrypt(String.valueOf(invitedList.get(i).get("mobile"))));
                    }

                    if (Objects.equals(invitedList.get(i).get("avatar"), null)) {
                        invitedList.get(i).put("avatar", "https://gxfacepay.oss-cn-shenzhen.aliyuncs.com/hummingbird/app/static/img/config/she.png");
                    }
                    temp.add(invitedList.get(i));
                }
            }
            data.put("list", temp);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseUtil.ok(data);
    }


    /**
     * 拼团邀请排行榜
     *
     * @return
     */
    @GetMapping("/rankList")
    public Object rankList(@LoginUser String userId) {
        try {
            List<Map<String, Object>> rankList = grouponService.queryRankList();
            if (CollectionUtils.isNotEmpty(rankList)) {
                for (int i = 0; i < rankList.size(); i++) {
                    if (!Objects.equals(rankList.get(i).get("nickName"), null)) {
                        rankList.get(i).put("nickName", NameUtil.nameDesensitizationDef(String.valueOf(rankList.get(i).get("nickName"))));
                    } else {
                        rankList.get(i).put("nickName", NameUtil.mobileEncrypt(String.valueOf(rankList.get(i).get("mobile"))));
                    }
                }
            }
            rankList = SensitiveUtil.desCopyCollection(rankList);
            // 获取当前时间，大于指定时间点触发
            int hour = DateUtil.hour(new Date(), true);
            Map<String, String> redPacketRuleMap = mallSystemConfigService.redPacketRule();// 红包规则
            String mallRedGroupShowRankDate = redPacketRuleMap.get("mall_red_group_showRankDate");
            if (StrUtil.isNotEmpty(mallRedGroupShowRankDate) && Integer.valueOf(mallRedGroupShowRankDate).intValue() > hour) {
                return ResponseUtil.ok(new ArrayList<>());
            }
            return ResponseUtil.ok(rankList);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("获取列表失败", e);
        }
        return ResponseUtil.ok(new ArrayList<>());
    }



    /**
     * 倒计时
     *
     * @return
     */
    @GetMapping("countDown")
    public Object countDown(@LoginUser String userId) {
        if (null == userId) {
            return ResponseUtil.unlogin();
        }
        Map<String, Object> data = new HashMap<String, Object>(3);
        try {
            String ruleStatus = "0,1,2";
            MallGrouponRules rules = rulesService.queryGroupRulesByAsc(ruleStatus);
            if (Objects.equals(rules, null)) {
                return ResponseUtil.fail(401, "团购规则为空!");
            }
            Integer status = 0;
            // 触发大额领取的时间戳
            Long startTimestamp;
            // 大额失效的时间戳
            Long endTimestamp = 0L;
            // 参团总人数
            Integer joinNum = (int) grouponService.queryGroupCountByRulesId(rules.getId());

            if (null == joinNum) {
                joinNum = 0;
            }
            Integer type = 1;
            if (1 == rules.getStatus()) {
                if (null == rules.getUpdateTime()) {
                    logger.error("获取成团时间失败！");
                    return ResponseUtil.fail("活动已结束");
                }
                status = 1;
                // 推送放入缓存
                Object pushObj = NewCacheInstanceServiceLoader.objectCache().get(rules.getId());
                if (null == pushObj) {
                    NewCacheInstanceServiceLoader.objectCache().put(rules.getId(), rules.getId());
                    PushUtil.pushMessageToApp("", "[未莱生活送大额红包] 本场开奖啦！", "/pages/game/red-bad-group", null, null, null);
                }
                // 2.判断用户领取大额还是整点红包金额
                MallRedQrcodeRecord mallRedQrcodeRecord =
                                iMallRedQrcodeRecordService.getOne(new QueryWrapper<MallRedQrcodeRecord>().eq("group_id", rules.getId()).eq("deleted", "0").eq("amount_type", "07").eq("to_user_id", userId));
                if (null != mallRedQrcodeRecord && 2 == mallRedQrcodeRecord.getIsReceived()) {
                    type = 2;
                }

                // 倒计时分钟转毫秒数
                long exTime = TimeUnit.MILLISECONDS.convert(rules.getDuration(), TimeUnit.MINUTES);

                String timestampConstant = JetcacheConstant.bigRedEndTimestampConstant(rules.getId());
                // 触发新春红包当前的时间戳
                startTimestamp = rules.getUpdateTime().toInstant(ZoneOffset.ofHours(8)).toEpochMilli();
                // 新春失效的时间戳
                endTimestamp = startTimestamp + exTime;

                // HashMap<String, Object> map = new HashMap<>();
                // map.put("endTimestamp", endTimestamp);
                // groupCache.PUT_IF_ABSENT(timestampConstant, map, (rules.getDuration()), TimeUnit.MINUTES);
                // Map<String, Object> map2 = groupCache.get(timestampConstant);
                // if (MapUtils.isNotEmpty(map2)) {
                // Long timestamp = (Long) map2.get("endTimestamp");
                // endTimestamp = timestamp;
                // } else {
                // 缓存失效即活动过期
                // 领取状态 0：未触发 1：已触发 2：已领完
                // }
                if (endTimestamp < System.currentTimeMillis()) {
                    status = 2;
                    rules.setStatus(status);
                    rules.setDeleted(true);
                    rulesService.updateById(rules);
                }
            } else {
                grouponService.joinGroup(userId, false);
            }

            // 1新春红包 2整点红包
            data.put("type", type);
            // 剩余人数
            data.put("num", rules.getDiscountMember() - joinNum);
            // 新春失效的时间戳
            data.put("endTimestamp", endTimestamp);
            // 当前服务器时间戳
            data.put("currentTimestamp", System.currentTimeMillis());
            // 0未开始(未满团) 1进行中(已满团) 2已结束)
            data.put("status", status);
            return ResponseUtil.ok(data);
        } catch (Exception e) {
            logger.error("获取倒计时失败，", e);
        }
        return ResponseUtil.ok(data);
    }

    /**
     * 查询团是否拼成功
     *
     * @return 团购列表
     */
    @PostMapping("getGroupIsSuccess")
    public Object getGroupIsSuccess(@LoginUser String userId, @RequestBody String data) {
        try {
            if (StrUtil.isEmpty(userId)) {
                return ResponseUtil.unlogin();
            }
            Map<String, Object> map = new HashMap<String, Object>();
            String groupId = JacksonUtil.parseString(data, "groupId");

            if (Objects.equals(groupId, null)) {
                return ResponseUtil.fail(401, "团购id不能为空");
            }

            // 查看是否是开团
            MallGroupon baseGroupon = grouponService.queryById(groupId);

            if (!Objects.equals("0", baseGroupon.getGrouponId())) {
                return ResponseUtil.fail(503, "groupId不是开团Id");
            }


            MallGrouponRules rules = rulesService.queryByIdV2(baseGroupon.getRulesId());
            if (rules == null) {
                return ResponseUtil.badArgumentValue();
            }

            // 团购活动已经过期
            if (rulesService.isExpired(rules)) {
                return ResponseUtil.fail(GROUPON_EXPIRED, "团购活动已过期!");
            }


            // 查看参团成员
            List<Map<String, Object>> groupList = grouponService.queryGroupById(groupId);
            if (rules.getDiscountMember() <= groupList.size()) {
                return ResponseUtil.fail(503, "团已满人");
            } else {
                return ResponseUtil.ok();
            }
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }

        return ResponseUtil.fail();


    }


    /**
     * 查询团剩余多少人可参团
     * @return 剩余人数
     */
    @PostMapping("getsurplusNum")
    public Object getsurplusNum(@RequestBody String data) {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            String groupId = JacksonUtil.parseString(data, "groupId");
            if (Objects.equals(groupId, null)) {
                return ResponseUtil.fail(401, "团购id不能为空");
            }
            // 查看是否是开团
            MallGroupon baseGroupon = grouponService.queryById(groupId);
            if (null != baseGroupon) {
                if (Objects.equals("0", baseGroupon.getGrouponId())) {
                    groupId = baseGroupon.getId();
                } else {
                    groupId = baseGroupon.getGrouponId();
                }
            }
            MallGrouponRules rules = rulesService.queryByIdV2(baseGroupon.getRulesId());
            if (rules == null) {
                return ResponseUtil.badArgumentValue();
            }
            // 团购活动已经过期
            if (rulesService.isExpired(rules)) {
                return ResponseUtil.fail(GROUPON_EXPIRED, "团购活动已过期!");
            }
            // 查看参团成员
            List<Map<String, Object>> groupList = grouponService.queryGroupById(groupId);
            if (rules.getDiscountMember() >= groupList.size()) {
                map.put("surplusNum", rules.getDiscountMember() - groupList.size());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseUtil.ok(map);


    }

    /**
     * 商品所对应的团购规则
     *
     * @param goodsId 商品ID
     * @return 团购规则详情
     */
    @GetMapping("query")
    public Object query(@NotNull String goodsId) {
        MallGoods goods = goodsService.findById(goodsId);
        if (goods == null) {
            return ResponseUtil.fail(GOODS_UNKNOWN, "未找到对应的商品");
        }

        List<MallGrouponRules> rules = rulesService.queryByGoodsId(goodsId);

        return ResponseUtil.ok(rules);
    }


    @PostMapping("updateGroupMark")
    public Object updateGroupMark(@LoginUser String userId,@RequestBody JSONObject body){
        int i = grouponService.updateGroupMark(userId, body.getStr("grouponId"));
        if (i>0){
            logger.info("团购标识更新成功");
            return ResponseUtil.ok();
        }
        logger.info("团购标识更新失败");
        return ResponseUtil.fail("团购标识更新失败");
    }


    /**
     * 获取团购浮动信息列表

     */
    @GetMapping("getFloatingWindowList")
    public Object getFloatingWindowList() {
        List<GroupFloatingWindowVo> list = new ArrayList<>();
        List<GroupVo> groupVoList = userService.getRobotUserIds(RandomUtil.randomInt(0, 500), 50);
        List<String> groupList = grouponService.getGroupList(50);
        for (GroupVo groupVo : groupVoList) {
            GroupFloatingWindowVo groupFloatingWindowVo = new GroupFloatingWindowVo();
            groupFloatingWindowVo.setNickName(NameUtil.nameDesensitizationDef(groupVo.getNickName()));
            groupFloatingWindowVo.setAvatar(groupVo.getAvatar());
            int num = RandomUtil.randomInt(0,100);
            //     * 0.5-1 20%
            //     * 1-10  50%
            //     * 10-50 30%
            //     * 保留2位小数
            if (num > 79) {
                groupFloatingWindowVo.setAmount(RandomUtil.randomBigDecimal(new BigDecimal("0.5"), new BigDecimal("1")).setScale(2, BigDecimal.ROUND_HALF_UP));
            } else if (num > 29) {
                groupFloatingWindowVo.setAmount(RandomUtil.randomBigDecimal(new BigDecimal("1"), new BigDecimal("10")).setScale(2, BigDecimal.ROUND_HALF_UP));
            } else {
                groupFloatingWindowVo.setAmount(RandomUtil.randomBigDecimal(new BigDecimal("10"), new BigDecimal("50")).setScale(2, BigDecimal.ROUND_HALF_UP));
            }
           String goodsName = groupList.get(RandomUtil.randomInt(0, groupList.size() - 1));
            groupFloatingWindowVo.setGoodsName(goodsName.length() > 6 ? goodsName.substring(0, 6) + "..." : goodsName);
            if (RandomUtil.randomInt(0, 100) > 70) {
                groupFloatingWindowVo.setDescribe("刚拼中" + groupFloatingWindowVo.getGoodsName());
            } else {
                groupFloatingWindowVo.setDescribe("获得" + groupFloatingWindowVo.getAmount() + "元拼团红包奖励");
            }
            list.add(groupFloatingWindowVo);
        }
        return ResponseUtil.ok(list);
    }

}
