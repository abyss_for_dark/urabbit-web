package com.graphai.mall.wx.web.other;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.validation.constraints.NotNull;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Header;
import org.apache.http.message.BasicHeader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.graphai.commons.constant.MallConstant;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.json.JacksonUtils;
import com.graphai.mall.db.domain.MallAd;
import com.graphai.mall.db.domain.MallBrand;
import com.graphai.mall.db.service.advertisement.MallAdService;
import com.graphai.mall.db.service.advertisement.MallBrandService;
import com.graphai.mall.db.util.StringHelper;
import com.graphai.open.api.getway.HttpRequestUtils;
import com.graphai.properties.PlatFormServerproperty;


/**
 * 专题服务
 */
@RestController
@RequestMapping("/wx/brand")
@Validated
public class WxBrandController {
    private final Log logger = LogFactory.getLog(WxBrandController.class);

    @Autowired
    private MallBrandService brandService;

    @Autowired
    private PlatFormServerproperty platFormServerproperty;

    @Autowired
    private MallAdService adService;

    /**
     * 品牌列表
     *
     * @param page 分页页数
     * @param limit 分页大小
     * @return 品牌列表
     */
    @GetMapping("list")
    public Object list(@RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit, @RequestParam(defaultValue = "10") String industryId,
                    String keyword) {

        // List<MallBrand> brandList = brandService.queryVO(page, limit);
        List<Map<String, Object>> brandList = brandService.getActivityList(page, limit, industryId, keyword);
        // int total = brandService.queryTotalCount();
        int total = brandService.getActivityCount(industryId, keyword);
        int totalPages = (int) Math.ceil((double) total / limit);

        Map<String, Object> data = new HashMap<String, Object>();
        data.put("brandList", brandList);
        data.put("totalPages", totalPages);
        return ResponseUtil.ok(data);
    }

    /**
     * 品牌详情
     *
     * @param id 品牌ID
     * @return 品牌详情
     */
    @GetMapping("detail")
    public Object detail(@NotNull String id) {
        MallBrand entity = brandService.findById(id);
        if (entity == null) {
            return ResponseUtil.badArgumentValue();
        }

        Map<String, Object> data = new HashMap<String, Object>();
        data.put("brand", entity);
        return ResponseUtil.ok(data);
    }


    /**
     * 根据类目id查找品牌
     *
     * @param id 品牌ID
     * @return 根据类目id查找品牌
     * @throws Exception
     */
    @GetMapping("getBrandByCid")
    public Object getBrandByCid(@RequestParam(defaultValue = "110", required = true) String cid) throws Exception {
        try {
            String url = platFormServerproperty.getUrl(MallConstant.TAOBAO);
            Map<String, Object> mappMap = new HashMap<String, Object>();
            mappMap.put("plateform", "dtk");
            mappMap.put("cids", cid);


            Map<String, Object> data = new HashMap<String, Object>();

            List<MallAd> adList = adService.queryIndex((byte) 111);

            data.put("ad", adList);
            Header[] headers = new BasicHeader[] {new BasicHeader("method", "queryBrandByCid"), new BasicHeader("version", "v1.0")};
            String result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);

            JSONObject object = JSONObject.parseObject(result);

            if (object.getString("errno").equals("0")) {
                List<MallBrand> brands = JSON.parseObject(object.get("data").toString(), List.class);
                if ("110".equals(cid)) {
                    // 精选打乱顺序
                    Collections.shuffle(brands);
                }
                data.put("brandList", brands);
            }

            return ResponseUtil.ok(data);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw e;
        }

    }


    /**
     * 根据类目id查找品牌
     *
     * @param cid 品牌ID cid首次为空
     * @return 根据类目id查找品牌
     * @throws Exception
     */
    @GetMapping("getPDDBrandByCid")
    public Object getPDDBrandByCid(String cid, @RequestParam String offset, @RequestParam String pageSize) throws Exception {
        if (StringHelper.isNullOrEmpty(offset)) {
            offset = "1";
        }
        if (StringHelper.isNullOrEmpty(pageSize)) {
            pageSize = "10";
        }
        try {
            String url = platFormServerproperty.getUrl(MallConstant.PINDUODUO);
            Map<String, Object> mappMap = new HashMap<String, Object>();
            mappMap.put("plateform", "pdd");
            mappMap.put("cid", cid);
            mappMap.put("offset", offset);
            mappMap.put("pageSize", pageSize);

            Map<String, Object> data = new HashMap<String, Object>();

            List<MallAd> adList = adService.queryIndex((byte) 111);

            data.put("adList", adList);
            Header[] headers = new BasicHeader[] {new BasicHeader("method", "queryBrandByCid"), new BasicHeader("version", "v1.0")};
            String result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);

            JSONObject object = JSONObject.parseObject(result);

            if (object != null && Objects.equals(object.getString("errno"), "0")) {
                Map map = (Map) object.get("data");
                data.putAll(map);
            }
            return ResponseUtil.ok(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseUtil.fail();


    }

    /**
     * 跳转到店铺 通过店铺ID
     */
//    @GetMapping("jumpBrandById")
//    public Object jumpBrandById(@LoginUser String userId, @RequestParam @NotNull String shopId) {
//        if (StringHelper.isNullOrEmpty(userId)) {
//            return ResponseUtil.unlogin();
//        }
//        if (StringHelper.isNullOrEmpty(shopId)) {
//            return ResponseUtil.badArgumentValue();
//        }
//        try {
//            PopClient client = new PopHttpClient(PlateFromConstant.PDD_CLIENT_ID, PlateFromConstant.PDD_CLIENT_SECRET);
//            PddDdkMallUrlGenRequest request = new PddDdkMallUrlGenRequest();
//            JSONObject uid = new JSONObject();
//            uid.put("uid", userId);
//            request.setCustomParameters(uid.toString());
//            request.setMallId(Long.parseLong(shopId));
//            request.setPid(PlateFromConstant.PDD_PID);
//            PddDdkMallUrlGenResponse response = client.syncInvoke(request);
//            if (StringHelper.isNullOrEmpty(response.getErrorResponse())) {
//                String mobileShortUrl = response.getMallCouponGenerateUrlResponse().getList().get(0).getMobileShortUrl();
//                return ResponseUtil.ok(mobileShortUrl);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return ResponseUtil.fail();
//    }


}
