package com.graphai.mall.wx.web.redpacket;


import static com.graphai.commons.util.servlet.ServletUtils.getRequest;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.graphai.commons.constant.JetcacheConstant;
import com.graphai.commons.util.NameUtil;
import com.graphai.commons.util.PushUtil;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.ip.IpUtils;
import com.graphai.commons.util.json.JacksonUtil;
import com.graphai.commons.util.json.JacksonUtils;
import com.graphai.commons.util.lock.MutexLockUtils;
import com.graphai.commons.util.rsa.RSA;
import com.graphai.id.GraphaiIdGenerator;
import com.graphai.mall.admin.domain.MallRedQrcodeRecord;
import com.graphai.mall.admin.domain.MallSystemRule;
import com.graphai.mall.admin.service.IGameRedHourRuleService;
import com.graphai.mall.admin.service.IMallRedQrcodeRecordService;
import com.graphai.mall.db.domain.GameRedpacketRain;
import com.graphai.mall.db.domain.MallGrouponRules;
import com.graphai.mall.db.domain.MallUser;
import com.graphai.mall.db.service.game.GameRedpacketRainService;
import com.graphai.mall.db.service.groupon.MallGrouponRulesService;
import com.graphai.mall.db.service.groupon.MallGrouponService;
import com.graphai.mall.db.service.red.WxRedRankService;
import com.graphai.mall.db.service.red.WxRedService;
import com.graphai.mall.db.service.sysconfig.MallSystemConfigService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.mall.wx.util.AESUtils;
import com.graphai.mall.wx.web.common.MyBaseController;
import com.graphai.nosql.redis.NewCacheInstanceServiceLoader;
import com.graphai.spi.mq.BasedMQProducer;
import com.graphai.spi.mq.MQBaseConstant;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;

@RestController
@RequestMapping("/wx/gamejackpot/V2")
@Validated
public class WxJackpotRedPacketControllerV2 extends MyBaseController {


    private final Log logger = LogFactory.getLog(WxJackpotRedPacketControllerV2.class);


    @Autowired
    private GameRedpacketRainService gameRedpacketRainService;
    @Autowired
    private MallUserService mallUserService;
    @Autowired
    private MallSystemConfigService mallSystemConfigService;
    @Autowired
    private IMallRedQrcodeRecordService iMallRedQrcodeRecordService;
    // @Autowired
    private BasedMQProducer _MQProducer;
    @Autowired
    private IGameRedHourRuleService redHourRuleService;
    @Autowired
    private MallGrouponRulesService mallGrouponRulesService;
    @Autowired
    private MallGrouponRulesService rulesService;
    @Autowired
    private MallGrouponService grouponService;

    @Autowired
    private WxRedService wxRedService;
    @Autowired
    private WxRedRankService wxRedRankService;

    @Value("${mall.wx.mpappid}")
    private String appid;
    @Value("${weiRed.app}")
    private String app;
    @Value("${weiRed.privateKey}")
    private String privateKey;
    @Value("${weiRed.publicKey}")
    private String publicKey;
    @Value("${weiRed.qrcodeUrl}")
    private String requestUrl;

    /**
     * 整点红包弹幕+疯抢排行
     *
     * @param
     * @throws IOException
     */
    @GetMapping("/barrage")
    public Object barrageList(@LoginUser String userId) {
        if (null == userId) {
            return ResponseUtil.unlogin();
        }
        /** 弹幕列表 */
        // List<Map<String, Object>> maps = gameRedpacketRainService.barrageList();// 方法1：数据库查询方法
        List<Map<String, Object>> maps = wxRedRankService.getRedRankingCommon("3");// 方法2：redis缓存方法

        /** 排行榜 */
        // List<Map<String, Object>> list1 = gameRedpacketRainService.barrageList1(); //方法1：数据库读取方法
        // List<Map<String, Object>> list1 =new ArrayList<Map<String,Object>>(); //方法2：临时解决，直接返回[]
        List<Map<String, Object>> list1 = wxRedRankService.getRedRankingCommon("1"); // 方法3：redis缓存方法

        /** 当日累计领取 */
        BigDecimal totalAmountByUser = iMallRedQrcodeRecordService.getTotalAmountByUser(userId);
        if (null != maps && maps.size() > 0) {
            for (Map<String, Object> map : maps) {
                map.put("nickname", NameUtil.nameDesensitizationDef(String.valueOf(map.get("nickname"))));
            }
        }
        if (null != list1 && list1.size() > 0) {
            for (Map<String, Object> map : list1) {
                map.put("nickname", NameUtil.nameDesensitizationDef(String.valueOf(map.get("nickname"))));
            }
        }
        Map<String, Object> map = new HashMap<>();
        map.put("barrage", maps);
        map.put("list", list1);
        map.put("totalAmount", totalAmountByUser);
        return ResponseUtil.ok(map);
    }

    /**
     * 领取整点红包
     */
    @GetMapping("/receive")
    public Object receive(@LoginUser String userId, @RequestParam String time, @RequestParam(defaultValue = "") String makerId, HttpServletRequest request) throws Exception {

        if (null == userId) {
            return ResponseUtil.unlogin();
        }


        try {
            synchronized (MutexLockUtils.getMutexLock(userId)) {
                String device = "";
                String userAgent = getRequest().getHeader("user-agent");
                if (null != userAgent && userAgent.contains("iPhone")) {
                    device = "iPhone";
                } else {
                    device = "Android";
                }
                /** 查询当前时间段的红包总金额 **/
                String time1 = time + ":00";
                GameRedpacketRain grain = gameRedpacketRainService.selectGameRedpacket(time1);
                /** 当前时间段有没有领取过 **/
                int receiveNumber = iMallRedQrcodeRecordService.count(new QueryWrapper<MallRedQrcodeRecord>().eq("to_user_id", userId).eq("type_of_claim", "01").eq("type", "02").eq("redpacket_id", grain.getId())
                                .between("create_time", LocalDateTime.of(LocalDateTime.now().toLocalDate(), LocalTime.MIN), LocalDateTime.of(LocalDateTime.now().toLocalDate(), LocalTime.MAX)));
                /** 当天领取多少次 **/
                int receiveDayNumber = iMallRedQrcodeRecordService.count(new QueryWrapper<MallRedQrcodeRecord>().eq("to_user_id", userId).eq("type_of_claim", "01").eq("type", "02").between("create_time",
                                LocalDateTime.of(LocalDateTime.now().toLocalDate(), LocalTime.MIN), LocalDateTime.of(LocalDateTime.now().toLocalDate(), LocalTime.MAX)));

                /** 说明该时间段已经领取过红包 **/
                if (receiveNumber > 0) {
                    return ResponseUtil.fail(403, "您在该时间段已经领取过红包！等待下一个时间段再次领取吧");
                }

                /** 获取整点红包配置规则 **/
                List<MallSystemRule> listRule = redHourRuleService.list(new QueryWrapper<MallSystemRule>().eq("rule_id", 1).eq("status", 0));
                MallUser mallUser = mallUserService.findById(userId);
                /** 通过规则随机获取红包点中的段位 **/
                MallSystemRule mallSystemRule = wxRedService.getRedIndex(listRule, mallUser.getIsTalent(), receiveDayNumber);
                /** 取出具体的配置段 **/
                // MallSystemRule mallSystemRule = listRule.get(redIndex);

                /** 为了兼容老规则 **/
                Map<String, String> map = mallSystemConfigService.redPacketRule();
                /** 整点红包区间1 给一个初始值防止出错后没有值 **/
                String packet1 = "0.01";
                /** 整点红包区间2 给一个初始值防止出错后没有值 **/
                String packet2 = "0.01";
                String rule = map.get("mall_red_rule");

                /** 看当前启用的是新规则还是旧规则 1 用配置表 2 新的规则 **/
                if ("1".equals(rule)) {
                    packet1 = map.get("mall_red_packet1");
                    packet2 = map.get("mall_red_packet2");
                } else {
                    packet1 = mallSystemRule.getStartSection().toString();
                    packet2 = mallSystemRule.getEndSection().toString();
                }
                /** 通过红包规则段获取最终领取金额 **/
                Random random = new Random();
                DecimalFormat df1 = new DecimalFormat("0.00");
                // 原金额
                BigDecimal residuer = new BigDecimal(df1.format(random.nextDouble() * (Double.parseDouble(packet2) - Double.parseDouble(packet1)) + Double.parseDouble(packet1)));
                // 达人金额
                BigDecimal talResiduer = residuer;
                /** 判断是否达人 */
                if ("1".equals(mallUser.getIsTalent())) {
                    // （新：新的规则表）(保留两位)
                    talResiduer = residuer.multiply(new BigDecimal(mallSystemRule.getReserveTwo())).setScale(2, BigDecimal.ROUND_DOWN);
                }
                Map<String, Object> datamap = new HashMap<>();
                String endtime = grain.getEndTime();// 红包结束时间
                LocalDate now = LocalDate.now();// 当前日期
                String end = now.toString() + " " + endtime;

                DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                LocalDateTime nowtime = LocalDateTime.now();// 当前时间

                String time2 = now.toString() + " " + time + ":00";
                LocalDateTime endldt1 = LocalDateTime.parse(time2, df);// 红包时间

                /** 判断当前时间段是否超时 **/
                if (nowtime.isBefore(endldt1)) {
                    return ResponseUtil.fail(403, "已超过领取时间！");
                }

                String cacheKey = JetcacheConstant.redCacheConstant();
                // "wred" + GraphaiIdGenerator.nextId("");
                String recordCahceKey = JetcacheConstant.redItemCacheConstant(userId, time);

                /** 业务执行所需传递必要参数 **/
                Map<String, Object> bizmap = new HashMap<>(10);
                bizmap.put("device", device);
                bizmap.put("mall_red_rule", map.get("mall_red_rule"));
                bizmap.put("version", getRequest().getHeader("version"));
                bizmap.put("ip", IpUtils.getIpAddress(getRequest()));
                bizmap.put("userId", userId);
                bizmap.put("makerId", makerId);
                // 原金额 用来直推人收益计算用
                bizmap.put("residuerStr", residuer.toString());
                // 乘达人倍率后的金额
                bizmap.put("talResiduer", talResiduer.toString());
                bizmap.put("grainId", grain.getId());
                bizmap.put("ruleId", mallSystemRule.getId());
                /** 将参数投递入缓存 **/

                logger.info("投递业务缓存的key : " + cacheKey);
                NewCacheInstanceServiceLoader.bizCache().put(cacheKey, bizmap);


                Map<String, Object> recordmap = new HashMap<>();
                recordmap.put("amount", talResiduer);
                recordmap.put("create_time", LocalDateTime.now());
                recordmap.put("to_user_id", userId);
                recordmap.put("nickname", mallUser.getNickname());
                recordmap.put("avatar", mallUser.getAvatar());
                NewCacheInstanceServiceLoader.bizCache().put(recordCahceKey, recordmap);
                /** 将缓存ID投递入MQ队列 **/
                // wxProducerService.produceMsg(JacksonUtils.bean2Jsn(bizmap), cacheKey,
                // MQBaseConstant.RED_WHOLE_ENVELOPE_TAG);

                Map<String, Object> message = new HashMap<>();
                message.put("param", JacksonUtils.bean2Jsn(bizmap));
                message.put("bizKey", cacheKey);
                message.put("tags", MQBaseConstant.RED_WHOLE_ENVELOPE_TAG);
                _MQProducer.send(recordmap);
                // wxProducerService.produceMsg(cacheKey, cacheKey, MQBaseConstant.RED_WHOLE_ENVELOPE_TAG);
                /** 触发红包领取逻辑 做实际的入库操作 **/
                // gameRedComponent.receive(userId, makerId, mallSystemRule, residuer, grain, map);
                /** 移除mutexlock **/
                MutexLockUtils.remove(userId);
                datamap.put("status", 1);
                datamap.put("money", talResiduer);
                return ResponseUtil.ok(datamap);
            }
        } catch (Exception e) {
            logger.error("领取红包异常：" + e.getMessage(), e);

            return ResponseUtil.fail("红包领取失败");
        }

    }


    /**
     * 判断抽取（领取）
     *
     * @param userId
     * @return
     */
    private HashMap<String, String> getReceiveStatus2(String userId) {
        HashMap<String, String> map = new HashMap<>();

        // 查询当天整点红包领取个数
        Integer redSum = iMallRedQrcodeRecordService.getRedSum();
        // 查询所有大额红包规则(降序查询，保证领取最新档)
        List<MallSystemRule> mallSystemRules = redHourRuleService.list(new QueryWrapper<MallSystemRule>().eq("rule_id", 3).last("ORDER BY (reserve_one +0) desc"));


        for (MallSystemRule rule : mallSystemRules) {
            // 规则中 触发大额红包的整点红包阈值
            Integer ruleSum = Integer.valueOf(rule.getReserveOne());
            // 达到阈值，触发
            if (redSum >= ruleSum) {


                // 判断该用户当天有无领取过同档次大额红包，
                boolean getStatus = iMallRedQrcodeRecordService.getStatusByUseId(rule.getId(), userId);
                if (getStatus) {
                    // 判断该档次大额红包当天份额有无领取完
                    // 1，查出该档次被领取数量
                    Integer receiveCount = iMallRedQrcodeRecordService.getReceiveCount(rule.getId());

                    if (receiveCount == 0) {
                        logger.warn("****************系统全局消息推送****************** 第一个领取的用户：" + userId);

                        // todo 每个阈值发一次消息推送
                        //
                        // String title ="大额红包开抢啦";
                        // String content = "本轮大额红包开启啦，速来领取最高2021元现金大额红包，无门槛，数量有限，先到先得，领完即止，错过本场，再等一轮~";
                        // String linkUrl = "/pages/game/red-bad-game";
                        // String type = "";
                        // String logoUrl= "";
                        // Integer noticeType= 0;
                        //
                        // NotificationTemplate template = PushUtil.getNotificationTemplate(title, content, linkUrl, type,
                        // logoUrl, noticeType);
                        // PushUtil.pushToSingle("fa4913a592192f5d8e2398b2a591ffef", "嘉华帅嘉华帅嘉华帅嘉华帅", "内容内容内容",
                        // "/pages/game/red-bad-game", null, 4, null);
                        // System.out.println("消息发送了*********************************************");
                        //
                    }


                    // 2,大额红包当天份额的最大数量 > 已领取数量
                    if (Integer.valueOf(rule.getReserveTwo()) > receiveCount) {


                        map.put("id", rule.getId());
                        map.put("startSection", rule.getStartSection().toString());
                        map.put("endSection", rule.getEndSection().toString());
                        map.put("status", "500");
                        map.put("proportion", rule.getProportion().toString());

                        return map;
                    } else {
                        map.put("status", "400");
                        return map;
                    }


                } else {

                    map.put("status", "401");
                    return map;
                }
            }
        }

        return null;

    }


    public static void main(String[] args) {
        LocalDateTime time = LocalDateTime.of(LocalDate.now(), LocalTime.MAX);
        long startTimestamp = time.toInstant(ZoneOffset.of("+8")).toEpochMilli();

        //
        // System.out.println(time);
        //
        // System.out.println(time.toInstant(ZoneOffset.of("+8")).toEpochMilli());

        System.out.println(startTimestamp);

        long l1 = System.currentTimeMillis();

        System.out.println(startTimestamp);
        System.out.println(l1);
        // 倒计时分钟转毫秒数
        long convert = TimeUnit.MILLISECONDS.convert(30L, TimeUnit.MINUTES);
        System.out.println(convert);


        System.out.println(new Date(System.currentTimeMillis()));
        System.out.println(new Date(System.currentTimeMillis() + convert));


        long l = System.currentTimeMillis();


        System.out.println(l);
        System.out.println(System.currentTimeMillis());
        startTimestamp = LocalDateTime.now().toInstant(ZoneOffset.of("+8")).toEpochMilli();

        System.out.println(startTimestamp);

        // todo 每个阈值发一次消息推送

        // String title ="大额红包开抢啦";
        // String content = "本轮大额红包开启啦，速来领取最高2021元现金大额红包，无门槛，数量有限，先到先得，领完即止，错过本场，再等一轮~";
        // String linkUrl = "/pages/game/red-bad-game";
        // String type = "";
        // String logoUrl= "";
        // Integer noticeType= 0;
        // PushUtil.pushToSingle("871799e3003ad27ebb15fbbbe2d69f1a", title, "我是内容内容", linkUrl, null, 2,
        // null);
        //// PushUtil.pushToSingle("3abf726d7e8075d32f6813ef6943b785", title, "我是内容内容", linkUrl, null, 2,
        // null);

    }

    /**
     * 判断抽取（返回当前大额红包档次）
     *
     * @param userId
     * @return
     */
    private MallSystemRule getCurrentRule(String userId) {
        // 查询当天整点红包领取个数
        Integer redSum = iMallRedQrcodeRecordService.getRedSum();
        // 查询所有大额红包规则(降序查询，保证领取最新档)
        List<MallSystemRule> mallSystemRules = redHourRuleService.list(new QueryWrapper<MallSystemRule>().eq("rule_id", 3).last("ORDER BY (reserve_one +0) desc"));

        for (MallSystemRule rule : mallSystemRules) {
            // 规则中 触发大额红包的整点红包阈值
            Integer ruleSum = Integer.valueOf(rule.getReserveOne());
            // 达到阈值，触发
            if (redSum >= ruleSum) {

                // 判断该用户当天有无领取过同档次大额红包，
                boolean getStatus = iMallRedQrcodeRecordService.getStatusByUseId(rule.getId(), userId);
                if (getStatus) {
                    // 判断该档次大额红包当天份额有无领取完
                    // 1，查出该档次被领取数量
                    Integer receiveCount = iMallRedQrcodeRecordService.getReceiveCount(rule.getId());
                    // 2,大额红包当天份额的最大数量 > 已领取数量
                    if (Integer.valueOf(rule.getReserveTwo()) > receiveCount) {


                        return rule;
                    }
                }
            }
        }

        return null;
    }

    /**
     * 领取大額红包
     */
    @PostMapping("/receiveBig")
    public Object receiveBigRedpacket(@LoginUser String userId, HttpServletRequest request, @RequestBody String body) throws Exception {
        if (null == userId) {
            return ResponseUtil.unlogin();
        }

        // 更新用户达人状态
        mallUserService.updateUserTalentStatus(userId);
        String params = JacksonUtil.parseString(body, "params");
        /** 业务执行所需传递必要参数 **/
        Map<String, Object> bizmap = new HashMap<>();
        // 返回参数
        Map<String, Object> datamap = new HashMap<>();
        try {
            synchronized (MutexLockUtils.getMutexLock(userId)) {
                Map<String, String> mapRequest = RSA.getSignAlgorithms(params);
                String timestamp = mapRequest.get("timestamp");
                String param = "xxx" + timestamp;
                String xMallAd = getRequest().getHeader("X-Mall-Ad");
                String aesBase64Decode = AESUtils.decodeFromBase64String(xMallAd, param.getBytes(), null);
                String device = "";
                String userAgent = getRequest().getHeader("user-agent");
                if (null != userAgent && userAgent.contains("iPhone")) {
                    device = "iPhone";
                } else {
                    device = "Android";
                }
                /** 判断是否有资格领取大额红包-获取大额红包配置规则 -返回异常信息 **/
                HashMap<String, String> mallSystemRule = getReceiveStatus2(userId);

                if ("400".equals(mallSystemRule.get("status"))) {
                    datamap.put("type", "01");
                    datamap.put("msg", "抢光啦");
                    return ResponseUtil.ok(datamap);
                }
                if ("401".equals(mallSystemRule.get("status"))) {
                    datamap.put("type", "02");
                    datamap.put("msg", "请勿重复领取");
                    return ResponseUtil.ok(datamap);
                }

                if (!StrUtil.isBlank(aesBase64Decode) && aesBase64Decode.contains("WLSH")) {
                    // 获取用户领取的对应随机红包记录
                    MallRedQrcodeRecord received = iMallRedQrcodeRecordService.getOneRandom();

                    /** 通过红包规则段获取最终领取金额 **/


                    BigDecimal residuer = received.getAmount().setScale(2, BigDecimal.ROUND_DOWN);

                    MallUser user = mallUserService.findById(userId);

                    /** 翻倍后金额 */
                    BigDecimal residuerTal = residuer;

                    // 属于普通用户还是达人，0 普通。 1 达人(乘以倍率)
                    if (user.getIsTalent().equals("1")) {
                        residuerTal = residuer.multiply(new BigDecimal(mallSystemRule.get("proportion"))).setScale(2, BigDecimal.ROUND_DOWN);
                    }


                    String cacheKey = MQBaseConstant.RED_BIG_KEY_PREFIX + GraphaiIdGenerator.nextId("");
                    String recordCahceKey = JetcacheConstant.bigRedItemCacheConstant(userId, mallSystemRule.get("id"));
                    // "wreditem" + userId + ":" + LocalDate.now().toString();
                    bizmap.put("device", device);
                    bizmap.put("version", getRequest().getHeader("version"));
                    bizmap.put("ip", IpUtils.getIpAddress(getRequest()));
                    bizmap.put("userId", userId);
                    // 原金额
                    bizmap.put("residuerStr", residuer.toString());
                    // 翻倍金额
                    bizmap.put("residuerTal", residuerTal.toString());
                    bizmap.put("ruleId", mallSystemRule.get("id"));
                    bizmap.put("xMallAd", aesBase64Decode);

                    // 该用户领取的对应红包记录
                    bizmap.put("received", received.getId());

                    /** 将参数投递入缓存 **/
                    // NewCacheInstanceServiceLoader.bizCache().put(cacheKey, bizmap);

                    MallUser mallUser = mallUserService.findById(userId);
                    Map<String, Object> recordmap = new HashMap<>();
                    recordmap.put("amount", residuerTal);
                    recordmap.put("create_time", LocalDateTime.now());
                    recordmap.put("to_user_id", userId);
                    recordmap.put("nickname", mallUser.getNickname());
                    recordmap.put("avatar", mallUser.getAvatar());
                    NewCacheInstanceServiceLoader.bizCache().put(recordCahceKey, recordmap);


                    /** 将缓存ID投递入MQ队列 **/
                    // wxProducerService.produceMsg(JacksonUtils.bean2Jsn(bizmap), cacheKey,
                    // MQBaseConstant.RED_BIG_ENVELOPE_TAG);
                    Map<String, Object> message = new HashMap<>();
                    message.put("param", JacksonUtils.bean2Jsn(bizmap));
                    message.put("bizKey", cacheKey);
                    message.put("tags", MQBaseConstant.RED_BIG_ENVELOPE_TAG);
                    _MQProducer.send(recordmap);

                    // wxProducerService.produceMsg(cacheKey, cacheKey, MQBaseConstant.RED_BIG_ENVELOPE_TAG);
                    /** 同步触发红包领取逻辑 做实际的入库操作 **/
                    // gameRedComponent.receive(userId, makerId, mallSystemRule, residuer, grain, map);
                    /** 移除mutexlock **/
                    MutexLockUtils.remove(userId);


                    datamap.put("type", "00");
                    datamap.put("msg", "领取成功");
                    datamap.put("money", residuerTal);
                    datamap.put("reminder", "恭喜，领取成功");
                    return ResponseUtil.ok(datamap);
                } else {
                    return ResponseUtil.fail("红包领取失败");
                }
            }
        } catch (Exception e) {
            logger.error("领取红包异常：" + e.getMessage(), e);

            return ResponseUtil.fail("红包领取失败");
        }
    }



    /**
     * 整点分享口令锁粉
     */
    @GetMapping("/hourShare")
    public Object getHourShare(@LoginUser String userId) {
        return wxRedService.createHour(userId);
    }

    /**
     * 根据userId查询整点分享口令
     */
    @GetMapping("/hourPwd")
    public Object getHourPwd(@RequestParam String userId) {
        return wxRedService.getHourPwd(userId);
    }


    /**
     * 领取新春拼团红包
     * 
     * @param userId
     * @param request
     * @param body
     * @return
     */
    @PostMapping("/receiveGroupRed")
    public Object receiveGroupRed(@LoginUser String userId, HttpServletRequest request, @RequestBody String body) {
        if (null == userId) {
            return ResponseUtil.unlogin();
        }

        MallGrouponRules mallGrouponRules = mallGrouponRulesService.queryGroupRulesByAsc("0,1");
        if (null == mallGrouponRules || StrUtil.isEmpty(mallGrouponRules.getRuleId())) {
            return ResponseUtil.fail("获取团信息失败，请稍后重试");
        } else if (0 == mallGrouponRules.getStatus()) {
            return ResponseUtil.fail("该团还没有解锁，无法领取红包");
        }
        String ruleId = mallGrouponRules.getRuleId();
        String groupId = mallGrouponRules.getId();
        Map<String, Object> bizmap = new HashMap<>();
        Map<String, Object> recordmap = new HashMap<>();
        Map<String, Object> datamap = new HashMap<>();

        MallUser user = mallUserService.findById(userId);

        // 是否新用户
        datamap.put("isNew", user.getIsNew());

        String xMallAd = getRequest().getHeader("X-Mall-Ad");
        String params = JacksonUtil.parseString(body, "params");
        Map<String, String> mapRequest = RSA.getSignAlgorithms(params);
        String timestamp = mapRequest.get("timestamp");
        String param = "xxx" + timestamp;
        String device = "";
        String userAgent = getRequest().getHeader("user-agent");
        if (null != userAgent && userAgent.contains("iPhone")) {
            device = "iPhone";
        } else {
            device = "Android";
        }
        bizmap.put("device", device);
        bizmap.put("version", getRequest().getHeader("version"));
        bizmap.put("ip", IpUtils.getIpAddress(getRequest()));
        bizmap.put("userId", userId);
        bizmap.put("groupId", groupId);
        bizmap.put("ruleId", ruleId);

        recordmap.put("create_time", LocalDateTime.now());
        recordmap.put("to_user_id", userId);
        recordmap.put("nickname", user.getNickname());
        recordmap.put("avatar", user.getAvatar());
        try {
            synchronized (MutexLockUtils.getMutexLock(userId)) {
                String aesBase64Decode = AESUtils.decodeFromBase64String(xMallAd, param.getBytes(), null);
                bizmap.put("xMallAd", aesBase64Decode);
                // 判断广告ID是否合法
                if (StrUtil.isBlank(aesBase64Decode) || !aesBase64Decode.contains("WLSH")) {
                    return ResponseUtil.fail("红包领取失败");
                }
                // 2.判断用户领取大额还是整点红包金额
                MallRedQrcodeRecord mallRedQrcodeRecord =
                                iMallRedQrcodeRecordService.getOne(new QueryWrapper<MallRedQrcodeRecord>().eq("group_id", groupId).eq("deleted", "0").eq("amount_type", "07").eq("to_user_id", userId));
                if (null != mallRedQrcodeRecord && 2 == mallRedQrcodeRecord.getIsReceived()) {
                    datamap.put("type", "02");
                    datamap.put("msg", "请勿重复领取");
                    return ResponseUtil.ok(datamap);
                }
                // 新春红包
                if (null != mallRedQrcodeRecord && 1 == mallRedQrcodeRecord.getIsReceived()) {
                    String cacheKey = JetcacheConstant.redCacheConstant();
                    GameRedpacketRain grain = gameRedpacketRainService.selectRedpacket(ruleId);
                    if (grain == null || StrUtil.isEmpty(grain.getActivityId())) {
                        return ResponseUtil.fail("红包领取失败，规则异常");
                    }
                    String recordCahceKey = JetcacheConstant.groupRedItemCacheConstant(userId, ruleId + groupId);

                    bizmap.put("receivedId", mallRedQrcodeRecord.getId());
                    // 原金额 用来直推人收益计算用
                    bizmap.put("residuerStr", mallRedQrcodeRecord.getAmount());
                    // bizmap.put("grainId", mallRedQrcodeRecord.getRedpacketId());
                    /** 将参数投递入缓存 **/

                    logger.info("投递业务缓存的key : " + cacheKey);
                    NewCacheInstanceServiceLoader.bizCache().put(cacheKey, bizmap);

                    recordmap.put("amount", mallRedQrcodeRecord.getAmount());
                    recordmap.put("nickname", user.getNickname());
                    recordmap.put("avatar", user.getAvatar());
                    NewCacheInstanceServiceLoader.bizCache().put(recordCahceKey, recordmap);
                    /** 将缓存ID投递入MQ队列 **/
                    // wxProducerService.produceMsg(JacksonUtils.bean2Jsn(bizmap), cacheKey,
                    // MQBaseConstant.RECEIVE_RED_GROUP_ENVELOPE_TAG);
                    // wxProducerService.produceMsg(cacheKey, cacheKey, MQBaseConstant.RED_WHOLE_ENVELOPE_TAG);
                    /** 触发红包领取逻辑 做实际的入库操作 **/
                    // gameRedComponent.receive(userId, makerId, mallSystemRule, residuer, grain, map);

                    Map<String, Object> message = new HashMap<>();
                    message.put("param", JacksonUtils.bean2Jsn(bizmap));
                    message.put("bizKey", cacheKey);
                    message.put("tags", MQBaseConstant.RED_WHOLE_ENVELOPE_TAG);
                    _MQProducer.send(recordmap);

                    /** 移除mutexlock **/
                    MutexLockUtils.remove(userId);
                    datamap.put("status", 1);
                    datamap.put("money", mallRedQrcodeRecord.getAmount());
                    datamap.put("type", "00");
                    datamap.put("msg", "领取成功");
                    datamap.put("reminder", "恭喜，领取成功");
                    if ("0".equals(user.getIsNew())) {
                        MallUser u = new MallUser();
                        u.setId(user.getId());
                        u.setIsNew("1");
                        mallUserService.updateById(u);
                    }
                    return ResponseUtil.ok(datamap);
                    // 整点红包金额
                } else {
                    List<MallSystemRule> listRule = redHourRuleService.list(new QueryWrapper<MallSystemRule>().eq("rule_id", 1).eq("status", 0));
                    MallSystemRule mallSystemRule = wxRedService.getRedIndex(listRule, user.getIsTalent(), 0);
                    BigDecimal amt = RandomUtil.randomBigDecimal(mallSystemRule.getStartSection(), mallSystemRule.getEndSection()).setScale(2, BigDecimal.ROUND_DOWN);
                    String cacheKey = MQBaseConstant.RED_GROUP_ENVELOPE_TAG + GraphaiIdGenerator.nextId("");
                    String recordCahceKey = JetcacheConstant.groupRedItemCacheConstant(userId, ruleId + groupId);
                    // 原金额
                    bizmap.put("residuerStr", amt);

                    recordmap.put("amount", amt);
                    recordmap.put("nickname", user.getNickname());
                    recordmap.put("avatar", user.getAvatar());
                    NewCacheInstanceServiceLoader.bizCache().put(recordCahceKey, recordmap);

                    /** 将缓存ID投递入MQ队列 **/
                    // wxProducerService.produceMsg(JacksonUtils.bean2Jsn(bizmap), cacheKey,
                    // MQBaseConstant.RECEIVE_RED_GROUP_ENVELOPE_TAG);
                    Map<String, Object> message = new HashMap<>();
                    message.put("param", JacksonUtils.bean2Jsn(bizmap));
                    message.put("bizKey", cacheKey);
                    message.put("tags", MQBaseConstant.RECEIVE_RED_GROUP_ENVELOPE_TAG);
                    _MQProducer.send(recordmap);

                    /** 移除mutexlock **/
                    MutexLockUtils.remove(userId);
                    datamap.put("status", 1);
                    datamap.put("money", amt);
                    datamap.put("type", "00");
                    datamap.put("msg", "领取成功");
                    datamap.put("reminder", "恭喜，领取成功");
                    if ("0".equals(user.getIsNew())) {
                        MallUser u = new MallUser();
                        u.setId(user.getId());
                        u.setIsNew("1");
                        mallUserService.updateById(u);
                    }
                    return ResponseUtil.ok(datamap);
                }
            }
        } catch (Exception e) {
            logger.error("领取红包异常：" + e.getMessage(), e);
            return ResponseUtil.fail("红包领取失败");
        }
    }

    /**
     * 倒计时 todo
     *
     * @return
     */
    @GetMapping("/countDown")
    public Object countDown(@LoginUser String userId) {
        if (null == userId) {
            return ResponseUtil.unlogin();
        }
        Map<String, Object> data = new HashMap<String, Object>(3);
        try {
            String ruleStatus = "0,1,2";
            MallGrouponRules rules = rulesService.queryGroupRulesByAsc(ruleStatus);
            if (Objects.equals(rules, null)) {
                return ResponseUtil.fail(401, "团购规则为空!");
            }
            Integer status = 0;
            // 触发大额领取的时间戳
            Long startTimestamp;
            // 大额失效的时间戳
            Long endTimestamp = 0L;
            // 参团总人数
            Integer joinNum = (int) grouponService.queryGroupCountByRulesId(rules.getId());

            if (null == joinNum) {
                joinNum = 0;
            }
            Integer type = 1;
            if (1 == rules.getStatus()) {
                if (null == rules.getUpdateTime()) {
                    logger.error("获取成团时间失败！");
                    return ResponseUtil.fail("活动已结束");
                }
                status = 1;
                // 推送放入缓存
                Object pushObj = NewCacheInstanceServiceLoader.objectCache().get(rules.getId());
                if (null == pushObj) {
                    NewCacheInstanceServiceLoader.objectCache().put(rules.getId(), rules.getId());
                    PushUtil.pushMessageToApp("", "[未莱生活送大额红包] 本场开奖啦！", "/pages/game/red-bad-group", null, null, null);
                }
                // 2.判断用户领取大额还是整点红包金额
                MallRedQrcodeRecord mallRedQrcodeRecord =
                                iMallRedQrcodeRecordService.getOne(new QueryWrapper<MallRedQrcodeRecord>().eq("group_id", rules.getId()).eq("deleted", "0").eq("amount_type", "07").eq("to_user_id", userId));
                if (null != mallRedQrcodeRecord && 2 == mallRedQrcodeRecord.getIsReceived()) {
                    type = 2;
                }
                // 倒计时分钟转毫秒数
                long exTime = TimeUnit.MILLISECONDS.convert(rules.getDuration(), TimeUnit.MINUTES);

                // 触发新春红包当前的时间戳
                startTimestamp = rules.getUpdateTime().toInstant(ZoneOffset.ofHours(8)).toEpochMilli();
                // 新春失效的时间戳
                endTimestamp = startTimestamp + exTime;
                if (endTimestamp < System.currentTimeMillis()) {
                    status = 2;
                    rules.setStatus(status);
                    rules.setDeleted(true);
                    rulesService.updateById(rules);
                }
            } else {
                grouponService.joinGroup(userId, false);
            }

            // 1新春红包 2整点红包
            data.put("type", type);
            // 剩余人数
            data.put("num", rules.getDiscountMember() - joinNum);
            // 新春失效的时间戳
            data.put("endTimestamp", endTimestamp);
            // 当前服务器时间戳
            data.put("currentTimestamp", System.currentTimeMillis());
            // 0未开始(未满团) 1进行中(已满团) 2已结束)
            data.put("status", status);
            String recordCahceKey = JetcacheConstant.groupRedItemCacheConstant(userId, rules.getRuleId() + rules.getId());
            data.put("user", NewCacheInstanceServiceLoader.bizCache().get(recordCahceKey));
            return ResponseUtil.ok(data);
        } catch (Exception e) {
            logger.error("获取倒计时失败，", e);
        }
        return ResponseUtil.ok(data);
    }
}
