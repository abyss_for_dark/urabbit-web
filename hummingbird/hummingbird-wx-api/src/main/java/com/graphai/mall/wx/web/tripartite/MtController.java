package com.graphai.mall.wx.web.tripartite;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.graphai.commons.constant.MallConstant;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.httprequest.HttpUtils;
import com.graphai.commons.util.json.JacksonUtils;
import com.graphai.mall.db.domain.MallUser;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.util.MtMd5Util;
import com.graphai.mall.wx.annotation.LoginUser;

@RestController
@RequestMapping("/wx/mt")
public class MtController {

	private final Log logger = LogFactory.getLog(MtController.class);

	@Autowired
	private MallUserService userService;

	/**
	 * 获取美团领劵链接
	 */
	@GetMapping("/getMeiTuanCouponLink")
	public Object getMeiTuanCouponLink(@LoginUser String userId, HttpServletRequest mrequest) {
		if (userId == null) {
			return ResponseUtil.unlogin();
		}

		MallUser user = userService.findById(userId);
		if (com.graphai.commons.util.obj.ObjectUtils.isNotNull(user)) {
			if (org.apache.commons.lang3.StringUtils.isBlank(user.getMobile())) {
				return ResponseUtil.fail(501, "获取用户手机号失败");
			}
		}

		try {
			String link = String.format(
					"https://i.meituan.com/awp/hfe/block/a945391288b790d558b7/78716/index.html?appkey=%s:%s",
					MallConstant.MEITUAN_APPKEY, user.getMobile());
			String encoderUrl = URLEncoder.encode(link, "UTF-8");
			String resultLink=URLEncoder.encode(String.format("https://runion.meituan.com/url?key=%s&url=%s&sid=%s",
					MallConstant.MEITUAN_APPKEY, encoderUrl, user.getMobile()), "UTF-8");
			return ResponseUtil.ok("/pages/web/webview?link="+resultLink);
		
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			e.printStackTrace();
		}
		return ResponseUtil.fail();
	}

//	@PostMapping("/Notify")
//	public Object mtNotify(@RequestBody Map<String, String> paraMap) {
//		logger.info("美团订单推送收据" + JSON.toJSONString(paraMap));
//
//		Map<String, Object> resultMap = new HashMap<String, Object>();
//
//		TreeMap<String, String> map = new TreeMap<String, String>();
////		    	map.put("smstitle", paraMap.get("smstitle")); 
////		    	map.put("quantity", paraMap.get("quantity"));
////		    	map.put("orderid", paraMap.get("orderid"));
////		    	map.put("dealid", paraMap.get("dealid")); 
////		    	map.put("direct", paraMap.get("direct")); 
////		    	map.put("paytime", paraMap.get("paytime"));
////		    	map.put("type", paraMap.get("type"));
////		    	map.put("ordertime", paraMap.get("ordertime"));
////		    	map.put("sid", paraMap.get("sid"));
////		    	map.put("uid", paraMap.get("uid"));
////		    	map.put("total", paraMap.get("total"));
////		    	map.put("ratio", paraMap.get("ratio"));
////		    	map.put("newbuyer", paraMap.get("newbuyer"));
//
//		map.put("key", MallConstant.MEITUAN_APPKEY);
//		map.put("sid", paraMap.get("sid"));
//		//map.put("type", paraMap.get("type"));
//		//map.put("oid", paraMap.get("orderid"));
//		//map.put("full", "0"); // 是否返回完整订单信息，0否，1 是
////		StringBuilder s = new StringBuilder();
////		for (Entry<String, String> entry : map.entrySet()) {
////			s.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
////		}
////		String params = s.toString();
////		s.append(MallConstant.PUSH_ORDER_SECRET); // 分配的 secret
////		String encrypt = DigestUtils.sha256Hex(s.toString().getBytes()).toLowerCase();
////		String url = "https://runion.meituan.com/api/rtnotify?" + params + "sign=" + encrypt;
////		String result = HttpUtils.doGet(url);
//		
//            String sign=MtMd5Util.genSign(map, MallConstant.GET_ORDER_SECRET);
//	    	String url = "https://runion.meituan.com/api/rtnotify"; 
//	    	map.put("sign", sign);
//	        String result=HttpUtils.get(url, map, null);
//
//
//		resultMap.put("errcode", "0");
//		resultMap.put("errmsg", "ok");
//
//		return resultMap;
//	}

}
