package com.graphai.mall.wx.web.help;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.mall.db.domain.MallCollect;
import com.graphai.mall.db.domain.MallFollow;
import com.graphai.mall.db.domain.MallHelpTeachSupport;
import com.graphai.mall.db.service.*;
import com.graphai.mall.db.service.helper.MallHelpCategoryService;
import com.graphai.mall.db.service.helper.MallHelpSupportService;
import com.graphai.mall.db.service.helper.MallHelpTeachCategoryService;
import com.graphai.mall.db.service.helper.MallHelpTeachSupportService;
import com.graphai.mall.db.service.user.behavior.MallCollectService;
import com.graphai.mall.db.service.user.behavior.MallFollowService;
import com.graphai.mall.db.vo.MallHelpSupportVo;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.mall.wx.web.common.MyBaseController;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 帮助管理
 *
 * @author LaoGF
 * @since  2020-07-06
 */
@RestController
@RequestMapping("/wx/help")
@Validated
public class WxHelpController extends MyBaseController {

    @Resource
    private MallHelpCategoryService mallHelpCategoryService;

    @Resource
    private MallHelpSupportService mallHelpSupportService;

    @Resource
    private MallHelpTeachCategoryService mallHelpTeachCategoryService;

    @Resource
    private MallHelpTeachSupportService mallHelpTeachSupportService;

    @Resource
    private MallFollowService mallFollowService;
    @Resource
    private MallCollectService mallCollectService;


    /**
     * 查询所有帮助类目列表
     */
    @GetMapping("/categoryList")
    public Object categoryList() {
        List<Map<String, Object>> list = mallHelpCategoryService.categoryList();
        Map<String,Object> data = new  HashMap<>(16);
        data.put("categoryList", list);
        return ResponseUtil.ok(data);
    }

    /**
     * 根据类目查询帮助问题
     */
    @GetMapping("/selectQuestion")
    public Object selectQuestionByCategory(@RequestParam String categoryId) {
        List<MallHelpSupportVo> list = mallHelpSupportService.selectQuestion(categoryId);
        Map<String,Object> data = new  HashMap<>(16);
        data.put("questionList", list);
        return ResponseUtil.ok(data);
    }


    /**
     * 查询所有私塾类目
     */
    @GetMapping("/teachCategoryList")
    public Object teachCategoryList() {
        return ResponseUtil.ok(mallHelpTeachCategoryService.selectTeachCategoryList());
    }

    /**
     * 获取私塾所有视频图文
     */
    @GetMapping("/selectTeachSupport")
    public Object selectTeachSupport() {
        return ResponseUtil.ok(mallHelpTeachSupportService.selectTeachSupport());
    }


    /**
     * 获取私塾所有素材
     */
    @GetMapping("/selectMaterial")
    public Object selectMaterial() {
        return ResponseUtil.ok(mallHelpTeachSupportService.selectMaterial());
    }

    /**
     * 获取河马咨询
     */
    @GetMapping("/selectHippoInfo")
    public Object selectHippoInfo(@RequestParam Integer page, @RequestParam Integer limit) {
        return ResponseUtil.ok(mallHelpTeachSupportService.selectHippoInfo(page,limit));
    }

    /**
     * 获取河马咨询详情
     * @param id
     * @return
     */
    @GetMapping("getHippoInfoDetail")
    public Object getHippoInfoDetail(@LoginUser String userId, String id){
        MallHelpTeachSupport mallHelpTeachSupport = mallHelpTeachSupportService.getById(id);

        // 修改浏览量
        executorService.execute(() -> {
            mallHelpTeachSupport.setInitNum(mallHelpTeachSupport.getInitNum() + 1);
            mallHelpTeachSupportService.update(mallHelpTeachSupport);
        });

        // 点赞数
        Byte a = 2;
        long goodCnt = mallFollowService.countByTargetId(id, a);

        // 是否点赞
        Boolean isGood = mallFollowService.isFollow(id,userId,a);

        // 是否收藏
        MallCollect isCollect = mallCollectService.queryByTypeAndValue(userId, a, id);

        Map<String, Object> map = new HashMap<>();
        map.put("mallHelpTeachSupport",mallHelpTeachSupport);
        map.put("goodCnt",goodCnt);
        map.put("isGood",isGood);
        map.put("isCollect",isCollect != null?true:false);
        return ResponseUtil.ok(map);
    }
}
