package com.graphai.mall.wx.dto;

import com.graphai.mall.db.domain.WangzhuanTask;
import com.graphai.mall.db.domain.WangzhuanTaskStep;
import com.graphai.mall.db.domain.WangzhuanTaskStepWithBLOBs;
import lombok.Data;

import java.util.List;

@Data
public class WangzhuanTaskDto {

    private WangzhuanTask wangzhuanTask;

    private List<WangzhuanTaskStepWithBLOBs> wangzhuanTaskStepList;
}
