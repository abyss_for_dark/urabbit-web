package com.graphai.mall.wx.web.integral;

import java.util.List;
import java.util.Objects;

import javax.validation.constraints.NotNull;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.graphai.commons.util.RegexUtil;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.json.JacksonUtil;
import com.graphai.mall.db.domain.MallAddress;
import com.graphai.mall.db.domain.MallUser;
import com.graphai.mall.db.service.common.MallRegionService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.service.user.behavior.MallAddressService;
import com.graphai.mall.db.util.UserUtils;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.mall.wx.service.GetRegionService;
import com.graphai.mall.wx.web.user.behavior.WxAddressController;

/**
 * 用户收货地址服务-积分商城
 * 
 * @author Qxian
 * @date 2020-10-15
 * @version V0.0.1
 */
@RestController
@RequestMapping("/wx/address/integral")
@Validated
public class WxAddressIntegralController extends GetRegionService {
    private final Log logger = LogFactory.getLog(WxAddressController.class);

    @Autowired
    private MallAddressService addressService;

    @Autowired
    private MallRegionService regionService;

    @Autowired
    private MallUserService userService;


    /**
     * 用户收货地址列表
     *
     * @param mobile-userId 用户ID-这里改为通过手机号码，mobile
     * @return 收货地址列表
     */
    @GetMapping("list")
    public Object list(@LoginUser String userId, @NotNull String mobile) {
        // if (body == null) {
        // return ResponseUtil.unlogin();
        // }
        // String mobile = JacksonUtil.parseString(body, "mobile");
        // if (mobile == null || "".equals(mobile)) {
        // return ResponseUtil.fail(501,"手机号码不能为空!");
        // }
        // String userId="";
        // List<MallUser> users = userService.queryByMobile(mobile);
        // if (CollectionUtils.isNotEmpty(users)) {
        // if (org.apache.commons.lang3.StringUtils.isNotEmpty(users.get(0).getId())) {
        // userId = users.get(0).getId();
        // }
        // }else {
        // return ResponseUtil.fail(501,"用户不存在!");
        // //return ResponseUtil.unlogin();
        // }
        if (null == userId) {
            Object result = UserUtils.getTokenUserId(mobile);
            if (!Objects.equals(result, null)) {
                userId = String.valueOf(result);
            }
        }
        List<MallAddress> addressList = addressService.queryByUid(userId);
        return ResponseUtil.ok(addressList);
    }

    /**
     * 获取默认地址
     *
     * @param mobile-userId 用户ID 这里改为通过手机号码，mobile
     * @return 收货地址信息
     */
    @GetMapping("getDefault")
    public Object getDefault(@LoginUser String userId, @NotNull String mobile) {
        // if (body == null) {
        // return ResponseUtil.unlogin();
        // }
        // String mobile = JacksonUtil.parseString(body, "mobile");
        // if (mobile == null || "".equals(mobile)) {
        // return ResponseUtil.fail(501, "手机号码不能为空!");
        // }
        // String userId = "";
        // List<MallUser> users = userService.queryByMobile(mobile);
        // if (CollectionUtils.isNotEmpty(users)) {
        // if (org.apache.commons.lang3.StringUtils.isNotEmpty(users.get(0).getId())) {
        // userId = users.get(0).getId();
        // }
        // } else {
        // return ResponseUtil.fail(501, "用户不存在!");
        // }
        if (null == userId) {
            Object result = UserUtils.getTokenUserId(mobile);
            if (!Objects.equals(result, null)) {
                userId = String.valueOf(result);
            }
        }
        MallAddress aDefault = addressService.findDefault(userId);
        return ResponseUtil.ok(aDefault);
    }

    /**
     * 收货地址详情
     *
     * @param mobile 用户ID 这里改为通过手机号码，mobile
     * @param id 收货地址ID
     * @return 收货地址详情
     */
    @GetMapping("detail")
    public Object detail(@NotNull String mobile, @NotNull String id) {
        // if (body == null) {
        // return ResponseUtil.unlogin();
        // }
        // String mobile = JacksonUtil.parseString(body, "mobile");
        // String id = JacksonUtil.parseString(body, "id");
        if (mobile == null || "".equals(mobile)) {
            return ResponseUtil.fail(501, "手机号码不能为空!");
        }
        if (id == null || "".equals(id)) {
            return ResponseUtil.fail(501, "地址ID不能为空!");
        }
        String userId = "";
        List<MallUser> users = userService.queryByMobile(mobile);
        if (CollectionUtils.isNotEmpty(users)) {
            if (org.apache.commons.lang3.StringUtils.isNotEmpty(users.get(0).getId())) {
                userId = users.get(0).getId();
            }
        } else {
            return ResponseUtil.fail(501, "用户不存在!");
        }

        MallAddress address = addressService.query(userId, id);
        if (address == null) {
            return ResponseUtil.badArgumentValue();
        }
        return ResponseUtil.ok(address);
    }

    private Object validate(MallAddress address) {
        String name = address.getName();
        if (StringUtils.isEmpty(name)) {
            return ResponseUtil.badArgument();
        }

        // 测试收货手机号码是否正确
        String mobile = address.getTel();
        if (StringUtils.isEmpty(mobile)) {
            return ResponseUtil.badArgument();
        }
        if (!RegexUtil.isMobileExact(mobile)) {
            return ResponseUtil.badArgument();
        }

        String province = address.getProvince();
        if (StringUtils.isEmpty(province)) {
            return ResponseUtil.badArgument();
        }

        String city = address.getCity();
        if (StringUtils.isEmpty(city)) {
            return ResponseUtil.badArgument();
        }

        String county = address.getCounty();
        if (StringUtils.isEmpty(county)) {
            return ResponseUtil.badArgument();
        }


        String areaCode = address.getAreaCode();
        if (StringUtils.isEmpty(areaCode)) {
            return ResponseUtil.badArgument();
        }

        String detailedAddress = address.getAddressDetail();
        if (StringUtils.isEmpty(detailedAddress)) {
            return ResponseUtil.badArgument();
        }

        Boolean isDefault = address.getIsDefault();
        if (isDefault == null) {
            return ResponseUtil.badArgument();
        }
        return null;
    }

    /**
     * 添加或更新收货地址
     *
     * @param body-userId 用户ID 这里改为通过手机号码，mobile
     * @param body-address 用户收货地址
     * @return 添加或更新操作结果
     */
    @PostMapping("save")
    public Object save(@LoginUser String userId, @RequestBody String body) {

        String mobile = JacksonUtil.parseString(body, "mobile");
        // if (mobile == null || "".equals(mobile)) {
        // // return ResponseUtil.unlogin();
        // return ResponseUtil.fail(501, "手机号不能为空!");
        // }
        // String userId = "";
        // List<MallUser> users = userService.queryByMobile(mobile);
        // if (CollectionUtils.isNotEmpty(users)) {
        // if (org.apache.commons.lang3.StringUtils.isNotEmpty(users.get(0).getId())) {
        // userId = users.get(0).getId();
        // }
        // } else {
        // // return ResponseUtil.unlogin();
        // return ResponseUtil.fail(501, "用户不存在!");
        // }

        if (null == userId) {
            Object result = UserUtils.getTokenUserId(mobile);
            if (!Objects.equals(result, null)) {
                userId = String.valueOf(result);
            }
        }
        String province = JacksonUtil.parseString(body, "province");
        String city = JacksonUtil.parseString(body, "city");//
        String county = JacksonUtil.parseString(body, "county");//
        String addressName = JacksonUtil.parseString(body, "addressName");//
        String addressDetail = JacksonUtil.parseString(body, "addressDetail");//
        String areaCode = JacksonUtil.parseString(body, "areaCode");//
        String id = JacksonUtil.parseString(body, "id");//
        String isDefault = JacksonUtil.parseString(body, "isDefault");//
        String name = JacksonUtil.parseString(body, "name");//
        String tel = JacksonUtil.parseString(body, "tel");//
        MallAddress address = new MallAddress();
        address.setUserId(userId);
        address.setProvince(province);
        address.setCity(city);
        address.setCounty(county);
        address.setAddressDetail(addressDetail);
        address.setAreaCode(areaCode);
        address.setId(id);
        address.setIsDefault(Boolean.parseBoolean(isDefault));
        address.setName(name);
        address.setTel(tel);
        Object error = validate(address);
        if (error != null) {
            return error;
        }

        if (address.getIsDefault()) {
            // 重置其他收获地址的默认选项
            addressService.resetDefault(userId);
        }

        if (address.getId() == null || address.getId().equals("0")) {
            address.setId(null);
            address.setUserId(userId);
            addressService.add(address);
        } else {
            address.setUserId(userId);
            if (addressService.update(address) == 0) {
                return ResponseUtil.updatedDataFailed();
            }
        }
        return ResponseUtil.ok(address.getId());
    }

    /**
     * 删除收货地址
     *
     * @param body-userId 用户ID
     * @param body-address 用户收货地址，{ id: xxx }
     * @return 删除操作结果
     */
    @PostMapping("delete")
    public Object delete(@RequestBody String body) {
        String mobile = JacksonUtil.parseString(body, "mobile");
        String id = JacksonUtil.parseString(body, "id");
        if (mobile == null || "".equals(mobile)) {
            return ResponseUtil.unlogin();
        }
        if (id == null || "".equals(id)) {
            return ResponseUtil.fail(501, "地址ID不能为空!");
        }
        String userId = "";
        List<MallUser> users = userService.queryByMobile(mobile);
        if (CollectionUtils.isNotEmpty(users)) {
            if (org.apache.commons.lang3.StringUtils.isNotEmpty(users.get(0).getId())) {
                userId = users.get(0).getId();
            }
        } else {
            return ResponseUtil.fail(501, "用户不存在!");
        }

        if (id == null) {
            return ResponseUtil.badArgument();
        }

        addressService.delete(id);
        return ResponseUtil.ok();
    }
}
