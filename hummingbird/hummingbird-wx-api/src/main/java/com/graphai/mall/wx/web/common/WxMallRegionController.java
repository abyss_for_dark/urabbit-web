package com.graphai.mall.wx.web.common;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.mall.db.service.common.MallRegionService;
import com.graphai.mall.db.vo.MallRegionVo;

import lombok.extern.slf4j.Slf4j;

/**
 * 行政区域
 */
@RestController
@RequestMapping("/wx/region")
@Validated
@Slf4j
public class WxMallRegionController {

    private final Log logger = LogFactory.getLog(WxMallRegionController.class);

    @Autowired
    private MallRegionService mallRegionService;


    /**
     * 获取全部行政区域
     */
    @GetMapping("/getAllRegion")
    public Object getAllRegion() {

        List<MallRegionVo> fiList = new ArrayList<MallRegionVo>();

        try {
            List<MallRegionVo> regions = mallRegionService.selectRegions();
            if (CollectionUtils.isNotEmpty(regions)) {

                for (MallRegionVo mallRegionVo : regions) {
                    if (Objects.equals("0", mallRegionVo.getPid())) {
                        fiList.add(mallRegionVo);
                    }
                }

            }

            if (CollectionUtils.isNotEmpty(fiList)) {
                for (MallRegionVo mallRegionVo : fiList) {
                    List<MallRegionVo> seList = new ArrayList<MallRegionVo>();
                    for (int i = 0; i < regions.size(); i++) {
                        if (Objects.equals(mallRegionVo.getId(), regions.get(i).getPid())) {
                            seList.add(regions.get(i));
                        }
                    }
                    mallRegionVo.setChilds(seList);
                }

                for (MallRegionVo mallRegionVo : fiList) {
                    for (int i = 0; i < mallRegionVo.getChilds().size(); i++) {
                        List<MallRegionVo> thiList = new ArrayList<MallRegionVo>();
                        for (int k = 0; k < regions.size(); k++) {
                            if (Objects.equals(mallRegionVo.getChilds().get(i).getId(), regions.get(k).getPid())) {
                                thiList.add(regions.get(k));
                            }
                        }
                        mallRegionVo.getChilds().get(i).setChilds(thiList);
                    }
                }

            }


            return ResponseUtil.ok(fiList);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw e;
        }

    }

}
