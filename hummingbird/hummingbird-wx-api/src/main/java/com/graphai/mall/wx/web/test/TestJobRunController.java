package com.graphai.mall.wx.web.test;

import com.graphai.mall.db.service.red.WxRedService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

/**
 * 定时任务测试类
 * @author Qxian
 * @date 2021-01-27
 */
@RestController
@RequestMapping("/wx/test/jobrun")
@Validated
public class TestJobRunController {

    private final Log log = LogFactory.getLog(TestJobRunController.class);

    @Autowired
    private WxRedService wxRedService;

    /**
     * 1、口令红包退款
     * @return 退款列表
     */
    @GetMapping("refundRedPwd")
    public Object refundRedPwd() {
        Object object =  wxRedService.refundRedPacketPwd();
        System.out.println("object："+object);
        return object;
    }

    // 测试类
    public static void main(String[] args) {
        /*String tkl = "牛奔马跃行千里，凤舞龙飞上九霄，我在未莱生活给您派发了新春红包，点击 http://sd.uulucky.com/cPODsGaL 下载APP  复制这段文字K#U38CTDPPPAK#，来未莱生活APP领取吧~";
        if (StringUtils.isNotBlank(tkl)) {
            int number = tkl.length() - tkl.replaceAll("K#", "").length();
            System.out.println("number=" + number);
            if (number >= 4) {
                String resultSub = RedpacketUtil.subString(tkl, "K#", "K#",2);
                System.out.println("结果：" + resultSub);
            }
        }*/

        BigDecimal amountBigTotal = new BigDecimal("0.10");
        BigDecimal redpacketSumBig = new BigDecimal("12");
        BigDecimal minBigDecimal = new BigDecimal("0.01");

        BigDecimal resultBigDecimal = amountBigTotal.divide(redpacketSumBig,6,BigDecimal.ROUND_HALF_UP);

        if (resultBigDecimal.compareTo(minBigDecimal) == -1) {
            System.out.println("结果：" + resultBigDecimal.compareTo(minBigDecimal));
        }
        System.out.println(resultBigDecimal.compareTo(minBigDecimal));
    }

}
