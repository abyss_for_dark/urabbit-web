package com.graphai.mall.wx.util;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.graphai.commons.constant.MallConstant;
import com.graphai.mall.db.domain.FcAccount;
import com.graphai.mall.db.domain.MallGoods;
import com.graphai.mall.db.domain.MallGoodsProduct;
import com.graphai.mall.db.domain.MallOrder;
import com.graphai.mall.db.domain.MallOrderGoods;
import com.graphai.mall.db.domain.MallRebateRecord;
import com.graphai.mall.db.domain.MallUser;
import com.graphai.mall.db.service.account.AccountService;
import com.graphai.mall.db.service.distribution.FenXiaoService;

public class OrderCalculationUtil {

	
	/**
	 * 获取产品优惠后的金额
	 * @return
	 */
	public static BigDecimal getAmountAfterPreference(BigDecimal tcRetailPrice,List<MallOrderGoods> orderGoods,MallGoodsProduct product){
		BigDecimal couponPrice = new BigDecimal(0.0);
		for (int j = 0; j < orderGoods.size(); j++) {
			MallOrderGoods orderGood = orderGoods.get(j);
			if(product.getId()!=null && product.getId().equals(orderGood.getProductId())){
				couponPrice = orderGood.getCouponPrice();
			}
		}
		
		return tcRetailPrice.subtract(couponPrice);
	}
	
	/**
	 * 在商品数据集合中通过单个SKU获取商品的数据
	 * @param goods
	 * @param product
	 * @return
	 */
	public static MallGoods getGoods(List<MallGoods> goods,MallGoodsProduct product){
		for (int i = 0; i < goods.size(); i++) {
			MallGoods mgoods = goods.get(i);
			if(mgoods.getId().equals(product.getGoodsId())){
				return mgoods;
			}
		}
		return null;
	}
	
	/**
	 * 获取对应的产品购买数量
	 * @return
	 */
	public static Short getBuyNumber(List<MallOrderGoods> orderGoods,MallGoodsProduct product){
		for (int j = 0; j < orderGoods.size(); j++) {
			MallOrderGoods orderGood = orderGoods.get(j);
			if(product.getId()!=null && product.getId().equals(orderGood.getProductId())){
				return orderGood.getNumber();
			}
		}
		
		return null;
	}
	
	/**
	 * 计算分润的金额  
	 * 
	 * 比例模式： 分润金额 = 产品原价 * 比例 * 数量
	 * 固定模式： 分润金额 = 固定值*数量 
	 * @return
	 */
	public static BigDecimal getAmountOfShare(String calModel,BigDecimal tcRetailPrice,Short number,BigDecimal ratio){
		if ("1R".equals(calModel)) {
			// 计算比例
			BigDecimal calamount1 = tcRetailPrice.multiply(ratio).setScale(2, BigDecimal.ROUND_HALF_UP);
			return calamount1.multiply(new BigDecimal(number)).setScale(2, BigDecimal.ROUND_HALF_UP);
		} else {
			// 固定值
			BigDecimal calamount1 = ratio;
			return calamount1.multiply(new BigDecimal(number)).setScale(2, BigDecimal.ROUND_HALF_UP);
		}
	}
	
	public static void shareAmountRecharge(String dType,AccountService accountService,
			FcAccount rechargeAccount,MallOrder order,
			MallGoods _goods,MallGoodsProduct _product,
			BigDecimal rechargeAmount,boolean isRecharge) throws Exception{
		
		if(rechargeAmount.compareTo(new BigDecimal(0)) != 0){
			Map<String, Object> paramsMap = new HashMap<String, Object>();
			paramsMap.put("channel", "wx");
			paramsMap.put("orderId", order.getId());
			paramsMap.put("orderSn", order.getOrderSn());
			paramsMap.put("buyerId", order.getUserId());
			paramsMap.put("goodsId", _goods.getId());
			paramsMap.put("productId", _product.getId());
			/** 计算一级分销的分润金额 计算本分销员的收益 **/
			if (rechargeAccount != null) {
				paramsMap.put("dType",dType);
				FcAccount rechargeAccount1 = new FcAccount();
				rechargeAccount1.setAccountId(rechargeAccount.getAccountId());
				rechargeAccount1.setAmount(rechargeAmount);
				if(isRecharge){
					accountService.accountTradeRecharge(rechargeAccount, rechargeAccount1, paramsMap,"");
				}
				
			}
		}
	}
	
	public static BigDecimal getCustomerAddAmount(FenXiaoService fenXiaoService,BigDecimal tcRetailPrice,MallRebateRecord rebat,MallUser user){
		
		/*************计算订单加价部分金额*************/
		if (rebat != null) {
			//说明用户是会员，那么支付的金额打折(9.5折)同样订单加价部分分润9.5折
			if(MallConstant.USER_LEVEL_VIP==user.getUserLevel()){
				return GoodsUtils.getAddAmount(rebat, tcRetailPrice).multiply(new BigDecimal(0.95)).setScale(2, BigDecimal.ROUND_HALF_UP);
			}else{
				return GoodsUtils.getAddAmount(rebat, tcRetailPrice);
			}
		}else{
			return new BigDecimal(0.0);
		}
	}
}
