package com.graphai.mall.wx.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class CouponVo {
    private String couponId;
    private String couponUserId;
    private String name;
    private String desc;
    private String tag;
    private String min;
    private BigDecimal discount;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private Short couponType;
    private String merchantId;
}
