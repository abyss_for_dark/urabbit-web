package com.graphai.mall.wx.web.integral;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.validation.constraints.NotNull;

//import com.mysql.jdbc.StringUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Header;
import org.apache.http.message.BasicHeader;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.graphai.commons.constant.MallConstant;
import com.graphai.commons.util.AppIdAlgorithm;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.image.ImageUtils;
import com.graphai.commons.util.json.JacksonUtils;
import com.graphai.framework.storage.StorageService;
import com.graphai.mall.core.system.SystemConfig;
import com.graphai.mall.db.domain.MallCategory;
import com.graphai.mall.db.domain.MallFootprint;
import com.graphai.mall.db.domain.MallGoods;
import com.graphai.mall.db.domain.MallGoodsAttribute;
import com.graphai.mall.db.domain.MallGoodsCodeSeq;
import com.graphai.mall.db.domain.MallGoodsProduct;
import com.graphai.mall.db.domain.MallGoodsSpecification;
import com.graphai.mall.db.domain.MallIssue;
import com.graphai.mall.db.domain.MallRebateRecord;
import com.graphai.mall.db.domain.MallSearchHistory;
import com.graphai.mall.db.domain.MallStorage;
import com.graphai.mall.db.domain.MallUser;
import com.graphai.mall.db.service.advertisement.MallBrandService;
import com.graphai.mall.db.service.category.MallCategoryService;
import com.graphai.mall.db.service.coupon.MallCouponService;
import com.graphai.mall.db.service.distribution.FenXiaoService;
import com.graphai.mall.db.service.goods.MallGoodsAttributeService;
import com.graphai.mall.db.service.goods.MallGoodsCodeSeqService;
import com.graphai.mall.db.service.goods.MallGoodsProductService;
import com.graphai.mall.db.service.goods.MallGoodsSpecificationService;
import com.graphai.mall.db.service.groupon.MallGrouponRulesService;
import com.graphai.mall.db.service.integral.MallGoodsIntegralService;
import com.graphai.mall.db.service.search.MallSearchHistoryService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.service.user.behavior.MallCollectService;
import com.graphai.mall.db.service.user.behavior.MallCommentService;
import com.graphai.mall.db.service.user.behavior.MallFootprintService;
import com.graphai.mall.db.service.user.behavior.MallIssueService;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.mall.wx.util.GoodsUtils;
import com.graphai.mall.wx.web.common.MyBaseController;
import com.graphai.mall.wx.web.goods.WxGoodsController;
import com.graphai.open.api.getway.HttpRequestUtils;
import com.graphai.properties.PlatFormServerproperty;
import com.graphai.validator.Order;
import com.graphai.validator.Sort;
import com.mysql.cj.util.StringUtils;

import cn.hutool.core.util.ObjectUtil;

/**
 * 商品服务-积分商城
 * @author Qxian
 * @date 2020-10-14
 * @version V0.0.1
 */
@RestController
@RequestMapping("/wx/goods/integral")
@Validated
public class WxGoodsIntegralController extends MyBaseController {
	private final Log logger = LogFactory.getLog(WxGoodsController.class);

	@Autowired
	private FenXiaoService fenXiaoService;
	@Autowired
	private MallGoodsIntegralService goodsService;

	@Autowired
	private MallGoodsProductService productService;

	@Autowired
	private MallIssueService goodsIssueService;

	@Autowired
	private MallGoodsAttributeService goodsAttributeService;

	@Autowired
	private MallBrandService brandService;

	@Autowired
	private MallCommentService commentService;

	@Autowired
	private MallUserService userService;

	@Autowired
	private MallCollectService collectService;

	@Autowired
	private MallFootprintService footprintService;

	@Autowired
	private MallCategoryService categoryService;

	@Autowired
	private MallSearchHistoryService searchHistoryService;

	@Autowired
	private MallGoodsSpecificationService goodsSpecificationService;

	@Autowired
	private MallGrouponRulesService rulesService;

	@Autowired
	private StorageService storageService;

	@Autowired
	private MallGoodsCodeSeqService goodsCodeSeqService;

	@Autowired
	private MallCouponService couponService;

	@Autowired
	private PlatFormServerproperty platFormServerproperty;

	/**
	 * 商品详情
	 * <p>
	 * 用户可以不登录。 如果用户登录，则记录用户足迹以及返回用户收藏信息。
	 *
	 * @param userId
	 *            用户ID
	 * @param id
	 *            商品ID
	 * @return 商品详情
	 */
	@GetMapping("detail")
	public Object detail(@LoginUser String userId, @NotNull String id, String shId) {
		Map<String, Object> data = new HashMap<>();

		// 商品信息
		if (org.apache.commons.lang.StringUtils.isBlank(id)) {
			return ResponseUtil.badArgument();
		}
		//MallGoods info = goodsService.findById(id);
		MallGoods info = null;
		String url = platFormServerproperty.getUrl(MallConstant.TAOBAO);
		Map<String, Object> mappMap = new HashMap<String, Object>();
		mappMap.put("plateform", "dtk");
		mappMap.put("id", id);
		Header[] headers = new BasicHeader[]{new BasicHeader("method", "getGoodsInfoById"), new BasicHeader("version", "v1.0")};
		String result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);
		JSONObject object = JSONObject.parseObject(result);
		if (!object.getString("errno").equals("0")) {
			return ResponseUtil.fail();
		}
		result = object.get("data").toString();
		JSONObject jsonObject = object.getJSONObject("data");
		logger.info("------coupon------------" + jsonObject.get("coupon"));
		logger.info("------goods----------" + jsonObject.get("goods"));
		if (ObjectUtil.isNotNull(jsonObject.get("coupon"))) {
			JSONObject jsonCoupon = jsonObject.parseObject(jsonObject.get("coupon").toString());
			data.put("coupon", jsonCoupon);
		}
		if (ObjectUtil.isNotNull(jsonObject.get("goods"))) {
			JSONObject jsonGoods = jsonObject.parseObject(jsonObject.get("goods").toString());
			data.put("info", jsonGoods);
		}

		//this.syncGoodsImg(info); // 判断是否需要合成图片

//		List<MallGoodsAttribute> goodsAttributeList = goodsAttributeService.queryByGid(id);

		// 商品规格 返回的是定制的GoodsSpecificationVo
//		Object goodsSpecification = goodsSpecificationService.getSpecificationVoList(id);

		// 商品规格对应的数量和价格
		List<MallGoodsProduct> productList =productService.queryByGidOpen(id);//productService.queryByGid(id);
		data.put("productList", productList);
		// 商品问题，这里是一些通用问题
		List<MallIssue> issue = goodsIssueService.querySelective("", 1, 4, "", "");

		// 用户收藏
		int userHasCollect = 0;
		if (userId != null) {
			userHasCollect = collectService.count(userId, id);
		}

		// 记录用户的足迹 异步处理
		if (userId != null) {
			executorService.execute(() -> {
				MallFootprint footprint = new MallFootprint();
				footprint.setUserId(userId);
				footprint.setGoodsId(id);
				footprintService.add(footprint);

				MallUser uUser = new MallUser();
				uUser.setId(userId);
				uUser.setUpdateTime(LocalDateTime.now());
				uUser.setFootprintId(footprint.getId());
				userService.updateById(uUser);

			});
		}

		try {

//			MallCoupon coupon = couponService.findById(info.getCouponId());
//			data.put("coupon",coupon);
			data.put("freight", null == info.getcRetailPrice()? SystemConfig.getFreight():info.getcRetailPrice());
//			data.put("info", info);
			data.put("userHasCollect", userHasCollect);
			data.put("issue", issue);
//			data.put("specificationList", goodsSpecification);
//			data.put("attribute", goodsAttributeList);
			List<Object> group = new ArrayList<Object>();
			data.put("groupon", group);

		} catch (Exception e) {
			e.printStackTrace();
		}

		if (info != null) {
			// 商品分享图片地址
			data.put("shareImage", info.getShareUrl());
		}
		return ResponseUtil.ok(data);

	}

	/**
	 * 商品详情
	 * <p>
	 * 用户可以不登录。 如果用户登录，则记录用户足迹以及返回用户收藏信息。
	 *
	 * @param userId
	 *            用户ID
	 * @param id
	 *            商品ID
	 * @return 商品详情
	 */
	@GetMapping("detailbak")
	public Object detailBak(@LoginUser Integer userId, @NotNull Integer id, Integer shId) {
		// 商品信息
		/*MallGoods info = goodsService.findById(id);
		this.syncGoodsImg(info); // 判断是否需要合成图片 added by sami 190627

		List<String> myDList = new ArrayList<String>();
		try {

			// 不是唯品仓同步过来的数据就要做处理 !"WPC".equals(info.getChannelCode())
			if ((org.apache.commons.lang.StringUtils.isEmpty(info.getChannelCode()) || "JW".equalsIgnoreCase(info.getChannelCode())) && info != null
					&& !StringUtils.isNullOrEmpty(info.getDetail())) {
				Element doc = Jsoup.parseBodyFragment(info.getDetail()).body();
				Elements pngs = doc.select("img[src]");
				for (Element element : pngs) {
					String imgUrl = element.attr("src");
					myDList.add(imgUrl);
				}
				info.setDetail(JacksonUtils.bean2Jsn(myDList));
			}

		} catch (Exception e1) {
			logger.error("商品详情解析json异常" + e1.getMessage());
		}
		// 商品属性
		Callable<List> goodsAttributeListCallable = () -> goodsAttributeService.queryByGid(id);

		// 商品规格 返回的是定制的GoodsSpecificationVo
		Callable<Object> objectCallable = () -> goodsSpecificationService.getSpecificationVoList(id);

		// 商品规格对应的数量和价格
		Callable<List<MallGoodsProduct>> productListCallable = () -> productService.queryByGid(id);

		// 商品问题，这里是一些通用问题
		Callable<List> issueCallable = () -> goodsIssueService.querySelective("", 1, 4, "", "");

		// 商品品牌商
		Callable<MallBrand> brandCallable = () -> {
			Integer brandId = info.getBrandId();
			MallBrand brand;
			if (brandId == 0) {
				brand = new MallBrand();
			} else {
				brand = brandService.findById(info.getBrandId());
			}
			return brand;
		};

		// 评论
		Callable<Map> commentsCallable = () -> {
			List<MallComment> comments = commentService.queryGoodsByGid(id, 0, 2);
			List<Map<String, Object>> commentsVo = new ArrayList<>(comments.size());
			long commentCount = PageInfo.of(comments).getTotal();
			for (MallComment comment : comments) {
				Map<String, Object> c = new HashMap<>();
				c.put("id", comment.getId());
				c.put("addTime", comment.getAddTime());
				c.put("content", comment.getContent());
				MallUser user = userService.findById(comment.getUserId());
				c.put("nickname", user == null ? "" : user.getNickname());
				c.put("avatar", user == null ? "" : user.getAvatar());
				c.put("picList", comment.getPicUrls());
				commentsVo.add(c);
			}
			Map<String, Object> commentList = new HashMap<>();
			commentList.put("count", commentCount);
			commentList.put("data", commentsVo);
			return commentList;
		};

		// 团购信息
		Callable<List> grouponRulesCallable = () -> rulesService.queryByGoodsId(id);

		// 用户收藏
		int userHasCollect = 0;
		if (userId != null) {
			userHasCollect = collectService.count(userId, id);
		}

		// 记录用户的足迹 异步处理
		if (userId != null) {
			executorService.execute(() -> {
				MallFootprint footprint = new MallFootprint();
				footprint.setUserId(userId);
				footprint.setGoodsId(id);
				footprintService.add(footprint);

				MallUser uUser = new MallUser();
				uUser.setId(userId);
				uUser.setUpdateTime(LocalDateTime.now());
				uUser.setFootprintId(footprint.getId());
				userService.updateById(uUser);

			});
		}
		FutureTask<List> goodsAttributeListTask = new FutureTask<>(goodsAttributeListCallable);
		FutureTask<Object> objectCallableTask = new FutureTask<>(objectCallable);
		FutureTask<List<MallGoodsProduct>> productListCallableTask = new FutureTask<>(productListCallable);
		FutureTask<List> issueCallableTask = new FutureTask<>(issueCallable);
		FutureTask<Map> commentsCallableTsk = new FutureTask<>(commentsCallable);
		FutureTask<MallBrand> brandCallableTask = new FutureTask<>(brandCallable);
		FutureTask<List> grouponRulesCallableTask = new FutureTask<>(grouponRulesCallable);

		executorService.submit(goodsAttributeListTask);
		executorService.submit(objectCallableTask);
		executorService.submit(productListCallableTask);
		executorService.submit(issueCallableTask);
		executorService.submit(commentsCallableTsk);
		executorService.submit(brandCallableTask);
		executorService.submit(grouponRulesCallableTask);

		Map<String, Object> data = new HashMap<>();

		try {

			List<MallGoodsProduct> productList = productListCallableTask.get();
			if (shId != null) {
				MallRebateRecord rebate = fenXiaoService.getMallRebateRecordById(shId);

				if (CollectionUtils.isNotEmpty(productList)) {
					logger.info("获取的产品数据 :" + JacksonUtils.bean2Jsn(productList));
					for (int i = 0; i < productList.size(); i++) {
						MallGoodsProduct myProduct = productList.get(i);
						BigDecimal nowAmount = myProduct.getPrice();
						BigDecimal getAmount = GoodsUtils.getAddAmount(rebate, nowAmount);
						logger.info("获取到getAmount ： " + getAmount);
						myProduct.setPrice(nowAmount.add(getAmount));
					}
				}
				logger.info("计算玩后的获取的产品数据 :" + JacksonUtils.bean2Jsn(productList));
				// 设置加价金额
				GoodsUtils.setRetailPrice(rebate, info);
			} else {

				// 计算省赚金额
				info.setSpreads(GoodsUtils.getSpreads(info));
			}

			data.put("info", info);
			data.put("userHasCollect", userHasCollect);
			data.put("issue", issueCallableTask.get());
			data.put("comment", commentsCallableTsk.get());
			data.put("specificationList", objectCallableTask.get());
			data.put("productList", productList);
			data.put("attribute", goodsAttributeListTask.get());
			data.put("brand", brandCallableTask.get());
			data.put("groupon", grouponRulesCallableTask.get());
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (info != null) {
			// 商品分享图片地址
			data.put("shareImage", info.getShareUrl());
		}
		return ResponseUtil.ok(data);*/
		return ResponseUtil.ok();
	}

	/**
	 * 商品分类类目
	 *
	 * @param id
	 *            分类类目ID
	 * @return 商品分类类目
	 */
	@GetMapping("category")
	public Object category(@NotNull String id) {
		/*if (org.apache.commons.lang.StringUtils.isBlank(id)) {
			// return ResponseUtil.fail(-1, "产品详情id为空");
			return ResponseUtil.badArgument();
		}

		MallCategory cur = categoryService.findById(Integer.parseInt(id));
		MallCategory parent = null;
		List<MallCategory> children = null;

		if (cur.getPid() == 0) {
			parent = cur;
			children = categoryService.queryByPid(cur.getId());
			cur = children.size() > 0 ? children.get(0) : cur;
		} else {
			parent = categoryService.findById(cur.getPid());
			children = categoryService.queryByPid(cur.getPid());
		}
		Map<String, Object> data = new HashMap<>();
		data.put("currentCategory", cur);
		data.put("parentCategory", parent);
		data.put("brotherCategory", children);
		return ResponseUtil.ok(data);*/
		return ResponseUtil.ok();
	}

	/**
	 * 根据条件搜素商品
	 * <p>
	 * 1. 这里的前五个参数都是可选的，甚至都是空 2. 用户是可选登录，如果登录，则记录用户的搜索关键字
	 *
	 * @param activityId
	 *            活动ID，可选【Integer-->String】
	 * @param categoryId
	 *            分类类目ID，可选
	 * @param brandId
	 *            品牌商ID，可选【Integer-->String】
	 * @param keyword
	 *            关键字，可选
	 * @param isNew
	 *            是否新品，可选
	 * @param isHot
	 *            是否热买，可选
	 * @param shId
	 *            分销记录id，可选【Integer-->String】
	 * @param userId
	 *            用户ID【Integer-->String】
	 * @param page
	 *            分页页数
	 * @param limit
	 *            分页大小
	 * @param sort
	 *            排序方式，支持"id", "retail_price"或"name"
	 * @param order
	 *            排序类型，顺序或者降序
	 * @return 根据条件搜素的商品详情
	 */
	@GetMapping("list")
	public Object list(@RequestParam(defaultValue = "children") String catLevel, String activityId, String categoryId, String brandId, String keyword,
                       Boolean isNew, Boolean isHot, String shId, @LoginUser String userId, @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit,
                       @Sort(accepts = { "id", "retail_price", "name" }) @RequestParam(defaultValue = "id") String sort,
                       @Order @RequestParam(defaultValue = "desc") String order) throws Exception{

		// 添加到搜索历史
		if (userId != null && !StringUtils.isNullOrEmpty(keyword)) {
			MallSearchHistory searchHistoryVo = new MallSearchHistory();
			searchHistoryVo.setKeyword(keyword);
			searchHistoryVo.setUserId(userId);
			searchHistoryVo.setFrom("wx");
			searchHistoryService.save(searchHistoryVo);
		}

		MallRebateRecord rebate = null;
		/*** 说明是分销员分享出来的 ***/
		if (shId != null) {
			rebate = fenXiaoService.getMallRebateRecordById(shId);
		}
		if (org.apache.commons.lang.StringUtils.isBlank(categoryId) || "null".equals(categoryId)) {
			categoryId = "0";
		}

		/*** 2019年6月5号改造，如果传入的目录是父目录ID将子目录下所有的商品全部查询出来 ***/
		// 查询列表数据
		List<MallGoods> goodsList = goodsService.querySelectiveV2(activityId, categoryId, catLevel, brandId, keyword, isHot, isNew, page, limit, sort, order);

		// 查询商品所属类目列表。
//		List<String> goodsCatIds = goodsService.getCatIds(brandId, keyword, isHot, isNew);
		List<MallCategory> categoryList = null;
//		if (goodsCatIds.size() != 0) {
//			categoryList = categoryService.queryL2ByIds(goodsCatIds);
//		} else {
//			categoryList = new ArrayList<>(0);
//		}

		Map<String, Object> data = new HashMap<>();
		data.put("goodsList", goodsList);
		data.put("count", PageInfo.of(goodsList).getTotal());
//		data.put("filterCategoryList", categoryList);

		return ResponseUtil.ok(data);
	}

	@GetMapping("v2/list")
	public Object listv2(@RequestParam(defaultValue = "children") String catLevel, Integer activityId, String categoryId, Integer brandId, String keyword,
                         Boolean isNew, Boolean isHot, Integer shId, @LoginUser Integer userId, @RequestParam(defaultValue = "1") Integer page,
                         @RequestParam(defaultValue = "10") Integer limit,
                         @Sort(accepts = { "id", "retail_price", "name" }) @RequestParam(defaultValue = "id") String sort,
                         @Order @RequestParam(defaultValue = "desc") String order,
                         @RequestParam(defaultValue = "0") Integer industryId) {

		/*List<Map<String,Object>> dataList = new ArrayList<Map<String,Object>>();
		List<VO> specificationVoList = null;
		MallRebateRecord rebate = null;
		*//*** 说明是分销员分享出来的 ***//*
		if (shId != null) {
			rebate = fenXiaoService.getMallRebateRecordById(shId);
		}

		if (org.apache.commons.lang.StringUtils.isBlank(categoryId) || "null".equals(categoryId)) {
			categoryId = "0";
		}
		*//*** 2019年6月5号改造，如果传入的目录是父目录ID将子目录下所有的商品全部查询出来 ***//*
		// 查询列表数据
		List<MallGoods> goodsList = goodsService.querySelectiveV3(activityId, Integer.parseInt(categoryId), catLevel, brandId, keyword, isHot, isNew, page,
				limit, sort, order,industryId);


		List<Integer> goodIds = new ArrayList<Integer>();

		if (CollectionUtils.isNotEmpty(goodsList)) {
			// 2B的时候需要计算省赚金额(分销所得金额)
			if (shId == null) {

			} else {
				GoodsUtils.setRetailPrice(rebate, goodsList);
			}

			for (int i = 0; i < goodsList.size() ; i++) {
				goodIds.add(goodsList.get(i).getId());
			}

			//查询属性集合
			List<MallGoodsAttribute> attributeList = goodsAttributeService.queryListByGids(goodIds);
			//查询规格数据集合
			List<MallGoodsSpecification> specificationList = goodsSpecificationService.queryListByGids(goodIds);
			//查询sku集合
			List<MallGoodsProduct> productList = productService.queryListByGid(goodIds);

			for (int i = 0; i < goodsList.size(); i++) {
				List<String> finalImgList = new ArrayList<String>();
				List<String> myDList = new ArrayList<String>();
				MallGoods myGoods = goodsList.get(i);
				Integer goodsId = myGoods.getId();
				if (shId == null) {
					//计算省赚金额
					myGoods.setSpreads(GoodsUtils.getSpreads(myGoods));
				}
				try {

					if (myGoods != null && (org.apache.commons.lang.StringUtils.isEmpty(myGoods.getChannelCode())
							|| "JW".equalsIgnoreCase(myGoods.getChannelCode()))
							&& !StringUtils.isNullOrEmpty(myGoods.getDetail())) {
						Element doc = Jsoup.parseBodyFragment(myGoods.getDetail()).body();
						Elements pngs = doc.select("img[src]");
						for (Element element : pngs) {
							String imgUrl = element.attr("src");
							myDList.add(imgUrl);
						}
						myGoods.setDetail(JacksonUtils.bean2Jsn(myDList));
					}else{
						myDList = JacksonUtils.jsn2List(myGoods.getDetail(), String.class);
					}
				} catch (Exception e1) {
					logger.error("商品详情解析json异常" + e1.getMessage());
				}

//				finalImgList.addAll(Arrays.asList(myGoods.getGallery()));
				finalImgList.addAll(myDList);

				String[] str = new String[finalImgList.size()];

				//myGoods.setGallery(finalImgList.toArray(str));
				//筛选响应产品的属性数据
				List<MallGoodsAttribute> atCollect = new ArrayList<MallGoodsAttribute>();
				CollectionUtils.select(attributeList, new Predicate() {
					@Override
					public boolean evaluate(Object object) {
						MallGoodsAttribute attribute = (MallGoodsAttribute)object;
						return (goodsId!=null && goodsId.equals(attribute.getGoodsId())) ? true : false;
					}},atCollect);

				//筛选规格数据
				List<MallGoodsSpecification> spCollect = new ArrayList<MallGoodsSpecification>();
				CollectionUtils.select(specificationList, new Predicate() {
					@Override
					public boolean evaluate(Object object) {
						MallGoodsSpecification sp = (MallGoodsSpecification)object;
						return (goodsId!=null && goodsId.equals(sp.getGoodsId())) ? true : false;
					}},spCollect);

				  Map<String, VO> map = new HashMap<>();
				  specificationVoList = new ArrayList<VO>();


				for (MallGoodsSpecification goodsSpecification : spCollect) {
		            String specification = goodsSpecification.getSpecification();
		            VO goodsSpecificationVo = map.get(specification);
		            if (goodsSpecificationVo == null) {
		                goodsSpecificationVo = new VO();
		                goodsSpecificationVo.setName(specification);
		                List<MallGoodsSpecification> valueList = new ArrayList<>();
		                valueList.add(goodsSpecification);
		                goodsSpecificationVo.setValueList(valueList);
		                map.put(specification, goodsSpecificationVo);
		                specificationVoList.add(goodsSpecificationVo);
		            } else {
		                List<MallGoodsSpecification> valueList = goodsSpecificationVo.getValueList();
		                valueList.add(goodsSpecification);
		            }
		        }

				//产品筛选
				List<MallGoodsProduct> pdCollect = new ArrayList<MallGoodsProduct>();
				CollectionUtils.select(productList, new Predicate() {
					@Override
					public boolean evaluate(Object object) {
						MallGoodsProduct pd = (MallGoodsProduct)object;
						return (goodsId!=null && goodsId.equals(pd.getGoodsId())) ? true : false;
					}},pdCollect);

				Map<String, Object> goodsData = new HashMap<String, Object>();
				goodsData.put("info", myGoods);
				goodsData.put("specificationList", specificationVoList);
				goodsData.put("productList", pdCollect);
				goodsData.put("attribute", atCollect);

				dataList.add(goodsData);
			}
		}

		Map<String, Object> data = new HashMap<>();
		data.put("goodsList",dataList );
		data.put("count", PageInfo.of(goodsList).getTotal());

		return ResponseUtil.ok(data);*/
		return ResponseUtil.ok();
	}

	private Object findDetail(MallGoods info, String shId) {
		String id = info.getId();
		List<String> myDList = new ArrayList<String>();
		try {
			// 不是唯品仓同步过来的数据就要做处理 !"WPC".equals(info.getChannelCode())
			if ((org.apache.commons.lang.StringUtils.isEmpty(info.getChannelCode()) || "JW".equalsIgnoreCase(info.getChannelCode())) && info != null
					&& !StringUtils.isNullOrEmpty(info.getDetail())) {
				Element doc = Jsoup.parseBodyFragment(info.getDetail()).body();
				Elements pngs = doc.select("img[src]");
				for (Element element : pngs) {
					String imgUrl = element.attr("src");
					myDList.add(imgUrl);
				}
				info.setDetail(JacksonUtils.bean2Jsn(myDList));
			}

		} catch (Exception e1) {
			logger.error("商品详情解析json异常" + e1.getMessage());
		}
		// 商品属性
		List<MallGoodsAttribute> goodsAttributeList = goodsAttributeService.queryByGid(id);
		// 商品规格
		Object goodsSpecification = goodsSpecificationService.getSpecificationVoList(id);
		// 商品规格对应的数量和价格
		List<MallGoodsProduct> productList = productService.queryByGid(id);
		Map<String, Object> data = new HashMap<>();
		if (shId != null) {
			MallRebateRecord rebate = fenXiaoService.getMallRebateRecordById(shId);

			if (CollectionUtils.isNotEmpty(productList)) {
				// logger.info("获取的产品数据 :" +
				// JacksonUtils.bean2Jsn(productList));
				for (int i = 0; i < productList.size(); i++) {
					MallGoodsProduct myProduct = productList.get(i);
					BigDecimal nowAmount = myProduct.getPrice();
					BigDecimal getAmount = GoodsUtils.getAddAmount(rebate, nowAmount);
					myProduct.setPrice(nowAmount.add(getAmount));
				}
			}
			// 设置加价金额
			GoodsUtils.setRetailPrice(rebate, info);
		} else {
			// 计算省赚金额
			info.setSpreads(GoodsUtils.getSpreads(info));
		}

		data.put("info", info);
		data.put("specificationList", goodsSpecification);
		data.put("productList", productList);
		data.put("attribute", goodsAttributeList);

		return data;
	}

	/**
	 * 商品详情页面“大家都在看”推荐商品
	 *
	 * @param id【Integer->String】
	 *            , 商品ID
	 * @return 商品详情页面推荐商品
	 */
	@GetMapping("related")
	public Object related(@NotNull String id) {
		MallGoods goods = goodsService.findById(id);
		if (goods == null) {
			return ResponseUtil.badArgumentValue();
		}

		// 目前的商品推荐算法仅仅是推荐同类目的其他商品
		String cid = goods.getCategoryId();

		// 查找六个相关商品
		int related = 6;
		List<MallGoods> goodsList = goodsService.queryByCategory(cid, 0, related);
		Map<String, Object> data = new HashMap<>();
		data.put("goodsList", goodsList);
		return ResponseUtil.ok(data);
	}

	/**
	 * 在售的商品总数
	 *
	 * @return 在售的商品总数
	 */
	@GetMapping("count")
	public Object count() {
		Integer goodsCount = goodsService.queryOnSale();
		Map<String, Object> data = new HashMap<>();
		data.put("goodsCount", goodsCount);
		return ResponseUtil.ok(data);
	}

	private void syncGoodsImg(MallGoods goods) {
		try {
			if (goods == null || goods.getId() == null || goods.getName() == null || goods.getPicUrl() == null && !goods.getName().contains(" ")) {
				return;
			}
			if (!goods.getPicUrl().contains("a.vpimg2.com") && !goods.getPicUrl().contains("akmer.aikucun.com")) {
				return;
			}
			String[] goodsNameSplitArr = goods.getName().split(" ");
			if (goodsNameSplitArr == null || goodsNameSplitArr.length < 1) {
				return;
			}

			String goodsCodeSeq = goodsNameSplitArr[goodsNameSplitArr.length - 1];
			MallGoodsCodeSeq codeSeq = goodsCodeSeqService.selectByPrimaryKey(goodsCodeSeq);
			if (codeSeq == null) {
				return;
			}

			String content = goodsCodeSeq;
			String urlPath = goods.getPicUrl();

			String newUrlPath = this.uploadToAliyun(urlPath, content);

			MallGoods syncGoodsImgDto = new MallGoods();
			syncGoodsImgDto.setId(goods.getId());
			syncGoodsImgDto.setPicUrl(newUrlPath);

			String[] newGallery = null;
			if (goods.getGallery() != null && goods.getGallery().length > 0) {
				newGallery = new String[goods.getGallery().length];
				for (int i = 0; i < goods.getGallery().length; i++) {
					String url = goods.getGallery()[i];
					if (url.contains("a.vpimg2.com") || url.contains("akmer.aikucun.com")) {
						newGallery[i] = this.uploadToAliyun(url, content);
					} else {
						newGallery[i] = url;
					}
				}
				syncGoodsImgDto.setGallery(newGallery);
			}
			goodsService.updateById(syncGoodsImgDto);

			goods.setPicUrl(newUrlPath);
			if (newGallery != null) {
				goods.setGallery(newGallery);
			}
		} catch (NumberFormatException e1) {
			logger.error("add goodsCode2Img fail:" + goods.getId());
		} catch (Exception e) {
			logger.error("", e);
		}
	}

	private String uploadToAliyun(String urlPath, String content) throws Exception {

		ByteArrayOutputStream os = new ByteArrayOutputStream();
		int fontSize = 16;
		BufferedImage bg = ImageUtils.drawStringForNetImage(urlPath, content, Color.white, 6, 20, "Microsoft YaHei", fontSize, new Color(220, 99, 119), 0, 0,
				95, 30);

		ImageIO.write(bg, "jpg", os);

		InputStream ips = new ByteArrayInputStream(os.toByteArray());

		String format = "jpg";
		String fileName = AppIdAlgorithm.generateCode(6).toUpperCase() + System.currentTimeMillis() + ".jpg";
		MallStorage storage = storageService.store(ips, os.toByteArray().length, format, fileName);
		return storage.getUrl();
	}

	private class VO {
	    private String name;
	    private List<MallGoodsSpecification> valueList;

	    public String getName() {
	        return name;
	    }

	    public void setName(String name) {
	        this.name = name;
	    }

	    public List<MallGoodsSpecification> getValueList() {
	        return valueList;
	    }

	    public void setValueList(List<MallGoodsSpecification> valueList) {
	        this.valueList = valueList;
	    }
	}

}

