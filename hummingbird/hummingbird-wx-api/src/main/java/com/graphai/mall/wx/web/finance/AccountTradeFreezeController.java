package com.graphai.mall.wx.web.finance;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.lock.MutexLock;
import com.graphai.commons.util.lock.MutexLockUtils;
import com.graphai.commons.util.json.JacksonUtils;
import com.graphai.mall.db.domain.FcAccount;
import com.graphai.mall.db.domain.FcAccountFreezeBill;
import com.graphai.mall.db.service.account.AccountService;
import com.graphai.mall.db.service.account.CapitalAccountUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/capital", consumes = { MediaType.MULTIPART_FORM_DATA_VALUE,
		MediaType.APPLICATION_ATOM_XML_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE,
		MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE, MediaType.TEXT_HTML_VALUE,
		MediaType.TEXT_XML_VALUE, MediaType.APPLICATION_OCTET_STREAM_VALUE })
public class AccountTradeFreezeController {
	private final Log log = LogFactory.getLog(AccountTradeFreezeController.class);
	@Autowired
	private AccountService accountManager;
	
	/**
	 * 账户预占金额接口
	 * @param response
	 * @param request
	 * @param paramJson
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/account/preoccupancy", method = RequestMethod.POST)
	public Object preoccupancyAccount(HttpServletResponse response, HttpServletRequest request, 
			@RequestBody String paramJson) throws Exception{

		Map<String,Object> paramsMap = JacksonUtils.jsn2map(paramJson,String.class, Object.class);
		String apsKey = MapUtils.getString(paramsMap, "apsKey");
		//传过来的单位是元,需要转换成分
		String amountY = MapUtils.getString(paramsMap, "amount");
		log.info("接收参数apsKey：" + apsKey);
		log.info("接收参数amountY：" + amountY);
		Map<String,Object> resultMap = new HashMap<String, Object>();
		MutexLock mutexLock =MutexLockUtils.getMutexLock(apsKey);
		synchronized (mutexLock) {
			//账户不存在
			FcAccount oldCustomerAccount = accountManager.getAccountByKey(apsKey);
			//校验账户状态
			Object obj = CapitalAccountUtils.validationAccount(oldCustomerAccount);
			if(obj!=null){
				return obj;
			}
			
			BigDecimal consumeAmount =  new BigDecimal(amountY);
			BigDecimal nowAmount = oldCustomerAccount.getAmount();
			//判断账户金额是否大于消费金额，如果否 则预占用不成功
					
			int flag = nowAmount.compareTo(consumeAmount);
			if(flag<0){
				return ResponseUtil.fail(-13, "预占用消费金额失败,消费金额不足");
			}
			
			FcAccount freezeAccount = new FcAccount();
			freezeAccount.setAccountId(oldCustomerAccount.getAccountId());
			freezeAccount.setAmount(consumeAmount);
			freezeAccount.setFreezeUncashAmount(consumeAmount);
			
			FcAccountFreezeBill accountFreezeBill = accountManager.accountTradeFreeze(oldCustomerAccount, freezeAccount, paramsMap);
			resultMap.put("freezeAcBillId", accountFreezeBill.getFreezeAcBillId());
			resultMap.put("sn", accountFreezeBill.getSn());
			
			MutexLockUtils.remove(apsKey);
		}
		
		return ResponseUtil.ok("账户金额预占用成功",resultMap);
	}
}
