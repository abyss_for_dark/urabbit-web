package com.graphai.mall.wx.service;

import static com.graphai.mall.wx.util.WxResponseCode.AUTH_CAPTCHA_UNMATCH;
import static com.graphai.mall.wx.util.WxResponseCode.AUTH_OPENID_UNACCESS;
import static com.graphai.mall.wx.util.WxResponseCode.GROUPON_EXPIRED;
import static com.graphai.mall.wx.util.WxResponseCode.ORDER_COMMENTED;
import static com.graphai.mall.wx.util.WxResponseCode.ORDER_COMMENT_EXPIRED;
import static com.graphai.mall.wx.util.WxResponseCode.ORDER_INVALID;
import static com.graphai.mall.wx.util.WxResponseCode.ORDER_INVALID_OPERATION;
import static com.graphai.mall.wx.util.WxResponseCode.ORDER_PAY_FAIL;
import static com.graphai.mall.wx.util.WxResponseCode.ORDER_UNKNOWN;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.qrcode.QrCodeUtil;
import com.gexin.fastjson.JSON;
import com.github.binarywang.wxpay.bean.request.WxPayUnifiedOrderV3Request;
import com.github.binarywang.wxpay.bean.result.enums.TradeTypeEnum;
import com.graphai.commons.util.NameUtil;
import com.graphai.commons.util.lock.MutexLockUtils;
import com.graphai.mall.admin.dao.MallGoodsPositionMapper;
import com.graphai.mall.admin.domain.*;
import com.graphai.mall.admin.service.*;
import com.graphai.mall.db.constant.account.MallUserProfitNewEnum;
import com.graphai.mall.db.constant.activity.MallActivityEnum;
import com.graphai.mall.db.constant.coupon.CouponUserConstant;
import com.graphai.mall.db.constant.order.PurchaseEnum;
import com.graphai.mall.db.dao.FcAccountRechargeBillMapper;
import com.graphai.mall.admin.domain.MallGoodsPosition;
import com.graphai.mall.db.constant.group.GroupEnum;
import com.graphai.mall.db.constant.user.MallUserEnum;
import com.graphai.mall.db.dao.*;
import com.graphai.mall.db.domain.*;
import com.graphai.mall.db.service.common.MallRegionService;
import com.graphai.mall.db.service.coupon.MallCouponService;
import com.graphai.mall.db.service.order.*;
import com.graphai.mall.db.vo.GroupVo;
import com.graphai.mall.live.domain.MallLiveRoom;
import com.graphai.mall.live.service.IMallLiveRoomService;
import com.graphai.mall.live.service.impl.MallLiveRoomServiceImpl;
import com.graphai.mall.setting.service.FreightTemplateService;
import com.graphai.open.mallordergoods.entity.IMallOrderGoods;
import com.graphai.open.mallordergoods.service.IMallOrderGoodsService;
import com.graphai.validator.Order;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Header;
import org.apache.http.message.BasicHeader;
import org.hibernate.validator.internal.util.StringHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.domain.AlipayTradeAppPayModel;
import com.alipay.api.internal.util.AlipaySignature;
import com.alipay.api.request.AlipayTradeAppPayRequest;
import com.alipay.api.response.AlipayTradeAppPayResponse;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bytedance.caijing.tt_pay.TTPayLog;
import com.bytedance.caijing.tt_pay.TTPayService;
import com.bytedance.caijing.tt_pay.model.TradeCreateRequest;
import com.bytedance.caijing.tt_pay.model.TradeCreateResponse;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.github.binarywang.wxpay.bean.notify.WxPayNotifyResponse;
import com.github.binarywang.wxpay.bean.notify.WxPayOrderNotifyResult;
import com.github.binarywang.wxpay.bean.order.WxPayMpOrderResult;
import com.github.binarywang.wxpay.bean.request.WxPayUnifiedOrderRequest;
import com.github.binarywang.wxpay.bean.result.BaseWxPayResult;
import com.github.binarywang.wxpay.config.WxPayConfig;
import com.github.binarywang.wxpay.constant.WxPayConstants;
import com.github.binarywang.wxpay.exception.WxPayException;
import com.github.binarywang.wxpay.service.WxPayService;
import com.github.pagehelper.PageInfo;
import com.graphai.commons.constant.MallConstant;
import com.graphai.commons.util.IpUtil;
import com.graphai.commons.util.RegexUtil;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.date.DateUtilTwo;
import com.graphai.commons.util.httprequest.HttpUtils;
import com.graphai.commons.util.json.JacksonUtil;
import com.graphai.commons.util.json.JacksonUtils;
import com.graphai.commons.util.sms.TecentSmsUtil;
import com.graphai.framework.storage.StorageService;
import com.graphai.id.GraphaiIdGenerator;
import com.graphai.mall.core.qcode.QCodeService;
import com.graphai.mall.db.constant.PlateFromConstant;
import com.graphai.mall.db.service.MallCartService;
import com.graphai.mall.db.service.WpcApiService;
import com.graphai.mall.db.service.account.AccountService;
import com.graphai.mall.db.constant.account.AccountStatus;
import com.graphai.mall.db.service.account.FcAccountPrechargeBillService;
import com.graphai.mall.db.service.common.MallPhoneValidationService;
import com.graphai.mall.db.service.common.ShipService;
import com.graphai.mall.db.service.coupon.CouponVerifyService;
import com.graphai.mall.db.service.coupon.MallCouponUserService;
import com.graphai.mall.db.service.distribution.FenXiaoService;
import com.graphai.mall.db.service.goods.MallGoodsCouponCodeService;
import com.graphai.mall.db.service.goods.MallGoodsProductService;
import com.graphai.mall.db.service.goods.MallGoodsService;
import com.graphai.mall.db.service.groupon.MallGrouponRulesService;
import com.graphai.mall.db.service.groupon.MallGrouponService;
import com.graphai.mall.db.service.pay.DoPayService;
import com.graphai.mall.db.service.red.WxRedService;
import com.graphai.mall.db.service.sysconfig.MallSystemConfigService;
import com.graphai.mall.db.service.user.MallUserFormIdService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.service.user.behavior.MallAddressService;
import com.graphai.mall.db.service.user.behavior.MallCommentService;
import com.graphai.mall.db.service.wangzhuan.WangzhuanTaskService;
import com.graphai.mall.db.service.wangzhuan.WangzhuanTaskServiceV2;
import com.graphai.mall.db.constant.CommonConstant;
import com.graphai.mall.db.util.OrderHandleOption;
import com.graphai.mall.db.util.OrderUtil;
import com.graphai.mall.db.util.UserUtils;
import com.graphai.mall.db.util.account.bobbi.AccountQYUtils;
import com.graphai.mall.db.util.account.AccountUtils;
import com.graphai.mall.db.util.account.bobbi.AccountZYUtils;
import com.graphai.mall.db.vo.MallGroupVo;
import com.graphai.mall.db.vo.MallOrderGoodsVo;
import com.graphai.mall.wx.util.GoodsUtils;
import com.graphai.mall.wx.util.Md5Utils;
import com.graphai.mall.wx.util.OrderCalculationUtil;
import com.graphai.open.api.getway.HttpRequestUtils;
import com.graphai.payment.dto.PayChannel;
import com.graphai.payment.dto.SignParams;
import com.graphai.payment.enumerate.Channel;
import com.graphai.payment.sdk.PaymentSDK;
import com.graphai.properties.AliProperties;
import com.graphai.properties.DouYinProperties;
import com.graphai.properties.HuifuProperties;
import com.graphai.properties.PlatFormServerproperty;
import com.graphai.properties.WxProperties;
import com.huifu.adapay.model.payment.PayChannelEnum;
import com.huifu.adapay.util.AdaPaySign;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSONUtil;

/**
 * 订单服务
 *
 * <p>
 * 订单状态： 101 订单生成，未支付；102，下单后未支付用户取消；103，下单后未支付超时系统自动取消 201 支付完成，商家未发货；202，订单生产，已付款未发货，但是退款取消； 301
 * 商家发货，用户未确认； 401 用户确认收货； 402 用户没有确认收货超过一定时间，系统自动确认收货；
 *
 * <p>
 * 用户操作： 当101用户未付款时，此时用户可以进行的操作是取消订单，或者付款操作 当201支付完成而商家未发货时，此时用户可以取消订单并申请退款
 * 当301商家已发货时，此时用户可以有确认收货的操作 当401用户确认收货以后，此时用户可以进行的操作是删除订单，评价商品，或者再次购买
 * 当402系统自动确认收货以后，此时用户可以删除订单，评价商品，或者再次购买
 *
 * <p>
 * 注意：目前不支持订单退货和售后服务
 */
@Service
public class WxOrderService {
    private final Log logger = LogFactory.getLog(WxOrderService.class);
    public static final String BASE64_PREFIX = "data:image/png;base64,";


    @Autowired
    private WxProperties properties;
    @Autowired
    private AliProperties aliProperties;
    @Autowired
    private HuifuProperties huifuProperties;
    @Autowired
    private DouYinProperties douYinProperties;
    @Autowired
    private MallOrderNotifyLogService logService;
    @Autowired
    private FenXiaoService fenXiaoService;
    @Autowired
    private AccountService accountService;
    @Autowired
    private MallUserService userService;
    @Autowired
    private MallOrderService orderService;
    @Autowired
    private MallOrderGoodsService orderGoodsService;
    @Autowired
    private MallGoodsService goodsService;
    @Autowired
    private MallAddressService addressService;
    @Autowired
    private IMallLiveRoomService mallLiveRoomService;
    @Autowired
    private MallCartService cartService;
    @Autowired
    private MallGoodsProductService productService;
    @Resource(name = "wxPayService")
    private WxPayService wxPayService;
    @Resource(name = "hsWxPayService")
    private WxPayService hsWxPayService;
    @Resource(name = "hsWxGroupPayService")
    private WxPayService hsWxGroupPayService;
    @Resource(name = "wxMiniActivityPayService")
    private WxPayService wxMiniActivityPayService;
    @Resource(name = "wxMiniAgentPayService")
    private WxPayService wxMiniAgentPayService;
    @Resource(name = "wxPubPayService")
    private WxPayService wxPubPayService;
    @Autowired
    private WxPayConfig wxPayConfig;
    @Autowired
    private MallUserFormIdService formIdService;
    @Autowired
    private MallGrouponRulesService grouponRulesService;
    @Autowired
    private MallGrouponService grouponService;
    @Autowired
    private QCodeService qCodeService;
    @Autowired
    private MallCommentService commentService;
    @Autowired
    private MallCouponUserService couponUserService;
    @Autowired
    private CouponVerifyService couponVerifyService;
    @Autowired
    private ShipService shipService;
    @Autowired
    private WpcApiService wpcApiService;
    @Autowired
    private MallGoodsCouponCodeService mallGoodsCouponCodeService;
    @Autowired
    private PlatFormServerproperty platFormServerproperty;
    @Autowired
    private MallSystemConfigService mallSystemConfigService;
    @Autowired
    private FcAccountPrechargeBillService fcAccountPrechargeBillService;
    @Autowired
    private IMallUserMerchantService mallUserMerchantService;

    @Value("${mall.wx.wap-name}")
    private String wapName;
    @Value("${mall.wx.wap-url}")
    private String wapUrl;
    @Resource
    private MallMarketingActivityMapper mallMarketingActivityMapper;
    @Value("${weiRed.qrcodeUrl}")
    private String requestUrl;
    @Value("${weiRed.wlsqUrl}")
    private String wlsqUrl;
    @Autowired
    private IMallUserProfitNewService serviceImplUser;
    @Autowired
    private FcAccountPrechargeBillMapper fcAccountPrechargeBillMapper;
    @Autowired
    private WxRedService wxRedService;
    @Autowired
    private MallPhoneValidationService phoneService;
    @Autowired
    private DoPayService doPayService;
    @Autowired
    private WxOrderRefundService wxOrderRefundService;
    @Autowired
    private WangzhuanTaskService wangzhuanTaskService;
    @Autowired
    private WangzhuanTaskServiceV2 wangzhuanTaskServiceV2;
    @Autowired
    private IMallGoodsUserService iMallGoodsUserService;

    @Value("${mall.wx.cert-path}")
    private String cert_path;// 证书地址
    @Value("${mall.wx.refund-url}")
    private String refund_url;// 退款接口
    @Autowired
    private IMallUserProfitNewService userProfitNewService;
    @Autowired
    private MallGrouponRulesService  mallGrouponRulesService;
    @Autowired
    private FcAccountRechargeBillMapper fcAccountRechargeBillMapper;
    @Autowired
    private FcAccountRefundBillMapper fcAccountRefundBillMapper;
    @Autowired
    private MallGoodsPositionMapper goodsPositionMapper;
    @Autowired
    private MallGrouponMapper mallGrouponMapper;
    @Autowired
    private FreightTemplateService freightTemplateService;
    @Autowired
    private IMallActivityGoodsProductService mallActivityGoodsProductService;
    @Autowired
    private MallCouponUserService mallCouponUserService;
    @Autowired
    private MallCouponService mallCouponService;
    @Autowired
    private MallUserService mallUserService;
    @Autowired
    private MallAddressService mallAddressService;
    @Autowired
    private MallRegionService mallRegionService;
    @Autowired
    private IMallMerchantService mallMerchantService;
    @Autowired
    private MallAftersaleService mallAftersaleService;
    @Autowired
    private IMallPurchaseOrderService mallPurchaseOrderService;
    @Autowired
    private IMallPurchaseOrderGoodsService mallPurchaseOrderGoodsService;
    @Value("${public.urlPrefix}")
    private String urlPrefix;

    @Autowired
    private IMallOrderGoodsService iorderGoodsService;
    /**
     * 订单列表
     *
     * @param userId 用户ID
     * @param showType 订单信息： 0，全部订单； 1，待付款； 2，待发货； 3，待收货； 4，待评价 6 售后。
     * @param orderType 订单类型 (0:普通；秒杀；1：直播)
     * @param page 分页页数
     * @param limit 分页大小
     * @return 订单列表
     */
    public Object list(String userId,String orderIdOrMobile, Integer showType,Integer orderType, Integer page, Integer limit,String flag) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }

        List<Short> orderStatus = OrderUtil.orderStatus(showType);
        List<MallOrder> orderList = null;
        if ("agent".equals(flag)){
            MallUserMerchant userMerchant = mallUserMerchantService.getOne(new QueryWrapper<MallUserMerchant>().lambda().eq(MallUserMerchant::getUserId, userId));
            if(ObjectUtil.isNotNull(orderIdOrMobile) && ObjectUtil.isNotEmpty(orderIdOrMobile)) {
                orderList = orderService.selectByAgent(userMerchant.getMerchantId(), "'%" + orderIdOrMobile + "%'", orderStatus, page, limit);
            }else{
                orderList = orderService.selectByAgent(userMerchant.getMerchantId(), orderIdOrMobile, orderStatus, page, limit);
            }
        }else{
            if(ObjectUtil.isNotNull(orderIdOrMobile) && ObjectUtil.isNotEmpty(orderIdOrMobile)) {
                orderList = orderService.selectByMerchant(userId,"'%"+orderIdOrMobile+"%'", orderStatus, orderType, page, limit,showType);
            }else{
                orderList = orderService.selectByMerchant(userId,orderIdOrMobile, orderStatus, orderType, page, limit,showType);
            }
        }
        long count = PageInfo.of(orderList).getTotal();
        int totalPages = (int) Math.ceil((double) count / limit);

        List<Map<String, Object>> orderVoList = new ArrayList<>(orderList.size());
        for (MallOrder order : orderList) {
            Map<String, Object> orderVo = new HashMap<>();
            orderVo.put("id", order.getId());
            orderVo.put("orderSn", order.getOrderSn());
            orderVo.put("actualPrice", order.getActualPrice());
            orderVo.put("orderStatusText", OrderUtil.orderStatusText(order));
            orderVo.put("orderStatus", order.getOrderStatus());
            orderVo.put("orderTime", order.getAddTime());
            orderVo.put("consignee",order.getConsignee());
            orderVo.put("mobile",order.getMobile());
            orderVo.put("address",order.getAddress());
            orderVo.put("comments",order.getComments());

            List<MallOrderGoods> orderGoodsList = orderGoodsService.queryByOid(order.getId());
            orderVo.put("goodsNumber", orderGoodsList.size());
            //检查售后下能否发货 收货
            checkShip(order,orderVo);

            List<Map<String, Object>> orderGoodsVoList = new ArrayList<>(orderGoodsList.size());
            for (MallOrderGoods orderGoods : orderGoodsList) {
                Map<String, Object> orderGoodsVo = new HashMap<>();
                orderGoodsVo.put("id", orderGoods.getId());
                orderGoodsVo.put("goodsId", orderGoods.getGoodsId());
                orderGoodsVo.put("goodsName", orderGoods.getGoodsName());
                orderGoodsVo.put("number", orderGoods.getNumber());
                orderGoodsVo.put("picUrl", orderGoods.getPicUrl());
                orderGoodsVo.put("price", orderGoods.getPrice());
                orderGoodsVo.put("specifications", orderGoods.getSpecifications());
                orderGoodsVoList.add(orderGoodsVo);
            }
            orderVo.put("goodsList", orderGoodsVoList);

            // 用户
            MallUser mallUser = mallUserService.queryByUserId(order.getUserId());
            orderVo.put("mallUser", mallUser);

            orderVoList.add(orderVo);
        }

        Map<String, Object> result = new HashMap<>();
        result.put("count", count);
        result.put("data", orderVoList);
        result.put("totalPages", totalPages);

        return ResponseUtil.ok(result);
    }

    private void checkShip(MallOrder order,Map<String, Object> orderVo){
        //发货 该订单未发货过
        if (ObjectUtil.isNull(order.getShipTime()) && OrderUtil.STATUS_AFTERSALE_REFUND.toString().equals(order.getOrderStatus().toString())) {
            List<IMallOrderGoods> goodsList = iorderGoodsService.list(new QueryWrapper<IMallOrderGoods>()
                    .lambda().eq(IMallOrderGoods::getOrderId, order.getId()));
            List<MallAftersale> aftersales = mallAftersaleService.findByOrderId(order.getId());
            if (aftersales.size() < goodsList.size()) {
                //部分商品售后
                orderVo.put("showSend", true);
            } else {
                //售后申请是否全部审核过
                boolean flag = true;
                boolean tag = false;
                for (MallAftersale a : aftersales) {
                    for (IMallOrderGoods g : goodsList) {
                        if (a.getGoodsId().equals(g.getGoodsId())){
                            //审核过
                            if (!"0".equals(a.getStatus())) {
                                flag = false;
                                if ("3".equals(a.getType())){
                                    orderVo.put("showSend", true);
                                    tag = true;
                                    break;
                                }else{
                                    //是否同意退款(换货)
                                    if (a.getIsRefund()) {
                                        //售后数量是否跟购物数量一致
                                        if (!a.getNumber().equals(g.getNumber())){
                                            orderVo.put("showSend", true);
                                            tag =true;
                                            break;
                                        }
                                    }else{
                                        //只要有一件商品不同意退款就可以发货
                                        orderVo.put("showSend", true);
                                        tag =true;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    if (tag){
                        break;
                    }
                }
                //都未审核过
                if (flag) {
                    orderVo.put("showSend", true);
                }
            }
        }

        //收货  订单发货时间不能为空 收货时间为空
        if (ObjectUtil.isNotNull(order.getShipTime()) && ObjectUtil.isNull(order.getConfirmTime())
                && OrderUtil.STATUS_AFTERSALE_REFUND.toString().equals(order.getOrderStatus().toString())) {
            List<IMallOrderGoods> goodsList = iorderGoodsService.list(new QueryWrapper<IMallOrderGoods>()
                    .lambda().eq(IMallOrderGoods::getOrderId, order.getId()));
            List<MallAftersale> aftersales = mallAftersaleService.findByOrderId(order.getId());
            if (aftersales.size() < goodsList.size()) {
                //部分商品售后
                orderVo.put("receipt", true);
            } else {
                //售后申请是否全部审核过
                boolean flag = true;
                boolean tag = false;
                for (MallAftersale a : aftersales) {
                    for (IMallOrderGoods g : goodsList) {
                        if (a.getGoodsId().equals(g.getGoodsId())){
                            //审核过
                            if (!"0".equals(a.getStatus())) {
                                flag = false;
                                if ("3".equals(a.getType())){
                                    orderVo.put("receipt", true);
                                    tag = true;
                                    break;
                                }else{
                                    //是否同意退款(换货)
                                    if (a.getIsRefund()) {
                                        //售后数量是否跟购物数量一致
                                        if (!a.getNumber().equals(g.getNumber())){
                                            orderVo.put("receipt", true);
                                            tag =true;
                                            break;
                                        }
                                    }else{
                                        //只要有一件商品不同意退款就可以发货
                                        orderVo.put("receipt", true);
                                        tag =true;
                                        break;
                                    }

                                }
                            }
                        }
                    }
                    if (tag){
                        break;
                    }
                }
                //都未审核过
                if (flag) {
                    orderVo.put("receipt", true);
                }
            }
        }
        //售后中还能评价 有发货 收货 未评价过 未全部商品都退款了
        if (ObjectUtil.isNotNull(order.getShipTime()) && ObjectUtil.isNotNull(order.getConfirmTime())
                && OrderUtil.STATUS_AFTERSALE_REFUND.toString().equals(order.getOrderStatus().toString()) && order.getComments() > 0 ) {
            List<IMallOrderGoods> goodsList = iorderGoodsService.list(new QueryWrapper<IMallOrderGoods>()
                    .lambda().eq(IMallOrderGoods::getOrderId, order.getId()));
            List<MallAftersale> aftersales = mallAftersaleService.findByOrderId(order.getId());
            if (aftersales.size() < goodsList.size()) {
                //部分商品售后
                orderVo.put("appraisal", true);
            } else {
                //售后申请是否全部审核过
                boolean flag = true;
                boolean tag = false;
                for (MallAftersale a : aftersales) {
                    for (IMallOrderGoods g : goodsList) {
                        if (a.getGoodsId().equals(g.getGoodsId())) {
                            //审核过
                            if (!"0".equals(a.getStatus())) {
                                flag = false;
                                //(换货)
                                if ("3".equals(a.getType())){
                                    orderVo.put("appraisal", true);
                                    tag = true;
                                    break;
                                }else{
                                    //是否同意退款
                                    if (a.getIsRefund()) {
                                        //售后数量是否跟购物数量一致
                                        if (!a.getNumber().equals(g.getNumber())) {
                                            orderVo.put("appraisal", true);
                                            tag = true;
                                            break;
                                        }
                                    } else {
                                        //只要有一件商品不同意退款就可以发货
                                        orderVo.put("appraisal", true);
                                        tag = true;
                                        break;
                                    }
                                }

                            }
                        }
                    }
                    if (tag) {
                        break;
                    }
                }
                //都未审核过
                if (flag) {
                    orderVo.put("appraisal", true);
                }
            }
        }
    }

    public Object fenxiaoList(Integer showBelong, String userId, Integer showType, Integer page, Integer limit) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }

        System.out.println("要查询的数据是 :" + showBelong);
        /**
         * 不查询的订单状态
         */
        List<Short> notQueryOrderStatus = null;
        List<String> userIds = new ArrayList<String>();
        // 获取发展的下线
        List<MallUser> userList = userService.getFenxiaoSubordinate(userId);
        if (showBelong == 0) {
            // 看全部
            for (int i = 0; i < userList.size(); i++) {
                userIds.add(userList.get(i).getId());
            }
            // 自己的订单也要查询出来
            userIds.add(userId);
        } else if (showBelong == 1) {
            // 看分销
            for (int i = 0; i < userList.size(); i++) {
                userIds.add(userList.get(i).getId());
            }
        } else if (showBelong == 2) {
            // 看自己
            userIds.add(userId);
        } else {
            // 看全部
            for (int i = 0; i < userList.size(); i++) {
                userIds.add(userList.get(i).getId());
            }
            // 自己的订单也要查询出来
            userIds.add(userId);

            notQueryOrderStatus = OrderUtil.orderStatus(5);
        }


        // for (int i = 0; i < userList.size(); i++) {
        // userIds.add(userList.get(i).getId());
        // }
        // 自己的订单也要查询出来
        // userIds.add(userId);
        List<Short> orderStatus = OrderUtil.orderStatus(showType);
        // List<MallOrder> orderList = orderService.queryByOrderStatusFenxiaoList(userId, orderStatus, page,
        // limit);
        List<MallOrder> orderList =
                orderService.queryByOrderStatusaUserIds(userIds, orderStatus, notQueryOrderStatus, page, limit);

        long count = PageInfo.of(orderList).getTotal();
        int totalPages = (int) Math.ceil((double) count / limit);

        List<Map<String, Object>> orderVoList = new ArrayList<>(orderList.size());
        for (MallOrder order : orderList) {
            String orderId = order.getId();

            Map<String, Object> orderVo = new HashMap<>();
            orderVo.put("id", orderId);
            orderVo.put("orderSn", order.getOrderSn());
            orderVo.put("actualPrice", order.getActualPrice());
            orderVo.put("orderStatusText", OrderUtil.orderStatusText(order));
            orderVo.put("orderStatus", order.getOrderStatus());
            orderVo.put("handleOption", OrderUtil.build(order));
            orderVo.put("orderTime", order.getAddTime());
            /// 分销相关的数据
            orderVo.put("isCal", order.getIsCal());// 是否已经计算
            orderVo.put("primaryDividend", order.getPrimaryDividend());
            orderVo.put("secondaryDividend", order.getSecondaryDividend());
            orderVo.put("markupDividend", order.getMarkupDividend());
            orderVo.put("consignee", order.getConsignee());
            // 查询出当前人的分润充值记录
            // FcAccountRechargeBill reBill = accountService.getAccountRechargeBillByUserId(userId, orderId);

            if (!order.getUserId().equals(userId)) {
                // orderVo.put("tpModel", "ZJFR");
                /// MallUser user = userService.findById(userId);
                // orderVo.put("buyer", user.getNickname());

                if (userId != null && userId.toString().equals(order.getTpid())) {
                    orderVo.put("tpModel", "ZJFR");
                } else {
                    orderVo.put("tpModel", "JJFR");
                }

            } else {
                orderVo.put("tpModel", "ZJFR");
                orderVo.put("buyer", "本人购买");
            }


            if (order.getTpid() != null) {
                orderVo.put("tpUserName", order.getTpUserName());
            } else {
                String myOrderUserId = order.getUserId();
                MallUser tpUser = null;
                if (CollectionUtils.isNotEmpty(userList)) {
                    for (int i = 0; i < userList.size(); i++) {
                        if (myOrderUserId == userList.get(i).getId()) {
                            // 取出当前订单的分销人员
                            tpUser = userList.get(i);
                            break;
                        }
                    }
                }
                if (tpUser != null) {
                    orderVo.put("tpUserName", tpUser.getNickname());
                }
            }

            MallGroupon groupon = grouponService.queryByOrderId(order.getId());
            if (groupon != null) {
                orderVo.put("isGroupin", true);
            } else {
                orderVo.put("isGroupin", false);
            }

            List<MallOrderGoods> orderGoodsList = orderGoodsService.queryByOid(order.getId());
            orderVo.put("goodsNumber", orderGoodsList.size());
            List<Map<String, Object>> orderGoodsVoList = new ArrayList<>(orderGoodsList.size());
            for (MallOrderGoods orderGoods : orderGoodsList) {
                Map<String, Object> orderGoodsVo = new HashMap<>();
                orderGoodsVo.put("id", orderGoods.getId());
                orderGoodsVo.put("goodsName", orderGoods.getGoodsName());
                orderGoodsVo.put("number", orderGoods.getNumber());
                orderGoodsVo.put("picUrl", orderGoods.getPicUrl());
                orderGoodsVo.put("price", orderGoods.getPrice());
                orderGoodsVo.put("specifications", orderGoods.getSpecifications());
                orderGoodsVoList.add(orderGoodsVo);
            }
            orderVo.put("goodsList", orderGoodsVoList);

            orderVoList.add(orderVo);
        }

        Map<String, Object> result = new HashMap<>();
        result.put("count", count);
        result.put("data", orderVoList);
        result.put("totalPages", totalPages);

        return ResponseUtil.ok(result);
    }


    /**
     * 订单详情
     *
     * @param userId 用户ID
     * @param orderId 订单ID
     * @return 订单详情
     * @throws Exception
     */
    public Object detail(String userId, String orderId) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        Map<String, Object> result = new HashMap<>();
        // 订单信息
        MallOrder order = orderService.findById(orderId);
        if (null == order) {
            return ResponseUtil.fail(ORDER_UNKNOWN, "订单不存在");
        }
        if (!order.getUserId().equals(userId)) {
            return ResponseUtil.fail(ORDER_INVALID, "不是当前用户的订单");
        }
        Map<String, Object> orderVo = new HashMap<String, Object>();
        // 从库2查询权益商品的规格和详情描述

        orderVo.put("id", order.getId());
        orderVo.put("orderSn", order.getOrderSn());
        orderVo.put("addTime", order.getAddTime());
        orderVo.put("message", order.getMessage());
        orderVo.put("consignee", order.getConsignee());
        orderVo.put("mobile", order.getMobile());
        orderVo.put("address", order.getAddress());
        orderVo.put("goodsPrice", order.getGoodsPrice());
        orderVo.put("couponPrice", order.getCouponPrice());
        orderVo.put("freightPrice", order.getFreightPrice());
        orderVo.put("actualPrice", order.getActualPrice());
        orderVo.put("orderStatusText", OrderUtil.orderStatusText(order));
        orderVo.put("orderStatus", order.getOrderStatus());
        orderVo.put("handleOption", OrderUtil.build(order));
        orderVo.put("expCode", order.getShipChannel());
        orderVo.put("expNo", order.getShipSn());
        orderVo.put("industryId", order.getBid());
        orderVo.put("merchantId", order.getMerchantId());
        if (com.graphai.mall.db.util.StringHelper.isNotNullAndEmpty(order.getCityName())) {
            orderVo.put("cityName", order.getCityName());
        }

        if (com.graphai.mall.db.util.StringHelper.isNotNullAndEmpty(order.getAreaName())) {
            orderVo.put("storeName", order.getAreaName());
        }

        if (com.graphai.mall.db.util.StringHelper.isNotNullAndEmpty(order.getAddressDetail())) {
            orderVo.put("addressDetail", order.getAddressDetail());
        }


        JSONObject obj = null;
        List<Map<String, Object>> goodsList = new ArrayList<>();
        List<MallOrderGoods> orderGoodsList = orderGoodsService.queryByOid(order.getId());
        if (CollectionUtils.isNotEmpty(orderGoodsList)) {
            for (int i = 0; i < orderGoodsList.size(); i++) {
                if (Objects.equals("zy", order.getHuid())) {
                    try {
//                        if (StringUtils.isNotEmpty(orderGoodsList.get(i).getSrcGoodId())) {
//                            obj = (JSONObject) goodsService.queryQuanYiGoods(orderGoodsList.get(i).getSrcGoodId(), null,
//                                    null, order.getHuid());
//                            if (StringUtils.isNotEmpty(obj.getString("detail"))) {
//                                orderGoodsList.get(i).setGoodsShortUrl(obj.getString("detail"));
//                            }
//                            if (StringUtils.isNotEmpty(obj.getString("name"))) {
//                                String strArray[] = obj.getString("name").split(",");
//                                orderGoodsList.get(i).setSpecifications(strArray);
//                            }
//
//                            if (StringUtils.isNotEmpty(obj.getString("categoryId"))) {
//                                result.put("categoryId", obj.getString("categoryId"));
//                            }
//                        }

                        // 若卡密为空,则将卡号的数据填入卡密字段，因为卡号也可以充当卡密
                        if (com.graphai.mall.db.util.StringHelper.isNullOrEmpty(orderGoodsList.get(i).getCardPwd())) {
                            if (com.graphai.mall.db.util.StringHelper
                                    .isNotNullAndEmpty(orderGoodsList.get(i).getCardNumber())) {
                                orderGoodsList.get(i).setCardPwd(orderGoodsList.get(i).getCardNumber());
                            }
                        }

                        // 退货状态
                        MallAftersale mallAftersale = mallAftersaleService.findByOrderIdAndOrderGoodsId(orderId,orderGoodsList.get(i).getGoodsId());
                        Map<String, Object> map = new HashMap<>();
                        map.put("goods",orderGoodsList.get(i));
                        map.put("mallAftersale",mallAftersale);
                        goodsList.add(map);
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                } else if (Objects.equals("fuluGoods", order.getHuid()) || Objects.equals("zmGoods", order.getHuid())
                        || Objects.equals("lsxdGoods", order.getHuid())) {
                    try {
                        if (StringUtils.isNotEmpty(orderGoodsList.get(i).getProductId())
                                && StringUtils.isNotEmpty(orderGoodsList.get(i).getGoodsId())) {
                            obj = (JSONObject) goodsService.queryQuanYiGoods(null, orderGoodsList.get(0).getProductId(),
                                    orderGoodsList.get(i).getGoodsId(), order.getHuid());
                            if (StringUtils.isNotEmpty(obj.getString("specifications"))) {
                                String s = obj.getString("specifications").replaceAll("\"", "");
                                String strSub = s.substring(1, s.length() - 1);
                                String strArray[] = strSub.split(",");
                                orderGoodsList.get(i).setSpecifications(strArray);
                            }

                            if (StringUtils.isNotEmpty(obj.getString("detail"))) {
                                orderGoodsList.get(i).setGoodsShortUrl(obj.getString("detail"));
                            }

                            if (StringUtils.isNotEmpty(obj.getString("categoryId"))) {
                                result.put("categoryId", obj.getString("categoryId"));
                            }
                        }
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                } else if (Objects.equals("pdd", order.getHuid()) || Objects.equals("jd", order.getHuid())
                        || Objects.equals("sn", order.getHuid()) || Objects.equals("wph", order.getHuid())
                        || Objects.equals("dtk", order.getHuid())) {
                    if (com.graphai.mall.db.util.StringHelper
                            .isNotNullAndEmpty(orderGoodsList.get(i).getActualPrice())) {
                        orderGoodsList.get(i).setPrice(orderGoodsList.get(i).getActualPrice());
                    }
                }

            }

        }
        MallMerchant mallMerchant = mallMerchantService.getById(order.getMerchantId());

        result.put("mallMerchant", mallMerchant);
        result.put("orderInfo", orderVo);
        result.put("orderGoods", goodsList);

        // 订单状态为已发货且物流信息不为空
        // "YTO", "800669400640887922"
        if (order.getOrderStatus().equals(OrderUtil.STATUS_SHIP)) {
            // ExpressInfo ei = expressService.getExpressInfo(order.getShipChannel(), order.getShipSn());
            Object object = shipService.getLogisticsInfo(order.getShipSn(), order.getShipCode());
            logger.info("查询订单物流数据 :" +  JacksonUtils.bean2Jsn(object));
            result.put("expressInfo", object);
        }


        return ResponseUtil.ok(result);

    }


    /**
     * 订单详情
     *
     * @param orderId 订单ID
     * @return 订单详情
     * @throws Exception
     */
    public Object hsDetail(String orderId) {
        Map<String, Object> result = new HashMap<>();
        // 订单信息
        MallOrder order = orderService.findById(orderId);
        if (null == order) {
            return ResponseUtil.fail(ORDER_UNKNOWN, "订单不存在");
        }

        String groupId = "";
        Integer num = 0;
        boolean isGroup = false;
        Map<String, Object> orderVo = new HashMap<String, Object>();
        Map<String, Object> fpOrderVo = new HashMap<String, Object>();
        // 从库2查询权益商品的规格和详情描述

        orderVo.put("id", order.getId());
        orderVo.put("orderSn", order.getOrderSn());
        orderVo.put("addTime", order.getAddTime());
        orderVo.put("payTime", order.getPayTime());
        orderVo.put("confirmTime", order.getConfirmTime());
        orderVo.put("consignee", order.getConsignee());
        orderVo.put("mobile", order.getMobile());
        orderVo.put("address", order.getAddress());
        orderVo.put("goodsPrice", order.getGoodsPrice());
        orderVo.put("couponPrice", order.getCouponPrice());
        orderVo.put("freightPrice", order.getFreightPrice());
        orderVo.put("actualPrice", order.getActualPrice());
        orderVo.put("orderStatusText", OrderUtil.orderStatusText(order));
        orderVo.put("orderStatus", order.getOrderStatus());
        orderVo.put("handleOption", OrderUtil.build(order));
        orderVo.put("expCode", order.getShipChannel());
        orderVo.put("expNo", order.getShipSn());
        orderVo.put("pubSharePreFee",order.getPubSharePreFee());
        orderVo.put("orderThreeStatus", order.getOrderThreeStatus()); // 拼团状态
        orderVo.put("retrunAmount",order.getTotalCommissionFee());
        JSONObject obj = null;

        List<MallOrderGoods> fpOrderGoodsList = new ArrayList<>();
        List<MallOrderGoods> orderGoodsList = orderGoodsService.queryByOid(order.getId());
        if (CollectionUtils.isNotEmpty(orderGoodsList)) {
            if (Objects.equals("zy", order.getHuid())) {
                try {
                    if (StringUtils.isNotEmpty(orderGoodsList.get(0).getSrcGoodId())) {
                        obj = (JSONObject) goodsService.queryQuanYiGoods(orderGoodsList.get(0).getSrcGoodId(), null,
                                null, order.getHuid());
                        if (StringUtils.isNotEmpty(obj.getString("detail"))) {
                            orderGoodsList.get(0).setGoodsShortUrl(obj.getString("detail"));
                        }
                        if (StringUtils.isNotEmpty(obj.getString("name"))) {
                            String strArray[] = obj.getString("name").split(",");
                            orderGoodsList.get(0).setSpecifications(strArray);
                        }

                        if (StringUtils.isNotEmpty(obj.getString("categoryId"))) {
                            result.put("categoryId", obj.getString("categoryId"));
                        }
                    }


                } catch (Exception e) {

                    e.printStackTrace();
                }
            } else if (Objects.equals("fuluGoods", order.getHuid()) || Objects.equals("zmGoods", order.getHuid())
                    || Objects.equals("lsxdGoods", order.getHuid())) {
                try {
                    if (StringUtils.isNotEmpty(orderGoodsList.get(0).getProductId())
                            && StringUtils.isNotEmpty(orderGoodsList.get(0).getGoodsId())) {
                        obj = (JSONObject) goodsService.queryQuanYiGoods(null, orderGoodsList.get(0).getProductId(),
                                orderGoodsList.get(0).getGoodsId(), order.getHuid());
                        if (StringUtils.isNotEmpty(obj.getString("specifications"))) {
                            String s = obj.getString("specifications").replaceAll("\"", "");
                            String strSub = s.substring(1, s.length() - 1);
                            String strArray[] = strSub.split(",");
                            orderGoodsList.get(0).setSpecifications(strArray);
                        }

                        if (StringUtils.isNotEmpty(obj.getString("detail"))) {
                            orderGoodsList.get(0).setGoodsShortUrl(obj.getString("detail"));
                        }

                        if (StringUtils.isNotEmpty(obj.getString("categoryId"))) {
                            result.put("categoryId", obj.getString("categoryId"));
                        }
                    }
                } catch (Exception e) {

                    e.printStackTrace();
                }
            } else if (Objects.equals(order.getBid(), AccountStatus.BID_36)) {
                isGroup = true;
                orderVo.put("Isgroup", isGroup);
                MallGroupon groupon = grouponService.queryByPayOrderId(order.getParentOrderOn());
                if (!Objects.equals(null, groupon)) {
                    if("0".equalsIgnoreCase(groupon.getGrouponId())){
                        orderVo.put("groupId", groupon.getId());
                    }else {
                        orderVo.put("groupId", groupon.getGrouponId());
                    }
                    if (Objects.equals("0", groupon.getGrouponId())) {
                        groupId = groupon.getId();
                    } else {
                        groupId = groupon.getGrouponId();
                    }
                    orderVo.put("joinTime", order.getPayTime());
                    orderVo.put("groupNo", groupId);
                    List<MallGroupVo> groupList = grouponService.queryGroupByIdV2(groupId, null, null);
                    orderVo.put("joinUsers", groupList);
                }
                QueryWrapper<MallGoodsPosition> gPWrapper = new QueryWrapper<>();
                gPWrapper.eq("goods_id",orderGoodsList.get(0).getGoodsId());
                MallGoodsPosition goodsPosition = goodsPositionMapper.selectOne(gPWrapper);

                // 获取参与团的人数
                // Integer joinNum = grouponService.queryJoinGrouponCountByGroupId(groupId);
                // if (num > joinNum) {
                // orderVo.put("surplusNum", num - joinNum);
                // }
                // 团购信息
                if (Objects.equals("groupgoods", order.getHuid())) {
                    //返回退款状态/退款金额
                        orderVo.put("orderStatusDesc",order.getOrderStatusDesc()== null ? null : order.getOrderStatusDesc());
//                        if (!StringUtil.isEmpty(order.getOrderStatusDesc()) && order.getOrderStatusDesc().equals("106")){
                            FcAccountRefundBillExample refundBillExample = new FcAccountRefundBillExample();
                            refundBillExample.createCriteria().andOrderIdEqualTo(Long.parseLong(order.getId()));
                            FcAccountRefundBill fcAccountRefundBill = fcAccountRefundBillMapper.selectOneByExample(refundBillExample);
                            if (fcAccountRefundBill != null){
                                if (fcAccountRefundBill.getCashAmt().compareTo(BigDecimal.ZERO) == 0){
                                    orderVo.put("cashAmt",fcAccountRefundBill.getCashAmt().toString());
                                }else{
                                    orderVo.put("cashAmt",fcAccountRefundBill.getCashAmt());
                                }
                            }
//                        }

                    // 团购2.0满团后返回开奖时间(拼团结束时间)
                    if (Objects.equals(200, order.getOrderThreeStatus())
                            || Objects.equals(500, order.getOrderThreeStatus())|| Objects.equals(199, order.getOrderThreeStatus())|| Objects.equals(501, order.getOrderThreeStatus())) {
                        MallGroupon group = grouponService.queryById(groupId);
                        if (!Objects.equals(null, group.getUpdateTime())
                                && group.getUpdateTime().isAfter(group.getAddTime())) {
                            orderVo.put("groupEndTime", group.getUpdateTime());
                            fpOrderVo.put("fpGroupEndTime", group.getUpdateTime());
                        }
                        if (goodsPosition != null && goodsPosition.getType() != null && goodsPosition.getType() == 1){
                            orderVo.put("cardNumber", orderGoodsList.get(0).getCardNumber());
                        }

                        try {
                            MallGrouponRules rules = grouponRulesService.queryByIdV2(groupon.getRulesId());
                            List<MallGoodsProduct> products=productService.queryByGid(orderGoodsList.get(0).getGoodsId());
                            BigDecimal returnAmount = ((products.get(0).getFaceValue()== null ? BigDecimal.ZERO : products.get(0).getFaceValue().subtract(
                                    products.get(0).getFactoryPrice() == null ? BigDecimal.ZERO : products.get(0).getFactoryPrice()))
                                    .multiply(new BigDecimal(rules.getSuccessNum()))
                                    .multiply(rules.getReturnRatio())) .setScale(2,
                                    BigDecimal.ROUND_HALF_UP)
                                    ;
                            if (returnAmount.compareTo(BigDecimal.ZERO) == 1) {
                                orderVo.put("returnAmount", returnAmount);
                                fpOrderVo.put("fpReturnAmount", returnAmount);
                            }
                        } catch (Exception e2) {
                            e2.printStackTrace();
                        }

                    } else if (Objects.equals(197, order.getOrderThreeStatus())|| Objects.equals(198, order.getOrderThreeStatus())) {
                        orderVo.put("groupEndTime", order.getDrawTime());
                        fpOrderVo.put("fpGroupEndTime", order.getDrawTime());
                    }
                    List<MallOrder> fpMallOrders = orderService.selectList(order.getParentOrderOn(), 3);
                    if (fpMallOrders.size()>0){
                        fpOrderGoodsList = orderGoodsService.queryByOid(fpMallOrders.get(0).getId());
                        if(fpMallOrders.get(0).getOrderStatus() != 201){
                            orderVo.put("cardNumber",null);
                        }
                        fpOrderVo.put("fpId", fpMallOrders.get(0).getId());
                        fpOrderVo.put("fpOrderSn", fpMallOrders.get(0).getOrderSn());
                        fpOrderVo.put("fpAddTime", fpMallOrders.get(0).getAddTime());
                        fpOrderVo.put("fpPayTime", fpMallOrders.get(0).getPayTime());
                        fpOrderVo.put("fpConfirmTime", fpMallOrders.get(0).getConfirmTime());
                        fpOrderVo.put("fpConsignee", fpMallOrders.get(0).getConsignee());
                        fpOrderVo.put("fpMobile", fpMallOrders.get(0).getMobile());
                        fpOrderVo.put("fpAddress", fpMallOrders.get(0).getAddress());
                        fpOrderVo.put("fpGoodsPrice", fpMallOrders.get(0).getGoodsPrice());
                        fpOrderVo.put("fpCouponPrice", fpMallOrders.get(0).getCouponPrice());
                        fpOrderVo.put("fpFreightPrice", 0);
                        fpOrderVo.put("fpActualPrice", fpMallOrders.get(0).getActualPrice());
                        fpOrderVo.put("fpOrderStatusText", OrderUtil.orderStatusText(fpMallOrders.get(0)));
                        fpOrderVo.put("fpOrderStatus", fpMallOrders.get(0).getOrderStatus());
                        fpOrderVo.put("fpHandleOption", OrderUtil.build(fpMallOrders.get(0)));
                        fpOrderVo.put("fpExpCode", fpMallOrders.get(0).getShipChannel());
                        fpOrderVo.put("fpExpNo", fpMallOrders.get(0).getShipSn());
                        fpOrderVo.put("fpOrderThreeStatus", fpMallOrders.get(0).getOrderThreeStatus()); // 拼团
                        fpOrderVo.put("fpOrderExPireTime", fpMallOrders.get(0).getDrawTime());
                        fpOrderVo.put("fpPubSharePreFee",fpMallOrders.get(0).getPubSharePreFee());
//                        if (!StringUtil.isEmpty(fpMallOrders.get(0).getOrderStatusDesc())){

                        orderVo.put("fpOrderStatusDesc",fpMallOrders.get(0).getOrderStatusDesc() == null ? null : fpMallOrders.get(0).getOrderStatusDesc() );
//                            if (!StringUtil.isEmpty(fpMallOrders.get(0).getOrderStatusDesc()) && fpMallOrders.get(0).getOrderStatusDesc().equals("106")){
                        FcAccountRefundBillExample refundBillV1Example = new FcAccountRefundBillExample();
                        refundBillV1Example.createCriteria().andOrderIdEqualTo(Long.parseLong(fpMallOrders.get(0).getId()));
                        FcAccountRefundBill FcAccountRefundBillV1 = fcAccountRefundBillMapper.selectOneByExample(refundBillV1Example);
                                if (FcAccountRefundBillV1 != null){
                                    orderVo.put("cashAmt",FcAccountRefundBillV1.getCashAmt());
                                }

//                            }
//                        }

                        result.put("fpOrderInfo", fpOrderVo);
                    }

                    MallGoodsUser goodsUser = iMallGoodsUserService.queryByGoodsId(orderGoodsList.get(0).getGoodsId());
                    if (!Objects.equals(null, goodsUser)) {
                        orderVo.put("merMobile", goodsUser.getMobile());
                        fpOrderVo.put("fpMerMobile", goodsUser.getMobile());
                    }

                }

            } else {
                if (Objects.equals(order.getBid(), AccountStatus.BID_27)) {
                    JSONObject goodObject;
                    try {
                        goodObject = goodsService.queryGoodsById(orderGoodsList.get(0).getGoodsId());
                        logger.info("商品信息：--" + goodObject);
                        if (Objects.equals(null, goodObject)) {
                            logger.error("商品信息有误!");
                        }
                        orderVo.put("shareAccelerate", goodObject.getInteger("shareAccelerate"));
                        FcAccountPrechargeBillExample example = new FcAccountPrechargeBillExample();
                        example.createCriteria().andOrderIdEqualTo(order.getId())
                                .andCustomerIdEqualTo(order.getUserId());
                        FcAccountPrechargeBill fcAccountPrechargeBill =
                                fcAccountPrechargeBillMapper.selectOneByExample(example);
                        if (fcAccountPrechargeBill != null)
                            orderVo.put("amt", fcAccountPrechargeBill.getAmt());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
        }
        result.put("orderInfo", orderVo);
        result.put("orderGoods", orderGoodsList);
        if (fpOrderGoodsList != null){
            result.put("fprderGoods",fpOrderGoodsList);
        }

        return ResponseUtil.ok(result);


    }


    /**
     * 插入福禄、中闽在线、蓝色兄弟 订单 type fuluGoods 、 zmGoods 、 lsxdGoods 通用方法，进行下单。 todo 退款方法，现在暂时没做
     *
     * 整体逻辑： 1、获取下单的基本信息 2、进行校验 3、开放平台接口调用, 返回结果，日志记录 4、如果是福禄平台，获取卡密信息，进行订单商品卡密修改 5、进行 差价 分佣计算（商品销售价 -
     * 出厂价 = 差价） 6、充值结果回调，更新状态，日志记录 7、完成 定位问题，sql select * from mall_order where huid = 'fuluGoods';
     * select * from mall_order where huid = 'zmGoods'; select * from mall_order where huid =
     * 'lsxdGoods';
     *
     * select * from mall_order_notify_log where wx_params = 'fulu'; select * from mall_order_notify_log
     * where wx_params = 'zm'; select * from mall_order_notify_log where wx_params = 'lsxd';
     *
     * @return
     */
    @Transactional(rollbackFor = {Exception.class})
    private Object insertOrderByThree(String userId, String body, String clientIp, String type) {

        // 蓝色兄弟专用
        String REGEX_QQ = "(^[1-3]{1}\\d{4,9}$)|(^[1-9]{1}\\d{4,8}$)"; // QQ号正则验证表达式
        String REGEX_MOBILE = "^1[3456789]\\d{9}$"; // 手机号正则验证表达式

        // 三方平台通用方
        String goods_name = JacksonUtil.parseString(body, "goods_name");
        String goods_sn = JacksonUtil.parseString(body, "goods_sn");
        String goods_id = JacksonUtil.parseString(body, "goods_id");
        String goods_product_id = JacksonUtil.parseString(body, "goods_product_id"); // 商品产品id
        String product_type = JacksonUtil.parseString(body, "product_type");
        // 销售价
        String goods_price = JacksonUtil.parseString(body, "goods_price");


        String factory_price = JacksonUtil.parseString(body, "factory_price");

        // 类目ID
        String categoryId = JacksonUtil.parseString(body, "categoryId");
        String categoryPic = JacksonUtil.parseString(body, "categoryPic");

        // 手机号 \ qq号
        String charge_account = JacksonUtil.parseString(body, "charge_account");
        String buy_num = JacksonUtil.parseString(body, "buy_num");

        // 蓝色兄弟专用
        // 充值类型 （1手机 2 QQ）
        String account_type = JacksonUtil.parseString(body, "account_type");
        // String extend_parameter = JacksonUtil.parseString(body, "extend_parameter");

        if (Objects.equals(type, "lsxdGoods")) {
            if (charge_account.matches(REGEX_MOBILE)) {
                logger.info("充值的类型是手机号" + charge_account);
                account_type = "1";
            } else if (charge_account.matches(REGEX_QQ)) {
                account_type = "2";
                logger.info("充值的类型是QQ号" + charge_account);
            }
        }


        if (StringHelper.isNullOrEmptyString(goods_id)) {
            return ResponseUtil.badArgument();
        }
        // if (StringHelper.isNullOrEmptyString(goods_product_id)) {
        // return ResponseUtil.badArgument();
        // }
        if (StringHelper.isNullOrEmptyString(goods_sn)) {
            return ResponseUtil.badArgument();
        }
        if (StringHelper.isNullOrEmptyString(goods_price)) {
            return ResponseUtil.badArgument();
        }

        if (!Objects.equals(product_type, "卡密") && StringHelper.isNullOrEmptyString(charge_account)) {
            return ResponseUtil.badArgument();
        }

        if (StringHelper.isNullOrEmptyString(buy_num)) {
            buy_num = "1";
        }


        /** 验证商品产品价格是否一致 **/
        // todo 远程调用 'getGoodsProductById'接口查询 商品产品信息, 发版后面去除
        if (StringUtils.isNotBlank(goods_product_id)) {
            Map<String, Object> responseMap = null;
            try {
                String url = platFormServerproperty.getUrl(MallConstant.TAOBAO);
                Map<String, Object> map = new HashMap<>();
                map.put("plateform", "dtk");
                map.put("id", goods_product_id);
                Header[] headers = new BasicHeader[] {new BasicHeader("method", "getGoodsProductById"),
                        new BasicHeader("version", "v1.0")};
                String result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(map), headers);
                JSONObject object = JSONObject.parseObject(result);
                if ("0".equals(object.getString("errno"))) {
                    Object productData = object.get("data");
                    if (ObjectUtil.isNotNull(productData)) {
                        responseMap = (Map<String, Object>) JSONArray.parseObject(productData.toString(), Map.class);
                    }
                }
            } catch (Exception e) {
                logger.info("查询商品产品信息失败！");
            }
            if (responseMap == null) {
                return ResponseUtil.badArgumentValue("商品产品不存在！");
            }
            Object goodPrice = responseMap.get("price"); // 商品产品价格
            if (goodPrice == null) {
                return ResponseUtil.badArgumentValue("商品产品价格出错！");
            }
            if (new BigDecimal(String.valueOf(goodPrice)).compareTo(new BigDecimal(goods_price)) != 0) {
                return ResponseUtil.badArgumentValue("商品产品价格错误！");
            }
        }
        // todo 远程调用接口查询 商品产品信息,后面发版去除


        MallOrder order = new MallOrder();
        order.setUserId(userId);
        order.setOrderSn(GraphaiIdGenerator.nextId("OrderSN"));
        order.setOrderStatus(OrderUtil.STATUS_CREATE);
        order.setMobile(charge_account);
        order.setMessage("购买权益商品");
        // 面值
        order.setGoodsPrice(new BigDecimal(goods_price));
        order.setFreightPrice(BigDecimal.ZERO);
        order.setCouponPrice(BigDecimal.ZERO);
        order.setIntegralPrice(BigDecimal.ZERO);
        // 订单总费用
        BigDecimal actualPrice = new BigDecimal(goods_price).multiply(new BigDecimal(buy_num));
        order.setActualPrice(actualPrice);
        order.setOrderPrice(actualPrice);
        order.setHuid(type);
        order.setClientIp(clientIp);
        order.setGrouponPrice(BigDecimal.ZERO); // 团购价格
        order.setBid(AccountStatus.BID_9);

        // 添加订单表
        orderService.add(order);

        for (int i = 1; i <= Integer.parseInt(buy_num); i++) {
            // 添加订单商品表项
            // 订单商品
            MallOrderGoods orderGoods = new MallOrderGoods();
            orderGoods.setOrderId(order.getId());
            orderGoods.setGoodsId(goods_id);
            orderGoods.setGoodsSn(goods_sn);
            orderGoods.setProductId(goods_product_id);
            orderGoods.setSrcGoodId(goods_sn);
            orderGoods.setGoodsName(goods_name);
            orderGoods.setPrice(new BigDecimal(goods_price));
            orderGoods.setNumber(Short.valueOf("1"));
            orderGoods.setAddTime(LocalDateTime.now());
            // 临时用于存储商品类型
            orderGoods.setComment(product_type);
            // 设置类目的图片，当作权益的商品
            orderGoods.setChargeType(account_type);
            orderGoods.setPicUrl(categoryPic);
            orderGoods.setUserId(userId);
            orderGoodsService.add(orderGoods);
        }

        if (StringHelper.isNullOrEmptyString(order.getId())) {
            return ResponseUtil.fail();
        }

        if (com.graphai.mall.db.util.StringHelper.isNotNullAndEmpty(factory_price)
                && com.graphai.mall.db.util.StringHelper.isNotNullAndEmpty(goods_price)) {
            // 出场价
            BigDecimal factoryPrice = new BigDecimal(factory_price);
            // 销售定价
            BigDecimal price = new BigDecimal(goods_price);
            // 佣金
            BigDecimal subtract = price.subtract(factoryPrice);
            if (subtract.intValue() > 0) {
                Map<String, String> map = new HashMap(6);
                map.put("userId", userId);
                map.put("bid", AccountStatus.BID_9);
                map.put("totalMoney", order.getPubSharePreFee());
                map.put("oderId", order.getId());
                map.put("rechargeType", CommonConstant.TASK_REWARD_CASH);
                map.put("orderSn", order.getOrderSn());
                try {
                    Map<String, Object> rateMap = (Map<String, Object>) AccountUtils.preCharge(map);
                    if (ObjectUtil.isNotNull(rateMap) && rateMap.containsKey("commissionReward")) {
                        if (com.graphai.mall.db.util.StringHelper.isNotNullAndEmpty(rateMap.get("commissionReward"))) {
                            BigDecimal commissionfee = new BigDecimal(String.valueOf(rateMap.get("commissionReward")));
                            order.setPubSharePreFee(String.valueOf(commissionfee));
                        } else {
                            order.setPubSharePreFee("0.0");
                        }
                    } else {
                        order.setPubSharePreFee("0.0");
                    }
                } catch (Exception e) {
                    logger.info("权益数据预存佣金出现异常！");
                    e.printStackTrace();
                }
            } else {
                order.setPubSharePreFee("0.0");
            }
        }

        Map<String, Object> data = new HashMap<>();
        data.put("orderId", order.getId());
        return ResponseUtil.ok(data);
    }


    private Object insertOrderByCost(String userId, String body, String clientIp, String type) {
        // 话费充值
        String goods_name = JacksonUtil.parseString(body, "goods_name");
        String goods_sn = JacksonUtil.parseString(body, "goods_sn");
        String goods_id = JacksonUtil.parseString(body, "goods_id");
        String goods_product_id = JacksonUtil.parseString(body, "goods_product_id"); // 商品产品id
        String product_type = JacksonUtil.parseString(body, "product_type");
        // 销售价
        String goods_price = JacksonUtil.parseString(body, "goods_price");
        // 实际充值金额
        // String factory_price = JacksonUtil.parseString(body, "factory_price");

        String categoryPic = JacksonUtil.parseString(body, "categoryPic");


        // 手机号
        String charge_account = JacksonUtil.parseString(body, "charge_account");
        String buy_num = JacksonUtil.parseString(body, "buy_num");


        if (StringHelper.isNullOrEmptyString(goods_id)) {
            return ResponseUtil.badArgument();
        }
        if (StringHelper.isNullOrEmptyString(goods_product_id)) {
            return ResponseUtil.badArgument();
        }
        if (StringHelper.isNullOrEmptyString(goods_sn)) {
            return ResponseUtil.badArgument();
        }
        if (StringHelper.isNullOrEmptyString(goods_price)) {
            return ResponseUtil.badArgument();
        }

        // if (StringHelper.isNullOrEmptyString(factory_price)) {
        // return ResponseUtil.badArgument();
        // }

        if (StringHelper.isNullOrEmptyString(charge_account)) {
            return ResponseUtil.badArgument();
        }

        if (StringHelper.isNullOrEmptyString(buy_num)) {
            buy_num = "1";
        }


        Object faceValue = null;

        /** 验证商品产品价格是否一致 **/
        // todo 远程调用 'getGoodsProductById'接口查询 商品产品信息, 发版后面去除
        if (StringUtils.isNotBlank(goods_product_id)) {
            Map<String, Object> responseMap = null;
            try {
                String url = platFormServerproperty.getUrl(MallConstant.TAOBAO);
                Map<String, Object> map = new HashMap<>();
                map.put("plateform", "dtk");
                map.put("id", goods_product_id);
                Header[] headers = new BasicHeader[] {new BasicHeader("method", "getGoodsProductById"),
                        new BasicHeader("version", "v1.0")};
                String result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(map), headers);
                JSONObject object = JSONObject.parseObject(result);
                if ("0".equals(object.getString("errno"))) {
                    Object productData = object.get("data");
                    if (ObjectUtil.isNotNull(productData)) {
                        responseMap = (Map<String, Object>) JSONArray.parseObject(productData.toString(), Map.class);
                    }
                }
            } catch (Exception e) {
                logger.info("查询商品产品信息失败！");
            }
            if (responseMap == null) {
                return ResponseUtil.badArgumentValue("商品产品不存在！");
            }
            Object goodPrice = responseMap.get("price"); // 商品产品价格
            if (goodPrice == null) {
                return ResponseUtil.badArgumentValue("商品产品价格出错！");
            }
            if (new BigDecimal(String.valueOf(goodPrice)).compareTo(new BigDecimal(goods_price)) != 0) {
                return ResponseUtil.badArgumentValue("商品产品价格错误！");
            }


            faceValue = responseMap.get("faceValue"); // 实际充值金额
            if (faceValue == null) {
                return ResponseUtil.badArgumentValue("充值金额出错！");
            }

        }
        // todo 远程调用接口查询 商品产品信息,后面发版去除


        MallOrder order = new MallOrder();
        order.setUserId(userId);
        order.setOrderSn(GraphaiIdGenerator.nextId("OrderSN"));
        order.setOrderStatus(OrderUtil.STATUS_CREATE);
        order.setMobile(charge_account);
        order.setMessage("购买权益商品");
        // 面值
        order.setGoodsPrice(new BigDecimal(goods_price));
        order.setFreightPrice(BigDecimal.ZERO);
        order.setCouponPrice(BigDecimal.ZERO);
        order.setIntegralPrice(BigDecimal.ZERO);
        // 订单总费用
        BigDecimal actualPrice = new BigDecimal(goods_price).multiply(new BigDecimal(buy_num));
        order.setActualPrice(actualPrice);
        order.setOrderPrice(new BigDecimal(String.valueOf(faceValue)));
        order.setHuid(type);
        order.setClientIp(clientIp);
        order.setGrouponPrice(BigDecimal.ZERO); // 团购价格
        order.setBid(AccountStatus.BID_9);

        // 添加订单表
        orderService.add(order);

        for (int i = 1; i <= Integer.parseInt(buy_num); i++) {
            // 添加订单商品表项
            // 订单商品
            MallOrderGoods orderGoods = new MallOrderGoods();
            orderGoods.setOrderId(order.getId());
            orderGoods.setGoodsId(goods_id);
            orderGoods.setGoodsSn(goods_sn);
            orderGoods.setProductId(goods_product_id);
            orderGoods.setSrcGoodId(goods_sn);
            orderGoods.setGoodsName(goods_name);
            orderGoods.setPrice(new BigDecimal(goods_price));
            orderGoods.setNumber(Short.valueOf("1"));
            orderGoods.setAddTime(LocalDateTime.now());
            // 临时用于存储商品类型
            orderGoods.setComment(product_type);
            // 设置类目的图片，当作权益的商品
            // orderGoods.setChargeType(account_type);
            orderGoods.setPicUrl(categoryPic);
            orderGoodsService.add(orderGoods);
        }

        if (StringHelper.isNullOrEmptyString(order.getId())) {
            return ResponseUtil.fail();
        }

        // if (com.graphai.mall.db.util.StringHelper.isNotNullAndEmpty(factory_price)
        // && com.graphai.mall.db.util.StringHelper.isNotNullAndEmpty(goods_price)) {
        // //出场价
        // BigDecimal factoryPrice = new BigDecimal(factory_price);
        // //销售定价
        // BigDecimal price = new BigDecimal(goods_price);
        // //佣金
        // BigDecimal subtract = price.subtract(factoryPrice);
        // if (subtract.intValue() > 0) {
        // Map<String, String> map = new HashMap(6);
        // map.put("userId", userId);
        // map.put("bid", AccountStatus.BID_9);
        // map.put("totalMoney", order.getPubSharePreFee());
        // map.put("oderId", order.getId());
        // map.put("rechargeType", CommonConstant.TASK_REWARD_CASH);
        // map.put("orderSn", order.getOrderSn());
        // try {
        // Map<String, Object> rateMap = (Map<String, Object>) AccountUtils.preCharge(map);
        // if (ObjectUtil.isNotNull(rateMap) && rateMap.containsKey("commissionReward")) {
        // if (com.graphai.mall.db.util.StringHelper.isNotNullAndEmpty(rateMap.get("commissionReward"))) {
        // BigDecimal commissionfee = new BigDecimal(String.valueOf(rateMap.get("commissionReward")));
        // order.setPubSharePreFee(String.valueOf(commissionfee));
        // } else {
        // order.setPubSharePreFee("0.0");
        // }
        // } else {
        // order.setPubSharePreFee("0.0");
        // }
        // } catch (Exception e) {
        // logger.info("权益数据预存佣金出现异常！");
        // e.printStackTrace();
        // }
        // } else {
        // order.setPubSharePreFee("0.0");
        // }
        // }

        Map<String, Object> data = new HashMap<>();
        data.put("orderId", order.getId());
        return ResponseUtil.ok(data);
    }


    /**
     * 插入福禄、中闽在线、蓝色兄弟 订单 type fuluGoods 、 zmGoods 、 lsxdGoods 通用方法，进行下单。 todo 退款方法，现在暂时没做
     *
     * 整体逻辑： 1、获取下单的基本信息 2、进行校验 3、开放平台接口调用, 返回结果，日志记录 4、如果是福禄平台，获取卡密信息，进行订单商品卡密修改 5、进行 差价 分佣计算（商品销售价 -
     * 出厂价 = 差价） 6、充值结果回调，更新状态，日志记录 7、完成 定位问题，sql select * from mall_order where huid = 'fuluGoods';
     * select * from mall_order where huid = 'zmGoods'; select * from mall_order where huid =
     * 'lsxdGoods';
     *
     * select * from mall_order_notify_log where wx_params = 'fulu'; select * from mall_order_notify_log
     * where wx_params = 'zm'; select * from mall_order_notify_log where wx_params = 'lsxd';
     *
     * @return
     */
    @Transactional(rollbackFor = {Exception.class})
    private Object insertOrderByThreeV2(String userId, String body, String clientIp, String type, String mobile,
                                        String platFrom) {
        // 三方平台通用方
        String goods_name = JacksonUtil.parseString(body, "goods_name");
        String goods_sn = JacksonUtil.parseString(body, "goods_sn");
        String goods_id = JacksonUtil.parseString(body, "goods_id");
        String goods_product_id = JacksonUtil.parseString(body, "goods_product_id"); // 商品产品id
        String product_type = JacksonUtil.parseString(body, "product_type");
        // 销售价
        String goods_price = JacksonUtil.parseString(body, "goods_price");
        String sign = JacksonUtil.parseString(body, "sign");
        String factory_price = JacksonUtil.parseString(body, "factory_price");

        // 类目ID
        // String categoryId = JacksonUtil.parseString(body, "categoryId");
        String categoryPic = JacksonUtil.parseString(body, "categoryPic");

        // 手机号 \ qq号
        String charge_account = JacksonUtil.parseString(body, "charge_account");
        String buy_num = JacksonUtil.parseString(body, "buy_num");

        // 蓝色兄弟专用
        // 充值类型 （1手机 2 QQ）
        String account_type = JacksonUtil.parseString(body, "account_type");
        // String extend_parameter = JacksonUtil.parseString(body, "extend_parameter");


        if (StringHelper.isNullOrEmptyString(goods_id)) {
            return ResponseUtil.badArgument();
        }
        // if (StringHelper.isNullOrEmptyString(goods_product_id)) {
        // return ResponseUtil.badArgument();
        // }
        if (StringHelper.isNullOrEmptyString(goods_sn)) {
            return ResponseUtil.badArgument();
        }
        if (StringHelper.isNullOrEmptyString(goods_price)) {
            return ResponseUtil.badArgument();
        }

        if (!Objects.equals(product_type, "卡密") && StringHelper.isNullOrEmptyString(charge_account)) {
            return ResponseUtil.badArgument();
        }

        if (StringHelper.isNullOrEmptyString(buy_num)) {
            buy_num = "1";
        }

        Map<String, Object> paramMap = new HashMap<>();

        paramMap.put("mobile", mobile);
        paramMap.put("platform", platFrom);
        paramMap.put("goods_name", goods_name);

        paramMap.put("goods_sn", goods_sn);
        paramMap.put("goods_id", goods_id);
        paramMap.put("goods_product_id", goods_product_id);
        paramMap.put("product_type", product_type);
        paramMap.put("charge_account", charge_account);


        paramMap.put("goods_price", goods_price);
        paramMap.put("factory_price", factory_price);
        // paramMap.put("categoryId", categoryId);
        paramMap.put("categoryPic", categoryPic);

        paramMap.put("buy_num", buy_num);
        paramMap.put("account_type", account_type);

        String string = JSONObject.toJSONString(paramMap);// 请求参数转换成字符串进行加密处理
        boolean flag = Md5Utils.verifySignature(string, sign);
        if (!flag) {
            return ResponseUtil.fail(403, "无效签名");
        }



        /** 验证商品产品价格是否一致 **/
        // todo 远程调用 'getGoodsProductById'接口查询 商品产品信息, 发版后面去除
        if (StringUtils.isNotBlank(goods_product_id)) {
            Map<String, Object> responseMap = null;
            try {
                String url = platFormServerproperty.getUrl(MallConstant.TAOBAO);
                Map<String, Object> map = new HashMap<>();
                map.put("plateform", "dtk");
                map.put("id", goods_product_id);
                Header[] headers = new BasicHeader[] {new BasicHeader("method", "getGoodsProductById"),
                        new BasicHeader("version", "v1.0")};
                String result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(map), headers);
                JSONObject object = JSONObject.parseObject(result);
                if ("0".equals(object.getString("errno"))) {
                    Object productData = object.get("data");
                    if (ObjectUtil.isNotNull(productData)) {
                        responseMap = (Map<String, Object>) JSONArray.parseObject(productData.toString(), Map.class);
                    }
                }
            } catch (Exception e) {
                logger.info("查询商品产品信息失败！");
            }
            if (responseMap == null) {
                return ResponseUtil.badArgumentValue("商品产品不存在！");
            }
            Object goodPrice = responseMap.get("price"); // 商品产品价格
            if (goodPrice == null) {
                return ResponseUtil.badArgumentValue("商品产品价格出错！");
            }
            if (new BigDecimal(String.valueOf(goodPrice)).compareTo(new BigDecimal(goods_price)) != 0) {
                return ResponseUtil.badArgumentValue("商品产品价格错误！");
            }
        }
        // todo 远程调用接口查询 商品产品信息,后面发版去除


        MallOrder order = new MallOrder();
        order.setUserId(userId);
        order.setOrderSn(GraphaiIdGenerator.nextId("OrderSN"));
        order.setOrderStatus(OrderUtil.STATUS_CREATE);
        order.setMobile(charge_account);
        order.setMessage("购买权益商品");
        // 面值
        order.setGoodsPrice(new BigDecimal(goods_price));
        order.setFreightPrice(BigDecimal.ZERO);
        order.setCouponPrice(BigDecimal.ZERO);
        order.setIntegralPrice(BigDecimal.ZERO);
        // 订单总费用
        BigDecimal actualPrice = new BigDecimal(goods_price).multiply(new BigDecimal(buy_num));
        order.setActualPrice(actualPrice);
        order.setOrderPrice(actualPrice);
        order.setHuid(type);
        order.setClientIp(clientIp);
        order.setGrouponPrice(BigDecimal.ZERO); // 团购价格
        order.setBid(AccountStatus.BID_9);

        // 添加订单表
        orderService.add(order);

        for (int i = 1; i <= Integer.parseInt(buy_num); i++) {
            // 添加订单商品表项
            // 订单商品
            MallOrderGoods orderGoods = new MallOrderGoods();
            orderGoods.setOrderId(order.getId());
            orderGoods.setGoodsId(goods_id);
            orderGoods.setGoodsSn(goods_sn);
            orderGoods.setProductId(goods_product_id);
            orderGoods.setSrcGoodId(goods_sn);
            orderGoods.setGoodsName(goods_name);
            orderGoods.setPrice(new BigDecimal(goods_price));
            orderGoods.setNumber(Short.valueOf("1"));
            orderGoods.setAddTime(LocalDateTime.now());
            // 临时用于存储商品类型
            orderGoods.setComment(product_type);
            // 设置类目的图片，当作权益的商品
            orderGoods.setChargeType(account_type);
            orderGoods.setPicUrl(categoryPic);
            orderGoodsService.add(orderGoods);
        }

        if (StringHelper.isNullOrEmptyString(order.getId())) {
            return ResponseUtil.fail();
        }

        if (com.graphai.mall.db.util.StringHelper.isNotNullAndEmpty(factory_price)
                && com.graphai.mall.db.util.StringHelper.isNotNullAndEmpty(goods_price)) {
            // 出场价
            BigDecimal factoryPrice = new BigDecimal(factory_price);
            // 销售定价
            BigDecimal price = new BigDecimal(goods_price);
            // 佣金
            BigDecimal subtract = price.subtract(factoryPrice);
            if (subtract.intValue() > 0) {
                Map<String, String> map = new HashMap(6);
                map.put("userId", userId);
                map.put("bid", AccountStatus.BID_9);
                map.put("totalMoney", order.getPubSharePreFee());
                map.put("oderId", order.getId());
                map.put("rechargeType", CommonConstant.TASK_REWARD_CASH);
                map.put("orderSn", order.getOrderSn());
                try {
                    Map<String, Object> rateMap = (Map<String, Object>) AccountUtils.preCharge(map);
                    if (ObjectUtil.isNotNull(rateMap) && rateMap.containsKey("commissionReward")) {
                        if (com.graphai.mall.db.util.StringHelper.isNotNullAndEmpty(rateMap.get("commissionReward"))) {
                            BigDecimal commissionfee = new BigDecimal(String.valueOf(rateMap.get("commissionReward")));
                            order.setPubSharePreFee(String.valueOf(commissionfee));
                        } else {
                            order.setPubSharePreFee("0.0");
                        }
                    } else {
                        order.setPubSharePreFee("0.0");
                    }
                } catch (Exception e) {
                    logger.info("权益数据预存佣金出现异常！");
                    e.printStackTrace();
                }
            } else {
                order.setPubSharePreFee("0.0");
            }
        }

        Map<String, Object> data = new HashMap<>();
        data.put("orderId", order.getId());
        return ResponseUtil.ok(data);
    }

    /**
     * 购买优惠券商品
     *
     * @param body
     *        {"platform":5,"goods_name":xx,"goods_sn":xx,"goods_id":xx,"goods_price":xx,"factory_price":xx,"categoryId":xx,"categoryPic":xx,"buy_num":xx,"coupon_id":xxx}
     *
     */
    private Object insertOrderByOther(String userId, String body, String clientIp, String type) {
        MallUser user = userService.findById(userId);

        // 三方平台通用方
        String goods_name = JacksonUtil.parseString(body, "goods_name"); // 商品名
        String goods_sn = JacksonUtil.parseString(body, "goods_sn"); // 商品sn
        String goods_id = JacksonUtil.parseString(body, "goods_id"); // 商品id
        // 销售价
        String goods_price = JacksonUtil.parseString(body, "goods_price"); // 商品价格
        String factory_price = JacksonUtil.parseString(body, "factory_price"); // 原价

        String categoryId = JacksonUtil.parseString(body, "categoryId"); // 类目ID
        String categoryPic = JacksonUtil.parseString(body, "categoryPic"); // 类目图片

        String buy_num = JacksonUtil.parseString(body, "buy_num"); // 购买数量
        String coupon_id = JacksonUtil.parseString(body, "coupon_id"); // 卡券id

        if (StringHelper.isNullOrEmptyString(goods_id)) {
            return ResponseUtil.badArgument();
        }
        // if (StringHelper.isNullOrEmptyString(goods_sn)) {
        // return ResponseUtil.badArgument();
        // }
        if (StringHelper.isNullOrEmptyString(goods_price)) {
            return ResponseUtil.badArgument();
        }
        if (StringHelper.isNullOrEmptyString(coupon_id)) {
            return ResponseUtil.badArgument();
        }

        if (StringHelper.isNullOrEmptyString(buy_num)) {
            buy_num = "1";
        }


        /** 通过id 查询优惠券商品产品信息 ——验证商品产品价格是否正确 **/
        // 远程调用 'getCouponGoodsDetail'接口查询 优惠券商品产品信息
        JSONObject responseMap = null;
        try {
            responseMap = goodsService.queryCouponGoodsById(goods_id);
        } catch (Exception e) {
            logger.info("查询优惠券商品失败！");
        }
        if (responseMap == null) {
            return ResponseUtil.badArgumentValue("优惠券商品不存在！");
        }
        String goodPrice = responseMap.getString("retail_price"); // 商品产品价格
        if (goodPrice == null) {
            return ResponseUtil.badArgumentValue("优惠券商品价格出错！");
        }
        if (new BigDecimal(goodPrice).compareTo(new BigDecimal(goods_price)) != 0) {
            return ResponseUtil.badArgumentValue("优惠券商品价格错误！");
        }

        /** 通过coupon_id 查询 优惠券信息 ——判断优惠券是否过期、 商品库存 **/
        // String surplus = null;
        // 远程调用 'getCouponGoodsDetail'接口查询 优惠券商品产品信息
        JSONObject couponMap = null;
        try {
            couponMap = goodsService.queryCouponById(coupon_id);
        } catch (Exception e) {
            logger.info("查询优惠券失败！");
        }
        if (couponMap == null) {
            return ResponseUtil.badArgumentValue("优惠券不存在！");
        }
        // surplus = couponMap.getString("surplus");
        // String endTime = couponMap.getString("endTime"); //优惠券过期时间
        // if(StringUtils.isNotEmpty(endTime)){
        // //判断优惠券是否过期
        // DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        // LocalDateTime parseEndTime = LocalDateTime.parse(endTime, formatter);
        // LocalDateTime now = LocalDateTime.now();
        // if(now.isAfter(parseEndTime)){
        // return ResponseUtil.badArgumentValue("下单失败,优惠券已经过期！");
        // }
        // }


        // if(StringUtils.isBlank(surplus)){
        // return ResponseUtil.badArgumentValue("库存出错！");
        // }
        // if(new BigDecimal(buy_num).compareTo(new BigDecimal(surplus)) == 1){
        // return ResponseUtil.badArgumentValue("优惠券库存不足！");
        // }



        /** 通过coupon_id 查询 券码库存是否充足 __查询优惠券商品券码库存 **/
        /** mall_goods_coupon_code 券码：未售出、未删除 **/
        MallGoodsCouponCode mallGoodsCouponCode = new MallGoodsCouponCode();
        mallGoodsCouponCode.setCouponId(coupon_id);
        mallGoodsCouponCode.setSaleStatus(false);

        List<MallGoodsCouponCode> mallGoodsCouponCodeList = mallGoodsCouponCodeService
                .selectMallGoodsCouponCodeList(mallGoodsCouponCode, null, null, null, null);
        if (mallGoodsCouponCodeList.size() == 0) {
            return ResponseUtil.badArgumentValue("库存不足！");
        }


        MallOrder order = new MallOrder();
        order.setUserId(userId);
        order.setOrderSn(GraphaiIdGenerator.nextId("MallOrder"));
        order.setOrderStatus(OrderUtil.STATUS_CREATE);
        order.setMobile(user.getMobile());
        order.setMessage("用户购买优惠券商品");
        // 面值
        order.setGoodsPrice(new BigDecimal(goods_price));
        order.setFreightPrice(BigDecimal.ZERO);
        order.setCouponPrice(BigDecimal.ZERO);
        order.setIntegralPrice(BigDecimal.ZERO);
        // 订单总费用
        BigDecimal actualPrice = new BigDecimal(goods_price).multiply(new BigDecimal(buy_num));
        order.setActualPrice(actualPrice);
        order.setOrderPrice(actualPrice);
        order.setHuid("zy");
        order.setClientIp(clientIp);
        order.setGrouponPrice(BigDecimal.ZERO); // 团购价格
        order.setBid(AccountStatus.BID_27);

        // 添加订单表
        orderService.add(order);

        // 添加订单商品表数据
        MallOrderGoods orderGoods = new MallOrderGoods();
        orderGoods.setOrderId(order.getId());
        orderGoods.setGoodsId(goods_id);
        orderGoods.setGoodsSn(goods_sn);
        orderGoods.setSrcGoodId(coupon_id);
        orderGoods.setGoodsName(goods_name);
        orderGoods.setPrice(actualPrice);
        orderGoods.setNumber(Short.valueOf(buy_num));
        orderGoods.setAddTime(LocalDateTime.now());
        // 临时用于存储商品类型
        orderGoods.setComment("优惠券商品");
        // 设置类目的图片，当作权益的商品
        orderGoods.setPicUrl(categoryPic);
        // orderGoods.setProductId(coupon_id); //商品产品id为 优惠券id
        orderGoods.setUserId(userId);
        orderGoodsService.add(orderGoods);

        if (StringHelper.isNullOrEmptyString(order.getId())) {
            return ResponseUtil.fail();
        }

        Map<String, Object> data = new HashMap<>();
        data.put("orderId", order.getId());
        return ResponseUtil.ok(data);
    }


    /**
     * 购买优惠券商品
     *
     * @param body
     *        {"platform":5,"goods_name":xx,"goods_sn":xx,"goods_id":xx,"goods_price":xx,"factory_price":xx,"categoryId":xx,"categoryPic":xx,"buy_num":xx,"coupon_id":xxx}
     *
     */
    private Object insertOrderByOtherV2(String userId, String body, String clientIp, String type, String mobile,
                                        String platFrom) {
        MallUser user = userService.findById(userId);

        // 三方平台通用方
        String sign = JacksonUtil.parseString(body, "sign");
        String goods_name = JacksonUtil.parseString(body, "goods_name"); // 商品名
        String goods_sn = JacksonUtil.parseString(body, "goods_sn"); // 商品sn
        String goods_id = JacksonUtil.parseString(body, "goods_id"); // 商品id
        // 销售价
        String goods_price = JacksonUtil.parseString(body, "goods_price"); // 商品价格
        String factory_price = JacksonUtil.parseString(body, "factory_price"); // 原价

        // String categoryId = JacksonUtil.parseString(body, "categoryId"); //类目ID
        String categoryPic = JacksonUtil.parseString(body, "categoryPic"); // 类目图片

        String buy_num = JacksonUtil.parseString(body, "buy_num"); // 购买数量
        String coupon_id = JacksonUtil.parseString(body, "coupon_id"); // 卡券id

        if (StringHelper.isNullOrEmptyString(goods_id)) {
            return ResponseUtil.badArgument();
        }
        // if (StringHelper.isNullOrEmptyString(goods_sn)) {
        // return ResponseUtil.badArgument();
        // }
        if (StringHelper.isNullOrEmptyString(goods_price)) {
            return ResponseUtil.badArgument();
        }
        if (StringHelper.isNullOrEmptyString(coupon_id)) {
            return ResponseUtil.badArgument();
        }

        if (StringHelper.isNullOrEmptyString(buy_num)) {
            buy_num = "1";
        }


        Map<String, Object> paramMap = new HashMap<>();

        paramMap.put("mobile", mobile);
        paramMap.put("platform", platFrom);
        paramMap.put("goods_name", goods_name);


        paramMap.put("goods_id", goods_id);


        paramMap.put("goods_price", goods_price);
        paramMap.put("factory_price", factory_price);
        // paramMap.put("categoryId", categoryId);
        paramMap.put("categoryPic", categoryPic);

        paramMap.put("buy_num", buy_num);
        paramMap.put("coupon_id", coupon_id);

        String string = JSONObject.toJSONString(paramMap);// 请求参数转换成字符串进行加密处理
        boolean flag = Md5Utils.verifySignature(string, sign);
        if (!flag) {
            return ResponseUtil.fail(403, "无效签名");
        }


        /** 通过id 查询优惠券商品产品信息 ——验证商品产品价格是否正确 **/
        // 远程调用 'getCouponGoodsDetail'接口查询 优惠券商品产品信息
        JSONObject responseMap = null;
        try {
            responseMap = goodsService.queryCouponGoodsById(goods_id);
        } catch (Exception e) {
            logger.info("查询优惠券商品失败！");
        }
        if (responseMap == null) {
            return ResponseUtil.badArgumentValue("优惠券商品不存在！");
        }
        String goodPrice = responseMap.getString("retail_price"); // 商品产品价格
        if (goodPrice == null) {
            return ResponseUtil.badArgumentValue("优惠券商品价格出错！！");
        }
        if (new BigDecimal(goodPrice).compareTo(new BigDecimal(goods_price)) != 0) {
            return ResponseUtil.badArgumentValue("优惠券商品价格错误！");
        }


        // 判断商品库存是否充足
        /** 通过coupon_id 查询 优惠券信息 ——判断优惠券是否过期、 商品库存 **/
        // String surplus = null;
        // //远程调用 'getCouponGoodsDetail'接口查询 优惠券商品产品信息
        // JSONObject couponMap = null;
        // try {
        // couponMap = goodsService.queryCouponById(coupon_id);
        // } catch (Exception e) {
        // logger.info("查询优惠券失败！");
        // }
        // if(couponMap == null){
        // return ResponseUtil.badArgumentValue("优惠券不存在！");
        // }
        // surplus = couponMap.getString("surplus");
        // if(StringUtils.isBlank(surplus)){
        // return ResponseUtil.badArgumentValue("库存出错！");
        // }
        // if(new BigDecimal(buy_num).compareTo(new BigDecimal(surplus)) == 1){
        // return ResponseUtil.badArgumentValue("优惠券库存不足！");
        // }


        /** 通过coupon_id 查询 券码库存是否充足(test库《mall_goods_coupon_code》数据) **/
        MallGoodsCouponCode mallGoodsCouponCode = new MallGoodsCouponCode();
        mallGoodsCouponCode.setCouponId(coupon_id);
        mallGoodsCouponCode.setSaleStatus(false);
        mallGoodsCouponCode.setDeleted(false);
        List<MallGoodsCouponCode> mallGoodsCouponCodeList = mallGoodsCouponCodeService
                .selectMallGoodsCouponCodeList(mallGoodsCouponCode, null, null, null, null);
        if (mallGoodsCouponCodeList.size() == 0) {
            return ResponseUtil.badArgumentValue("库存不足！");
        }


        MallOrder order = new MallOrder();
        order.setUserId(userId);
        order.setOrderSn(GraphaiIdGenerator.nextId("MallOrder"));
        order.setOrderStatus(OrderUtil.STATUS_CREATE);
        order.setMobile(user.getMobile());
        order.setMessage("用户购买优惠券商品");
        // 面值
        order.setGoodsPrice(new BigDecimal(goods_price));
        order.setFreightPrice(BigDecimal.ZERO);
        order.setCouponPrice(BigDecimal.ZERO);
        order.setIntegralPrice(BigDecimal.ZERO);
        // 订单总费用
        BigDecimal actualPrice = new BigDecimal(goods_price).multiply(new BigDecimal(buy_num));
        order.setActualPrice(actualPrice);
        order.setOrderPrice(actualPrice);
        order.setHuid("zy");
        order.setClientIp(clientIp);
        order.setGrouponPrice(BigDecimal.ZERO); // 团购价格
        order.setBid(AccountStatus.BID_27);

        // 添加订单表
        orderService.add(order);

        // 添加订单商品表数据
        MallOrderGoods orderGoods = new MallOrderGoods();
        orderGoods.setOrderId(order.getId());
        orderGoods.setGoodsId(goods_id);
        orderGoods.setGoodsSn(goods_sn);
        orderGoods.setSrcGoodId(coupon_id);
        orderGoods.setGoodsName(goods_name);
        orderGoods.setPrice(actualPrice);
        orderGoods.setNumber(Short.valueOf(buy_num));
        orderGoods.setAddTime(LocalDateTime.now());
        // 临时用于存储商品类型
        orderGoods.setComment("优惠券商品");
        // 设置类目的图片，当作权益的商品
        orderGoods.setPicUrl(categoryPic);
        // orderGoods.setProductId(coupon_id); //商品产品id为 优惠券id
        orderGoodsService.add(orderGoods);

        if (StringHelper.isNullOrEmptyString(order.getId())) {
            return ResponseUtil.fail();
        }

        Map<String, Object> data = new HashMap<>();
        data.put("orderId", order.getId());
        return ResponseUtil.ok(data);
    }

    /**
     * 购买自营商品
     *
     */
    private Object buyZyGoods(String userId, String body, String clientIp, String deptId) {
        try {
            logger.info("购买自营商品 参数body:" + body);
            String cartId = JacksonUtil.parseString(body, "cartId");
            String message = JacksonUtil.parseString(body, "message");
            // 团购专用
            String grouponRulesId = JacksonUtil.parseString(body, "grouponRulesId");
            String grouponLinkId = JacksonUtil.parseString(body, "grouponLinkId");
            String groupProductId = JacksonUtil.parseString(body, "groupProductId");
            String shareId = JacksonUtil.parseString(body, "shareId"); // 分享人id
            Integer buyNum = JacksonUtil.parseInteger(body, "buyNum");
            Integer paymdoe = JacksonUtil.parseInteger(body, "paymdoe");
            if (ObjectUtils.isEmpty(paymdoe)) {
                paymdoe = GroupEnum.PAYMDOE_1.getIntCode();
            }
            // 下单端口H5端默认写Customer，小程序目前是空，后续可以直接写MiniBusiness
            String submitClient = JacksonUtil.parseString(body, "submitClient");
            // 如果接口为空的话
            submitClient = StringUtils.isNotBlank(submitClient) ? submitClient : "MiniBusiness";
            String sign = JacksonUtil.parseString(body, "sign");
            String name = JacksonUtil.parseString(body, "name"); // 收货人姓名
            String tel = JacksonUtil.parseString(body, "tel"); // 收货人电话
            String province = JacksonUtil.parseString(body, "province"); // 省
            String city = JacksonUtil.parseString(body, "city"); // 市
            String county = JacksonUtil.parseString(body, "county"); // 区
            String addressDetail = JacksonUtil.parseString(body, "addressDetail"); // 详细地址
            BigDecimal price = new BigDecimal(JacksonUtil.parseString(body, "price")); // 金额

            if (Objects.equals(null, price)) {
                return ResponseUtil.badArgumentValue("金额不能为空!");
            }

            if (StrUtil.isEmpty(grouponLinkId)) {
                List<GroupVo> groupon = grouponService.queryListByPage(grouponRulesId, 0, 1);
                if (ArrayUtil.isNotEmpty(groupon) && groupon.size() > 0) {
                    grouponLinkId = String.valueOf(groupon.get(0).getGroupId());
                }
            }

            // 如果是团购项目,验证活动是否有效
            MallGrouponRules rules = null;
            Integer virtualGood = null;
            MallUserProfitNew userProfitNew = null;
            if (grouponRulesId != null && ! "0".equals(grouponRulesId)) {
                rules = grouponRulesService.queryByIdV2(grouponRulesId);
                // 找不到记录
                if (rules == null) {
                    return ResponseUtil.badArgument();
                }
                // 团购活动已经过期
                if (grouponRulesService.isExpired(rules)) {
                    return ResponseUtil.fail(GROUPON_EXPIRED, "团购活动已过期!");
                }
            }

            // 查询团是否满人
            if (grouponLinkId != null) {
                // 查看参团成员
                List<MallGroupVo> groupList = grouponService.queryGroupByIdV2(grouponLinkId, null, null);
                if (rules.getDiscountMember() <= groupList.size()) {
                    return ResponseUtil.fail(503, "团已满人");
                }
                if (grouponService.checkUserGroup(grouponLinkId, userId) > 0) {
                    return ResponseUtil.fail(743, "您已存在参与该团的订单，不支持重复参团!");
                }
            }

            BigDecimal checkedGoodsPrice = BigDecimal.ZERO;
            BigDecimal orderGoodsPrice = BigDecimal.ZERO;
            // 使用优惠券减免的金额
            BigDecimal couponPrice = BigDecimal.ZERO;
            // 根据订单商品总价计算运费，多个商品满88则免运费，否则8元；如果是单个商品则按商品的邮费计算
            BigDecimal freightPrice = BigDecimal.ZERO;
            // 总成本价
            BigDecimal costPrice = BigDecimal.ZERO;

            // 团购优惠
            BigDecimal grouponDiscountPrice = BigDecimal.ZERO;

            BigDecimal returnRatio = BigDecimal.ZERO;

            // 团购价格
            BigDecimal grouponPrice = BigDecimal.ZERO;
            if (! Objects.equals(grouponRulesId, "0")) {
                if (rules != null) {
                    grouponDiscountPrice = rules.getDiscount();
                    if (! Objects.equals(null, groupProductId)) {
//                        MallGoodsProduct product = productService.findById(groupProductId);
                        MallActivityGoodsProduct product = mallActivityGoodsProductService.getOneByProductId(MallActivityEnum.ACTIVITY_GOODS_2.strCode2Int(), groupProductId);
                        if (null != product) {
                            grouponPrice = product.getActivityPrice();
                        } else {
                            return ResponseUtil.fail("支付失败! 该产品没有参与团购");
                        }
                    }

                }
            }
            String merchantId = null;
            List<MallCart> checkedGoodsList = null;
            // 团购商品不在购物车下单
            if (com.graphai.mall.db.util.StringHelper.isNullOrEmpty(grouponRulesId)) {

                if (cartId.equals(0) || "0".equals(cartId)) {
                    logger.info("进入userid 查询购物车产品 ：" + userId);
                    checkedGoodsList = cartService.queryByUidAndChecked(userId);
                } else {
                    logger.info("进入cartId 查询购物车产品 ：" + cartId);
                    MallCart cart = cartService.findById(cartId);
                    checkedGoodsList = new ArrayList<>(1);
                    String deptIdTemp = cart.getDeptId();
                    if (deptIdTemp != null && ! "".equals(deptIdTemp)) {
                        deptId = deptIdTemp; // 设置归属部门编号
                    }
                    checkedGoodsList.add(cart);
                }
                if (checkedGoodsList.size() == 0) {
                    return ResponseUtil.badArgumentValue("选择商品数据不正确");
                }

                List<MallGoodsProduct> productList = null; // 产品列表
                List<String> productIds =
                        checkedGoodsList.stream().map(MallCart::getProductId).collect(Collectors.toList());
                if (! productIds.isEmpty()) {
                    productList = productService.findByIds(productIds);
                }
                if (CollectionUtils.isNotEmpty(checkedGoodsList)) {
                    List<String> gids = checkedGoodsList.stream().map(MallCart::getGoodsId).collect(Collectors.toList());
                    // 求出购物车中的商品的全部运费总和
                    freightPrice = goodsService.queryTotalPostage(gids);
                    for (MallCart checkGoods : checkedGoodsList) {
                        // 商品初始价格
                        BigDecimal goodsPrice = checkGoods.getPrice();
                        BigDecimal finalPrice = goodsPrice;
                        // 订单成交计算金额
                        checkedGoodsPrice = checkedGoodsPrice.add(finalPrice.multiply(new BigDecimal(checkGoods.getNumber())));
                        if (CollectionUtils.isNotEmpty(productList)) {
                            for (MallGoodsProduct product : productList) {
                                if (Objects.equals(product.getId(), checkGoods.getProductId())) {
                                    if (product.getFactoryPrice() != null) {
                                        // 成本价
                                        costPrice = costPrice.add(product.getFactoryPrice().multiply(new BigDecimal(checkGoods.getNumber())));
                                    }
                                }
                            }
                        }
                    }
                } else if (CollectionUtils.isNotEmpty(checkedGoodsList) && checkedGoodsList.get(0).getChecked() == true) {
                    String productId = checkedGoodsList.get(0).getProductId();
                    JSONObject obj;
                    try {
                        obj = goodsService.queryGoodsByProductId(productId);
                        if (Objects.equals(null, obj)) {
                            return ResponseUtil.fail(507, "商品信息有误!");
                        }
                        freightPrice = new BigDecimal(obj.getString("postage"));
                        deptId = obj.getString("deptId");
                        logger.info(groupProductId + "归属部门ID=" + deptId);
                        checkedGoodsPrice = checkedGoodsList.get(0).getPrice()
                                .multiply(new BigDecimal(checkedGoodsList.get(0).getNumber()));
                        if (CollectionUtils.isNotEmpty(productList)) {
                            for (MallGoodsProduct product : productList) {
                                if (Objects.equals(product.getId(), checkedGoodsList.get(0).getProductId())) {
                                    if (product.getFactoryPrice() != null) {
                                        // 成本价
                                        costPrice = costPrice.add(product.getFactoryPrice()
                                                .multiply(new BigDecimal(checkedGoodsList.get(0).getNumber())));
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else {
                JSONObject obj;
                try {
//                    obj = goodsService.queryGoodsByProductId(groupProductId);
                    obj = mallActivityGoodsProductService.queryGoodsByProductId(groupProductId);
                    merchantId = obj.getString("merchantId");
                    if (Objects.equals(null, obj)) {
                        return ResponseUtil.fail(507, "商品信息有误!");
                    }
                    virtualGood = obj.getInteger("virtualGood");
                    freightPrice = new BigDecimal(obj.getString("postage"));
                    deptId = obj.getString("deptId");
                    Integer stock = obj.getInteger("stock"); // 限购量
                    Integer orderStock = orderGoodsService.selectOrderGoodsTotal(userId, obj.getString("gid"));
                    if (! Objects.equals(null, stock) && orderStock != null) {
                        if (orderStock >= stock) {
                            ResponseUtil.badArgumentValue("此商品已被限购!");

                        }
                    }
                    logger.info("单价" + obj.getString("activity_price"));
                    logger.info("数量" + buyNum);
                    logger.info(groupProductId + "归属部门ID=" + deptId);
                    logger.info("购买单价" + obj.getString("activity_price"));
                    logger.info("购买数量" + buyNum);

                    if (null != rules.getGroupPrice()) {
                        orderGoodsPrice = grouponPrice.multiply(new BigDecimal(buyNum));
                        //查询分润比例
                        QueryWrapper<MallUserProfitNew> upwWrapper = new QueryWrapper<>();
                        upwWrapper.eq("goods_id", obj.getString("gid"));
                        userProfitNew = userProfitNewService.getOne(upwWrapper);
                        if (! Objects.equals(null, userProfitNew)) {
                            checkedGoodsPrice = grouponPrice.multiply(new BigDecimal(buyNum)).multiply(userProfitNew.getOrdinaryUsersProfit());
//                            }
                        } else {
                            checkedGoodsPrice = grouponPrice.multiply(new BigDecimal(buyNum));
                        }
                    } else {
                        if(grouponDiscountPrice == null){
                            grouponDiscountPrice = BigDecimal.ZERO;
                        }
                        orderGoodsPrice = (new BigDecimal(obj.getString("activity_price")).subtract(grouponDiscountPrice))
                                .multiply(new BigDecimal(buyNum));
                        checkedGoodsPrice = (new BigDecimal(obj.getString("activity_price")).subtract(grouponDiscountPrice))
                                .multiply(new BigDecimal(buyNum));
                    }

                    if (obj.getString("factory_price") != null) {
                        costPrice = new BigDecimal(obj.getString("factory_price"));
                    }
                    if (! Objects.equals(null, obj.getBigDecimal("face_value"))) {
                        if (! Objects.equals(null, userProfitNew)) {
                            MallGrouponRules mallGrouponRules = mallGrouponRulesService.queryById(grouponRulesId);
                          /*  if (! Objects.equals(null, mallGrouponRules)) {
                                returnRatio = obj.getBigDecimal("face_value") == null ? BigDecimal.ZERO : obj.getBigDecimal("face_value")
                                        .multiply(userProfitNew.getOrdinaryUsersProfit())
                                        .multiply(new BigDecimal(mallGrouponRules.getSuccessNum()))
                                        .multiply(userProfitNew.getTwoMerProfit())
                                        .divide((new BigDecimal(mallGrouponRules.getDiscountMember() - mallGrouponRules.getSuccessNum())), 2, BigDecimal.ROUND_HALF_UP);
                            }*/
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            // 可以使用的其他钱，例如用户积分
            BigDecimal integralPrice = BigDecimal.ZERO;
            BigDecimal checkedGoodsPriceForPayMode1 = BigDecimal.ZERO;
            BigDecimal orderTotalPrice = BigDecimal.ZERO;
            if (GroupEnum.PAYMDOE_2.getIntCode() == paymdoe.intValue()) {
                // 订单费用
                orderTotalPrice = checkedGoodsPrice.add(freightPrice == null ? BigDecimal.ZERO : freightPrice).subtract(couponPrice).setScale(2, BigDecimal.ROUND_HALF_UP);
            } else if (GroupEnum.PAYMDOE_1.getIntCode() == paymdoe.intValue() && Objects.equals(virtualGood, 7)) {
                checkedGoodsPriceForPayMode1 = grouponPrice.multiply(new BigDecimal(buyNum)).add(freightPrice == null ? BigDecimal.ZERO : freightPrice).subtract(couponPrice).setScale(2, BigDecimal.ROUND_HALF_UP);
                orderTotalPrice = checkedGoodsPriceForPayMode1;
            } else {
                orderTotalPrice = checkedGoodsPrice.add(freightPrice == null ? BigDecimal.ZERO : freightPrice).subtract(couponPrice).setScale(2, BigDecimal.ROUND_HALF_UP);
            }
            //尾款订单费用
            BigDecimal lastPrice = orderGoodsPrice.add(freightPrice == null ? BigDecimal.ZERO : freightPrice).subtract(couponPrice).subtract(orderTotalPrice).setScale(2, BigDecimal.ROUND_HALF_UP);
            // 最终支付费用
            BigDecimal actualPrice = orderTotalPrice.subtract(integralPrice).setScale(2, BigDecimal.ROUND_HALF_UP);
            BigDecimal finalActualPrice = actualPrice;

            BigDecimal commissionPrice = BigDecimal.ZERO;
            if (rules != null) {
                // 团购分佣总金额
                commissionPrice = checkedGoodsPrice.setScale(2, BigDecimal.ROUND_HALF_UP);
            } else {
                // 分佣总金额
                commissionPrice = checkedGoodsPrice.subtract(costPrice).setScale(2, BigDecimal.ROUND_HALF_UP);
            }

            if (price.compareTo(finalActualPrice) != 0) {
                logger.info("金额不对!");
            }

            String orderId = null;
            String lastOrderId = null;
            MallOrder order = null;
            MallOrder order2 = null;
            String parentOrderOn = GraphaiIdGenerator.nextId("parentOrderOn");
            // 订单
            order = new MallOrder();
            order.setParentOrderOn(parentOrderOn);
            order.setUserId(userId);
            order.setOrderSn(GraphaiIdGenerator.nextId("OrderSN"));
            order.setOrderStatus(OrderUtil.STATUS_CREATE);
            order.setConsignee(name);
            order.setMobile(tel);
            order.setMessage(message);
            order.setAddress(addressDetail);
            order.setGoodsPrice(orderGoodsPrice);
            order.setFreightPrice(freightPrice);
            order.setCouponPrice(couponPrice);
            order.setIntegralPrice(integralPrice);
            order.setOrderPrice(orderTotalPrice);
            order.setActualPrice(finalActualPrice); // 订单实付金额
            if (userProfitNew != null && Objects.equals(virtualGood, 7)) {
                order.setPubSharePreFee(String.valueOf(userProfitNew.getOrdinaryUsersProfit())); // 分佣总比例
            } else {
                order.setPubSharePreFee(String.valueOf(commissionPrice)); // 分佣总金额
            }
            order.setTotalCommissionFee(String.valueOf(BigDecimal.ZERO));  //团购2.0返还红包金额
            order.setSource(submitClient);
            order.setProvinceName(province);
            order.setCityName(city);
            order.setAreaName(county);
            order.setAddressDetail(addressDetail);
            order.setClientIp(clientIp);
            if (StrUtil.isNotEmpty(grouponRulesId) && ! grouponRulesId.equalsIgnoreCase(GroupEnum.GROUP_COMMANDER.getCode())) {
                order.setBid(AccountStatus.BID_36);
            } else if (Objects.equals(grouponRulesId, null)) {
                order.setBid(AccountStatus.BID_27);
            }
            if (com.graphai.mall.db.util.StringHelper.isNotNullAndEmpty(shareId)) {
                order.setShareId(shareId);
            }
            if (Objects.equals(virtualGood, 7)) { // || order.getBid().equals("36")
                order.setHuid("groupgoods");    //团购2.0
            } else {
                order.setHuid("zygoods"); //自营和团购1.0
            }
            order.setMerchantId(merchantId);
            order.setGrouponPrice(grouponPrice); // 团购价
            order.setDeptId(deptId);// 订单加归属部门id
            order.setOrderPayType(1);
            // 添加订单表
            orderService.add(order);
            orderId = order.getId();
            if (2 == paymdoe) {
                // 订单2
                order2 = new MallOrder();
                order2.setUserId(userId);
                order2.setParentOrderOn(parentOrderOn);
                order2.setOrderSn(GraphaiIdGenerator.nextId("OrderSN"));
                order2.setOrderStatus(OrderUtil.STATUS_CREATE);
                order2.setConsignee(name);
                order2.setMobile(tel);
                order2.setMessage(message);
                order2.setAddress(addressDetail);
                order2.setGoodsPrice(orderGoodsPrice);
                order2.setFreightPrice(freightPrice);
                order2.setCouponPrice(couponPrice);
                order2.setIntegralPrice(integralPrice);
                order2.setOrderPrice(lastPrice);
                order2.setActualPrice(lastPrice); // 订单实付金额
                if (userProfitNew != null && Objects.equals(virtualGood, 7)) {
                    order2.setPubSharePreFee(String.valueOf(userProfitNew.getOrdinaryUsersProfit())); // 分佣总比例
                } else {
                    order2.setPubSharePreFee(String.valueOf(commissionPrice)); // 分佣总金额
                }
                order.setTotalCommissionFee(String.valueOf(BigDecimal.ZERO));  //团购2.0返还红包金额
                order2.setSource(submitClient);
                order2.setProvinceName(province);
                order2.setCityName(city);
                order2.setAreaName(county);
                order2.setAddressDetail(addressDetail);
                order2.setClientIp(clientIp);
                if (! Objects.equals(grouponRulesId, null) && ! Objects.equals(grouponRulesId, "0")) {
                    order2.setBid(AccountStatus.BID_36);
                } else if (Objects.equals(grouponRulesId, null)) {
                    order2.setBid(AccountStatus.BID_27);
                }
                if (com.graphai.mall.db.util.StringHelper.isNotNullAndEmpty(shareId)) {
                    order2.setShareId(shareId);
                }
                if (Objects.equals(virtualGood, 7)) {
                    order2.setHuid("groupgoods");    //团购2.0
                } else {
                    order2.setHuid("zygoods"); //自营和团购1.0
                }
                order2.setGrouponPrice(grouponPrice); // 团购价
                order2.setDeptId(deptId);// 订单加归属部门id
                order2.setOrderPayType(2);
                // 添加订单表
                orderService.add(order2);
                lastOrderId = order2.getId();
            }

            // 添加订单商品表项(购物车)
            if (CollectionUtils.isNotEmpty(checkedGoodsList)) {
                for (MallCart cartGoods : checkedGoodsList) {
                    String goodsId = cartGoods.getGoodsId();
                    // 订单商品
                    MallOrderGoods orderGoods = new MallOrderGoods();
                    orderGoods.setOrderId(order.getId());
                    orderGoods.setGoodsId(goodsId);
                    orderGoods.setGoodsSn(cartGoods.getGoodsSn());
                    orderGoods.setSrcGoodId(cartGoods.getSrcGoodId());
                    orderGoods.setProductId(cartGoods.getProductId());
                    orderGoods.setGoodsName(cartGoods.getGoodsName());
                    orderGoods.setUserId(userId);
                    orderGoods.setPicUrl(cartGoods.getPicUrl());
                    orderGoods.setPrice(cartGoods.getPrice());
                    orderGoods.setNumber(cartGoods.getNumber());
                    orderGoods.setSpecifications(cartGoods.getSpecifications());
                    orderGoods.setAddTime(LocalDateTime.now());
                    orderGoods.setCardNumber("group" + System.currentTimeMillis());
                    orderGoods.setChargeType("2"); //2没核销 3已核销
                    orderGoodsService.add(orderGoods);
                }
            }

            //判断是否新用户团购
            String newcomerGroupGoodsId = mallSystemConfigService.getMallSystemCommonKeyValue("newcomer_group_goods_id");

            if (groupProductId != null) {
                JSONObject obj = null;
                try {
                    obj = mallActivityGoodsProductService.queryGoodsByProductId(groupProductId);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                if (obj.getString("gid").equalsIgnoreCase(newcomerGroupGoodsId)) {
                    List<MallOrderGoods> mallOrderGoodsList = orderService.getUserOrderGoods(newcomerGroupGoodsId, userId);
                    if (null != mallOrderGoodsList && mallOrderGoodsList.size() > 0) {
                        return ResponseUtil.fail("您已经参与过新人团购活动");
                    }
                }
                // 订单商品(非购物车)
                MallOrderGoods orderGoods = new MallOrderGoods();
                orderGoods.setOrderId(orderId);//lastOrderId
                orderGoods.setGoodsId(obj.getString("gid"));
                orderGoods.setProductId(groupProductId);
                orderGoods.setGoodsName(obj.getString("gName"));
                orderGoods.setUserId(userId);
                orderGoods.setPicUrl(obj.getString("picUrl"));
//            orderGoods.setPrice(new BigDecimal(obj.getString("price"))); 这个是获取售价 目前不确定是否直接拿团购价会产生影响
                orderGoods.setPrice(grouponPrice);
                orderGoods.setActualPrice(order.getActualPrice());
                orderGoods.setNumber(Short.parseShort(String.valueOf(buyNum)));
                String s = obj.getString("specifications").replaceAll("\"", "");
                String strSub = s.substring(1, s.length() - 1);
                String strArray[] = strSub.split(",");
                orderGoods.setSpecifications(strArray);
                // 判断商品是否为自营商品,且商品是否存在返现队列
                String cashbackType = obj.getString("cashbackType"); // 是否返现商品
                // String cashbackRate = obj.getString("cashbackRate"); // 返现率 默认0%
                if (StringUtils.isNotEmpty(cashbackType)) {
                    orderGoods.setCashbackType(cashbackType);
                    // 返现金额目前取值：商品售价
                    BigDecimal surplusCashback =
                            new BigDecimal(obj.getString("activity_price")).multiply(new BigDecimal(String.valueOf(buyNum)));
                    // 有返现额度则不是全返
                    if (! Objects.equals(null, obj.getBigDecimal("cashbackQuota"))
                            && obj.getBigDecimal("cashbackQuota").compareTo(new BigDecimal(0)) == 1) {
                        surplusCashback = surplusCashback.multiply(obj.getBigDecimal("cashbackQuota"));
                    }
                    orderGoods.setCashbackAmount(new BigDecimal(0));
                    orderGoods.setSurplusCashback(surplusCashback);
                }
                orderGoodsService.add(orderGoods);
                if (GroupEnum.PAYMDOE_2.getIntCode() == paymdoe) {
                    // 订单商品(非购物车)
                    MallOrderGoods orderGoodsV2 = new MallOrderGoods();
                    orderGoodsV2.setOrderId(lastOrderId);
                    orderGoodsV2.setGoodsId(obj.getString("gid"));
                    orderGoodsV2.setProductId(groupProductId);
                    orderGoodsV2.setGoodsName(obj.getString("gName"));
                    orderGoodsV2.setUserId(userId);
                    orderGoodsV2.setPicUrl(obj.getString("picUrl"));
                    orderGoodsV2.setPrice(grouponPrice);
                    orderGoodsV2.setActualPrice(order2.getActualPrice());
                    orderGoodsV2.setNumber(Short.parseShort(String.valueOf(buyNum)));
                    orderGoodsV2.setCardNumber("group" + System.currentTimeMillis());
                    orderGoodsV2.setChargeType("2"); //2没核销 3已核销
                    String s1 = obj.getString("specifications").replaceAll("\"", "");
                    String strSubV1 = s1.substring(1, s.length() - 1);
                    String strArrayV1[] = strSubV1.split(",");
                    orderGoodsV2.setSpecifications(strArrayV1);
                    // 判断商品是否为自营商品,且商品是否存在返现队列
                    String cashbackTypeV1 = obj.getString("cashbackType"); // 是否返现商品
                    if (StringUtils.isNotEmpty(cashbackTypeV1)) {
                        orderGoodsV2.setCashbackType(cashbackTypeV1);
                        // 返现金额目前取值：商品售价
                        BigDecimal surplusCashback =
                                new BigDecimal(obj.getString("activity_price")).multiply(new BigDecimal(String.valueOf(buyNum)));
                        // 有返现额度则不是全返
                        if (! Objects.equals(null, obj.getBigDecimal("cashbackQuota"))
                                && obj.getBigDecimal("cashbackQuota").compareTo(new BigDecimal(0)) == 1) {
                            surplusCashback = surplusCashback.multiply(obj.getBigDecimal("cashbackQuota"));
                        }
                        orderGoodsV2.setCashbackAmount(new BigDecimal(0));
                        orderGoodsV2.setSurplusCashback(surplusCashback);
                    }
                    orderGoodsService.add(orderGoodsV2);
                }
            }
            // 删除购物车里面的商品信息
            if (CollectionUtils.isNotEmpty(checkedGoodsList)) {
                cartService.clearGoods(userId);
            }
            // 商品货品数量校验
            if (CollectionUtils.isNotEmpty(checkedGoodsList)) {
                for (MallCart checkGoods : checkedGoodsList) {
                    String productId = checkGoods.getProductId();
                    JSONObject product = null;
                    try {
                        product = goodsService.queryGoodsByProductId(productId);
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    if (Objects.equals(null, product)) {
                        return ResponseUtil.fail(507, "商品信息有误!");
                    }
                    Integer remainNumber = product.getInteger("number") - checkGoods.getNumber();
                    if (remainNumber < 0) {
                        throw new RuntimeException("下单的商品货品数量大于库存量");
                    }
                }
            }
            //达人限制
            MallUser user = userService.findById(userId);
            if(null == user){
                return ResponseUtil.unlogin();
            }
          /*  if (GroupEnum.GROUP_COMMANDER.getCode().equals(grouponRulesId) && user.getIsTalent().equalsIgnoreCase(MallUserEnum.IS_TALENT_0.getCode())
                    && rules.getOpenGrouponUser().equalsIgnoreCase(GroupEnum.OPEN_GROUPON_USER_TALENT.getCode())) {
                return ResponseUtil.fail("您还不是达人，无法发起团购");
            }*/
            // 如果是团购项目，添加团购信息
            String groupId = "";
            if (StrUtil.isNotEmpty(grouponRulesId) && ! GroupEnum.GROUP_COMMANDER.getCode().equals(grouponRulesId)) {
                MallGroupon groupon = new MallGroupon();
                groupon.setOrderId(parentOrderOn);
                groupon.setPayed(false);
                groupon.setUserId(userId);
                groupon.setIsTalent(user.getIsTalent());
                groupon.setRulesId(grouponRulesId);
                groupon.setExpirationTime(LocalDateTime.now().plusHours(rules.getDuration()));
                groupon.setMobile(user.getMobile());
                if (! Objects.equals(null, user.getNickname())) {
                    //shareUrl冗余字段用来存名字 团购脱敏3.0
                    groupon.setShareUrl(NameUtil.nameDesensitizationDef(user.getNickname()));
                }
                if (null == user.getHiddenPoints()) {
                    groupon.setHiddenPoints(Integer.valueOf(mallSystemConfigService.getMallSystemCommonKeyValue("mall_user_hidden_points")));
                   //初始化用户隐藏分
                    userService.groupHiddenPoints(user.getId(), groupon.getHiddenPoints());
                } else {
                    groupon.setHiddenPoints(user.getHiddenPoints());
                }
                groupon.setVipPoints(0);
                if (MallUserEnum.IS_TALENT_1.getCode().equalsIgnoreCase(user.getIsTalent())) {
                    Integer talentPoints = Integer.valueOf(mallSystemConfigService.getMallSystemCommonKeyValue("mall_group_talent_points"));
                    groupon.setVipPoints(talentPoints);
                }
                groupon.setUserPoints(groupon.getHiddenPoints() - groupon.getVipPoints());
                if (! Objects.equals(null, user.getAvatar())) {
                    groupon.setAvatar(user.getAvatar());
                }
                groupon.setCreatorUserId(userId);
                // 参与者
                if (grouponLinkId != null) {
                    groupon.setGrouponId(grouponLinkId);
                    groupId = grouponLinkId;
                } else {
                    groupon.setGrouponId("0");
                    groupon.setJoinNum(0);
                }
                grouponService.createGroupon(groupon);
                MallOrder mallOrder = new MallOrder();
                mallOrder.setId(orderId);
                if ("0".equalsIgnoreCase(groupon.getGrouponId())) {
                    mallOrder.setGrouponId(groupon.getId());
                } else {
                    mallOrder.setGrouponId(groupon.getGrouponId());
                }
                orderService.updateByPrimaryKeySelective(mallOrder);
                if (StrUtil.isEmpty(grouponLinkId)) {
                    groupId = groupon.getId();
                }
            }

            Map<String, Object> data = new HashMap<>();
            data.put("orderId", orderId);
            data.put("price", finalActualPrice);
            data.put("freightPrice", freightPrice);
            data.put("groupId", groupId);
            return ResponseUtil.ok(data);
        } catch (Exception e) {
            throw e;
//            return ResponseUtil.fail(-1,e.getMessage());
        }

    }

    /**
     * 提交订单
     * <p>
     * 1. 创建订单表项和订单商品表项; 2. 购物车清空; 3. 优惠券设置已用; 4. 商品货品库存减少; 5. 如果是团购商品，则创建团购活动表项。
     *
     * @param userId 用户ID
     * @param body 订单信息，{ cartId：xxx, addressId: xxx, couponId: xxx, message: xxx, grouponRulesId: xxx,
     *        grouponLinkId: xxx}
     * @return 提交订单操作结果
     * @Param platFrom 数据来自哪个平台（1、大淘客 2、福禄 3、中闵在线 4、蓝色兄弟 5、优惠券商品）
     */
    @Transactional
    public Object submit(String userId, String body, String clientIp) {

        Map<String, Object> data = null;
        try {
            if (userId == null) {
                return ResponseUtil.unlogin();
            }
            if (body == null) {
                return ResponseUtil.badArgument();
            }

            // 平台ID
            String platFrom = JacksonUtil.parseString(body, "platform");

            // 商户或工厂入驻
            if (Objects.equals(platFrom, OrderUtil.PLATFROM_MERCHANT) || Objects.equals(platFrom, OrderUtil.PLATFROM_PLANT)) {
                return insertOrderByMerchant(userId, body, clientIp, platFrom);
            }


            // 福禄商品下单
            if (Objects.equals(platFrom, OrderUtil.PLATFROM_FULU)) {
                return insertOrderByThree(userId, body, clientIp, "fuluGoods");
            }
            // 中闵在线
            else if (Objects.equals(platFrom, OrderUtil.PLATFROM_ZM)) {
                return insertOrderByThree(userId, body, clientIp, "zmGoods");
            }
            // 蓝色兄弟
            else if (Objects.equals(platFrom, OrderUtil.PLATFROM_LSXD)) {
                return insertOrderByThree(userId, body, clientIp, "lsxdGoods");
            }
            // 购买优惠券商品
            else if (Objects.equals(platFrom, OrderUtil.PLATFROM_OTHER)) {
                return insertOrderByOther(userId, body, clientIp, "couponGoods");
            }

            // 话费充值
            else if (Objects.equals(platFrom, OrderUtil.PLATFROM_COST)) {
                return insertOrderByCost(userId, body, clientIp, "cost");
            }

            logger.info("参数body:" + body);
            MallCoupon coupon = null;
            MallCouponUser couponUser = null;
            String cartId = JacksonUtil.parseString(body, "cartId");
            String addressId = JacksonUtil.parseString(body, "addressId");
            String couponId = JacksonUtil.parseString(body, "couponId");
            // 商户id
            String merchantId = JacksonUtil.parseString(body, "merchantId");
            // 优惠券编码
            String couponCode = JacksonUtil.parseString(body, "couponCode");

            String message = JacksonUtil.parseString(body, "message");
            String grouponRulesId = JacksonUtil.parseString(body, "grouponRulesId");
            String grouponLinkId = JacksonUtil.parseString(body, "grouponLinkId");
            Boolean isUseIntegral = JacksonUtil.parseBoolean(body, "isUseIntegral");
            String roomId = JacksonUtil.parseString(body, "roomId");//直播间ID，只有直播间下单才存在

            // 分销员的主键ID
            String tpid = JacksonUtil.parseString(body, "tpid");
            // String shId = JacksonUtil.parseInteger(body, "shId");
            String shId = JacksonUtil.parseString(body, "shId");

            // 下单端口H5端默认写Customer，小程序目前是空，后续可以直接写MiniBusiness
            String submitClient = JacksonUtil.parseString(body, "submitClient");
            // 如果接口为空的话
            submitClient = StringUtils.isNotBlank(submitClient) ? submitClient : "MiniBusiness";
            // 当前购买商品的是什么用户
            MallUser myUser = userService.findById(userId);

            // 如果是C端用户下单端口，tpId和shId是不能为空的
            if (MallConstant.CLIENT_CUSTOMER.equals(submitClient)) {
                if (StringUtils.isBlank(tpid) || StringUtils.isBlank(shId)) {
                    return ResponseUtil.badArgument();
                } else {
                    // 校验tpid和shId里面的记录是不是真确的
                    MallRebateRecord rebate = fenXiaoService.getMallRebateRecordById(shId);
                    if (rebate != null && rebate.getUserId() != null && tpid.equals(rebate.getUserId().toString())) {

                    } else {
                        return ResponseUtil.badArgument();
                    }
                }
            }

            // 如果是团购项目,验证活动是否有效
            if (grouponRulesId != null && !"0".equals(grouponRulesId)) {
                MallGrouponRules rules = grouponRulesService.queryById(grouponRulesId);
                // 找不到记录
                if (rules == null) {
                    return ResponseUtil.badArgument();
                }
                // 团购活动已经过期
                if (grouponRulesService.isExpired(rules)) {
                    return ResponseUtil.fail(GROUPON_EXPIRED, "团购活动已过期!");
                }
            }

            if (cartId == null || addressId == null || couponId == null) {
                return ResponseUtil.badArgument();
            }

            // 收货地址
            MallAddress checkedAddress = addressService.query(userId, addressId);
            if (checkedAddress == null) {
                return ResponseUtil.badArgument();
            }

            // 团购优惠
            BigDecimal grouponPrice = BigDecimal.ZERO;
            MallGrouponRules grouponRules = grouponRulesService.queryById(grouponRulesId);
            if (grouponRules != null) {
                grouponPrice = grouponRules.getDiscount();
            }

            // 货品价格
            List<MallCart> checkedGoodsList = null;
            if (cartId.equals(0) || "0".equals(cartId)) {
                logger.info("进入userid 查询购物车产品 ：" + userId);
                checkedGoodsList = cartService.queryByUidAndChecked(userId);
            } else {
                logger.info("进入cartId 查询购物车产品 ：" + cartId);
                MallCart cart = cartService.findById(cartId);
                checkedGoodsList = new ArrayList<>(1);
                checkedGoodsList.add(cart);
            }
            if (checkedGoodsList.size() == 0) {
                return ResponseUtil.badArgumentValue("选择商品数据不正确");
            }
            BigDecimal checkedGoodsPrice = BigDecimal.ZERO;
            // 使用优惠券减免的金额
            BigDecimal couponPrice = BigDecimal.ZERO;

            // 根据订单商品总价计算运费，满足条件（例如88元）则免运费，否则需要支付运费（例如8元）；
            BigDecimal freightPrice = new BigDecimal(0.00);
            // 业务类型
            String bid = AccountStatus.BID_53;
            // 直推人分佣
            BigDecimal pubSharePreFee = BigDecimal.ZERO;
            for (MallCart checkGoods : checkedGoodsList) {
                BigDecimal price = BigDecimal.ZERO;
                // 只有当团购规格商品ID符合才进行团购优惠
                if (grouponRules != null && grouponRules.getGoodsId().equals(checkGoods.getGoodsId())) {
                    price = checkGoods.getPrice().subtract(grouponPrice)
                            .multiply(new BigDecimal(checkGoods.getNumber()));
                    checkedGoodsPrice = checkedGoodsPrice.add(price);
                } else {
                    // 商品初始价格
                    BigDecimal goodsPrice = checkGoods.getPrice();
                    BigDecimal finalPrice = goodsPrice;
                    // if(shId!=null){
                    // MallRebateRecord rebate =
                    // fenXiaoService.getMallRebateRecordById(shId);
                    // finalPrice = GoodsUtils.getRetailPrice(rebate, goodsPrice);
                    // }
                    price = finalPrice.multiply(new BigDecimal(checkGoods.getNumber()));
                    // 订单成交计算金额
                    checkedGoodsPrice =
                            checkedGoodsPrice.add(price);
                }

                // 获取可用的优惠券信息
                if(StringUtils.isNotEmpty(couponId)){
                    MallCouponUser mallCouponUser = mallCouponUserService.findById(couponId);
                    //获取当前时间
                    LocalDateTime nowTime= LocalDateTime.now();
                    if(mallCouponUser == null || !mallCouponUser.getStatus().equals(CouponUserConstant.STATUS_USABLE)
                            || !((nowTime.isAfter(mallCouponUser.getStartTime()) && (nowTime.isBefore(mallCouponUser.getEndTime()))))){
                        return ResponseUtil.fail("用户无法使用该优惠卷");
                    }
                    MallCoupon mallCoupon = mallCouponService.findById(mallCouponUser.getCouponId());

                    if(mallCoupon.getCouponType() == 1){
                        // 优惠券折扣券
                        couponPrice = checkedGoodsPrice.subtract(checkedGoodsPrice.multiply(mallCoupon.getDiscount()));
                    }else{
                        // 优惠券代金券
                        couponPrice = mallCoupon.getDiscount();
                    }
                }

                MallGoods mallGoods = goodsService.findById(checkGoods.getGoodsId());

                pubSharePreFee = pubSharePreFee.add(price.multiply(mallGoods.getFirstRatio().divide(BigDecimal.valueOf(100))));
                if(StringUtils.isEmpty(merchantId)){
                    merchantId = mallGoods.getMerchantId();
                }

                // 运费计算
                // 发货地址
                if(!merchantId.equals("00")){
                    MallUserMerchant mallUserMerchant = mallUserMerchantService.getOne(new QueryWrapper<MallUserMerchant>().eq("merchant_id", merchantId));
                    MallMerchant mallMerchant = mallMerchantService.getById(merchantId);
//                    MallUser mallUser = mallUserService.queryByUserId(mallUserMerchant.getUserId());
//                    MallAddress mallAddress = mallAddressService.queryByUidAndIsDefault(mallUser.getId(),"2");
//                    if(mallAddress != null){
                    String sendAddress = mallMerchant.getLongitude() + "," + mallMerchant.getLatitude();
                    // 收货地址
                    String receiveAddress = checkedAddress.getLongitude() + "," + checkedAddress.getLatitude();

                    freightPrice = freightPrice.add(freightTemplateService.calculateCost(mallGoods.getTemplateId(),
                            mallGoods.getMerchantId(),checkedAddress.getProvince(),checkedAddress.getCity(),Integer.parseInt(checkGoods.getNumber().toString()),
                            sendAddress,receiveAddress));
//                    }
                }

                if(checkGoods.getIsActivity() == 2){
                    // 直播
                    bid = AccountStatus.BID_55;
                }else if(checkGoods.getIsActivity() == 1){
                    // 秒杀
                    bid = AccountStatus.BID_56;
                }else if(checkGoods.getIsActivity() == 3){
                    // 团购
                    bid = AccountStatus.BID_57;
                }else if(checkGoods.getIsActivity() == 4){
                    // 工厂或商户入驻
                    bid = AccountStatus.BID_59;
                }
            }

            // 平台下单
            if(merchantId.equals("00")){
                bid = AccountStatus.BID_58;
            }

            // 下单人是商家用户
            Boolean isMerchant = false;
            if(myUser.getLevelId().equals("9") || myUser.getLevelId().equals("10") || myUser.getLevelId().equals("11")){
                isMerchant = true;
                bid = PurchaseEnum.BID_2.getCode();
            }

            // 可以使用的其他钱，例如用户积分
            BigDecimal integralPrice = BigDecimal.ZERO;
            if(!merchantId.equals("00") && isUseIntegral){
                MallMerchant mallMerchant = mallMerchantService.getById(merchantId);
                if(mallMerchant != null && checkedGoodsPrice.compareTo(mallMerchant.getEmployIntegral()) > -1){
                    // 订单大于商户设置的积分满减
                    FcAccount account = accountService.getUserAccount(userId);
                    BigDecimal integral = account.getIntegralAmount();

                    // 账户可抵扣金额
                    BigDecimal price = integral.divide(BigDecimal.valueOf(100));
                    if(price.compareTo(mallMerchant.getDeduction()) > -1){
                        // 用户积分大于等于商户设置的可抵扣积分
                        integral = integral.subtract(mallMerchant.getDeduction().multiply(BigDecimal.valueOf(100)));
                        account.setIntegralAmount(integral);
                        integralPrice = mallMerchant.getDeduction();
                    }else{
//                        price = price.setScale( 0, BigDecimal.ROUND_DOWN);
                        account.setIntegralAmount(integral.subtract(price.multiply(BigDecimal.valueOf(100))));
                        integralPrice = price;
                    }
                }
            }

            // 订单费用
            BigDecimal orderTotalPrice = checkedGoodsPrice.add(freightPrice);
            // 最终支付费用
            BigDecimal actualPrice = orderTotalPrice.subtract(couponPrice).subtract(integralPrice);
            BigDecimal finalActualPrice = actualPrice;

            String orderId = null;
            if(isMerchant){
                // 商家下单
                MallPurchaseOrder order = null;
                // 用户订单
                order = new MallPurchaseOrder();
                List<MallUserMerchant> mallUserMerchant = mallUserMerchantService.list(new QueryWrapper<MallUserMerchant>().eq("user_id", userId));
                MallMerchant mallMerchant = null;
                for (MallUserMerchant tempMerchant: mallUserMerchant) {
                    // 商户信息
                    QueryWrapper<MallMerchant> merchantWrapper = new QueryWrapper<MallMerchant>();
                    merchantWrapper.eq("status", 0);// 正常营业
                    merchantWrapper.eq("audit_status", 1);// 审核通过
                    merchantWrapper.eq("id",tempMerchant.getMerchantId());
                    mallMerchant = mallMerchantService.getOne(merchantWrapper);

                    if(mallMerchant != null){
                        break;
                    }
                }
                if(mallMerchant == null){
                    return ResponseUtil.fail("该商户账号异常");
                }
                MallUserMerchant factory = mallUserMerchantService.getOne(new QueryWrapper<MallUserMerchant>().eq("merchant_id", merchantId));

                order.setUserId(userId);
                order.setMerchantId(mallMerchant.getId());
                order.setFactoryMerchantId(merchantId);
                order.setFactoryUserId(factory.getUserId());

                order.setOrderSn(GraphaiIdGenerator.nextId("OrderSN"));
                order.setOrderStatus(Integer.parseInt(OrderUtil.STATUS_CREATE.toString()));
                order.setConsignee(checkedAddress.getName());
                order.setMobile(checkedAddress.getTel());
                order.setMessage(message);
                String detailedAddress = checkedAddress.getProvince() + checkedAddress.getCity()
                        + checkedAddress.getCounty() + " " + checkedAddress.getAddressDetail();
                order.setAddress(detailedAddress);
                order.setLongitude(checkedAddress.getLongitude());
                order.setLatitude(checkedAddress.getLatitude());
                order.setGoodsPrice(checkedGoodsPrice);
                order.setFreightPrice(freightPrice);
                order.setCouponPrice(couponPrice);
                order.setIntegralPrice(integralPrice);
                order.setOrderPrice(orderTotalPrice);
                order.setActualPrice(finalActualPrice); // 订单实付金额
                order.setComments(checkedGoodsList.size());

                order.setAddTime(LocalDateTime.now());
                order.setCouponId(couponId);
                order.setSource(submitClient);
                order.setProvinceName(checkedAddress.getProvince());
                order.setCityName(checkedAddress.getCity());
                order.setAreaName(checkedAddress.getCounty());
                order.setAddressDetail(checkedAddress.getAddressDetail());
                order.setClientIp(clientIp);
                order.setBid(bid);
                order.setRemainingRefundAmount(finalActualPrice);
                order.setHuid("zy");
                order.setOrderType((short)1);
                if(StrUtil.isNotEmpty(roomId)){
                    order.setDeptId(roomId);
                }

                // 有团购活动
                if (grouponRules != null) {
                    order.setGrouponPrice(grouponPrice); // 团购价格
                } else {
                    order.setGrouponPrice(BigDecimal.ZERO); // 团购价格
                }

                // 添加订单表
                mallPurchaseOrderService.save(order);
                orderId = order.getId();

                // 添加订单商品表项
                for (MallCart cartGoods : checkedGoodsList) {
                    String goodsId = cartGoods.getGoodsId();

                    // 订单商品
                    MallPurchaseOrderGoods orderGoods = new MallPurchaseOrderGoods();
                    orderGoods.setOrderId(order.getId());
                    orderGoods.setGoodsId(goodsId);
                    orderGoods.setUserId(userId);
                    orderGoods.setGoodsSn(cartGoods.getGoodsSn());
                    orderGoods.setSrcGoodId(cartGoods.getSrcGoodId());
                    orderGoods.setProductId(cartGoods.getProductId());
                    orderGoods.setGoodsName(cartGoods.getGoodsName());

                    if (coupon != null && couponUser != null) {
                        String[] goodsIds = coupon.getGoodsValue();
                        if (ArrayUtils.contains(goodsIds, goodsId)) {
                            orderGoods.setCouponUserId(couponUser.getId());
                            orderGoods.setCouponPrice(coupon.getDiscount());
                        }
                    }

                    orderGoods.setPicUrl(cartGoods.getPicUrl());
                    orderGoods.setPrice(cartGoods.getPrice());
                    orderGoods.setNumber(Integer.parseInt(cartGoods.getNumber().toString()));
                    orderGoods.setSpecifications(JSON.toJSONString(cartGoods.getSpecifications()));
                    orderGoods.setAddTime(LocalDateTime.now());
                    orderGoods.setUserId(userId);
                    mallPurchaseOrderGoodsService.save(orderGoods);
                }
            }else{
                // 普通用户下单
                MallOrder order = null;
                // 用户订单
                order = new MallOrder();
                order.setUserId(userId);
                order.setOrderSn(GraphaiIdGenerator.nextId("OrderSN"));
                order.setOrderStatus(OrderUtil.STATUS_CREATE);
                order.setConsignee(checkedAddress.getName());
                order.setMobile(checkedAddress.getTel());
                order.setMessage(message);
                String detailedAddress = checkedAddress.getProvince() + checkedAddress.getCity()
                        + checkedAddress.getCounty() + " " + checkedAddress.getAddressDetail();
                order.setAddress(detailedAddress);
                order.setLongitude(checkedAddress.getLongitude());
                order.setLatitude(checkedAddress.getLatitude());
                order.setGoodsPrice(checkedGoodsPrice);
                order.setFreightPrice(freightPrice);
                order.setCouponPrice(couponPrice);
                order.setIntegralPrice(integralPrice);
                order.setOrderPrice(orderTotalPrice);
                order.setActualPrice(finalActualPrice); // 订单实付金额
                order.setMerchantId(merchantId);
                order.setComments((short)checkedGoodsList.size());
                if (StringUtils.isNotBlank(shId)) {
                    order.setShId(shId);
                }

                order.setCouponId(couponId);
                order.setSource(submitClient);
                order.setProvinceName(checkedAddress.getProvince());
                order.setCityName(checkedAddress.getCity());
                order.setAreaName(checkedAddress.getCounty());
                order.setAddressDetail(checkedAddress.getAddressDetail());
                order.setClientIp(clientIp);
                order.setBid(bid);
                if(AccountStatus.BID_55.equals(bid)){
                    //如果是直播订单，则存入直播间ID
                    order.setTbkRelationId(roomId);
                }
                order.setRemainingRefundAmount(finalActualPrice);
                order.setPubSharePreFee(pubSharePreFee.toString());
                order.setHuid("zy");

                // 1代表为分销商用户购买商品,2代表普通用户购买，普通用户购买的情况下要计算分销商收益，真正的收益计算在支付后
                if ("1".equals(myUser.getIsDistribut())) {
                    order.setTpid(userId.toString());
                    order.setTpUserName(myUser.getNickname());
                } else if (!"1".equals(myUser.getIsDistribut()) && StringUtils.isNotBlank(tpid)) {
                    // 分销员信息
                    MallUser fenXiaoUser = userService.findById(tpid);
                    order.setTpid(tpid);
                    order.setTpUserName(fenXiaoUser.getNickname());
                }

                // 有团购活动
                if (grouponRules != null) {
                    order.setGrouponPrice(grouponPrice); // 团购价格
                } else {
                    order.setGrouponPrice(BigDecimal.ZERO); // 团购价格
                }

                List<MallOrderGoods> orderGoodsList = new ArrayList<MallOrderGoods>();
                Boolean becomeVip = false;
                for (MallCart cartGoods : checkedGoodsList) {
                    /**
                     * 为了卖VIP加的逻辑
                     */
                    MallGoods goodsInfo = goodsService.findById(cartGoods.getGoodsId());
                    // 如果商品的Id是VIP商品目录下的，说明在办理会员
                    if (goodsInfo.getCategoryId() != null && goodsInfo.getCategoryId().toString().equals("1036108")) {
                        logger.info("会员订单.............");
                        becomeVip = MallConstant.ORDER_BECOME_VIP;
                        order.setBecomeVip(becomeVip);
                    }

                }
                // 添加订单表
                orderService.add(order);
                orderId = order.getId();


                // 添加订单商品表项
                for (MallCart cartGoods : checkedGoodsList) {
                    String goodsId = cartGoods.getGoodsId();

                    // 订单商品
                    MallOrderGoods orderGoods = new MallOrderGoods();
                    orderGoods.setOrderId(order.getId());
                    orderGoods.setGoodsId(goodsId);
                    orderGoods.setUserId(userId);
                    orderGoods.setGoodsSn(cartGoods.getGoodsSn());
                    orderGoods.setSrcGoodId(cartGoods.getSrcGoodId());
                    orderGoods.setProductId(cartGoods.getProductId());
                    orderGoods.setGoodsName(cartGoods.getGoodsName());

                    if (coupon != null && couponUser != null) {
                        String[] goodsIds = coupon.getGoodsValue();
                        if (ArrayUtils.contains(goodsIds, goodsId)) {
                            orderGoods.setCouponUserId(couponUser.getId());
                            orderGoods.setCouponPrice(coupon.getDiscount());
                        }
                    }

                    orderGoods.setPicUrl(cartGoods.getPicUrl());
                    orderGoods.setPrice(cartGoods.getPrice());
                    orderGoods.setNumber(cartGoods.getNumber());
                    orderGoods.setSpecifications(cartGoods.getSpecifications());
                    orderGoods.setAddTime(LocalDateTime.now());
                    orderGoods.setUserId(userId);
                    orderGoodsService.add(orderGoods);
                    orderGoodsList.add(orderGoods);
                }
            }

            // submitOrderToWpc(order, orderGoodsList);

            // 删除购物车里面的商品信息
            cartService.clearGoodsById(cartId);

            // 商品货品数量减少
            for (MallCart checkGoods : checkedGoodsList) {
                String productId = checkGoods.getProductId();

                if(checkGoods.getIsActivity() != null && checkGoods.getIsActivity() != 0){
                    // 直播活动商品
                    MallActivityGoodsProduct product = mallActivityGoodsProductService.getById(productId);
                    if(product.getNumber() != null && checkGoods.getNumber() != null){
                        Integer tempRemainNumber = 0;
                        tempRemainNumber = product.getNumber() - checkGoods.getNumber();
                        product.setNumber(tempRemainNumber);
                        mallActivityGoodsProductService.updateById(product);
                    }

                    productId = product.getProductId();
                }

                Integer remainNumber = 0;
                MallGoodsProduct product = productService.findById(productId);
                remainNumber = product.getNumber() - checkGoods.getNumber();

                if (remainNumber < 0) {
                    throw new RuntimeException("下单的商品货品数量大于库存量");
                }
                if (productService.reduceStock(productId, checkGoods.getNumber()) == 0) {
                    throw new RuntimeException("商品货品库存减少失败");
                }

            }

            if (!"0".equals(couponId) && !"-1".equals(couponId) && StringUtils.isNotBlank(couponCode)) {
                // MallCouponUser couponUser = couponUserService.queryOne(userId, couponId);
                couponUser = couponUserService.findCouponUserByCouponCode(couponCode);
                couponUser.setStatus(CouponUserConstant.STATUS_USED);
                couponUser.setUsedTime(LocalDateTime.now());
                couponUser.setOrderId(orderId);
                couponUserService.update(couponUser);
            }

            // 如果是团购项目，添加团购信息
            if (grouponRulesId != null && !"0".equals(grouponRulesId)) {
                MallGroupon groupon = new MallGroupon();
                groupon.setOrderId(orderId);
                groupon.setPayed(false);
                groupon.setUserId(userId);
                groupon.setRulesId(grouponRulesId);

                // 参与者
                if (grouponLinkId != null) {
                    // 参与的团购记录
                    MallGroupon baseGroupon = grouponService.queryById(grouponLinkId);
                    groupon.setCreatorUserId(baseGroupon.getCreatorUserId());
                    groupon.setGrouponId(grouponLinkId);
                    groupon.setShareUrl(baseGroupon.getShareUrl());
                } else {
                    groupon.setCreatorUserId(userId);
                    groupon.setGrouponId("0");
                }

                grouponService.createGroupon(groupon);
            }

            data = new HashMap<>();
            data.put("orderId", orderId);
        } catch (Exception e) {
            logger.error("订单提交异常: " + e.getMessage(), e);
        }
        return ResponseUtil.ok(data);
    }

    private Object insertOrderByMerchant(String userId, String body, String clientIp, String platFrom){
        // 销售价
        String goods_price = JacksonUtil.parseString(body, "goods_price");
        //手机号
        String charge_account = JacksonUtil.parseString(body, "charge_account");
        String merchantId = null;
        if(Objects.equals(platFrom, OrderUtil.PLATFROM_MERCHANT)) {
            //商户号
            merchantId = JacksonUtil.parseString(body, "merchant_id");
        }

        MallOrder order = new MallOrder();
        order.setUserId(userId);
        order.setOrderSn(GraphaiIdGenerator.nextId("OrderSN"));
        order.setOrderStatus(OrderUtil.STATUS_CREATE);
        order.setMobile(charge_account);
        order.setMessage("商户入驻费用");
        // 面值
        order.setGoodsPrice(new BigDecimal(goods_price));
        order.setFreightPrice(BigDecimal.ZERO);
        order.setCouponPrice(BigDecimal.ZERO);
        order.setIntegralPrice(BigDecimal.ZERO);
        // 订单总费用
        BigDecimal actualPrice = new BigDecimal(goods_price);
        order.setActualPrice(actualPrice);
        order.setOrderPrice(actualPrice);
        order.setClientIp(clientIp);
        order.setBid(platFrom);
        order.setMerchantId(merchantId);

        // 添加订单表
        orderService.add(order);

        Map<String, Object> data = new HashMap<>();
        data.put("orderId", order.getId());
        return ResponseUtil.ok(data);
    }




    /**
     * 提交订单
     * <p>
     * 1. 创建订单表项和订单商品表项; 2. 购物车清空; 3. 优惠券设置已用; 4. 商品货品库存减少; 5. 如果是团购商品，则创建团购活动表项。
     *
     * @param body 订单信息，{ cartId：xxx, addressId: xxx, couponId: xxx, message: xxx, grouponRulesId: xxx,
     *        grouponLinkId: xxx}
     * @return 提交订单操作结果
     * @Param platFrom 数据来自哪个平台（1、大淘客 2、福禄 3、中闵在线 4、蓝色兄弟 5、优惠券商品）
     */
    @Transactional
    public Object hsCardSubmit(String userId, String body, String clientIp) {
        String deptId = "";// 归属部门ID
        try {
            if (body == null) {
                return ResponseUtil.badArgument();
            }
            // 平台ID
            String platFrom = JacksonUtil.parseString(body, "platform");
            // 手机号
            String mobile = JacksonUtil.parseString(body, "mobile");
            if (StrUtil.isEmpty(userId)) {
                Object result = UserUtils.getTokenUserId(mobile);
                if (!Objects.equals(result, null)) {
                    userId = String.valueOf(result);
                }
            }

            // 福禄商品下单
            if (Objects.equals(platFrom, OrderUtil.PLATFROM_FULU)) {
                return insertOrderByThreeV2(userId, body, clientIp, "fuluGoods", mobile, platFrom);
            }
            // 中闵在线
            else if (Objects.equals(platFrom, OrderUtil.PLATFROM_ZM)) {
                return insertOrderByThreeV2(userId, body, clientIp, "zmGoods", mobile, platFrom);
            }
            // 蓝色兄弟
            else if (Objects.equals(platFrom, OrderUtil.PLATFROM_LSXD)) {
                return insertOrderByThreeV2(userId, body, clientIp, "lsxdGoods", mobile, platFrom);
            }
            // 购买优惠券商品
            else if (Objects.equals(platFrom, OrderUtil.PLATFROM_OTHER)) {
                return insertOrderByOtherV2(userId, body, clientIp, "couponGoods", mobile, platFrom);
            }

            // 购买自营商品
            return buyZyGoods(userId, body, clientIp, deptId);

        } catch (Exception e) {
            logger.error("订单提交异常: " + e.getMessage(), e);
            throw e;
        }

    }

    /**
     * 取消订单
     * <p>
     * 1. 检测当前订单是否能够取消； 2. 设置订单取消状态； 3. 商品货品库存恢复； 4. TODO 优惠券； 5. TODO 团购活动。
     *
     * @param userId 用户ID
     * @param body 订单信息，{ orderId：xxx }
     * @return 取消订单操作结果
     */
    @Transactional
    public Object cancel(String userId, String body) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        String orderId = JacksonUtil.parseString(body, "orderId");
        if (orderId == null) {
            return ResponseUtil.badArgument();
        }

        MallOrder order = orderService.findById(orderId);
        if (order == null) {
            // 采购订单
            MallPurchaseOrder order1 = mallPurchaseOrderService.getById(orderId);
            if (!order1.getUserId().equals(userId)) {
                return ResponseUtil.badArgumentValue("订单不属于本人,不能取消");
            }
            // 检测是否能够取消
            OrderHandleOption handleOption = OrderUtil.build(order1);
            if (!handleOption.isCancel()) {
                return ResponseUtil.fail(ORDER_INVALID_OPERATION, "订单不能取消");
            }

            // 设置订单已取消状态
            order1.setOrderStatus(OrderUtil.STATUS_CANCEL.intValue());
            order1.setEndTime(LocalDateTime.now());
            if (!mallPurchaseOrderService.updateById(order1)) {
                throw new RuntimeException("更新数据已失效");
            }

            return ResponseUtil.ok();
        }
        if (!order.getUserId().equals(userId)) {
            return ResponseUtil.badArgumentValue("订单不属于本人,不能取消");
        }

        LocalDateTime preUpdateTime = order.getUpdateTime();

        // 检测是否能够取消
        OrderHandleOption handleOption = OrderUtil.build(order);
        if (!handleOption.isCancel()) {
            return ResponseUtil.fail(ORDER_INVALID_OPERATION, "订单不能取消");
        }

        // 设置订单已取消状态
        order.setOrderStatus(OrderUtil.STATUS_CANCEL);
        order.setEndTime(LocalDateTime.now());
        if (orderService.updateWithOptimisticLocker(order) == 0) {
            throw new RuntimeException("更新数据已失效");
        }

        // 不需要处理货品数量
        if (Objects.equals(order.getHuid(), "fuluGoods") || Objects.equals(order.getHuid(), "zmGoods")
                || Objects.equals(order.getHuid(), "lsxdGoods")) {

        } else if (Objects.equals(order.getHuid(), "zygoods")) {

        } else {
            // 商品货品数量增加
            List<MallOrderGoods> orderGoodsList = orderGoodsService.queryByOid(orderId);
            for (MallOrderGoods orderGoods : orderGoodsList) {
                String productId = orderGoods.getProductId();
                Short number = orderGoods.getNumber();
                if (productService.addStock(productId, number) == 0) {
                    throw new RuntimeException("商品货品库存增加失败");
                }
            }
        }

        return ResponseUtil.ok();
    }

    /**
     * 付款订单的预支付会话标识
     * <p>
     * 1. 检测当前订单是否能够付款 2. 微信商户平台返回支付订单ID 3. 设置订单付款状态
     *
     * @param userId 用户ID
     * @param body 订单信息，{ orderId：xxx, channel: xxx,openId: xxx }
     * @return 支付订单ID
     */
    @Transactional
    public Object prepay(String userId, String body, HttpServletRequest request) {
        logger.info("-----预支付参数: " + body);

        String mobile = JacksonUtil.parseString(body, "mobile");
        String channel = JacksonUtil.parseString(body, "channel");
        String orderId = JacksonUtil.parseString(body, "orderId");

        String openId = JacksonUtil.parseString(body, "openId");
//        if (orderId == null) {
//            return ResponseUtil.badArgument();
//        }

        MallOrder order = orderService.findById(orderId);
        MallPurchaseOrder order2 = null;
        if(order == null){
            order2 = mallPurchaseOrderService.getById(orderId);
            if(order2 == null){
                return ResponseUtil.badArgument();
            }
            order2.setOrderSn(GraphaiIdGenerator.nextId("OrderSN"));
            // 修改支付方式
            order2.setSource(channel);
            mallPurchaseOrderService.updateById(order2);
        }else{
            order.setOrderSn(GraphaiIdGenerator.nextId("OrderSN"));
            // 修改支付方式
            order.setSource(channel);
            orderService.updateByExample(order);
        }



        // if (!order.getUserId().equals(userId)) {
        // return ResponseUtil.badArgumentValue();
        // }



        // 检测是否能够取消
        if (ObjectUtil.isNotNull(order)){
            OrderHandleOption handleOption = OrderUtil.build(order);
            if (!handleOption.isPay()) {
                return ResponseUtil.fail(ORDER_INVALID_OPERATION, "订单不能支付");
            }
        }else{
            OrderHandleOption handleOption = OrderUtil.build(order2);
            if (!handleOption.isPay()) {
                return ResponseUtil.fail(ORDER_INVALID_OPERATION, "订单不能支付");
            }
        }

        MallUser user = new MallUser();
        if(StringUtils.isNotEmpty(userId)){
            user = userService.findById(userId);
        }

        WxPayMpOrderResult result = null;
        String prepayId = null;
        try {
            // 区分当前是属于微信环境还是属于微信外H5环境
            if (Channel.wx_h5.equals(channel)) {
                // Nginx有做配置上的修改，详情请查看Nginx Config文件
                String userIp = request.getHeader("X-real-ip");

                Map<String, Object> metadata = new HashMap<String, Object>();
                metadata.put("wapName", wapName);
                metadata.put("wapUrl", wapUrl);
                metadata.put("spbill_create_ip", userIp);
                logger.info("wx_h5--------" + metadata.toString());
                // IpUtil.getRealIp(request)
                // 元转成分
                Integer fee = 0;
                BigDecimal actualPrice = BigDecimal.ZERO;
                SignParams signParams = new SignParams();
                if (ObjectUtil.isNotNull(order)){
                    actualPrice = order.getActualPrice();
                    signParams.setOrderNo(order.getOrderSn());
                    signParams.setBody("orderNo:" + order.getOrderSn());
                }else{
                    actualPrice = order2.getActualPrice();
                    signParams.setOrderNo(order2.getOrderSn());
                    signParams.setBody("orderNo:" + order2.getOrderSn());
                }
                fee = actualPrice.multiply(new BigDecimal(100)).intValue();
                signParams.setAmount(fee.toString());
                signParams.setNotifyUrl(wxPayConfig.getNotifyUrl());
                signParams.setMetadata(metadata);
                signParams.setChannel(Channel.wx_h5);
                PayChannel payChannel = new PayChannel();
                payChannel.setAppId(wxPayConfig.getAppId());
                payChannel.setPartnerId(wxPayConfig.getMchId());
                payChannel.setApiKey(wxPayConfig.getMchKey());
                logger.info("wx_h5----------payChannel----" + payChannel.toString());
                Map<String, Object> mapresult = PaymentSDK.getPaymentSignature(signParams, payChannel);
                String pachage = MapUtils.getString(mapresult, "package");
                prepayId = pachage.replace("prepay_id=", "");
                logger.info("wx_h5----------prepayId----" + prepayId);
                MallUserFormid userFormid = new MallUserFormid();
                userFormid.setOpenid(user.getWeixinOpenid());
                userFormid.setFormid(prepayId);
                userFormid.setIsprepay(true);
                userFormid.setUseamount(3);
                userFormid.setExpireTime(LocalDateTime.now().plusDays(7));
                formIdService.addUserFormid(userFormid);

                return ResponseUtil.ok(mapresult);
            } else if (Channel.wx.equals(channel)) {
                Integer fee = 0;
                BigDecimal actualPrice = BigDecimal.ZERO;
                SignParams signParams = new SignParams();
                if (ObjectUtil.isNotNull(order)){
                    actualPrice = order.getActualPrice();
                    signParams.setOrderNo(order.getOrderSn());
                    signParams.setBody("orderNo:" + order.getOrderSn());
                    signParams.setSubject(order.getOrderSn());
                }else{
                    actualPrice = order2.getActualPrice();
                    signParams.setOrderNo(order2.getOrderSn());
                    signParams.setBody("orderNo:" + order2.getOrderSn());
                    signParams.setSubject(order2.getOrderSn());
                }
                fee = actualPrice.multiply(new BigDecimal(100)).intValue();
                signParams.setAmount(fee.toString());
                signParams.setNotifyUrl(wxPayConfig.getNotifyUrl());
                signParams.setChannel(Channel.wx);

                PayChannel payChannel = new PayChannel();
                payChannel.setAppId(properties.getAppId());
                payChannel.setPartnerId(properties.getMchId());
                payChannel.setApiKey(properties.getMchKey());
                return ResponseUtil.ok(PaymentSDK.getPaymentSignature(signParams, payChannel));

            } else if (Channel.wx_pub.equals(channel)) {
                // 微信公众号
                WxPayUnifiedOrderRequest orderRequest = new WxPayUnifiedOrderRequest();
                BigDecimal actualPrice = BigDecimal.ZERO;
                if (ObjectUtil.isNotNull(order)){
                    orderRequest.setOutTradeNo(order.getOrderSn());
                    orderRequest.setBody("orderNo:" + order.getOrderSn());
                    actualPrice = order.getActualPrice();
                }else{
                    orderRequest.setOutTradeNo(order2.getOrderSn());
                    orderRequest.setBody("orderNo:" + order2.getOrderSn());
                    actualPrice = order2.getActualPrice();
                }

                // orderRequest.setOpenid(openid);
                // 元转成分
                int fee = 0;
                fee = actualPrice.multiply(new BigDecimal(100)).intValue();
                orderRequest.setTotalFee(fee);
                orderRequest.setSpbillCreateIp(IpUtil.getIpAddr(request));
                orderRequest.setOpenid(openId);

                result = wxPubPayService.createOrder(orderRequest);
            } else if (Channel.wx_mini_activity.equals(channel)) {
                // 微信活动小程序支付
                String openid = user.getWeixinMiniOpenid();
                if (openid == null) {
                    return ResponseUtil.fail(AUTH_OPENID_UNACCESS, "openid为空订单不能支付");
                }

                WxPayUnifiedOrderRequest orderRequest = new WxPayUnifiedOrderRequest();
                WxPayUnifiedOrderV3Request orderRequestV3 = new WxPayUnifiedOrderV3Request();

                BigDecimal actualPrice = BigDecimal.ZERO;
                if (ObjectUtil.isNotNull(order)){
                    orderRequest.setOutTradeNo(order.getOrderSn());
                    orderRequest.setBody("orderNo:" + order.getOrderSn());
                    orderRequestV3.setOutTradeNo(order.getOrderSn());
                    actualPrice = order.getActualPrice();
                }else{
                    orderRequest.setOutTradeNo(order2.getOrderSn());
                    orderRequest.setBody("orderNo:" + order2.getOrderSn());
                    orderRequestV3.setOutTradeNo(order2.getOrderSn());
                    actualPrice = order2.getActualPrice();
                }
                // orderRequest.setOpenid(openid);
                // 元转成分
                int fee = 0;
                fee = actualPrice.multiply(new BigDecimal(100)).intValue();
                orderRequest.setTotalFee(fee);
                orderRequest.setSpbillCreateIp(IpUtil.getIpAddr(request));
                orderRequest.setOpenid(openid);

                result = wxMiniActivityPayService.createOrder(orderRequest);
//                result = wxMiniActivityPayService.createOrderV3(TradeTypeEnum.JSAPI,orderRequestV3);

                // 缓存prepayID用于后续模版通知
                prepayId = result.getPackageValue();
                prepayId = prepayId.replace("prepay_id=", "");
                // result.setPackageValue(prepayId);
                MallUserFormid userFormid = new MallUserFormid();
                userFormid.setOpenid(user.getWeixinOpenid());
                userFormid.setFormid(prepayId);
                userFormid.setIsprepay(true);
                userFormid.setUseamount(3);
                userFormid.setExpireTime(LocalDateTime.now().plusDays(7));
                formIdService.addUserFormid(userFormid);
            } else if (Channel.wx_mini_agent.equals(channel)) {
                // 微信活动小程序支付
                String openid = user.getMiniProgramOpenid();
                if (openid == null) {
                    return ResponseUtil.fail(AUTH_OPENID_UNACCESS, "openid为空订单不能支付");
                }

                WxPayUnifiedOrderRequest orderRequest = new WxPayUnifiedOrderRequest();

                BigDecimal actualPrice = BigDecimal.ZERO;
                if (ObjectUtil.isNotNull(order)){
                    orderRequest.setOutTradeNo(order.getOrderSn());
                    orderRequest.setBody("orderNo:" + order.getOrderSn());
                    actualPrice = order.getActualPrice();
                }else{
                    orderRequest.setOutTradeNo(order2.getOrderSn());
                    orderRequest.setBody("orderNo:" + order2.getOrderSn());
                    actualPrice = order2.getActualPrice();
                }

                // orderRequest.setOpenid(openid);
                // 元转成分
                int fee = 0;
                fee = actualPrice.multiply(new BigDecimal(100)).intValue();
                orderRequest.setTotalFee(fee);
                orderRequest.setSpbillCreateIp(IpUtil.getIpAddr(request));
                orderRequest.setOpenid(openid);
//                orderRequest.setAppid("");
//                orderRequest.setMchId("");
                result = wxMiniAgentPayService.createOrder(orderRequest);

                // 缓存prepayID用于后续模版通知
                prepayId = result.getPackageValue();
                prepayId = prepayId.replace("prepay_id=", "");
                // result.setPackageValue(prepayId);
                MallUserFormid userFormid = new MallUserFormid();
                userFormid.setOpenid(user.getWeixinOpenid());
                userFormid.setFormid(prepayId);
                userFormid.setIsprepay(true);
                userFormid.setUseamount(3);
                userFormid.setExpireTime(LocalDateTime.now().plusDays(7));
                formIdService.addUserFormid(userFormid);
            } else if (Channel.alipay.equals(channel)) {
                Integer fee = 0;
                SignParams signParams = new SignParams();
                BigDecimal actualPrice = BigDecimal.ZERO;
                if(ObjectUtil.isNotNull(order)){
                    actualPrice = order.getActualPrice();
                    signParams.setSubject(order.getOrderSn());
                    signParams.setOrderNo(order.getOrderSn());
                    signParams.setBody("orderNo:" + order.getOrderSn());
                }else{
                    actualPrice = order2.getActualPrice();
                    signParams.setSubject(order2.getOrderSn());
                    signParams.setOrderNo(order2.getOrderSn());
                    signParams.setBody("orderNo:" + order2.getOrderSn());
                }

                fee = actualPrice.multiply(new BigDecimal(100)).intValue();

                signParams.setAmount(fee.toString());

                signParams.setNotifyUrl(aliProperties.getNotifyUrl());
                signParams.setChannel(Channel.alipay);

                PayChannel payChannel = new PayChannel();
                payChannel.setAppId(aliProperties.getAppId());
                payChannel.setPartnerId(aliProperties.getPartnerId());
                payChannel.setAliPublicKey(aliProperties.getRsaPublicKey());
                payChannel.setPrivateKey(aliProperties.getRsaPrivateKey());
                return ResponseUtil.ok(PaymentSDK.getPaymentSignature(signParams, payChannel));
            }else if (Channel.jsApiPay.equals(channel)) {
                // 微信小程序支付先获取小程序Openid
                if (openId == null) {
                    return ResponseUtil.fail(AUTH_OPENID_UNACCESS, "openid为空订单不能支付");
                }


//                // 取消订单后再次支付因为汇付不能用同一订单sn支付，所以生成新sn
//                order.setOrderSn(GraphaiIdGenerator.nextId("MallOrder"));
//                order.setUserId(userId);
//                orderService.updateByExample(order);

                WxPayUnifiedOrderRequest orderRequest = new WxPayUnifiedOrderRequest();
                BigDecimal actualPrice = BigDecimal.ZERO;
                if (ObjectUtil.isNotNull(order)){
                    orderRequest.setBody("orderNo:" + order.getOrderSn());
                    orderRequest.setOutTradeNo(order.getOrderSn());
                    actualPrice = order.getActualPrice();
                }else{
                    orderRequest.setBody("orderNo:" + order2.getOrderSn());
                    orderRequest.setOutTradeNo(order2.getOrderSn());
                    actualPrice = order2.getActualPrice();
                }
                // orderRequest.setOpenid(openId);
                // 元转成分
                int fee = 0;
                fee = actualPrice.multiply(new BigDecimal(100)).intValue();
                orderRequest.setTotalFee(fee);
                orderRequest.setSpbillCreateIp(IpUtil.getIpAddr(request));
                orderRequest.setSubOpenid(openId);
                // WxPayConfig payConfig = new WxPayConfig();
                // payConfig.setAppId(properties.getHsAppId());
                // payConfig.setMchId(properties.getHsMchId());
                // payConfig.setSubAppId(properties.getHsAppletsAppId());
                // payConfig.setSubMchId(properties.getHsChildMchId());
                // payConfig.setMchKey(properties.getHsMchKey());
                // payConfig.setNotifyUrl(properties.getHsNotifyUrl());
                // payConfig.setKeyPath(properties.getKeyPath());
                // payConfig.setTradeType("JSAPI");
                // payConfig.setSignType("MD5");
                // hsWxPayService.setConfig(payConfig);

//                if(isPurchase){
//                    payConfig.setNotifyUrl(properties.getMerchantNotifyUrl());
//                }

                result = hsWxPayService.createOrder(orderRequest);

                // 缓存prepayID用于后续模版通知
                prepayId = result.getPackageValue();
                prepayId = prepayId.replace("prepay_id=", "");

                // result.setPackageValue(prepayId);

                MallUserFormid userFormid = new MallUserFormid();
                userFormid.setOpenid(user.getWeixinOpenid());
                userFormid.setFormid(prepayId);
                userFormid.setIsprepay(true);
                userFormid.setUseamount(3);
                userFormid.setExpireTime(LocalDateTime.now().plusDays(7));
                formIdService.addUserFormid(userFormid);


            }else if (Objects.equals(channel, Channel.jsApi_AliPay)) {

                String buyerId = JacksonUtil.parseString(body, "buyerId");
                Integer fee = 0;
                BigDecimal actualPrice = BigDecimal.ZERO;
                SignParams signParams = new SignParams();
                if (ObjectUtil.isNotNull(order)){
                    actualPrice = order.getActualPrice();
                    signParams.setSubject(order.getOrderSn());
                    signParams.setOrderNo(order.getOrderSn());
                    signParams.setBody("orderNo:" + order.getOrderSn());
                }else{
                    signParams.setSubject(order2.getOrderSn());
                    signParams.setOrderNo(order2.getOrderSn());
                    signParams.setBody("orderNo:" + order2.getOrderSn());
                }

                fee = actualPrice.multiply(new BigDecimal(100)).intValue();
                signParams.setAmount(fee.toString());

                signParams.setNotifyUrl(aliProperties.getHsNotifyUrl());
                signParams.setChannel(Channel.jsApi_AliPay);
                signParams.setBuyerId(buyerId);

                PayChannel payChannel = new PayChannel();
                payChannel.setAppId(aliProperties.getHsAppId());
                payChannel.setPartnerId(aliProperties.getPartnerId());
                payChannel.setAliPublicKey(aliProperties.getHsRsaPublicKey());
                payChannel.setPrivateKey(aliProperties.getHsRsaPrivateKey());
                return ResponseUtil.ok(PaymentSDK.getPaymentSignature(signParams, payChannel));
            }
            else {
                // 微信小程序支付先获取小程序Openid
                String openid = user.getMiniProgramOpenid();
                if (openid == null) {
                    return ResponseUtil.fail(AUTH_OPENID_UNACCESS, "订单不能支付");
                }

                WxPayUnifiedOrderRequest orderRequest = new WxPayUnifiedOrderRequest();
                BigDecimal actualPrice = BigDecimal.ZERO;
                if (ObjectUtil.isNotNull(order)){
                    orderRequest.setOutTradeNo(order.getOrderSn());
                    orderRequest.setBody("orderNo:" + order.getOrderSn());
                    actualPrice = order.getActualPrice();
                }else{
                    orderRequest.setOutTradeNo(order2.getOrderSn());
                    orderRequest.setBody("orderNo:" + order2.getOrderSn());
                    actualPrice = order2.getActualPrice();
                }
                // 元转成分
                int fee = 0;
                fee = actualPrice.multiply(new BigDecimal(100)).intValue();
                orderRequest.setTotalFee(fee);
                orderRequest.setSpbillCreateIp(IpUtil.getIpAddr(request));
                orderRequest.setOpenid(openid);
                WxPayConfig payConfig = new WxPayConfig();
                payConfig.setAppId(properties.getMiniProgramAppId());
                payConfig.setMchId(properties.getMchId());
                payConfig.setMchKey(properties.getMchKey());
                payConfig.setNotifyUrl(properties.getNotifyUrl());
                payConfig.setKeyPath(properties.getKeyPath());
                payConfig.setTradeType("JSAPI");
                payConfig.setSignType("MD5");
                wxPayService.setConfig(payConfig);
                result = wxPayService.createOrder(orderRequest);

                // 缓存prepayID用于后续模版通知
                prepayId = result.getPackageValue();
                prepayId = prepayId.replace("prepay_id=", "");

                MallUserFormid userFormid = new MallUserFormid();
                userFormid.setOpenid(user.getWeixinOpenid());
                userFormid.setFormid(prepayId);
                userFormid.setIsprepay(true);
                userFormid.setUseamount(3);
                userFormid.setExpireTime(LocalDateTime.now().plusDays(7));
                formIdService.addUserFormid(userFormid);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseUtil.fail(ORDER_PAY_FAIL, "订单不能支付");
        }

//        if (orderService.updateWithOptimisticLocker(order) == 0) {
//            return ResponseUtil.updatedDateExpired();
//        }
        return ResponseUtil.ok(result);
    }


    @Autowired
    private StorageService storageService;

    /**
     * 微信付款成功或失败回调接口
     * <p>
     * 1. 检测当前订单是否是付款状态; 2. 设置订单付款成功状态相关信息; 3. 响应微信商户平台.
     *
     * @param request 请求内容
     * @param response 响应内容
     * @return 操作结果
     */
    @Transactional(rollbackFor = {Exception.class})
    public Object payNotify(HttpServletRequest request, HttpServletResponse response) {
        MallOrderNotifyLog log = new MallOrderNotifyLog();
        // 1、获取微信回调数据
        String xmlResult = null;
        try {
            xmlResult = IOUtils.toString(request.getInputStream(), request.getCharacterEncoding());
            logger.info("------------->payNotify: " + xmlResult);
            log.setWxParams(xmlResult);
        } catch (IOException e) {
            e.printStackTrace();
            try {
                log.setErrMsg("微信通知参数解析异常：" + e.getMessage());
                log.setIsSuccess("0F");
                logService.add(log);
            } catch (Exception e1) {
                logger.error("日志添加异常: " + e1.getMessage(), e1);
            }
            return WxPayNotifyResponse.fail(e.getMessage());
        }

        WxPayOrderNotifyResult result = null;
        try {
            logger.info("------------->payNotify->WxPayOrderNotifyResult");
            result = wxPayService.parseOrderNotifyResult(xmlResult);

            if (!WxPayConstants.ResultCode.SUCCESS.equals(result.getResultCode())) {
                logger.error(xmlResult);
                throw new WxPayException("微信通知支付失败！");
            }
            if (!WxPayConstants.ResultCode.SUCCESS.equals(result.getReturnCode())) {
                logger.error(xmlResult);
                throw new WxPayException("微信通知支付失败！");
            }
        } catch (WxPayException e) {
            try {
                log.setErrMsg("微信通知参数解析异常：" + e.getMessage());
                log.setIsSuccess("0F");
                logService.add(log);
            } catch (Exception e1) {
                logger.error("日志添加异常: " + e1.getMessage(), e1);
            }
            e.printStackTrace();
            return WxPayNotifyResponse.fail(e.getMessage());
        }

        logger.info("处理腾讯支付平台的订单支付");
        logger.info(result);

        String orderSn = result.getOutTradeNo();
        String payId = result.getTransactionId();
        log.setOrderSn(orderSn);

        // 分转化成元
        String totalFee = BaseWxPayResult.fenToYuan(result.getTotalFee());
        MallOrder order = orderService.findBySn(orderSn);
        if(order == null){
            QueryWrapper<MallPurchaseOrder> orderQueryWrapper = new QueryWrapper<>();
            orderQueryWrapper.eq("order_sn", orderSn);
            MallPurchaseOrder order1 = mallPurchaseOrderService.getOne(orderQueryWrapper);

            if (order1 == null) {
                try {
                    log.setErrMsg("订单不存在 sn=" + orderSn);
                    log.setIsSuccess("0F");
                    logService.add(log);
                } catch (Exception e1) {
                    logger.error("日志添加异常: " + e1.getMessage(), e1);
                }
                return WxPayNotifyResponse.fail("订单不存在 sn=" + orderSn);
            }
            logger.info("----->商户下单回调处理" + JSON.toJSONString(order1));
            if (!Objects.equals(order1.getUserId(), null)) {
                log.setUserId(order1.getUserId());
            }

            // 检查这个订单是否已经处理过
            if (OrderUtil.isPayStatus(order1) && order1.getPayId() != null) {
                try {
                    log.setErrMsg("订单已经处理过");
                    log.setIsSuccess("0F");
                    logService.add(log);
                } catch (Exception e1) {
                    logger.error("日志添加异常: " + e1.getMessage(), e1);
                }
                return WxPayNotifyResponse.success("订单已经处理成功!");
            }

            // 检查支付订单金额
            if (!totalFee.equals(order1.getActualPrice().toString())) {
                try {
                    log.setErrMsg(order1.getOrderSn() + " : 支付金额不符合 totalFee=" + totalFee);
                    log.setIsSuccess("0F");
                    logService.add(log);
                } catch (Exception e1) {
                    logger.error("日志添加异常: " + e1.getMessage(), e1);
                }
                return WxPayNotifyResponse.fail(order1.getOrderSn() + " : 支付金额不符合 totalFee=" + totalFee);
            }
            logger.info("回调节点1，orderId=" + order1.getId());
            order1.setPayId(payId);
            order1.setPayTime(LocalDateTime.now());
            order1.setOrderStatus(OrderUtil.STATUS_PAY.intValue());
            if (StrUtil.isNotEmpty(order1.getDeptId())) {
                //如果采购订单存在直播间id（deptId暂存直播间id）
                mallLiveRoomService.updateGoodsSellingNumber(order.getDeptId(), 1);
//                MallLiveRoom mallLiveRoom = mallLiveRoomService.getById(order.getDeptId());
//                Integer last = mallLiveRoom.getLastSellingGoodsTotal();
//                Integer total = mallLiveRoom.getSellingGoodsTotal();
//                mallLiveRoom.setLastSellingGoodsTotal(last+1);
//                mallLiveRoom.setSellingGoodsTotal(total+1);
//                mallLiveRoomService.updateById(mallLiveRoom);
            }

            logger.info("回调节点2，orderId=" + order1.getId());
            mallPurchaseOrderService.updateById(order1);

            doPayService.doMerchangOrderWithActive(order1);
            String succes = WxPayNotifyResponse.success("处理成功!");
            logger.info(succes);
            return succes;
        }

        logger.info("回调节点3，orderId=");
        try {
            if (order == null) {
                try {
                    log.setErrMsg("订单不存在 sn=" + orderSn);
                    log.setIsSuccess("0F");
                    logService.add(log);
                } catch (Exception e1) {
                    logger.error("日志添加异常: " + e1.getMessage(), e1);
                }
                return WxPayNotifyResponse.fail("订单不存在 sn=" + orderSn);
            }

            if (!Objects.equals(order.getUserId(), null)) {
                log.setUserId(order.getUserId());
            }

            // 检查这个订单是否已经处理过
            if (OrderUtil.isPayStatus(order) && order.getPayId() != null) {
                try {
                    log.setErrMsg("订单已经处理过");
                    log.setIsSuccess("0F");
                    logService.add(log);
                } catch (Exception e1) {
                    logger.error("日志添加异常: " + e1.getMessage(), e1);
                }
                return WxPayNotifyResponse.success("订单已经处理成功!");
            }

            logger.info("回调节点4，orderId=");
            // 检查支付订单金额
            if (!totalFee.equals(order.getActualPrice().toString())) {
                try {
                    log.setErrMsg(order.getOrderSn() + " : 支付金额不符合 totalFee=" + totalFee);
                    log.setIsSuccess("0F");
                    logService.add(log);
                } catch (Exception e1) {
                    logger.error("日志添加异常: " + e1.getMessage(), e1);
                }
                return WxPayNotifyResponse.fail(order.getOrderSn() + " : 支付金额不符合 totalFee=" + totalFee);
            }

            // this.calFenXiao(order,true,true);
            // 如果订单是购买会员产品，那么成为会员
            // userService.becomeVip(order);


            order.setPayId(payId);
            order.setPayTime(LocalDateTime.now());
            order.setOrderStatus(OrderUtil.STATUS_PAY);
            order.setUserId(order.getUserId());

            logger.info("回调节点5，orderId=");
            if (orderService.updateWithOptimisticLocker(order) == 0) {
                logger.info("三方平台开始下单updateWithOptimisticLocker。。。。。。。。。。。。。。" + order.toString());

                // 这里可能存在这样一个问题，用户支付和系统自动取消订单发生在同时
                // 如果数据库首先因为系统自动取消订单而更新了订单状态；
                // 此时用户支付完成回调这里也要更新数据库，而由于乐观锁机制这里的更新会失败
                // 因此，这里会重新读取数据库检查状态是否是订单自动取消，如果是则更新成支付状态。
                order = orderService.findBySn(orderSn);

                int updated = 0;

                // 订单自动取消
                if (OrderUtil.isAutoCancelStatus(order)) {
                    order.setPayId(payId);
                    order.setPayTime(LocalDateTime.now());
                    order.setOrderStatus(OrderUtil.STATUS_PAY);
                    order.setUserId(order.getUserId());
                    updated = orderService.updateWithOptimisticLocker(order);

                    // 修改销量
                    List<MallOrderGoods> mallOrderGoodsList = orderGoodsService.queryByOid(order.getId());
                    if (CollectionUtils.isNotEmpty(mallOrderGoodsList)) {
                        for (int i = 0; i < mallOrderGoodsList.size(); i++) {
                            MallOrderGoods mallOrderGoods = mallOrderGoodsList.get(i);
                            if (mallOrderGoods.getNumber() != null) {
                                MallGoods mallGoods = new MallGoods();
                                mallGoods.setId(mallOrderGoods.getGoodsId());
                                mallGoods.setItemsale(Integer.parseInt(mallOrderGoods.getNumber().toString()));
                                goodsService.updateMallGoodsSales(mallGoods);
                            }
                        }
                    }
                }

                try {
                    log.setRemark("订单支付通知计算完成");
                    log.setIsSuccess("1F");
                    logService.add(log);
                } catch (Exception e1) {
                    logger.error("日志添加异常: " + e1.getMessage(), e1);
                }

                // 如果updated是0，那么数据库更新失败
                if (updated == 0) {
                    return WxPayNotifyResponse.fail("更新数据已失效");
                }
            }

            // 判断是-网赚任务-订单就拦截处理，不往下执行了
          /*  if (AccountStatus.BID_47.equals(order.getBid())) {
                try {
                    return wangzhuanTaskService.orderProcess(order, payId);
                } catch (Exception ex) {
                    logger.info("三方下--网赚任务--单支付出错：" + ex.getMessage(), ex);
                }
            }

            // 判断是-网赚任务-上调数量/上调单价-订单就拦截处理，不往下执行了
            if (AccountStatus.BID_48.equals(order.getBid())) {
                try {
                    return wangzhuanTaskService.orderProcessAddNumOrAmount(order,payId);
                }catch (Exception e){
                    logger.info("三方下-- 网赚任务-上调任务单价/任务数量--订单支付出错："+e.getMessage(),e);
                }
            }
            // 判断是-网赚任务-购买推荐、置顶 / 竞价 -订单就拦截处理，不往下执行了
            if (AccountStatus.BID_49.equals(order.getBid())) {
                try {
                    return wangzhuanTaskServiceV2.orderProcessTBR(order,payId);
                }catch (Exception e){
                    logger.info("三方下-- 网赚任务-上调任务单价/任务数量--订单支付出错："+e.getMessage(),e);
                }
            }*/

            // 支付成功，有团购信息，更新团购信息
            MallGroupon groupon = grouponService.queryByOrderId(order.getId());
            if (groupon != null) {
                MallGrouponRules grouponRules = grouponRulesService.queryById(groupon.getRulesId());

                // 仅当发起者才创建分享图片
                if ("0".equals(groupon.getGrouponId())) {
                    String url = qCodeService.createGrouponShareImage(grouponRules.getGoodsName(),
                            grouponRules.getPicUrl(), groupon);
                    groupon.setShareUrl(url);
                }
                groupon.setPayed(true);
                if (grouponService.updateById(groupon) == 0) {
                    return WxPayNotifyResponse.fail("更新数据已失效");
                }
            }

            logger.info("三方平台开始下单。。。。。。。。。。。。。。");
            logger.info("三方平台开始下单。。。。。。。。。。。。。。" + order.toString());
            logger.info("三方平台开始下单。。。。。。。。。。。。。。" + order.getHuid());
            // 防止重复下单
            try {
                logger.info("开始下单：--------------------------------------");

                // 三方下单支付回调
                if (Objects.equals(order.getHuid(), "fuluGoods")) {
                    payFuLuGoodsOrder(order, "wx");
                } else if (Objects.equals(order.getHuid(), "zmGoods")) {
                    payZMGoodsOrder(order, "wx");
                } else if (Objects.equals(order.getHuid(), "lsxdGoods")) {
                    payLSXDGoodsOrder(order, "wx");
                } else if (Objects.equals(order.getHuid(), "cost")) {
                    payCOSTGoodsOrder(order, "wx");
                }
            } catch (Exception e) {
                logger.info("三方下单支付完成。。。。。。。。。");
            }

            // 购买者id
            String userId = order.getUserId();
            String shareId = order.getShareId();

            // 订单支付成功处理用户权益逻辑
            doPayService.doUserAllBid(order);

            // TODO 发送邮件和短信通知，这里采用异步发送
            // 订单支付成功以后，会发送短信给用户，以及发送邮件给管理员
            // notifyService.notifyMail("新订单通知", order.toString(·));
            // 这里微信的短信平台对参数长度有限制，所以将订单号只截取后6位
            // notifyService.notifySmsTemplateSync(order.getMobile(),
            // NotifyType.PAY_SUCCEED, new String[]{orderSn.substring(8,
            // 14)});

            // 请依据自己的模版消息配置更改参数
            // String[] parms = new String[]{
            // order.getOrderSn(),
            // order.getOrderPrice().toString(),
            // DateTimeUtil.getDateTimeDisplayString(order.getAddTime()),
            // order.getConsignee(),
            // order.getMobile(),
            // order.getAddress()
            // };

            // notifyService.notifyWxTemplate(result.getOpenid(),
            // NotifyType.PAY_SUCCEED, parms, "pages/index/index?orderId=" +
            // order.getId());
        } catch (Exception e) {
            try {
                log.setRemark("订单支付发生异常: " + e.getMessage());
                log.setIsSuccess("0F");
                logService.add(log);
            } catch (Exception e1) {
                logger.error("日志添加异常: " + e1.getMessage(), e1);
            }
            logger.error("支付订单通知处理异常 :" + e.getMessage(), e);
        }

        return WxPayNotifyResponse.success("处理成功!");
    }

    /**
     * 公众号下单回调
     * @param request
     * @param response
     * @return
     */
    @Transactional(rollbackFor = {Exception.class})
    public Object pubNotify(HttpServletRequest request, HttpServletResponse response) {
        MallOrderNotifyLog log = new MallOrderNotifyLog();
        // 1、获取微信回调数据
        String xmlResult = null;
        try {
            xmlResult = IOUtils.toString(request.getInputStream(), request.getCharacterEncoding());
            logger.info("------------->pubNotify: " + xmlResult);
            log.setWxParams(xmlResult);
        } catch (IOException e) {
            e.printStackTrace();
            try {
                log.setErrMsg("微信通知参数解析异常：" + e.getMessage());
                log.setIsSuccess("0F");
                logService.add(log);
            } catch (Exception e1) {
                logger.error("日志添加异常: " + e1.getMessage(), e1);
            }
            return WxPayNotifyResponse.fail(e.getMessage());
        }

        WxPayOrderNotifyResult result = null;
        try {
            logger.info("------------->pubNotify->WxPayOrderNotifyResult");
            result = wxPubPayService.parseOrderNotifyResult(xmlResult);

            if (!WxPayConstants.ResultCode.SUCCESS.equals(result.getResultCode())) {
                logger.error(xmlResult);
                throw new WxPayException("微信通知支付失败！");
            }
            if (!WxPayConstants.ResultCode.SUCCESS.equals(result.getReturnCode())) {
                logger.error(xmlResult);
                throw new WxPayException("微信通知支付失败！");
            }
        } catch (WxPayException e) {
            try {
                log.setErrMsg("微信通知参数解析异常：" + e.getMessage());
                log.setIsSuccess("0F");
                logService.add(log);
            } catch (Exception e1) {
                logger.error("日志添加异常: " + e1.getMessage(), e1);
            }
            e.printStackTrace();
            return WxPayNotifyResponse.fail(e.getMessage());
        }

        logger.info("处理腾讯支付平台的订单支付");
        logger.info(result);

        String orderSn = result.getOutTradeNo();
        String payId = result.getTransactionId();
        log.setOrderSn(orderSn);

        // 分转化成元
        String totalFee = BaseWxPayResult.fenToYuan(result.getTotalFee());
        MallOrder order = orderService.findBySn(orderSn);
        if(order == null){
            QueryWrapper<MallPurchaseOrder> orderQueryWrapper = new QueryWrapper<>();
            orderQueryWrapper.eq("order_sn", orderSn);
            MallPurchaseOrder order1 = mallPurchaseOrderService.getOne(orderQueryWrapper);

            if (order1 == null) {
                try {
                    log.setErrMsg("订单不存在 sn=" + orderSn);
                    log.setIsSuccess("0F");
                    logService.add(log);
                } catch (Exception e1) {
                    logger.error("日志添加异常: " + e1.getMessage(), e1);
                }
                return WxPayNotifyResponse.fail("订单不存在 sn=" + orderSn);
            }
            logger.info("----->商户下单回调处理" + JSON.toJSONString(order1));
            if (!Objects.equals(order1.getUserId(), null)) {
                log.setUserId(order1.getUserId());
            }

            // 检查这个订单是否已经处理过
            if (OrderUtil.isPayStatus(order1) && order1.getPayId() != null) {
                try {
                    log.setErrMsg("订单已经处理过");
                    log.setIsSuccess("0F");
                    logService.add(log);
                } catch (Exception e1) {
                    logger.error("日志添加异常: " + e1.getMessage(), e1);
                }
                return WxPayNotifyResponse.success("订单已经处理成功!");
            }

            // 检查支付订单金额
            if (!totalFee.equals(order1.getActualPrice().toString())) {
                try {
                    log.setErrMsg(order1.getOrderSn() + " : 支付金额不符合 totalFee=" + totalFee);
                    log.setIsSuccess("0F");
                    logService.add(log);
                } catch (Exception e1) {
                    logger.error("日志添加异常: " + e1.getMessage(), e1);
                }
                return WxPayNotifyResponse.fail(order1.getOrderSn() + " : 支付金额不符合 totalFee=" + totalFee);
            }

            order1.setPayId(payId);
            order1.setPayTime(LocalDateTime.now());
            order1.setOrderStatus(OrderUtil.STATUS_PAY.intValue());
            mallPurchaseOrderService.updateById(order1);
            doPayService.doMerchangOrderWithActive(order1);
            String succes = WxPayNotifyResponse.success("处理成功!");
            logger.info(succes);
            return succes;
        }

        try {
            if (order == null) {
                try {
                    log.setErrMsg("订单不存在 sn=" + orderSn);
                    log.setIsSuccess("0F");
                    logService.add(log);
                } catch (Exception e1) {
                    logger.error("日志添加异常: " + e1.getMessage(), e1);
                }
                return WxPayNotifyResponse.fail("订单不存在 sn=" + orderSn);
            }

            if (!Objects.equals(order.getUserId(), null)) {
                log.setUserId(order.getUserId());
            }

            // 检查这个订单是否已经处理过
            if (OrderUtil.isPayStatus(order) && order.getPayId() != null) {
                try {
                    log.setErrMsg("订单已经处理过");
                    log.setIsSuccess("0F");
                    logService.add(log);
                } catch (Exception e1) {
                    logger.error("日志添加异常: " + e1.getMessage(), e1);
                }
                return WxPayNotifyResponse.success("订单已经处理成功!");
            }

            // 检查支付订单金额
            if (!totalFee.equals(order.getActualPrice().toString())) {
                try {
                    log.setErrMsg(order.getOrderSn() + " : 支付金额不符合 totalFee=" + totalFee);
                    log.setIsSuccess("0F");
                    logService.add(log);
                } catch (Exception e1) {
                    logger.error("日志添加异常: " + e1.getMessage(), e1);
                }
                return WxPayNotifyResponse.fail(order.getOrderSn() + " : 支付金额不符合 totalFee=" + totalFee);
            }

            // this.calFenXiao(order,true,true);
            // 如果订单是购买会员产品，那么成为会员
            // userService.becomeVip(order);


            order.setPayId(payId);
            order.setPayTime(LocalDateTime.now());
            order.setOrderStatus(OrderUtil.STATUS_PAY);
            order.setUserId(order.getUserId());
            if (orderService.updateWithOptimisticLocker(order) == 0) {
                logger.info("三方平台开始下单updateWithOptimisticLocker。。。。。。。。。。。。。。" + order.toString());

                // 这里可能存在这样一个问题，用户支付和系统自动取消订单发生在同时
                // 如果数据库首先因为系统自动取消订单而更新了订单状态；
                // 此时用户支付完成回调这里也要更新数据库，而由于乐观锁机制这里的更新会失败
                // 因此，这里会重新读取数据库检查状态是否是订单自动取消，如果是则更新成支付状态。
                order = orderService.findBySn(orderSn);

                int updated = 0;

                // 订单自动取消
                if (OrderUtil.isAutoCancelStatus(order)) {
                    order.setPayId(payId);
                    order.setPayTime(LocalDateTime.now());
                    order.setOrderStatus(OrderUtil.STATUS_PAY);
                    order.setUserId(order.getUserId());
                    updated = orderService.updateWithOptimisticLocker(order);

                    // 修改销量
                    List<MallOrderGoods> mallOrderGoodsList = orderGoodsService.queryByOid(order.getId());
                    if (CollectionUtils.isNotEmpty(mallOrderGoodsList)) {
                        for (int i = 0; i < mallOrderGoodsList.size(); i++) {
                            MallOrderGoods mallOrderGoods = mallOrderGoodsList.get(i);
                            if (mallOrderGoods.getNumber() != null) {
                                MallGoods mallGoods = new MallGoods();
                                mallGoods.setId(mallOrderGoods.getGoodsId());
                                mallGoods.setItemsale(Integer.parseInt(mallOrderGoods.getNumber().toString()));
                                goodsService.updateMallGoodsSales(mallGoods);
                            }
                        }
                    }
                }

                try {
                    log.setRemark("订单支付通知计算完成");
                    log.setIsSuccess("1F");
                    logService.add(log);
                } catch (Exception e1) {
                    logger.error("日志添加异常: " + e1.getMessage(), e1);
                }

                // 如果updated是0，那么数据库更新失败
                if (updated == 0) {
                    return WxPayNotifyResponse.fail("更新数据已失效");
                }
            }

            // 支付成功，有团购信息，更新团购信息
            MallGroupon groupon = grouponService.queryByOrderId(order.getId());
            if (groupon != null) {
                MallGrouponRules grouponRules = grouponRulesService.queryById(groupon.getRulesId());

                // 仅当发起者才创建分享图片
                if ("0".equals(groupon.getGrouponId())) {
                    String url = qCodeService.createGrouponShareImage(grouponRules.getGoodsName(),
                            grouponRules.getPicUrl(), groupon);
                    groupon.setShareUrl(url);
                }
                groupon.setPayed(true);
                if (grouponService.updateById(groupon) == 0) {
                    return WxPayNotifyResponse.fail("更新数据已失效");
                }
            }

            // 购买者id
            String userId = order.getUserId();
            String shareId = order.getShareId();

            // 用户购买优惠券商品
            doPayService.doUserCouponGoods(order);
            // 用户购买骑士卡
            doPayService.doUserQsk(order);
            // 用户报名游戏
            doPayService.doUserGame(order);
            // 用户购买活动名额
            doPayService.doUserActivity(order);
            // 升级创客
            doPayService.doUserMaker(order);
            // 升级达人
            doPayService.doUserTalent(order);
            // 订单支付成功处理用户权益逻辑
            doPayService.doUserAllBid(order);

            // TODO 发送邮件和短信通知，这里采用异步发送
            // 订单支付成功以后，会发送短信给用户，以及发送邮件给管理员
            // notifyService.notifyMail("新订单通知", order.toString(·));
            // 这里微信的短信平台对参数长度有限制，所以将订单号只截取后6位
            // notifyService.notifySmsTemplateSync(order.getMobile(),
            // NotifyType.PAY_SUCCEED, new String[]{orderSn.substring(8,
            // 14)});

            // 请依据自己的模版消息配置更改参数
            // String[] parms = new String[]{
            // order.getOrderSn(),
            // order.getOrderPrice().toString(),
            // DateTimeUtil.getDateTimeDisplayString(order.getAddTime()),
            // order.getConsignee(),
            // order.getMobile(),
            // order.getAddress()
            // };

            // notifyService.notifyWxTemplate(result.getOpenid(),
            // NotifyType.PAY_SUCCEED, parms, "pages/index/index?orderId=" +
            // order.getId());
        } catch (Exception e) {
            try {
                log.setRemark("订单支付发生异常: " + e.getMessage());
                log.setIsSuccess("0F");
                logService.add(log);
            } catch (Exception e1) {
                logger.error("日志添加异常: " + e1.getMessage(), e1);
            }
            logger.error("支付订单通知处理异常 :" + e.getMessage(), e);
        }

        return WxPayNotifyResponse.success("处理成功!");
    }

    /**
     * 汇付付款成功或失败回调接口
     * <p>
     * 1. 检测当前订单是否是付款状态; 2. 设置订单付款成功状态相关信息; 3. 响应汇付结果.
     *
     * @param request 请求内容
     * @return 操作结果
     * @throws Exception
     */
    @Transactional(rollbackFor = {Exception.class})
    public Object huifuNotify(HttpServletRequest request) throws Exception {
        MallOrderNotifyLog log = new MallOrderNotifyLog();
        logger.info("---------------------汇付回调进来了--------------------------------");
        // 验签请参data
        String data = request.getParameter("data");
        // 验签请参sign
        String sign = request.getParameter("sign");
        try {

            // 验签标记
            boolean checkSign;
            // 验签请参publicKey
            // 验签请参publicKey
            String publicKey = huifuProperties.getAdpayPublicKey();
            logger.info("验签请参：data=" + data);
            // 验签
            checkSign = AdaPaySign.verifySign(data, sign, publicKey);
            if (checkSign) {
                // 验签成功逻辑
                logger.info("成功返回数据data:" + data);
            } else {
                // 验签失败逻辑
                log.setIsSuccess("0F");
                logService.add(log);
                throw new WxPayException("汇付验签支付失败！");
            }
        } catch (IOException e) {
            e.printStackTrace();
            try {
                log.setErrMsg("汇付通知参数解析异常：" + e.getMessage());
                log.setIsSuccess("0F");
                logService.add(log);
            } catch (Exception e1) {
                logger.error("日志添加异常: " + e1.getMessage(), e1);
            }
            return ResponseUtil.fail(e.getMessage());
        }


        JSONObject obj = JSONObject.parseObject(data);
        // JSONObject dataObject = JSONObject.parseObject(obj.getString("expend"));
        String orderSn = obj.getString("order_no");
        String totalFee = obj.getString("pay_amt");
        String payId = obj.getString("id");
        String result = obj.getString("status");
        log.setOrderSn(orderSn);

        if (!Objects.equals(result, "succeeded")) {
            logger.error(obj);
            throw new Exception("汇付支付失败");
        }

        MallOrder order = orderService.findBySn(orderSn);



        try {
            if (order == null) {
                try {
                    log.setErrMsg("订单不存在 sn=" + orderSn);
                    log.setIsSuccess("0F");
                    logService.add(log);
                } catch (Exception e1) {
                    logger.error("日志添加异常: " + e1.getMessage(), e1);
                }
                return WxPayNotifyResponse.fail("订单不存在 sn=" + orderSn);
            }

            if (!Objects.equals(null, order.getUserId())) {
                log.setUserId(order.getUserId());
                order.setUserId(order.getUserId());
            }

            // 检查这个订单是否已经处理过
            if (OrderUtil.isPayStatus(order) && order.getPayId() != null) {
                try {
                    log.setErrMsg("订单已经处理过");
                    log.setIsSuccess("0F");
                    logService.add(log);
                } catch (Exception e1) {
                    logger.error("日志添加异常: " + e1.getMessage(), e1);
                }
                return WxPayNotifyResponse.success("订单已经处理成功!");
            }


            // 检查支付订单金额
            if (!totalFee.equals(order.getActualPrice().toString())) {
                try {
                    log.setErrMsg(order.getOrderSn() + " : 支付金额不符合 totalFee=" + totalFee);
                    log.setIsSuccess("0F");
                    logService.add(log);
                } catch (Exception e1) {
                    logger.error("日志添加异常: " + e1.getMessage(), e1);
                }
                return WxPayNotifyResponse.fail(order.getOrderSn() + " : 支付金额不符合 totalFee=" + totalFee);
            }



            order.setPayId(payId);
            order.setSource("huifu");
            order.setPayTime(LocalDateTime.now());
            order.setOrderStatus(OrderUtil.STATUS_PAY);
            if (orderService.updateWithOptimisticLocker(order) == 0) {
                logger.info("三方平台开始下单updateWithOptimisticLocker。。。。。。。。。。。。。。" + order.toString());

                // 这里可能存在这样一个问题，用户支付和系统自动取消订单发生在同时
                // 如果数据库首先因为系统自动取消订单而更新了订单状态；
                // 此时用户支付完成回调这里也要更新数据库，而由于乐观锁机制这里的更新会失败
                // 因此，这里会重新读取数据库检查状态是否是订单自动取消，如果是则更新成支付状态。
                order = orderService.findBySn(orderSn);

                int updated = 0;

                // 订单自动取消
                if (OrderUtil.isAutoCancelStatus(order)) {
                    order.setPayId(payId);
                    order.setSource("huifu");
                    order.setPayTime(LocalDateTime.now());
                    order.setOrderStatus(OrderUtil.STATUS_PAY);

                    updated = orderService.updateWithOptimisticLocker(order);

                    // 修改销量
                    List<MallOrderGoods> mallOrderGoodsList = orderGoodsService.queryByOid(order.getId());
                    if (CollectionUtils.isNotEmpty(mallOrderGoodsList)) {
                        for (int i = 0; i < mallOrderGoodsList.size(); i++) {
                            MallOrderGoods mallOrderGoods = mallOrderGoodsList.get(i);
                            if (mallOrderGoods.getNumber() != null) {
                                MallGoods mallGoods = new MallGoods();
                                mallGoods.setId(mallOrderGoods.getGoodsId());
                                mallGoods.setItemsale(Integer.parseInt(mallOrderGoods.getNumber().toString()));
                                goodsService.updateMallGoodsSales(mallGoods);
                            }
                        }
                    }
                }

                try {
                    log.setRemark("订单支付通知计算完成");
                    log.setIsSuccess("1F");
                    logService.add(log);
                } catch (Exception e1) {
                    logger.error("日志添加异常: " + e1.getMessage(), e1);
                }

                // 如果updated是0，那么数据库更新失败
                if (updated == 0) {
                    return WxPayNotifyResponse.fail("更新数据已失效");
                }
            }

            // 判断是口令红包订单就拦截处理，不往下执行了
            if (AccountStatus.BID_45.equals(order.getBid())) {
                try {
                    return wxRedService.processRedPwd(order, payId);
                } catch (Exception ex) {
                    logger.info("三方下单--口令红包--支付出错：" + ex.getMessage(), ex);
                }
            }

            // 判断是-网赚任务-订单就拦截处理，不往下执行了
            if (AccountStatus.BID_47.equals(order.getBid())) {
                try {
                    return wangzhuanTaskService.orderProcess(order, payId);
                } catch (Exception ex) {
                    logger.info("三方下--网赚任务--单支付出错：" + ex.getMessage(), ex);
                }
            }

            // 判断是-网赚任务-上调数量/上调单价-订单就拦截处理，不往下执行了
            if (AccountStatus.BID_48.equals(order.getBid())) {
                try {
                    return wangzhuanTaskService.orderProcessAddNumOrAmount(order,payId);
                }catch (Exception e){
                    logger.info("三方下-- 网赚任务-上调任务单价/任务数量--订单支付出错："+e.getMessage(),e);
                }
            }

            // 判断是-网赚任务-购买推荐、置顶 / 竞价 -订单就拦截处理，不往下执行了
            if (AccountStatus.BID_49.equals(order.getBid())) {
                try {
                    return wangzhuanTaskServiceV2.orderProcessTBR(order,payId);
                }catch (Exception e){
                    logger.info("三方下-- 网赚任务-上调任务单价/任务数量--订单支付出错："+e.getMessage(),e);
                }
            }

            // 支付成功，有团购信息，更新团购信息
            MallGroupon groupon = grouponService.queryByOrderId(order.getId());
            if (groupon != null) {
                MallGrouponRules grouponRules = grouponRulesService.queryById(groupon.getRulesId());

                // 仅当发起者才创建分享图片
                if ("0".equals(groupon.getGrouponId())) {
                    String url = qCodeService.createGrouponShareImage(grouponRules.getGoodsName(),
                            grouponRules.getPicUrl(), groupon);
                    groupon.setShareUrl(url);
                }
                groupon.setPayed(true);
                if (grouponService.updateById(groupon) == 0) {
                    return WxPayNotifyResponse.fail("更新数据已失效");
                }
            }

            logger.info("三方平台开始下单。。。。。。。。。。。。。。");
            logger.info("三方平台开始下单。。。。。。。。。。。。。。" + order.toString());
            logger.info("三方平台开始下单。。。。。。。。。。。。。。" + order.getHuid());
            // 防止重复下单
            try {
                logger.info("开始下单：--------------------------------------");

                // 三方下单支付回调
                if (Objects.equals(order.getHuid(), "fuluGoods")) {
                    payFuLuGoodsOrder(order, "huifu");
                } else if (Objects.equals(order.getHuid(), "zmGoods")) {
                    payZMGoodsOrder(order, "huifu");
                } else if (Objects.equals(order.getHuid(), "lsxdGoods")) {
                    payLSXDGoodsOrder(order, "huifu");
                } else if (Objects.equals(order.getHuid(), "cost")) {
                    payCOSTGoodsOrder(order, "huifu");
                }
            } catch (Exception e) {
                logger.info("三方下单支付完成。。。。。。。。。");
            }

            // 购买者id
            String userId = order.getUserId();
            // 分享人id
            String shareId = order.getShareId();

            // 订单支付成功处理用户权益逻辑
            doPayService.doUserAllBid(order);

        } catch (Exception e3) {
            logger.error("汇付付款成功或失败回调接口处理出现异常：" + e3.getMessage(), e3);
            e3.printStackTrace();
        }

        return ResponseUtil.ok("处理成功!");
    }

    // public static void main(String[] args) {
    // String number = "1232131231232132121";
    //
    // String qrcode = "1231312322";
    // ImgUtil.pressImage(FileUtil.file("C:\\Users\\Administrator.SC-202011302240\\Desktop\\红包码\\red.png"),
    // // 源图像文件
    // FileUtil.file("C:\\Users\\Administrator.SC-202011302240\\Desktop\\红包码\\" + qrcode + ".png"), //
    // 目标图像文件，保存生成图片地址
    // ImgUtil.read(FileUtil
    // .file("C:\\Users\\Administrator.SC-202011302240\\Desktop\\红包码\\" + "pre.png")),
    // -8, // x坐标修正值。 默认在中间，偏移量相对于中间偏移
    // 30, // y坐标修正值。 默认在中间，偏移量相对于中间偏移
    // 1f);
    //
    // String saveUrl = "C:\\Users\\Administrator.SC-202011302240\\Desktop\\红包码\\" + qrcode + ".png";
    // ImgUtil.pressText(FileUtil.file("C:\\Users\\Administrator.SC-202011302240\\Desktop\\红包码\\red.png"),
    // /// 源图像文件，就是需要添加文字的源文件
    // FileUtil.file(saveUrl), // 目标图像文件，就是生成添加文字存放地址
    // qrcode, Color.BLACK, // 文字
    // new Font("黑体", Font.BOLD, 38), // 字体
    // -5, // x坐标修正值。 默认在中间，偏移量相对于中间偏移
    // 383, // y坐标修正值。 默认在中间，偏移量相对于中间偏移
    // 1f);// 透明度：alpha 必须是范围 [0.0, 1.0] 之内（包含边界值）的一个浮点数字
    //
    // }

    /**
     * 微信付款成功或失败回调接口
     * <p>
     * 1. 检测当前订单是否是付款状态; 2. 设置订单付款成功状态相关信息; 3. 响应微信商户平台.
     *
     * @param request 请求内容
     * @param response 响应内容
     * @return 操作结果
     */
//    @Transactional(rollbackFor = {Exception.class})
    public Object payHsNotify(HttpServletRequest request, HttpServletResponse response) {
        MallOrderNotifyLog log = new MallOrderNotifyLog();
        try {
            int h = 1;
            logger.info("___________第"+h+"次回调");

            String xmlResult = null;
            try {

                xmlResult = IOUtils.toString(request.getInputStream(), request.getCharacterEncoding());
                log.setWxParams(xmlResult);

            } catch (IOException e) {
                e.printStackTrace();
                try {
                    log.setErrMsg("微信通知参数解析异常：" + e.getMessage());
                    log.setIsSuccess("0F");
                    logService.add(log);
                } catch (Exception e1) {
                    logger.error("日志添加异常: " + e1.getMessage(), e1);
                }
                return WxPayNotifyResponse.fail(e.getMessage());
            }

            WxPayOrderNotifyResult result = null;
            try {
                result = hsWxPayService.parseOrderNotifyResult(xmlResult);

                if (!WxPayConstants.ResultCode.SUCCESS.equals(result.getResultCode())) {
                    logger.error(xmlResult);
                    throw new WxPayException("微信通知支付失败！");
                }
                if (!WxPayConstants.ResultCode.SUCCESS.equals(result.getReturnCode())) {
                    logger.error(xmlResult);
                    throw new WxPayException("微信通知支付失败！");
                }
            } catch (WxPayException e) {
                try {
                    log.setErrMsg("微信通知参数解析异常：" + e.getMessage());
                    log.setIsSuccess("0F");
                    logService.add(log);
                } catch (Exception e1) {
                    logger.error("日志添加异常: " + e1.getMessage(), e1);
                }
                e.printStackTrace();
                return WxPayNotifyResponse.fail(e.getMessage());
            }

            logger.info("处理腾讯支付平台的订单支付");
            logger.info(result);

            String orderSn = result.getOutTradeNo();
            String payId = result.getTransactionId();
            log.setOrderSn(orderSn);

            // 分转化成元
            String totalFee = BaseWxPayResult.fenToYuan(result.getTotalFee());
            MallOrder order = orderService.findBySn(orderSn);

            if(order == null){
                QueryWrapper<MallPurchaseOrder> orderQueryWrapper = new QueryWrapper<>();
                orderQueryWrapper.eq("order_sn", orderSn);
                MallPurchaseOrder order1 = mallPurchaseOrderService.getOne(orderQueryWrapper);

                if (order1 == null) {
                    try {
                        log.setErrMsg("订单不存在 sn=" + orderSn);
                        log.setIsSuccess("0F");
                        logService.add(log);
                    } catch (Exception e1) {
                        logger.error("日志添加异常: " + e1.getMessage(), e1);
                    }
                    return WxPayNotifyResponse.fail("订单不存在 sn=" + orderSn);
                }

                logger.info("----->商户下单回调处理" + JSON.toJSONString(order1));
                if (!Objects.equals(order1.getUserId(), null)) {
                    log.setUserId(order1.getUserId());
                }

                // 检查这个订单是否已经处理过
                if (OrderUtil.isPayStatus(order1) && order1.getPayId() != null) {
                    try {
                        log.setErrMsg("订单已经处理过");
                        log.setIsSuccess("0F");
                        logService.add(log);
                    } catch (Exception e1) {
                        logger.error("日志添加异常: " + e1.getMessage(), e1);
                    }
                    return WxPayNotifyResponse.success("订单已经处理成功!");
                }

                // 检查支付订单金额
                if (!totalFee.equals(order1.getActualPrice().toString())) {
                    try {
                        log.setErrMsg(order1.getOrderSn() + " : 支付金额不符合 totalFee=" + totalFee);
                        log.setIsSuccess("0F");
                        logService.add(log);
                    } catch (Exception e1) {
                        logger.error("日志添加异常: " + e1.getMessage(), e1);
                    }
                    return WxPayNotifyResponse.fail(order1.getOrderSn() + " : 支付金额不符合 totalFee=" + totalFee);
                }

                order1.setPayId(payId);
                order1.setPayTime(LocalDateTime.now());
                order1.setOrderStatus(OrderUtil.STATUS_PAY.intValue());
                mallPurchaseOrderService.updateById(order1);
                doPayService.doMerchangOrderWithActive(order1);
                String succes = WxPayNotifyResponse.success("处理成功!");
                logger.info(succes);
                return succes;
            }

            // 检查这个订单是否已经处理过
            logger.info("___________________________"+order.getOrderStatus()+" , "+order.getPayId());
            if ((OrderUtil.isPayStatus(order) || order.getOrderStatus() == 500 )) { //&& order.getPayId() != null
                try {
                    log.setErrMsg("订单已经处理过");
                    log.setIsSuccess("0F");
                    logService.add(log);
                } catch (Exception e1) {
                    logger.error("日志添加异常: " + e1.getMessage(), e1);
                }
                return WxPayNotifyResponse.success("订单已经处理成功!");
            }

            if (!Objects.equals(null, order.getUserId())) {
                log.setUserId(order.getUserId());
                order.setUserId(order.getUserId());
            }

            // 检查支付订单金额
            if (!totalFee.equals(order.getActualPrice().toString())) {
                try {
                    log.setErrMsg(order.getOrderSn() + " : 支付金额不符合 totalFee=" + totalFee);
                    log.setIsSuccess("0F");
                    logService.add(log);
                } catch (Exception e1) {
                    logger.error("日志添加异常: " + e1.getMessage(), e1);
                }
                return WxPayNotifyResponse.fail(order.getOrderSn() + " : 支付金额不符合 totalFee=" + totalFee);
            }

            // this.calFenXiao(order,true,true);
            // 如果订单是购买会员产品，那么成为会员
            // userService.becomeVip(order);


            order.setPayId(payId);
            order.setPayTime(LocalDateTime.now());
            order.setOrderStatus(OrderUtil.STATUS_PAY);
            //团购2.0要多加个状态显示

            if (Objects.equals(order.getBid(), AccountStatus.BID_36)) {
                logger.info("回调bid值-----" + order.getBid());
                order.setOrderThreeStatus(OrderUtil.STATUS_CREATE_LOADING);
            }

            // 支付成功，有团购信息，更新团购信息
            MallGroupon groupon = grouponService.queryByOrderId(order.getParentOrderOn());
            String groupId=null;
            String  rulesId=null;
            if (groupon != null) {
                //MallGrouponRules grouponRules = grouponRulesService.queryByIdV2(groupon.getRulesId());

                // 仅当发起者才创建分享图片
//                if ("0".equals(groupon.getGrouponId())) {
//                    String url = qCodeService.createGrouponShareImage(grouponRules.getGoodsName(),
//                                    grouponRules.getPicUrl(), groupon);
//                    groupon.setShareUrl(url);
//                }
                groupon.setPayed(true);
                if (Objects.equals(groupon.getGrouponId(), "0")) {
                    groupId=groupon.getId();
                }else {
                    groupId=groupon.getGrouponId();
                }
                rulesId=groupon.getRulesId();
                if (grouponService.updateById(groupon) == 0) {
                    return WxPayNotifyResponse.fail("更新数据已失效");
                }

                //开团记录每次添加参团人数+1
                MallGroupon mallGroupon=grouponService.queryById(groupId);
                if (!Objects.equals(null, mallGroupon)) {
                    mallGroupon.setJoinNum(mallGroupon.getJoinNum()+1);
                    logger.info("__________________1"+mallGroupon.getJoinNum());
                    grouponService.updateById(mallGroupon);
                }
                MallGroupon openGroup=grouponService.queryById(groupId);
                List<MallOrder> startOrder  = orderService.selectList(openGroup.getOrderId(),0);
                if (!Objects.equals(null, openGroup.getOrderId())) {
                    for (MallOrder mallOrder : startOrder){
                        MallOrder openOrder=new MallOrder();
                        openOrder.setId(mallOrder.getId());
//                       openOrder.setUserId(openGroup.getUserId());
                        MallGrouponRules rules=grouponRulesService.queryById(rulesId);
                        if (!Objects.equals(null, rules.getDuration())) {
                            order.setDrawTime(openGroup.getAddTime().plusHours(rules.getDuration()));
                            openOrder.setDrawTime(openGroup.getAddTime().plusHours(rules.getDuration()));
                            if (!Objects.equals("00", openGroup.getOrderId())) {
                                orderService.updateByPrimaryKeySelective(openOrder);
                            }
                        }
                    }
                }
            }
            int update = 0;
            MallOrder orderV2 = new MallOrder();
            orderV2.setPayId(payId);
            orderV2.setPayTime(LocalDateTime.now());
            orderV2.setOrderStatus(OrderUtil.STATUS_PAY);
            orderV2.setId(order.getId());
            orderV2.setOrderThreeStatus(order.getOrderThreeStatus());
            update = orderService.updateByPrimaryKeySelective(orderV2);
            if (update == 0) {
                logger.info("三方平台开始下单updateWithOptimisticLocker。。。。。。。。。。。。。。" + order.toString());

                // 这里可能存在这样一个问题，用户支付和系统自动取消订单发生在同时
                // 如果数据库首先因为系统自动取消订单而更新了订单状态；
                // 此时用户支付完成回调这里也要更新数据库，而由于乐观锁机制这里的更新会失败
                // 因此，这里会重新读取数据库检查状态是否是订单自动取消，如果是则更新成支付状态。
                order = orderService.findBySn(orderSn);
                int updated = 0;
                // 订单自动取消
                if (OrderUtil.isAutoCancelStatus(order)) {
                    order.setPayId(payId);
                    order.setPayTime(LocalDateTime.now());
                    order.setOrderStatus(OrderUtil.STATUS_PAY);

                    updated = orderService.updateWithOptimisticLocker(order);

                    // 修改销量
                    List<MallOrderGoods> mallOrderGoodsList = orderGoodsService.queryByOid(order.getId());
                    for (int i = 0; i < mallOrderGoodsList.size(); i++) {
                        MallOrderGoods mallOrderGoods = mallOrderGoodsList.get(i);
                        if (mallOrderGoods.getNumber() != null) {
                            MallGoods mallGoods = new MallGoods();
                            mallGoods.setId(mallOrderGoods.getGoodsId());
                            mallGoods.setItemsale(Integer.parseInt(mallOrderGoods.getNumber().toString()));
                            goodsService.updateMallGoodsSales(mallGoods);
                        }
                    }
                }

                try {
                    log.setRemark("订单支付通知计算完成");
                    log.setIsSuccess("1F");
                    logService.add(log);
                } catch (Exception e1) {
                    logger.error("日志添加异常: " + e1.getMessage(), e1);
                }

                // 如果updated是0，那么数据库更新失败
                if (updated == 0) {
                    return WxPayNotifyResponse.fail("更新数据已失效");
                }
            }



            //给开团者和参团的订单记录都插入一个这个团的过期时间,这样避免订单列表显示的时候再做其他很多查询
            //这里要改 拿出来的openGroup.getOrderId()其实是MallOrder的父级订单id 需要先判断一下如果是支付模式2 查出来的会又两条 拿两条都要改
            MallGroupon openGroup = grouponService.queryById(groupId);
//            MallOrder startOrder = orderService.findById(openGroup.getOrderId());
            List<MallOrder> startOrder  = orderService.selectList(openGroup.getOrderId(),0);
            if (!Objects.equals(null, openGroup.getOrderId())) {
                MallGrouponRules rules = grouponRulesService.queryById(rulesId);
                if (!Objects.equals(null, rules.getDuration())) {
                    for (MallOrder mallOrder : startOrder){
                        MallOrder openOrder = new MallOrder();
                        openOrder.setId(mallOrder.getId());
//                        openOrder.setUserId(openGroup.getUserId()); 不确定是否可以去掉 但是 分表分库的情况下不能修改关联键
                        if (!Objects.equals(null, mallOrder.getPayTime())) {
                            order.setDrawTime(mallOrder.getPayTime().plusHours(rules.getDuration()));
                            openOrder.setDrawTime(mallOrder.getPayTime().plusHours(rules.getDuration()));
                        } else {
                            order.setDrawTime(openGroup.getAddTime().plusHours(rules.getDuration()));
                            openOrder.setDrawTime(openGroup.getAddTime().plusHours(rules.getDuration()));
                        }

                        if (!Objects.equals("00", mallOrder.getId())) {
                            orderService.updateByPrimaryKeySelective(openOrder);
                        }
                    }
                }
            }

            logger.info("三方平台开始下单。。。。。。。。。。。。。。");
            logger.info("三方平台开始下单。。。。。。。。。。。。。。" + order.toString());
            logger.info("三方平台开始下单。。。。。。。。。。。。。。" + order.getHuid());
            // 防止重复下单
            try {
                logger.info("开始下单：--------------------------------------");

                // 三方下单支付回调
                if (Objects.equals(order.getHuid(), "fuluGoods")) {
                    payFuLuGoodsOrder(order, "wx");
                } else if (Objects.equals(order.getHuid(), "zmGoods")) {
                    payZMGoodsOrder(order, "wx");
                } else if (Objects.equals(order.getHuid(), "lsxdGoods")) {
                    payLSXDGoodsOrder(order, "wx");
                } else if (Objects.equals(order.getHuid(), "cost")) {
                    payCOSTGoodsOrder(order, "wx");
                }
            } catch (Exception e) {
                logger.info("三方下单支付完成。。。。。。。。。");
            }

            // 购买者id
            String userId = order.getUserId();
            // 分享人id
            String shareId = order.getShareId();
            JSONObject obj = null;

            String virtual_good = null;
            String goodsId = null;
            String flagZy = null;
            /** 用户购买优惠券商品 **/
            // 通过orderId 查询订单商品
            List<MallOrderGoods> orderGoodsList = orderGoodsService.queryByOid(order.getId());
            if (orderGoodsList.size() > 0) {
                for (MallOrderGoods mallOrderGoods : orderGoodsList) {
                    String productId = mallOrderGoods.getProductId();
                    try {
                        obj = goodsService.queryGoodsByProductId(productId);
                        if (!Objects.equals(null, obj)) {
                            logger.info("--------修改自营商品销量和库存-----------------");
                            // 在开放平台goods表添加销量和product表减去库存
                            // 商品属于自营商品
                            if (Objects.equals("0", obj.get("virtualGood"))
                                    || 0 == Integer.parseInt(String.valueOf(obj.get("virtualGood")))) {
                                Integer buyNum = Integer.parseInt(String.valueOf(mallOrderGoods.getNumber()));
                                if (!goodsService.updateZyOpenGoods(productId, buyNum)) {
                                    logger.info("修改自营商品销量和库存失败");
                                }
                            }
                        }
                    } catch (Exception e) {
                        logger.info("远程修改库存和销量出现异常!");
                        e.printStackTrace();
                    }

                    String id = mallOrderGoods.getId();
                    goodsId = mallOrderGoods.getGoodsId();
                    /** (1)通过goodsId 查询 优惠券商品信息(test2库商品数据) **/
                    try {
                        logger.info("----------远程调用open项目接口 getCouponGoodsDetail----------");
                        // JSONObject object = goodsService.queryCouponGoodsById(goodsId);
                        logger.info("obj=" + obj);
                        if (obj != null) {
                            virtual_good = obj.getString("virtualGood");

                            // 判断 商品是否属于 优惠券商品
                            if ("2".equals(virtual_good)) {
                                // 若商品属于 优惠券商品,则srcGoodId 为couponId
                                String srcGoodId = mallOrderGoods.getSrcGoodId();// 优惠券id

                                // 查询可用的优惠券券码list
                                MallGoodsCouponCode mallGoodsCouponCode = new MallGoodsCouponCode();
                                mallGoodsCouponCode.setCouponId(srcGoodId);
                                mallGoodsCouponCode.setSaleStatus(false);
                                mallGoodsCouponCode.setDeleted(false);
                                List<MallGoodsCouponCode> mallGoodsCouponCodeList =
                                        mallGoodsCouponCodeService.selectMallGoodsCouponCodeList(
                                                mallGoodsCouponCode, null, null, null, null);
                                /** (1.2)随机分配用户 优惠券券码 **/
                                JSONObject couponObject = null;
                                if (mallGoodsCouponCodeList.size() > 0) {
                                    MallGoodsCouponCode couponCode = mallGoodsCouponCodeList.get(0);
                                    couponCode.setUserId(userId);
                                    couponCode.setSaleStatus(true);
                                    mallGoodsCouponCodeService.updateMallGoodsCouponCodeV2(couponCode);

                                    /** (1.3)商品订单加入 优惠券券码 **/
                                    MallOrderGoods orderGoods = new MallOrderGoods();
                                    orderGoods.setId(id);
                                    orderGoods.setCardNumber(couponCode.getCouponCard()); // 卡号
                                    orderGoods.setCardPwd(couponCode.getCouponCode()); // 卡密

                                    /** (1.3.2)查询优惠券信息 **/
                                    try {
                                        logger.info("----------远程调用open项目接口 getCouponDetail----------");
                                        couponObject = goodsService.queryCouponById(srcGoodId);
                                        if (couponObject != null) {
                                            String endTime = couponObject.getString("endTime");
                                            String shortUrl = couponObject.getString("shortUrl");
                                            if (StringUtils.isNotEmpty(endTime)) {
                                                orderGoods.setCardDeadline(endTime); // 卡券有效期
                                            }
                                            if (StringUtils.isNotEmpty(shortUrl)) {
                                                orderGoods.setGoodsShortUrl(shortUrl); // 卡券短链接 (推广链接)
                                            }
                                        }
                                    } catch (Exception e) {
                                        logger.info("远程调用查询优惠券信息接口失败！");
                                    }

                                    if (com.graphai.mall.db.util.StringHelper.isNotNullAndEmpty(order.getUserId())) {
                                        orderGoods.setUserId(order.getUserId());
                                    }
                                    orderGoodsService.updateById(orderGoods);
                                    logger.info("分配券码的商品信息------ " + JacksonUtils.bean2Jsn(orderGoods));
                                }

                                /** (1.4)优惠券 剩余库存减少(远程调用接口，修改test2库数据) **/
                                if (couponObject != null) {
                                    Short number = mallOrderGoods.getNumber(); // 订单商品数量
                                    String couponSurplus = couponObject.getString("surplus");
                                    if (StringUtils.isNotEmpty(couponSurplus)) {
                                        Short surplus = Short.valueOf(couponSurplus);
                                        Integer lastSurplus = 0;
                                        if (surplus.compareTo(number) > -1) {
                                            lastSurplus = surplus - number; // 剩余数量
                                        }
                                        /** 修改优惠券库存 **/
                                        try {
                                            logger.info("----------远程调用open项目接口 updateCoupon----------");
                                            goodsService.updateCouponById(srcGoodId, lastSurplus, goodsId);
                                        } catch (Exception e) {
                                            logger.info("远程调用修改优惠券信息接口失败！");
                                        }
                                    } else {
                                        logger.info("优惠券库存不存在！");
                                    }
                                }
                            }


                            if (Objects.equals(order.getBid(),AccountStatus.BID_36)) {
                                doPayService.openGroup(rulesId, groupId);   //团购2.0开奖逻辑
                            }



                            // 调用队列返现方法
                            flagZy = linkUp(goodsId, mallOrderGoods, shareId, userId, order);
                            logger.info("调用队列返现方法flagZy=" + flagZy);
                            if ("0".equals(flagZy)) {
                                logger.info("队列金额没有返回给用户");
                            } else if ("1".equals(flagZy)) {
                                logger.info("队列金额已经返回给用户");
                            } else {
                                logger.info("调用队列返现方法有误，请检查。");
                                return WxPayNotifyResponse.fail("调用队列返现方法有误，请检查。");
                            }

                            // 自营商品和优惠券商品分润模式
                            logger.info("virtual_good=" + virtual_good);


                        }
                    } catch (Exception e) {
                        logger.info("远程调用商品信息接口失败！",e);
                    }

                }
            }


            if (("2".equals(virtual_good) || "0".equals(virtual_good)) && !Objects.equals("36", order.getBid())) {
                logger.info("自营商品和优惠券商品分润模式");
                /** 创建用户预估分润 **/
                Map<String, Object> map = new HashMap<>();
                map.put("userId", userId);
                map.put("bid", AccountStatus.BID_27);
                map.put("totalMoney", String.valueOf(order.getPubSharePreFee())); // pub_share_pre_fee
                // 付款预估收入=付款金额*提成。指买家付款金额为基数，预估您可能获得的收入。因买家退款等原因，可能与结算预估收入不一致(唯一佣金金额)
                map.put("rechargeType", CommonConstant.TASK_REWARD_CASH);
                map.put("orderId", order.getId());
                map.put("orderSn", order.getOrderSn());
                map.put("shareId", shareId);
                map.put("goodsId", goodsId);
                map.put("flagZy", flagZy); // flagZy=0:队列金额没有返回给用户 flagZy=1:队列金额已经返回给用户
                Object zyObj = AccountZYUtils.preChargeZy(map);
                logger.info("自营商品分润结果：" + zyObj);
            }

            // 用户购买骑士卡
            doPayService.doUserQsk(order);
            // 用户报名游戏
            doPayService.doUserGame(order);



            logger.info("订单信息。。。。。。。。。。。。。。" + order.toString());

            // TODO 发送邮件和短信通知，这里采用异步发送
            // 订单支付成功以后，会发送短信给用户，以及发送邮件给管理员
            // notifyService.notifyMail("新订单通知", order.toString());
            // 这里微信的短信平台对参数长度有限制，所以将订单号只截取后6位
            // notifyService.notifySmsTemplateSync(order.getMobile(),
            // NotifyType.PAY_SUCCEED, new String[]{orderSn.substring(8,
            // 14)});

            // 请依据自己的模版消息配置更改参数
            // String[] parms = new String[]{
            // order.getOrderSn(),
            // order.getOrderPrice().toString(),
            // DateTimeUtil.getDateTimeDisplayString(order.getAddTime()),
            // order.getConsignee(),
            // order.getMobile(),
            // order.getAddress()
            // };

            // notifyService.notifyWxTemplate(result.getOpenid(),
            // NotifyType.PAY_SUCCEED, parms, "pages/index/index?orderId=" +
            // order.getId());
        } catch (Exception e) {
            try {
                log.setRemark("订单支付发生异常: " + e.getMessage());
                log.setIsSuccess("0F");
                logService.add(log);
            } catch (Exception e1) {
                logger.error("日志添加异常: " + e1.getMessage(), e1);
            }
            logger.error("支付订单通知处理异常 :" + e.getMessage(), e);
        }

        return WxPayNotifyResponse.success("处理成功!");
    }

    /**
     * 1、自营商品队列方法
     *
     * @param goodsId 订单id
     * @param mallOrderGoods 商品信息
     * @param shareId 分享者ID
     * @param userId 用户id
     * @param order 订单对象
     * @return flagZY // 0-不走队列 1-走队列 2-信息有误
     * @throws Exception
     */
    private String linkUp(String goodsId, MallOrderGoods mallOrderGoods, String shareId, String userId, MallOrder order)
            throws Exception {

        /** 支付成功队列商品返现 **/
        String flagZY = "0"; // 0-不走队列 1-走队列 2-信息有误

        if (true) {
            /** (2)通过goodsId 查询商品信息(test2库商品数据) **/
            JSONObject goodObject = goodsService.queryGoodsById(goodsId);
            logger.info("商品信息：--" + goodObject);
            if (Objects.equals(null, goodObject)) {
                // return ResponseUtil.fail(507, "商品信息有误!");
                logger.info("1、商品信息有误");
                flagZY = "2";
                return flagZY;
            }
            String cashbackType = goodObject.getString("cashbackType"); // 是否返现商品
            // String cashbackRate = goodObject.getString("cashbackRate"); // 返现率 默认0%
            Integer shareAccelerate = goodObject.getInteger("shareAccelerate"); // 是否开启分享加速
            Date cashbackEffective = goodObject.getDate("cashbackEffective"); // 队列开始有效期
            Date cashbackEndTime = goodObject.getDate("cashbackEndTime"); // 队列开始有效期
            Integer cashbackLength = goodObject.getInteger("cashbackLength"); // 队列长度

            BigDecimal userFee = BigDecimal.ZERO;
            BigDecimal shareFee = BigDecimal.ZERO;
            Map<String, String> resuMap = null;

            // 商品返现
            BigDecimal totalCashback = new BigDecimal(order.getPubSharePreFee()); // 返现金额=售价-出厂价

            if (StringUtils.isNotEmpty(cashbackType)) {
                // 若商品的cashbackType不为空、cashbackRate大于0,则属于队列返现商品、是否开启分享加速
                // if (!Objects.equals(null, totalCashback) && totalCashback.compareTo(new BigDecimal(0)) == 1) {

                // Date effectiveDate = null; // 返现有效日期
                // Short number = mallOrderGoods.getNumber(); // 用户购买商品数量
                // BigDecimal unitCashback = mallOrderGoods.getUnitCashback(); // 单件商品返现金额
                // totalCashback = unitCashback.multiply(new BigDecimal(number));

                MallOrderGoodsVo orderGoods = new MallOrderGoodsVo();
                orderGoods.setGoodsId(goodsId);
                // orderGoods.setProductId(productId);
                orderGoods.setSurplusCashback(new BigDecimal(0));
                orderGoods.setPage(0);
                orderGoods.setLimit(50);
                // 成功返现人数
                int count = orderGoodsService.selectCashbackCount(orderGoods);
                List<MallOrderGoodsVo> mallOrderGoodsList = orderGoodsService.selectCashbackListByGoodsId(orderGoods);
                logger.info("返现列表： " + JSONUtil.toJsonStr(mallOrderGoodsList));
                if (CollectionUtils.isNotEmpty(mallOrderGoodsList)) {

                    if (Objects.equals(cashbackEffective, null)
                            || (ObjectUtils.allNotNull(cashbackEffective, cashbackEndTime)
                            && !DateUtilTwo.isMoreThanCurrentDate(cashbackEffective)
                            && DateUtilTwo.isMoreThanCurrentDate(cashbackEndTime))) {
                        // 队列若有长度限制则截取
                        Integer realLength = null; // 真正队列长度
                        if (cashbackLength >= count) {
                            realLength = cashbackLength - count;
                        }
                        if (com.graphai.mall.db.util.StringHelper.isNotNullAndEmpty(realLength)
                                && mallOrderGoodsList.size() > realLength) {
                            mallOrderGoodsList = mallOrderGoodsList.subList(0, realLength);
                        }

                        // 获取队列中的用户id
                        List<String> usersList = mallOrderGoodsList.stream().map(MallOrderGoodsVo::getUserId)
                                .collect(Collectors.toList());
                        if (totalCashback.compareTo(new BigDecimal(0)) == 1) {
                            resuMap = AccountZYUtils.getProfitZy1OnlyUser(userId, AccountStatus.BID_27, totalCashback,
                                    MallUserProfitNewEnum.PROFIT_TYPE_2.strCodeInt());
                        } else {
                            flagZY = "0";
                            // return flagZY;
                        }
                        // 分享者若存在队列中则分享和返现都分给分享者，在队列外的话只有分享，返现给到队列中的用户
                        if (com.graphai.mall.db.util.StringHelper.isNotNullAndEmpty(shareId)
                                && usersList.contains(shareId) && Objects.equals(1, shareAccelerate)) {
                            orderGoods.setUserId(shareId);
                            List<MallOrderGoodsVo> orderGoodsVosList =
                                    orderGoodsService.selectCashbackListByGoodsId(orderGoods);
                            if (CollectionUtils.isNotEmpty(orderGoodsVosList)) {
                                BigDecimal cashbackAmount = orderGoodsVosList.get(0).getCashbackAmount(); // 已返现金额
                                BigDecimal surplusCashback = orderGoodsVosList.get(0).getSurplusCashback(); // 剩余返现金额
                                BigDecimal amout = new BigDecimal(0); // 用户返现金额
                                // 比较 '剩余返现金额' 和 '每次返现金额' 大小,确保最后金额全部返现
                                // 用户账户金额增加
                                // Map<String, Object> paramsMap = new HashMap<>();
                                // paramsMap.put("userId", userId);
                                // paramsMap.put("rechargeId", shareId);
                                // paramsMap.put("shareId", shareId);
                                // paramsMap.put("bid", AccountStatus.BID_27);
                                // paramsMap.put("totalMoney", totalCashback);
                                // paramsMap.put("rechargeType", CommonConstant.TASK_REWARD_CASH);
                                // paramsMap.put("orderId", order.getId());
                                // paramsMap.put("goodsId", orderGoodsVosList.get(0).getGoodsId());

                                // 返现、分享赚都给分享者
                                // userFee = AccountZYUtils.preChargeZy(paramsMap);

                                if (Objects.equals("0000", resuMap.get("code"))) {
                                    userFee = new BigDecimal(resuMap.get("commissionReward"));
                                    shareFee = new BigDecimal(resuMap.get("shareReward"));
                                    Map<String, Object> paramsMap = new HashMap<>();
                                    paramsMap.put("orderId", order.getId());
                                    paramsMap.put("buyerId", userId);
                                    paramsMap.put("goodsId", goodsId);
                                    paramsMap.put("dType", AccountStatus.DISTRIBUTION_TYPE_00); // 直接分润

                                    if (userFee.compareTo(new BigDecimal(0)) == 1) {
                                        paramsMap.put("note", "用户返佣"); // 备注
                                        paramsMap.put("recharType", "1"); // 1 用户返
                                        // 用户预充值
                                        accountService.accountTradePrechargeZy(shareId, userFee,
                                                CommonConstant.TASK_REWARD_CASH, AccountStatus.BID_27,
                                                paramsMap, null);
                                    }

                                    if (shareFee.compareTo(new BigDecimal(0)) == 1) {
                                        paramsMap.put("note", "用户分享赚"); // 备注
                                        paramsMap.put("recharType", "2"); // 2 分享赚
                                        // 用户分享赚预充值
                                        accountService.accountTradePrechargeZy(shareId, shareFee,
                                                CommonConstant.TASK_REWARD_CASH, AccountStatus.BID_27,
                                                paramsMap, null);
                                    }
                                    flagZY = "1";
                                }



                                if (userFee.compareTo(surplusCashback) == 1) {
                                    amout = surplusCashback;
                                    cashbackAmount = cashbackAmount.add(surplusCashback);
                                    surplusCashback = new BigDecimal(0);
                                } else {
                                    amout = userFee;
                                    cashbackAmount = cashbackAmount.add(amout);
                                    surplusCashback = surplusCashback.subtract(amout);
                                }
                                MallOrderGoods updateOrderGoods = new MallOrderGoods();
                                updateOrderGoods.setId(orderGoodsVosList.get(0).getId());
                                updateOrderGoods.setCashbackAmount(cashbackAmount);
                                updateOrderGoods.setSurplusCashback(surplusCashback);
                                orderGoodsService.updateById(updateOrderGoods);

                                totalCashback = userFee.subtract(amout);
                            }

                        }

                        // 在返现有效期内返现或无有效期返现给队列中的用户
                        else {

                            // 计算下单时间加上有效天数的日期时间
                            // if (!Objects.equals(cashbackEffective, null) && cashbackEffective > 0) {
                            // effectiveDate = DateUtilTwo.addDay(
                            // mallOrderGoodsList.get(0).getPayTime(),
                            // cashbackEffective);
                            //
                            // if (!DateUtilTwo.isMoreThanCurrentDate(effectiveDate)) {
                            // logger.info("------------队列已过期-----------!");
                            // }
                            // }

                            // 算出金额
                            // userFee = AccountZYUtils.preChargeZy(paramsMap);

                            for (MallOrderGoodsVo mallOrderGoodsVo : mallOrderGoodsList) {

                                BigDecimal cashbackAmount = mallOrderGoodsVo.getCashbackAmount(); // 已返现金额
                                BigDecimal surplusCashback = mallOrderGoodsVo.getSurplusCashback(); // 剩余返现金额
                                BigDecimal amout = new BigDecimal(0); // 用户返现金额
                                // 比较 '剩余返现金额' 和 '每次返现金额' 大小,确保最后金额全部返现
                                // 用户账户金额增加
                                // Map<String, Object> paramsMap = new HashMap<>();
                                // if (com.graphai.mall.db.util.StringHelper.isNotNullAndEmpty(shareId)) {
                                // // todo若此订单是分享下单的，分享者在队列外则会有分享赚的钱,返现的钱分给队列里面的用户
                                // paramsMap.put("shareId", shareId);
                                // } else {
                                // paramsMap.put("shareId", "");
                                // }
                                // paramsMap.put("userId", userId);
                                // paramsMap.put("rechargeId", mallOrderGoodsVo.getUserId());
                                // paramsMap.put("bid", AccountStatus.BID_27);
                                // paramsMap.put("totalMoney", totalCashback);
                                // paramsMap.put("rechargeType", CommonConstant.TASK_REWARD_CASH);
                                // paramsMap.put("orderId", order.getId());
                                // paramsMap.put("goodsId", mallOrderGoodsVo.getGoodsId());

                                if (Objects.equals("0000", resuMap.get("code"))) {
                                    userFee = new BigDecimal(resuMap.get("commissionReward"));
                                    Map<String, Object> paramsMap = new HashMap<>();
                                    paramsMap.put("orderId", order.getId());
                                    paramsMap.put("buyerId", userId);
                                    paramsMap.put("goodsId", goodsId);
                                    paramsMap.put("dType", AccountStatus.DISTRIBUTION_TYPE_00); // 直接分润

                                    if (userFee.compareTo(new BigDecimal(0)) == 1) {
                                        paramsMap.put("note", "用户返佣"); // 备注
                                        paramsMap.put("recharType", "1"); // 1 用户返
                                        // 用户预充值
                                        accountService.accountTradePrechargeZy(mallOrderGoodsVo.getUserId(), userFee,
                                                CommonConstant.TASK_REWARD_CASH, AccountStatus.BID_27,
                                                paramsMap, null);
                                    }
                                    flagZY = "1";
                                }



                                if (userFee.compareTo(surplusCashback) == 1) {
                                    amout = surplusCashback;
                                    cashbackAmount = cashbackAmount.add(surplusCashback);
                                    surplusCashback = new BigDecimal(0);
                                } else {
                                    amout = userFee;
                                    cashbackAmount = cashbackAmount.add(amout);
                                    surplusCashback = surplusCashback.subtract(amout);
                                }


                                // todo将用户返现的钱再到队列用户去，调用预充值

                                MallOrderGoods updateOrderGoods = new MallOrderGoods();
                                updateOrderGoods.setId(mallOrderGoodsVo.getId());
                                updateOrderGoods.setCashbackAmount(cashbackAmount);
                                updateOrderGoods.setSurplusCashback(surplusCashback);
                                orderGoodsService.updateById(updateOrderGoods);


                                if (userFee.compareTo(amout) == 0) {
                                    break;
                                }

                                // 返现给用户的钱递减
                                if (userFee.compareTo(amout) == 1) {
                                    userFee = userFee.subtract(amout);
                                }

                                // else {
                                // break;
                                // }
                                if (new BigDecimal(0).compareTo(userFee) > -1) {
                                    break;
                                }

                            }
                        }

                    }
                } else {
                    logger.info("----------------------没有队列，或者队列已满-------------------");
                    flagZY = "0";
                    return flagZY;
                }
                // }
            }
        }

        return flagZY;
    }

    /**
     * 活动小程序支付回调(998活动支付)
     */
    @Transactional(rollbackFor = {Exception.class})
    public Object payActivityNotify(HttpServletRequest request, HttpServletResponse response) {
        MallOrderNotifyLog log = new MallOrderNotifyLog();
        String xmlResult = null;
        try {
            xmlResult = IOUtils.toString(request.getInputStream(), request.getCharacterEncoding());
            log.setWxParams(xmlResult);
        } catch (IOException e) {
            e.printStackTrace();
            try {
                log.setErrMsg("微信通知参数解析异常：" + e.getMessage());
                log.setIsSuccess("0F");
                logService.add(log);
            } catch (Exception e1) {
                logger.error("日志添加异常: " + e1.getMessage(), e1);
            }
            return WxPayNotifyResponse.fail(e.getMessage());
        }

        WxPayOrderNotifyResult result = null;
        try {
            result = wxMiniActivityPayService.parseOrderNotifyResult(xmlResult);

            if (!WxPayConstants.ResultCode.SUCCESS.equals(result.getResultCode())) {
                logger.error(xmlResult);
                throw new WxPayException("微信通知支付失败！");
            }
            if (!WxPayConstants.ResultCode.SUCCESS.equals(result.getReturnCode())) {
                logger.error(xmlResult);
                throw new WxPayException("微信通知支付失败！");
            }
        } catch (WxPayException e) {
            try {
                log.setErrMsg("微信通知参数解析异常：" + e.getMessage());
                log.setIsSuccess("0F");
                logService.add(log);
            } catch (Exception e1) {
                logger.error("日志添加异常: " + e1.getMessage(), e1);
            }
            e.printStackTrace();
            return WxPayNotifyResponse.fail(e.getMessage());
        }

        logger.info("处理腾讯支付平台的订单支付");
        logger.info(result);

        String orderSn = result.getOutTradeNo();
        String payId = result.getTransactionId();
        log.setOrderSn(orderSn);

        // 分转化成元
        String totalFee = BaseWxPayResult.fenToYuan(result.getTotalFee());
        MallOrder order = orderService.findBySn(orderSn);
        try {
            if(order == null){
                QueryWrapper<MallPurchaseOrder> orderQueryWrapper = new QueryWrapper<>();
                orderQueryWrapper.eq("order_sn", orderSn);
                MallPurchaseOrder order1 = mallPurchaseOrderService.getOne(orderQueryWrapper);

                if (order1 == null) {
                    try {
                        log.setErrMsg("订单不存在 sn=" + orderSn);
                        log.setIsSuccess("0F");
                        logService.add(log);
                    } catch (Exception e1) {
                        logger.error("日志添加异常: " + e1.getMessage(), e1);
                    }
                    return WxPayNotifyResponse.fail("订单不存在 sn=" + orderSn);
                }
                logger.info("----->商户下单回调处理" + JSON.toJSONString(order1));

                if (!Objects.equals(order1.getUserId(), null)) {
                    log.setUserId(order1.getUserId());
                }

                // 检查这个订单是否已经处理过
                if (OrderUtil.isPayStatus(order1) && order1.getPayId() != null) {
                    try {
                        log.setErrMsg("订单已经处理过");
                        log.setIsSuccess("0F");
                        logService.add(log);
                    } catch (Exception e1) {
                        logger.error("日志添加异常: " + e1.getMessage(), e1);
                    }
                    return WxPayNotifyResponse.success("订单已经处理成功!");
                }

                // 检查支付订单金额
                if (!totalFee.equals(order1.getActualPrice().toString())) {
                    try {
                        log.setErrMsg(order1.getOrderSn() + " : 支付金额不符合 totalFee=" + totalFee);
                        log.setIsSuccess("0F");
                        logService.add(log);
                    } catch (Exception e1) {
                        logger.error("日志添加异常: " + e1.getMessage(), e1);
                    }
                    return WxPayNotifyResponse.fail(order1.getOrderSn() + " : 支付金额不符合 totalFee=" + totalFee);
                }

                order1.setPayId(payId);
                order1.setPayTime(LocalDateTime.now());
                order1.setOrderStatus(OrderUtil.STATUS_PAY.intValue());
                mallPurchaseOrderService.updateById(order1);
                doPayService.doMerchangOrderWithActive(order1);
                String succes = WxPayNotifyResponse.success("处理成功!");
                logger.info(succes);
                return succes;
            }

            // 检查这个订单是否已经处理过
            if (OrderUtil.isPayStatus(order) && order.getPayId() != null) {
                try {
                    log.setErrMsg("订单已经处理过");
                    log.setIsSuccess("0F");
                    logService.add(log);
                } catch (Exception e1) {
                    logger.error("日志添加异常: " + e1.getMessage(), e1);
                }
                return WxPayNotifyResponse.success("订单已经处理成功!");
            }

            // 检查支付订单金额
            if (!totalFee.equals(order.getActualPrice().toString())) {
                try {
                    log.setErrMsg(order.getOrderSn() + " : 支付金额不符合 totalFee=" + totalFee);
                    log.setIsSuccess("0F");
                    logService.add(log);
                } catch (Exception e1) {
                    logger.error("日志添加异常: " + e1.getMessage(), e1);
                }
                return WxPayNotifyResponse.fail(order.getOrderSn() + " : 支付金额不符合 totalFee=" + totalFee);
            }


            order.setPayId(payId);
            order.setPayTime(LocalDateTime.now());
            order.setOrderStatus(OrderUtil.STATUS_PAY);
            if (orderService.updateWithOptimisticLocker(order) == 0) {
                logger.info("三方平台开始下单updateWithOptimisticLocker。。。。。。。。。。。。。。" + order.toString());

                // 这里可能存在这样一个问题，用户支付和系统自动取消订单发生在同时
                // 如果数据库首先因为系统自动取消订单而更新了订单状态；
                // 此时用户支付完成回调这里也要更新数据库，而由于乐观锁机制这里的更新会失败
                // 因此，这里会重新读取数据库检查状态是否是订单自动取消，如果是则更新成支付状态。
                order = orderService.findBySn(orderSn);

                int updated = 0;

                // 订单自动取消
                if (OrderUtil.isAutoCancelStatus(order)) {
                    order.setPayId(payId);
                    order.setPayTime(LocalDateTime.now());
                    order.setOrderStatus(OrderUtil.STATUS_PAY);
                    updated = orderService.updateWithOptimisticLocker(order);
                    // 修改销量
                    List<MallOrderGoods> orderGoodsList = orderGoodsService.queryByOid(order.getId());
                    if (orderGoodsList.size() > 0) {
                        JSONObject obj = null;
                        for (MallOrderGoods mallOrderGoods : orderGoodsList) {
                            String productId = mallOrderGoods.getProductId();
                            String newProductId = "";
                            try {
                                obj = goodsService.queryGoodsByProductId(productId);
                                if (Objects.equals(null, obj)) {
                                    MallActivityGoodsProduct mallActivityGoodsProduct = mallActivityGoodsProductService.getById(productId);
                                    if(mallActivityGoodsProduct != null){
                                        newProductId = mallActivityGoodsProduct.getProductId();
                                        productId = newProductId;
                                    }
                                }

                                if(!Objects.equals(null, obj) || StringUtils.isNotEmpty(newProductId)){
                                    logger.info("--------修改自营商品销量和库存-----------------");
                                    // 在开放平台goods表添加销量和product表减去库存
                                    // 商品属于自营商品
                                    Integer buyNum = Integer.parseInt(String.valueOf(mallOrderGoods.getNumber()));
                                    if (!goodsService.updateZyOpenGoods(productId, buyNum)) {
                                        logger.info("修改自营商品销量和库存失败");
                                    }
                                }
                            } catch (Exception e) {
                                logger.info("远程修改库存和销量出现异常!");
                                e.printStackTrace();
                            }
                        }
                    }
                }

                try {
                    log.setRemark("订单支付通知计算完成");
                    log.setIsSuccess("1F");
                    logService.add(log);
                } catch (Exception e1) {
                    logger.error("日志添加异常: " + e1.getMessage(), e1);
                }
                // 如果updated是0，那么数据库更新失败
                if (updated == 0) {
                    return WxPayNotifyResponse.fail("更新数据已失效");
                }
            }

            // 订单支付成功处理用户权益逻辑
            doPayService.doUserAllBid(order);
            logger.info("三方平台开始下单。。。。。。。。。。。。。。");
            logger.info("三方平台开始下单。。。。。。。。。。。。。。" + order.toString());
            logger.info("三方平台开始下单。。。。。。。。。。。。。。" + order.getHuid());
            // 防止重复下单
            try {
                logger.info("开始下单：--------------------------------------");

                // 三方下单支付回调
                if (Objects.equals(order.getHuid(), "fuluGoods")) {
                    payFuLuGoodsOrder(order, "xcx");
                } else if (Objects.equals(order.getHuid(), "zmGoods")) {
                    payZMGoodsOrder(order, "xcx");
                } else if (Objects.equals(order.getHuid(), "lsxdGoods")) {
                    payLSXDGoodsOrder(order, "xcx");
                } else if (Objects.equals(order.getHuid(), "cost")) {
                    payCOSTGoodsOrder(order, "xcx");
                }
            } catch (Exception e) {
                logger.info("三方下单支付完成。。。。。。。。。");
            }

            List<MallOrderGoods> orderGoodsList = orderGoodsService.queryByOid(order.getId());
            if (orderGoodsList.size() > 0) {
                JSONObject obj = null;
                for (MallOrderGoods mallOrderGoods : orderGoodsList) {
                    String productId = mallOrderGoods.getProductId();
                    String newProductId = "";
                    try {
                        obj = goodsService.queryGoodsByProductId(productId);
                        if (Objects.equals(null, obj)) {
                            MallActivityGoodsProduct mallActivityGoodsProduct = mallActivityGoodsProductService.getById(productId);
                            if(mallActivityGoodsProduct != null){
                                newProductId = mallActivityGoodsProduct.getProductId();
                                productId = newProductId;
                            }
                        }

                        if(!Objects.equals(null, obj) || StringUtils.isNotEmpty(newProductId)){
                            logger.info("--------修改自营商品销量和库存-----------------");
                            // 在开放平台goods表添加销量和product表减去库存
                            // 商品属于自营商品
                            Integer buyNum = Integer.parseInt(String.valueOf(mallOrderGoods.getNumber()));
                            if (!goodsService.updateZyOpenGoods(productId, buyNum)) {
                                logger.info("修改自营商品销量和库存失败");
                            }
                        }
                    } catch (Exception e) {
                        logger.info("远程修改库存和销量出现异常!");
                        e.printStackTrace();
                    }
                }
            }
            /** 业务员活动名额减少 **/

            if (!org.apache.commons.lang3.StringUtils.isEmpty(order.getTbkPubId())) {
                List<MallMarketingActivity> mallUserList =
                        userService.selectUserActivity(null, null, order.getTbkPubId());
                if (mallUserList.size() > 0) {

                    Integer surplusQuota = Integer.valueOf(mallUserList.get(0).getSurplusQuota()) - 1;// 剩余名额
                    mallUserList.get(0).setSurplusQuota(surplusQuota + "");
                    mallMarketingActivityMapper.updateByPrimaryKey(mallUserList.get(0));
                }
            }


            // TODO 发送邮件和短信通知，这里采用异步发送
            // 订单支付成功以后，会发送短信给用户，以及发送邮件给管理员
            // notifyService.notifyMail("新订单通知", order.toString());
            // 这里微信的短信平台对参数长度有限制，所以将订单号只截取后6位
            // notifyService.notifySmsTemplateSync(order.getMobile(),
            // NotifyType.PAY_SUCCEED, new String[]{orderSn.substring(8,
            // 14)});

            // 请依据自己的模版消息配置更改参数
            // String[] parms = new String[]{
            // order.getOrderSn(),
            // order.getOrderPrice().toString(),
            // DateTimeUtil.getDateTimeDisplayString(order.getAddTime()),
            // order.getConsignee(),
            // order.getMobile(),
            // order.getAddress()
            // };

            // notifyService.notifyWxTemplate(result.getOpenid(),
            // NotifyType.PAY_SUCCEED, parms, "pages/index/index?orderId=" +
            // order.getId());
        } catch (Exception e) {
            try {
                log.setRemark("订单支付发生异常: " + e.getMessage());
                log.setIsSuccess("0F");
                logService.add(log);
            } catch (Exception e1) {
                logger.error("日志添加异常: " + e1.getMessage(), e1);
            }
            logger.error("支付订单通知处理异常 :" + e.getMessage(), e);
        }

        return WxPayNotifyResponse.success("处理成功!");
    }

    /**
     * 活动小程序支付回调(998活动支付)
     */
    @Transactional(rollbackFor = {Exception.class})
    public Object agentNotify(HttpServletRequest request, HttpServletResponse response) {
        MallOrderNotifyLog log = new MallOrderNotifyLog();
        String xmlResult = null;
        try {
            xmlResult = IOUtils.toString(request.getInputStream(), request.getCharacterEncoding());
            log.setWxParams(xmlResult);
        } catch (IOException e) {
            e.printStackTrace();
            try {
                log.setErrMsg("微信通知参数解析异常：" + e.getMessage());
                log.setIsSuccess("0F");
                logService.add(log);
            } catch (Exception e1) {
                logger.error("日志添加异常: " + e1.getMessage(), e1);
            }
            return WxPayNotifyResponse.fail(e.getMessage());
        }

        WxPayOrderNotifyResult result = null;
        try {
            result = wxMiniAgentPayService.parseOrderNotifyResult(xmlResult);

            if (!WxPayConstants.ResultCode.SUCCESS.equals(result.getResultCode())) {
                logger.error(xmlResult);
                throw new WxPayException("微信通知支付失败！");
            }
            if (!WxPayConstants.ResultCode.SUCCESS.equals(result.getReturnCode())) {
                logger.error(xmlResult);
                throw new WxPayException("微信通知支付失败！");
            }
        } catch (WxPayException e) {
            try {
                log.setErrMsg("微信通知参数解析异常：" + e.getMessage());
                log.setIsSuccess("0F");
                logService.add(log);
            } catch (Exception e1) {
                logger.error("日志添加异常: " + e1.getMessage(), e1);
            }
            e.printStackTrace();
            return WxPayNotifyResponse.fail(e.getMessage());
        }

        logger.info("处理腾讯支付平台的订单支付");
        logger.info(result);

        String orderSn = result.getOutTradeNo();
        String payId = result.getTransactionId();
        log.setOrderSn(orderSn);

        // 分转化成元
        String totalFee = BaseWxPayResult.fenToYuan(result.getTotalFee());
        MallOrder order = orderService.findBySn(orderSn);
        try {
            if(order == null){
                QueryWrapper<MallPurchaseOrder> orderQueryWrapper = new QueryWrapper<>();
                orderQueryWrapper.eq("order_sn", orderSn);
                MallPurchaseOrder order1 = mallPurchaseOrderService.getOne(orderQueryWrapper);

                if (order1 == null) {
                    try {
                        log.setErrMsg("订单不存在 sn=" + orderSn);
                        log.setIsSuccess("0F");
                        logService.add(log);
                    } catch (Exception e1) {
                        logger.error("日志添加异常: " + e1.getMessage(), e1);
                    }
                    return WxPayNotifyResponse.fail("订单不存在 sn=" + orderSn);
                }
                logger.info("----->商户下单回调处理" + JSON.toJSONString(order1));

                if (!Objects.equals(order1.getUserId(), null)) {
                    log.setUserId(order1.getUserId());
                }

                // 检查这个订单是否已经处理过
                if (OrderUtil.isPayStatus(order1) && order1.getPayId() != null) {
                    try {
                        log.setErrMsg("订单已经处理过");
                        log.setIsSuccess("0F");
                        logService.add(log);
                    } catch (Exception e1) {
                        logger.error("日志添加异常: " + e1.getMessage(), e1);
                    }
                    return WxPayNotifyResponse.success("订单已经处理成功!");
                }

                // 检查支付订单金额
                if (!totalFee.equals(order1.getActualPrice().toString())) {
                    try {
                        log.setErrMsg(order1.getOrderSn() + " : 支付金额不符合 totalFee=" + totalFee);
                        log.setIsSuccess("0F");
                        logService.add(log);
                    } catch (Exception e1) {
                        logger.error("日志添加异常: " + e1.getMessage(), e1);
                    }
                    return WxPayNotifyResponse.fail(order1.getOrderSn() + " : 支付金额不符合 totalFee=" + totalFee);
                }

                order1.setPayId(payId);
                order1.setPayTime(LocalDateTime.now());
                order1.setOrderStatus(OrderUtil.STATUS_PAY.intValue());
                mallPurchaseOrderService.updateById(order1);
                doPayService.doMerchangOrderWithActive(order1);
                String succes = WxPayNotifyResponse.success("处理成功!");
                logger.info(succes);
                return succes;
            }

            // 检查这个订单是否已经处理过
            if (OrderUtil.isPayStatus(order) && order.getPayId() != null) {
                try {
                    log.setErrMsg("订单已经处理过");
                    log.setIsSuccess("0F");
                    logService.add(log);
                } catch (Exception e1) {
                    logger.error("日志添加异常: " + e1.getMessage(), e1);
                }
                return WxPayNotifyResponse.success("订单已经处理成功!");
            }

            // 检查支付订单金额
            if (!totalFee.equals(order.getActualPrice().toString())) {
                try {
                    log.setErrMsg(order.getOrderSn() + " : 支付金额不符合 totalFee=" + totalFee);
                    log.setIsSuccess("0F");
                    logService.add(log);
                } catch (Exception e1) {
                    logger.error("日志添加异常: " + e1.getMessage(), e1);
                }
                return WxPayNotifyResponse.fail(order.getOrderSn() + " : 支付金额不符合 totalFee=" + totalFee);
            }


            order.setPayId(payId);
            order.setPayTime(LocalDateTime.now());
            order.setOrderStatus(OrderUtil.STATUS_PAY);
            if (orderService.updateWithOptimisticLocker(order) == 0) {
                logger.info("三方平台开始下单updateWithOptimisticLocker。。。。。。。。。。。。。。" + order.toString());

                // 这里可能存在这样一个问题，用户支付和系统自动取消订单发生在同时
                // 如果数据库首先因为系统自动取消订单而更新了订单状态；
                // 此时用户支付完成回调这里也要更新数据库，而由于乐观锁机制这里的更新会失败
                // 因此，这里会重新读取数据库检查状态是否是订单自动取消，如果是则更新成支付状态。
                order = orderService.findBySn(orderSn);

                int updated = 0;

                // 订单自动取消
                if (OrderUtil.isAutoCancelStatus(order)) {
                    order.setPayId(payId);
                    order.setPayTime(LocalDateTime.now());
                    order.setOrderStatus(OrderUtil.STATUS_PAY);
                    updated = orderService.updateWithOptimisticLocker(order);
                    // 修改销量
                    List<MallOrderGoods> orderGoodsList = orderGoodsService.queryByOid(order.getId());
                    if (orderGoodsList.size() > 0) {
                        JSONObject obj = null;
                        for (MallOrderGoods mallOrderGoods : orderGoodsList) {
                            String productId = mallOrderGoods.getProductId();
                            String newProductId = "";
                            try {
                                obj = goodsService.queryGoodsByProductId(productId);
                                if (Objects.equals(null, obj)) {
                                    MallActivityGoodsProduct mallActivityGoodsProduct = mallActivityGoodsProductService.getById(productId);
                                    if(mallActivityGoodsProduct != null){
                                        newProductId = mallActivityGoodsProduct.getProductId();
                                        productId = newProductId;
                                    }
                                }

                                if(!Objects.equals(null, obj) || StringUtils.isNotEmpty(newProductId)){
                                    logger.info("--------修改自营商品销量和库存-----------------");
                                    // 在开放平台goods表添加销量和product表减去库存
                                    // 商品属于自营商品
                                    Integer buyNum = Integer.parseInt(String.valueOf(mallOrderGoods.getNumber()));
                                    if (!goodsService.updateZyOpenGoods(productId, buyNum)) {
                                        logger.info("修改自营商品销量和库存失败");
                                    }
                                }
                            } catch (Exception e) {
                                logger.info("远程修改库存和销量出现异常!");
                                e.printStackTrace();
                            }
                        }
                    }
                }

                try {
                    log.setRemark("订单支付通知计算完成");
                    log.setIsSuccess("1F");
                    logService.add(log);
                } catch (Exception e1) {
                    logger.error("日志添加异常: " + e1.getMessage(), e1);
                }
                // 如果updated是0，那么数据库更新失败
                if (updated == 0) {
                    return WxPayNotifyResponse.fail("更新数据已失效");
                }
            }

            // 订单支付成功处理用户权益逻辑
            doPayService.doUserAllBid(order);
            logger.info("三方平台开始下单。。。。。。。。。。。。。。");
            logger.info("三方平台开始下单。。。。。。。。。。。。。。" + order.toString());
            logger.info("三方平台开始下单。。。。。。。。。。。。。。" + order.getHuid());
            // 防止重复下单
            try {
                logger.info("开始下单：--------------------------------------");

                // 三方下单支付回调
                if (Objects.equals(order.getHuid(), "fuluGoods")) {
                    payFuLuGoodsOrder(order, "xcx");
                } else if (Objects.equals(order.getHuid(), "zmGoods")) {
                    payZMGoodsOrder(order, "xcx");
                } else if (Objects.equals(order.getHuid(), "lsxdGoods")) {
                    payLSXDGoodsOrder(order, "xcx");
                } else if (Objects.equals(order.getHuid(), "cost")) {
                    payCOSTGoodsOrder(order, "xcx");
                }
            } catch (Exception e) {
                logger.info("三方下单支付完成。。。。。。。。。");
            }

            List<MallOrderGoods> orderGoodsList = orderGoodsService.queryByOid(order.getId());
            if (orderGoodsList.size() > 0) {
                JSONObject obj = null;
                for (MallOrderGoods mallOrderGoods : orderGoodsList) {
                    String productId = mallOrderGoods.getProductId();
                    String newProductId = "";
                    try {
                        obj = goodsService.queryGoodsByProductId(productId);
                        if (Objects.equals(null, obj)) {
                            MallActivityGoodsProduct mallActivityGoodsProduct = mallActivityGoodsProductService.getById(productId);
                            if(mallActivityGoodsProduct != null){
                                newProductId = mallActivityGoodsProduct.getProductId();
                                productId = newProductId;
                            }
                        }

                        if(!Objects.equals(null, obj) || StringUtils.isNotEmpty(newProductId)){
                            logger.info("--------修改自营商品销量和库存-----------------");
                            // 在开放平台goods表添加销量和product表减去库存
                            // 商品属于自营商品
                            Integer buyNum = Integer.parseInt(String.valueOf(mallOrderGoods.getNumber()));
                            if (!goodsService.updateZyOpenGoods(productId, buyNum)) {
                                logger.info("修改自营商品销量和库存失败");
                            }
                        }
                    } catch (Exception e) {
                        logger.info("远程修改库存和销量出现异常!");
                        e.printStackTrace();
                    }
                }
            }
            /** 业务员活动名额减少 **/

            if (!org.apache.commons.lang3.StringUtils.isEmpty(order.getTbkPubId())) {
                List<MallMarketingActivity> mallUserList =
                        userService.selectUserActivity(null, null, order.getTbkPubId());
                if (mallUserList.size() > 0) {

                    Integer surplusQuota = Integer.valueOf(mallUserList.get(0).getSurplusQuota()) - 1;// 剩余名额
                    mallUserList.get(0).setSurplusQuota(surplusQuota + "");
                    mallMarketingActivityMapper.updateByPrimaryKey(mallUserList.get(0));
                }
            }


            // TODO 发送邮件和短信通知，这里采用异步发送
            // 订单支付成功以后，会发送短信给用户，以及发送邮件给管理员
            // notifyService.notifyMail("新订单通知", order.toString());
            // 这里微信的短信平台对参数长度有限制，所以将订单号只截取后6位
            // notifyService.notifySmsTemplateSync(order.getMobile(),
            // NotifyType.PAY_SUCCEED, new String[]{orderSn.substring(8,
            // 14)});

            // 请依据自己的模版消息配置更改参数
            // String[] parms = new String[]{
            // order.getOrderSn(),
            // order.getOrderPrice().toString(),
            // DateTimeUtil.getDateTimeDisplayString(order.getAddTime()),
            // order.getConsignee(),
            // order.getMobile(),
            // order.getAddress()
            // };

            // notifyService.notifyWxTemplate(result.getOpenid(),
            // NotifyType.PAY_SUCCEED, parms, "pages/index/index?orderId=" +
            // order.getId());
        } catch (Exception e) {
            try {
                log.setRemark("订单支付发生异常: " + e.getMessage());
                log.setIsSuccess("0F");
                logService.add(log);
            } catch (Exception e1) {
                logger.error("日志添加异常: " + e1.getMessage(), e1);
            }
            logger.error("支付订单通知处理异常 :" + e.getMessage(), e);
        }

        return WxPayNotifyResponse.success("处理成功!");
    }

    /**
     * 业务逻辑 商城下单---请求中闵在线下单---获取下单状态，修改为充值中 在回调接口进行真正的充值结果记录 中闵在线
     *
     * @param order 订单对象
     * @param source 支付类型：wx-微信支付 huifu-汇付支付 alipay-支付宝支付 xcx-小程序支付 test-测试支付
     */
    public void payZMGoodsOrder(MallOrder order, String source) {
        // 防止重复下单
        if (orderIsRepeat(order.getId())) {
            return;
        }
        logger.info("开始中闵在线payZMGoodsOrder支付下单---------------------------------");
        List<MallOrderGoods> mallOrderGoods = orderGoodsService.queryByOid(order.getId());
        if (CollectionUtils.isEmpty(mallOrderGoods)) {
            logger.info("没有查询到订单详情---------------------------------");
            return;
        }

        String url = platFormServerproperty.getUrl(MallConstant.SUNING);

        Map<String, Object> mappMap = new HashMap<String, Object>();
        mappMap.put("plateform", "zm");
        mappMap.put("goodsId", mallOrderGoods.get(0).getGoodsSn());
        mappMap.put("customerOrderNo", order.getId());
        mappMap.put("account", order.getMobile());
        mappMap.put("num", mallOrderGoods.size());

        // 直充
        String method = "zhongmin.order.zc.insert";

        // 卡密
        if (Objects.equals(mallOrderGoods.get(0).getComment(), "卡密")) {
            method = "zhongmin.order.km.insert";
        }
        // 话费
        else if (Objects.equals(mallOrderGoods.get(0).getComment(), "话费")) {
            method = "zhongmin.order.hf.insert";
        }



        Header[] headers = new BasicHeader[] {new BasicHeader("method", method), new BasicHeader("version", "v1.0")};

        try {
            String result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);

            JSONObject object = JSONObject.parseObject(result);



            MallOrder mallOrder = new MallOrder();
            MallOrderNotifyLog log = new MallOrderNotifyLog();
            if (org.apache.commons.lang3.StringUtils.isNotBlank(order.getUserId())) {
                log.setUserId(order.getUserId());
                mallOrder.setUserId(order.getUserId());
            }
            log.setRetWx("订单--下单");
            log.setRemark(result);
            log.setWxParams("zm");



            if (com.graphai.mall.db.util.StringHelper.isNullOrEmpty(object)) {
                log.setErrMsg("中闵下单失败：开放平台出现错误");
                log.setIsSuccess("失败");
                log.setOrderId(order.getId());
                logService.add(log);
                return;
            }

            // 记录信息
            if (!object.getString("errno").equals("0")) {
                log.setErrMsg("中闵下单失败：" + object);
                log.setIsSuccess("失败");
                log.setOrderId(order.getId());

                mallOrder.setId(order.getId());
                mallOrder.setOrderThreeStatus(OrderUtil.STATUS_CREATE_FAIL);
                mallOrder.setSource(source);
                orderService.updateByPrimaryKeySelective(mallOrder);

                logger.info("----------------------中闽在线开始在线退款，订单id=" + mallOrder.getId());
                wxOrderRefundService.refundQY(order);
            } else {
                log.setIsSuccess("成功");
                log.setErrMsg("中闽在线下单成功：" + object);

                log.setOrderId(order.getId());

                mallOrder.setOrderThreeStatus(OrderUtil.STATUS_CREATE_LOADING);
                mallOrder.setId(order.getId());
                mallOrder.setSource(source);
                orderService.updateByPrimaryKeySelective(mallOrder);

            }
            logService.add(log);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    /**
     * 业务逻辑 商城下单---请求蓝色兄弟在线下单---获取下单状态，修改为充值中 在回调接口进行真正的充值结果记录 蓝色兄弟在线
     *
     * @param order 订单对象
     * @param source 支付类型：wx-微信支付 huifu-汇付支付 alipay-支付宝支付 xcx-小程序支付 test-测试支付
     */
    public void payLSXDGoodsOrder(MallOrder order, String source) {
        if (orderIsRepeat(order.getId())) {
            return;
        }
        logger.info("开始 payLSXDGoodsOrder 支付下单---------------------------------");

        MallOrderNotifyLog log = new MallOrderNotifyLog();
        log.setOrderId(order.getId());
        log.setWxParams("lsxd");
        log.setRetWx("订单--下单");

        if (org.apache.commons.lang3.StringUtils.isNotBlank(order.getUserId())) {
            log.setUserId(order.getUserId());
        }

        List<MallOrderGoods> mallOrderGoods = orderGoodsService.queryByOid(order.getId());
        if (CollectionUtils.isEmpty(mallOrderGoods)) {
            logger.info("查询订单详情失败!");
            log.setIsSuccess("失败");
            log.setErrMsg("查询订单详情失败");
            logService.add(log);
            return;
        }

        String url = platFormServerproperty.getUrl(MallConstant.SUNING);

        Map<String, Object> mappMap = new HashMap<String, Object>();
        mappMap.put("plateform", "lsxd");
        mappMap.put("goods_sn", mallOrderGoods.get(0).getGoodsSn());
        mappMap.put("customer_order_no", order.getId());
        mappMap.put("charge_account", order.getMobile());
        mappMap.put("buy_num", mallOrderGoods.get(0).getNumber());
        // 暂时的业务需求： QQ系列产品优先使用QQ号，其他产品是用手机号
        // 目前QQ只有十位数，最大位为3
        // 账号类型(1:手机号 2:QQ号)
        // 下面的判断存在问题，如果商品名称是黄钻，但是充值账号是QQ，就会出现错误
        // if (StringUtils.isNotBlank(mallOrderGoods.get(0).getChargeType())) {
        // if (mallOrderGoods.get(0).getChargeType().contains("QQ")) {
        // mappMap.put("account_type", "2");
        // } else {
        // mappMap.put("account_type", "1");
        // }
        // } else {
        // if (mallOrderGoods.get(0).getGoodsName().contains("QQ")) {
        // mappMap.put("account_type", "2");
        // } else {
        // mappMap.put("account_type", "1");
        // }
        // }

        if (!Objects.equals(null, mallOrderGoods.get(0).getChargeType())) {
            mappMap.put("account_type", mallOrderGoods.get(0).getChargeType());
        } else {
            mappMap.put("account_type", "1");
        }
        // 直充
        String method = "lsxd.order.zc.insert";

        // 卡密
        if (Objects.equals(mallOrderGoods.get(0).getComment(), "卡密")) {
            method = "lsxd.order.km.insert";
        }
        // 话费
        else if (Objects.equals(mallOrderGoods.get(0).getComment(), "话费")) {
            method = "lsxd.order.hf.insert";
        }


        Header[] headers = new BasicHeader[] {new BasicHeader("method", method), new BasicHeader("version", "v1.0")};


        String result = null;

        try {
            result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);
            if (result == null) {
                throw new RuntimeException();
            }
        } catch (Exception e) {
            log.setIsSuccess("失败");
            logger.info("请求 " + url + " 调用失败： " + result);
            logger.info("尝试重新调用接口");
            log.setErrMsg("接口第一次 调用失败 result ：" + result);

            try {
                Thread.sleep(1000);
                result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);
            } catch (Exception e1) {
                logger.info("请求 " + url + "调用第二次失败： " + result);
                logger.info("接口调用失败");
                log.setErrMsg(log.getErrMsg() + "; 请求 " + url + " 调用第二次失败");
                logService.add(log);
                return;
            }
        }
        try {


            JSONObject object = JSONObject.parseObject(result);

            MallOrder mallOrder = new MallOrder();
            log.setRemark(result);


            if (org.apache.commons.lang3.StringUtils.isNotBlank(order.getUserId())) {
                mallOrder.setUserId(order.getUserId());
            }

            if (com.graphai.mall.db.util.StringHelper.isNullOrEmpty(object)) {
                log.setErrMsg("蓝色兄弟下单失败：开放平台出现错误");
                log.setIsSuccess("失败");
                log.setOrderId(order.getId());
                logService.add(log);
                return;
            }

            // 记录信息
            if (!object.getString("errno").equals("0")) {
                log.setErrMsg("蓝色兄弟下单失败：" + object.getString("errmsg"));
                log.setIsSuccess("失败");
                log.setOrderId(order.getId());

                mallOrder.setId(order.getId());
                mallOrder.setOrderThreeStatus(OrderUtil.STATUS_CREATE_FAIL);
                mallOrder.setSource(source);

                orderService.updateByPrimaryKeySelective(mallOrder);

                logger.info("----------------------蓝色兄弟开始在线退款，订单id=" + mallOrder.getId());
                wxOrderRefundService.refundQY(order);
            } else {
                log.setIsSuccess("成功");
                log.setErrMsg("蓝色兄弟下单成功");

                log.setOrderId(order.getId());
                mallOrder.setOrderThreeStatus(OrderUtil.STATUS_CREATE_LOADING);
                mallOrder.setId(order.getId());
                mallOrder.setSource(source);
                orderService.updateByPrimaryKeySelective(mallOrder);

            }
            logService.add(log);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 业务逻辑 商城下单---请求话费充值下单---获取下单状态，修改为充值中 在回调接口进行真正的充值结果记录
     *
     * @param order 订单
     * @param source 支付类型：wx-微信支付 huifu-汇付支付 alipay-支付宝支付 xcx-小程序支付 test-测试支付
     */
    public void payCOSTGoodsOrder(MallOrder order, String source) {
        if (orderIsRepeat(order.getId())) {
            return;
        }
        logger.info("开始 payCOSTGoodsOrder 支付下单---------------------------------");

        MallOrderNotifyLog log = new MallOrderNotifyLog();
        log.setOrderId(order.getId());
        log.setWxParams("COST");
        log.setRetWx("话费充值--下单");

        List<MallOrderGoods> mallOrderGoods = orderGoodsService.queryByOid(order.getId());
        if (CollectionUtils.isEmpty(mallOrderGoods)) {
            logger.info("查询订单详情失败!");
            log.setIsSuccess("失败");
            log.setErrMsg("查询订单详情失败");
            logService.add(log);
            return;
        }

        String url = platFormServerproperty.getUrl(MallConstant.SUNING);

        Map<String, Object> mappMap = new HashMap<String, Object>();
        mappMap.put("plateform", "cost");

        mappMap.put("order_no", order.getId());
        mappMap.put("phone", order.getMobile());
        mappMap.put("amount", order.getOrderPrice());


        // 直充
        String method = "cost.order.zc.insert";


        Header[] headers = new BasicHeader[] {new BasicHeader("method", method), new BasicHeader("version", "v1.0")};


        String result = null;

        try {
            result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);
            if (result == null) {
                throw new RuntimeException();
            }
        } catch (Exception e) {
            log.setIsSuccess("失败");
            logger.info("请求 " + url + " 调用失败： " + result);
            logger.info("尝试重新调用接口");
            log.setErrMsg("接口第一次 调用失败 result ：" + result);

            try {
                Thread.sleep(1000);
                result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);
            } catch (Exception e1) {
                logger.info("请求 " + url + "调用第二次失败： " + result);
                logger.info("接口调用失败");
                log.setErrMsg(log.getErrMsg() + "; 请求 " + url + " 调用第二次失败");
                logService.add(log);
                return;
            }
        }
        try {
            JSONObject object = JSONObject.parseObject(result);

            MallOrder mallOrder = new MallOrder();
            log.setRemark(result);

            if (com.graphai.mall.db.util.StringHelper.isNullOrEmpty(object)) {
                log.setErrMsg("话费充值失败：开放平台出现错误");
                log.setIsSuccess("失败");
                log.setOrderId(order.getId());
                logService.add(log);
                return;
            }

            // 记录信息
            if (!object.getString("errno").equals("0")) {
                log.setErrMsg("话费充值下单失败：" + object.getString("errmsg"));
                log.setIsSuccess("失败");
                log.setOrderId(order.getId());

                mallOrder.setId(order.getId());
                mallOrder.setOrderThreeStatus(OrderUtil.STATUS_CREATE_FAIL);
                mallOrder.setSource(source);
                orderService.updateByPrimaryKeySelective(mallOrder);

                logger.info("----------------------话费充值开始在线退款，订单id=" + mallOrder.getId());
                wxOrderRefundService.refundQY(order);
            } else {
                log.setIsSuccess("成功");
                log.setErrMsg("话费充值下单成功");

                log.setOrderId(order.getId());
                mallOrder.setOrderThreeStatus(OrderUtil.STATUS_CREATE_LOADING);
                mallOrder.setId(order.getId());
                mallOrder.setSource(source);
                orderService.updateByPrimaryKeySelective(mallOrder);

            }
            logService.add(log);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    /**
     * 防止重复下单
     */
    private boolean orderIsRepeat(String orderId) {
        return logService.getCount(orderId);
    }

    /**
     * 购买福禄商品 业务描述： 下单回调成功---》 回调信息给商城，更新三方支付状态 ---》 记录订单信息 下单回调失败---》 回调给商城,更新三方支付状态 --- 》 记录失败原因 ----
     * 》 不进行退款，人工在福禄后台补单
     *
     * @param order 订单对象
     * @param source 支付类型：wx-微信支付 huifu-汇付支付 alipay-支付宝支付 xcx-小程序支付 test-测试支付
     */
    public void payFuLuGoodsOrder(MallOrder order, String source) {
        MallOrderNotifyLog log = new MallOrderNotifyLog();
        log.setRetWx("订单--下单");

        // 防止重复下单
        try {
            boolean isRepeat = orderIsRepeat(order.getId());
            logger.info("是否重复下单：--------------------------------------" + isRepeat);
            if (isRepeat) {
                return;
            }

            if (org.apache.commons.lang3.StringUtils.isNotBlank(order.getUserId())) {
                log.setUserId(order.getUserId());
            }

            logger.info("福禄订单下单开始============================================");

            log.setOrderId(order.getId());
            log.setIsSuccess("失败");
            log.setWxParams("fulu");

            List<MallOrderGoods> mallOrderGoods = orderGoodsService.queryByOid(order.getId());
            if (CollectionUtils.isEmpty(mallOrderGoods)) {
                logger.info("查询订单详情失败!");
                log.setIsSuccess("失败");
                log.setErrMsg("查询订单详情失败");
                logService.add(log);
                return;
            }

            String url = platFormServerproperty.getUrl(MallConstant.SUNING);

            Map<String, Object> mappMap = new HashMap<String, Object>();
            mappMap.put("plateform", "fulu");
            mappMap.put("product_id", mallOrderGoods.get(0).getGoodsSn());
            mappMap.put("buy_num", mallOrderGoods.size());
            mappMap.put("customer_order_no", order.getId());
            mappMap.put("charge_account", order.getMobile());

            // 直充
            String method = "fulu.order.direct.add";

            // 话费
            if (Objects.equals(mallOrderGoods.get(0).getComment(), "话费")) {
                method = "fulu.order.mobile.add";
                mappMap.put("charge_value", order.getGoodsPrice());
                mappMap.put("charge_phone", order.getMobile());

            }
            // 卡密
            else if (Objects.equals(mallOrderGoods.get(0).getComment(), "卡密")) {
                method = "fulu.order.card.add";
            }

            Header[] headers =
                    new BasicHeader[] {new BasicHeader("method", method), new BasicHeader("version", "v1.0")};


            String result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);

            logger.info("返回信息：" + result);
            log.setRemark(mappMap + "返回结果：" + result);
            if (result == null) {
                log.setIsSuccess("失败");
                log.setErrMsg("第一次" + url + "调用下单接口错误" + result);
                logService.add(log);

                logger.info("调用下单接口错误" + result);
                logger.info("休眠1秒，重新尝试调用");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);
                if (result == null) {
                    log.setIsSuccess("失败");
                    log.setErrMsg("第二次" + url + "调用下单接口错误" + result);
                    logService.add(log);
                    return;
                }
            }
            JSONObject object = null;
            try {
                object = JSONObject.parseObject(result);
                if (com.graphai.mall.db.util.StringHelper.isNullOrEmpty(object)) {
                    throw new RuntimeException();
                }
            } catch (Exception e) {
                log.setErrMsg("福禄下单失败：开放平台出现错误:" + object);
                log.setIsSuccess("失败");
                log.setOrderId(order.getId());
                logService.add(log);
                return;
            }



            MallOrder mallOrder = new MallOrder();
            if (org.apache.commons.lang3.StringUtils.isNotBlank(order.getUserId())) {
                mallOrder.setUserId(order.getUserId());
            }

            log.setOrderId(order.getId());
            // 记录信息
            if (!Objects.equals(object.getString("errno"), "0")) {
                log.setErrMsg("福禄下单失败：" + object);
                log.setIsSuccess("失败");

                mallOrder.setId(order.getId());
                mallOrder.setOrderThreeStatus(OrderUtil.STATUS_CREATE_FAIL);
                mallOrder.setSource(source);
                orderService.updateByPrimaryKeySelective(mallOrder);
                logger.info("----------------------福禄开始在线退款，订单id=" + mallOrder.getId());
                wxOrderRefundService.refundQY(order);
            } else {

                mallOrder.setOrderThreeStatus(OrderUtil.STATUS_CREATE_SUCCESS);
                log.setIsSuccess("成功");
                log.setErrMsg("福禄下单成功：" + object);

                mallOrder.setId(order.getId());
                mallOrder.setSource(source);
                orderService.updateByPrimaryKeySelective(mallOrder);

                // 卡密信息，仅卡密订单返回（注意：卡密是密文，需要进行解密使用），当解密出来的卡号为“”或“无卡号”时，则以卡密为准，否则以卡号+密码为准；
                // 卡密 、 话费
                if (!Objects.equals(mallOrderGoods.get(0).getComment(), "直充")) {
                    net.sf.json.JSONArray jsonArray = null;
                    try {
                        jsonArray = net.sf.json.JSONArray.fromObject(object.getString("data"));
                        logger.info("卡密返回结果--------------------------" + jsonArray);
                        if (jsonArray == null) {
                            throw new RuntimeException("卡密信息返回失败");
                        }
                        for (int i = 0; i < jsonArray.size(); i++) {
                            logger.info("进行卡密插入--------------------------------------");
                            MallOrderGoods newMallOrderGoods = new MallOrderGoods();

                            net.sf.json.JSONObject jsonObject = jsonArray.getJSONObject(i);

                            if (jsonObject.containsKey("card_number")) {
                                newMallOrderGoods.setCardNumber(jsonObject.getString("card_number"));
                            }
                            if (jsonObject.containsKey("card_deadline")) {
                                newMallOrderGoods.setCardDeadline(jsonObject.getString("card_deadline"));
                            }
                            if (jsonObject.containsKey("card_pwd")) {
                                newMallOrderGoods.setCardPwd(jsonObject.getString("card_pwd"));
                            }
                            // 顺序无所谓，都是同一笔订单的不同商品，不重复即可
                            newMallOrderGoods.setId(mallOrderGoods.get(i).getId());
                            if (org.apache.commons.lang3.StringUtils.isNotBlank(order.getUserId())) {
                                newMallOrderGoods.setUserId(order.getUserId());
                            }
                            orderGoodsService.updateById(newMallOrderGoods);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        logger.info("json解析失败--------------------------- ---  / 卡密信息返回失败  / ");
                        log.setIsSuccess("失败");
                        log.setErrMsg("福禄下单失败：json解析失败");
                        logService.add(log);
                        return;
                    }

                }
            }
            logService.add(log);

        } catch (Exception e) {
            e.printStackTrace();
            logger.info("出现异常" + e.getMessage());
            log.setIsSuccess("失败");
            log.setErrMsg("出现异常" + e.getMessage());
            logService.add(log);
        }
    }

    @Transactional(rollbackFor = {Exception.class})
    public String alipayNotify(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("-----------------------------------支付宝回调进来了----------------------------------");
        // 获取支付宝POST过来反馈信息
        String return_code = "fail";
        MallOrderNotifyLog log = new MallOrderNotifyLog();
        // try {

        Map<String, String> params = new HashMap<String, String>();
        Map<String, String[]> requestParams = request.getParameterMap();
        for (Iterator<String> iter = requestParams.keySet().iterator(); iter.hasNext();) {
            String name = (String) iter.next();
            String[] values = (String[]) requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i] : valueStr + values[i] + ",";
            }
            // 乱码解决，这段代码在出现乱码时使用。
            // valueStr = new String(valueStr.getBytes("ISO-8859-1"), "utf-8");
            params.put(name, valueStr);
        }
        log.setRemark("alipay...");

        // 移除非验证参数
        params.remove("sign_type");
        boolean signVerified = false;
        try {
            signVerified = AlipaySignature.rsaCheckV2(params, aliProperties.getRsaPublicKey(), "utf-8", "RSA2");
        } catch (AlipayApiException e2) {
            e2.printStackTrace();
        }
        logger.info("[支付宝支付]-支付宝回调 sign：[{}]" + JSONObject.toJSONString(signVerified));
        if (signVerified) {
            String notifyAppid = params.get("app_id");
            if (StringUtils.isBlank(notifyAppid) || !notifyAppid.equals(aliProperties.getAppId())) {
                logger.error("[支付宝支付]-支付宝回调：appId校验失败 appId:" + aliProperties.getAppId());
                logger.error("notifyAppid" + notifyAppid);
                return "fail";
            }
        }

        String outTradeNo = MapUtils.getString(params, "out_trade_no");
        String subject = MapUtils.getString(params, "subject");// 商品名称
        String sellerId = MapUtils.getString(params, "seller_id");// 卖家用户号
        String sellerEmail = MapUtils.getString(params, "seller_email");// 卖家账号
        String buyerId = MapUtils.getString(params, "buyer_id");// 卖家用户号
        String buyerEmail = MapUtils.getString(params, "buyer_email");// 卖家账号
        String totalFee = MapUtils.getString(params, "total_amount");// 交易金额
        String body = MapUtils.getString(params, "body");// 商品描述
        String gmtCreate = MapUtils.getString(params, "gmt_create");// 交易创建时间
        String gmtPayment = MapUtils.getString(params, "gmt_payment");// 交易付款时间
        String tradeNo = MapUtils.getString(params, "trade_no");// 支付宝交易号
        String tradeStatus = MapUtils.getString(params, "trade_status");// 交易状态
        String sign = MapUtils.getString(params, "sign");// 交易签名

        // 切记alipaypublickey是支付宝的公钥，请去open.alipay.com对应应用下查看。
        // boolean AlipaySignature.rsaCheckV1(Map<String, String> params, String publicKey, String charset,
        // String sign_type)
        boolean flag = true;// AlipaySignature.rsaCheckV1(params, aliProperties.getRsaPublicKey(), "utf-8","RSA2");


        if (Objects.equals(tradeStatus, "TRADE_CLOSED") || Objects.equals(tradeStatus, "WAIT_BUYER_PAY")) {
            log.setErrMsg(outTradeNo + " : 订单支付回调通知失败" + tradeStatus);
            log.setIsSuccess("0F");
            logService.add(log);
            return return_code;
        }

        try {
            logger.info("~~~~~~支付宝参数： " + JacksonUtils.bean2Jsn(params));
        } catch (Exception e) {
            logger.error("支付宝回调处理: ", e);
        }

        logger.info("~~~~~~支付宝验证签名flag： " + flag);
        if (flag) {
            MallOrder order = orderService.findBySn(outTradeNo);
            if(order == null){
                QueryWrapper<MallPurchaseOrder> orderQueryWrapper = new QueryWrapper<>();
                orderQueryWrapper.eq("order_sn", outTradeNo);
                MallPurchaseOrder order1 = mallPurchaseOrderService.getOne(orderQueryWrapper);

                if (order1 == null) {
                    try {
                        log.setErrMsg("订单不存在 sn=" + outTradeNo);
                        log.setIsSuccess("0F");
                        logService.add(log);
                    } catch (Exception e1) {
                        logger.error("日志添加异常: " + e1.getMessage(), e1);
                    }
                    return WxPayNotifyResponse.fail("订单不存在 sn=" + outTradeNo);
                }

                if (order1 == null) {
                    try {
                        log.setErrMsg("订单不存在 sn=" + outTradeNo);
                        log.setIsSuccess("0F");
                        logService.add(log);
                    } catch (Exception e1) {
                        logger.error("日志添加异常: " + e1.getMessage(), e1);
                    }
                    return WxPayNotifyResponse.fail("订单不存在 sn=" + outTradeNo);
                }

                if (!Objects.equals(order1.getUserId(), null)) {
                    log.setUserId(order1.getUserId());
                }

                // 检查这个订单是否已经处理过
                if (OrderUtil.isPayStatus(order1) && order1.getPayId() != null) {
                    try {
                        log.setErrMsg("订单已经处理过");
                        log.setIsSuccess("0F");
                        logService.add(log);
                    } catch (Exception e1) {
                        logger.error("日志添加异常: " + e1.getMessage(), e1);
                    }
                    return WxPayNotifyResponse.success("订单已经处理成功!");
                }

                // 检查支付订单金额
                if (!totalFee.equals(order1.getActualPrice().toString())) {
                    try {
                        log.setErrMsg(order1.getOrderSn() + " : 支付金额不符合 totalFee=" + totalFee);
                        log.setIsSuccess("0F");
                        logService.add(log);
                    } catch (Exception e1) {
                        logger.error("日志添加异常: " + e1.getMessage(), e1);
                    }
                    return WxPayNotifyResponse.fail(order1.getOrderSn() + " : 支付金额不符合 totalFee=" + totalFee);
                }

                order1.setPayId(tradeNo);
                order1.setPayTime(LocalDateTime.now());
                order1.setOrderStatus(OrderUtil.STATUS_PAY.intValue());
                mallPurchaseOrderService.updateById(order1);
                doPayService.doMerchangOrderWithActive(order1);
                String succes = WxPayNotifyResponse.success("处理成功!");
                logger.info(succes);
                return succes;
            }

            // 检查这个订单是否已经处理过
            if (OrderUtil.isPayStatus(order) && order.getPayId() != null) {
                try {
                    log.setErrMsg("订单已经处理过");
                    log.setIsSuccess("0F");
                    logService.add(log);
                } catch (Exception e1) {
                    logger.error("日志添加异常: " + e1.getMessage(), e1);
                }

                return return_code;
                // return WxPayNotifyResponse.success("订单已经处理成功!");
            }

            // 检查支付订单金额
            if (!order.getActualPrice().toString().equals(totalFee)) {
                try {
                    log.setErrMsg(order.getOrderSn() + " : 支付金额不符合 totalFee=" + totalFee);
                    log.setIsSuccess("0F");
                    logService.add(log);
                } catch (Exception e1) {
                    logger.error("日志添加异常: " + e1.getMessage(), e1);
                }

                return return_code;
                // return WxPayNotifyResponse.fail(order.getOrderSn() + " : 支付金额不符合 totalFee=" + totalFee);
            }

            order.setPayId(tradeNo);
            order.setPayTime(LocalDateTime.now());
            order.setOrderStatus(OrderUtil.STATUS_PAY);
            if (orderService.updateWithOptimisticLocker(order) == 0) {
                // 这里可能存在这样一个问题，用户支付和系统自动取消订单发生在同时
                // 如果数据库首先因为系统自动取消订单而更新了订单状态；
                // 此时用户支付完成回调这里也要更新数据库，而由于乐观锁机制这里的更新会失败
                // 因此，这里会重新读取数据库检查状态是否是订单自动取消，如果是则更新成支付状态。
                order = orderService.findBySn(outTradeNo);
                int updated = 0;

                // 订单自动取消
                if (OrderUtil.isAutoCancelStatus(order)) {
                    order.setPayId(tradeNo);
                    order.setPayTime(LocalDateTime.now());
                    order.setOrderStatus(OrderUtil.STATUS_PAY);

                    updated = orderService.updateWithOptimisticLocker(order);

                    // 修改销量
                    List<MallOrderGoods> mallOrderGoodsList = orderGoodsService.queryByOid(order.getId());
                    if (CollectionUtils.isNotEmpty(mallOrderGoodsList)) {
                        for (int i = 0; i < mallOrderGoodsList.size(); i++) {
                            MallOrderGoods mallOrderGoods = mallOrderGoodsList.get(i);
                            if (mallOrderGoods.getNumber() != null) {
                                MallGoods mallGoods = new MallGoods();
                                mallGoods.setId(mallOrderGoods.getGoodsId());
                                mallGoods.setItemsale(Integer.parseInt(mallOrderGoods.getNumber().toString()));
                                goodsService.updateMallGoodsSales(mallGoods);
                            }
                        }
                    }
                }

                try {
                    log.setRemark("订单支付通知计算完成");
                    log.setIsSuccess("1F");
                    logService.add(log);
                } catch (Exception e1) {
                    logger.error("日志添加异常: " + e1.getMessage(), e1);
                }

                // 如果updated是0，那么数据库更新失败
                if (updated == 0) {
                    return return_code;
                    // return WxPayNotifyResponse.fail("更新数据已失效");
                }
            }

            // 判断是口令红包订单就拦截处理，不往下执行了
            if (AccountStatus.BID_45.equals(order.getBid())) {
                try {
                    return_code = wxRedService.processRedPwd(order, tradeNo);
                    return return_code;
                } catch (Exception ex) {
                    logger.info("三方下单支付出错：" + ex);
                }
            }

            // 判断是-网赚任务-订单就拦截处理，不往下执行了
            if (AccountStatus.BID_47.equals(order.getBid())) {
                try {
                    return wangzhuanTaskService.orderProcess(order, tradeNo);
                } catch (Exception ex) {
                    logger.info("三方下--网赚任务--单支付出错：" + ex.getMessage(), ex);
                }
            }

            // 判断是-网赚任务-上调数量/上调单价-订单就拦截处理，不往下执行了
            if (AccountStatus.BID_48.equals(order.getBid())) {
                try {
                    return wangzhuanTaskService.orderProcessAddNumOrAmount(order,tradeNo);
                }catch (Exception e){
                    logger.info("三方下-- 网赚任务-上调任务单价/任务数量--订单支付出错："+e.getMessage(),e);
                }
            }

            // 判断是-网赚任务-购买推荐、置顶 / 竞价 -订单就拦截处理，不往下执行了
            if (AccountStatus.BID_49.equals(order.getBid())) {
                try {
                    return wangzhuanTaskServiceV2.orderProcessTBR(order,tradeNo);
                }catch (Exception e){
                    logger.info("三方下-- 网赚任务-上调任务单价/任务数量--订单支付出错："+e.getMessage(),e);
                }
            }

            // 更新团购信息
            order.setSource("alipay");
//            doPayService.doGroupV3(order);
            doPayService.doNewGroupV3(order);
//            doPayService.doUserGroupon(order);

            try {
                logger.info("开始下单：--------------------------------------");
                // 福禄支付回调
                if (Objects.equals(order.getHuid(), "fuluGoods")) {
                    payFuLuGoodsOrder(order, "alipay");
                } else if (Objects.equals(order.getHuid(), "zmGoods")) {
                    payZMGoodsOrder(order, "alipay");
                } else if (Objects.equals(order.getHuid(), "lsxdGoods")) {
                    payLSXDGoodsOrder(order, "alipay");
                } else if (Objects.equals(order.getHuid(), "cost")) {
                    payCOSTGoodsOrder(order, "alipay");
                }
            } catch (Exception e) {
                logger.info("三方下单支付出错。。。。。。。。。");
            }


            // 用户购买优惠券商品
            doPayService.doUserCouponGoods(order);
            // 升级达人
            doPayService.doUserTalent(order);
            // 订单支付成功处理用户权益逻辑
            doPayService.doUserAllBid(order);

            return_code = "success";
            return return_code;
        } else {
            return return_code;
        }

        // } catch (AlipayApiException e) {
        // // TODO Auto-generated catch block
        // e.printStackTrace();
        // }

        // return return_code;
    }


    @Transactional(rollbackFor = {Exception.class})
    public String alipayPubNotify(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("-----------------------------------支付宝回调进来了----------------------------------");
        // 获取支付宝POST过来反馈信息
        String return_code = "fail";
        MallOrderNotifyLog log = new MallOrderNotifyLog();
        // try {

        Map<String, String> params = new HashMap<String, String>();
        Map<String, String[]> requestParams = request.getParameterMap();
        for (Iterator<String> iter = requestParams.keySet().iterator(); iter.hasNext();) {
            String name = (String) iter.next();
            String[] values = (String[]) requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i] : valueStr + values[i] + ",";
            }
            // 乱码解决，这段代码在出现乱码时使用。
            // valueStr = new String(valueStr.getBytes("ISO-8859-1"), "utf-8");
            params.put(name, valueStr);
        }
        log.setRemark("alipay...");

        // 移除非验证参数
        params.remove("sign_type");
        boolean signVerified = false;
        try {
            signVerified = AlipaySignature.rsaCheckV2(params, aliProperties.getHsRsaPublicKey(), "utf-8", "RSA2");
        } catch (AlipayApiException e2) {
            e2.printStackTrace();
        }
        logger.info("[支付宝支付]-支付宝回调 sign：[{}]" + JSONObject.toJSONString(signVerified));
        if (signVerified) {
            String notifyAppid = params.get("app_id");
            if (StringUtils.isBlank(notifyAppid) || !notifyAppid.equals(aliProperties.getHsAppId())) {
                logger.error("[支付宝支付]-支付宝回调：appId校验失败 appId:" + aliProperties.getHsAppId());
                logger.error("notifyAppid" + notifyAppid);
                return "fail";
            }
        }

        String outTradeNo = MapUtils.getString(params, "out_trade_no");
        String subject = MapUtils.getString(params, "subject");// 商品名称
        String sellerId = MapUtils.getString(params, "seller_id");// 卖家用户号
        String sellerEmail = MapUtils.getString(params, "seller_email");// 卖家账号
        String buyerId = MapUtils.getString(params, "buyer_id");// 卖家用户号
        String buyerEmail = MapUtils.getString(params, "buyer_email");// 卖家账号
        String totalFee = MapUtils.getString(params, "total_amount");// 交易金额
        String body = MapUtils.getString(params, "body");// 商品描述
        String gmtCreate = MapUtils.getString(params, "gmt_create");// 交易创建时间
        String gmtPayment = MapUtils.getString(params, "gmt_payment");// 交易付款时间
        String tradeNo = MapUtils.getString(params, "trade_no");// 支付宝交易号
        String tradeStatus = MapUtils.getString(params, "trade_status");// 交易状态
        String sign = MapUtils.getString(params, "sign");// 交易签名

        // 切记alipaypublickey是支付宝的公钥，请去open.alipay.com对应应用下查看。
        // boolean AlipaySignature.rsaCheckV1(Map<String, String> params, String publicKey, String charset,
        // String sign_type)
        boolean flag = true;// AlipaySignature.rsaCheckV1(params, aliProperties.getRsaPublicKey(), "utf-8","RSA2");


        if (Objects.equals(tradeStatus, "TRADE_CLOSED") || Objects.equals(tradeStatus, "WAIT_BUYER_PAY")) {
            log.setErrMsg(outTradeNo + " : 订单支付回调通知失败" + tradeStatus);
            log.setIsSuccess("0F");
            logService.add(log);
            return return_code;
        }

        try {
            logger.info("~~~~~~支付宝参数： " + JacksonUtils.bean2Jsn(params));
        } catch (Exception e) {
            logger.error("支付宝回调处理: ", e);
        }

        logger.info("~~~~~~支付宝验证签名flag： " + flag);
        if (flag) {
            MallOrder order = orderService.findBySn(outTradeNo);
            if(order == null){
                QueryWrapper<MallPurchaseOrder> orderQueryWrapper = new QueryWrapper<>();
                orderQueryWrapper.eq("order_sn", outTradeNo);
                MallPurchaseOrder order1 = mallPurchaseOrderService.getOne(orderQueryWrapper);

                if (order1 == null) {
                    try {
                        log.setErrMsg("订单不存在 sn=" + outTradeNo);
                        log.setIsSuccess("0F");
                        logService.add(log);
                    } catch (Exception e1) {
                        logger.error("日志添加异常: " + e1.getMessage(), e1);
                    }
                    return WxPayNotifyResponse.fail("订单不存在 sn=" + outTradeNo);
                }

                if (order1 == null) {
                    try {
                        log.setErrMsg("订单不存在 sn=" + outTradeNo);
                        log.setIsSuccess("0F");
                        logService.add(log);
                    } catch (Exception e1) {
                        logger.error("日志添加异常: " + e1.getMessage(), e1);
                    }
                    return WxPayNotifyResponse.fail("订单不存在 sn=" + outTradeNo);
                }

                if (!Objects.equals(order1.getUserId(), null)) {
                    log.setUserId(order1.getUserId());
                }

                // 检查这个订单是否已经处理过
                if (OrderUtil.isPayStatus(order1) && order1.getPayId() != null) {
                    try {
                        log.setErrMsg("订单已经处理过");
                        log.setIsSuccess("0F");
                        logService.add(log);
                    } catch (Exception e1) {
                        logger.error("日志添加异常: " + e1.getMessage(), e1);
                    }
                    return WxPayNotifyResponse.success("订单已经处理成功!");
                }

                // 检查支付订单金额
                if (!totalFee.equals(order1.getActualPrice().toString())) {
                    try {
                        log.setErrMsg(order1.getOrderSn() + " : 支付金额不符合 totalFee=" + totalFee);
                        log.setIsSuccess("0F");
                        logService.add(log);
                    } catch (Exception e1) {
                        logger.error("日志添加异常: " + e1.getMessage(), e1);
                    }
                    return WxPayNotifyResponse.fail(order1.getOrderSn() + " : 支付金额不符合 totalFee=" + totalFee);
                }

                order1.setPayId(tradeNo);
                order1.setPayTime(LocalDateTime.now());
                order1.setOrderStatus(OrderUtil.STATUS_PAY.intValue());
                mallPurchaseOrderService.updateById(order1);
                String succes = WxPayNotifyResponse.success("处理成功!");
                logger.info(succes);
                return succes;
            }

            // 检查这个订单是否已经处理过
            if (OrderUtil.isPayStatus(order) && order.getPayId() != null) {
                try {
                    log.setErrMsg("订单已经处理过");
                    log.setIsSuccess("0F");
                    logService.add(log);
                } catch (Exception e1) {
                    logger.error("日志添加异常: " + e1.getMessage(), e1);
                }

                return return_code;
                // return WxPayNotifyResponse.success("订单已经处理成功!");
            }

            // 检查支付订单金额
            if (!order.getActualPrice().toString().equals(totalFee)) {
                try {
                    log.setErrMsg(order.getOrderSn() + " : 支付金额不符合 totalFee=" + totalFee);
                    log.setIsSuccess("0F");
                    logService.add(log);
                } catch (Exception e1) {
                    logger.error("日志添加异常: " + e1.getMessage(), e1);
                }

                return return_code;
                // return WxPayNotifyResponse.fail(order.getOrderSn() + " : 支付金额不符合 totalFee=" + totalFee);
            }

            order.setPayId(tradeNo);
            order.setPayTime(LocalDateTime.now());
            order.setOrderStatus(OrderUtil.STATUS_PAY);
            if (orderService.updateWithOptimisticLocker(order) == 0) {
                // 这里可能存在这样一个问题，用户支付和系统自动取消订单发生在同时
                // 如果数据库首先因为系统自动取消订单而更新了订单状态；
                // 此时用户支付完成回调这里也要更新数据库，而由于乐观锁机制这里的更新会失败
                // 因此，这里会重新读取数据库检查状态是否是订单自动取消，如果是则更新成支付状态。
                order = orderService.findBySn(outTradeNo);
                int updated = 0;

                // 订单自动取消
                if (OrderUtil.isAutoCancelStatus(order)) {
                    order.setPayId(tradeNo);
                    order.setPayTime(LocalDateTime.now());
                    order.setOrderStatus(OrderUtil.STATUS_PAY);

                    updated = orderService.updateWithOptimisticLocker(order);

                    // 修改销量
                    List<MallOrderGoods> mallOrderGoodsList = orderGoodsService.queryByOid(order.getId());
                    if (CollectionUtils.isNotEmpty(mallOrderGoodsList)) {
                        for (int i = 0; i < mallOrderGoodsList.size(); i++) {
                            MallOrderGoods mallOrderGoods = mallOrderGoodsList.get(i);
                            if (mallOrderGoods.getNumber() != null) {
                                MallGoods mallGoods = new MallGoods();
                                mallGoods.setId(mallOrderGoods.getGoodsId());
                                mallGoods.setItemsale(Integer.parseInt(mallOrderGoods.getNumber().toString()));
                                goodsService.updateMallGoodsSales(mallGoods);
                            }
                        }
                    }
                }

                try {
                    log.setRemark("订单支付通知计算完成");
                    log.setIsSuccess("1F");
                    logService.add(log);
                } catch (Exception e1) {
                    logger.error("日志添加异常: " + e1.getMessage(), e1);
                }

                // 如果updated是0，那么数据库更新失败
                if (updated == 0) {
                    return return_code;
                    // return WxPayNotifyResponse.fail("更新数据已失效");
                }
            }

            // 判断是口令红包订单就拦截处理，不往下执行了
            if (AccountStatus.BID_45.equals(order.getBid())) {
                try {
                    return_code = wxRedService.processRedPwd(order, tradeNo);
                    return return_code;
                } catch (Exception ex) {
                    logger.info("三方下单支付出错：" + ex);
                }
            }

            // 判断是-网赚任务-订单就拦截处理，不往下执行了
            if (AccountStatus.BID_47.equals(order.getBid())) {
                try {
                    return wangzhuanTaskService.orderProcess(order, tradeNo);
                } catch (Exception ex) {
                    logger.info("三方下--网赚任务--单支付出错：" + ex.getMessage(), ex);
                }
            }

            // 判断是-网赚任务-上调数量/上调单价-订单就拦截处理，不往下执行了
            if (AccountStatus.BID_48.equals(order.getBid())) {
                try {
                    return wangzhuanTaskService.orderProcessAddNumOrAmount(order,tradeNo);
                }catch (Exception e){
                    logger.info("三方下-- 网赚任务-上调任务单价/任务数量--订单支付出错："+e.getMessage(),e);
                }
            }

            // 判断是-网赚任务-购买推荐、置顶 / 竞价 -订单就拦截处理，不往下执行了
            if (AccountStatus.BID_49.equals(order.getBid())) {
                try {
                    return wangzhuanTaskServiceV2.orderProcessTBR(order,tradeNo);
                }catch (Exception e){
                    logger.info("三方下-- 网赚任务-上调任务单价/任务数量--订单支付出错："+e.getMessage(),e);
                }
            }

            // 更新团购信息
            order.setSource("alipay");
//            doPayService.doGroupV3(order);
            doPayService.doNewGroupV3(order);
//            doPayService.doUserGroupon(order);

            try {
                logger.info("开始下单：--------------------------------------");
                // 福禄支付回调
                if (Objects.equals(order.getHuid(), "fuluGoods")) {
                    payFuLuGoodsOrder(order, "alipay");
                } else if (Objects.equals(order.getHuid(), "zmGoods")) {
                    payZMGoodsOrder(order, "alipay");
                } else if (Objects.equals(order.getHuid(), "lsxdGoods")) {
                    payLSXDGoodsOrder(order, "alipay");
                } else if (Objects.equals(order.getHuid(), "cost")) {
                    payCOSTGoodsOrder(order, "alipay");
                }
            } catch (Exception e) {
                logger.info("三方下单支付出错。。。。。。。。。");
            }


            // 用户购买优惠券商品
            doPayService.doUserCouponGoods(order);
            // 升级达人
            doPayService.doUserTalent(order);
            // 订单支付成功处理用户权益逻辑
            doPayService.doUserAllBid(order);

            return_code = "success";
            return return_code;
        } else {
            return return_code;
        }

        // } catch (AlipayApiException e) {
        // // TODO Auto-generated catch block
        // e.printStackTrace();
        // }

        // return return_code;
    }


    @Transactional(rollbackFor = {Exception.class})
    public String hsalipayNotify(HttpServletRequest request, HttpServletResponse response) {
        // 获取支付宝POST过来反馈信息
        String return_code = "fail";
        MallOrderNotifyLog log = new MallOrderNotifyLog();
        // try {

        Map<String, String> params = new HashMap<String, String>();
        Map<String, String[]> requestParams = request.getParameterMap();
        for (Iterator<String> iter = requestParams.keySet().iterator(); iter.hasNext();) {
            String name = (String) iter.next();
            String[] values = (String[]) requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i] : valueStr + values[i] + ",";
            }
            // 乱码解决，这段代码在出现乱码时使用。
            // valueStr = new String(valueStr.getBytes("ISO-8859-1"), "utf-8");
            params.put(name, valueStr);
        }
        log.setRemark("alipay...");
        logger.info("~~~~~~支付宝参数： " + params);
        // 移除非验证参数
        params.remove("sign_type");
        boolean signVerified = false;
        try {
            signVerified = AlipaySignature.rsaCheckV2(params, aliProperties.getHsRsaPublicKey(), "utf-8", "RSA2");
        } catch (AlipayApiException e2) {
            e2.printStackTrace();
        }
        logger.info("[支付宝支付]-支付宝回调 sign：[{}]" + JSONObject.toJSONString(signVerified));
        if (signVerified) {
            String notifyAppid = params.get("app_id");
            if (StringUtils.isBlank(notifyAppid) || !notifyAppid.equals(aliProperties.getHsAppId())) {
                logger.error("[支付宝支付]-支付宝回调：appId校验失败 appId:" + aliProperties.getHsAppId());
                logger.error("notifyAppid" + notifyAppid);
                return "fail";
            }
        }

        String outTradeNo = MapUtils.getString(params, "out_trade_no");
        String subject = MapUtils.getString(params, "subject");// 商品名称
        String sellerId = MapUtils.getString(params, "seller_id");// 卖家用户号
        String sellerEmail = MapUtils.getString(params, "seller_email");// 卖家账号
        String buyerId = MapUtils.getString(params, "buyer_id");// 卖家用户号
        String buyerEmail = MapUtils.getString(params, "buyer_email");// 卖家账号
        String totalFee = MapUtils.getString(params, "total_amount");// 交易金额
        String body = MapUtils.getString(params, "body");// 商品描述
        String gmtCreate = MapUtils.getString(params, "gmt_create");// 交易创建时间
        String gmtPayment = MapUtils.getString(params, "gmt_payment");// 交易付款时间
        String tradeNo = MapUtils.getString(params, "trade_no");// 支付宝交易号
        String tradeStatus = MapUtils.getString(params, "trade_status");// 交易状态
        String sign = MapUtils.getString(params, "sign");// 交易签名

        // 切记alipaypublickey是支付宝的公钥，请去open.alipay.com对应应用下查看。
        // boolean AlipaySignature.rsaCheckV1(Map<String, String> params, String publicKey, String charset,
        // String sign_type)
        boolean flag = true;// AlipaySignature.rsaCheckV1(params, aliProperties.getRsaPublicKey(), "utf-8","RSA2");
        logger.info("~~~~~~支付宝验证签名flag： " + flag);
        if (flag) {
            MallOrder order = orderService.findBySn(outTradeNo);
            if (order == null) {
                try {

                    log.setErrMsg("订单不存在 sn=" + outTradeNo);
                    log.setIsSuccess("0F");
                    logService.add(log);
                } catch (Exception e1) {
                    logger.error("日志添加异常: " + e1.getMessage(), e1);
                }

                return return_code;
                // return WxPayNotifyResponse.fail("订单不存在 sn=" + outTradeNo);
            }

            // 检查这个订单是否已经处理过
            if (OrderUtil.isPayStatus(order) && order.getPayId() != null) {
                try {
                    log.setErrMsg("订单已经处理过");
                    log.setIsSuccess("0F");
                    logService.add(log);
                } catch (Exception e1) {
                    logger.error("日志添加异常: " + e1.getMessage(), e1);
                }

                return return_code;
                // return WxPayNotifyResponse.success("订单已经处理成功!");
            }

            // 检查支付订单金额
            if (!order.getActualPrice().toString().equals(totalFee)) {
                try {
                    log.setErrMsg(order.getOrderSn() + " : 支付金额不符合 totalFee=" + totalFee);
                    log.setIsSuccess("0F");
                    logService.add(log);
                } catch (Exception e1) {
                    logger.error("日志添加异常: " + e1.getMessage(), e1);
                }

                return return_code;
                // return WxPayNotifyResponse.fail(order.getOrderSn() + " : 支付金额不符合 totalFee=" + totalFee);
            }

            order.setPayId(tradeNo);
            order.setPayTime(LocalDateTime.now());
            order.setOrderStatus(OrderUtil.STATUS_PAY);
            if (orderService.updateWithOptimisticLocker(order) == 0) {
                // 这里可能存在这样一个问题，用户支付和系统自动取消订单发生在同时
                // 如果数据库首先因为系统自动取消订单而更新了订单状态；
                // 此时用户支付完成回调这里也要更新数据库，而由于乐观锁机制这里的更新会失败
                // 因此，这里会重新读取数据库检查状态是否是订单自动取消，如果是则更新成支付状态。
                order = orderService.findBySn(outTradeNo);
                int updated = 0;

                // 订单自动取消
                if (OrderUtil.isAutoCancelStatus(order)) {
                    order.setPayId(tradeNo);
                    order.setPayTime(LocalDateTime.now());
                    order.setOrderStatus(OrderUtil.STATUS_PAY);

                    updated = orderService.updateWithOptimisticLocker(order);

                    // 修改销量
                    List<MallOrderGoods> mallOrderGoodsList = orderGoodsService.queryByOid(order.getId());
                    for (int i = 0; i < mallOrderGoodsList.size(); i++) {
                        MallOrderGoods mallOrderGoods = mallOrderGoodsList.get(i);
                        if (mallOrderGoods.getNumber() != null) {
                            MallGoods mallGoods = new MallGoods();
                            mallGoods.setId(mallOrderGoods.getGoodsId());
                            mallGoods.setItemsale(Integer.parseInt(mallOrderGoods.getNumber().toString()));
                            goodsService.updateMallGoodsSales(mallGoods);
                        }
                    }
                }

                try {
                    log.setRemark("订单支付通知计算完成");
                    log.setIsSuccess("1F");
                    logService.add(log);
                } catch (Exception e1) {
                    logger.error("日志添加异常: " + e1.getMessage(), e1);
                }

                // 如果updated是0，那么数据库更新失败
                if (updated == 0) {
                    return return_code;
                    // return WxPayNotifyResponse.fail("更新数据已失效");
                }
            }

            // 支付成功，有团购信息，更新团购信息
            MallGroupon groupon = grouponService.queryByOrderId(order.getId());
            if (groupon != null) {
                MallGrouponRules grouponRules = grouponRulesService.queryById(groupon.getRulesId());

                // 仅当发起者才创建分享图片
                if ("0".equals(groupon.getGrouponId())) {
                    String url = qCodeService.createGrouponShareImage(grouponRules.getGoodsName(),
                            grouponRules.getPicUrl(), groupon);
                    groupon.setShareUrl(url);
                }
                groupon.setPayed(true);
                if (grouponService.updateById(groupon) == 0) {
                    return return_code;
                    // return WxPayNotifyResponse.fail("更新数据已失效");
                }
            }

            try {
                logger.info("开始下单：--------------------------------------");
                // 福禄支付回调
                if (Objects.equals(order.getHuid(), "fuluGoods")) {
                    payFuLuGoodsOrder(order, "alipay");
                } else if (Objects.equals(order.getHuid(), "zmGoods")) {
                    payZMGoodsOrder(order, "alipay");
                } else if (Objects.equals(order.getHuid(), "lsxdGoods")) {
                    payLSXDGoodsOrder(order, "alipay");
                }
            } catch (Exception e) {
                logger.info("三方下单支付出错。。。。。。。。。");
            }

            try {
                // 购买者id
                String userId = order.getUserId();
                /** 用户购买优惠券商品 **/
                // 通过orderId 查询订单商品
                List<MallOrderGoods> orderGoodsList = orderGoodsService.queryByOid(order.getId());
                if (orderGoodsList.size() > 0) {
                    for (MallOrderGoods mallOrderGoods : orderGoodsList) {
                        String id = mallOrderGoods.getId();
                        String goodsId = mallOrderGoods.getGoodsId();
                        /** (1)通过goodsId 查询 商品信息(test2库商品数据) **/
                        try {
                            JSONObject object = goodsService.queryCouponGoodsById(goodsId);
                            if (object != null) {
                                String virtual_good = object.getString("virtual_good");
                                // 判断 商品是否属于 优惠券商品
                                if (virtual_good == "2" || "2".equals(virtual_good)) {
                                    // 若商品属于 优惠券商品,则srcGoodId 为couponId
                                    String srcGoodId = mallOrderGoods.getSrcGoodId();// 优惠券id

                                    // 查询可用的优惠券券码list
                                    MallGoodsCouponCode mallGoodsCouponCode = new MallGoodsCouponCode();
                                    mallGoodsCouponCode.setCouponId(srcGoodId);
                                    mallGoodsCouponCode.setSaleStatus(false);
                                    mallGoodsCouponCode.setDeleted(false);
                                    List<MallGoodsCouponCode> mallGoodsCouponCodeList =
                                            mallGoodsCouponCodeService.selectMallGoodsCouponCodeList(
                                                    mallGoodsCouponCode, null, null, null, null);
                                    /** (2)随机分配用户 优惠券券码 **/
                                    JSONObject couponObject = null;
                                    if (mallGoodsCouponCodeList.size() > 0) {
                                        MallGoodsCouponCode couponCode = mallGoodsCouponCodeList.get(0);
                                        couponCode.setUserId(userId);
                                        couponCode.setSaleStatus(true);
                                        mallGoodsCouponCodeService.updateMallGoodsCouponCodeV2(couponCode);

                                        /** (3)商品订单加入 优惠券券码 **/
                                        MallOrderGoods orderGoods = new MallOrderGoods();
                                        orderGoods.setId(id);
                                        orderGoods.setCardNumber(couponCode.getCouponCard()); // 卡号
                                        orderGoods.setCardPwd(couponCode.getCouponCode()); // 卡密

                                        /** (3-2)查询优惠券信息 **/
                                        try {
                                            logger.info("----------远程调用open项目接口 getCouponDetail----------");
                                            couponObject = goodsService.queryCouponById(srcGoodId);
                                            if (couponObject != null) {
                                                String endTime = couponObject.getString("endTime");
                                                String shortUrl = couponObject.getString("shortUrl");
                                                if (StringUtils.isNotEmpty(endTime)) {
                                                    orderGoods.setCardDeadline(endTime); // 卡券有效期
                                                }
                                                if (StringUtils.isNotEmpty(shortUrl)) {
                                                    orderGoods.setGoodsShortUrl(shortUrl); // 卡券短链接 (推广链接)
                                                }
                                            }
                                        } catch (Exception e) {
                                            logger.info("远程调用查询优惠券信息接口失败！");
                                        }
                                        orderGoodsService.updateById(orderGoods);
                                        logger.info("分配券码的商品信息------ " + JacksonUtils.bean2Jsn(orderGoods));
                                    }


                                    /** (4)优惠券 剩余库存减少(远程调用接口，修改test2库数据) **/
                                    if (couponObject != null) {
                                        Short number = mallOrderGoods.getNumber(); // 订单商品数量
                                        String couponSurplus = couponObject.getString("surplus");
                                        if (StringUtils.isNotEmpty(couponSurplus)) {
                                            Short surplus = Short.valueOf(couponSurplus);
                                            Integer lastSurplus = 0;
                                            if (surplus.compareTo(number) > -1) {
                                                lastSurplus = surplus - number; // 剩余数量
                                            }
                                            /** 修改优惠券库存 **/
                                            try {
                                                logger.info("----------远程调用open项目接口 updateCoupon----------");
                                                goodsService.updateCouponById(srcGoodId, lastSurplus, goodsId);
                                            } catch (Exception e) {
                                                logger.info("远程调用修改优惠券信息接口失败！");
                                            }

                                        } else {
                                            logger.info("优惠券库存不存在！");
                                        }
                                    }

                                    /** 创建用户预估分润 **/
                                    Map<String, String> map = new HashMap<>();
                                    map.put("userId", userId);
                                    map.put("bid", AccountStatus.BID_27);
                                    map.put("totalMoney", String.valueOf(order.getActualPrice()));
                                    map.put("rechargeType", CommonConstant.TASK_REWARD_CASH);
                                    map.put("orderId", order.getId());
                                    map.put("orderSn", order.getOrderSn());
                                    AccountUtils.preCharge(map);
                                }
                            }

                        } catch (Exception e) {
                            logger.info("远程调用商品信息接口失败！");
                        }

                    }
                }

            } catch (Exception e) {
                logger.error("修改优惠券商品出错: " + e.getMessage(), e);
            }

            return_code = "success";
            return return_code;
        } else {
            return return_code;
        }

        // } catch (AlipayApiException e) {
        // // TODO Auto-generated catch block
        // e.printStackTrace();
        // }

        // return return_code;
    }

    public String checkProfit(MallOrder order) {


        return null;
    }

    public void supplementSheet(String body) {
        String orderSn = JacksonUtil.parseString(body, "orderSn");
        String userId = JacksonUtil.parseString(body, "userId");

        // String channel = JacksonUtil.parseString(body, "channel");

        List<Short> status = new ArrayList<Short>();
        status.add(OrderUtil.STATUS_PAY);
        status.add(OrderUtil.STATUS_CONFIRM);
        status.add(OrderUtil.STATUS_SHIP);
        status.add(OrderUtil.STATUS_AUTO_CONFIRM);

        List<MallOrder> orders = null;

        if (StringUtils.isNotBlank(orderSn)) {
            orders = new ArrayList<MallOrder>();
            MallOrder order = orderService.findBySn(orderSn);
            orders.add(order);
        } else {
            // orders = orderService.queryByOrderStatus(userId, status, null, null);
        }

        try {
            System.out.println("查询出所有的数据 orders:" + JacksonUtils.bean2Jsn(orders));
            if (CollectionUtils.isNotEmpty(orders)) {
                for (int ix = 0; ix < orders.size(); ix++) {
                    MallOrder order = orders.get(ix);

                    order.getPrimaryDividend();
                    // 第二个true 代表是否充值
                    this.calFenXiao(order, false, true);
                }
            }
        } catch (JsonParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JsonMappingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    public void sendSms(MallOrder order, String body, MallUser fenXiaoUser, String smsContent2fenxiao) {
        // body+= "【收件人: "+order.getConsignee() + ",电话: "+ order.getMobile() + ",地址: "+
        // order.getAddress()+"】";
        String[] params = {order.getOrderSn(), order.getActualPrice().toString(), body};

        int templateId = 353450;
        // 需要发送短信的手机号码
        String[] phoneNumbers = {"13924172773", "18905740203"};
        // String[] phoneNumbers = { "15920880905" ,"18998828258" };
        // 订单支付成功通知 有新的支付订单来了！ 单号：{1} 金额：{2} 商品：{3}
        // 参数1 订单
        // 参数2 价格
        // 参数2 【商品名称;商品规格数组;数量;原系统产品ID】【收件人;电话;地址】
        TecentSmsUtil.sendForTemplate(phoneNumbers, templateId, params);

        if (fenXiaoUser != null) {
            String[] fenxiaoPhoneNumbers = {fenXiaoUser.getMobile()};
            String[] params2 = {order.getOrderSn(), order.getActualPrice().toString(), order.getConsignee(),
                    smsContent2fenxiao};
            int noteSaleMantemplateId = 364271;
            // 有新的支付订单来了！ 订单号：{1}金额：{2} 收件人 ：{3} 商品：{4}
            TecentSmsUtil.sendForTemplate(fenxiaoPhoneNumbers, noteSaleMantemplateId, params2);
        }
    }

    private void submitOrderToWpc(MallOrder order, List<MallOrderGoods> orderGoodsList) {
        boolean isWpcGoodsFlag = false; // 是否唯品仓商品

        Map<String, Integer> sizeInfo = new HashMap<String, Integer>();
        // 添加订单商品表项
        for (MallOrderGoods orderGoods : orderGoodsList) {
            // 订单商品
            MallGoods goods = goodsService.selectOneById(orderGoods.getGoodsId());
            if ("WPC".equals(goods.getChannelCode())) {
                isWpcGoodsFlag = true;
                MallGoodsProduct product = productService.findById(orderGoods.getProductId());
                sizeInfo.put(product.getSrcSizeId(), (int) orderGoods.getNumber()); // key为唯品仓对应的sizeId
            }
        }
        if (isWpcGoodsFlag) {
            WpcOrderReqVo vo = new WpcOrderReqVo();

            vo.setSizeInfo(sizeInfo);

            vo.setProvinceName(order.getProvinceName());
            vo.setCityName(order.getCityName());
            vo.setAreaName(order.getAreaName());
            // vo.setTownName(null);
            vo.setAddress(order.getAddressDetail());
            vo.setConsignee(order.getConsignee());
            vo.setMobile(order.getMobile());
            vo.setTraceId(order.getId().toString());
            vo.setClientIp(order.getClientIp());

            try {
                WpcApiRespDto respDto = wpcApiService.createOrder(vo);
                if ("200".equals(respDto.getCode())) {
                    // 提交订单成功
                    if (respDto.getData() != null && respDto.getData() instanceof Map) {
                        Map<String, Object> retData = (Map) respDto.getData();
                        if (retData.containsKey("orders")) {
                            List<Map<String, Object>> srcOrders = (List<Map<String, Object>>) retData.get("orders");
                            if (srcOrders != null && srcOrders.size() > 0) {
                                StringBuffer srcOrdersStr = new StringBuffer();
                                for (Map<String, Object> srcOrder : srcOrders) {
                                    srcOrdersStr.append(MapUtils.getString(srcOrder, "orderSn"));
                                }

                                MallOrder updateOrdervo = new MallOrder();
                                updateOrdervo.setSrcOrderIds(srcOrdersStr.toString());
                                updateOrdervo.setChannelCode("WPC");
                                updateOrdervo.setId(order.getId());
                                orderService.updateByPrimaryKeySelective(updateOrdervo);
                            }
                        }
                    }
                } else if ("600008".equals(respDto.getCode())) {
                    logger.info("唯品仓接口调用失败," + respDto.getMsg() + ";订单ID:" + order.getId());
                    throw new RuntimeException("库存不足!");
                } else {
                    logger.info("唯品仓接口调用结果:" + JacksonUtils.bean2Jsn(respDto));
                    throw new RuntimeException("库存不足!");
                }
                // } catch (ServiceException e) {
                // logger.error("提交唯品仓接口报错:"+body, e);
                // throw new RuntimeException("库存不足");
            } catch (Exception e) {
                logger.error("提交唯品仓接口报错:orderId:" + order.getId(), e);
                throw new RuntimeException("库存不足!");
            }
        }
    }

    /**
     * @param order
     * @param isSend
     * @throws Exception
     */
    public void calFenXiaoBak(MallOrder order, boolean isSend, boolean isRecharge) throws Exception {

        /** 直接分润最终累计值 **/
        BigDecimal primaryDividend = new BigDecimal(0);
        /** 间接分润最终累计值 **/
        BigDecimal secondaryDividend = new BigDecimal(0);
        /** 订单加价分润最终累计值 **/
        BigDecimal markupDividend = new BigDecimal(0);

        MallRebateRecord rebat = null;
        /** 订单加价金额分利 **/
        BigDecimal shAddAmount = null;
        /** 订单直接分利 **/
        BigDecimal getR1amount = null;
        /** 订单间接分利 **/
        BigDecimal getR2amount = null;
        // 有分销员ID的时候订单需要计算分销金额
        String tpid = order.getTpid();
        String shId = order.getShId();

        String body = "";
        String smsContent2fenxiao = "";
        MallUser fenXiaoUser = null;
        /** 先查询商品 **/
        List<MallOrderGoods> orderGoods = orderGoodsService.queryByOid(order.getId());

        /** 查询商品购买人 **/
        MallUser user = userService.findById(order.getUserId());
        int loop = CollectionUtils.isNotEmpty(orderGoods) ? orderGoods.size() : 0;
        List<String> goodIds = new ArrayList<String>();
        // 购买的产品Id集合
        List<String> productIds = new ArrayList<String>();

        for (int i = 0; i < loop; i++) {
            MallOrderGoods orderGood = orderGoods.get(i);


            body += "【商品: " + orderGood.getGoodsName() + ",数量: " + orderGood.getNumber() + ",规格: "
                    + Arrays.toString(orderGood.getSpecifications()) + ",原系统产品ID: " + orderGood.getSrcGoodId()
                    + "】";
            smsContent2fenxiao += "【商品: " + orderGood.getGoodsName() + ",数量: " + orderGood.getNumber() + ",规格: "
                    + Arrays.toString(orderGood.getSpecifications()) + "】";

            String goodId = orderGood.getGoodsId() != null ? orderGood.getGoodsId() : "0";

            String productId = orderGood.getProductId() != null ? orderGood.getProductId() : "0";
            productIds.add(productId);
            goodIds.add(goodId);
        }

        /*** 取出订单的所有产品 **/
        List<MallGoods> goods = goodsService.findByIdList(goodIds);

        // List<MallGoodsProduct> products = productService.findByIds(productIds);
        int _loop = CollectionUtils.isNotEmpty(goods) ? goods.size() : 0;
        Short number = 1;
        FcAccount fenXiaoUserAccount = null;

        if (StringUtils.isBlank(tpid)) {
            tpid = order.getUserId().toString();
        }

        /********************************* 计算单个商品的返利数据 *********************************************/
        for (int i = 0; i < _loop; i++) {
            // MallGoodsProduct _product = products.get(i);

            MallGoods _goods = goods.get(i);
            /** 单个商品如果没有设置分润比例则默认给 ***/
            BigDecimal tcRetailPrice = _goods.getRetailPrice();
            BigDecimal firstRatio = _goods.getFirstRatio() != null ? _goods.getFirstRatio() : new BigDecimal(0.1);
            BigDecimal secondRatio = _goods.getSecondRatio() != null ? _goods.getSecondRatio() : new BigDecimal(0.05);
            String calModel = _goods.getCalModel() != null ? _goods.getCalModel() : "1R";

            String productId = "0";
            BigDecimal couponPrice = null;
            // 分润比例不为0的时候才记入充值
            if (firstRatio.compareTo(new BigDecimal(0)) != 0) {
                // 寻找订单购物的数量
                for (int j = 0; j < loop; j++) {
                    MallOrderGoods orderGood = orderGoods.get(j);
                    if (_goods.getId() != null && _goods.getId().equals(orderGood.getGoodsId())) {
                        number = orderGood.getNumber();
                        productId = orderGood.getProductId();

                        // 优惠金额
                        couponPrice = orderGood.getCouponPrice();
                    }
                }

                if (!"0".equals(productId)) {
                    MallGoodsProduct product = productService.findById(productId);
                    // 实际要取产品的金额
                    tcRetailPrice = product.getPrice();
                    if (couponPrice != null) {
                        tcRetailPrice = tcRetailPrice.subtract(couponPrice);
                    }

                    /************* 计算订单加价部分金额 *************/
                    if (shId != null) {
                        // 取到加价规则
                        if (rebat == null) {
                            rebat = fenXiaoService.getMallRebateRecordById(shId);
                        }

                        // 说明用户是会员，那么支付的金额打折(9.5折)同样订单加价部分分润9.5折
                        if (MallConstant.USER_LEVEL_VIP == user.getUserLevel()) {
                            shAddAmount = GoodsUtils.getAddAmount(rebat, tcRetailPrice).multiply(new BigDecimal(0.95))
                                    .setScale(2, BigDecimal.ROUND_HALF_UP);
                        } else {
                            shAddAmount = GoodsUtils.getAddAmount(rebat, tcRetailPrice);
                        }
                        // 如果非等于0的话就说明加价了，需要写入加价部分给分销商
                        // 订单加价累计
                        markupDividend = markupDividend.add(shAddAmount);
                    }

                }

                // 通过比例的方式计算分润
                if ("1R".equals(calModel)) {
                    // 计算比例
                    BigDecimal calamount1 = tcRetailPrice.multiply(firstRatio).setScale(2, BigDecimal.ROUND_HALF_UP);
                    BigDecimal calamount2 = tcRetailPrice.multiply(secondRatio).setScale(2, BigDecimal.ROUND_HALF_UP);
                    getR1amount = calamount1.multiply(new BigDecimal(number)).setScale(2, BigDecimal.ROUND_HALF_UP);
                    getR2amount = calamount2.multiply(new BigDecimal(number)).setScale(2, BigDecimal.ROUND_HALF_UP);
                } else {
                    // 固定值
                    BigDecimal calamount1 = firstRatio;
                    BigDecimal calamount2 = secondRatio;
                    getR1amount = calamount1.multiply(new BigDecimal(number)).setScale(2, BigDecimal.ROUND_HALF_UP);
                    getR2amount = calamount2.multiply(new BigDecimal(number)).setScale(2, BigDecimal.ROUND_HALF_UP);
                }

                // 如果分销id为空那么自己就是分销员


                if (fenXiaoUser == null) {
                    fenXiaoUser = userService.findById(tpid);
                }

                // 上级分销员
                String firstLeader = fenXiaoUser.getFirstLeader();
                Map<String, Object> paramsMap = new HashMap<String, Object>();
                paramsMap.put("channel", "wx");
                paramsMap.put("orderId", order.getId());
                paramsMap.put("orderSn", order.getOrderSn());
                paramsMap.put("buyerId", order.getUserId());
                paramsMap.put("goodsId", _goods.getId());
                /** 计算一级分销的分润金额 计算本分销员的收益 **/
                fenXiaoUserAccount = accountService.getAccountByKey(tpid);
                if (fenXiaoUserAccount != null) {
                    paramsMap.put("dType", MallConstant.DISTRIBUTION_TYPE_00);
                    FcAccount rechargeAccount1 = new FcAccount();
                    rechargeAccount1.setAccountId(fenXiaoUserAccount.getAccountId());
                    rechargeAccount1.setAmount(getR1amount);

                    primaryDividend = primaryDividend.add(getR1amount);
                    if (isRecharge) {
                        accountService.accountTradeRecharge(fenXiaoUserAccount, rechargeAccount1, paramsMap, "");
                    }

                }

                /** 计算二级分销的分润金额 **/
                FcAccount account2 = accountService.getAccountByKey(firstLeader);
                if (account2 != null) {
                    paramsMap.put("dType", MallConstant.DISTRIBUTION_TYPE_01);
                    FcAccount rechargeAccount2 = new FcAccount();
                    rechargeAccount2.setAccountId(account2.getAccountId());
                    rechargeAccount2.setAmount(getR2amount);

                    secondaryDividend = secondaryDividend.add(getR2amount);
                    if (isRecharge) {
                        accountService.accountTradeRecharge(account2, rechargeAccount2, paramsMap, "");
                    }
                }
                logger.info("计算商品分销金额成功...................");
            }

        }

        /*************************** 计算订单加价分润 *********************************/
        for (int i = 0; i < _loop; i++) {
            // MallGoodsProduct _product = products.get(i);
            MallGoods _goods = goods.get(i);
            // BigDecimal tcRetailPrice = _goods.getRetailPrice();

        }

        System.out.println("markupDividend计算出的订单加价部分 :" + markupDividend.toString());

        if (markupDividend.compareTo(new BigDecimal(0)) != 0) {
            // 加价部分全部算给一级分销商自己
            fenXiaoUserAccount = accountService.getAccountByKey(tpid);
            Map<String, Object> paramsMap = new HashMap<String, Object>();
            paramsMap.put("channel", "wx");
            paramsMap.put("orderId", order.getId());
            paramsMap.put("orderSn", order.getOrderSn());
            paramsMap.put("buyerId", order.getUserId());
            paramsMap.put("dType", "D11");

            System.out.println("查询出账户fenXiaoUserAccount " + JacksonUtils.bean2Jsn(fenXiaoUserAccount));
            if (fenXiaoUserAccount != null) {
                FcAccount rechargeAccount1 = new FcAccount();
                rechargeAccount1.setAccountId(fenXiaoUserAccount.getAccountId());
                rechargeAccount1.setAmount(markupDividend);
                if (isRecharge) {
                    accountService.accountTradeRecharge(fenXiaoUserAccount, rechargeAccount1, paramsMap, "");
                }
            }
            logger.info("计算订单分销金额成功...................");
        }

        /********************************* 每个商品的返利金额计算结束 *********************************************/

        /*********************************
         * 计算整个订单的加价部分 (2019/7/4 重写订单分润逻辑，从产品中计算，设计到会员折扣的问题)*************************************** if (shId
         * != null) { BigDecimal actualPrice = order.getActualPrice(); // 取到加价规则 MallRebateRecord rebat =
         * fenXiaoService.getMallRebateRecordById(shId); shAddAmount = GoodsUtils.getAddAmount(rebat,
         * actualPrice); // 如果非等于0的话就说明加价了，需要写入加价部分给分销商 if (shAddAmount.compareTo(new BigDecimal(0)) != 0) {
         * // 加价部分全部算给一级分销商自己 fenXiaoUserAccount = accountService.getAccountByKey(tpid); Map<String, Object>
         * paramsMap = new HashMap<String, Object>(); paramsMap.put("channel", "wx");
         * paramsMap.put("orderId", order.getId()); paramsMap.put("orderSn", order.getOrderSn());
         * paramsMap.put("buyerId", order.getUserId()); paramsMap.put("dType", "D11");
         *
         * if (fenXiaoUserAccount != null) { FcAccount rechargeAccount1 = new FcAccount();
         * rechargeAccount1.setAccountId(fenXiaoUserAccount.getAccountId());
         * rechargeAccount1.setAmount(shAddAmount); accountService.accountTradeRecharge(fenXiaoUserAccount,
         * rechargeAccount1, paramsMap); } logger.info("计算订单分销金额成功..................."); } }
         ******/

        /********************************* 计算整个订单的加价部分结束 *********************************************/


        /********************************* 计算完成将计算后的金额写入订单中 *********************************************/
        MallOrder uOrder = new MallOrder();
        uOrder.setId(order.getId());
        uOrder.setPrimaryDividend(primaryDividend);
        uOrder.setSecondaryDividend(secondaryDividend);
        uOrder.setMarkupDividend(markupDividend);
        uOrder.setIsCal("1");
        orderService.updateByPrimaryKeySelective(uOrder);

        // 是否发送通知
        if (isSend) {
            // 支付订单通知
            try {
                sendSms(order, body, fenXiaoUser, smsContent2fenxiao);
            } catch (Exception e) {
                logger.error("", e);
            }
            try {
                submitOrderToWpc(order, orderGoods);
            } catch (Exception e) {
                logger.error("", e);
            }
        }
    }


    private static BigDecimal ZERO = new BigDecimal(0);

    /**
     * @param order
     * @param isSend
     * @throws Exception
     */
    public void calFenXiao(MallOrder order, boolean isSend, boolean isRecharge) throws Exception {
        /** 直接分润最终累计值 **/
        BigDecimal primaryDividend = new BigDecimal(0);
        /** 间接分润最终累计值 **/
        BigDecimal secondaryDividend = new BigDecimal(0);
        /** 订单加价分润最终累计值 **/
        BigDecimal markupDividend = new BigDecimal(0);

        /** 分销员账户 **/
        FcAccount primaryUserAccount = null;
        FcAccount secondaryUserAccount = null;

        MallRebateRecord rebat = null;

        // 有分销员ID的时候订单需要计算分销金额
        String shId = order.getShId();

        String body = "";
        String smsContent2fenxiao = "";
        MallUser fenXiaoUser = null;
        /** 先查询商品 **/
        List<MallOrderGoods> orderGoods = orderGoodsService.queryByOid(order.getId());

        /** 查询商品购买人 **/
        MallUser user = userService.findById(order.getUserId());

        int loop = CollectionUtils.isNotEmpty(orderGoods) ? orderGoods.size() : 0;
        List<String> goodIds = new ArrayList<String>();
        // 购买的产品Id集合
        List<String> productIds = new ArrayList<String>();

        for (int i = 0; i < loop; i++) {
            MallOrderGoods orderGood = orderGoods.get(i);
            String goodId = orderGood.getGoodsId() != null ? orderGood.getGoodsId() : "0";
            String productId = orderGood.getProductId() != null ? orderGood.getProductId() : "0";
            productIds.add(productId);
            goodIds.add(goodId);
        }

        /*** 取出订单的所有产品 **/
        List<MallGoods> goods = goodsService.findByIdList(goodIds);
        /** 获取所有的SKU数据 ***/
        List<MallGoodsProduct> products = productService.findByIds(productIds);

        int _loop = CollectionUtils.isNotEmpty(products) ? products.size() : 0;
        String tpid = StringUtils.isBlank(order.getTpid()) ? order.getUserId().toString() : order.getTpid();


        /********************************* 计算单个商品的返利数据 *********************************************/
        for (int i = 0; i < _loop; i++) {
            MallGoodsProduct _product = products.get(i);
            MallGoods _goods = OrderCalculationUtil.getGoods(goods, _product);

            /** 单个商品如果没有设置分润比例则默认给0.1和0.05 ***/
            BigDecimal tcRetailPrice = _product.getPrice();
            BigDecimal firstRatio = _goods.getFirstRatio() != null ? _goods.getFirstRatio() : new BigDecimal(0.1);
            BigDecimal secondRatio = _goods.getSecondRatio() != null ? _goods.getSecondRatio() : new BigDecimal(0.05);
            String calModel = _goods.getCalModel() != null ? _goods.getCalModel() : "1R";

            // 分润比例不为0的时候才记入充值
            if (firstRatio.compareTo(ZERO) != 0 && secondRatio.compareTo(ZERO) != 0) {
                /** 获取sku购买数量 **/
                Short number = OrderCalculationUtil.getBuyNumber(orderGoods, _product);
                /** 计算优惠后的金额 **/
                tcRetailPrice = OrderCalculationUtil.getAmountAfterPreference(tcRetailPrice, orderGoods, _product);

                /** 直接分润 **/
                BigDecimal getR1amount =
                        OrderCalculationUtil.getAmountOfShare(calModel, tcRetailPrice, number, firstRatio);
                /** 间接分润 **/
                BigDecimal getR2amount =
                        OrderCalculationUtil.getAmountOfShare(calModel, tcRetailPrice, number, secondRatio);

                /** 累加直接分润总金额 **/
                primaryDividend = primaryDividend.add(getR1amount);
                /** 累加间接分润总金额 **/
                secondaryDividend = secondaryDividend.add(getR2amount);

                /** 直接分销员 **/
                fenXiaoUser = fenXiaoUser != null ? fenXiaoUser : userService.findById(tpid);
                /** 间接分销员 **/
                String firstLeader = fenXiaoUser.getFirstLeader();

                /** 直接分润账户产品分润充值 **/
                primaryUserAccount =
                        primaryUserAccount != null ? primaryUserAccount : accountService.getAccountByKey(tpid);
                OrderCalculationUtil.shareAmountRecharge(MallConstant.DISTRIBUTION_TYPE_00, accountService,
                        primaryUserAccount, order, _goods, _product, getR1amount, isRecharge);

                /** 间接分润账户充值 **/
                secondaryUserAccount = secondaryUserAccount != null ? secondaryUserAccount
                        : accountService.getAccountByKey(firstLeader);
                OrderCalculationUtil.shareAmountRecharge(MallConstant.DISTRIBUTION_TYPE_01, accountService,
                        secondaryUserAccount, order, _goods, _product, getR2amount, isRecharge);
            }

            /** 自定义加价分润部分,里面包含扣除VIP折扣部分 **/
            rebat = rebat != null ? rebat : fenXiaoService.getMallRebateRecordById(shId);
            BigDecimal getShAmount =
                    OrderCalculationUtil.getCustomerAddAmount(fenXiaoService, tcRetailPrice, rebat, user);
            /** 累加自定义加价总金额 **/
            markupDividend = markupDividend.add(getShAmount);

            /** 直接分润账户自定义加价部分充值 **/
            primaryUserAccount = primaryUserAccount != null ? primaryUserAccount : accountService.getAccountByKey(tpid);
            OrderCalculationUtil.shareAmountRecharge(MallConstant.DISTRIBUTION_TYPE_11, accountService,
                    primaryUserAccount, order, _goods, _product, getShAmount, isRecharge);
        }
        /********************************* 计算整个订单的加价部分结束 *********************************************/


        /********************************* 计算完成将计算后的金额写入订单中 *********************************************/
        MallOrder uOrder = new MallOrder();
        uOrder.setId(order.getId());
        uOrder.setPrimaryDividend(primaryDividend);
        uOrder.setSecondaryDividend(secondaryDividend);
        uOrder.setMarkupDividend(markupDividend);
        uOrder.setIsCal("1");
        orderService.updateByPrimaryKeySelective(uOrder);

        // 是否发送通知
        if (isSend) {
            for (int i = 0; i < loop; i++) {
                MallOrderGoods orderGood = orderGoods.get(i);
                body += "【商品: " + orderGood.getGoodsName() + ",数量: " + orderGood.getNumber() + ",规格: "
                        + Arrays.toString(orderGood.getSpecifications()) + ",原系统产品ID: "
                        + orderGood.getSrcGoodId() + "】";
                smsContent2fenxiao += "【商品: " + orderGood.getGoodsName() + ",数量: " + orderGood.getNumber() + ",规格: "
                        + Arrays.toString(orderGood.getSpecifications()) + "】";
            }
            // 支付订单通知
            try {
                sendSms(order, body, fenXiaoUser, smsContent2fenxiao);
            } catch (Exception e) {
                logger.error("", e);
            }
            try {
                submitOrderToWpc(order, orderGoods);
            } catch (Exception e) {
                logger.error("", e);
            }
        }
    }

    /**
     * 订单申请退款
     * <p>
     * 1. 检测当前订单是否能够退款； 2. 设置订单申请退款状态。
     *
     * @param userId 用户ID
     * @param body 订单信息，{ orderId：xxx }
     * @return 订单退款操作结果
     * @throws Exception
     */
    public Object refund(String userId, String body) throws Exception {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        String orderId = JacksonUtil.parseString(body, "orderId");
        if (orderId == null) {
            // 采购订单
            MallPurchaseOrder order1 = mallPurchaseOrderService.getById(orderId);
            if (!order1.getUserId().equals(userId)) {
                return ResponseUtil.badArgumentValue("订单不属于本人,不能取消");
            }
            OrderHandleOption handleOption = OrderUtil.build(order1);
            if (!handleOption.isRefund()) {
                return ResponseUtil.fail(ORDER_INVALID_OPERATION, "订单不能取消");
            }

            accountService.refund(MallConstant.REFUND_TYPE_ALL, userId, null, orderId, order1);

            // 设置订单申请退款状态
            order1.setOrderStatus(OrderUtil.STATUS_REFUND.intValue());
            if (!mallPurchaseOrderService.updateById(order1)) {
                return ResponseUtil.updatedDateExpired();
            }

            return ResponseUtil.ok();
        }

        MallOrder order = orderService.findById(orderId);
        if (order == null) {
            return ResponseUtil.badArgument();
        }
        if (!order.getUserId().equals(userId)) {
            return ResponseUtil.badArgumentValue();
        }

        OrderHandleOption handleOption = OrderUtil.build(order);
        if (!handleOption.isRefund()) {
            return ResponseUtil.fail(ORDER_INVALID_OPERATION, "订单不能取消");
        }

        // TODO 发送邮件和短信通知，这里采用异步发送
        // 有用户申请退款，邮件通知运营人员
        // notifyService.notifyMail("退款申请", order.toString());


        /********* 上面部分完全可以为C端电商退单逻辑，下面增加分销商退款逻辑 *********/
        accountService.refund(MallConstant.REFUND_TYPE_ALL, userId, null, orderId, order);


        // 设置订单申请退款状态
        order.setOrderStatus(OrderUtil.STATUS_REFUND);
        if (orderService.updateWithOptimisticLocker(order) == 0) {
            return ResponseUtil.updatedDateExpired();
        }

        return ResponseUtil.ok();
    }

    public Object manualRefund(String body) throws Exception {
        String orderId = JacksonUtil.parseString(body, "orderId");
        if (orderId == null) {
            return ResponseUtil.badArgument();
        }

        MallOrder order = orderService.findById(orderId);
        if (order == null) {
            return ResponseUtil.badArgument();
        }

        OrderHandleOption handleOption = OrderUtil.build(order);
        if (!handleOption.isRefund()) {
            return ResponseUtil.fail(ORDER_INVALID_OPERATION, "订单不能取消");
        }

        // TODO 发送邮件和短信通知，这里采用异步发送
        // 有用户申请退款，邮件通知运营人员
        // notifyService.notifyMail("退款申请", order.toString());


        /********* 上面部分完全可以为C端电商退单逻辑，下面增加分销商退款逻辑 *********/
        Object str = accountService.refund(MallConstant.REFUND_TYPE_ALL, null, null, orderId, order);
        System.out.println("退账情况: " + str);

        // 设置订单申请退款状态
        order.setOrderStatus(OrderUtil.STATUS_REFUND);
        if (orderService.updateWithOptimisticLocker(order) == 0) {
            return ResponseUtil.updatedDateExpired();
        }

        return ResponseUtil.ok();
    }

    /**
     * 确认收货
     * <p>
     * 1. 检测当前订单是否能够确认收货； 2. 设置订单确认收货状态。
     *
     * @param userId 用户ID
     * @param body 订单信息，{ orderId：xxx }
     * @return 订单操作结果
     */
    public Object confirm(String userId, String body) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        String orderId = JacksonUtil.parseString(body, "orderId");
        if (orderId == null) {
            return ResponseUtil.badArgument();
        }

        MallOrder order = orderService.findById(orderId);
        if (order == null) {
            // 采购订单
            MallPurchaseOrder order1 = mallPurchaseOrderService.getById(orderId);
            if (!order1.getUserId().equals(userId)) {
                return ResponseUtil.badArgumentValue("订单不属于本人,不能取消");
            }
            if (!order1.getUserId().equals(userId)) {
                return ResponseUtil.badArgumentValue();
            }

            OrderHandleOption handleOption = OrderUtil.build(order1);
            if (!handleOption.isConfirm()) {
                return ResponseUtil.fail(ORDER_INVALID_OPERATION, "订单不能确认收货");
            }

            Short comments = orderGoodsService.getComments(orderId);
            order1.setComments(comments.intValue());

            order1.setOrderStatus(OrderUtil.STATUS_CONFIRM.intValue()); // 自营订单是已收货，第三方平台是已结算状态
            order1.setConfirmTime(LocalDateTime.now());
            if (!mallPurchaseOrderService.updateById(order1)) {
                return ResponseUtil.updatedDateExpired();
            }
            return ResponseUtil.ok();
        }
        if (!order.getUserId().equals(userId)) {
            return ResponseUtil.badArgumentValue();
        }

        OrderHandleOption handleOption = OrderUtil.build(order);
        if (!handleOption.isConfirm()) {
            return ResponseUtil.fail(ORDER_INVALID_OPERATION, "订单不能确认收货");
        }

        Short comments = orderGoodsService.getComments(orderId);
        order.setComments(comments);

        order.setOrderStatus(OrderUtil.STATUS_CONFIRM); // 自营订单是已收货，第三方平台是已结算状态
        order.setConfirmTime(LocalDateTime.now());
        if (orderService.updateWithOptimisticLocker(order) == 0) {
            return ResponseUtil.updatedDateExpired();
        }

        // 批量修改指定orderId的 预充值订单状态
        FcAccountPrechargeBill rechargeBill = new FcAccountPrechargeBill();
        rechargeBill.setBillStatus(AccountStatus.BILL_STATUS_01);
        rechargeBill.setCheckStatus("1"); // 审核状态（0未审核 1待审核 2审核通过 3审核不通过）
        rechargeBill.setCheckDate(LocalDateTime.now());
        fcAccountPrechargeBillService.updateByOrderId(orderId, rechargeBill);
        return ResponseUtil.ok();
    }

    /**
     * 删除订单
     * <p>
     * 1. 检测当前订单是否可以删除； 2. 删除订单。
     *
     * @param userId 用户ID
     * @param body 订单信息，{ orderId：xxx }
     * @return 订单操作结果
     */
    public Object delete(String userId, String body) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        String orderId = JacksonUtil.parseString(body, "orderId");
        if (orderId == null) {
            return ResponseUtil.badArgument();
        }

        MallOrder order = orderService.findById(orderId);
        if (order == null) {
            // 采购订单
            MallPurchaseOrder order1 = mallPurchaseOrderService.getById(orderId);

            if (!order1.getUserId().equals(userId)) {
                return ResponseUtil.badArgumentValue();
            }

            OrderHandleOption handleOption = OrderUtil.build(order1);
            if (!handleOption.isDelete()) {
                return ResponseUtil.fail(ORDER_INVALID_OPERATION, "订单不能删除");
            }

            // 订单order_status没有字段用于标识删除
            // 而是存在专门的delete字段表示是否删除
            mallPurchaseOrderService.removeById(orderId);

            return ResponseUtil.ok();
        }
        if (!order.getUserId().equals(userId)) {
            return ResponseUtil.badArgumentValue();
        }

        OrderHandleOption handleOption = OrderUtil.build(order);
        if (!handleOption.isDelete()) {
            return ResponseUtil.fail(ORDER_INVALID_OPERATION, "订单不能删除");
        }

        // 订单order_status没有字段用于标识删除
        // 而是存在专门的delete字段表示是否删除
        orderService.deleteById(orderId);

        return ResponseUtil.ok();
    }

    /**
     * 待评价订单商品信息
     *
     * @param userId 用户ID
     * @param orderId 订单ID
     * @param goodsId 商品ID
     * @return 待评价订单商品信息
     */
    public Object goods(String userId, String orderId, String goodsId) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }

        MallOrder order = orderService.findById(orderId);
        if (order == null) {
            // 采购订单
            QueryWrapper<MallPurchaseOrderGoods> mallPurchaseOrderGoodsQueryWrapper = new QueryWrapper<>();
            mallPurchaseOrderGoodsQueryWrapper.eq("order_id", order.getId());
            mallPurchaseOrderGoodsQueryWrapper.eq("goods_id", goodsId);
            List<MallPurchaseOrderGoods> orderGoodsList = mallPurchaseOrderGoodsService.list(mallPurchaseOrderGoodsQueryWrapper);

            int size = orderGoodsList.size();

            Assert.state(size < 2, "存在多个符合条件的订单商品");

            if (size == 0) {
                return ResponseUtil.badArgumentValue();
            }

            MallPurchaseOrderGoods orderGoods = orderGoodsList.get(0);
            return ResponseUtil.ok(orderGoods);
        }
        List<MallOrderGoods> orderGoodsList = orderGoodsService.findByOidAndGid(orderId, goodsId);
        int size = orderGoodsList.size();

        Assert.state(size < 2, "存在多个符合条件的订单商品");

        if (size == 0) {
            return ResponseUtil.badArgumentValue();
        }

        MallOrderGoods orderGoods = orderGoodsList.get(0);
        return ResponseUtil.ok(orderGoods);
    }

    /**
     * 评价订单商品
     * <p>
     * 确认商品收货或者系统自动确认商品收货后7天内可以评价，过期不能评价。
     *
     * @param userId 用户ID
     * @param body 订单信息，{ orderId：xxx }
     * @return 订单操作结果
     */
    public Object comment(String userId, String body) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }

        String orderGoodsId = JacksonUtil.parseString(body, "orderGoodsId");
        if (orderGoodsId == null) {
            return ResponseUtil.badArgument();
        }
        MallOrderGoods orderGoods = orderGoodsService.findById(orderGoodsId);
        if (orderGoods == null) {
            return ResponseUtil.badArgumentValue();
        }
        String orderId = orderGoods.getOrderId();
        MallOrder order = orderService.findById(orderId);
        if (order == null) {
            return ResponseUtil.badArgumentValue();
        }
        Short orderStatus = order.getOrderStatus();
        if (!OrderUtil.isConfirmStatus(order) && !OrderUtil.isAutoConfirmStatus(order)) {
            return ResponseUtil.fail(ORDER_INVALID_OPERATION, "当前商品不能评价");
        }
        if (!order.getUserId().equals(userId)) {
            return ResponseUtil.fail(ORDER_INVALID, "当前商品不属于用户");
        }
        String commentId = orderGoods.getComment();
        if (!"-1".equals(commentId)) {
            return ResponseUtil.fail(ORDER_COMMENT_EXPIRED, "当前商品评价时间已经过期");
        }
        if (!"0".equals(commentId)) {
            return ResponseUtil.fail(ORDER_COMMENTED, "订单商品已评价");
        }

        String content = JacksonUtil.parseString(body, "content");
        Integer star = JacksonUtil.parseInteger(body, "star");
        if (star == null || star < 0 || star > 5) {
            return ResponseUtil.badArgumentValue();
        }
        Boolean hasPicture = JacksonUtil.parseBoolean(body, "hasPicture");
        List<String> picUrls = JacksonUtil.parseStringList(body, "picUrls");
        if (hasPicture == null || !hasPicture) {
            picUrls = new ArrayList<>(0);
        }

        // 1. 创建评价
        MallComment comment = new MallComment();
        comment.setUserId(userId);
        comment.setType((byte) 0);
        comment.setValueId(orderGoods.getGoodsId());
        comment.setStar((byte) star.shortValue());
        comment.setContent(content);
        comment.setHasPicture(hasPicture);
        comment.setPicUrls(picUrls.toArray(new String[] {}));
        commentService.save(comment);

        // 2. 更新订单商品的评价列表
        orderGoods.setComment(comment.getId());
        orderGoodsService.updateById(orderGoods);

        // 3. 更新订单中未评价的订单商品可评价数量
        Short commentCount = order.getComments();
        if (commentCount > 0) {
            commentCount--;
        }
        order.setComments(commentCount);
        orderService.updateWithOptimisticLocker(order);

        return ResponseUtil.ok();
    }


    /**
     * 福禄商品充值回调
     *
     * @return
     */
    public Object payNotifyByFuLu(Map<String, Object> request) {
        logger.info("福禄订单充值回调开始---------------------------------");
        String customer_order_no = MapUtils.getString(request, "customer_order_no");
        String order_status = MapUtils.getString(request, "order_status");
        if (StringUtils.isBlank(customer_order_no)) {
            logger.info("福禄 回调参数customer_order_no为空：" + customer_order_no);
            return ResponseUtil.fail();
        }
        if (StringUtils.isBlank(order_status)) {
            logger.info("福禄 回调参数order_status为空：" + order_status);
            return ResponseUtil.fail();
        }
        MallOrderNotifyLog log = new MallOrderNotifyLog();
        log.setOrderId(customer_order_no);
        log.setRemark(request.toString());
        log.setWxParams("fulu");
        log.setRetWx("订单--充值回调");

        MallOrder mallOrder = new MallOrder();
        mallOrder.setId(customer_order_no);
        boolean success = Objects.equals(order_status, "success");
        if (success) {
            log.setIsSuccess("成功");
            mallOrder.setOrderThreeStatus(OrderUtil.STATUS_RESULT_SUCCESS);
        } else {
            log.setIsSuccess("失败");
            mallOrder.setOrderThreeStatus(OrderUtil.STATUS_RESULT_FAIL);
        }

        MallOrder oneMallOrder = orderService.findById(customer_order_no);
        if (oneMallOrder == null) {
            logger.info("福禄回调没有查询到该订单的存在-------------------------------");
            // 失败
            log.setIsSuccess("失败");
            log.setErrMsg("福禄回调没有查询到该订单的存在MallOrder：" + customer_order_no);
            logService.add(log);
            return "fail";
        }
        logService.add(log);

        // 福禄商品退款自动退款接入，并且调整定价
        if (Objects.equals(order_status, "03")) {
            // 支付失败
            logger.info("----------------------福禄开始在线退款，订单id=" + oneMallOrder.getId());
            wxOrderRefundService.refundQY(oneMallOrder);
        } else if (success) {
            // 支付成功
            logger.info("----------------------福禄支付成功，开始处理业务，订单id=" + oneMallOrder.getId());
            if (OrderUtil.STATUS_RESULT_SUCCESS.equals(oneMallOrder.getOrderThreeStatus())) {
                logger.info("福禄订单已支付成功，重复了，驳回");
                return "fail";
            } else if (OrderUtil.STATUS_PAY.equals(oneMallOrder.getOrderStatus())
                    && !OrderUtil.STATUS_RESULT_SUCCESS.equals(oneMallOrder.getOrderThreeStatus())) {
                logger.info("福禄开始处理业务");
                orderService.updateByPrimaryKeySelective(mallOrder); // 更新回调后商品状态
                AccountQYUtils.preChargeQYStart(mallOrder.getId()); // 调用权益分润
            } else {
                logger.info("福禄未知情况，order_status" + order_status);
            }
        } else {
            logger.info("福禄未知参数order_status状态：" + order_status);
        }

        return success ? ResponseUtil.ok() : ResponseUtil.fail();
    }

    /**
     * 中闵充值结果回调
     *
     * @return
     */
    public Object payNotifyByZM(Map<String, Object> request) {
        logger.info("中闵充值结果回调-------------------------");
        String customer_order_no = String.valueOf(request.get("customerOrderNo"));
        String order_status = String.valueOf(request.get("status"));
        String cards = String.valueOf(request.get("cards"));

        if (StringUtils.isBlank(customer_order_no)) {
            logger.info("中闽在线回调参数customer_order_no为空：" + customer_order_no);
            return ResponseUtil.fail();
        }
        if (StringUtils.isBlank(order_status)) {
            logger.info("中闽在线回调参数order_status为空：" + order_status);
            return ResponseUtil.fail();
        }
        MallOrderNotifyLog log = new MallOrderNotifyLog();


        MallOrder mallOrder = new MallOrder();
        mallOrder.setId(customer_order_no);
        mallOrder.setUpdateTime(LocalDateTime.now());
        boolean success = Objects.equals(order_status, "success");
        if (success) {
            log.setIsSuccess("成功");
            mallOrder.setOrderThreeStatus(OrderUtil.STATUS_RESULT_SUCCESS);
        } else {
            log.setIsSuccess("失败");
            mallOrder.setOrderThreeStatus(OrderUtil.STATUS_RESULT_FAIL);
        }
        log.setRetWx("订单--充值回调");
        log.setOrderId(customer_order_no);
        log.setRemark(request.toString());
        log.setWxParams("zm");

        MallOrder oneMallOrder = orderService.findById(customer_order_no);
        if (oneMallOrder == null) {
            logger.info("中闽在线回调没有查询到该订单的存在-------------------------------");
            // 失败
            log.setIsSuccess("失败");
            log.setErrMsg("中闽在线回调没有查询到该订单的存在MallOrder：" + customer_order_no);
            logService.add(log);

            return ResponseUtil.fail(400, "中闽在线回调没有查询到该订单的存在");
        }

        // 中闽在线商品退款自动退款接入，并且调整定价
        if (!success) {
            // 支付失败
            logger.info("----------------------中闽在线开始在线退款，订单id=" + oneMallOrder.getId());
            wxOrderRefundService.refundQY(oneMallOrder);
        } else {
            // 支付成功
            logger.info("----------------------中闽在线支付成功，开始处理业务，订单id=" + oneMallOrder.getId());
            if (OrderUtil.STATUS_RESULT_SUCCESS.equals(oneMallOrder.getOrderThreeStatus())) {
                logger.info("中闽在线订单已支付成功，重复了，驳回");
                return "fail";
            } else if (OrderUtil.STATUS_PAY.equals(oneMallOrder.getOrderStatus())
                    && !OrderUtil.STATUS_RESULT_SUCCESS.equals(oneMallOrder.getOrderThreeStatus())) {
                logger.info("中闽在线开始处理业务");
                orderService.updateByPrimaryKeySelective(mallOrder); // 更新回调后商品状态
                AccountQYUtils.preChargeQYStart(mallOrder.getId()); // 调用权益分润

                // 更新卡密信息
                if (StringUtils.isNotBlank(cards) && !Objects.equals(cards, "null")) {
                    logger.info("返回卡密信息");
                    List<MallOrderGoods> mallOrderGoods = orderGoodsService.queryByOid(customer_order_no);
                    if (CollectionUtils.isEmpty(mallOrderGoods)) {
                        log.setIsSuccess("失败");
                        log.setErrMsg("中闽在线回调没有查询到该订单详情的存在mallOrderGoods：" + customer_order_no);
                        logService.add(log);
                        logger.info("中闽在线回调没有查询到该商品订单详情的存在-------------------------------");
                        return ResponseUtil.fail();
                    }

                    try {
                        JSONArray jsonArray = JSONArray.parseArray(cards);
                        for (int i = 0; i < jsonArray.size(); i++) {
                            String card_number = jsonArray.getJSONObject(i).getString("number");
                            String card_pwd = jsonArray.getJSONObject(i).getString("password");
                            String card_deadline = jsonArray.getJSONObject(i).getString("deadline");
                            MallOrderGoods newMallOrderGoods = new MallOrderGoods();
                            newMallOrderGoods.setCardDeadline(card_deadline);
                            newMallOrderGoods.setCardNumber(card_number);
                            newMallOrderGoods.setCardPwd(card_pwd);
                            newMallOrderGoods.setOrderId(customer_order_no);
                            newMallOrderGoods.setId(mallOrderGoods.get(i).getId());
                            orderGoodsService.updateById(newMallOrderGoods);
                        }

                    } catch (Exception e) {
                        log.setIsSuccess("失败");
                        log.setErrMsg("json解析失败：" + cards);
                        logService.add(log);
                        logger.info("json解析失败--------------------------- ---");
                        return ResponseUtil.fail(400, "json解析失败" + cards);
                    }

                }

                log.setIsSuccess("成功");
                log.setErrMsg("中闽在线回调成功：" + customer_order_no);
                logService.add(log);

            } else {
                logger.info("中闽在线未知情况，order_status" + order_status);
            }
        }

        /*
         * if (!success) { logger.info("----------------------中闽在线开始在线退款，订单id=" + oneMallOrder.getId());
         * refundQY(oneMallOrder); // logger.info("退款结果："+res); } else { // 更新回调后商品状态
         * orderService.updateByPrimaryKeySelective(mallOrder);
         *
         * // AccountUtils.applyPrecharge(customer_order_no, "1"); // 直接充值，不用预充值了 if
         * (OrderUtil.STATUS_RESULT_SUCCESS.equals(mallOrder.getOrderThreeStatus())) {
         * AccountQYUtils.preChargeQYStart(mallOrder.getId()); }
         *
         * // 更新卡密信息 if (StringUtils.isNotBlank(cards) && !Objects.equals(cards, "null")) {
         * logger.info("返回卡密信息");
         *
         * List<MallOrderGoods> mallOrderGoods = orderGoodsService.queryByOid(customer_order_no);
         *
         * if (CollectionUtils.isEmpty(mallOrderGoods)) { log.setIsSuccess("失败");
         * log.setErrMsg("中闽在线回调没有查询到该订单详情的存在mallOrderGoods：" + customer_order_no); logService.add(log);
         * logger.info("中闽在线回调没有查询到该商品订单详情的存在-------------------------------"); return ResponseUtil.fail();
         * }
         *
         * try { JSONArray jsonArray = JSONArray.parseArray(cards);
         *
         * for (int i = 0; i < jsonArray.size(); i++) {
         *
         * String card_number = jsonArray.getJSONObject(i).getString("number"); String card_pwd =
         * jsonArray.getJSONObject(i).getString("password"); String card_deadline =
         * jsonArray.getJSONObject(i).getString("deadline");
         *
         * MallOrderGoods newMallOrderGoods = new MallOrderGoods();
         * newMallOrderGoods.setCardDeadline(card_deadline); newMallOrderGoods.setCardNumber(card_number);
         * newMallOrderGoods.setCardPwd(card_pwd); newMallOrderGoods.setOrderId(customer_order_no);
         * newMallOrderGoods.setId(mallOrderGoods.get(i).getId());
         *
         * orderGoodsService.updateById(newMallOrderGoods); }
         *
         * } catch (Exception e) { log.setIsSuccess("失败"); log.setErrMsg("json解析失败：" + cards);
         * logService.add(log); logger.info("json解析失败--------------------------- ---"); return
         * ResponseUtil.fail(400, "json解析失败" + cards); }
         *
         * }
         *
         * log.setIsSuccess("成功"); log.setErrMsg("中闽在线回调成功：" + customer_order_no); logService.add(log); }
         */
        return success ? ResponseUtil.ok() : ResponseUtil.fail();
    }

    /**
     * 蓝色兄弟充值结果回调
     *
     * @return
     */
    public Object payNotifyByLSXD(Map<String, Object> request) {

        logger.info("蓝色兄弟充值结果回调-------------------------");
        String customer_order_no = String.valueOf(request.get("customerOrderNo"));
        String order_status = String.valueOf(request.get("status")); // 01:充值成功,03:充值失败

        if (StringUtils.isBlank(customer_order_no)) {
            logger.info("蓝色兄弟回调参数customer_order_no为空：" + customer_order_no);
            return ResponseUtil.fail();
        }
        if (StringUtils.isBlank(order_status)) {
            logger.info("蓝色兄弟回调参数order_status为空：" + order_status);
            return ResponseUtil.fail();
        }
        MallOrderNotifyLog log = new MallOrderNotifyLog();
        MallOrder mallOrder = new MallOrder();
        mallOrder.setId(customer_order_no);
        log.setRetWx("订单--充值回调");

        mallOrder.setUpdateTime(LocalDateTime.now());
        boolean success = Objects.equals(order_status, "01");
        if (success) {
            log.setIsSuccess("成功");
            mallOrder.setOrderThreeStatus(OrderUtil.STATUS_RESULT_SUCCESS);
        } else {
            log.setIsSuccess("失败");
            mallOrder.setOrderThreeStatus(OrderUtil.STATUS_RESULT_FAIL);
        }
        log.setOrderId(customer_order_no);
        log.setRemark(request.toString());
        log.setWxParams("lsxd");

        MallOrder oneMallOrder = orderService.findById(customer_order_no);
        if (oneMallOrder == null) {
            logger.info("蓝色兄弟在线回调没有查询到该订单的存在-------------------------------");
            // 失败
            log.setIsSuccess("失败");
            log.setErrMsg("在线回调没有查询到该订单的存在MallOrder：" + customer_order_no);
            logService.add(log);
            return "fail";
        }

        // 权益商品退款自动退款接入，并且调整定价
        if (Objects.equals(order_status, "03")) {
            // 支付失败
            logger.info("----------------------蓝色兄弟开始在线退款，订单id=" + oneMallOrder.getId());
            wxOrderRefundService.refundQY(oneMallOrder);
        } else if (success) {
            // 支付成功
            logger.info("----------------------蓝色兄弟支付成功，开始处理业务，订单id=" + oneMallOrder.getId());
            if (OrderUtil.STATUS_RESULT_SUCCESS.equals(oneMallOrder.getOrderThreeStatus())) {
                logger.info("蓝色兄弟订单已支付成功，重复了，驳回");
                return "fail";
            } else if (OrderUtil.STATUS_PAY.equals(oneMallOrder.getOrderStatus())
                    && !OrderUtil.STATUS_RESULT_SUCCESS.equals(oneMallOrder.getOrderThreeStatus())) {
                logger.info("蓝色兄弟开始处理业务");
                orderService.updateByPrimaryKeySelective(mallOrder); // 更新回调后商品状态
                AccountQYUtils.preChargeQYStart(mallOrder.getId()); // 调用权益分润
            } else {
                logger.info("蓝色兄弟未知情况，order_status" + order_status);
            }
        } else {
            logger.info("蓝色兄弟未知参数order_status状态：" + order_status);
        }

        return success ? "success" : "fail"; // return success ? ResponseUtil.ok() : ResponseUtil.fail();
    }

    public Object payNotifyByYl(Map<String, String> map) {
        Map<String, Object> obj = new HashMap<String, Object>();
        try {
            logger.info("壹林话费快充订单充值结果回调payNotifyByYl-----------------------开始，回调参数：" + map.toString());
            String orderid = map.get("orderId");
            String corderid = map.get("corderid");
            String status = map.get("status");
            MallOrder oneMallOrder = orderService.findById(corderid);
            MallOrderNotifyLog mallOrderNotifyLog = new MallOrderNotifyLog();
            mallOrderNotifyLog.setWxParams("yl");
            mallOrderNotifyLog.setOrderId(corderid);
            mallOrderNotifyLog.setOrderSn(orderid);
            mallOrderNotifyLog.setCreateTime(LocalDateTime.now());
            mallOrderNotifyLog.setRetWx("订单-回调");
            mallOrderNotifyLog.setType("业务--壹林话费快充回调");
            logger.info("手机号：" + map.get("phone") + "充值凭证：" + map.get("voucher"));
            if (status.equals("0")) {
                mallOrderNotifyLog.setIsSuccess("失败");
                logger.info("回调---壹林话费快充充值失败");
                wxOrderRefundService.refundQY(oneMallOrder);
            } else if (status.equals("1")) {
                mallOrderNotifyLog.setIsSuccess("成功");
                logger.info("回调---壹林话费快充充值成功");
            } else if (status.equals("2")) {
                mallOrderNotifyLog.setIsSuccess("处理中");
                logger.info("回调---壹林话费快充订单处理中");
            }
            mallOrderNotifyLog.setErrMsg("回调成功");
            logService.add(mallOrderNotifyLog);
            logger.info("---------------------------壹林话费快充回调成功");
            obj.put("status", "ok");
            obj.put("msg", "成功");
            return obj;
        } catch (Exception e) {
            logger.info("---------------------------壹林话费快充回调失败");
            obj.put("status", "500");
            obj.put("msg", "失败");
            return obj;
        }
    }


    /**
     * 话费充值充值回调
     *
     * @return
     */
    public Object payNotifyByCost(HttpServletRequest request) {
        logger.info("话费充值回调开始---------------------------------");

        String result = request.getParameter("result");
        String msg = request.getParameter("msg");
        String order = request.getParameter("order");
        String phoneNo = request.getParameter("phone_no");
        String amount = request.getParameter("amount");
        String opNo = request.getParameter("op_no");
        String sign = request.getParameter("sign");
        if (StringUtils.isBlank(result)) {
            logger.info("话费 回调参数result为空：" + result);
            return ResponseUtil.fail();
        }
        if (StringUtils.isBlank(msg)) {
            logger.info("话费 回调参数msg为空：" + msg);
            return ResponseUtil.fail();
        }

        if (StringUtils.isBlank(phoneNo)) {
            logger.info("话费 回调参数phoneNo为空：" + phoneNo);
            return ResponseUtil.fail();
        }

        if (StringUtils.isBlank(amount)) {
            logger.info("话费 回调参数amount为空：" + amount);
            return ResponseUtil.fail();
        }

        if (StringUtils.isBlank(opNo)) {
            logger.info("话费 回调参数msg为空：" + opNo);
            return ResponseUtil.fail();
        }

        if (StringUtils.isBlank(sign)) {
            logger.info("话费 回调参数sign为空：" + sign);
            return ResponseUtil.fail();
        }

        String signString = result + msg + order + phoneNo + amount + opNo + PlateFromConstant.COST_KEY;

        if (!Objects.equals(sign, DigestUtils.md5Hex(signString))) {
            logger.info("话费 回调参数sign验签失败：" + sign);
            return ResponseUtil.fail();
        }

        logger.info("话费充值查询充值状态api调用---------------------------------");

        try {
            Map<String, String> map = new LinkedHashMap<String, String>();
            MallOrderNotifyLog log = new MallOrderNotifyLog();
            map.put("sid", PlateFromConstant.APPId);
            map.put("orderId", opNo);

            String signString2 = map.get("orderId") + map.get("sid") + PlateFromConstant.COST_KEY;
            map.put("sign", DigestUtils.md5Hex(signString2).toLowerCase());

            String statusResult = HttpUtils.get("http://156.225.3.139:8080/api/order/queryStatus", map, null);

            JSONObject object = JSONObject.parseObject(statusResult);

            log.setOrderId(opNo);
            log.setRemark(request.toString());
            log.setWxParams("cost");
            log.setRetWx("话费订单--充值回调");
            log.setOrderSn(order);
            if (!Objects.equals(object.getString("result"), "SUCCESS")
                    || Objects.equals(object.getString("msg"), "充值失败")
                    || Objects.equals(object.getString("msg"), "未充值")) {
                logger.info("话费充值查询状态为充值失败：" + object.getString("result"));
                log.setIsSuccess("失败");
                log.setErrMsg("失败的详细返参：" + object);
                logService.add(log);
                return ResponseUtil.fail();
            }



            MallOrder mallOrder = new MallOrder();
            mallOrder.setId(opNo);
            boolean success = Objects.equals(result, "success");
            if (success) {
                log.setIsSuccess("成功");
                mallOrder.setOrderThreeStatus(OrderUtil.STATUS_RESULT_SUCCESS);
            } else {
                log.setIsSuccess("失败");
                mallOrder.setOrderThreeStatus(OrderUtil.STATUS_RESULT_FAIL);
            }

            MallOrder oneMallOrder = orderService.findById(opNo);
            if (oneMallOrder == null) {
                logger.info("话费充值回调没有查询到该订单的存在-------------------------------");
                // 失败
                log.setIsSuccess("失败");
                log.setErrMsg("话费充值回调没有查询到该订单的存在MallOrder：" + opNo);
                logService.add(log);

                return ResponseUtil.fail(400, "话费充值回调没有查询到该订单的存在");
            }

            logService.add(log);

            orderService.updateByPrimaryKeySelective(mallOrder);
        } catch (Exception e) {
            e.printStackTrace();
        }



        // AccountUtils.applyPrecharge(customer_order_no, "1");

        return "success";
    }

    /**
     * 提交订单(创客)
     *
     * @param userId 用户ID
     * @param body 订单信息，{ addressId:地址id, message:订单备注, tpid:分享人id, submitClient:Customer }
     * @param clientIp 提交订单操作结果
     * @return
     */
    @Transactional
    public Object submitMaker(String userId, String body, String clientIp) {
        Map<String, Object> data = null;
        try {
            if (userId == null) {
                return ResponseUtil.unlogin();
            }
            // if (body == null) {
            // return ResponseUtil.badArgument();
            // }
            logger.info("参数body:" + body);

            String firstLeaderId = JacksonUtil.parseString(body, "firstLeaderId");
            // String message = JacksonUtil.parseString(body, "message");
            // // 邀请人的主键ID(firstLeader)
            // String tpid = JacksonUtil.parseString(body, "tpid");
            // // 渠道商ID
            // String channelId = JacksonUtil.parseString(body, "channelId");
            // 卡类型(0.app自购 1.分享购买 )
            String cardType = JacksonUtil.parseString(body, "cardType"); // 预支付——用户购买骑士卡或 购买创客用
            String addressId = JacksonUtil.parseString(body, "addressId");
            String mobile = JacksonUtil.parseString(body, "mobile");
            // 用户购买创客赠送商品类型（1 红酒 ，2 小球藻片）
            String giftType = JacksonUtil.parseString(body, "giftType");
            if (StringUtils.isBlank(mobile)) {
                return ResponseUtil.fail(403, "请输入电话号码！");
            }
            //
            // // 下单端口H5端默认写Customer，小程序目前是空，后续可以直接写MiniBusiness
            // String submitClient = JacksonUtil.parseString(body, "submitClient");
            // // 如果接口为空的话
            // submitClient = StringUtils.isNotBlank(submitClient) ? submitClient : "MiniBusiness";



            // todo 2020-12-14逻辑修改-----------

            // 创客单价（默认730）
            BigDecimal price = new BigDecimal("730");
            Boolean becomeVip = false;
            List<MallUserProfitNew> list1 = serviceImplUser
                    .list(new QueryWrapper<MallUserProfitNew>().eq("deleted", 0).eq("profit_type", 4));
            Map<String, String> stringStringMap = mallSystemConfigService.queryMaker();// 查询卡信息
            if (list1.size() > 0) {
                price = list1.get(0).getPackageAmount();
                // price = new BigDecimal(stringStringMap.get("mall_maker_price"));
            } else {
                return ResponseUtil.badArgumentValue("创客配置信息错误！");
            }



            String orderId = null;
            MallOrder order = null;
            List<MallUser> malluser = userService.queryByMobile(mobile);
            logger.info("用户提交电话-----" + mobile);
            logger.info("用户信息-----" + malluser);
            if (null != malluser) {
                if (null != malluser.get(0) && "1".equals(malluser.get(0).getIsMaker())) {
                    return ResponseUtil.fail(402, "您已是创客，请勿重复购买！");
                }
            } else {
                return ResponseUtil.fail(403, "订单信息有误！");
            }



            // 订单
            order = new MallOrder();
            order.setUserId(malluser.get(0).getId());
            // order.setOrderSn(orderService.generateOrderSn(userId));
            order.setOrderSn(GraphaiIdGenerator.nextId("OrderSN"));
            order.setOrderStatus(OrderUtil.STATUS_CREATE);
            // order.setConsignee(malluser.getNickname());
            order.setMobile(malluser.get(0).getMobile());
            order.setGoodsPrice(price);
            order.setOrderPrice(price);
            order.setActualPrice(price);
            if (!ObjectUtil.equals(null, addressId) && !"".equals(addressId)) {
                // 收货地址
                MallAddress checkedAddress = addressService.query(malluser.get(0).getId(), addressId);
                if (checkedAddress == null) {
                    return ResponseUtil.badArgument();
                }
                order.setConsignee(checkedAddress.getName());
                String detailedAddress = checkedAddress.getProvince() + checkedAddress.getCity()
                        + checkedAddress.getCounty() + " " + checkedAddress.getAddressDetail();
                order.setAddress(detailedAddress);
            }

            // 推荐人id后续用来修改锁定关系
            order.setTpid(firstLeaderId);
            if ("1".equals(cardType)) {
                becomeVip = true;
                order.setSource("分享购买创客");
            } else {
                order.setSource("app购买创客");
            }
            order.setClientIp(clientIp);
            // 订单类型 购买创客
            order.setBid(AccountStatus.BID_38);
            // 创客订单赠送商品类型 （1 红酒 ，2 小球藻片）
            order.setChannelCode(giftType);
            order.setBecomeVip(becomeVip);
            // 添加订单表
            orderService.add(order);
            orderId = order.getId();



            data = new HashMap<>();
            data.put("orderId", orderId); // 订单号

        } catch (Exception e) {
            logger.error("订单提交异常: " + e.getMessage(), e);
        }
        return ResponseUtil.ok(data);
    }

    /**
     * 提交订单(达人)
     *
     * @param userId 用户ID
     *
     * @param body 订单信息
     *
     * @param clientIp 提交订单操作结果
     *
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Object submitTalent(String userId, String body, String clientIp) {
        Map<String, Object> data = null;
        try {

            logger.info("购买达人参数body:" + body);
            // 分享人Id
            String firstLeaderId = JacksonUtil.parseString(body, "firstLeaderId");
            // 验证码
            String valCode = JacksonUtil.parseString(body, "valCode");
            // 卡的类型 2-月卡 3-季卡 4-年
            String cardType = JacksonUtil.parseString(body, "cardType");
            // 卡类型(1.app自购 2.分享购买 )
            String buyType = JacksonUtil.parseString(body, "buyType");
            // 手机号码 h5分享升级用
            String mobile = JacksonUtil.parseString(body, "mobile");
            // 用户名称 h5分享升级用
            String nickename = JacksonUtil.parseString(body, "nickename");


            // 默认月卡价格 防止出错
            BigDecimal price = new BigDecimal("9.9");
            Boolean becomeVip = false;
            Map<String, String> stringStringMap = mallSystemConfigService.queryTalent();// 查询卡信息
            if ("2".equals(cardType)) {
                // 月卡
                price = new BigDecimal(stringStringMap.get("mall_talent_month_card"));
            } else if ("3".equals(cardType)) {
                // 季卡
                price = new BigDecimal(stringStringMap.get("mall_talent_season_card"));
            } else if ("4".equals(cardType)) {
                // 年卡
                price = new BigDecimal(stringStringMap.get("mall_talent_year_card"));
            }

            String orderId = null;
            MallOrder order = null;
            MallUser malluser = null;
            if (!StringUtils.isEmpty(userId) && buyType.equals("1")) {
                malluser = userService.findById(userId);
            } else if (!StringUtils.isEmpty(buyType) && buyType.equals("2")) {
                if (!StringUtils.isEmpty(firstLeaderId)) {
                    MallUser mall = userService.findById(firstLeaderId);
                    if (null == mall) {
                        return ResponseUtil.fail(403, "分享人错误！");
                    }
                } else {
                    return ResponseUtil.fail(403, "分享人为空！");
                }
                // 通过手机号、手机验证码 判断验证码是否正确
                String smsType = "APP_REGISTER";
                MallPhoneValidation mallPhoneValidation =
                        phoneService.getPhoneValidationByCode(mobile, valCode, smsType);
                if (mallPhoneValidation == null) {
                    return ResponseUtil.fail(AUTH_CAPTCHA_UNMATCH, "验证码错误");
                }

                List<MallUser> mallUsers = userService.queryByMobile(mobile);

                // 暂不升级 如果是新用户直接创建用户信息不创建订单
                if (null != mallUsers && mallUsers.size() == 0) {
                    MallUser user = new MallUser();
                    user.setMobile(mobile);
                    user.setUsername(nickename);
                    // 只锁直推人
                    if (!StringUtils.isEmpty(firstLeaderId)) {
                        user.setDirectLeader(firstLeaderId);
                        //set绑定直推人时间
                        user.setLockTime(LocalDateTime.now());
                    }
                    user.setSource("达人分享注册");
                    userService.add(user);
                    // 创建账户
                    accountService.accountTradeCreateAccount(user.getId());
                    malluser = user;
                    // 更新验证码
                    MallPhoneValidation updateVal = new MallPhoneValidation();
                    updateVal.setId(mallPhoneValidation.getId());
                    updateVal.setValState("00");
                    phoneService.update(updateVal);
                    // 暂不升级只注册 其它生成订单
                    if ("5".equals(cardType)) {
                        return ResponseUtil.ok("注册成功！");
                    }
                }
                // 老用户提示登录
                else {
                    return ResponseUtil.fail("您已注册，请直接下载并登录APP进行达人升级！");
                }

            } else {
                return ResponseUtil.fail(403, "订单数据有误！");
            }


            // 订单
            order = new MallOrder();
            order.setUserId(malluser.getId());
            // order.setOrderSn(orderService.generateOrderSn(userId));
            order.setOrderSn(GraphaiIdGenerator.nextId("OrderSN"));
            order.setOrderStatus(OrderUtil.STATUS_CREATE);
            order.setConsignee(malluser.getNickname());
            order.setMobile(malluser.getMobile());
            order.setGoodsPrice(price);
            order.setOrderPrice(price);
            order.setActualPrice(price);
            // 使用该字段标识购买哪种类型的卡
            order.setActivityType(cardType);
            // 推荐人id后续用来修改锁定关系
            order.setTpid(firstLeaderId);
            if ("1".equals(buyType)) {
                becomeVip = true;
                order.setSource("app升级达人");
            } else if ("2".equals(buyType)) {
                order.setSource("分享购买达人");
            }
            order.setClientIp(clientIp);
            // 订单类型 升级达人
            order.setBid(AccountStatus.BID_44);
            order.setBecomeVip(becomeVip);
            // 添加订单表
            orderService.add(order);
            orderId = order.getId();
            data = new HashMap<>();
            // token
            String token = UserTokenManager.generateToken(malluser.getId(), malluser.getTbkRelationId(),
                    malluser.getTbkSpecialId());
            // 订单号
            data.put("orderId", orderId);
            data.put("token", token);
        } catch (Exception e) {
            logger.error("订单提交异常: " + e.getMessage(), e);
        }
        return ResponseUtil.ok(data);
    }


    @Transactional
    public Object exchangePayTalent(String userId, String body, String clientIp) throws Exception {
        try {
            logger.info("购买达人参数body:" + body);
            // 分享人Id
            // String firstLeaderId = JacksonUtil.parseString(body, "firstLeaderId");
            // 卡的类型 2-月卡 3-季卡 4-年
            String cardType = JacksonUtil.parseString(body, "cardType");
            // 卡类型(1.app自购 2.分享购买 )
            String buyType = JacksonUtil.parseString(body, "buyType");


            // 默认月卡价格 防止出错
            BigDecimal price = new BigDecimal("9.9");
            Boolean becomeVip = false;
            Map<String, String> stringStringMap = mallSystemConfigService.queryTalent();// 查询卡信息
            if ("2".equals(cardType)) {
                // 月卡
                price = new BigDecimal(stringStringMap.get("mall_talent_month_card"));
            } else if ("3".equals(cardType)) {
                // 季卡
                price = new BigDecimal(stringStringMap.get("mall_talent_season_card"));
            } else if ("4".equals(cardType)) {
                // 年卡
                price = new BigDecimal(stringStringMap.get("mall_talent_year_card"));
            }

            // 判断余额是否充足
            FcAccount accountByAccountId = accountService.getAccountByKey(userId);
            if (null == accountByAccountId || accountByAccountId.getExchangeAmount().compareTo(price) == -1) {
                return ResponseUtil.fail(403, "余额不足");
            }

            MallOrder order = null;
            MallUser malluser = null;
            if (!StringUtils.isEmpty(userId) && buyType.equals("1")) {
                malluser = userService.findById(userId);
            } else {
                return ResponseUtil.fail(403, "只支持在APP内使用余额购买");
            }

            // 订单
            order = new MallOrder();
            order.setUserId(malluser.getId());
            order.setOrderSn(GraphaiIdGenerator.nextId("OrderSN"));
            order.setOrderStatus(OrderUtil.STATUS_CREATE);
            order.setConsignee(malluser.getNickname());
            order.setMobile(malluser.getMobile());
            order.setGoodsPrice(price);
            order.setOrderPrice(price);
            order.setActualPrice(price);
            // 使用该字段标识购买哪种类型的卡
            order.setActivityType(cardType);
            // 推荐人id后续用来修改锁定关系
            // order.setTpid(firstLeaderId);
            // 账户余额支付
            order.setSource("fcPay");
            order.setClientIp(clientIp);
            // 订单类型 升级达人
            order.setBid(AccountStatus.BID_44);
            order.setBecomeVip(becomeVip);
            // 添加订单表
            orderService.add(order);
            order.setPayId(GraphaiIdGenerator.nextId("OrderSN"));
            order.setPayTime(LocalDateTime.now());
            order.setOrderStatus(OrderUtil.STATUS_PAY);
            orderService.updateWithOptimisticLocker(order);
            // 扣除商户余额

            // 升级达人
            doPayService.doUserTalent(order);
            Map<String, Object> paramsMap = new HashMap();
            paramsMap.put("message", "购买达人");
            paramsMap.put("buyerId",malluser.getId());
            paramsMap.put("capitalTrend", AccountStatus.CAPITAL_TREND_02);
            paramsMap.put("changeType", AccountStatus.CHANGE_TYPE_02);
            paramsMap.put("changeAccountType", AccountStatus.CHANGE_ACCOUNT_TYPE_2);
            paramsMap.put("tradeType", AccountStatus.TRADE_TYPE_33);
            AccountUtils.accountChangeByExchange(userId, order.getActualPrice(), CommonConstant.TASK_REWARD_CASH,
                    order.getBid(), paramsMap);
            return ResponseUtil.ok(order);
        } catch (Exception e) {
            logger.error("====================>购买达人失败", e);
            throw e;
        }
    }


    /**
     * 提交订单(注册购买 实体卡)
     *
     * @param userId 用户ID
     * @param body 订单信息，{ addressId:地址id, message:订单备注, tpid:分享人id, submitClient:Customer }
     *        {"submitClient":"Customer", "addressId":"地址id", "message":"订单备注", "tpid":"分享人id",
     *        "channelId":"渠道id", "cardType":"会员卡类型"}
     * @param clientIp 提交订单操作结果
     * @return
     */
    @Transactional
    public Object buyCardSubmit(String userId, String body, String clientIp) {
        Map<String, Object> data = null;
        try {
            if (userId == null) {
                return ResponseUtil.unlogin();
            }
            if (body == null) {
                return ResponseUtil.badArgument();
            }
            logger.info("参数body:" + body);

            String addressId = JacksonUtil.parseString(body, "addressId");
            String message = JacksonUtil.parseString(body, "message");
            // 邀请人的主键ID(firstLeader)
            String tpid = JacksonUtil.parseString(body, "tpid");
            // 渠道商ID
            String channelId = JacksonUtil.parseString(body, "channelId");
            // 卡类型(1.电子卡 2.实体卡(普通快递) 3.实体卡(顺丰快递) )
            String cardType = JacksonUtil.parseString(body, "cardType"); // 预支付——用户购买骑士卡

            // 下单端口H5端默认写Customer，小程序目前是空，后续可以直接写MiniBusiness
            String submitClient = JacksonUtil.parseString(body, "submitClient");
            // 如果接口为空的话
            submitClient = StringUtils.isNotBlank(submitClient) ? submitClient : "MiniBusiness";


            // 收货地址
            MallAddress checkedAddress = addressService.query(userId, addressId);
            if (checkedAddress == null) {
                return ResponseUtil.badArgument();
            }

            if (StringUtils.isBlank(cardType)) {
                return ResponseUtil.badArgumentValue("请选择卡类型！");
            }
            // todo 2020-11-5购卡逻辑修改-----------
            // Map<String, String> cardPriceMap = mallSystemConfigService.listCard(); //查询卡信息
            // //电子卡价格(默认9.9)
            // BigDecimal cardPrice = new BigDecimal(9.9);
            // Boolean becomeVip = false;
            // if(cardType == "2" || "2".equals(cardType)){
            // //电子卡(优惠价格：Mall_card_electron_yprice)
            // String mall_card_yprice = cardPriceMap.get("Mall_card_electron_yprice");
            // if(StringUtils.isNotBlank(mall_card_yprice)){
            // cardPrice = new BigDecimal(mall_card_yprice);
            // }
            // }else if(cardType == "3" || "3".equals(cardType)){
            // becomeVip = true;
            // cardPrice = new BigDecimal(17.9);
            // }else if(cardType == "4" || "4".equals(cardType)){
            // //实体卡(优惠价格：Mall_card_yprice)
            // becomeVip = true;
            // String mall_card_yprice = cardPriceMap.get("Mall_card_yprice");
            // if(StringUtils.isNotBlank(mall_card_yprice)){
            // cardPrice = new BigDecimal(mall_card_yprice);
            // }
            // }
            // todo 2020-11-5购卡逻辑修改-----------

            // 电子卡价格(默认9.9)
            BigDecimal cardPrice = new BigDecimal(9.9);
            Boolean becomeVip = false;
            if (cardType == "4" || "4".equals(cardType)) {
                // 实体卡
                becomeVip = true;
            }
            MallSystem mallSystem = new MallSystem();
            mallSystem.setKeyName("Mall_card_%");
            mallSystem.setType(cardType);
            List<MallSystem> cardList = mallSystemConfigService.getSystemList(mallSystem); // 查询卡信息
            if (cardList.size() > 0) {
                MallSystem card = cardList.get(0);
                String price = card.getKeyValue();
                if (StringUtils.isNotBlank(price)) {
                    cardPrice = new BigDecimal(price);
                }
            } else {
                return ResponseUtil.badArgumentValue("参数错误,卡信息不存在！");
            }


            String orderId = null;
            MallOrder order = null;
            // 订单
            order = new MallOrder();
            order.setUserId(userId);
            // order.setOrderSn(orderService.generateOrderSn(userId));
            order.setOrderSn(GraphaiIdGenerator.nextId("OrderSN"));
            order.setOrderStatus(OrderUtil.STATUS_CREATE);
            order.setConsignee(checkedAddress.getName());
            order.setMobile(checkedAddress.getTel());
            order.setMessage(message);
            String detailedAddress = checkedAddress.getProvince() + checkedAddress.getCity()
                    + checkedAddress.getCounty() + " " + checkedAddress.getAddressDetail();
            order.setAddress(detailedAddress);
            order.setGoodsPrice(cardPrice);
            order.setOrderPrice(cardPrice);
            order.setActualPrice(cardPrice);

            order.setSource(submitClient);
            order.setProvinceName(checkedAddress.getProvince());
            order.setCityName(checkedAddress.getCity());
            order.setAreaName(checkedAddress.getCounty());
            order.setAddressDetail(checkedAddress.getAddressDetail());
            order.setClientIp(clientIp);
            // 订单类型
            order.setBid(AccountStatus.BID_14); // 购买骑士卡

            if (StringUtils.isNotBlank(tpid)) {
                MallUser tpUser = userService.findById(tpid);
                order.setTpid(tpid);
                if (tpUser != null) {
                    order.setTpUserName(tpUser.getNickname());
                }
            }
            if (StringUtils.isNotBlank(channelId)) {
                order.setChannelId(channelId);
            }
            order.setBecomeVip(becomeVip);
            // 添加订单表
            orderService.add(order);
            orderId = order.getId();

            // //用户信息
            // MallUser mallUser = userService.findById(userId);
            // Map<String, Object> userInfo = new HashMap<>();
            // userInfo.put("userId", userId); //用户id
            // userInfo.put("username", mallUser.getUsername());
            // userInfo.put("mobile", mallUser.getMobile());
            // /**生成token返回给前端**/
            // String token = UserTokenManager.generateToken(userId, mallUser.getTbkRelationId(),
            // mallUser.getTbkSpecialId());

            data = new HashMap<>();
            data.put("orderId", orderId); // 订单号
            // data.put("token", token); //用户token
            // data.put("userInfo", userInfo); //用户信息
        } catch (Exception e) {
            logger.error("订单提交异常: " + e.getMessage(), e);
        }
        return ResponseUtil.ok(data);
    }


    /**
     * 参见活动支付
     *
     * @param body 订单信息，{ name:用户姓名, mobile:联系电话 }
     * @return
     */
    @Transactional
    public Object activitySubmit(String body, String clientIp) {
        Map<String, Object> data = null;
        try {
            // if (userId == null) {
            // return ResponseUtil.unlogin();
            // }

            if (body == null) {
                return ResponseUtil.badArgument();
            }
            logger.info("参数body:" + body);

            String name = JacksonUtil.parseString(body, "name"); // 姓名
            String mobile = JacksonUtil.parseString(body, "mobile"); // 手机号
            String userId = JacksonUtil.parseString(body, "userId"); // 用户id
            String buserId = JacksonUtil.parseString(body, "buserId"); // 业务员id
            if (StringUtils.isEmpty(name)) {
                return ResponseUtil.badArgumentValue("请输入姓名！");
            }
            if (StringUtils.isEmpty(mobile)) {
                return ResponseUtil.badArgumentValue("请输入手机号！");
            }
            if (!RegexUtil.isMobileExact(mobile)) {
                return ResponseUtil.badArgumentValue("请输入正确的手机号!");
            }
            List<MallMarketingActivity> mallUserList = userService.selectUserActivity(null, null, buserId);
            // 下单端口H5端默认写Customer，小程序目前是空，后续可以直接写MiniBusiness
            String submitClient = JacksonUtil.parseString(body, "submitClient");
            // 如果接口为空的话
            submitClient = StringUtils.isNotBlank(submitClient) ? submitClient : "MiniBusiness";
            MallMarketingActivity mallMarketingActivity = new MallMarketingActivity();
            if (mallUserList.size() > 0) {
                mallMarketingActivity = mallUserList.get(0);
            }

            // 活动支付价格
            Map<String, String> activityPriceMap = mallSystemConfigService.listActivity();
            if (activityPriceMap == null) {
                return ResponseUtil.badArgumentValue("活动信息错误！");
            }
            if (null == mallMarketingActivity) {
                return ResponseUtil.badArgumentValue("活动信息错误！");
            }
            // Integer total = Integer.valueOf(activityPriceMap.get("Mall_activity_number")); //总数量
            Integer total = Integer.valueOf(mallMarketingActivity.getQuota()); // 总数量
            Integer surplusQuota = Integer.valueOf(mallMarketingActivity.getSurplusQuota()); // 剩余数量
            // String mall_activity_price = activityPriceMap.get("Mall_activity_price");
            String mall_activity_price = mallMarketingActivity.getAmount().toString();
            if (total == null) {
                return ResponseUtil.badArgumentValue("活动人数错误！");
            }
            if (StringUtils.isEmpty(mall_activity_price)) {
                return ResponseUtil.badArgumentValue("活动金额错误！");
            }
            // 查询活动目前参加成功的人数
            MallOrder mallOrder = new MallOrder();
            mallOrder.setOrderStatus(OrderUtil.STATUS_PAY);
            mallOrder.setBid(AccountStatus.BID_34);
            List<MallOrder> orderList = orderService.findOrderListByCondition(mallOrder);
            if (surplusQuota <= 0) {
                return ResponseUtil.badArgumentValue("活动参加人数已满！");
            }

            BigDecimal activityPrice = new BigDecimal(mall_activity_price); // 支付金额


            // 订单
            MallOrder order = new MallOrder();
            if (StringUtils.isEmpty(userId)) {
                userId = "00"; // 暂时默认平台账户userId
            }
            if (StringUtils.isEmpty(buserId)) {
                buserId = "00"; // 暂时默认平台账户buserId
            }
            order.setUserId(userId);
            order.setOrderSn(GraphaiIdGenerator.nextId("OrderSN"));
            order.setOrderStatus(OrderUtil.STATUS_CREATE);
            order.setConsignee(name);
            order.setMobile(mobile);
            order.setMessage("购买活动资格");
            order.setAddress(null);
            order.setGoodsPrice(activityPrice);
            order.setOrderPrice(activityPrice);
            order.setActualPrice(activityPrice);
            order.setSource(submitClient);
            order.setProvinceName(null);
            order.setCityName(null);
            order.setAreaName(null);
            order.setAddressDetail(null);
            order.setClientIp(clientIp);
            order.setTbkPubId(buserId);// 业务员id
            // 订单类型
            order.setBid(AccountStatus.BID_34); // 购买骑士卡


            // 添加订单表
            orderService.add(order);
            String orderId = order.getId();

            data = new HashMap<>();
            String token = UserTokenManager.generateToken(userId, null, null);
            data.put("orderId", orderId); // 订单号
            data.put("token", token); // 订单号

        } catch (Exception e) {
            logger.error("订单提交异常: " + e.getMessage(), e);
        }
        return ResponseUtil.ok(data);
    }

    /**
     * 通用支付方法(不与商品挂钩，目前用于：报名游戏)
     *
     * @param userId 用户ID
     * @param body 订单信息，{ message:订单备注, submitClient:Customer }
     * @param clientIp 提交订单操作结果
     * @return
     */
    @Transactional
    public Object paySubmit(String userId, String body, String clientIp) {
        Map<String, Object> data = null;
        try {
            if (userId == null) {
                return ResponseUtil.unlogin();
            }
            if (body == null) {
                return ResponseUtil.badArgument();
            }
            logger.info("参数body:" + body);
            // 用户信息
            MallUser mallUser = userService.findById(userId);

            String message = JacksonUtil.parseString(body, "message");
            String price = JacksonUtil.parseString(body, "price");
            String amountType = JacksonUtil.parseString(body, "amountType");
            String bid = JacksonUtil.parseString(body, "bid");

            if (StringUtils.isBlank(price) || StringUtils.isBlank(bid)) {
                return ResponseUtil.badArgument();
            }

            // 下单端口H5端默认写Customer，小程序目前是空，后续可以直接写MiniBusiness
            String submitClient = JacksonUtil.parseString(body, "submitClient");
            // 如果接口为空的话
            submitClient = StringUtils.isNotBlank(submitClient) ? submitClient : "MiniBusiness";


            String orderId = null;
            // 订单
            MallOrder order = new MallOrder();
            order.setUserId(userId);
            order.setOrderSn(GraphaiIdGenerator.nextId("OrderSN"));
            order.setOrderStatus(OrderUtil.STATUS_CREATE);
            order.setMobile(mallUser.getMobile());
            order.setMessage(message);
            order.setGoodsPrice(new BigDecimal(price));
            order.setOrderPrice(new BigDecimal(price));
            order.setActualPrice(new BigDecimal(price));
            order.setOrderAmountType(amountType);
            order.setSource(submitClient);
            order.setClientIp(clientIp);
            // 订单类型
            order.setBid(bid); // 业务形态id

            // 游戏额外字段
            if (bid.equals(AccountStatus.BID_7)) {
                String gameRuleId = JacksonUtil.parseString(body, "gameRuleId");
                String signNumber = JacksonUtil.parseString(body, "signNumber");
                String enrollTime = JacksonUtil.parseString(body, "enrollTime");
                order.setOrderGameRuleId(gameRuleId);
                order.setOrderSignNumber(Integer.valueOf(signNumber));
                order.setOrderEnrollTime(LocalDate.parse(enrollTime));
            }

            // 添加订单表
            orderService.add(order);
            orderId = order.getId();

            /** 生成token返回给前端 **/
            String token = UserTokenManager.generateToken(userId, "", "");
            data = new HashMap<>();
            data.put("orderId", orderId); // 订单号
            data.put("price", new BigDecimal(price)); // 卡金额
            data.put("token", token); // 用户token
        } catch (Exception e) {
            logger.error("订单提交异常: " + e.getMessage(), e);
        }
        return ResponseUtil.ok(data);
    }



    // todo 测试方法------------20201015------------后续删除
    // todo 测试方法------------20201015
    @Transactional(rollbackFor = {Exception.class})
    public Object testPayHsNotify(String param) {
        String orderSn = JacksonUtil.parseString(param, "orderSn");
        String payId = JacksonUtil.parseString(param, "payId");

        // 分转化成元
        String totalFee = BaseWxPayResult.fenToYuan(JacksonUtil.parseInteger(param, "totalFee"));
        MallOrder order = orderService.findBySn(orderSn);
        try {
            if (order == null) {
                return ResponseUtil.badArgumentValue("订单不存在！");
            }

            // 检查这个订单是否已经处理过
            if (OrderUtil.isPayStatus(order) && order.getPayId() != null) {
                try {

                } catch (Exception e1) {
                    logger.error("日志添加异常: " + e1.getMessage(), e1);
                }
                return WxPayNotifyResponse.success("订单已经处理成功!");
            }

            // 检查支付订单金额
            if (!totalFee.equals(order.getActualPrice().toString())) {
                try {

                } catch (Exception e1) {
                    logger.error("日志添加异常: " + e1.getMessage(), e1);
                }
                return WxPayNotifyResponse.fail(order.getOrderSn() + " : 支付金额不符合 totalFee=" + totalFee);
            }


            order.setPayId(payId);
            order.setPayTime(LocalDateTime.now());
            order.setOrderStatus(OrderUtil.STATUS_PAY);
            if (orderService.updateWithOptimisticLocker(order) == 0) {
                logger.info("三方平台开始下单updateWithOptimisticLocker。。。。。。。。。。。。。。" + order.toString());

                // 这里可能存在这样一个问题，用户支付和系统自动取消订单发生在同时
                // 如果数据库首先因为系统自动取消订单而更新了订单状态；
                // 此时用户支付完成回调这里也要更新数据库，而由于乐观锁机制这里的更新会失败
                // 因此，这里会重新读取数据库检查状态是否是订单自动取消，如果是则更新成支付状态。
                order = orderService.findBySn(orderSn);

                int updated = 0;

                // 订单自动取消
                if (OrderUtil.isAutoCancelStatus(order)) {
                    order.setPayId(payId);
                    order.setPayTime(LocalDateTime.now());
                    order.setOrderStatus(OrderUtil.STATUS_PAY);

                    updated = orderService.updateWithOptimisticLocker(order);

                    // 修改销量
                    List<MallOrderGoods> mallOrderGoodsList = orderGoodsService.queryByOid(order.getId());
                    for (int i = 0; i < mallOrderGoodsList.size(); i++) {
                        MallOrderGoods mallOrderGoods = mallOrderGoodsList.get(i);
                        if (mallOrderGoods.getNumber() != null) {
                            MallGoods mallGoods = new MallGoods();
                            mallGoods.setId(mallOrderGoods.getGoodsId());
                            mallGoods.setItemsale(Integer.parseInt(mallOrderGoods.getNumber().toString()));
                            goodsService.updateMallGoodsSales(mallGoods);
                        }
                    }
                }

                try {

                } catch (Exception e1) {
                    logger.error("日志添加异常: " + e1.getMessage(), e1);
                }

                // 如果updated是0，那么数据库更新失败
                if (updated == 0) {
                    return WxPayNotifyResponse.fail("更新数据已失效");
                }
            }

            // 支付成功，有团购信息，更新团购信息
            MallGroupon groupon = grouponService.queryByOrderId(order.getId());
            if (groupon != null) {
                MallGrouponRules grouponRules = grouponRulesService.queryByIdV2(groupon.getRulesId());

                // 仅当发起者才创建分享图片
                if ("0".equals(groupon.getGrouponId())) {
                    String url = qCodeService.createGrouponShareImage(grouponRules.getGoodsName(),
                            grouponRules.getPicUrl(), groupon);
                    groupon.setShareUrl(url);
                }
                groupon.setPayed(true);
                if (grouponService.updateById(groupon) == 0) {
                    return WxPayNotifyResponse.fail("更新数据已失效");
                }
            }

            logger.info("三方平台开始下单。。。。。。。。。。。。。。");
            logger.info("三方平台开始下单。。。。。。。。。。。。。。" + order.toString());
            logger.info("三方平台开始下单。。。。。。。。。。。。。。" + order.getHuid());
            // 防止重复下单
            try {
                logger.info("开始下单：--------------------------------------");

                // 三方下单支付回调
                if (Objects.equals(order.getHuid(), "fuluGoods")) {
                    payFuLuGoodsOrder(order, "test");
                } else if (Objects.equals(order.getHuid(), "zmGoods")) {
                    payZMGoodsOrder(order, "test");
                } else if (Objects.equals(order.getHuid(), "lsxdGoods")) {
                    payLSXDGoodsOrder(order, "test");
                } else if (Objects.equals(order.getHuid(), "cost")) {
                    payCOSTGoodsOrder(order, "test");
                }
            } catch (Exception e) {
                logger.info("三方下单支付完成。。。。。。。。。");
            }

            // 购买者id
            String userId = order.getUserId();
            /** 用户购买优惠券商品 **/
            // 通过orderId 查询订单商品
            List<MallOrderGoods> orderGoodsList = orderGoodsService.queryByOid(order.getId());
            if (orderGoodsList.size() > 0) {
                for (MallOrderGoods mallOrderGoods : orderGoodsList) {

                    String productId = mallOrderGoods.getProductId();

                    try {
                        JSONObject obj = goodsService.queryGoodsByProductId(productId);
                        if (!Objects.equals(null, obj)) {
                            logger.info("--------修改自营商品销量和库存-----------------");
                            // 在开放平台goods表添加销量和product表减去库存
                            // 商品属于自营商品
                            if (Objects.equals("0", obj.get("virtualGood"))
                                    || 0 == Integer.parseInt(String.valueOf(obj.get("virtualGood")))) {
                                Integer buyNum = Integer.parseInt(String.valueOf(mallOrderGoods.getNumber()));
                                if (!goodsService.updateZyOpenGoods(productId, buyNum)) {
                                    logger.info("修改自营商品销量和库存失败");
                                }
                            }
                        }
                    } catch (Exception e) {
                        logger.info("远程修改库存和销量出现异常!");
                        e.printStackTrace();
                    }



                    String id = mallOrderGoods.getId();
                    String goodsId = mallOrderGoods.getGoodsId();
                    /** (1)通过goodsId 查询 优惠券商品信息(test2库商品数据) **/
                    try {
                        logger.info("----------远程调用open项目接口 getCouponGoodsDetail----------");
                        String url = platFormServerproperty.getUrl(MallConstant.TAOBAO);
                        Map<String, Object> goodMap = new HashMap<String, Object>();
                        goodMap.put("plateform", "dtk");
                        goodMap.put("id", goodsId);
                        Header[] headers = new BasicHeader[] {new BasicHeader("method", "getCouponGoodsDetail"),
                                new BasicHeader("version", "v1.0")};
                        String requestResult = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(goodMap), headers);
                        JSONObject object = JSONObject.parseObject(requestResult);

                        if ("0".equals(object.getString("errno"))) {
                            Object responseData = object.get("data");
                            if (ObjectUtil.isNotNull(responseData)) {
                                Map<String, Object> responseMap = (Map<String, Object>) JSONArray
                                        .parseObject(responseData.toString(), Map.class);
                                logger.info("商品responseMap------ "
                                        + JacksonUtils.bean2Jsn(responseMap));
                                String virtual_good = responseMap.get("virtual_good").toString();

                                // MallGoods mallGoods = goodsService.findById(goodsId);
                                // 判断 商品是否属于 优惠券商品
                                if (virtual_good == "2" || "2".equals(virtual_good)) {
                                    // 若商品属于 优惠券商品,则srcGoodId 为couponId
                                    String srcGoodId = mallOrderGoods.getSrcGoodId();// 优惠券id

                                    // 查询可用的优惠券券码list
                                    MallGoodsCouponCode mallGoodsCouponCode = new MallGoodsCouponCode();
                                    mallGoodsCouponCode.setCouponId(srcGoodId);
                                    mallGoodsCouponCode.setSaleStatus(false);
                                    mallGoodsCouponCode.setDeleted(false);
                                    List<MallGoodsCouponCode> mallGoodsCouponCodeList =
                                            mallGoodsCouponCodeService.selectMallGoodsCouponCodeList(
                                                    mallGoodsCouponCode, null, null, null, null);
                                    /** (1.2)随机分配用户 优惠券券码 **/
                                    Map<String, Object> couponMap = new HashMap<String, Object>();
                                    Map<String, Object> responseMap2 = null;

                                    if (mallGoodsCouponCodeList.size() > 0) {
                                        MallGoodsCouponCode couponCode = mallGoodsCouponCodeList.get(0);
                                        couponCode.setUserId(userId);
                                        couponCode.setSaleStatus(true);
                                        mallGoodsCouponCodeService.updateMallGoodsCouponCodeV2(couponCode);

                                        /** (1.3)商品订单加入 优惠券券码 **/
                                        MallOrderGoods orderGoods = new MallOrderGoods();
                                        orderGoods.setId(id);
                                        orderGoods.setCardNumber(couponCode.getCouponCard()); // 卡号
                                        orderGoods.setCardPwd(couponCode.getCouponCode()); // 卡密

                                        /** (1.3.2)查询优惠券信息 **/
                                        logger.info("----------远程调用open项目接口 getCouponDetail----------");
                                        try {
                                            couponMap.put("plateform", "dtk");
                                            couponMap.put("id", srcGoodId);
                                            Header[] couponHeaders = new BasicHeader[] {
                                                    new BasicHeader("method", "getCouponDetail"),
                                                    new BasicHeader("version", "v1.0")};
                                            String couponResult = HttpRequestUtils.post(url,
                                                    JacksonUtils.bean2Jsn(couponMap),
                                                    couponHeaders);
                                            JSONObject couponObject = JSONObject.parseObject(couponResult);
                                            if ("0".equals(couponObject.getString("errno"))) {
                                                Object couponData = couponObject.get("data");
                                                if (ObjectUtil.isNotNull(couponData)) {
                                                    responseMap2 = (Map<String, Object>) JSONArray
                                                            .parseObject(couponData.toString(), Map.class);
                                                    if (ObjectUtil.isNotNull(responseMap2.get("endTime"))) {
                                                        orderGoods.setCardDeadline(
                                                                String.valueOf(responseMap2.get("endTime"))); // 卡券有效期
                                                    }
                                                    if (ObjectUtil.isNotNull(responseMap2.get("shortUrl"))) {
                                                        orderGoods.setGoodsShortUrl(
                                                                String.valueOf(responseMap2.get("shortUrl"))); // 卡券短链接
                                                        // (推广链接)
                                                    }
                                                } else {
                                                    logger.info("优惠券信息不存在！");
                                                }
                                            }
                                        } catch (Exception e) {
                                            logger.info("远程调用查询优惠券信息接口失败！");
                                        }
                                        orderGoodsService.updateById(orderGoods);
                                        logger.info("分配券码的商品信息------ "
                                                +  JacksonUtils.bean2Jsn(orderGoods));
                                    }


                                    /** (1.4)优惠券 剩余库存减少(远程调用接口，修改test2库数据) **/
                                    Short number = mallOrderGoods.getNumber(); // 订单商品数量
                                    if (ObjectUtil.isNotNull(responseMap2.get("surplus"))) {
                                        Integer surplus = (Integer) responseMap2.get("surplus");
                                        Integer lastSurplus = surplus - number; // 剩余数量
                                        /** 修改优惠券库存 **/
                                        // Map<String, Object> couponMap2 = new HashMap<String, Object>();
                                        // couponMap2.put("plateform", "dtk");
                                        // couponMap2.put("id", srcGoodId);
                                        couponMap.put("surplus", lastSurplus);
                                        logger.info("----------远程调用open项目接口 updateCoupon----------");
                                        try {
                                            Header[] couponHeaders2 = new BasicHeader[] {
                                                    new BasicHeader("method", "updateCoupon"),
                                                    new BasicHeader("version", "v1.0")};
                                            HttpRequestUtils.post(url,
                                                    JacksonUtils.bean2Jsn(couponMap),
                                                    couponHeaders2);
                                        } catch (Exception e) {
                                            logger.info("远程调用修改优惠券信息接口失败！");
                                        }

                                    } else {
                                        logger.info("优惠券库存不存在！");
                                    }


                                    /** 创建用户预估分润 **/
                                    Map<String, String> map = new HashMap<>();
                                    map.put("userId", userId);
                                    map.put("bid", AccountStatus.BID_9);
                                    map.put("totalMoney", String.valueOf(order.getActualPrice()));
                                    map.put("rechargeType", CommonConstant.TASK_REWARD_CASH);
                                    map.put("orderId", order.getId());
                                    map.put("orderSn", order.getOrderSn());
                                    AccountUtils.preCharge(map);
                                }
                            }

                        }
                    } catch (Exception e) {
                        logger.info("远程调用商品信息接口失败！");
                    }


                    /** 支付成功队列商品返现 **/
                    if (true) {
                        /** (2)通过goodsId 查询商品信息(test2库商品数据) **/
                        JSONObject goodObject = goodsService.queryGoodsById(goodsId);
                        logger.info("商品信息：--" + goodObject);
                        if (Objects.equals(null, goodObject)) {
                            return ResponseUtil.fail(507, "商品信息有误!");
                        }
                        String cashbackType = goodObject.getString("cashbackType"); // 是否返现商品
                        String cashbackRate = goodObject.getString("cashbackRate"); // 返现率 默认0%
                        if (StringUtils.isNotEmpty(cashbackType)) {
                            // 若商品的cashbackType不为空、cashbackRate大于0,则属于队列返现商品
                            if (StringUtils.isNotEmpty(cashbackRate)
                                    && new BigDecimal(cashbackRate).compareTo(new BigDecimal(0)) == 1) {
                                // 商品返现
                                Short number = mallOrderGoods.getNumber(); // 用户购买商品数量
                                BigDecimal unitCashback = mallOrderGoods.getUnitCashback(); // 单件商品返现金额
                                BigDecimal totalCashback = unitCashback.multiply(new BigDecimal(number)); // 总返现金额

                                MallOrderGoodsVo orderGoods = new MallOrderGoodsVo();
                                orderGoods.setGoodsId(goodsId);
                                orderGoods.setProductId(productId);
                                orderGoods.setSurplusCashback(new BigDecimal(0));
                                List<MallOrderGoodsVo> mallOrderGoodsList =
                                        orderGoodsService.selectCashbackListByGoodsId(orderGoods);
                                logger.info("返现列表： " + JSONUtil.toJsonStr(mallOrderGoodsList));
                                if (mallOrderGoodsList.size() > 0) {
                                    for (MallOrderGoodsVo mallOrderGoodsVo : mallOrderGoodsList) {
                                        if (totalCashback.compareTo(new BigDecimal(0)) == 1) {
                                            BigDecimal cashbackAmount = mallOrderGoodsVo.getCashbackAmount(); // 已返现金额
                                            BigDecimal surplusCashback = mallOrderGoodsVo.getSurplusCashback(); // 剩余返现金额
                                            BigDecimal amout = new BigDecimal(0); // 用户返现金额
                                            // 比较 '剩余返现金额' 和 '每次返现金额' 大小,确保最后金额全部返现
                                            if (totalCashback.compareTo(surplusCashback) == 1) {
                                                amout = surplusCashback;
                                                cashbackAmount = cashbackAmount.add(surplusCashback);
                                                surplusCashback = new BigDecimal(0);
                                            } else {
                                                amout = totalCashback;
                                                cashbackAmount = cashbackAmount.add(totalCashback);
                                                surplusCashback = surplusCashback.subtract(totalCashback);
                                            }
                                            MallOrderGoods updateOrderGoods = new MallOrderGoods();
                                            updateOrderGoods.setId(mallOrderGoodsVo.getId());
                                            updateOrderGoods.setCashbackAmount(cashbackAmount);
                                            updateOrderGoods.setSurplusCashback(surplusCashback);
                                            orderGoodsService.updateById(updateOrderGoods);

                                            // //用户账户金额增加
                                            // Map<String, Object> paramsMap = new HashMap<>();
                                            // paramsMap.put("dType",AccountStatus.DISTRIBUTION_TYPE_00); //直接分润
                                            // paramsMap.put("message","MallOrderGoods id：" + mallOrderGoodsVo.getId() +
                                            // ",商品队列返现");
                                            // paramsMap.put("capitalTrend",AccountStatus.CAPITAL_TREND_01);
                                            // paramsMap.put("changeType",AccountStatus.CHANGE_TYPE_01);
                                            // paramsMap.put("changeAccountType",AccountStatus.CHANGE_ACCOUNT_TYPE_1);
                                            // paramsMap.put("tradeType", AccountStatus.TRADE_TYPE_07);
                                            // if (amout.compareTo(new BigDecimal(0)) == 1) {
                                            // AccountUtils.accountChange(userId, amout,
                                            // CommonConstant.TASK_REWARD_CASH, AccountStatus.BID_35, paramsMap);
                                            // }
                                            totalCashback = totalCashback.subtract(amout);
                                        } else {
                                            break;
                                        }
                                    }



                                }
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            try {

            } catch (Exception e1) {
                logger.error("日志添加异常: " + e1.getMessage(), e1);
            }
            logger.error("支付订单通知处理异常 :" + e.getMessage(), e);
        }

        return WxPayNotifyResponse.success("处理成功!");
    }

    /**
     * 团购2.0回调
     * @param request
     * @param response
     * @return
     */
//    @Transactional(rollbackFor = {Exception.class})
    public Object groupHsnotify(HttpServletRequest request, HttpServletResponse response) {
        MallOrderNotifyLog log = new MallOrderNotifyLog();
        try{
            String xmlResult = null;
            try {
                int h = 1;
                logger.info("___________第"+h+"次回调");
                xmlResult = IOUtils.toString(request.getInputStream(), request.getCharacterEncoding());
                log.setWxParams(xmlResult);

            } catch (IOException e) {
                e.printStackTrace();
                try {
                    log.setErrMsg("微信通知参数解析异常：" + e.getMessage());
                    log.setIsSuccess("0F");
                    logService.add(log);
                } catch (Exception e1) {
                    logger.error("日志添加异常: " + e1.getMessage(), e1);
                }
                return WxPayNotifyResponse.fail(e.getMessage());
            }
            WxPayOrderNotifyResult result = null;
            try {
                result = hsWxPayService.parseOrderNotifyResult(xmlResult);

                if (!WxPayConstants.ResultCode.SUCCESS.equals(result.getResultCode())) {
                    logger.error(xmlResult);
                    throw new WxPayException("微信通知支付失败！");
                }
                if (!WxPayConstants.ResultCode.SUCCESS.equals(result.getReturnCode())) {
                    logger.error(xmlResult);
                    throw new WxPayException("微信通知支付失败！");
                }
            } catch (WxPayException e) {
                try {
                    log.setErrMsg("微信通知参数解析异常：" + e.getMessage());
                    log.setIsSuccess("0F");
                    logService.add(log);
                } catch (Exception e1) {
                    logger.error("日志添加异常: " + e1.getMessage(), e1);
                }
                e.printStackTrace();
                return WxPayNotifyResponse.fail(e.getMessage());
            }
            logger.info("处理腾讯支付平台的订单支付");
            logger.info(result);
            String orderSn = result.getOutTradeNo();
            String payId = result.getTransactionId();
            log.setOrderSn(orderSn);
            // 分转化成元
            String totalFee = BaseWxPayResult.fenToYuan(result.getTotalFee());
            int update = 0;
            MallOrder order = null;
            logger.info("orderSn " + orderSn);
                order = orderService.findBySn(orderSn);
                if (order == null) {
                    logger.info("订单未找到");
                    return WxPayNotifyResponse.success("成功!");
                }
                if (OrderUtil.isPayStatus(order)) {
                    return WxPayNotifyResponse.success("成功");
                }
                if (Objects.equals(order.getBid(), AccountStatus.BID_36)) {
                    logger.info("回调bid值-----" + order.getBid());
                    MutexLockUtils.remove(orderSn);
                    order.setOrderThreeStatus(OrderUtil.STATUS_CREATE_LOADING);
                }

            if(order == null){
                QueryWrapper<MallPurchaseOrder> orderQueryWrapper = new QueryWrapper<>();
                orderQueryWrapper.eq("order_sn", orderSn);
                MallPurchaseOrder order1 = mallPurchaseOrderService.getOne(orderQueryWrapper);

                if (order1 == null) {
                    try {
                        log.setErrMsg("订单不存在 sn=" + orderSn);
                        log.setIsSuccess("0F");
                        logService.add(log);
                    } catch (Exception e1) {
                        logger.error("日志添加异常: " + e1.getMessage(), e1);
                    }
                    return WxPayNotifyResponse.fail("订单不存在 sn=" + orderSn);
                }

                logger.info("----->商户下单回调处理" + JSON.toJSONString(order1));
                if (!Objects.equals(order1.getUserId(), null)) {
                    log.setUserId(order1.getUserId());
                }

                // 检查这个订单是否已经处理过
                if (OrderUtil.isPayStatus(order1) && order1.getPayId() != null) {
                    try {
                        log.setErrMsg("订单已经处理过");
                        log.setIsSuccess("0F");
                        logService.add(log);
                    } catch (Exception e1) {
                        logger.error("日志添加异常: " + e1.getMessage(), e1);
                    }
                    return WxPayNotifyResponse.success("订单已经处理成功!");
                }

                // 检查支付订单金额
                if (!totalFee.equals(order1.getActualPrice().toString())) {
                    try {
                        log.setErrMsg(order1.getOrderSn() + " : 支付金额不符合 totalFee=" + totalFee);
                        log.setIsSuccess("0F");
                        logService.add(log);
                    } catch (Exception e1) {
                        logger.error("日志添加异常: " + e1.getMessage(), e1);
                    }
                    return WxPayNotifyResponse.fail(order1.getOrderSn() + " : 支付金额不符合 totalFee=" + totalFee);
                }

                order1.setPayId(payId);
                order1.setPayTime(LocalDateTime.now());
                order1.setOrderStatus(OrderUtil.STATUS_PAY.intValue());
                mallPurchaseOrderService.updateById(order1);
                doPayService.doMerchangOrderWithActive(order1);
                String succes = WxPayNotifyResponse.success("处理成功!");
                logger.info(succes);
                return succes;
            }

                // 检查这个订单是否已经处理过
                logger.info("___________________________" + order.getOrderStatus() + " , " + order.getPayId());
                if (order.getOrderStatus() != 101) { //&& order.getPayId() != null  (OrderUtil.isPayStatus(order) || order.getOrderStatus() == 500 || order.getOrderStatus() == 203)
                    try {
                        log.setErrMsg("订单已经处理过");
                        log.setIsSuccess("0F");
                        logService.add(log);
                    } catch (Exception e1) {
                        logger.error("日志添加异常: " + e1.getMessage(), e1);
                    }
                    return WxPayNotifyResponse.success("订单已经处理成功!");
                }

                MallOrder orderV2 = new MallOrder();
                orderV2.setPayId(payId);
                orderV2.setPayTime(LocalDateTime.now());
                orderV2.setOrderStatus(OrderUtil.STATUS_PAY);
                orderV2.setId(order.getId());
                orderV2.setOrderThreeStatus(order.getOrderThreeStatus());
                update = orderService.updateByPrimaryKeySelective(orderV2);
//                order = orderService.findBySn(orderSn);
//                logger.info("order" + JSONUtil.toJsonStr(order));
                if (!Objects.equals(null, order.getUserId())) {
                    log.setUserId(order.getUserId());
                    order.setUserId(order.getUserId());
                }

                // 检查支付订单金额
                if (!totalFee.equals(order.getActualPrice().toString())) {
                    try {
                        log.setErrMsg(order.getOrderSn() + " : 支付金额不符合 totalFee=" + totalFee);
                        log.setIsSuccess("0F");
                        logService.add(log);
                    } catch (Exception e1) {
                        logger.error("日志添加异常: " + e1.getMessage(), e1);
                    }
                    return WxPayNotifyResponse.fail(order.getOrderSn() + " : 支付金额不符合 totalFee=" + totalFee);
                }
                // 支付成功，有团购信息，更新团购信息
                MallGroupon groupon = grouponService.queryByOrderId(order.getParentOrderOn());
                Boolean isLeader = true;
                if (!groupon.getGrouponId().equals("0")){
                    isLeader = false;
                }

                String groupId = null;
                String rulesId = null;
                boolean isOpen=false;
                if (groupon != null) {

                    groupon.setPayed(true);
                    if (Objects.equals(groupon.getGrouponId(), "0")) {
                        groupId = groupon.getId();
                    } else {
                        groupId = groupon.getGrouponId();
                    }
                    rulesId = groupon.getRulesId();
                    if (grouponService.updateById(groupon) == 0) {
                        return WxPayNotifyResponse.fail("更新数据已失效");
                    }


                    if (GroupEnum.ISOPEN_1.getIntCode() == groupon.getIsOpen() &&Objects.equals("0", groupon.getGrouponId())) {
                        isOpen=true;
                    }
                    if (!isOpen){
                        //开团记录每次添加参团人数+1
                        MallGroupon mallGroupon = grouponService.queryById(groupId);
                        if (!Objects.equals(null, mallGroupon)) {
                            mallGroupon.setJoinNum(mallGroupon.getJoinNum() + 1);
                            logger.info("__________________1" + mallGroupon.getJoinNum());
//                   grouponService.updateByIdAndTime(mallGroupon);
                            grouponService.updateById(mallGroupon);
                        }
                        //给开团者和参团的订单记录都插入一个这个团的过期时间,这样避免订单列表显示的时候再做其他很多查询
                        //这里要改 拿出来的openGroup.getOrderId()其实是MallOrder的父级订单id 需要先判断一下如果是支付模式2 查出来的会又两条 拿两条都要改
                        MallGroupon openGroup = grouponService.queryById(groupId);
                        //            MallOrder startOrder = orderService.findById(openGroup.getOrderId());
                        List<MallOrder> startOrder  = orderService.selectList(order.getParentOrderOn(),0);
                        if (startOrder.size()>0 ) { //&& !Objects.equals(null, openGroup.getOrderId())
                            MallGrouponRules rules = grouponRulesService.queryById(rulesId);
                            if (!Objects.equals(null, rules.getDuration())) {
                                for (MallOrder mallOrder : startOrder){
                                    MallOrder openOrder = new MallOrder();
                                    openOrder.setId(mallOrder.getId());
//                        openOrder.setUserId(openGroup.getUserId()); 不确定是否可以去掉 但是 分表分库的情况下不能修改关联键
                                    if (!Objects.equals(null, mallOrder.getPayTime())) {
                                        if(!isLeader){
                                        //获取团结束时间（拿团长结束时间）
                                            List<MallOrder> leaderOrders = orderService.selectList(mallGroupon.getOrderId(), 2);
                                            order.setDrawTime(leaderOrders.get(0).getDrawTime());
                                            openOrder.setDrawTime(leaderOrders.get(0).getDrawTime());
                                        }else{
                                            order.setDrawTime(mallOrder.getPayTime().plusHours(rules.getDuration()));
                                            openOrder.setDrawTime(mallOrder.getPayTime().plusHours(rules.getDuration()));
                                        }

                                    } else {
                                        order.setDrawTime(openGroup.getAddTime().plusHours(rules.getDuration()));
                                        openOrder.setDrawTime(openGroup.getAddTime().plusHours(rules.getDuration()));
                                    }

                                    if (!Objects.equals("00", mallOrder.getId())) {
                                        orderService.updateByPrimaryKeySelective(openOrder);
                                    }
                                }
                            }
                        }
                    }




                }
                //orderService.updateWithOptimisticLocker(order)
//                //开团记录每次添加参团人数+1
//                MallGroupon mallGroupon = grouponService.queryById(groupId);
//                if (!Objects.equals(null, mallGroupon)) {
//                    mallGroupon.setJoinNum(mallGroupon.getJoinNum() + 1);
//                    logger.info("__________________1" + mallGroupon.getJoinNum());
////                   grouponService.updateByIdAndTime(mallGroupon);
//                    grouponService.updateById(mallGroupon);
//                }
//                //给开团者和参团的订单记录都插入一个这个团的过期时间,这样避免订单列表显示的时候再做其他很多查询
//                //这里要改 拿出来的openGroup.getOrderId()其实是MallOrder的父级订单id 需要先判断一下如果是支付模式2 查出来的会又两条 拿两条都要改
//                MallGroupon openGroup = grouponService.queryById(groupId);
////            MallOrder startOrder = orderService.findById(openGroup.getOrderId());
//                List<MallOrder> startOrder  = orderService.selectList(openGroup.getOrderId(),0);
//                if (!Objects.equals(null, openGroup.getOrderId())) {
//                    MallGrouponRules rules = grouponRulesService.queryById(rulesId);
//                    if (!Objects.equals(null, rules.getDuration())) {
//                        for (MallOrder mallOrder : startOrder){
//                            MallOrder openOrder = new MallOrder();
//                            openOrder.setId(mallOrder.getId());
////                        openOrder.setUserId(openGroup.getUserId()); 不确定是否可以去掉 但是 分表分库的情况下不能修改关联键
//                            if (!Objects.equals(null, mallOrder.getPayTime())) {
//                                order.setDrawTime(mallOrder.getPayTime().plusHours(rules.getDuration()));
//                                openOrder.setDrawTime(mallOrder.getPayTime().plusHours(rules.getDuration()));
//                            } else {
//                                order.setDrawTime(openGroup.getAddTime().plusHours(rules.getDuration()));
//                                openOrder.setDrawTime(openGroup.getAddTime().plusHours(rules.getDuration()));
//                            }
//
//                            if (!Objects.equals("00", mallOrder.getId())) {
//                                orderService.updateByPrimaryKeySelective(openOrder);
//                            }
//                        }
//                    }
//                }

                JSONObject obj = null;
                /** 用户购买优惠券商品 **/

                if (Objects.equals(order.getBid(),AccountStatus.BID_36)) {
                    doPayService.openGroup(rulesId, groupId);   //团购2.0开奖逻辑
                }

                if(!isOpen){
                    // 通过orderId 查询订单商品
                    List<MallOrderGoods> orderGoodsList = orderGoodsService.queryByOid(order.getId());
                    if (orderGoodsList.size() > 0){
                        for (MallOrderGoods mallOrderGoods : orderGoodsList) {
                            String productId = mallOrderGoods.getProductId();
                            try {
                                obj = goodsService.queryGoodsByProductId(productId);
                                if (!Objects.equals(null, obj)) {
                                    logger.info("--------修改自营商品销量和库存-----------------");
                                    // 在开放平台goods表添加销量和product表减去库存
                                    // 商品属于自营商品
                                    if (Objects.equals("7", obj.get("virtualGood")) || 7 == Integer.parseInt(String.valueOf(obj.get("virtualGood")))) {
                                        Integer buyNum = Integer.parseInt(String.valueOf(mallOrderGoods.getNumber()));
                                        if (!goodsService.updateZyOpenGoods(productId, buyNum)) {
                                            logger.info("修改自营商品销量和库存失败");
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                logger.info("远程修改库存和销量出现异常!");
                                e.printStackTrace();
                            }
                        }

                    }
                }

//                MutexLockUtils.remove(orderSn);

        }catch (Exception e){
            log.setRemark("订单支付发生异常: " + e.getMessage());
            log.setIsSuccess("0F");
            logService.add(log);
            logger.error("日志添加异常: " + e.getMessage(), e);
            logger.error("支付订单通知处理异常 :" + e.getMessage(), e);
        }
        String succes = WxPayNotifyResponse.success("处理成功!");
        logger.info(succes);
        return succes;
    }


    /**
     * 团购2.0APP端回调
     * @param request
     * @param response
     * @return
     */
//    @Transactional(rollbackFor = {Exception.class})
    public Object appGroupHsnotify(HttpServletRequest request, HttpServletResponse response) {
        MallOrderNotifyLog log = new MallOrderNotifyLog();
        try{
            String xmlResult = null;
            try {
                int h = 1;
                logger.info("___________第"+h+"次回调");
                xmlResult = IOUtils.toString(request.getInputStream(), request.getCharacterEncoding());
                log.setWxParams(xmlResult);

            } catch (IOException e) {
                e.printStackTrace();
                try {
                    log.setErrMsg("微信通知参数解析异常：" + e.getMessage());
                    log.setIsSuccess("0F");
                    logService.add(log);
                } catch (Exception e1) {
                    logger.error("日志添加异常: " + e1.getMessage(), e1);
                }
                return WxPayNotifyResponse.fail(e.getMessage());
            }
            WxPayOrderNotifyResult result = null;
            try {
                result = wxPayService.parseOrderNotifyResult(xmlResult);

                if (!WxPayConstants.ResultCode.SUCCESS.equals(result.getResultCode())) {
                    logger.error(xmlResult);
                    throw new WxPayException("微信通知支付失败！");
                }
                if (!WxPayConstants.ResultCode.SUCCESS.equals(result.getReturnCode())) {
                    logger.error(xmlResult);
                    throw new WxPayException("微信通知支付失败！");
                }
            } catch (WxPayException e) {
                try {
                    log.setErrMsg("微信通知参数解析异常：" + e.getMessage());
                    log.setIsSuccess("0F");
                    logService.add(log);
                } catch (Exception e1) {
                    logger.error("日志添加异常: " + e1.getMessage(), e1);
                }
                e.printStackTrace();
                return WxPayNotifyResponse.fail(e.getMessage());
            }
            logger.info("处理腾讯支付平台的订单支付");
            logger.info(result);
            String orderSn = result.getOutTradeNo();
            String payId = result.getTransactionId();
            log.setOrderSn(orderSn);
            // 分转化成元
            String totalFee = BaseWxPayResult.fenToYuan(result.getTotalFee());
            int update = 0;
            MallOrder order = null;
            logger.info("orderSn " + orderSn);
            order = orderService.findBySn(orderSn);
            if (order == null) {
                logger.info("订单未找到");
                return WxPayNotifyResponse.success("成功!");
            }
            if (OrderUtil.isPayStatus(order)) {
                return WxPayNotifyResponse.success("成功");
            }
            if (Objects.equals(order.getBid(), AccountStatus.BID_36)) {
                logger.info("回调bid值-----" + order.getBid());
                MutexLockUtils.remove(orderSn);
                order.setOrderThreeStatus(OrderUtil.STATUS_CREATE_LOADING);
            }

            if(order == null){
                QueryWrapper<MallPurchaseOrder> orderQueryWrapper = new QueryWrapper<>();
                orderQueryWrapper.eq("order_sn", orderSn);
                MallPurchaseOrder order1 = mallPurchaseOrderService.getOne(orderQueryWrapper);

                if (order1 == null) {
                    try {
                        log.setErrMsg("订单不存在 sn=" + orderSn);
                        log.setIsSuccess("0F");
                        logService.add(log);
                    } catch (Exception e1) {
                        logger.error("日志添加异常: " + e1.getMessage(), e1);
                    }
                    return WxPayNotifyResponse.fail("订单不存在 sn=" + orderSn);
                }

                logger.info("----->商户下单回调处理" + JSON.toJSONString(order1));
                if (!Objects.equals(order1.getUserId(), null)) {
                    log.setUserId(order1.getUserId());
                }

                // 检查这个订单是否已经处理过
                if (OrderUtil.isPayStatus(order1) && order1.getPayId() != null) {
                    try {
                        log.setErrMsg("订单已经处理过");
                        log.setIsSuccess("0F");
                        logService.add(log);
                    } catch (Exception e1) {
                        logger.error("日志添加异常: " + e1.getMessage(), e1);
                    }
                    return WxPayNotifyResponse.success("订单已经处理成功!");
                }

                // 检查支付订单金额
                if (!totalFee.equals(order1.getActualPrice().toString())) {
                    try {
                        log.setErrMsg(order1.getOrderSn() + " : 支付金额不符合 totalFee=" + totalFee);
                        log.setIsSuccess("0F");
                        logService.add(log);
                    } catch (Exception e1) {
                        logger.error("日志添加异常: " + e1.getMessage(), e1);
                    }
                    return WxPayNotifyResponse.fail(order1.getOrderSn() + " : 支付金额不符合 totalFee=" + totalFee);
                }

                order1.setPayId(payId);
                order1.setPayTime(LocalDateTime.now());
                order1.setOrderStatus(OrderUtil.STATUS_PAY.intValue());
                mallPurchaseOrderService.updateById(order1);

                doPayService.doMerchangOrderWithActive(order1);
                String succes = WxPayNotifyResponse.success("处理成功!");
                logger.info(succes);
                return succes;
            }

            // 检查这个订单是否已经处理过
            logger.info("___________________________" + order.getOrderStatus() + " , " + order.getPayId());
            if (order.getOrderStatus() != 101) { //&& order.getPayId() != null  (OrderUtil.isPayStatus(order) || order.getOrderStatus() == 500 || order.getOrderStatus() == 203)
                try {
                    log.setErrMsg("订单已经处理过");
                    log.setIsSuccess("0F");
                    logService.add(log);
                } catch (Exception e1) {
                    logger.error("日志添加异常: " + e1.getMessage(), e1);
                }
                return WxPayNotifyResponse.success("订单已经处理成功!");
            }

            MallOrder orderV2 = new MallOrder();
            orderV2.setPayId(payId);
            orderV2.setPayTime(LocalDateTime.now());
            orderV2.setOrderStatus(OrderUtil.STATUS_PAY);
            orderV2.setId(order.getId());
            orderV2.setOrderThreeStatus(order.getOrderThreeStatus());
            update = orderService.updateByPrimaryKeySelective(orderV2);
//                order = orderService.findBySn(orderSn);
//                logger.info("order" + JSONUtil.toJsonStr(order));
            if (!Objects.equals(null, order.getUserId())) {
                log.setUserId(order.getUserId());
                order.setUserId(order.getUserId());
            }

            // 检查支付订单金额
            if (!totalFee.equals(order.getActualPrice().toString())) {
                try {
                    log.setErrMsg(order.getOrderSn() + " : 支付金额不符合 totalFee=" + totalFee);
                    log.setIsSuccess("0F");
                    logService.add(log);
                } catch (Exception e1) {
                    logger.error("日志添加异常: " + e1.getMessage(), e1);
                }
                return WxPayNotifyResponse.fail(order.getOrderSn() + " : 支付金额不符合 totalFee=" + totalFee);
            }
            // 支付成功，有团购信息，更新团购信息
            MallGroupon groupon = grouponService.queryByOrderId(order.getParentOrderOn());
            String groupId = null;
            String rulesId = null;
            boolean isOpen=false;
            if (groupon != null) {

                groupon.setPayed(true);
                if (Objects.equals(groupon.getGrouponId(), "0")) {
                    groupId = groupon.getId();
                } else {
                    groupId = groupon.getGrouponId();
                }
                rulesId = groupon.getRulesId();
                if (grouponService.updateById(groupon) == 0) {
                    return WxPayNotifyResponse.fail("更新数据已失效");
                }


                if (GroupEnum.ISOPEN_1.getIntCode() == groupon.getIsOpen() &&Objects.equals("0", groupon.getGrouponId())) {
                    isOpen=true;
                }
                if (!isOpen){
                    //开团记录每次添加参团人数+1
                    MallGroupon mallGroupon = grouponService.queryById(groupId);
                    if (!Objects.equals(null, mallGroupon)) {
                        mallGroupon.setJoinNum(mallGroupon.getJoinNum() + 1);
                        logger.info("__________________1" + mallGroupon.getJoinNum());
//                   grouponService.updateByIdAndTime(mallGroupon);
                        grouponService.updateById(mallGroupon);
                    }
                    //给开团者和参团的订单记录都插入一个这个团的过期时间,这样避免订单列表显示的时候再做其他很多查询
                    //这里要改 拿出来的openGroup.getOrderId()其实是MallOrder的父级订单id 需要先判断一下如果是支付模式2 查出来的会又两条 拿两条都要改
                    MallGroupon openGroup = grouponService.queryById(groupId);
                    //            MallOrder startOrder = orderService.findById(openGroup.getOrderId());
                    List<MallOrder> startOrder  = orderService.selectList(order.getParentOrderOn(),0);
                    if (startOrder.size()>0 ) { //&& !Objects.equals(null, openGroup.getOrderId())
                        MallGrouponRules rules = grouponRulesService.queryById(rulesId);
                        if (!Objects.equals(null, rules.getDuration())) {
                            for (MallOrder mallOrder : startOrder){
                                MallOrder openOrder = new MallOrder();
                                openOrder.setId(mallOrder.getId());
//                        openOrder.setUserId(openGroup.getUserId()); 不确定是否可以去掉 但是 分表分库的情况下不能修改关联键
                                if (!Objects.equals(null, mallOrder.getPayTime())) {
                                    order.setDrawTime(mallOrder.getPayTime().plusHours(rules.getDuration()));
                                    openOrder.setDrawTime(mallOrder.getPayTime().plusHours(rules.getDuration()));
                                } else {
                                    order.setDrawTime(openGroup.getAddTime().plusHours(rules.getDuration()));
                                    openOrder.setDrawTime(openGroup.getAddTime().plusHours(rules.getDuration()));
                                }

                                if (!Objects.equals("00", mallOrder.getId())) {
                                    orderService.updateByPrimaryKeySelective(openOrder);
                                }
                            }
                        }
                    }
                }




            }
            //orderService.updateWithOptimisticLocker(order)
//                //开团记录每次添加参团人数+1
//                MallGroupon mallGroupon = grouponService.queryById(groupId);
//                if (!Objects.equals(null, mallGroupon)) {
//                    mallGroupon.setJoinNum(mallGroupon.getJoinNum() + 1);
//                    logger.info("__________________1" + mallGroupon.getJoinNum());
////                   grouponService.updateByIdAndTime(mallGroupon);
//                    grouponService.updateById(mallGroupon);
//                }
//                //给开团者和参团的订单记录都插入一个这个团的过期时间,这样避免订单列表显示的时候再做其他很多查询
//                //这里要改 拿出来的openGroup.getOrderId()其实是MallOrder的父级订单id 需要先判断一下如果是支付模式2 查出来的会又两条 拿两条都要改
//                MallGroupon openGroup = grouponService.queryById(groupId);
////            MallOrder startOrder = orderService.findById(openGroup.getOrderId());
//                List<MallOrder> startOrder  = orderService.selectList(openGroup.getOrderId(),0);
//                if (!Objects.equals(null, openGroup.getOrderId())) {
//                    MallGrouponRules rules = grouponRulesService.queryById(rulesId);
//                    if (!Objects.equals(null, rules.getDuration())) {
//                        for (MallOrder mallOrder : startOrder){
//                            MallOrder openOrder = new MallOrder();
//                            openOrder.setId(mallOrder.getId());
////                        openOrder.setUserId(openGroup.getUserId()); 不确定是否可以去掉 但是 分表分库的情况下不能修改关联键
//                            if (!Objects.equals(null, mallOrder.getPayTime())) {
//                                order.setDrawTime(mallOrder.getPayTime().plusHours(rules.getDuration()));
//                                openOrder.setDrawTime(mallOrder.getPayTime().plusHours(rules.getDuration()));
//                            } else {
//                                order.setDrawTime(openGroup.getAddTime().plusHours(rules.getDuration()));
//                                openOrder.setDrawTime(openGroup.getAddTime().plusHours(rules.getDuration()));
//                            }
//
//                            if (!Objects.equals("00", mallOrder.getId())) {
//                                orderService.updateByPrimaryKeySelective(openOrder);
//                            }
//                        }
//                    }
//                }

            JSONObject obj = null;
            /** 用户购买优惠券商品 **/

            if (Objects.equals(order.getBid(),AccountStatus.BID_36)) {
                doPayService.openGroup(rulesId, groupId);   //团购2.0开奖逻辑
            }

            if(!isOpen){
                // 通过orderId 查询订单商品
                List<MallOrderGoods> orderGoodsList = orderGoodsService.queryByOid(order.getId());
                if (orderGoodsList.size() > 0){
                    for (MallOrderGoods mallOrderGoods : orderGoodsList) {
                        String productId = mallOrderGoods.getProductId();
                        try {
                            obj = goodsService.queryGoodsByProductId(productId);
                            if (!Objects.equals(null, obj)) {
                                logger.info("--------修改自营商品销量和库存-----------------");
                                // 在开放平台goods表添加销量和product表减去库存
                                // 商品属于自营商品
                                if (Objects.equals("7", obj.get("virtualGood")) || 7 == Integer.parseInt(String.valueOf(obj.get("virtualGood")))) {
                                    Integer buyNum = Integer.parseInt(String.valueOf(mallOrderGoods.getNumber()));
                                    if (!goodsService.updateZyOpenGoods(productId, buyNum)) {
                                        logger.info("修改自营商品销量和库存失败");
                                    }
                                }
                            }
                        } catch (Exception e) {
                            logger.info("远程修改库存和销量出现异常!");
                            e.printStackTrace();
                        }
                    }

                }
            }

//                MutexLockUtils.remove(orderSn);

        }catch (Exception e){
            log.setRemark("订单支付发生异常: " + e.getMessage());
            log.setIsSuccess("0F");
            logService.add(log);
            logger.error("日志添加异常: " + e.getMessage(), e);
            logger.error("支付订单通知处理异常 :" + e.getMessage(), e);
        }
        String succes = WxPayNotifyResponse.success("处理成功!");
        logger.info(succes);
        return succes;
    }





    @Transactional
    public Object groupPrepay(String userId, String body, HttpServletRequest request) {
        try {
            logger.info("-----预支付参数: " + body);
            String mobile = JacksonUtil.parseString(body, "mobile");
            String channel = JacksonUtil.parseString(body, "channel");
            String orderId = JacksonUtil.parseString(body, "orderId");
            String openId = JacksonUtil.parseString(body, "openId");
            if (orderId == null) {
                return ResponseUtil.badArgument();
            }
            MallOrder order = orderService.findById(orderId);
            List<MallGroupon> groupByPo = orderService.getGroupByPo(order.getParentOrderOn());
            String groupId = null;
            if (groupByPo != null) {
                if (groupByPo.get(0).getGrouponId().equals("0")) {
                    groupId = groupByPo.get(0).getId();
                } else {
                    groupId = groupByPo.get(0).getGrouponId();
                }
            }
            List<MallGroupon> groupIds = grouponService.queryIsGroupLead(groupId);
            MallGrouponRules rules = orderService.getRules(groupIds.get(0).getRulesId());
            if(groupIds.get(0).getJoinNum() >= rules.getDiscountMember()){
                return ResponseUtil.fail(502,"该团已满人");
            }
//            MallGrouponExample mallGrouponExample = new MallGrouponExample();
//            mallGrouponExample.or().andOrderIdEqualTo(order.getParentOrderOn());
//            List<MallGroupon> mallGroupons = mallGrouponMapper.selectByExample(mallGrouponExample);
//            if (mallGroupons.size()>0 && mallGroupons!= null){
//                if(!mallGroupons.get(0).getIsOpen()){
//                    return ResponseUtil.fail(502,"该团已满人");
//                }
//            }

            if (order == null) {
                return ResponseUtil.badArgumentValue();
            }
            // 检测是否能够取消
            OrderHandleOption handleOption = OrderUtil.build(order);
            if (!handleOption.isPay()) {
                return ResponseUtil.fail(ORDER_INVALID_OPERATION, "订单不能支付");
            }
            MallUser user = userService.findById(userId);

            WxPayMpOrderResult result = null;
            String prepayId = null;
            if (Channel.wx_mini_activity.equals(channel)) {
                // 微信小程序支付先获取小程序Openid
                if (openId == null) {
                    return ResponseUtil.fail(AUTH_OPENID_UNACCESS, "openid为空订单不能支付");
                }
                // 取消订单后再次支付因为汇付不能用同一订单sn支付，所以生成新sn
                order.setOrderSn(GraphaiIdGenerator.nextId("MallOrder"));
                order.setUserId(userId);
                orderService.updateByExample(order);

                WxPayUnifiedOrderRequest orderRequest = new WxPayUnifiedOrderRequest();
                orderRequest.setOutTradeNo(order.getOrderSn());
                // orderRequest.setOpenid(openId);
                orderRequest.setBody("orderNo:" + order.getOrderSn());
                // 元转成分
                int fee = 0;
                BigDecimal actualPrice = order.getActualPrice();
                fee = actualPrice.multiply(new BigDecimal(100)).intValue();
                orderRequest.setTotalFee(fee);
                orderRequest.setSpbillCreateIp(IpUtil.getIpAddr(request));
                orderRequest.setOpenid(openId);
                // WxPayConfig payConfig = new WxPayConfig();
                // payConfig.setAppId(properties.getHsAppId());
                // payConfig.setMchId(properties.getHsMchId());
                // payConfig.setSubAppId(properties.getHsAppletsAppId());
                // payConfig.setSubMchId(properties.getHsChildMchId());
                // payConfig.setMchKey(properties.getHsMchKey());
                // payConfig.setNotifyUrl(properties.getHsNotifyUrl());
                // payConfig.setKeyPath(properties.getKeyPath());
                // payConfig.setTradeType("JSAPI");
                // payConfig.setSignType("MD5");
                // hsWxPayService.setConfig(payConfig);
                result = hsWxGroupPayService.createOrder(orderRequest);
                // 缓存prepayID用于后续模版通知
                prepayId = result.getPackageValue();
                prepayId = prepayId.replace("prepay_id=", "");
                MallUserFormid userFormid = new MallUserFormid();
                userFormid.setOpenid(user.getWeixinOpenid());
                userFormid.setFormid(prepayId);
                userFormid.setIsprepay(true);
                userFormid.setUseamount(3);
                userFormid.setExpireTime(LocalDateTime.now().plusDays(7));
                formIdService.addUserFormid(userFormid);
            }else if (Channel.wx.equals(channel)) {
                Integer fee = 0;
                BigDecimal actualPrice = order.getActualPrice();
                fee = actualPrice.multiply(new BigDecimal(100)).intValue();
                SignParams signParams = new SignParams();
                signParams.setAmount(fee.toString());
                signParams.setSubject(order.getOrderSn());
                signParams.setOrderNo(order.getOrderSn());
                signParams.setBody("orderNo:" + order.getOrderSn());
                signParams.setNotifyUrl(properties.getHsAppGroupNotifyUrl());
                signParams.setChannel(Channel.wx);


                PayChannel payChannel = new PayChannel();
                payChannel.setAppId(properties.getAppId());
                payChannel.setPartnerId(properties.getMchId());
                payChannel.setApiKey(properties.getMchKey());
                return ResponseUtil.ok(PaymentSDK.getPaymentSignature(signParams, payChannel));
            }else if (Objects.equals(channel, Channel.huifu) || Objects.equals(channel, Channel.huifu_wx_pub)) {

                // 取消订单后再次支付因为汇付不能用同一订单sn支付，所以生成新sn
                order.setOrderSn(GraphaiIdGenerator.nextId("MallOrder"));
//                order.setUserId(userId);
                orderService.updateByExample(order);

                // 请求参数
                Map<String, Object> paymentParams = new HashMap<String, Object>();
                paymentParams.put("order_no", order.getOrderSn()); // order.getOrderSn()
                paymentParams.put("pay_amt", order.getActualPrice()); // order.getActualPrice()

                if (Objects.equals(channel, Channel.huifu)) {
                    paymentParams.put("pay_channel", PayChannelEnum.WX_LITE.getCode());
                    paymentParams.put("goods_title", PayChannelEnum.WX_LITE.getDesc());
                    paymentParams.put("goods_desc", PayChannelEnum.WX_LITE.getDesc());
                } else if (Objects.equals(channel, Channel.huifu_wx_pub)) {
                    paymentParams.put("pay_channel", PayChannelEnum.WX_PUB.getCode());
                    paymentParams.put("goods_title", PayChannelEnum.WX_PUB.getDesc());
                    paymentParams.put("goods_desc", PayChannelEnum.WX_PUB.getDesc());
                }


                Map<String, Object> map = new HashMap<String, Object>();
                map.put("open_id", openId);
                paymentParams.put("expend", map);
                paymentParams.put("app_id", huifuProperties.getAppId());
                // 异步通知地址，url为http/https路径，服务器POST回调，URL 上请勿附带参数
                paymentParams.put("notify_url", huifuProperties.getGroupNotifyUrl());
                logger.info("汇付支付请求返参----------------" + JSONUtil.toJsonStr(paymentParams));
                // 调用创建方法，获取 Payment对象_
                Map<String, Object> response = com.huifu.adapay.model.payment.Payment.create(paymentParams, "hf");
                logger.info("汇付支付请求返参结果----------------" + JSONUtil.toJsonStr(response));
                if (!response.isEmpty()) {
                    JSONObject expend = JSONObject.parseObject(String.valueOf(response.get("expend")));
                    if (!expend.isEmpty()) {
                        JSONObject payInfo = JSONObject.parseObject(String.valueOf(expend.get("pay_info")));
                        String hfPrepayId = payInfo.getString("package").replace("prepay_id=", "");
                        response.put("hfPrepayId", hfPrepayId);
                    }
                }
                logger.info("预付款结果" + response);
                return ResponseUtil.ok(response);
            } else if (Channel.alipay.equals(channel)) {
                Integer fee = 0;
                BigDecimal actualPrice = order.getActualPrice();
                fee = actualPrice.multiply(new BigDecimal(100)).intValue();
                SignParams signParams = new SignParams();
                signParams.setAmount(fee.toString());
                signParams.setSubject(order.getOrderSn());
                signParams.setOrderNo(order.getOrderSn());
                signParams.setBody("orderNo:" + order.getOrderSn());
                signParams.setNotifyUrl(aliProperties.getNotifyUrl());
                signParams.setChannel(Channel.alipay);

                PayChannel payChannel = new PayChannel();
                payChannel.setAppId(aliProperties.getAppId());
                payChannel.setPartnerId(aliProperties.getPartnerId());
                payChannel.setAliPublicKey(aliProperties.getRsaPublicKey());
                payChannel.setPrivateKey(aliProperties.getRsaPrivateKey());
                return ResponseUtil.ok(PaymentSDK.getPaymentSignature(signParams, payChannel));
            }
            if (orderService.updateWithOptimisticLocker(order) == 0) {
                return ResponseUtil.updatedDateExpired();
            }
            return ResponseUtil.ok(result);
        }catch (Exception e){
            logger.error("汇付预付异常"+e.getMessage(),e);
            throw new RuntimeException(e);
        }
    }

    /**
     * 汇付付款成功或失败回调接口
     * <p>
     * 1. 检测当前订单是否是付款状态; 2. 设置订单付款成功状态相关信息; 3. 响应汇付结果.
     *
     * @param request 请求内容
     * @return 操作结果
     * @throws Exception
     */
    @Transactional(rollbackFor = {Exception.class})
    public Object grouphuifunotify(HttpServletRequest request) throws Exception {
        MallOrderNotifyLog log = new MallOrderNotifyLog();
        logger.info("---------------------汇付回调进来了--------------------------------");
        // 验签请参data
        String data = request.getParameter("data");
        // 验签请参sign
        String sign = request.getParameter("sign");
        try {

            // 验签标记
            boolean checkSign;
            // 验签请参publicKey
            // 验签请参publicKey
            String publicKey = huifuProperties.getAdpayPublicKey();
            logger.info("验签请参：data=" + data);
            // 验签
            checkSign = AdaPaySign.verifySign(data, sign, publicKey);
            if (checkSign) {
                // 验签成功逻辑
                logger.info("成功返回数据data:" + data);
            } else {
                // 验签失败逻辑
                log.setIsSuccess("0F");
                logService.add(log);
                throw new WxPayException("汇付验签支付失败！");
            }
        } catch (IOException e) {
            e.printStackTrace();
            try {
                log.setErrMsg("汇付通知参数解析异常：" + e.getMessage());
                log.setIsSuccess("0F");
                logService.add(log);
            } catch (Exception e1) {
                logger.error("日志添加异常: " + e1.getMessage(), e1);
            }
            return ResponseUtil.fail(e.getMessage());
        }


        JSONObject obj = JSONObject.parseObject(data);
        // JSONObject dataObject = JSONObject.parseObject(obj.getString("expend"));
        String orderSn = obj.getString("order_no");
        String totalFee = obj.getString("pay_amt");
        String payId = obj.getString("id");
        String result = obj.getString("status");
        log.setOrderSn(orderSn);

        if (!Objects.equals(result, "succeeded")) {
            logger.error(obj);
            throw new Exception("汇付支付失败");
        }

        MallOrder order = orderService.findBySn(orderSn);

        if (Objects.equals(order.getBid(), AccountStatus.BID_36)) {
            logger.info("回调bid值-----" + order.getBid());
            MutexLockUtils.remove(orderSn);
            order.setOrderThreeStatus(OrderUtil.STATUS_CREATE_LOADING);
        }
        try {
            if (order == null) {
                try {
                    log.setErrMsg("订单不存在 sn=" + orderSn);
                    log.setIsSuccess("0F");
                    logService.add(log);
                } catch (Exception e1) {
                    logger.error("日志添加异常: " + e1.getMessage(), e1);
                }
                return WxPayNotifyResponse.fail("订单不存在 sn=" + orderSn);
            }
            if (!Objects.equals(null, order.getUserId())) {
                log.setUserId(order.getUserId());
//                order.setUserId(order.getUserId());
            }
            // 检查支付订单金额
            if (!totalFee.equals(order.getActualPrice().toString())) {
                try {
                    log.setErrMsg(order.getOrderSn() + " : 支付金额不符合 totalFee=" + totalFee);
                    log.setIsSuccess("0F");
                    logService.add(log);
                } catch (Exception e1) {
                    logger.error("日志添加异常: " + e1.getMessage(), e1);
                }
                return WxPayNotifyResponse.fail(order.getOrderSn() + " : 支付金额不符合 totalFee=" + totalFee);
            }
            if (order.getOrderStatus() != 101) {
                try {
                    log.setErrMsg("订单已经处理过");
                    log.setIsSuccess("0F");
                    logService.add(log);
                } catch (Exception e1) {
                    logger.error("日志添加异常: " + e1.getMessage(), e1);
                }
                return WxPayNotifyResponse.success("订单已经处理成功!");
            }
            int update = 0;
            order.setPayId(payId);
            order.setSource("huifu");
//            doPayService.doGroupV3(order);
            doPayService.doNewGroupV3(order);
        } catch (Exception e3) {
            logger.error("汇付付款成功或失败回调接口处理出现异常：" + e3.getMessage(), e3);
            e3.printStackTrace();
        }
        return ResponseUtil.ok("处理成功!");
    }

}
