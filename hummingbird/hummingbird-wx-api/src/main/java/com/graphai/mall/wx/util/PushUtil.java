package com.graphai.mall.wx.util;

import com.gexin.rp.sdk.base.IPushResult;
import com.gexin.rp.sdk.base.impl.SingleMessage;
import com.gexin.rp.sdk.base.impl.Target;
import com.gexin.rp.sdk.base.payload.APNPayload;
import com.gexin.rp.sdk.base.payload.MultiMedia;
import com.gexin.rp.sdk.exceptions.RequestException;
import com.gexin.rp.sdk.http.IGtPush;
import com.gexin.rp.sdk.template.AbstractTemplate;
import com.gexin.rp.sdk.template.NotificationTemplate;
import com.gexin.rp.sdk.template.TransmissionTemplate;
import com.gexin.rp.sdk.template.style.AbstractNotifyStyle;
import com.gexin.rp.sdk.template.style.Style0;
import com.graphai.mall.core.qianzhu.kfc.boot.model.KfcOrderModel;

import lombok.extern.slf4j.Slf4j;

import javax.xml.bind.DatatypeConverter;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


@Slf4j(topic = "PushUtil")
public class PushUtil extends Thread{


	// STEP1：获取应用基本信息
	private static String appId = "2aXMw3nfcWA7XdDWUwuqT";
	private static String appKey = "CHuicrco2b7LcJvl6rjPZ2";
	private static String masterSecret = "3okKHdIhFr8gwlRBvWaSO3";
	private static String url = "http://sdk.open.api.igexin.com/apiex.htm";
	private String clientId;

	public PushUtil(String clientId) {
		this.clientId = clientId;
	}

	@Override
	public void run() {
		log.debug("开始个推.....");
		//pushToSingle(clientId);
	}
	
	  public static void main(String[] args) {
//	        getLinkTemplate();
//	        getTransmissionTemplate();
//	        getStartActivityTemplate();
//	        getRevokeTemplate("XXXX");
		  //安卓
		  //fa4913a592192f5d8e2398b2a591ffef
		  //ios
		  //fb9edc1455347eefbd2bc9b7514ce126
//		 pushToSingle("fa4913a592192f5d8e2398b2a591ffef",10);
			  com.graphai.commons.util.PushUtil.pushToSingle("871799e3003ad27ebb15fbbbe2d69f1a", "", "[未莱生活送大额红包] 本场开奖啦！", "/pages/game/red-bad-group", null, null, null);
//		  com.graphai.mall.core.util.PushUtil.pushToSingle("871799e3003ad27ebb15fbbbe2d69f1a", "[未莱生活送大额红包] 本场开奖啦！", " ", "/pages/game/red-bad-game", null, 4, null);

		/*
			!!谨慎使用，会影响线上
		*/
		////com.graphai.mall.core.util.PushUtil.pushMessageToApp("","[未莱生活送大额红包] 本场开奖啦！","/pages/game/red-bad-group",null,null,null);
		/*
			!!谨慎使用，会影响线上
		*/

	  }
	  
	  /**
	     * Style0 系统样式
	     * @link http://docs.getui.com/getui/server/java/template/查看效果
	     * @return
	     */
	    public static AbstractNotifyStyle getStyle0(Integer status) {
	        Style0 style = new Style0();
	        
	        if (-3==status) {
	        	// 设置通知栏标题与内容
		        style.setTitle("肯德基取消订单");
		        style.setText("您在肯德基取消了部分订单");
			}else if (-5==status) {
				// 设置通知栏标题与内容
		        style.setTitle("肯德基取消订单");
		        style.setText("您在肯德基取消了订单");
			}else if (-10==status) {
				// 设置通知栏标题与内容
		        style.setTitle("肯德基订单退款");
		        style.setText("您在肯德基退款了订单");
			}else if (10==status) {
				// 设置通知栏标题与内容
		        style.setTitle("肯德基订单已出票");
		        style.setText("您在肯德基购买的订单已出票");
			}
	        	
			
	        
	        // 配置通知栏图标
	        style.setLogo("icon.png"); //配置通知栏图标，需要在客户端开发时嵌入，默认为push.png
	        // 配置通知栏网络图标
	        style.setLogoUrl("");

	        // 设置通知是否响铃，震动，或者可清除
	        style.setRing(true);
	        style.setVibrate(true);
	        style.setClearable(true);
	        style.setChannel("Default");
	        style.setChannelName("Default");
	        style.setChannelLevel(3); //设置通知渠道重要性
	        return style;
	    }
	    
	    
	    private static APNPayload getAPNPayload(Integer status) {
	        APNPayload payload = new APNPayload();
	        //在已有数字基础上加1显示，设置为-1时，在已有数字上减1显示，设置为数字时，显示指定数字
	        //payload.setAutoBadge("+1");
	        payload.setContentAvailable(0);
	        //ios 12.0 以上可以使用 Dictionary 类型的 sound
//	        payload.setSound("default");
//	        payload.setCategory("123456");
//	        Long nowTime=System.currentTimeMillis();
//	        payload.addCustomMsg("date",nowTime);
	        //简单模式APNPayload.SimpleMsg
	        
	        if (-3==status) {
		        payload.setAlertMsg(new APNPayload.SimpleAlertMsg("您在肯德基已取消了部分订单"));
			}else if (-5==status) {
		        payload.setAlertMsg(new APNPayload.SimpleAlertMsg("您在肯德基已取消了订单"));
			}else if (-10==status) {
		        payload.setAlertMsg(new APNPayload.SimpleAlertMsg("您在肯德基的订单已退款"));
			}else if (10==status) {
				payload.setAlertMsg(new APNPayload.SimpleAlertMsg("您在肯德基的订单已出票")); 
			}
	        
	        //payload.setAlertMsg(getDictionaryAlertMsg());  //字典模式使用APNPayload.DictionaryAlertMsg

	        //设置语音播报类型，int类型，0.不可用 1.播放body 2.播放自定义文本
	        //payload.setVoicePlayType(2);
	        //设置语音播报内容，String类型，非必须参数，用户自定义播放内容，仅在voicePlayMessage=2时生效
	        //注：当"定义类型"=2, "定义内容"为空时则忽略不播放
	        //payload.setVoicePlayMessage("定义内容");

	        // 添加多媒体资源
	        payload.addMultiMedia(new MultiMedia()
	                .setResType(MultiMedia.MediaType.pic)
	                .setResUrl("资源文件地址")
	                .setOnlyWifi(true));

	        return payload;
	    }
	    
	    
	    private static APNPayload.DictionaryAlertMsg getDictionaryAlertMsg( ) {
	        APNPayload.DictionaryAlertMsg alertMsg = new APNPayload.DictionaryAlertMsg();
	        alertMsg.setBody("body1");
	        alertMsg.setActionLocKey("显示关闭和查看两个按钮的消息");
	        alertMsg.setLocKey("loc-key1");
	        alertMsg.addLocArg("loc-ary1");
	        alertMsg.setLaunchImage("调用已经在应用程序中绑定的图形文件名");
	     // 设置通知栏标题与内容 
	        alertMsg.setTitle("肯德基取消订单");
	        alertMsg.setTitleLocKey("您在肯德基取消了部分订单");
	        alertMsg.addTitleLocArg("您在肯德基取消了部分订单");
	        // iOS8.2以上版本支持
//	        if (-3==status) {
//	        	// 设置通知栏标题与内容 
//		        alertMsg.setTitle("肯德基取消订单");
//		        alertMsg.setTitleLocKey("您在肯德基取消了部分订单");
//		        alertMsg.addTitleLocArg("您在肯德基取消了部分订单");
//			}else if (-5==status) {
//				// 设置通知栏标题与内容
//				alertMsg.setTitle("肯德基取消订单");
//		        alertMsg.setTitleLocKey("您在肯德基取消了订单");
//		        alertMsg.addTitleLocArg("您在肯德基取消了订单");
//			}else if (-10==status) {
//				// 设置通知栏标题与内容
//		        alertMsg.setTitle("肯德基订单退款");
//		        alertMsg.setTitleLocKey("您在肯德基退款了订单");
//		        alertMsg.addTitleLocArg("您在肯德基退款了订单");
//			}else if (10==status) {
//				// 设置通知栏标题与内容
//		        alertMsg.setTitle("肯德基订单已出票");
//		        alertMsg.setTitleLocKey("您在肯德基购买的订单已出票");
//		        alertMsg.addTitleLocArg("您在肯德基购买的订单已出票");
//			}
	        
	        return alertMsg;
	    }

	    /**
	     * 点击通知打开应用模板, 在通知栏显示一条含图标、标题等的通知，用户点击后激活您的应用。
	     * 通知模板(点击后续行为: 支持打开应用、发送透传内容、打开应用同时接收到透传 这三种行为)
	     * @return
	     */
	    public static NotificationTemplate getNotificationTemplate(Integer status) {
	        NotificationTemplate template = new NotificationTemplate();
	        // 设置APPID与APPKEY
	        template.setAppId(appId);
	        template.setAppkey(appKey);
	        //设置展示样式 
	        template.setStyle(getStyle0(status));
	        //template.setTransmissionType(2);  // 透传消息设置，收到消息是否立即启动应用： 1为立即启动，2则广播等待客户端自启动
	        //template.setTransmissionContent("请输入您要透传的内容");
//	        template.setSmsInfo(PushSmsInfo.getSmsInfo()); //短信补量推送

//	        template.setDuration("2019-07-09 11:40:00", "2019-07-09 12:24:00");  // 设置定时展示时间，安卓机型可用
	        //template.setNotifyid(123); // 在消息推送的时候设置notifyid。如果需要覆盖此条消息，则下次使用相同的notifyid发一条新的消息。客户端sdk会根据notifyid进行覆盖。
	        template.setAPNInfo(getAPNPayload(status)); //ios消息推送
	        return template;
	    }
	    
	    
	    /**
	     * 点击通知打开应用模板, 在通知栏显示一条含图标、标题等的通知，用户点击后激活您的应用。
	     * 通知模板(点击后续行为: 支持打开应用、发送透传内容、打开应用同时接收到透传 这三种行为)
	     * @return
	     */
	    public static TransmissionTemplate getTransmissionTemplate(Integer status) {
	    	TransmissionTemplate template = new TransmissionTemplate();
	        // 设置APPID与APPKEY
	        template.setAppId(appId);
	        template.setAppkey(appKey);
	        //设置展示样式 
	        //template.setStyle(getStyle0(status));
	        template.setTransmissionType(1);  // 透传消息设置，收到消息是否立即启动应用： 1为立即启动，2则广播等待客户端自启动
	        template.setTransmissionContent("请输入您要透传的内容");
//	        template.setSmsInfo(PushSmsInfo.getSmsInfo()); //短信补量推送

//	        template.setDuration("2019-07-09 11:40:00", "2019-07-09 12:24:00");  // 设置定时展示时间，安卓机型可用
	        //template.setNotifyid(123); // 在消息推送的时候设置notifyid。如果需要覆盖此条消息，则下次使用相同的notifyid发一条新的消息。客户端sdk会根据notifyid进行覆盖。
	        template.setAPNInfo(getAPNPayload(status)); //ios消息推送
	        return template;
	    }
	    
	    
	    /**
	     * 对单个用户推送消息
	     * <p>
	     * 场景1：某用户发生了一笔交易，银行及时下发一条推送消息给该用户。
	     * <p>
	     * 场景2：用户定制了某本书的预订更新，当本书有更新时，需要向该用户及时下发一条更新提醒信息。
	     * 这些需要向指定某个用户推送消息的场景，即需要使用对单个用户推送消息的接口。
	     */
	    public static void pushToSingle(String clientId,Integer status) {
	        AbstractTemplate template =getNotificationTemplate(status); //通知模板(点击后续行为: 支持打开应用、发送透传内容、打开应用同时接收到透传 这三种行为)
	        
	        
//	        AbstractTemplate template = PushTemplate.getLinkTemplate(); //点击通知打开(第三方)网页模板
//	        AbstractTemplate template = PushTemplate.getTransmissionTemplate(); //透传消息模版
//	        AbstractTemplate template = PushTemplate.getRevokeTemplate(); //消息撤回模版
//	        AbstractTemplate template = PushTemplate.getStartActivityTemplate(); //点击通知, 打开（自身）应用内任意页面
	        
	        
	        //AbstractTemplate template =getTransmissionTemplate(status);

	        // 单推消息类型
	        SingleMessage message = getSingleMessage(template);
	        Target target = new Target();
	        target.setAppId(appId);
	        target.setClientId(clientId);

	        IPushResult ret = null;
	        IGtPush push = new IGtPush(url, appKey, masterSecret);
	        try {
	            ret = push.pushMessageToSingle(message, target);
	        } catch (RequestException e) {
	            e.printStackTrace();
	            ret = push.pushMessageToSingle(message, target, e.getRequestId());
	        }
	        if (ret != null) {
	        	log.info("推送通知结果"+ret.getResponse().toString());
	        } else {
	        	log.error("服务器响应异常");
	        }
	    }
	    
	    
	    private static SingleMessage getSingleMessage(AbstractTemplate template) {
	        SingleMessage message = new SingleMessage();
	        message.setData(template);
	        // 设置消息离线，并设置离线时间
	        message.setOffline(true);
	        // 离线有效时间，单位为毫秒，可选
	        message.setOfflineExpireTime(72 * 3600 * 1000);
	        // 判断客户端是否wifi环境下推送。1为仅在wifi环境下推送，0为不限制网络环境，默认不限
	        message.setPushNetWorkType(0);
//	        message.setPushNetWorkType(1); // 判断客户端是否wifi环境下推送。1为仅在wifi环境下推送，0为不限制网络环境，默认不限
	        // 厂商下发策略；1: 个推通道优先，在线经个推通道下发，离线经厂商下发(默认);2: 在离线只经厂商下发;3: 在离线只经个推通道下发;4: 优先经厂商下发，失败后经个推通道下发;
	        message.setStrategyJson("{\"default\":1,\"ios\":2}");
	        return message;
	    }


}
