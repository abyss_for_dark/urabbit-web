package com.graphai.mall.wx.web.delivery;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.json.JacksonUtil;
import com.graphai.mall.setting.domain.FreightTemplate;
import com.graphai.mall.setting.domain.FreightTemplateChild;
import com.graphai.mall.setting.service.FreightTemplateChildService;
import com.graphai.mall.setting.service.FreightTemplateService;
import com.graphai.open.mallgoods.entity.IMallGoods;
import com.graphai.open.mallgoods.service.IMallGoodsService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/wx/delivery")
public class DeliveryController {

    @Resource
    private FreightTemplateChildService freightTemplateChildService;

    @Resource
    private FreightTemplateService freightTemplateService;

    @Resource
    private IMallGoodsService imallGoodsService;

    /**
     * 检查商品是否在配送范围
     */
    @PostMapping("/examineGoods")
    public Object examineGoods(@RequestBody String body){
        List<String> goodsIds = JacksonUtil.parseStringList(body, "goodsIds");
        String province = JacksonUtil.parseString(body, "province");
        List<String> list = new ArrayList<>();
        if(goodsIds.size() > 0){
            goodsIds.forEach(goodsId->{
                IMallGoods goods = imallGoodsService.getOne(new QueryWrapper<IMallGoods>().lambda().eq(IMallGoods::getId, goodsId));
                String[] split = province.split(",");
                //如果商品的运费模板是空的，则默认配送
                if(ObjectUtil.isNotNull(goods.getTemplateId()) && ObjectUtil.isNotEmpty(goods.getTemplateId())) {
                    FreightTemplate freightTemplate = freightTemplateService.getById(goods.getTemplateId());

                    if (!"FREE".equals(freightTemplate.getPricingMethod())) {
                        List<FreightTemplateChild> freightTemplateChild = freightTemplateChildService.list(new QueryWrapper<FreightTemplateChild>().lambda()
                                .eq(FreightTemplateChild::getFreightTemplateId, goods.getTemplateId())
                                .like(FreightTemplateChild::getArea, split[0])
                                .or(fc -> {
                                    fc.like(FreightTemplateChild::getArea, split[1]);
                                })
                        );
                        if (ObjectUtil.isEmpty(freightTemplateChild)) {
                            list.add(goodsId);
                        }
                    }
                }
            });
        }
        return ResponseUtil.ok(list);
    }
}
