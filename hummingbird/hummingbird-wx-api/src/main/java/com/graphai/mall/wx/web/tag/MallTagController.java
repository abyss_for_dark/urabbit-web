package com.graphai.mall.wx.web.tag;


import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.obj.ObjectUtils;
import com.graphai.mall.admin.domain.MallTag;
import com.graphai.mall.admin.service.IMallTagMallUserService;
import com.graphai.mall.admin.service.IMallTagWangzhuanTaskService;
import com.graphai.mall.db.service.tag.IMallTagService;
import com.graphai.mall.db.vo.MallTagVo;
import com.graphai.mall.wx.annotation.LoginUser;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/wx/tag")
@Validated
public class MallTagController {

    @Resource
    private IMallTagService mallTagService;

    @Resource
    private IMallTagMallUserService mallTagMallUserService;

    @Resource
    private IMallTagWangzhuanTaskService mallTagWangzhuanTaskService;

    /**
     * 显示所有用户类型的标签
     * @return
     */
    @GetMapping("/listTag")
    public Object listTag(){
        List<MallTagVo> mallTagVos= mallTagService.listUserTag();
        return ResponseUtil.ok(mallTagVos);
    }

    /**
     * 插入用户标签关联数据
     * @param mallTags
     * @return
     */
    @PostMapping("/insertUserTag")
    public Object insertUserTag(@LoginUser String userId, @RequestBody String data ){
        JSONObject jsonObject = JSONUtil.parseObj(data);
        if (userId==null){
            return ResponseUtil.unlogin();
        }
        List<Map<String,String>> list = (List<Map<String,String>>) jsonObject.get("mallTags");
        //如果用户传递的标签集合为空,说明用户一个标签都不要
        if (ObjectUtils.isEmpty(list)){
            //把用户可能拥有存在的标签设置为逻辑删除状态
            mallTagMallUserService.delete(userId);
        }else{
            mallTagMallUserService.insertUserTag(list,userId);
        }
        return ResponseUtil.ok();
    }

    /**
     * 获取该用户拥有的未删除标签
     * @return
     */
    @GetMapping("/listUserTag")
    public Object listUserTag(@LoginUser String userId){
        if (userId==null){
            return ResponseUtil.unlogin();
        }
        List<MallTag> mallTags=mallTagMallUserService.listUserTag(userId);
        return ResponseUtil.ok(mallTags);
    }
}
