package com.graphai.mall.wx.web.user;

import static com.graphai.mall.wx.util.WxResponseCode.AUTH_CAPTCHA_UNMATCH;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import cn.hutool.extra.qrcode.QrCodeUtil;
import com.graphai.mall.admin.domain.MallMerchant;
import com.graphai.mall.admin.domain.MallUserMerchant;
import com.graphai.mall.admin.service.*;
import com.graphai.mall.db.constant.user.MallUserEnum;
import com.graphai.mall.db.domain.*;
import com.graphai.mall.db.service.coupon.MallCouponUserService;
import com.graphai.mall.db.service.user.behavior.MallCollectService;
import com.graphai.mall.db.util.OrderUtil;
import com.graphai.open.mallcoupon.entity.IMallCoupon;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.houbb.sensitive.core.api.SensitiveUtil;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.util.StringUtil;
import com.graphai.commons.util.IpUtil;
import com.graphai.commons.util.NameUtil;
import com.graphai.commons.util.PushUtil;
import com.graphai.commons.util.RegexUtil;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.bcrypt.BCryptPasswordEncoder;
import com.graphai.commons.util.date.DateUtilTwo;
import com.graphai.commons.util.json.JacksonUtil;
import com.graphai.commons.util.lang.StringHelper;
import com.graphai.id.GraphaiIdGenerator;
import com.graphai.mall.admin.domain.MallRedQrcodeRecord;
import com.graphai.mall.admin.domain.MallUserProfitNew;
import com.graphai.commons.util.json.JacksonUtils;
import com.graphai.mall.db.service.MallInvitationCodeService;
import com.graphai.mall.db.service.account.AccountService;
import com.graphai.mall.db.constant.account.AccountStatus;
import com.graphai.mall.db.service.account.FcAccountCapitalBillService;
import com.graphai.mall.db.service.account.FcAccountPrechargeBillService;
import com.graphai.mall.db.service.account.FcAccountRechargeBillService;
import com.graphai.mall.db.service.bmdesign.BmdesignBusinessFormService;
import com.graphai.mall.db.service.bmdesign.BmdesignUserLevelService;
import com.graphai.mall.db.service.category.MallCategoryService;
import com.graphai.mall.db.service.common.MallPhoneValidationService;
import com.graphai.mall.db.service.order.MallOrderService;
import com.graphai.mall.db.service.promotion.MarketingSignRecordService;
import com.graphai.mall.db.service.sysconfig.MallSystemConfigService;
import com.graphai.mall.db.service.user.MallUserLevelChangeService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.service.user.behavior.MallFootprintService;
import com.graphai.mall.db.service.user.behavior.MallUserCardService;
import com.graphai.mall.db.constant.CommonConstant;
import com.graphai.mall.db.util.RetrialUtils;
import com.graphai.mall.db.util.account.AccountUtils;
import com.graphai.mall.db.util.phone.PushUtils;
import com.graphai.mall.db.vo.FcAccountRechargeBillVo;
import com.graphai.mall.db.vo.MallOrderVo;
import com.graphai.mall.db.vo.MallUserAgentVo;
import com.graphai.mall.db.vo.MallUserTeamVo;
import com.graphai.mall.db.vo.SetUserBirthdayVo;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.mall.wx.service.UserTokenManager;
import com.graphai.validator.Order;
import com.vdurmont.emoji.EmojiParser;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSONNull;
import cn.hutool.json.JSONUtil;

/**
 * 用户服务
 */
@RestController
@RequestMapping("/wx/user")
@Validated
public class WxUserController {
    private final Log logger = LogFactory.getLog(WxUserController.class);
    public static final String BASE64_PREFIX = "data:image/png;base64,";
    private final static Log logger2 = LogFactory.getLog(WxUserController.class);

    @Autowired
    private MallOrderService orderService;
    @Autowired
    private MallUserService userService;
    @Autowired
    private AccountService accountService;
    @Autowired
    private MallFootprintService footprintService;
    @Autowired
    private FcAccountPrechargeBillService fcAccountPrechargeBillService;
    @Autowired
    private MallPhoneValidationService phoneService;
    @Autowired
    private MallInvitationCodeService mallInvitationCodeService;
    @Autowired
    private MallUserCardService mallUserCardService;
    @Autowired
    private BmdesignUserLevelService bmdesignUserLevelService;
    @Autowired
    private FcAccountRechargeBillService fcAccountRechargeBillService;
    @Autowired
    private FcAccountCapitalBillService fcAccountCapitalBillService;
    @Autowired
    private BmdesignBusinessFormService bmdesignBusinessFormService;
    @Autowired
    private MallSystemConfigService systemConfigService;
    @Autowired
    private MallCategoryService categoryService;
    @Autowired
    private MallSystemConfigService mallSystemConfigService;
    @Autowired
    private MallUserLevelChangeService mallUserLevelChangeService;
    @Autowired
    private MarketingSignRecordService marketingSignRecordService;
    @Autowired
    private IMallRedQrcodeRecordService iMallRedQrcodeRecordService;
    @Autowired
    private MallCouponUserService couponUserService;

    @Value("${hscard.privateKey}")
    private String privateKey;

    @Autowired
    private IMallUserProfitNewService serviceImpl;
    @Autowired
    private MallCollectService mallCollectService;
    @Autowired
    private MallFootprintService mallFootprintService;
    @Autowired
    private IMallUserMerchantService mallUserMerchantService;
    @Autowired
    private IMallMerchantService mallMerchantService;
//    @Autowired
//    public PublicPlatformParameter pp;
    @Value("${public.urlPrefix}")
    private String urlPrefix;

    /**
     * 用户个人页面数据
     * <p>
     * 目前是用户订单统计信息
     *
     * @param userId 用户ID
     * @return 用户个人页面数据
     */
    @GetMapping("index")
    public Object list(@LoginUser String userId) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }

        Map<Object, Object> data = new HashMap<Object, Object>();
        data.put("order", orderService.orderInfo(userId));
        return ResponseUtil.ok(data);
    }

    /**
     * 获取个人用户信息
     *
     * @param userId
     * @return
     */
    @GetMapping("info")
    public Object getUserInfo(@LoginUser String userId, @RequestParam(defaultValue = "") String version,
                              HttpServletRequest request) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        Map<Object, Object> result = new HashMap<Object, Object>();
        MallUser user = userService.findById(userId);
        if (user == null) {
            return ResponseUtil.badArgumentValue("用户不存在！");
        }

        BmdesignUserLevel bmdesignUserLevel = bmdesignUserLevelService.selectBmdesignUserLevelById(user.getLevelId());


        boolean isExamine = RetrialUtils.examine(request, version);
        if (isExamine) {
            user.setIsActivate((byte) 1);
        }
        if (null != user.getNickname()) {
            user.setNickname(EmojiParser.parseToUnicode(user.getNickname()));
        }


        if(StringUtils.isNotEmpty(user.getFirstLeader())){
            // 用户商户关联表
            QueryWrapper<MallUserMerchant> mallUserMerchantWrapper = new QueryWrapper<MallUserMerchant>();
            mallUserMerchantWrapper.eq("user_id", user.getFirstLeader());
            List<MallUserMerchant> mallUserMerchant = mallUserMerchantService.list(mallUserMerchantWrapper);

            MallMerchant mallMerchant = null;
            for (MallUserMerchant temp: mallUserMerchant) {
                // 商户信息
                QueryWrapper<MallMerchant> merchantWrapper = new QueryWrapper<MallMerchant>();
                merchantWrapper.eq("status", 0);// 正常营业
                merchantWrapper.eq("audit_status", 1);// 审核通过
                merchantWrapper.eq("id",temp.getMerchantId());
                mallMerchant = mallMerchantService.getOne(merchantWrapper);

                if(mallMerchant != null){
                    break;
                }
            }

            result.put("directMerchant", mallMerchant);
        }

        if(!user.getLevelId().equals("0")){
            // 用户商户关联表
            QueryWrapper<MallUserMerchant> mallUserMerchantWrapper = new QueryWrapper<MallUserMerchant>();
            mallUserMerchantWrapper.eq("user_id", user.getId());
            List<MallUserMerchant> mallUserMerchant = mallUserMerchantService.list(mallUserMerchantWrapper);

            MallMerchant mallMerchant = null;
            for (MallUserMerchant temp: mallUserMerchant) {
                // 商户信息
                QueryWrapper<MallMerchant> merchantWrapper = new QueryWrapper<MallMerchant>();
                merchantWrapper.eq("status", 0);// 正常营业
                merchantWrapper.eq("audit_status", 1);// 审核通过
                merchantWrapper.eq("id",temp.getMerchantId());
                mallMerchant = mallMerchantService.getOne(merchantWrapper);

                if(mallMerchant != null){
                    break;
                }
            }
            result.put("userMerchant", mallMerchant);
        }

        result.put("userInfo", user);
        result.put("userLevel", bmdesignUserLevel);
//        if (!isExamine) {
            // if (StringUtils.isEmpty(user.getDirectLeader())) {
            // return ResponseUtil.other(1000, "用户未被邀请！", result);
            // }
//        }

        return ResponseUtil.ok(result);
    }

    /**
     * 获取个人用户信息
     *
     * @param userId
     * @return
     */
    @GetMapping("getUserDetail")
    public Object getUserDetail(@LoginUser String userId, @RequestParam(defaultValue = "") String version,
                              HttpServletRequest request) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        // 商品收藏
        Byte a = 0;
        int goodsCnt = mallCollectService.countByType(userId, a);
        // 文章收藏
        a = 2;
        int articleCnt = mallCollectService.countByType(userId, a);
        // 店铺关注
        a = 3;
        int merchantCnt = mallCollectService.countByType(userId, a);
        // 我的足迹
        long footprintCnt = mallFootprintService.findByUserIdAllCount(userId);
        //优惠劵数量
        Long couponTotal = couponUserService.queryTotal(userId);
        //积分
        FcAccount fcAccount = accountService.getUserAccount(userId);
        //钱包
        BigDecimal amount = fcAccount.getAmount();
        //粉丝
        Long fans = userService.queryByDirectLeader(userId);
        Map<String,Object> map = new HashMap<>();
        if(fcAccount != null){
            BigDecimal integralAmount = fcAccount.getIntegralAmount();
            map.put("integralAmount",integralAmount);
        }

        map.put("goodsCnt",goodsCnt);
        map.put("articleCnt",articleCnt);
        map.put("merchantCnt",merchantCnt);
        map.put("footprintCnt",footprintCnt);
        map.put("couponTotal",couponTotal);
        map.put("amount",amount);
        map.put("fans",fans);
        return ResponseUtil.ok(map);
    }

    /**
     * 获取我的团队
     * @param userId
     * @param page
     * @param limit
     * @return
     */
    @GetMapping("getTeam")
    public Object getTeam(@LoginUser String userId,@RequestParam(defaultValue = "1") Integer page
            ,@RequestParam(defaultValue = "10") Integer limit){
        List<Object> fans = new ArrayList<>();

        List<MallUser> mallUsers = userService.getUserListFans(userId,page,limit);
        long total = PageInfo.of(mallUsers).getTotal();
        List<String> bids = new ArrayList<>();
        bids.add(AccountStatus.BID_53);
        bids.add(AccountStatus.BID_55);
        bids.add(AccountStatus.BID_56);
        bids.add(AccountStatus.BID_57);
        bids.add(AccountStatus.BID_58);
        for (MallUser temp:mallUsers) {
            Map<String, Object> map = new HashMap<>();

            temp.setMobile(NameUtil.nicknameDesensitization(temp.getMobile()));
            List<MallUser> tempMallUsers = userService.getUserListFans(temp.getId(),null,null);

//            // 收益
//            BigDecimal profit = fcAccountRechargeBillService.getAccountRechargeBillListByDirectLeader(userId,
//                    temp.getId(),bids,1,"01");

            // 收益
            BigDecimal profit = fcAccountPrechargeBillService.getAccountRechargeBillListByDirectLeader(userId, temp.getId(),bids,1);

            map.put("user", temp);
            map.put("teamCnt", tempMallUsers.size());
            map.put("profit", profit);
            fans.add(map);
        }

        // 跳转首页
        String url = urlPrefix + "/pages/home/index";
        String qrcodImg = BASE64_PREFIX + Base64.getEncoder().encodeToString(QrCodeUtil.generatePng(url,250, 250));

        Map<String ,Object> map = new HashMap<>();
        map.put("fans", fans);
        map.put("fansCnt", fans.size());
        map.put("total",total);
        map.put("qrcodImg", qrcodImg);
        return ResponseUtil.ok(map);
    }

    /**
     * 获取我的团队粉丝
     * @param userId
     * @param page
     * @param limit
     * @return
     */
    @GetMapping("getTeamFans")
    public Object getTeamFans(@LoginUser String userId,@RequestParam(defaultValue = "1") Integer page
            ,@RequestParam(defaultValue = "10") Integer limit){
        List<Map<String,Object>> fans = new ArrayList<>();

        List<MallUser> mallUsers = userService.getUserListFans(userId,page,limit);
        for (MallUser temp:mallUsers) {
            List<MallUser> tempMallUsers = userService.getUserListFans(temp.getId(),null,null);

            for (MallUser mall:tempMallUsers) {
                Map<String, Object> map = new HashMap<>();

                mall.setMobile(NameUtil.nicknameDesensitization(mall.getMobile()));
                List<MallUser> tempMallUsers2 = userService.getUserListFans(mall.getId(),null,null);

                map.put("user", mall);
                map.put("teamCnt", tempMallUsers2.size());
                map.put("invitationUser", temp);
                fans.add(map);
            }
        }
        Map<String,Object> map = new HashMap<>();
        map.put("fans",fans);
        map.put("fansCnt",fans.size());
        return ResponseUtil.ok(map);
    }

    @GetMapping("subordinat")
    public Object getFenxiaoSubordinate(@LoginUser String userId) {
        try {
            List<MallUser> userList = userService.getFenxiaoSubordinate(userId);

            return ResponseUtil.ok(userList);
        } catch (Exception e) {
            logger.error("查询下线数据异常:" + e.getMessage(), e);
        }

        return ResponseUtil.fail();
    }

    /**
     * 获取我的客户
     *
     * @param userId
     * @return
     */
    @GetMapping("my/customer")
    public Object getMyCustomer(@LoginUser String userId) {
        try {
            List<MallUser> userList = userService.getMyCustoerList(userId);

            return ResponseUtil.ok(userList);
        } catch (Exception e) {
            logger.error("查询我的客户异常:" + e.getMessage(), e);
        }

        return ResponseUtil.fail();
    }

    /**
     * 绑定支付宝账户
     *
     * @param userId
     * @param account
     * @param userName
     * @return
     */
    @PostMapping("binding/alipay")
    public Object bindingAlipayAccount(@LoginUser String userId, @RequestParam("account") String account,
                    @RequestParam("userName") String userName) {
        try {
            MallUser user = new MallUser();
            user.setId(userId);
            user.setAlipayAccount(account);
            user.setAlipayUsername(userName);
            userService.updateById(user);

            return ResponseUtil.ok("绑定支付宝账户成功");
        } catch (Exception e) {
            logger.error("绑定支付宝账户成功异常:" + e.getMessage(), e);
        }

        return ResponseUtil.fail();
    }

    @PostMapping("update/info")
    public Object updateUserInfo(@LoginUser String userId,@RequestBody MallUser postUser) {
        try {
            if (userId == null) {
                return ResponseUtil.unlogin();
            }
            // 判断用户身份,代理身份无法修改昵称
            MallUser userInfo = userService.findById(userId);
            if (ObjectUtil.isEmpty(userInfo)){
                return ResponseUtil.fail("无法修改此用户!");
            }
            MallUser user = new MallUser();
            user.setId(userId);
            if (StringUtils.isNotBlank(postUser.getNickname())) {
                user.setNickname(postUser.getNickname());
            }
            if (StringUtils.isNotBlank(postUser.getAvatar())) {
                user.setAvatar(postUser.getAvatar());
            }
            if (StringUtils.isNotBlank(postUser.getGender())) {
                user.setGender(postUser.getGender());
            }
            userService.updateById(user);
            return ResponseUtil.ok(user);
        } catch (Exception e) {
            logger.error("修改用户信息异常:" + e.getMessage(), e);
        }

        return ResponseUtil.fail();
    }


    @GetMapping("my/customerV2")
    public Object getMyCustomerV2(@LoginUser String userId) {
        try {
            if (userId == null) {
                return ResponseUtil.unlogin();
            }
            // List<Map<String,Object>> results = new ArrayList<Map<String,Object>>();
            // List<MallUser> userList= userService.getMyCustoerList(userId);
            //
            // logger.info("开玩笑 ： " + JacksonUtils.bean2Jsn(userList));
            // if(CollectionUtils.isNotEmpty(userList)){
            // for (int i = 0; i < userList.size(); i++) {
            // Map<String,Object> result = new HashMap<String, Object>();
            // MallUser myUser = userList.get(i);
            //
            // result.put("myUser", myUser);
            // Integer footprintId = myUser.getFootprintId();
            // if(footprintId!=null){
            // MallFootprint footprint = footprintService.findById(footprintId);
            // result.put("myLastFootprint", footprint);
            // }
            // results.add(result);
            // }
            // }
            // return ResponseUtil.ok(results);

            List<MallUser> userList = userService.getMyCustoerList(userId);

            return ResponseUtil.ok(userList);
        } catch (Exception e) {
            logger.error("查询我的客户异常:" + e.getMessage(), e);
        }

        return ResponseUtil.fail();
    }


    /**
     * 我的团队 mobile(搜索条件)：手机号 levelId(会员等级) choose(粉丝类型)： all 全部粉丝、first 直属粉丝、other 间接粉丝 sort(排序字段)id
     * order(顺序、倒序)
     */
    @GetMapping("/getUserTeam")
    public Object getUserTeam(@LoginUser String userId, @RequestParam(required = false) String mobile,
                    @RequestParam(required = false) String levelId, @RequestParam(defaultValue = "all") String choose,
                    @RequestParam(defaultValue = "add_time") String sort,
                    @Order @RequestParam(defaultValue = "desc") String order) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        // 查询当前用户身份
        MallUser user = userService.findById(userId);
        if (user == null) {
            return ResponseUtil.badArgumentValue("用户不存在！");
        }
        List<MallUserTeamVo> allUserList = null;
        List<MallUserTeamVo> userList = null;

//        allUserList =  userService.getUserFansByUserId2(userId, null, null, "all", null, null);


        if (user.getLevelId().equals(AccountStatus.LEVEL_4) || user.getLevelId().equals(AccountStatus.LEVEL_5)
                        || user.getLevelId().equals(AccountStatus.LEVEL_6)) {
            /** 代理 **/
//            allUserList = userService.getUserTeamByUserId2(userId, null, null, "all", null, null);
//
//
//
//            for (MallUserTeamVo vo : allUserList) {
//                String voLevelId = vo.getLevelId();
//
//                  if(voLevelId.equals("0")){
//                      vo.setLevelName("普通用户");
//                  }
//                  if(voLevelId.equals("1")){
//                      vo.setLevelName("VIP");
//
//                  }
//                  if(voLevelId.equals("4")){
//                      vo.setLevelName("联盟商家");
//
//                  }
//                  if(voLevelId.equals("5")){
//                      vo.setLevelName("区代");
//
//                  }
//                  if(voLevelId.equals("6")){
//                      vo.setLevelName("市代");
//
//                  }
//
//            }



            userList = userService.getUserTeamByUserId2(userId, mobile, levelId, choose, "add_time", order);



            List<String> collect = userList.stream().map(MallUserTeamVo::getId).collect(Collectors.toList());
            /**拿到所有粉丝的粉丝数量*/
            List<Map> fensByUserId = userService.getFensByUserId(collect);
            for (MallUserTeamVo vo : userList) {
                List<Map> id = fensByUserId.stream().filter(map -> map.get("id").equals(vo.getId())).collect(Collectors.toList());
                if(null != id && id.size()>0){
                    vo.setTotalNum(MapUtils.getInteger(id.get(0), "totalNum"));
                }
                String voLevelId = vo.getLevelId();

                if(voLevelId.equals("0")){
                    vo.setLevelName("普通用户");
                }
                if(voLevelId.equals("1")){
                    vo.setLevelName("VIP");

                }
                if(voLevelId.equals("4")){
                    vo.setLevelName("联盟商家");

                }
                if(voLevelId.equals("5")){
                    vo.setLevelName("区代");

                }
                if(voLevelId.equals("6")){
                    vo.setLevelName("市代");

                }

            }
            //内存排序
            if ("totalNum".equals(sort) && "asc".equalsIgnoreCase(order)){
                userList = userList.stream().sorted(Comparator.comparing(MallUserTeamVo::getTotalNum)).collect(Collectors.toList());
            }

            if ("totalNum".equals(sort) && "desc".equalsIgnoreCase(order)){
                userList = userList.stream().sorted(Comparator.comparing(MallUserTeamVo::getTotalNum)).collect(Collectors.toList());
                Collections.reverse(userList);
            }

        } else {
            /** 普通用户、VIP **/
//            allUserList = userService.getUserTeamByUserId(userId, null, null, "all", null, null);
//
//            for (MallUserTeamVo vo : allUserList) {
//                String voLevelId = vo.getLevelId();
//
//                if(voLevelId.equals("0")){
//                    vo.setLevelName("普通用户");
//                }
//                if(voLevelId.equals("1")){
//                    vo.setLevelName("VIP");
//
//                }
//                if(voLevelId.equals("4")){
//                    vo.setLevelName("联盟商家");
//
//                }
//                if(voLevelId.equals("5")){
//                    vo.setLevelName("区代");
//
//                }
//                if(voLevelId.equals("6")){
//                    vo.setLevelName("市代");
//
//                }
//
//            }
            userList = userService.getUserTeamByUserId(userId, mobile, levelId, choose, "add_time", order);

            List<String> collect = userList.stream().map(MallUserTeamVo::getId).collect(Collectors.toList());
            /**拿到所有粉丝的粉丝数量*/
            List<Map> fensByUserId = userService.getFensByUserId(collect);

            for (MallUserTeamVo vo : userList) {
                String voLevelId = vo.getLevelId();
                List<Map> id = fensByUserId.stream().filter(map -> map.get("id").equals(vo.getId())).collect(Collectors.toList());
                if(null != id && id.size()>0){
                    vo.setTotalNum(MapUtils.getInteger(id.get(0), "totalNum"));
                }
                if(voLevelId.equals("0")){
                    vo.setLevelName("普通用户");
                }
                if(voLevelId.equals("1")){
                    vo.setLevelName("VIP");

                }
                if(voLevelId.equals("4")){
                    vo.setLevelName("联盟商家");

                }
                if(voLevelId.equals("5")){
                    vo.setLevelName("区代");

                }
                if(voLevelId.equals("6")){
                    vo.setLevelName("市代");

                }

            }
            //内存排序
            if ("totalNum".equals(sort) && "asc".equalsIgnoreCase(order)){
                userList = userList.stream().sorted(Comparator.comparing(MallUserTeamVo::getTotalNum)).collect(Collectors.toList());
            }

            if ("totalNum".equals(sort) && "desc".equalsIgnoreCase(order)){
                userList = userList.stream().sorted(Comparator.comparing(MallUserTeamVo::getTotalNum)).collect(Collectors.toList());
                Collections.reverse(userList);
            }

        }

        // 用户名字、推荐人名字脱敏
        // userList = SensitiveUtil.desCopyCollection(userList);
        String index = "1";
        Map<String, Object> map = new HashMap<>();
//        map.put("totalNum", allUserList.size());
        map.put("totalNum", userList.size());
        map.put("userList", userList);

        // 邀请好友的类目信息
        MallCategory mallCategory = categoryService.findById("1259030897824231425");
        // 邀请好友的内链地址
        map.put("linkUrl", mallCategory.getLinkUrl());
        return ResponseUtil.ok(map);
    }

    /**
     *  封装粉丝数量&直推人名字
     * @param allUserList
     * @param fans
     * @param fansDirectLeader
     */
    private void packageFansAndDir(List<MallUserTeamVo> allUserList, List<Map<String, Object>> fans, List<Map<String, Object>> fansDirectLeader) {

        for (MallUserTeamVo vo : allUserList) {
            String id = vo.getId();
            String directLeader = vo.getDirectLeader();
            //封装fans
            if (CollectionUtils.isNotEmpty(fans)) {
                Optional<Map<String, Object>> optional = fans.stream().filter(obj -> MapUtils.getString(obj, "direct_leader").equals(id)).findFirst();
                if (optional.isPresent()) {
                    Map<String, Object> map = optional.get();
                    vo.setTotalNum(MapUtils.getInteger(map, "fans"));
                }
            }
            //封装直推人名字
            if (StringUtils.isNotBlank(directLeader) && CollectionUtils.isNotEmpty(fansDirectLeader)){
                Optional<Map<String, Object>> optionalDl = fansDirectLeader.stream().filter(obj -> MapUtils.getString(obj, "id").equals(directLeader)).findFirst();
                if (optionalDl.isPresent()){
                    Map<String, Object> map = optionalDl.get();
                    vo.setFirstLeaderName(MapUtils.getString(map, "nickname"));
                }
            }
        }
    }

    /**
     * 查看我的邀请人（ 当前用户权限： 1.可以查看下级的预估收益 2.上级的头像、姓名）
     */
    @GetMapping("/getMyFirstLeader")
    public Object getMyFirstLeader(@LoginUser String userId, @RequestParam(required = false) String subUserId) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        Map<String, Object> map = new HashMap<>();

        // 当前用户
        MallUser mallUser = userService.findById(userId);
        String directLeaderId = mallUser.getDirectLeader();
        if (StringUtils.isNotEmpty(subUserId)) {
            // 查询下级
            BigDecimal lastMonthAward = new BigDecimal(0.00); // 上月预估收益
            BigDecimal totalAward = new BigDecimal(0.00); // 累计预估收益

            MallUser subUser = userService.findById(subUserId);
            directLeaderId = subUser.getDirectLeader(); // 推荐人

            // 获取当前月份
            int monthValue = LocalDate.now().getMonthValue();
            // 获取当前年份
            int year = LocalDate.now().getYear();
            LocalDateTime endDate = LocalDateTime.of(year, monthValue, 1, 0, 0, 0); // 结束时间
            LocalDateTime beginDate = endDate.minusMonths(1); // 开始时间

            lastMonthAward = fcAccountPrechargeBillService.selectPrechargeAmountByCondition(subUser.getId(), true, null,
                            AccountStatus.DISTRIBUTION_TYPE_00, CommonConstant.TASK_REWARD_CASH, beginDate, endDate);
            totalAward = fcAccountPrechargeBillService.selectPrechargeAmountByCondition(subUser.getId(), true, null,
                            AccountStatus.DISTRIBUTION_TYPE_00, CommonConstant.TASK_REWARD_CASH, null, null);

            map.put("userInfo", subUser);
            map.put("lastMonthAward", lastMonthAward);
            map.put("totalAward", totalAward);
        }
        // 查询上级
        MallUser firstUser = userService.findById(directLeaderId);
        map.put("firstUser", firstUser);
        return ResponseUtil.ok(map);
    }


    /**
     * 查询我的骑士卡订单
     */
    @PostMapping("/findCardOrder")
    public Object findCardOrder(@LoginUser String userId) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        List<MallOrderVo> orderList = orderService.findCardOrder(userId, AccountStatus.BID_14);
        Map<String, Object> map = new HashMap<>();
        map.put("orderList", orderList);
        return ResponseUtil.ok(map);

    }

    /**
     * 用户激活 {"name":"","activateCode":"","mobile":"","code":"","status":""}
     */
    @PostMapping("/userActivate")
    @Transactional(rollbackFor = {Exception.class})
    public Object userActivate(@RequestBody String body, HttpServletRequest request) throws Exception {
        String name = JacksonUtil.parseString(body, "name"); // 姓名
        String activateCode = JacksonUtil.parseString(body, "activateCode"); // 激活码
        String mobile = JacksonUtil.parseString(body, "mobile"); // 手机号
        String code = JacksonUtil.parseString(body, "code"); // 验证码
        String status = JacksonUtil.parseString(body, "status"); // 是否发送验证码(0:不发送 1:发送)
        String checkStatus = "1"; // 用来校验是否发送验证码,默认1(发送)
        if (StringUtils.isEmpty(name)) {
            return ResponseUtil.badArgumentValue("未获取到姓名！");
        }
        if (StringUtils.isEmpty(activateCode)) {
            return ResponseUtil.badArgumentValue("未获取到激活码！");
        }
        if (StringUtils.isEmpty(mobile)) {
            return ResponseUtil.badArgumentValue("未获取到手机号！");
        }
        if (StringUtils.isNotEmpty(status)) {
            checkStatus = status;
        }


        /** 验证验证码是否正确 **/
        MallPhoneValidation mallPhoneValidation = null;
        if (checkStatus.equals("1")) {
            if (code == null) {
                return ResponseUtil.badArgumentValue("未获取到验证码！");
            }
            /** 通过手机号、手机验证码 判断验证码是否正确 **/
            String smsType = "APP_ACTIVATE";
            mallPhoneValidation = phoneService.getPhoneValidationByCode(mobile, code, smsType);
            if (mallPhoneValidation == null) {
                return ResponseUtil.fail(AUTH_CAPTCHA_UNMATCH, "验证码错误");
            }
        }


        /** 验证激活码是否正确 **/
        MallUser mallUser = new MallUser();
        List<MallUser> mallUserList = userService.queryByMobile(mobile);
        if (mallUserList.size() == 0) {
            mallUser.setMobile(mobile);
            mallUser.setNickname(mobile);
            mallUser.setLastLoginTime(LocalDateTime.now());
            mallUser.setLastLoginIp(IpUtil.getIpAddr(request));
            mallUser.setUserLevel((byte) 0);
            mallUser.setLevelId(AccountStatus.LEVEL_0);
            userService.add(mallUser);
            // 开户
            if (mallUser.getId() != null) {
                logger.info("开户成功..............");
                accountService.accountTradeCreateAccount(mallUser.getId());
            }
        } else if (mallUserList.size() == 1) {
            mallUser = mallUserList.get(0);
        } else {
            return ResponseUtil.badArgumentValue("存在多条记录！");
        }
        if (mallUser.getIsActivate() == 1) {
            return ResponseUtil.badArgumentValue("你已经激活了,无需再次激活！");
        }


        /** 激活码失效 **/
        MallInvitationCode mallInvitationCode = new MallInvitationCode();
        mallInvitationCode.setCode(activateCode);
        mallInvitationCode.setUseStatus("000");
        List<MallInvitationCode> mallInvitationCodeList = mallInvitationCodeService
                        .selectMallInvitationCodeList(mallInvitationCode, null, null, null, null);
        if (mallInvitationCodeList.size() == 0) {
            return ResponseUtil.badArgumentValue("激活码不存在或已经失效！");
        }
        mallInvitationCode = mallInvitationCodeList.get(0);
        MallInvitationCode updateCode = new MallInvitationCode();
        updateCode.setId(mallInvitationCode.getId());
        updateCode.setUseStatus("111");
        updateCode.setUseTime(LocalDateTime.now());
        updateCode.setSpendUserId(mallUser.getId());
        mallInvitationCodeService.updateMallInvitationCode(updateCode);
        /** 用户卡激活 **/
        /** 通过userId、是否激活 查询可激活的卡号信息 **/
        MallUserCard userCard = new MallUserCard();
        userCard.setUserId(mallUser.getId());
        List<MallUserCard> mallUserCardList =
                        mallUserCardService.selectMallUserCardList(userCard, null, null, null, null);
        if (mallUserCardList.size() > 0) {
            userCard = mallUserCardList.get(0);
            userCard.setActivationCode(activateCode);
            userCard.setActivateTime(LocalDateTime.now());
            userCard.setSpendUserId(mallUser.getId());
            mallUserCardService.updateMallUserCard(userCard);
        }
        /** 用户激活,同时成为会员 **/
        MallUser updateUser = new MallUser();
        updateUser.setId(mallUser.getId());
        updateUser.setLevelId(AccountStatus.LEVEL_1); // 激活卡成为 'vip用户'
        updateUser.setIsActivate((byte) 1);
        userService.updateById(updateUser);

        if (checkStatus.equals("1")) {
            /** 使验证码失效 **/
            MallPhoneValidation mallPhoneValidation1 = new MallPhoneValidation();
            mallPhoneValidation1.setId(mallPhoneValidation.getId());
            mallPhoneValidation1.setValState("00");
            phoneService.update(mallPhoneValidation1);
        }

        // 消息推送
        PushUtils.pushMessage(mallUser.getId(), AccountStatus.PUSH_TITLE_ACTIVATE, AccountStatus.PUSH_CONTENT_ACTIVATE,
                        2, null);

        // todo 用户激活会员，上级获得直推奖励
        // String firstLeaderId = mallUser.getFirstLeader();
        // if (StringUtils.isNotBlank(firstLeaderId)) {
        // //查询 业务形态的直推奖励
        // BmdesignBusinessForm bmdesignBusinessForm =
        // bmdesignBusinessFormService.selectBmdesignBusinessFormById(AccountStatus.BID_11);
        // BigDecimal reward = bmdesignBusinessForm.getReward(); //奖励金额
        // String rewardType = bmdesignBusinessForm.getRewardType(); //奖励类型
        // Map<String, Object> paramsMap = new HashMap<>();
        //// paramsMap.put("orderId", order.getId());
        //// paramsMap.put("orderSn", order.getOrderSn());
        // paramsMap.put("buyerId", mallUser.getId());
        // paramsMap.put("tpid", firstLeaderId);
        // paramsMap.put("dType", AccountStatus.DISTRIBUTION_TYPE_00);
        // paramsMap.put("message", "推荐奖励");
        // paramsMap.put("capitalTrend", AccountStatus.CAPITAL_TREND_01);
        // paramsMap.put("changeType", AccountStatus.CHANGE_TYPE_01);
        // paramsMap.put("changeAccountType",AccountStatus.CHANGE_ACCOUNT_TYPE_1);
        // if(reward.compareTo(new BigDecimal(0.00)) == 1){
        // AccountUtils.accountChange(firstLeaderId, reward, rewardType, AccountStatus.BID_11, paramsMap);
        // }
        // }
        return ResponseUtil.ok("用户激活成功！");

    }

    /** 锁定关系 */
    @GetMapping("/lockRelation")
    public Object lockRelation(@LoginUser String userId, @RequestParam(required = true) String code) {
        // 用户
        MallUser mallUser = userService.findById(userId);
        // 直推人
        MallUser mallUser1 = userService.selectMallUserByCode(code);
        if (null != mallUser1) {
            if (mallUser1.getId().equals(userId)) {
                return ResponseUtil.fail(403, "请勿填写自己的邀请码！");
            }
            mallUser.setDirectLeader(mallUser1.getId());
            //set绑定直推人时间
            mallUser.setLockTime(LocalDateTime.now());
            userService.updateById(mallUser);
        } else {
            return ResponseUtil.fail(403, "邀请人不存在！");
        }
        return ResponseUtil.ok();
    }

    /** -------------------------------代理端接口------------------------------- **/
    /**
     * 代理端小程序_区级代理、服务商查询
     * 
     * @param isAgent true:区级代理 false:服务商
     * @param param 搜索条件：账号/手机号/姓名
     * @return
     */
    @GetMapping("/getAgentList")
    public Object getAgentList(@LoginUser String userId, @RequestParam Boolean isAgent,
                    @RequestParam(required = false) String param, @RequestParam(defaultValue = "1") Integer page,
                    @RequestParam(defaultValue = "10") Integer limit) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        MallUser mallUser = userService.findById(userId);
        String levelId = mallUser.getLevelId();
        String province = mallUser.getProvince();
        String city = mallUser.getCity();
        String county = null;

        if (StringUtils.isEmpty(levelId)) {
            return ResponseUtil.badArgumentValue("用户身份有误！");
        }

        if (levelId.equals(AccountStatus.LEVEL_4) || levelId.equals(AccountStatus.LEVEL_5)) {
            county = mallUser.getCounty();
        }

        List<MallUserAgentVo> userList = null;
        // 查询分润计算方式(key_value 0:服务商、区代、市代方式 1:用户firstLeader、secondLeader、thirderLeader方式 )
        Map<String, String> profitTypeMap = mallSystemConfigService.listProfitType();
        String profitType = profitTypeMap.get("Mall_profit_type");
        if (Objects.equals(profitType, "0")) {
            /** 方式一(根据省市区) **/
            if (isAgent) {
                // 区级代理
                if (!(levelId.equals(AccountStatus.LEVEL_5) || levelId.equals(AccountStatus.LEVEL_6))) {
                    return ResponseUtil.badArgumentValue("用户无权限！");
                }
                userList = userService.getUserListByCondition(isAgent, province, city, county, AccountStatus.LEVEL_5,
                                param, page, limit);
            } else {
                // 服务商
                userList = userService.getUserListByCondition(isAgent, province, city, county, AccountStatus.LEVEL_4,
                                param, page, limit);
            }
        } else {
            /** 方式二(根据userId) **/
            // 普通用户、VIP
            List<String> levelIdList = new ArrayList<>();
            levelIdList.add(AccountStatus.LEVEL_0);
            levelIdList.add(AccountStatus.LEVEL_1);
            // 服务商
            List<String> levelIdList2 = new ArrayList<>();
            if (isAgent) {
                // 区级代理
                if (!(levelId.equals(AccountStatus.LEVEL_5) || levelId.equals(AccountStatus.LEVEL_6))) {
                    return ResponseUtil.badArgumentValue("用户无权限！");
                }
                levelIdList2.add(AccountStatus.LEVEL_5);
            } else {
                // 服务商
                levelIdList2.add(AccountStatus.LEVEL_4);
            }
            userList = userService.getUserListByCondition2(userId, levelIdList2, param, page, limit);

            for (int i = 0; i < userList.size(); i++) {
                // 普通用户、VIP总数
                int userTotal = userService
                                .getUserListByCondition2(userList.get(i).getId(), levelIdList, null, null, null).size();
                userList.get(i).setTotal(userTotal);
                if (isAgent) {
                    // 服务商总数
                    List<String> levelIdList3 = new ArrayList<>();
                    levelIdList.add(AccountStatus.LEVEL_4);
                    int serviceTotal = userService
                                    .getUserListByCondition2(userList.get(i).getId(), levelIdList3, null, null, null)
                                    .size();
                    userList.get(i).setServiceTotal(serviceTotal);
                }
            }
        }
        long total = PageInfo.of(userList).getTotal();
        /** 脱敏处理 **/
        if (userList.size() > 0) {
            // 用户昵称脱敏
            for (int i = 0; i < userList.size(); i++) {
                String nickname = userList.get(i).getNickname();
                if (StringUtils.isNotEmpty(nickname)) {
                    // 判断用户昵称类型
                    if (!RegexUtil.isMobileExact(nickname)) {
                        // 不是手机号
                        nickname = NameUtil.nameDesensitizationDef(nickname);
                    } else {
                        // 是手机号
                        nickname = NameUtil.mobileEncrypt(nickname);
                    }
                    userList.get(i).setNickname(nickname);
                }
            }
            // 手机号脱敏
            userList = SensitiveUtil.desCopyCollection(userList);
        }

        Map<String, Object> map = new HashMap<>();
        map.put("list", userList);
        map.put("total", total);
        return ResponseUtil.ok(map);
    }

    /**
     * 代理端小程序_查询用户list信息 type:1、全部 2、直推会员 3、间推会员
     */
    @GetMapping("/getUserList")
    public Object getUserList(@LoginUser String userId, @RequestParam(defaultValue = "1") String type,
                    @RequestParam(required = false) String param, @RequestParam(defaultValue = "1") Integer page,
                    @RequestParam(defaultValue = "10") Integer limit) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        MallUser mallUser = userService.findById(userId);
        String province = mallUser.getProvince();
        String city = mallUser.getCity();
        String county = "";
        if (mallUser.getLevelId().equals(AccountStatus.LEVEL_4)) {
            // 服务商
            county = mallUser.getCounty();
        } else if (mallUser.getLevelId().equals(AccountStatus.LEVEL_5)) {
            // 区级代理
            county = mallUser.getCounty();
        }


        // 查询分润计算方式(key_value 0:服务商、区代、市代方式 1:用户firstLeader、secondLeader、thirderLeader方式 )
        Map<String, String> profitTypeMap = mallSystemConfigService.listProfitType();
        String profitType = profitTypeMap.get("Mall_profit_type");

        List<MallUserAgentVo> userList = new ArrayList<>();
        if (Objects.equals(profitType, "0")) {
            /** 方式一(根据省市区) **/
            userList = userService.getUserListByType(false, userId, type, province, city, county, param, page, limit);
        } else {
            /** 方式二(根据userId) **/
            userList = userService.getUserListByType(true, userId, type, null, null, null, param, page, limit);
        }
        long total = PageInfo.of(userList).getTotal();
        /** 脱敏处理 **/
        if (userList.size() > 0) {
            // 用户昵称脱敏
            for (int i = 0; i < userList.size(); i++) {
                String nickname = userList.get(i).getNickname();
                if (StringUtils.isNotEmpty(nickname)) {
                    // 判断用户昵称类型
                    if (!RegexUtil.isMobileExact(nickname)) {
                        // 不是手机号
                        nickname = NameUtil.nameDesensitizationDef(nickname);
                    } else {
                        // 是手机号
                        nickname = NameUtil.mobileEncrypt(nickname);
                    }
                    userList.get(i).setNickname(nickname);
                }
            }
            // 手机号脱敏
            userList = SensitiveUtil.desCopyCollection(userList);
        }

        Map<String, Object> map = new HashMap<>();
        map.put("userList", userList);
        map.put("total", total);
        map.put("date", LocalDate.now());
        return ResponseUtil.ok(map);
    }

    /**
     * 代理端小程序_查询代理详情
     */
    @GetMapping("/getAgentDetail")
    public Object getAgentDetail(@RequestParam String userId) {
        MallUser user = userService.findById(userId);
        return ResponseUtil.ok(user);
    }

    /**
     * 代理端小程序_修改代理信息(todo 后期看是否需要)
     */
    @PostMapping("/updateAgent")
    public Object updateAgent(@RequestBody MallUser mallUser) {
        userService.updateById(mallUser);
        return ResponseUtil.ok();
    }

    /**
     * 代理端小程序_修改用户密码
     */
    @PostMapping("/resetAgent")
    public Object resetAgent(@LoginUser String userId, @RequestBody String body) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        String mobile = JacksonUtil.parseString(body, "mobile"); // 新手机号
        String oldMobile = JacksonUtil.parseString(body, "oldMobile"); // 旧手机号
        String code = JacksonUtil.parseString(body, "code"); // 验证码
        String password = JacksonUtil.parseString(body, "password"); // 密码
        String rePassword = JacksonUtil.parseString(body, "rePassword"); // 确认密码
        String type = JacksonUtil.parseString(body, "type"); // 1.修改手机号 2.修改密码
        MallUser mallUser = new MallUser();
        mallUser.setId(userId);
        if (StringUtils.isEmpty(type)) {
            return ResponseUtil.badArgument();
        }


        MallPhoneValidation mallPhoneValidation = null;
        if ("1".equals(type)) {
            /** 修改手机号 **/
            if (StringUtils.isEmpty(mobile)) {
                return ResponseUtil.badArgumentValue("未获取到新手机号！");
            }
            if (StringUtils.isEmpty(oldMobile)) {
                return ResponseUtil.badArgumentValue("未获取到旧手机号！");
            }
            if (StringUtils.isEmpty(code)) {
                return ResponseUtil.badArgumentValue("未获取到验证码！");
            }
            if (!RegexUtil.isMobileExact(mobile)) {
                return ResponseUtil.badArgumentValue("请输入正确的手机号!");
            }

            // 通过旧手机号、手机验证码 判断验证码是否正确
            String smsType = "APP_MOD_PWD";
            mallPhoneValidation = phoneService.getPhoneValidationByCode(oldMobile, code, smsType);
            if (mallPhoneValidation == null) {
                return ResponseUtil.fail(AUTH_CAPTCHA_UNMATCH, "验证码错误");
            }

            /** todo 判断手机号是否重复 **/
            List<String> levelIds = new ArrayList<>();
            levelIds.add(AccountStatus.LEVEL_4);
            levelIds.add(AccountStatus.LEVEL_5);
            levelIds.add(AccountStatus.LEVEL_6);
            List<MallUser> mallUserList = userService.selectUser(null, mobile, levelIds);
            if (mallUserList.size() > 0) {
                return ResponseUtil.badArgumentValue("手机号号码已经存在！");
            }
            mallUser.setMobile(mobile);
        } else if ("2".equals(type)) {
            /** 修改密码 **/
            if (StringUtils.isEmpty(password)) {
                return ResponseUtil.badArgumentValue("未获取到密码！");
            }
            if (StringUtils.isEmpty(rePassword)) {
                return ResponseUtil.badArgumentValue("请确认密码！");
            }
            // 判断密码是否一致
            if (!password.equals(rePassword)) {
                return ResponseUtil.badArgumentValue("两次输入的密码不一致！");
            }
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
            mallUser.setPassword(encoder.encode(password));
        }

        userService.updateById(mallUser);
        /** 验证码失效 **/
        if (mallPhoneValidation != null) {
            MallPhoneValidation mallPhoneValidation1 = new MallPhoneValidation();
            mallPhoneValidation1.setId(mallPhoneValidation.getId());
            mallPhoneValidation1.setValState("00");
            phoneService.update(mallPhoneValidation1);
        }
        return ResponseUtil.ok();
    }

    /**
     * 代理端小程序_重置密码
     */
    @PostMapping("/resetAgentPwd")
    public Object resetAgentPwd(@RequestBody String body) {
//        String username = JacksonUtil.parseString(body, "username"); // 账号
        String mobile = JacksonUtil.parseString(body, "mobile"); // 手机号
        String code = JacksonUtil.parseString(body, "code"); // 验证码
        String password = JacksonUtil.parseString(body, "password"); // 密码
//        String rePassword = JacksonUtil.parseString(body, "rePassword"); // 确认密码
//        if (StringUtils.isEmpty(username)) {
//            return ResponseUtil.badArgumentValue("未获取到账号！");
//        }
        if (StringUtils.isEmpty(mobile)) {
            return ResponseUtil.badArgumentValue("未获取到手机号！");
        } else {
            if (!RegexUtil.isMobileExact(mobile)) {
                return ResponseUtil.badArgumentValue("请输入正确的手机号!");
            }
        }
        if (StringUtils.isEmpty(code)) {
            return ResponseUtil.badArgumentValue("未获取到验证码！");
        }
//        if (StringUtils.isNotEmpty(password) && StringUtils.isNotEmpty(rePassword)) {
//            if (!password.equals(rePassword)) {
//                return ResponseUtil.badArgumentValue("两次输入的密码不一致！");
//            }
//        } else {
//            return ResponseUtil.badArgumentValue("未获取到密码！");
//        }
        /** 验证验证码是否正确 **/
        // 通过手机号、手机验证码 判断验证码是否正确
        String smsType = "APP_RESET_PASSWORD";
        MallPhoneValidation mallPhoneValidation = phoneService.getPhoneValidationByCode(mobile, code, smsType);
         if (mallPhoneValidation == null) {
            return ResponseUtil.fail(AUTH_CAPTCHA_UNMATCH, "验证码错误");
        }

        // 查询代理身份list
        BmdesignUserLevel bmdesignUserLevel = new BmdesignUserLevel();
        bmdesignUserLevel.setType("identity");
        List<BmdesignUserLevel> levelList = bmdesignUserLevelService.selectBmdesignUserLevelList(bmdesignUserLevel,
                        null, null, "level_num", "asc");
        List<String> levelIds = new ArrayList<>();
        MallUser mallUser = null;
        levelIds.add("0");
        levelIds.add("9");
        levelIds.add("10");
        levelIds.add("11");
        levelIds.add("98");
        List<MallUser> mallUserList = userService.selectUser(null, mobile, levelIds);
        if (mallUserList.size() == 1) {
            mallUser = mallUserList.get(0);
        }

        if (mallUser == null) {
            return ResponseUtil.badArgumentValue("用户不存在！");
        }

        /** 修改用户密码 **/
        MallUser updateUser = new MallUser();
        updateUser.setId(mallUser.getId());
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        updateUser.setPassword(encoder.encode(password));
        userService.updateById(updateUser);
        /** 使验证码失效 **/
        MallPhoneValidation mallPhoneValidation1 = new MallPhoneValidation();
        mallPhoneValidation1.setId(mallPhoneValidation.getId());
        mallPhoneValidation1.setValState("00");
        phoneService.update(mallPhoneValidation1);

        return ResponseUtil.ok();
    }

    /** -------------------------------代理端接口------------------------------- **/



    /** -------------------------------未莱卡接口------------------------------- **/
    /**
     * 花生卡_获取用户会员卡信息、开卡 (注：直推人必为代理身份)
     */
    @PostMapping("/getUserCardInfo")
    public Object getUserCardInfo(@RequestBody String body, HttpServletRequest request) throws Exception {

        logger.info("body参数： " + JacksonUtils.bean2Jsn(body));
//        String sign = JacksonUtil.parseString(body, "sign");
        String mobile = JacksonUtil.parseString(body, "mobile");
        if (StringUtils.isEmpty(mobile)) {
            return ResponseUtil.badArgumentValue("手机号不存在！");
        }
        synchronized (mobile) {
            String weixinMiniOpenid = JacksonUtil.parseString(body, "weixinOpenid");// 小程序openid
            String directLeaderId = JacksonUtil.parseString(body, "firstLeaderId"); // 直推人id
            String avatar = JacksonUtil.parseString(body, "avatar"); // 用户头像
            String nickname = JacksonUtil.parseString(body, "nickname"); // 用户昵称
            String provinceId = JacksonUtil.parseString(body, "provinceId"); // 省code
            String cityId = JacksonUtil.parseString(body, "cityId"); // 市code
            String districtId = JacksonUtil.parseString(body, "districtId"); // 区code
            String province = JacksonUtil.parseString(body, "province"); // 省
            String city = JacksonUtil.parseString(body, "city"); // 市
            String district = JacksonUtil.parseString(body, "district"); // 区
//            if (StringUtils.isEmpty(sign)) {
//                return ResponseUtil.badArgumentValue("未获取到签名信息！");
//            }

            Map<String, Object> paramMap = new HashMap<>();
            paramMap.put("mobile", mobile);
            paramMap.put("firstLeaderId", directLeaderId);
            paramMap.put("avatar", avatar);
            paramMap.put("nickname", nickname);
            String string = JSONObject.toJSONString(paramMap);// 请求参数转换成字符串进行加密处理
            // boolean b = verifySignature(string, sign,privateKey);
            // if(!b){
            // return ResponseUtil.fail(403,"验签失败");
            // }
            if (StringUtils.isNotEmpty(nickname)) {
                nickname = EmojiParser.parseToAliases(nickname);
            }
            // 查询上级信息,校验firstLeaderId是否正确
            MallUser directLeader = null;
            if (StringUtils.isNotEmpty(directLeaderId)) {
                directLeader = userService.findById(directLeaderId);// 推荐人信息
                if (directLeader == null) {
                    return ResponseUtil.badArgumentValue("上级不存在！");
                }
            } /*
               * else{ directLeaderId = "00"; //todo 暂时默认平台系统用户的 id主键 }
               */


            // 查询用户信息
            List<MallUser> mallUserList = userService.queryByMobile(mobile);
            MallUser mallUser = new MallUser();
            Boolean isUpdate = false; // 用来判断是否需要修改用户信息
            if (mallUserList.size() == 0) {
                if (directLeader != null) {
                    mallUser.setDirectLeader(directLeaderId);
                    //set绑定直推人时间
                    mallUser.setLockTime(LocalDateTime.now());
                    if (directLeader.getLevelId().equals(AccountStatus.LEVEL_4)) {
                        // 服务商
                        mallUser.setFirstLeader(directLeader.getId());
                        mallUser.setSecondLeader(directLeader.getSecondLeader());
                        mallUser.setThirdLeader(directLeader.getThirdLeader());
                    } else if (directLeader.getLevelId().equals(AccountStatus.LEVEL_5)) {
                        // 区代
                        mallUser.setFirstLeader(directLeader.getFirstLeader());
                        mallUser.setSecondLeader(directLeader.getId());
                        mallUser.setThirdLeader(directLeader.getThirdLeader());
                    } else if (directLeader.getLevelId().equals(AccountStatus.LEVEL_6)) {
                        // 市代
                        mallUser.setFirstLeader(directLeader.getFirstLeader());
                        mallUser.setSecondLeader(directLeader.getSecondLeader());
                        mallUser.setThirdLeader(directLeader.getId());
                    }
                }
                if (!StringUtils.isEmpty(weixinMiniOpenid)) {
                    mallUser.setWeixinMiniOpenid(weixinMiniOpenid);
                }
                if (StringHelper.isNotNullAndEmpty(provinceId)) {
                    mallUser.setProvinceCode(Integer.parseInt(provinceId));
                }

                if (StringHelper.isNotNullAndEmpty(cityId)) {
                    mallUser.setCityCode(Integer.parseInt(cityId));
                }

                if (StringHelper.isNotNullAndEmpty(districtId)) {
                    mallUser.setAreaCode(Integer.parseInt(districtId));
                }


                if (StringHelper.isNotNullAndEmpty(province)) {
                    mallUser.setProvince(province);
                }

                if (StringHelper.isNotNullAndEmpty(city)) {
                    mallUser.setCity(city);
                }

                if (StringHelper.isNotNullAndEmpty(district)) {
                    mallUser.setCounty(district);
                }


                mallUser.setMobile(mobile);
                mallUser.setAvatar(avatar);
                if (StringUtils.isNotEmpty(nickname)) {
                    mallUser.setNickname(nickname);
                } else {
                    mallUser.setNickname(mobile);
                }


                /** 自动激活的用户 **/
                // 注册渠道为商圈
                mallUser.setChannelId(MallUserEnum.CHANNEL_ID_0.getCode());
                mallUser.setSource("商圈用户授权创建");
                userService.add(mallUser);
                // 开户
                if (mallUser.getId() != null) {
                    logger.info("开户成功..............");
                    accountService.accountTradeCreateAccount(mallUser.getId());
                }
            } else if (mallUserList.size() == 1) {
                mallUser = mallUserList.get(0);
                if (!StringUtils.isEmpty(weixinMiniOpenid)) {
                    mallUser.setWeixinMiniOpenid(weixinMiniOpenid);
                }
                // 若用户直推人不存在,则添加默认直推人
                if (StringUtils.isEmpty(mallUser.getDirectLeader())) {
                    isUpdate = true;
                    mallUser.setDirectLeader(directLeaderId);
                    //set绑定直推人时间
                    mallUser.setLockTime(LocalDateTime.now());
                }
                // 判断用户上级身份(若当前用户的上三级都是空的,则进行修改逻辑)
                if (directLeader != null) {
                    if (directLeader.getLevelId().equals(AccountStatus.LEVEL_4)
                                    || directLeader.getLevelId().equals(AccountStatus.LEVEL_5)
                                    || directLeader.getLevelId().equals(AccountStatus.LEVEL_6)) {
                        if (StringUtils.isEmpty(mallUser.getFirstLeader())
                                        && StringUtils.isEmpty(mallUser.getSecondLeader())
                                        && StringUtils.isEmpty(mallUser.getThirdLeader())) {
                            isUpdate = true;
                            if (directLeader.getLevelId().equals(AccountStatus.LEVEL_4)) {
                                // 服务商
                                mallUser.setFirstLeader(directLeader.getId());
                                mallUser.setSecondLeader(directLeader.getSecondLeader());
                                mallUser.setThirdLeader(directLeader.getThirdLeader());
                            } else if (directLeader.getLevelId().equals(AccountStatus.LEVEL_5)) {
                                // 区代
                                mallUser.setFirstLeader(directLeader.getFirstLeader());
                                mallUser.setSecondLeader(directLeader.getId());
                                mallUser.setThirdLeader(directLeader.getThirdLeader());
                            } else if (directLeader.getLevelId().equals(AccountStatus.LEVEL_6)) {
                                // 市代
                                mallUser.setFirstLeader(directLeader.getFirstLeader());
                                mallUser.setSecondLeader(directLeader.getSecondLeader());
                                mallUser.setThirdLeader(directLeader.getId());
                            }
                        }
                    }
                }

                if (null == mallUser.getProvinceCode() && StringHelper.isNotNullAndEmpty(provinceId)) {
                    mallUser.setProvinceCode(Integer.parseInt(provinceId));
                }

                if (null == mallUser.getCityCode() && StringHelper.isNotNullAndEmpty(cityId)) {
                    mallUser.setCityCode(Integer.parseInt(cityId));
                }

                if (null == mallUser.getAreaCode() && StringHelper.isNotNullAndEmpty(districtId)) {
                    mallUser.setAreaCode(Integer.parseInt(districtId));
                }


                if (StringUtils.isEmpty(mallUser.getProvince()) && StringHelper.isNotNullAndEmpty(province)) {
                    mallUser.setProvince(province);
                }

                if (StringUtils.isEmpty(mallUser.getCity()) && StringHelper.isNotNullAndEmpty(city)) {
                    mallUser.setCity(city);
                }

                if (StringUtils.isEmpty(mallUser.getCounty()) && StringHelper.isNotNullAndEmpty(district)) {
                    mallUser.setCounty(district);
                }

            } else {
                return ResponseUtil.badArgumentValue("存在多条记录！");
            }


            /** 如果用户没有激活，那就帮他激活（防止C端用户已经存在） **/
            MallUserCard cardInfo = null;
            if (directLeader != null) {
                if (mallUser.getIsActivate() == 0) {
                    isUpdate = true;
                    mallUser.setLevelId(AccountStatus.LEVEL_1); // 激活卡成为 'vip用户'
                    mallUser.setIsActivate((byte) 1);
                    // 卡信息
                    cardInfo = mallUserCardService.selectMallUserCardByUserId(mallUser.getId());
                    if (cardInfo != null) {
                        MallUserCard updateCard = new MallUserCard();
                        updateCard.setId(cardInfo.getId());
                        updateCard.setActivationCode("商圈用户自动激活");
                        updateCard.setActivateTime(LocalDateTime.now());
                        mallUserCardService.updateMallUserCard(updateCard);
                    }
                }
            }
            if (StringUtils.isNotEmpty(avatar) || StringUtils.isNotEmpty(nickname)
                            || StringUtils.isNotEmpty(weixinMiniOpenid)) {
                isUpdate = true;
                mallUser.setAvatar(avatar);
                mallUser.setNickname(nickname);
                mallUser.setWeixinMiniOpenid(weixinMiniOpenid);
            }

            // 同步省市区


            // 判断是否需要修改
            if (isUpdate) {
                userService.updateById(mallUser);
            }


            /** 账户余额信息 **/
            BigDecimal money = new BigDecimal(0);
            FcAccount fcAccount = accountService.getAccountByKey(mallUser.getId());
            if (fcAccount != null) {
                money = fcAccount.getAmount();
            }

            Map<String, Object> map = new HashMap<>();
            if (StringUtils.isNotEmpty(mallUser.getNickname())) {
                mallUser.setNickname(EmojiParser.parseToUnicode(mallUser.getNickname()));
            }
            String token = UserTokenManager.generateToken(mallUser.getId(), null, null);
            map.put("userInfo", mallUser);
            if (!ObjectUtil.equals(null, mallUser.getDirectLeader())) {
                MallUser directLeader1 = userService.findById(mallUser.getDirectLeader());// 推荐人信息
                if (ObjectUtil.isNotNull(directLeader1)) {
                    if ("1".equals(directLeader1.getIsMaker())) {
                        map.put("makerMobile", directLeader1.getMobile());
                    }
                }
            }

            map.put("cardInfo", cardInfo);
            map.put("money", money); // 账户余额
            map.put("token", token);

            return ResponseUtil.ok(map);

        }

    }

    /**
     * 查询充值记录
     */
    @GetMapping("/getRechargeList")
    public Object getRechargeList(@RequestParam String mobile, @RequestParam(defaultValue = "1") Integer page,
                    @RequestParam(defaultValue = "10") Integer limit) {
        // 通过手机号查询用户信息
        List<MallUser> mallUserList = userService.queryByMobile(mobile);
        MallUser mallUser = null;
        if (mallUserList.size() == 1) {
            mallUser = mallUserList.get(0);
        }
        if (mallUser == null) {
            return ResponseUtil.badArgumentValue("未查询或查询到多条用户信息！");
        }
        Integer start = (page - 1) * limit;
        BigDecimal totalRecharge = fcAccountRechargeBillService.selectRechargeAmountByCondition(mallUser.getId(), true,
                        null, AccountStatus.DISTRIBUTION_TYPE_00, CommonConstant.TASK_REWARD_CASH, null, null);
        List<FcAccountRechargeBillVo> rechargeList = fcAccountRechargeBillService.getAccountRechargeBillListByUserId(
                        mallUser.getId(), true, null, AccountStatus.DATA_STATUS_1, AccountStatus.DISTRIBUTION_TYPE_00,
                        CommonConstant.TASK_REWARD_CASH, start, limit); // AccountStatus.DISTRIBUTION_TYPE_00
        Map<String, Object> map = new HashMap<>();
        map.put("totalRecharge", totalRecharge); // 充值总金额
        map.put("rechargeList", rechargeList); // 充值记录
        return ResponseUtil.ok(map);
    }


    /**
     * 查询交易记录(余额明细)
     */
    @GetMapping("/getCapitalList")
    public Object getCapitalList(@RequestParam String mobile, @RequestParam(defaultValue = "1") Integer page,
                    @RequestParam(defaultValue = "10") Integer limit,
                    @RequestParam(defaultValue = "create_date") String sort,
                    @RequestParam(defaultValue = "desc") String order) {
        // 通过手机号查询用户信息
        List<MallUser> mallUserList = userService.queryByMobile(mobile);
        MallUser mallUser = null;
        if (mallUserList.size() == 1) {
            mallUser = mallUserList.get(0);
        }
        if (mallUser == null) {
            return ResponseUtil.badArgumentValue("未查询或查询到多条用户信息！");
        }
        // 查询账户信息
        FcAccount fcAccount = accountService.getAccountByKey(mallUser.getId());
        if (fcAccount == null) {
            return ResponseUtil.badArgumentValue("未查询到账户信息！");
        }

        FcAccountCapitalBill fcAccountCapitalBill = new FcAccountCapitalBill();
        fcAccountCapitalBill.setAccountId(fcAccount.getAccountId());
        List<FcAccountCapitalBill> capitalList = fcAccountCapitalBillService
                        .selectFcAccountCapitalBillList(fcAccountCapitalBill, page, limit, sort, order);
        Map<String, Object> map = new HashMap<>();
        map.put("capitalList", capitalList); // 余额明细
        return ResponseUtil.ok(map);
    }

    @GetMapping("/editVersion")
    public Object setVersion(@LoginUser String userId, @RequestParam(required = false) String version,
                    HttpServletRequest request) {
        // 通过手机号查询用户信息
        MallUser user = userService.findById(userId);
        String mobileTye = request.getHeader("User-Agent");

        if (user == null) {

        } else {
            user.setUserVersion(version);
            if (mobileTye.contains("iPhone")) {
                user.setLoginTerminal("2");
            } else if (mobileTye.contains("Android")) {
                user.setLoginTerminal("1");
            }
            if (userService.updateById(user) > 0) {
                return ResponseUtil.ok();
            }
        }

        return ResponseUtil.ok();
    }

    /**
     * 解绑 微信或淘宝
     * 
     * @param userId
     * @param type
     * @param
     * @return
     */
    @GetMapping("/editUser")
    public Object setUser(@LoginUser String userId, @RequestParam(required = false) String type) {

        if (null == userId) {
            return ResponseUtil.unlogin();
        }

        MallUser user = userService.findById(userId);
        if (user == null) {
            return ResponseUtil.fail("用户不存在！");
        } else {
            if ("1".equals(type)) {
                user.setWeixinOpenid("");
                user.setWxNickname("");
            } else if ("2".equals(type)) {
                user.setTbkRelationId("");
                user.setTbNickname("");
                user.setTbkAccessToken("");
                user.setTbkSpecialId("");
                user.setTbkPubId("");
            }
            if (userService.updateById(user) > 0) {
                return ResponseUtil.ok();
            }
        }

        return ResponseUtil.ok(user);
    }


    @PostMapping("/pushSms")
    public Object pushSms(@RequestBody String data) {
        String mobile = JacksonUtil.parseString(data, "mobile"); // 直推人id
        String content = JacksonUtil.parseString(data, "content"); // 用户头像
        String title = JacksonUtil.parseString(data, "title"); // 用户昵称
        // 通过手机号查询用户信息
        List<MallUser> mallUserList = userService.queryByMobile(mobile);
        MallUser mallUser = null;
        if (mallUserList.size() == 1) {
            mallUser = mallUserList.get(0);
        }
        if (mallUser == null) {
            return ResponseUtil.badArgumentValue("未查询或查询到多条用户信息！");
        }
        if (null != mallUser.getClientId()) {
            String clientId = mallUser.getClientId();
            if (!clientId.isEmpty() && !"".equals(clientId)) {
                PushUtil.pushToSingle(clientId, title, content, null, null, 4, null);
            }
        }
        return ResponseUtil.ok();
    }



    @PostMapping("/rechargeHSCard")
    @Transactional(rollbackFor = {Exception.class})
    public Object rechargeHSCard(@RequestBody String data) throws Exception {
        // 后面有可能修改
        // appID: wx89762a3ecedb1148 未莱卡
        // appSecret: b6a007a7cd7e77d13df3f2072aa44f4a 未莱卡
        // String privateKey ="b6a007a7cd7e77d13df3f2072aa44f4a";
        List<String> list = new ArrayList<>();
        if (JSONUtil.isJsonArray(data)) {
            List<cn.hutool.json.JSONObject> jsonObjects =
                            JSONUtil.parseArray(data).toList(cn.hutool.json.JSONObject.class);
            for (cn.hutool.json.JSONObject jsonObject : jsonObjects) {
                String mobile = jsonObject.getStr("mobile");
                BigDecimal amount = jsonObject.getBigDecimal("amount");
//                String sign = jsonObject.getStr("sign");
                String orderSn = jsonObject.getStr("orderSn");
                String type = jsonObject.getStr("type");
                cn.hutool.json.JSONObject order1 = jsonObject.getJSONObject("order");
                String merchantName = "";
                String agentName = "";
                String storeName = "";
                String str = "";
                String distance = "";
                String buyerPhone = "";
                String wlshUserId = "";
                BigDecimal receiptAmount = new BigDecimal("0.00");
                BigDecimal tradeAmount = new BigDecimal("0.00");
                if (!JSONNull.NULL.equals(order1) && null != order1) {
                    cn.hutool.json.JSONObject params = order1.getJSONObject("params");
                    merchantName = order1.getStr("merchantName");// 商户名称
                    agentName = order1.getStr("agentName");// 代理名称
                    if (!JSONNull.NULL.equals(params) && null != params) {
                        storeName = params.getStr("storeName");// 门店名称
                        wlshUserId = params.getStr("wlshUserId");// 代理id
                    }
                    receiptAmount = order1.getBigDecimal("receiptAmount");// 实付金额
                    tradeAmount = order1.getBigDecimal("tradeAmount");// 订单金额
                    distance = order1.getStr("distance");// 支付距离
                    buyerPhone = order1.getStr("buyerPhone");// 支付人手机号
                    if (null != order1.getStr("cashbackPattern") && !"".equals(order1.getStr("cashbackPattern"))) {
                        str = order1.getStr("cashbackPattern");
                    }
                }
                // 通过手机号查询用户信息
                List<MallUser> mallUserList = userService.queryByMobile(mobile);
                List<MallUser> mallUserList1 = new ArrayList<>();
                if (null != buyerPhone) {
                    mallUserList1 = userService.queryByMobile(buyerPhone);
                }
                MallUser mallUser = null;// 被充值人
                MallUser mallUser1 = null;// 下单人
                if (mallUserList.size() == 1) {
                    mallUser = mallUserList.get(0);
                }
                if (mallUserList1.size() == 1) {
                    mallUser1 = mallUserList1.get(0);
                }
                if (null == mallUser) {
                    return ResponseUtil.badArgumentValue("未查询或查询到多条用户信息！");
                }
                // 查询账户信息
                FcAccount fcAccount = accountService.getAccountByKey(mallUser.getId());
                if (fcAccount == null) {
                    return ResponseUtil.badArgumentValue("未查询到账户信息！");
                }
                Map map1 = null;
                if (amount.compareTo(new BigDecimal("0")) == 1) {
                    map1 = (Map) parse(str, type, orderSn, amount, fcAccount, mallUser);// 未来卡 充值。提现
                }
                MallOrder mallOrder = orderService.findBySn(orderSn);
                if (null == mallOrder) {
                    MallOrder order = new MallOrder();
                    order.setId(GraphaiIdGenerator.nextId("order"));
                    if (null != mallUser1) {
                        order.setUserId(mallUser1.getId());
                    } else {
                        order.setUserId("00");// 队列返
                    }
                    order.setOrderStatus((short) 108);
                    order.setActualPrice(receiptAmount);
                    order.setOrderPrice(tradeAmount);
                    order.setPayTime(LocalDateTime.now());
                    order.setOrderSn(orderSn);
                    order.setAddTime(LocalDateTime.now());
                    order.setUpdateTime(LocalDateTime.now());
                    if ("".equalsIgnoreCase(str) || "common".equalsIgnoreCase(str)) {
                        order.setMessage("未莱商圈消费返现");
                        order.setBid(AccountStatus.BID_31);
                    } else if ("ladder".equalsIgnoreCase(str)) {
                        order.setMessage("未莱商圈队列全额返现");
                        order.setBid(AccountStatus.BID_35);
                    }
                    order.setHuid("wlsq");
                    order.setStorename(storeName);
                    order.setAgentname(agentName);
                    order.setMerchantname(merchantName);
                    order.setPubSharePreFee(amount.toString());
                    order.setDistance(distance);
                    order.setMobile(buyerPhone);
                    order.setMerchantId(wlshUserId);
                    orderService.add(order);


                    /** 修改用户达标状态 **/
                    if (null != mallUser1) {
                        RetrialUtils.updateUserEligible(mallUser1);
                    }
                } else {
                    return ResponseUtil.badArgumentValue("订单已存在！");
                }
            }

        } else {
            cn.hutool.json.JSONObject jsonObject = JSONUtil.parseObj(data);
            String mobile = jsonObject.getStr("mobile");
//            String sign = jsonObject.getStr("sign");
            BigDecimal amount = jsonObject.getBigDecimal("amount");
            String orderSn = jsonObject.getStr("orderSn");
            String type = jsonObject.getStr("type");

            cn.hutool.json.JSONObject order1 = jsonObject.getJSONObject("order");
            String merchantName = "";
            String agentName = "";
            String storeName = "";
            String str = "";
            String distance = "";
            String buyerPhone = "";// 支付人手机号
            String wlshUserId = "";
            BigDecimal receiptAmount = new BigDecimal("0.00");
            BigDecimal tradeAmount = new BigDecimal("0.00");
            if (!JSONNull.NULL.equals(order1) && null != order1) {
                cn.hutool.json.JSONObject params = order1.getJSONObject("params");
                merchantName = order1.getStr("merchantName");// 商户名称
                agentName = order1.getStr("agentName");// 代理名称
                if (!JSONNull.NULL.equals(params) && null != params) {
                    storeName = params.getStr("storeName");// 门店名称
                    wlshUserId = params.getStr("wlshUserId");// 代理id
                }
                receiptAmount = order1.getBigDecimal("receiptAmount");// 实付金额
                tradeAmount = order1.getBigDecimal("tradeAmount");// 订单金额
                distance = order1.getStr("distance");// 支付距离
                buyerPhone = order1.getStr("buyerPhone");// 支付人手机号
                if (null != order1.getStr("cashbackPattern") && !"".equals(order1.getStr("cashbackPattern"))) {
                    str = order1.getStr("cashbackPattern");
                }

            }

            // Map<String, Object> map = new HashMap<>();
            // map.put("mobile", mobile);
            // map.put("type", type);
            // map.put("amount", amount);
            // map.put("orderSn", orderSn);
            // String string = JSONObject.toJSONString(map);//请求参数转换成字符串进行加密处理
            // boolean b = verifySignature(string, sign, privateKey);
            // 通过手机号查询用户信息
            List<MallUser> mallUserList = userService.queryByMobile(mobile);
            List<MallUser> mallUserList1 = new ArrayList<>();
            if (!StringUtils.isEmpty(buyerPhone)) {
                mallUserList1 = userService.queryByMobile(buyerPhone);
            }
            MallUser mallUser = null;// 被充值人
            MallUser mallUser1 = null;// 下单人
            if (mallUserList.size() == 1) {
                mallUser = mallUserList.get(0);
            }
            if (mallUserList1.size() == 1) {
                mallUser1 = mallUserList1.get(0);
            }
            if (null == mallUser) {
                return ResponseUtil.badArgumentValue("未查询或查询到多条用户信息！");
            }
            // 查询账户信息
            FcAccount fcAccount = accountService.getAccountByKey(mallUser.getId());
            if (fcAccount == null) {
                return ResponseUtil.badArgumentValue("未查询到账户信息！");
            }
            if (amount.compareTo(new BigDecimal("0")) == 1) {

                parse(str, type, orderSn, amount, fcAccount, mallUser);// 未来卡 充值。提现
            }
            // MallOrder mallOrder = orderService.findBySn(orderSn);
            // if(null == mallOrder){
            MallOrder order = new MallOrder();
            order.setId(GraphaiIdGenerator.nextId("order"));
            if (null != mallUser1) {
                order.setUserId(mallUser1.getId());
            } else {
                order.setUserId("00");// 队列返
            }
            order.setOrderStatus((short) 108);
            order.setActualPrice(receiptAmount);
            order.setOrderPrice(tradeAmount);
            order.setPayTime(LocalDateTime.now());
            order.setOrderSn(orderSn);
            order.setAddTime(LocalDateTime.now());
            order.setUpdateTime(LocalDateTime.now());
            if ("".equalsIgnoreCase(str) || "common".equalsIgnoreCase(str)) {
                order.setMessage("未莱商圈消费返现");
                order.setBid(AccountStatus.BID_31);
            } else if ("ladder".equalsIgnoreCase(str)) {
                order.setMessage("未莱商圈队列全额返现");
                order.setBid(AccountStatus.BID_35);
            }
            order.setHuid("wlsq");
            order.setStorename(storeName);
            order.setAgentname(agentName);
            order.setMerchantname(merchantName);
            order.setPubSharePreFee(amount.toString());
            order.setDistance(distance);
            order.setMobile(buyerPhone);
            order.setMerchantId(wlshUserId);
            orderService.add(order);


            /** 修改用户达标状态 **/
            if (null != mallUser1) {
                RetrialUtils.updateUserEligible(mallUser1);
            }
            // }else{
            // return ResponseUtil.badArgumentValue("订单已存在！");
            // }
        }
        return ResponseUtil.ok();
    }


    public Object parse(String str, String type, String orderSn, BigDecimal amount, FcAccount fcAccount,
                    MallUser mallUser) throws Exception {
        // 充值
        if ("1".equals(type)) {
            Map paramsMap = new HashMap();
            paramsMap.put("dType", AccountStatus.DISTRIBUTION_TYPE_00); // 直接分润

            paramsMap.put("capitalTrend", AccountStatus.CAPITAL_TREND_01);
            paramsMap.put("changeType", AccountStatus.CHANGE_TYPE_01);
            paramsMap.put("orderSn", orderSn);
            paramsMap.put("changeAccountType", AccountStatus.CHANGE_ACCOUNT_TYPE_1);
            if ("".equals(str) || "common".equalsIgnoreCase(str)) {
                paramsMap.put("message", "未莱商圈消费返现");
                paramsMap.put("tradeType", AccountStatus.TRADE_TYPE_11);
            } else if ("ladder".equalsIgnoreCase(str)) {
                paramsMap.put("message", "未莱商圈队列全额返现");
                paramsMap.put("tradeType", AccountStatus.TRADE_TYPE_14);
            }

            // BigDecimal reward = new BigDecimal(amount);
            // List<FcAccountRechargeBill> rechargeList =
            // fcAccountRechargeBillService.selectRechargeBillByOrderSn1(orderSn, mallUser.getId());
            if (amount.compareTo(new BigDecimal(0.00)) == 1) {
                AccountUtils.accountChange(mallUser.getId(), amount, CommonConstant.TASK_REWARD_CASH,
                                AccountStatus.BID_31, paramsMap);
            } else {
                return ResponseUtil.fail();
            }
        }
        // 扣款
        else if ("2".equals(type)) {
            Map paramsMap = new HashMap();
            paramsMap.put("dType", AccountStatus.DISTRIBUTION_TYPE_00); // 直接分润
            paramsMap.put("message", "未莱卡余额提现");
            paramsMap.put("capitalTrend", AccountStatus.CAPITAL_TREND_02);
            paramsMap.put("changeType", AccountStatus.CHANGE_TYPE_03);
            paramsMap.put("changeAccountType", AccountStatus.CHANGE_ACCOUNT_TYPE_1);
            paramsMap.put("tradeType", AccountStatus.TRADE_TYPE_07);
            // paramsMap.put("orderSn",orderSn);
            // BigDecimal reward = new BigDecimal(amount);
            // List<FcAccountRechargeBill> rechargeList =
            // fcAccountRechargeBillService.selectRechargeBillByOrderSn1(orderSn);
            if (amount.compareTo(new BigDecimal(0.00)) == 1 && null != fcAccount.getAmount()
                            && fcAccount.getAmount().compareTo(amount) >= 0) {

                AccountUtils.accountChange(mallUser.getId(), amount, CommonConstant.TASK_REWARD_CASH,
                                AccountStatus.BID_31, paramsMap);

            } else {
                return ResponseUtil.badArgumentValue("提现额度大于账户余额！");
            }
        }
        return ResponseUtil.ok();
    }


    /**
     * 签名验证 MD5
     * 
     * @param string 签名的字符串对象jsonStr
     * @param sign 请求签名
     * @param privateKey 签名秘钥
     * @return true通过 false不通过
     */
    private static boolean verifySignature(String string, String sign, String privateKey) {
        Map<String, Object> stringStringMap =
                        JSONObject.parseObject(string, new TypeReference<Map<String, Object>>() {});
        StringBuffer sb = new StringBuffer();

        TreeMap<String, Object> objectObjectTreeMap = new TreeMap<>();
        objectObjectTreeMap.putAll(stringStringMap);
        // logger.info("签名待处理json map{}", JSONObject.toJSONString(objectObjectTreeMap));

        for (String str : objectObjectTreeMap.keySet()) {
            Object v = objectObjectTreeMap.get(str);
            if (null != v && !"".equals(v)) {
                sb.append(str + "=" + v + "&");
            }
        }
        // MD5加密,结果转换为大写字符
        String str = String.valueOf(sb.append("key=" + privateKey));
        // logger.info("进行签名的字符串{}", str);
        String s = DigestUtils.md5Hex(str).toUpperCase();

        logger2.info("系统签名： " + s);
        logger2.info("请求签名： " + sign);

        if (s.equals(sign)) {
            // logger.info("校验通过");
            return true;
        } else {
            // logger.info("校验失败");
            return false;
        }
    }


    /**
     * 通过手机号创建代理账号账号(账号：手机号,密码：88888888) levelId：4、服务商 5、区级代理 6、市级代理
     */
    @PostMapping("/serviceAccount")
    public Object serviceAccount(@RequestBody String body, HttpServletRequest request) throws Exception {
        logger.info("创建代理接口_body参数： " + JacksonUtils.bean2Jsn(body));
//        String sign = JacksonUtil.parseString(body, "sign");
        String mobile = JacksonUtil.parseString(body, "mobile"); // 手机号
        String province = JacksonUtil.parseString(body, "province"); // 省
        String city = JacksonUtil.parseString(body, "city"); // 市
        String county = JacksonUtil.parseString(body, "county"); // 区
        String levelId = JacksonUtil.parseString(body, "levelId"); // 用户等级
        String provinceId = JacksonUtil.parseString(body, "provinceId");
        String cityId = JacksonUtil.parseString(body, "cityId");
        String countyId = JacksonUtil.parseString(body, "countyId");

        String directLeaderId = JacksonUtil.parseString(body, "firstLeaderId"); // 直推人id(默认平台系统用户的 id主键)
        String nickname = JacksonUtil.parseString(body, "nickname"); // 用户昵称
//        if (StringUtils.isEmpty(sign)) {
//            return ResponseUtil.badArgumentValue("未获取到签名信息！");
//        }
        if (StringUtils.isEmpty(mobile)) {
            return ResponseUtil.badArgumentValue("未获取到手机号！");
        }
        if (StringUtils.isEmpty(levelId)) {
            return ResponseUtil.badArgumentValue("未获取到用户等级！");
        }
        // if (!(levelId.equals(AccountStatus.LEVEL_4) || levelId.equals(AccountStatus.LEVEL_5)
        // || levelId.equals(AccountStatus.LEVEL_6))) {
        // return ResponseUtil.badArgumentValue("用户等级错误！");
        // }
        //
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("mobile", mobile);
        paramMap.put("province", province);
        paramMap.put("city", city);
        paramMap.put("county", county);
        paramMap.put("provinceId", provinceId);
        paramMap.put("cityId", cityId);
        paramMap.put("countyId", countyId);
        paramMap.put("firstLeaderId", directLeaderId);
        paramMap.put("levelId", levelId);
        paramMap.put("nickname", nickname);

        Map<String, String> rewardMap = mallSystemConfigService.syncUserInfo();
        String type = rewardMap.get("mall_sync_user_type");
        Map<String, Object> map = new HashMap<>();
        if ("1".equals(type)) {
            map = (Map) syncUserInfo1(paramMap);
        } else {
            map = (Map) syncUserInfo2(paramMap);
        }

        return ResponseUtil.ok(map);
    }



    /** 通过手机号创建代理账号账号(账号：手机号,密码：88888888) levelId：4、服务商 5、区级代理 6、市级代理(方式一 老模式) */
    @Transactional(rollbackFor = Exception.class)
    public Object syncUserInfo1(Map<String, Object> map) throws Exception {

        String mobile = (String) map.get("mobile"); // 手机号
        String province = (String) map.get("province"); // 省
        String city = (String) map.get("city"); // 市
        String county = (String) map.get("county"); // 区
        String levelId = (String) map.get("levelId"); // 用户等级
        String provinceId = (String) map.get("provinceId");// 省code
        String cityId = (String) map.get("cityId");// 市code
        String countyId = (String) map.get("countyId");// 区code
        String directLeaderId = (String) map.get("firstLeaderId"); // 直推人id(默认平台系统用户的 id主键)
        String nickname = (String) map.get("nickname"); // 用户昵称


        if (StringUtils.isNotEmpty(nickname)) {
            nickname = EmojiParser.parseToAliases(nickname);
        }

        // 校验firstLeaderId是否正确
        MallUser directLeader = null;
        if (StringUtils.isNotEmpty(directLeaderId)) {
            directLeader = userService.findById(directLeaderId);// 推荐人信息
            if (directLeader == null) {
                return ResponseUtil.badArgumentValue("上级不存在！");
            }
        } else {
            directLeaderId = "00"; // todo 暂时默认平台系统用户的 id主键
        }


        List<MallUser> mallUserList = userService.queryByMobile(mobile);
        MallUser user = new MallUser();

        if (mallUserList.size() == 0) {
            user.setId(GraphaiIdGenerator.nextId("MallUser"));
            user.setUsername(mobile);
            user.setPassword("88888888");
            user.setMobile(mobile);
            if (StringUtils.isNotEmpty(nickname)) {
                user.setNickname(nickname);
            } else {
                user.setNickname(mobile);
            }
            if (StringHelper.isNotNullAndEmpty(provinceId)) {
                user.setProvinceCode(Integer.parseInt(provinceId));
            }

            if (StringHelper.isNotNullAndEmpty(cityId)) {
                user.setCityCode(Integer.parseInt(cityId));
            }

            if (StringHelper.isNotNullAndEmpty(countyId)) {
                user.setAreaCode(Integer.parseInt(countyId));
            }


            if (StringHelper.isNotNullAndEmpty(province)) {
                user.setProvince(province);
            }

            if (StringHelper.isNotNullAndEmpty(city)) {
                user.setCity(city);
            }

            if (StringHelper.isNotNullAndEmpty(county)) {
                user.setCounty(county);
            }
            if ("8".equals(levelId)) {
                user.setLevelId(AccountStatus.LEVEL_0);
            } else {
                user.setLevelId(levelId);
            }

            // 注册渠道为商圈
            user.setChannelId(MallUserEnum.CHANNEL_ID_0.getCode());

            // 判断用户身份
            String firstLeaderId = null;
            String secondLeaderId = null;
            String thirdLeaderId = null;

            // 同步普通用户
            if ("0".equals(levelId)) {
                user.setDirectLeader(directLeaderId);
                //set绑定直推人时间
                user.setLockTime(LocalDateTime.now());
                if (!ObjectUtil.equals(null, directLeader.getFirstLeader())) {
                    user.setFirstLeader(directLeader.getFirstLeader());
                }
                if (!ObjectUtil.equals(null, directLeader.getSecondLeader())) {
                    user.setSecondLeader(directLeader.getSecondLeader());
                }
                if (!ObjectUtil.equals(null, directLeader.getThirdLeader())) {
                    user.setThirdLeader(directLeader.getThirdLeader());
                }
                userService.addUser(user);
            }
            // 同步 4联盟商家（服务商）5区代 6市代
            else if (levelId.equals(AccountStatus.LEVEL_4) || levelId.equals(AccountStatus.LEVEL_5)
                            || levelId.equals(AccountStatus.LEVEL_6)) {
                user.setIsActivate((byte) 1);
                if (levelId.equals(AccountStatus.LEVEL_4)) {
                    /** (1)用户当前身份为服务商, 上级只能为区代、市代 **/
                    firstLeaderId = user.getId();
                    // 校验用户当前身份 和 直推人身份是否符合条件
                    // if (directLeader != null) {
                    // if (directLeader.getLevelId().equals(AccountStatus.LEVEL_5)) {
                    // /** (1.1)上级为区代/市代 **/
                    // secondLeaderId = directLeader.getSecondLeader();
                    // thirdLeaderId = directLeader.getThirdLeader();
                    // } else if (directLeader.getLevelId().equals(AccountStatus.LEVEL_6)) {
                    // thirdLeaderId = directLeader.getThirdLeader();
                    // } else {
                    // return ResponseUtil.badArgumentValue("上级身份错误！");
                    // }
                    // }
                } else if (levelId.equals(AccountStatus.LEVEL_5)) {
                    /** (2)用户当前身份为区级代理, 上级只能为市代 **/
                    secondLeaderId = user.getId();
                    // if (directLeader != null) {
                    // if (directLeader.getLevelId().equals(AccountStatus.LEVEL_6)) {
                    // /** (2.1)上级为市代 **/
                    // thirdLeaderId = directLeader.getThirdLeader();
                    // } else {
                    // return ResponseUtil.badArgumentValue("上级身份错误！");
                    // }
                    // }
                } else if (levelId.equals(AccountStatus.LEVEL_6)) {
                    /** (3)用户当前身份为市级代理 **/
                    thirdLeaderId = user.getId();
                    // if(directLeader != null){
                    // return ResponseUtil.badArgumentValue("上级身份错误！");
                    // }
                }
                user.setDirectLeader(directLeaderId);
                //set绑定直推人时间
                user.setLockTime(LocalDateTime.now());
                user.setFirstLeader(firstLeaderId);
                user.setSecondLeader(secondLeaderId);
                user.setThirdLeader(thirdLeaderId);

                // 代理用户送一个月达人
                Map<String, String> stringStringMap = mallSystemConfigService.queryTalent();
                /** 是否送达人 1 送 其它不送 */
                if (stringStringMap.get("mall_talent_is_on").equals("1")) {
                    // 用户达人卡的信息
                    MallUserCard mallUserCard = new MallUserCard();
                    user.setIsTalent(AccountStatus.IS_Talent_1);
                    /** 用户达人卡的信息 */
                    Map<String, Object> dataMap = marketingSignRecordService.queryUserTalentCard(user.getId());
                    // 有卡 就续期
                    if (null != dataMap) {
                        long end = ((Timestamp) dataMap.get("endTime")).getTime();
                        LocalDateTime endTime =
                                        Instant.ofEpochMilli(end).atZone(ZoneOffset.ofHours(8)).toLocalDateTime();
                        LocalDateTime now = LocalDateTime.now();
                        String id = (String) dataMap.get("id");
                        mallUserCard.setId(id);
                        // 月卡
                        /** 已过期 **/
                        if (endTime.isBefore(now)) {
                            // 月卡
                            mallUserCard.setEndTime(now.plusMonths(1L));
                        } else {
                            // 月卡
                            mallUserCard.setEndTime(endTime.plusMonths(1L));
                        }
                        // 更新卡的信息
                        mallUserCardService.updateMallUserCard(mallUserCard);
                    }
                    // 没有卡
                    else {
                        // 月卡时间 开始时间 mstart - 结束时间 mend
                        LocalDateTime mstart = LocalDateTime.now();
                        LocalDateTime mend = LocalDateTime.now().plus(1, ChronoUnit.MONTHS);
                        mallUserCard.setId(GraphaiIdGenerator.nextId("mallCard"));
                        mallUserCard.setIsActivate((byte) 1);
                        mallUserCard.setPhone(user.getMobile());
                        mallUserCard.setUserId(user.getId());
                        mallUserCard.setCardType((byte) 2);
                        // 月卡
                        mallUserCard.setStartTime(mstart);
                        mallUserCard.setEndTime(mend);

                        // 插入卡记录
                        mallUserCardService.insertMallUserCard(mallUserCard);
                    }

                }
                userService.addSelective(user);

            }
            /** 同步红包用户 送一年达人 */
            else if ("8".equals(levelId)) {

                user.setDirectLeader(directLeaderId);
                //set绑定直推人时间
                user.setLockTime(LocalDateTime.now());
                if (!ObjectUtil.equals(null, directLeader.getFirstLeader())) {
                    user.setFirstLeader(directLeader.getFirstLeader());
                }
                if (!ObjectUtil.equals(null, directLeader.getSecondLeader())) {
                    user.setSecondLeader(directLeader.getSecondLeader());
                }
                if (!ObjectUtil.equals(null, directLeader.getThirdLeader())) {
                    user.setThirdLeader(directLeader.getThirdLeader());
                }
                /** 商圈红包码用户 */
                user.setSource("商圈红包码用户");
                user.setIsTalent(AccountStatus.IS_Talent_2);
                userService.addUser(user);

                List<MallUser> mallUsers = userService.queryByMobile(mobile);
                if (null != mallUsers && mallUsers.size() > 0) {
                    // 年卡时间 开始时间 ystart - 结束时间 yend
                    LocalDateTime ystart = LocalDateTime.now();
                    LocalDateTime yend = LocalDateTime.now().plus(1, ChronoUnit.YEARS);
                    MallUserCard mallUserCard = new MallUserCard();
                    mallUserCard.setId(GraphaiIdGenerator.nextId("mallCard"));
                    mallUserCard.setIsActivate((byte) 1);
                    mallUserCard.setPhone(mobile);
                    mallUserCard.setUserId(mallUsers.get(0).getId());
                    // 年卡
                    mallUserCard.setCardType((byte) 4);
                    mallUserCard.setStartTime(ystart);
                    mallUserCard.setEndTime(yend);
                    mallUserCard.setActivationCode("商圈红包码用户送年卡");
                    // 插入卡记录
                    mallUserCardService.insertMallUserCard(mallUserCard);
                }
            }


        } else if (mallUserList.size() == 1) {
            // 若用户存在,则需要修改用户信息
            user = mallUserList.get(0);
            // 用户上三级修改记录-----------------------------------
            // 旧用户上级信息
            MallUserLevelChange mallUserLevelChange = new MallUserLevelChange();
            mallUserLevelChange.setUserId(user.getId());
            mallUserLevelChange.setNickname(user.getNickname());
            mallUserLevelChange.setMobile(user.getMobile());
            mallUserLevelChange.setOldLevelId(user.getLevelId());
            mallUserLevelChange.setOldDirectLeader(user.getDirectLeader());
            mallUserLevelChange.setOldFirstLeader(user.getFirstLeader());
            mallUserLevelChange.setOldSecondLeader(user.getSecondLeader());
            mallUserLevelChange.setOldThirdLeader(user.getThirdLeader());
            // 用户上三级修改记录-----------------------------------


            Boolean isUpdate = false; // 用来判断是否需要修改用户信息
            Boolean levelChange = false; // 用来判断用户等级是否改变
            /** 判断用户是否存在username、password **/
            if (StringUtils.isEmpty(user.getUsername()) || StringUtils.isEmpty(user.getPassword())) {
                isUpdate = true;
                user.setUsername(mobile);
                user.setPassword(new BCryptPasswordEncoder().encode("88888888")); // 默认密码
            }

            // ‘用户等级’改变了
            if (!user.getLevelId().equals(levelId) && !"8".equals(levelId)) {
                // 判断用户等级是否符合条件
                if (Integer.valueOf(user.getLevelId()) > Integer.valueOf(levelId)) {
                    return ResponseUtil.badArgumentValue("用户的新等级不符合条件！");
                }
                logger.info("---------用户等级发生了改变---------");
                user.setLevelId(levelId);
                isUpdate = true;
                levelChange = true;
                // 用户升级送一个月达人
                Map<String, String> stringStringMap = mallSystemConfigService.queryTalent();
                /** 是否送达人 1 送 其它不送 */
                if (stringStringMap.get("mall_talent_is_on").equals("1")) {
                    // 用户达人卡的信息
                    MallUserCard mallUserCard = new MallUserCard();
                    user.setIsTalent(AccountStatus.IS_Talent_1);
                    /** 用户达人卡的信息 */
                    Map<String, Object> dataMap = marketingSignRecordService.queryUserTalentCard(user.getId());
                    // 有卡 就续期
                    if (null != dataMap) {
                        long end = ((Timestamp) dataMap.get("endTime")).getTime();
                        LocalDateTime endTime =
                                        Instant.ofEpochMilli(end).atZone(ZoneOffset.ofHours(8)).toLocalDateTime();
                        LocalDateTime now = LocalDateTime.now();
                        String id = (String) dataMap.get("id");
                        mallUserCard.setId(id);
                        // 月卡
                        /** 已过期 **/
                        if (endTime.isBefore(now)) {
                            // 月卡
                            mallUserCard.setEndTime(now.plusMonths(1L));
                        } else {
                            // 月卡
                            mallUserCard.setEndTime(endTime.plusMonths(1L));
                        }
                        // 更新卡的信息
                        mallUserCardService.updateMallUserCard(mallUserCard);
                    }
                    // 没有卡
                    else {
                        // 月卡时间 开始时间 mstart - 结束时间 mend
                        LocalDateTime mstart = LocalDateTime.now();
                        LocalDateTime mend = LocalDateTime.now().plus(1, ChronoUnit.MONTHS);
                        mallUserCard.setId(GraphaiIdGenerator.nextId("mallCard"));
                        mallUserCard.setIsActivate((byte) 1);
                        mallUserCard.setPhone(user.getMobile());
                        mallUserCard.setUserId(user.getId());
                        mallUserCard.setCardType((byte) 2);
                        // 月卡
                        mallUserCard.setStartTime(mstart);
                        mallUserCard.setEndTime(mend);

                        // 插入卡记录
                        mallUserCardService.insertMallUserCard(mallUserCard);
                    }

                }

            }
            /** 用户等级不变，只是给商圈红包码用户送一年达人 */
            else if ("8".equals(levelId)) {

                MallUserCard mallUserCard = new MallUserCard();
                /** 商圈红包码用户 */
                user.setSource("商圈红包码用户");
                user.setIsTalent(AccountStatus.IS_Talent_2);
                /** 用户达人卡的信息 */
                Map<String, Object> dataMap = marketingSignRecordService.queryUserTalentCard(user.getId());
                // 有卡 就续期
                if (null != dataMap) {
                    long end = ((Timestamp) dataMap.get("endTime")).getTime();
                    LocalDateTime endTime = Instant.ofEpochMilli(end).atZone(ZoneOffset.ofHours(8)).toLocalDateTime();
                    LocalDateTime now = LocalDateTime.now();
                    /** 已过期 **/
                    if (endTime.isBefore(now)) {
                        mallUserCard.setEndTime(now.plusYears(1L));
                    } else {
                        // 年卡
                        mallUserCard.setEndTime(endTime.plusYears(1L));
                    }
                    String id = (String) dataMap.get("id");
                    mallUserCard.setId(id);
                    mallUserCard.setActivationCode("商圈红包码用户送年卡");
                    // 更新卡的信息
                    mallUserCardService.updateMallUserCard(mallUserCard);
                }
                // 没有卡
                else {
                    // 年卡时间 开始时间 ystart - 结束时间 yend
                    LocalDateTime ystart = LocalDateTime.now();
                    LocalDateTime yend = LocalDateTime.now().plus(1, ChronoUnit.YEARS);

                    mallUserCard.setId(GraphaiIdGenerator.nextId("mallCard"));
                    mallUserCard.setIsActivate((byte) 1);
                    mallUserCard.setPhone(user.getMobile());
                    mallUserCard.setUserId(user.getId());
                    mallUserCard.setCardType((byte) 4);
                    mallUserCard.setStartTime(ystart);
                    mallUserCard.setEndTime(yend);
                    mallUserCard.setActivationCode("商圈红包码用户送年卡");

                    // 插入卡记录
                    mallUserCardService.insertMallUserCard(mallUserCard);

                }


            }

            // 若用户的直推人改变并且满足变更条件,或者用户等级改变, 则用户上三级关系也改变
            if ((StringUtils.isNotEmpty(directLeaderId) && !directLeaderId.equals(user.getDirectLeader()))
                            || levelChange) {
                // 判断用户上级身份(若当前用户的上三级都是空的,则进行修改逻辑)
                // 判断用户身份
                String firstLeaderId = "";
                String secondLeaderId = "";
                String thirdLeaderId = "";
                if (levelId.equals(AccountStatus.LEVEL_4)) {
                    /** (1)用户当前身份为服务商, 上级只能为区代、市代 作废**/
                    firstLeaderId = user.getId();
                    // (校验用户当前身份 和 直推人身份是否符合条件 作废) 新的规则是 所有等级用户都可以推，商家，区代，市代
//                    if (directLeader != null) {
//                        if (directLeader.getLevelId().equals(AccountStatus.LEVEL_4)
//                                        || directLeader.getLevelId().equals(AccountStatus.LEVEL_5)
//                                        || directLeader.getLevelId().equals(AccountStatus.LEVEL_6)) {
//                            /** (1.1)上级为区代/市代 **/
//                            if (StringUtils.isNotEmpty(directLeader.getSecondLeader())) {
//                                secondLeaderId = directLeader.getSecondLeader();
//                            }
//                            if (StringUtils.isNotEmpty(directLeader.getThirdLeader())) {
//                                thirdLeaderId = directLeader.getThirdLeader();
//                            }
//                        } else {
//                            return ResponseUtil.badArgumentValue("上级身份错误！");
//                        }
//                    }
                } else if (levelId.equals(AccountStatus.LEVEL_5)) {
                    /** (2)用户当前身份为区级代理, 上级只能为市代 **/
                    secondLeaderId = user.getId();
//                    if (directLeader != null) {
//                        if (directLeader.getLevelId().equals(AccountStatus.LEVEL_6)) {
//                            /** (2.1)上级为市代 **/
//                            if (StringUtils.isNotEmpty(directLeader.getThirdLeader())) {
//                                thirdLeaderId = directLeader.getThirdLeader();
//                            }
//                        } else {
//                            return ResponseUtil.badArgumentValue("上级身份错误！");
//                        }
//                    }
                } else if (levelId.equals(AccountStatus.LEVEL_6)) {
                    /** (3)用户当前身份为市级代理 **/
                    thirdLeaderId = user.getId();
                    // if(directLeader != null){
                    // return ResponseUtil.badArgumentValue("上级身份错误！");
                    // }
                }
                user.setFirstLeader(firstLeaderId);
                user.setSecondLeader(secondLeaderId);
                user.setThirdLeader(thirdLeaderId);
                isUpdate = true;


                // 用户上三级修改记录-----------------------------------
                // 新用户上级信息
                mallUserLevelChange.setLevelId(levelId);
                mallUserLevelChange.setDirectLeader(directLeaderId);
                mallUserLevelChange.setFirstLeader(firstLeaderId);
                mallUserLevelChange.setSecondLeader(secondLeaderId);
                mallUserLevelChange.setThirdLeader(thirdLeaderId);
                mallUserLevelChange.setChangeApi("/serviceAccount");
                // todo 修改之前-----------
                MallUserLevelChange updateUserLevel = new MallUserLevelChange();
                updateUserLevel.setUserId(user.getId());
                updateUserLevel.setDeleted(MallUserLevelChange.IS_DELETED);
                mallUserLevelChangeService.updateMallUserLevelChangeByOther(updateUserLevel);
                mallUserLevelChangeService.insertMallUserLevelChange(mallUserLevelChange);
                // 用户上三级修改记录-----------------------------------
            }

            // 若用户直推人不存在,则添加直推人
            if (StringUtils.isEmpty(user.getDirectLeader())) {
                isUpdate = true;
                user.setDirectLeader(directLeaderId);
                //set绑定直推人时间
                user.setLockTime(LocalDateTime.now());
            }

            if (StringUtils.isNotEmpty(nickname)) {
                isUpdate = true;
                user.setNickname(nickname);
            }
            // 新模式 （有省市区就不修改）
            // if (!province.equals(user.getProvince()) || !city.equals(user.getCity())
            // || !county.equals(user.getCounty())) {
            // isUpdate = true;
            // user.setProvince(province);
            // user.setCity(city);
            // user.setCounty(county);
            // }

            if (null == user.getProvinceCode() && StringHelper.isNotNullAndEmpty(provinceId)) {
                user.setProvinceCode(Integer.parseInt(provinceId));
            }

            if (null == user.getCityCode() && StringHelper.isNotNullAndEmpty(cityId)) {
                user.setCityCode(Integer.parseInt(cityId));
            }

            if (null == user.getAreaCode() && StringHelper.isNotNullAndEmpty(countyId)) {
                user.setAreaCode(Integer.parseInt(countyId));
            }


            if (StringUtils.isEmpty(user.getProvince()) && StringHelper.isNotNullAndEmpty(province)) {
                user.setProvince(province);
            }

            if (StringUtils.isEmpty(user.getCity()) && StringHelper.isNotNullAndEmpty(city)) {
                user.setCity(city);
            }

            if (StringUtils.isEmpty(user.getCounty()) && StringHelper.isNotNullAndEmpty(county)) {
                user.setCounty(county);
            }


            /** 如果用户没有激活，那就帮他激活（防止C端用户已经存在） **/
            if (user.getIsActivate() == 0) {
                isUpdate = true;
                user.setIsActivate((byte) 1);
                // 卡信息
                MallUserCard cardInfo = mallUserCardService.selectMallUserCardByUserId(user.getId());
                if (cardInfo != null) {
                    MallUserCard updateCard = new MallUserCard();
                    updateCard.setId(cardInfo.getId());
                    updateCard.setActivationCode("商圈代理用户自动激活");
                    updateCard.setActivateTime(LocalDateTime.now());
                    mallUserCardService.updateMallUserCard(updateCard);
                }
            }

            if (isUpdate) {
                // 代理身份送创客
                String codeUrl = (String) userService.createMaker(user);
                if (!ObjectUtil.equals(null, codeUrl)) {
                    user.setSessionKey(codeUrl);
                    user.setIsMaker("1");
                }


                userService.updateById(user);
            }

        } else {
            return ResponseUtil.badArgumentValue("查询到多条数据！");
        }

        Map<String, Object> mapUser = new HashMap<>();
        if (StringUtils.isNotEmpty(user.getNickname())) {
            user.setNickname(EmojiParser.parseToUnicode(user.getNickname()));
        }
        mapUser.put("userInfo", user); // 用户信息
        return mapUser;
    }

    /** 通过手机号创建代理账号账号(账号：手机号,密码：88888888) levelId：4、服务商 5、区级代理 6、市级代理 或者同步支付系统用户信息(方式二 新模式) */
    public Object syncUserInfo2(Map<String, Object> map) throws Exception {
        String mobile = (String) map.get("mobile"); // 手机号
        String province = (String) map.get("province"); // 省
        String city = (String) map.get("city"); // 市
        String county = (String) map.get("county"); // 区
        String levelId = (String) map.get("levelId"); // 用户等级
        String provinceId = (String) map.get("provinceId");// 省code
        String cityId = (String) map.get("cityId");// 市code
        String countyId = (String) map.get("countyId");// 区code
        String directLeaderId = (String) map.get("firstLeaderId"); // 直推人id(默认平台系统用户的 id主键)
        String nickname = (String) map.get("nickname"); // 用户昵称

        if (StringUtils.isNotEmpty(nickname)) {
            nickname = EmojiParser.parseToAliases(nickname);
        }

        // 校验firstLeaderId是否正确
        MallUser directLeader = null;
        if (StringUtils.isNotEmpty(directLeaderId)) {
            directLeader = userService.findById(directLeaderId);// 推荐人信息
            if (directLeader == null) {
                return ResponseUtil.badArgumentValue("上级不存在！");
            }
        }

        List<MallUser> mallUserList = userService.queryByMobile(mobile);
        MallUser user = new MallUser();

        if (mallUserList.size() == 0) {
            user.setId(GraphaiIdGenerator.nextId("MallUser"));
            user.setUsername(mobile);
            user.setPassword("88888888");
            user.setMobile(mobile);
            if (StringUtils.isNotEmpty(nickname)) {
                user.setNickname(nickname);
            } else {
                user.setNickname(mobile);
            }
            if (StringHelper.isNotNullAndEmpty(provinceId)) {
                user.setProvinceCode(Integer.parseInt(provinceId));
            }

            if (StringHelper.isNotNullAndEmpty(cityId)) {
                user.setCityCode(Integer.parseInt(cityId));
            }

            if (StringHelper.isNotNullAndEmpty(countyId)) {
                user.setAreaCode(Integer.parseInt(countyId));
            }
            if (StringHelper.isNotNullAndEmpty(province)) {
                user.setProvince(province);
            }

            if (StringHelper.isNotNullAndEmpty(city)) {
                user.setCity(city);
            }

            if (StringHelper.isNotNullAndEmpty(county)) {
                user.setCounty(county);
            }
            user.setLevelId(levelId);
            // 注册渠道为商圈
            user.setChannelId(MallUserEnum.CHANNEL_ID_0.getCode());

            // 判断用户身份
            String firstLeaderId = null;
            String secondLeaderId = null;
            String thirdLeaderId = null;
            // 同步普通用户
            if ("0".equals(levelId)) {
                user.setDirectLeader(directLeaderId);
                //set绑定直推人时间
                user.setLockTime(LocalDateTime.now());
                if (!ObjectUtil.equals(null, directLeader.getFirstLeader())) {
                    user.setFirstLeader(directLeader.getFirstLeader());
                }
                if (!ObjectUtil.equals(null, directLeader.getSecondLeader())) {
                    user.setSecondLeader(directLeader.getSecondLeader());
                }
                if (!ObjectUtil.equals(null, directLeader.getThirdLeader())) {
                    user.setThirdLeader(directLeader.getThirdLeader());
                }
                userService.addUser(user);
            }
            // 同步 4联盟商家（服务商）5区代 6市代
            else {
                user.setIsActivate((byte) 1);
                if (levelId.equals(AccountStatus.LEVEL_4)) {
                    /** (1)用户当前身份为服务商(联盟商家), 上级只能为区代、市代 **/
                    firstLeaderId = user.getId();
                    // 直推人身份是否符合条件
                    if (directLeader != null) {
                        if (!ObjectUtil.equals(null, directLeader.getSecondLeader())
                                        || !ObjectUtil.equals(null, directLeader.getThirdLeader())) {
                            secondLeaderId = directLeader.getSecondLeader();
                            thirdLeaderId = directLeader.getThirdLeader();
                        }
                    }
                } else if (levelId.equals(AccountStatus.LEVEL_5)) {
                    /** (2)用户当前身份为区级代理, 上级只能为市代 **/
                    secondLeaderId = user.getId();
                    if (directLeader != null) {
                        if (!ObjectUtil.equals(null, directLeader.getThirdLeader())) {
                            /** (2.1)上级为市代 **/
                            thirdLeaderId = directLeader.getThirdLeader();
                        }
                    }
                } else if (levelId.equals(AccountStatus.LEVEL_6)) {
                    /** (3)用户当前身份为市级代理 **/
                    thirdLeaderId = user.getId();
                }
                user.setDirectLeader(directLeaderId);
                //set绑定直推人时间
                user.setLockTime(LocalDateTime.now());
                user.setFirstLeader(firstLeaderId);
                user.setSecondLeader(secondLeaderId);
                user.setThirdLeader(thirdLeaderId);
                userService.addSelective(user);
            }
        } else if (mallUserList.size() == 1) {
            // 若用户存在,则需要修改用户信息
            user = mallUserList.get(0);
            // 用户上三级修改记录-----------------------------------
            // 旧用户上级信息
            MallUserLevelChange mallUserLevelChange = new MallUserLevelChange();
            mallUserLevelChange.setUserId(user.getId());
            mallUserLevelChange.setNickname(user.getNickname());
            mallUserLevelChange.setMobile(user.getMobile());
            mallUserLevelChange.setOldLevelId(user.getLevelId());
            mallUserLevelChange.setOldDirectLeader(user.getDirectLeader());
            mallUserLevelChange.setOldFirstLeader(user.getFirstLeader());
            mallUserLevelChange.setOldSecondLeader(user.getSecondLeader());
            mallUserLevelChange.setOldThirdLeader(user.getThirdLeader());
            // 用户上三级修改记录-----------------------------------


            Boolean isUpdate = false; // 用来判断是否需要修改用户信息
            Boolean levelChange = false; // 用来判断用户等级是否改变
            /** 判断用户是否存在username、password **/
            if (StringUtils.isEmpty(user.getUsername()) || StringUtils.isEmpty(user.getPassword())) {
                isUpdate = true;
                user.setUsername(mobile);
                user.setPassword(new BCryptPasswordEncoder().encode("88888888")); // 默认密码
            }
            /** 如果用户没有激活，那就帮他激活（防止C端用户已经存在） **/
            // if (user.getIsActivate() == 0) {
            // isUpdate = true;
            // user.setIsActivate((byte) 1);
            // }

            // ‘用户等级’改变了
            if (!user.getLevelId().equals(levelId)) {
                // 判断用户等级是否符合条件
                if (Integer.valueOf(user.getLevelId()) > Integer.valueOf(levelId)) {
                    return ResponseUtil.badArgumentValue("用户的新等级不符合条件！");
                }
                logger.info("---------用户等级发生了改变---------");
                user.setLevelId(levelId);
                isUpdate = true;
                levelChange = true;
            }

            // 若用户的直推人改变并且满足变更条件,或者用户等级改变, 则用户上三级关系也改变
            if ((StringUtils.isNotEmpty(directLeaderId) && !directLeaderId.equals(user.getDirectLeader()))
                            || levelChange) {
                // 判断用户上级身份(若当前用户的上三级都是空的,则进行修改逻辑)
                // 判断用户身份
                String firstLeaderId = "";
                String secondLeaderId = "";
                String thirdLeaderId = "";
                if (levelId.equals(AccountStatus.LEVEL_4)) {
                    /** (1)用户当前身份为服务商, 上级只能为区代、市代 **/
                    firstLeaderId = user.getId();
                    // 校验用户当前身份 和 直推人身份是否符合条件
                    if (directLeader != null) {
                        if (directLeader.getLevelId().equals(AccountStatus.LEVEL_5)
                                        || directLeader.getLevelId().equals(AccountStatus.LEVEL_6)) {
                            /** (1.1)上级为区代/市代 **/
                            if (StringUtils.isNotEmpty(directLeader.getSecondLeader())) {
                                secondLeaderId = directLeader.getSecondLeader();
                            }
                            if (StringUtils.isNotEmpty(directLeader.getThirdLeader())) {
                                thirdLeaderId = directLeader.getThirdLeader();
                            }
                        } else {
                            return ResponseUtil.badArgumentValue("上级身份错误！");
                        }
                    }
                } else if (levelId.equals(AccountStatus.LEVEL_5)) {
                    /** (2)用户当前身份为区级代理, 上级只能为市代 **/
                    secondLeaderId = user.getId();
                    if (directLeader != null) {
                        if (directLeader.getLevelId().equals(AccountStatus.LEVEL_6)) {
                            /** (2.1)上级为市代 **/
                            if (StringUtils.isNotEmpty(directLeader.getThirdLeader())) {
                                thirdLeaderId = directLeader.getThirdLeader();
                            }
                        } else {
                            return ResponseUtil.badArgumentValue("上级身份错误！");
                        }
                    }
                } else if (levelId.equals(AccountStatus.LEVEL_6)) {
                    /** (3)用户当前身份为市级代理 **/
                    thirdLeaderId = user.getId();

                }
                user.setFirstLeader(firstLeaderId);
                user.setSecondLeader(secondLeaderId);
                user.setThirdLeader(thirdLeaderId);
                isUpdate = true;


                // 用户上三级修改记录-----------------------------------
                // 新用户上级信息
                mallUserLevelChange.setLevelId(levelId);
                mallUserLevelChange.setDirectLeader(directLeaderId);
                mallUserLevelChange.setFirstLeader(firstLeaderId);
                mallUserLevelChange.setSecondLeader(secondLeaderId);
                mallUserLevelChange.setThirdLeader(thirdLeaderId);
                mallUserLevelChange.setChangeApi("/serviceAccount");
                // todo 修改之前-----------
                MallUserLevelChange updateUserLevel = new MallUserLevelChange();
                updateUserLevel.setUserId(user.getId());
                updateUserLevel.setDeleted(MallUserLevelChange.IS_DELETED);
                mallUserLevelChangeService.updateMallUserLevelChangeByOther(updateUserLevel);
                mallUserLevelChangeService.insertMallUserLevelChange(mallUserLevelChange);
                // 用户上三级修改记录-----------------------------------
            }

            // 若用户直推人不存在,则添加直推人
            if (StringUtils.isEmpty(user.getDirectLeader())) {
                isUpdate = true;
                user.setDirectLeader(directLeaderId);
                //set绑定直推人时间
                user.setLockTime(LocalDateTime.now());
            }

            if (StringUtils.isNotEmpty(nickname)) {
                isUpdate = true;
                user.setNickname(nickname);
            }
            // if (!province.equals(user.getProvince()) || !city.equals(user.getCity())
            // || !county.equals(user.getCounty())) {
            // isUpdate = true;
            // user.setProvince(province);
            // user.setCity(city);
            // user.setCounty(county);
            // }
            if (null != user.getProvinceCode() && StringHelper.isNotNullAndEmpty(provinceId)) {
                user.setProvinceCode(Integer.parseInt(provinceId));
            }

            if (null != user.getCityCode() && StringHelper.isNotNullAndEmpty(cityId)) {
                user.setCityCode(Integer.parseInt(cityId));
            }

            if (null != user.getAreaCode() && StringHelper.isNotNullAndEmpty(countyId)) {
                user.setAreaCode(Integer.parseInt(countyId));
            }


            if (StringUtils.isEmpty(user.getProvince()) && StringHelper.isNotNullAndEmpty(province)) {
                user.setProvince(province);
            }

            if (StringUtils.isEmpty(user.getCity()) && StringHelper.isNotNullAndEmpty(city)) {
                user.setCity(city);
            }

            if (StringUtils.isEmpty(user.getCounty()) && StringHelper.isNotNullAndEmpty(county)) {
                user.setCounty(county);
            }

            /** 如果用户没有激活，那就帮他激活（防止C端用户已经存在） **/
            if (user.getIsActivate() == 0) {
                isUpdate = true;
                user.setIsActivate((byte) 1);
                // 卡信息
                MallUserCard cardInfo = mallUserCardService.selectMallUserCardByUserId(user.getId());
                if (cardInfo != null) {
                    MallUserCard updateCard = new MallUserCard();
                    updateCard.setId(cardInfo.getId());
                    updateCard.setActivationCode("商圈代理用户自动激活");
                    updateCard.setActivateTime(LocalDateTime.now());
                    mallUserCardService.updateMallUserCard(updateCard);
                }
            }

            if (isUpdate) {
                userService.updateById(user);
            }

        } else {
            return ResponseUtil.badArgumentValue("查询到多条数据！");
        }

        Map<String, Object> mapUser = new HashMap<>();
        if (StringUtils.isNotEmpty(user.getNickname())) {
            user.setNickname(EmojiParser.parseToUnicode(user.getNickname()));
        }
        mapUser.put("userInfo", user); // 用户信息
        return mapUser;
    }



    /**
     * 未莱合伙人——通过手机号查询代理token
     */
    @PostMapping("/getUserToken")
    public Object getUserToken(@RequestBody String body) throws IOException {
        logger.info("未莱商圈——body参数： " + JacksonUtils.bean2Jsn(body));
//        String sign = JacksonUtil.parseString(body, "sign");
        String mobile = JacksonUtil.parseString(body, "mobile"); // 手机号
//        if (StringUtils.isEmpty(sign)) {
//            return ResponseUtil.badArgumentValue("未获取到签名信息！");
//        }
        if (StringUtils.isEmpty(mobile)) {
            return ResponseUtil.badArgumentValue("未获取到手机号！");
        }
        // 参数验签
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("mobile", mobile);
        String string = JSONObject.toJSONString(paramMap);// 请求参数转换成字符串进行加密处理
//        boolean b = verifySignature(string, sign, privateKey);
//        if (!b) {
//            return ResponseUtil.fail(403, "验签失败");
//        }
        List<MallUser> mallUserList = userService.queryByMobile(mobile);
        MallUser mallUser = null;
        if (mallUserList.size() == 1) {
            mallUser = mallUserList.get(0);
        }
        if (mallUser == null) {
            return ResponseUtil.badArgumentValue("用户不存在！");
        }
        // if (!(mallUser.getLevelId().equals(AccountStatus.LEVEL_4) ||
        // mallUser.getLevelId().equals(AccountStatus.LEVEL_5)
        // || mallUser.getLevelId().equals(AccountStatus.LEVEL_6) || "1".equals(mallUser.getIsMaker()))) {
        // return ResponseUtil.ok();
        // }
        String token = UserTokenManager.generateToken(mallUser.getId(), null, null);
        Map<String, Object> result = new HashMap<>();
        result.put("token", token);
        return ResponseUtil.ok(result);
    }

    /**
     * 未莱合伙人——查询当前代理下级代理信息(todo：暂时无用------------)
     */
    @GetMapping("/getSubAgentList")
    public Object getSubAgentList(@LoginUser String userId, @RequestParam(defaultValue = "1") Integer page,
                    @RequestParam(defaultValue = "10") Integer limit) {

        List<MallUserAgentVo> agentList = new ArrayList<>();
        long total = 0;
        if (userId != null) {
            MallUser mallUser = userService.findById(userId);
            if (mallUser != null) {
                List<String> levelIdList = new ArrayList<>();
                MallUserAgentVo mallUserAgentVo = new MallUserAgentVo();
                if (mallUser.getLevelId().equals(AccountStatus.LEVEL_5)) {
                    // 区代
                    mallUserAgentVo.setSecondLeader(mallUser.getId());
                    levelIdList.add(AccountStatus.LEVEL_4);
                } else if (mallUser.getLevelId().equals(AccountStatus.LEVEL_6)) {
                    // 市代
                    mallUserAgentVo.setThirdLeader(mallUser.getId());
                    levelIdList.add(AccountStatus.LEVEL_4);
                    levelIdList.add(AccountStatus.LEVEL_5);
                }
                if (levelIdList.size() > 0) {
                    mallUserAgentVo.setLevelIdList(levelIdList);
                    agentList = userService.getSubAgentList(mallUserAgentVo, page, limit);
                    total = PageInfo.of(agentList).getTotal();
                }
            }
        }

        Map<String, Object> map = new HashMap<>();
        map.put("agentList", agentList); // 代理信息
        map.put("total", total);
        return ResponseUtil.ok(map);
    }


    /**
     * 未莱合伙人——查询商户直推会员信息 type:1、全部 2、达标 3、未达标 4、已结算 5、未结算 levelType:1、会员 2、非会员(代理)
     */
    @GetMapping("/getUserListByAgent")
    public Object getUserListByAgent(@LoginUser String userId, @RequestParam(defaultValue = "1") String type,
                    @RequestParam(required = false) String param, @RequestParam(required = false) String beginDate,
                    @RequestParam(required = false) String endDate, @RequestParam(required = false) String leaderId,
                    @RequestParam(required = false) String leaderLevel,
                    @RequestParam(required = false) String levelType, @RequestParam(defaultValue = "1") Integer page,
                    @RequestParam(defaultValue = "10") Integer limit) {
        Map<String, String> rewardMap = mallSystemConfigService.listReward();
        String distance = null; // 支付距离
        String amount = null; // 订单金额
        int number = 0; // 订单数量
        if (ObjectUtil.isNotNull(rewardMap)) {
            distance = rewardMap.get("Mall_reward_condition_distance");
            amount = rewardMap.get("Mall_reward_condition_amount");
            number = Integer.parseInt(rewardMap.get("Mall_reward_condition_number"));
        }

        List<MallUserAgentVo> userList = new ArrayList<>();
        long total = 0;
        Integer totalUser = 0;
        Integer totalEligible = 0;
        Integer totalNoEligible = 0;
        Integer totalSettlement = 0;
        if (userId != null) {
            MallUser mallUser = userService.findById(userId);
            if (mallUser != null) {
                if (!(mallUser.getLevelId().equals(AccountStatus.LEVEL_4)
                                || mallUser.getLevelId().equals(AccountStatus.LEVEL_5)
                                || mallUser.getLevelId().equals(AccountStatus.LEVEL_6)
                                || "1".equals(mallUser.getIsMaker()))) {
                    return ResponseUtil.ok();
                }
                String isEligible = null; // 是否达标
                String isSettlement = null; // 是否结算
                if (type.equals("2")) {
                    // 已达标
                    isEligible = "1";
                } else if (type.equals("3")) {
                    // 未达标
                    isEligible = "0";
                } else if (type.equals("4")) {
                    // 已结算
                    isSettlement = "1";
                } else if (type.equals("5")) {
                    isEligible = "1";
                    isSettlement = "0";
                }

                List<String> levelIdList = new ArrayList<>();
                MallUserAgentVo mallUser1 = new MallUserAgentVo();
                MallUserAgentVo mallUser2 = new MallUserAgentVo();
                // (1)列表数据
                String choose = null;
                String firstLeader = null;
                String secondLeader = null;
                // 上级id和上级等级都不为空
                if (StringUtils.isNotEmpty(leaderId) && StringUtils.isNotEmpty(leaderLevel)) {
                    if (leaderLevel.equals(AccountStatus.LEVEL_4)) {
                        firstLeader = leaderId;
                    } else if (leaderLevel.equals(AccountStatus.LEVEL_5)) {
                        secondLeader = leaderId;
                    }
                }
                // 上级信息
                mallUser1.setFirstLeader(firstLeader);
                mallUser2.setFirstLeader(firstLeader);
                mallUser1.setSecondLeader(secondLeader);
                mallUser2.setSecondLeader(secondLeader);

                if (mallUser.getLevelId().equals(AccountStatus.LEVEL_4)) {
                    /** 服务商 **/
                    choose = "1";
                    mallUser1.setFirstLeader(userId);
                    mallUser2.setFirstLeader(userId);
                    if (StringUtils.isEmpty(levelType)) {
                        levelIdList.add("1");
                        levelIdList.add("4");
                    } else if (levelType.equals("1")) {
                        levelIdList.add("1"); // 会员
                    } else if (levelType.equals("2")) {
                        levelIdList.add("4"); // 服务商
                    }
                } else if (mallUser.getLevelId().equals(AccountStatus.LEVEL_5)) {
                    /** 区代 **/
                    choose = "2";
                    mallUser1.setSecondLeader(userId);
                    mallUser2.setSecondLeader(userId);
                    if (StringUtils.isEmpty(levelType)) {
                        levelIdList.add("1");
                        levelIdList.add("4");
                        levelIdList.add("5");
                    } else if (levelType.equals("1")) {
                        levelIdList.add("1"); // 会员
                    } else if (levelType.equals("2")) {
                        levelIdList.add("4"); // 服务商
                        levelIdList.add("5"); // 区代
                    }
                } else {
                    /** 市代 **/
                    choose = "3";
                    mallUser1.setThirdLeader(userId);
                    mallUser2.setThirdLeader(userId);
                    if (StringUtils.isEmpty(levelType)) {
                        levelIdList.add("1");
                        levelIdList.add("4");
                        levelIdList.add("5");
                    } else if (levelType.equals("1")) {
                        // 会员
                        levelIdList.add("1"); // 会员
                    } else if (levelType.equals("2")) {
                        // 非会员
                        levelIdList.add("4"); // 服务商
                        levelIdList.add("5"); // 区代
                    }
                }
                userList = userService.getUserListByAgent(choose, userId, param, isEligible, isSettlement, distance,
                                amount, beginDate, endDate, firstLeader, secondLeader, levelIdList, page, limit);
                total = PageInfo.of(userList).getTotal();


                // (2)统计数据
                mallUser1.setId(userId);
                mallUser1.setNickname(param); // 商户名称或会员手机号
                mallUser1.setLevelIdList(levelIdList);
                // mallUser1.setLevelId(AccountStatus.LEVEL_1); //会员
                mallUser1.setChannelId(MallUserEnum.CHANNEL_ID_0.getCode()); // 商圈渠道
                mallUser1.setBeginDate(beginDate);
                mallUser1.setEndDate(endDate);

                mallUser2.setId(userId);
                mallUser2.setNickname(param);
                mallUser2.setLevelIdList(levelIdList);
                // mallUser2.setLevelId(AccountStatus.LEVEL_1);
                mallUser2.setChannelId(MallUserEnum.CHANNEL_ID_0.getCode());
                mallUser2.setBeginDate(beginDate);
                mallUser2.setEndDate(endDate);

                // 全部
                totalUser = userService.countByCondition(mallUser1);
                // 达标数量
                mallUser1.setIsEligible((byte) 1);
                totalEligible = userService.countByCondition(mallUser1);
                // 未达标数量
                mallUser1.setIsEligible((byte) 0);
                totalNoEligible = userService.countByCondition(mallUser1);
                // 已结算数量
                mallUser2.setIsSettlement((byte) 1);
                totalSettlement = userService.countByCondition(mallUser2);

            }
        }
        if (userList.size() > 0) {
            userList = SensitiveUtil.desCopyCollection(userList);
        }

        Map<String, Object> map = new HashMap<>();
        map.put("userList", userList);
        map.put("total", total);
        map.put("date", LocalDate.now());

        map.put("totalUser", totalUser); // 全部
        map.put("totalEligible", totalEligible); // 达标数量
        map.put("totalNoEligible", totalNoEligible); // 未达标数量
        map.put("totalSettlement", totalSettlement); // 已结算数量

        Double distance2 = 0.0;
        if (StringUtils.isNotEmpty(distance)) {
            distance2 = Double.valueOf(distance) * 1000;
        }
        map.put("distance", distance2);
        map.put("amount", amount);
        map.put("number", number);
        return ResponseUtil.ok(map);
    }


    /**
     * 未莱合伙人——查询商户直推会员信息 type:1、全部 2、达标 3、未达标 4、已结算 5、未结算 levelType:1、会员 2、非会员(代理)
     */
    @GetMapping("/getUserListByAgentV2")
    public Object getUserListByAgentV2(@LoginUser String userId, @RequestParam(defaultValue = "1") String type,
                    @RequestParam(required = false) String param, @RequestParam(required = false) String beginDate,
                    @RequestParam(required = false) String endDate, @RequestParam(required = false) String leaderId,
                    @RequestParam(required = false) String leaderLevel,
                    @RequestParam(required = false) String levelType, @RequestParam(defaultValue = "1") Integer page,
                    @RequestParam(defaultValue = "10") Integer limit) {

        List<MallUser> userList = new ArrayList<>();
        /** 粉丝列表 */
        List<Map<String, Object>> userListFans = new ArrayList<>();
        long total = 0;
        Integer totalUser = 0;
        Integer totalEligible = 0;
        Integer totalNoEligible = 0;
        Integer totalSettlement = 0;
        Integer totalNoSettlement = 0;
        if (userId != null) {
            MallUser mallUser = userService.findById(userId);
            if (mallUser != null) {
                // 是否达标
                String isEligible = null;
                // 是否结算
                String isSettlement = null;
                if (type.equals("2")) {
                    // 已达标
                    isEligible = "1";
                } else if (type.equals("3")) {
                    // 未达标
                    isEligible = "0";
                } else if (type.equals("4")) {
                    // 已结算
                    isSettlement = "1";
                } else if (type.equals("5")) {
                    // 未结算
                    isEligible = "1";
                    isSettlement = "0";
                }



                userList = userService.getFansListByAgent(userId, param, isEligible, isSettlement, page, limit);
                total = PageInfo.of(userList).getTotal();
                List<String> collect = userList.stream().map(MallUser::getId).distinct().collect(Collectors.toList());
                /** 领取红包 */
                List<MallRedQrcodeRecord> list = new ArrayList<>();
                if (null != collect && collect.size() > 0) {
                    list = iMallRedQrcodeRecordService.selectListFans(collect);
                }
                for (MallUser user : userList) {
                    Map<String, Object> dataMap = new HashMap<>();
                    List<MallRedQrcodeRecord> collect1 = list.stream().filter(
                                    mallRedQrcodeRecord -> mallRedQrcodeRecord.getToUserId().equals(user.getId()))
                                    .collect(Collectors.toList());
                    if (StringUtil.isNotEmpty(user.getWeixinOpenid()) && null != user.getRegisterDevice()
                                    && null != collect1 && collect1.size() > 0
                                    && StringUtil.isNotEmpty(user.getChannelId()) && user.getChannelId().equals("3")) {
                        /** 已达标 */
                        if (user.getIsEligible() == 0) {
                            user.setIsEligible((byte) 1);
                        } else if (user.getIsEligible() == 2) {
                            user.setIsEligible((byte) 3);
                        }
                        user.setEligibleTime(LocalDateTime.now());
                        userService.updateById(user);
                    }

                    /** 是否领取红包 */
                    if (null != collect1 && collect1.size() > 0) {
                        dataMap.put("isRed", 1);
                    } else {
                        dataMap.put("isRed", 0);
                    }
                    /** 是否微信授权 */
                    if (StringUtil.isNotEmpty(user.getWeixinOpenid())) {
                        dataMap.put("isWx", 1);
                    } else {
                        dataMap.put("isWx", 0);
                    }
                    if (null != user.getRegisterDevice()) {
                        dataMap.put("isActivate", user.getRegisterDevice());
                    } else {
                        dataMap.put("isActivate", 0);
                    }
                    // 是否结算
                    dataMap.put("isSettlement", user.getIsSettlement());
                    if (StringUtil.isNotEmpty(user.getNickname())) {
                        dataMap.put("userName", user.getNickname());
                    } else {
                        dataMap.put("userName", user.getMobile());
                    }
                    // todo手机号脱敏
                    // dataMap.put("mobile", NameUtil.mobileEncrypt(user.getMobile()));
                    dataMap.put("mobile", user.getMobile());
                    // 注册时间
                    dataMap.put("addTime", user.getAddTime().toLocalDate());
                    // 粉丝来源
                    if (MallUserEnum.CHANNEL_ID_0.getCode().equals(user.getChannelId())) {
                        dataMap.put("channle", "商圈渠道");
                    } else if (MallUserEnum.CHANNEL_ID_1.getCode().equals(user.getChannelId())) {
                        dataMap.put("channle", "分享注册");
                    } else if (MallUserEnum.CHANNEL_ID_2.getCode().equals(user.getChannelId())) {
                        dataMap.put("channle", "活动注册");
                    } else if (MallUserEnum.CHANNEL_ID_3.getCode().equals(user.getChannelId())) {
                        dataMap.put("channle", "扫码领红包");
                    } else {
                        dataMap.put("channle", "自主注册");
                    }

                    // 用户等级
                    if (AccountStatus.LEVEL_0.equals(user.getLevelId())) {
                        dataMap.put("levelId", "普通用户");
                    } else if (AccountStatus.LEVEL_1.equals(user.getLevelId())) {
                        dataMap.put("levelId", "VIP用户");
                    } else if (AccountStatus.LEVEL_4.equals(user.getLevelId())) {
                        dataMap.put("levelId", "联盟商家");
                    } else if (AccountStatus.LEVEL_5.equals(user.getLevelId())) {
                        dataMap.put("levelId", "区代");
                    } else if (AccountStatus.LEVEL_6.equals(user.getLevelId())) {
                        dataMap.put("levelId", "市代");
                    }
                    userListFans.add(dataMap);
                }
                List<MallUser> userListFans1 = userService.getUserListFans(userId,null,null);
                for (MallUser user1 : userListFans1) {
                    List<MallUser> collect1 = userListFans1.stream()
                                    .filter(user3 -> user3.getId().equals(user1.getId())).collect(Collectors.toList());
                    if (null != collect1 && collect1.size() > 0) {
                        String mobile = collect1.get(0).getMobile();
                        for (Map map : userListFans) {
                            if (map.get("mobile").equals(mobile)) {
                                // 结算时间
                                if (null != collect1.get(0).getSettlementTime()) {
                                    map.put("settlementTime", collect1.get(0).getSettlementTime().toLocalDate());
                                } else {
                                    map.put("settlementTime", "");
                                }
                                // 达标时间
                                if (null != collect1.get(0).getEligibleTime()) {
                                    map.put("eligibleTime", collect1.get(0).getEligibleTime().toLocalDate());
                                } else {
                                    map.put("eligibleTime", "");
                                }
                                // 达标状态
                                map.put("isEligible", collect1.get(0).getIsEligible());

                            }
                        }
                    }
                }


                // (2)统计数据
                // 全部
                totalUser = userListFans1.size();
                // 达标数量
                totalEligible = userListFans1.stream()
                                .filter(mallUser1 -> mallUser1.getIsEligible().equals((byte) 1)
                                                || mallUser1.getIsEligible().equals((byte) 3))
                                .collect(Collectors.toList()).size();
                // 未达标数量
                totalNoEligible = userListFans1.stream().filter(mallUser1 -> mallUser1.getIsEligible().equals((byte) 0))
                                .collect(Collectors.toList()).size();
                // 已结算数量
                totalSettlement =
                                userListFans1.stream().filter(mallUser1 -> mallUser1.getIsSettlement().equals((byte) 1))
                                                .collect(Collectors.toList()).size();
                // 未结算数量
                totalNoSettlement = userListFans1.stream()
                                .filter(mallUser1 -> mallUser1.getIsSettlement().equals((byte) 0)
                                                && mallUser1.getIsEligible().equals((byte) 1)
                                                || mallUser1.getIsEligible().equals((byte) 3))
                                .collect(Collectors.toList()).size();
            }
        }
        if (userList.size() > 0) {
            userListFans = SensitiveUtil.desCopyCollection(userListFans);
        }

        Map<String, Object> map = new HashMap<>();
        map.put("userList", userListFans);
        map.put("total", totalUser);

        map.put("totalUser", total);
        map.put("totalEligible", totalEligible);
        map.put("totalNoEligible", totalNoEligible);
        map.put("totalSettlement", totalSettlement);
        map.put("totalNoSettlement", totalNoSettlement);

        return ResponseUtil.ok(map);
    }

    /**
     * 未莱合伙人——查询粉丝
     */
    @GetMapping("/getUserListByMaker")
    public Object getUserListByMaker(@LoginUser String userId, @RequestParam(defaultValue = "1") String type,
                    @RequestParam(required = false) String param, @RequestParam(defaultValue = "1") Integer page,
                    @RequestParam(defaultValue = "10") Integer limit) {

        List<MallUserAgentVo> userList = new ArrayList<>();
        long total = 0;
        Integer totalUser = 0;
        if (null != userId) {
            MallUser mallUser = userService.findById(userId);
            if (null != mallUser) {
                userList = userService.getUserListByMaker(userId, param, page, limit);
                total = PageInfo.of(userList).getTotal();
            }
        }
        List<MallUserAgentVo> userList1 = new ArrayList<>();
        List<String> users = new ArrayList<>();
        if (null != userList && userList.size() > 0) {
            for (MallUserAgentVo mallUser : userList) {
                users.add(mallUser.getId());
            }
            // 同一查询
            List<MallOrder> byUserIds = orderService.findByUserIds(users);
            for (MallUserAgentVo mallUser1 : userList) {
                BigDecimal orderAmount = new BigDecimal("0");// 累计金额
                int orderNum = 0;// 下单数量
                LocalDateTime now = null;
                if (null != byUserIds && byUserIds.size() > 0) {
                    for (MallOrder order : byUserIds) {
                        now = order.getPayTime();
                        if (order.getUserId().equals(mallUser1.getId())) {
                            if (null != order.getActualPrice()) {
                                orderAmount = orderAmount.add(order.getActualPrice());
                            }
                            orderNum++;
                            if (null != order.getPayTime() && order.getPayTime().isAfter(now)) {
                                now = order.getPayTime();
                            }
                        }
                    }
                }
                switch (mallUser1.getLevelId()) {
                    case "1":
                        mallUser1.setLevelName("VIP");
                        break;
                    case "0":
                        mallUser1.setLevelName("普通用户");
                        break;
                    case "4":
                        mallUser1.setLevelName("联盟商家");
                        break;
                    case "5":
                        mallUser1.setLevelName("区代");
                        break;
                    case "6":
                        mallUser1.setLevelName("市代");
                        break;
                }


                mallUser1.setPayTime(now);
                mallUser1.setOrderNum(orderNum);
                mallUser1.setOrderAmount(orderAmount);
                userList1.add(mallUser1);
            }
            userList1 = SensitiveUtil.desCopyCollection(userList);
        }

        Map<String, Object> map = new HashMap<>();
        map.put("userList", userList1);
        // map.put("total", total);
        map.put("date", LocalDate.now());
        map.put("total", total); // 全部
        map.put("totalUser", total); // 全部

        return ResponseUtil.ok(map);
    }



    /**
     * 未莱合伙人——拉新概况统计
     */
    @GetMapping("/pullNewMemberStatistics")
    public Object pullNewMemberStatistics(@LoginUser String userId,
                    @RequestParam(defaultValue = "day") String conditions) {

        Integer totalUser = 0;
        Integer totalEligible = 0;
        Integer totalNoEligible = 0;
        BigDecimal rate = BigDecimal.ZERO;
        List<Map<String, Object>> returnList = new ArrayList<>();
        if (userId != null) {
            MallUser mallUser = userService.findById(userId);
            if (mallUser != null) {
                if (!(mallUser.getLevelId().equals(AccountStatus.LEVEL_5)
                                || mallUser.getLevelId().equals(AccountStatus.LEVEL_6)
                                || "1".equals(mallUser.getIsMaker()))) {
                    return ResponseUtil.ok();
                }
                String isEligible = null; // 是否达标
                String isSettlement = null; // 是否结算

                List<String> levelIdList = new ArrayList<>();
                MallUserAgentVo mallUser1 = new MallUserAgentVo();

                if (mallUser.getLevelId().equals(AccountStatus.LEVEL_5)) {
                    /** 区代 **/

                    mallUser1.setSecondLeader(userId);

                    // if(StringUtils.isEmpty(levelType)){
                    // levelIdList.add("1");
                    // levelIdList.add("4");
                    // levelIdList.add("5");
                    // }else if(levelType.equals("1")){
                    // levelIdList.add("1"); //会员
                    // }else if(levelType.equals("2")){
                    // levelIdList.add("4"); //服务商
                    // levelIdList.add("5"); //区代
                    // }
                } else if (mallUser.getLevelId().equals(AccountStatus.LEVEL_6)) {
                    /** 市代 **/
                    mallUser1.setThirdLeader(userId);

                    // if(StringUtils.isEmpty(levelType)){
                    // levelIdList.add("1");
                    // levelIdList.add("4");
                    // levelIdList.add("5");
                    // }else if(levelType.equals("1")){
                    // //会员
                    // levelIdList.add("1"); //会员
                    // }else if(levelType.equals("2")){
                    // //非会员
                    // levelIdList.add("4"); //服务商
                    // levelIdList.add("5"); //区代
                    // }
                }

                // (2)统计数据
                mallUser1.setConditions(conditions);
                mallUser1.setId(userId);
                // mallUser1.setLevelIdList(levelIdList);
                mallUser1.setLevelId(AccountStatus.LEVEL_1); // 会员
                mallUser1.setChannelId(MallUserEnum.CHANNEL_ID_0.getCode()); // 商圈渠道

                // 全部
                totalUser = userService.countByCondition(mallUser1);
                // 达标数量
                mallUser1.setIsEligible((byte) 1);
                totalEligible = userService.countByCondition(mallUser1);
                List<Map<String, Object>> EligibleByDay = userService.eligibleUserCountByDay(mallUser1);

                // 未达标数量
                mallUser1.setIsEligible((byte) 0);
                totalNoEligible = userService.countByCondition(mallUser1);
                List<Map<String, Object>> noEligibleByDay = userService.eligibleUserCountByDay(mallUser1);

                if ("day".equals(conditions)) {
                    List<String> days = DateUtilTwo.getDate(6);
                    for (String date : days) {
                        Long eligibleCount = 0L;
                        Long noEligibleCount = 0L;
                        Map<String, Object> returnMap = new HashMap<>(4);
                        for (Map<String, Object> map : EligibleByDay) {
                            if (date.equals(map.get("dayTime"))) {
                                eligibleCount = Long.parseLong(map.get("userCount").toString());
                            }
                        }

                        for (Map<String, Object> map : noEligibleByDay) {
                            if (date.equals(map.get("dayTime"))) {
                                noEligibleCount = Long.parseLong(map.get("userCount").toString());
                            }
                        }
                        date = date.substring(5, date.length());
                        returnMap.put("dayTime", date);
                        returnMap.put("eligibleCount", eligibleCount);
                        returnMap.put("noEligibleCount", noEligibleCount);
                        returnList.add(returnMap);
                    }
                } else if (Objects.equals(conditions, "week")) {
                    List<String> days = DateUtilTwo.getDate(28);

                    // 近第一周
                    String firstEnd = days.get(0);
                    String firstStart = days.get(6);
                    // 近第二周
                    String secondEnd = days.get(7);
                    String secondStart = days.get(13);
                    // 近第三周
                    String thirdEnd = days.get(14);
                    String thirdStart = days.get(20);
                    // 近第四周
                    String fourthEnd = days.get(21);
                    String fourthStart = days.get(27);
                    Map<String, Object> firstMap2 = new HashMap<String, Object>();
                    firstMap2.put("noEligibleCount", 0);

                    Map<String, Object> secondMap2 = new HashMap<String, Object>();
                    secondMap2.put("noEligibleCount", 0);

                    Map<String, Object> thirdMap2 = new HashMap<String, Object>();
                    thirdMap2.put("noEligibleCount", 0);

                    Map<String, Object> fourthMap2 = new HashMap<String, Object>();
                    fourthMap2.put("noEligibleCount", 0);

                    mallUser1.setIsEligible((byte) 1);
                    mallUser1.setBeginDate(firstStart);
                    mallUser1.setEndDate(firstEnd);
                    Map<String, Object> firstMap = userService.eligibleUserCountByDateTime(mallUser1);
                    mallUser1.setBeginDate(secondStart);
                    mallUser1.setEndDate(secondEnd);
                    Map<String, Object> secondMap = userService.eligibleUserCountByDateTime(mallUser1);
                    mallUser1.setBeginDate(thirdStart);
                    mallUser1.setEndDate(thirdEnd);
                    Map<String, Object> thirdMap = userService.eligibleUserCountByDateTime(mallUser1);
                    mallUser1.setBeginDate(fourthStart);
                    mallUser1.setEndDate(fourthEnd);
                    Map<String, Object> fourthMap = userService.eligibleUserCountByDateTime(mallUser1);
                    mallUser1.setIsEligible((byte) 0);
                    mallUser1.setBeginDate(firstStart);
                    mallUser1.setEndDate(firstEnd);
                    firstMap2 = userService.eligibleUserCountByDateTime(mallUser1);
                    mallUser1.setBeginDate(secondStart);
                    mallUser1.setEndDate(secondEnd);
                    secondMap2 = userService.eligibleUserCountByDateTime(mallUser1);
                    mallUser1.setBeginDate(thirdStart);
                    mallUser1.setEndDate(thirdEnd);
                    thirdMap2 = userService.eligibleUserCountByDateTime(mallUser1);
                    mallUser1.setBeginDate(fourthStart);
                    mallUser1.setEndDate(fourthEnd);
                    fourthMap2 = userService.eligibleUserCountByDateTime(mallUser1);

                    firstMap2.put("eligibleCount", firstMap.get("noEligibleCount"));
                    firstMap2.put("dayTime", firstStart.substring(5, firstStart.length()) + "~"
                                    + firstEnd.substring(5, firstEnd.length()));
                    returnList.add(firstMap2);
                    secondMap2.put("eligibleCount", secondMap.get("noEligibleCount"));
                    secondMap2.put("dayTime", secondStart.substring(5, secondStart.length()) + "~"
                                    + secondEnd.substring(5, secondEnd.length()));
                    returnList.add(secondMap2);
                    thirdMap2.put("eligibleCount", thirdMap.get("noEligibleCount"));
                    thirdMap2.put("dayTime", thirdStart.substring(5, thirdStart.length()) + "~"
                                    + thirdEnd.substring(5, thirdEnd.length()));
                    returnList.add(thirdMap2);
                    fourthMap2.put("eligibleCount", fourthMap.get("noEligibleCount"));
                    fourthMap2.put("dayTime", fourthStart.substring(5, fourthStart.length()) + "~"
                                    + fourthEnd.substring(5, fourthEnd.length()));
                    returnList.add(fourthMap2);

                } else if (Objects.equals(conditions, "month")) {
                    for (int i = 0; i < 3; i++) {
                        String date = DateUtilTwo.getDateMonth(DateUtilTwo.DATE_PATTERN_M2, i == 0 ? 0 : -(i));
                        Long eligibleCount = 0L;
                        Long noEligibleCount = 0L;
                        Map<String, Object> returnMap = new HashMap<>(4);
                        for (Map<String, Object> map : EligibleByDay) {
                            if (date.equals(map.get("monthTime"))) {
                                eligibleCount = Long.parseLong(map.get("userCount").toString());
                            }
                        }

                        for (Map<String, Object> map : noEligibleByDay) {
                            if (date.equals(map.get("monthTime"))) {
                                noEligibleCount = Long.parseLong(map.get("userCount").toString());
                            }
                        }

                        returnMap.put("dayTime", date);
                        returnMap.put("eligibleCount", eligibleCount);
                        returnMap.put("noEligibleCount", noEligibleCount);
                        returnList.add(returnMap);

                    }
                }

                // 达标率
                if (0 != totalUser) {
                    rate = new BigDecimal(totalEligible).divide(new BigDecimal(totalUser), 3, BigDecimal.ROUND_UP)
                                    .multiply(new BigDecimal(100)).setScale(2, BigDecimal.ROUND_DOWN);
                }

            }
        }

        Map<String, Object> map = new HashMap<>();

        map.put("totalUser", totalUser); // 全部
        map.put("totalEligible", totalEligible); // 达标数量
        map.put("totalNoEligible", totalNoEligible); // 未达标数量
        map.put("rate", rate); // 达标率
        map.put("eligibleByTime", returnList);

        return ResponseUtil.ok(map);
    }



    /** -------------------------------创客接口------------------------------- **/



    // 查询创客价格
    @RequestMapping("/queryMakerPrice")
    public Object queryMakerPrice() {
        Map<String, String> stringStringMap = mallSystemConfigService.queryMaker();// 查询创客信息
        List<MallUserProfitNew> list1 =
                        serviceImpl.list(new QueryWrapper<MallUserProfitNew>().eq("deleted", 0).eq("profit_type", 4));
        List<Map> list = new ArrayList<>();
        if (stringStringMap.size() > 0) {
            String type = "";
            if (list1.size() > 0) {
                type = stringStringMap.get("mall_maker_type");
            } else {
                // 防止一种模式出错
                type = "1";
            }
            if ("1".equals(type)) {
                String price = stringStringMap.get("mall_maker_price");
                String level = stringStringMap.get("mall_maker_level");
                String[] str = price.split(",");
                String[] str1 = level.split(",");
                if (str.length > 1) {
                    for (int i = 0; i < str.length; i++) {
                        Map<String, Object> map = new HashMap<>();
                        map.put("price", str[i]);
                        map.put("type", str1[i]);
                        list.add(map);
                    }
                    return ResponseUtil.ok(list);
                } else {
                    Map<String, Object> map = new HashMap<>();
                    map.put("price", price);
                    map.put("type", level);
                    list.add(map);
                    return ResponseUtil.ok(list);
                }
            } else {
                String level = stringStringMap.get("mall_maker_level");
                Map<String, Object> map = new HashMap<>();
                map.put("price", list1.get(0).getPackageAmount());
                map.put("type", level);
                list.add(map);
                return ResponseUtil.ok(list);

            }
        }
        return ResponseUtil.ok();
    }

    /**
     * 升级创客赠送商品
     * 
     * @return 商品信息
     */
    @GetMapping("/makerGoods")
    public Object makerGoods() {
        Map<String, String> stringStringMap = mallSystemConfigService.makerGoods();
        // 红酒
        String alcohol = stringStringMap.get("mall_maker_goods_alcohol");
        String alcoholName = stringStringMap.get("mall_maker_goods_alcohol_name");
        String alcoholSubtitle = stringStringMap.get("mall_maker_goods_alcohol_subtitle");
        // 小藻球
        String microalgae = stringStringMap.get("mall_maker_goods_microalgae");
        String microalgaeName = stringStringMap.get("mall_maker_goods_microalgae_name");
        String microalgaeSubtitle = stringStringMap.get("mall_maker_goods_microalgae_subtitle");
        // 小藻球
        String microalgae1 = stringStringMap.get("mall_maker_goods_microal");
        String microalgaeName1 = stringStringMap.get("mall_maker_goods_microal_name");
        String microalgaeSubtitle1 = stringStringMap.get("mall_maker_goods_microal_subtitle");
        Map<String, String> alcoholMap = new HashMap<>();
        alcoholMap.put("type", alcohol);
        alcoholMap.put("name", alcoholName);
        alcoholMap.put("subtitle", alcoholSubtitle);
        Map<String, String> map = new HashMap<>();
        map.put("type", microalgae);
        map.put("name", microalgaeName);
        map.put("subtitle", microalgaeSubtitle);
        Map<String, String> map2 = new HashMap<>();
        map2.put("type", microalgae1);
        map2.put("name", microalgaeName1);
        map2.put("subtitle", microalgaeSubtitle1);
        List<Map<String, String>> list = new ArrayList<>();
        list.add(alcoholMap);
        list.add(map);
        list.add(map2);
        return ResponseUtil.ok(list);
    }

    /**
     * 查询客服电话
     */
    @GetMapping("/getPhoneList")
    public Object getPhone() {

        Map<String, String> stringStringMap = mallSystemConfigService.queryPhone();// 查询客服电话
        Map<String, Object> map = new HashMap<>();
        map.put("makerPhone", stringStringMap.get("Mall_phone_maker"));// 创客客服
        map.put("zyPhone", stringStringMap.get("Mall_phone_zy")); // 自营客服
        map.put("qyPhone", stringStringMap.get("Mall_phone_qy")); // 权益客服
        return ResponseUtil.ok(map);
    }

    /**
     * 设置用户的生日信息/获取信用分
     * @param userId
     * @param
     * @return
     */
    @PostMapping("/setBirthdayOrGender")
    public Object setUserBirthday(@LoginUser String userId,@RequestBody SetUserBirthdayVo setUserBirthdayVo){
        if (userId==null){
            return ResponseUtil.unlogin();
        }
        try {
            MallUser mallUser = new MallUser();
            if (setUserBirthdayVo.getGender()!=null){
                mallUser.setGender(setUserBirthdayVo.getGender());
            }
            if(setUserBirthdayVo.getBirthday()!=null){
                DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                LocalDate birthday = LocalDate.parse(setUserBirthdayVo.getBirthday()+"-01-01",df);
                mallUser.setBirthday(birthday);
            }
            mallUser.setId(userId);
            userService.updateById(mallUser);
            Map<String, Object> paramsMap1 = new HashMap();
            paramsMap1.put("dType", AccountStatus.DISTRIBUTION_TYPE_00); // 直接分润
            String content="";
            if (mallUser.getBirthday()!=null){
                paramsMap1.put("message", "设置生日奖励");
                content="生日";
            }else if (mallUser.getGender()!=null){
                paramsMap1.put("message", "设置性别奖励");
                content="性别";
            }
            paramsMap1.put("capitalTrend", AccountStatus.CAPITAL_TREND_01);
            paramsMap1.put("changeType", AccountStatus.CHANGE_TYPE_01);
            paramsMap1.put("changeAccountType", AccountStatus.CHANGE_ACCOUNT_TYPE_4);
            AccountUtils.accountChange(userId, new BigDecimal(10), CommonConstant.TASK_REWARD_SCORE,
                    AccountStatus.BID_6, paramsMap1);

            PushUtils.pushMessage(userId, "佣金通知", "您补充自己的"+content+"信息获得了10信用分", 3, 0);
        } catch (Exception e) {
            logger.info( "信用分增加失败:"+e.getMessage());
            return ResponseUtil.fail("信用分增加失败,请联系管理员");
        }

        return ResponseUtil.ok();
    }

}
