package com.graphai.mall.wx.web.redpacket;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.graphai.mall.db.service.index.IndexGoodsComponent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.graphai.commons.util.NameUtil;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.mall.admin.service.IMallRedQrcodeRecordService;
import com.graphai.mall.db.service.game.GameRedpacketRainService;
import com.graphai.mall.db.service.red.WxRedRankService;
import com.graphai.mall.db.service.red.WxRedService;
import com.graphai.mall.wx.annotation.LoginUser;

/**
 * 拼团红包
 * 
 * @author biteam 整理
 *
 */
@RestController
@RequestMapping("/wx/gamejackpot")
@Validated
public class WxGroupRedPacketController {

    @Autowired
    private WxRedService wxRedService;
    @Autowired
    private WxRedRankService wxRedRankService;
    @Autowired
    private GameRedpacketRainService gameRedpacketRainService;
    @Autowired
    private IMallRedQrcodeRecordService iMallRedQrcodeRecordService;
    @Autowired
    private IndexGoodsComponent indexGoodsComponent;

    /**
     * 拼团红包弹幕+疯抢排行
     *
     * @param
     * @throws IOException
     */
    @GetMapping("/groupBarrage")
    public Object groupBarrageList(@LoginUser String userId) {
        if (null == userId) {
            return ResponseUtil.unlogin();
        }
        /** 弹幕列表 */
        // List<Map<String, Object>> maps = gameRedpacketRainService.barrageList2();;// 方法1：mysql直接查询
        List<Map<String, Object>> maps = wxRedRankService.getRedRankingCommon("4");// 方法2：redis缓存方法

        /** 排行榜 */
        // List<Map<String, Object>> list1 = gameRedpacketRainService.barrageList3(); // 方法1：mysql直接查询
        List<Map<String, Object>> list1 = wxRedRankService.getRedRankingCommon("2"); // 方法2：redis缓存方法

        /** 参团排行 */
        List<Map<String, Object>> groupList = gameRedpacketRainService.barrageList4();

        /** 当日累计领取 */

        BigDecimal totalAmountByUser = iMallRedQrcodeRecordService.getTotalAmountByUser(userId);
        if (null != maps && maps.size() > 0) {
            for (Map<String, Object> map : maps) {
                String nickname = NameUtil.nameDesensitizationDef(String.valueOf(map.get("nickname")));
                if ("".equals(nickname)) {
                    map.put("nickname", NameUtil.nameDesensitizationDef(String.valueOf(map.get("mobile"))));
                } else {
                    map.put("nickname", nickname);
                }
            }
        }
        if (null != list1 && list1.size() > 0) {
            for (Map<String, Object> map : list1) {
                String nickname = NameUtil.nameDesensitizationDef(String.valueOf(map.get("nickname")));
                if ("".equals(nickname)) {
                    map.put("nickname", NameUtil.nameDesensitizationDef(String.valueOf(map.get("mobile"))));
                } else {
                    map.put("nickname", nickname);
                }
            }
        }
        if (null != groupList && groupList.size() > 0) {
            for (Map<String, Object> map : groupList) {
                String nickname = NameUtil.nameDesensitizationDef(String.valueOf(map.get("nickname")));
                if ("".equals(nickname)) {
                    map.put("nickname", NameUtil.nameDesensitizationDef(String.valueOf(map.get("mobile"))));
                } else {
                    map.put("nickname", nickname);
                }
            }
        }
        Map<String, Object> map = new HashMap<>();
        map.put("barrage", maps);
        map.put("list", list1);
        map.put("groupList", groupList);
        map.put("totalAmount", totalAmountByUser);
        return ResponseUtil.ok(map);
    }

    /**
     * 2.1、拼团红包-获取拼团口令
     */
    @GetMapping("/findRedGroupShare")
    public Object getFindRedGroupShare(@LoginUser String userId) {
        return wxRedService.getFindRedGroupShare(userId);
    }

    /**
     * 2.1、拼团红包-获取拼团口令(免token)
     */
    @GetMapping("/findRedGroupShareNoToken")
    public Object getFindRedGroupShareNoToken(@RequestParam String userId) {
        return wxRedService.getFindRedGroupShare(userId);
    }

    /**
     * 2.2、拼团红包-分享口令ID查询信息（登录）
     */
    @GetMapping("/findRedGroupIdAndUserId")
    public Object getFindRedGroupIdAndUserId(@LoginUser String userId, @RequestParam String shareId) {
        if (null == userId) {
            return ResponseUtil.unlogin();
        }
        return wxRedService.getFindRedGroupId(shareId);
    }

    /**
     * 2.3、拼团红包-分享口令ID查询信息（免登录）
     */
    @GetMapping("/findRedGroupId")
    public Object getFindRedGroupId(@RequestParam String shareId) {
        return wxRedService.getFindRedGroupId(shareId);
    }

    /**
     * 2.4、拼团红包-参团
     */
    @PostMapping("/joinRedGroup")
    public Object joinInRedGroup(@LoginUser String userId, @RequestBody String body) {
        return wxRedService.joinInRedGroup(userId, body);
    }

    /**
     * 拼团红包-日期/金额预告
     */
    @GetMapping("/getGroupRedAdvance")
    public Object getGroupRedAdvance(@LoginUser String userId) {
        return wxRedService.getGroupRedAdvance(userId);

    }

    /**
     * 团购分享-获取口令
     */
    @PostMapping("/getGroupPwd")
    public Object getGroupPwd(@RequestBody String body) {
        return indexGoodsComponent.getGroupPwd(body);
    }

}
