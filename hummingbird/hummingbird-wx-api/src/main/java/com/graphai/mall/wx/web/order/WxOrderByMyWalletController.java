package com.graphai.mall.wx.web.order;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.mall.db.domain.MallGroupon;
import com.graphai.mall.db.domain.MallGrouponRules;
import com.graphai.mall.db.domain.MallOrder;
import com.graphai.mall.db.service.groupon.MallGrouponService;
import com.graphai.mall.db.service.order.MallOrderService;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.mall.wx.service.WxOrderByMyWalletService;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/wx/order/wallet")
@Validated
public class WxOrderByMyWalletController {
    private final Log logger = LogFactory.getLog(WxOrderByMyWalletController.class);

    @Resource
    private WxOrderByMyWalletService wxOrderByMyWalletService;
    @Autowired
    private MallOrderService orderService;
    @Autowired
    private MallGrouponService grouponService;
    @Autowired
    private MallGrouponService mallGrouponService;
    @Value("${rsa.payPassword.privateKey}")
    private String privateKey;

    /**
     * 校验用户 的 钱包  是否有足够的扣款余额
     * @param userId 当前登录用户的id
     * @param orderId 订单表的 id
     * @param type 钱包类型 (task:任务钱包   coins:钱包)
     * @return
     */
    @GetMapping("/checkUserWalletIsFull")
    public Object checkUserWalletIsFull(@LoginUser String userId, @Param("orderId")String orderId,@Param("type")String type){
        if (StringUtils.isEmpty(userId)) {
            return ResponseUtil.unlogin();
        }
        return wxOrderByMyWalletService.checkUserWalletIsFull(userId,orderId,type);
    }

    /**
     * 开始扣钱，以及，处理相应的业务逻辑
     * @param userId
     * @param orderId 订单Id
     * @return
     */
    @GetMapping("/goToUpdateAboutTaskPayAfter")
    public Object goToUpdateAboutTaskPayAfter(@LoginUser String userId,@Param("orderId")String orderId){
        if(StringUtils.isEmpty(userId)){
           return ResponseUtil.unlogin();
        }
        MallOrder order = orderService.findById(orderId);
        if (order != null){


            List<MallGroupon> groupByPo = orderService.getGroupByPo(order.getParentOrderOn());
            String groupId = null;
            if (groupByPo != null && groupByPo.size() > 0) {
                if (groupByPo.get(0).getGrouponId().equals("0")) {
                    groupId = groupByPo.get(0).getId();
                } else {
                    groupId = groupByPo.get(0).getGrouponId();
                }
            }
            //discount_member
            List<MallGroupon> groupIds = grouponService.queryIsGroupLead(groupId);
            MallGrouponRules rules = orderService.getRules(groupIds.get(0).getRulesId());
            if(groupIds.get(0).getJoinNum() != null && groupIds.get(0).getJoinNum() >= rules.getDiscountMember()){
                return ResponseUtil.fail(502,"该团已满人");
            }
        }
        Object task = wxOrderByMyWalletService.checkUserWalletIsFull(userId, orderId, "task");
        Map<String,Object> map = (Map)task;
        Integer i =  Integer.parseInt(String.valueOf(map.get("errno")));
        if (i != 0){
            return ResponseUtil.fail((String)map.get("errmsg"));
        }
        return wxOrderByMyWalletService.goToUpdateAboutTaskPayAfter(userId,orderId);
    }

    /**
     *  判断支付密码是否正确，如果正确的话，返回orderId，错误的话，返回错误信息
     * @param userId
     * @param payPassword 加密后的支付密码
     * @param orderId 订单id
     * @return
     */
    @PostMapping("/checkUserPayPassword")
    public Object checkUserPayPassword(@LoginUser String userId, @RequestParam("payPassword")String payPassword,@Param("orderId")String orderId){
        if(StringUtils.isEmpty(userId)){
            return ResponseUtil.unlogin();
        }
        return wxOrderByMyWalletService.checkUserPayPassword(userId,payPassword,privateKey,orderId);
    }





}
