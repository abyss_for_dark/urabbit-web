package com.graphai.mall.wx.web.game;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.mall.wx.util.TuiAUtils;
import com.graphai.mall.wx.vo.TuiAResult;
import com.graphai.mall.wx.vo.TuiAVo;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author: jw
 * @Date: 2021/1/28 16:18
 * 推啊游戏接入
 */


@RestController
@RequestMapping("/wx/tuia")
public class WxGameTuiAController {


    @PostMapping("/getGame")
    public Object getGameJackpotRedpackt2(@LoginUser String userId, @RequestBody TuiAVo tuiAVo) {
        if (null == userId) {
            return ResponseUtil.unlogin();
        }

        TuiAResult result = null;
        try {
             result = TuiAUtils.generatorUrl(tuiAVo,userId);
        } catch (Exception e) {
            e.printStackTrace();
        }


        HashMap<String, Object> map = new HashMap<>();



        if (result==null){
            //获取链接失败
            map.put("status","0");
            ResponseUtil.ok(map);
        }

            //获取链接成功
        map.put("status","1");
        map.put("data",result.getData());

        return ResponseUtil.ok(map);


    }




}
