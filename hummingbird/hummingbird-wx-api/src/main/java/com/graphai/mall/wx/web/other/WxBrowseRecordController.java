package com.graphai.mall.wx.web.other;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Header;
import org.apache.http.message.BasicHeader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.graphai.commons.constant.MallConstant;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.id.GraphaiIdGenerator;
import com.graphai.commons.util.json.JacksonUtils;
import com.graphai.mall.db.domain.MallBrowseRecord;
import com.graphai.mall.db.domain.MallCategory;
import com.graphai.mall.db.domain.MallUser;
import com.graphai.mall.db.service.MallBrowseRecordService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.util.StringHelper;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.open.api.getway.HttpRequestUtils;
import com.graphai.properties.PlatFormServerproperty;

/**
 * 浏览记录服务
 */
@RestController
@RequestMapping("/wx/browseRecord")
@Validated
public class WxBrowseRecordController {
	
	private final Log logger = LogFactory.getLog(WxBrowseRecordController.class);
	
	@Autowired
	private MallBrowseRecordService mallBrowseRecordService;
	
	 @Autowired
	 private PlatFormServerproperty platFormServerproperty;
	 
	 @Autowired
	 private MallUserService userService;
	
	
	/**
     * 获取浏览记录或者热门活动
     *
     * @param type record浏览记录   activity热门活动
     * @return 获取浏览记录或者热门活动
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonParseException 
     */
    @GetMapping("getRecordOrActivitys")
    public Object current(@LoginUser String userId,@RequestParam(defaultValue = "activity") String type ) throws JsonParseException, JsonMappingException, IOException {
    	
       
    	if (Objects.equals(type, "record")) {
		    if (null!=userId) {
		    	List<MallBrowseRecord> browseRecords=	mallBrowseRecordService.selectByPage(userId,1, 10);
		    	if (CollectionUtils.isNotEmpty(browseRecords)) {
		    		return ResponseUtil.ok(browseRecords);
				}
			}
		}else {
			String url = platFormServerproperty.getUrl(MallConstant.TAOBAO);
			Map<String, Object> mappMap = new HashMap<String, Object>();
			mappMap.put("plateform", "dtk");
			mappMap.put("position", "3");

			Header[] headers = new BasicHeader[] { new BasicHeader("method", "queryActivityByPosition"),
					new BasicHeader("version", "v1.0") };

			String result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);

			JSONObject object = JSONObject.parseObject(result);

			if ("0".equals(object.getString("errno"))) {

				List mallActivitys = JSON.parseObject(object.get("data").toString(), List.class);
				return ResponseUtil.ok(mallActivitys);
			}
		}
    	
        return ResponseUtil.fail(509,"没查到数据!");
    }
    
    
    /**
     * 添加
     */
    @GetMapping("/add")
    public Object add(@LoginUser String userId,@RequestParam(required = true) String id) {
    	
    	if (userId == null) {
            return ResponseUtil.unlogin();
        }
    	
		MallCategory mallCategory = null;
		
		MallUser user = userService.findById(userId);
		if (Objects.equals(user, null)) {
			return ResponseUtil.fail(508, "查询用户信息异常!");
		}
		
		MallBrowseRecord record=mallBrowseRecordService.selectBrowseRecordByUserIdAndCategoryId(id, userId);
		
		//若存在则修改数据
		if (!Objects.equals(record, null)) {
			 if (mallBrowseRecordService.update(record)>0) {
          	   return ResponseUtil.ok();
			  } 
		}
		
		try {
			String url = platFormServerproperty.getUrl(MallConstant.TAOBAO);
			Map<String, Object> mappMap = new HashMap<String, Object>();
			mappMap.put("plateform", "dtk");
			mappMap.put("id", id);
			logger.info("入参类目id" + id);
			Header[] headers = new BasicHeader[] { new BasicHeader("method", "queryCategoryById"),
					new BasicHeader("version", "v1.0") };

			String result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);
			logger.info("open开放平台返参" + result);
			JSONObject object = JSONObject.parseObject(result);

			if ("0".equals(object.getString("errno"))) {
				mallCategory = JSON.parseObject(object.get("data").toString(), MallCategory.class);
			}

			if (!Objects.equals(mallCategory, null)) {
				
				
               MallBrowseRecord mallBrowseRecord=new MallBrowseRecord();
               mallBrowseRecord.setId(GraphaiIdGenerator.nextId("mallBrowseRecord"));
               mallBrowseRecord.setUserId(userId);
               mallBrowseRecord.setName(mallCategory.getName());
               mallBrowseRecord.setCategoryId(id);
               if (StringHelper.isNotNullAndEmpty(mallCategory.getIconUrl())) {
            	   mallBrowseRecord.setIconUrl(mallCategory.getIconUrl());
			   }
               
               if (StringHelper.isNotNullAndEmpty(mallCategory.getLinkUrl())) {
            	   mallBrowseRecord.setLinkUrl(mallCategory.getLinkUrl());
			   }
               
               if (StringHelper.isNotNullAndEmpty(mallCategory.getNavigatorType())) {
            	   mallBrowseRecord.setNavigatorType(mallCategory.getNavigatorType());
			   }
               
               if (StringHelper.isNotNullAndEmpty(mallCategory.getPositionCode())) {
            	   mallBrowseRecord.setPositionCode(mallCategory.getPositionCode());
			   }
               
               if (mallBrowseRecordService.add(mallBrowseRecord)>0) {
            	   return ResponseUtil.ok();
			  } 
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return ResponseUtil.fail();
    }
	

}
