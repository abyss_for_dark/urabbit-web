package com.graphai.mall.wx.web.wangzhuan;

import static com.graphai.mall.wx.util.WxResponseCode.AUTH_CAPTCHA_UNMATCH;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Header;
import org.apache.http.message.BasicHeader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.graphai.commons.constant.MallConstant;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.json.JacksonUtil;
import com.graphai.id.GraphaiIdGenerator;
import com.graphai.mall.admin.vo.WangzhuanTaskVo;
import com.graphai.commons.util.json.JacksonUtils;
import com.graphai.mall.db.domain.CustomWangZhuanTask;
import com.graphai.mall.db.domain.FcAccount;
import com.graphai.mall.db.domain.FcAccountRechargeBill;
import com.graphai.mall.db.domain.MallAd;
import com.graphai.mall.db.domain.MallCategory;
import com.graphai.mall.db.domain.MallFootprint;
import com.graphai.mall.db.domain.MallOrder;
import com.graphai.mall.db.domain.MallPhoneValidation;
import com.graphai.mall.db.domain.MallRegion;
import com.graphai.mall.db.domain.MallUser;
import com.graphai.mall.db.domain.WangzhuanCallbackCreditcard;
import com.graphai.mall.db.domain.WangzhuanTask;
import com.graphai.mall.db.domain.WangzhuanTaskStep;
import com.graphai.mall.db.domain.WangzhuanTaskStepWithBLOBs;
import com.graphai.mall.db.domain.WangzhuanUserTask;
import com.graphai.mall.db.service.account.AccountService;
import com.graphai.mall.db.constant.account.AccountStatus;
import com.graphai.mall.db.service.account.FcAccountRechargeBillService;
import com.graphai.mall.db.service.advertisement.MallAdService;
import com.graphai.mall.db.service.category.MallCategoryService;
import com.graphai.mall.db.service.common.AppVersionService;
import com.graphai.mall.db.service.common.MallPhoneValidationService;
import com.graphai.mall.db.service.common.MallRegionService;
import com.graphai.mall.db.service.order.MallOrderGoodsService;
import com.graphai.mall.db.service.order.MallOrderService;
import com.graphai.mall.db.service.sysconfig.MallSystemConfigService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.service.user.behavior.MallFootprintService;
import com.graphai.mall.db.service.wangzhuan.WangzhuanTaskService;
import com.graphai.mall.db.service.wangzhuan.WangzhuanTaskStepService;
import com.graphai.mall.db.service.wangzhuan.WangzhuanUserTaskService;
import com.graphai.mall.db.constant.CommonConstant;
import com.graphai.mall.db.util.RetrialUtils;
import com.graphai.mall.db.util.account.AccountUtils;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.mall.wx.util.YRDUtils;
import com.graphai.open.api.getway.HttpRequestUtils;
import com.graphai.properties.PlatFormServerproperty;
import com.graphai.validator.Order;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * (网赚)领取个人任务记录Controller
 *
 * @author ruoyi
 * @date 2020-04-08
 */
@RestController
@RequestMapping("/wx/userTask")
@Validated
public class WxWangzhuanUserTaskController {
    private final Log logger = LogFactory.getLog(WxWangzhuanUserTaskController.class);

    @Autowired
    private WangzhuanTaskService wangzhuanTaskService;
    @Autowired
    private WangzhuanTaskStepService wangzhuanTaskStepService;
    @Autowired
    private WangzhuanUserTaskService wangzhuanUserTaskService;
    @Autowired
    private MallAdService adService;
    @Autowired
    private MallPhoneValidationService phoneService;
    @Autowired
    private AccountService accountService;
    @Autowired
    private MallUserService mallUserService;
    @Autowired
    private MallOrderService mallOrderService;
    @Autowired
    private MallOrderGoodsService mallOrderGoodsService;
    @Autowired
    private MallFootprintService mallFootprintService;
    @Autowired
    private FcAccountRechargeBillService fcAccountRechargeBillService;
    @Autowired
    private AppVersionService versionService;
    @Resource
    private PlatFormServerproperty platFormServerproperty;
    @Resource
    private MallSystemConfigService mallSystemConfigService;
    @Resource
    private MallRegionService mallRegionService;

    @Resource
    private MallCategoryService mallCategoryService;

    private Object lock2 = new Object();

    /**
     * 查询(网赚)领取个人任务记录列表
     */
    @GetMapping("/list")
    public Object list(@LoginUser String userId, @RequestParam(defaultValue = "") String version,
                    @RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit,
                    @RequestParam(defaultValue = "id") String sort,
                    @Order @RequestParam(defaultValue = "desc") String order,
                    @RequestParam(defaultValue = "T09") String type, HttpServletRequest request) {
        // if ( null == userId ) {
        // return ResponseUtil.unlogin();
        // }
        Map<String, Object> data = new HashMap<>();

        WangzhuanUserTask wangzhuanUserTask = new WangzhuanUserTask();
        wangzhuanUserTask.setUserId(userId);
        // 当前用户试玩任务记录
        WangzhuanUserTask wzu = wangzhuanUserTaskService.selectCustomWangzhuanUserTaskList(userId);
        WangzhuanTask wangzhuanTask = null;
        // 每日任务列表
        List<WangzhuanTask> list2 = null;
        // 高佣任务列表
        List<WangzhuanTaskVo> list3 = null;


        List list = new ArrayList();
        List<MallAd> layout = adService.queryIndex((byte) 127);
        data.put("ad", layout);

        // 每日任务列表
        if ("T09".equals(type)) {
            list2 = wangzhuanTaskService.selectCustomWangzhuanTaskListByType(userId, page, limit, type);
            if (list2.size() > 0) {
                for (WangzhuanTask wz : list2) {
                    List<WangzhuanUserTask> wu =
                                    wangzhuanTaskService.selectCustomWzUserTaskListByType(userId, wz.getId());
                    Map<String, Object> wmap = new HashMap<>();
                    wmap.put("name", wz.getTaskName());
                    wmap.put("icon", wz.getImageUrl());
                    wmap.put("gold", wz.getAmount());
                    wmap.put("desc", wz.getSubName());
                    wmap.put("taskId", wz.getId());
                    if (null != wu && wu.size() > 0) {
                        wmap.put("status", 1);
                    } else {
                        wmap.put("status", 0);
                    }
                    list.add(wmap);
                }
            }


            data.put("itemlist", list);
            return ResponseUtil.ok(data);
        }
        // 高佣任务列表
        else if ("T03".equals(type)) {
            list3 = wangzhuanTaskService.selectCustomWangzhuanTaskList(userId, page, limit,null);

            if (null == wzu) {

            } else {
                // 当前用户试玩任务
                wangzhuanTask = wangzhuanTaskService.selectWangzhuanTaskById(wzu.getTaskId());
            }
            List list1 = new ArrayList();
            if (!StringUtils.isEmpty(wzu)) {
                String taskId = wzu.getTaskId();
                LocalDateTime uTime = wzu.getUpdateTime();
                LocalDateTime now = LocalDateTime.now();
                LocalDateTime cTime = wzu.getCreateTime();
                Duration duration = Duration.between(now, uTime);
                long c = duration.toMinutes();
                list1.add(now.toString());
                list1.add(uTime.toString());
                // 任务未过期
                if (c > 0) {

                    data.put("showdown", list1);
                    data.put("items", wangzhuanTask);
                    data.put("itemlist", list3);
                }
                // 任务过期
                else {
                    wzu.setTstatus(CommonConstant.TASK_STATUS_CANCEL);
                    wangzhuanUserTaskService.updateWangZTaskStauts(wzu, taskId, userId);
                    list3 = wangzhuanTaskService.selectCustomWangzhuanTaskList(userId, page, limit,null);
                    data.put("showdown", list1);
                    data.put("itemlist", list3);
                }


            } else {
                data.put("showdown", list1);
                data.put("items", wangzhuanTask);
                data.put("itemlist", list3);
            }
            String mobileTye = request.getHeader("User-Agent");
            if (RetrialUtils.examine(request, version)) {
                return ResponseUtil.ok(list);
            } else {
                return ResponseUtil.ok(data);
            }
        }


        return ResponseUtil.ok();


    }

    /**
     * 查询(网赚)领取个人任务记录列表
     */
    @GetMapping("/userList")
    public Object list1(@LoginUser String userId, @RequestParam(defaultValue = "1") Integer page,
                    @RequestParam(defaultValue = "10") Integer limit, @RequestParam(defaultValue = "id") String sort,
                    @Order @RequestParam(defaultValue = "desc") String order) {
        // if (userId == null) {
        // return ResponseUtil.unlogin();
        // }
        WangzhuanUserTask wangzhuanUserTask = new WangzhuanUserTask();
        wangzhuanUserTask.setUserId(userId);
        List<Map> list = wangzhuanUserTaskService.selectWzUserTaskBy(userId, page, limit);
        List<CustomWangZhuanTask> list1 = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            CustomWangZhuanTask customWangZhuanTask = new CustomWangZhuanTask();
            // 查询任务详情
            WangzhuanTask wangzhuanTask =
                            wangzhuanTaskService.selectWangzhuanTaskById((String) list.get(i).get("task_id"));
            if (null != wangzhuanTask) {
                customWangZhuanTask.setTaskname(wangzhuanTask.getTaskName());
                customWangZhuanTask.setImageUrl(wangzhuanTask.getImageUrl());
            }
            customWangZhuanTask.setReward((BigDecimal) list.get(i).get("amount"));
            Timestamp time = (Timestamp) list.get(i).get("create_time");
            LocalDateTime datetime = time.toLocalDateTime();
            customWangZhuanTask.setCreateTime(datetime);
            customWangZhuanTask.setRemarks((String) list.get(i).get("remarks"));
            switch ((String) list.get(i).get("tstatus")) {
                case "UST100":
                    customWangZhuanTask.setTstatus("进行中");
                    break;
                case "UST101":
                    customWangZhuanTask.setTstatus("已取消");
                    break;
                case "UST102":
                    customWangZhuanTask.setTstatus("审核中");
                    break;
                case "UST103":
                    customWangZhuanTask.setTstatus("审核不通过");
                    break;
                case "UST104":
                    customWangZhuanTask.setTstatus("审核通过");
                    break;
            }
            list1.add(customWangZhuanTask);
        }
        // WangzhuanUserTask list = wangzhuanUserTaskService.selectCustomWangzhuanUserTaskList(userId);
        WangzhuanTask wangzhuanTask = null;
        Map<String, Object> data = new HashMap<>();
        int total = list.size();
        // data.put("total",total);
        data.put("items", list1);
        logger.info("当前已领取任务-------" + list1);
        return ResponseUtil.ok(data);
    }



    /**
     * 新增保存(网赚)领取个人任务记录
     */
    @PostMapping("/add")
    public Object addSave(@LoginUser String userId, @RequestBody Map map) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        String taskId = (String) map.get("taskId");
        WangzhuanUserTask wangzhuanUserTask = new WangzhuanUserTask();
        wangzhuanUserTask.setTaskId(taskId);
        Object error = validate(wangzhuanUserTask);
        if (error != null) {
            return error;
        }
        // 查询任务详情
        WangzhuanTask wangzhuanTask = wangzhuanTaskService.selectWangzhuanTaskById(taskId);
        if (!(wangzhuanTask.getStockNumber() > 0)) {
            return ResponseUtil.serious();
        }
        // 查询用户已领取的任务
        List<WangzhuanUserTask> list = wangzhuanUserTaskService.selectUserTaskByUserId(userId);
        if (list.size() > 0) {
            // 已经领取任务不能再领
            WangzhuanUserTask wz = new WangzhuanUserTask();
            wz.setTstatus(CommonConstant.TASK_STATUS_CANCEL); // 取消上个任务
            int a = wangzhuanUserTaskService.updateWZTaskStauts(wz, userId);
            System.out.println("取消上个任务--" + wz.toString());
            if (a > 0) {
                wangzhuanUserTask.setId(GraphaiIdGenerator.nextId("WangzhuanUserTask"));
                wangzhuanUserTask.setUserId(userId);
                wangzhuanUserTask.setRewardType(CommonConstant.TASK_REWARD_GOLD); // todo 金币奖励
                wangzhuanUserTask.setReward(wangzhuanTask.getAmount());
                wangzhuanUserTask.setTstatus(CommonConstant.TASK_STATUS_PRE); // 进行中
                wangzhuanUserTask.setDeleted(false);
                wangzhuanUserTask.setDuration(wangzhuanTask.getDuration());

                //LocalDateTime dateTime = LocalDateTime.now().plusMinutes(wangzhuanTask.getDuration());
               // wangzhuanUserTask.setUpdateTime(dateTime);
                System.out.println("取消上个任务成功------" + wangzhuanUserTask.toString());
                wangzhuanUserTaskService.insertWangzhuanUserTask(wangzhuanUserTask);

            }
        } else {
            // 没有领取任务
            wangzhuanUserTask.setId(GraphaiIdGenerator.nextId("WangzhuanUserTask"));
            wangzhuanUserTask.setUserId(userId);
            wangzhuanUserTask.setRewardType(CommonConstant.TASK_REWARD_GOLD); // todo 金币奖励
            wangzhuanUserTask.setReward(wangzhuanTask.getAmount());
            wangzhuanUserTask.setTstatus(CommonConstant.TASK_STATUS_PRE); // 进行中
            wangzhuanUserTask.setDeleted(false);
            wangzhuanUserTask.setDuration(wangzhuanTask.getDuration());
            // 添加任务有效时间
            //LocalDateTime dateTime = LocalDateTime.now().plusMinutes(wangzhuanTask.getDuration());
            //wangzhuanUserTask.setUpdateTime(dateTime);
            System.out.println("领取任务成功------" + wangzhuanUserTask.toString());
            // 加入个人任务表
            wangzhuanUserTaskService.insertWangzhuanUserTask(wangzhuanUserTask);
        }
        // }
        // 任务数量减少
        WangzhuanTask task = new WangzhuanTask();
        task.setId(wangzhuanUserTask.getTaskId());
        task.setStockNumber(wangzhuanTask.getStockNumber() - 1);
        wangzhuanTaskService.updateWangzhuanTask(task);

        return ResponseUtil.ok();
    }

    /**
     * 审核个人任务
     */
    @GetMapping("/edit/{id}")
    @Transactional(rollbackFor = {Exception.class})
    public Object edit(@LoginUser String userId, @PathVariable("id") String id) throws Exception {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        WangzhuanUserTask wangzhuanUserTask = wangzhuanUserTaskService.selectWangzhuanUserTaskById(id);
        wangzhuanUserTask.setTstatus(CommonConstant.TASK_STATUS_AUDIT);
        Map paramsMap = new HashMap();
        paramsMap.put("dType", AccountStatus.DISTRIBUTION_TYPE_00); // 直接分润
        paramsMap.put("message", "任务奖励");
        paramsMap.put("capitalTrend", AccountStatus.CAPITAL_TREND_01);
        paramsMap.put("changeType", AccountStatus.CHANGE_TYPE_01);
        paramsMap.put("changeAccountType", AccountStatus.CHANGE_ACCOUNT_TYPE_2);
        if (null == wangzhuanUserTask) {
            return ResponseUtil.badArgumentValue();
        }
        if (!userId.equals(wangzhuanUserTask.getUserId())) {
            return ResponseUtil.unauthz();
        }
        if (wangzhuanUserTaskService.updateWangZhuanTaskStauts(wangzhuanUserTask, id) > 0) {
            BigDecimal reward = wangzhuanUserTask.getReward();
            if (reward.compareTo(new BigDecimal(0.00)) == 1) {
                AccountUtils.accountChange(userId, reward, CommonConstant.TASK_REWARD_GOLD, AccountStatus.BID_6,
                                paramsMap);
            }
            return ResponseUtil.ok();
        }

        return ResponseUtil.updatedDataFailed();


    }

    /**
     * 修改保存(网赚)领取个人任务记录 : 完成、取消
     */
    @PostMapping("/edit")
    public Object editSave(@LoginUser String userId, @RequestBody WangzhuanUserTask wangzhuanUserTask) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        // 判断任务主键id是否为空
        if (StringUtils.isEmpty(wangzhuanUserTask.getId())) {
            return ResponseUtil.badArgument();
        }
        // 判断个人任务 是否属于 登录用户
        WangzhuanUserTask task = wangzhuanUserTaskService.selectWangzhuanUserTaskById(wangzhuanUserTask.getId());
        if (null == task) {
            return ResponseUtil.badArgumentValue();
        }
        if (!userId.equals(task.getUserId())) {
            return ResponseUtil.unauthz();
        }

        if (!StringUtils.isEmpty(wangzhuanUserTask.getTstatus())) {
            if (wangzhuanUserTask.getTstatus().equals(CommonConstant.TASK_STATUS_COMPLETE)) {
                // 完成个人任务
                Object errorComplete = validateComplete(wangzhuanUserTask);
                if (errorComplete != null) {
                    return errorComplete;
                }
                LocalDateTime now = LocalDateTime.now();
                Duration duration = Duration.between(task.getCreateTime(), now);
               // wangzhuanUserTask.setDuration(Math.toIntExact(duration.toMillis() / 1000));
                wangzhuanUserTask.setUnit(CommonConstant.TASK_UNIT_SECOND);
            }
        }
        wangzhuanUserTaskService.updateWangzhuanUserTask(wangzhuanUserTask);

        return ResponseUtil.ok();
    }

    /**
     * 通过任务id 查询任务步骤
     */
    @GetMapping("/getTaskStepById/{taskId}")
    public Object getTaskStepById(@LoginUser String userId, @PathVariable("taskId") String taskId) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        WangzhuanTaskStep wangzhuanTaskStep = new WangzhuanTaskStep();
        wangzhuanTaskStep.setTaskId(taskId);
        wangzhuanTaskStep.setDeleted(false);
        List<WangzhuanTaskStepWithBLOBs> wangzhuanTaskStepList =
                        wangzhuanTaskStepService.selectWangzhuanTaskStepList(wangzhuanTaskStep);
        Map<String, Object> data = new HashMap<>();
        WangzhuanUserTask list = wangzhuanUserTaskService.selectCustomWangzhuanUserTaskList(userId);
        WangzhuanTask wangzhuanTask = null;
        List list1 = new ArrayList();
        List list2 = new ArrayList();
        if (!StringUtils.isEmpty(list)) {
            // String taskId = list.getTaskId();
            LocalDateTime uTime = list.getUpdateTime();
            LocalDateTime cTime = list.getCreateTime();
            LocalDateTime now = LocalDateTime.now();
            Duration duration = Duration.between(now, uTime);
            long c = duration.toMinutes();
            long b = duration.toMillis();
            list2.add(now.toString());
            list2.add(uTime.toString());
            if (c > 0) {
                data.put("showdown", list2);
                data.put("items", wangzhuanTaskStepList);
            } else {
                list.setTstatus(CommonConstant.TASK_STATUS_CANCEL);
                wangzhuanUserTaskService.updateWangZTaskStauts(list, taskId, userId);
                return ResponseUtil.fail(403, "任务已超时！");
            }


        }
        return ResponseUtil.ok(data);
    }


    /**
     * 放弃任务
     */
    @GetMapping("/giveupTask")
    public Object isGiveUpTask(@LoginUser String userId, @RequestParam("taskId") String taskId) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        WangzhuanUserTask wut = new WangzhuanUserTask();
        wut.setTstatus(CommonConstant.TASK_STATUS_CANCEL);
        int result = wangzhuanUserTaskService.updateWangZTaskStauts(wut, taskId, userId);
        if (result > 0) {
            return ResponseUtil.ok();
        }
        logger.info("放弃任务------" + result);
        return ResponseUtil.fail(403, "放弃任务失败!");
    }

    /**
     * 1.1 完成每日任务
     */
    @GetMapping("/completeEverydayTask")
    public Object completeEverydayTask(@LoginUser String userId, @RequestParam String taskId,
                                       @RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit,
                                       @RequestParam(defaultValue = "") String type) throws Exception {

        MallFootprint mallFootprint = new MallFootprint();
        mallFootprint.setAddTime(LocalDateTime.now());
        mallFootprint.setUserId(userId);
        mallFootprint.setDeleted(false);


//        List<WangzhuanTask> list2 = wangzhuanTaskService.selectCustomWangzhuanTaskListByType(userId, page, limit, type);
        WangzhuanUserTask wut = new WangzhuanUserTask();
        wut.setTstatus(CommonConstant.TASK_STATUS_AUDIT);
        //wut.setDuration(60);
        wut.setReward(new BigDecimal(100));
        wut.setRewardType(CommonConstant.TASK_REWARD_GOLD);
        wut.setUpdateTime(LocalDateTime.now());
        wut.setDeleted(false);
        wut.setId(GraphaiIdGenerator.nextId("WangzhuanUserTask"));
        wut.setUserId(userId);

        Map<String, Object> paramsMap1 = new HashMap();
        paramsMap1.put("dType", AccountStatus.DISTRIBUTION_TYPE_00); // 直接分润
        paramsMap1.put("capitalTrend", AccountStatus.CAPITAL_TREND_01);
        paramsMap1.put("changeType", AccountStatus.CHANGE_TYPE_01);
        paramsMap1.put("changeAccountType", AccountStatus.CHANGE_ACCOUNT_TYPE_2);
        // for(WangzhuanTask task : list2){
        switch (taskId) {
            case "284442424425366689":
                // 浏览专题
                WangzhuanTask wzTask = wangzhuanTaskService.selectWangzhuanTaskById(taskId);
                wut.setRemarks(wzTask.getTaskName());
                wut.setTaskId(wzTask.getId());
                paramsMap1.put("message", wzTask.getTaskName());
                mallFootprint.setTaskid(taskId);
                List<MallFootprint> list = mallFootprintService.findByUserTaskId(userId, taskId);
                if (list.size() == 0) {
                    mallFootprintService.add(mallFootprint);
                    if (wangzhuanUserTaskService.selectWzByUserId(taskId, userId).size() == 0) {
                        wangzhuanUserTaskService.insertWangzhuanUserTask(wut);
                        if (wzTask.getAmount().compareTo(new BigDecimal(0.00)) == 1) {
                            AccountUtils.accountChange(userId, wzTask.getAmount(), CommonConstant.TASK_REWARD_GOLD,
                                    AccountStatus.BID_1, paramsMap1);
                        }
                    }
                }
                break;
            case "284142355514255877":
                // 每日购物
                LocalDate begin = LocalDate.now();
                LocalDate end = LocalDate.now();
                List<Map> listo = wangzhuanTaskService.selectOrderGoods(userId, begin.toString(), end.toString(), "");
                if (listo.size() >= 3) {
                    WangzhuanTask wzTask1 = wangzhuanTaskService.selectWangzhuanTaskById(taskId);
                    wut.setRemarks(wzTask1.getTaskName());
                    wut.setTaskId(wzTask1.getId());
                    paramsMap1.put("message", wzTask1.getTaskName());
                    mallFootprint.setTaskid(taskId);
                    List<MallFootprint> listf = mallFootprintService.findByUserTaskId(userId, taskId);
                    if (listf.size() == 0) {
                        mallFootprintService.add(mallFootprint);
                        if (wangzhuanUserTaskService.selectWzByUserId(taskId, userId).size() == 0) {
                            wangzhuanUserTaskService.insertWangzhuanUserTask(wut);
                            if (wzTask1.getAmount().compareTo(new BigDecimal(0.00)) == 1) {
                                AccountUtils.accountChange(userId, wzTask1.getAmount(), CommonConstant.TASK_REWARD_GOLD,
                                        AccountStatus.BID_1, paramsMap1);
                            }
                        }
                    }
                    }
                    break;
                    // todo 外卖达人暂时不做
                    // case "284535365654435353" :
                    // wangzhuanUserTaskService.insertWangzhuanUserTask(wut);
                    // if(wzTask.getAmount().compareTo(new BigDecimal(0.00)) == 1){
                    // AccountUtils.accountChange(userId, wzTask.getAmount(),
                    // CommonConstant.TASK_REWARD_GOLD,AccountStatus.BID_1,paramsMap1);
                    // }
                    // break;
                    case "284534533113232324":
                        // 复制淘宝商品
                        WangzhuanTask wzTask2 = wangzhuanTaskService.selectWangzhuanTaskById(taskId);
                        wut.setRemarks(wzTask2.getTaskName());
                        wut.setTaskId(wzTask2.getId());
                        paramsMap1.put("message", wzTask2.getTaskName());
                        mallFootprint.setTaskid(taskId);
                        List<MallFootprint> listfz = mallFootprintService.findByUserTaskId(userId, taskId);
                        if (listfz.size() == 0) {
                            mallFootprintService.add(mallFootprint);
                            if (wangzhuanUserTaskService.selectWzByUserId(taskId, userId).size() == 0) {
                                wangzhuanUserTaskService.insertWangzhuanUserTask(wut);
                                if (wzTask2.getAmount().compareTo(new BigDecimal(0.00)) == 1) {
                                    AccountUtils.accountChange(userId, wzTask2.getAmount(), CommonConstant.TASK_REWARD_GOLD,
                                            AccountStatus.BID_1, paramsMap1);
                                }
                            }
                        }
                        break;
                    case "284132343234545352":
                        // 边看边买
                        LocalDate begin1 = LocalDate.now();
                        LocalDate end1 = LocalDate.now();
                        List<Map> list1 =
                                wangzhuanTaskService.selectOrderGoods(userId, begin1.toString(), end1.toString(), "1");
                        if (list1.size() >= 3) {
                            WangzhuanTask wzTask3 = wangzhuanTaskService.selectWangzhuanTaskById(taskId);
                            wut.setRemarks(wzTask3.getTaskName());
                            wut.setTaskId(wzTask3.getId());
                            paramsMap1.put("message", wzTask3.getTaskName());
                            mallFootprint.setTaskid(taskId);
                            List<MallFootprint> listb = mallFootprintService.findByUserTaskId(userId, taskId);
                            if (listb.size() == 0) {
                                mallFootprintService.add(mallFootprint);
                                if (wangzhuanUserTaskService.selectWzByUserId(taskId, userId).size() == 0) {
                                    wangzhuanUserTaskService.insertWangzhuanUserTask(wut);
                                    if (wzTask3.getAmount().compareTo(new BigDecimal(0.00)) == 1) {
                                        AccountUtils.accountChange(userId, wzTask3.getAmount(), CommonConstant.TASK_REWARD_GOLD,
                                                AccountStatus.BID_1, paramsMap1);
                                    }
                                }
                            }
                        }
                            break;
                            case "284252544225555252":
                                // 领取优惠券
                                WangzhuanTask wzTask4 = wangzhuanTaskService.selectWangzhuanTaskById(taskId);
                                wut.setRemarks(wzTask4.getTaskName());
                                wut.setTaskId(wzTask4.getId());
                                paramsMap1.put("message", wzTask4.getTaskName());
                                mallFootprint.setTaskid(taskId);
                                List<MallFootprint> listl = mallFootprintService.findByUserTaskId(userId, taskId);
                                if (listl.size() == 0) {
                                    mallFootprintService.add(mallFootprint);
                                    if (wangzhuanUserTaskService.selectWzByUserId(taskId, userId).size() == 0) {
                                        wangzhuanUserTaskService.insertWangzhuanUserTask(wut);
                                        if (wzTask4.getAmount().compareTo(new BigDecimal(0.00)) == 1) {
                                            AccountUtils.accountChange(userId, wzTask4.getAmount(), CommonConstant.TASK_REWARD_GOLD,
                                                    AccountStatus.BID_1, paramsMap1);
                                        }
                                    }
                                }
                                break;
                            case "284572425565982256":
                                // 浏览商品详情
                                List<MallFootprint> list3 = mallFootprintService.findByUserId(userId);
                                if (list3.size() == 1) {
                                    WangzhuanTask wzTask5 = wangzhuanTaskService.selectWangzhuanTaskById(taskId);
                                    wut.setRemarks(wzTask5.getTaskName());
                                    wut.setTaskId(wzTask5.getId());
                                    paramsMap1.put("message", wzTask5.getTaskName());
                                    List<WangzhuanUserTask> wangzhuanUserTasks = wangzhuanUserTaskService.selectWzByUserId(taskId, userId);
                                    if (wangzhuanUserTasks.size() == 0) {
                                        wangzhuanUserTaskService.insertWangzhuanUserTask(wut);
                                        if (wzTask5.getAmount().compareTo(new BigDecimal(0.00)) == 1) {
                                            AccountUtils.accountChange(userId, wzTask5.getAmount(), CommonConstant.TASK_REWARD_GOLD,
                                                    AccountStatus.BID_1, paramsMap1);
                                        }
                                    }

                                }
                                break;
                        }



        return ResponseUtil.ok();


    }
    /**
     * 1.1 完成任务
     */
    @PostMapping("/completeTask")
    public Object iscompleteTask(@LoginUser String userId, @RequestBody WangzhuanTaskStep wangzhuanTaskStep)
                    throws Exception {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        Object errorComplete = validateStep(wangzhuanTaskStep);
        if (errorComplete != null) {
            return errorComplete;
        }
        // 每日任务
        if ("T09".equals(wangzhuanTaskStep.getSetpType())) {
            List<WangzhuanUserTask> wu = wangzhuanTaskService.selectCustomWzUserTaskListByType(userId,
                            wangzhuanTaskStep.getTaskId());
            WangzhuanTask wzTask = wangzhuanTaskService.selectWangzhuanTaskById(wangzhuanTaskStep.getTaskId());
            WangzhuanUserTask wut = new WangzhuanUserTask();
            Map<String, Object> map = new HashMap<>();

            List<MallFootprint> list = mallFootprintService.findByUserId(userId);
            if (null != list && list.size() > 0) {
                if (wu.size() > 0) {
                    map.put("status", 1);
                } else if (wu.size() == 0) {
                    map.put("status", 1);
                    map.put("gold", 100);
                    wut.setUserId(userId);
                    wut.setRemarks(wzTask.getTaskName());
                    wut.setTstatus(CommonConstant.TASK_STATUS_AUDIT);
                    wut.setTaskId(wangzhuanTaskStep.getTaskId());
                    //wut.setDuration(60);
                    wut.setReward(new BigDecimal(100));
                    wut.setRewardType(CommonConstant.TASK_REWARD_GOLD);
                    wut.setUpdateTime(LocalDateTime.now());
                    wut.setDeleted(false);
                    wut.setId(GraphaiIdGenerator.nextId("WangzhuanUserTask"));
                    wangzhuanUserTaskService.insertWangzhuanUserTask(wut);


                    FcAccount fc = accountService.getUserAccount(userId);
                    if (null != fc) {
                        // todo 暂时写死后面从业务形态表获取
                        BigDecimal gold = fc.getGold().add(new BigDecimal(100));
                        fc.setGold(gold);
                    } else {
                        return ResponseUtil.fail(403, "未查到账户！");
                    }

                    // accountService.updateAccount(fc);
                    Map<String, Object> paramsMap1 = new HashMap();
                    paramsMap1.put("dType", AccountStatus.DISTRIBUTION_TYPE_00); // 直接分润
                    paramsMap1.put("message", wzTask.getTaskName());
                    paramsMap1.put("capitalTrend", AccountStatus.CAPITAL_TREND_01);
                    paramsMap1.put("changeType", AccountStatus.CHANGE_TYPE_01);
                    paramsMap1.put("changeAccountType", AccountStatus.CHANGE_ACCOUNT_TYPE_2);
                    if (wzTask.getAmount().compareTo(new BigDecimal(0.00)) == 1) {
                        AccountUtils.accountChange(userId, wzTask.getAmount(), CommonConstant.TASK_REWARD_GOLD,
                                        AccountStatus.BID_1, paramsMap1);
                    }

                }

            } else {
                map.put("status", 0);
            }

            return ResponseUtil.ok(map);
        }
        // 高佣任务
        else if ("T03".equals(wangzhuanTaskStep.getSetpType())) {
            String answer = wangzhuanTaskStep.getAnswer();
            List<WangzhuanTaskStepWithBLOBs> list =
                            wangzhuanTaskStepService.selectWangzhuanTaskStepList(wangzhuanTaskStep);
            String ans = list.get(1).getAnswer();

            if (!StringUtils.isEmpty(answer) && answer.equals(ans)) {
                WangzhuanUserTask wut = new WangzhuanUserTask();
                wut.setTstatus(CommonConstant.TASK_STATUS_COMPLETE);
                int result = wangzhuanUserTaskService.updateWangZTaskStauts(wut, wangzhuanTaskStep.getTaskId(), userId);
                if (result > 0) {
                    return ResponseUtil.ok();
                }
            }
            return ResponseUtil.fail(403, "答案错误!");
        }

        return ResponseUtil.ok();

    }

    // /**
    // * 2 任务。下载app 改成 --> 2 任务。申请信用卡接口
    // *
    // */
    // @PostMapping("/downLoadApp")
    // public Object applyAppTask(@LoginUser String userId,
    // @RequestBody JSONObject user){
    // if( null == userId){
    // return ResponseUtil.unlogin();
    // }
    // String name =(String) user.get("name");//名称
    // String phone = (String) user.get("phone");//电话号码
    // String productId =(String) user.get("productId");//银行产品id
    // String type = (String) user.get("type");//产品类型 bank 信用卡 loan 贷款 Insurance 保险
    // Map<String,Object> map = null;
    // if("bank".equals(type)){
    // map = YRDUtils.doBankList(CommonConstant.BANKAPPLY,name,phone,productId);
    // }else if("loan".equals(type)){
    // // todo 扩展接口 贷款产品
    // // map = YRDUtils.doBankList(CommonConstant.LOANAPPLY,name,phone,productId); //产品已下架
    // }else if("Insurance".equals(type)){
    // // todo 扩展接口 保险产品
    // map = YRDUtils.doBankList(CommonConstant.INSURANCEAPPLY,name,phone,productId);
    // }
    //
    // Map<String,Object> datamap = new HashMap<>();
    // Map bank= new HashMap();
    // String bankurl="";
    // for(String str:map.keySet()){
    // String code = (String)map.get("code");
    // if("200".equals(code)){
    // if("data".equals(str)){
    // bank = (Map)map.get(str);
    // }
    // }else{
    // bankurl="";
    // }
    // }
    // if( null != bank && bank.size()>0){
    // for(Object str1:bank.keySet()){
    // if("bank_url".equals(str1)){
    // bankurl=(String)bank.get(str1);
    // }
    // }
    // }
    //
    // datamap.put("bankurl",bankurl);
    //
    // return ResponseUtil.ok(datamap);
    // }



    /**
     * 2 任务。申请信用卡接口
     *
     */
    @PostMapping("/applyCard")
    public Object applyCreditCard(@LoginUser String userId, @RequestBody JSONObject user) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        String name = (String) user.get("name");// 名称
        String phone = (String) user.get("phone");// 电话号码
        String productId = (String) user.get("productId");// 银行产品id
        String type = (String) user.get("type");// 产品类型 bank 信用卡 loan 贷款 Insurance 保险
        Map<String, Object> map = null;
        if ("bank".equals(type)) {
            map = YRDUtils.doBankList(CommonConstant.BANKAPPLY, name, phone, productId);
        } else if ("loan".equals(type)) {
            // todo 扩展接口 贷款产品
            // map = YRDUtils.doBankList(CommonConstant.LOANAPPLY,name,phone,productId); //产品已下架
        } else if ("Insurance".equals(type)) {
            // todo 扩展接口 保险产品
            map = YRDUtils.doBankList(CommonConstant.INSURANCEAPPLY, name, phone, productId);
        }

        Map<String, Object> datamap = new HashMap<>();
        Map bank = new HashMap();
        String bankurl = "";
        for (String str : map.keySet()) {
            String code = (String) map.get("code");
            if ("200".equals(code)) {
                if ("data".equals(str)) {
                    bank = (Map) map.get(str);
                }
            } else {
                bankurl = "";
            }
        }
        if (null != bank && bank.size() > 0) {
            for (Object str1 : bank.keySet()) {
                if ("bank_url".equals(str1)) {
                    bankurl = (String) bank.get(str1);
                }
            }
        }

        List<WangzhuanTaskStep> list = wangzhuanTaskStepService.seleteWangzhuanTaskStepById(productId);
        WangzhuanTaskStep bankurl1 = list.get(0);
        if (!StringUtils.isEmpty(bankurl)) {
            bankurl1.setIosDownloadUrl(bankurl);
            bankurl1.setAndroidDownloadUrl(bankurl);

        }
        datamap.put("step", bankurl1);


        return ResponseUtil.ok(datamap);
    }



    /**
     * 2.1任务。申请信用卡接口回调
     *
     */
    @PostMapping("/Callback")
    public Object applyCreditCardCallback(String order_list) throws IOException {
        logger.info("user-------" + JacksonUtils.bean2Jsn(order_list));



        JSONArray jsonlist = JSONArray.fromObject(order_list);
        JSONObject json = (JSONObject) jsonlist.get(0);

        String order_no = (String) json.get("order_no");
        Integer product_id = (Integer) json.get("product_id");
        String product_type = (String) json.get("product_type");
        Integer amount = (Integer) json.get("amount");
        Integer loan_amount = (Integer) json.get("loan_amount");
        String status = (String) json.get("status");

        WangzhuanCallbackCreditcard wan = new WangzhuanCallbackCreditcard();
        if (null != amount) {
            wan.setAmount(new BigDecimal(amount));
        } else {
            wan.setAmount(new BigDecimal("0.00"));
        }

        wan.setIsDelete("0");
        wan.setOrderNo(order_no);
        wan.setProductId(product_id + "");
        wan.setProductType(product_type);
        if (!StringUtils.isEmpty(status)) {
            wan.setStatus(Integer.parseInt(status));
        } else {
            wan.setStatus(0);
        }

        if (!StringUtils.isEmpty(loan_amount)) {
            wan.setLoanAmount(loan_amount + "");
        }
        wangzhuanUserTaskService.insertWangzhuancreditCard(wan);


        return ResponseUtil.ok();
    }

    /**
     * 2.2 信用卡任务提交
     */
    @PostMapping("/subCrad")
    public Object subCard(@LoginUser String userId, @RequestBody String data) {
        if (null == userId) {
            return ResponseUtil.unlogin();
        }
        String phone = JacksonUtil.parseString(data, "phone");// 电话
        String checkCode = JacksonUtil.parseString(data, "code");// 验证码
        String url = JacksonUtil.parseString(data, "url");// 图片路径
        String taskId = JacksonUtil.parseString(data, "taskId");// 任务id
        String smsType = JacksonUtil.parseString(data, "smsType");// 验证码类型

        WangzhuanUserTask wzt = wangzhuanUserTaskService.selectCustomWangzhuanUserTaskList(userId);
        MallPhoneValidation mallPhoneValidation = phoneService.getPhoneValidationByCode(phone, checkCode, smsType);
        if (null == mallPhoneValidation) {
            return ResponseUtil.fail(AUTH_CAPTCHA_UNMATCH, "验证码错误");
        } else {
            wzt.setTstatus(CommonConstant.TASK_STATUS_COMPLETE);// 已提交任务 审核中
            wzt.setScreenshotImages(url);
            wzt.setPhone(phone);
            wangzhuanUserTaskService.updateWangZTaskStauts(wzt, taskId, userId);
        }

        return ResponseUtil.ok();
    }

    /**
     * 任务中心提现额度
     */
    public Object withdrawalMoney(@LoginUser String userId) {


        FcAccount fc = accountService.getUserAccount(userId);
        if (null != fc) {
            BigDecimal gold = fc.getGold(); // 账户金币
        }
        return 1;

    }

    /**
     * 任务列表
     */
    @RequestMapping("/taskList")
    public Object getTaskList(@LoginUser String userId, String type) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }

        List<WangzhuanTask> list = wangzhuanTaskService.selectCustomTaskList(userId, 0, 1, type);


        return 1;
    }


    /**
     * 校验数据(完成任务)
     */
    private Object validateStep(WangzhuanTaskStep wangzhuanTaskStep) {
        if (StringUtils.isEmpty(wangzhuanTaskStep.getTaskId())) {
            return ResponseUtil.badArgument();
        }
        // if (StringUtils.isEmpty(wangzhuanTaskStep.getAnswer())) {
        // return ResponseUtil.badArgument();
        // }
        if (StringUtils.isEmpty(wangzhuanTaskStep.getSetpType())) {
            return ResponseUtil.badArgument();
        }
        return null;
    }

    /**
     * 校验数据(添加)
     */
    private Object validate(WangzhuanUserTask wangzhuanUserTask) {
        if (StringUtils.isEmpty(wangzhuanUserTask.getTaskId())) {
            return ResponseUtil.badArgument();
        }
        return null;
    }

    /**
     * 校验数据(完成)
     */
    private Object validateComplete(WangzhuanUserTask wangzhuanUserTask) {
        // validate(wangzhuanUserTask);
        // 截图图片
        if (StringUtils.isEmpty(wangzhuanUserTask.getScreenshotImages())) {
            return ResponseUtil.badArgument();
        }
        // 任务验证手机号码
        if (StringUtils.isEmpty(wangzhuanUserTask.getPhone())) {
            return ResponseUtil.badArgument();
        }
        // 本次输入验证码
        if (StringUtils.isEmpty(wangzhuanUserTask.getCaptcha())) {
            return ResponseUtil.badArgument();
        }
        return null;
    }


    /**
     * 完成新人任务IOS授权
     */
    @GetMapping("/completeNewTask")
    public Object newTask(@LoginUser String userId, @RequestParam String iosuser, @RequestParam String type) {
        MallUser mallUser = mallUserService.findById(userId);
        if (mallUser == null) {
            return ResponseUtil.badArgumentValue("用户不存在！");
        }
        List<String> areas = new ArrayList<String>();
        String county = null;
        if (!StringUtils.isEmpty(type)) {
            // ios授权
            if ("1".equals(type)) {
                mallUser.setIosUserId(iosuser);
            }
            // 地理位置授权
            else if ("2".equals(type)) {
                String[] str = iosuser.split(",");
                if (str.length == 2) {
                    mallUser.setProvince(str[0]);
                    mallUser.setCity(str[1]);
                    areas.add(str[0]);
                    areas.add(str[1]);
                } else if (str.length > 2) {
                    mallUser.setProvince(str[0]);
                    mallUser.setCity(str[1]);
                    mallUser.setCounty(str[2]);
                    areas.add(str[0]);
                    areas.add(str[1]);
                    county = str[2];
                }
                List<MallRegion> regions = mallRegionService.queryByName(areas);
                MallRegion area = null;
                if (CollectionUtils.isNotEmpty(regions)) {
                    mallUser.setProvinceCode(regions.get(0).getCode());
                    if (regions.size() > 1) {
                        mallUser.setCityCode(regions.get(1).getCode());

                        if (!Objects.equals(null, county)) {
                            area = mallRegionService.queryByNameAndPid(county, regions.get(1).getId());

                            if (!Objects.equals(null, area)) {
                                mallUser.setAreaCode(area.getCode());
                            }
                        }

                    }



                }



            }
        }
        if (mallUserService.updateById(mallUser) > 0) {
            return ResponseUtil.ok();
        } else {
            return ResponseUtil.fail(403, "授权失败！");
        }

    }

    /**
     * 查询新人任务(0:未完成 1:已完成)
     */
    @GetMapping("/newTask")
    public Object newTask(@LoginUser String userId) throws Exception {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }

        String isNew = "1";
        String isTaobao = "0";
        String isFirstOrderByFans = "0";
        String isFirstOrder = "0";
        String isWx = "0";
        String isIOS = "0";
        String isPe = "0";
        MallUser mallUser = mallUserService.findById(userId);
        if (mallUser == null) {
            return ResponseUtil.badArgumentValue("用户不存在！");
        }
        // 淘宝授权
        if (!StringUtils.isEmpty(mallUser.getTbkRelationId())) {
            isTaobao = "1";
            String str = "淘宝授权";
            List<FcAccountRechargeBill> list = fcAccountRechargeBillService.listFcAccountBillListUserId(userId, str);
            if (list.size() == 0) {
                Map<String, Object> paramsMap1 = new HashMap();
                paramsMap1.put("dType", AccountStatus.DISTRIBUTION_TYPE_00); // 直接分润
                paramsMap1.put("message", "淘宝授权奖励");
                paramsMap1.put("capitalTrend", AccountStatus.CAPITAL_TREND_01);
                paramsMap1.put("changeType", AccountStatus.CHANGE_TYPE_01);
                paramsMap1.put("changeAccountType", AccountStatus.CHANGE_ACCOUNT_TYPE_2);
                AccountUtils.accountChange(userId, new BigDecimal(300), CommonConstant.TASK_REWARD_GOLD,
                                AccountStatus.BID_6, paramsMap1);
                AccountUtils.accountChange(userId, new BigDecimal(10), CommonConstant.TASK_REWARD_SCORE,
                                AccountStatus.BID_6, paramsMap1);
            }

        } else {
            String str = "淘宝授权";
            List<FcAccountRechargeBill> list = fcAccountRechargeBillService.listFcAccountBillListUserId(userId, str);
            if (list.size() > 0) {
                isTaobao = "1";
            }
        }
        // IOS授权
        if (!StringUtils.isEmpty(mallUser.getIosUserId())) {
            isIOS = "1";
            String str = "Apple授权";
            List<FcAccountRechargeBill> list = fcAccountRechargeBillService.listFcAccountBillListUserId(userId, str);
            if (list.size() == 0) {
                Map<String, Object> paramsMap1 = new HashMap();
                paramsMap1.put("dType", AccountStatus.DISTRIBUTION_TYPE_00); // 直接分润
                paramsMap1.put("message", "Apple授权奖励");
                paramsMap1.put("capitalTrend", AccountStatus.CAPITAL_TREND_01);
                paramsMap1.put("changeType", AccountStatus.CHANGE_TYPE_01);
                paramsMap1.put("changeAccountType", AccountStatus.CHANGE_ACCOUNT_TYPE_2);
                AccountUtils.accountChange(userId, new BigDecimal(200), CommonConstant.TASK_REWARD_GOLD,
                                AccountStatus.BID_6, paramsMap1);
                AccountUtils.accountChange(userId, new BigDecimal(10), CommonConstant.TASK_REWARD_SCORE,
                        AccountStatus.BID_6, paramsMap1);
            }

        }

        // 绑定微信
        if (!StringUtils.isEmpty(mallUser.getWeixinOpenid())) {
            isWx = "1";
            String str = "绑定微信";
            List<FcAccountRechargeBill> list = fcAccountRechargeBillService.listFcAccountBillListUserId(userId, str);
            if (list.size() < 1) {
                Map<String, Object> paramsMap1 = new HashMap();
                paramsMap1.put("dType", AccountStatus.DISTRIBUTION_TYPE_00); // 直接分润
                paramsMap1.put("message", "绑定微信奖励");
                paramsMap1.put("capitalTrend", AccountStatus.CAPITAL_TREND_01);
                paramsMap1.put("changeType", AccountStatus.CHANGE_TYPE_01);
                paramsMap1.put("changeAccountType", AccountStatus.CHANGE_ACCOUNT_TYPE_2);
                AccountUtils.accountChange(userId, new BigDecimal(200), CommonConstant.TASK_REWARD_GOLD,
                                AccountStatus.BID_6, paramsMap1);
                AccountUtils.accountChange(userId, new BigDecimal(10), CommonConstant.TASK_REWARD_SCORE,
                        AccountStatus.BID_6, paramsMap1);
            }
        } else {
            String str = "绑定微信";
            List<FcAccountRechargeBill> list = fcAccountRechargeBillService.listFcAccountBillListUserId(userId, str);
            if (list.size() > 0) {
                isWx = "1";
            }
        }
        // 粉丝首单
        List<String> userIds = mallUserService.getFansIdByUserId(userId);
        if (userIds.size() > 0) {
            List<MallOrder> fanMallOrderList = mallOrderService.findByUserIds(userIds);
            if (fanMallOrderList.size() > 0) {
                isFirstOrderByFans = "1";
                String str = "粉丝首单";
                List<FcAccountRechargeBill> list =
                                fcAccountRechargeBillService.listFcAccountBillListUserId(userId, str);
                if (list.size() < 1) {

                    Map<String, Object> paramsMap1 = new HashMap();
                    paramsMap1.put("dType", AccountStatus.DISTRIBUTION_TYPE_00); // 直接分润
                    paramsMap1.put("message", "粉丝首单奖励");
                    paramsMap1.put("capitalTrend", AccountStatus.CAPITAL_TREND_01);
                    paramsMap1.put("changeType", AccountStatus.CHANGE_TYPE_01);
                    paramsMap1.put("changeAccountType", AccountStatus.CHANGE_ACCOUNT_TYPE_2);
                    AccountUtils.accountChange(userId, new BigDecimal(300), CommonConstant.TASK_REWARD_GOLD,
                                    AccountStatus.BID_6, paramsMap1);
                }
            }
        }
        // 首次出单
        MallOrder mallOrder = new MallOrder();
        mallOrder.setUserId(userId);
        List<MallOrder> mallOrderList = mallOrderService.findByCondition(mallOrder);
        if (mallOrderList.size() > 0) {
            isFirstOrder = "1";
            String strName = "首次出单奖励";
            List<FcAccountRechargeBill> list =
                            fcAccountRechargeBillService.listFcAccountBillListUserId(userId, strName);
            if (list.size() < 1) {
                Map<String, Object> paramsMap1 = new HashMap();
                paramsMap1.put("dType", AccountStatus.DISTRIBUTION_TYPE_00); // 直接分润
                paramsMap1.put("message", "首次出单奖励");
                paramsMap1.put("capitalTrend", AccountStatus.CAPITAL_TREND_01);
                paramsMap1.put("changeType", AccountStatus.CHANGE_TYPE_01);
                paramsMap1.put("changeAccountType", AccountStatus.CHANGE_ACCOUNT_TYPE_2);
                AccountUtils.accountChange(userId, new BigDecimal(500), CommonConstant.TASK_REWARD_GOLD,
                                AccountStatus.BID_6, paramsMap1);
            }
        }

        // 绑定地理位置
        if (!StringUtils.isEmpty(mallUser.getProvince()) && !StringUtils.isEmpty(mallUser.getCity())
                        && !StringUtils.isEmpty(mallUser.getCounty())) {
            isPe = "1";
            String str = "绑定地理位置";
            List<FcAccountRechargeBill> list = fcAccountRechargeBillService.listFcAccountBillListUserId(userId, str);
            synchronized (lock2) {
                if (list.size() == 0) {
                    Map<String, Object> paramsMap1 = new HashMap();
                    paramsMap1.put("dType", AccountStatus.DISTRIBUTION_TYPE_00); // 直接分润
                    paramsMap1.put("message", "绑定地理位置奖励");
                    paramsMap1.put("capitalTrend", AccountStatus.CAPITAL_TREND_01);
                    paramsMap1.put("changeType", AccountStatus.CHANGE_TYPE_01);
                    paramsMap1.put("changeAccountType", AccountStatus.CHANGE_ACCOUNT_TYPE_2);
                    AccountUtils.accountChange(userId, new BigDecimal(100), CommonConstant.TASK_REWARD_GOLD,
                                    AccountStatus.BID_6, paramsMap1);
                    AccountUtils.accountChange(userId, new BigDecimal(5), CommonConstant.TASK_REWARD_SCORE,
                            AccountStatus.BID_6, paramsMap1);
                }
            }
        }


        // 新人注册
        String strNew = "新人注册奖励";
        List<FcAccountRechargeBill> list1 = fcAccountRechargeBillService.listFcAccountBillListUserId(userId, strNew);
        if (list1.size() < 1) {
            Map<String, Object> paramsMap1 = new HashMap();
            paramsMap1.put("dType", AccountStatus.DISTRIBUTION_TYPE_00); // 直接分润
            paramsMap1.put("message", "新人注册奖励");
            paramsMap1.put("capitalTrend", AccountStatus.CAPITAL_TREND_01);
            paramsMap1.put("changeType", AccountStatus.CHANGE_TYPE_01);
            paramsMap1.put("changeAccountType", AccountStatus.CHANGE_ACCOUNT_TYPE_2);
            AccountUtils.accountChange(userId, new BigDecimal(200), CommonConstant.TASK_REWARD_GOLD,
                            AccountStatus.BID_6, paramsMap1);
        }


        // 新人注册
        Map<String, Object> isNewMap = new HashMap<>();
        isNewMap.put("icon", "https://gxfacepay.oss-cn-shenzhen.aliyuncs.com/hummingbird/level/1.png");
        isNewMap.put("name", "新人注册");
        isNewMap.put("gold", "200");
        isNewMap.put("desc", "在APP内完成绑定注册");
        isNewMap.put("status", isNew);

        // 淘宝授权
        Map<String, Object> isTaobaoMap = new HashMap<>();
        isTaobaoMap.put("icon", "https://gxfacepay.oss-cn-shenzhen.aliyuncs.com/hummingbird/level/2.png");
        isTaobaoMap.put("name", "淘宝授权");
        isTaobaoMap.put("gold", "300");
        isTaobaoMap.put("desc", "成功完成淘宝授权");
        isTaobaoMap.put("status", isTaobao);
        // 粉丝首单
        Map<String, Object> isFirstOrderByFansMap = new HashMap<>();
        isFirstOrderByFansMap.put("icon", "https://gxfacepay.oss-cn-shenzhen.aliyuncs.com/hummingbird/level/3.png");
        isFirstOrderByFansMap.put("name", "粉丝首单");
        isFirstOrderByFansMap.put("gold", "100");
        isFirstOrderByFansMap.put("desc", "完成查询粉丝首次出单");
        isFirstOrderByFansMap.put("status", isFirstOrderByFans);
        // 首次出单
        Map<String, Object> isFirstOrderMap = new HashMap<>();
        isFirstOrderMap.put("icon", "https://gxfacepay.oss-cn-shenzhen.aliyuncs.com/hummingbird/level/4.png");
        isFirstOrderMap.put("name", "首次出单");
        isFirstOrderMap.put("gold", "500");
        isFirstOrderMap.put("desc", "完成首次出单");
        isFirstOrderMap.put("status", isFirstOrder);

        // //绑定IOS
        // Map<String,Object> isIOSMap = new HashMap<>();
        // isIOSMap.put("icon","http://gxfacepay.oss-cn-shenzhen.aliyuncs.com/hummingbird/mipmap/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20200703163554.png");
        // isIOSMap.put("name","Apple授权");
        // isIOSMap.put("gold","200");
        // isIOSMap.put("desc","完成Apple授权登录");
        // isIOSMap.put("status",isIOS);

        // 绑定微信
        Map<String, Object> isWxMap = new HashMap<>();
        isWxMap.put("icon", "https://gxfacepay.oss-cn-shenzhen.aliyuncs.com/hummingbird/level/5.png");
        isWxMap.put("name", "绑定微信");
        isWxMap.put("gold", "200");
        isWxMap.put("desc", "完成微信号填写");
        isWxMap.put("status", isWx);

        // 地理位置授权
        Map<String, Object> isPlaceEmpower = new HashMap<>();
        isPlaceEmpower.put("icon",
                        "http://gxfacepay.oss-cn-shenzhen.aliyuncs.com/hummingbird/mipmap/%E4%BD%8D%E7%BD%AE_20200709155627.png");
        isPlaceEmpower.put("name", "地理位置授权");
        isPlaceEmpower.put("gold", "100");
        isPlaceEmpower.put("desc", "完成地理位置授权");
        isPlaceEmpower.put("status", isPe);

        List<Map<String, Object>> list = new ArrayList<>();

        list.add(isNewMap);
        list.add(isTaobaoMap);
        list.add(isFirstOrderByFansMap);
        list.add(isFirstOrderMap);
        // list.add(isIOSMap);
        list.add(isWxMap);
        list.add(isPlaceEmpower);

        return ResponseUtil.ok(list);
    }

    /**
     * 新版任务中心
     */
    @GetMapping("/new/taskList")
    public Object getNewTaskList(@LoginUser String userId, @RequestParam(defaultValue = "") String version,
                    @RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit,
                    @RequestParam(defaultValue = "id") String sort,
                    @Order @RequestParam(defaultValue = "desc") String order, HttpServletRequest request)
                    throws IOException {
        // if(userId == null){
        // return ResponseUtil.unlogin();
        // }

        Map<String, Object> dataMap = new HashMap<>();

        // 调用开放平台数据
        String url = platFormServerproperty.getUrl(MallConstant.TAOBAO);
        Map<String, Object> mappMap = new HashMap<String, Object>();
        mappMap.put("plateform", "pub");
        mappMap.put("categoryTask", "categoryTask");
        Header[] headers = new BasicHeader[] {new BasicHeader("method", "pub.task.category"),
                new BasicHeader("version", "v1.0")};

        String result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);

        // 调用开放平台数据
        String url1 = platFormServerproperty.getUrl(MallConstant.TAOBAO);
        Map<String, Object> mappMap1 = new HashMap<String, Object>();
        mappMap1.put("plateform", "pub");
        mappMap1.put("categoryTask", "categoryTask_gs");
        Header[] headers1 = new BasicHeader[] {new BasicHeader("method", "pub.task.category"),
                new BasicHeader("version", "v1.0")};

        String result1 = HttpRequestUtils.post(url1, JacksonUtils.bean2Jsn(mappMap1), headers1);

        // JSONObject object = JSONObject.fromObject(result);
        String mobileTye = request.getHeader("User-Agent");
        // if (object.getString("errno").equals("0")) { JSON.parseObject(result, List.class);

        List<MallCategory> kmMallCategoryList = JSON.parseArray(result, MallCategory.class);
        List<MallCategory> kmMallCategoryList1 = JSON.parseArray(result1, MallCategory.class);
        List<MallCategory> list = new ArrayList<>();
        List<MallCategory> listA = new ArrayList<>();
        // ios过审

        if (RetrialUtils.examine(request, version)) {
            for (MallCategory game : kmMallCategoryList) {
                if ("L3".equals(game.getLevel()) || "L4".equals(game.getLevel())) {


                } else {
                    if ("在线游戏".equals(game.getName())) {
                        for (MallCategory game1 : kmMallCategoryList1) {
                            if ("categoryTask_gs".equals(game1.getPositionCode()) && "1".equals(game1.getPid())) {
                                game.setName(game1.getName());// 在线玩游戏换成早起打卡
                                game.setLinkUrl(game1.getLinkUrl());
                                game.setIconUrl(game1.getIconUrl());
                                game.setChainChange(game1.getChainChange());
                            }
                        }

                    } else if ("试玩赚钱".equals(game.getName())) {
                        for (MallCategory game1 : kmMallCategoryList1) {
                            if ("categoryTask_gs".equals(game1.getPositionCode()) && "2".equals(game1.getPid())) {
                                game.setName(game1.getName());// 在线玩游戏换成运动健康
                                game.setLinkUrl(game1.getLinkUrl());
                                game.setIconUrl(game1.getIconUrl());
                                game.setChainChange(game1.getChainChange());
                            }
                        }
                    }

                    list.add(game);
                }
            }
            dataMap.put("item", list);
        } else if (RetrialUtils.examineAndroid(mobileTye, version)) {
            for (MallCategory game : kmMallCategoryList) {
                if ("在线游戏".equals(game.getName())) {

                } else if ("试玩赚钱".equals(game.getName())) {

                } else {
                    listA.add(game);
                }
            }
            dataMap.put("item", listA);
        } else {
            dataMap.put("item", kmMallCategoryList);
        }

        return ResponseUtil.ok(dataMap);
    }

    /**
     * 新版任务中心
     */
    @GetMapping("/new/taskListV2")
    public Object getNewTaskListV2(@LoginUser String userId, @RequestParam(defaultValue = "") String version,
                    @RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit,
                    @RequestParam(defaultValue = "id") String sort,
                    @Order @RequestParam(defaultValue = "desc") String order, HttpServletRequest request)
                    throws IOException {
        // if(userId == null){
        // return ResponseUtil.unlogin();
        // }

        Map<String, Object> dataMap = new HashMap<>();



        // JSONObject object = JSONObject.fromObject(result);
        String mobileTye = request.getHeader("User-Agent");
        // if (object.getString("errno").equals("0")) { JSON.parseObject(result, List.class);

        List<MallCategory> kmMallCategoryList = (List) mallCategoryService.getCategoryByTask("categoryTask");
        List<MallCategory> kmMallCategoryList1 = (List) mallCategoryService.getCategoryByTask("categoryTask_gs");
        // List<MallCategory> kmMallCategoryList = JSON.parseArray(result, MallCategory.class);
        // List<MallCategory> kmMallCategoryList1 = JSON.parseArray(result1, MallCategory.class);
        List<MallCategory> list = new ArrayList<>();
        List<MallCategory> listA = new ArrayList<>();
        // ios过审

        if (RetrialUtils.examine(request, version)) {
            for (MallCategory game : kmMallCategoryList) {
                if ("L3".equals(game.getLevel()) || "L4".equals(game.getLevel())) {


                } else {
                    if ("在线游戏".equals(game.getName())) {
                        for (MallCategory game1 : kmMallCategoryList1) {
                            if ("categoryTask_gs".equals(game1.getPositionCode()) && "1".equals(game1.getPid())) {
                                game.setName(game1.getName());// 在线玩游戏换成早起打卡
                                game.setLinkUrl(game1.getLinkUrl());
                                game.setIconUrl(game1.getIconUrl());
                                game.setChainChange(game1.getChainChange());
                            }
                        }

                    } else if ("试玩赚钱".equals(game.getName())) {
                        for (MallCategory game1 : kmMallCategoryList1) {
                            if ("categoryTask_gs".equals(game1.getPositionCode()) && "2".equals(game1.getPid())) {
                                game.setName(game1.getName());// 在线玩游戏换成运动健康
                                game.setLinkUrl(game1.getLinkUrl());
                                game.setIconUrl(game1.getIconUrl());
                                game.setChainChange(game1.getChainChange());
                            }
                        }
                    }

                    list.add(game);
                }
            }
            dataMap.put("item", list);
        } else if (RetrialUtils.examineAndroid(mobileTye, version)) {
            for (MallCategory game : kmMallCategoryList) {
                if ("在线游戏".equals(game.getName())) {

                } else if ("试玩赚钱".equals(game.getName())) {

                } else {
                    listA.add(game);
                }
            }
            dataMap.put("item", listA);
        } else {
            dataMap.put("item", kmMallCategoryList);
        }

        return ResponseUtil.ok(dataMap);
    }

    // 兼容 任务 2 . 0 ， 首页图标数据
    @GetMapping("/new/taskListV3")
    public Object getNewTaskListV3(@RequestParam(defaultValue = "14")String industryId)throws IOException {
        List<MallCategory> kmMallCategoryList = (List) mallCategoryService.getCategoryByIndustryId(industryId);
        Map<String,Object> map = new HashMap<>();
        map.put("item",kmMallCategoryList);
        return ResponseUtil.ok(map);
    }



    /**
     * ios过审
     *
     * @param userId
     * @param version
     * @param request
     * @return
     */
    @GetMapping("/task/v2")
    public Object getTaskListV2(@LoginUser String userId, @RequestParam(defaultValue = "") String version,
                    HttpServletRequest request) {
        // if(userId == null){
        // return ResponseUtil.unlogin();
        // }

        Map<String, Object> map = new HashMap();
        String mobileTye = request.getHeader("User-Agent");
        // ios过审
        if (RetrialUtils.examine(request, version)) {
            map.put("newTask", "新人任务");
            map.put("everyDay", "每日任务");
        }
        // 不过审
        else {
            map.put("newTask", "新人任务");
            map.put("everyDay", "每日任务");
            map.put("high", "高佣任务");
        }
        return ResponseUtil.ok(map);
    }





}
