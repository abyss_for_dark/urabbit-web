package com.graphai.mall.wx.web.cart;

import static com.graphai.mall.wx.util.WxResponseCode.GOODS_NO_STOCK;
import static com.graphai.mall.wx.util.WxResponseCode.GOODS_UNSHELVE;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.graphai.mall.admin.dao.MallMerchantMapper;
import com.graphai.mall.admin.domain.*;
import com.graphai.mall.admin.service.*;
import com.graphai.mall.db.constant.coupon.CouponUserConstant;
import com.graphai.mall.db.constant.activity.MallActivityEnum;
import com.graphai.mall.db.dao.CustomCategoryMapper;
import com.graphai.mall.db.dao.MallUserMapper;
import com.graphai.mall.db.domain.*;
import com.graphai.mall.db.service.account.AccountService;
import com.graphai.mall.db.service.category.MallCategoryService;
import com.graphai.mall.db.service.common.MallRegionService;
import com.graphai.mall.db.service.groupon.MallGrouponRulesService;
import com.graphai.mall.db.util.StringUtils;
import com.graphai.mall.setting.service.FreightTemplateService;
import com.graphai.open.mallgrouponrules.entity.IMallGrouponRules;
import com.graphai.open.mallgrouponrules.service.IMallGrouponRulesService;
import com.graphai.server.dto.Sys;
import com.sun.org.apache.xpath.internal.operations.Bool;
import com.tencentcloudapi.ocr.v20181119.models.TableOCRRequest;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.alibaba.fastjson.JSONObject;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.json.JacksonUtil;
import com.graphai.commons.util.json.JacksonUtils;
import com.graphai.id.GraphaiIdGenerator;
import com.graphai.mall.core.system.SystemConfig;
import com.graphai.mall.db.service.MallCartService;
import com.graphai.mall.db.service.coupon.CouponVerifyService;
import com.graphai.mall.db.service.coupon.MallCouponService;
import com.graphai.mall.db.service.coupon.MallCouponUserService;
import com.graphai.mall.db.service.distribution.FenXiaoService;
import com.graphai.mall.db.service.goods.MallGoodsProductService;
import com.graphai.mall.db.service.goods.MallGoodsService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.service.user.behavior.MallAddressService;
import com.graphai.mall.db.util.UserUtils;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.mall.wx.util.GoodsUtils;
import com.graphai.mall.wx.util.Md5Utils;
import com.graphai.mall.wx.util.WxResponseCode;

import javax.annotation.Resource;

/**
 * 用户购物车服务
 */
@RestController
@RequestMapping("/wx/cart")
@Validated
public class WxCartController {
    private final Log logger = LogFactory.getLog(WxCartController.class);

    @Autowired
    private MallCartService cartService;
    @Autowired
    private MallGoodsService goodsService;
    @Autowired
    private MallGoodsProductService productService;
    @Autowired
    private MallAddressService addressService;
    @Autowired
    private MallGrouponRulesService grouponRulesService;
    @Autowired
    private MallCouponService couponService;
    @Autowired
    private MallCouponUserService couponUserService;
    @Autowired
    private CouponVerifyService couponVerifyService;
    @Autowired
    private FenXiaoService fenXiaoService;
    @Autowired
    private MallUserService userService;
    @Autowired
    private IMallUserProfitNewService userProfitNewService;
    @Autowired
    private IMallGoodsPositionService mallGoodsPositionService;
    @Autowired
    private FreightTemplateService freightTemplateService;
    @Autowired
    private IMallMerchantService mallMerchantService;
    @Autowired
    private MallUserService mallUserService;
    @Autowired
    private IMallUserMerchantService mallUserMerchantService;
    @Autowired
    private IMallActivityGoodsProductService mallActivityGoodsProductService;
    @Autowired
    private MallCouponUserService mallCouponUserService;
    @Autowired
    private MallCouponService mallCouponService;
    @Autowired
    private MallAddressService mallAddressService;
    @Autowired
    private MallRegionService mallRegionService;
    @Autowired
    private AccountService accountService;

    /**
     * 用户购物车信息
     *
     * @param userId 用户ID
     * @return 用户购物车信息
     */
    @GetMapping("index")
    public Object index(@LoginUser String userId) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        int num = 0;
        int number = 0;
        String productId = "";
        BigDecimal single = BigDecimal.ZERO;
        List<MallCart> cartList = cartService.queryByUid(userId);
        Integer goodsCount = 0;
        BigDecimal goodsAmount = new BigDecimal(0.00);
        Integer checkedGoodsCount = 0;
        BigDecimal checkedGoodsAmount = new BigDecimal(0.00);

        BigDecimal freightPrice = new BigDecimal(0.00);
        if (CollectionUtils.isNotEmpty(cartList)) {
            List<String> gids = cartList.stream().map(MallCart::getGoodsId).collect(Collectors.toList());
            MallGoodsExample example1 = new MallGoodsExample();
            MallGoodsExample.Criteria criteria1 = example1.or();
            criteria1.andIdIn(gids);
            List<MallGoods> goods = goodsService.selectByExample(example1);


            // 求出购物车中的商品的全部运费总和
//            freightPrice = goodsService.queryTotalPostage(gids);
            for (MallCart cart : cartList) {
                for(MallGoods item : goods){
                    if(cart.getGoodsId().equals(item.getId())){
                        if("10".equals(item.getCategoryIdTwo())){
                            cart.setShId("true");
                        }else{
                            cart.setShId("false");
                        }
                    }
                }


                goodsCount += cart.getNumber();
                goodsAmount = goodsAmount.add(cart.getPrice().multiply(new BigDecimal(cart.getNumber())));
                if (cart.getChecked()) {
                    num++;
                    number = cart.getNumber();
                    single = cart.getPrice();
                    productId = cart.getProductId();
                    checkedGoodsCount += cart.getNumber();
                    checkedGoodsAmount =
                                    checkedGoodsAmount.add(cart.getPrice().multiply(new BigDecimal(cart.getNumber())));
                }
            }

            // 根据订单商品总价计算运费，多个商品满88则免运费，否则8元；如果是单个商品则按商品的邮费计算(保留代码)
            // if (num > 1) {
            // if (checkedGoodsAmount.compareTo(SystemConfig.getFreightLimit()) < 0) {
            //
            // freightPrice = SystemConfig.getFreight();
            // // freightPrice = new BigDecimal("0.00");
            // // freightPrice = new BigDecimal("8.00");
            // }
            // } else if (num == 1) {
            // try {
            // JSONObject obj = goodsService.queryGoodsByProductId(productId);
            // if (Objects.equals(null, obj)) {
            // return ResponseUtil.fail(507, "商品信息有误!");
            // }
            // freightPrice = new BigDecimal(obj.getString("postage"));
            // // checkedGoodsAmount=single.multiply(new BigDecimal(number));
            // } catch (Exception e) {
            // // TODO: handle exception
            // e.printStackTrace();
            // }
            // }
        }

        Map<String, Object> cartTotal = new HashMap<>();
        cartTotal.put("goodsCount", goodsCount);
        cartTotal.put("goodsAmount", goodsAmount);
        cartTotal.put("checkedGoodsCount", checkedGoodsCount);
        cartTotal.put("checkedGoodsAmount", checkedGoodsAmount.add(freightPrice));
        cartTotal.put("freightPrice", freightPrice);
        Map<String, Object> result = new HashMap<>();
        result.put("cartList", cartList);
        result.put("cartTotal", cartTotal);


        return ResponseUtil.ok(result);
    }


    /**
     * 获取临时购物车信息(当前购物车不勾选或选中的数据变化)
     *
     * @param userId 用户ID
     * @return 用户购物车信息
     */
    // public Object tempCart(String userId,List<String> productIds,boolean checked) {
    // if (userId == null) {
    // return ResponseUtil.unlogin();
    // }
    //
    // List<MallCart> cartList = cartService.queryByUidProductIdsChecked(userId, productIds, checked);
    // Integer goodsCount = 0;
    // BigDecimal goodsAmount = new BigDecimal(0.00);
    // Integer checkedGoodsCount = 0;
    // BigDecimal checkedGoodsAmount = new BigDecimal(0.00);
    // for (MallCart cart : cartList) {
    // goodsCount += cart.getNumber();
    // goodsAmount = goodsAmount.add(cart.getPrice().multiply(new BigDecimal(cart.getNumber())));
    // if (cart.getChecked()) {
    // checkedGoodsCount += cart.getNumber();
    // checkedGoodsAmount = checkedGoodsAmount.add(cart.getPrice().multiply(new
    // BigDecimal(cart.getNumber())));
    // }
    // }
    // Map<String, Object> cartTotal = new HashMap<>();
    // cartTotal.put("goodsCount", goodsCount);
    // cartTotal.put("goodsAmount", goodsAmount);
    // cartTotal.put("checkedGoodsCount", checkedGoodsCount);
    // cartTotal.put("checkedGoodsAmount", checkedGoodsAmount);
    //
    // Map<String, Object> result = new HashMap<>();
    // result.put("cartList", cartList);
    // result.put("cartTotal", cartTotal);
    //
    // // 根据订单商品总价计算运费，满88则免运费，否则8元；
    // BigDecimal freightPrice = new BigDecimal(0.00);
    // if (checkedGoodsAmount.compareTo(SystemConfig.getFreightLimit()) < 0) {
    //
    // //freightPrice = SystemConfig.getFreight();
    // //freightPrice = new BigDecimal("0.00");
    // //freightPrice = new BigDecimal("8.00");
    // }
    // result.put("freightPrice", freightPrice);
    // return ResponseUtil.ok(result);
    // }


    /**
     * 用户购物车信息
     *
     * @param userId 用户ID
     * @return 用户购物车信息
     */
    @PostMapping("wlkIndex")
    public Object wlkIndex(@LoginUser String userId, @RequestBody String data) {

        String sign = JacksonUtil.parseString(data, "sign");
        // 手机号
        String mobile = JacksonUtil.parseString(data, "mobile");

        if (null == userId) {
            Object result = UserUtils.getTokenUserId(mobile);
            if (!Objects.equals(result, null)) {
                userId = String.valueOf(result);
            }
        }

        List<MallCart> cartList = cartService.queryByUid(userId);
        Integer goodsCount = 0;
        BigDecimal goodsAmount = new BigDecimal(0.00);
        Integer checkedGoodsCount = 0;
        BigDecimal checkedGoodsAmount = new BigDecimal(0.00);

        BigDecimal freightPrice = new BigDecimal(0.00);
        int num = 0;
        int number = 0;
        String productId = "";
        BigDecimal single = BigDecimal.ZERO;
        if (CollectionUtils.isNotEmpty(cartList)) {
            List<String> gids = cartList.stream().map(MallCart::getGoodsId).collect(Collectors.toList());
            // 求出购物车中的商品的全部运费总和
            freightPrice = goodsService.queryTotalPostage(gids);
            for (MallCart cart : cartList) {
                goodsCount += cart.getNumber();
                goodsAmount = goodsAmount.add(cart.getPrice().multiply(new BigDecimal(cart.getNumber())));
                if (cart.getChecked()) {
                    num++;
                    number = cart.getNumber();
                    single = cart.getPrice();
                    productId = cart.getProductId();
                    checkedGoodsCount += cart.getNumber();
                    checkedGoodsAmount =
                                    checkedGoodsAmount.add(cart.getPrice().multiply(new BigDecimal(cart.getNumber())));
                }
            }


            // 根据订单商品总价计算运费，多个商品满88则免运费，否则8元；如果是单个商品则按商品的邮费计算（废掉先注释保留）
            // if (num > 1) {
            // if (checkedGoodsAmount.compareTo(SystemConfig.getFreightLimit()) < 0) {
            // freightPrice = SystemConfig.getFreight();
            // }
            // } else if (num == 1) {
            // try {
            // JSONObject obj = goodsService.queryGoodsByProductId(productId);
            // if (Objects.equals(null, obj)) {
            // return ResponseUtil.fail(507, "商品信息有误!");
            // }
            // freightPrice = new BigDecimal(obj.getString("postage"));
            // // checkedGoodsAmount=single.multiply(new BigDecimal(number));
            // } catch (Exception e) {
            // // TODO: handle exception
            // e.printStackTrace();
            // }
            // }

        }
        Map<String, Object> cartTotal = new HashMap<>();
        cartTotal.put("goodsCount", goodsCount);
        cartTotal.put("goodsAmount", goodsAmount);
        cartTotal.put("checkedGoodsCount", checkedGoodsCount);
        cartTotal.put("checkedGoodsAmount",
                        checkedGoodsAmount.add(freightPrice == null ? BigDecimal.ZERO : freightPrice));
        cartTotal.put("freightPrice", freightPrice);

        Map<String, Object> result = new HashMap<>();
        result.put("cartList", cartList);
        result.put("cartTotal", cartTotal);

        return ResponseUtil.ok(result);
    }

    /**
     * 加入商品到购物车
     * <p>
     * 如果已经存在购物车货品，则增加数量； 否则添加新的购物车货品项。
     *
     * @param userId 用户ID
     * @param cart 购物车商品信息， { goodsId: xxx, productId: xxx, number: xxx }
     * @return 加入购物车操作结果
     */
    @PostMapping("add")
    public Object add(@LoginUser String userId, @RequestBody MallCart cart) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        if (cart == null) {
            return ResponseUtil.badArgument();
        }
        // 是否为商户用户
        Boolean isMerchant = false;
        MallUser mallUser = mallUserService.queryByUserId(userId);
        if(mallUser != null && (mallUser.getLevelId().equals("9") || mallUser.getLevelId().equals("10") || mallUser.getLevelId().equals("11"))){
            isMerchant = true;
        }

        String productId = cart.getProductId();
        Integer number = cart.getNumber().intValue();
        String goodsId = cart.getGoodsId();
        String shId = cart.getShId();
        if (!ObjectUtils.allNotNull(productId, number, goodsId)) {
            return ResponseUtil.badArgument();
        }
        if (number <= 0) {
            return ResponseUtil.badArgument();
        }

        // 判断商品是否可以购买
        MallGoods goods = goodsService.findById(goodsId);
        if (goods == null || !goods.getIsOnSale()) {
            return ResponseUtil.fail(GOODS_UNSHELVE, "商品已下架");
        }

        MallRebateRecord rebate = null;
        if (shId != null) {
            rebate = fenXiaoService.getMallRebateRecordById(shId);
        }

        MallGoodsProduct product = productService.findById(productId);

        if(cart.getIsActivity() != null && cart.getIsActivity() != 0){
            // 活动商品
            MallActivityGoodsProduct mallActivityGoodsProduct = mallActivityGoodsProductService.getById(productId);
            product = new MallGoodsProduct();
            product.setNumber(mallActivityGoodsProduct.getNumber());
            product.setPrice(mallActivityGoodsProduct.getActivityPrice());
            product.setSpecifications(mallActivityGoodsProduct.getSpecifications());
        }
        // 判断购物车中是否存在此规格商品
        MallCart existCart = cartService.queryExist(goodsId, productId, userId);
        if (existCart == null) {
            // 取得规格的信息,判断规格库存
            if (product == null || number > product.getNumber()) {
                return ResponseUtil.fail(GOODS_NO_STOCK, "库存不足");
            }

            cart.setId(null);
            cart.setGoodsSn(goods.getGoodsSn());
            cart.setGoodsName((goods.getName()));
            cart.setPicUrl(goods.getPicUrl());
            // cart.setPrice(product.getPrice());
            if (rebate != null) {
                BigDecimal productPrice = product.getPrice();
                BigDecimal finalPrice = GoodsUtils.getRetailPrice(rebate, productPrice);
                cart.setPrice(finalPrice);
            } else {
                BigDecimal price = product.getPrice();
                if(isMerchant){
                    if(cart.getIsActivity() == null || cart.getIsActivity() == 0) {
                        price = product.getFactoryPrice();
                    }
                }
                cart.setPrice(price);
            }
            cart.setSrcGoodId(goods.getSrcGoodId());
            cart.setSpecifications(product.getSpecifications());
            cart.setUserId(userId);
            cart.setChecked(true);
            cartService.add(cart);
        } else {
            // 取得规格的信息,判断规格库存
            int num = existCart.getNumber() + number;
            if (num > product.getNumber()) {
                return ResponseUtil.fail(GOODS_NO_STOCK, "库存不足");
            }
            BigDecimal price = product.getPrice();
            if(isMerchant){
                if(cart.getIsActivity() == null || cart.getIsActivity() == 0){
                    price = product.getFactoryPrice();
                }
            }
            existCart.setPrice(price);
            existCart.setNumber((short) num);
            if (cartService.updateById(existCart) == 0) {
                return ResponseUtil.updatedDataFailed();
            }
        }

        return goodscount(userId);
    }



    /**
     * 加入商品到购物车
     * <p>
     * 如果已经存在购物车货品，则增加数量； 否则添加新的购物车货品项。
     *
     * @param userId 用户ID
     * @param cart 购物车商品信息， { goodsId: xxx, productId: xxx, number: xxx }
     * @return 加入购物车操作结果
     */
    @PostMapping("wlkZyAdd")
    public Object wlkZyAdd(@LoginUser String userId, @RequestBody String data) {


        String sign = JacksonUtil.parseString(data, "sign");
        // 手机号
        String mobile = JacksonUtil.parseString(data, "mobile");
        String goodsId = JacksonUtil.parseString(data, "goodsId");
        String goodsName = JacksonUtil.parseString(data, "goodsName");
        String productId = JacksonUtil.parseString(data, "productId");
        BigDecimal price = new BigDecimal(JacksonUtil.parseString(data, "price"));
        Short num = JacksonUtil.parseShort(data, "num");
        // String specifications [] = JacksonUtil.parseObject(data, "specifications",String [].class);
        // Integer checked = JacksonUtil.parseInteger(data, "checked");
        String specifications = JacksonUtil.parseString(data, "specifications");
        String picUrl = JacksonUtil.parseString(data, "picUrl");

        // if (cart == null) {
        // return ResponseUtil.badArgument();
        // }
        // String userId = "";
        // List<MallUser> users = userService.queryByMobile(mobile);
        // if (CollectionUtils.isNotEmpty(users)) {
        // if (org.apache.commons.lang3.StringUtils.isNotEmpty(users.get(0).getId())) {
        // userId = users.get(0).getId();
        // }
        // } else {
        // return ResponseUtil.fail(501, "用户不存在!");
        // }

        if (null == userId) {
            Object result = UserUtils.getTokenUserId(mobile);
            if (!Objects.equals(result, null)) {
                userId = String.valueOf(result);
            }
        }



        // Map<String, Object> paramMap = new HashMap<>();
        // paramMap.put("goodsId", goodsId);
        // paramMap.put("goodsName", goodsName);
        // paramMap.put("productId", productId);
        // paramMap.put("price", price);
        // paramMap.put("num", num);
        // paramMap.put("specifications", specifications);
        // // paramMap.put("checked", checked);
        // paramMap.put("picUrl", picUrl);
        // paramMap.put("mobile", mobile);
        // String string = JSONObject.toJSONString(paramMap);// 请求参数转换成字符串进行加密处理
        // boolean flag = Md5Utils.verifySignature(string, sign);
        // if (!flag) {
        // return ResponseUtil.fail(403, "无效签名");
        // }

        JSONObject obj = null;
        try {
            obj = goodsService.queryGoodsByProductId(productId);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        if (Objects.equals(null, obj)) {
            return ResponseUtil.fail(507, "商品信息有误!");
        }


        // 判断商品是否可以购买
        // MallGoods goods = goodsService.findById(goodsId);
        if (!obj.getBooleanValue("isOnSale")) {
            return ResponseUtil.fail(GOODS_UNSHELVE, "商品已下架");
        }


        // String shId = cart.getShId();
        if (!ObjectUtils.allNotNull(productId, num, goodsId)) {
            return ResponseUtil.badArgument();
        }
        if (num <= 0) {
            return ResponseUtil.badArgument();
        }



        // MallRebateRecord rebate = null;
        // if (shId != null) {
        // rebate = fenXiaoService.getMallRebateRecordById(shId);
        // }

        // MallGoodsProduct product = productService.findById(productId);
        // 判断购物车中是否存在此规格商品
        MallCart existCart = cartService.queryExist(goodsId, productId, userId);
        if (existCart == null) {
            // // 取得规格的信息,判断规格库存
            // if (product == null || number > product.getNumber()) {
            // return ResponseUtil.fail(GOODS_NO_STOCK, "库存不足");
            // }
            MallCart cart = new MallCart();
            cart.setId(GraphaiIdGenerator.nextId("MallCart"));
            // cart.setGoodsSn(goods.getGoodsSn());
            cart.setGoodsName((obj.getString("gName")));
            cart.setPicUrl(obj.getString("picUrl"));
            cart.setPrice(price);
            cart.setGoodsId(goodsId);
            cart.setProductId(productId);
            cart.setNumber(num);
            // if (rebate != null) {
            // BigDecimal productPrice = product.getPrice();
            // BigDecimal finalPrice = GoodsUtils.getRetailPrice(rebate, productPrice);
            // cart.setPrice(finalPrice);
            // } else {
            // cart.setPrice(product.getPrice());
            // }
            // cart.setSrcGoodId(goods.getSrcGoodId());
            // String str1 = specifications.substring(1, specifications.length() - 1);
            String[] arr = specifications.split(",");
            cart.setSpecifications(arr);
            cart.setUserId(userId);
            cart.setChecked(true);
            cartService.add(cart);
        } else {
            // 取得规格的信息,判断规格库存
            int number = existCart.getNumber() + num;
            if (number > Integer.parseInt(obj.getString("number"))) {
                return ResponseUtil.fail(GOODS_NO_STOCK, "库存不足");
            }
            existCart.setNumber((short) number);
            if (cartService.updateById(existCart) == 0) {
                return ResponseUtil.updatedDataFailed();
            }
        }

        return goodscount(userId) == null ? 1 : goodscount(userId);
    }

    /**
     * 立即购买
     * <p>
     * 和add方法的区别在于： 1. 如果购物车内已经存在购物车货品，前者的逻辑是数量添加，这里的逻辑是数量覆盖 2.
     * 添加成功以后，前者的逻辑是返回当前购物车商品数量，这里的逻辑是返回对应购物车项的ID
     *
     * @param userId 用户ID
     * @param cart 购物车商品信息， { goodsId: xxx, productId: xxx, number: xxx }
     * @return 立即购买操作结果
     */
    @PostMapping("fastadd")
    public Object fastadd(@LoginUser String userId, @RequestBody MallCart cart) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        if (cart == null) {
            return ResponseUtil.badArgument();
        }

        // 是否为商户用户
        Boolean isMerchant = false;
        MallUser mallUser = mallUserService.queryByUserId(userId);
        if(mallUser != null && (mallUser.getLevelId().equals("9") || mallUser.getLevelId().equals("10") || mallUser.getLevelId().equals("11"))){
            isMerchant = true;
        }

        String productId = cart.getProductId();
        Integer number = cart.getNumber().intValue();
        String goodsId = cart.getGoodsId();
        String shId = cart.getShId();

        MallRebateRecord rebate = null;
        if (shId != null) {
            rebate = fenXiaoService.getMallRebateRecordById(shId);
        }

        if (!ObjectUtils.allNotNull(productId, number, goodsId)) {
            return ResponseUtil.badArgument();
        }
        if (number <= 0) {
            return ResponseUtil.badArgument();
        }

        // 判断商品是否可以购买
        MallGoods goods = goodsService.findById(goodsId);
        if (goods == null || !goods.getIsOnSale()) {
            return ResponseUtil.fail(GOODS_UNSHELVE, "商品已下架");
        }

        MallGoodsProduct product = productService.findById(productId);

        if(cart.getIsActivity() != null && cart.getIsActivity() != 0){
            // 直播活动商品
            MallActivityGoodsProduct mallActivityGoodsProduct = mallActivityGoodsProductService.getById(productId);
            if(mallActivityGoodsProduct != null){
                product = new MallGoodsProduct();
                product.setNumber(mallActivityGoodsProduct.getNumber());
                product.setPrice(mallActivityGoodsProduct.getActivityPrice());
                product.setSpecifications(mallActivityGoodsProduct.getSpecifications());
            }
        }
        // 判断购物车中是否存在此规格商品
        MallCart existCart = cartService.queryExist(goodsId, productId, userId);
        if (existCart == null) {
            // 取得规格的信息,判断规格库存
            if (product == null || number > product.getNumber()) {
                return ResponseUtil.fail(GOODS_NO_STOCK, "库存不足");
            }

            cart.setId(null);
            cart.setGoodsSn(goods.getGoodsSn());
            cart.setGoodsName((goods.getName()));
            cart.setPicUrl(goods.getPicUrl());
            cart.setSrcGoodId(goods.getSrcGoodId());
            if (rebate != null) {
                BigDecimal productPrice = product.getPrice();
                BigDecimal finalPrice = GoodsUtils.getRetailPrice(rebate, productPrice);
                cart.setPrice(finalPrice);
            } else {
                BigDecimal price = product.getPrice();
                if(isMerchant){
                    if(cart.getIsActivity() == null || cart.getIsActivity() == 0) {
                        price = product.getFactoryPrice();
                    }
                }
                cart.setPrice(price);
            }
            // cart.setPrice(product.getPrice());
            cart.setSpecifications(product.getSpecifications());
            cart.setUserId(userId);
            cart.setChecked(true);
            cartService.add(cart);
        } else {
            // 取得规格的信息,判断规格库存
            int num = number;
            if (num > product.getNumber()) {
                return ResponseUtil.fail(GOODS_NO_STOCK, "库存不足");
            }
            BigDecimal price = product.getPrice();
            if(isMerchant){
                if(cart.getIsActivity() == null || cart.getIsActivity() == 0){
                    price = product.getFactoryPrice();
                }
            }
            existCart.setPrice(price);
            existCart.setNumber((short) num);
            if (cartService.updateById(existCart) == 0) {
                return ResponseUtil.updatedDataFailed();
            }
        }

        return ResponseUtil.ok(existCart != null ? existCart.getId() : cart.getId());
    }


    /**
     * 立即购买
     * <p>
     * 和add方法的区别在于： 1. 如果购物车内已经存在购物车货品，前者的逻辑是数量添加，这里的逻辑是数量覆盖 2.
     * 添加成功以后，前者的逻辑是返回当前购物车商品数量，这里的逻辑是返回对应购物车项的ID
     *
     * @param userId 用户ID
     * @param cart 购物车商品信息， { goodsId: xxx, productId: xxx, number: xxx }
     * @return 立即购买操作结果
     */
    @PostMapping("wlkFastAdd")
    public Object wlkFastAdd(@LoginUser String userId, @RequestBody String data) {
        String sign = JacksonUtil.parseString(data, "sign");
        // 手机号
        String mobile = JacksonUtil.parseString(data, "mobile");
        String goodsId = JacksonUtil.parseString(data, "goodsId");
        String goodsName = JacksonUtil.parseString(data, "goodsName");
        String productId = JacksonUtil.parseString(data, "productId");
        BigDecimal price = new BigDecimal(JacksonUtil.parseString(data, "price"));
        Short num = JacksonUtil.parseShort(data, "num");
        String specifications = JacksonUtil.parseString(data, "specifications");
        String picUrl = JacksonUtil.parseString(data, "picUrl");



        if (null == userId) {
            Object result = UserUtils.getTokenUserId(mobile);
            if (!Objects.equals(result, null)) {
                userId = String.valueOf(result);
            }
        }

        String cartId = "";
        JSONObject obj = null;
        try {
            obj = goodsService.queryGoodsByProductId(productId);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        if (Objects.equals(null, obj)) {
            return ResponseUtil.fail(507, "商品信息有误!");
        }


        // 判断商品是否可以购买
        // MallGoods goods = goodsService.findById(goodsId);
        if (!obj.getBooleanValue("isOnSale")) {
            return ResponseUtil.fail(GOODS_UNSHELVE, "商品已下架");
        }


        // String shId = cart.getShId();
        if (!ObjectUtils.allNotNull(productId, num, goodsId)) {
            return ResponseUtil.badArgument();
        }
        if (num <= 0) {
            return ResponseUtil.badArgument();
        }

        if (num > Integer.parseInt(obj.getString("number"))) {
            return ResponseUtil.fail(GOODS_NO_STOCK, "库存不足");
        }
        // 判断购物车中是否存在此规格商品
        MallCart existCart = cartService.queryExist(goodsId, productId, userId);
        if (existCart == null) {
            // 取得规格的信息,判断规格库存

            MallCart cart = new MallCart();
            cart.setId(GraphaiIdGenerator.nextId("MallCart"));
            // cart.setGoodsSn(goods.getGoodsSn());
            cart.setGoodsName((obj.getString("gName")));
            cart.setPicUrl(obj.getString("picUrl"));
            cart.setPrice(price);
            cart.setGoodsId(goodsId);
            cart.setProductId(productId);
            cart.setNumber(num);
            String[] arr = specifications.split(",");
            cart.setSpecifications(arr);
            cart.setUserId(userId);
            cart.setChecked(true);
            cart.setDeptId(obj.getString("deptId"));// 购物车加归属部门ID
            cartService.add(cart);
            cartId = cart.getId();
        } else {

            existCart.setNumber((short) num);
            if (cartService.updateById(existCart) == 0) {
                return ResponseUtil.updatedDataFailed();
            }
        }

        return ResponseUtil.ok(existCart != null ? existCart.getId() : cartId);
    }

    /**
     * 修改购物车商品货品数量
     *
     * @param userId 用户ID
     * @param cart 购物车商品信息， { id: xxx, goodsId: xxx, productId: xxx, number: xxx }
     * @return 修改结果
     */
    @PostMapping("update")
    public Object update(@LoginUser String userId, @RequestBody MallCart cart) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        if (cart == null) {
            return ResponseUtil.badArgument();
        }
        String productId = cart.getProductId();
        Integer number = cart.getNumber().intValue();
        String goodsId = cart.getGoodsId();
        String id = cart.getId();
        if (!ObjectUtils.allNotNull(id, productId, number, goodsId)) {
            return ResponseUtil.badArgument();
        }
        if (number <= 0) {
            return ResponseUtil.badArgument();
        }

        // 判断是否存在该订单
        // 如果不存在，直接返回错误
        MallCart existCart = cartService.findById(id);
        if (existCart == null) {
            return ResponseUtil.badArgumentValue();
        }

        // 判断goodsId和productId是否与当前cart里的值一致
        if (!existCart.getGoodsId().equals(goodsId)) {
            return ResponseUtil.badArgumentValue();
        }
        if (!existCart.getProductId().equals(productId)) {
            return ResponseUtil.badArgumentValue();
        }

        // 判断商品是否可以购买
        MallGoods goods = goodsService.findById(goodsId);
        if (goods == null || !goods.getIsOnSale()) {
            return ResponseUtil.fail(GOODS_UNSHELVE, "商品已下架");
        }

        // 取得规格的信息,判断规格库存
        MallGoodsProduct product = productService.findById(productId);
        if (product == null || product.getNumber() < number) {
            return ResponseUtil.fail(GOODS_UNSHELVE, "库存不足");
        }

        existCart.setNumber(number.shortValue());

        if (cartService.updateById(existCart) == 0) {
            return ResponseUtil.updatedDataFailed();
        }
        return index(userId);
    }


    /**
     * 修改购物车商品货品数量
     *
     * @param userId 用户ID
     * @param cart 购物车商品信息， { id: xxx, goodsId: xxx, productId: xxx, number: xxx }
     * @return 修改结果
     */
    @PostMapping("wlkUpdate")
    public Object wlkUpdate(@LoginUser String userId, @RequestBody String data) {

        // 手机号
        String mobile = JacksonUtil.parseString(data, "mobile");
        String goodsId = JacksonUtil.parseString(data, "goodsId");
        String productId = JacksonUtil.parseString(data, "productId");
        String id = JacksonUtil.parseString(data, "id");
        Short num = JacksonUtil.parseShort(data, "num");
        boolean checked = JacksonUtil.parseBoolean(data, "checked");



        if (null == userId) {
            Object result = UserUtils.getTokenUserId(mobile);
            if (!Objects.equals(result, null)) {
                userId = String.valueOf(result);
            }
        }

        if (!ObjectUtils.allNotNull(id, productId, num, goodsId)) {
            return ResponseUtil.badArgument();
        }
        if (num <= 0) {
            return ResponseUtil.badArgument();
        }

        // 判断是否存在该订单
        // 如果不存在，直接返回错误
        MallCart existCart = cartService.findById(id);
        if (existCart == null) {
            return ResponseUtil.badArgumentValue();
        }

        // 判断goodsId和productId是否与当前cart里的值一致
        if (!existCart.getGoodsId().equals(goodsId)) {
            return ResponseUtil.badArgumentValue();
        }
        if (!existCart.getProductId().equals(productId)) {
            return ResponseUtil.badArgumentValue();
        }

        JSONObject obj = null;
        try {
            obj = goodsService.queryGoodsByProductId(productId);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        if (Objects.equals(null, obj)) {
            return ResponseUtil.fail(507, "商品信息有误!");
        }


        // 判断商品是否可以购买
        // MallGoods goods = goodsService.findById(goodsId);
        if (!obj.getBooleanValue("isOnSale")) {
            return ResponseUtil.fail(GOODS_UNSHELVE, "商品已下架");
        }



        // 取得规格的信息,判断规格库存
        // MallGoodsProduct product = productService.findById(productId);
        if (obj.getShort("number") < num) {
            return ResponseUtil.fail(GOODS_NO_STOCK, "库存不足");
        }

        existCart.setNumber(num.shortValue());
        existCart.setChecked(checked);

        if (cartService.updateById(existCart) == 0) {
            return ResponseUtil.updatedDataFailed();
        }
        return index(userId);
    }

    /**
     * 购物车商品货品勾选状态
     * <p>
     * 如果原来没有勾选，则设置勾选状态；如果商品已经勾选，则设置非勾选状态。
     *
     * @param userId 用户ID
     * @param body 购物车商品信息， { productIds: xxx, isChecked: 1/0 }
     * @return 购物车信息
     */
    @PostMapping("checked")
    public Object checked(@LoginUser String userId, @RequestBody String body) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        if (body == null) {
            return ResponseUtil.badArgument();
        }

        List<String> productIds = JacksonUtil.parseStringList(body, "productIds");
        if (productIds == null) {
            return ResponseUtil.badArgument();
        }

        Integer checkValue = JacksonUtil.parseInteger(body, "isChecked");
        if (checkValue == null) {
            return ResponseUtil.badArgument();
        }
        Boolean isChecked = (checkValue == 1);

        cartService.updateCheck(userId, productIds, isChecked);
        return index(userId);
    }


    /**
     * 购物车商品货品勾选状态
     * <p>
     * 如果原来没有勾选，则设置勾选状态；如果商品已经勾选，则设置非勾选状态。
     *
     * @param userId 用户ID
     * @param body 购物车商品信息， { productIds: xxx, isChecked: 1/0 }
     * @return 购物车信息
     */
    @PostMapping("wlkChecked")
    public Object wlkChecked(@LoginUser String userId, @RequestBody String data) {
        if (data == null) {
            return ResponseUtil.badArgument();
        }

        String sign = JacksonUtil.parseString(data, "sign");
        // 手机号
        String mobile = JacksonUtil.parseString(data, "mobile");
        String productIds = JacksonUtil.parseString(data, "productIds");
        // List<String> productIds = JacksonUtil.parseStringList(data, "productIds");
        boolean isChecked = JacksonUtil.parseBoolean(data, "isChecked");
        if (productIds == null) {
            return ResponseUtil.badArgument();
        }

        // String userId = "";
        // List<MallUser> users = userService.queryByMobile(mobile);
        // if (CollectionUtils.isNotEmpty(users)) {
        // if (org.apache.commons.lang3.StringUtils.isNotEmpty(users.get(0).getId())) {
        // userId = users.get(0).getId();
        // }
        // } else {
        // return ResponseUtil.fail(501, "用户不存在!");
        // }
        //
        // Map<String, Object> paramMap = new HashMap<>();
        // paramMap.put("productIds", productIds);
        // paramMap.put("mobile", mobile);
        // paramMap.put("isChecked", isChecked);
        // String string = JSONObject.toJSONString(paramMap);// 请求参数转换成字符串进行加密处理
        // boolean flag = Md5Utils.verifySignature(string, sign);
        // if (!flag) {
        // return ResponseUtil.fail(403, "无效签名");
        // }


        if (null == userId) {
            Object result = UserUtils.getTokenUserId(mobile);
            if (!Objects.equals(result, null)) {
                userId = String.valueOf(result);
            }
        }

        List lis = Arrays.asList(productIds.split(","));

        if (productIds == null || lis.size() == 0) {
            return ResponseUtil.badArgument();
        }


        cartService.updateCheck(userId, lis, isChecked);
        return index(userId);
    }

    /**
     * 购物车商品删除
     *
     * @param userId 用户ID
     * @param body 购物车商品信息， { productIds: xxx }
     * @return 购物车信息 成功则 { errno: 0, errmsg: '成功', data: xxx } 失败则 { errno: XXX, errmsg: XXX }
     */
    @PostMapping("delete")
    public Object delete(@LoginUser String userId, @RequestBody String body) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        if (body == null) {
            return ResponseUtil.badArgument();
        }

        List<String> productIds = JacksonUtil.parseStringList(body, "productIds");

        if (productIds == null || productIds.size() == 0) {
            return ResponseUtil.badArgument();
        }

        cartService.delete(productIds, userId);
        return index(userId);
    }


    /**
     * 购物车商品删除
     *
     * @param userId 用户ID
     * @param body 购物车商品信息， { productIds: xxx }
     * @return 购物车信息 成功则 { errno: 0, errmsg: '成功', data: xxx } 失败则 { errno: XXX, errmsg: XXX }
     */
    @PostMapping("wlkDelete")
    public Object wlkDelete(@LoginUser String userId, @RequestBody String data) {

        if (data == null) {
            return ResponseUtil.badArgument();
        }

        String sign = JacksonUtil.parseString(data, "sign");
        // 手机号
        String mobile = JacksonUtil.parseString(data, "mobile");
        String productIds = JacksonUtil.parseString(data, "productIds");

        // String userId = "";
        // List<MallUser> users = userService.queryByMobile(mobile);
        // if (CollectionUtils.isNotEmpty(users)) {
        // if (org.apache.commons.lang3.StringUtils.isNotEmpty(users.get(0).getId())) {
        // userId = users.get(0).getId();
        // }
        // } else {
        // return ResponseUtil.fail(501, "用户不存在!");
        // }
        //
        // Map<String, Object> paramMap = new HashMap<>();
        // paramMap.put("productIds", productIds);
        // paramMap.put("mobile", mobile);
        // String string = JSONObject.toJSONString(paramMap);// 请求参数转换成字符串进行加密处理
        // boolean flag = Md5Utils.verifySignature(string, sign);
        // if (!flag) {
        // return ResponseUtil.fail(403, "无效签名");
        // }

        List lis = Arrays.asList(productIds.split(","));

        if (productIds == null || lis.size() == 0) {
            return ResponseUtil.badArgument();
        }

        cartService.delete(lis, userId);
        return index(userId);
    }

    /**
     * 购物车商品货品数量
     * <p>
     * 如果用户没有登录，则返回空数据。
     *
     * @param userId 用户ID
     * @return 购物车商品货品数量
     */
    @GetMapping("goodscount")
    public Object goodscount(@LoginUser String userId) {
        if (userId == null) {
            return ResponseUtil.ok(0);
        }

        int goodsCount = 0;
        List<MallCart> cartList = cartService.queryByUid(userId);
        for (MallCart cart : cartList) {
            goodsCount += cart.getNumber();
        }

        return ResponseUtil.ok(goodsCount);
    }

    /**
     * 购物车下单
     *
     * @param userId 用户ID
     * @param cartId 购物车商品ID： 如果购物车商品ID是空，则下单当前用户所有购物车商品； 如果购物车商品ID非空，则只下单当前购物车商品。
     * @param addressId 收货地址ID： 如果收货地址ID是空，则查询当前用户的默认地址。
     * @param couponId 优惠券ID： 如果优惠券ID是空，则自动选择合适的优惠券。
     * @return 购物车操作结果
     */
    @GetMapping("checkout")
    public Object checkout(@LoginUser String userId, String cartId, String addressId, String couponId,
                    String grouponRulesId,Boolean isUseIntegral,@RequestParam(defaultValue = "") String merchantId) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }

        // 收货地址
        MallAddress checkedAddress = null;
        if ("0".equals(addressId)) {
            checkedAddress = addressService.findDefault(userId);
            // 如果仍然没有地址，则是没有收获地址
            // 返回一个空的地址id=0，这样前端则会提醒添加地址
            if (checkedAddress == null) {
                checkedAddress = new MallAddress();
                checkedAddress.setId("0");
                addressId = "0";
            } else {
                addressId = checkedAddress.getId();
            }

        } else {
            checkedAddress = addressService.query(userId, addressId);
            // 如果null, 则报错
            if (checkedAddress == null) {
                return ResponseUtil.badArgumentValue();
            }
        }

        // 团购优惠
        BigDecimal grouponPrice = new BigDecimal(0.00);
        MallGrouponRules grouponRules = grouponRulesService.queryById(grouponRulesId);
        if (grouponRules != null) {
            grouponPrice = grouponRules.getDiscount();
        }

        // 商品价格
        List<MallCart> checkedGoodsList = null;
        if (cartId == null || cartId.equals("0")) {
            checkedGoodsList = cartService.queryByUidAndChecked(userId);
        } else {
            MallCart cart = cartService.findById(cartId);
            if (cart == null) {
                return ResponseUtil.badArgumentValue();
            }
            checkedGoodsList = new ArrayList<>(1);
            checkedGoodsList.add(cart);
        }
        BigDecimal checkedGoodsPrice = new BigDecimal(0.00);
        // 根据订单商品总价计算运费，满88则免运费，否则8元；
        BigDecimal freightPrice = new BigDecimal(0.00);

        Integer isActivity = null;
        BigDecimal realPrice = BigDecimal.ZERO;

        /**
         * 临时商户id；用来判断是否为相同商家商品
         */
        String tempMerchantId = "";
        for (MallCart cart : checkedGoodsList) {
            // 只有当团购规格商品ID符合才进行团购优惠
            if (grouponRules != null && grouponRules.getGoodsId().equals(cart.getGoodsId())) {
                checkedGoodsPrice = checkedGoodsPrice
                                .add(cart.getPrice().subtract(grouponPrice).multiply(new BigDecimal(cart.getNumber())));
                realPrice = realPrice.add(cart.getPrice().subtract(grouponPrice).multiply(new BigDecimal(cart.getNumber())));
            } else {
                checkedGoodsPrice = checkedGoodsPrice.add(cart.getPrice().multiply(new BigDecimal(cart.getNumber())));
                realPrice = realPrice.add(cart.getPrice().multiply(new BigDecimal(cart.getNumber())));
            }
            MallGoods mallGoods = goodsService.findById(cart.getGoodsId());
            if(!StringUtils.isEmpty(tempMerchantId) && !tempMerchantId.equals(mallGoods.getMerchantId())){
                return ResponseUtil.fail("不能支付多家商户商品");
            }
            tempMerchantId = mallGoods.getMerchantId();
            if(StringUtils.isEmpty(merchantId)){
                merchantId = tempMerchantId;
            }

            if(isActivity != null && isActivity != cart.getIsActivity()){
                return ResponseUtil.fail("不能支付不同类型商品");
            }
            isActivity = cart.getIsActivity();

            /**
             * 运费计算
             * 00:平台
             */
            if(!addressId.equals("0") && !merchantId.equals("00")){
                // 发货地址
                MallUserMerchant mallUserMerchant = mallUserMerchantService.getOne(new QueryWrapper<MallUserMerchant>().eq("merchant_id", merchantId));
                MallMerchant mallMerchant = mallMerchantService.getById(merchantId);
//                MallUser mallUser = mallUserService.queryByUserId(mallUserMerchant.getUserId());
//                MallAddress mallAddress = mallAddressService.queryByUidAndIsDefault(mallUser.getId(),"2");
//                if(mallAddress == null){
//                    return ResponseUtil.fail("该商户未配置收货地址");
//                }
                String sendAddress = mallMerchant.getLongitude() + "," + mallMerchant.getLatitude();
                // 收货地址
                String receiveAddress = checkedAddress.getLongitude() + "," + checkedAddress.getLatitude();
                freightPrice = freightPrice.add(freightTemplateService.calculateCost(mallGoods.getTemplateId(),
                        mallGoods.getMerchantId(),checkedAddress.getProvince(),checkedAddress.getCity(),Integer.parseInt(cart.getNumber().toString()),sendAddress,
                        receiveAddress));
            }
        }

        // 优惠卷使用
        BigDecimal couponPrice = new BigDecimal(0.0);
        if(StringUtils.isNotEmpty(couponId)){
            MallCouponUser mallCouponUser = mallCouponUserService.findById(couponId);
            // 获取当前时间
            LocalDateTime nowTime= LocalDateTime.now();
            if(mallCouponUser == null || !mallCouponUser.getStatus().equals(CouponUserConstant.STATUS_USABLE)
                    || !((nowTime.isAfter(mallCouponUser.getStartTime()) && (nowTime.isBefore(mallCouponUser.getEndTime()))))){
                return ResponseUtil.fail("用户无法使用该优惠卷");
            }
            MallCoupon mallCoupon = mallCouponService.findById(mallCouponUser.getCouponId());

            if(mallCoupon.getCouponType() == 1){
                // 优惠券折扣券
                couponPrice = checkedGoodsPrice.subtract(checkedGoodsPrice.multiply(mallCoupon.getDiscount()));
            }else{
                // 优惠券代金券
                couponPrice = mallCoupon.getDiscount();
            }
        }

        // 可以使用的其他钱，例如用户积分
        BigDecimal integralPrice = new BigDecimal(0.00);

        // 订单费用
        BigDecimal orderTotalPrice = checkedGoodsPrice.add(freightPrice == null ? BigDecimal.ZERO : freightPrice)
                        .subtract(couponPrice);
        MallMerchant mallMerchant = mallMerchantService.getById(merchantId);
        if(mallMerchant == null && merchantId.equals("00")){
            mallMerchant = new MallMerchant();
            mallMerchant.setId("00");
            mallMerchant.setTitle("平台配送");
        }
        BigDecimal actualPrice = orderTotalPrice.subtract(integralPrice);

        // 商品商户信息
        MallMerchant goodsMerchant = mallMerchantService.getById(tempMerchantId);

        // 积分减免
        Boolean canUseReduction = false;
        FcAccount account = accountService.getUserAccount(userId);
        // 免减金额
        BigDecimal reductionPrice = BigDecimal.ZERO;
        if(mallMerchant != null && mallMerchant.getEmployIntegral() != null){
            if(realPrice.compareTo(mallMerchant.getEmployIntegral()) > -1){
                canUseReduction = true;

                BigDecimal integral = account.getIntegralAmount();
                // 账户可抵扣金额
                BigDecimal price = integral.divide(BigDecimal.valueOf(100));
                if(price.compareTo(mallMerchant.getDeduction()) > -1) {
                    // 用户积分大于等于商户设置的可抵扣积分
                    reductionPrice = mallMerchant.getDeduction();
                }else{
//                    price = price.setScale( 0, BigDecimal.ROUND_DOWN);
                    reductionPrice = price;
                }
                if(isUseIntegral){
                    realPrice = realPrice.subtract(price);
                    actualPrice = orderTotalPrice.subtract(reductionPrice);
                }
            }
        }

        if(actualPrice.compareTo(BigDecimal.ZERO) < 1){
            return ResponseUtil.fail("支付金额异常");
        }

        Map<String, Object> data = new HashMap<>();
        data.put("addressId", addressId);
        data.put("couponId", couponId);
        data.put("cartId", cartId);
        data.put("mallMerchant", goodsMerchant);
        data.put("chooseMerchant", mallMerchant);
        data.put("grouponRulesId", grouponRulesId);
        data.put("grouponPrice", grouponPrice);
        data.put("checkedAddress", checkedAddress);
        data.put("goodsTotalPrice", checkedGoodsPrice);
        data.put("reductionPrice", reductionPrice);
        data.put("canUseReduction", canUseReduction);
        // 运费
        data.put("freightPrice", freightPrice);
        data.put("couponPrice", couponPrice);
        data.put("orderTotalPrice", orderTotalPrice);
        data.put("actualPrice", actualPrice);
        data.put("checkedGoodsList", checkedGoodsList);
        data.put("account", account);
        data.put("realPrice", realPrice);
        return ResponseUtil.ok(data);
    }

    /**
     * 获取门店列表
     * @param addressId 工厂商户id
     * @return
     */
    @GetMapping("/getMerchantAtCheckout")
    public Object getMerchantAtCheckout(@LoginUser String userId,String addressId, @RequestParam(required = false) String name,
                                        @RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit,
                                        @RequestParam(required = false) String order, @RequestParam(required = false) String category){
        MallAddress mallAddress = mallAddressService.query(userId, addressId);
        if(mallAddress == null){
            return ResponseUtil.ok();
        }

        Map<String,Object> mallMerchants = new HashMap<>();

        MallUser mallUser = mallUserService.queryByUserId(userId);
        if(StringUtils.isNotEmpty(mallUser.getFirstLeader())){
            /**
             * 锁客商家
             * 用户商户关联表
             */
            QueryWrapper<MallUserMerchant> mallUserMerchantWrapper = new QueryWrapper<MallUserMerchant>();
            mallUserMerchantWrapper.eq("user_id", mallUser.getFirstLeader());
            List<MallUserMerchant> mallUserMerchant = mallUserMerchantService.list(mallUserMerchantWrapper);

            MallMerchant mallMerchant = null;
            for (MallUserMerchant temp: mallUserMerchant) {
                // 商户信息
                QueryWrapper<MallMerchant> merchantWrapper = new QueryWrapper<MallMerchant>();
                merchantWrapper.eq("status", 0);// 正常营业
                merchantWrapper.eq("audit_status", 1);// 审核通过
                merchantWrapper.eq("id",temp.getMerchantId());
                mallMerchant = mallMerchantService.getOne(merchantWrapper);

                if(mallMerchant != null){
                    break;
                }
            }
            List<MallMerchant> mallMerchantList = new ArrayList<>();
            mallMerchantList.add(mallMerchant);
            mallMerchants.put("data",mallMerchantList);
            mallMerchants.put("total",1);
            return ResponseUtil.ok(mallMerchants);
        }

        /**
         * 相同地区商户
         * 中级
         */

        mallMerchants = createMallMerchant(mallAddress.getCountyCode(),name,3,category,order,mallAddress.getLongitude(),mallAddress.getLatitude(),page,limit);
        if(Integer.parseInt(mallMerchants.get("total").toString()) > 0){
            return ResponseUtil.ok(mallMerchants);
        }
//        /**
//         * 相同地区商户
//         * 高级
//         */
//        mallMerchants = createMallMerchant(mallAddress.getCountyCode(),name,2,category,order,mallAddress.getLongitude(),mallAddress.getLatitude(),page,limit);
//        mallMerchantList.addAll(mallMerchants);
//        if(mallMerchantList.size() > 0){
//            return ResponseUtil.ok(mallMerchantList);
//        }
        /**
         * 相同地区商户
         * 初级
         */
        mallMerchants = createMallMerchant(mallAddress.getCountyCode(),name,0,category,order,mallAddress.getLongitude(),mallAddress.getLatitude(),page,limit);
        if(Integer.parseInt(mallMerchants.get("total").toString()) > 0){
            return ResponseUtil.ok(mallMerchants);
        }

        /**
         * 非相同地区商户
         */
        mallMerchants = createMallMerchant(null,name,null,category,order,mallAddress.getLongitude(),mallAddress.getLatitude(),page,limit);
        if(Integer.parseInt(mallMerchants.get("total").toString()) > 0){
            return ResponseUtil.ok(mallMerchants);
        }
        return ResponseUtil.ok(mallMerchants);
    }

    @Resource
    private MallCategoryService mallCategoryService;

    /**
     * 根据地区查商户
     * @param countyId  区县code
     * @param level     门店等级
     * @param category  一级分类id
     * @param lng
     * @param lat
     * @param name      门店名称
     * @return
     */
    private Map<String,Object> createMallMerchant(String countyId,String name,Integer level,String category,String order,String lng,String lat,Integer page, Integer limit){
        Map<String, Object> map = new HashMap<>();
        map.put("lng", lng);
        map.put("lat", lat);
        map.put("level",level);
        map.put("type", 1);//查询经销商
        if (ObjectUtil.isNotNull(name) && ObjectUtil.isNotEmpty(name)) {
            map.put("name", name);
        }
        if (ObjectUtil.isNotNull(countyId) && ObjectUtil.isNotEmpty(countyId)) {
            map.put("countyId",countyId);
        }

        if (ObjectUtil.isNotNull(category) && ObjectUtil.isNotEmpty(category)) {
            List<MallCategory> categories = mallCategoryService.getFangtaoCategory("zyCategory");
            List<String> categoryIds = new ArrayList<>();
            for (MallCategory item : categories) {
                if (item.getId().equals(category)) {
                    categoryIds.add(item.getId());
                    for (MallCategory it : item.getChildren()) {
                        categoryIds.add(it.getId());
                    }
                }
            }
            map.put("category", categoryIds);
        }
        String sort = "distance";
        if (ObjectUtil.isNotNull(order) && ObjectUtil.isNotEmpty(order)) {
            sort = order + " desc";
            if("distance".equals(order)){
                sort = "distance";
            }
        }
        PageHelper.startPage(page,limit,sort);
        List<MallMerchant> data = mallMerchantService.selectMerchantList(map);
        long total = PageInfo.of(data).getTotal();
        if(ObjectUtil.isNull(countyId) && page == 1){
            // 平台
            MallMerchant mallMerchant = new MallMerchant();
            mallMerchant.setTitle("平台配送");
            mallMerchant.setId("00");
            data.add(mallMerchant);
            total++;
        }
        Map<String,Object> result = new HashMap<>();
        result.put("data",data);
        result.put("total", total);

        return result;
    }
    /**
     * 购物车下单(立即购买)
     *
     * @param userId 用户ID
     * @param data
     * @return 购物车操作结果
     * @throws Exception
     */
    @PostMapping("wlkCheckout")
    public Object wlkCheckout(@LoginUser String userId, @RequestBody String data) throws Exception {
        if (data == null) {
            return ResponseUtil.badArgument();
        }

        String sign = JacksonUtil.parseString(data, "sign");
        // 手机号
        String mobile = JacksonUtil.parseString(data, "mobile");
        String cartId = JacksonUtil.parseString(data, "cartId");


        Map<String, Object> resultData = new HashMap<>();
        // String userId = "";
        // List<MallUser> users = userService.queryByMobile(mobile);
        // if (CollectionUtils.isNotEmpty(users)) {
        // if (org.apache.commons.lang3.StringUtils.isNotEmpty(users.get(0).getId())) {
        // userId = users.get(0).getId();
        // }
        // } else {
        // return ResponseUtil.fail(501, "用户不存在!");
        // }


        if (null == userId) {
            Object result = UserUtils.getTokenUserId(mobile);
            if (!Objects.equals(result, null)) {
                userId = String.valueOf(result);
            }
        }



        // 团购优惠
        // BigDecimal grouponPrice = new BigDecimal(0.00);
        // MallGrouponRules grouponRules = grouponRulesService.queryByIdV2(grouponRulesId);
        // if (grouponRules != null) {
        // grouponPrice = grouponRules.getDiscount();
        // }

        // 商品价格
        List<MallCart> checkedGoodsList = null;
        if (cartId == null || cartId.equals("0")) {
            checkedGoodsList = cartService.queryByUidAndChecked(userId);
        } else {
            MallCart cart = cartService.findById(cartId);
            if (cart == null) {
                return ResponseUtil.badArgumentValue();
            }
            checkedGoodsList = new ArrayList<>(1);
            checkedGoodsList.add(cart);
            MallGoods goods=goodsService.findById(cart.getGoodsId());
            resultData.put("virtualGood", goods.getVirtualGood());
        }
        BigDecimal checkedGoodsPrice = new BigDecimal(0.00);

        BigDecimal freightPrice = new BigDecimal(0.00);
        if (CollectionUtils.isNotEmpty(checkedGoodsList)) {

            List<String> gids = checkedGoodsList.stream().map(MallCart::getGoodsId).collect(Collectors.toList());
            // 求出购物车中的商品的全部运费总和
            freightPrice = goodsService.queryTotalPostage(gids);
            for (MallCart cart : checkedGoodsList) {
                checkedGoodsPrice = checkedGoodsPrice.add(cart.getPrice().multiply(new BigDecimal(cart.getNumber())));

            }
            // if (checkedGoodsPrice.compareTo(SystemConfig.getFreightLimit()) < 0) {
            //
            // freightPrice = SystemConfig.getFreight();
            // }
        }

        // 根据订单商品总价计算运费，多个商品满88则免运费，否则8元；如果是单个商品则按商品的邮费计算（废掉暂时保留代码）
        // else if (CollectionUtils.isNotEmpty(checkedGoodsList) && checkedGoodsList.get(0).getChecked() ==
        // true) {
        // String productId = checkedGoodsList.get(0).getProductId();
        // JSONObject obj = goodsService.queryGoodsByProductId(productId);
        // if (Objects.equals(null, obj)) {
        // return ResponseUtil.fail(507, "商品信息有误!");
        // }
        // freightPrice = new BigDecimal(obj.getString("postage"));
        // checkedGoodsPrice = checkedGoodsList.get(0).getPrice()
        // .multiply(new BigDecimal(checkedGoodsList.get(0).getNumber()));
        // }

        // 可以使用的其他钱，例如用户积分
        BigDecimal integralPrice = new BigDecimal(0.00);

        // 订单费用
        BigDecimal orderTotalPrice = checkedGoodsPrice.add(freightPrice == null ? BigDecimal.ZERO : freightPrice);
        BigDecimal actualPrice = orderTotalPrice.subtract(integralPrice);



        resultData.put("cartId", cartId);
        resultData.put("goodsTotalPrice", checkedGoodsPrice);
        // resultData.put("grouponPrice", grouponPrice);
        // 运费
        resultData.put("freightPrice", freightPrice);
        resultData.put("orderTotalPrice", orderTotalPrice);
        resultData.put("actualPrice", actualPrice);
        resultData.put("checkedGoodsList", checkedGoodsList);
        return ResponseUtil.ok(resultData);
    }


    /**
     * 团购立即下单回显信息
     *
     * @throws Exception
     */

    @PostMapping("wlkGroupCheckout")
    public Object wlkGroupCheckout(@LoginUser String userId, @RequestBody String data) throws Exception {
        if (data == null) {
            return ResponseUtil.badArgument();
        }

        // 手机号
        String mobile = JacksonUtil.parseString(data, "mobile");


        // 团购专用
        String grouponRulesId = JacksonUtil.parseString(data, "grouponRulesId");
        // String grouponLinkId = JacksonUtil.parseString(data, "grouponLinkId");

        String groupProductId = JacksonUtil.parseString(data, "groupProductId");

        Integer buyNum = JacksonUtil.parseInteger(data, "buyNum");



        if (null == userId) {
            Object result = UserUtils.getTokenUserId(mobile);
            if (!Objects.equals(result, null)) {
                userId = String.valueOf(result);
            }
        }

        // 如果是团购项目,验证活动是否有效
        MallGrouponRules rules = null;
        MallGrouponRules grouponRules = null;
        if (grouponRulesId != null && !"0".equals(grouponRulesId)) {
            // MallGrouponRules rules = grouponRulesService.queryById(grouponRulesId);
            rules = grouponRulesService.queryByIdV2(grouponRulesId);
            // 找不到记录
            if (rules == null) {
                return ResponseUtil.badArgument();
            }
            // 团购活动已经过期
            if (grouponRulesService.isExpired(rules)) {
                return ResponseUtil.fail(WxResponseCode.GROUPON_EXPIRED, "团购活动已过期!");
            }
             grouponRules = grouponRulesService.queryById(grouponRulesId);
        }


        BigDecimal checkedGoodsPrice = new BigDecimal(0.00);
        // 根据订单商品总价计算运费，多个商品满88则免运费，否则8元；如果是单个商品则按商品的邮费计算
        BigDecimal freightPrice = new BigDecimal(0.00);

        // 团购优惠
        BigDecimal grouponDiscountPrice = new BigDecimal(0.00);
        // 团购价格
        BigDecimal grouponPrice = new BigDecimal(0.00);

        if (!grouponRulesId.equals(0) || !"0".equals(grouponRulesId)) {
            if (rules != null) {
                grouponDiscountPrice = rules.getDiscount();
                grouponPrice=rules.getGroupPrice();
            }
        }
        JSONObject obj = goodsService.queryGoodsByProductId(groupProductId);

        MallActivityGoodsProduct product = mallActivityGoodsProductService.getOneByProductId(MallActivityEnum.ACTIVITY_GOODS_2.strCode2Int(), groupProductId);
        if (null == product) {
            return ResponseUtil.fail("支付失败! 该产品没有参与团购");
        }
        if (Objects.equals(null, obj)) {
            return ResponseUtil.fail(507, "商品信息有误!");
        }
        freightPrice = new BigDecimal(obj.getString("postage"));

//        if (null!=rules.getGroupPrice()) {
        checkedGoodsPrice =product.getActivityPrice().multiply(new BigDecimal(buyNum));
//        }else {
//            checkedGoodsPrice = (product.getActivityPrice()).subtract(grouponDiscountPrice)
//                            .multiply(new BigDecimal(buyNum));
//        }

//        if (buyNum > 1) {
//            if (checkedGoodsPrice.compareTo(SystemConfig.getFreightLimit()) < 0) {
//                freightPrice = SystemConfig.getFreight();
//            }
//        }


        // 团购优惠
        // BigDecimal grouponPrice = new BigDecimal(0.00);
        // MallGrouponRules grouponRules = grouponRulesService.queryByIdV2(grouponRulesId);
        // if (grouponRules != null) {
        // grouponPrice = grouponRules.getDiscount();
        // }

        // 可以使用的其他钱，例如用户积分
        BigDecimal integralPrice = new BigDecimal(0.00);

        //查询分润比例
        QueryWrapper<MallUserProfitNew> upwWrapper = new QueryWrapper<>();
        upwWrapper.eq("goods_id",obj.getString("gid")).eq("profit_type",7);
        MallUserProfitNew userProfitNew = userProfitNewService.getOne(upwWrapper);
        BigDecimal orderTotalPrice =  new BigDecimal(0.00);
        //支付模式2 （预付款加尾款的情况下先算出去预付款价格）
        if (!ObjectUtils.isEmpty(userProfitNew) && 2 == grouponRules.getPaymode()){
            // 订单费用
            orderTotalPrice = new BigDecimal(obj.getString("face_value")).multiply(userProfitNew.getOrdinaryUsersProfit()).add(freightPrice == null ? BigDecimal.ZERO : freightPrice).setScale(2,BigDecimal.ROUND_HALF_UP);
        }else{
            // 订单费用
            orderTotalPrice = checkedGoodsPrice.add(freightPrice == null ? BigDecimal.ZERO : freightPrice);
        }

        BigDecimal actualPrice = orderTotalPrice.subtract(integralPrice);
        Map<String, Object> resultData = new HashMap<>();
        resultData.put("goodsTotalPrice", checkedGoodsPrice);
        resultData.put("grouponPrice", checkedGoodsPrice);
        // 运费
        resultData.put("freightPrice", freightPrice);
        resultData.put("orderTotalPrice", orderTotalPrice);
        resultData.put("actualPrice", actualPrice);
        resultData.put("paymdoe",grouponRules == null? null : grouponRules.getPaymode());

//        QueryWrapper<MallGoodsPosition> gpWrapper = new QueryWrapper<>();
//        gpWrapper.eq("goods_id",obj.getString("gid"));
//        MallGoodsPosition one = mallGoodsPositionService.getOne(gpWrapper);
//        if (one != null){
//            resultData.put("goodsPosition",one.getType());
//        }

        return ResponseUtil.ok(resultData);



    }

    /**
     * 校验购物车中的商品
     *
     * @param userId
     * @param cartId
     * @return
     */
    @GetMapping("checkout/goods")
    public Object checkoutCart(@LoginUser String userId, String cartId) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }


        // 商品价格
        List<MallCart> checkedGoodsList = null;
        if (cartId == null || cartId.equals("0")) {
            checkedGoodsList = cartService.queryByUidAndChecked(userId);
        } else {
            MallCart cart = cartService.findById(cartId);
            if (cart == null) {
                return ResponseUtil.badArgumentValue();
            }
            checkedGoodsList = new ArrayList<>(1);
            checkedGoodsList.add(cart);
        }

        Map<String, Object> data = new HashMap<>();
        data.put("cartId", cartId);
        data.put("checkedGoodsList", checkedGoodsList);
        return ResponseUtil.ok(data);
    }



    /**
     * 校验购物车中的商品
     *
     * @param data
     * @return
     */
    @PostMapping("checkout/wlkGoods")
    public Object checkoutWlkCart(@RequestBody String data) {
        if (data == null) {
            return ResponseUtil.badArgument();
        }

        String sign = JacksonUtil.parseString(data, "sign");
        // 手机号
        String mobile = JacksonUtil.parseString(data, "mobile");
        String cartId = JacksonUtil.parseString(data, "cartId");

        String userId = "";
        List<MallUser> users = userService.queryByMobile(mobile);
        if (CollectionUtils.isNotEmpty(users)) {
            if (org.apache.commons.lang3.StringUtils.isNotEmpty(users.get(0).getId())) {
                userId = users.get(0).getId();
            }
        } else {
            return ResponseUtil.fail(501, "用户不存在!");
        }

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("cartId", cartId);
        paramMap.put("mobile", mobile);
        String string = JSONObject.toJSONString(paramMap);// 请求参数转换成字符串进行加密处理
        boolean flag = Md5Utils.verifySignature(string, sign);
        if (!flag) {
            return ResponseUtil.fail(403, "无效签名");
        }


        // 商品价格
        List<MallCart> checkedGoodsList = null;
        if (cartId == null || cartId.equals("0")) {
            checkedGoodsList = cartService.queryByUidAndChecked(userId);
        } else {
            MallCart cart = cartService.findById(cartId);
            if (cart == null) {
                return ResponseUtil.badArgumentValue();
            }
            checkedGoodsList = new ArrayList<>(1);
            checkedGoodsList.add(cart);
        }

        Map<String, Object> resultData = new HashMap<>();
        resultData.put("cartId", cartId);
        resultData.put("checkedGoodsList", checkedGoodsList);
        return ResponseUtil.ok(resultData);
    }



}
