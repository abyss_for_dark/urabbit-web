package com.graphai.mall.wx.web.statistics;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.mall.db.bo.MallIRealTimeOrderBo;
import com.graphai.mall.db.bo.MallUserBo;
import com.graphai.mall.db.bo.TradingVolumeBo;
import com.graphai.mall.db.bo.TradingVolumeOrdersTodayBo;
import com.graphai.mall.db.service.account.FcAccountRechargeBillService;
import com.graphai.mall.db.service.order.MallOrderService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.validator.Order;
import com.graphai.validator.Sort;
import com.alibaba.fastjson.JSON;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * 未来生活实时数据
 */
@RestController
@RequestMapping("/wx/realtimedata")
@Validated
public class WxRealTimeDataController {
    private final Log logger = LogFactory.getLog(WxRealTimeDataController.class);

    @Autowired
    private MallOrderService mallOrderService;
    @Autowired
    private MallUserService mallUserService;
    @Autowired
    private FcAccountRechargeBillService fcAccountRechargeBillService;

    /**
     * 1.实时订单交易
     * @param page 页数
     * @param limit 行数
     * @param sort 排序字段
     * @param order 排序方式
     * @return String
     */
    @GetMapping("/orderList")
    public String orderList(@RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "20") Integer limit,
                       @Sort @RequestParam(defaultValue = "id") String sort,
                       @Order @RequestParam(defaultValue = "desc") String order) {
        Integer start = (page - 1) * limit;
        List<MallIRealTimeOrderBo> data = mallOrderService.realtimedataOrderListWX(page, start,limit, sort, order);
        return JSON.toJSONString(data);
    }

    /**
     * 2.最新会员开卡数据
     * @param page 页数
     * @param limit 行数
     * @param sort 排序字段
     * @param order 排序方式
     * @return String
     */
    @GetMapping("/userCardList")
    public String userCardList(@RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "20") Integer limit,
                       @Sort @RequestParam(defaultValue = "id") String sort,
                       @Order @RequestParam(defaultValue = "desc") String order) {
        List<MallUserBo> data = mallUserService.realtimedataCardListWX(page,limit, sort, order);
        return JSON.toJSONString(data);
    }

    /**
     * 3.实时交易额-【暂停使用】
     * @return Object
     */
    @GetMapping("/tradingVolume")
    public Object tradingVolume() {
        TradingVolumeBo tradingVolumeBo = fcAccountRechargeBillService.realtimeDataTradingVolume();
        return ResponseUtil.ok(tradingVolumeBo);
    }
    /**
     * 3.1实时交易额-实时交易总额
     * @return String
     */
    @GetMapping("/tradingTotalAmount")
    public String tradingTotalAmount() {
        return fcAccountRechargeBillService.getTradingTotalAmount();
    }
    /**
     * 3.2实时交易额-总用户数
     * @return String
     */
    @GetMapping("/totalUsers")
    public String totalUsers() {
        return fcAccountRechargeBillService.getTotalUsers();
    }
    /**
     * 3.3实时交易额-4个分润
     * @return String
     */
    @GetMapping("/fourFenfun")
    public String fourFenfun() {
        return fcAccountRechargeBillService.getFourFenfun();
    }


    /**
     * 4.今日订单量实时统计【暂停使用】
     * @return Object
     */
    @GetMapping("/tradingOrdersToday")
    public Object tradingOrdersToday() {
        TradingVolumeOrdersTodayBo tradingVolumeOrdersTodayBo = fcAccountRechargeBillService.tradingOrdersToday();
        return ResponseUtil.ok(tradingVolumeOrdersTodayBo);
    }

    /**
     * 4.1未来生活实时数据墙-今日订单量实时统计-电商订单量
     * @return String
     */
    @GetMapping("/commerceOrderQuantity")
    public String commerceOrderQuantity() {
        return fcAccountRechargeBillService.getCommerceOrderQuantity();
    }
    /**
     * 4.2未来生活实时数据墙-今日订单量实时统计-权益订单量
     * @return String
     */
    @GetMapping("/equityOrderVolume")
    public String equityOrderVolume() {
        return fcAccountRechargeBillService.getEquityOrderVolume();
    }
    /**
     * 4.3未来生活实时数据墙-今日订单量实时统计-电商交易额
     * @return String
     */
    @GetMapping("/transactionAmount")
    public String transactionAmount() {
        return fcAccountRechargeBillService.getTransactionAmount();
    }
    /**
     * 4.4未来生活实时数据墙-今日订单量实时统计-权益交易额
     * @return String
     */
    @GetMapping("/equityTradingAmount")
    public String equityTradingAmount() {
        return fcAccountRechargeBillService.getEquityTradingAmount();
    }
    /**
     * 4.5未来生活实时数据墙-今日订单量实时统计-今日订单量
     * @return String
     */
    @GetMapping("/ordersToday")
    public String getOrdersToday() {
        return fcAccountRechargeBillService.getOrdersToday();
    }
    /**
     * 4.6未来生活实时数据墙-今日订单量实时统计-今日订单量变差化
     * @return String
     */
    @GetMapping("/ordersTodayChange")
    public String getOrdersTodayChange() {
        return fcAccountRechargeBillService.getOrdersTodayChange();
    }
    /**
     * 4.6.1未来生活实时数据墙-今日订单量变化量-购物
     * @return String
     */
    @GetMapping("/shopping")
    public String getBhlShopping() {
        return fcAccountRechargeBillService.getBhlShopping();
    }
    /**
     * 4.6.2未来生活实时数据墙-今日订单量变化量-餐饮
     * @return String
     */
    @GetMapping("/restaurant")
    public String getBhlRestaurant() {
        return fcAccountRechargeBillService.getBhlRestaurant();
    }
    /**
     * 4.6.3未来生活实时数据墙-今日订单量变化量-出行
     * @return String
     */
    @GetMapping("/travel")
    public String getBhlTravel() {
        return fcAccountRechargeBillService.getBhlTravel();
    }
    /**
     * 4.6.4未来生活实时数据墙-今日订单量变化量-娱乐
     * @return String
     */
    @GetMapping("/entertainment")
    public String getBhlEntertainment() {
        return fcAccountRechargeBillService.getBhlEntertainment();
    }
    /**
     * 4.6.5未来生活实时数据墙-今日订单量变化量-车主
     * @return String
     */
    @GetMapping("/owner")
    public String getBhlOwner() {
        return fcAccountRechargeBillService.getBhlOwner();
    }
    /**
     * 4.6.6未来生活实时数据墙-今日订单量变化量-礼品卡
     * @return String
     */
    @GetMapping("/giftCard")
    public String getBhlGiftCard() {
        return fcAccountRechargeBillService.getBhlGiftCard();
    }

}
