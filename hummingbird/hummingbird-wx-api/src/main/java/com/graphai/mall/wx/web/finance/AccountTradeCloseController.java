package com.graphai.mall.wx.web.finance;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.lock.MutexLock;
import com.graphai.commons.util.lock.MutexLockUtils;
import com.graphai.commons.util.json.JacksonUtils;
import com.graphai.mall.db.domain.FcAccount;
import com.graphai.mall.db.domain.FcAccountFreezeBill;
import com.graphai.mall.db.service.account.AccountService;
import com.graphai.mall.db.service.account.CapitalAccountUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api_gateway", consumes = { MediaType.MULTIPART_FORM_DATA_VALUE,
		MediaType.APPLICATION_ATOM_XML_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE,
		MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE, MediaType.TEXT_HTML_VALUE,
		MediaType.TEXT_XML_VALUE, MediaType.APPLICATION_OCTET_STREAM_VALUE })
public class AccountTradeCloseController {

	private final Log log = LogFactory.getLog(AccountTradeCloseController.class);
	@Autowired
	private AccountService accountManager;

	@RequestMapping(value = "/account.trade.close", method = RequestMethod.POST)
	public Object accountTradeColse(HttpServletResponse response, HttpServletRequest request,
			@RequestBody String paramJson) throws Exception {

		Map<String, Object> paramsMap = JacksonUtils.jsn2map(paramJson, String.class, Object.class);
		String apsKey = MapUtils.getString(paramsMap, "apsKey");
		String freezeAcBillId = MapUtils.getString(paramsMap, "freezeAcBillId");
		// 传过来的单位是元,需要转换成分
		// String sn = MapUtils.getString(paramsMap, "sn");

		log.info("接收参数apsKey：" + apsKey);
		log.info("接收参数freezeAcBillId：" + freezeAcBillId);
		MutexLock mutexLock = MutexLockUtils.getMutexLock(apsKey);
		synchronized (mutexLock) {
			// 账户不存在
			FcAccount oldCustomerAccount = accountManager.getAccountByKey(apsKey);
			// 校验账户状态
			FcAccountFreezeBill accountFreezeBill = accountManager.getFcAccountFreezeBillById(freezeAcBillId);
			if (accountFreezeBill == null) {
				return ResponseUtil.fail(-16, "冻结流水ID不存在");
			}

			Object obj = CapitalAccountUtils.validationAccount(oldCustomerAccount);
			if (obj != null) {
				return obj;
			}


			FcAccount releaseAccount = new FcAccount();
			releaseAccount.setAccountId(oldCustomerAccount.getAccountId());
			releaseAccount.setAmount(accountFreezeBill.getFreezeUncashAmt());
			releaseAccount.setFreezeUncashAmount(accountFreezeBill.getFreezeUncashAmt());
			// 释放账户金额
			accountManager.accountTradeClose(oldCustomerAccount, releaseAccount, paramsMap);

			MutexLockUtils.remove(apsKey);
		}

		return ResponseUtil.ok("释放金额成功");
	}

}
