package com.graphai.mall.wx.web.test;

import java.util.HashMap;
import java.util.Map;

import org.apache.http.Header;
import org.apache.http.message.BasicHeader;

import com.graphai.commons.util.rsa.RSA;
import com.graphai.mall.wx.service.UserTokenManager;
import com.graphai.mall.wx.util.AESUtils;

public class RedPacketTestController {

    
    public static void main(String[] args) {
        try {
            String token = UserTokenManager.generateToken("5004220396916965376", "", "");
            
            String rsaPublicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCmQnuOdm9R42GhvfGhNRFKJzcg+8IjTrDRbT9U0U/ZwqsNID4as1Qzjno5D29RLU4+iipNSgK4fVCY2GLWJ9iIc8f4qfcFKOgPP0wfebvfcy876PNakWyGd+XZyaexqelgbiL7wBQid+gsZCGugXsVxBHUH0FzI5gbyZxY70KrdQIDAQAB";
            String adId = "WLSHRewardedVideoAd121616046842999";
            String key = System.currentTimeMillis()+"";
            //String miwen = AesUtil.decrypt(adId, key);
            String aesKey = "xxx" + key;
            String encodeAdId = AESUtils.encodeToBase64String(adId, aesKey.getBytes(), null);
            
            String minWenParams = "time=13:00&timestamp="+key;
            
            String encryptParams =  RSA.encrypt(minWenParams, rsaPublicKey);
            String url = "http://test.weilaylife.com/wx/gamejackpot/receive/V2";
            Map<String, String> params = new HashMap<String, String>();
            params.put("params", encryptParams);
            Header[] headers = new BasicHeader[] {new BasicHeader("X-Mall-Token", token), new BasicHeader("X-Mall-Ad", encodeAdId)};
            
            
            System.out.println("~~~~~~~~~~~~encodeAdId:" + encodeAdId);
            System.out.println("~~~~~~~~~~~~encryptParams:" + encryptParams);
            
            System.out.println("~~~~~~~~~~~~token:" + token);
            String value = String.valueOf(Long.valueOf("5239639649039699968") % 11);
            System.out.println(value);
            //System.out.println( "调用接口结果 ：" + HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(params), headers));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
