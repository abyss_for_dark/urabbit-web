package com.graphai.mall.wx.web.live;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.id.GraphaiIdGenerator;
import com.graphai.mall.admin.domain.MallActivityGoodsProduct;
import com.graphai.mall.admin.domain.MallLiveRoomProduct;
import com.graphai.mall.admin.domain.MallMerchant;
import com.graphai.mall.admin.domain.MallUserMerchant;
import com.graphai.mall.admin.service.IMallActivityGoodsProductService;
import com.graphai.mall.admin.service.IMallLiveRoomProductService;
import com.graphai.mall.admin.service.IMallMerchantService;
import com.graphai.mall.admin.service.IMallUserMerchantService;
import com.graphai.mall.db.domain.*;
import com.graphai.mall.db.service.account.AccountStatus;
import com.graphai.mall.db.service.goods.MallGoodsProductService;
import com.graphai.mall.db.service.goods.MallGoodsService;
import com.graphai.mall.db.service.order.MallOrderService;
import com.graphai.mall.db.service.sysconfig.MallSystemConfigService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.service.user.behavior.MallCollectService;
import com.graphai.mall.db.constant.CommonConstant;
import com.graphai.mall.db.util.LiveUtil;
import com.graphai.mall.db.util.OrderUtil;
import com.graphai.mall.db.util.StringUtils;
import com.graphai.mall.live.domain.MallLiveRoom;
import com.graphai.mall.live.domain.MallLiveRoomUser;
import com.graphai.mall.live.domain.TLSSigAPIv2;
import com.graphai.mall.live.service.IMallLiveRoomService;
import com.graphai.mall.live.service.IMallLiveRoomUserService;
import com.graphai.mall.live.vo.LiveVo;
import com.graphai.mall.wx.annotation.LoginUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * wx直播
 * @author longx
 * @date 2021/6/309:34
 */

@RestController
@RequestMapping("/wx/live")
@Validated
public class WxLiveController {
    @Autowired
    private IMallLiveRoomService mallLiveRoomService;
    @Autowired
    private MallUserService userService;
    @Autowired
    private IMallUserMerchantService mallUserMerchantService;
    @Autowired
    private IMallMerchantService mallMerchantService;
    @Autowired
    private IMallLiveRoomUserService mallLiveRoomUserService;
    @Autowired
    private MallCollectService mallCollectService;
    @Autowired
    private IMallLiveRoomProductService mallLiveRoomProductService;
    @Autowired
    private MallGoodsService mallGoodsService;
    @Autowired
    private MallSystemConfigService systemConfigService;
    @Autowired
    private MallOrderService mallOrderService;
    @Autowired
    private MallGoodsProductService mallGoodsProductService;
    @Autowired
    private IMallActivityGoodsProductService mallActivityGoodsProductService;
    @Autowired
    private MallUserService mallUserService;
    private final static Integer FAKEUSER = 10;

    /**
     * 获取即时通讯IM签名
     * @param userId
     * @return
     */
    @GetMapping("getSign")
    public Object getSign(String userId){
        TLSSigAPIv2 api = new TLSSigAPIv2();
        String sign = api.genUserSig(userId, 180*86400);
        return ResponseUtil.ok(sign);
    }
    /**
     * 获取直播间列表-观众
     * @param page
     * @param limit
     * @return
     */
    @GetMapping("/list")
    public Object getList(@LoginUser String userId,@RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit){
        List<String> ids = new ArrayList<>();
        MallUser mallUser = userService.queryByUserId(userId);
        if((mallUser.getLevelId().equals("0") || mallUser.getLevelId().equals("98")) ){
            // 普通用户和工厂能看到商户直播
            QueryWrapper<MallMerchant> wrapper = new QueryWrapper<MallMerchant>();
            wrapper.eq("type",1);
            wrapper.eq("status", 0);// 正常营业
            wrapper.eq("audit_status", 1);// 审核通过
            List<MallMerchant> mallMerchants = mallMerchantService.list(wrapper);

            for(MallMerchant temp: mallMerchants){
                QueryWrapper<MallUserMerchant> mallMerchantQueryWrapper = new QueryWrapper<MallUserMerchant>();
                mallMerchantQueryWrapper.eq("merchant_id",temp.getId());
                MallUserMerchant mallUserMerchant = mallUserMerchantService.getOne(mallMerchantQueryWrapper);
                if(mallUserMerchant != null){
                    ids.add(mallUserMerchant.getUserId());
                }
            }
        }else if((mallUser.getLevelId().equals("9") || mallUser.getLevelId().equals("10") || mallUser.getLevelId().equals("11"))){
            // 商家只能看到工厂直播
            QueryWrapper<MallMerchant> wrapper = new QueryWrapper<MallMerchant>();
            wrapper.eq("type",0);
            wrapper.eq("status", 0);// 正常营业
            wrapper.eq("audit_status", 1);// 审核通过
            List<MallMerchant> mallMerchants = mallMerchantService.list(wrapper);

            for(MallMerchant temp: mallMerchants){
                QueryWrapper<MallUserMerchant> mallMerchantQueryWrapper = new QueryWrapper<MallUserMerchant>();
                mallMerchantQueryWrapper.eq("merchant_id",temp.getId());
                MallUserMerchant mallUserMerchant = mallUserMerchantService.getOne(mallMerchantQueryWrapper);
                if(mallUserMerchant != null){
                    ids.add(mallUserMerchant.getUserId());
                }
            }
        }

        QueryWrapper<MallLiveRoom> wrapper = new QueryWrapper<MallLiveRoom>();
        wrapper.eq("deleted", 0);
        wrapper.eq("room_status", 1);
        wrapper.ne("add_id",userId);
        wrapper.in("add_id", ids);
        PageHelper.startPage(page, limit);

        List<MallLiveRoom> mallLiveRooms = mallLiveRoomService.list(wrapper);
        List<LiveVo> roomList = new ArrayList<>();
        for (MallLiveRoom temp: mallLiveRooms) {
            // 用户商户关联表
            QueryWrapper<MallUserMerchant> mallUserMerchantWrapper = new QueryWrapper<MallUserMerchant>();
            mallUserMerchantWrapper.eq("user_id", temp.getAddId());
            List<MallUserMerchant> mallUserMerchant = mallUserMerchantService.list(mallUserMerchantWrapper);

            MallMerchant mallMerchant = null;
            for (MallUserMerchant tempMerchant: mallUserMerchant) {
                // 商户信息
                QueryWrapper<MallMerchant> merchantWrapper = new QueryWrapper<MallMerchant>();
                merchantWrapper.eq("status", 0);// 正常营业
                merchantWrapper.eq("audit_status", 1);// 审核通过
                merchantWrapper.eq("id",tempMerchant.getMerchantId());
                mallMerchant = mallMerchantService.getOne(merchantWrapper);

                if(mallMerchant != null){
                    break;
                }
            }
            temp.setRoomWatchingNum(temp.getRoomWatchingNum() * FAKEUSER);
            LiveVo liveVo = new LiveVo();
            liveVo.setMallMerchant(mallMerchant);
            liveVo.setMallLiveRoom(temp);
            roomList.add(liveVo);
        }

        long total = PageInfo.of(mallLiveRooms).getTotal();

        Map<String, Object> map = new HashMap<>();
        map.put("total",total);
        map.put("roomList",roomList);
        return ResponseUtil.ok(map);
    }

    /**
     * 开通直播间-》生成该商户的直播间-主播
     * @return
     */
    @PostMapping("/openLiveRoom")
    public Object openLiveRoom(@LoginUser String userId){
        if (userId == null) {
            return ResponseUtil.unlogin();
        }

        // 用户商户关联表
        QueryWrapper<MallUserMerchant> mallUserMerchantWrapper = new QueryWrapper<MallUserMerchant>();
        mallUserMerchantWrapper.eq("user_id", userId);
        List<MallUserMerchant> mallUserMerchant = mallUserMerchantService.list(mallUserMerchantWrapper);

        MallMerchant mallMerchant = null;
        for (MallUserMerchant temp: mallUserMerchant) {
            // 商户信息
            QueryWrapper<MallMerchant> merchantWrapper = new QueryWrapper<MallMerchant>();
            merchantWrapper.eq("status", 0);// 正常营业
            merchantWrapper.eq("audit_status", 1);// 审核通过
            merchantWrapper.eq("id",temp.getMerchantId());
            mallMerchant = mallMerchantService.getOne(merchantWrapper);

            if(mallMerchant != null){
                break;
            }
        }

        if(mallMerchant == null){
            return ResponseUtil.fail("没该用户商户信息");
        }

        QueryWrapper<MallLiveRoom> liveRoomWrapper = new QueryWrapper<MallLiveRoom>();
        liveRoomWrapper.eq("add_id", userId);
        liveRoomWrapper.eq("deleted", false);
        MallLiveRoom liveRoom = mallLiveRoomService.getOne(liveRoomWrapper);
        if(liveRoom != null){
            return ResponseUtil.fail("该用户已有直播间");
        }

        MallLiveRoom mallLiveRoom = new MallLiveRoom();
        mallLiveRoom.setRoomImage(mallMerchant.getImg());
        mallLiveRoom.setRoomName(mallMerchant.getName());
        mallLiveRoom.setRoomStatus("2");// 下播状态
        mallLiveRoom.setDeleted(false);
        mallLiveRoom.setAddId(userId);
        mallLiveRoom.setAddTime(LocalDateTime.now());
        mallLiveRoomService.save(mallLiveRoom);
        return ResponseUtil.ok();
    }

    /**
     * 获取我的直播信息-主播
     * @param userId
     * @return
     */
    @GetMapping("liveInfo")
    public Object liveInfo(@LoginUser String userId){
        if (userId == null) {
            return ResponseUtil.unlogin();
        }

        QueryWrapper<MallLiveRoom> wrapper = new QueryWrapper<MallLiveRoom>();
        wrapper.eq("add_id", userId);
        MallLiveRoom mallLiveRoom = mallLiveRoomService.getOne(wrapper);

        if(mallLiveRoom == null){
            return ResponseUtil.fail("未开通直播间");
        }

        // 用户商户关联表
        QueryWrapper<MallUserMerchant> mallUserMerchantWrapper = new QueryWrapper<MallUserMerchant>();
        mallUserMerchantWrapper.eq("user_id", userId);
        List<MallUserMerchant> mallUserMerchant = mallUserMerchantService.list(mallUserMerchantWrapper);

        MallMerchant mallMerchant = null;
        for (MallUserMerchant temp: mallUserMerchant) {
            // 商户信息
            QueryWrapper<MallMerchant> merchantWrapper = new QueryWrapper<MallMerchant>();
            merchantWrapper.eq("status", 0);// 正常营业
            merchantWrapper.eq("audit_status", 1);// 审核通过
            merchantWrapper.eq("id",temp.getMerchantId());
            mallMerchant = mallMerchantService.getOne(merchantWrapper);

            if(mallMerchant != null){
                break;
            }
        }

        Map<String,Object> map = new HashMap<>();
        map.put("mallLiveRoom",mallLiveRoom);
        map.put("mallMerchant",mallMerchant);
        return ResponseUtil.ok(map);
    }

    /**
     * 修改直播间图片
     * @param userId
     * @param imgUrl
     * @return
     */
    @PostMapping("changeRoomImg")
    public Object changeRoomImg(@LoginUser String userId, String imgUrl){
        QueryWrapper<MallLiveRoom> wrapper = new QueryWrapper<MallLiveRoom>();
        wrapper.eq("add_id", userId);
        MallLiveRoom mallLiveRoom = mallLiveRoomService.getOne(wrapper);

        mallLiveRoom.setRoomImage(imgUrl);
        mallLiveRoomService.updateById(mallLiveRoom);
        return ResponseUtil.ok();
    }

    /**
     * 开启直播-主播
     * @return
     */
    @PostMapping("/startLive")
    @Transactional
    public Object startLive(@LoginUser String userId){
        if (userId == null) {
            return ResponseUtil.unlogin();
        }

        try{
            // 直播间
            QueryWrapper<MallLiveRoom> wrapper = new QueryWrapper<MallLiveRoom>();
            wrapper.eq("add_id", userId);
            MallLiveRoom mallLiveRoom = mallLiveRoomService.getOne(wrapper);

            // 生成直播推流/播放地址
            /***直播有效时长 1天***/
            int day = 1;
            long txTime = (System.currentTimeMillis() + (1000 * 60 * 60 * (24 * day)))/1000;
            String streamName = mallLiveRoom.getId();
            mallLiveRoom.setPushUrl(LiveUtil.getPushUrl(streamName, txTime));
            mallLiveRoom.setPlayUrl(LiveUtil.getPlayUrl(streamName));

            // 生成截图规则
//            LiveUtil.createCutRule(streamName);

            // 修改直播间状态
            mallLiveRoom.setRoomWatchingNum(0);
            mallLiveRoom.setWatchTotal(0);
            mallLiveRoom.setRoomStatus("1");
            mallLiveRoom.setStartLiveAt(LocalDateTime.now());
            mallLiveRoom.setEndLiveAt(null);
            mallLiveRoom.setUpdateTime(LocalDateTime.now());
            mallLiveRoom.setLastSellingGoodsTotal(0);
            mallLiveRoomService.updateById(mallLiveRoom);

            // 用户商户关联表
            QueryWrapper<MallUserMerchant> mallUserMerchantWrapper = new QueryWrapper<MallUserMerchant>();
            mallUserMerchantWrapper.eq("user_id", mallLiveRoom.getAddId());
            List<MallUserMerchant> mallUserMerchant = mallUserMerchantService.list(mallUserMerchantWrapper);

            MallMerchant mallMerchant = null;
            for (MallUserMerchant temp: mallUserMerchant) {
                // 商户信息
                QueryWrapper<MallMerchant> merchantWrapper = new QueryWrapper<MallMerchant>();
                merchantWrapper.eq("status", 0);// 正常营业
                merchantWrapper.eq("audit_status", 1);// 审核通过
                merchantWrapper.eq("id",temp.getMerchantId());
                mallMerchant = mallMerchantService.getOne(merchantWrapper);

                if(mallMerchant != null){
                    break;
                }
            }

            Map<String, Object> map = new HashMap<>();
            map.put("mallLiveRoom",mallLiveRoom);
            map.put("mallMerchant",mallMerchant);
            return ResponseUtil.ok(map);
        }catch(Exception e){
            return ResponseUtil.fail(e.getMessage());
        }
    }

    /**
     * 加入直播间-观众
     * @param userId
     * @param liveVo
     * @return
     */
    @PostMapping("joinLive")
    public Object joinLive(@LoginUser String userId, @RequestBody LiveVo liveVo){
        try {
            // 观看的直播间
            MallLiveRoom mallLiveRoom = mallLiveRoomService.getById(liveVo.getRoomId());
            if(mallLiveRoom == null){
                return ResponseUtil.fail("直播间不存在");
            }

            // 正在观看+1
            Integer people = mallLiveRoom.getRoomWatchingNum()+1;
            mallLiveRoom.setRoomWatchingNum(people);
            mallLiveRoom.setWatchTotal(mallLiveRoom.getWatchTotal()+1);
            mallLiveRoomService.updateById(mallLiveRoom);

            Map<String,Object> map = new HashMap<>();
            MallCollect mallCollect = null;
            if(!StringUtils.isEmpty(userId)){
                // 生成用户与直播间关系数据
                QueryWrapper<MallLiveRoomUser> mallLiveRoomUserQueryWrapper = new QueryWrapper<MallLiveRoomUser>();
                mallLiveRoomUserQueryWrapper.eq("user_id", userId);
                MallLiveRoomUser mallLiveRoomUser = mallLiveRoomUserService.getOne(mallLiveRoomUserQueryWrapper);
                if(mallLiveRoomUser == null){
                    mallLiveRoomUser = new MallLiveRoomUser();
                    mallLiveRoomUser.setUserId(userId);
                    mallLiveRoomUser.setRoomId(mallLiveRoom.getId());
                    mallLiveRoomUser.setAddTime(LocalDateTime.now());
                    mallLiveRoomUserService.save(mallLiveRoomUser);
                }else{
                    mallLiveRoomUser.setAddTime(LocalDateTime.now());
                    mallLiveRoomUserService.updateById(mallLiveRoomUser);
                }

                // 关注信息
                mallCollect = mallCollectService.queryByTypeAndValue(userId, (byte) 3, liveVo.getRoomId());
            }

            // 用户商户关联表
            QueryWrapper<MallUserMerchant> mallUserMerchantWrapper = new QueryWrapper<MallUserMerchant>();
            mallUserMerchantWrapper.eq("user_id", mallLiveRoom.getAddId());
            List<MallUserMerchant> mallUserMerchant = mallUserMerchantService.list(mallUserMerchantWrapper);

            MallMerchant mallMerchant = null;
            for (MallUserMerchant temp: mallUserMerchant) {
                // 商户信息
                QueryWrapper<MallMerchant> merchantWrapper = new QueryWrapper<MallMerchant>();
                merchantWrapper.eq("status", 0);// 正常营业
                merchantWrapper.eq("audit_status", 1);// 审核通过
                merchantWrapper.eq("id",temp.getMerchantId());
                mallMerchant = mallMerchantService.getOne(merchantWrapper);

                if(mallMerchant != null){
                    break;
                }
            }

            mallLiveRoom.setRoomWatchingNum(people * FAKEUSER);
            map.put("mallLiveRoom",mallLiveRoom);
            map.put("mallCollect",mallCollect);
            map.put("mallMerchant",mallMerchant);
            return ResponseUtil.ok(map);
        }catch (Exception e){
            return ResponseUtil.fail(e.getMessage());
        }
    }

    /**
     * 获取直播间观看用户
     * @param roomId
     * @return
     */
    @GetMapping("getLiveUser")
    public Object getLiveUser(String roomId,Integer limit){
        MallLiveRoom mallLiveRoom = mallLiveRoomService.getById(roomId);

        QueryWrapper<MallLiveRoomUser> mallLiveRoomUserQueryWrapper = new QueryWrapper<MallLiveRoomUser>();
        mallLiveRoomUserQueryWrapper.eq("room_id", roomId);
        mallLiveRoomUserQueryWrapper.orderByDesc("add_time");
        mallLiveRoomUserQueryWrapper.last("limit "+limit);
        List<MallLiveRoomUser> mallLiveRoomUsers = mallLiveRoomUserService.list(mallLiveRoomUserQueryWrapper);

        for (MallLiveRoomUser temp: mallLiveRoomUsers) {
            MallUser mallUser = mallUserService.queryByUserId(temp.getUserId());
            if(temp != null){
                temp.setUserUrl(mallUser.getAvatar());
            }
        }
        Map<String, Object> map = new HashMap<>();
        map.put("userCnt",mallLiveRoom.getRoomWatchingNum() * FAKEUSER);
        map.put("mallLiveRoomUsers",mallLiveRoomUsers);
        return ResponseUtil.ok(map);
    }

    /**
     * 退出直播间-观众
     * @param userId
     * @param liveVo
     * @return
     */
    @PostMapping("leaveLive")
    public Object leaveLive(@LoginUser String userId, @RequestBody LiveVo liveVo){
        try {
            if(!StringUtils.isEmpty(userId)){
                // 删除用户与直播间关系数据
                QueryWrapper<MallLiveRoomUser> wrapper = new QueryWrapper<MallLiveRoomUser>();
                wrapper.eq("user_id", userId);
                wrapper.eq("room_id", liveVo.getRoomId());
                mallLiveRoomUserService.remove(wrapper);
            }

            // 正在观看-1
            MallLiveRoom mallLiveRoom = mallLiveRoomService.getById(liveVo.getRoomId());
            mallLiveRoom.setRoomWatchingNum(mallLiveRoom.getRoomWatchingNum()-1);
            mallLiveRoomService.updateById(mallLiveRoom);
            return ResponseUtil.ok(mallLiveRoom);
        }catch (Exception e){
            return ResponseUtil.fail(e.getMessage());
        }
    }


    /**
     * 暂停直播-主播
     * @param liveVo
     * @return
     */
    @PostMapping("stopLive")
    public Object stopLive(@LoginUser String userId, @RequestBody LiveVo liveVo){
        if (userId == null) {
            return ResponseUtil.unlogin();
        }

        return ResponseUtil.ok(LiveUtil.stopPush(liveVo.getRoomId()));
    }

    /**
     * 重新开始直播-主播
     */
    @PostMapping("restartLive")
    public Object restartLive(@LoginUser String userId, @RequestBody LiveVo liveVo){
        if (userId == null) {
            return ResponseUtil.unlogin();
        }

        return ResponseUtil.ok(LiveUtil.startPush(liveVo.getRoomId()));
    }

    /**
     * 关注-观众
     * @param userId
     * @param liveVo
     * @return
     */
    @PostMapping("follow")
    public Object follow(@LoginUser String userId,@RequestBody LiveVo liveVo){
        if (userId == null) {
            return ResponseUtil.unlogin();
        }

        // 添加关注信息
        MallCollect mallCollect = new MallCollect();
        mallCollect.setAddTime(LocalDateTime.now());
        mallCollect.setUserId(userId);
        mallCollect.setType((byte) 3);

        MallLiveRoom mallLiveRoom = mallLiveRoomService.getById(liveVo.getRoomId());
        // 用户商户关联表
        QueryWrapper<MallUserMerchant> mallUserMerchantWrapper = new QueryWrapper<MallUserMerchant>();
        mallUserMerchantWrapper.eq("user_id", mallLiveRoom.getAddId());
        List<MallUserMerchant> mallUserMerchant = mallUserMerchantService.list(mallUserMerchantWrapper);

        MallMerchant mallMerchant = null;
        for (MallUserMerchant temp: mallUserMerchant) {
            // 商户信息
            QueryWrapper<MallMerchant> merchantWrapper = new QueryWrapper<MallMerchant>();
            merchantWrapper.eq("status", 0);// 正常营业
            merchantWrapper.eq("audit_status", 1);// 审核通过
            merchantWrapper.eq("id",temp.getMerchantId());
            mallMerchant = mallMerchantService.getOne(merchantWrapper);

            if(mallMerchant != null){
                break;
            }
        }
        mallCollect.setValueId(mallMerchant.getId());
        mallCollect.setMerchantId(mallMerchant.getId());
        mallCollectService.add(mallCollect);

        // 修改直播间信息
        mallLiveRoom.setRoomFans(mallLiveRoom.getRoomFans() + 1);
        mallLiveRoomService.updateById(mallLiveRoom);
        return ResponseUtil.ok();
    }

    /**
     * 取消关注-观众
     * @param userId
     * @param liveVo
     * @return
     */
    @PostMapping("unfollow")
    public Object unfollow(@LoginUser String userId,@RequestBody LiveVo liveVo){
        if (userId == null) {
            return ResponseUtil.unlogin();
        }

        // 删除关注信息
        MallCollect mallCollect = mallCollectService.queryByTypeAndValue(userId, (byte) 3, liveVo.getRoomId());
        if(mallCollect != null){
            mallCollectService.deleteById(mallCollect.getId());

            // 修改直播间信息
            MallLiveRoom mallLiveRoom = mallLiveRoomService.getById(liveVo.getRoomId());
            mallLiveRoom.setRoomFans(mallLiveRoom.getRoomFans() - 1);
            mallLiveRoomService.updateById(mallLiveRoom);
        }

        return ResponseUtil.ok();
    }

    /**
     * 获取该直播间商品-主播
     * @param userId
     * @param goodsName
     * @param type 0:橱窗商品；1：非橱窗商品；2：所有商品
     * @return
     */
    @GetMapping("getGoods")
    public Object getGoodsByRoomId(@LoginUser String userId, String goodsName, Integer type,@RequestParam(defaultValue = "1") Integer page,@RequestParam(defaultValue = "10") Integer limit){
        // 直播间
        QueryWrapper<MallLiveRoom> wrapper = new QueryWrapper<MallLiveRoom>();
        wrapper.eq("add_id", userId);
        MallLiveRoom mallLiveRoom = mallLiveRoomService.getOne(wrapper);
        if(mallLiveRoom == null){
            return ResponseUtil.fail("直播间不存在");
        }

        // 用户商户关联表
        QueryWrapper<MallUserMerchant> mallUserMerchantWrapper = new QueryWrapper<MallUserMerchant>();
        mallUserMerchantWrapper.eq("user_id", mallLiveRoom.getAddId());
        List<MallUserMerchant> mallUserMerchant = mallUserMerchantService.list(mallUserMerchantWrapper);

        MallMerchant mallMerchant = null;
        for (MallUserMerchant temp: mallUserMerchant) {
            // 商户信息
            QueryWrapper<MallMerchant> merchantWrapper = new QueryWrapper<MallMerchant>();
            merchantWrapper.eq("status", 0);// 正常营业
            merchantWrapper.eq("audit_status", 1);// 审核通过
            merchantWrapper.eq("id",temp.getMerchantId());
            mallMerchant = mallMerchantService.getOne(merchantWrapper);

            if(mallMerchant != null){
                break;
            }
        }

        if(mallMerchant == null){
            return ResponseUtil.fail("没该用户商户信息");
        }

        // 获取直播间的关联商品
        QueryWrapper<MallLiveRoomProduct> mallLiveRoomProductQueryWrapper = new QueryWrapper<MallLiveRoomProduct>();
        mallLiveRoomProductQueryWrapper.eq("room_id", mallLiveRoom.getId());
        mallLiveRoomProductQueryWrapper.orderByAsc("sort");

        List<MallLiveRoomProduct> mallLiveRoomProducts = new ArrayList<MallLiveRoomProduct>();
        if(type == 0){
            // 现有橱窗商品
            if(goodsName != null && !goodsName.equals("")){
                mallLiveRoomProductQueryWrapper.like("name ","%"+goodsName+"%");
            }

            long total = mallLiveRoomProductService.list(mallLiveRoomProductQueryWrapper).size();
            mallLiveRoomProductQueryWrapper.last("limit "+(page-1)*limit+","+limit);
            mallLiveRoomProducts = mallLiveRoomProductService.list(mallLiveRoomProductQueryWrapper);
            for (MallLiveRoomProduct temp:mallLiveRoomProducts) {
                Byte a = 1;
                temp.setIsLive(a);

                // 获取最便宜的价格
                QueryWrapper<MallActivityGoodsProduct> mallActivityGoodsProductQueryWrapper = new QueryWrapper<MallActivityGoodsProduct>();
                mallActivityGoodsProductQueryWrapper.eq("activity_rule_id", temp.getId());
                mallActivityGoodsProductQueryWrapper.orderByAsc("activity_price");
                MallActivityGoodsProduct mallActivityGoodsProduct = mallActivityGoodsProductService.list(mallActivityGoodsProductQueryWrapper).get(0);
                if(mallActivityGoodsProduct != null){
                    temp.setPrice(mallActivityGoodsProduct.getActivityPrice());
                }
            }

            Map<String, Object> data = new HashMap<>(16);
            data.put("total", total);
            data.put("items", mallLiveRoomProducts);
            return ResponseUtil.ok(data);
        }

        mallLiveRoomProducts = mallLiveRoomProductService.list(mallLiveRoomProductQueryWrapper);

        // 获取关联商品id
        List<String> ids = mallLiveRoomProducts.stream().map(MallLiveRoomProduct::getGoodsId).collect(Collectors.toList());
        Object data = mallGoodsService.findByIdListAndParams(ids, goodsName, type, mallMerchant.getId(),page,limit,mallLiveRoom.getId());
        return ResponseUtil.ok(data);
    }

    /**
     * 获取该直播间商品-观众
     * @param roomId
     * @param page
     * @param limit
     * @return
     */
    @GetMapping("getGoodsWithUser")
    public Object getGoodsWithUser(String roomId,@RequestParam(defaultValue = "1") Integer page,@RequestParam(defaultValue = "10") Integer limit){
        // 直播间
        MallLiveRoom mallLiveRoom = mallLiveRoomService.getById(roomId);
        if(mallLiveRoom == null){
            return ResponseUtil.fail("直播间不存在");
        }

        // 用户商户关联表
        QueryWrapper<MallUserMerchant> mallUserMerchantWrapper = new QueryWrapper<MallUserMerchant>();
        mallUserMerchantWrapper.eq("user_id", mallLiveRoom.getAddId());
        List<MallUserMerchant> mallUserMerchant = mallUserMerchantService.list(mallUserMerchantWrapper);

        MallMerchant mallMerchant = null;
        for (MallUserMerchant temp: mallUserMerchant) {
            // 商户信息
            QueryWrapper<MallMerchant> merchantWrapper = new QueryWrapper<MallMerchant>();
            merchantWrapper.eq("status", 0);// 正常营业
            merchantWrapper.eq("audit_status", 1);// 审核通过
            merchantWrapper.eq("id",temp.getMerchantId());
            mallMerchant = mallMerchantService.getOne(merchantWrapper);

            if(mallMerchant != null){
                break;
            }
        }

        if(mallMerchant == null){
            return ResponseUtil.fail("没该商户信息");
        }

        // 获取直播间的关联商品
        QueryWrapper<MallLiveRoomProduct> mallLiveRoomProductQueryWrapper = new QueryWrapper<MallLiveRoomProduct>();
        mallLiveRoomProductQueryWrapper.eq("room_id", mallLiveRoom.getId());
        mallLiveRoomProductQueryWrapper.orderByAsc("sort");

        List<MallLiveRoomProduct> mallLiveRoomProducts = new ArrayList<MallLiveRoomProduct>();

        // 现有橱窗商品
        long total = mallLiveRoomProductService.list(mallLiveRoomProductQueryWrapper).size();
        mallLiveRoomProductQueryWrapper.last("limit "+(page-1)*limit+","+limit);
        mallLiveRoomProducts = mallLiveRoomProductService.list(mallLiveRoomProductQueryWrapper);
        for (MallLiveRoomProduct temp: mallLiveRoomProducts) {
            // 获取最便宜的价格
            QueryWrapper<MallActivityGoodsProduct> mallActivityGoodsProductQueryWrapper = new QueryWrapper<MallActivityGoodsProduct>();
            mallActivityGoodsProductQueryWrapper.eq("activity_rule_id", temp.getId());
            mallActivityGoodsProductQueryWrapper.orderByAsc("activity_price");
            MallActivityGoodsProduct mallActivityGoodsProduct = mallActivityGoodsProductService.list(mallActivityGoodsProductQueryWrapper).get(0);
            if(mallActivityGoodsProduct != null){
                temp.setPrice(mallActivityGoodsProduct.getActivityPrice());
            }
        }

        Map<String, Object> data = new HashMap<>(16);
        data.put("total", total);
        data.put("items", mallLiveRoomProducts);
        return ResponseUtil.ok(data);
    }

    /**
     * 添加商品到橱窗-主播
     * @param userId
     * @param liveVo
     * @return
     */
    @PostMapping("addGoods")
    public Object addGoods(@LoginUser String userId, @RequestBody LiveVo liveVo){
        // 直播间
        QueryWrapper<MallLiveRoom> wrapper = new QueryWrapper<MallLiveRoom>();
        wrapper.eq("add_id", userId);
        MallLiveRoom mallLiveRoom = mallLiveRoomService.getOne(wrapper);
        // 商品
        MallGoods mallGoods = mallGoodsService.findById(liveVo.getGoodsId());

        // 添加关联信息
        MallLiveRoomProduct mallLiveRoomProduct = new MallLiveRoomProduct();
        mallLiveRoomProduct.setGoodsId(liveVo.getGoodsId());
        mallLiveRoomProduct.setRoomId(mallLiveRoom.getId());
        mallLiveRoomProduct.setAddId(userId);
        mallLiveRoomProduct.setAddTime(LocalDateTime.now());
        mallLiveRoomProduct.setDeleted(true);
        mallLiveRoomProduct.setIsTalk(false);
        mallLiveRoomProduct.setPrice(mallGoods.getActualPrice());
//        mallLiveRoomProduct.setStockNumber(mallGoodsProduct.getNumber());
        mallLiveRoomProduct.setPicUrl(mallGoods.getPicUrl());
        mallLiveRoomProduct.setName(mallGoods.getName());
        mallLiveRoomProduct.setBrief(mallGoods.getBrief());
        mallLiveRoomProduct.setPrice(mallGoods.getRetailPrice());
        mallLiveRoomProductService.save(mallLiveRoomProduct);

        // 添加规格
        List<MallGoodsProduct> mallGoodsProducts = mallGoodsProductService.queryByGid(mallGoods.getId());
        for (MallGoodsProduct temp: mallGoodsProducts) {
            MallActivityGoodsProduct mallActivityGoodsProduct = new MallActivityGoodsProduct();
            mallActivityGoodsProduct.setActivityType(1);// 直播
            mallActivityGoodsProduct.setActivityRuleId(mallLiveRoomProduct.getId());
            mallActivityGoodsProduct.setGoodsId(mallGoods.getId());
            mallActivityGoodsProduct.setProductId(temp.getId());
            mallActivityGoodsProduct.setSpecifications(temp.getSpecifications());
            mallActivityGoodsProduct.setActivityPrice(temp.getPrice());
            mallActivityGoodsProduct.setNumber(temp.getNumber());
//            mallActivityGoodsProduct.setSurplusNumber(temp.get) 活动剩余数量
            mallActivityGoodsProduct.setUrl(temp.getUrl());
            mallActivityGoodsProduct.setAddTime(LocalDateTime.now());
            mallActivityGoodsProduct.setDeleted(false);
            mallActivityGoodsProductService.save(mallActivityGoodsProduct);
        }
        return ResponseUtil.ok();
    }

    /**
     * 批量添加-主播
     * @param userId
     * @param liveVo
     * @return
     */
    @PostMapping("addAllGoods")
    public Object addAllGoods(@LoginUser String userId, @RequestBody LiveVo liveVo){
        // 直播间
        QueryWrapper<MallLiveRoom> wrapper = new QueryWrapper<MallLiveRoom>();
        wrapper.eq("add_id", userId);
        MallLiveRoom mallLiveRoom = mallLiveRoomService.getOne(wrapper);

        List<String> ids = Arrays.asList(liveVo.getGoodsIds());
        for (String temp: ids) {
            // 商品
            MallGoods mallGoods = mallGoodsService.findById(temp);

            // 添加关联信息
            MallLiveRoomProduct mallLiveRoomProduct = new MallLiveRoomProduct();
            mallLiveRoomProduct.setGoodsId(temp);
            mallLiveRoomProduct.setRoomId(mallLiveRoom.getId());
            mallLiveRoomProduct.setAddId(userId);
            mallLiveRoomProduct.setAddTime(LocalDateTime.now());
            mallLiveRoomProduct.setDeleted(false);
            mallLiveRoomProduct.setIsTalk(false);
            mallLiveRoomProduct.setPicUrl(mallGoods.getPicUrl());
            mallLiveRoomProduct.setName(mallGoods.getName());
            mallLiveRoomProduct.setBrief(mallGoods.getBrief());
            mallLiveRoomProduct.setPrice(mallGoods.getRetailPrice());
            mallLiveRoomProductService.save(mallLiveRoomProduct);

            // 添加规格
            List<MallGoodsProduct> mallGoodsProducts = mallGoodsProductService.queryByGid(mallGoods.getId());
            for (MallGoodsProduct temp2: mallGoodsProducts) {
                MallActivityGoodsProduct mallActivityGoodsProduct = new MallActivityGoodsProduct();
                mallActivityGoodsProduct.setActivityType(1);// 直播
                mallActivityGoodsProduct.setActivityRuleId(mallLiveRoomProduct.getId());
                mallActivityGoodsProduct.setGoodsId(mallGoods.getId());
                mallActivityGoodsProduct.setProductId(temp2.getId());
                mallActivityGoodsProduct.setSpecifications(temp2.getSpecifications());
                mallActivityGoodsProduct.setActivityPrice(temp2.getPrice());
                mallActivityGoodsProduct.setNumber(temp2.getNumber());
//            mallActivityGoodsProduct.setSurplusNumber(temp.get) 活动剩余数量
                mallActivityGoodsProduct.setUrl(temp2.getUrl());
                mallActivityGoodsProduct.setAddTime(LocalDateTime.now());
                mallActivityGoodsProduct.setDeleted(false);
                mallActivityGoodsProductService.save(mallActivityGoodsProduct);
            }
        }

        return ResponseUtil.ok();
    }

    /**
     * 删除橱窗商品-主播
     * @param userId
     * @param liveVo
     * @return
     */
    @PostMapping("removeGoods")
    public Object removeGoods(@LoginUser String userId,@RequestBody LiveVo liveVo){
        // 直播间
        QueryWrapper<MallLiveRoom> wrapper = new QueryWrapper<MallLiveRoom>();
        wrapper.eq("add_id", userId);
        MallLiveRoom mallLiveRoom = mallLiveRoomService.getOne(wrapper);

        // 商品
        QueryWrapper<MallLiveRoomProduct> wrapper2 = new QueryWrapper<MallLiveRoomProduct>();
        wrapper2.eq("goods_id", liveVo.getGoodsId());
        wrapper2.eq("room_id", mallLiveRoom.getId());
        MallLiveRoomProduct mallLiveRoomProduct = mallLiveRoomProductService.getOne(wrapper2);
        mallLiveRoomProductService.remove(wrapper2);

        // 商品规格
        QueryWrapper<MallActivityGoodsProduct> wrapper3 = new QueryWrapper<MallActivityGoodsProduct>();
        wrapper3.eq("activity_rule_id",mallLiveRoomProduct.getId());
        mallActivityGoodsProductService.remove(wrapper3);
        return ResponseUtil.ok();
    }

    /**
     * 批量删除橱窗商品-主播
     * @param userId
     * @param liveVo
     * @return
     */
    @PostMapping("removeMoreGoods")
    public Object removeMoreGoods(@LoginUser String userId,@RequestBody LiveVo liveVo){
        // 直播间
        QueryWrapper<MallLiveRoom> wrapper = new QueryWrapper<MallLiveRoom>();
        wrapper.eq("add_id", userId);
        MallLiveRoom mallLiveRoom = mallLiveRoomService.getOne(wrapper);

        List<String> ids = Arrays.asList(liveVo.getGoodsIds());
        for (String temp: ids) {
            // 商品
            QueryWrapper<MallLiveRoomProduct> wrapper2 = new QueryWrapper<MallLiveRoomProduct>();
            wrapper2.eq("goods_id", temp);
            wrapper2.eq("room_id", mallLiveRoom.getId());
            MallLiveRoomProduct mallLiveRoomProduct = mallLiveRoomProductService.getOne(wrapper2);
            mallLiveRoomProductService.remove(wrapper2);

            // 商品规格
            QueryWrapper<MallActivityGoodsProduct> wrapper3 = new QueryWrapper<MallActivityGoodsProduct>();
            wrapper3.eq("activity_rule_id",mallLiveRoomProduct.getId());
            mallActivityGoodsProductService.remove(wrapper3);
        }
        return ResponseUtil.ok();
    }

    /**
     * 修改设置信息-主播
     * @param liveVo
     * @return
     */
    @PostMapping("updateSetting")
    public Object updateSetting(@LoginUser String userId, @RequestBody LiveVo liveVo){
        // 直播间
        QueryWrapper<MallLiveRoom> wrapper = new QueryWrapper<MallLiveRoom>();
        wrapper.eq("add_id", userId);
        MallLiveRoom mallLiveRoom = mallLiveRoomService.getOne(wrapper);

        MallLiveRoomProduct mallLiveRoomProduct = mallLiveRoomProductService.getById(liveVo.getProductId());
        if(liveVo.getSort() != null && !liveVo.getSort().equals("")){
            mallLiveRoomProduct.setSort(Integer.parseInt(liveVo.getSort()));
        }
        if(liveVo.getIsTalk()){
            // 讲解是唯一的
            QueryWrapper<MallLiveRoomProduct> wrapper2 = new QueryWrapper<MallLiveRoomProduct>();
            wrapper2.eq("room_id", mallLiveRoom.getId());
            wrapper2.eq("is_talk", true);
            MallLiveRoomProduct temp = mallLiveRoomProductService.getOne(wrapper2);

            if(temp != null){
                temp.setIsTalk(false);
                mallLiveRoomProductService.updateById(temp);
            }
        }
        mallLiveRoomProduct.setIsTalk(liveVo.getIsTalk());
        mallLiveRoomProductService.updateById(mallLiveRoomProduct);
        return ResponseUtil.ok();
    }

    /**
     * 关闭直播间-主播
     * @param userId
     * @return
     */
    @PostMapping("closeLive")
    public Object closeLive(@LoginUser String userId){
        if (userId == null) {
            return ResponseUtil.unlogin();
        }

        try{
            // 直播间
            QueryWrapper<MallLiveRoom> wrapper = new QueryWrapper<MallLiveRoom>();
            wrapper.eq("add_id", userId);
            MallLiveRoom mallLiveRoom = mallLiveRoomService.getOne(wrapper);

            if(mallLiveRoom == null){
                return ResponseUtil.fail("没找到直播间");
            }

            // 断开推流状态
            LiveUtil.stopPush(mallLiveRoom.getId());

            // 修改直播间下播信息
            mallLiveRoom.setRoomStatus("2");
            mallLiveRoom.setOpenTotal(mallLiveRoom.getOpenTotal()+1);
            mallLiveRoom.setPushUrl("");
            mallLiveRoom.setPlayUrl("");
            mallLiveRoom.setUpdateTime(LocalDateTime.now());
            mallLiveRoom.setEndLiveAt(LocalDateTime.now());
            mallLiveRoomService.updateById(mallLiveRoom);

            QueryWrapper<MallLiveRoomUser> mallLiveRoomUserQueryWrapper = new QueryWrapper<MallLiveRoomUser>();
            mallLiveRoomUserQueryWrapper.eq("room_id", mallLiveRoom.getId());
            mallLiveRoomUserService.remove(mallLiveRoomUserQueryWrapper);
            return ResponseUtil.ok(mallLiveRoom);
        }catch(Exception e){
            return ResponseUtil.fail(e.getMessage());
        }
    }


    /**
     * todo:推断流事件回调；
     * todo:后台强制停播后此处应根据直播间状态判断是否修改直播间下播信息
     * @param data
     * @return
     */
    @PostMapping("pushOrStopLiveNotify")
    public void pushOrStopLiveNotify(String data){
        System.out.println(data);
    }

    /**
     * todo:截图事件回调
     * @param data
     * @return
     */
    @PostMapping("cutLiveNotify")
    public void cutLiveCallBack(String data){
        System.out.println(data);
    }

    /**
     * todo:鉴黄事件回调
     * @param data
     * @return
     */
    @PostMapping("cutYellowNotify")
    public void cutYellowLiveCallBack(String data){
        System.out.println(data);
    }

    /**
     * 生成直播支付订单
     * @param userId
     * @return
     */
    @PostMapping("createOrder")
    public Object createOrder(@LoginUser String userId){
        // 用户商户关联表
        QueryWrapper<MallUserMerchant> mallUserMerchantWrapper = new QueryWrapper<MallUserMerchant>();
        mallUserMerchantWrapper.eq("user_id", userId);
        List<MallUserMerchant> mallUserMerchant = mallUserMerchantService.list(mallUserMerchantWrapper);

        MallMerchant mallMerchant = null;
        for (MallUserMerchant temp: mallUserMerchant) {
            // 商户信息
            QueryWrapper<MallMerchant> merchantWrapper = new QueryWrapper<MallMerchant>();
            merchantWrapper.eq("status", 0);// 正常营业
            merchantWrapper.eq("audit_status", 1);// 审核通过
            merchantWrapper.eq("id",temp.getMerchantId());
            mallMerchant = mallMerchantService.getOne(merchantWrapper);

            if(mallMerchant != null){
                break;
            }
        }

        if(mallMerchant == null){
            return ResponseUtil.fail("没该用户商户信息");
        }

        QueryWrapper<MallLiveRoom> liveRoomWrapper = new QueryWrapper<MallLiveRoom>();
        liveRoomWrapper.eq("add_id", userId);
        liveRoomWrapper.eq("deleted", false);
        MallLiveRoom liveRoom = mallLiveRoomService.getOne(liveRoomWrapper);
        if(liveRoom != null){
            return ResponseUtil.fail("该用户已有直播间");
        }

        // 获取开通金额
        MallSystem mallSystem = systemConfigService.getMallSystemCommonByKeyName(CommonConstant.LIVE_RECHARGE_KEY_NAME);
        if(mallSystem == null){
            return ResponseUtil.fail("未配置直播间开通金额!");
        }
        BigDecimal price = new BigDecimal(mallSystem.getKeyValue());

        // 生成订单
        String sn = GraphaiIdGenerator.nextId("ORDER_SN");
        MallOrder mallOrder = new MallOrder();
        mallOrder.setId(GraphaiIdGenerator.nextId("MallOrder"));
        mallOrder.setHuid("ktzb");// 开通直播
        mallOrder.setOrderSn(sn);
        mallOrder.setUserId(userId);
        mallOrder.setOrderStatus(Short.parseShort(String.valueOf(OrderUtil.STATUS_CREATE)));
        mallOrder.setMessage("开通直播支付订单");
        mallOrder.setDeleted(false);
        mallOrder.setOrderPrice(price);
        mallOrder.setActualPrice(price);
        mallOrder.setPubSharePreFee(String.valueOf(price));
        mallOrder.setBid(AccountStatus.BID_52);
        mallOrder.setMerchantId(mallMerchant.getId());
        mallOrder.setAddTime(LocalDateTime.now());
        mallOrderService.add(mallOrder);
        return ResponseUtil.ok(mallOrder);
    }

    /**
     * 获取开通直播间金额
     * @return
     */
    @GetMapping("getOpenPrice")
    public Object getOpenPrice(){
        MallSystem mallSystem = systemConfigService.getMallSystemCommonByKeyName(CommonConstant.LIVE_RECHARGE_KEY_NAME);
        if(mallSystem == null){
            return ResponseUtil.fail("未配置直播间开通金额!");
        }
        return ResponseUtil.ok(mallSystem);
    }
}
