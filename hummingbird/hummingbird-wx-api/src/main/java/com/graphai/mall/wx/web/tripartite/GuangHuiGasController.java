package com.graphai.mall.wx.web.tripartite;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.httprequest.HttpUtil;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.util.StringHelper;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.mall.wx.web.common.MyBaseController;
import com.graphai.nosql.redis.NewCacheInstanceServiceLoader;
import com.graphai.properties.GuangHuiGasProperty;

import cn.hutool.core.util.ObjectUtil;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/wx/ghGas")
@Slf4j
public class GuangHuiGasController extends MyBaseController {
    @Autowired
    private MallUserService userService;

    @Autowired
    private GuangHuiGasProperty guangHuiGasProperty;


    /**
     * 获取商户（我司渠道）token
     *
     * @return token
     */
    public String getToken() {

        try {

            Map<String, String> map = new HashMap<String, String>();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
            Calendar nowTime = Calendar.getInstance();
            Date startBeforeTime = nowTime.getTime();
            String now = sdf.format(startBeforeTime);

            String jsonParam = String.format("{\"mchId\":\"%s\",\"appId\":\"%s\",\"timestamp\":\"%s\",\"version\":\"v1\",\"nonce\":\"%s\",\"method\":\"app.token.create\",\"bizContent\":{\"appsecret\":\"%s\"}}",
                            guangHuiGasProperty.getMchId(), guangHuiGasProperty.getAppId(), now, String.valueOf(UUID.randomUUID()), guangHuiGasProperty.getAppSecret());

            String result = HttpUtil.sendPostJson(guangHuiGasProperty.getHttpUrl(), jsonParam);

            JSONObject object = JSONObject.parseObject(result);
            if (Objects.equals(object.getString("code"), "10000") && Objects.equals(object.getString("subCode"), "000000")) {
                JSONObject obj = (JSONObject) object.parse(object.get("data").toString());
                if (ObjectUtil.isNotNull(obj)) {
                    if (StringHelper.isNotNullAndEmpty(obj.getString("token"))) {
                        NewCacheInstanceServiceLoader.objectCache().put("gasToken", obj.getString("token"));
                        return obj.getString("token");
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 注册会员信息
     *
     * @param userId 用户id
     * @return token
     */
    @GetMapping("/regMember")
    public Object regMember(@LoginUser String userId) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }

        // String token = "";
        // String nickName = "";
        // String mobile = "";
        //
        // try {
        // MallUser user = userService.findById(userId);
        // JSONObject jsonObject = new JSONObject(new LinkedHashMap());
        //
        // // 将token放到缓存两小时自动失效
        // Object home = homeCache.get("gasToken");
        // if (home == null) {
        // token = getToken();
        // } else {
        // token = String.valueOf(homeCache.get("gasToken"));
        // }
        //
        // if (ObjectUtil.isNotNull(user) && StringUtils.isNotBlank(user.getGasOpenId())) {
        // String linkUrl = String.format(
        // "%s#/gaslist?activityCode=%s&ghOpenId=%s",
        // guangHuiGasProperty.getBuyCardUrl(),guangHuiGasProperty.getMchId(),user.getGasOpenId());
        // return ResponseUtil.ok(linkUrl);
        // }
        // if (ObjectUtil.isNotNull(user) && StringUtils.isNotBlank(user.getMobile())) {
        // mobile = user.getMobile();
        // jsonObject.put("mobile", mobile);
        // if (StringHelper.isNotNullAndEmpty(user.getNickname())) {
        // nickName = user.getNickname();
        //
        // jsonObject.put("nickName", nickName);
        // }
        //
        // }
        //
        // SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        // Calendar nowTime = Calendar.getInstance();
        // Date startBeforeTime = nowTime.getTime();
        // String now = sdf.format(startBeforeTime);
        // String nonce = String.valueOf(UUID.randomUUID()).replaceAll("-", "");
        //
        // JSONObject jsonObj = new JSONObject(new LinkedHashMap());
        //
        // jsonObj.put("mchId", guangHuiGasProperty.getMchId());
        // jsonObj.put("appId", guangHuiGasProperty.getAppId());
        // jsonObj.put("timestamp", now);
        // jsonObj.put("version", "v1");
        // jsonObj.put("nonce", nonce);
        // jsonObj.put("method", "member.register");
        // jsonObj.put("signType", "MD5");
        //
        // jsonObj.put("appAuthToken", token);
        // jsonObj.put("bizContent", String.valueOf(jsonObject));
        // // jsonObj.put("signKey", guangHuiGasProperty.getSignKey());
        //
        // String signString = "appAuthToken=" + jsonObj.get("appAuthToken") + "&appId=" +
        // jsonObj.get("appId")
        // + "&bizContent=" + jsonObj.get("bizContent") + "&mchId=" + jsonObj.get("mchId") + "&method="
        // + jsonObj.get("method") + "&nonce=" + jsonObj.get("nonce") + "&signType=" +
        // jsonObj.get("signType")
        // + "&timestamp=" + jsonObj.get("timestamp") + "&version=" + jsonObj.get("version") + "&signKey="
        // + guangHuiGasProperty.getSignKey();
        //
        // String jsonParam = String.format(
        // "{\"mchId\":\"%s\",\"appId\":\"%s\",\"timestamp\":\"%s\",\"version\":\"v1\",\"method\":\"member.register\",\"nonce\":\"%s\",\"signType\":\"MD5\",\"appAuthToken\":\"%s\",\"bizContent\":{\"mobile\":\"%s\",\"nickName\":\"%s\"},\"sign\":\"%s\"}",
        // guangHuiGasProperty.getMchId(), guangHuiGasProperty.getAppId(), now, nonce, token,
        // mobile != null ? mobile : null, nickName != null ? nickName : null,
        // SignMD5Util.newMd5(signString));
        //
        // System.out.println("入参值" + jsonParam);
        // String result = HttpUtil.sendPostJson(guangHuiGasProperty.getHttpUrl(), jsonParam);
        //
        // System.out.println("返回结果" + result);
        //
        // JSONObject object = JSONObject.parseObject(result);
        // if (Objects.equals(object.getString("code"), "10000")
        // && Objects.equals(object.getString("subCode"), "000000")) {
        // JSONObject obj = (JSONObject) object.parse(object.get("data").toString());
        // if (ObjectUtil.isNotNull(obj)) {
        // if (StringHelper.isNotNullAndEmpty(obj.getString("openId"))) {
        // if (ObjectUtil.isNotNull(user)) {
        // user.setGasOpenId(obj.getString("openId"));
        // userService.updateById(user);
        // String linkUrl = String.format(
        // "%s#/gaslist?activityCode=%s&ghOpenId=%s",
        // guangHuiGasProperty.getBuyCardUrl(),guangHuiGasProperty.getMchId(),user.getGasOpenId());
        // return ResponseUtil.ok(linkUrl);
        // }
        //
        // }
        // }
        // }
        // } catch (Exception e) {
        // log.error(e.getMessage(), e);
        // e.printStackTrace();
        // }
        //
        // return ResponseUtil.fail(502, "注册会员失败");
        // 因光汇云油渠道关闭接口，暂时写死链接
        return ResponseUtil.ok("https%3a%2f%2fm.lechebang.com%2fduijie%2fqsk%2findex%3fcollectBusinessId%3d2792");
    }

}
