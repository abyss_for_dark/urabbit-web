package com.graphai.mall.wx.web.redpacket;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.mall.db.domain.MallOrder;
import com.graphai.mall.db.domain.MallUser;
import com.graphai.mall.db.service.order.MallOrderService;
import com.graphai.mall.db.service.red.WxRedService;
import com.graphai.mall.db.util.RedpacketUtil;
import com.graphai.mall.wx.annotation.LoginUser;

@RestController
@RequestMapping("/wx/gamejackpot")
@Validated
public class WxPasswordRedPacketController {
    private final Log logger = LogFactory.getLog(WxPasswordRedPacketController.class);

    @Autowired
    private WxRedService wxRedService;
    @Autowired
    private MallOrderService orderService;

    /**
     * 整点分享口令锁粉
     */
    @GetMapping("/hourShare")
    public Object getHourShare(@LoginUser String userId) {
        return wxRedService.createHour(userId);
    }

    /**
     * 根据userId查询整点分享口令
     */
    @GetMapping("/hourPwd")
    public Object getHourPwd(@RequestParam String userId) {
        return wxRedService.getHourPwd(userId);
    }

    /**
     * 1.1、口令红包-查询用户的口令红包列表
     */
    @GetMapping("/findRedPwdList")
    public Object getFindRedPwdList(@LoginUser String userId) {
        return wxRedService.getFindRedPwdList(userId);
    }

    /**
     * 1.2、口令红包-口令红包详情
     */
    @GetMapping("/findRedPwdDetail")
    public Object getFindRedPwdDetail(@LoginUser String userId, @RequestParam String id, @RequestParam String findType) {
        return wxRedService.getFindRedPwdDetail(userId, id, findType);
    }

    /**
     * 1.3、口令红包-分享页
     */
    @GetMapping("/findRedPwdShare")
    public Object getFindRedPwdShare(@LoginUser String userId, @RequestParam String id) {
        return wxRedService.getFindRedPwdShare(userId, id);
    }

    /**
     * 1.4、口令红包-业务处理类
     */
    @GetMapping("/getFindRedPwdProcess")
    public Object getFindRedPwdProcess(@RequestParam String payId, @RequestParam String orderId) {
        if (null == payId || null == orderId) {
            return ResponseUtil.unlogin();
        }
        MallOrder mallOrder = orderService.findById(orderId);
        String result = wxRedService.processRedPwd(mallOrder, orderId);
        return ResponseUtil.ok(result);
    }

    /**
     * 1.5、口令红包-长链接转短链接
     */
    @GetMapping("/getLongToShortUrl")
    public Object getLongToShortUrl(@RequestParam String longUrl) {
        if (null == longUrl) {
            return ResponseUtil.unlogin();
        }
        String shortUrl = RedpacketUtil.getLongToShortUrl(longUrl, "uulucky");
        logger.info("口令红包-长链接转短链接shortUrl=" + shortUrl);
        return ResponseUtil.ok(shortUrl);
    }

    /**
     * 1.6、口令红包-领取
     */
    @GetMapping("/receiveRedPwd")
    public Object getReceiveRedPwd(@LoginUser String userId, @RequestParam String id, @RequestParam String platformResource) {
        if (null == userId) {
            return ResponseUtil.unlogin();
        }
        if (null == id) {
            return ResponseUtil.fail(402, "口令红包编号不能为空");
        }
        return wxRedService.getReceiveRedPwd(userId, id, platformResource);
    }

    /**
     * 1.7、口令红包-分享页用户点击领取
     */
    @GetMapping("/findRedPwdShareReceive")
    public Object getFindRedPwdShareReceive(@RequestParam String id) {
        if (null == id) {
            return ResponseUtil.fail("红包编号不能为空");
        }
        return wxRedService.getFindRedPwdShareReceive(id);
    }

    /**
     * 1.8、口令红包-首页领取记录
     */
    @GetMapping("/findRedPwdDetailIndex")
    public Object getFindRedPwdDetailIndex(@LoginUser String userId, @RequestParam String id) {
        return wxRedService.getFindRedPwdDetailIndex(userId, id);
    }

    /**
     * 2.1、商圈线下红包码-获取口令
     */
    @PostMapping("/offLineShareCode")
    public Object offLineShareCode(@RequestBody MallUser user) {
        return wxRedService.getOffLineShareCode(user);
    }

}
