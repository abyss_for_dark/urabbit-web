package com.graphai.mall.wx.web.game;

import com.alibaba.druid.support.json.JSONUtils;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.mall.db.constant.account.AccountStatus;
import com.graphai.mall.db.constant.CommonConstant;
import com.graphai.mall.db.util.account.AccountUtils;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.mall.wx.util.StringUtils;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.*;

/**
 * 豆萌游戏接口
 *
 * @author huangyang
 * @since 2021-02-24
 */
@RestController
@RequestMapping("/wx/game/doumob")
public class WxGameDoumobController {

    private final Log log = LogFactory.getLog(WxGameDoumobController.class);

    @Value("${mall.doumob.url}")
    private String url;

    @Value("${mall.doumob.app-key}")
    private String appKey;

    @Value("${mall.doumob.ad-space-key}")
    private String adSpaceKey;

    @Value("${mall.doumob.sign-key}")
    private String signKey;


    /**
     * 获取游戏入口链接
     * @param userId
     * @return
     * @throws UnsupportedEncodingException
     */
    @GetMapping("/getLink")
    public Object getLink(@LoginUser String userId) throws UnsupportedEncodingException {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        String link = url + "appkey=" + appKey + "&adSpaceKey=" + adSpaceKey + "&mediaUid=" + userId;
        log.info("-----------获取养成游戏链接: " + link);

        Map<String, String> map = new HashMap<>();
        map.put("link", URLEncoder.encode(link, "UTF-8"));
        map.put("appKey", appKey);

        return ResponseUtil.ok(map);
    }

    /**
     * 金币同步接口
     * @param params
     * @return
     */
    @PostMapping("/coinSync")
    public Object coinSync(@RequestBody Map<String, String> params) {

        log.info("养成游戏金币同步请求参数: "+ JSONUtils.toJSONString(params));
                                                                // 类型   是否必须      说明
        if (    StringUtils.isBlank(params.get("requestor")) || // String   Y   请求方, 固定为doumeng
                StringUtils.isBlank(params.get("recordId")) ||  // Integer  Y   养成游戏方金币记录ID
                StringUtils.isBlank(params.get("mediaUid")) ||  // String   Y   媒体方用户ID
                StringUtils.isBlank(params.get("coin")) ||      // Integer  Y   金币数
                StringUtils.isBlank(params.get("timeStamp")) || // Long     Y   时间戳
                StringUtils.isBlank(params.get("sign"))) {      // String   Y   签名，用于验证请求数据合法性
            return result(3, "参数错误");
        }

        try {
            if (!verifySign(params)) {
                return result(3, "签名错误");
            }
            Map<String, Object> map = new HashMap<>();
            map.put("orderId", params.get("recordId"));
            map.put("capitalTrend", AccountStatus.CAPITAL_TREND_01);
            map.put("tradeType", AccountStatus.TRADE_TYPE_10);
            map.put("changeType", AccountStatus.CHANGE_TYPE_01);
            map.put("changeAccountType", AccountStatus.CHANGE_ACCOUNT_TYPE_2);
            map.put("message", "养宠物赚金币");

            Map<String, Object> m = (Map<String, Object>) AccountUtils.accountChangeByExchange(params.get("mediaUid"), new BigDecimal(params.get("coin")), CommonConstant.TASK_REWARD_GOLD, AccountStatus.BID_7, map);

            log.info("金币同步结果:" + JSONUtils.toJSONString(m));

            if (m.get("errno").equals(0)) {
                return result(0, "success");
            } else {
                String msg = m.get("errmsg") == null ? "" : m.get("errmsg").toString();
                return result(3, msg);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result(1, "同步失败");
    }


    /**
     * 检验签名: 将参数名称(除sign外)按字母升序排序，进行参数拼接，再进行MD5
     * @param map
     * @return
     */
    public boolean verifySign(Map<String, String> map) {
        if (CollectionUtils.isEmpty(map)) {
            return false;
        }
        List<String> keys = new ArrayList<>(map.size());
        keys.addAll(map.keySet());
        Collections.sort(keys);

        StringBuilder signStr = new StringBuilder();
        for (String key : keys) {
            if (key.equalsIgnoreCase("sign")) continue;
            signStr.append(key).append("=").append(map.get(key)).append("&");
        }
        signStr.append("signKey=").append(signKey);

        String sign = DigestUtils.md5Hex(signStr.toString());

        return sign.equals(map.get("sign"));
    }

    /**
     * 0成功 1失败(需重试, 会再重试2次，每次间隔10秒) 其他失败(不需要重试)
     * @param code
     * @param msg
     * @return
     */
    public Map<String, Object> result(int code, String msg) {
        Map<String, Object> ret = new HashMap<>();
        ret.put("code", code);
        ret.put("message", msg);
        return ret;
    }

}
