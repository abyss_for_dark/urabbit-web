package com.graphai.mall.wx.web.tripartite;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.validation.constraints.NotEmpty;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.SecurityUtil;
import com.graphai.commons.util.httprequest.HttpUtil;
import com.graphai.id.GraphaiIdGenerator;
import com.graphai.mall.db.dao.MallGasMapper;
import com.graphai.mall.db.dao.MallOliMapper;
import com.graphai.mall.db.domain.MallGas;
import com.graphai.mall.db.domain.MallOli;
import com.graphai.mall.db.domain.MallOrder;
import com.graphai.mall.db.domain.MallOrderGoods;
import com.graphai.mall.db.domain.MallUser;
import com.graphai.mall.db.constant.account.AccountStatus;
import com.graphai.mall.db.service.gasoline.MallGasService;
import com.graphai.mall.db.service.gasoline.MallOliService;
import com.graphai.mall.db.service.order.MallOrderGoodsService;
import com.graphai.mall.db.service.order.MallOrderService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.constant.CommonConstant;
import com.graphai.mall.db.util.OrderUtil;
import com.graphai.mall.db.util.StringHelper;
import com.graphai.mall.db.util.account.AccountUtils;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.nosql.redis.NewCacheInstanceServiceLoader;

import cn.hutool.core.util.ObjectUtil;
import lombok.extern.slf4j.Slf4j;

@Slf4j(topic = "XiaoJuGasController")
@RestController
@RequestMapping("/wx/xiaoju")
public class XiaoJuGasController {
    @Autowired
    private MallGasService mallGasService;

    @Autowired
    private MallOliService mallOliService;

    @Autowired
    private MallGasMapper mallGasMapper;

    @Autowired
    private MallOliMapper mallOliMapper;

    @Autowired
    private MallUserService userService;

    @Autowired
    private MallOrderService mallOrderService;

    @Autowired
    private MallOrderGoodsService mallOrderGoodsService;
    

    /**
     * 获取小桔加油token
     * 
     * @return token
     */
    @GetMapping("/getxiaoJuToken")
    public Object getxiaoJuToken() {

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("appSecret", SecurityUtil.appSecret);

        try {

            String jsonParam = SecurityUtil.encryptSignRequest(map);

            String result = HttpUtil.sendPostJson(SecurityUtil.requestUrl + "/queryToken", jsonParam);

            JSONObject object = JSONObject.parseObject(result);
            if (Objects.equals("0", object.get("code"))) {
                String respoString = SecurityUtil.decryptResponse(object.getString("data"));
                if (StringHelper.isNotNullAndEmpty(respoString)) {
                    JSONObject obj = JSONObject.parseObject(respoString);
                    if (!Objects.equals(null, obj.getString("accessToken"))) {
                        NewCacheInstanceServiceLoader.xiaojuCache().put("xiaojuToken", obj.getString("accessToken"));
                        return obj.getString("accessToken");

                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * 获取小桔token
     *
     * @return token
     */
    public String getToken() {

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("appSecret", SecurityUtil.appSecret);

        try {

            String jsonParam = SecurityUtil.encryptSignRequest(map);

            String result = HttpUtil.sendPostJson(SecurityUtil.requestUrl + "/queryToken", jsonParam);

            JSONObject object = JSONObject.parseObject(result);
            if (Objects.equals("0", object.get("code"))) {
                String respoString = SecurityUtil.decryptResponse(object.getString("data"));
                if (StringHelper.isNotNullAndEmpty(respoString)) {
                    JSONObject obj = JSONObject.parseObject(respoString);
                    if (!Objects.equals(null, obj.getString("accessToken"))) {
                        NewCacheInstanceServiceLoader.xiaojuCache().put("xiaojuToken", obj.getString("accessToken"));
                        log.info("获取小桔token"+obj.getString("accessToken"));
                        return obj.getString("accessToken");
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 小桔用户信息校验回调
     * 
     * @param 用户id和手机号
     * @return
     */
    @PostMapping("/notifyCheckUserInfo")
    public Object notifyCheckUserInfo(@RequestBody Map<String, Object> param) {
        log.info("-------------------小桔用户验证回调进来了------------------------------------");

        String reString = "";
        JSONObject object = null;
        Map<String, Object> map = new HashMap<String, Object>();
        JSONObject ob = null;

        try {
            String respoString = SecurityUtil.decryptResponse(String.valueOf(param.get("data")));
            if (StringHelper.isNotNullAndEmpty(respoString)) {
                ob = JSONObject.parseObject(respoString);
                log.info("-------------------小桔用户手机号" + ob.getString("mobile"));
                log.info("-------------------小桔用户用户id" + ob.getString("outUserId"));
                List<MallUser> users = userService.queryByMobile(ob.getString("mobile"));
                if (CollectionUtils.isNotEmpty(users) && Objects.equals(users.get(0).getId(), ob.getString("outUserId"))) {
                    map.put("checkState", 0);
                } else {
                    map.put("checkState", 1);
                    map.put("checkMsg", "用户不存在!");
                }
            }

            reString = SecurityUtil.encryptSignRespon(map);
            object = JSONObject.parseObject(reString.toString());

            return object;

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return object;
    }



    /**
     * 支付订单回调通知
     * 
     * @param 订单相关信息
     * @return
     */
    @PostMapping("/notifyOrderInfo")
    public Object notifyOrderInfo(@RequestBody Map<String, Object> param) {
        String orderId = String.valueOf(param.get("orderId"));
        String outUserId = String.valueOf(param.get("outUserId"));
        String totalMoney = String.valueOf(param.get("outUserId"));
        String realMoney = String.valueOf(param.get("realMoney"));
        String reString = "";
        JSONObject object = null;
        List<MallOrder> mallOrders = null;
        Map<String, Object> map = new HashMap<String, Object>();
        Map<String, String> paramMap = new HashMap(6);
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        JSONObject ob = null;
        try {

            if (StringHelper.isNullOrEmpty(orderId)) {
                log.info("小桔订单号为空" + orderId);
                map.put("message", "OrderId为空");
                map.put("state", 0);
                reString = SecurityUtil.encryptSignRespon(map);
                object = JSONObject.parseObject(reString.toString());
                return object;
            }

            if (StringHelper.isNullOrEmpty(outUserId)) {
                log.info("outUserId为空" + outUserId);
                map.put("message", "outUserId为空");
                map.put("state", 0);
                reString = SecurityUtil.encryptSignRespon(map);
                object = JSONObject.parseObject(reString.toString());
                return object;
            }

            if (StringHelper.isNullOrEmpty(realMoney)) {
                log.info("实付金额为空" + realMoney);
                map.put("message", "实付金额字段缺失");
                map.put("state", 0);
                reString = SecurityUtil.encryptSignRespon(map);
                object = JSONObject.parseObject(reString.toString());
                return object;
            }

            if (StringHelper.isNullOrEmpty(totalMoney)) {
                log.info("订单金额为空" + totalMoney);
                map.put("message", "金额字段缺失");
                map.put("state", 0);
                reString = SecurityUtil.encryptSignRespon(map);
                object = JSONObject.parseObject(reString.toString());
                return object;
            }

            MallUser user = userService.findById(outUserId);

            if (null == user) {
                log.info("-----用户不存在----");
                map.put("message", "用户不存在");
                map.put("state", 0);
                reString = SecurityUtil.encryptSignRespon(map);
                object = JSONObject.parseObject(reString.toString());
                return object;
            }


            Map<String, Object> paMap = new HashMap<String, Object>();
            paMap.put("orderId", orderId);
            String jsonParam = SecurityUtil.encryptSignRequest(paMap);

            String result = HttpUtil.sendPostJson(SecurityUtil.requestUrl + "/queryOrderInfo", jsonParam);

            JSONObject obj = JSONObject.parseObject(result);
            if (Objects.equals("0", obj.get("code"))) {
                String respoString = SecurityUtil.decryptResponse(obj.getString("data"));
                if (StringHelper.isNotNullAndEmpty(respoString)) {
                    ob = JSONObject.parseObject(respoString);

                    mallOrders = mallOrderService.findOrderBySrcOrderIdHuid(orderId, "xiaoju");
                    if (CollectionUtils.isEmpty(mallOrders)) {



                        MallOrder mallOrder = new MallOrder();

                        mallOrder.setSrcOrderIds(ob.getString("orderId"));
                        mallOrder.setHuid("xiaoju");
                        mallOrder.setOrderSn(ob.getString("orderId"));
                        mallOrder.setDeleted(false);
                        mallOrder.setUserId(ob.getString("outUserId"));

                        // mallOrder.setOrderStatusDesc(ob.getString("orderStatusName"));
                        if (Objects.equals(0, ob.getInteger("payStatus"))) {
                            mallOrder.setOrderStatus(OrderUtil.STATUS_CREATE);
                        } else if (Objects.equals(1, ob.getInteger("payStatus"))) {
                            mallOrder.setOrderStatus(OrderUtil.STATUS_PAY);
                        }

                        // if (Objects.equals(0, ob.getInteger("refundStatus"))) {
                        // mallOrder.setOrderStatus(OrderUtil.STATUS_REFUND);
                        // } else if (Objects.equals(1, ob.getInteger("refundStatus"))) {
                        // mallOrder.setOrderStatus(OrderUtil.STATUS_REFUND_CONFIRM);
                        // }

                        mallOrder.setDeleted(false);

                        if (!Objects.equals(null, ob.getString("payTime"))) {
                            LocalDateTime payTime = LocalDateTime.parse(ob.getString("payTime"), dtf);
                            mallOrder.setPayTime(payTime);
                        }

                        if (StringHelper.isNotNullAndEmpty(ob.get("realMoney"))) {
                            mallOrder.setActualPrice(new BigDecimal(String.valueOf(ob.get("realMoney"))).divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP));

                            mallOrder.setPubSharePreFee(String.valueOf(mallOrder.getActualPrice().multiply(new BigDecimal("0.003"))));
                        }

                        mallOrder.setCityName(ob.getString("cityName"));

                        if (StringHelper.isNotNullAndEmpty(ob.get("totalMoney"))) {
                            mallOrder.setOrderPrice(new BigDecimal(String.valueOf(ob.get("totalMoney"))).divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP));
                        }

                        mallOrder.setBid(AccountStatus.BID_25);
                        mallOrderService.add(mallOrder);

                        // 插入订单商品表
                        MallOrderGoods orderGoods = new MallOrderGoods();

                        orderGoods.setOrderId(mallOrder.getId());
                        orderGoods.setDeleted(false);
                        // 油号
                        orderGoods.setGoodsSn(ob.getString("gunNo"));
                        // 商品标题
                        orderGoods.setGoodsName(ob.getString("storeName"));
                        // 升数
                        if (StringHelper.isNotNullAndEmpty(ob.getString("quantity"))) {
                            orderGoods.setLitre(ob.getString("quantity"));
                        }
                        orderGoods.setPrice(new BigDecimal(ob.getString("priceGun")));

                        mallOrderGoodsService.add(orderGoods);

                        if (Objects.equals(1, ob.getInteger("payStatus"))) {
                            paramMap.put("userId", ob.getString("outUserId"));
                            paramMap.put("bid", AccountStatus.BID_25);
                            paramMap.put("totalMoney", String.valueOf(mallOrder.getPubSharePreFee()));
                            paramMap.put("orderId", mallOrder.getId());
                            paramMap.put("rechargeType", CommonConstant.TASK_REWARD_CASH);
                            paramMap.put("orderSn", mallOrder.getOrderSn());
                            AccountUtils.preCharge(paramMap);
                        }

                    } else {
                        if (Objects.equals(1, ob.getInteger("refundStatus")) && Objects.equals(mallOrders.get(0).getOrderStatus(), OrderUtil.STATUS_PAY)) {
                            mallOrders.get(0).setOrderStatus(OrderUtil.STATUS_REFUND_CONFIRM);
                            if (mallOrderService.updateByPrimaryKeySelective(mallOrders.get(0)) > 0) {
                                // 预估存 取消
                                AccountUtils.deletePreChange(mallOrders.get(0).getId());
                            }
                        } else if (Objects.equals(0, ob.getInteger("refundStatus")) && Objects.equals(mallOrders.get(0).getOrderStatus(), OrderUtil.STATUS_PAY)) {
                            mallOrders.get(0).setOrderStatus(OrderUtil.STATUS_REFUND);
                        }

                        mallOrderService.updateByPrimaryKeySelective(mallOrders.get(0));
                    }

                }
            }

            map.put("state", 1);
            reString = SecurityUtil.encryptSignRespon(map);
            object = JSONObject.parseObject(reString.toString());

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }

        return object;
    }



    /**
     * 初始化小桔油站数据
     *
     * @param userId 用户id
     * @return token
     */
    @GetMapping("/getXiaoJuGasList")
    public Object getXiaoJuGasList() {

        Integer total = 0;
        Integer forTotal = 2;
        Integer page = 1;
        JSONObject object = null;
        String token = "";
        try {

            // 小桔token缓存redis两小时自动失效
            Object home = NewCacheInstanceServiceLoader.xiaojuCache().get("xiaojuToken");
            if (home == null) {
                token = getToken();
            } else {
                token = String.valueOf(NewCacheInstanceServiceLoader.xiaojuCache().get("xiaojuToken"));
            }

            log.info("--------------------小桔token" + token);

            for (int k = 0; k < forTotal; k++) {
                Map<String, Object> map = new LinkedHashMap<String, Object>();
                map.put("pageIndex", String.valueOf(page));
                map.put("pageSize", "100");

                String jsonParam = SecurityUtil.encryptSignRequest(map);

                String result = HttpUtil.sendPostJson(SecurityUtil.requestUrl + "/queryStoreList", jsonParam, token);

                object = JSONObject.parseObject(result);

                // if (Objects.equals("0", object.get("code"))) {
                //
                // if (StringHelper.isNotNullAndEmpty(respoString)) {
                // JSONObject obj = JSONObject.parseObject(respoString);
                // if (!Objects.equals(null, obj.getString("accessToken"))) {
                // NewCacheInstanceServiceLoader.objectCache().put("xiaojuToken", obj.getString("accessToken"));
                // return obj.getString("accessToken");
                //
                // }
                // }
                // }

                if (Objects.equals(object.getString("code"), "0")) {

                    String respoString = SecurityUtil.decryptResponse(object.getString("data"));

                    JSONObject obj = JSONObject.parseObject(respoString);

                    JSONArray array = JSONArray.parseArray(obj.get("storeInfoList").toString());

                    if (CollectionUtils.isNotEmpty(array)) {
                        for (int i = 0; i < array.size(); i++) {
                            JSONObject ob = (JSONObject) array.get(i);

                            // 查询是否已存在油站记录
                            // MallGasExample example = new MallGasExample();
                            // example.or().andGasIdEqualTo(ob.getString("gasId")).andDeletedEqualTo(false);
                            // List<MallGas> gaslList = mallGasMapper.selectByExample(example);
                            //
                            // if (CollectionUtils.isEmpty(gaslList)) {
                            MallGas gas = new MallGas();
                            gas.setId(GraphaiIdGenerator.nextId("MallGas"));
                            gas.setPlatform(2);
                            gas.setGasId(ob.getString("storeId"));
                            gas.setGasName(ob.getString("storeName"));
                            // gas.setGasType(ob.getInteger("gasType"));
                            // gas.setProvinceCode(ob.getInteger("provinceCode"));
                            gas.setProvinceName(ob.getString("provinceName"));
                            // gas.setCityCode(ob.getInteger("cityCode"));
                            gas.setCityName(ob.getString("cityName"));
                            // if (ObjectUtil.isNotNull(ob.getInteger("countyCode"))) {
                            // gas.setCountyCode(ob.getInteger("countyCode"));
                            // }

                            if (StringUtils.isNotEmpty(ob.getString("countyName"))) {
                                gas.setCountyName(ob.getString("countyName"));
                            }

                            gas.setGasLogoBig(ob.getString("storeLogo"));
                            // gas.setGasLogoSmall(ob.getString("gasLogoSmall"));
                            gas.setGasAddress(ob.getString("address"));
                            gas.setGasLongitude(ob.getDouble("lon"));
                            gas.setGasLatitude(ob.getDouble("lat"));
                            List<Double> doubles = new ArrayList<Double>(2);
                            doubles.add(Double.parseDouble(ob.getString("lon")));
                            doubles.add(Double.parseDouble(ob.getString("lat")));
                            gas.setLocation(doubles);
                            gas.setDeleted(false);
                            gas.setAddTime(LocalDateTime.now());
                            // if (0 == ob.getInteger("isInvoice")) {
                            // gas.setIsInvoice(false);
                            // } else {
                            // gas.setIsInvoice(true);
                            // }
                            // mongoTemplate.insert(gas,"gas");
                            mallGasMapper.insert(gas);
                            JSONArray arr = JSONArray.parseArray(ob.get("itemInfoList").toString());
                            if (CollectionUtils.isNotEmpty(arr)) {
                                for (int j = 0; j < arr.size(); j++) {
                                    JSONObject ob2 = (JSONObject) arr.get(j);
                                    MallOli oli = new MallOli();
                                    oli.setGasId(gas.getId());
                                    oli.setId(GraphaiIdGenerator.nextId("MallOli"));
                                    oli.setOliNo(ob2.getString("itemId"));
                                    oli.setOliName(ob2.getString("itemName"));
                                    // oli.setYfqPrice();
                                    oli.setGunPrice(new BigDecimal(ob2.getString("storePrice")).divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP));
                                    oli.setOfficialPrice(new BigDecimal(ob2.getString("cityPrice")).divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP));
                                    // oli.setOilType(ob2.getInteger("oilType"));
                                    oli.setDeleted(false);
                                    oli.setAddTime(LocalDateTime.now());
                                    mallOliMapper.insert(oli);
                                    // mongoTemplate.insert(oli, "oli");
                                }
                            }
                            // } else {
                            // if (0 == ob.getInteger("isInvoice")) {
                            // gaslList.get(0).setIsInvoice(false);
                            // } else {
                            // gaslList.get(0).setIsInvoice(true);
                            // }
                            //
                            //
                            // gaslList.get(0).setGasId(ob.getString("gasId"));
                            // gaslList.get(0).setGasName(ob.getString("gasName"));
                            // gaslList.get(0).setGasType(ob.getInteger("gasType"));
                            // gaslList.get(0).setProvinceCode(ob.getInteger("provinceCode"));
                            // gaslList.get(0).setProvinceName(ob.getString("provinceName"));
                            // gaslList.get(0).setCityCode(ob.getInteger("cityCode"));
                            // gaslList.get(0).setCityName(ob.getString("cityName"));
                            // if (ObjectUtil.isNotNull(ob.getInteger("countyCode"))) {
                            // gaslList.get(0).setCountyCode(ob.getInteger("countyCode"));
                            // }
                            //
                            // if (StringUtils.isNotEmpty(ob.getString("countyName"))) {
                            // gaslList.get(0).setCountyName(ob.getString("countyName"));
                            // }
                            //
                            // gaslList.get(0).setGasLogoBig(ob.getString("gasLogoBig"));
                            // gaslList.get(0).setGasLogoSmall(ob.getString("gasLogoSmall"));
                            // gaslList.get(0).setGasAddress(ob.getString("gasAddress"));
                            // gaslList.get(0).setGasLongitude(ob.getDouble("gasAddressLongitude"));
                            // gaslList.get(0).setGasLatitude(ob.getDouble("gasAddressLatitude"));
                            // List<Double> doubles=new ArrayList<Double>(2);
                            // doubles.add(Double.parseDouble(ob.getString("gasAddressLongitude")));
                            // doubles.add(Double.parseDouble(ob.getString("gasAddressLatitude")));
                            // gaslList.get(0).setLocation(doubles);
                            //
                            // //mallGasMapper.updateByPrimaryKeySelective(gaslList.get(0));
                            // mongoTemplate.update(gaslList.get(0).getClass());
                            //
                            // JSONArray arr = JSONArray.parseArray(ob.get("oilPriceList").toString());
                            // if (CollectionUtils.isNotEmpty(arr)) {
                            // for (int j = 0; j < arr.size(); j++) {
                            // JSONObject ob2 = (JSONObject) arr.get(j);
                            // // 查询油站id和油号是否存在
                            // MallOliExample oliExample = new MallOliExample();
                            // oliExample.or().andGasIdEqualTo(gaslList.get(0).getId())
                            // .andOliNoEqualTo(ob2.getString("oilNo")).andDeletedEqualTo(false);
                            // List<MallOli> oList = mallOliMapper.selectByExample(oliExample);
                            // if (CollectionUtils.isEmpty(oList)) {
                            // MallOli oli = new MallOli();
                            // oli.setGasId(gaslList.get(0).getId());
                            // oli.setId(GraphaiIdGenerator.nextId("MallOli"));
                            // oli.setOliNo(ob2.getString("oilNo"));
                            // oli.setOliName(ob2.getString("oilName"));
                            // oli.setYfqPrice(new BigDecimal(ob2.getString("priceYfq")));
                            // oli.setGunPrice(new BigDecimal(ob2.getString("priceGun")));
                            // oli.setOfficialPrice(new BigDecimal(ob2.getString("priceOfficial")));
                            // oli.setOilType(ob2.getInteger("oilType"));
                            // oli.setDeleted(false);
                            // oli.setAddTime(LocalDateTime.now());
                            // //mallOliMapper.insert(oli);
                            // mongoTemplate.insert(oli, "oli");
                            // } else {
                            // oList.get(0).setOliName(ob2.getString("oilName"));
                            // oList.get(0).setYfqPrice(new BigDecimal(ob2.getString("priceYfq")));
                            // oList.get(0).setGunPrice(new BigDecimal(ob2.getString("priceGun")));
                            // oList.get(0).setOfficialPrice(new BigDecimal(ob2.getString("priceOfficial")));
                            // oList.get(0).setOilType(ob2.getInteger("oilType"));
                            // oList.get(0).setUpdateTime(LocalDateTime.now());
                            // //mallOliMapper.updateByPrimaryKeySelective(oList.get(0));
                            // mongoTemplate.update(oList.get(0).getClass());
                            // }
                            //
                            // }
                            // }
                            //
                            // }

                        }
                    }

                    total = Integer.parseInt(obj.get("totalSize").toString());
                    forTotal = (total / 100) + 1;
                    page = page + 1;
                    log.info("总条数" + total);

                }
            }

            return ResponseUtil.ok();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseUtil.fail();
    }

    /**
     * 根据用户查询油站状态和油价(小桔加油)
     *
     * @param userId 用户id
     * @return token
     */
    @GetMapping("/queryStorePrice")
    public Object queryStorePrice( /*
                                    * @NotEmpty String longitude, @NotEmpty String latitude, @RequestParam(defaultValue = "92") String
                                    * oliNo
                                    */) {


        String token = "";


        Map<String, Object> map = new LinkedHashMap<String, Object>();
        map.put("lon", "116.290276");
        map.put("lat", "40.043476");
        map.put("mobile", "13268286441");
        map.put("openChannel", 1);
        map.put("itemName", "92#");
        List<String> list = Arrays.asList("5165633531352193403", "5202188195790855162");
        map.put("storeIdList", list);

        JSONObject object = null;
        String jsonParam;
        try {


            // 小桔token缓存redis两小时自动失效
            Object home = NewCacheInstanceServiceLoader.xiaojuCache().get("xiaojuToken");
            if (home == null) {
                token = getToken();
            } else {
                token = String.valueOf(NewCacheInstanceServiceLoader.xiaojuCache().get("xiaojuToken"));
            }
            jsonParam = SecurityUtil.encryptSignRequest(map);
            String result = HttpUtil.sendPostJson(SecurityUtil.requestUrl + "/queryStorePrice", jsonParam, token);
            object = JSONObject.parseObject(result);
            if (Objects.equals("0", object.get("code"))) {
                String respoString = SecurityUtil.decryptResponse(object.getString("data"));
                JSONObject obj = JSONObject.parseObject(respoString);

                JSONArray array = JSONArray.parseArray(obj.get("itemInfoList").toString());
                if (CollectionUtils.isNotEmpty(array)) {
                    for (int i = 0; i < array.size(); i++) {
                        JSONObject ob = (JSONObject) array.get(i);
                        if (StringHelper.isNotNullAndEmpty(ob.getString("itemName")) && StringHelper.isNotNullAndEmpty(ob.getString("storeId"))) {
                            MallOli oli = mallOliService.queryOliByGasIdAndOliName(ob.getString("storeId"), ob.getString("itemName"));
                            if (null != oli) {
                                if (StringHelper.isNotNullAndEmpty(ob.getString("cityPrice"))) {
                                    oli.setOfficialPrice(new BigDecimal(ob.getString("cityPrice")).divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP));
                                }

                                if (StringHelper.isNotNullAndEmpty(ob.getString("storePrice"))) {
                                    oli.setGunPrice(new BigDecimal(ob.getString("storePrice")).divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP));
                                }

                                if (StringHelper.isNotNullAndEmpty(ob.getString("vipPrice"))) {
                                    oli.setYfqPrice(new BigDecimal(ob.getString("vipPrice")).divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP));
                                }

                                mallOliService.updateOli(oli);
                            }
                        }
                    }
                }

                return ResponseUtil.ok(obj);

            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return ResponseUtil.fail();

    }


    /**
     * 获取小桔加油详情url
     * 
     * @return token
     */
    @GetMapping("/queryEnergyUrl")
    public Object queryEnergyUrl(@LoginUser String userId, @NotEmpty String longitude, @NotEmpty String latitude, @RequestParam(defaultValue = "92#") String oliName,
                    @RequestParam(value = "gasId", required = true) String gasId) {


        if (userId == null) {
            return ResponseUtil.unlogin();
        }


        String token = "";

        MallUser user = userService.findById(userId);
        if (ObjectUtil.isNull(user) || Objects.equals(user.getMobile(), null)) {
            log.info("用户或用户手机号不存在!");
            return ResponseUtil.fail();
        }

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("lon", longitude);
        map.put("lat", latitude);
        map.put("openChannel", 1);
        map.put("mobile", user.getMobile());
        map.put("outUserId", userId);
        map.put("storeId", gasId);
        map.put("itemName", oliName);
        try {
            // 小桔token缓存redis两小时自动失效
            Object home =  NewCacheInstanceServiceLoader.xiaojuCache().get("xiaojuToken");
            if (home == null) {
                token = getToken();
            } else {
                token = String.valueOf( NewCacheInstanceServiceLoader.xiaojuCache().get("xiaojuToken"));
            }

            String jsonParam = SecurityUtil.encryptSignRequest(map);

            String result = HttpUtil.sendPostJson(SecurityUtil.requestUrl + "/queryEnergyUrl", jsonParam, token);

            JSONObject object = JSONObject.parseObject(result);
            if (Objects.equals("0", object.get("code"))) {
                String respoString = SecurityUtil.decryptResponse(object.getString("data"));
                if (StringHelper.isNotNullAndEmpty(respoString)) {
                    JSONObject obj = JSONObject.parseObject(respoString);
                    if (!Objects.equals(null, obj.getString("link"))) {
                        return ResponseUtil.ok(obj.getString("link"));
                    }
                }
            }

            return ResponseUtil.fail(511, object.getString("msg"));

        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseUtil.fail();
    }

}
