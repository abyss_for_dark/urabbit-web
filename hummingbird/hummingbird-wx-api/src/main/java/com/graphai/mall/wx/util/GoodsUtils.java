package com.graphai.mall.wx.util;

import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;

import com.graphai.commons.constant.MallConstant;
import com.graphai.mall.db.domain.MallDistributionSettings;
import com.graphai.mall.db.domain.MallGoods;
import com.graphai.mall.db.domain.MallRebateRecord;

public class GoodsUtils {

	/**
	 * 计算商品的省赚金额
	 * @param goods
	 * @return
	 */
	public static BigDecimal getSpreads(MallGoods goods){
		if( goods.getRetailPrice()!=null  && goods.getFirstRatio()!=null){
			if(MallConstant.GOODS_CAL_MODE_FIXED_VALUE.equals(goods.getCalModel())){
				return goods.getFirstRatio();
			}else if(MallConstant.GOODS_CAL_MODE_PROPORTIONAL_VALUE.equals(goods.getCalModel())){
				return goods.getRetailPrice().multiply(goods.getFirstRatio()).setScale(2, BigDecimal.ROUND_HALF_UP);
			}else{
				return goods.getRetailPrice().multiply(new BigDecimal(0.1)).setScale(2, BigDecimal.ROUND_HALF_UP);
			}
		}else  {
			//没有设置的情况下就按照默认的10% 计算
			return goods.getRetailPrice().multiply(new BigDecimal(0.1)).setScale(2, BigDecimal.ROUND_HALF_UP);
		}
	}
	
	/**
	 * 计算商品的省赚金额
	 * @param goods
	 * @return
	 */
	public static void setSpreads(List<MallGoods> goodsList){
		for (int i = 0; i < goodsList.size(); i++) {
			MallGoods goods = goodsList.get(i);
			goods.setSpreads(getSpreads(goods));
		}
	}
	
	/**
	 * 设置返利金额
	 * @param setting
	 * @param goodsList
	 */
	public static void setRebateAmount(MallDistributionSettings setting,List<MallGoods> goodsList){
		for (int i = 0; i < goodsList.size(); i++) {
			MallGoods goods = goodsList.get(i);
			//系统根据用户等级配置的返利比例
			String value  = setting.getDisIntegralValue();
			BigDecimal devalue = new  BigDecimal(value);
			//数据方返佣的比例
			Integer commissionValue = goods.getCommissionValue();
			if(commissionValue==null){
				commissionValue = 0;
			}
			BigDecimal decommissionValue = new  BigDecimal(commissionValue.toString());
			//进行运算
			BigDecimal rebateAmount = goods.getRetailPrice().multiply(decommissionValue).divide(new BigDecimal(100)).multiply(devalue).divide(new BigDecimal(100)).setScale(2, BigDecimal.ROUND_HALF_UP);
			goods.setRebateAmount(rebateAmount);
		}
	}
	
	public static void setRebateAmount(MallDistributionSettings setting, MallGoods goods){
		//系统根据用户等级配置的返利比例
		String value  = setting.getDisIntegralValue();
		BigDecimal devalue = new  BigDecimal(value);
		//数据方返佣的比例
		Integer commissionValue = goods.getCommissionValue();
		if(commissionValue==null){
			commissionValue = 0;
		}
		
//		System.out.println("用来计算的goods.getRetailPrice() ： " + goods.getRetailPrice());
//		System.out.println("用来计算的devalue ： " + devalue);
//		System.out.println("用来计算的commissionValue ： " + commissionValue);
		BigDecimal decommissionValue = new  BigDecimal(commissionValue.toString());
		//进行运算
		BigDecimal rebateAmount = goods.getRetailPrice().multiply(decommissionValue).divide(new BigDecimal(100)).multiply(devalue).divide(new BigDecimal(100)).setScale(2, BigDecimal.ROUND_HALF_UP);
//		System.out.println("算出来的rebateAmount ： " + rebateAmount);
		goods.setRebateAmount(rebateAmount);
	
	}
	
	
	
	/*说明分销原有设置分销金额
	 * 1 当前不加价
	 * 2 固定金额加价
	 * 3 比例金额加价
	 */
	public static void setRetailPrice(MallRebateRecord rebate,List<MallGoods> goodsList){
		if(rebate!=null){
			if("1".equals(rebate.getMarkupType())){
				
			}else if("2".equals(rebate.getMarkupType())){
				BigDecimal amount = rebate.getMarkupRange();
				for (int i = 0; i < goodsList.size(); i++) {
					MallGoods myGoods = goodsList.get(i);
					myGoods.setRetailPrice(myGoods.getRetailPrice().add(amount));
				}
			}else if("3".equals(rebate.getMarkupType())){
				BigDecimal proportion = rebate.getMarkupRange();
				for (int i = 0; i < goodsList.size(); i++) {
					MallGoods myGoods = goodsList.get(i);
					
					BigDecimal bili = proportion.divide(new BigDecimal(String.valueOf(100)));
					BigDecimal addAmount = myGoods.getRetailPrice().multiply(bili.setScale(2,BigDecimal.ROUND_HALF_UP)).setScale(2, BigDecimal.ROUND_HALF_UP);
					myGoods.setRetailPrice(myGoods.getRetailPrice().add(addAmount));
				}
				
			}
		}
	}
	
	/*说明分销原有设置分销金额
	 * 1 当前不加价
	 * 2 固定金额加价
	 * 3 比例金额加价
	 */
	public static void setRetailPrice(MallRebateRecord rebate,MallGoods myGoods){
		if(rebate!=null){
			if("1".equals(rebate.getMarkupType())){
			}else if("2".equals(rebate.getMarkupType())){
				BigDecimal amount = rebate.getMarkupRange();
				myGoods.setRetailPrice(myGoods.getRetailPrice().add(amount));
			
			}else if("3".equals(rebate.getMarkupType())){
				BigDecimal proportion = rebate.getMarkupRange();
				BigDecimal bili = proportion.divide(new BigDecimal(100));
				BigDecimal addAmount = myGoods.getRetailPrice().multiply(bili.setScale(2,BigDecimal.ROUND_HALF_UP)).setScale(2, BigDecimal.ROUND_HALF_UP);
				myGoods.setRetailPrice(myGoods.getRetailPrice().add(addAmount));
			}
		}
	}
	
	public static BigDecimal getRetailPrice(MallRebateRecord rebate,BigDecimal nowAmount){
		if(rebate!=null){
			if("1".equals(rebate.getMarkupType())){
				return nowAmount;
			}else if("2".equals(rebate.getMarkupType())){
				BigDecimal amount = rebate.getMarkupRange();
				return nowAmount.add(amount);
			}else if("3".equals(rebate.getMarkupType())){
				BigDecimal proportion = rebate.getMarkupRange();
				BigDecimal bili = proportion.divide(new BigDecimal(100));
				BigDecimal addAmount = nowAmount.multiply(bili.setScale(2,BigDecimal.ROUND_HALF_UP)).setScale(2, BigDecimal.ROUND_HALF_UP);
				
				return nowAmount.add(addAmount);
				//BigDecimal proportion = rebate.getMarkupRange();
				//return nowAmount.multiply(proportion);
			}
		}else{
			return nowAmount;
		}
		return nowAmount;
	}
	
	/**
	 * 通过规则获取加价部分的金额
	 * @param rebate
	 * @param nowAmount
	 * @return
	 */
	public static BigDecimal getAddAmount(MallRebateRecord rebate,BigDecimal nowAmount){
		if(rebate!=null){
			if("1".equals(rebate.getMarkupType())){
				return new BigDecimal(0);
			}else if("2".equals(rebate.getMarkupType())){
				BigDecimal amount = rebate.getMarkupRange();
				return amount;
			}else if("3".equals(rebate.getMarkupType())){
				BigDecimal proportion = rebate.getMarkupRange();
				BigDecimal bili = proportion.divide(new BigDecimal(100));
				BigDecimal addAmount = nowAmount.multiply(bili).setScale(2, BigDecimal.ROUND_HALF_UP);
				return addAmount;
			}
		}else{
			return new BigDecimal(0);
		}
		return new BigDecimal(0);
	}
	 
}
