package com.graphai.mall.wx.web.other;

import cn.binarywang.wx.miniapp.api.WxMaService;
import cn.binarywang.wx.miniapp.api.impl.WxMaServiceImpl;
import cn.binarywang.wx.miniapp.bean.WxMaJscode2SessionResult;
import cn.binarywang.wx.miniapp.bean.WxMaRunStepInfo;
import cn.binarywang.wx.miniapp.config.impl.WxMaDefaultConfigImpl;
import com.alibaba.fastjson.JSONObject;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.json.JacksonUtil;
import com.graphai.commons.util.lang.StringHelper;
import com.graphai.mall.db.domain.GameMotionMakeMoney;
import com.graphai.mall.db.domain.MallUser;
import com.graphai.mall.db.service.game.GameMotionMakeMoneyService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.properties.WxProperties;
import me.chanjar.weixin.common.error.WxErrorException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@RestController
@RequestMapping("/wx/mini")
@Validated
public class MiniController {

    @Autowired
    private WxProperties properties;

    @Autowired
    private MallUserService mallUserService;

    @Autowired
    private GameMotionMakeMoneyService gameMotionMakeMoneyService;

    private final Log logger = LogFactory.getLog(MiniController.class);

    /**
     * 解析微信运动步数
     *
     * @param data
     * @return
     * @throws org.apache.http.ParseException
     * @throws IOException
     */
    @PostMapping(value = "/decryptRunInfo")
    public Object decryptWeChatRunInfo(@LoginUser String userId, @RequestBody String data) {
        logger.info("-------------------------解析微信运动步数----------------------" + JSONObject.toJSONString(data)
                        + "-------------------------------------");

        String encryptedData = JacksonUtil.parseString(data, "encryptedData");
        String ivStr = JacksonUtil.parseString(data, "ivStr");
        String sessionKey = JacksonUtil.parseString(data, "sessionKey");
        String code = JacksonUtil.parseString(data, "code");

        Map<String, Object> map = new HashMap<String, Object>();
        try {
            WxMaService wxMaService = new WxMaServiceImpl();
            WxMaDefaultConfigImpl wxMaDefaultConfig = new WxMaDefaultConfigImpl();
            wxMaDefaultConfig.setAppid(properties.getHsAppId());
            wxMaDefaultConfig.setSecret(properties.getHsAppSecret());
            wxMaService.setWxMaConfig(wxMaDefaultConfig);
            if (StringHelper.isNullOrEmpty(sessionKey)) {
                WxMaJscode2SessionResult wxMaJscode2SessionResult = wxMaService.getUserService().getSessionInfo(code);
                sessionKey = wxMaJscode2SessionResult.getSessionKey();
                logger.info("-------------------------获取微信用户的sessionKey----------------------"
                                + JSONObject.toJSONString(wxMaJscode2SessionResult)
                                + "-------------------------------------");
            }
            List<WxMaRunStepInfo> wxMaUserInfo =
                            wxMaService.getRunService().getRunStepInfo(sessionKey, encryptedData, ivStr);
            if (CollectionUtils.isNotEmpty(wxMaUserInfo)) {
                MallUser user = mallUserService.findById(userId);
                if (!Objects.equals(null, user)) {
                    GameMotionMakeMoney gm = gameMotionMakeMoneyService.selectMontionAlreadySteps(userId, "2");
                    GameMotionMakeMoney gm1 = gameMotionMakeMoneyService.selectMontionAlreadySteps(userId, "1");
                    String step = JacksonUtil.parseString(
                                    String.valueOf(wxMaUserInfo.get(wxMaUserInfo.size() - 1).getStep()), "steps");


                    LocalDateTime now = LocalDateTime.now();
                    LocalDateTime cre = gm.getCreateTime();
                    Duration duration = Duration.between(cre, now);
                    long a = duration.toMillis() / 1000;// 过了多少秒

                    if (null != gm) {
                        gm.setSteps(Long.parseLong(step) + "");
                        logger.info("后台步数--step-----" + step);
                        gameMotionMakeMoneyService.updateMontionStep(gm);
                    } else {
                        if (null == step) {
                            gameMotionMakeMoneyService.insertMotionSteps(userId, "0", "2");
                        }
                        gameMotionMakeMoneyService.insertMotionSteps(userId, step, "2");
                    }

                    if (null == gm1) {
                        gameMotionMakeMoneyService.insertMotionSteps(userId, "0", "1");
                    }
                }

            }

            map.put("wxMaUserInfo", wxMaUserInfo);
        } catch (WxErrorException e) {
            e.printStackTrace();
        }
        return ResponseUtil.ok(map);
    }



}
