package com.graphai.mall.wx.service;


import java.time.LocalDateTime;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.graphai.mall.db.dao.MallMarketingActivityMapper;
import com.graphai.mall.db.domain.MallMarketingActivity;
import com.graphai.mall.db.domain.MallMarketingActivityExample;
import com.graphai.mall.db.domain.MallOrder;
import com.graphai.mall.db.service.order.MallOrderService;
import com.graphai.mall.wx.util.StringUtils;

@Service
public class WxMallMarketingActivityService {

    @Resource
    private MallMarketingActivityMapper mallMarketingActivityMapper;
    @Autowired
    private MallOrderService orderService;

    public int editActivity(MallMarketingActivity mallMarketingActivity) {
        mallMarketingActivity.setUpdateTime(LocalDateTime.now());
        MallMarketingActivityExample example = new MallMarketingActivityExample();
        example.createCriteria().andIdEqualTo(mallMarketingActivity.getId());
        return mallMarketingActivityMapper.updateByExampleSelective(mallMarketingActivity, example);
    }

    public MallMarketingActivity selectActivity(String userId) {

        MallMarketingActivityExample example = new MallMarketingActivityExample();
        MallMarketingActivityExample.Criteria criteria = example.createCriteria();
        criteria.andDeletedEqualTo(false);
        if (!StringUtils.isBlank(userId)) {
            criteria.andIdEqualTo(userId);
        }
        return mallMarketingActivityMapper.selectOneByExample(example);
    }

    public List<MallOrder> find998Order(String userId) {
        return orderService.find998Order(userId, "34");
    }
}
