package com.graphai.mall.wx.web.promotion;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.mall.db.domain.MallGoods;
import com.graphai.mall.db.domain.MallTopic;
import com.graphai.mall.db.service.goods.MallGoodsService;
import com.graphai.mall.db.service.promotion.MallTopicService;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.validator.Order;
import com.graphai.validator.Sort;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 专题服务
 */
@RestController
@RequestMapping("/wx/topic")
@Validated
public class WxTopicController {
    private final Log logger = LogFactory.getLog(WxTopicController.class);

    @Autowired
    private MallTopicService topicService;
    @Autowired
    private MallGoodsService goodsService;

    /**
     * 专题列表
     *
     * @param page 分页页数
     * @param limit 分页大小
     * @return 专题列表
     */
    @GetMapping("list")
    public Object list(
    		@RequestParam(defaultValue = "1") String cclassId,
    		@RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit,
                       @Sort @RequestParam(defaultValue = "id") String sort,
                       @Order @RequestParam(defaultValue = "desc") String order) {
        List<MallTopic> topicList = topicService.queryList(cclassId,page, limit, sort, order);
        Map<String, Object> data = new HashMap<String, Object>();
        List<String> topicIds = new ArrayList<String>();
        if(CollectionUtils.isNotEmpty(topicList)){
        	 for (int i = 0; i < topicList.size(); i++) {
        		 topicIds.add(topicList.get(i).getId());
      		}
        	 
        	 
        	 List<Map<String,Object>> retDataList = new ArrayList<Map<String,Object>>();
     		
             /**
              * 获取商品数据
              */
             List<MallGoods> goodsList =  goodsService.getTopicGoods(topicIds);
             if(CollectionUtils.isNotEmpty(goodsList)){
             	for (int i = 0; i < topicList.size(); i++) {
             		Map<String,Object> retData = new HashMap<String, Object>();
             		MallTopic topic =topicList.get(i);
             		
             		List<MallGoods> topicGoods = new ArrayList<MallGoods>();
         			CollectionUtils.select(goodsList, new Predicate() {
         				@Override
         				public boolean evaluate(Object object) {
         					MallGoods  goods = (MallGoods) object;
         					return (topic!=null && topic.getId().equals(goods.getTopicId())) ? true : false;
         				}},topicGoods);
         			
         			
         			retData.put("topic", topic);
         			retData.put("goodsInfoList", topicGoods);
         			
         			retDataList.add(retData);
          		}
     			
             }
             
             int total = topicService.queryTotal();
           
             data.put("data", retDataList);
             data.put("count", total);
        }
        return ResponseUtil.ok(data);
       
    }

    /**
     * 专题详情
     *
     * @param id 专题ID
     * @return 专题详情
     */
    @GetMapping("detail")
    public Object detail(@LoginUser String userId,@NotNull String id) {
    	if (userId == null) {
			return ResponseUtil.unlogin();
		}
        Map<String, Object> data = new HashMap<>();
        MallTopic topic = topicService.findById(id);
        data.put("topic", topic);
        List<MallGoods> goods = new ArrayList<>();
        for (String i : topic.getGoods()) {
            MallGoods good = goodsService.findByIdVO(i);
            if (null != good)
                goods.add(good);
        }
        data.put("goods", goods);
        return ResponseUtil.ok(data);
    }

    /**
     * 相关专题
     *
     * @param id 专题ID
     * @return 相关专题
     */
    @GetMapping("related")
    public Object related(@NotNull String id) {
        List<MallTopic> topicRelatedList = topicService.queryRelatedList(id, 0, 4);
        return ResponseUtil.ok(topicRelatedList);
    }
}