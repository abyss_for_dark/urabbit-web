package com.graphai.mall.wx.web.user;

import cn.hutool.core.util.StrUtil;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.json.JacksonUtil;
import com.graphai.commons.util.lock.MutexLock;
import com.graphai.commons.util.lock.MutexLockUtils;
import com.graphai.id.GraphaiIdGenerator;
import com.graphai.mall.db.dao.MallUserMapper;
import com.graphai.mall.db.domain.*;
import com.graphai.mall.db.service.*;
import com.graphai.mall.db.service.account.AccountService;
import com.graphai.mall.db.constant.account.AccountStatus;
import com.graphai.mall.db.service.account.FcAccountPrechargeBillService;
import com.graphai.mall.db.service.account.FcAccountWithdrawBillService;
import com.graphai.mall.db.service.order.MallOrderGoodsService;
import com.graphai.mall.db.service.sysconfig.MallSystemConfigService;
import com.graphai.mall.db.service.wangzhuan.WangZhuanTaskChanceService;
import com.graphai.mall.db.util.account.AccountUtils;
import com.graphai.mall.db.constant.CommonConstant;
import com.graphai.mall.db.util.phone.PushUtils;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.mall.wx.web.common.MyBaseController;

import com.vdurmont.emoji.EmojiParser;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.*;


/**
 * 收益报告
 */
@RestController
@RequestMapping("/wx/Profit")
@Validated
public class WxProfitController extends MyBaseController {

    private final Log log = LogFactory.getLog(WxProfitController.class);

    @Autowired
    private MallOrderGoodsService mallOrderGoodsService;
    @Resource
    private FcAccountWithdrawBillService fcAccountWithdrawBillService;
    @Resource
    private MallUserMapper userMapper;
    @Resource
    private AccountService accountService;
    @Resource
    private FcAccountPrechargeBillService fcAccountPrechargeBillService;
    @Autowired
    private WangZhuanTaskChanceService wangZhuanTaskChanceService;
    @Autowired
    private MallSystemConfigService mallSystemConfigService;

    @Autowired
    private WxEnterpriseTransferService wxEnterpriseTransferService;

    @Value("${mall.wx.hs-applets-app-id}")
    private String hs_appId;//appid

    /**
     * 查询收益报告
     * @param userId
     * @return
     */

    @RequestMapping(value = "/report", produces = "application/json;charset=UTF-8;")
    public Object getProfitReport(@LoginUser String userId) {
        if (null == userId) {
            return ResponseUtil.unlogin();
        }
        LocalDate createtime = LocalDate.now();
        DateTimeFormatter dtf2 = DateTimeFormatter.ofPattern("yyyy-MM");
        /** 1.提现报告 **/
        Map<String, Object> withdrawMap = new HashMap<String, Object>();
        int year = LocalDate.now().getYear();    //年
        int month = LocalDate.now().getMonthValue();    //月
        LocalDate date = LocalDate.of(year, month, 1);
        LocalDateTime beginDate = LocalDateTime.of(date, LocalTime.of(0, 0, 0));    //开始时间
        LocalDateTime endDate = beginDate.plusMonths(1);    //结束时间
        
        //本月待入账金额
        BigDecimal withdrawableMoney = fcAccountPrechargeBillService.selectPrechargeAmountByCondition(userId,true,null, AccountStatus.DISTRIBUTION_TYPE_00, CommonConstant.TASK_REWARD_CASH, beginDate, endDate);
        //累计提现金额
        BigDecimal withdrawnMoney = fcAccountWithdrawBillService.selectWithdrawBillAmountByCondition(userId,true,null,AccountStatus.WITHDRAW_STATUS_FINISH,null, null);

        //当日销售额
        BigDecimal mapsum = mallOrderGoodsService.selectAmountCount("0", userId, createtime.toString());
        //当日 当月 各业务销售额
        Map<String,Object> dataMap = mallOrderGoodsService.selectAmountCountBy( userId, createtime.toString());
        //当月销售额
        BigDecimal monthsum = mallOrderGoodsService.selectAmountCount("2", userId, dtf2.format(createtime));

        FcAccount fcAccount = accountService.getUserAccount(userId);
        BigDecimal money = new BigDecimal("0.00");
        if (null != fcAccount && null != fcAccount.getAmount()) {
            money = fcAccount.getAmount();
        }

        if (null == mapsum) {
            dataMap.put("daySum", 0.00);
        } else {
            dataMap.put("daySum", mapsum.setScale(2, BigDecimal.ROUND_DOWN));
        }
        if (null == monthsum) {
            dataMap.put("monthSum", 0.00);
        } else {
            dataMap.put("monthSum", monthsum.setScale(2, BigDecimal.ROUND_DOWN));
        }


        dataMap.put("withdrawableMoney", withdrawableMoney);
        dataMap.put("withdrawnMoney", withdrawnMoney);
        dataMap.put("money", money);

        return ResponseUtil.ok(dataMap);
    }

    /**
     * （房淘）支付宝提现
     */
    @Transactional(rollbackFor = Exception.class)
    @RequestMapping(value = "/withdrawal/ali", produces = "application/json;charset=UTF-8;")
    public Object getWithdrawalAli(@LoginUser String userId,
                                @RequestBody String data)  {
        if (null == userId) {
            return ResponseUtil.unlogin();
        }
        String typeW = JacksonUtil.parseString(data, "typeW"); //提现类型（04小程序零钱提现 05 小程序任务提现）
        String name = JacksonUtil.parseString(data, "name"); //真实姓名
        String account = JacksonUtil.parseString(data, "account"); //支付宝账户
        String money = JacksonUtil.parseString(data, "money"); //提现金额
        // 提现账户金额类型（1（购物，邀请好友）的现金, 2 （金币兑换，领红包）的现金 ）
        String status = JacksonUtil.parseString(data, "status");

        BigDecimal bd = new BigDecimal(money);
        bd = bd.setScale(2,BigDecimal.ROUND_DOWN);//提现金额默认取两位

        FcAccount fcAccount = accountService.getUserAccount(userId);

        //查询用户信息
        MallUserExample userExample = new MallUserExample();
        userExample.createCriteria().andIdEqualTo(userId);
        MallUser mallUser = userMapper.selectOneByExampleSelective(userExample);

        //提现流水
        FcAccountWithdrawBill fcAccountWithdrawBill = new FcAccountWithdrawBill();
        fcAccountWithdrawBill.setCustomerId(userId);
        fcAccountWithdrawBill.setWithdrawType("01");//01 --收益（零钱）提现    02 --任务奖励提现 04 小程序任务提现 05 小程序零钱提现

        //查询当天有没有提现过
        LocalDate now = LocalDate.now();
        LocalDateTime start = LocalDateTime.of(now, LocalTime.of(00,00,00));
        LocalDateTime end = LocalDateTime.of(now, LocalTime.of(23,59,59));
        //查询当天有没有提现过
        List<FcAccountWithdrawBill> list = fcAccountWithdrawBillService.selectFcAccountWithdrawBillList2(fcAccountWithdrawBill,start,end,"1");

        //提现规则
        Map map = mallSystemConfigService.listRule();
        //提现次数
        String number = (String)map.get("Mall_withdrawal_task_rule_number");
        if(StringUtils.isEmpty(number)){
            //如果为空 默认给一个次数防止出错
            number="1";
        }
        //提现每笔金额
        String amount = (String)map.get("Mall_withdrawal_task_rule_amount");

        String[] strs = amount.split(",");
        BigDecimal min = new BigDecimal(strs[0]);
        BigDecimal max = new BigDecimal(strs[1]);

        if(list.size() >= Integer.parseInt(number)){
            //超出提现次数
            return ResponseUtil.fail(403, "您今日提现次数已用完，请明日再来！");
        }
        if (bd.compareTo(new BigDecimal(0.00)) <= 0 || min.compareTo(bd) > 0 || max.compareTo(bd) < 0) {
            //当提现金额小于0，小于最小提现金额，大于最大提现金额，抛出异常
            return ResponseUtil.fail(403, "提现金额不符合提现规则！");
        }
        if(bd.compareTo(fcAccount.getAmount()) > 0){
            //提现金额大于账户余额
            return ResponseUtil.fail(403, "提现额度不足！");
        }


        fcAccountWithdrawBill.setCustName(name);
        fcAccountWithdrawBill.setWithdrawAccount(account);
        fcAccountWithdrawBill.setWithdrawAccountType("2");//账户类型  1 银行卡 2 支付宝 3 微信
        fcAccountWithdrawBill.setNote("支付宝提现");
        fcAccountWithdrawBill.setPayOrderNum("支付宝流水");	//支付宝订单流水
        fcAccountWithdrawBill.setWithdrawState("WAIT_AUDIT");// 提现状态:WAIT_AUDIT等待审核 WAIT_PAY:审核成功，等待提现 FINISH:提现完成 AUDIT_
        fcAccountWithdrawBill.setUserName(EmojiParser.parseToAliases(mallUser.getNickname()));
        fcAccountWithdrawBill.setBillId(GraphaiIdGenerator.nextId("FcAccountWithdrawBill"));
        fcAccountWithdrawBill.setAccountId(fcAccount.getAccountId());
        fcAccountWithdrawBill.setAmt(bd);
        fcAccountWithdrawBill.setCreateDate(LocalDateTime.now());
        fcAccountWithdrawBill.setWithdrawDate(LocalDateTime.now());
        fcAccountWithdrawBill.setCheckStatus("00C");//00C 未对账
        fcAccountWithdrawBill.setStatus((short) 1);//1 存在   0 删除

        Map paramsMap = new HashMap();
        paramsMap.put("dType", AccountStatus.DISTRIBUTION_TYPE_00);  //直接分润
        paramsMap.put("capitalTrend",AccountStatus.CAPITAL_TREND_02);//出账
        paramsMap.put("changeType",AccountStatus.CHANGE_TYPE_03);//提现
        paramsMap.put("message","收益零钱提现申请");
        paramsMap.put("changeAccountType",AccountStatus.CHANGE_ACCOUNT_TYPE_1);
        paramsMap.put("tradeType",AccountStatus.TRADE_TYPE_07);
        MutexLock mutexLock = MutexLockUtils.getMutexLock(userId);
        synchronized (mutexLock) {
            try {
                AccountUtils.accountChange(userId, bd, CommonConstant.TASK_REWARD_CASH, null, paramsMap);
                fcAccountWithdrawBillService.add(fcAccountWithdrawBill);
            } catch (Exception e) {
                e.printStackTrace();
                log.info("收益零钱提现失败："+e.getMessage());
                return ResponseUtil.fail(403,"提现失败！");
            }
            MutexLockUtils.remove(userId);
        }

        return ResponseUtil.ok();
    }




    /**
     * （收益，金币兑换金额）提现
     */
    @Transactional(rollbackFor = Exception.class)
    @RequestMapping(value = "/withdrawal", produces = "application/json;charset=UTF-8;")
    public Object getWithdrawal(@LoginUser String userId,
                                @RequestBody String data)  {
        if (null == userId) {
            return ResponseUtil.unlogin();
        }
        String typeW = JacksonUtil.parseString(data, "typeW"); //提现类型（04小程序零钱提现 05 小程序任务提现）
        String type = JacksonUtil.parseString(data, "type"); //账户类型（01微信 02 支付宝）
        String name = JacksonUtil.parseString(data, "name"); //真实姓名
        String account = JacksonUtil.parseString(data, "account"); //支付宝账户
        String money = JacksonUtil.parseString(data, "money"); //提现金额
        // 提现账户金额类型（1（购物，邀请好友）的现金, 2 （金币兑换，领红包）的现金 ）
        String status = JacksonUtil.parseString(data, "status");

        BigDecimal bd = new BigDecimal(money);
        bd = bd.setScale(2,BigDecimal.ROUND_DOWN);//提现金额默认取两位

        FcAccount fcAccount = accountService.getUserAccount(userId);
        BigDecimal fca = new BigDecimal(0.00);
        BigDecimal fca1 = new BigDecimal(0.00);

        //查询用户信息
        MallUserExample userExample = new MallUserExample();
        userExample.createCriteria().andIdEqualTo(userId);
        MallUser mallUser = userMapper.selectOneByExampleSelective(userExample);

        String message = "【$授*权*信*息*已*过*期&】";
        if(message.equals(mallUser.getWeixinOpenid())){
            return ResponseUtil.fail("微信绑定冲突，请重新绑定！");
        }else if (StrUtil.isBlank(mallUser.getWeixinMiniOpenid()) && StrUtil.isBlank(mallUser.getMiniProgramOpenid())){
            return ResponseUtil.fail("微信授权过期，请重新授权！");
        }

        if (null != fcAccount) {
            if("1".equals(status)){
                if (null != fcAccount.getAmount() && fcAccount.getAmount().compareTo(new BigDecimal(0.00)) == 1) {
                    fca1 = fcAccount.getAmount();//账户1可提现金额 ,零钱账户
                } else {
                    return ResponseUtil.fail(403, "账户无可提现余额");
                }
             }
            if("2".equals(status)){
                if( null != fcAccount.getExchangeAmount() && fcAccount.getExchangeAmount().compareTo(new BigDecimal(0.00)) == 1){
                    fca = fcAccount.getExchangeAmount();//账户2可提现金额，收益账户
                }else {
                    return ResponseUtil.fail(403, "账户无可提现余额");
                }
            }
            //提现机会
            if("3".equals(status)){
                if( null != fcAccount.getExchangeAmount() && fcAccount.getExchangeAmount().compareTo(new BigDecimal(0.00)) == 1){
                    fca = fcAccount.getExchangeAmount();//账户2可提现金额，收益账户
                }else {
                    return ResponseUtil.fail(403, "账户无可提现余额");
                }
            }

        }

        FcAccountWithdrawBill fcAccountWithdrawBill = new FcAccountWithdrawBill();
        BigDecimal acc2= new BigDecimal(0.00);//账户2提现后的金额
        BigDecimal acc1= new BigDecimal(0.00);//账户1提现后的金额

        //微信
        if ("01".equals(type)) {
            fcAccountWithdrawBill.setWithdrawAccountType("3");//账户类型   1 银行卡 2 支付宝 3 微信
            fcAccountWithdrawBill.setNote("微信提现");
            fcAccountWithdrawBill.setPayOrderNum("微信流水");	//微信订单流水
            fcAccountWithdrawBill.setCustName(EmojiParser.parseToAliases(mallUser.getNickname()));
        }
        //支付宝
        else if ("02".equals(type)) {
            fcAccountWithdrawBill.setCustName(name);
            fcAccountWithdrawBill.setWithdrawAccount(account);
            fcAccountWithdrawBill.setWithdrawAccountType("2");//账户类型  1 银行卡 2 支付宝 3 微信
            fcAccountWithdrawBill.setNote("支付宝提现");
            fcAccountWithdrawBill.setPayOrderNum("支付宝流水");	//支付宝订单流水
        }

      //  {type: "01", money: "0.3", status: 2, typeW: "05", sign: "99E304B202ACCF0B3CC6AA28F3977E19"}
        //查询当天有没有提现过
        LocalDate now = LocalDate.now();
        LocalDateTime start = LocalDateTime.of(now, LocalTime.of(00,00,00));
        LocalDateTime end = LocalDateTime.of(now, LocalTime.of(23,59,59));

        FcAccountWithdrawBill withdrawBill = new FcAccountWithdrawBill();
        withdrawBill.setCustomerId(userId);
        //收益零钱提现
        if("1".equals(status)){
            if("04".equals(typeW)){
                withdrawBill.setWithdrawType("04");
            }else{
                withdrawBill.setWithdrawType("01");
            }
        }
        //任务赚钱提现
        else if("2".equals(status)){
            if("05".equals(typeW)){
                withdrawBill.setWithdrawType("05");
            }else{
                withdrawBill.setWithdrawType("02");
            }
        }
        //查询当天有没有提现过
        List<FcAccountWithdrawBill> list = fcAccountWithdrawBillService.selectFcAccountWithdrawBillList2(withdrawBill,start,end,"1");
        // todo 待修改查历史记录，看有没有提过，如果没提过，第一次大于0.3少于1元免审核到账
        List<FcAccountWithdrawBill> list1 = fcAccountWithdrawBillService.selectFcAccountWithdrawBillList2(withdrawBill,null,null,"1");

        //查询当天有没有提现机会，有则使用
        List<WangzhuanTaskWithdrawaChancelSum> wlist = wangZhuanTaskChanceService.selectWithdrawaChancelSum(userId);

        //提现规则
        Map map = mallSystemConfigService.listRule();

        /** todo 使用提现机会提现 */
        if("3".equals(status)){
            if(wlist.size()>0){
                if(bd.compareTo(wlist.get(0).getChanceNum()) > 0){
                    return ResponseUtil.fail(403, "提现额度不足！");
                }else if(fca.compareTo(wlist.get(0).getChanceNum()) < 0){
                    return ResponseUtil.fail(403, "账户可提现金额不足！");
                }else if(fca.compareTo(wlist.get(0).getChanceNum()) >= 0){
                    acc2 = fca.subtract(wlist.get(0).getChanceNum());//账户2提现后的金额
                }
//                fcAccount.setExchangeAmount(acc2);
                //fcAccount.setFreezeCashAmount(free);
                fcAccountWithdrawBill.setUserName(EmojiParser.parseToAliases(mallUser.getNickname()));
                fcAccountWithdrawBill.setBillId(GraphaiIdGenerator.nextId("FcAccountWithdrawBill"));
                fcAccountWithdrawBill.setAccountId(fcAccount.getAccountId());
                fcAccountWithdrawBill.setCustomerId(userId);
                fcAccountWithdrawBill.setAmt(wlist.get(0).getChanceNum());
                fcAccountWithdrawBill.setCreateDate(LocalDateTime.now());
                fcAccountWithdrawBill.setUpdateDate(LocalDateTime.now());
                fcAccountWithdrawBill.setWithdrawDate(LocalDateTime.now());
                if(!StringUtils.isEmpty(typeW) && "05".equals(typeW)){
                    fcAccountWithdrawBill.setWithdrawType("05");//01 --收益（零钱）提现    02 --任务奖励提现 04 小程序任务提现 05 小程序零钱提现
                }else{
                    fcAccountWithdrawBill.setWithdrawType("02");//01 --收益（零钱）提现    02 --任务奖励提现 04 小程序任务提现 05 小程序零钱提现
                }
                fcAccountWithdrawBill.setCheckStatus("00C");//00C 未对账
                fcAccountWithdrawBill.setStatus((short) 1);//1 存在   0 删除
                fcAccountWithdrawBill.setWithdrawState("WAIT_AUDIT"); // 提现状态:WAIT_AUDIT等待审核 WAIT_PAY:审核成功，等待提现 FINISH:提现完成 AUDIT_
                fcAccountWithdrawBill.setAuditFlag((short) 1);
                fcAccountWithdrawBill.setWithdrawChance("1");//使用提现机会

                Map paramsMap = new HashMap();
                paramsMap.put("dType", AccountStatus.DISTRIBUTION_TYPE_00);  //直接分润
                paramsMap.put("capitalTrend",AccountStatus.CAPITAL_TREND_02);//出账
                paramsMap.put("changeType",AccountStatus.CHANGE_TYPE_03);//提现
                paramsMap.put("message","任务提现申请");
                paramsMap.put("changeAccountType",AccountStatus.CHANGE_ACCOUNT_TYPE_2);
                paramsMap.put("tradeType",AccountStatus.TRADE_TYPE_03);
                if(wlist.get(0).getChanceNum().compareTo(new BigDecimal(0.00)) == 1){
                    try {
                        AccountUtils.accountChangeByExchange(userId, wlist.get(0).getChanceNum(), CommonConstant.TASK_REWARD_CASH,null,paramsMap);
                        fcAccountWithdrawBillService.add(fcAccountWithdrawBill);
                    } catch (Exception e) {
                        e.printStackTrace();
                        log.info("提现失败："+e.getMessage());
                        return ResponseUtil.fail(403,"提现失败！");
                    }
                }
                //更新机会状态已使用
                wlist.get(0).setStatus(1);
                //更新机会状态
                wangZhuanTaskChanceService.updateWithdrawaChancelSumByWeek(wlist.get(0));
            }
        }
        /** todo（金币兑换，领红包）的现金 提现需要卡点 */
        else if ("2".equals(status)) {
            //提现次数
            String number = (String)map.get("Mall_withdrawal_task_rule_number");
            if(!StringUtils.isEmpty(number)){
            }else{
                //如果为空 默认给一个次数防止出错
                number="1";
            }
            if(!StringUtils.isEmpty(number)){
                //提现每笔金额
                String amount = (String)map.get("Mall_withdrawal_task_rule_amount");
                //不用过审金额
                String amount1 = (String)map.get("Mall_withdrawal_task_rule_amount1");
                //不用过审次数
                String examine = (String)map.get("Mall_withdrawal_task_rule_examine");
                    if(!StringUtils.isEmpty(examine)){
                    }else{
                        //如果为空 默认给一个次数防止出错
                        examine="1";
                    }
                    fcAccountWithdrawBill.setUserName(EmojiParser.parseToAliases(mallUser.getNickname()));
                    fcAccountWithdrawBill.setBillId(GraphaiIdGenerator.nextId("FcAccountWithdrawBill"));
                    fcAccountWithdrawBill.setAccountId(fcAccount.getAccountId());
                    fcAccountWithdrawBill.setCustomerId(userId);
                    fcAccountWithdrawBill.setAmt(bd);
                    fcAccountWithdrawBill.setCreateDate(LocalDateTime.now());
                    fcAccountWithdrawBill.setUpdateDate(LocalDateTime.now());
                    fcAccountWithdrawBill.setWithdrawDate(LocalDateTime.now());
                    fcAccountWithdrawBill.setCheckDate(LocalDateTime.now());
                    if(!StringUtils.isEmpty(typeW) && "05".equals(typeW)){
                        //01 --收益（零钱）提现    02 --任务奖励提现 04 小程序任务提现 05 小程序零钱提现
                        fcAccountWithdrawBill.setWithdrawType("05");
                    }else{
                        //01 --收益（零钱）提现    02 --任务奖励提现 04 小程序任务提现 05 小程序零钱提现
                        fcAccountWithdrawBill.setWithdrawType("02");
                    }
                    //00C 未对账
                    fcAccountWithdrawBill.setCheckStatus("00C");
                   //1 存在   0 删除
                    fcAccountWithdrawBill.setStatus((short) 1);

                    fcAccountWithdrawBill.setAuditFlag((short) 1);
                    String resson = "微信打款";
                    Map<String,Object> paramsMap = new HashMap();
                    //直接分润
                    paramsMap.put("dType", AccountStatus.DISTRIBUTION_TYPE_00);
                     //出账
                    paramsMap.put("capitalTrend",AccountStatus.CAPITAL_TREND_02);
                    //提现
                    paramsMap.put("changeType",AccountStatus.CHANGE_TYPE_03);
                    paramsMap.put("message","任务提现申请");
                    paramsMap.put("changeAccountType",AccountStatus.CHANGE_ACCOUNT_TYPE_2);
                    paramsMap.put("tradeType",AccountStatus.TRADE_TYPE_03);

                    if(list.size() < Integer.parseInt(examine)){
                        if(fca.compareTo(bd) < 0){
                            return ResponseUtil.fail(403, "账户可用余额小于提现金额");
                        }

                        //获取毫秒数
                        Long milliSecond = LocalDateTime.now().toInstant(ZoneOffset.of("+8")).toEpochMilli();
                        //加锁
                        MutexLock mutexLock = MutexLockUtils.getMutexLock(userId);
                        String[] strs1 = amount1.split(",");
                        synchronized (mutexLock) {
                            if( bd.compareTo(new BigDecimal("0.00")) == 1 && Arrays.asList(strs1).contains(money)){
                                Map map1 = new HashMap();
                                /**微信有授权*/
                                if(!StringUtils.isEmpty(mallUser.getWeixinOpenid()) ){
                                    map1 = wxEnterpriseTransferService.transfer2SmallChange(null,mallUser.getWeixinOpenid(),  milliSecond+"", bd.toString(), resson);
                                }/**微信没有授权，小程序有*/
                                else if(StringUtils.isEmpty(mallUser.getWeixinOpenid()) && !StringUtils.isEmpty(mallUser.getWeixinMiniOpenid())){
                                    map1 = wxEnterpriseTransferService.transfer2SmallChange(hs_appId,mallUser.getWeixinMiniOpenid(),  milliSecond+"", bd.toString(), resson);
                                }

                                if(null != map1 && (int)map1.get("code") == 0){
                                    log.info("----------fcAccountWithdrawBill------------"+fcAccountWithdrawBill.toString());

                                    try {
                                        log.info("map1="+map1);
                                        Map<String,Object> maps = (Map) map1.get("errmsg");
                                        String paymentNo =(String) maps.get("payment_no");
                                        // 提现状态:WAIT_AUDIT等待审核 WAIT_PAY:审核成功，等待提现 FINISH:提现完成 AUDIT_
                                        fcAccountWithdrawBill.setWithdrawState("FINISH");
                                        /**打款单号*/
                                        log.info("打款单号paymentNo=" + paymentNo);
                                        fcAccountWithdrawBill.setPaymentNo(paymentNo);
                                        AccountUtils.accountChangeByExchange(userId, bd, CommonConstant.TASK_REWARD_CASH, null, paramsMap);
                                        fcAccountWithdrawBillService.add(fcAccountWithdrawBill);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        log.info("任务提现失败：" + e.getMessage(), e);
                                        return ResponseUtil.fail(403,"提现失败！");
                                    }
                                    //消息推送
                                        DateTimeFormatter dtf2 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                                        String time = dtf2.format(LocalDateTime.now());
                                        String content ="您的提现申请（申请时间/金额："+time+"/¥"+bd+"）已成功打款，请登录对应提现账户进行查收。" ;
                                    try {
                                        PushUtils.pushMessage(withdrawBill.getCustomerId(),"提现到账",content,3,2);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        log.info("消息推送失败："+e.getMessage());
//                                        return ResponseUtil.fail(403,"提现失败！");
                                    }
                                    //释放锁
                                        MutexLockUtils.remove(userId);
                                        Map map2 = new HashMap();
                                        map2.put("is_first",1);
                                        return ResponseUtil.ok(map2);
                                }else{
                                        /**打款失败直接插入一条待审核记录，等待人工打款，不用返回错误*/
                                        try {
                                            // 提现状态:WAIT_AUDIT等待审核 WAIT_PAY:审核成功，等待提现 FINISH:提现完成 AUDIT_
                                            fcAccountWithdrawBill.setWithdrawState("WAIT_AUDIT");
                                            AccountUtils.accountChangeByExchange(userId, bd, CommonConstant.TASK_REWARD_CASH, null, paramsMap);
                                            fcAccountWithdrawBillService.add(fcAccountWithdrawBill);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                            log.info("任务提现失败："+e.getMessage());
                                            return ResponseUtil.fail(403,"提现失败！");
                                        }

                                        //释放锁
                                        MutexLockUtils.remove(userId);
                                        Map map2 = new HashMap();
                                        map2.put("is_first",0);
                                        return ResponseUtil.ok(map2);
                                }
                            }else{

                                try {
                                    // 提现状态:WAIT_AUDIT等待审核 WAIT_PAY:审核成功，等待提现 FINISH:提现完成 AUDIT_
                                    fcAccountWithdrawBill.setWithdrawState("WAIT_AUDIT");
                                    AccountUtils.accountChangeByExchange(userId, bd, CommonConstant.TASK_REWARD_CASH, null, paramsMap);
                                    fcAccountWithdrawBillService.add(fcAccountWithdrawBill);
                                    //释放锁
                                    MutexLockUtils.remove(userId);
                                    Map map2 = new HashMap();
                                    map2.put("is_first",0);
                                    return ResponseUtil.ok(map2);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    log.info("任务提现失败："+e.getMessage());
                                    return ResponseUtil.fail(403,"提现失败！");
                                }

                            }
                        }
                    }else {
                        if (list.size() < Integer.parseInt(number)) {
                            if (fca.compareTo(bd) < 0) {
                                return ResponseUtil.fail(403, "账户可用余额小于提现金额");
                            }

                            fcAccountWithdrawBill.setUserName(EmojiParser.parseToAliases(mallUser.getNickname()));
                            fcAccountWithdrawBill.setBillId(GraphaiIdGenerator.nextId("FcAccountWithdrawBill"));
                            fcAccountWithdrawBill.setAccountId(fcAccount.getAccountId());
                            fcAccountWithdrawBill.setCustomerId(userId);
                            fcAccountWithdrawBill.setAmt(bd);
                            fcAccountWithdrawBill.setCreateDate(LocalDateTime.now());
                            fcAccountWithdrawBill.setUpdateDate(LocalDateTime.now());
                            fcAccountWithdrawBill.setWithdrawDate(LocalDateTime.now());
                            if(!StringUtils.isEmpty(typeW) && "05".equals(typeW)){
                                fcAccountWithdrawBill.setWithdrawType("05");//01 --收益（零钱）提现    02 --任务奖励提现 04 小程序任务提现 05 小程序零钱提现
                            }else{
                                fcAccountWithdrawBill.setWithdrawType("02");//01 --收益（零钱）提现    02 --任务奖励提现 04 小程序任务提现 05 小程序零钱提现
                            }

                            fcAccountWithdrawBill.setCheckStatus("00C");//00C 未对账
                            fcAccountWithdrawBill.setStatus((short) 1);//1 存在   0 删除
                            fcAccountWithdrawBill.setWithdrawState("WAIT_AUDIT"); // 提现状态:WAIT_AUDIT等待审核 WAIT_PAY:审核成功，等待提现 FINISH:提现完成 AUDIT_
                            fcAccountWithdrawBill.setAuditFlag((short) 1);


                            String[] strs = amount.split(",");
                            MutexLock mutexLock = MutexLockUtils.getMutexLock(userId);
                            synchronized (mutexLock) {
                                if (bd.compareTo(new BigDecimal(0.00)) == 1 && Arrays.asList(strs).contains(money)) {
                                    try {
                                        AccountUtils.accountChangeByExchange(userId, bd, CommonConstant.TASK_REWARD_CASH, null, paramsMap);
                                        fcAccountWithdrawBillService.add(fcAccountWithdrawBill);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        log.info("任务提现失败："+e.getMessage());
                                        return ResponseUtil.fail(403,"提现失败！");
                                    }
                                    MutexLockUtils.remove(userId);
                                    Map map2 = new HashMap();
                                    map2.put("is_first",0);
                                    return ResponseUtil.ok(map2);
                                } else {
                                    MutexLockUtils.remove(userId);
                                    return ResponseUtil.fail(403, "提现金额不符合提现规则！");
                                }
                            }
                        } else {
                            return ResponseUtil.fail(403, "您今日提现次数已用完，请明日再来！");
                        }
                    }
            }

        }
        /** todo（购物，邀请好友）的现金 提现不需要卡点*/
        else if ("1".equals(status)) {
            //收益提现次数
            String number = (String)map.get("Mall_withdrawal_profit_rule_number");
            String minAmount = (String)map.get("Mall_withdrawal_profit_rule_minAmount");
            String maxAmount = (String)map.get("Mall_withdrawal_profit_rule_maxAmount");
            if(!StringUtils.isEmpty(number)){
            } else {
                //如果为空 默认给一个次数防止出错
                number="1";
            }
            if(!StringUtils.isEmpty(number)){
                if(list.size() < Integer.parseInt(number)){
                     if(fca1.compareTo(bd) < 0){
                        return ResponseUtil.fail(403, "账户可用余额小于提现金额");
                    }
                    if(bd.compareTo(new BigDecimal(minAmount))>=0 && bd.compareTo(new BigDecimal(maxAmount))<=0){

                        fcAccountWithdrawBill.setBillId(GraphaiIdGenerator.nextId("FcAccountWithdrawBill"));
                        fcAccountWithdrawBill.setUserName(EmojiParser.parseToAliases(mallUser.getNickname()));
                        fcAccountWithdrawBill.setAccountId(fcAccount.getAccountId());
                        fcAccountWithdrawBill.setCustomerId(userId);
                        fcAccountWithdrawBill.setAmt(bd);
                        fcAccountWithdrawBill.setCreateDate(LocalDateTime.now());
                        fcAccountWithdrawBill.setUpdateDate(LocalDateTime.now());
                        fcAccountWithdrawBill.setWithdrawDate(LocalDateTime.now());
                        if(!StringUtils.isEmpty(typeW) && "04".equals(typeW)){
                            fcAccountWithdrawBill.setWithdrawType("04");//01 --收益（零钱）提现    02 --任务奖励提现 04 小程序任务提现 05 小程序零钱提现
                        }else{
                            fcAccountWithdrawBill.setWithdrawType("01");//01 --收益（零钱）提现    02 --任务奖励提现 04 小程序任务提现 05 小程序零钱提现
                        }

                        fcAccountWithdrawBill.setCheckStatus("00C");//00C 未对账
                        fcAccountWithdrawBill.setStatus((short)1);//1 存在   0 删除
                        fcAccountWithdrawBill.setWithdrawState("WAIT_AUDIT"); // 提现状态:WAIT_AUDIT等待审核 WAIT_PAY:审核成功，等待提现 FINISH:提现完成 AUDIT_
                        fcAccountWithdrawBill.setAuditFlag((short) 1);


                        Map paramsMap = new HashMap();
                        paramsMap.put("dType", AccountStatus.DISTRIBUTION_TYPE_00);  //直接分润
                        paramsMap.put("capitalTrend",AccountStatus.CAPITAL_TREND_02);//出账
                        paramsMap.put("changeType",AccountStatus.CHANGE_TYPE_03);//提现
                        paramsMap.put("message","收益零钱提现申请");
                        paramsMap.put("changeAccountType",AccountStatus.CHANGE_ACCOUNT_TYPE_1);
                        paramsMap.put("tradeType",AccountStatus.TRADE_TYPE_07);
                        MutexLock mutexLock = MutexLockUtils.getMutexLock(userId);
                        synchronized (mutexLock) {
                            if (bd.compareTo(new BigDecimal(0.00)) == 1) {
                                try {
                                    AccountUtils.accountChange(userId, bd, CommonConstant.TASK_REWARD_CASH, null, paramsMap);
                                    fcAccountWithdrawBillService.add(fcAccountWithdrawBill);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    log.info("收益零钱提现失败："+e.getMessage());
                                    return ResponseUtil.fail(403,"提现失败！");
                                }
                                MutexLockUtils.remove(userId);
                            }
                        }
                    }else{
                        return ResponseUtil.fail(403, "提现金额未在可提现范围内，请重新输入");
                    }
                }else{
                    return ResponseUtil.fail(403, "您今日提现次数已用完，请明日再来！");
                }
            }


        }



        return ResponseUtil.ok();
    }

    /**
     * 提现申请记录列表
     */
    @GetMapping("/withdrawalList")
    public Object getwithdrawalRecordList(@LoginUser String userId,@RequestParam(required = false) String type) {

        if (null == userId) {
            return ResponseUtil.unlogin();
        }
        FcAccountWithdrawBill fcAccountWithdrawBill = new FcAccountWithdrawBill();
        fcAccountWithdrawBill.setCustomerId(userId);
        fcAccountWithdrawBill.setWithdrawType(type);
        List<FcAccountWithdrawBill> list = fcAccountWithdrawBillService.selectFcAccountWithdrawBillList2(fcAccountWithdrawBill,null,null,"");

        List<FcAccountWithdrawBill> list1 = new ArrayList<>();
        for (FcAccountWithdrawBill lw : list) {
            // 提现状态:WAIT_AUDIT等待审核 WAIT_PAY:审核成功，等待提现 FINISH:提现完成 REJECT 已驳回
            if (("WAIT_AUDIT").equals(lw.getWithdrawState())) {
                lw.setWithdrawState("等待审核");
            } else if (("WAIT_PAY").equals(lw.getWithdrawState())) {
                lw.setWithdrawState("审核成功，等待打款");
            } else if (("FINISH").equals(lw.getWithdrawState())) {
                lw.setWithdrawState("提现完成");
            }else if (("REJECT").equals(lw.getWithdrawState())) {
                lw.setWithdrawState("已驳回");
            }
            //转换类型，前端展示用
            if("1".equals(lw.getWithdrawAccountType())){
                lw.setWithdrawAccountType("03");//银行卡
            }else if("2".equals(lw.getWithdrawAccountType())){
                lw.setWithdrawAccountType("02");//支付宝
            }else if("3".equals(lw.getWithdrawAccountType())){
                lw.setWithdrawAccountType("01");//微信
            }
            list1.add(lw);
        }

        return ResponseUtil.ok(list1);
    }
    /**
     *查询用户账户收益金额
     */
    @GetMapping("/account")
    public  Object getFcAccount(@LoginUser String userId){
        if( null == userId ){
            return ResponseUtil.unlogin();
        }
        //查询当天有没有提现机会，有则使用
        List<WangzhuanTaskWithdrawaChancelSum> wlist = wangZhuanTaskChanceService.selectWithdrawaChancelSum(userId);

        //查询提现机会位置


        FcAccount fcAccount =  accountService.getUserAccount(userId);
        Map map = new HashMap();
        if(null != fcAccount){
           BigDecimal amount = fcAccount.getExchangeAmount();
            map.put("amount",amount);
        }
        if(wlist.size()>0){
            if(null != wangZhuanTaskChanceService.selectWithdrawaC(userId,wlist.get(0).getChancePlace())){
                map.put("chancel",wlist.get(0).getChanceNum());
                map.put("chStstus",wlist.get(0).getStatus());
            }else{
                map.put("chancel",0);
                map.put("chStstus","1");
            }
        }else{
            map.put("chancel",0);
            map.put("chStstus","1");
        }
        
        return ResponseUtil.ok(map);
    }
    /**
     *查询提现规则
     */
    @RequestMapping(value = "/rule", produces = "application/json;charset=UTF-8;")
    public  Object getWithdrawlRule() {

        Map map = mallSystemConfigService.listRule();

        return ResponseUtil.ok(map);
    }
}
