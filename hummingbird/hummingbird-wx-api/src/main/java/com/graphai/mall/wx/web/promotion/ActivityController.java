package com.graphai.mall.wx.web.promotion;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

import javax.validation.constraints.NotNull;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Header;
import org.apache.http.message.BasicHeader;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.beust.jcommander.internal.Lists;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.graphai.commons.constant.MallConstant;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.json.JacksonUtils;
import com.graphai.mall.db.domain.ActivityGoodsVo;
import com.graphai.mall.db.domain.MallAd;
import com.graphai.mall.db.domain.MallRebateRecord;
import com.graphai.mall.db.service.advertisement.MallAdService;
import com.graphai.mall.db.service.advertisement.MallBrandService;
import com.graphai.mall.db.service.distribution.FenXiaoService;
import com.graphai.mall.db.service.promotion.ActivityService;
import com.graphai.mall.db.vo.ActivityBrandGoodsVo;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.mall.wx.web.common.MyBaseController;
import com.graphai.open.api.getway.HttpRequestUtils;
import com.graphai.properties.PlatFormServerproperty;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
// @RequestMapping(value = "/api_gateway", consumes = {
@RequestMapping(value = "/wx/activity", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.APPLICATION_ATOM_XML_VALUE,
        MediaType.APPLICATION_FORM_URLENCODED_VALUE, MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE, MediaType.TEXT_HTML_VALUE,
        MediaType.TEXT_XML_VALUE, MediaType.APPLICATION_OCTET_STREAM_VALUE})
public class ActivityController extends MyBaseController {

    private final Log logger = LogFactory.getLog(ActivityController.class);

    @Autowired
    private MallAdService adService;

    @Autowired
    private MallBrandService brandService;

    @Autowired
    private FenXiaoService fenXiaoService;

    @Autowired
    private ActivityService activityService;
    
    @Autowired
    private PlatFormServerproperty platFormServerproperty;


    @GetMapping("/clickActivity")
    public Object clickActivity(String activityId, @LoginUser String userId) throws Exception {

        String url = platFormServerproperty.getUrl(MallConstant.TAOBAO);
        Map<String, Object> mappMap = new HashMap<String, Object>();
        mappMap.put("plateform", "dtk");
        mappMap.put("userId", userId);
        mappMap.put("activityId", activityId);


        Header[] headers = new BasicHeader[]{new BasicHeader("method", "insertUserClickActivity"), new BasicHeader("version", "v1.0")};

        String result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);

        JSONObject object = JSONObject.parseObject(result);

        if (object.getString("errno").equals("0")) {
            return ResponseUtil.ok();
        }

        return ResponseUtil.fail();
    }


    @GetMapping("/detail")
    public Object detail(String id, String shId) {
        String finalId = id;
        if (id == null || "0".equals(id)) {
            MallRebateRecord rebate = fenXiaoService.getMallRebateRecordById(shId);
            finalId = rebate.getItemId();
        }

        MallAd activity = adService.findById(finalId);
        if (id == null) {
            return ResponseUtil.badArgumentValue();
        }
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("activity", activity);
        return ResponseUtil.ok(data);
    }

    /**
     * 品牌列表
     *
     * @param page  分页页数
     * @param limit 分页大小
     * @return 品牌列表
     * @throws IOException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    @GetMapping("/list")
    public Object list(@RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit, @NotNull String cid,
                       String isPage) throws Exception {

        if (page <= 0) {
            page = 1;
        }

        String url = platFormServerproperty.getUrl(MallConstant.TAOBAO);
        Map<String, Object> mappMap = new HashMap<String, Object>();
        mappMap.put("plateform", "dtk");
        mappMap.put("page", page);
        mappMap.put("size", limit);
        mappMap.put("cid", cid);
        mappMap.put("isPage", isPage);

        Header[] headers = new BasicHeader[]{new BasicHeader("method", "getActicityGoods"), new BasicHeader("version", "v1.0")};
        String result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);

        JSONObject object = JSONObject.parseObject(result);

        if (!object.getString("errno").equals("0")) {
            return ResponseUtil.fail();
        }

        JSONObject arrayData = (JSONObject) object.get("data");
        if (com.graphai.commons.util.obj.ObjectUtils.isNotNull(arrayData)) {
            return ResponseUtil.ok(arrayData);
        }
        return ResponseUtil.ok();
    }


    /**
     *
     */
    @GetMapping("/getGoodsListByBrandId")
    public Object getGoodsListByBrandId(@RequestParam(defaultValue = "1") Integer page,
                                        @RequestParam(defaultValue = "10") Integer limit,
                                        @NotNull String brandId,
                                        String isPage) throws Exception {

        if (page <= 0) {
            page = 1;
        }
        String url = platFormServerproperty.getUrl(MallConstant.TAOBAO);
        Map<String, Object> mappMap = new HashMap<String, Object>();
        mappMap.put("plateform", "dtk");
        mappMap.put("page", page);
        mappMap.put("size", limit);
        mappMap.put("brandId", brandId);
        mappMap.put("isPage", isPage);

        Header[] headers = new BasicHeader[]{new BasicHeader("method", "getGoodsByBrandId"), new BasicHeader("version", "v1.0")};
        String result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);

        JSONObject object = JSONObject.parseObject(result);

        if (!object.getString("errno").equals("0")) {
            return ResponseUtil.fail();
        }

        JSONObject arrayData = (JSONObject) object.get("data");
        return ResponseUtil.ok(arrayData);
    }

    @GetMapping("/listbak")
    public Object listbak(@RequestParam(defaultValue = "1") Integer isGetGoods, @RequestParam(defaultValue = "1") Integer page,
                          @RequestParam(defaultValue = "10") Integer limit, String shId, String industryId) {
        List<Map<String, Object>> brandList = null;
        List<String> onlyQueryIds = null;
        if (shId != null) {
            MallRebateRecord rebate = fenXiaoService.getMallRebateRecordById(shId);
            //品牌活动批量分享
            if ("activityBatch".equals(rebate.getItemType())) {
                String[] ids = rebate.getItemIds();

                try {
                    onlyQueryIds = new ArrayList<String>();
                    for (int i = 0; i < ids.length; i++) {
                        onlyQueryIds.add(ids[i]);
                    }
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }

                if (CollectionUtils.isNotEmpty(onlyQueryIds)) {
                    brandList = brandService.getActivityListByCondition(onlyQueryIds, page, limit);
                }
            }

        } else {
            // List<MallBrand> brandList = brandService.queryVO(page, limit);
            brandList = brandService.getActivityList(page, limit, industryId, null);
        }

        Map<String, FutureTask<List<ActivityGoodsVo>>> myTaskMap = new HashMap<String, FutureTask<List<ActivityGoodsVo>>>();
        if (isGetGoods.equals(1)) {

            if (CollectionUtils.isNotEmpty(brandList)) {
                for (int i = 0; i < brandList.size(); i++) {
                    Map<String, Object> activity = brandList.get(i);
                    String activityId = MapUtils.getString(activity, "activityId");
                    Callable<List<ActivityGoodsVo>> goodsListCallable = () -> activityService.getGoodsByActivity(activityId, 1, 5);
                    FutureTask<List<ActivityGoodsVo>> goodsListTask = new FutureTask<>(goodsListCallable);
                    //taskList.add(goodsListTask);
                    myTaskMap.put(activityId, goodsListTask);
                    executorService.submit(goodsListTask);
                    //activity.put("goods", goodsList);
                }
            }

            try {
                if (CollectionUtils.isNotEmpty(brandList)) {
                    for (int i = 0; i < brandList.size(); i++) {
                        Map<String, Object> activity = brandList.get(i);
                        String activityId = MapUtils.getString(activity, "activityId");
                        FutureTask<List<ActivityGoodsVo>> goodsListTask = myTaskMap.get(activityId);
                        //taskList.add(goodsListTask);
                        //executorService.submit(goodsListTask);
                        activity.put("goods", goodsListTask.get());
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }

        // int total = brandService.queryTotalCount();
        int total = brandService.getActivityCount(null, null);
        int totalPages = (int) Math.ceil((double) total / limit);

        Map<String, Object> data = new HashMap<String, Object>();
        data.put("brandList", brandList);
        data.put("totalPages", totalPages);
        return ResponseUtil.ok(data);
    }

}
