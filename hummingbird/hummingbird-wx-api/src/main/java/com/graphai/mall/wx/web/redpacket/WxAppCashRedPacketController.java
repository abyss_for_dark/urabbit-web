package com.graphai.mall.wx.web.redpacket;

import static com.graphai.commons.util.servlet.ServletUtils.getRequest;

import java.math.BigDecimal;
import java.security.KeyFactory;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;

import javax.crypto.Cipher;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.ip.IpUtils;
import com.graphai.mall.admin.domain.MallPageView;
import com.graphai.mall.admin.domain.MallRedQrcode;
import com.graphai.mall.admin.domain.MallRedQrcodeRecord;
import com.graphai.mall.admin.service.IMallPageViewService;
import com.graphai.mall.admin.service.IMallRedQrcodeRecordService;
import com.graphai.mall.admin.service.IMallRedQrcodeService;
import com.graphai.mall.db.domain.MallAd;
import com.graphai.mall.db.domain.MallUser;
import com.graphai.mall.db.constant.account.AccountStatus;
import com.graphai.mall.db.service.advertisement.MallAdService;
import com.graphai.mall.db.service.sysconfig.MallSystemConfigService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.constant.CommonConstant;
import com.graphai.mall.db.util.RandomUtils;
import com.graphai.mall.db.util.account.AccountUtils;
import com.graphai.mall.db.util.phone.PushUtils;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.nosql.redis.NewCacheInstanceServiceLoader;

import cn.hutool.core.util.RandomUtil;

/**
 * app 现金红包
 * 
 * @author biteam 整理
 *
 */
@RestController
@RequestMapping("/wx/gamejackpot")
@Validated
public class WxAppCashRedPacketController {
    @Autowired
    private IMallRedQrcodeRecordService iMallRedQrcodeRecordService;
    @Autowired
    private MallSystemConfigService mallSystemConfigService;
    @Autowired
    private MallAdService mallAdService;
    @Autowired
    private IMallRedQrcodeService serviceImpl;
    @Autowired
    private MallUserService mallUserService;
    @Autowired
    private IMallPageViewService mallPageViewService;

    @Value("${mall.wx.mpappid}")
    private String appid;
    @Value("${weiRed.app}")
    private String app;
    @Value("${weiRed.privateKey}")
    private String privateKey;
    @Value("${weiRed.publicKey}")
    private String publicKey;
    @Value("${weiRed.qrcodeUrl}")
    private String requestUrl;

    public static final String TASK_REWARD_CASH = CommonConstant.TASK_REWARD_CASH;
    private final Log logger = LogFactory.getLog(WxAppCashRedPacketController.class);


    /**
     * APP领取红包，获取加密串
     */
    @GetMapping("/queryRandom")
    public Object queryRandom(@LoginUser String userId) {
        if (null == userId) {
            return ResponseUtil.unlogin();
        }
        // 获取毫秒数
        Long milliSecond = LocalDateTime.now().toInstant(ZoneOffset.of("+8")).toEpochMilli();
        // 生成随机串
        String randomCode = RandomUtils.genRandomNum(6);
        NewCacheInstanceServiceLoader.objectCache().PUT("randomCode" + milliSecond, randomCode);
        String conent = "time=" + milliSecond + "&randomCode=" + randomCode + ",randomCode" + milliSecond;
        logger.info("queryRandom-----明文------" + conent);
        String outStr = "";
        try {
            // base64编码的公钥
            byte[] decoded = org.apache.commons.codec.binary.Base64.decodeBase64(publicKey);
            RSAPublicKey pubKey = (RSAPublicKey) KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(decoded));
            // RSA加密
            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.ENCRYPT_MODE, pubKey);
            outStr = org.apache.commons.codec.binary.Base64.encodeBase64String(cipher.doFinal(conent.getBytes("UTF-8")));
            logger.info("queryRandom-----密文------" + outStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String redCode = "63725C";// APP红包code
        // QueryWrapper queryWrapper = new QueryWrapper();
        // queryWrapper.eq("mall_red_qrcode.code",redCode);
        MallRedQrcode one = serviceImpl.getOne(new QueryWrapper<MallRedQrcode>().eq("mall_red_qrcode.code", redCode));
        logger.info("queryRandom-----MallRedQrcode------" + one.getId());
        // 领取次数
        String statr1 = LocalDateTime.of(LocalDate.now(), LocalTime.of(00, 00, 00)).toString();
        String end1 = LocalDateTime.of(LocalDate.now(), LocalTime.of(23, 59, 59)).toString();
        int cnt = iMallRedQrcodeRecordService.count(new QueryWrapper<MallRedQrcodeRecord>().eq("redpacket_id", one.getId()).eq("to_user_id", userId).eq("amount_type", "02").between("create_time", statr1, end1));
        logger.info("queryRandom-----用户领取次数------" + cnt);
        Map<String, Object> map = new HashMap<>();
        MallUser mallUser = mallUserService.findById(userId);
        if (null == mallUser) {
            return ResponseUtil.fail(402, "用户不存在！");
        } else {
            logger.info("queryRandom-----用户------" + mallUser.getId());
            LocalDate now = mallUser.getAddTime().toLocalDate();
            if (now.isEqual(LocalDate.now())) {
                map.put("isNew", "1");
            } else {
                map.put("isNew", "0");
            }
        }
        map.put("number", cnt);
        map.put("random", outStr);
        return ResponseUtil.ok(map);

    }

    /**
     * APP领取现金红包
     */
    @GetMapping("/receiveRedApp")
    public Object receiveRedApp(@LoginUser String userId, @RequestParam String random) {
        try {
            String userAgent = getRequest().getHeader("user-agent");
            String device = "";// 设备类型 iPhone(苹果) android（安卓）
            if (null != userAgent && userAgent.contains("Android")) {
                device = "Android";
            } else if (null != userAgent && userAgent.contains("iPhone")) {
                device = "iPhone";
            }
            String IP = IpUtils.getIpAddress(getRequest());
            MallPageView mallPageView = new MallPageView();
            // 64位解码加密后的字符串
            byte[] inputByte = org.apache.commons.codec.binary.Base64.decodeBase64(random.getBytes("UTF-8"));
            // base64编码的私钥
            byte[] decoded1 = Base64.decodeBase64(privateKey);
            RSAPrivateKey priKey = (RSAPrivateKey) KeyFactory.getInstance("RSA").generatePrivate(new PKCS8EncodedKeySpec(decoded1));
            // RSA解密
            Cipher cipher1 = Cipher.getInstance("RSA");
            cipher1.init(Cipher.DECRYPT_MODE, priKey);
            // 解密后红包code
            String code = new String(cipher1.doFinal(inputByte));
            Map<String, String> mapRequest = new HashMap<String, String>();
            String[] arrSplit = code.split("[&]");
            for (String strSplit : arrSplit) {
                String[] arrSplitEqual = null;
                arrSplitEqual = strSplit.split("[=]");
                // 解析出键值
                if (arrSplitEqual.length > 1) {
                    // 正确解析
                    mapRequest.put(arrSplitEqual[0], arrSplitEqual[1]);
                } else {
                    if (arrSplitEqual[0] != "") {
                        // 只有参数没有值，不加入
                        mapRequest.put(arrSplitEqual[0], "");
                    }
                }
            }
            String randomCode1 = mapRequest.get("randomCode");
            String[] strings = randomCode1.split(",");
            String randomCode = (String) NewCacheInstanceServiceLoader.objectCache().get(strings[1]);// redis中随机串key
            String signa = strings[0];// 随机串
            String redCode = "63725C";// APP红包code

            List<MallAd> listTad = mallAdService.queryByPosition("107");// 第三方广告
            int nu = RandomUtil.randomInt(0, listTad.size());
            if (randomCode.equals(signa)) {
                QueryWrapper queryWrapper = new QueryWrapper();
                queryWrapper.eq("mall_red_qrcode.code", redCode);
                MallRedQrcode one = serviceImpl.getOne(queryWrapper);
                // 领取次数
                String statr1 = LocalDateTime.of(LocalDate.now(), LocalTime.of(00, 00, 00)).toString();
                String end1 = LocalDateTime.of(LocalDate.now(), LocalTime.of(23, 59, 59)).toString();
                int cnt = iMallRedQrcodeRecordService.count(new QueryWrapper<MallRedQrcodeRecord>().eq("redpacket_id", one.getId()).eq("to_user_id", userId).eq("amount_type", "02").between("create_time", statr1, end1));

                Map<String, String> stringStringMap = mallSystemConfigService.queryMaker();// 查询创客信息
                if (null != one && cnt < one.getUserNumber()) {
                    // 用户领取记录
                    MallRedQrcodeRecord mallRedQrcodeRecord = new MallRedQrcodeRecord();
                    mallRedQrcodeRecord.setCreateTime(LocalDateTime.now());
                    mallRedQrcodeRecord.setUpdateTime(LocalDateTime.now());
                    mallRedQrcodeRecord.setDeleted(false);
                    mallRedQrcodeRecord.setRedpacketId(one.getId());
                    mallRedQrcodeRecord.setToUserId(userId);
                    mallRedQrcodeRecord.setUsersId(one.getUsersId());
                    mallRedQrcodeRecord.setRemark("APP现金红包领取记录");
                    mallRedQrcodeRecord.setDevice(device);
                    mallRedQrcodeRecord.setClientIp(IP);
                    mallRedQrcodeRecord.setAmountType("02");// 业务类型：01-红包码 02-app红包 03-整点红包
                    mallRedQrcodeRecord.setType("01");// 分类 01：现金红包 02：整点红包
                    mallRedQrcodeRecord.setTypeOfClaim("01");// 领取类型 01：自领 02：推荐
                    if (!Objects.equals(null, listTad.get(nu))) {
                        mallPageView.setPvName(listTad.get(nu).getName());
                        mallRedQrcodeRecord.setAdName(listTad.get(nu).getName());
                    }

                    // 推荐人记录
                    MallRedQrcodeRecord recordMaker = new MallRedQrcodeRecord();
                    recordMaker.setCreateTime(LocalDateTime.now());
                    recordMaker.setUpdateTime(LocalDateTime.now());
                    recordMaker.setDeleted(false);
                    recordMaker.setRedpacketId(one.getId());
                    recordMaker.setToUserId(userId);
                    recordMaker.setUsersId(one.getUsersId());
                    recordMaker.setRemark("APP现金红包分润记录");
                    recordMaker.setAmountType("02");// 业务类型：01-红包码 02-app红包 03-整点红包
                    recordMaker.setType("01");// 分类 01：现金红包 02：整点红包
                    recordMaker.setTypeOfClaim("02");// 领取类型 01：自领 02：推荐
                    recordMaker.setClientIp(IP);
                    recordMaker.setDevice(device);
                    if (!Objects.equals(null, listTad.get(nu))) {
                        recordMaker.setAdName(listTad.get(nu).getName());
                    }

                    // 查询用户信息
                    MallUser mallUserList = mallUserService.findById(userId);
                    // 判断是否新用户
                    LocalDate now = mallUserList.getAddTime().toLocalDate();

                    Map<String, Object> paramsMap = new HashMap();
                    paramsMap.put("dType", AccountStatus.DISTRIBUTION_TYPE_00); // 直接分润
                    paramsMap.put("message", "现金红包奖励");
                    paramsMap.put("capitalTrend", AccountStatus.CAPITAL_TREND_01);
                    paramsMap.put("changeType", AccountStatus.CHANGE_TYPE_01);
                    paramsMap.put("changeAccountType", AccountStatus.CHANGE_ACCOUNT_TYPE_2);
                    paramsMap.put("tradeType", AccountStatus.TRADE_TYPE_16);

                    // 直推人分润
                    Map paramsMapMaker = new HashMap();
                    paramsMapMaker.put("dType", AccountStatus.DISTRIBUTION_TYPE_10); // 直接分润
                    paramsMapMaker.put("message", "现金红包分享收益");
                    paramsMapMaker.put("capitalTrend", AccountStatus.CAPITAL_TREND_01);
                    paramsMapMaker.put("changeType", AccountStatus.CHANGE_TYPE_01);
                    paramsMapMaker.put("changeAccountType", AccountStatus.CHANGE_ACCOUNT_TYPE_2);
                    paramsMapMaker.put("tradeType", AccountStatus.TRADE_TYPE_23);

                    String oneNum = one.getOneNum();// 红包额度区间第一次
                    String fiveNum = one.getFiveNum();// 红包额度区间第五次
                    String[] str = oneNum.split(",");// 第一次
                    String[] str2 = one.getTwoNum().split(",");// 第二次
                    String[] strt = one.getThreeNum().split(",");// 第三次
                    String[] strf = one.getFourNum().split(",");// 第四次
                    String[] str1 = fiveNum.split(",");// KA第五次( 新用户)
                    String[] strSix = one.getSixNum().split(",");// KM第五次( 旧用户)


                    // 1到5次区间
                    double ed1 = Double.parseDouble(str[0]);// 区间1
                    double ed2 = Double.parseDouble(str[1]);// 区间1
                    double edt1 = Double.parseDouble(str2[0]);// 区间2
                    double edt2 = Double.parseDouble(str2[1]);// 区间2
                    double edth1 = Double.parseDouble(strt[0]);// 区间3
                    double edth2 = Double.parseDouble(strt[1]);// 区间3
                    double edf1 = Double.parseDouble(strf[0]);// 区间4
                    double edf2 = Double.parseDouble(strf[1]);// 区间4
                    double edf4 = Double.parseDouble(str1[0]);// 区间5(ka)
                    double edf5 = Double.parseDouble(str1[1]);// 区间5(ka)
                    double edKm1 = Double.parseDouble(strSix[0]);// 区间5(km)
                    double edKm2 = Double.parseDouble(strSix[1]);// 区间5(km)
                    Random random1 = new Random();
                    // Math.round(Math.random()*(y-x))+x;

                    DecimalFormat df = new DecimalFormat("0.00");
                    BigDecimal red = new BigDecimal(df.format(random1.nextDouble() * (ed2 - ed1) + ed1));// 第一次
                    BigDecimal red1 = new BigDecimal(df.format(random1.nextDouble() * (edt2 - edt1) + edt1));// 第二次
                    BigDecimal red2 = new BigDecimal(df.format(random1.nextDouble() * (edth2 - edth1) + edth1));// 第三次
                    BigDecimal red3 = new BigDecimal(df.format(random1.nextDouble() * (edf2 - edf1) + edf1));// 第四次
                    BigDecimal red4 = new BigDecimal(df.format(random1.nextDouble() * (edf5 - edf4) + edf4));// 第五次(KA)
                    BigDecimal redKm = new BigDecimal(df.format(random1.nextDouble() * (edKm2 - edKm1) + edKm1));// 第五次(KM)
                    BigDecimal amount = new BigDecimal("0.00");

                    if (cnt == 0) {
                        mallRedQrcodeRecord.setAmount(red);// 小额红包第一次
                        amount = red;
                    } else if (cnt == 1) {
                        mallRedQrcodeRecord.setAmount(red1);// 小额红包第二次
                        amount = red1;
                    } else if (cnt == 2) {
                        mallRedQrcodeRecord.setAmount(red2);// 小额红包第三次
                        amount = red2;
                    } else if (cnt == 3) {
                        mallRedQrcodeRecord.setAmount(red3);// 小额红包第四次
                        amount = red3;
                    } else if (now.isEqual(LocalDate.now()) && cnt == 4) {
                        // 新用户第五次(KA)
                        mallRedQrcodeRecord.setAmount(red4);// 第五次大额红包
                        amount = red4;

                    } else if (cnt == 4) {
                        // 老用户第五次(KM)
                        mallRedQrcodeRecord.setAmount(redKm);// 第五次大额红包
                        amount = redKm;

                    }

                    if (iMallRedQrcodeRecordService.save(mallRedQrcodeRecord)) {
                        // todo看广告生成记录

                        if (StringUtils.isNoneEmpty(device)) {
                            mallPageView.setMobileType(device);
                        }
                        mallPageView.setClientIp(IP);
                        mallPageView.setOutUserId(userId);
                        mallPageView.setType("01");
                        if (StringUtils.isNotEmpty(getRequest().getHeader("version"))) {
                            mallPageView.setPlatform("app");
                        }


                        // 自己领
                        if (null != mallUserList) {
                            // 是达人
                            if (null != mallUserList.getIsTalent() && mallUserList.getIsTalent().equals("1")) {
                                amount = amount.add(new BigDecimal(stringStringMap.get("mall_maker_profit_proportion")).multiply(amount));
                                // 充值任务钱包
                                AccountUtils.accountChangeByExchange(mallUserList.getId(), amount, CommonConstant.TASK_REWARD_CASH, AccountStatus.BID_37, paramsMap);

                            }
                            // 不是达人
                            else {
                                // 充值任务钱包
                                AccountUtils.accountChangeByExchange(mallUserList.getId(), amount, CommonConstant.TASK_REWARD_CASH, AccountStatus.BID_37, paramsMap);
                            }

                        }

                        // 有直推人
                        if (!StringUtils.isEmpty(mallUserList.getDirectLeader())) {
                            // 查询直推人信息
                            MallUser mallUser = mallUserService.findById(mallUserList.getDirectLeader());
                            // 直推人收益 1 是比例
                            if ("1".equals(stringStringMap.get("mall_maker_profit_type"))) {
                                // 直推人收益 给个默认值防止出错
                                BigDecimal makerRes = new BigDecimal("0.01");

                                // 判断直推人是达人
                                if (null != mallUser.getIsTalent() && mallUser.getIsTalent().equals("1")) {
                                    // 直推人收益
                                    makerRes = new BigDecimal(stringStringMap.get("mall_maker_profit_proportion")).multiply(amount);

                                }
                                // 不是达人
                                else {
                                    // 直推人收益
                                    makerRes = new BigDecimal(stringStringMap.get("mall_maker_profit_proportion1")).multiply(amount);
                                }

                                // 直推人收益记录
                                recordMaker.setAmount(makerRes);
                                iMallRedQrcodeRecordService.save(recordMaker);
                                AccountUtils.accountChangeByExchange(mallUser.getId(), makerRes, CommonConstant.TASK_REWARD_CASH, AccountStatus.BID_1, paramsMapMaker);
                                // todo md
                                // 消息推送
                                String fansName = StringUtils.isNotEmpty(mallUserList.getNickname()) ? mallUserList.getNickname() : mallUserList.getMobile();
                                String content = "您的粉丝" + fansName + "成功领取现金红包，您所得收益¥" + makerRes.setScale(6, BigDecimal.ROUND_DOWN).toString() + "已转至可提现账户，可点击“有料-零钱提现-钱包流水”进行查看。";
                                PushUtils.pushMessage(mallUser.getId(), "佣金通知", content, 3, 0);
                            }
                            // todo 2 是固定金额 暂时不需要扩展用
                            else {
                                // 直推人收益
                                AccountUtils.accountChangeByExchange(mallUser.getId(), new BigDecimal(stringStringMap.get("mall_maker_profit_fixed")), TASK_REWARD_CASH, AccountStatus.BID_1, paramsMapMaker);

                                // todo md
                                // 消息推送
                                String fansName = StringUtils.isNotEmpty(mallUserList.getNickname()) ? mallUserList.getNickname() : mallUserList.getMobile();
                                String content = "您的粉丝" + fansName + "成功领取现金红包，您所得收益¥" + new BigDecimal(stringStringMap.get("mall_maker_profit_fixed")).setScale(6, BigDecimal.ROUND_DOWN).toString()
                                                + "已转至可提现账户，可点击“有料-零钱提现-钱包流水”进行查看。";
                                PushUtils.pushMessage(mallUser.getId(), "佣金通知", content, 3, 0);
                            }
                        }


                    }

                    Map<String, Object> mapData = new HashMap<>();
                    mapData.put("red", amount);
                    mapData.put("ad", listTad.get(nu));
                    mapData.put("userNum", cnt + 1);
                    mallPageView.setCreateTime(LocalDateTime.now());
                    mallPageViewService.saveOrUpdate(mallPageView);
                    return ResponseUtil.ok(mapData);
                } else {
                    return ResponseUtil.fail(402, "今日领取次数达到上限，请明日再来！");
                }

            } else {
                return ResponseUtil.fail(402, "网络繁忙，请稍后重试！");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseUtil.ok();
    }

}
