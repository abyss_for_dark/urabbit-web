package com.graphai.mall.wx.dto;

import java.util.Map;

public class WxLoginInfo {
    private String code;
    private String redirectURI;
    //分销员ID
    private String tpid;
    private String mobile;  //手机号码

    /**
     * 前端传入的路由参数数据
     */
    private Map<String,Object> router;
    
    private UserInfo userInfo;
    
    public Map<String, Object> getRouter() {
		return router;
	}

	public void setRouter(Map<String, Object> router) {
		this.router = router;
	}

	public String getRedirectURI() {
		return redirectURI;
	}

	public void setRedirectURI(String redirectURI) {
		this.redirectURI = redirectURI;
	}

	public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

	public String getTpid() {
		return tpid;
	}

	public void setTpid(String tpid) {
		this.tpid = tpid;
	}

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
