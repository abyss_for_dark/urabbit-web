package com.graphai.mall.wx.web.category;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Header;
import org.apache.http.message.BasicHeader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.graphai.commons.constant.MallConstant;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.json.JacksonUtils;
import com.graphai.mall.db.domain.MallAd;
import com.graphai.mall.db.domain.MallCategory;
import com.graphai.mall.db.domain.MallIndustry;
import com.graphai.mall.db.service.advertisement.MallAdService;
import com.graphai.mall.db.service.category.IndustryService;
import com.graphai.mall.db.service.category.MallCategoryService;
import com.graphai.mall.db.service.goods.MallGoodsService;
import com.graphai.open.api.getway.HttpRequestUtils;
import com.graphai.properties.PlatFormServerproperty;

@RestController
@RequestMapping("/wx/industry")
public class IndustryController {
	
	private final Log logger = LogFactory.getLog(IndustryController.class);
	
	@Autowired
	private IndustryService industryService;
	
	@Autowired
	private MallCategoryService categoryService;
	
	@Autowired
	private MallAdService adService;
	
	@Autowired
	private MallGoodsService goodsService;
	
	@Autowired
    private PlatFormServerproperty platFormServerproperty;
	
	@GetMapping("list")
	public Object list() {
		List<MallIndustry> industryList = industryService.getIndustryDictionaries();
		return ResponseUtil.ok(industryList);
	}
	
	
	
	@GetMapping("getDataByIndustry")
	public Object list(@NotNull String industryId,@NotNull String cateId) {
		
//		List<MallAd> ads=new ArrayList<MallAd>();
//		List<MallCategory> categories =new ArrayList<MallCategory>();
		// List<MallCategory> activityCategories=new ArrayList<MallCategory>();

		try {
			Map<String, Object> map = new HashMap<String, Object>();

			// 中间类目
			// categories = categoryService.getCategoryByIndustry(industryId,
			// "smallcategory");
			// 类目
			String url = platFormServerproperty.getUrl(MallConstant.TAOBAO);
			Map<String, Object> mappMap = new HashMap<String, Object>();
			mappMap.put("plateform", "dtk");
			mappMap.put("pid", cateId);
			mappMap.put("positionCode", "dtkcategory");

			Header[] headers = new BasicHeader[] { new BasicHeader("method", "queryCategorysByPid"),
					new BasicHeader("version", "v1.0") };
			String result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);

			JSONObject object = JSONObject.parseObject(result);
			List<MallCategory> categories = new ArrayList<MallCategory>();
			if (object.getString("errno").equals("0")) {

				categories = JSON.parseObject(object.get("data").toString(), List.class);
				if (CollectionUtils.isNotEmpty(categories)) {
					map.put("categories", categories);
				}
				
			}

			//// 广告
			String url2 = platFormServerproperty.getUrl(MallConstant.TAOBAO);
			Map<String, Object> mappMap2 = new HashMap<String, Object>();
			mappMap2.put("plateform", "dtk");
			mappMap2.put("industryId", industryId);
			mappMap2.put("relaType", "smallad");

			Header[] headers2 = new BasicHeader[] { new BasicHeader("method", "getAdsByIndustry"),
					new BasicHeader("version", "v1.0") };
			String result2 = HttpRequestUtils.post(url2, JacksonUtils.bean2Jsn(mappMap2), headers2);

			JSONObject object2 = JSONObject.parseObject(result2);
			List<MallAd> ads = new ArrayList<MallAd>();
			if (object2.getString("errno").equals("0")) {

				ads = JSON.parseObject(object2.get("data").toString(), List.class);
				if (CollectionUtils.isNotEmpty(ads)) {
					map.put("ads", ads);
				}

			}

			// 广告
			// ads=adService.getAdsByIndustry(industryId, "smallad");
			// 下面活动类目
//			activityCategories = categoryService.getCategoryByIndustry(industryId, "smallactivity");

			// 关联行业商品
//            List<MallGoods> hotGoodsList = goodsService.queryByHot(1, 10, industryId, null, null);
//			
//			map.put("goodsList", hotGoodsList);
			return ResponseUtil.ok(map);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return ResponseUtil.fail();
	}
	
	
	
}
