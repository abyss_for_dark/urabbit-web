package com.graphai.mall.wx.web.integral;

import static com.graphai.mall.wx.util.WxResponseCode.GOODS_NO_STOCK;
import static com.graphai.mall.wx.util.WxResponseCode.GOODS_UNSHELVE;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.validation.constraints.NotNull;

import com.graphai.mall.db.service.groupon.MallGrouponRulesService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.json.JacksonUtil;
import com.graphai.id.GraphaiIdGenerator;
import com.graphai.mall.core.system.SystemConfig;
import com.graphai.mall.db.domain.MallAddress;
import com.graphai.mall.db.domain.MallCart;
import com.graphai.mall.db.domain.MallGoods;
import com.graphai.mall.db.domain.MallGoodsProduct;
import com.graphai.mall.db.domain.MallRebateRecord;
import com.graphai.mall.db.domain.MallUser;
import com.graphai.mall.db.service.MallCartService;
import com.graphai.mall.db.service.coupon.CouponVerifyService;
import com.graphai.mall.db.service.coupon.MallCouponService;
import com.graphai.mall.db.service.coupon.MallCouponUserService;
import com.graphai.mall.db.service.distribution.FenXiaoService;
import com.graphai.mall.db.service.goods.MallGoodsProductService;
import com.graphai.mall.db.service.goods.MallGoodsService;
import com.graphai.mall.db.service.order.MallOrderGoodsService;
import com.graphai.mall.db.service.order.MallOrderService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.service.user.behavior.MallAddressService;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.mall.wx.util.GoodsUtils;
import com.graphai.mall.wx.web.cart.WxCartController;

/**
 * 用户购物车服务-积分商城
 */
@RestController
@RequestMapping("/wx/cart/integral")
@Validated
public class WxCartIntegralController {

	private final Log logger = LogFactory.getLog(WxCartController.class);

	@Autowired
	private MallCartService cartService;
	@Autowired
	private MallGoodsService goodsService;
	@Autowired
	private MallGoodsProductService productService;
	@Autowired
	private MallAddressService addressService;
	@Autowired
	private MallGrouponRulesService grouponRulesService;
	@Autowired
	private MallCouponService couponService;
	@Autowired
	private MallCouponUserService couponUserService;
	@Autowired
	private CouponVerifyService couponVerifyService;
	@Autowired
	private FenXiaoService fenXiaoService;

	@Autowired
	MallOrderService orderService;
	@Autowired
	private MallOrderGoodsService orderGoodsService;
	@Autowired
	private MallUserService userService;

	/**
	 * 用户购物车信息
	 *
	 * @param userId
	 *            用户ID
	 * @return 用户购物车信息
	 */
	@GetMapping("index")
	public Object index(@LoginUser String userId) {
		if (userId == null) {
			return ResponseUtil.unlogin();
		}

		List<MallCart> cartList = cartService.queryByUid(userId);
		Integer goodsCount = 0;
		BigDecimal goodsAmount = new BigDecimal(0.00);
		Integer checkedGoodsCount = 0;
		BigDecimal checkedGoodsAmount = new BigDecimal(0.00);
		for (MallCart cart : cartList) {
			goodsCount += cart.getNumber();
			goodsAmount = goodsAmount.add(cart.getPrice().multiply(new BigDecimal(cart.getNumber())));
			if (cart.getChecked()) {
				checkedGoodsCount += cart.getNumber();
				checkedGoodsAmount = checkedGoodsAmount.add(cart.getPrice().multiply(new BigDecimal(cart.getNumber())));
			}
		}
		Map<String, Object> cartTotal = new HashMap<>();
		cartTotal.put("goodsCount", goodsCount);
		cartTotal.put("goodsAmount", goodsAmount);
		cartTotal.put("checkedGoodsCount", checkedGoodsCount);
		cartTotal.put("checkedGoodsAmount", checkedGoodsAmount);

		Map<String, Object> result = new HashMap<>();
		result.put("cartList", cartList);
		result.put("cartTotal", cartTotal);

		return ResponseUtil.ok(result);
	}

	/**
	 * 加入商品到购物车
	 * <p>
	 * 如果已经存在购物车货品，则增加数量； 否则添加新的购物车货品项。
	 *
	 * @param userId
	 *            用户ID
	 * @param cart
	 *            购物车商品信息， { goodsId: xxx, productId: xxx, number: xxx }
	 * @return 加入购物车操作结果
	 */
	@PostMapping("add")
	public Object add(@LoginUser String userId, @RequestBody MallCart cart) {
		if (userId == null) {
			return ResponseUtil.unlogin();
		}
		if (cart == null) {
			return ResponseUtil.badArgument();
		}

		String productId = cart.getProductId();
		Integer number = cart.getNumber().intValue();
		String goodsId = cart.getGoodsId();
//		Integer shId = cart.getShId();
		if (!ObjectUtils.allNotNull(productId, number, goodsId)) {
			return ResponseUtil.badArgument();
		}
		if (number <= 0) {
			return ResponseUtil.badArgument();
		}

		// 判断商品是否可以购买
		MallGoods goods = goodsService.findById(goodsId);
		if (goods == null || !goods.getIsOnSale()) {
			return ResponseUtil.fail(GOODS_UNSHELVE, "商品已下架");
		}
//		Integer restrictionQuantity = goods.getRestrictionQuantity();
//		if(restrictionQuantity!=null){
//			 List<MallCart>  goodsCartList = cartService.querySelective(userId, goodsId, 1, 10, null, null);
//			 if(CollectionUtils.isNotEmpty(goodsCartList)){
//				 MallCart myCart = goodsCartList.get(0);
//				 Short goodInCartNumber = myCart.getNumber();
//				 if(goodInCartNumber>=restrictionQuantity){
//					 return ResponseUtil.fail(GOODS_UNSHELVE, "商品限购");
//				 }
//			 }
//
//			 List<Map<String,Object>> orderGoods = orderService.findMyOrderGoods(userId, goodsId);
//			 Integer NowNumber = 0 ;
//			 if(CollectionUtils.isNotEmpty(orderGoods)){
//				 for (int i = 0; i < orderGoods.size(); i++) {
//					 Map<String,Object> map = orderGoods.get(i);
//					 NowNumber += (MapUtils.getInteger(map, "number") !=null ? MapUtils.getInteger(map, "number") : 0);
//				}
//			 }
//
//			 System.out.println("已经购买的数量: "+NowNumber );
//			 if(NowNumber>=restrictionQuantity){
//				 return ResponseUtil.fail(GOODS_UNSHELVE, "商品限购");
//			 }
//		}


//		MallRebateRecord rebate = null;
//		if (shId != null) {
//			rebate = fenXiaoService.getMallRebateRecordById(shId);
//		}

		MallGoodsProduct product = productService.findById(productId);
		// 判断购物车中是否存在此规格商品
		MallCart existCart = cartService.queryExist(goodsId, productId, userId);
		if (existCart == null) {
			// 取得规格的信息,判断规格库存
			if (product == null || number > product.getNumber()) {
				return ResponseUtil.fail(GOODS_NO_STOCK, "库存不足");
			}

			cart.setId(null);
			cart.setGoodsSn(goods.getGoodsSn());
			cart.setGoodsName((goods.getName()));
			cart.setPicUrl(goods.getPicUrl());
			 cart.setPrice(product.getPrice());
//			if (rebate != null) {
//				BigDecimal productPrice = product.getPrice();
//				BigDecimal finalPrice = GoodsUtils.getRetailPrice(rebate, productPrice);
//				cart.setPrice(finalPrice);
//			} else {
//				cart.setPrice(product.getPrice());
//			}
			cart.setSrcGoodId(goods.getSrcGoodId());
			cart.setSpecifications(product.getSpecifications());
			cart.setUserId(userId);
			cart.setChecked(true);
			cartService.add(cart);
		} else {
			// 取得规格的信息,判断规格库存
			int num = existCart.getNumber() + number;
			if (num > product.getNumber()) {
				return ResponseUtil.fail(GOODS_NO_STOCK, "库存不足");
			}
			existCart.setNumber((short) num);
			if (cartService.updateById(existCart) == 0) {
				return ResponseUtil.updatedDataFailed();
			}
		}

		return goodscount(userId);
	}

	/**
	 * 立即购买
	 * <p>
	 * 和add方法的区别在于： 1. 如果购物车内已经存在购物车货品，前者的逻辑是数量添加，这里的逻辑是数量覆盖 2.
	 * 添加成功以后，前者的逻辑是返回当前购物车商品数量，这里的逻辑是返回对应购物车项的ID
	 *
	 * @param data
	 *            购物车商品信息， { goodsId: xxx, productId: xxx, number: xxx }
	 * @return 立即购买操作结果
	 */
	@PostMapping("fastadd")
	public Object fastadd(@RequestBody String data) {

		//手机号
		String mobile = JacksonUtil.parseString(data, "mobile");
//		if (cart == null) {
//			return ResponseUtil.badArgument();
//		}
//		String productId = cart.getProductId();//商品货品表ID
//		Integer number = cart.getNumber().intValue();//购买数量
//		String goodsId = cart.getGoodsId();//商品ID
		String productId = JacksonUtil.parseString(data, "productId");
		String numberStr = JacksonUtil.parseString(data, "number");
		String goodsId = JacksonUtil.parseString(data, "goodsId");
		MallRebateRecord rebate = null;
		if (!ObjectUtils.allNotNull(productId, numberStr, goodsId)) {
			return ResponseUtil.badArgument();
		}
		int number = Integer.parseInt(numberStr);
		if (number <= 0) {
			return ResponseUtil.badArgument();
		}
		MallCart cart = new MallCart();
		cart.setProductId(productId);
		cart.setNumber(Short.parseShort(numberStr));
		cart.setGoodsId(goodsId);

		String userId="";
		List<MallUser> users = userService.queryByMobile(mobile);
		if (CollectionUtils.isNotEmpty(users)) {
			if (org.apache.commons.lang3.StringUtils.isNotEmpty(users.get(0).getId())) {
				userId = users.get(0).getId();
			}
		}else {
			return ResponseUtil.fail(501,"用户不存在!");
		}

		JSONObject obj=null;
		try {
			obj = goodsService.queryGoodsByProductId(productId);
			logger.info("商品货品表ID="+productId+"查询返回结果："+obj.toJSONString());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (Objects.equals(null, obj)) {
			return ResponseUtil.fail(507, "商品信息有误!");
		}
		// 判断商品是否可以购买
		if (!obj.getBooleanValue("isOnSale")) {
			return ResponseUtil.fail(GOODS_UNSHELVE, "商品已下架");
		}

		// 判断购物车中是否存在此规格商品
		MallCart existCart = cartService.queryExist(goodsId, productId, userId);
		if (existCart == null) {
			// 取得规格的信息,判断规格库存
			if (number > obj.getIntValue("number")) {
				return ResponseUtil.fail(GOODS_NO_STOCK, "库存不足");
			}
			cart.setId(GraphaiIdGenerator.nextId("MallCart"));//cart.setId(null);
			cart.setGoodsSn(obj.getString("gGoodsSn"));
			cart.setGoodsName(obj.getString("gName"));
			cart.setPicUrl(obj.getString("picUrl"));
			cart.setSrcGoodId(obj.getString("gSrcGoodId"));
			if (rebate != null) {
				BigDecimal productPrice = obj.getBigDecimal("price");
				BigDecimal finalPrice = GoodsUtils.getRetailPrice(rebate, productPrice);
				cart.setPrice(finalPrice);
			} else {
				cart.setPrice(obj.getBigDecimal("price"));
			}
			String specifications = obj.getString("specifications");
//			String[] arr = null;
//			if(!"".equals(specifications)){
//				arr = specifications.split(",");
//			}
//			cart.setSpecifications(arr);
			cart.setUserId(userId);
			cart.setChecked(true);//是否勾选购物车商品
			cart.setDeptId(obj.getString("deptId"));//购物车加归属部门ID
			cartService.add(cart);
		} else {
			// 取得规格的信息,判断规格库存
			int num = number;
			if (num > obj.getIntValue("number")) {
				return ResponseUtil.fail(GOODS_NO_STOCK, "库存不足");
			}
			existCart.setNumber((short) num);
			if(obj.getBigDecimal("price").compareTo(new BigDecimal(0)) > 0){
				existCart.setPrice(obj.getBigDecimal("price"));
			}
			if (cartService.updateById(existCart) == 0) {
				return ResponseUtil.updatedDataFailed();
			}
		}

		return ResponseUtil.ok(existCart != null ? existCart.getId() : cart.getId());
	}


	/**
	 * 修改购物车商品货品数量
	 *
	 * @param userId
	 *            用户ID
	 * @param cart
	 *            购物车商品信息， { id: xxx, goodsId: xxx, productId: xxx, number: xxx
	 *            }
	 * @return 修改结果
	 */
//	@PostMapping("update")
//	public Object update(@LoginUser Integer userId, @RequestBody MallCart cart) {
//		if (userId == null) {
//			return ResponseUtil.unlogin();
//		}
//		if (cart == null) {
//			return ResponseUtil.badArgument();
//		}
//		Integer productId = cart.getProductId();
//		Integer number = cart.getNumber().intValue();
//		Integer goodsId = cart.getGoodsId();
//		Integer id = cart.getId();
//		if (!ObjectUtils.allNotNull(id, productId, number, goodsId)) {
//			return ResponseUtil.badArgument();
//		}
//		if (number <= 0) {
//			return ResponseUtil.badArgument();
//		}
//
//		// 判断是否存在该订单
//		// 如果不存在，直接返回错误
//		MallCart existCart = cartService.findById(id);
//		if (existCart == null) {
//			return ResponseUtil.badArgumentValue();
//		}
//
//		// 判断goodsId和productId是否与当前cart里的值一致
//		if (!existCart.getGoodsId().equals(goodsId)) {
//			return ResponseUtil.badArgumentValue();
//		}
//		if (!existCart.getProductId().equals(productId)) {
//			return ResponseUtil.badArgumentValue();
//		}
//
//		// 判断商品是否可以购买
//		MallGoods goods = goodsService.findById(goodsId);
//		if (goods == null || !goods.getIsOnSale()) {
//			return ResponseUtil.fail(GOODS_UNSHELVE, "商品已下架");
//		}
//
//		// 取得规格的信息,判断规格库存
//		MallGoodsProduct product = productService.findById(productId);
//		if (product == null || product.getNumber() < number) {
//			return ResponseUtil.fail(GOODS_UNSHELVE, "库存不足");
//		}
//
//		existCart.setNumber(number.shortValue());
//
//		if (cartService.updateById(existCart) == 0) {
//			return ResponseUtil.updatedDataFailed();
//		}
//		return index(userId);
//	}

	/**
	 * 购物车商品货品勾选状态
	 * <p>
	 * 如果原来没有勾选，则设置勾选状态；如果商品已经勾选，则设置非勾选状态。
	 *
	 * @param userId
	 *            用户ID
	 * @param body
	 *            购物车商品信息， { productIds: xxx, isChecked: 1/0 }
	 * @return 购物车信息
	 */
	@PostMapping("checked")
	public Object checked(@LoginUser Integer userId, @RequestBody String body) {
		if (userId == null) {
			return ResponseUtil.unlogin();
		}
		if (body == null) {
			return ResponseUtil.badArgument();
		}

		List<String> productIds = JacksonUtil.parseStringList(body, "productIds");
		if (productIds == null) {
			return ResponseUtil.badArgument();
		}

		Integer checkValue = JacksonUtil.parseInteger(body, "isChecked");
		if (checkValue == null) {
			return ResponseUtil.badArgument();
		}
		Boolean isChecked = (checkValue == 1);

		cartService.updateCheck(userId.toString(), productIds, isChecked);
		return index(userId.toString());
	}

	/**
	 * 购物车商品删除
	 *
	 * @param userId
	 *            用户ID
	 * @param body
	 *            购物车商品信息， { productIds: xxx }
	 * @return 购物车信息 成功则 { errno: 0, errmsg: '成功', data: xxx } 失败则 { errno: XXX,
	 *         errmsg: XXX }
	 */
//	@PostMapping("delete")
//	public Object delete(@LoginUser String userId, @RequestBody String body) {
//		if (userId == null) {
//			return ResponseUtil.unlogin();
//		}
//		if (body == null) {
//			return ResponseUtil.badArgument();
//		}
//
//		List<Integer> productIds = JacksonUtil.parseIntegerList(body, "productIds");
//
//		if (productIds == null || productIds.size() == 0) {
//			return ResponseUtil.badArgument();
//		}
//
//		cartService.delete(productIds, userId);
//		return index(userId);
//	}

	/**
	 * 购物车商品货品数量
	 * <p>
	 * 如果用户没有登录，则返回空数据。
	 *
	 * @param userId
	 *            用户ID
	 * @return 购物车商品货品数量
	 */
	@GetMapping("goodscount")
	public Object goodscount(@LoginUser String userId) {
		if (userId == null) {
			return ResponseUtil.ok(0);
		}

		int goodsCount = 0;
		List<MallCart> cartList = cartService.queryByUid(userId);
		for (MallCart cart : cartList) {
			goodsCount += cart.getNumber();
		}

		return ResponseUtil.ok(goodsCount);
	}

	/**
	 * 购物车下单-购物车信息
	 *
	 * @param mobile
	 *            用户ID
	 * @param cartId
	 *            购物车商品ID： 如果购物车商品ID是空，则下单当前用户所有购物车商品； 如果购物车商品ID非空，则只下单当前购物车商品。
	 * @param addressId
	 *            收货地址ID： 如果收货地址ID是空，则查询当前用户的默认地址。
	 * @param couponId
	 *            优惠券ID： 如果优惠券ID是空，则自动选择合适的优惠券。
	 * @return 购物车操作结果
	 */
	@GetMapping("checkout")
	public Object checkout(@NotNull String mobile,@NotNull String cartId,String addressId,String couponId,String couponCode,String grouponRulesId) throws Exception {

//		String mobile = JacksonUtil.parseString(reqData, "mobile");
//		String cartId = JacksonUtil.parseString(reqData, "cartId");
//		String addressId = JacksonUtil.parseString(reqData, "addressId");
//		String couponId = JacksonUtil.parseString(reqData, "couponId");
//		String couponCode = JacksonUtil.parseString(reqData, "couponCode");
//		String grouponRulesId = JacksonUtil.parseString(reqData, "grouponRulesId");

		String userId="";
		List<MallUser> users = userService.queryByMobile(mobile);
		if (CollectionUtils.isNotEmpty(users)) {
			if (org.apache.commons.lang3.StringUtils.isNotEmpty(users.get(0).getId())) {
				userId = users.get(0).getId();
			}
		}else {
			return ResponseUtil.fail(501,"用户不存在!");
		}

		// 收货地址
		MallAddress checkedAddress = null;
		if (addressId == null || addressId.equals("0")) {
			checkedAddress = addressService.findDefault(userId);
			// 如果仍然没有地址，则是没有收获地址
			// 返回一个空的地址id=0，这样前端则会提醒添加地址
			if (checkedAddress == null) {
				checkedAddress = new MallAddress();
				//checkedAddress.setId("0");
				addressId = "0";
			} else {
				addressId = checkedAddress.getId();
			}
		} else {
			checkedAddress = addressService.query(userId, addressId);
			// 如果null, 则报错,返回空对象
			if (checkedAddress == null) {
				checkedAddress = new MallAddress();
			}
		}

		// 团购优惠,么有团控，注释掉
		BigDecimal grouponPrice = new BigDecimal(0.00);

		// 商品价格
		List<MallCart> checkedGoodsList = null;
		if (cartId == null || cartId.equals(0)) {
			checkedGoodsList = cartService.queryByUidAndChecked(userId);
		} else {
			MallCart cart = cartService.findById(cartId);
			if (cart == null) {
				return ResponseUtil.badArgumentValue();
			}
			checkedGoodsList = new ArrayList<>(1);
			checkedGoodsList.add(cart);
		}
		BigDecimal checkedGoodsPrice = new BigDecimal(0.00);//商品金额
		BigDecimal finalActual = new BigDecimal(0.00);//运费

//		List<String> goodsList = new ArrayList<String>();
//		for (MallCart cart : checkedGoodsList) {
//			// 计算商品金额
//			checkedGoodsPrice = checkedGoodsPrice.add(cart.getPrice().multiply(new BigDecimal(cart.getNumber())));
//			// 运费
//			BigDecimal yprice = goodsService.findById(cart.getGoodsId()).getcRetailPrice(); // 通过商品ID获取运费
//			finalActual = finalActual.add(null == yprice? SystemConfig.getFreight():yprice);// 如果商品运费为空，则获取系统默认配置运费
//			goodsList.add(cart.getGoodsId());
//		}
		if(checkedGoodsList.size()<= 0){
			return ResponseUtil.fail(507, "商品信息有误!购物车不存在商品！");
		}
		String productId=checkedGoodsList.get(0).getProductId();
		JSONObject obj = goodsService.queryGoodsByProductId(productId);
		if (Objects.equals(null, obj)) {
			return ResponseUtil.fail(507, "商品信息有误!");
		}
		checkedGoodsPrice=checkedGoodsList.get(0).getPrice().multiply(new BigDecimal(checkedGoodsList.get(0).getNumber()));// 计算商品金额
		// 运费
		String gCRetailPrice = obj.getString("gCRetailPrice");
		if("".equals(gCRetailPrice)){
			gCRetailPrice = "0";
		}
		BigDecimal yprice = new BigDecimal(gCRetailPrice); // 通过商品ID获取运费
		if(yprice.compareTo(new BigDecimal(0)) < 0){
			yprice = BigDecimal.ZERO;
		}
		finalActual = finalActual.add(yprice);// 如果商品运费为空，则获取系统默认配置运费

//		Boolean addressShow = false;
//		List<MallGoods> mallGoodsList = goodsService.findByIdList(goodsList);
//		for (MallGoods MallGoods:mallGoodsList){
//			if ("2" == MallGoods.getCategoryId()){ // 商品类目为2是礼品，只有它才收配送费
//				addressShow = true;
//				break;
//			}
//		}
		Boolean addressShow = false;
		if ("2".equals(obj.getString("gCategoryId"))){ // 商品类目为2是礼品，只有它才收配送费
			addressShow = true;
		}

		//商品原价
//		BigDecimal factoryPrice = BigDecimal.ZERO;
//		if(mallGoodsList != null){
//			if(mallGoodsList.size()>0){
//				factoryPrice = mallGoodsList.get(0).getFactoryPrice();
//			}
//		}
		BigDecimal factoryPrice = BigDecimal.ZERO;
//		if(obj.getString("gFactoryPrice") != null){
//			factoryPrice = new BigDecimal(obj.getString("gFactoryPrice"));
//		}

		//因为商城只能购买一个商品，可以直接set值
//		for(MallCart checkList:checkedGoodsList){
//			checkList.setFactoryPrice(factoryPrice);
//		}

		// 计算优惠券可用情况
		BigDecimal tmpCouponPrice = new BigDecimal(0.00);
		String tmpCouponId = "";
		int tmpCouponLength = 0;
//		List<MallCouponUser> couponUserList = couponUserService.queryAll(userId);
//		for (MallCouponUser couponUser : couponUserList) {
//			MallCoupon coupon = couponVerifyService.checkCoupon(userId, couponUser.getCouponId(), checkedGoodsPrice);
//			if (coupon == null) {
//				continue;
//			}
//
//			tmpCouponLength++;
//			if (tmpCouponPrice.compareTo(coupon.getDiscount()) == -1) {
//				tmpCouponPrice = coupon.getDiscount();
//				tmpCouponId = coupon.getId();
//			}
//		}
		// 获取优惠券减免金额，优惠券可用数量
		int availableCouponLength = tmpCouponLength;
		BigDecimal couponPrice = new BigDecimal(0);
		// 这里存在三种情况
		// 1. 用户不想使用优惠券，则不处理
		// 2. 用户想自动使用优惠券，则选择合适优惠券
		// 3. 用户已选择优惠券，则测试优惠券是否合适
		if (couponId == null || couponId.equals(-1)) {
			couponId = "-1";
		} else if (couponId.equals("0")) {
			couponPrice = tmpCouponPrice;
			couponId = tmpCouponId;
		} else {
			//校验购物车内的商品是不是
//			for (MallCart cart : checkedGoodsList) {
//				MallCoupon coupon = couponVerifyService.checkCoupon(userId,cart.getGoodsId(), couponCode, checkedGoodsPrice);
//
//				try {
//					logger.info("返回的优惠券信息 ： " + JacksonUtils.bean2Jsn(coupon));
//				} catch (JsonParseException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				} catch (JsonMappingException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				} catch (IOException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//				// 用户选择的优惠券有问题，则选择合适优惠券，否则使用用户选择的优惠券
//				if (coupon == null) {
//					couponPrice = tmpCouponPrice;
//					couponId = tmpCouponId;
//				} else {
//					couponPrice = coupon.getDiscount();
//				}
//			}
		}

		// 根据订单商品总价计算运费，满88则免运费，否则8元；
		BigDecimal freightPrice = new BigDecimal(0.00);
		// 可以使用的其他钱，例如用户积分
		BigDecimal integralPrice = new BigDecimal(0.00);
		// 订单费用 = 订单金额 + 配送费 - 优惠金额
		BigDecimal orderTotalPrice = checkedGoodsPrice.add(freightPrice).subtract(couponPrice);
		BigDecimal actualPrice = new BigDecimal(0.00);

		//如果地址存在，则运费赋值返回
		if (addressShow){
			actualPrice = finalActual;
		}

		Map<String, Object> data = new HashMap<>();
		data.put("addressId", addressId);
		data.put("couponId", couponId);
		data.put("cartId", cartId);
		data.put("grouponRulesId", grouponRulesId);
		data.put("grouponPrice", grouponPrice);
		data.put("checkedAddress", checkedAddress);
		data.put("availableCouponLength", availableCouponLength);
		data.put("goodsTotalPrice", checkedGoodsPrice);
		// 运费
		data.put("freightPrice", freightPrice);
		data.put("couponPrice", couponPrice);
		data.put("orderTotalPrice", orderTotalPrice);
		data.put("actualPrice", actualPrice);
		data.put("checkedGoodsList", checkedGoodsList);
		data.put("factoryPrice",factoryPrice);
		return ResponseUtil.ok(data);
	}
}
