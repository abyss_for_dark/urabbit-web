package com.graphai.mall.wx.web.order;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.graphai.mall.admin.domain.MallGoodsPosition;
import com.graphai.mall.admin.domain.MallMerchant;
import com.graphai.mall.admin.domain.MallPurchaseOrder;
import com.graphai.mall.admin.domain.MallPurchaseOrderGoods;
import com.graphai.mall.admin.service.IMallGoodsPositionService;
import com.graphai.mall.admin.service.IMallMerchantService;
import com.graphai.mall.admin.service.IMallPurchaseOrderGoodsService;
import com.graphai.mall.admin.service.IMallPurchaseOrderService;
import com.graphai.mall.db.constant.group.GroupEnum;
import com.graphai.mall.db.dao.MallOrderMapper;
import com.graphai.mall.db.domain.*;
import com.graphai.mall.db.service.common.ShipService;
import com.graphai.mall.db.service.order.MallAftersaleService;
import com.graphai.mall.db.vo.MallGroupVo;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.github.houbb.sensitive.core.api.SensitiveUtil;
import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.date.DateUtilTwo;
import com.graphai.commons.util.ip.IpUtils;
import com.graphai.commons.util.json.JacksonUtil;
import com.graphai.id.GraphaiIdGenerator;
import com.graphai.mall.db.dao.MallLoseOrderMapper;
import com.graphai.mall.db.constant.account.AccountStatus;
import com.graphai.mall.db.service.account.FcAccountPrechargeBillService;
import com.graphai.mall.db.service.goods.MallGoodsCouponCodeService;
import com.graphai.mall.db.service.goods.MallGoodsService;
import com.graphai.mall.db.service.groupon.MallGrouponRulesService;
import com.graphai.mall.db.service.groupon.MallGrouponService;
import com.graphai.mall.db.service.integral.MallOrderIntegralService;
import com.graphai.mall.db.service.order.MallOrderGoodsService;
import com.graphai.mall.db.service.order.MallOrderService;
import com.graphai.mall.db.service.pay.DoPayService;
import com.graphai.mall.db.service.red.WxRedService;
import com.graphai.mall.db.service.sysconfig.MallSystemConfigService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.util.OrderUtil;
import com.graphai.mall.db.util.StringHelper;
import com.graphai.mall.db.util.UserUtils;
import com.graphai.mall.db.vo.MallOrderGoodsVo;
import com.graphai.mall.db.vo.MallOrderGroupVo;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.mall.wx.service.WxOrderService;
import com.graphai.open.mallorder.service.IMallOrderYLService;
import com.graphai.properties.WxProperties;

import static com.graphai.mall.wx.util.WxResponseCode.ORDER_PAY_FAIL;

@RestController
@RequestMapping("/wx/order")
@Validated
public class WxOrderController {
    private final Log logger = LogFactory.getLog(WxOrderController.class);

    @Autowired
    private WxOrderService wxOrderService;
    @Autowired
    private WxProperties properties;
    @Autowired
    private MallUserService mallUserService;
    @Autowired
    private MallGrouponService grouponService;
    @Autowired
    private MallOrderService MallOrderService;
    @Autowired
    private MallOrderGoodsService orderGoodsService;
    @Autowired
    private MallLoseOrderMapper MallLoseOrderMapper;
    @Autowired
    private MallGoodsCouponCodeService mallGoodsCouponCodeService;
    @Autowired
    private MallSystemConfigService mallSystemConfigService;
    @Autowired
    private MallGrouponService groupService;
    @Autowired
    private MallGrouponRulesService rulesService;
    @Autowired
    private MallOrderIntegralService mallOrderIntegralService;
    @Autowired
    private FcAccountPrechargeBillService fcAccountPrechargeBillService;
    @Autowired
    private MallGoodsService goodsService;
    @Autowired
    private WxRedService wxRedService;
    @Autowired
    IMallOrderYLService mallOrderYLService;
    @Autowired
    private MallOrderService mallOrderService;
    @Autowired
    private IMallGoodsPositionService mallGoodsPositionService;
    @Autowired
    private IMallPurchaseOrderService mallPurchaseOrderService;
    @Autowired
    private IMallPurchaseOrderGoodsService mallPurchaseOrderGoodsService;
    @Autowired
    private ShipService shipService;
    @Autowired
    private MallAftersaleService mallAftersaleService;
    @Autowired
    private IMallMerchantService mallMerchantService;

    /**
     * 订单列表
     *
     * @param userId 用户ID
     * @param showType 订单信息
     * @param orderType 订单类型 (0:普通；秒杀；1：直播)
     * @param page 分页页数
     * @param limit 分页大小
     * @return 订单列表
     */
    @GetMapping("list")
    public Object list(@LoginUser String userId, @RequestParam(defaultValue = "0") Integer showType,Integer orderType,
                    @RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit) {
        MallUser mallUser = mallUserService.queryByUserId(userId);
        Object orders = new Object();
        if(mallUser != null && (mallUser.getLevelId().equals("9") || mallUser.getLevelId().equals("10") || mallUser.getLevelId().equals("11"))){
            //商户
            orders = mallPurchaseOrderService.list(userId,null, showType, orderType, page, limit,"client");
        }else{
            // 普通用户或工厂
            orders = wxOrderService.list(userId,null, showType, orderType, page, limit,"client");
        }
        return orders;
    }


    @GetMapping("getBackOrder")
    public Object getBackOrder(@LoginUser String userId,
                    @RequestParam(value = "orderId", required = true) String orderId,
                    @RequestParam(defaultValue = "dtk") String platForm) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }

        if (StringHelper.isNullOrEmpty(orderId)) {
            return ResponseUtil.fail(509, "订单号不能为空!");
        }



        MallLoseOrderExample example = new MallLoseOrderExample();
        example.createCriteria().andOrderIdEqualTo(orderId).andDeletedEqualTo(false);
        List<MallLoseOrder> loseOrders = MallLoseOrderMapper.selectByExample(example);

        if (CollectionUtils.isNotEmpty(loseOrders)) {
            return ResponseUtil.fail(512, "此订单已存在找回记录!");
        }

        try {
            MallLoseOrder order = new MallLoseOrder();
            order.setId(GraphaiIdGenerator.nextId("MallLoseOrder"));
            order.setUserId(userId);
            order.setOrderId(orderId);
            order.setPlatform(platForm);
            order.setDeleted(false);
            order.setStatus("01");
            order.setAddTime(LocalDateTime.now());
            MallLoseOrderMapper.insert(order);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw e;
        }

        return ResponseUtil.ok();


    }


    /**
     * 订单列表
     *
     * @param userId 用户ID
     * @param page 分页页数
     * @param limit 分页大小
     * @return 订单列表
     */
    @GetMapping("newList")
    public Object newList(@LoginUser String userId, @RequestParam(defaultValue = "shop") String orderType,
                    @RequestParam String huid, @RequestParam(defaultValue = "1") Integer page,
                    @RequestParam(defaultValue = "10") Integer limit) {


        if (userId == null) {
            return ResponseUtil.unlogin();
        }

        try {
            if (page <= 0) {
                page = 1;
            }

            List<MallOrder> orderList =
                            MallOrderService.queryListByType(orderType, huid, (page - 1) * limit, limit, userId);
            List<Map<String, Object>> orderVoList = new ArrayList<>(orderList.size());
            if (CollectionUtils.isNotEmpty(orderList)) {

                for (MallOrder order : orderList) {
                    Map<String, Object> orderVo = new HashMap<>();
                    orderVo.put("id", order.getId());
                    orderVo.put("orderSn", order.getOrderSn());
                    orderVo.put("actualPrice", order.getActualPrice());
                    orderVo.put("orderTime", order.getPayTime());
                    orderVo.put("createTime", order.getAddTime());
                    orderVo.put("huid", order.getHuid());
                    orderVo.put("orderStatus", order.getOrderStatus());
                    if (StringHelper.isNotNullAndEmpty(order.getMobile())) {
                        orderVo.put("account", order.getMobile());
                    }
                    if ("shop".equals(orderType)) {
                        orderVo.put("orderStatusDesc", order.getOrderStatusDesc());
                    } else {
                        orderVo.put("orderStatusDesc", OrderUtil.orderStatusText(order));
                    }

                    List<MallOrderGoods> orderGoodsList = orderGoodsService.queryByOid(order.getId());
                    orderVo.put("goodsNumber", orderGoodsList.size());
                    List<Map<String, Object>> orderGoodsVoList = new ArrayList<>(orderGoodsList.size());
                    for (MallOrderGoods orderGoods : orderGoodsList) {
                        Map<String, Object> orderGoodsVo = new HashMap<>();
                        orderGoodsVo.put("id", orderGoods.getId());
                        orderGoodsVo.put("goodsName", orderGoods.getGoodsName());
                        orderGoodsVo.put("number", orderGoods.getNumber());
                        orderGoodsVo.put("picUrl", orderGoods.getPicUrl());
                        orderGoodsVo.put("price", orderGoods.getPrice());
                        orderGoodsVo.put("specifications", orderGoods.getSpecifications());

                        if (StringHelper.isNotNullAndEmpty(orderGoods.getLitre())) {
                            orderGoodsVo.put("litre", orderGoods.getLitre());
                        }
                        if (StringHelper.isNotNullAndEmpty(orderGoods.getCardPwd())) {
                            orderGoodsVo.put("cardPwd", orderGoods.getCardPwd());
                        }
                        if (StringHelper.isNotNullAndEmpty(orderGoods.getCardDeadline())) {
                            orderGoodsVo.put("cardDeadLine", orderGoods.getCardDeadline());
                        }
                        if (StringHelper.isNotNullAndEmpty(orderGoods.getCardNumber())) {
                            orderGoodsVo.put("cardNumber", orderGoods.getCardNumber());
                        }
                        if (StringHelper.isNotNullAndEmpty(orderGoods.getComment())) {
                            orderGoodsVo.put("type", orderGoods.getComment());
                        }

                        if (Objects.equals("zy", order.getHuid())) {
                            if (StringUtils.isNotEmpty(orderGoods.getCardNumber())) {
                                List<MallGoodsCouponCode> couponCodes =
                                                mallGoodsCouponCodeService.selectMallGoodsCouponCodeByCouponCard(
                                                                orderGoodsList.get(0).getCardNumber());
                                if (CollectionUtils.isNotEmpty(couponCodes)) {
                                    if (com.graphai.mall.db.util.StringHelper
                                                    .isNotNullAndEmpty(couponCodes.get(0).getLink())) {
                                        orderGoodsVo.put("link", couponCodes.get(0).getLink());
                                    }
                                }
                            }
                        }


                        orderGoodsVoList.add(orderGoodsVo);
                    }
                    orderVo.put("goodsList", orderGoodsVoList);
                    orderVoList.add(orderVo);
                }
            }
            return ResponseUtil.ok(orderVoList);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw e;
        }

    }


    /**
     *
     * @param data {"mobile":手机号,"orderStatus":订单状态,"page":分页页数,"limit":分页大小，"orderType":equity权益和自营优惠券
     *        zygoods自营}
     * @param request
     * @return
     * @throws Exception
     */
    @PostMapping("/getOrdersHSCard")
    public Object getOrdersHSCard(@LoginUser String userId, @RequestBody String data, HttpServletRequest request)
                    throws Exception {

        Integer orderStatus = Integer.parseInt(JacksonUtil.parseString(data, "orderStatus"));
        String mobile = JacksonUtil.parseString(data, "mobile");
        String orderType = JacksonUtil.parseString(data, "orderType");
        Integer page = Integer.parseInt(JacksonUtil.parseString(data, "page"));
        Integer limit = Integer.parseInt(JacksonUtil.parseString(data, "limit"));

        Map<String, Object> map = new HashMap<String, Object>();

        // 团购总数
        Integer num = 0;
        String groupId = "";

        try {
            if (page <= 0) {
                page = 1;
            }


            if (null == userId) {
                Object result = UserUtils.getTokenUserId(mobile);
                if (!Objects.equals(result, null)) {
                    userId = String.valueOf(result);
                }
            }

            boolean isGroup = false;

            List<MallOrder> orderList = MallOrderService.queryListByHsCard((page - 1) * limit, limit, userId,
                            orderStatus == 100 ? null : orderStatus, orderType);
            Integer zyGoodsCount = MallOrderService.queryCountByHsCard(userId, orderStatus == 100 ? null : orderStatus,
                            "zygoods");
            Integer privilegeCount = MallOrderService.queryCountByHsCard(userId,
                            orderStatus == 100 ? null : orderStatus, "equity");
            List<Map<String, Object>> orderVoList = new ArrayList<>(orderList.size());
            if (CollectionUtils.isNotEmpty(orderList)) {

                for (MallOrder order : orderList) {
                    Map<String, Object> orderVo = new HashMap<>();
                    orderVo.put("id", order.getId());
                    orderVo.put("orderSn", order.getOrderSn());
                    orderVo.put("actualPrice", order.getActualPrice());
                    orderVo.put("orderTime", order.getAddTime());
                    orderVo.put("payTime", order.getPayTime() == null ? null : order.getPayTime());
                    orderVo.put("confirmTime", order.getConfirmTime() == null ? null : order.getConfirmTime());
                    orderVo.put("huid", order.getHuid());
                    orderVo.put("freightPrice", order.getFreightPrice());
                    orderVo.put("orderStatus", order.getOrderStatus());

                    // 团购信息
                    if (Objects.equals(order.getBid(), "36")) {

                            isGroup = true;
                            orderVo.put("Isgroup", isGroup);
                            MallGroupon groupon = groupService.queryByPayOrderId(order.getId());
                            if (groupon != null) {
                                if (Objects.equals("0", groupon.getGrouponId())) {
                                    groupId = groupon.getId();
                                } else {
                                    groupId = groupon.getGrouponId();
                                }
                                try {
                                    MallGrouponRules rules = rulesService.queryByIdV2(groupon.getRulesId());
                                    if (rules == null) {
                                        logger.error("团购规则记录不存在!");
                                    }

                                    if (!Objects.equals(rules.getDiscountMember(), null)) {
                                        num = rules.getDiscountMember();
                                    }
                                    // 获取参与团的人数
                                    Integer joinNum = groupService.queryJoinGrouponCountByGroupId(groupId);
                                    if (num > joinNum) {
                                        orderVo.put("surplusNum", num - joinNum);
                                    } else {
//                                        List<MallGroupVo> groupList = groupService.queryGroupByIdV2(groupId);
//                                        orderVo.put("joinUsers", groupList);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }



                            }

                    }


                    List<MallOrderGoods> orderGoodsList = orderGoodsService.queryByOid(order.getId());

                    // 自营返现商品返回排名队列的名次
                    if (CollectionUtils.isNotEmpty(orderGoodsList)) {
                        if (Objects.equals(order.getBid(), "27")
                                        && !Objects.equals(orderGoodsList.get(0).getCashbackType(), null)) {
                            // if (orderGoodsList.get(0).getSurplusCashback().compareTo(BigDecimal.ZERO) == 1) {
                            Integer cashbackLength = 0;
                            JSONObject goodObject = goodsService.queryGoodsById(orderGoodsList.get(0).getGoodsId());
                            logger.info("商品信息：--" + goodObject);
                            if (!Objects.equals(null, goodObject)) {
                                orderVo.put("shareAccelerate", goodObject.getInteger("shareAccelerate"));
                                cashbackLength = goodObject.getInteger("cashbackLength"); // 总队列长度
                            }



                            MallOrderGoodsVo mallOrderGoodsVo = new MallOrderGoodsVo();
                            mallOrderGoodsVo.setGoodsId(orderGoodsList.get(0).getGoodsId());
                            // mallOrderGoodsVo.setProductId(productId);
                            mallOrderGoodsVo.setSurplusCashback(new BigDecimal(0));
                            mallOrderGoodsVo.setPage(0);
                            mallOrderGoodsVo.setLimit(20);


                            // 成功返现人数
                            int count = orderGoodsService.selectCashbackCount(mallOrderGoodsVo);

                            Integer realLength = null; // 真正队列长度
                            if (!Objects.equals(null, cashbackLength) && cashbackLength >= count) {
                                realLength = cashbackLength - count;
                            } else {
                                orderVo.put("rank", "无");
                            }

                            // 查询用户当前排行(最近的、待返现)

                            // mallOrderGoodsVo.setMobile(mobile);
                            List<MallOrderGoodsVo> mallOrderGoodsVoList =
                                            orderGoodsService.selectCashbackListByGoodsId(mallOrderGoodsVo);
                            if (CollectionUtils.isNotEmpty(mallOrderGoodsVoList)) {

                                for (int i = 0; i < mallOrderGoodsVoList.size(); i++) {
                                    mallOrderGoodsVoList.get(i).setOrderNum(i + 1);
                                }
                                Integer rankInteger = null;
                                for (MallOrderGoodsVo vo : mallOrderGoodsVoList) {
                                    if (vo.getId().equals(orderGoodsList.get(0).getId())) {
                                        rankInteger = vo.getOrderNum();
                                        orderVo.put("rank", rankInteger);
                                    }
                                }

                                if (realLength != null)
                                    if (realLength > 0 && (!Objects.equals(null, rankInteger)
                                                    && rankInteger > realLength)) {
                                        orderVo.put("rank", "无");
                                    }

                                orderVo.put("surplusCashback", orderGoodsList.get(0).getSurplusCashback());
                            } else {
                                // 不在队列里面
                                mallOrderGoodsVo.setSurplusCashback(null);
                                mallOrderGoodsVoList =
                                                orderGoodsService.selectUserCashbackListByGoodsId(mallOrderGoodsVo);
                                if (CollectionUtils.isNotEmpty(mallOrderGoodsVoList)) {
                                    orderVo.put("surplusCashback", orderGoodsList.get(0).getSurplusCashback());
                                }
                            }
                            // }


                        }
                    }


                    if (CollectionUtils.isNotEmpty(orderGoodsList)) {
                        orderVo.put("goodsNumber", orderGoodsList.size());
                        List<Map<String, Object>> orderGoodsVoList = new ArrayList<>(orderGoodsList.size());
                        for (MallOrderGoods orderGoods : orderGoodsList) {
                            Map<String, Object> orderGoodsVo = new HashMap<>();
                            orderGoodsVo.put("id", orderGoods.getId());
                            orderGoodsVo.put("goodsName", orderGoods.getGoodsName());
                            orderGoodsVo.put("number", orderGoods.getNumber());
                            orderGoodsVo.put("picUrl", orderGoods.getPicUrl());
                            orderGoodsVo.put("price", orderGoods.getPrice());
                            if (!Objects.equals(null, orderGoods.getActualPrice())) {
                                orderGoodsVo.put("actualPrice", orderGoods.getActualPrice());
                            }
                            orderGoodsVo.put("specifications", orderGoods.getSpecifications());
                            orderGoodsVo.put("goodsId", orderGoods.getGoodsId());

                            if (StringHelper.isNotNullAndEmpty(orderGoods.getCardPwd())) {
                                orderGoodsVo.put("cardPwd", orderGoods.getCardPwd());
                            }
                            if (StringHelper.isNotNullAndEmpty(orderGoods.getCardDeadline())) {
                                orderGoodsVo.put("cardDeadLine", orderGoods.getCardDeadline());
                            }
                            if (StringHelper.isNotNullAndEmpty(orderGoods.getCardNumber())) {
                                orderGoodsVo.put("cardNumber", orderGoods.getCardNumber());
                            }
                            if (StringHelper.isNotNullAndEmpty(orderGoods.getComment())) {
                                orderGoodsVo.put("type", orderGoods.getComment());
                            }



                            orderGoodsVoList.add(orderGoodsVo);
                        }
                        orderVo.put("goodsList", orderGoodsVoList);
                    }

                    orderVoList.add(orderVo);
                }
            }

            map.put("orderList", orderVoList);
            map.put("zyGoodsCount", zyGoodsCount == null ? 0 : zyGoodsCount);
            map.put("privilegeCount", privilegeCount == null ? 0 : privilegeCount);
            return ResponseUtil.ok(map);
        } catch (Exception e) {
            logger.error("订单查询异常");
            throw e;
        }
    }



    /**
    *
    * @param data {"mobile":手机号,"orderStatus":订单状态,"page":分页页数,"limit":分页大小，"huid":zygoods团购1.0 groupgoods团购2.0}
    * @param request
    * @return
    * @throws Exception
    */
   @PostMapping("/getGroupOrder")
   public Object getGroupOrder(@LoginUser String userId, @RequestBody String data, HttpServletRequest request)
                    throws Exception {
        Integer orderThreeStatus = Integer.parseInt(JacksonUtil.parseString(data, "orderThreeStatus"));
        String mobile = JacksonUtil.parseString(data, "mobile");
        Integer page = Integer.parseInt(JacksonUtil.parseString(data, "page"));
        Integer limit = Integer.parseInt(JacksonUtil.parseString(data, "limit"));
       String orderStatus1 = JacksonUtil.parseString(data, "orderStatus");
       Integer orderStatus = 0;
      if (orderStatus1 != null){
          orderStatus = Integer.parseInt(JacksonUtil.parseString(data, "orderStatus"));
      }
       if (orderStatus == null || orderStatus == 0){
           orderStatus = null;
       }
        List<MallGroupon> groupons = null;
        try {
            if (page <= 0) {
                page = 1;
            }
            if (null == userId) {
                Object result = UserUtils.getTokenUserId(mobile);
                if (!Objects.equals(result, null)) {
                    userId = String.valueOf(result);
                }
            }
            List<MallOrderGroupVo> orderList = MallOrderService.queryListByGroup((page - 1) * limit, limit, userId,
                            orderThreeStatus == 100 ? null : orderThreeStatus, "36", orderStatus);
            List<String> orderIds = new ArrayList<String>();
            if (CollectionUtils.isNotEmpty(orderList)){
                for (MallOrderGroupVo mallOrderGroupVo : orderList){
                    QueryWrapper<MallGoodsPosition> gpWrapper = new QueryWrapper<>();
                    gpWrapper.eq("goods_id",mallOrderGroupVo.getGoodsId());
                    List<MallGoodsPosition> list = mallGoodsPositionService.list(gpWrapper);
                    mallOrderGroupVo.setGoodsPosition(list.get(0).getType());
//                    MallOrderExample mallOrderExample = new MallOrderExample();
                    /*MallOrderExample.Criteria criteria1 = mallOrderExample.createCriteria();
                    criteria1.andParentOrderOnEqualTo(mallOrderGroupVo.getParentOrderOn());
                    criteria1.andOrderPayTypeEqualTo(2);
                    List<MallOrder> mallOrders = mallOrderMapper.selectByExample(mallOrderExample);
                    if (CollectionUtils.isNotEmpty(mallOrders)){
                        for (MallOrder mallOrder : mallOrders){
                            mallOrderGroupVo.setFPOrderId(mallOrder.getId());
                            mallOrderGroupVo.setFPActualPrice(mallOrder.getActualPrice());
                            mallOrderGroupVo.setFPOrderSn(mallOrder.getOrderSn());
                            mallOrderGroupVo.setFPOrderPayType(mallOrder.getOrderPayType());
                            mallOrderGroupVo.setFPOrderStatus((int) mallOrder.getOrderStatus());
                            mallOrderGroupVo.setFPOrderThreeStatus(mallOrder.getOrderThreeStatus());
                            mallOrderGroupVo.setFPExpireTime(mallOrder.getDrawTime());
                            mallOrderGroupVo.setFPFreightPrice(new BigDecimal(0));
                            mallOrderGroupVo.setFPOrderStatusDesc(mallOrder.getOrderStatusDesc());
                        }
                    }*/
                }
            }

            if (CollectionUtils.isNotEmpty(orderList)) {
                for (int i = 0; i < orderList.size(); i++) {
                    if (Objects.equals(198, orderList.get(i).getOrderThreeStatus())) {
                        orderIds.add(orderList.get(i).getParentOrderOn());
                    }
                }
                if (CollectionUtils.isNotEmpty(orderIds)) {
                    groupons = groupService.queryByOrderIds(orderIds);
                }
                for (int j = 0; j < orderList.size(); j++) {
                    if (CollectionUtils.isNotEmpty(groupons)) {
                        for (int i = 0; i < groupons.size(); i++) {
                            if (Objects.equals(orderList.get(j).getParentOrderOn(), groupons.get(i).getOrderId())) {
                                orderList.get(j).setGroupId(groupons.get(i).getId());
                                orderList.get(j).setLeaderGroupId(groupons.get(i).getGrouponId());
                                orderList.get(j).setExpireTime(groupons.get(i).getExpirationTime());
                            }
                        }
                    }
                }
            }
            return ResponseUtil.ok(orderList);
        } catch (Exception e) {
            logger.error("订单查询异常");
            throw e;
        }
    }


    /**
     *
     * @param data {"mobile":手机号,"orderStatus":订单状态,"page":分页页数,"limit":分页大小}
     * @param request
     * @return
     * @throws Exception
     */
    @PostMapping("/getOrdersTotalHSCard")
    public Object getOrdersTotalHSCard(@RequestBody String data, HttpServletRequest request) throws Exception {

//        String sign = JacksonUtil.parseString(data, "sign");
        String mobile = JacksonUtil.parseString(data, "mobile");

        List<Map<String, Object>> map = new ArrayList(2);

//        boolean flag = Md5Utils.verifySignature(string, sign);
//        if (!flag) {
//            return ResponseUtil.fail(403, "无效签名");
//        }

        String userId = "";
        try {

            List<MallUser> users = mallUserService.queryByMobile(mobile);
            if (CollectionUtils.isNotEmpty(users)) {
                if (org.apache.commons.lang3.StringUtils.isNotEmpty(users.get(0).getId())) {
                    userId = users.get(0).getId();
                }
            }

            Integer zyGoodsCount = MallOrderService.queryCountByHsCard(userId, null, "zygoods");
            Integer privilegeCount = MallOrderService.queryCountByHsCard(userId, null, "equity");
            Integer integralCount = mallOrderIntegralService.count(userId);
            Map<String, Object> zyMap = new HashMap<String, Object>();
            zyMap.put("value", zyGoodsCount == null ? 0 : zyGoodsCount);// zyMap.put("zyGoodsCount", zyGoodsCount ==
                                                                        // null ? 0 : zyGoodsCount);
            zyMap.put("name", "zygoods");
            Map<String, Object> privilegeMap = new HashMap<String, Object>();
            privilegeMap.put("value", privilegeCount == null ? 0 : privilegeCount);// privilegeMap.put("privilegeCount",
                                                                                   // privilegeCount == null ? 0 :
                                                                                   // privilegeCount);
            privilegeMap.put("name", "equity");
            Map<String, Object> integralMap = new HashMap<String, Object>();
            integralMap.put("value", integralCount);
            integralMap.put("name", "integral");
            map.add(zyMap);
            map.add(privilegeMap);
            map.add(integralMap);
            return ResponseUtil.ok(map);
        } catch (Exception e) {
            logger.error("订单查询异常");
            throw e;
        }
    }

    /**
     *
     * @param userId
     * @param showBelong 0看全部，1看分销，2看本人，3屏蔽已取消
     * @param showType
     * @param page
     * @param limit
     * @return
     */
    @GetMapping("/fenxiao/list")
    public Object fenxiaoList(@LoginUser String userId, @RequestParam(defaultValue = "0") Integer showBelong,
                    @RequestParam(defaultValue = "0") Integer showType, @RequestParam(defaultValue = "1") Integer page,
                    @RequestParam(defaultValue = "10") Integer limit) {
        return wxOrderService.fenxiaoList(showBelong, userId, showType, page, limit);
    }

    /**
     * 订单详情
     *
     * @param userId 用户ID
     * @param orderId 订单ID
     * @return 订单详情
     */
    @GetMapping("detail")
    public Object detail(@LoginUser String userId, @NotNull String orderId) {
        MallOrder order = mallOrderService.findById(orderId);
        if(order != null){
            return  wxOrderService.detail(userId, orderId);
        }else{
            Map<String,Object> map = new HashMap<>();
            MallPurchaseOrder mallOrder = mallPurchaseOrderService.getById(orderId);
            QueryWrapper<MallPurchaseOrderGoods> mallPurchaseOrderGoodsQueryWrapper = new QueryWrapper<MallPurchaseOrderGoods>();
            mallPurchaseOrderGoodsQueryWrapper.eq("order_id",orderId);
            List<MallPurchaseOrderGoods> mallOrderGoods = mallPurchaseOrderGoodsService.list(mallPurchaseOrderGoodsQueryWrapper);
            MallUser mallUser = mallUserService.queryByUserId(mallOrder.getUserId());
            if (OrderUtil.STATUS_SHIP.equals(mallOrder.getOrderStatus().shortValue())) {
                // ExpressInfo ei = expressService.getExpressInfo(order.getShipChannel(), order.getShipSn());
                Object object = shipService.getLogisticsInfo(mallOrder.getShipSn(), mallOrder.getShipCode());
                map.put("expressInfo", object);
            }

            // 售后信息
            List<Map<String,Object>> goodsMap = new ArrayList<>();
            for (MallPurchaseOrderGoods temp: mallOrderGoods) {
                Map<String,Object> tempMap = new HashMap<>();
                MallAftersale mallAftersale = mallAftersaleService.findByOrderIdAndOrderGoodsId(orderId,temp.getGoodsId());
                tempMap.put("orderGoods",temp);
                tempMap.put("mallAftersale",mallAftersale);
                goodsMap.add(tempMap);
            }
            MallMerchant mallMerchant = mallMerchantService.getById(mallOrder.getFactoryMerchantId());

            map.put("mallUser",mallUser);
            map.put("mallMerchant",mallMerchant);
            map.put("mallOrder",mallOrder);
            map.put("goodsMap",goodsMap);
            return ResponseUtil.ok(map);
        }
    }


    /**
     * 未来卡订单详情
     *
     * @return 订单详情
     */
    @PostMapping("hsDetail")
    public Object hsDetail(@LoginUser String userId, @RequestBody String data, HttpServletRequest request) {

        String id = JacksonUtil.parseString(data, "id");
        String mobile = JacksonUtil.parseString(data, "mobile");
        if (null == userId) {
            Object result = UserUtils.getTokenUserId(mobile);
            if (!Objects.equals(result, null)) {
                userId = String.valueOf(result);
            }
        }

        return wxOrderService.hsDetail(id);
    }


    /**
     * 活动详情
     */
    @GetMapping("activityDetail")
    public Object activityDetail() {
        // 查询活动参数
        Map<String, String> activityPriceMap = mallSystemConfigService.listActivity();
        if (activityPriceMap == null) {
            return ResponseUtil.badArgumentValue("信息错误！");
        }
        Integer total = Integer.valueOf(activityPriceMap.get("Mall_activity_number")); // 总数量

        // 查询活动目前参加成功的人数
        MallOrder mallOrder = new MallOrder();
        mallOrder.setOrderStatus(OrderUtil.STATUS_PAY);
        mallOrder.setBid(AccountStatus.BID_34);
        List<MallOrder> orderList = MallOrderService.findOrderListByCondition(mallOrder);
        int surplus = total - orderList.size();
        activityPriceMap.put("Mall_activity_surplus", String.valueOf(surplus)); // 剩余名额
        return ResponseUtil.ok(activityPriceMap);
    }

    /**
     * 提交订单
     *
     * @param userId 用户ID
     * @param body 订单信息，{ cartId：xxx, addressId: xxx, couponId: xxx, message: xxx, grouponRulesId: xxx,
     *        grouponLinkId: xxx}
     * @return 提交订单操作结果
     */
    @PostMapping("submit")
    public Object submit(@LoginUser String userId, @RequestBody String body, HttpServletRequest request) {
        return wxOrderService.submit(userId, body, IpUtils.getIpAddress(request));
    }

    /**
     * 提交订单(创客)
     *
     * @param userId 用户ID
     * @return 提交订单操作结果
     */
    @PostMapping("submitMaker")
    public Object submitMaker(@LoginUser String userId, @RequestBody String body, HttpServletRequest request) {
        return wxOrderService.submitMaker(userId, body, IpUtils.getIpAddress(request));
    }

    /**
     * 提交订单(壹林话费快充)
     *
     * @param userId 用户ID
     * @return 提交订单操作结果
     */
    @PostMapping("submitYl")
    public Object submitYl(@LoginUser String userId, @RequestBody String body, HttpServletRequest request) {
        return mallOrderYLService.submitOrder(userId, body, IpUtils.getIpAddress(request));
    }

    /**
     * 提交订单(壹林话费快充)
     *
     * @param userId 用户ID
     * @return 提交订单操作结果
     */
    @PostMapping("getPriceInfo")
    public Object getPriceInfo(@LoginUser String userId, @RequestBody String body) {
        return mallOrderYLService.getPriceInfo(userId, body);
    }

    /**
     * 提交订单(口令红包)
     *
     * @param userId 用户ID
     * @return 提交订单操作结果
     */

    @PostMapping("submitPassword")
    public Object submitPassword(@LoginUser String userId, @RequestBody String body, HttpServletRequest request) {
        return wxRedService.submitPassword(userId, body, IpUtils.getIpAddress(request));
    }

    /**
     * 提交订单(达人)
     *
     * @param userId 用户ID
     * @return 提交订单操作结果
     */
    @PostMapping("submitTalent")
    public Object submitTalent(@LoginUser String userId, @RequestBody String body, HttpServletRequest request) {
        return wxOrderService.submitTalent(userId, body, IpUtils.getIpAddress(request));
    }

    /**
     * 支付订单(达人-余额支付)
     *
     * @param userId 用户ID
     * @return 提交订单操作结果
     */
    @PostMapping("exchangePayTalent")
    public Object exchangePayTalent(@LoginUser String userId, @RequestBody String body, HttpServletRequest request)
                    throws Exception {
        return wxOrderService.exchangePayTalent(userId, body, IpUtils.getIpAddress(request));
    }


    /**
     * 提交订单
     *
     * @param body 订单信息，{ cartId：xxx, addressId: xxx, couponId: xxx, message: xxx, grouponRulesId: xxx,
     *        grouponLinkId: xxx}
     * @return 提交订单操作结果
     */
    @PostMapping("hsCardSubmit")
//    @LoginUser String userId,
    public Object hsCardSubmit(@LoginUser String userId,@RequestBody String body, HttpServletRequest request) {
        try {
            return wxOrderService.hsCardSubmit(userId, body, IpUtils.getIpAddress(request));
        }catch (Exception e){
            return ResponseUtil.fail(500, "订单提交有误!");
        }
    }


    /**
     * 提交订单(注册购买 实体卡)
     *
     * @param userId 用户ID
     * @param body 订单信息，{ addressId:地址id, message:订单备注, tpid:分享人id, submitClient:Customer }
     * @param request
     * @return
     */
    @PostMapping("buyCardSubmit")
    public Object buyCardSubmit(@LoginUser String userId, @RequestBody String body, HttpServletRequest request) {
        return wxOrderService.buyCardSubmit(userId, body, IpUtils.getIpAddress(request));
    }

    /**
     * 参见活动支付
     *
     * @param body 订单信息，{ name:用户姓名, mobile:联系电话 ,userId:用户id ,submitClient:Customer}
     * @param request
     * @return
     */
    @PostMapping("activitySubmit")
    public Object activitySubmit(@RequestBody String body, HttpServletRequest request) {
        return wxOrderService.activitySubmit(body, IpUtils.getIpAddress(request));
    }


    /**
     * 取消订单
     *
     * @param userId 用户ID
     * @param body 订单信息，{ orderId：xxx }
     * @return 取消订单操作结果
     */
    @PostMapping("cancel")
    public Object cancel(@LoginUser String userId, @RequestBody String body) {
        return wxOrderService.cancel(userId, body);
    }



    /**
     * 未莱卡自营商城取消订单
     *
     * @param data 订单信息，{ orderId：xxx }
     * @return 取消订单操作结果
     */
    @PostMapping("wlkCancel")
    public Object wlkCancel(@LoginUser String userId, @RequestBody String data) {
        String mobile = JacksonUtil.parseString(data, "mobile");

        if (null == userId) {
            Object result = UserUtils.getTokenUserId(mobile);
            if (!Objects.equals(result, null)) {
                userId = String.valueOf(result);
            }
        }

        return wxOrderService.cancel(userId, data);
    }

    /**
     * 付款订单的预支付会话标识
     *
     * @param userId 用户ID
     * @param body 订单信息，{ orderId：xxx, channel: xxx,openId: xxx }
     * @return 支付订单ID
     */
    @PostMapping("prepay")
    public Object prepay(@LoginUser String userId, @RequestBody String body, HttpServletRequest request) {

        String orderId = JacksonUtil.parseString(body, "orderId");
        MallOrder mallOrder = MallOrderService.findById(orderId);
        if (mallOrder!= null && "groupgoods".equals(mallOrder.getHuid())){
            return wxOrderService.groupPrepay(null, body, request);
        }
        return wxOrderService.prepay(userId, body, request);
    }

    /**
     * 付款订单的预支付会话标识
     *
     * @param body 订单信息，{ orderId：xxx }
     * @return 支付订单ID
     */
    @PostMapping("hsPrepay")
    public Object hsPrepay(@LoginUser String userId, @RequestBody String body, HttpServletRequest request) {
        try {
            if (body == null) {
                return ResponseUtil.badArgument();
            }

            // 手机号
            String mobile = JacksonUtil.parseString(body, "mobile");


            if (null == userId) {
                Object result = UserUtils.getTokenUserId(mobile);
                if (!Objects.equals(result, null)) {
                    userId = String.valueOf(result);
                }
            }
            String orderId = JacksonUtil.parseString(body, "orderId");
            MallOrder mallOrder = MallOrderService.findById(orderId);
            if (mallOrder!= null && "groupgoods".equals(mallOrder.getHuid())){
                return wxOrderService.groupPrepay(userId, body, request);
            }
            return wxOrderService.prepay(userId, body, request);
        }catch (Exception e){
            return ResponseUtil.fail(ORDER_PAY_FAIL, "订单不能支付");
        }
    }



    /**
     * 订单申请退款
     *
     * @param userId 用户ID
     * @param body 订单信息，{ orderId：xxx }
     * @return 订单退款操作结果
     */
    @PostMapping("refund")
    public Object refund(@LoginUser String userId, @RequestBody String body) {
        try {
            return wxOrderService.refund(userId, body);
        } catch (Exception e) {
            logger.error("订单退单异常 :" + e.getMessage(), e);
        }

        return ResponseUtil.fail();
    }

    /**
     * 手动退订单退账
     *
     * @param body
     * @return
     */
    @PostMapping("mrefund")
    public Object mrefund(@RequestBody String body) {
        try {
            return wxOrderService.manualRefund(body);
        } catch (Exception e) {
            logger.error("订单退单异常 :" + e.getMessage(), e);
        }
        return ResponseUtil.fail();
    }

    /**
     * 确认收货
     *
     * @param body 订单信息，{ orderId：xxx }
     * @return 订单操作结果
     */
    @PostMapping("wlkConfirm")
    public Object wlkConfirm(@LoginUser String userId, @RequestBody String body) {
        if (body == null) {
            return ResponseUtil.badArgument();
        }
        // 手机号
        String mobile = JacksonUtil.parseString(body, "mobile");

        if (null == userId) {
            Object result = UserUtils.getTokenUserId(mobile);
            if (!Objects.equals(result, null)) {
                userId = String.valueOf(result);
            }
        }
        return wxOrderService.confirm(userId, body);
    }


    /**
     * 确认收货
     *
     * @param userId 用户ID
     * @param body 订单信息，{ orderId：xxx }
     * @return 订单操作结果
     */
    @PostMapping("confirm")
    public Object confirm(@LoginUser String userId, @RequestBody String body) {
        return wxOrderService.confirm(userId, body);
    }

    /**
     * 删除订单
     *
     * @param userId 用户ID
     * @param body 订单信息，{ orderId：xxx }
     * @return 订单操作结果
     */
    @PostMapping("delete")
    public Object delete(@LoginUser String userId, @RequestBody String body) {
        return wxOrderService.delete(userId, body);
    }

    /**
     * 待评价订单商品信息
     *
     * @param userId 用户ID
     * @param orderId 订单ID
     * @param goodsId 商品ID
     * @return 待评价订单商品信息
     */
    @GetMapping("goods")
    public Object goods(@LoginUser String userId, @NotNull String orderId, @NotNull String goodsId) {
        return wxOrderService.goods(userId, orderId, goodsId);
    }

    /**
     * 评价订单商品
     *
     * @param userId 用户ID
     * @param body 订单信息，{ orderId：xxx }
     * @return 订单操作结果
     */
    @PostMapping("comment")
    public Object comment(@LoginUser String userId, @RequestBody String body) {
        return wxOrderService.comment(userId, body);
    }

    @PostMapping("supplement-sheet")
    public Object supplementSheet(@RequestBody String body) {
        wxOrderService.supplementSheet(body);
        return ResponseUtil.ok("计算成功");
    }

    /**
     * 查询队列返现列表
     */
    @GetMapping("/cashbackList")
    public Object cashbackList(@RequestParam(required = false) String goodsId,
                    @RequestParam(required = false) String productId, @RequestParam(required = false) String mobile,
                    @RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit) {
        if (StringUtils.isEmpty(goodsId) && StringUtils.isEmpty(productId)) {
            return ResponseUtil.badArgumentValue("goodsId、productId不能同时为空！");
        }

        Map<String, Object> data = new HashMap<>();
        try {
            List<MallOrderGoodsVo> mallOrderGoodsVoList = null;

            MallOrderGoodsVo mallOrderGoodsVo = new MallOrderGoodsVo();
            mallOrderGoodsVo.setGoodsId(goodsId);
            // mallOrderGoodsVo.setProductId(productId);
            mallOrderGoodsVo.setSurplusCashback(new BigDecimal(0));
            mallOrderGoodsVo.setPage((page - 1) * limit);
            mallOrderGoodsVo.setLimit(limit);
            // 查询用户当前排行(最近的、待返现)
            MallOrderGoodsVo userRankInfo = null;
            if (!StringUtils.isEmpty(mobile)) {
                mallOrderGoodsVo.setMobile(mobile);
                mallOrderGoodsVoList = orderGoodsService.selectUserCashbackListByGoodsId(mallOrderGoodsVo);
                for (int i = 0; i < mallOrderGoodsVoList.size(); i++) {
                    mallOrderGoodsVoList.get(i).setOrderNum(i + 1);
                }
                if (CollectionUtils.isNotEmpty(mallOrderGoodsVoList)) {
                    userRankInfo = mallOrderGoodsVoList.get(0);
                } else {
                    // 不在队列里面
                    mallOrderGoodsVo.setSurplusCashback(null);
                    mallOrderGoodsVoList = orderGoodsService.selectUserCashbackListByGoodsId(mallOrderGoodsVo);
                    if (CollectionUtils.isNotEmpty(mallOrderGoodsVoList)) {
                        for (int i = 0; i < mallOrderGoodsVoList.size(); i++) {
                            mallOrderGoodsVoList.get(i).setOrderNum(i + 1);
                        }
                        userRankInfo = mallOrderGoodsVoList.get(0);
                    }

                }
            }

            JSONObject goodObject = null;
            try {
                goodObject = goodsService.queryGoodsById(goodsId);
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            logger.info("商品信息：--" + goodObject);
            if (Objects.equals(null, goodObject)) {
                logger.info("商品已下架或被删除!--------------");
            }


            // 成功返现人数
            int count = orderGoodsService.selectCashbackCount(mallOrderGoodsVo);
            // 查询总排行
            // mallOrderGoodsVo.setPage((page - 1) * limit);
            // mallOrderGoodsVo.setLimit(limit);
            List<MallOrderGoodsVo> list = orderGoodsService.selectCashbackListByGoodsId(mallOrderGoodsVo);
            boolean flag = false;
            if (CollectionUtils.isNotEmpty(list)) {



                Integer cashbackLength = goodObject.getInteger("cashbackLength"); // 总队列长度

                Integer realLength = null; // 真正队列长度
                if (cashbackLength >= count) {
                    realLength = cashbackLength - count;
                }

                // 队列若有长度限制则截取
                if (com.graphai.mall.db.util.StringHelper.isNotNullAndEmpty(realLength) && list.size() > realLength) {
                    list = list.subList(0, realLength);
                }



                for (int i = 0; i < list.size(); i++) {
                    List<FcAccountPrechargeBill> fcAccountPrechargeBills =
                                    fcAccountPrechargeBillService.selectByCustomerId(list.get(i).getUserId(),
                                                    list.get(i).getGoodsId(), AccountStatus.RECHARTYPE_SHARE);
                    if (CollectionUtils.isNotEmpty(fcAccountPrechargeBills)) {
                        flag = true;
                    }
                    list.get(i).setShare(flag);
                }

                for (int i = 0; i < list.size(); i++) {
                    list.get(i).setOrderNum(i + 1);
                }
            }
            long total = PageInfo.of(list).getTotal();
            list = SensitiveUtil.desCopyCollection(list);



            data.put("total", total); // 总条数
            data.put("items", list); // 列表
            data.put("userRankInfo", userRankInfo); // 当前用户最近的一条排行
            data.put("count", count); // 成功免单人数量
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseUtil.ok(data);
    }



    /**
     * 助力分享页面
     */
    @GetMapping("/shareProfit")
    public Object shareProfit(@RequestParam(required = true) String orderId) {

        Map<String, Object> data = new HashMap<String, Object>();
        List<Map<String, Object>> preList = null;
        boolean isExpire = false;
        try {
            Map<String, Object> orderMap = MallOrderService.findShareOrderByOrderId(orderId);
            if (orderMap.isEmpty()) {
                return ResponseUtil.fail(500, "助力订单信息有误!");
            }



            if (!Objects.equals(null, String.valueOf(orderMap.get("goodsId")))) {
                JSONObject goodObject = goodsService.queryGoodsById(String.valueOf(orderMap.get("goodsId")));
                logger.info("商品信息：--" + goodObject);
                if (Objects.equals(null, goodObject)) {
                    return ResponseUtil.fail(507, "商品信息有误!");
                }


                // 返现有效期，若过期则不进队列
                Date cashbackEffective = goodObject.getDate("cashbackEffective"); // 队列开始有效期
                Date cashbackEndTime = goodObject.getDate("cashbackEndTime"); // 队列开始有效期
                // 计算下单时间加上有效天数的日期时间
                if ((ObjectUtils.allNotNull(cashbackEffective, cashbackEndTime)
                                && !DateUtilTwo.isMoreThanCurrentDate(cashbackEffective)
                                && DateUtilTwo.isMoreThanCurrentDate(cashbackEndTime))) {
                    isExpire = true;
                }
                data.put("isExpire", isExpire);


                // 助力队列列表
                preList = fcAccountPrechargeBillService.selectZygoodsPrechargesList(
                                String.valueOf(orderMap.get("goodsId")), String.valueOf(orderMap.get("userId")),
                                AccountStatus.RECHARTYPE_PROFIT);
                boolean flag = false;
                if (CollectionUtils.isNotEmpty(preList)) {
                    for (int i = 0; i < preList.size(); i++) {
                        List<FcAccountPrechargeBill> fcAccountPrechargeBills = fcAccountPrechargeBillService
                                        .selectByCustomerId(String.valueOf(preList.get(i).get("userId")),
                                                        String.valueOf(orderMap.get("goodsId")),
                                                        AccountStatus.RECHARTYPE_SHARE);
                        if (CollectionUtils.isNotEmpty(fcAccountPrechargeBills)) {
                            flag = true;
                        }
                        preList.get(i).put("isShare", flag);
                    }
                }

            }



            data.put("orderInfo", orderMap);
            data.put("prechargeBill", preList);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseUtil.ok(data);
    }



//    // todo 测试方法------------
//    // todo 测试方法------------
//    @PostMapping("testPayHsNotify")
//    public Object testPayHsNotify(@RequestParam String param) {
//        return wxOrderService.testPayHsNotify(param);
//    }

//    @PostMapping("/cancelOrder")
//    public Object cancelOrder(@LoginUser String userId, @RequestBody String body){
//        try {
//
//            Object object = MallOrderService.cancelOrder(body);
//            return ResponseUtil.ok("取消订单成功");
//        }catch (Exception e){
//         return ResponseUtil.fail(-1,e.toString());
//        }
//    }

    /**
     * 退款申请审核
     * @param userId
     * @param body
     * @return
     */
    @PostMapping("/refundApplication")
    //@LoginUser String userId,
    public Object refundApplication(@LoginUser String userId, @RequestBody String body){
        try {
//            Object object = MallOrderService.cancelOrder(body);
            return MallOrderService.refundApplication(body);
//            return ResponseUtil.ok();
        }catch (Exception e){
            return ResponseUtil.fail(-1,e.toString());
        }
    }

    /**
     *  退款审核
     * @param userId
     * @param body
     * @return
     */
    @PostMapping("/refundReview")
    //@LoginUser String userId,
    public Object refundReview(@LoginUser String userId, @RequestBody String body){
        try {
            String orderId = JacksonUtil.parseString(body, "orderId");

            String rulesId = JacksonUtil.parseString(body, "rulesId");
            String isV = JacksonUtil.parseString(body, "isV");
            MallOrderService.cancelOrder(orderId,rulesId,isV);
            return ResponseUtil.ok("审核通过");
        }catch (Exception e){
            return ResponseUtil.fail(-1,e.toString());
        }
    }

    /**
     * 获取未处理订单
     *
     * @param userId
     * @return
     */
    @GetMapping("/getAwaitOrderList")
    public Object getAwaitOrderList(@LoginUser String userId) {
        Map<String, Object> map = new HashMap<>();
        List<MallGroupVo> mallGroupVoList = grouponService.queryGroupByUser(userId, GroupEnum.ISOPEN_1.getIntCode(), false);
        map.put("groupOrder", mallGroupVoList);
        return ResponseUtil.ok(map);
    }

    /**
     * 获取订单-h5
     * @param orderId
     * @return
     */
    @GetMapping("getOrderById")
    public Object getOrderById(String orderId){
        MallOrder mallOrder = MallOrderService.findById(orderId);
        if(mallOrder == null){
            return ResponseUtil.fail("未找到订单");
        }

        Map<String,Object> map = new HashMap<>();
        map.put("status",mallOrder.getOrderStatus());
        map.put("price",mallOrder.getActualPrice());
        map.put("bid",mallOrder.getBid());

        return  ResponseUtil.ok(map);
    }
}
