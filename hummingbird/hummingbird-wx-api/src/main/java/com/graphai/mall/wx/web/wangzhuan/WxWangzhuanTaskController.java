package com.graphai.mall.wx.web.wangzhuan;

import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.ip.IpUtils;
import com.graphai.commons.util.lang.StringUtils;
import com.graphai.commons.util.obj.ObjAndMapUtil;
import com.graphai.commons.util.obj.ObjectUtils;
import com.graphai.id.GraphaiIdGenerator;
import com.graphai.mall.admin.vo.WangzhuanTaskVo;
import com.graphai.mall.db.constant.wangzhuan.WangzhuanTaskEnum;
import com.graphai.mall.db.dao.MallUserMapper;
import com.graphai.mall.db.domain.MallUser;
import com.graphai.mall.db.domain.WangzhuanTask;
import com.graphai.mall.db.domain.WangzhuanTaskStep;
import com.graphai.mall.db.domain.WangzhuanTaskStepWithBLOBs;
import com.graphai.mall.db.service.sysconfig.MallSystemConfigService;
import com.graphai.mall.db.service.wangzhuan.WangzhuanTaskService;
import com.graphai.mall.db.service.wangzhuan.WangzhuanTaskStepService;
import com.graphai.mall.db.service.wangzhuan.WangzhuanUserTaskService;
import com.graphai.mall.db.service.wangzhuan.WxWangzhuanTaskRedisService;
import com.graphai.mall.db.constant.CommonConstant;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.mall.wx.dto.WangzhuanTaskDto;
import com.graphai.mall.wx.web.home.WxHomeController;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.*;

/**
 * (网赚)任务Controller
 *
 * @author ruoyi
 * @date 2020-04-08
 */
@RestController
@RequestMapping("/wx/task")
@Validated
public class WxWangzhuanTaskController {
    private final Log logger = LogFactory.getLog(WxWangzhuanTaskController.class);

    @Autowired
    private WangzhuanTaskService wangzhuanTaskService;
    @Autowired
    private WangzhuanTaskStepService wangzhuanTaskStepService;
    @Autowired
    private WangzhuanUserTaskService wangzhuanUserTaskService;
    @Autowired
    private WxWangzhuanTaskRedisService wxWangzhuanTaskRedisService;
    @Resource
    private MallSystemConfigService mallSystemConfigService;
    @Resource
    private WxHomeController wxHomeController;
    @Resource
    private MallUserMapper mallUserMapper;


    /**
     * 1、查询(网赚)任务列表-公益任务
     */
    @GetMapping("/list")
    public Object list(@RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit, String cityCode, @LoginUser String userId) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        logger.info("进来了List ----------1 ");
        List<WangzhuanTaskVo> list = wangzhuanTaskService.selectCustomWangzhuanTaskList(userId,page,limit,cityCode);
        long total = PageInfo.of(list).getTotal();
        Map<String, Object> data = new HashMap<>();
        data.put("total", total);
        data.put("items", list);
        return ResponseUtil.ok(data);
    }

    /**
     * 2、网赚任务
     */
    @GetMapping("/listWZ")
    public Object listWZ(@RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit, String cityCode, @LoginUser String userId,String taskCategory, @RequestParam(defaultValue = "orderby") String orderby) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        List<WangzhuanTaskVo> list = wangzhuanTaskService.selectWZTaskList(userId,page,limit,cityCode,taskCategory,orderby);
        long total = PageInfo.of(list).getTotal();
        Map<String, Object> data = new HashMap<>();
        data.put("total", total);
        data.put("items", list);
        return ResponseUtil.ok(data);
    }

    /**
     * 新增保存(网赚)任务
     */
    @PostMapping("/add")
    public Object addSave(@RequestBody WangzhuanTaskDto wangzhuanTaskDto) {
        WangzhuanTask wangzhuanTask = wangzhuanTaskDto.getWangzhuanTask();
        Object error = validateTask(wangzhuanTask);
        if (error != null) {
            return error;
        }
        wangzhuanTask.setId(GraphaiIdGenerator.nextId("WangzhuanTask"));
        wangzhuanTask.setDeleted(false);    //是否删除
        wangzhuanTask.setTstatus(CommonConstant.TASK_ONSHELF_PRE);  //状态( ST100 未上架、ST101 已上架、ST102 预上架)
        wangzhuanTaskService.insertWangzhuanTask(wangzhuanTask);

        //添加 网赚任务步骤
        List<WangzhuanTaskStepWithBLOBs> taskStepList = wangzhuanTaskDto.getWangzhuanTaskStepList();
        if (taskStepList.size() > 0) {
            for (WangzhuanTaskStepWithBLOBs wangzhuanTaskStep : taskStepList) {
                Object stepError = validateTaskStep(wangzhuanTaskStep);
                if (stepError != null) {
                    return error;
                }
                wangzhuanTaskStep.setTaskId(wangzhuanTask.getId());
                wangzhuanTaskStep.setDeleted(false);
                wangzhuanTaskStepService.insertWangzhuanTaskStep(wangzhuanTaskStep);
            }
        }

        return ResponseUtil.ok();
    }

    /**
     * 发布任务
     * @param userId 发布者id
     * @param data 发布参数
     * @return Object
     */
    @PostMapping("/createTask")
    public Object addSave(@LoginUser String userId, @RequestBody String data, HttpServletRequest request) {

        try {
            // 参数验证
            Object error = wangzhuanTaskService.validatereateTask(data);
            if (error != null) {
                return error;
            }
            JSONObject jsonObject = JSONUtil.parseObj(data);
            WangzhuanTask wangzhuanTask = new WangzhuanTask();
            wangzhuanTask.setAmount(jsonObject.getBigDecimal("amount"));//任务单价
            wangzhuanTask.setTaskType(jsonObject.getStr("taskType"));//投放类型
            wangzhuanTask.setRemarks(jsonObject.getStr("remark"));
            wangzhuanTask.setTstatus(WangzhuanTaskEnum.TSTATUS_ST105.getCode()); //任务状态
            wangzhuanTask.setTaskName(jsonObject.getStr("taskName"));//任务名称
            wangzhuanTask.setSubName(jsonObject.getStr("subName"));//副标题
            wangzhuanTask.setDeleted(false);//是否禁用
            wangzhuanTask.setTaskCategory(jsonObject.getStr("taskCategory"));//任务类型
            String imgUrl = jsonObject.getStr("imageUrl");
            if(StringUtils.isBlank(imgUrl)) {
                Object obj = wxHomeController.getIndexBarrage("27", request, "","");
                if (ObjectUtils.isNotNull(obj)) {
                    Map<String, String> map = new HashMap<String, String>();
                    List<Map> list = ObjAndMapUtil.castList(((HashMap) obj).get("data"), Map.class);
                    if (CollectionUtils.isNotEmpty(list)) {
                        for (int i = 0; list.size() > i; i++) {
                            map.put(list.get(i).get("title").toString(), list.get(i).get("picUrl").toString());
                        }
                    }
                    if (WangzhuanTaskEnum.TASK_CATEGORY_T01.getCode().equals(jsonObject.getStr("taskCategory"))) {
                        wangzhuanTask.setImageUrl(map.get("GY"));
                    } else if (WangzhuanTaskEnum.TASK_CATEGORY_T02.getCode().equals(jsonObject.getStr("taskCategory"))) {
                        wangzhuanTask.setImageUrl(map.get("SPH"));
                    } else if (WangzhuanTaskEnum.TASK_CATEGORY_T03.getCode().equals(jsonObject.getStr("taskCategory"))) {
                        wangzhuanTask.setImageUrl(map.get("DY"));
                    } else if (WangzhuanTaskEnum.TASK_CATEGORY_T04.getCode().equals(jsonObject.getStr("taskCategory"))) {
                        MallUser mallUser = mallUserMapper.selectByPrimaryKey(userId);
                        wangzhuanTask.setImageUrl(mallUser.getAvatar());
                    }
                }
            }else{
                wangzhuanTask.setImageUrl(imgUrl);
            }
            wangzhuanTask.setUnit(WangzhuanTaskEnum.UNIT_US102.getCode());//完成时间单位
            wangzhuanTask.setDuration(jsonObject.getStr("duration"));//任务完成时长
            wangzhuanTask.setAuditDuration(jsonObject.getStr("auditDuration")); // 审核时长，单位：小时
            wangzhuanTask.setStockNumber(Integer.parseInt(jsonObject.get("stockNumber").toString()));//任务数量-这个是会减少的。
            wangzhuanTask.setSumStockNumber(Integer.parseInt(jsonObject.get("stockNumber").toString()));//任务数量-这个是一直不变化的。
            String id = GraphaiIdGenerator.nextId("WangzhuanTask");
            wangzhuanTask.setId(id);//id
            wangzhuanTask.setUserId(userId); // 发布者用户ID
            // 省市区
            JSONArray regin = new JSONArray(jsonObject.getStr("region"));
            if (CollectionUtils.isNotEmpty(regin)) {
                String provinceCode="";
                String cityCode="";
                String areaCode="";
                String province="";
                String city="";
                String county="";
                for(int i = 0;regin.size()>i;i++){
                    JSONObject regionObject = JSONUtil.parseObj(regin.get(i));
                    if(ObjectUtils.isNotNull(regionObject.get("provinceCode"))){
                        provinceCode= provinceCode.concat(com.graphai.commons.util.lang.StringUtils.isNotBlank(provinceCode)? ","+regionObject.get("provinceCode").toString() : regionObject.get("provinceCode").toString());
                    }
                    if(ObjectUtils.isNotNull(regionObject.get("cityCode"))) {
                        cityCode = cityCode.concat(com.graphai.commons.util.lang.StringUtils.isNotBlank(cityCode) ? "," + regionObject.get("cityCode").toString() : regionObject.get("cityCode").toString());
                    }
                    if(ObjectUtils.isNotNull(regionObject.get("province"))) {
                        province = province.concat(com.graphai.commons.util.lang.StringUtils.isNotBlank(province) ? "," + regionObject.get("province").toString() : regionObject.get("province").toString());
                    }
                    if(ObjectUtils.isNotNull(regionObject.get("city"))) {
                        city = city.concat(com.graphai.commons.util.lang.StringUtils.isNotBlank(city) ? "," + regionObject.get("city").toString() : regionObject.get("city").toString());
                    }
                    if(ObjectUtils.isNotNull(regionObject.get("areaCode"))) {
                        areaCode = areaCode.concat(com.graphai.commons.util.lang.StringUtils.isNotBlank(areaCode) ? "," + regionObject.get("areaCode").toString() : regionObject.get("areaCode").toString());
                    }
                    if(ObjectUtils.isNotNull(regionObject.get("county"))) {
                        county = county.concat(com.graphai.commons.util.lang.StringUtils.isNotBlank(county) ? "," + regionObject.get("county").toString() : regionObject.get("county").toString());
                    }
                }
                wangzhuanTask.setProvinceCode(provinceCode);
                wangzhuanTask.setCityCode(cityCode);
                wangzhuanTask.setAreaCode(areaCode);
                wangzhuanTask.setProvince(province);
                wangzhuanTask.setCity(city);
                wangzhuanTask.setCounty(county);
            }
            BigDecimal amount = jsonObject.getBigDecimal("amount"); // 任务单价
            String stockNumber = jsonObject.getStr("stockNumber");  // 任务数量
            wangzhuanTask = wangzhuanTaskService.chooseAmount(wangzhuanTask,amount,stockNumber);
            // 业务处理
            return wangzhuanTaskService.addSave(wangzhuanTask,jsonObject, IpUtils.getIpAddress(request));

        } catch (Exception ex) {
            logger.info("发布任务出现异常"+ex.getMessage() , ex);
            return ResponseUtil.badArgument();
        }
    }



    /**
     * 修改(网赚)任务
     */
    @GetMapping("/edit/{id}")
    public Object edit(@PathVariable("id") String id) {
        //任务
        WangzhuanTask wangzhuanTask = wangzhuanTaskService.selectWangzhuanTaskById(id);
        //任务步骤
        WangzhuanTaskStep wangzhuanTaskStep = new WangzhuanTaskStep();
        wangzhuanTaskStep.setTaskId(id);
        wangzhuanTaskStep.setDeleted(false);
        List<WangzhuanTaskStepWithBLOBs> wangzhuanTaskStepList = wangzhuanTaskStepService.selectWangzhuanTaskStepList(wangzhuanTaskStep);

        Map<String, Object> data = new HashMap<>();
        WangzhuanTaskDto wangzhuanTaskDto = new WangzhuanTaskDto();
        wangzhuanTaskDto.setWangzhuanTask(wangzhuanTask);
        wangzhuanTaskDto.setWangzhuanTaskStepList(wangzhuanTaskStepList);

        data.put("items", wangzhuanTaskDto);
        return ResponseUtil.ok(data);
    }

    /**
     * 修改保存(网赚)任务
     */
    @PostMapping("/edit")
    public Object editSave(@RequestBody WangzhuanTaskDto wangzhuanTaskDto) {
        WangzhuanTask wangzhuanTask = wangzhuanTaskDto.getWangzhuanTask();
        Object error = validateTask(wangzhuanTask);
        if (error != null) {
            return error;
        }
        if (wangzhuanTaskService.updateWangzhuanTask(wangzhuanTask) == 0) {
            return ResponseUtil.updatedDataFailed();
        }
        //修改任务步骤
        List<WangzhuanTaskStepWithBLOBs> taskStepList = wangzhuanTaskDto.getWangzhuanTaskStepList();
        if (taskStepList.size() > 0) {
            //todo 删除旧的任务步骤(逻辑删除)
            //wangzhuanTaskStepService.deleteWangzhuanTaskStepById(wangzhuanTask.getId());
            WangzhuanTaskStepWithBLOBs taskStep = new WangzhuanTaskStepWithBLOBs();
            taskStep.setTaskId(wangzhuanTask.getId());
            taskStep.setDeleted(true);
            wangzhuanTaskStepService.updateWangzhuanTaskStep(taskStep);
            for (WangzhuanTaskStepWithBLOBs wangzhuanTaskStep : taskStepList) {
                Object stepError = validateTaskStep(wangzhuanTaskStep);
                if (stepError != null) {
                    return error;
                }
                wangzhuanTaskStep.setTaskId(wangzhuanTask.getId());
                wangzhuanTaskStep.setDeleted(false);
                wangzhuanTaskStepService.insertWangzhuanTaskStep(wangzhuanTaskStep);
            }
        }

        return ResponseUtil.ok();
    }



    /**
     * 删除(网赚)任务
     */
    @PostMapping("/remove/{id}")
    public Object remove(@PathVariable("id") String id) {
        WangzhuanTask wangzhuanTask = new WangzhuanTask();
        wangzhuanTask.setId(id);
        wangzhuanTask.setDeleted(true);
        wangzhuanTaskService.updateWangzhuanTask(wangzhuanTask);

        WangzhuanTaskStepWithBLOBs wangzhuanTaskStep = new WangzhuanTaskStepWithBLOBs();
        wangzhuanTaskStep.setTaskId(id);
        wangzhuanTaskStep.setDeleted(true);
        wangzhuanTaskStepService.updateWangzhuanTaskStep(wangzhuanTaskStep);

        return ResponseUtil.ok();
    }

    /**
     * 上、下架(网赚)任务
     */
    @PostMapping("/isOnShelf")
    public Object isOnShelf(@RequestParam("id") String id, @RequestParam("status") String status) {
        WangzhuanTask wangzhuanTask = new WangzhuanTask();
        wangzhuanTask.setId(id);
        wangzhuanTask.setTstatus(status);
        wangzhuanTaskService.updateWangzhuanTask(wangzhuanTask);
        return ResponseUtil.ok();
    }





    /**
     * 校验数据(网赚任务)
     */
    private Object validateTask(WangzhuanTask wangzhuanTask) {
        if (StringUtils.isBlank(wangzhuanTask.getTaskName())) {
            return ResponseUtil.badArgument();
        }
//        if (StringUtils.isEmpty(wangzhuanTask.getStockNumber())) {
//            return ResponseUtil.badArgument();
//        }
//        if (StringUtils.isEmpty(wangzhuanTask.getSubName())) {
//            return ResponseUtil.badArgument();
//        }
//
//        if (StringUtils.isEmpty(wangzhuanTask.getAndroidDownloadUrl())) {
//            return ResponseUtil.badArgument();
//        }
//        if (wangzhuanTask.getAmount() == null) {
//            return ResponseUtil.badArgument();
//        }
        return null;
    }

    /**
     * 校验数据(网赚任务步骤)
     */
    private Object validateTaskStep(WangzhuanTaskStep wangzhuanTaskStep) {
        if (StringUtils.isEmpty(wangzhuanTaskStep.getStepTitle())) {
            return ResponseUtil.badArgument();
        }
//        if (StringUtils.isEmpty(wangzhuanTaskStep.getStepSubTitle())) {
//            return ResponseUtil.badArgument();
//        }
//        if (StringUtils.isEmpty(wangzhuanTaskStep.getRemarks())) {
//            return ResponseUtil.badArgument();
//        }
//
//        if (StringUtils.isEmpty(wangzhuanTaskStep.getSetpType())) {
//            return ResponseUtil.badArgument();
//        }

        return null;
    }

}
