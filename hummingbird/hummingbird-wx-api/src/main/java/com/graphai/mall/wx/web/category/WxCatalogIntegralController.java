package com.graphai.mall.wx.web.category;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Header;
import org.apache.http.message.BasicHeader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

//import cn.hutool.core.util.ObjectUtil;
//import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.graphai.commons.constant.MallConstant;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.json.JacksonUtils;
import com.graphai.mall.db.domain.MallCategory;
import com.graphai.mall.db.service.category.MallCategoryService;
import com.graphai.open.api.getway.HttpRequestUtils;
import com.graphai.properties.PlatFormServerproperty;

/**
 * 类目服务-积分商城
 * @author Qxian
 * @date 2020-10-15
 * @version V0.0.1
 */
@RestController
@RequestMapping("/wx/catalog/integral")
@Validated
public class WxCatalogIntegralController {
    private final Log logger = LogFactory.getLog(WxCatalogController.class);

    @Autowired
    private MallCategoryService categoryService;
    @Autowired
    private PlatFormServerproperty platFormServerproperty;

    /**
     * 分类详情
     *
     * @param id   分类类目ID。
     *             如果分类类目ID是空，则选择第一个分类类目。
     *             需要注意，这里分类类目是一级类目
     * @return 分类详情
     */
    @GetMapping("index")
    public Object index(String id,@RequestParam(value="industryId",defaultValue = "0") String industryId) throws Exception {

        /*// 所有一级分类目录
        List<MallCategory> l1CatList = categoryService.queryL1(industryId);

        // 当前一级分类目录
        MallCategory currentCategory = null;
        if (id != null) {
            currentCategory = categoryService.findById(id);
        } else {
            currentCategory = l1CatList.get(0);
        }

        // 当前一级分类目录对应的二级分类目录
        List<MallCategory> currentSubCategory = null;
        if (null != currentCategory) {
            currentSubCategory = categoryService.queryByPid(currentCategory.getId());
        }

        Map<String, Object> data = new HashMap<String, Object>();
        data.put("categoryList", l1CatList);
        data.put("currentCategory", currentCategory);
        data.put("currentSubCategory", currentSubCategory);
        return ResponseUtil.ok(data);*/

        String url = platFormServerproperty.getUrl(MallConstant.TAOBAO);
        Map<String, Object> mappMap = new HashMap<String, Object>();
        mappMap.put("plateform", "wlk");
        mappMap.put("id", id);
        Header[] headers = new BasicHeader[]{new BasicHeader("method", "getCurrentCategoryIntegral"), new BasicHeader("version", "v1.0")};
        String result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);

        //方法1：
        JSONObject object = JSONObject.parseObject(result);
        if (object.getString("errno").equals("0")) {
            return object;
        }

        //方法二
//        JSONObject object = JSONObject.parseObject(result);
//        if (!object.getString("errno").equals("0")) {
//            return ResponseUtil.fail();
//        }
//        Map<String, Object> dataResult = new HashMap<>();
//        JSONObject jsonObject = object.getJSONObject("data");
//        if (ObjectUtil.isNotNull(jsonObject.get("errno"))) {
//            JSONObject jsonCoupon = jsonObject.parseObject(jsonObject.get("data").toString());
//            dataResult.put("data", jsonCoupon);
//            return ResponseUtil.ok(dataResult);
//        }

        Map<String, Object> data = new HashMap<String, Object>();
        return ResponseUtil.ok(data);
    }

    /**
     * 所有分类数据
     *
     * @return 所有分类数据
     */
    @GetMapping("all")
    public Object queryAll(@RequestParam(value="industryId",defaultValue = "0") Integer industryId) {
        //优先从缓存中读取
        /*if (HomeCacheManager.hasData(HomeCacheManager.CATALOG)) {
            return ResponseUtil.ok(HomeCacheManager.getCacheData(HomeCacheManager.CATALOG));
        }

        // 所有一级分类目录
        List<MallCategory> l1CatList = categoryService.queryL1(industryId);

        //所有子分类列表
        Map<Integer, List<MallCategory>> allList = new HashMap<>();
        List<MallCategory> sub;
        for (MallCategory category : l1CatList) {
            sub = categoryService.queryByPid(category.getId());
            allList.put(category.getId(), sub);
        }

        // 当前一级分类目录
        MallCategory currentCategory = l1CatList.get(0);

        // 当前一级分类目录对应的二级分类目录
        List<MallCategory> currentSubCategory = null;
        if (null != currentCategory) {
            currentSubCategory = categoryService.queryByPid(currentCategory.getId());
        }

        Map<String, Object> data = new HashMap<String, Object>();
        data.put("categoryList", l1CatList);
        data.put("allList", allList);
        data.put("currentCategory", currentCategory);
        data.put("currentSubCategory", currentSubCategory);

        //缓存数据
        HomeCacheManager.loadData(HomeCacheManager.CATALOG, data);
        return ResponseUtil.ok(data);*/
        return ResponseUtil.ok();
    }

    /**
     * 当前分类栏目
     *
     * @param id 分类类目ID
     * @return 当前分类栏目
     */
    @GetMapping("current")
    public Object current(@NotNull String id) {
        // 当前分类
        MallCategory currentCategory = categoryService.findById(id);
        List<MallCategory> currentSubCategory = categoryService.queryByPid(currentCategory.getId());

        Map<String, Object> data = new HashMap<String, Object>();
        data.put("currentCategory", currentCategory);
        data.put("currentSubCategory", currentSubCategory);
        return ResponseUtil.ok(data);
    }
}