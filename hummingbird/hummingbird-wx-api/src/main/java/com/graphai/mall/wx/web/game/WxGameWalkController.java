package com.graphai.mall.wx.web.game;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.graphai.mall.db.vo.WxMpUserDTO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.json.JacksonUtil;
import com.graphai.mall.db.domain.GameJackpot;
import com.graphai.mall.db.domain.GameMotionInviteFriend;
import com.graphai.mall.db.domain.GameMotionMakeMoney;
import com.graphai.mall.db.domain.GameRuleConfig;
import com.graphai.mall.db.domain.GameUserEnroll;
import com.graphai.mall.db.domain.MallUser;
import com.graphai.mall.db.constant.account.AccountStatus;
import com.graphai.mall.db.service.game.GameJackpotService;
import com.graphai.mall.db.service.game.GameMotionInviteFriendService;
import com.graphai.mall.db.service.game.GameMotionMakeMoneyService;
import com.graphai.mall.db.service.game.GameRuleConfigService;
import com.graphai.mall.db.service.game.GameUserEnrollService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.constant.CommonConstant;
import com.graphai.mall.db.util.account.AccountUtils;
import com.graphai.mall.wx.annotation.LoginUser;

import me.chanjar.weixin.mp.bean.result.WxMpUser;

@RestController
@RequestMapping("/wx/gamejackpot")
@Validated
public class WxGameWalkController {
    private final Log logger = LogFactory.getLog(WxGameWalkController.class);

    @Autowired
    private MallUserService mallUserService;

    @Autowired
    private GameMotionMakeMoneyService gameMotionMakeMoneyService;
    @Autowired
    private GameMotionInviteFriendService gameMotionInviteFriendService;

    @Autowired
    private GameRuleConfigService gameRuleConfigService;

    @Autowired
    private GameJackpotService gameJackpotService;
    @Autowired
    private GameUserEnrollService gameUserEnrollService;

    /**
     * 2:走路赚钱游戏,查询步数
     */
    @RequestMapping("/seSteps")
    public Object selectReceiveSteps(@LoginUser String userId) {
        if (null == userId) {
            return ResponseUtil.unlogin();
        }
        MallUser user = mallUserService.findById(userId);
        if (null == user) {
            return ResponseUtil.unlogin();
        }
        List<GameMotionMakeMoney> gm = gameMotionMakeMoneyService.selectMontionSteps(userId);
        List<GameMotionInviteFriend> list = gameMotionInviteFriendService.selectInviteFriend(userId);
        Map datamap = new HashMap();
        Map datamap1 = new HashMap();
        Map map = new HashMap();
        LocalDateTime now = LocalDateTime.now();
        long Steps = 0;// 步数
        if (gm.size() > 0) {
            for (GameMotionMakeMoney game : gm) {
                if ("1".equals(game.getType())) {
                    datamap.put("step", Integer.parseInt(game.getSteps()));
                } else if ("2".equals(game.getType())) {
                    LocalDateTime cre = game.getCreateTime();
                    Duration duration = Duration.between(cre, now);
                    long a = duration.toMillis() / 1000;// 过了多少秒
                    Steps = Long.parseLong(game.getSteps());
                    if (list.size() > 0) {// 有好友助力
                        for (GameMotionInviteFriend gamef : list) {
                            Duration duration1 = Duration.between(now, gamef.getInviteEndTime());
                            if (duration1.toMillis() > 0) {
                                // 好友助力未过期
                                Steps += gamef.getTimes() * a;
                            }
                        }
                        if (Steps >= 60000) {
                            // 最大只存储60000步
                            Steps = 60000;
                            datamap1.put("step", Steps);
                        } else {
                            datamap1.put("step", Steps);
                        }
                    } else {
                        // 没有好友助力
                        Steps += 50 * a;
                        if (Steps >= 60000) {
                            // 最大只存储60000步
                            Steps = 60000;
                            datamap1.put("step", Steps);
                        } else {
                            datamap1.put("step", Steps);
                        }
                    }


                }
            }
            if (null != datamap && datamap.size() > 0) {
                map.put("alreadyStep", datamap);
            } else {
                map.put("alreadyStep", 0);
            }
            if (null != datamap1 && datamap1.size() > 0) {
                map.put("recordStep", datamap1);
            } else {
                map.put("recordStep", 0);
            }


        }


        if (null == gm) {
            return ResponseUtil.fail();
        }
        return ResponseUtil.ok(map);


    }

    /**
     * 3:走路赚钱游戏,领取步数
     */
    @PostMapping("/reSteps")
    public Object getReceiveSteps(@LoginUser String userId, @RequestBody String steps) {
        if (null == userId) {
            return ResponseUtil.unlogin();
        }
        MallUser user = mallUserService.findById(userId);
        if (null == user) {
            return ResponseUtil.unlogin();
        }
        String step1 = JacksonUtil.parseString(steps, "steps");
        // String type = JacksonUtil.parseString(steps, "type");

        // List<GameMotionMakeMoney> gm = gameMotionMakeMoneyService.selectMontionSteps(userId);
        GameMotionMakeMoney gm = gameMotionMakeMoneyService.selectMontionAlreadySteps(userId, "1");
        GameMotionMakeMoney gm1 = gameMotionMakeMoneyService.selectMontionAlreadySteps(userId, "2");
        int i = 0;
        Integer st = Integer.parseInt(gm.getSteps());
        Integer st1 = Integer.parseInt(step1);
        String step = (st + st1) + "";
        Map map = new HashMap();
        if (null != gm && null != gm1) {
            gm.setSteps(step);
            gm1.setSteps("0");
            i = gameMotionMakeMoneyService.updateMontionStep(gm);
            gameMotionMakeMoneyService.updateMontionStep(gm1);
            map.put("steps", Integer.parseInt(gm.getSteps()));
        } else {
            i = gameMotionMakeMoneyService.insertMotionSteps(userId, step, "1");
            map.put("steps", 0);
        }


        if (i > 0) {
            return ResponseUtil.ok(map);
        } else {
            return ResponseUtil.fail(403, "领取步数失败");
        }

    }

    /**
     * 3.1:走路赚钱游戏,存后台步数
     */
    @PostMapping("/storeSteps")
    public Object storeSteps(@LoginUser String userId, @RequestBody String steps) {
        if (null == userId) {
            return ResponseUtil.unlogin();
        }
        MallUser user = mallUserService.findById(userId);
        if (null == user) {
            return ResponseUtil.unlogin();
        }
        GameMotionMakeMoney gm = gameMotionMakeMoneyService.selectMontionAlreadySteps(userId, "2");
        GameMotionMakeMoney gm1 = gameMotionMakeMoneyService.selectMontionAlreadySteps(userId, "1");
        String step = JacksonUtil.parseString(steps, "steps");


        LocalDateTime now = LocalDateTime.now();
        LocalDateTime cre = gm.getCreateTime();
        Duration duration = Duration.between(cre, now);
        long a = duration.toMillis() / 1000;// 过了多少秒

        if (null != gm) {
            gm.setSteps(Long.parseLong(step) + "");
            logger.info("后台步数--step-----" + step);
            gameMotionMakeMoneyService.updateMontionStep(gm);
        } else {
            if (null == step) {
                gameMotionMakeMoneyService.insertMotionSteps(userId, "0", "2");
            }
            gameMotionMakeMoneyService.insertMotionSteps(userId, step, "2");
        }

        if (null == gm1) {
            gameMotionMakeMoneyService.insertMotionSteps(userId, "0", "1");
        }

        return ResponseUtil.ok();
    }


    /**
     * 4:走路赚钱游戏,兑换步数
     */
    @RequestMapping("/coSteps")
    public Object convertSteps(@LoginUser String userId) throws Exception {

        if (null == userId) {
            return ResponseUtil.unlogin();
        }
        MallUser user = mallUserService.findById(userId);
        if (null == user) {
            return ResponseUtil.unlogin();
        }
        GameMotionMakeMoney gm = gameMotionMakeMoneyService.selectMontionAlreadySteps(userId, "1");
        if (Long.parseLong(gm.getSteps()) < 40000) {
            return ResponseUtil.badArgumentValue("需要累计40000步才能兑换");
        }
        Long step = Long.parseLong(gm.getSteps()) / 10000;// 运动币兑换金币 10000:1
        Map paramsMap = new HashMap();
        paramsMap.put("dType", AccountStatus.DISTRIBUTION_TYPE_00); // 直接分润
        paramsMap.put("message", "走路赚钱游戏奖励");
        paramsMap.put("capitalTrend", AccountStatus.CAPITAL_TREND_01);
        paramsMap.put("changeType", AccountStatus.CHANGE_TYPE_01);
        gm.setSteps("0");// 步数归零

        int i = gameMotionMakeMoneyService.updateMontionStep(gm);
        // 插入用户账户金额
        if (new BigDecimal(step).compareTo(new BigDecimal(0.00)) == 1) {
            AccountUtils.accountChange(userId, new BigDecimal(step), CommonConstant.TASK_REWARD_GOLD, AccountStatus.BID_7, paramsMap);
        }
        Map datamap = new HashMap();
        datamap.put("gold", step);
        if (i > 0) {
            return ResponseUtil.ok(datamap);
        } else {
            return ResponseUtil.fail(403, "兑换失败！");
        }
    }

    /**
     * 5:分享好友助力接口
     */
    @PostMapping("/shareFriend")
    public Object shareGoodFriend(@RequestBody WxMpUserDTO wxMpUserDTO) {
        String userId = wxMpUserDTO.getUserId();
        if (userId == null) {
            return ResponseUtil.badArgumentValue("未获取到用户id！");
        }
        WxMpUser wxMpUser = wxMpUserDTO.getWxMpUser();
        String openId = wxMpUser.getOpenId();
        String headImgUrl = wxMpUser.getHeadImgUrl();
        String nickname = wxMpUser.getNickname();
        LocalDateTime localDateTime = LocalDateTime.now();
        // todo 判断用户是否属于自己助力自己
        MallUser mallUser = mallUserService.findById(userId);
        if (mallUser == null) {
            return ResponseUtil.badArgumentValue("未查询到用户信息！");
        } else {
            if (openId.equals(mallUser.getWeixinMpOpenid())) {
                return ResponseUtil.badArgumentValue("无法给自己助力！");
            }
        }

        // 判断助力用户是否已经助力过
        GameMotionInviteFriend friend = new GameMotionInviteFriend();
        friend.setUserId(userId);
        friend.setFriendOpenid(openId);
        friend.setInviteEndTime(localDateTime);
        List<GameMotionInviteFriend> friendList = gameMotionInviteFriendService.selectInviteFriendByCondition(friend);
        if (friendList.size() > 0) {
            return ResponseUtil.badArgumentValue("助力失败,你已经助力过该用户！");
        }


        // 走路分享,GameMotionInviteFriend表记录 邀请人和被邀请人关系
        GameMotionInviteFriend gameMotionInviteFriend = new GameMotionInviteFriend();
        // gameMotionInviteFriend.setFriendId(friendId);
        gameMotionInviteFriend.setUserId(userId);
        gameMotionInviteFriend.setInviteStartTime(localDateTime); // 好友助力开始时间
        gameMotionInviteFriend.setInviteEndTime(localDateTime.plusHours(4)); // todo 邀请有效期结束时间，暂时定为2个小时
        gameMotionInviteFriend.setTimes(100); // todo 倍数，暂时定为2倍
        gameMotionInviteFriend.setAvatar(headImgUrl);
        gameMotionInviteFriend.setFriendOpenid(openId);
        gameMotionInviteFriend.setNickname(nickname);
        gameMotionInviteFriendService.insertInviteFriend(gameMotionInviteFriend);

        return ResponseUtil.ok();
    }

    /**
     * 查询分享好友列表
     */
    @GetMapping("/friendList")
    public Object getshareFriendList(@LoginUser String userId) {
        if (null == userId) {
            return ResponseUtil.unlogin();
        }
        String avater = "http://gxfacepay.oss-cn-shenzhen.aliyuncs.com/hummingbird/mipmap/she.png";// 默认头像
        List<GameMotionInviteFriend> list = gameMotionInviteFriendService.selectInviteFriend(userId);
        Map datamap = new TreeMap();
        MallUser user = mallUserService.findById(userId);
        List<Map> list1 = new ArrayList<>();
        if (null != user) {
            if (null != user.getAvatar() && !"".equals(user.getAvatar())) {
                datamap.put("avatar", user.getAvatar());// 助力人头像

            } else {
                datamap.put("avatar", avater);
            }
        } else {
            return ResponseUtil.unlogin();
        }

        datamap.put("times", 50);
        list1.add(datamap);
        LocalDateTime now = LocalDateTime.now();
        // 少于4个好友
        if (list.size() > 0) {
            for (GameMotionInviteFriend game : list) {
                Map<String, Object> map = new HashMap<String, Object>();
                Duration duration = Duration.between(now, game.getInviteEndTime());
                long millis = duration.toMillis();// 相差毫秒数
                if (millis > 0) {
                    if (null != game.getAvatar() && !"".equals(game.getAvatar())) {
                        map.put("avatar", game.getAvatar());// 助力人头像
                        map.put("nickname", game.getNickname());// 昵称
                    } else {
                        map.put("avatar", avater);// 助力人头像
                    }

                    map.put("times", game.getTimes()); // 倍数
                    map.put("endTime", game.getInviteEndTime().toString());// 结束时间
                    list1.add(map);
                } else {
                    game.setDeleted(true);
                    gameMotionInviteFriendService.updateGameFriend(game);
                }
            }
        }


        return ResponseUtil.ok(list1);
    }

    /**
     * 走路赚钱分场次
     */
    @GetMapping("/jackpot")
    public Object getMotionJackpot(@LoginUser String userId, @RequestParam(defaultValue = "1000") Integer step) {
        if (null == userId) {
            return ResponseUtil.unlogin();
        }
        LocalDate now = LocalDate.now();
        GameRuleConfig ruleConfig = gameRuleConfigService.selectGameRuleConfigByStep(step);
        String ruleId = "";
        if (null != ruleConfig) {
            ruleId = ruleConfig.getId();
        } else {
            return ResponseUtil.fail(403, "请传入正确的规则id");
        }

        Map maptoday = new HashMap();// 今天
        Map mapyes = new HashMap();// 昨天
        Map tomorrow = new HashMap();// 明天
        Map datamap = new HashMap();
        LocalDate previousDay = now.minusDays(1);// 前一天
        LocalDate tom = now.plusDays(1);// 明天


        // todo 当天统计
        GameJackpot jackpotday = gameJackpotService.selectGameJackpotByRuleIdOne(ruleId, now);
        if (null != jackpotday) {
            maptoday.put("jackpot", jackpotday.getJvalue());// 今天奖池值
            maptoday.put("number", jackpotday.getParticipantsNumber());// 参加人
            Map tarmap = gameMotionMakeMoneyService.countTargetStepSum(jackpotday.getId(), step + "", now.toString());
            maptoday.put("targetNumber", tarmap.get("snum"));// 达到目标人数
            BigDecimal profit = jackpotday.getJvalue().divide(BigDecimal.valueOf((int) jackpotday.getParticipantsNumber()));// 预计收益金币
            maptoday.put("profit", profit);// 预计收益金币

            GameUserEnroll game = gameUserEnrollService.selectAlreadyEnrollOne(now.toString(), jackpotday.getId(), userId);
            if (null != game) {
                maptoday.put("status", true);// 今日已报名
                maptoday.put("dayStep", game.getGameStep());// 今日步数
                if (game.getGameStep() >= step) {
                    maptoday.put("isChallenge", true);
                } else {
                    maptoday.put("isChallenge", false);
                }
            } else {
                maptoday.put("status", false);// 今日未报名
                maptoday.put("isChallenge", false);
                maptoday.put("dayStep", 0);// 今日步数
            }


        } else {
            maptoday.put("jackpot", 0);// 今天奖池值
            maptoday.put("number", 0);// 参加人
            maptoday.put("dayStep", 0);// 今日步数
            maptoday.put("targetNumber", 0);// 达到目标人数
            maptoday.put("profit", 0);// 预计收益金币
            maptoday.put("status", false);// 今天未报名
            maptoday.put("dayStep", 0);// 今日步数
            maptoday.put("isChallenge", false);
        }

        // todo 昨天统计
        GameJackpot jackpotyes = gameJackpotService.selectGameJackpotByRuleIdOne(ruleId, now.minusDays(1));
        if (null != jackpotyes) {
            mapyes.put("jackpot", jackpotyes.getJvalue());// 昨天奖池值
            Map tarmap = gameMotionMakeMoneyService.countTargetStepSum(jackpotyes.getId(), step + "", previousDay.toString());
            mapyes.put("targetNumber", tarmap.get("snum"));// 昨天达到目标人数
            mapyes.put("yesterday", now.minusDays(1));// 昨天日期
            GameUserEnroll game = gameUserEnrollService.selectAlreadyEnrollOne(now.minusDays(1).toString(), jackpotyes.getId(), userId);
            if (null != game) {
                mapyes.put("status", true);// 昨日已报名
            } else {
                mapyes.put("status", false);// 昨日未报名
            }
            mapyes.put("number", jackpotyes.getParticipantsNumber());// 参加人
        } else {
            mapyes.put("jackpot", 0);// 昨天奖池值
            mapyes.put("targetNumber", 0);// 昨天达到目标人数
            mapyes.put("yesterday", now.minusDays(1));// 昨天日期
            mapyes.put("status", false);// 昨天默认未报名
            mapyes.put("number", 0);// 参加人
        }

        // todo 明天报名
        GameJackpot jackpottom = gameJackpotService.selectGameJackpotByRuleIdOne(ruleId, tom);
        tomorrow.put("tomorrw", tom);// 明天日期
        tomorrow.put("step", step);// 目标步数
        tomorrow.put("signAmount", ruleConfig.getSignAmount());// 报名费
        tomorrow.put("ruleId", ruleId);// 报名规则id
        if (null != jackpottom) {
            tomorrow.put("number", jackpottom.getParticipantsNumber());// 参加人
        } else {
            tomorrow.put("number", 0);// 参加人
        }
        GameUserEnroll game = null;
        if (null != jackpottom) {
            game = gameUserEnrollService.selectAlreadyEnrollOne(tom.toString(), jackpottom.getId(), userId);
        }

        if (null != game) {
            tomorrow.put("status", true);// 昨日已报名
        } else {
            tomorrow.put("status", false);// 昨日未报名
        }

        datamap.put("today", maptoday);
        datamap.put("yesterday", mapyes);
        datamap.put("tomorrow", tomorrow);
        return ResponseUtil.ok(datamap);
    }


    /**
     * 看广告翻倍
     */
    @PostMapping("/advert")
    public Object advertDouble(@LoginUser String userId, @RequestBody String steps) {
        if (null == userId) {
            return ResponseUtil.unlogin();
        }
        String type = JacksonUtil.parseString(steps, "type");
        // 请财神头像
        String God = "http://gxfacepay.oss-cn-shenzhen.aliyuncs.com/hummingbird/mipmap/%E7%BB%84%202%402x.png";
        // 加速头像
        String advert = "http://gxfacepay.oss-cn-shenzhen.aliyuncs.com/hummingbird/mipmap/%E7%BB%84%201%402x.png";

        GameMotionInviteFriend gameMotionInviteFriend = new GameMotionInviteFriend();
        gameMotionInviteFriend.setUserId(userId);
        gameMotionInviteFriend.setInviteStartTime(LocalDateTime.now());
        gameMotionInviteFriend.setInviteEndTime(LocalDateTime.now().plusHours(2)); // todo 邀请有效期结束时间，暂时定为2个小时
        gameMotionInviteFriend.setTimes(20); // todo 倍数，暂时定为2倍
        // 请财神
        if ("1".equals(type)) {
            gameMotionInviteFriend.setAvatar(God);
            gameMotionInviteFriend.setNickname("请财神");
        }
        // 加速
        else if ("2".equals(type)) {
            gameMotionInviteFriend.setAvatar(advert);
            gameMotionInviteFriend.setNickname("加速卡");
        }
        gameMotionInviteFriend.setFriendOpenid("");


        gameMotionInviteFriendService.insertInviteFriend(gameMotionInviteFriend);
        return ResponseUtil.ok();
    }


}
