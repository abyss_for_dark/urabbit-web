package com.graphai.mall.wx.web.game;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.mall.db.domain.*;
import com.graphai.mall.db.service.*;
import com.graphai.mall.db.service.game.GameDefinitionService;
import com.graphai.mall.db.service.game.GameJackpotRedpacketService;
import com.graphai.mall.db.service.game.GameJackpotService;
import com.graphai.mall.db.service.game.GameRedPacketRecordService;
import com.graphai.mall.db.service.game.GameRuleConfigService;
import com.graphai.mall.db.service.game.GameUserEnrollService;
import com.graphai.mall.db.util.CommonUtils;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.validator.Order;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * 游戏定义、游戏规则配置
 */
@RestController
@RequestMapping("/wx/game")
@Validated
public class WxGameController {
    private static final Log logger = LogFactory.getLog(WxGameController.class);

    @Autowired
    private GameDefinitionService gameDefinitionService;
    @Autowired
    private GameRuleConfigService gameRuleConfigService;
    @Autowired
    private GameUserEnrollService gameUserEnrollService;
    @Autowired
    private GameJackpotService gameJackpotService;
    @Autowired
    private GameJackpotRedpacketService gameJackpotRedpacketService;
    @Autowired
    private GameRedPacketRecordService gameRedPacketRecordService;


    /**
     * 查询游戏定义
     */
    @GetMapping("/gameDefinitionList")
    public Object gameDefinitionList(@RequestParam(defaultValue = "id") String sort,
                                     @Order @RequestParam(defaultValue = "desc") String order) {
        List<GameDefinition> list = gameDefinitionService.selectGameDefinitionList(null, null, null, sort, order);
        Map<String, Object> data = new HashMap<>();
        data.put("items", list);
        return ResponseUtil.ok(data);
    }

    /**
     * 根据gameId 查询游戏规则配置list
     */
    @GetMapping("/gameRuleConfigList")
    public Object gameRuleConfigList(@RequestParam("gameId") String gameId,
                                     @RequestParam(defaultValue = "id") String sort,
                                     @Order @RequestParam(defaultValue = "desc") String order) {
        GameRuleConfig gameRuleConfig = new GameRuleConfig();
        gameRuleConfig.setGameId(gameId);
        List<GameRuleConfig> list = gameRuleConfigService.selectGameRuleConfigList(gameRuleConfig, null, null, sort, order);
        Map<String, Object> data = new HashMap<>();
        data.put("items", list);
        return ResponseUtil.ok(data);
    }

    /**
     * 根据gameRuleId 查询游戏规则配置
     */
    @GetMapping("/gameRuleConfig")
    public Object gameRuleConfig(@RequestParam("gameRuleId") String gameRuleId){
        GameRuleConfig gameRuleConfig = gameRuleConfigService.selectGameRuleConfigById(gameRuleId);
        return ResponseUtil.ok(gameRuleConfig);
    }


    /**
     * 游戏奖池金额分配 (定时任务:每天12点执行)
     * 根据 (随机红包奖励、行榜奖励、下期奖池累积奖励)比例、打卡人数完成情况  分配奖池金额
     *      规则：  1.全打卡
     *              2.部分打卡
     */
//    @Scheduled(cron = "0 0 12 * * ?")
//    @PostMapping("/autoGameJackPotShare")   //todo 后续去除
//    public Object autoGameJackPotShare() throws Exception {
////        LocalDateTime now = LocalDateTime.now();
//        LocalDateTime now = LocalDateTime.of(LocalDate.now(), LocalTime.of(12,00,00));  //测试
//
//        //查询奖池信息
//        GameJackpot gameJackpot = new GameJackpot();
//        gameJackpot.setSettlementTime(now);
//        gameJackpot.setJstatus(CommonConstant.GAME_JSSTATUS_1);
//        List<GameJackpot> gameJackpotList = gameJackpotService.selectGameJackpotList(gameJackpot, null, null, null, null);
//        if(gameJackpotList.size() > 0){
//            logger.info("gameJackpotList========= " + JacksonUtils.bean2Jsn(gameJackpotList));
//            //todo 增加异常处理
//
//            for (GameJackpot jackpot : gameJackpotList){
//                String jackpotId = jackpot.getId(); //奖池id
//                String gameId = jackpot.getGameId();    //游戏id
//                String gameRuleId = jackpot.getGameRuleId();    //游戏规则id
//                Integer totalNumber = jackpot.getParticipantsNumber();  //奖池最终结算人数(总人数)
//                Integer copiesNumber = jackpot.getCopiesNumber();   //奖池份数
//                String amountType = jackpot.getAmountType();    //游戏金额类型(JL20001 金币奖励 JL20002 现金奖励)
//                BigDecimal jvalue = jackpot.getJvalue();    //奖池金额
//
//
//                //查询游戏完成人数信息
//                GameDefinition gameDefinition = gameDefinitionService.selectGameDefinitionById(gameId); //游戏定义
//                String gameType = gameDefinition.getGameType(); //游戏类型
//                GameRuleConfig gameRuleConfig = gameRuleConfigService.selectGameRuleConfigById(gameRuleId); //游戏规则
//
//                GameUserEnroll gameUserEnroll = new GameUserEnroll();
//                gameUserEnroll.setJackpotId(jackpotId);
//                gameUserEnroll.setGameStep(gameRuleConfig.getStepCount());
//                //拼接结束时间和开始时间
//                String ruleBeginTime = LocalDate.now() + " " + gameRuleConfig.getRuleBeginTime();
//                String ruleEndTime = LocalDate.now() + " " + gameRuleConfig.getRuleEndTime();
//
//                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
//                LocalDateTime beginTime = LocalDateTime.parse(ruleBeginTime, formatter);    //开始时间
//                LocalDateTime endTime = LocalDateTime.parse(ruleEndTime, formatter);    //结束时间
//                long second = CommonUtils.getSecond(beginTime, endTime);    //时间间隔(秒)
//
//
//                List<GameUserEnroll> gameUserEnrollCompleteList = gameUserEnrollService.selectGameUserEnrollListByComplete(gameUserEnroll,gameType,"clock_time","desc");     /**完成userList信息**/
//                Integer completeNumber = gameUserEnrollCompleteList.size(); //完成人数
//                Integer punishNumber = 0;    //完成但需惩罚人数(小数直接进1,如‘5.1’ 变为 6)        //todo 可删除
//                List<GameUserEnroll> gameUserEnrollPunishList = null;   /**惩罚userList信息**/
//                List<GameUserEnroll> gameUserEnrollFailList = gameUserEnrollService.selectGameUserEnrollListByFail(gameUserEnroll,gameType);    /**未完成userList信息**/
//                BigDecimal punishSecond = null; //惩罚时间
//
//                if(gameType.equals(CommonConstant.GAME_TYPE_CLOCK)){
//                    //打卡：   惩罚人数是按照时间段来计算的(6:00——8:00, 惩罚最后一段时间打卡的人)
//                    //计算惩罚段 的开始时间 = 结束时间 - 计算的秒数
//                    punishSecond = new BigDecimal(second).multiply(gameRuleConfig.getNegativeProportion()).setScale(0,BigDecimal.ROUND_UP);
//                    LocalDateTime newBeginTime = CommonUtils.getLocalDateTime(false, endTime, punishSecond.longValue());
//
//                    gameUserEnrollPunishList = gameUserEnrollService.selectGameUserEnrollListByPunish(gameUserEnroll, gameType, newBeginTime, endTime, null);
////                    punishNumber = BigDecimal.valueOf(completeNumber).multiply(gameRuleConfig.getNegativeProportion()).setScale(0,BigDecimal.ROUND_UP).intValue();
//                    punishNumber = gameUserEnrollPunishList.size();
//
//                }else{
//                    //走路：   查询走路排行的标准步数值(仅仅只有走路的时候需要判断标准值)
//                    //         惩罚人数是按照来计算的
//                    List<GameUserEnroll> gameUserEnrollList = gameUserEnrollService.selectGameUserEnrollList(gameUserEnroll, null, null, null, null);
//                    int i = new BigDecimal(totalNumber).multiply(gameRuleConfig.getNegativeProportion()).setScale(0,BigDecimal.ROUND_UP).intValue();
//                    Integer standardValue = gameUserEnrollList.get(i - 1).getGameStep();   //步数标准值
//                    gameUserEnrollPunishList = gameUserEnrollService.selectGameUserEnrollListByPunish(gameUserEnroll, gameType,null, null, standardValue);
//                    punishNumber = gameUserEnrollPunishList.size();
//                }
//
//                /**判断规则情景 （1.全打卡     2.部分打卡）**/
//                Boolean flag = true;    //全完成
//                //完成人中的惩罚金额
//                BigDecimal punishMoney = new BigDecimal(0.00);  //惩罚金额
//                if(gameUserEnrollPunishList.size() > 0){
//                    for (GameUserEnroll enroll: gameUserEnrollPunishList){
//                        punishMoney = punishMoney.add(enroll.getSignAmount());
//                    }
//                }
//                //未完成人的金额
//                BigDecimal failMoney = new BigDecimal(0.00);  //未完成总结金额
//                if(gameUserEnrollFailList.size() > 0){
//                    flag = false;   //未全完成
//                    for (GameUserEnroll enroll: gameUserEnrollFailList){
//                        failMoney = failMoney.add(enroll.getSignAmount());
//                    }
//                }
//
//                /**获取奖金池 分配的金额**/
//                Map<String,Object> reward = CommonUtils.getReward(flag, gameRuleConfig, punishMoney, failMoney);
//                logger.info("奖励map------" + JacksonUtils.bean2Jsn(reward));
//                BigDecimal rankReward = new BigDecimal(MapUtils.getString(reward, "rankReward"));       //排行奖励
//                BigDecimal serviceReward = new BigDecimal(MapUtils.getString(reward, "serviceReward")); //服务费
//                BigDecimal envelopesReward = new BigDecimal(MapUtils.getString(reward, "envelopesReward")); //红包奖励
//                BigDecimal nextJackpotReward = new BigDecimal(MapUtils.getString(reward, "nextJackpotReward")); //下期奖池奖励
//
//
//
//                //1.排行奖励
//                //结算 分配的金额
//                if(completeNumber > 0){
//                    //在完成人数中筛选  “奖励人数” 和 “惩罚人数”
//                    /**奖励人数 = 完成总人数 - 惩罚人数**/
//                    Integer rewardNumber = completeNumber - punishNumber;
//                    Integer intervalNumber;    //段数
//                    BigDecimal maxRewardMultiple = gameRuleConfig.getMaxRewardMultiple();   //奖励最高倍位 an
//                    BigDecimal minRewardMultiple = gameRuleConfig.getMinRewardMultiple();   //奖励最低倍位 a1
//                    BigDecimal singleReward = new BigDecimal(0.00);    //单份倍数金额(单份奖励金额)
//                    BigDecimal subRewardMultiple;   //奖励倍数下降幅度 d
//
//                    Map<String, Object> paramsMap = new HashMap<>();
//                    paramsMap.put("dType", AccountStatus.DISTRIBUTION_TYPE_00);
//                    paramsMap.put("message",now.toLocalDate() + " 场次" + gameRuleConfig.getRuleName() + "游戏瓜分奖励");
//                    paramsMap.put("capitalTrend",AccountStatus.CAPITAL_TREND_01);
//                    paramsMap.put("changeType",AccountStatus.CHANGE_TYPE_01);
//                    //todo -------- 后续改动遍历方法，多重for循环可能会影响执行效率 ---------
//                    if(gameType.equals(CommonConstant.GAME_TYPE_CLOCK)){
//                        /**1.打卡
//                         *
//                         **/
//                        intervalNumber = gameRuleConfig.getIntervalNumber();
//                        //单份奖励金额
//                        singleReward = CommonUtils.getSingleReward(rankReward, intervalNumber, maxRewardMultiple, minRewardMultiple);
//                        //下降幅度 d
//                        subRewardMultiple = CommonUtils.getSubRewardMultiple(intervalNumber, maxRewardMultiple, minRewardMultiple);
////                        List<GameUserEnroll> rewardList = gameUserEnrollCompleteList.subList(0, rewardNumber); //奖励
//
//                        //计算时间间距
//                        BigDecimal rewardSecond = new BigDecimal(second).subtract(punishSecond);    //奖励的总时间段
//                        BigDecimal singleSecond = rewardSecond.divide(new BigDecimal(intervalNumber),0,BigDecimal.ROUND_DOWN);  //单份时间段
//
//                        //1.完成人数中 奖励金额的
//                        for (int i = 0; i < intervalNumber; i++) {
//                            //计算每个阶段应得的奖励金额 和 奖励人数信息
//                            BigDecimal addReward = singleReward.multiply(maxRewardMultiple.subtract(new BigDecimal(i).multiply(subRewardMultiple)));
////                            BigDecimal addSecond = singleSecond.multiply(new BigDecimal(i));    //递增的秒数
//
//                            LocalDateTime newEndTime = CommonUtils.getLocalDateTime(true, beginTime, singleSecond.longValue());
//                            logger.info("开始：" + beginTime + "，结算：" + newEndTime);
//                            List<GameUserEnroll> gameUserEnrollRewardList = gameUserEnrollService.selectGameUserEnrollListByPunish(gameUserEnroll, gameType, beginTime, newEndTime, null);
//                            for (int j = 0; j < gameUserEnrollRewardList.size() ; j++) {
//                                GameUserEnroll enroll = gameUserEnrollRewardList.get(j);
//                                BigDecimal lastReward = enroll.getSignAmount().add(addReward);   //每个阶段应得的最终金额 = 本金 + 奖励金额
//
//                                GameUserEnroll updateGameUserEnroll = new GameUserEnroll();
//                                updateGameUserEnroll.setId(enroll.getId());
//                                updateGameUserEnroll.setRewardType(amountType);
//                                updateGameUserEnroll.setReward(lastReward);
//                                updateGameUserEnroll.setEstatus(String.valueOf(1)); //已结算
//                                gameUserEnrollService.updateGameUserEnroll(updateGameUserEnroll);
//
//                                //todo 充值记录，以及用户账号金额变动------------------------------------------------------------------------------------CYF
//                                AccountUtils.accountChange(enroll.getUserId(),lastReward,amountType,AccountStatus.BID_7,paramsMap);
//                                //todo 充值记录，以及用户账号金额变动------------------------------------------------------------------------------------CYF
//                            }
//                            beginTime = newEndTime;   //重置开始时间
//                        }
//
//
//                        //2.完成人数中 惩罚金额的
//                        if(punishNumber > 0){
//                            for (int i = 0; i < punishNumber; i++) {
//                                GameUserEnroll enroll = gameUserEnrollPunishList.get(i);
//                                BigDecimal lastReward = enroll.getSignAmount().
//                                        multiply(new BigDecimal(1).subtract(gameRuleConfig.getSubRewardProportion()))
//                                        .setScale(2,BigDecimal.ROUND_DOWN);   //惩罚人数的最终奖励金额 = 本金*(1-惩罚比例)
//
//                                GameUserEnroll updateGameUserEnroll = new GameUserEnroll();
//                                updateGameUserEnroll.setId(enroll.getId());
//                                updateGameUserEnroll.setRewardType(amountType);
//                                updateGameUserEnroll.setReward(lastReward);
//                                updateGameUserEnroll.setEstatus(String.valueOf(1)); //已结算
//                                gameUserEnrollService.updateGameUserEnroll(updateGameUserEnroll);
//
//                                //todo 充值记录，以及用户账号金额变动------------------------------------------------------------------------------------CYF
//                                AccountUtils.accountChange(enroll.getUserId(),lastReward,amountType,AccountStatus.BID_7,paramsMap);
//                                //todo 充值记录，以及用户账号金额变动------------------------------------------------------------------------------------CYF
//                            }
//                        }
//
//                    }else{
//                        /**2.走路
//                         *  奖励人数(符合条件的人数):
//                         *      (1)步数高于 标准步数(总人数根据步数排序,然后取出中间人的步数，即为标准步数 )
//                         *      (2)完成挑战步数
//                         **/
//                        BigDecimal rewardProportion = gameRuleConfig.getRewardProportion(); //最终奖励人数 在奖励人数中 的占比
//                        Integer lastRewardNumber = rewardProportion.multiply(new BigDecimal(rewardNumber)).setScale(0,BigDecimal.ROUND_DOWN).intValue();
//                        //单份奖励金额
//                        if(lastRewardNumber > 0){
//                            singleReward = CommonUtils.getSingleReward(rankReward, lastRewardNumber, maxRewardMultiple, minRewardMultiple);
//                        }
//                        //下降幅度 d
//                        subRewardMultiple = CommonUtils.getSubRewardMultiple(lastRewardNumber, maxRewardMultiple, minRewardMultiple);
//                        //1.完成人数中 最终奖励人数
//                        for (int i = 0; i < lastRewardNumber; i++) {
//                            /** 奖励金额 = 单份奖励金额 * 最高份额 - 递减金额 **/
//                            BigDecimal addReward = singleReward.multiply(maxRewardMultiple.subtract(new BigDecimal(i).multiply(subRewardMultiple))) ;
//                            GameUserEnroll enroll = gameUserEnrollCompleteList.get(i);
//                            BigDecimal lastReward = enroll.getSignAmount().add(addReward);
//
//                            GameUserEnroll updateGameUserEnroll = new GameUserEnroll();
//                            updateGameUserEnroll.setId(enroll.getId());
//                            updateGameUserEnroll.setRewardType(amountType);
//                            updateGameUserEnroll.setReward(lastReward);
//                            updateGameUserEnroll.setEstatus(String.valueOf(1)); //已结算
//                            gameUserEnrollService.updateGameUserEnroll(updateGameUserEnroll);
//
//                            //todo 充值记录，以及用户账号金额变动------------------------------------------------------------------------------------CYF
//                            AccountUtils.accountChange(enroll.getUserId(),lastReward,amountType,AccountStatus.BID_7,paramsMap);
//                            //todo 充值记录，以及用户账号金额变动------------------------------------------------------------------------------------CYF
//                        }
//                        //2.完成人数中 金额不变的(返还本金)
//                        Integer noRewardNumber = completeNumber - punishNumber;
//                        if(noRewardNumber > 0){
//                            for (int i = lastRewardNumber; i < noRewardNumber; i++) {
//                                GameUserEnroll enroll = gameUserEnrollCompleteList.get(i);
//
//                                GameUserEnroll updateGameUserEnroll = new GameUserEnroll();
//                                updateGameUserEnroll.setId(enroll.getId());
//                                updateGameUserEnroll.setRewardType(amountType);
//                                updateGameUserEnroll.setReward(enroll.getSignAmount());
//                                updateGameUserEnroll.setEstatus(String.valueOf(1)); //已结算
//                                gameUserEnrollService.updateGameUserEnroll(updateGameUserEnroll);
//
//                                //todo 充值记录，以及用户账号金额变动------------------------------------------------------------------------------------CYF
//                                AccountUtils.accountChange(enroll.getUserId(),enroll.getSignAmount(),amountType,AccountStatus.BID_7,paramsMap);
//                                //todo 充值记录，以及用户账号金额变动------------------------------------------------------------------------------------CYF
//                            }
//                        }
//                        //3.完成人数中 惩罚金额的
//                        if(punishNumber > 0){
//                            for (int i = 0; i < punishNumber; i++) {
//                                GameUserEnroll enroll = gameUserEnrollPunishList.get(i);
//                                BigDecimal lastReward = enroll.getSignAmount().
//                                        multiply(new BigDecimal(1).subtract(gameRuleConfig.getSubRewardProportion()))
//                                        .setScale(2,BigDecimal.ROUND_DOWN);   //惩罚人数的最终奖励金额 = 本金*(1-惩罚比例)
//
//                                GameUserEnroll updateGameUserEnroll = new GameUserEnroll();
//                                updateGameUserEnroll.setId(enroll.getId());
//                                updateGameUserEnroll.setRewardType(amountType);
//                                updateGameUserEnroll.setReward(lastReward);
//                                updateGameUserEnroll.setEstatus(String.valueOf(1)); //已结算
//                                gameUserEnrollService.updateGameUserEnroll(updateGameUserEnroll);
//
//                                //todo 充值记录，以及用户账号金额变动------------------------------------------------------------------------------------CYF
//                                AccountUtils.accountChange(enroll.getUserId(),enroll.getSignAmount(),amountType,AccountStatus.BID_7,paramsMap);
//                                //todo 充值记录，以及用户账号金额变动------------------------------------------------------------------------------------CYF
//                            }
//                        }
//
//
//                    }
//                }
//
//                //修改其他未完成人的结算状态
//                GameUserEnroll gameUserEnrollOther = new GameUserEnroll();
//                gameUserEnrollOther.setJackpotId(jackpotId);
//                gameUserEnrollOther.setRewardType(amountType);
//                gameUserEnrollOther.setReward(new BigDecimal(0.00));
//                gameUserEnrollOther.setEstatus(String.valueOf(1));
//                gameUserEnrollService.updateGameUserEnrollByOther(gameUserEnrollOther);
//
//                //结算完成之后修改奖池状态
//                GameJackpot updateGameJackpot = new GameJackpot();
//                updateGameJackpot.setId(jackpotId);
//                updateGameJackpot.setJstatus(CommonConstant.GAME_JSSTATUS_2);
//                updateGameJackpot.setSettlementNumber(completeNumber);  //结算人数
//                gameJackpotService.updateGameJackpot(updateGameJackpot);
//
//
//                //2.系统服务费
//                if(serviceReward.compareTo(new BigDecimal(0.00)) == 1){
//                    //服务费打入系统账户
//                    Map<String, Object> paramsMap = new HashMap<>();
//                    paramsMap.put("dType", AccountStatus.DISTRIBUTION_TYPE_00);
//                    paramsMap.put("message",now.toLocalDate() + " 场次" + gameRuleConfig.getRuleName() + "系统服务费");
//                    paramsMap.put("capitalTrend",AccountStatus.CAPITAL_TREND_01);
//                    paramsMap.put("changeType",AccountStatus.CHANGE_TYPE_01);
//                    //todo 充值记录，以及用户账号金额变动------------------------------------------------------------------------------------CYF
//                    AccountUtils.accountChange("00",serviceReward,amountType,AccountStatus.BID_7,paramsMap);
//                    //todo 充值记录，以及用户账号金额变动------------------------------------------------------------------------------------CYF
//
//                }
//
//                //3.下期奖金池生成
//                if(nextJackpotReward.compareTo(new BigDecimal(0.00)) == 1){
//                    LocalDate tomorrowDate = LocalDate.now().plusDays(1);
//                    //结算时间
//                    LocalDateTime settlementTime = LocalDateTime.of(tomorrowDate, LocalTime.of(12,00,00));
//
//                    //查询是否存在下期奖池
//                    GameJackpot nextJackpot = new GameJackpot();
//                    nextJackpot.setGameId(gameId);
//                    nextJackpot.setGameRuleId(gameRuleId);
//                    nextJackpot.setEnrollTime(tomorrowDate);
//                    List<GameJackpot> nextGameJackpotList = gameJackpotService.selectGameJackpotList(nextJackpot, null, null, null, null);
//                    if(nextGameJackpotList.size() > 0){
//                        nextJackpot = nextGameJackpotList.get(0);
//                        nextJackpot.setJvalue(nextJackpot.getJvalue().add(nextJackpotReward));
//                        gameJackpotService.updateGameJackpot(nextJackpot);
//                    }else{
//                        nextJackpot.setJackpotName(tomorrowDate + "期" + gameRuleConfig.getRuleName());
//                        nextJackpot.setAmountType(amountType);
//                        nextJackpot.setParticipantsNumber(0);
//                        nextJackpot.setCopiesNumber(0);
//                        nextJackpot.setSettlementNumber(0);
//                        nextJackpot.setJvalue(nextJackpotReward);
//                        nextJackpot.setRemarks(tomorrowDate + "期"+ gameRuleConfig.getRuleName() + "游戏奖池备注");
//                        nextJackpot.setSettlementTime(settlementTime);
//                        nextJackpot.setJackpotStartTime(LocalDateTime.now());   //奖池开始时间
//                        gameJackpotService.insertGameJackpot(nextJackpot);
//                    }
//                }
//
//                //4.红包生成
//                if(envelopesReward.compareTo(new BigDecimal(0.00)) == 1){
//                    GameJackpotRedpacket gameJackpotRedpacket = new GameJackpotRedpacket();
//                    gameJackpotRedpacket.setGameId(gameId);
//                    gameJackpotRedpacket.setGameRuleId(gameRuleId);
//                    gameJackpotRedpacket.setGameJackpotId(jackpotId);
//                    gameJackpotRedpacket.setEnrollTime(jackpot.getEnrollTime());
//                    gameJackpotRedpacket.setAmountType(amountType);
//                    gameJackpotRedpacket.setAmount(envelopesReward);
//                    gameJackpotRedpacketService.insertGameJackpotRedpacket(gameJackpotRedpacket);
//                }
//                //todo 红包随机发放,奖励完成任务用户
////                for (GameUserEnroll gameUserEnroll2: gameUserEnrollCompleteList){
////
////                }
//
//
//            }
//
//        }
//
//
//        return ResponseUtil.ok();
//    }



    /**
     * 自动分配红包金额(定时任务：每天下午2点执行)
     */
//    @Scheduled(cron = "0 0 14 * * ?")
//    @PostMapping("/autoGameJackpotRedpacket")   //todo 后续去除
//    public Object autoGameJackpotRedpacket() throws Exception {
////        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
////        LocalDate date = LocalDate.parse(time, formatter);
//        LocalDate date = LocalDate.now();
//        //查询当天未分配的红包
//        GameJackpotRedpacket gameJackpotRedpacket = new GameJackpotRedpacket();
//        gameJackpotRedpacket.setEnrollTime(date);
//        gameJackpotRedpacket.setGjrStatus("0");
//        List<GameJackpotRedpacket> gameJackpotRedpacketList = gameJackpotRedpacketService.selectGameJackpotRedpacketList(gameJackpotRedpacket, null, null, null, null);
//        if(gameJackpotRedpacketList.size() > 0){
//            for (int i = 0; i < gameJackpotRedpacketList.size(); i++) {
//                GameJackpotRedpacket redpacket = gameJackpotRedpacketList.get(i);
//                String redpacketId = redpacket.getId(); //红包id
//                BigDecimal amount = redpacket.getAmount();  //红包金额
//                String amountType = redpacket.getAmountType();  //金额类型
//                Integer num = 10; // todo 红包数量暂时定义为10个
//                if(amount.compareTo(new BigDecimal(0.00)) == 1){
//                    String gameJackpotId = redpacket.getGameJackpotId();    //奖池id
//                    String gameId = redpacket.getGameId();//游戏id
//                    //查询游戏定义
//                    GameDefinition gameDefinition = gameDefinitionService.selectGameDefinitionById(gameId);
//                    GameUserEnroll gameUserEnroll = new GameUserEnroll();
//                    gameUserEnroll.setJackpotId(gameJackpotId);
//                    //查询完成游戏的人数信息
//                    List<GameUserEnroll> gameUserEnrollList = gameUserEnrollService.selectGameUserEnrollListByComplete(gameUserEnroll, gameDefinition.getGameType(), "clock_time","desc");
//
//                    Map<String, Object> paramsMap = new HashMap<>();
//                    paramsMap.put("dType", AccountStatus.DISTRIBUTION_TYPE_00);
//                    paramsMap.put("message",LocalDate.now() + " 好运榜红包奖励");
//                    paramsMap.put("capitalTrend",AccountStatus.CAPITAL_TREND_01);
//                    paramsMap.put("changeType",AccountStatus.CHANGE_TYPE_01);
//                    if(gameUserEnrollList.size() > 0){
//                        /** 存在完成任务的人，可随机瓜分红包金额 **/
//                        if (gameUserEnrollList.size() < num) {
//                            num = gameUserEnrollList.size();
//                        }
//                        //(1)随机取出用户id组合成新的userIdList
//                        List<String> userIdList = RedpacketUtil.getRandomList(gameUserEnrollList, num);
//                        //(2)随机合成新的moneyList
//                        List<BigDecimal> moneyList = RedpacketUtil.generate(amount, num);
//                        logger.info("userIdList " + JacksonUtils.bean2Jsn(userIdList));
//                        logger.info("moneyList " + JacksonUtils.bean2Jsn(moneyList));
//                        if(userIdList.size() > 0){
//                            //随机分配红包金额
//                            for (int j = 0; j < userIdList.size(); j++) {
//                                BigDecimal redpacketAmount = moneyList.get(j);
//                                if(redpacketAmount.compareTo(new BigDecimal(0.00)) == 1){
//                                    //用户id
//                                    String userId = userIdList.get(j);
//                                    //金额存进用户账户
//                                    AccountUtils.accountChange(userId,redpacketAmount,amountType,AccountStatus.BID_7,paramsMap);
//
//                                    GameRedpacketRecord gameRedpacketRecord = new GameRedpacketRecord();
//                                    gameRedpacketRecord.setRedpacketId(redpacketId);
//                                    gameRedpacketRecord.setAmount(redpacketAmount);
//                                    gameRedpacketRecord.setAmountType(amountType);
//                                    gameRedpacketRecord.setToUserId(userId);
//                                    gameRedPacketRecordService.insertRedPacketRecode(gameRedpacketRecord);
//                                }
//                            }
//                        }
//
//                    }else{
//                        /** 不存在完成任务的人，红包总金额直接打入系统用户账号 **/
//                        AccountUtils.accountChange("00",amount,amountType,AccountStatus.BID_7,paramsMap);
//                    }
//                }
//
//                //将红包状态改为 发送状态
//                redpacket.setGjrStatus("1");
//                gameJackpotRedpacketService.updateGameJackpotRedpacket(redpacket);
//
//            }
//        }
//
//        return ResponseUtil.ok();
//    }



    public static void main(String[] args) throws IOException {

//        List<GameUserEnroll> list = new ArrayList();
//        for (int i = 7; i > 0; i--) {
//            GameUserEnroll gameUserEnroll = new GameUserEnroll();
//            gameUserEnroll.setId(i + "");
//            gameUserEnroll.setUserId(i*10 + "");
//            list.add(gameUserEnroll);
//        }
//        logger.info(JacksonUtils.bean2Jsn(list));
//
//
//        int num = list.size();
//        BigDecimal bigDecimal = new BigDecimal(0.5);
//        int sub = new BigDecimal(num).multiply(bigDecimal).setScale(0,BigDecimal.ROUND_DOWN).intValue();
//        logger.info("奖励人数： " + sub);
//
//
//        List<GameUserEnroll> newList = list.subList(0, sub);
//        List<GameUserEnroll> newList2 = list.subList(sub, num);
//        logger.info(JacksonUtils.bean2Jsn(newList));
//        logger.info(JacksonUtils.bean2Jsn(newList2));
//
//        List<String> ids = new ArrayList();
//        List<String> ids2 = new ArrayList();
//        List<String> userIds = new ArrayList();
//        List<String> userIds2 = new ArrayList();
//        for (GameUserEnroll enroll : newList){
//            ids.add(enroll.getId());
//            userIds.add(enroll.getUserId());
//        }
//        for (GameUserEnroll enroll : newList2){
//            ids2.add(enroll.getId());
//            userIds2.add(enroll.getUserId());
//        }
//        logger.info(ids + " --- " + userIds);
//        logger.info(ids2 + " --- " + userIds2);
//
//        logger.info();
//
//        logger.info("num 为 " + num);
//        int i = (BigDecimal.valueOf(num).multiply(new BigDecimal(0.5))).setScale(0,BigDecimal.ROUND_UP).intValue();
//        logger.info("i 为 " + i);
//        String standardValue = list.get(i - 1).getId();   //步数标准值
//        logger.info(standardValue);
//
//


        /** 总：100 , 份数：5 , 最高倍数：5倍, 最低倍数：1倍 **/
//        BigDecimal reward = new BigDecimal(200);
//        Integer fraction = 5;
//        BigDecimal maxRewardMultiple = new BigDecimal(5);
//        BigDecimal minRewardMultiple = new BigDecimal(1);
//
//        BigDecimal singleReward = CommonUtils.getSingleReward(reward, fraction, maxRewardMultiple, minRewardMultiple);
//        BigDecimal subRewardMultiple = CommonUtils.getSubRewardMultiple(fraction, maxRewardMultiple, minRewardMultiple);
//
//        BigDecimal last = singleReward.multiply(maxRewardMultiple);
//        BigDecimal first = singleReward.multiply(minRewardMultiple);
//        logger.info("单份倍数：" + singleReward);
//        logger.info("首项：" + first);
//        logger.info("尾项：" + last);
//        logger.info("下降幅度：" + subRewardMultiple);

        /** 计算时间段数 **/
        String begin = "06:00:00";
        String end = "08:00:00";
        LocalDate date = LocalDate.now();
        logger.info(date);

        String beginDate = date + " " + begin;
        String endDate = date + " " + end;

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime beginTime = LocalDateTime.parse(beginDate, formatter);
        LocalDateTime endTime = LocalDateTime.parse(endDate, formatter);

        logger.info(beginTime);
        logger.info(endTime);

        //计算时间时间差距
        long second = CommonUtils.getSecond(beginTime, endTime);
        logger.info(second);

        //按比例分成(计算惩罚的时间段)
        BigDecimal multiply = new BigDecimal(0.2).multiply(new BigDecimal(second)).setScale(0,BigDecimal.ROUND_UP);

        LocalDateTime startTime = CommonUtils.getLocalDateTime(false, endTime, multiply.longValue());
        logger.info(startTime);

        //设置份数


    }








}
