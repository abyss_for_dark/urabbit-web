package com.graphai.mall.wx.web.user;

import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.mall.db.domain.MallUser;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.vo.MallAgentFansVo;
import com.graphai.mall.wx.annotation.LoginUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/wx/fansManager")
public class WxUserFansManager {
    @Autowired
    private MallUserService userService;
    /**
     * 房淘 --- 查询粉丝
     */
    @GetMapping("/getUserByFans")
    public Object getUserListByMaker(@LoginUser String userId, @RequestParam(defaultValue = "1") Integer page,
                                     @RequestParam(defaultValue = "10") Integer limit) {

        List<MallAgentFansVo>  userList = new ArrayList<>();
        long total = 0;
        if (null != userId) {
            MallUser mallUser = userService.findById(userId);
            if (null != mallUser) {
                userList = userService.getAgentByDriectLeader(userId, page, limit);
                total = PageInfo.of(userList).getTotal();
            }
        }
        Map<String, Object> map = new HashMap<>();
        map.put("userList", userList);
        map.put("total", total); // 全部
        return ResponseUtil.ok(map);
    }

}
