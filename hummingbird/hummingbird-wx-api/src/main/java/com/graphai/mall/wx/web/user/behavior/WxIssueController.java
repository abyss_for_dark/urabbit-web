package com.graphai.mall.wx.web.user.behavior;

import com.github.pagehelper.PageInfo;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.graphai.commons.constant.MallConstant;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.mall.db.domain.MallGoodsProduct;
import com.graphai.mall.db.domain.MallIssue;
import com.graphai.mall.db.service.user.behavior.MallIssueService;
import com.graphai.validator.Order;
import com.graphai.validator.Sort;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/wx/issue")
@Validated
public class WxIssueController {
	private final Log logger = LogFactory.getLog(WxIssueController.class);

	@Autowired
	private MallIssueService issueService;

	/**
	 * 帮助中心
	 */
	@RequestMapping("/list")
	public Object list(String question, @RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer size,
			@RequestParam(defaultValue = "product") String issueType, @Sort @RequestParam(defaultValue = "id") String sort,
			@Order @RequestParam(defaultValue = "desc") String order) {
		
		List<String> fpids = new ArrayList<String>();
		fpids.add("0");
		List<MallIssue> issueList = issueService.querySelective(issueType,fpids, question, page, size, sort, order);
		if (MallConstant.ISSUE_TYPE_HELP.equals(issueType) && CollectionUtils.isNotEmpty(issueList)) {
			
			List<Map<String,Object>> data = new ArrayList<Map<String,Object>>();
			
			List<String> spids = new ArrayList<String>();
			for (int i = 0; i < issueList.size(); i++) {
				MallIssue issue = issueList.get(i);
				spids.add(issue.getId());
			}
			List<MallIssue> childrenIssueList = issueService.querySelective(issueType, spids, question, page, size, sort, order);
			
			for (int i = 0; i < issueList.size(); i++) {
				Map<String,Object> idata = new  HashMap<String, Object>();
				MallIssue issue = issueList.get(i);
				
				//产品筛选
				List<MallIssue> cissueCollect = new ArrayList<MallIssue>();
				CollectionUtils.select(childrenIssueList, new Predicate() {
					@Override
					public boolean evaluate(Object object) {
						MallIssue cissue = (MallIssue)object;
						return (cissue!=null && cissue.getPid().equals(issue.getId())) ? true : false;
					}},cissueCollect);
				
				idata.put("issue", issue);
				idata.put("cissueCollect", cissueCollect);
				data.add(idata);
			}
			
			long total = PageInfo.of(issueList).getTotal();
			Map<String, Object> retdata = new HashMap<String, Object>();
			retdata.put("data", data);
			retdata.put("count", total);
			
			return ResponseUtil.ok(retdata);
		} else {
			long total = PageInfo.of(issueList).getTotal();
			Map<String, Object> data = new HashMap<String, Object>();
			data.put("data", issueList);
			data.put("count", total);
			return ResponseUtil.ok(data);
		}

	}

}
