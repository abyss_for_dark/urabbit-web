package com.graphai.mall.wx.web.test;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;

import com.graphai.mall.db.constant.account.MallUserProfitNewEnum;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.json.JacksonUtils;
import com.graphai.mall.admin.service.IMallRedDStatisticsService;
import com.graphai.mall.db.dao.MallAdMapper;
import com.graphai.mall.db.dao.MallUserMapper;
import com.graphai.mall.db.domain.MallAd;
import com.graphai.mall.db.domain.MallOrderGoods;
import com.graphai.mall.db.domain.MallRegion;
import com.graphai.mall.db.domain.MallUser;
import com.graphai.mall.db.domain.MallUserExample;
import com.graphai.mall.db.service.account.AccountService;
import com.graphai.mall.db.constant.account.AccountStatus;
import com.graphai.mall.db.service.common.MallRegionService;
import com.graphai.mall.db.service.goods.MallGoodsService;
import com.graphai.mall.db.service.order.MallOrderGoodsService;
import com.graphai.mall.db.service.order.MallOrderService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.constant.CommonConstant;
import com.graphai.mall.db.util.account.bobbi.AccountQYUtils;
import com.graphai.mall.db.util.account.bobbi.AccountZYUtils;
import com.graphai.mall.db.vo.MallOrderGoodsVo;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.properties.TaoBaoProperties;
import com.taobao.api.DefaultTaobaoClient;
import com.taobao.api.TaobaoClient;
import com.taobao.api.request.TbkActivitylinkGetRequest;
import com.taobao.api.request.TbkScInvitecodeGetRequest;
import com.taobao.api.request.TbkScPublisherInfoGetRequest;
import com.taobao.api.request.TbkScPublisherInfoSaveRequest;
import com.taobao.api.response.TbkActivitylinkGetResponse;
import com.taobao.api.response.TbkScInvitecodeGetResponse;
import com.taobao.api.response.TbkScPublisherInfoGetResponse;
import com.taobao.api.response.TbkScPublisherInfoGetResponse.MapData;
import com.taobao.api.response.TbkScPublisherInfoSaveResponse;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSONUtil;

@RestController
@RequestMapping("/wx/tbk")
@Validated
public class TestTbkController {

    private final Log logger = LogFactory.getLog(TestTbkController.class);

    @Autowired
    private MallUserService userService;

    @Autowired
    private TaoBaoProperties taoBaoProperties;

    @Autowired
    private MallOrderService mallOrderService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private MallOrderGoodsService orderGoodsService;

    @Autowired
    private MallAdMapper mallAdMapper;

    @Autowired
    private IMallRedDStatisticsService mallRedDStatisticsService;

    @Resource
    private MallRegionService mallRegionService;

    @Resource
    private MallUserMapper userMapper;

    @Autowired
    private MallGoodsService mallGoodsService;


    private static String url = "https://eco.taobao.com/router/rest";


    // private final HotsearchApi client = new HotsearchApi();

    /**
     *
     * 淘宝客-公用-私域用户邀请码生成
     */
    /**
     * @param userId
     * @param type 渠道或会员
     * @return
     */
    @GetMapping("getInvitationCode")
    public Object getInvitationCode(String userId, @Validated String type) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }

        try {

            MallUser user = userService.findById(userId);
            if (com.graphai.commons.util.obj.ObjectUtils.isNotNull(user)) {
                // if (StringUtils.isNotBlank(user.getTbkAccessToken())) {
                TaobaoClient client = new DefaultTaobaoClient(url, taoBaoProperties.getLianmengAppKey(),
                                taoBaoProperties.getLianmengAppSecret());
                TbkScInvitecodeGetRequest req = new TbkScInvitecodeGetRequest();
                // req.setRelationId(11L);
                req.setRelationApp("common");

                if (type.equals("relation")) {

                    req.setCodeType(1L);
                }

                if (type.equals("relationChange")) {

                    req.setCodeType(2L);
                }

                if (type.equals("member")) {

                    req.setCodeType(3L);
                }

                TbkScInvitecodeGetResponse rsp =
                                client.execute(req, "6100927b7d199af357d73ea6ea095d085cd1c32378aa3043448122507");
                logger.info("获取渠道邀请码------------" + JacksonUtils.bean2Jsn(rsp));
                if (rsp.getData() != null) {
                    String code = rsp.getData().getInviterCode();
                    return ResponseUtil.ok(rsp);
                } else {
                    System.out.println("错误码" + rsp.getErrorCode());
                    return ResponseUtil.fail(400, rsp.getSubMsg());
                }
                // }

            }

        } catch (Exception e) {
            logger.error(e.getMessage(), e);

        }
        return ResponseUtil.fail();
    }


    /**
     *
     * 淘宝客-公用-私域用户备案
     */
    /**
     * @param userId
     * @param type 渠道或会员
     * @return
     */
    @GetMapping("tbkPublisher")
    public Object tbkPublisher(String userId) {


        if (userId == null) {
            return ResponseUtil.unlogin();
        }


        try {
            MallUser user = userService.findById(userId);
            if (com.graphai.commons.util.obj.ObjectUtils.isNotNull(user)) {
                // token暂时写死，实际从user表获取
                // if (StringUtils.isNotBlank(user.getTbkAccessToken())) {
                TaobaoClient client = new DefaultTaobaoClient(url, taoBaoProperties.getLianmengAppKey(),
                                taoBaoProperties.getLianmengAppSecret());
                TbkScPublisherInfoSaveRequest req = new TbkScPublisherInfoSaveRequest();

                req.setRelationFrom("1");
                req.setOfflineScene("1");
                req.setOnlineScene("1");
                req.setInviterCode("MTCQ32");
                req.setInfoType(1L);
                req.setNote("小蜜蜂");

                // 暂时写死，从user表中获取
                TbkScPublisherInfoSaveResponse rsp =
                                client.execute(req, "6202107b258cb43ZZ60431c8b026b640a150d3fec1e7e702939123716");
                logger.info("渠道备案信息获取------------" + JacksonUtils.bean2Jsn(rsp));
                if (rsp.getData() != null) {

                    return ResponseUtil.ok(rsp);
                } else {
                    return ResponseUtil.fail(400, rsp.getSubMsg());
                }

                // }

            }

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return ResponseUtil.fail();
    }


    /**
     *
     * 淘宝客-公用-私域用户备案信息查询(获取会员或渠道信息及pid)
     */
    /**
     * @param userId
     * @param type 渠道或会员
     * @return
     */
    @GetMapping("getPublisherPid")
    public Object getPublisherPid(String userId, @Validated String type) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }


        try {
            MallUser user = userService.findById(userId);
            if (com.graphai.commons.util.obj.ObjectUtils.isNotNull(user)) {
                // token暂时写死，实际从user表获取
                // if (StringUtils.isNotBlank(user.getTbkAccessToken())) {
                TaobaoClient client = new DefaultTaobaoClient(url, taoBaoProperties.getLianmengAppKey(),
                                taoBaoProperties.getLianmengAppSecret());
                TbkScPublisherInfoGetRequest req = new TbkScPublisherInfoGetRequest();

                if (type.equals("relation")) {
                    if (StringUtils.isNotBlank(user.getTbkRelationId())) {
                        // 暂时写死，从user表中获取

                    }
                    req.setInfoType(1L);
                }

                if (type.equals("member")) {
                    if (StringUtils.isNotBlank(user.getTbkSpecialId())) {
                        // req.setSpecialId(user.getTbkSpecialId());
                    }
                    req.setInfoType(2L);
                }

                req.setPageNo(1L);
                req.setPageSize(10L);
                req.setRelationApp("common");

                // 暂时写死，从user表中获取
                TbkScPublisherInfoGetResponse rsp =
                                client.execute(req, "610202024c06e83ecdff8c926bf219cb9cbb598a72fbe473448122507");
                logger.info("渠道备案信息获取------------" + JacksonUtils.bean2Jsn(rsp));
                if (rsp.getData() != null) {
                    List<MapData> mapDatas = rsp.getData().getInviterList();

                    String pid = mapDatas.get(0).getRootPid();
                    user.setTbkPubId(pid);
                    userService.updateById(user);
                    return ResponseUtil.ok(rsp);
                } else {
                    return ResponseUtil.fail(400, JacksonUtils.bean2Jsn(rsp));
                }

                // }

            }

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return ResponseUtil.fail();
    }


    /**
     *
     * 淘宝客-推广者-所有订单查询
     */
    /**
     * @return
     * @throws Exception
     */
    @PostMapping("getTbOrderDetail")
    public Object getTbOrderDetail() throws Exception {

        // String startTime = map.get("startTime");
        // String endTime = map.get("endTime");
        //
        //
        // // 参数错误
        // if (StringUtils.isBlank(startTime) || StringUtils.isBlank(endTime)) {
        // return ResponseUtil.badArgument();
        // }


        // long nd = 1000 * 24 * 60 * 60;
        // long nh = 1000 * 60 * 60;
        //
        // SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        //
        // java.util.Date sDate=sdf.parse(startTime);
        // java.util.Date eDate=sdf.parse(endTime);
        //
        // // 获得两个时间的毫秒时间差异
        // long diff = eDate.getTime() - sDate.getTime();
        //
        // // 计算差多少小时
        // long hour = diff / nh;
        // if (hour>=3) {
        // return ResponseUtil.fail(402, "订单开始和结束时间不能超过三小时!");
        // }


        // Calendar beginCalendar = Calendar.getInstance();
        // beginCalendar.setTime(sDate); //设定时间为2017年3月3日 1:0:0
        // Calendar endCalendar = Calendar.getInstance();
        // endCalendar.setTime(eDate); //设定时间为2017年3月3日 14:0:0
        // long beginTime = beginCalendar.getTime().getTime();
        // long eTime = endCalendar.getTime().getTime();
        // long betweenHours = (long)((eTime - beginTime) / (1000 * 60 * 60 ));
        //
        // if (betweenHours>=3) {
        // return ResponseUtil.fail(402, "订单开始和结束时间不能超过三小时!");
        // }


        try {
            // return mallOrderService.getSnOrderDetail();
            String str = "4979752655426420736?sub_user=";

            String str1 = str.substring(0, str.indexOf("?"));
            String str2 = str.substring(str1.length() + 1, str.length());
            System.out.println("字符串" + str2);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage(), e);
        }
        return ResponseUtil.ok();
    }


    /**
     * 测试红包汇总
     * 
     * @return
     * @throws Exception
     */
    @PostMapping("getRedStatistics")
    public Object getRedStatistics() throws Exception {


        try {
            return mallRedDStatisticsService.insertStatistics();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage(), e);
        }
        return ResponseUtil.ok();
    }


    /**
     * 同步省市区
     * 
     * @return
     * @throws Exception
     */
    @PostMapping("synchronization")
    public Object synchronization() throws Exception {


        try {

            MallUserExample example = new MallUserExample();
            example.createCriteria().andProvinceLike("%" + "市" + "%").andDeletedEqualTo(false);
            List<MallUser> users = userMapper.selectByExample(example);


            String cityId = null;
            String pid = null;
            if (CollectionUtils.isNotEmpty(users)) {
                for (MallUser mallUser : users) {

                    if (!Objects.equals(null, mallUser.getProvince())) {
                        if (mallRegionService.queryByRegionName(mallUser.getProvince()) != null) {
                            mallUser.setProvinceCode(
                                            mallRegionService.queryByRegionName(mallUser.getProvince()).getCode());
                            pid = mallRegionService.queryByRegionName(mallUser.getProvince()).getId();
                        }
                    }



                    if (!Objects.equals(null, mallUser.getCity())) {

                        if (mallUser.getCity().equals(mallUser.getProvince())) {
                            List<MallRegion> regions = mallRegionService.queryByPid(pid);
                            if (CollectionUtils.isNotEmpty(regions)) {
                                mallUser.setCityCode(regions.get(0).getCode());
                            }
                        } else {
                            if (mallRegionService.queryByRegionName(mallUser.getCity()) != null) {
                                mallUser.setCityCode(mallRegionService.queryByRegionName(mallUser.getCity()).getCode());
                                cityId = mallRegionService.queryByRegionName(mallUser.getCity()).getId();
                            }
                        }


                    }
                    MallRegion region = null;
                    if (!Objects.equals(null, mallUser.getCounty())) {
                        if (cityId != null) {
                            region = mallRegionService.queryByNameAndPid(mallUser.getCounty(), cityId);
                        } else {
                            region = mallRegionService.queryByRegionName(mallUser.getCounty());
                        }

                        if (region != null) {
                            mallUser.setAreaCode(region.getCode());
                        }
                    }
                    userService.updateById(mallUser);
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage(), e);
        }
        return ResponseUtil.ok();
    }



    /**
     * 同步市辖区的code
     * 
     * @return
     * @throws Exception
     */
    @PostMapping("synchronizationAreaCode")
    public Object synchronizationAreaCode() throws Exception {

        try {
            MallUserExample example = new MallUserExample();
            example.createCriteria().andCountyEqualTo("市辖区").andAreaCodeIsNull();
            List<MallUser> users = userMapper.selectByExample(example);

            if (CollectionUtils.isNotEmpty(users)) {
                for (MallUser mallUser : users) {

                    if (!Objects.equals(null, mallUser.getCityCode())) {
                        String areaCode = String.valueOf(mallUser.getCityCode()).substring(0, 5) + "1";
                        mallUser.setAreaCode(Integer.parseInt(areaCode));
                    }
                    userService.updateById(mallUser);
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage(), e);
        }
        return ResponseUtil.ok();
    }


    /**
     *
     * 官方活动转链
     */
    /**
     * @return
     * @throws Exception
     */
    // @PostMapping("getTaoBaoWord")
    // public Object getTaoBaoWord() throws Exception {
    //
    //
    //
    //
    // try {
    // TaobaoClient client = new DefaultTaobaoClient(url,
    // taoBaoProperties.getLianmengAppKey(),taoBaoProperties.getLianmengAppSecret());
    // TbkActivityInfoGetRequest req = new TbkActivityInfoGetRequest();
    // req.setAdzoneId(110282750499L);
    // //req.setSubPid("mm_1_2_3");
    // //req.setRelationId(123L);
    // req.setActivityMaterialId("1585018034441");
    // req.setUnionId("demo");
    // TbkActivityInfoGetResponse rsp = client.execute(req);
    // logger.info("官方活动------------" + JacksonUtils.bean2Jsn(rsp));
    // } catch (Exception e) {
    // e.printStackTrace();
    // logger.error(e.getMessage(), e);
    // }
    // return ResponseUtil.ok();
    // }


    /**
     *
     * 抖音
     */
    /**
     * @return
     * @throws Exception
     */
    // @PostMapping("getDouYinHotToken")
    // public Object getDouYinHot() throws Exception {
    //
    //
    // DefaultApi defaultApi=new DefaultApi();
    //
    // try {
    // OauthClientTokenResponse
    // oauthClientTokenResponse=defaultApi.oauthClientTokenGet("awv0pqjbsxryoot2",
    // "afda499ba3145a0b8669053d1a5862d7","client_credential");
    //
    // logger.info("抖音获取授权------------" + JacksonUtils.bean2Jsn(oauthClientTokenResponse));
    // } catch (Exception e) {
    // e.printStackTrace();
    // logger.error(e.getMessage(), e);
    // }
    // return ResponseUtil.ok();
    // }
    //
    //
    //
    // /**
    // * 抖音热词
    // *
    // *
    // */
    // @PostMapping("getDouYinHotMsg")
    // public Object getDouYinHotMsg() throws Exception {
    //
    //
    // HotsearchApi apiInstance = new HotsearchApi();
    // String accessToken = "clt.20a2de03d00dbaa9acd097100efbc2277tyY2WbpmcwO3p9MnipT6tilKzm8";
    //
    // try {
    // HotsearchSentencesResponse result = apiInstance.hotsearchSentencesGet(accessToken);
    //
    //
    // logger.info("抖音获取热词------------" + JacksonUtils.bean2Jsn(result));
    // return ResponseUtil.ok(result);
    // } catch (Exception e) {
    // e.printStackTrace();
    // logger.error(e.getMessage(), e);
    // }
    // return ResponseUtil.ok();
    //
    // }
    //
    //
    //
    // /**
    // * 抖音热词
    // *
    // *
    // */
    @PostMapping("testZy")
    public Object testZy() throws Exception {



        BigDecimal userFee = BigDecimal.ZERO;
        BigDecimal shareFee = BigDecimal.ZERO;
        Map<String, String> resuMap = null;
        // 商品返现
        BigDecimal totalCashback = new BigDecimal(0.1); // 返现金额=售价-出厂价
        MallOrderGoodsVo orderGoods = new MallOrderGoodsVo();
        orderGoods.setGoodsId("5028439511340679168");
        // orderGoods.setProductId(productId);
        orderGoods.setSurplusCashback(new BigDecimal(0));
        // 成功返现人数
        int count = orderGoodsService.selectCashbackCount(orderGoods);
        List<MallOrderGoodsVo> mallOrderGoodsList = orderGoodsService.selectCashbackListByGoodsId(orderGoods);
        logger.info("返现列表： " + JSONUtil.toJsonStr(mallOrderGoodsList));
        if (CollectionUtils.isNotEmpty(mallOrderGoodsList)) {



            // 获取队列中的用户id
            List<String> usersList =
                            mallOrderGoodsList.stream().map(MallOrderGoodsVo::getUserId).collect(Collectors.toList());
            if (totalCashback.compareTo(new BigDecimal(0)) == 1) {
                resuMap = AccountZYUtils.getProfitZy1OnlyUser("308717663974002688", AccountStatus.BID_27, totalCashback,
                        MallUserProfitNewEnum.PROFIT_TYPE_2.strCodeInt());
            }



            for (MallOrderGoodsVo mallOrderGoodsVo : mallOrderGoodsList) {

                BigDecimal cashbackAmount = mallOrderGoodsVo.getCashbackAmount(); // 已返现金额
                BigDecimal surplusCashback = mallOrderGoodsVo.getSurplusCashback(); // 剩余返现金额
                BigDecimal amout = new BigDecimal(0); // 用户返现金额


                if (Objects.equals("0000", resuMap.get("code"))) {
                    userFee = new BigDecimal(resuMap.get("commissionReward"));
                    Map<String, Object> paramsMap = new HashMap<>();
                    paramsMap.put("orderId", "5028596123028160513");
                    paramsMap.put("buyerId", "308717663974002688");
                    paramsMap.put("goodsId", "5028439511340679168");
                    paramsMap.put("dType", AccountStatus.DISTRIBUTION_TYPE_00); // 直接分润

                    if (userFee.compareTo(new BigDecimal(0)) == 1) {
                        paramsMap.put("note", "用户返佣"); // 备注
                        paramsMap.put("recharType", "1"); // 1 用户返
                        // 用户预充值
                        accountService.accountTradePrechargeZy(mallOrderGoodsVo.getUserId(), userFee,
                                        CommonConstant.TASK_REWARD_CASH, AccountStatus.BID_27, paramsMap, null);
                    }

                }



                if (userFee.compareTo(surplusCashback) == 1) {
                    amout = surplusCashback;
                    cashbackAmount = cashbackAmount.add(surplusCashback);
                    surplusCashback = new BigDecimal(0);
                } else {
                    amout = userFee;
                    cashbackAmount = cashbackAmount.add(amout);
                    surplusCashback = surplusCashback.subtract(amout);
                }


                // todo将用户返现的钱再到队列用户去，调用预充值

                MallOrderGoods updateOrderGoods = new MallOrderGoods();
                updateOrderGoods.setId(mallOrderGoodsVo.getId());
                updateOrderGoods.setCashbackAmount(cashbackAmount);
                updateOrderGoods.setSurplusCashback(surplusCashback);
                orderGoodsService.updateById(updateOrderGoods);

                if (userFee.compareTo(amout) == 0) {
                    break;
                }

                // 返现给用户的钱递减
                if (userFee.compareTo(amout) == 1) {
                    userFee = userFee.subtract(amout);
                }

                // else {
                // break;
                // }
                if (new BigDecimal(0).compareTo(userFee) > -1) {
                    break;
                }

            }


        }


        return ResponseUtil.ok();

    }


    /**
     * 活动转链
     */
    @GetMapping("getActivityLink")
    public Object getActivityLink(@LoginUser String userId, @NotNull String aid, @NotNull String id) throws Exception {

        if (org.apache.commons.lang.StringUtils.isBlank(userId)) {
            return ResponseUtil.unlogin();
        }

        if (org.apache.commons.lang.StringUtils.isBlank(aid)) {
            return ResponseUtil.badArgument();
        }

        Long activityId = Long.parseLong(aid);

        try {
            MallUser user = userService.findById(userId);
            if (com.graphai.commons.util.obj.ObjectUtils.isNotNull(user)
                            && StringUtils.isNotEmpty(user.getTbkRelationId())) {
                TaobaoClient client = new DefaultTaobaoClient(url, taoBaoProperties.getLianmengAppKey(),
                                taoBaoProperties.getLianmengAppSecret());
                TbkActivitylinkGetRequest req = new TbkActivitylinkGetRequest();
                req.setPlatform(2L);
                req.setAdzoneId(110282750499L);
                req.setPromotionSceneId(activityId);
                req.setRelationId(user.getTbkRelationId());
                TbkActivitylinkGetResponse rsp = client.execute(req);
                System.out.println(rsp.getBody());

                logger.info("获取活动转链------------" + JacksonUtils.bean2Jsn(rsp));
                if (rsp.getData() != null) {
                    MallAd ad = mallAdMapper.selectByPrimaryKey(id);
                    if (ObjectUtil.isNotNull(ad)) {
                        ad.setLink("/pages/web/webview?link=" + rsp.getData());
                        if (mallAdMapper.updateByPrimaryKey(ad) > 0) {
                            return ResponseUtil.ok("转链成功");
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw e;
        }
        return ResponseUtil.fail(506, "转链失败!");

    }


    /**
     * 选品库
     *
     */
    // @PostMapping("getFavorites")
    // public Object getFavorites() throws Exception {
    //
    // try {
    // TaobaoClient client = new DefaultTaobaoClient(url,
    // taoBaoProperties.getLianmengAppKey(),taoBaoProperties.getLianmengAppSecret());
    // TbkUatmFavoritesGetRequest req = new TbkUatmFavoritesGetRequest();
    // req.setPageNo(1L);
    // req.setPageSize(20L);
    // req.setFields("favorites_title,favorites_id,type");
    // req.setType(1L);
    // TbkUatmFavoritesGetResponse rsp = client.execute(req);
    // logger.info("选品库列表------------" + JacksonUtils.bean2Jsn(rsp));
    //
    // return ResponseUtil.ok(rsp.getData());
    // } catch (Exception e) {
    // logger.error(e.getMessage(), e);
    // throw e;
    // }
    //
    // }


    @GetMapping("lxsdTest")
    public Object getPublisherPid(String orderId) {
        // MallOrder mallOrder = mallOrderService.findById(orderId);
        // if (OrderUtil.STATUS_RESULT_SUCCESS.equals(mallOrder.getOrderThreeStatus())) {
        // return AccountQYUtils.preChargeQYStart(orderId);// 5004661198000488449
        // } else {
        // return ResponseUtil.fail("订单不存在，或者三方状态=" + mallOrder.getOrderThreeStatus());
        // }


        return AccountQYUtils.preChargeQYStart(orderId);
    }


    @GetMapping("testZeroGoods")
    public Object testZeroGoods() {
        Map<String, Object> zeroGoodMap = mallGoodsService.findBySrcGoodsIdV2("600544893018");
        BigDecimal returnFee = BigDecimal.ZERO;
        if (!zeroGoodMap.isEmpty()) {
            if (!Objects.equals(zeroGoodMap.get("counterPrice"), null)
                            && !Objects.equals(zeroGoodMap.get("couponDiscount"), null)) {
                if (new BigDecimal(String.valueOf(zeroGoodMap.get("counterPrice")))
                                .compareTo(new BigDecimal(String.valueOf(zeroGoodMap.get("couponDiscount")))) == 1) {
                    returnFee = new BigDecimal(String.valueOf(zeroGoodMap.get("counterPrice")))
                                    .subtract(new BigDecimal(String.valueOf(zeroGoodMap.get("couponDiscount"))));
                    if (returnFee.compareTo(new BigDecimal(0)) == 1) {

                    }
                }
            }
        }
        return ResponseUtil.ok(zeroGoodMap);
    }



}
