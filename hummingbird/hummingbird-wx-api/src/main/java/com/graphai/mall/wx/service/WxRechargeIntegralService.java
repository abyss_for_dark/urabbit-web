package com.graphai.mall.wx.service;

import com.github.binarywang.wxpay.bean.notify.WxPayNotifyResponse;
import com.github.binarywang.wxpay.bean.notify.WxPayOrderNotifyResult;
import com.github.binarywang.wxpay.bean.order.WxPayMpOrderResult;
import com.github.binarywang.wxpay.bean.request.WxPayUnifiedOrderRequest;
import com.github.binarywang.wxpay.bean.result.BaseWxPayResult;
import com.github.binarywang.wxpay.config.WxPayConfig;
import com.github.binarywang.wxpay.constant.WxPayConstants;
import com.github.binarywang.wxpay.exception.WxPayException;
import com.github.binarywang.wxpay.service.WxPayService;
import com.graphai.commons.util.IpUtil;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.json.JacksonUtil;
import com.graphai.mall.admin.domain.MallSystemRule;
import com.graphai.mall.admin.service.IGameRedHourRuleService;
import com.graphai.mall.db.domain.*;
import com.graphai.mall.db.service.account.AccountService;
import com.graphai.mall.db.constant.account.AccountStatus;
import com.graphai.mall.db.service.account.FcAccountCapitalBillService;
import com.graphai.mall.db.service.groupon.MallGrouponService;
import com.graphai.mall.db.service.order.MallOrderGoodsService;
import com.graphai.mall.db.service.order.MallOrderNotifyLogService;
import com.graphai.mall.db.service.order.MallOrderService;
import com.graphai.mall.db.service.user.MallUserFormIdService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.constant.CommonConstant;
import com.graphai.mall.db.util.account.AccountUtils;
import com.graphai.properties.WxProperties;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;

import static com.graphai.mall.wx.util.WxResponseCode.*;
@Service
public class WxRechargeIntegralService {
	private final Log logger = LogFactory.getLog(WxRechargeIntegralService.class);

	@Autowired
	private WxProperties properties;

	@Autowired
	private MallOrderNotifyLogService logService;
	@Autowired
	private MallUserService userService;
	@Autowired
	private MallOrderService orderService;
	@Autowired
	private MallOrderGoodsService orderGoodsService;
	@Autowired
	private MallUserFormIdService formIdService;
	@Autowired
	private MallGrouponService grouponService;
	@Autowired
	private IGameRedHourRuleService redHourRuleService;
	@Resource(name="hsWxPayService")
	private WxPayService hsWxPayService;
	@Autowired
	private MallOrderGoodsService mallOrderGoodsService;
	@Autowired
	private FcAccountCapitalBillService fcAccountCapitalBillService;
	@Autowired
	private AccountService accountService;
	/**
	 * 列表
	 *
	 * @param userId
	 *            用户ID
	 * @return 列表
	 */
	public Object list(String userId) {
		if (userId == null) {
			return ResponseUtil.unlogin();
		}
		FcAccount fcAccount = accountService.getAccountByKey(userId);

		List<MallSystemRule> mallSystemRules = redHourRuleService.findByRuleId(6);
		Map<String,Object> map = new HashMap<>();
		map.put("integral", fcAccount.getIntegralAmount());
		map.put("mallSystemRules", mallSystemRules);
		return ResponseUtil.ok(map);
	}

	/**
	 * 积分充值流水列表
	 * @param userId
	 * @param page
	 * @param limit
	 * @return
	 */
	public Object rechargeList(String userId,Integer page, Integer limit, String sort,String order) {
		if (userId == null) {
			return ResponseUtil.unlogin();
		}

		// 查询账户信息
		FcAccount fcAccount = accountService.getAccountByKey(userId);
		if (fcAccount == null) {
			return ResponseUtil.badArgumentValue("未查询到账户信息！");
		}

		FcAccountCapitalBill fcAccountCapitalBill = new FcAccountCapitalBill();
		fcAccountCapitalBill.setAccountId(fcAccount.getAccountId());
		fcAccountCapitalBill.setRechargeType(CommonConstant.TASK_REWARD_INTEGRAL);
		List<FcAccountCapitalBill> capitalList = fcAccountCapitalBillService
				.selectFcAccountCapitalBillList(fcAccountCapitalBill, page, limit, sort, order);
		Map<String, Object> map = new HashMap<>();
		map.put("capitalList", capitalList);
		return ResponseUtil.ok(map);
	}


	/**
	 * 充值积分预支付
	 * @param body
	 * @param request
	 * @return
	 */
	@Transactional
	public Object setMealPrepay(String body, HttpServletRequest request) {
		//判断用户信息
		String mobile = JacksonUtil.parseString(body, "mobile");
		if (mobile == null || "".equals(mobile)) {
			return ResponseUtil.fail(501,"手机号码不能为空!");
		}
		String userId="";
		List<MallUser> users = userService.queryByMobile(mobile);
		if (CollectionUtils.isNotEmpty(users)) {
			if (org.apache.commons.lang3.StringUtils.isNotEmpty(users.get(0).getId())) {
				userId = users.get(0).getId();
			}
		}else {
			logger.info("1、用户不存在!");
			return ResponseUtil.fail(501,"用户不存在!");
		}
		if (userId == null || userId.equals("")) {
			logger.info("2、用户不存在!userId为空！");
			return ResponseUtil.unlogin();
		}

		//套餐id
		String ruleId = JacksonUtil.parseString(body, "ruleId");
		if (ruleId == null || "".equals(ruleId)) {
			return ResponseUtil.badArgument();
		}

		WxPayMpOrderResult result = null;
		String prepayId = null;
		try {
			//目前只用到这个
			//（5）微信小程序支付先获取小程序Openid
			String subOpenid = JacksonUtil.parseString(body, "subOpenid");
			if (subOpenid == null || "".equals(subOpenid)) {
				logger.info("7、订单不能支付，subOpenid参数为空！subOpenid="+subOpenid);
				return ResponseUtil.fail("订单不能支付，subOpenid参数为空！");
			}

			// 获取积分套餐数据
			MallSystemRule mallSystemRule = redHourRuleService.getById(ruleId);
			if (mallSystemRule == null) {
				return ResponseUtil.badArgumentValue();
			}

			// 生成订单
			MallOrderGoods mallOrderGoods = new MallOrderGoods();
			mallOrderGoods.setUserId(userId); // userId
			mallOrderGoods.setGoodsId(ruleId); // 套餐id
			mallOrderGoods.setOrderId(this.generateOrderSn(userId));// 订单流水号
			mallOrderGoods.setPrice(mallSystemRule.getProportion()); // 充值金额
			mallOrderGoods.setCouponPrice(mallSystemRule.getShareProportion()); // 充值积分
			mallOrderGoods.setGoodsSn("integral"); // 积分类型
			mallOrderGoodsService.add(mallOrderGoods);

			WxPayUnifiedOrderRequest orderRequest = new WxPayUnifiedOrderRequest();
			orderRequest.setOutTradeNo(mallOrderGoods.getOrderId());
			orderRequest.setSubOpenid(subOpenid);
			orderRequest.setBody("订单：" + mallOrderGoods.getOrderId());

			// 元转成分
			int fee = 0;
			BigDecimal actualPrice = mallSystemRule.getProportion();
			fee = actualPrice.multiply(new BigDecimal(100)).intValue();
			orderRequest.setTotalFee(fee);
			orderRequest.setSpbillCreateIp(IpUtil.getIpAddr(request));

			//自营商城支付参数
			WxPayConfig payConfig = new WxPayConfig();
			payConfig.setAppId(properties.getHsAppId());
			payConfig.setMchId(properties.getHsMchId());
			payConfig.setSubAppId(properties.getHsAppletsAppId());
			payConfig.setSubMchId(properties.getHsChildMchId());
			payConfig.setMchKey(properties.getHsMchKey());
			payConfig.setNotifyUrl(properties.getIntegralSetMealNotifyUrl());
			payConfig.setKeyPath(properties.getKeyPath());
			payConfig.setTradeType("JSAPI");
			payConfig.setSignType("MD5");
			hsWxPayService.setConfig(payConfig);
			result = hsWxPayService.createOrder(orderRequest);

			// 缓存prepayID用于后续模版通知
			prepayId = result.getPackageValue();
			prepayId = prepayId.replace("prepay_id=", "");

			MallUserFormid userFormid = new MallUserFormid();
			userFormid.setOpenid(subOpenid);
			userFormid.setFormid(prepayId);
			userFormid.setIsprepay(true);
			userFormid.setUseamount(3);
			userFormid.setExpireTime(LocalDateTime.now().plusDays(7));
			formIdService.addUserFormid(userFormid);
			logger.info("10、预支付请求成功。");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("11、订单不能支付，原因："+e.getMessage());
			return ResponseUtil.fail(ORDER_PAY_FAIL, "订单不能支付");
		}
		logger.info("13、预支付请求成功。返回结果："+result);
		return ResponseUtil.ok(result);
	}

	/**
	 * 充值积分回调接口
	 * <p>
	 * 1. 检测当前订单是否是付款状态; 2. 设置订单付款成功状态相关信息; 3. 响应微信商户平台.
	 *
	 * @param request
	 *            请求内容
	 * @param response
	 *            响应内容
	 * @return 操作结果
	 */
	@Transactional
	public Object setMealPayNotify(HttpServletRequest request, HttpServletResponse response) {
		logger.error("-------------------积分充值付款成功或失败回调接口------------------------");

		MallOrderNotifyLog log = new MallOrderNotifyLog();
		String xmlResult = null;
		try {
			xmlResult = IOUtils.toString(request.getInputStream(), request.getCharacterEncoding());
			log.setWxParams(xmlResult);

		} catch (IOException e) {
			e.printStackTrace();
			try {
				log.setErrMsg("微信通知参数解析异常：" + e.getMessage());
				log.setIsSuccess("0F");
				logService.add(log);
			} catch (Exception e1) {
				logger.error("日志添加异常: "+e1.getMessage(),e1);
			}
			return WxPayNotifyResponse.fail(e.getMessage());
		}

		WxPayOrderNotifyResult result = null;
		try {
			result = hsWxPayService.parseOrderNotifyResult(xmlResult);

			if (!WxPayConstants.ResultCode.SUCCESS.equals(result.getResultCode())) {
				logger.error(xmlResult);
				throw new WxPayException("微信通知支付失败！");
			}
			if (!WxPayConstants.ResultCode.SUCCESS.equals(result.getReturnCode())) {
				logger.error(xmlResult);
				throw new WxPayException("微信通知支付失败！");
			}
		} catch (WxPayException e) {
			try {
				log.setErrMsg("微信通知参数解析异常：" + e.getMessage());
				log.setIsSuccess("0F");
				logService.add(log);
			} catch (Exception e1) {
				logger.error("日志添加异常: "+e1.getMessage(),e1);
			}
			e.printStackTrace();
			return WxPayNotifyResponse.fail(e.getMessage());
		}

		logger.error("处理腾讯支付平台的订单支付");
		logger.error(result);

		String orderSn = result.getOutTradeNo();
		String payId = result.getTransactionId();
		log.setOrderSn(orderSn);

		// 获取订单数据
		MallOrderGoods mallOrderGoods = mallOrderGoodsService.findByGoodsSn(orderSn);
		try {
			if (mallOrderGoods == null) {
				try {
					log.setErrMsg("订单不存在 sn=" + orderSn);
					log.setIsSuccess("0F");
					logService.add(log);
				} catch (Exception e1) {
					logger.error("日志添加异常: "+e1.getMessage(),e1);
				}
				return WxPayNotifyResponse.fail("订单不存在 sn=" + orderSn);
			}

			// 分转化成元
			String totalFee = BaseWxPayResult.fenToYuan(result.getTotalFee());
			// 检查支付订单金额
			if (!totalFee.equals(mallOrderGoods.getPrice().toString())) {
				try {
					log.setErrMsg(mallOrderGoods.getOrderId() + " : 支付金额不符合 totalFee=" + totalFee);
					log.setIsSuccess("0F");
					logService.add(log);
				} catch (Exception e1) {
					logger.error("日志添加异常: "+e1.getMessage(),e1);
				}
				return WxPayNotifyResponse.fail(mallOrderGoods.getOrderId() + " : 支付金额不符合 totalFee=" + totalFee);
			}

			// 修改订单状态
			mallOrderGoods.setProductId(payId); // 支付id
			mallOrderGoods.setUpdateTime(LocalDateTime.now()); // 支付时间
			mallOrderGoodsService.updateById(mallOrderGoods);

			String userId = mallOrderGoods.getUserId();
			// 积分
			BigDecimal integral = mallOrderGoods.getCouponPrice();

			// 账户变动-增加积分
			Map paramsMap = new HashMap();
			paramsMap.put("dType",AccountStatus.DISTRIBUTION_TYPE_00);  //直接分润
			paramsMap.put("capitalTrend",AccountStatus.CAPITAL_TREND_01);
			paramsMap.put("changeType",AccountStatus.CHANGE_TYPE_01);
			paramsMap.put("changeAccountType",AccountStatus.CHANGE_ACCOUNT_TYPE_2);
			paramsMap.put("message","充值"+ integral +"积分");
			AccountUtils.accountChange(userId, integral, CommonConstant.TASK_REWARD_INTEGRAL,AccountStatus.BID_51,paramsMap);

			try {
				log.setRemark("订单支付通知计算完成");
				log.setIsSuccess("1F");
				logService.add(log);
			} catch (Exception e1) {
				logger.error("日志添加异常: "+e1.getMessage(),e1);
			}
		} catch (Exception e) {
			try {
				log.setRemark("9、订单支付发生异常: " +e.getMessage());
				log.setIsSuccess("0F");
				logService.add(log);
			} catch (Exception e1) {
				logger.error("10、日志添加异常: "+e1.getMessage(),e1);
			}
			logger.error("11、支付订单通知处理异常 :" + e.getMessage(), e);
		}

		logger.info("12、积分商城异步通知业务全部处理完成！");
		return WxPayNotifyResponse.success("处理成功!");
	}

	/**
	 * 生产流水号
	 * @param userId
	 * @return
	 */
	private String generateOrderSn(String userId){
		// 毫秒级时间戳
		String orderSn = System.currentTimeMillis()+userId;
		return  orderSn;
	}
}
