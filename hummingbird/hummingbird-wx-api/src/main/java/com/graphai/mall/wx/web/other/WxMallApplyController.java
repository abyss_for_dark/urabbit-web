package com.graphai.mall.wx.web.other;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.RegexUtil;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.lang.StringUtils;
import com.graphai.mall.db.domain.BmdesignUserLevel;
import com.graphai.mall.db.domain.MallApply;
import com.graphai.mall.db.domain.MallUser;
import com.graphai.mall.db.service.MallApplyService;
import com.graphai.mall.db.constant.account.AccountStatus;
import com.graphai.mall.db.service.bmdesign.BmdesignUserLevelService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.vo.MallApplyVo;
import com.graphai.mall.db.vo.MallUserAgentVo;
import com.graphai.mall.wx.annotation.LoginUser;

/**
 * 代理端_报单
 */
@RestController
@RequestMapping("/wx/mallApply")
@Validated
public class WxMallApplyController {
    private final Log logger = LogFactory.getLog(WxMallApplyController.class);

    @Autowired
    private MallApplyService mallApplyService;
    @Autowired
    private MallUserService mallUserService;
    @Autowired
    private BmdesignUserLevelService bmdesignUserLevelService;


    /**
     * 查询报单列表
     */
    @GetMapping("/list")
    public Object list(@LoginUser String userId, @RequestParam(required = false) String param, @RequestParam(required = false) String beginDate,
                    @RequestParam(required = false) String endDate, @RequestParam(required = false) Integer status, @RequestParam(defaultValue = "1") Integer page,
                    @RequestParam(defaultValue = "10") Integer limit) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        MallUser mallUser = mallUserService.findById(userId);
        String levelId = mallUser.getLevelId(); // 用户等级
        if (StringUtils.isEmpty(levelId)) {
            return ResponseUtil.badArgumentValue("用户身份有误！");
        }
        String province = mallUser.getProvince(); // 省
        String city = mallUser.getCity(); // 市
        String county = mallUser.getCounty(); // 区
        List<MallApplyVo> list = new ArrayList<>();
        if (levelId.equals(AccountStatus.LEVEL_4)) {
            // 服务商
            list = mallApplyService.selectMallApplyVoList(userId, param, beginDate, endDate, page, limit, status, null, null, null);
        } else if (levelId.equals(AccountStatus.LEVEL_5)) {
            // 区级代理
            list = mallApplyService.selectMallApplyVoList(userId, param, beginDate, endDate, page, limit, status, province, city, county);
        } else if (levelId.equals(AccountStatus.LEVEL_6)) {
            // 市级代理
            list = mallApplyService.selectMallApplyVoList(userId, param, beginDate, endDate, page, limit, status, province, city, null);
        } else {
            return ResponseUtil.badArgumentValue("权限不足！");
        }

        // 各个等级只能看到自己对应的奖励金额
        if (list.size() > 0) {
            if (levelId.equals(AccountStatus.LEVEL_5)) {
                for (MallApplyVo mallApplyVo : list) {
                    if (!mallApplyVo.getApplyUserId().equals(userId)) {
                        mallApplyVo.setActualReward(mallApplyVo.getQactualReward());
                    }
                }
            } else if (levelId.equals(AccountStatus.LEVEL_6)) {
                for (MallApplyVo mallApplyVo : list) {
                    if (!mallApplyVo.getApplyUserId().equals(userId)) {
                        mallApplyVo.setActualReward(mallApplyVo.getSactualReward());
                    }

                }
            }
        }


        long total = PageInfo.of(list).getTotal();
        Map<String, Object> data = new HashMap<>();
        data.put("total", total);
        data.put("items", list);
        data.put("userId", userId);
        return ResponseUtil.ok(data);
    }

    /**
     * 创建个人报单
     */
    @PostMapping("/add")
    public Object add(@LoginUser String userId, @RequestBody MallApply mallApply) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        Object error = validate(mallApply);
        if (error != null) {
            return error;
        }

        /** todo 0.判断手机号是否已经注册 **/
        // List<MallUser> mallUserList = mallUserService.queryByMobile(mallApply.getMobile());
        // if(mallUserList.size() > 0){
        // return ResponseUtil.badArgumentValue("手机号已经注册！");
        // }

        MallUser mallUser = mallUserService.findById(userId);
        String levelId = mallUser.getLevelId(); // 用户等级
        if (StringUtils.isEmpty(levelId)) {
            return ResponseUtil.badArgumentValue("用户身份有误！");
        }
        String province = mallUser.getProvince(); // 省
        String city = mallUser.getCity(); // 市
        String county = mallUser.getCounty(); // 区

        String province1 = mallApply.getProvince(); // 报单_省
        String city1 = mallApply.getCity(); // 报单_市
        String county1 = mallApply.getCounty(); // 报单_区
        String levelId1 = mallApply.getLevelId();
        BigDecimal money = mallApply.getMoney(); // 报单金额


        /** todo 1.验证是否符合报单条件 **/
        // if(levelId1.equals(AccountStatus.LEVEL_5)){
        // //区级代理
        // //判断报单用户 是否存在区代
        // List<MallUserAgentVo> qUserList = mallUserService.getCountByCondition(province1, city1, county1,
        // AccountStatus.LEVEL_5);
        // if(qUserList.size() > 0){
        // return ResponseUtil.badArgumentValue("该区域已存在区级代理！");
        // }
        // }else if(levelId1.equals(AccountStatus.LEVEL_6)){
        // //市级代理
        // //判断报单用户 是否存在市代
        // List<MallUserAgentVo> sUserList = mallUserService.getCountByCondition(province1, city1, null,
        // AccountStatus.LEVEL_6);
        // if(sUserList.size() > 0){
        // return ResponseUtil.badArgumentValue("该区域已存在市级代理！");
        // }
        // }


        Byte status = 2; // 管理员待审核(默认)
        BigDecimal rate = new BigDecimal(0); // 协商报单(默认)
        BigDecimal rate0 = new BigDecimal(0.5); // 跨区报单
        BigDecimal rate1 = new BigDecimal(0.2); // 跨区报单_市代、区代同时存在
        BigDecimal rate2 = new BigDecimal(0.4); // 跨区报单_只有区代
        BigDecimal rate3 = new BigDecimal(0.3); // 跨区报单_只有市代
        Boolean isCross = false; // 是否跨区
        String qUserId = null; // 区级代理id
        String sUserId = null; // 市级代理id


        /**
         * 2.1 判断用户报单是否跨区 2.2 判断用户身份 2.3 判断用户报单奖励金额
         **/
        // 判断是否存在区代(根据报单区域)
        List<MallUserAgentVo> qUserList = mallUserService.getCountByCondition(false, null, province1, city1, county1, AccountStatus.LEVEL_5);
        // 判断是否存在市代(根据报单区域)
        List<MallUserAgentVo> sUserList = mallUserService.getCountByCondition(false, null, province1, city1, null, AccountStatus.LEVEL_6);
        int qCount = qUserList.size();
        int sCount = sUserList.size();

        if (province.equals(province1) && city.equals(city1)) {
            /** 本省本市 **/
            if (levelId.equals(AccountStatus.LEVEL_4)) {
                /** (1)报单人为服务商 **/
                if (qCount > 0 && sCount > 0) {
                    // 同时存在市代、区代
                    status = 0; // 区代待审核
                    qUserId = qUserList.get(0).getId();
                    sUserId = sUserList.get(0).getId();
                } else if (qCount > 0 && sCount == 0) {
                    // 仅存在区代
                    status = 0; // 区代待审核
                    qUserId = qUserList.get(0).getId();
                } else if (qCount == 0 && sCount > 0) {
                    // 仅存在市代
                    status = 1; // 市代待审核
                    sUserId = sUserList.get(0).getId();
                } else {
                    // 无市代、无区代
                    rate = rate0;
                }
            } else if (levelId.equals(AccountStatus.LEVEL_5)) {
                /** (2)报单人为区级代理 **/
                if (sCount > 0) {
                    status = 1; // 市代待审核
                    sUserId = sUserList.get(0).getId();
                } else {
                    rate = rate0;
                }
            }

        } else {
            /** 跨区 **/
            isCross = true;
            rate = rate0;
            if (qCount > 0 && sCount > 0) {
                // rate = rate1;
                qUserId = qUserList.get(0).getId();
                sUserId = sUserList.get(0).getId();
            } else if (qCount > 0 && sCount == 0) {
                // rate = rate2;
                qUserId = qUserList.get(0).getId();
            } else if (qCount == 0 && sCount > 0) {
                // rate = rate3;
                sUserId = sUserList.get(0).getId();
            }
        }

        BigDecimal reward = money.multiply(rate).setScale(2, BigDecimal.ROUND_DOWN); // 跨省或跨市或跨区(奖励金额为 报单金额的一半)
        mallApply.setApplyUserId(userId);
        mallApply.setStatus(status);
        mallApply.setReward(reward);
        mallApply.setRate(rate);

        mallApply.setIsCross(isCross);
        mallApply.setsUserId(sUserId);
        mallApply.setqUserId(qUserId);
        mallApplyService.insertMallApply(mallApply);
        return ResponseUtil.ok();
    }

    // @PostMapping("/add")
    // public Object add(@LoginUser String userId,
    // @RequestBody MallApply mallApply) {
    // if (userId == null) {
    // return ResponseUtil.unlogin();
    // }
    // Object error = validate(mallApply);
    // if (error != null) {
    // return error;
    // }
    //
    // /**todo 0.判断手机号是否已经注册 **/
    //// List<MallUser> mallUserList = mallUserService.queryByMobile(mallApply.getMobile());
    //// if(mallUserList.size() > 0){
    //// return ResponseUtil.badArgumentValue("手机号已经注册！");
    //// }
    //
    // MallUser mallUser = mallUserService.findById(userId);
    // String levelId = mallUser.getLevelId(); //用户等级
    // if (StringUtils.isEmpty(levelId)) {
    // return ResponseUtil.badArgumentValue("用户身份有误！");
    // }
    // String province = mallUser.getProvince(); //省
    // String city = mallUser.getCity(); //市
    // String county = mallUser.getCounty(); //区
    //
    // String province1 = mallApply.getProvince(); //报单_省
    // String city1 = mallApply.getCity(); //报单_市
    // String county1 = mallApply.getCounty(); //报单_区
    // String levelId1 = mallApply.getLevelId();
    // BigDecimal money = mallApply.getMoney(); //报单金额
    //
    //
    // /**todo 1.验证是否符合报单条件 **/
    //// if(levelId1.equals(AccountStatus.LEVEL_5)){
    //// //区级代理
    //// //判断报单用户 是否存在区代
    //// List<MallUserAgentVo> qUserList = mallUserService.getCountByCondition(province1, city1,
    // county1, AccountStatus.LEVEL_5);
    //// if(qUserList.size() > 0){
    //// return ResponseUtil.badArgumentValue("该区域已存在区级代理！");
    //// }
    //// }else if(levelId1.equals(AccountStatus.LEVEL_6)){
    //// //市级代理
    //// //判断报单用户 是否存在市代
    //// List<MallUserAgentVo> sUserList = mallUserService.getCountByCondition(province1, city1, null,
    // AccountStatus.LEVEL_6);
    //// if(sUserList.size() > 0){
    //// return ResponseUtil.badArgumentValue("该区域已存在市级代理！");
    //// }
    //// }
    //
    //
    // Byte status = 2; //管理员待审核(默认)
    // BigDecimal rate = new BigDecimal(0.5); //跨区报单(默认)
    // BigDecimal rate1 = new BigDecimal(0.2); //跨区报单_市代、区代同时存在
    // BigDecimal rate2 = new BigDecimal(0.4); //跨区报单_只有区代
    // BigDecimal rate3 = new BigDecimal(0.3); //跨区报单_只有市代
    // BigDecimal rate4 = new BigDecimal(1); //本区
    // /** 2.1 判断用户报单是否跨区
    // * 2.2 判断用户身份
    // * 2.3 判断用户报单奖励金额
    // * **/
    // if(AccountStatus.LEVEL_4.equals(levelId) || AccountStatus.LEVEL_5.equals(levelId)){
    // /** (1)服务商、区代 **/
    // if(province.equals(province1) && city.equals(city1) && county.equals(county1)){
    // /** 本省本市本区 **/
    // if(levelId.equals(AccountStatus.LEVEL_4)){
    // //(1)报单人为服务商
    // //判断当前用户 是否存在区代
    // int qCount = mallUserService.getCountByCondition(province, city, county,
    // AccountStatus.LEVEL_5).size();
    // //判断当前用户 是否存在市代
    // int sCount = mallUserService.getCountByCondition(province, city, null,
    // AccountStatus.LEVEL_6).size();
    // if(qCount > 0 && sCount > 0){
    // //同时存在市代、区代
    // status = 0; //区代待审核
    // }else if(qCount > 0 && sCount == 0){
    // //仅存在区代
    // status = 0; //区代待审核
    // }else if(qCount == 0 && sCount > 0){
    // //仅存在市代
    // status = 1; //市代待审核
    // }
    //
    // }else if(levelId.equals(AccountStatus.LEVEL_5)){
    // //(2)报单人为区级代理
    // //判断当前用户 是否存在市代
    // int sCount = mallUserService.getCountByCondition(province, city, null,
    // AccountStatus.LEVEL_6).size();
    // if(sCount > 0){
    // //todo --------rate 比例与市级代理协商----------
    // //rate
    //
    // status = 1; //市代待审核
    // }else{
    // rate = rate4;
    // }
    // }
    // }else{
    // /** 跨区 **/
    // //判断跨区 是否存在区代
    // int qCount = mallUserService.getCountByCondition(province1, city1, county1,
    // AccountStatus.LEVEL_5).size();
    // //判断跨区 是否存在市代
    // int sCount = mallUserService.getCountByCondition(province1, city1, null,
    // AccountStatus.LEVEL_6).size();
    // if(qCount > 0 && sCount > 0){
    // rate = rate1;
    // }else if(qCount > 0 && sCount == 0){
    // rate = rate2;
    // }else if(qCount == 0 && sCount > 0){
    // rate = rate3;
    // }
    // }
    //
    // }else{
    // /** (2)市代 **/
    // if(province.equals(province1) && city.equals(city1)){
    // /** 本省本市本区 **/
    // rate = rate4;
    // }else{
    // /** 跨区 **/
    // //判断跨区 是否存在区代
    // int qCount = mallUserService.getCountByCondition(province1, city1, county1,
    // AccountStatus.LEVEL_5).size();
    // //判断跨区 是否存在市代
    // int sCount = mallUserService.getCountByCondition(province1, city1, null,
    // AccountStatus.LEVEL_6).size();
    // if(qCount > 0 && sCount > 0){
    // rate = rate1;
    // }else if(qCount > 0 && sCount == 0){
    // rate = rate2;
    // }else if(qCount == 0 && sCount > 0){
    // rate = rate3;
    // }
    // }
    // }
    //
    //
    // BigDecimal reward = money.multiply(rate).setScale(2,BigDecimal.ROUND_DOWN); //跨省或跨市或跨区(奖励金额为
    // 报单金额的一半)
    // mallApply.setApplyUserId(userId);
    // mallApply.setStatus(status);
    // mallApply.setReward(reward);
    // mallApply.setRate(rate);
    // mallApplyService.insertMallApply(mallApply);
    // return ResponseUtil.ok();
    // }

    /**
     * 查询审核中的报单列表
     */
    @GetMapping("/applyList")
    public Object applyList(@LoginUser String userId, @RequestParam(required = false) String param, @RequestParam(defaultValue = "1") Integer page,
                    @RequestParam(defaultValue = "10") Integer limit) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        MallUser mallUser = mallUserService.findById(userId);
        String levelId = mallUser.getLevelId(); // 用户等级
        if (StringUtils.isEmpty(levelId)) {
            return ResponseUtil.badArgumentValue("用户身份有误！");
        }
        String province = mallUser.getProvince(); // 省
        String city = mallUser.getCity(); // 市
        String county = mallUser.getCounty(); // 区

        List<MallApplyVo> list = null;
        // 判断用户身份
        // if(levelId.equals(AccountStatus.LEVEL_5)){
        // //区代
        // list =
        // mallApplyService.selectMallApplyVoList2(param,null,null,page,limit,0,province,city,county);
        // }else if(levelId.equals(AccountStatus.LEVEL_6)){
        // //市代
        // list = mallApplyService.selectMallApplyVoList2(param,null,null,page,limit,1,province,city,null);
        // }else{
        // //服务商
        // return ResponseUtil.badArgumentValue("权限不足！");
        // }

        List<Integer> statusList = new ArrayList<>();
        statusList.add(0); // 区代待审核
        statusList.add(1); // 市代待审核
        statusList.add(2); // 管理员待审核
        if (levelId.equals(AccountStatus.LEVEL_5)) {
            // 区代
            list = mallApplyService.selectMallApplyVoList2(param, null, null, page, limit, statusList, province, city, county);
        } else if (levelId.equals(AccountStatus.LEVEL_6)) {
            // 市代
            list = mallApplyService.selectMallApplyVoList2(param, null, null, page, limit, statusList, province, city, null);
        } else {
            // 服务商
            return ResponseUtil.badArgumentValue("权限不足！");
        }
        long total = PageInfo.of(list).getTotal();
        Map<String, Object> data = new HashMap<>();
        data.put("total", total);
        data.put("items", list);
        data.put("levelId", levelId); // 用户等级
        return ResponseUtil.ok(data);
    }

    /**
     * 用户审核
     */
    @PostMapping("/update")
    public Object update(@LoginUser String userId, @RequestBody MallApply mallApply) {
        // 验证用户是否有权限审核其他的 报单
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        if (null == mallApply || null == mallApply.getStatus()) {
            return ResponseUtil.badArgument();
        }
        MallUser mallUser = mallUserService.findById(userId);
        if (null == mallUser || null == mallUser.getLevelId()) {
            return ResponseUtil.badArgumentValue("用户/用户等级不存在！");
        }
        MallApply apply = mallApplyService.selectMallApplyById(mallApply.getId());
        if (null == apply || null == apply.getStatus()) {
            return ResponseUtil.badArgumentValue("该报单申请不存在！");
        }
        Byte onOff = mallApply.getStatus(); // 驳回 0 通过 1
        String levelId = mallUser.getLevelId(); // 审核用户等级
        Byte status = apply.getStatus(); // 审核前报单状态
        if (AccountStatus.LEVEL_5.equals(levelId)) {
            // 区级代理
            if (0 == status) {
                mallApply.setStatus(onOff == (byte) 1 ? (byte) 1 : (byte) 0);
            } else {
                return ResponseUtil.badArgumentValue("操作权限不足！");
            }
        } else if (AccountStatus.LEVEL_6.equals(levelId)) {
            // 市级代理
            if (1 == status) {
                mallApply.setStatus(onOff == (byte) 1 ? (byte) 2 : (byte) 0);
            } else {
                return ResponseUtil.badArgumentValue("操作权限不足！");
            }
        } else {
            return ResponseUtil.badArgumentValue("操作权限不足！");
        }
        mallApplyService.updateMallApply(mallApply);
        return ResponseUtil.ok();
    }


    /**
     * 查询等级列表
     */
    @GetMapping("/getLevelList")
    public Object getLevelList() {
        BmdesignUserLevel bmdesignUserLevel = new BmdesignUserLevel();
        bmdesignUserLevel.setType("identity");
        List<BmdesignUserLevel> levelList = bmdesignUserLevelService.selectBmdesignUserLevelList(bmdesignUserLevel, null, null, "level_num", "asc");
        return ResponseUtil.ok(levelList);
    }


    /**
     * 返回报单价格 (是否官方价格(0:是 1:否) ) 规则： 1.跨省市：官方定价 2.未跨省市：判断报单的地区是否存在上级,若存在则自主协商;否则官方定价
     */
    @PostMapping("/applyMoney")
    public Object applyMoney(@LoginUser String userId, @RequestBody MallApply mallApply) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        if (null == mallApply || null == mallApply.getLevelId() || null == mallApply.getProvince() || null == mallApply.getCity() || null == mallApply.getCounty()) {
            return ResponseUtil.badArgumentValue("缺少参数！");
        }
        MallUser mallUser = mallUserService.findById(userId);
        if (null == mallUser || null == mallUser.getProvince() || null == mallUser.getCity() || null == mallUser.getCounty()) {
            return ResponseUtil.badArgumentValue("用户省市区不存在！");
        }
        String province = mallUser.getProvince(); // 报单人_省
        String city = mallUser.getCity(); // 报单人_市
        String county = mallUser.getCounty(); // 报单人_区

        String province1 = mallApply.getProvince(); // 报单_省
        String city1 = mallApply.getCity(); // 报单_市
        String county1 = mallApply.getCounty(); // 报单_区

        /** 验证用户是否本市本区的 报单 **/
        // Integer status = 0; //是否本市本区(0:否 1:是)
        Integer status = 0; // 是否官方价格(0:是 1:否)
        BigDecimal price = getApplyPrice(mallApply); // 报单的官方价格

        // 判断是否存在区代(根据报单区域)
        List<MallUserAgentVo> qUserList = mallUserService.getCountByCondition(false, null, province1, city1, county1, AccountStatus.LEVEL_5);
        // 判断是否存在市代(根据报单区域)
        List<MallUserAgentVo> sUserList = mallUserService.getCountByCondition(false, null, province1, city1, null, AccountStatus.LEVEL_6);
        int qCount = qUserList.size();
        int sCount = sUserList.size();

        if (province.equals(province1) && city.equals(city1)) {
            if (AccountStatus.LEVEL_4.equals(mallUser.getLevelId())) {
                // 服务商
                if (qCount > 0 || sCount > 0) {
                    status = 1;
                }
            } else if (AccountStatus.LEVEL_5.equals(mallUser.getLevelId())) {
                // 区级代理
                if (sCount > 0) {
                    status = 1;
                }
            } else {
                status = 1;
            }
        }

        Map<String, Object> map = new HashMap<>();
        map.put("status", status);
        map.put("price", price);
        return ResponseUtil.ok(map);
    }


    private BigDecimal getApplyPrice(MallApply mallApply) {
        BigDecimal defaultPrice = new BigDecimal(0);
        BigDecimal sPrice = new BigDecimal(680000); // 跨区_市代价格
        BigDecimal qPrice = new BigDecimal(80000); // 跨区_区代价格
        BigDecimal fPrice = new BigDecimal(9800); // 跨区_服务商价格
        if (AccountStatus.LEVEL_6.equals(mallApply.getLevelId())) {
            defaultPrice = sPrice;
        } else if (AccountStatus.LEVEL_5.equals(mallApply.getLevelId())) {
            defaultPrice = qPrice;
        } else if (AccountStatus.LEVEL_4.equals(mallApply.getLevelId())) {
            defaultPrice = fPrice;
        }
        return defaultPrice;
    }


    /**
     * 校验数据(添加)
     */
    private Object validate(MallApply mallApply) {
        // 身份
        if (StringUtils.isEmpty(mallApply.getLevelId())) {
            return ResponseUtil.badArgumentValue("请选择等级！");
        }
        // 用户名
        if (StringUtils.isEmpty(mallApply.getUserName())) {
            return ResponseUtil.badArgumentValue("请填写用户名！");
        }
        // 手机号
        if (StringUtils.isEmpty(mallApply.getMobile())) {
            return ResponseUtil.badArgumentValue("请填写手机号！");
        }
        if (!RegexUtil.isMobileExact(mallApply.getMobile())) {
            return ResponseUtil.badArgumentValue("请填写正确的手机号！");
        }
        // 省
        if (StringUtils.isEmpty(mallApply.getProvince())) {
            return ResponseUtil.badArgumentValue("请选择省！");
        }
        // 市
        if (StringUtils.isEmpty(mallApply.getCity())) {
            return ResponseUtil.badArgumentValue("请选择市！");
        }
        // 区
        if (StringUtils.isEmpty(mallApply.getCounty())) {
            return ResponseUtil.badArgumentValue("请选择区！");
        }
        // 报单价格
        if (mallApply.getMoney() == null) {
            return ResponseUtil.badArgumentValue("请填写报单金额！");
        }
        if (mallApply.getMoney().compareTo(new BigDecimal(0)) == -1) {
            return ResponseUtil.badArgumentValue("请填写正确的报单金额！");
        }
        return null;
    }



}
