package com.graphai.mall.wx.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.alibaba.fastjson.serializer.SerializerFeature;

import java.util.List;
import java.util.Map;

public final class JsonUtil {

	public static JSONArray JsonStrToJsonArray(String jsonStr) {
		if (isNotEmpty(jsonStr)) {
			return JSON.parseArray(jsonStr);
		}
		return null;
	}

	public static Map<String, Object> JsonStrToMap(String jsonStr) {
		if (isNotEmpty(jsonStr)) {
			return JSON.parseObject(jsonStr,
					new TypeReference<Map<String, Object>>() {
					});
		}
		return null;
	}

	public static JSONObject JsonStrToJSONObject(String jsonStr) {
		if (isNotEmpty(jsonStr)) {
			return JSON.parseObject(jsonStr);
		}
		return null;
	}

	public static String objectToJsonStr(Object obj) {
		if (null != obj) {
			return JSON.toJSONString(obj,
					SerializerFeature.DisableCircularReferenceDetect);
		}
		return null;
	}

	public static JSONArray objectToJSONArray(Object obj) {
		if (null != obj) {
			return (JSONArray) JSON.toJSON(obj);
		}
		return null;
	}

	public static JSONObject objectToJSONObject(Object obj) {
		if (null != obj) {
			return (JSONObject) JSON.toJSON(obj);
		}
		return null;
	}

	public static <T> T jsonStrToObject(Class<T> t, String jsonStr) {
		if (null != t && isNotEmpty(jsonStr)) {
			return JSON.parseObject(jsonStr, t);
		}
		return null;
	}

	public static <T> List<T> jsonStrToArray(Class<T> t, String jsonStr) {
		if (null != t && isNotEmpty(jsonStr)) {
			return JSON.parseArray(jsonStr, t);
		}
		return null;
	}

	public static <T> T jsonToObject(TypeReference<T> t, String jsonStr) {
		if (null != t && isNotEmpty(jsonStr)) {
			return JSON.parseObject(jsonStr, t);
		}
		return null;
	}

	public static boolean isNotEmpty(String str) {
		if (str == null || "".equals(str)) {
			return false;
		} else {
			return true;
		}

	}
}
