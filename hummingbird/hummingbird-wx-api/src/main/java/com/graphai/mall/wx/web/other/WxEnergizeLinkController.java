package com.graphai.mall.wx.web.other;


import com.graphai.commons.util.ResponseUtil;
import com.graphai.mall.db.domain.MallEnergizeRecharge;
import com.graphai.mall.db.domain.MallEnergizeRecord;
import com.graphai.mall.db.service.funeng.MallEnergizeService;
import com.graphai.mall.wx.annotation.LoginUser;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;

/**
 * 赋能链接
 *
 * @author LaoGF
 * @since 2020-07-27
 */
@RestController
@RequestMapping("/wx/energizeLink")
@Validated
public class WxEnergizeLinkController {

    @Resource
    private MallEnergizeService mallEnergizeService;


    /**
     * 会员在线充值
     */
    @PostMapping("/recharge")
    public Object recharge(@LoginUser String userId, @RequestBody MallEnergizeRecharge mallEnergizeRecharge) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        mallEnergizeRecharge.setUserId(userId);
        if(mallEnergizeService.recharge(mallEnergizeRecharge)){
            return ResponseUtil.ok("操作成功");
        }
        return ResponseUtil.fail();
    }

    /**
     * 生成赋能链接
     */
    @PostMapping("/expend")
    public Object expend(@LoginUser String userId, @RequestBody MallEnergizeRecord mallEnergizeRecord) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        mallEnergizeRecord.setUserId(userId);
        return ResponseUtil.ok(mallEnergizeService.createLink(mallEnergizeRecord));
    }

    /**
     * 查询生成记录(用户)
     */
    @GetMapping("/getRecord")
    public Object getRecord(@LoginUser String userId) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        return ResponseUtil.ok(mallEnergizeService.getRecord(userId));
    }

    /**
     * 查询生成记录(记录ID)
     */
    @GetMapping("/getRecordById")
    public Object getRecordById(HttpServletRequest request, @RequestParam String id) {
        String sessionId = request.getRequestedSessionId();
        return ResponseUtil.ok(mallEnergizeService.getRecordById(id));
    }



    /**
     * 查询会员用户信息
     */
    @GetMapping("/getDetail")
    public Object getDetail(@LoginUser String userId) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        return ResponseUtil.ok(mallEnergizeService.getDetail(userId));
    }


    /**
     * 查询所有会员用户
     */
//    @GetMapping("/getUser")
//    public Object getUser() {
//        return ResponseUtil.ok(mallEnergizeService.getUser());
//    }


}
