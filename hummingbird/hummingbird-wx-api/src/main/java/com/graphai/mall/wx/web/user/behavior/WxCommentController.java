package com.graphai.mall.wx.web.user.behavior;

import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gexin.fastjson.JSON;
import com.gexin.fastjson.JSONArray;
import com.gexin.fastjson.JSONObject;
import com.gexin.fastjson.TypeReference;
import com.graphai.commons.util.json.JacksonUtil;
import com.graphai.mall.admin.domain.MallPurchaseOrder;
import com.graphai.mall.admin.domain.MallPurchaseOrderGoods;
import com.graphai.mall.admin.service.IMallPurchaseOrderGoodsService;
import com.graphai.mall.admin.service.IMallPurchaseOrderService;
import com.graphai.mall.db.domain.*;
import com.graphai.mall.db.service.order.MallOrderGoodsService;
import com.graphai.mall.db.service.order.MallOrderService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.NameUtil;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.mall.db.service.goods.MallGoodsService;
import com.graphai.mall.db.service.promotion.MallTopicService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.service.user.behavior.MallCommentService;
import com.graphai.mall.db.service.user.behavior.MallFollowService;
import com.graphai.mall.db.vo.MallCommentVo;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.mall.wx.dto.UserInfo;
import com.graphai.mall.wx.service.UserInfoService;

/**
 * 用户评论服务
 */
@RestController
@RequestMapping("/wx/comment")
@Validated
public class WxCommentController {
    private final Log logger = LogFactory.getLog(WxCommentController.class);

    @Autowired
    private MallCommentService commentService;
    @Autowired
    private MallUserService userService;
    @Autowired
    private UserInfoService userInfoService;
    @Autowired
    private MallGoodsService goodsService;
    @Autowired
    private MallTopicService topicService;
    @Autowired
    private MallFollowService followService;
    @Autowired
    private MallOrderService mallOrderService;
    @Autowired
    private IMallPurchaseOrderService mallPurchaseOrderService;
    @Autowired
    private MallOrderGoodsService mallOrderGoodsService;
    @Autowired
    private IMallPurchaseOrderGoodsService mallPurchaseOrderGoodsService;

    private Object validate(MallComment comment) {
        String content = comment.getContent();
        if (StringUtils.isEmpty(content)) {
            return ResponseUtil.badArgument();
        }

        Byte star = comment.getStar();
        if (star == null) {
            return ResponseUtil.badArgument();
        }
        if (star < 0 || star > 5) {
            return ResponseUtil.badArgumentValue();
        }

        Byte type = comment.getType();
        String valueId = comment.getValueId();
        if (type == null || valueId == null) {
            return ResponseUtil.badArgument();
        }
        if (type == 0) {
            if (goodsService.findById(valueId) == null) {
                return ResponseUtil.badArgumentValue();
            }
        } else if (type == 1) {
            if (topicService.findById(valueId) == null) {
                return ResponseUtil.badArgumentValue();
            }
        } else {
            return ResponseUtil.badArgumentValue();
        }
        return null;
    }

    /**
     * 发表评论
     *
     * @param userId  用户ID
     * @param comment 评论内容
     * @return 发表评论操作结果
     */
    @PostMapping("post")
    public Object post(@LoginUser String userId, @RequestBody MallComment comment) {
        //String userId = "310007654159167488";
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        Object error = validate(comment);
        if (error != null) {
            return error;
        }
        comment.setUserId(userId);
        commentService.save(comment);
        return ResponseUtil.ok(comment);
    }

    /**
     * 评论数量
     *
     * @param type    类型ID。 如果是0，则查询商品评论；如果是1，则查询专题评论。
     * @param valueId 商品或专题ID。如果type是0，则是商品ID；如果type是1，则是专题ID。
     * @return 评论数量
     */
    @GetMapping("count")
    public Object count(@NotNull Byte type, @NotNull String valueId) {
        int allCount = commentService.count(type, valueId,0);
        int hasPicCount = commentService.count(type, valueId,1);
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("allCount", allCount);
        data.put("hasPicCount", hasPicCount);
        return ResponseUtil.ok(data);
    }

    /**
     * 评论列表
     *
     * @param type     类型ID。 如果是0，则查询商品评论；如果是1，则查询专题评论。
     * @param valueId  商品或专题ID。如果type是0，则是商品ID；如果type是1，则是专题ID。
     * @param showType 显示类型。如果是0，则查询全部；如果是1，则查询有图片或视频的评论。2:好评；3：差评
     * @param page     分页页数
     * @param limit    分页大小
     * @return 评论列表
     */
    @GetMapping("/list")
    public Object list(@NotNull Byte type,
                       @NotNull String valueId,
                       @NotNull Integer showType,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit) {
        List<MallComment> commentList = commentService.query(type, valueId, showType, page, limit);
        long count = PageInfo.of(commentList).getTotal();

        List<Map<String, Object>> commentVoList = new ArrayList<>(commentList.size());
        for (MallComment comment : commentList) {
            Map<String, Object> commentVo = new HashMap<>();
            commentVo.put("addTime", comment.getAddTime());
            commentVo.put("content", comment.getContent());
            commentVo.put("picList", comment.getPicUrls());
            commentVo.put("star", comment.getStar());
            commentVo.put("merchantStar", comment.getMerchantStar());

            UserInfo userInfo = userInfoService.getInfo(comment.getUserId());
            commentVo.put("userInfo", userInfo);

            String reply = commentService.queryReply(comment.getId());
            commentVo.put("reply", reply);

            List<MallOrderGoods> mallOrderGoods = mallOrderGoodsService.findByOidAndGid(comment.getTopId(),comment.getValueId());

            String specifications = "";
            if(mallOrderGoods.size() > 0){
                for (MallOrderGoods temp : mallOrderGoods) {
                    specifications += Arrays.toString(temp.getSpecifications());
                }
            }else{
                QueryWrapper<MallPurchaseOrderGoods> mallPurchaseOrderGoodsQueryWrapper = new QueryWrapper<MallPurchaseOrderGoods>();
                mallPurchaseOrderGoodsQueryWrapper.eq("order_id", comment.getTopId());
                mallPurchaseOrderGoodsQueryWrapper.eq("goods_id", comment.getValueId());
                List<MallPurchaseOrderGoods> mallPurchaseOrderGoodsList = mallPurchaseOrderGoodsService.list(mallPurchaseOrderGoodsQueryWrapper);

                for (MallPurchaseOrderGoods temp : mallPurchaseOrderGoodsList) {
                    specifications += temp.getSpecifications();
                }
            }
            commentVo.put("specifications", specifications);
            commentVo.put("reply", reply);

            commentVoList.add(commentVo);
        }
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("data", commentVoList);
        data.put("count", count);
        data.put("currentPage", page);
        return ResponseUtil.ok(data);
    }

    /**
     * 抖货视频评论列表
     * @param userId
     * @param id 抖货id/评论id/回复id
     * @param page
     * @param limit
     * @return
     */
    @GetMapping("/video/list")//LoginUser
    public Object list(@RequestParam(required = false) String userId,
                       @NotNull String id,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit,
                       @RequestParam(defaultValue = "asc") String sortType,
                        Integer type) {
        MallCommentVo query = new MallCommentVo();
        query.setId(id);
        query.setUserId(userId);
        query.setSortType(sortType);
        query.setType(type);

        PageHelper.startPage(page, limit);
        List<MallCommentVo> list = commentService.selectVideoCommentList(query);

        MallGoods video = goodsService.selectOneById(id);
        List<String> ids = list.stream().map(MallCommentVo::getId).collect(Collectors.toList());
        if (ids.size() > 0) {
            // 如果是获取评论列表而非回复列表, 返回评论的回复总数
            //List<MallCommentVo> replayNums = video == null ? null : commentService.getCommentReplayNum(ids);
            List<MallCommentVo> replayNums =  commentService.getCommentReplayNum(ids);
            List<MallCommentVo> isLikes = userId == null ? null : commentService.selectIsLikeComment(ids, userId);
            List<MallCommentVo> likeNums = commentService.selectLikeNum(ids);
            list.forEach(comment -> {
                if (replayNums != null) {
                    for (MallCommentVo vo : replayNums) {
                        if (vo.getId().equals(comment.getId())) {
                            comment.setReplyNum(vo.getReplyNum());
                        }
                    }
                }
                if(comment.getUserId().equals(userId)){
                    comment.setIsMe(true);
                }else{
                    comment.setIsMe(false);
                }
                if (isLikes != null) {
                    for (MallCommentVo vo : isLikes) {
                        if (vo.getId().equals(comment.getId())) {
                            if (vo.getIsLike() != null && vo.getIsLike() > 0) {
                                comment.setIsLike(1);
                            } else {
                                comment.setIsLike(0);
                            }
                        }
                    }
                }
                if (likeNums != null) {
                    for (MallCommentVo vo : likeNums) {
                        if (vo.getId().equals(comment.getId())) {
                            comment.setLikeNum(vo.getLikeNum());
                        }
                    }
                }
            });
        }
        Map<String, Object> map = new HashMap<>();
        map.put("list", list);
        map.put("total", PageInfo.of(list).getTotal());

        return ResponseUtil.ok(map);
    }

    /**
     * 批量添加
     * @param userId
     * @param body
     * @return
     */
    @PostMapping("addMore")
    public Object addMore(@LoginUser String userId, @RequestBody String body){
        JSONObject json = JSONObject.parseObject(body);
        String str = json.get("comments").toString();
        List<MallComment> mallComments = JSON.parseObject(str,new TypeReference<List<MallComment>>(){});
        if(mallComments.size() <= 0){
            return ResponseUtil.fail();
        }

        MallUser user = userService.findById(userId);
        String userName = this.getNoNull(this.getNoNull(user.getNickname(), user.getMobile()), user.getUsername());
        if (Pattern.matches("^1[\\d]{10}", userName)) {
            userName = NameUtil.nicknameDesensitization(userName);
        }

        String orderId = "";
        for (MallComment temp: mallComments) {
            if(StringUtils.isNotEmpty(temp.getReplyUserId())){
                orderId = temp.getReplyUserId();
            }
            temp.setReplyUserId(null);
            temp.setUserId(userId);
            temp.setUserAvatar(user.getAvatar());
            temp.setUserName(userName);
            commentService.save(temp);
        }

        // 修改评论数量
        MallOrder mallOrder = mallOrderService.findById(orderId);
        if(mallOrder == null){
            MallPurchaseOrder mallPurchaseOrder = mallPurchaseOrderService.getById(orderId);
            if(mallPurchaseOrder != null){
                mallPurchaseOrder.setComments(0);
                mallPurchaseOrderService.updateById(mallPurchaseOrder);
            }
        }else{
            mallOrder.setComments((short)0);
            mallOrderService.updateByExample(mallOrder);
        }
        return ResponseUtil.ok();
    }

    /**
     * 添加评论/回复
     * @return
     */
    @PostMapping("/video/add")
    public Object list(@LoginUser String userId,
                       @RequestBody MallComment params) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        if (StringUtils.isEmpty(params.getValueId()) || StringUtils.isEmpty(params.getContent()) || StringUtils.isEmpty(params.getTopId())) {
            return ResponseUtil.badArgument();
        }

        params.setUserId(userId);

        MallUser user = userService.findById(userId);
        params.setUserAvatar(user.getAvatar());

        // nickname -> mobile -> username 按次序拿到非空值, 然后判断值是否是1开头11位数字的手机号, 如果是则脱敏
        String userName = this.getNoNull(this.getNoNull(user.getNickname(), user.getMobile()), user.getUsername());
        if (Pattern.matches("^1[\\d]{10}", userName)) {
            userName = NameUtil.nicknameDesensitization(userName);
        }
        params.setUserName(userName);

        MallComment reply = commentService.selectOneById(params.getValueId());
        if (reply != null) {
            MallComment reply2 = commentService.selectOneById(reply.getValueId());
            if (reply2 != null) {
                // 目前是做成只有两阶评论, a评论下回复的回复, 回复的回复的回复...都挂在a下面
                params.setValueId(reply2.getId());
            }
            MallGoods video = goodsService.selectOneById(reply.getValueId());
            // 如果回复的是一条评论, 不保存回复用户信息
            if (video == null) {
                params.setReplyUserId(reply.getUserId());
                params.setReplyUserName(reply.getUserName());
            }
        }
        return commentService.save(params) > 0 ? ResponseUtil.ok() : ResponseUtil.fail();
    }

    private String getNoNull(String var1, String var2) {
        return !StringUtils.isEmpty(var1) ? var1 : !StringUtils.isEmpty(var2) ? var2 : "";
    }

    /**
     * 删除自己的评论/回复
     */
    @PostMapping("/video/del")
    public Object del(@LoginUser String userId,
                       @RequestBody MallComment params) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        if (StringUtils.isEmpty(params.getId())) {
            return ResponseUtil.badArgument();
        }
        MallComment comment = commentService.selectOneById(params.getId());
        if (comment == null) {
            return ResponseUtil.badArgument();
        }
        if (!comment.getUserId().equals(userId)) {
            return ResponseUtil.fail("只能删除自己的评论");
        }
        return commentService.deleteById(params.getId()) > 0 ? ResponseUtil.ok() : ResponseUtil.fail();
    }

    /**
     * 获取抖货视频点赞总数, 评论总数
     */
    @GetMapping("/video/countInfo")
    public Object countInfo(@LoginUser String userId,
                            @RequestParam String id) {
        Map<String, Object> map = new HashMap<>();
        map.put("totalComment", commentService.countByTopId(id));
        map.put("totalLike", followService.countByTargetId(id, (byte) 2));
        map.put("isLike", followService.isFollow(id, userId, (byte) 2));
        return ResponseUtil.ok(map);
    }

    /**
     * 视频/评论/回复点赞
     */
    @PostMapping("/like")
    public Object like(@LoginUser String userId,
                       @RequestBody MallComment params) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        if (StringUtils.isEmpty(params.getId())) {
            return ResponseUtil.badArgument();
        }
        MallFollow existFollow = followService.selectOne(params.getId(), userId, (byte) 2);
        if (existFollow != null) {
            return ResponseUtil.fail("已经点过赞了");
        }
        MallFollow follow = new MallFollow();
        follow.setUserId(userId);
        follow.setType((byte) 2);
        follow.setTargetId(params.getId());

        return followService.save(follow) > 0 ? ResponseUtil.ok() : ResponseUtil.fail();
    }

    /**
     * 取消点赞
     */
    @PostMapping("/unLike")
    public Object unLike(@LoginUser String userId,
                         @RequestBody MallComment params) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        if (StringUtils.isEmpty(params.getId())) {
            return ResponseUtil.badArgument();
        }
        followService.delete(userId, params.getId(), (byte) 2);
        return ResponseUtil.ok();
    }

}
