package com.graphai.mall.wx.web.seckill;


import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.obj.ObjectUtils;
import com.graphai.commons.util.servlet.ServletUtils;
import com.graphai.mall.admin.domain.*;
import com.graphai.mall.admin.service.*;
import com.graphai.mall.db.domain.*;
import com.graphai.mall.db.service.goods.MallGoodsProductService;
import com.graphai.mall.db.service.goods.MallGoodsService;
import com.graphai.mall.db.service.goods.MallGoodsSpecificationService;
import com.graphai.mall.db.service.tag.IMallTagService;
import com.graphai.mall.db.vo.MallGoodsSpecificationVo;
import com.graphai.mall.db.vo.MallTagVo;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.mall.wx.vo.MallSeckillGoodsVo;
import com.graphai.open.mallgoods.entity.IMallGoods;
import com.graphai.open.mallgoods.service.IMallGoodsService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;
import javax.xml.crypto.Data;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/wx/seckill")
@Validated
public class MallSeckillController {

    @Autowired
    private IMallSeckillService serviceImpl;
    @Autowired
    private IMallSeckillGoodsService seckillGoodsService;
    @Autowired
    private IMallSeckillTimeService seckillTimeService;
    @Autowired
    private IMallActivityGoodsProductService mallActivityGoodsProductService;
    @Autowired
    private IMallGoodsService goodsService;
    @Autowired
    private IMallUserMerchantService mallUserMerchantService;
    @Autowired
    private MallGoodsService mallGoodsService;
    @Autowired
    private MallGoodsSpecificationService mallGoodsSpecificationService;
    @Autowired
    private MallGoodsProductService mallGoodsProductService;
    @Autowired
    private IMallMerchantService mallMerchantService;
    public static final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:MM:SS");




    /**
     * 第一个接口，提供服务器时间，正在进行的秒杀活动时间，正在进行的秒杀活动时间段的商品，下一场秒杀活动时间，
     *
     * @return
     */
    @GetMapping("/home/page/seckill")
    public Object homePageSeckill() {
        Map<String, Object> result = new HashMap<>();

        //当前时间大于开始时间，小于结束时间（即活动正在进行中）
        QueryWrapper<MallSeckill> meq = new QueryWrapper<>();
        meq.eq("seckill_status","1");
        List<MallSeckill> mallSeckills = serviceImpl.list(meq);
        MallSeckill mallSeckill = null;
        for (MallSeckill item : mallSeckills) {
            mallSeckill = item;
            break;
        }
        if (ObjectUtil.isNull(mallSeckill)) {
            return ResponseUtil.fail("当前没有秒杀活动");
        }

        //当前时间大于开始时间（即正在进行中的时间段）
        QueryWrapper<MallSeckillTime> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByAsc("seckill_time");
        queryWrapper.eq("seckill_id", mallSeckill.getId());
        List<MallSeckillTime> mallSeckillTimes = seckillTimeService.list(queryWrapper);
        MallSeckillTime mallSeckillTime = null;
        MallSeckillTime notStartSeckillTime = null;
        for (int i = 0 ;i < mallSeckillTimes.size(); i++) {
            MallSeckillTime item = mallSeckillTimes.get(i);

            int a = LocalDateTime.now().getHour();
            int b = item.getSeckillTime().getHour();
            if( b <= a){
                mallSeckillTime = item;
                LocalDateTime time = mallSeckillTime.getSeckillTime();
                LocalDateTime time1 = LocalDateTime.of(LocalDateTime.now().getYear(),LocalDateTime.now().getMonth(),LocalDateTime.now().getDayOfMonth(),time.getHour(),time.getMinute());
                mallSeckillTime.setSeckillTime(time1);
            }

            if(item.getSeckillTime().getHour() > LocalDateTime.now().getHour()){
                notStartSeckillTime = item;
                LocalDateTime time = notStartSeckillTime.getSeckillTime();
                LocalDateTime time1 = LocalDateTime.of(LocalDateTime.now().getYear(),LocalDateTime.now().getMonth(),LocalDateTime.now().getDayOfMonth(),time.getHour(),time.getMinute());
                notStartSeckillTime.setSeckillTime(time1);
                break;
            }

        }
        if (ObjectUtil.isNull(mallSeckillTime)) {
            return ResponseUtil.fail("当前没有秒杀活动时间段");
        }

        QueryWrapper<MallSeckillGoods> mallSeckillGoodsQueryWrapper = new QueryWrapper<>();
        mallSeckillGoodsQueryWrapper.eq("seckill_time_id", mallSeckillTime.getId());
        mallSeckillGoodsQueryWrapper.eq("is_on_sale", 1);
        PageHelper.orderBy("create_time DESC");
        List<MallSeckillGoods> mallSeckillGoods = seckillGoodsService.list(mallSeckillGoodsQueryWrapper);

        List<IMallGoods> goods = new ArrayList<>();
        if (ObjectUtil.isNotNull(mallSeckillGoods) && ObjectUtil.isNotEmpty(mallSeckillGoods)) {
            List<String> goodsIds = new ArrayList<>();
            for (MallSeckillGoods item : mallSeckillGoods) {
                goodsIds.add(item.getGoodsId());
                if (goodsIds.size() >= 10) {
                    break;
                }
            }

            goods = goodsService.queryByGoodsIds(goodsIds);

            QueryWrapper<MallActivityGoodsProduct> mallActivityGoodsProductQueryWrapper = new QueryWrapper<>();
            mallActivityGoodsProductQueryWrapper.eq("activity_rule_id", mallSeckillTime.getId());
            mallActivityGoodsProductQueryWrapper.in("goods_id", goodsIds);
            List<MallActivityGoodsProduct> productList = mallActivityGoodsProductService.list(mallActivityGoodsProductQueryWrapper);

            for (IMallGoods mallGoods : goods) {
                List<MallActivityGoodsProduct> it = new ArrayList<>();
                for (MallActivityGoodsProduct item : productList) {
                    if (item.getGoodsId().equals(mallGoods.getId())) {
                        it.add(item);
                    }
                }
                mallGoods.setMallActivityGoodsProductList(it);
            }
        }
        //服务器时间
        result.put("time", LocalDateTime.now());
        //正在进行中的秒杀活动
        result.put("seckill", mallSeckill);
        //正在进行中的秒杀活动时间段
        result.put("seckillTime", mallSeckillTime);
        //下一场秒杀活动时间段
        result.put("notStartSeckillTime", notStartSeckillTime);
        //正在进行的秒杀活动时间段的前10个商品
        result.put("seckillTimeGoods", goods);

        return ResponseUtil.ok(result);
    }

    /**
     * 第二个接口，提供服务器时间，当天所有场次的时间段
     *
     * @return
     */
    @GetMapping("/allSeckillTimes")
    public Object allSeckillTimes() {
        Map<String, Object> result = new HashMap<>();

        //当前时间大于开始时间，小于结束时间（即活动正在进行中）
        QueryWrapper<MallSeckill> meq = new QueryWrapper<>();
        meq.eq("seckill_status","1");
        List<MallSeckill> mallSeckills = serviceImpl.list(meq);
        MallSeckill mallSeckill = null;
        for (MallSeckill item : mallSeckills) {
            mallSeckill = item;
            break;
        }
        if (ObjectUtil.isNull(mallSeckill)) {
            return ResponseUtil.fail("当前没有秒杀活动");
        }

        //当前时间大于开始时间（即正在进行中的时间段）
        QueryWrapper<MallSeckillTime> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByAsc("seckill_time");
        queryWrapper.eq("seckill_id", mallSeckill.getId());
        List<MallSeckillTime> mallSeckillTimes = seckillTimeService.list(queryWrapper);
        MallSeckillTime mallSeckillTime = null;
        for (int i = 0 ;i < mallSeckillTimes.size(); i++) {
            MallSeckillTime item = mallSeckillTimes.get(i);
            LocalDateTime time = item.getSeckillTime();
            LocalDateTime time1 = LocalDateTime.of(LocalDateTime.now().getYear(),LocalDateTime.now().getMonth(),LocalDateTime.now().getDayOfMonth(),time.getHour(),time.getMinute());
            item.setSeckillTime(time1);

            if(item.getSeckillTime().getHour() <= LocalDateTime.now().getHour()){
                mallSeckillTime = item;
            }

        }
        if (ObjectUtil.isNull(mallSeckillTime)) {
            return ResponseUtil.fail("当前没有秒杀活动时间段");
        }

        //服务器时间
        result.put("time", LocalDateTime.now());
        //正在进行中的秒杀活动时间段
        result.put("seckillTime", mallSeckillTime);
        //所有秒杀活动时间段
        result.put("seckillTimes", mallSeckillTimes);
        return ResponseUtil.ok(result);
    }

    /**
     * 第三个接口，根据时间段id获取所有商品
     *
     * @return
     */
    @GetMapping("/seckill/goods")
    public Object seckillGoods(@RequestParam String timeId,
                               @RequestParam(defaultValue = "1") Integer page,
                               @RequestParam(defaultValue = "10") Integer limit) {
        Map<String, Object> result = new HashMap<>();

        QueryWrapper<MallSeckillGoods> mallSeckillGoodsQueryWrapper = new QueryWrapper<>();
        mallSeckillGoodsQueryWrapper.eq("seckill_time_id", timeId);
        mallSeckillGoodsQueryWrapper.eq("is_on_sale", 1);
        PageHelper.startPage(page,limit);
        PageHelper.orderBy("create_time DESC");
        List<MallSeckillGoods> mallSeckillGoods = seckillGoodsService.list(mallSeckillGoodsQueryWrapper);
        long total = PageInfo.of(mallSeckillGoods).getTotal();
        List<IMallGoods> goods = new ArrayList<>();
        if (ObjectUtil.isNotNull(mallSeckillGoods) && ObjectUtil.isNotEmpty(mallSeckillGoods)) {
            List<String> goodsIds = mallSeckillGoods.stream().map(MallSeckillGoods::getGoodsId).collect(Collectors.toList());

            goods = goodsService.queryByGoodsIds(goodsIds);

            QueryWrapper<MallActivityGoodsProduct> mallActivityGoodsProductQueryWrapper = new QueryWrapper<>();
            mallActivityGoodsProductQueryWrapper.eq("activity_rule_id", timeId);
            mallActivityGoodsProductQueryWrapper.in("goods_id", goodsIds);
            List<MallActivityGoodsProduct> productList = mallActivityGoodsProductService.list(mallActivityGoodsProductQueryWrapper);

            for (IMallGoods mallGoods : goods) {
                List<MallActivityGoodsProduct> it = new ArrayList<>();
                for (MallActivityGoodsProduct item : productList) {
                    if (item.getGoodsId().equals(mallGoods.getId())) {
                        it.add(item);
                    }
                }
                mallGoods.setMallActivityGoodsProductList(it);
            }
        }


        result.put("goods", goods);
        result.put("total",total);
        return ResponseUtil.ok(result);
    }


    /**
     * 秒杀商品详情
     */
    @GetMapping("/seckillDetail")
    public Object seckillDetail(@LoginUser String userId,String goodsId) {
        Map<String, Object> result = new HashMap<>();
        MallGoods goods = mallGoodsService.findById(goodsId);
        QueryWrapper<MallSeckill> seckillQueryWrapper = new QueryWrapper<>();
        seckillQueryWrapper.eq("seckill_status","1");
        List<MallSeckill> seckills = serviceImpl.list(seckillQueryWrapper);
        if(seckills.size() == 0){
            return ResponseUtil.fail("没有正在启用的秒杀活动");
        }

        //当前时间大于开始时间（即正在进行中的时间段）
        QueryWrapper<MallSeckillTime> queryWrapper1 = new QueryWrapper<>();
        queryWrapper1.eq("seckill_id",seckills.get(0).getId());
        queryWrapper1.orderByDesc("seckill_time");
        List<MallSeckillTime> mallSeckillTimes = seckillTimeService.list(queryWrapper1);
        MallSeckillTime mallSeckillTime = null;
        for (int i = 0 ;i < mallSeckillTimes.size(); i++) {
            MallSeckillTime item = mallSeckillTimes.get(i);

            if(item.getSeckillTime().getHour() <= LocalDateTime.now().getHour()){
                mallSeckillTime = item;
                break;
            }

        }

        List<MallGoodsSpecification> specifications = mallGoodsSpecificationService.queryByGid(goodsId);
        List<MallGoodsProduct> mallGoodsProducts = mallGoodsProductService.queryByGid(goodsId);
        List<String> productIds = mallGoodsProducts.stream().map(MallGoodsProduct::getId).collect(Collectors.toList());

        QueryWrapper<MallActivityGoodsProduct> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("activity_rule_id",mallSeckillTime.getId());
        queryWrapper.in("product_id",productIds);
        List<MallActivityGoodsProduct> activityGoodsProducts = mallActivityGoodsProductService.list(queryWrapper);
        for(MallGoodsProduct product : mallGoodsProducts){
            for(MallActivityGoodsProduct item: activityGoodsProducts){
                if(product.getId().equals(item.getProductId())){
                    product.setPrice(item.getActivityPrice());
                    product.setNumber(item.getSurplusNumber());
                    product.setChannelCode(item.getNumber()+"");
                    product.setId(item.getId());
                    product.setDeleted(true);
                }
            }
        }

        // 重新封装规格数据
        Map<String, List<MallGoodsSpecification>> specificationsMap = new LinkedHashMap<>();
        for (MallGoodsSpecification temp : specifications) {
            if (specificationsMap.containsKey(temp.getSpecification())) {
                specificationsMap.get(temp.getSpecification()).add(temp);
            } else {
                List<MallGoodsSpecification> list = new ArrayList<>();
                list.add(temp);
                specificationsMap.put(temp.getSpecification(), list);
            }
        }

        List<MallGoodsSpecificationVo> specifications1 = new ArrayList<>();
        for (String key : specificationsMap.keySet()) {
            List<MallGoodsSpecification> value = specificationsMap.get(key);
            MallGoodsSpecificationVo mallGoodsSpecificationVo = new MallGoodsSpecificationVo();
            mallGoodsSpecificationVo.setName(key);
            mallGoodsSpecificationVo.setChilds(value);
            specifications1.add(mallGoodsSpecificationVo);
        }

        MallMerchant mallMerchant = mallMerchantService.getById(goods.getMerchantId());

        String min = "";
        String max = "";
        String priceStr = "";
        String priceStrs = "";
        if(mallGoodsProducts.size() == 1){
            priceStr = mallGoodsProducts.get(0).getPrice().toString();
            priceStrs = mallGoodsProducts.get(0).getFactoryPrice().toString();
        }
        else if(mallGoodsProducts.size() > 1) {
            max = mallGoodsProducts.stream().max(Comparator.comparing(MallGoodsProduct::getPrice)).get().getPrice().toString();
            min = mallGoodsProducts.stream().min(Comparator.comparing(MallGoodsProduct::getPrice)).get().getPrice().toString();
            priceStr = min + "-" + max;
            max = mallGoodsProducts.stream().max(Comparator.comparing(MallGoodsProduct::getFactoryPrice)).get().getFactoryPrice().toString();
            min = mallGoodsProducts.stream().min(Comparator.comparing(MallGoodsProduct::getFactoryPrice)).get().getFactoryPrice().toString();
            priceStrs = min + "-" + max;
        }
        Boolean isMerchant = false;
        MallUserMerchant mallUserMerchant = mallUserMerchantService.getByUserId(userId);
        if(ObjectUtil.isNotNull(mallUserMerchant) && ObjectUtil.isNotEmpty(mallUserMerchant)) {
            if (mallMerchant.getId().equals(mallUserMerchant.getMerchantId())) {
                isMerchant = true;
            }
        }
        result.put("time",LocalDateTime.now());
        result.put("goods", goods);
        result.put("priceStr", priceStr);
        result.put("priceStrs", priceStrs);
        result.put("isMerchant", isMerchant);
        result.put("specifications", specifications1);
        result.put("products", mallGoodsProducts);
        result.put("mallMerchant", mallMerchant);
        return ResponseUtil.ok(result);
    }


















    /**
     * 第一个接口，提供服务器时间，正在进行的秒杀活动时间，正在进行的秒杀活动时间段的商品，下一场秒杀活动时间，
     *
     * @return
     */
//    @GetMapping("/home/page/seckill")
//    public Object homePageSeckill() {
//        Map<String, Object> result = new HashMap<>();
//
//        //当前时间大于开始时间，小于结束时间（即活动正在进行中）
//        QueryWrapper<MallSeckill> meq = new QueryWrapper<>();
//        meq.le("start_time", LocalDateTime.now());
//        meq.ge("end_time", LocalDateTime.now());
//        meq.orderByDesc("start_time");
//        List<MallSeckill> mallSeckills = serviceImpl.list(meq);
//        MallSeckill mallSeckill = null;
//        for (MallSeckill item : mallSeckills) {
//            mallSeckill = item;
//            break;
//        }
//        if (ObjectUtil.isNull(mallSeckill)) {
//            return ResponseUtil.fail("当前没有秒杀活动");
//        }
//
//        //当前时间大于开始时间（即正在进行中的时间段）
//        QueryWrapper<MallSeckillTime> queryWrapper = new QueryWrapper<>();
//        queryWrapper.le("seckill_time", LocalDateTime.now());
//        queryWrapper.orderByDesc("seckill_time");
//        queryWrapper.eq("seckill_id", mallSeckill.getId());
//        List<MallSeckillTime> mallSeckillTimes = seckillTimeService.list(queryWrapper);
//        MallSeckillTime mallSeckillTime = null;
//        for (MallSeckillTime item : mallSeckillTimes) {
//            mallSeckillTime = item;
//            break;
//        }
//        if (ObjectUtil.isNull(mallSeckillTime)) {
//            return ResponseUtil.fail("当前没有秒杀活动时间段");
//        }
//
//        //当前时间小于开始时间（即下一场的时间段）
//        QueryWrapper<MallSeckillTime> queryWrapper1 = new QueryWrapper<>();
//        queryWrapper1.ge("seckill_time", LocalDateTime.now());
//        queryWrapper1.orderByAsc("seckill_time");
//        queryWrapper1.eq("seckill_id", mallSeckill.getId());
//        List<MallSeckillTime> notStartSeckillTimes = seckillTimeService.list(queryWrapper1);
//        MallSeckillTime notStartSeckillTime = null;
//        for (MallSeckillTime item : notStartSeckillTimes) {
//            notStartSeckillTime = item;
//            break;
//        }
////        if (ObjectUtil.isNull(notStartSeckillTime)) {
////            return ResponseUtil.fail("没有下一场秒杀活动时间段");
////        }
//
//        QueryWrapper<MallSeckillGoods> mallSeckillGoodsQueryWrapper = new QueryWrapper<>();
//        mallSeckillGoodsQueryWrapper.eq("seckill_time_id", mallSeckillTime.getId());
//        mallSeckillGoodsQueryWrapper.eq("is_on_sale", 1);
//        List<MallSeckillGoods> mallSeckillGoods = seckillGoodsService.list(mallSeckillGoodsQueryWrapper);
//
//        List<IMallGoods> goods = new ArrayList<>();
//        if (ObjectUtil.isNotNull(mallSeckillGoods) && ObjectUtil.isNotEmpty(mallSeckillGoods)) {
//            List<String> goodsIds = new ArrayList<>();
//            for (MallSeckillGoods item : mallSeckillGoods) {
//                goodsIds.add(item.getGoodsId());
//                if (goodsIds.size() >= 10) {
//                    break;
//                }
//            }
//
//            goods = goodsService.queryByGoodsIds(goodsIds);
//
//            QueryWrapper<MallActivityGoodsProduct> mallActivityGoodsProductQueryWrapper = new QueryWrapper<>();
//            mallActivityGoodsProductQueryWrapper.eq("activity_rule_id", mallSeckillTime.getId());
//            mallActivityGoodsProductQueryWrapper.in("goods_id", goodsIds);
//            List<MallActivityGoodsProduct> productList = mallActivityGoodsProductService.list(mallActivityGoodsProductQueryWrapper);
//
//            for (IMallGoods mallGoods : goods) {
//                List<MallActivityGoodsProduct> it = new ArrayList<>();
//                for (MallActivityGoodsProduct item : productList) {
//                    if (item.getGoodsId().equals(mallGoods.getId())) {
//                        it.add(item);
//                    }
//                }
//                mallGoods.setMallActivityGoodsProductList(it);
//            }
//        }
//        //服务器时间
//        result.put("time", LocalDateTime.now());
//        //正在进行中的秒杀活动
//        result.put("seckill", mallSeckill);
//        //正在进行中的秒杀活动时间段
//        result.put("seckillTime", mallSeckillTime);
//        //下一场秒杀活动时间段
//        result.put("notStartSeckillTime", notStartSeckillTime);
//        //正在进行的秒杀活动时间段的前10个商品
//        result.put("seckillTimeGoods", goods);
//
//        return ResponseUtil.ok(result);
//    }




}
