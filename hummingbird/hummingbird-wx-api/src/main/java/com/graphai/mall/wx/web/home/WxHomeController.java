package com.graphai.mall.wx.web.home;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;

import com.graphai.mall.db.constant.activity.MallActivityEnum;
import com.graphai.mall.db.domain.*;
import com.graphai.mall.db.service.common.AppVersionService;
import com.graphai.mall.db.service.groupon.MallGrouponService;
import com.graphai.mall.db.service.sysconfig.MallSystemConfigService;
import com.graphai.mall.db.vo.MallGoodsDTO;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Header;
import org.apache.http.message.BasicHeader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.graphai.commons.constant.MallConstant;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.json.JacksonUtil;
import com.graphai.commons.util.json.JacksonUtils;
import com.graphai.mall.core.system.SystemConfig;
import com.graphai.mall.db.service.advertisement.MallAdService;
import com.graphai.mall.db.service.advertisement.MallBrandService;
import com.graphai.mall.db.service.category.MallCategoryService;
import com.graphai.mall.db.service.coupon.MallCouponService;
import com.graphai.mall.db.service.distribution.MallDistributionService;
import com.graphai.mall.db.service.goods.MallGoodsService;
import com.graphai.mall.db.service.groupon.MallGrouponRulesService;
import com.graphai.mall.db.service.promotion.MallTopicService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.util.RetrialUtils;
import com.graphai.mall.db.util.StringHelper;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.mall.wx.service.HomeCacheManager;
import com.graphai.mall.wx.util.GoodsUtils;
import com.graphai.mall.wx.util.Md5Utils;
import com.graphai.mall.wx.web.common.MyBaseController;
import com.graphai.nosql.redis.NewCacheInstanceServiceLoader;
import com.graphai.open.api.getway.HttpRequestUtils;
import com.graphai.open.mallgoods.vo.MallGoodsListVo;
import com.graphai.properties.PlatFormServerproperty;

import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * 首页服务
 */
@RestController
@RequestMapping("/wx/home")
@Validated
@Slf4j
public class WxHomeController extends MyBaseController {
    private final Log logger = LogFactory.getLog(WxHomeController.class);

    @Autowired
    private MallAdService adService;

    @Autowired
    private MallGoodsService goodsService;

    @Autowired
    private MallBrandService brandService;

    @Autowired
    private MallTopicService topicService;

    @Autowired
    private MallCategoryService categoryService;

    @Autowired
    private MallGrouponRulesService grouponRulesService;

    @Autowired
    private MallCouponService couponService;
    @Autowired
    private MallDistributionService distributionService;
    @Autowired
    private MallUserService userService;

    @Autowired
    private PlatFormServerproperty platFormServerproperty;

    @Autowired
    private MallSystemConfigService mallSystemConfigService;

    @Autowired
    private AppVersionService appVersionService;

    @Autowired
    private MallGrouponService mallGrouponService;



    /**
     * 清除key对应的缓存
     *
     * @param key
     * @return
     */
    @GetMapping("/clearCacheByKey")
    public Object clearCache(@NotNull String key) {
        if (Objects.equals(null, key)) {
            return ResponseUtil.fail();
        }

        // 清除缓存
        NewCacheInstanceServiceLoader.objectCache().remove(key);
        return ResponseUtil.ok("缓存已清除");
    }



    @GetMapping("/cache")
    public Object cache(@NotNull String key) {
        if (!key.equals("Mall_cache")) {
            return ResponseUtil.fail();
        }

        // 清除缓存
        HomeCacheManager.clearAll();
        return ResponseUtil.ok("缓存已清除");
    }


    /**
     * 获取首页活动图
     */
    @GetMapping("/getActivitys")
    public Object getActivitys() {



        try {
            List<MallCategory> firstCategories = categoryService.queryL1("upActivity");
            List<MallCategory> secondCategories = categoryService.queryL1("underActivity");
            List<MallCategory> background = categoryService.queryL1("backgroundPic");

            Map<String, Object> map = new HashMap<String, Object>();
            map.put("firstCategories", firstCategories);
            map.put("secondCategories", secondCategories);
            if (CollectionUtils.isNotEmpty(background)) {
                map.put("background", background.get(0).getPicUrl());
            }
            return ResponseUtil.ok(map);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw e;
        }

    }

    /**
     * 分享素材v2分页版
     */
    @GetMapping("/shareMaterial/v2")
    public Object shareMaterialV2(@RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit) {

        try {
            PageHelper.startPage(page, limit);
            List<MallAd> ads = adService.queryIndex((byte) 112);
            long total = PageInfo.of(ads).getTotal();

            HashMap<String, Object> data = new HashMap<>();
            data.put("total", total);
            data.put("items", ads);
            return ResponseUtil.ok(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseUtil.fail();
    }

    /**
     * 分享素材
     */
    @GetMapping("/shareMaterial")
    public Object shareMaterial() {

        try {
            List<MallAd> ads = adService.queryIndex((byte) 112);
            return ResponseUtil.ok(ads);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseUtil.fail();
    }



    /**
     * 获取首页弹幕
     *
     * @throws Exception
     */
    @GetMapping("/getIndexBarrage")
    public Object getIndexBarrage(@RequestParam(value = "position", required = false) String position, HttpServletRequest request, @RequestParam(defaultValue = "1.6.6") String version,
                                  @RequestParam(value = "userId", required = false) String userId) throws Exception {

        List<Map<String, Object>> activities = null;
        try {
            logger.info("用户当前版本：" + version + "；用户ID=" + userId + "； position=" + position);
            String url = platFormServerproperty.getUrl(MallConstant.TAOBAO);
            Map<String, Object> mappMap = new HashMap<String, Object>();
            mappMap.put("plateform", "dtk");
            if (!Objects.equals(position, null)) {
                mappMap.put("position", position);
            } else {
                mappMap.put("position", "1");
            }

            MallUser mallUser = null;
            if (StringUtils.isNotBlank(userId)) {
                mallUser = userService.findById(userId);
            }

            // todo 2021-07-06 新人团商品【version 大于等于 2.2.2 才会显示拼团】
            if (Objects.nonNull(mallUser)) {
                boolean lessNewAppVersion = appVersionService.isLessNewAppVersion(version, "2.2.2");
                int mallGrouponCount = mallGrouponService.queryMyGrouponNewGroupGoods(userId);
                if (!lessNewAppVersion && 0 == mallGrouponCount) {
                    logger.info("可以显示新人团商品");
                    mappMap.put("position", MallActivityEnum.POSITION_29.getCode());
                    //Map<String, String> newcomerGroupMap = mallSystemConfigService.getMallSystemNewcomerGroup();
                    //String newcomerGroupGoods = newcomerGroupMap.get("newcomer_group_goods_id");
                }
            }

            // todo 2021-07-15 新人专享团要登录才显示
            if (MallActivityEnum.POSITION_30.getCode().equals(position) || MallActivityEnum.POSITION_31.getCode().equals(position)) {
                if (Objects.isNull(mallUser)) {
                    return ResponseUtil.fail("客官请先去登录");
                }
                mappMap.put("position", position);
            }

            Header[] headers = new BasicHeader[] {new BasicHeader("method", "indexActivity"), new BasicHeader("version", "v1.0")};
            String result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);
            JSONObject object = JSONObject.parseObject(result);
            if (object.getString("errno").equals("0")) {
                activities = (List<Map<String, Object>>) JSON.parseObject(object.get("data").toString(), List.class);
                // 个人中心切换成过审类目
                if (RetrialUtils.examine(request, version)) {
                    if (Objects.equals(null, position)) {
                        if (CollectionUtils.isNotEmpty(activities)) {
                            for (int i = 0; i < activities.size(); i++) {
                                if ("12".equals(activities.get(i).get("id"))) {
                                    activities.remove(activities.get(i));
                                    i--;
                                }
                                if ("10".equals(activities.get(i).get("id"))) {
                                    activities.remove(activities.get(i));
                                    i--;
                                }
                            }
                        }
                    }
                }
                return ResponseUtil.ok(activities);
            }

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw e;
        }
        return ResponseUtil.fail();

    }

    /**
     * 首页数据
     *
     * @param userId 当用户已经登录时，非空。为登录状态为null
     * @return 首页数据
     */
    @GetMapping("/index")
    // public Object index(@LoginUser String userId,String industryId) {
    public Object index(String industryId, @LoginUser String userId) {
        // if (userId == null) {
        // return ResponseUtil.unlogin();
        // }
        if (industryId == null) {
            industryId = "0";
        }


        try {

            // String key = "12345678901l" + industryId;
            // Object home = homeCache.get(key);
            // if (home != null) {
            // return ResponseUtil.ok(home);
            // }
            Map<String, Object> data = new HashMap<>();

            // 广告列表
            // Callable<List<MallAd>> bannerListCallable = () -> adService.queryIndex();
            // List<MallAd> adList = adService.queryIndex((byte) 1);
            List<MallAd> adList = adService.queryByPosition("1");
            // 布局颜色
            List<MallAd> layout = adService.queryIndex((byte) 110);
            // 头部行业列表
            // List<MallIndustry> industries = industryService.queryIndustry("head");
            // 中间行业列表
            // List<MallIndustry> centerIndustries = industryService.queryIndustry("center");



            // String url=platFormServerproperty.getUrl(MallConstant.TAOBAO);
            String url = platFormServerproperty.getUrl(MallConstant.TAOBAO);
            Map<String, Object> mappMap = new HashMap<String, Object>();
            mappMap.put("plateform", "dtk");
            mappMap.put("type", "dtkcategory");

            Header[] headers = new BasicHeader[] {new BasicHeader("method", "queryIndustryByType"), new BasicHeader("version", "v1.0")};
            String result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);

            JSONObject object = JSONObject.parseObject(result);

            if (object.getString("errno").equals("0")) {

                data.put("centerIndustryList", JSON.parseObject(object.get("data").toString(), List.class));

            }


            /**
             * 首页中间主推广告列表
             */
            List<MallAd> mainPushList = adService.queryIndex((byte) 3);

            // 查询产品目录
            // Callable<List<MallCategory>> channelListCallable = () -> categoryService.queryChannel();


            // 首页目录配置
            List<Object> listMaps = new ArrayList<Object>();
            // List<MallCategory> channelList = categoryService.queryChannel("0", "home");
            List<MallCategory> channelList = categoryService.queryExtend(industryId, "home");

            if (CollectionUtils.isNotEmpty(channelList)) {
                for (int i = 0; i < (channelList.size() % 15 > 0 ? (channelList.size() / 15 + 1) : channelList.size() / 15); i++) {

                    // List<Map<String, String>> tmp = new ArrayList<>(list.subList(20, 40));
                    // Map<String, Object> map=new HashMap<String, Object>();
                    // map.put("channel", );
                    List<Object> objects = new ArrayList<Object>(channelList.subList(15 * i, channelList.size() < 15 * (i + 1) ? channelList.size() : 15 * (i + 1)));
                    listMaps.add(objects);
                }
            }


            // Object obj=categoryService.queryIndustryAndCategory("head", "home");

            // Object obj=categoryService.queryIndustryAndCategory("head", "home");


            // 首頁活动目录配置
            // List<MallCategory> activitychannelList = categoryService.queryChannel(industryId,
            // "homeActivity");

            // 新品上市
            // List<MallGoods> newGoodsList = goodsService.queryByNew(0, SystemConfig.getNewLimit(),
            // industryId);


            // 今日推荐
            // List<MallTopic> topicList = topicService.queryList(0, SystemConfig.getTopicLimit());

            // 团购专区
            // List<Map<String, Object>> groupList = grouponRulesService.queryList(0, 5);


            // logger.info("获取到广告列表的数据 ： " + JacksonUtils.bean2Jsn(adList));

            /*
             * if (CollectionUtils.isNotEmpty(newGoodsList)) { for (int i = 0; i < newGoodsList.size(); i++) {
             * MallGoods myGoods = newGoodsList.get(i); myGoods.setSpreads(GoodsUtils.getSpreads(myGoods)); } }
             */

            /*** 小花咪电商：需要计算返利金额，并且是根据不同的代理基本进行计算 */
            /*
             * Byte userLevl = 0; MallDistributionSettings setting =
             * distributionService.getlDistributionSetting(userLevl, "D00");
             *
             * List<Map<String, Object>> finalHotGoodsList = new ArrayList<Map<String, Object>>(); List<String>
             * hotGoodIds = new ArrayList<String>(); if (CollectionUtils.isNotEmpty(hotGoodsList)) { for (int i
             * = 0; i < hotGoodsList.size(); i++) { MallGoods myGoods = hotGoodsList.get(i);
             * hotGoodIds.add(myGoods.getId()); }
             *//**
                * 热门产品的优惠券
                *//*
                   * List<Map<String, Object>> couponList = couponService.getProductUnderCoupons(hotGoodIds); for (int
                   * i = 0; i < hotGoodsList.size(); i++) {
                   *
                   * Map<String, Object> hotGoodsData = new HashMap<String, Object>();
                   *
                   * MallGoods goods = hotGoodsList.get(i); String goodsId = goods.getId();
                   *
                   * goods.setSpreads(GoodsUtils.getSpreads(goods)); //计算返的金额 GoodsUtils.setRebateAmount(setting,
                   * goods);
                   *
                   * List<Map<String, Object>> couponCollect = new ArrayList<Map<String, Object>>();
                   * CollectionUtils.select(couponList, new Predicate() {
                   *
                   * @Override public boolean evaluate(Object object) {
                   *
                   * @SuppressWarnings("unchecked") Map<String, Object> coupon = (Map<String, Object>) object; return
                   * (goodsId != null && goodsId.equals(MapUtils.getString(coupon, "goodsId"))) ? true : false; } },
                   * couponCollect);
                   *
                   *
                   * hotGoodsData.put("goodsInfo", goods); hotGoodsData.put("coupons", couponCollect);
                   * finalHotGoodsList.add(hotGoodsData); }
                   *
                   * }
                   */

            // 目录分类
            // data.put("channelList", listMaps);
            // data.put("channelListAndIndustry", obj);
            // 活动目录
            // data.put("activitychannel", activitychannelList);
            data.put("banner", adList);
            // 主推广告
            data.put("mainPushList", mainPushList);
            // 布局
            data.put("layout", layout);
            // 新品
            // data.put("newGoodsList", newGoodsList);
            // 热卖产品

            // 品牌制造商
            // data.put("brandList", brandListTask.get());
            // 精选商品
            // data.put("topicList", topicList);
            // 团购专区
            // data.put("grouponList", groupList);
            // data.put("floorGoodsList", floorGoodsListTask.get());
            // 缓存数据
            // HomeCacheManager.loadData(HomeCacheManager.INDEX, data);

            // 头部行业
            // data.put("headIndustryList", industries);

            // homeCache.put(key, data);

            // 品牌制造商
            return ResponseUtil.ok(data);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // executorService.shutdown();
        }

        return ResponseUtil.ok();
    }



    /**
     * 首页数据
     *
     * @param userId 当用户已经登录时，非空。为登录状态为null
     * @return 首页数据
     */
    @GetMapping("/indexV2")
    // public Object index(@LoginUser String userId,String industryId) {
    public Object indexV2(String industryId, @LoginUser String userId, HttpServletRequest request, @RequestParam(defaultValue = "1.6.6") String version) {

        if (industryId == null) {
            industryId = "0";
        }


        List<MallAd> adList = null;

        try {

            // String key = "12345678901l" + industryId;
            // Object home = homeCache.get(key);
            // if (home != null) {
            // return ResponseUtil.ok(home);
            // }
            Map<String, Object> data = new HashMap<>();

            // 广告列表
            // Callable<List<MallAd>> bannerListCallable = () -> adService.queryIndex();
            adList = adService.queryIndex((byte) 1);



            // 布局颜色
            List<MallAd> layout = adService.queryIndex((byte) 110);



            String url = platFormServerproperty.getUrl(MallConstant.TAOBAO);
            Map<String, Object> mappMap = new HashMap<String, Object>();
            mappMap.put("plateform", "dtk");
            mappMap.put("type", "dtkcategory");

            Header[] headers = new BasicHeader[] {new BasicHeader("method", "queryIndustryByType"), new BasicHeader("version", "v1.0")};
            String result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);

            JSONObject object = JSONObject.parseObject(result);

            if (object.getString("errno").equals("0")) {

                data.put("centerIndustryList", JSON.parseObject(object.get("data").toString(), List.class));

            }



            /**
             * 首页中间主推广告列表
             */
            List<MallAd> mainPushList = adService.queryIndex((byte) 3);

            // 查询产品目录
            // Callable<List<MallCategory>> channelListCallable = () -> categoryService.queryChannel();


            // 首页行业频道放入缓存
            Object obj = categoryService.queryIndustryAndCategory("head", "home", RetrialUtils.examine(request, version));

            data.put("banner", adList);
            if (RetrialUtils.examine(request, version)) {
                List<MallAd> mallAds = new ArrayList<MallAd>();
                adList.get(0).setLink("/pages/brand-specials/list?");
                mallAds.add(adList.get(0));

                data.put("banner", mallAds);
            }


            data.put("channelListAndIndustry", obj);
            // 活动目录
            // data.put("activitychannel", activitychannelList);

            // 主推广告
            data.put("mainPushList", mainPushList);
            // 布局
            data.put("layout", layout);

            // 品牌制造商
            return ResponseUtil.ok(data);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // executorService.shutdown();
        }

        return ResponseUtil.ok();
    }


    /**
     * 首页数据
     *
     * @param userId 当用户已经登录时，非空。为登录状态为null
     * @return 首页数据
     */
    @GetMapping("/index3")
    // public Object index(@LoginUser String userId,String industryId) {
    public Object index3(String industryId, @LoginUser String userId) {
        if (industryId == null) {
            industryId = "0";
        }

        try {
            // String key = "12345678901l" + industryId;
            // Object home = homeCache.get(key);
            // if (home != null) {
            // return ResponseUtil.ok(home);
            // }

            Map<String, Object> data = new HashMap<>();

            if (userId != null) {
                List<Map<String, Object>> activitys = goodsService.queryKmActivity(userId);
                data.put("activityList", activitys);
            }


            // 限时秒杀
            JSONArray timeSpikeGoodsJson = goodsService.queryTimeSpike(0, SystemConfig.getHotLimit(), industryId, "timeSpHome");

            // 新人专享
            List<MallGoods> newPersonGoods = goodsService.queryNewPerson(0, 3, industryId, "categoryhome3");

            // 天天特价
            JSONArray dayDayGoods = goodsService.queryDayPrice(0, 8, industryId, "dayhome");

            List<MallGoods> douyinList = goodsService.queryKmByDouYin();

            // 新人共享
            data.put("newPersonGoods", newPersonGoods);
            // 天天特价
            data.put("dayDayGoods", dayDayGoods);


            // 限时秒杀
            data.put("timeSpikeGoodsJson", timeSpikeGoodsJson);

            // 品牌制造商
            data.put("brandList", brandService.getbrandByPage(0, 5));

            data.put("nowTime", new Date());
            // homeCache.put(key, data);

            return ResponseUtil.ok(data);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseUtil.ok();
    }

    /**
     * 首页热销商品数据(包括自营和非自营)
     *
     * @param userId 当用户已经登录时，非空。为登录状态为null
     * @return 首页数据
     */
    @GetMapping("/goodsList")
    public Object goodsList(@RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit, String industryId, @LoginUser String userId) {

        if (page <= 0) {
            page = 1;
        }

        if (industryId == null) {
            industryId = "0";
        }

        try {
            Map<String, Object> data = new HashMap<>();

            // 热卖产品
            List<MallGoods> hotGoodsList = goodsService.queryByHot(page, limit, industryId, null, null);

            // List<MallGoods> douyinList = goodsService.queryKmByDouYin();

            // 抖音商品
            // data.put("douyinList", douyinList);

            // 热卖产品
            data.put("hotGoodsList", hotGoodsList);

            return ResponseUtil.ok(data);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return ResponseUtil.ok();
    }


    /**
     * 首页抖音
     *
     * @param userId 当用户已经登录时，非空。为登录状态为null
     * @return 首页数据
     */
    @GetMapping("/douYinGoodsList")
    public Object douYinGoodsList(@RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit, @LoginUser String userId) {

        if (page <= 0) {
            page = 1;
        }

        try {

            String url = platFormServerproperty.getUrl(MallConstant.TAOBAO);
            Map<String, Object> mappMap = new HashMap<String, Object>();
            mappMap.put("plateform", "dtk");
            mappMap.put("page", page);
            mappMap.put("size", limit);
            mappMap.put("sort", "id");
            mappMap.put("columnType", "douyin");

            Header[] headers = new BasicHeader[] {new BasicHeader("method", "selectDtkGoodsByPage"), new BasicHeader("version", "v1.0")};

            String result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);

            JSONObject object = JSONObject.parseObject(result);

            if (object.getString("errno").equals("0")) {

                List<MallGoodsDTO> douYinGoodsList = JSONArray.parseArray(object.get("data").toString(), MallGoodsDTO.class);

                // for (int i = 0; i < douYinGoodsList.size(); i++) {
                // if (!HttpPostUtil.isEffective(douYinGoodsList.get(i).getMediaUrl())) {
                // douYinGoodsList.remove(douYinGoodsList.get(i));
                // i--;
                // }
                // }

                // 打乱顺序
                Collections.shuffle(douYinGoodsList);
                return ResponseUtil.ok(douYinGoodsList);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return ResponseUtil.ok();
    }



    /**
     * 随机获取下标抖货商品数据
     *
     * @param page
     * @param limit
     * @param id 商品id
     * @param index 下标
     * @return
     */
    @GetMapping("/douYinGoodsListV2")
    public Object douYinGoodsListV2(@RequestParam(defaultValue = "10") Integer limit, @RequestParam(value = "id", required = false) String id, @RequestParam(value = "index", required = false) String index) {


        Map<String, Object> map = new HashMap<String, Object>();
        Object douhuo = NewCacheInstanceServiceLoader.douhuo().get("douhuo");
        Map<String, List<String>> idMap = new HashMap<String, List<String>>();
        if (douhuo == null) {
            // 将全部抖货商品分组
            List<String> total = goodsService.queryDouHuoTotal();
            Collections.shuffle(total);
            Integer totalPage = total.size() / limit;
            for (int i = 0; i < totalPage; i++) {
                List<String> ids = null;
                if (i != totalPage) {
                    // List.subList方法返回的只是一个视图，而jetcache写入磁盘需要的是实体,所以到重新new ArrayList
                    ids = new ArrayList<>(total.subList((i) * limit, (i) * limit + limit));
                } else {
                    ids = new ArrayList<>(total.subList((i) * limit, totalPage - 1));
                }

                // Collections.shuffle(ids);
                idMap.put(String.valueOf(i + 1), ids);
            }
            NewCacheInstanceServiceLoader.douhuo().put("douhuo", idMap);
        } else {
            idMap = (Map<String, List<String>>) douhuo;
        }



        try {

            List<String> indexs = new ArrayList<String>();
            if (!idMap.isEmpty()) {
                Iterator<String> iterator = idMap.keySet().iterator();
                while (iterator.hasNext()) {
                    indexs.add(iterator.next());
                }
            }

            // 删除之前累积查询返回的下标，避免查询到重复数据
            if (!Objects.equals(null, index)) {
                List lis = Arrays.asList(index.split(","));
                if (CollectionUtils.isNotEmpty(lis)) {
                    indexs.removeAll(lis);
                }
            }
            Random rand = new Random();
            String num = indexs.get(rand.nextInt(indexs.size()));
            // int num = (int) (Math.random() * indexs.size());

            List<String> idsList = idMap.get(num);

            if (StringHelper.isNotNullAndEmpty(id)) {
                idsList.add(0, id);
            }

            map.put("index", num);

            List<MallGoodsListVo> douYinGoodsList = goodsService.queryGoodsByIds(idsList, null);
            if (CollectionUtils.isNotEmpty(douYinGoodsList)) {


                for (int j = 0; j < douYinGoodsList.size(); j++) {
                    // 图片没有http或https则加上
                    if (!douYinGoodsList.get(j).getPicUrl().contains("https") && !douYinGoodsList.get(j).getPicUrl().contains("http")) {
                        douYinGoodsList.get(j).setPicUrl("https:" + douYinGoodsList.get(j).getPicUrl());
                    }
                    if (!Objects.equals(null, id)) {
                        if (Objects.equals(douYinGoodsList.get(j).getId(), id)) {
                            douYinGoodsList.remove(j);
                            j--;
                        }
                    }


                }
                // 打乱顺序
                Collections.shuffle(douYinGoodsList);
                if (StrUtil.isNotEmpty(id)) {
                    List<MallGoodsListVo> firstGoods = goodsService.queryGoodsByIds(null, id);
                    if (CollectionUtils.isNotEmpty(firstGoods)) {
                        douYinGoodsList.add(0, firstGoods.get(0));
                    }
                }


            }
            map.put("list", douYinGoodsList);

            return ResponseUtil.ok(map);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseUtil.ok();


    }


    /**
     * 推广页数据
     *
     * @param industryId
     * @return 首页数据
     * @throws IOException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    @GetMapping("/shareIndex")
    public Object shareIndex(String industryId) throws Exception {

        if (industryId == null) {
            industryId = "0";
        }


        Map<String, Object> data = new HashMap<>();
        // 首页目录配置
        List<Object> listMaps = new ArrayList<Object>();
        List<MallCategory> channelList = categoryService.queryExtend(industryId, "extendHome");

        if (CollectionUtils.isNotEmpty(channelList)) {
            for (int i = 0; i < (channelList.size() % 15 > 0 ? (channelList.size() / 15 + 1) : channelList.size() / 15); i++) {
                System.out.println("数组长度" + channelList.size() / 15);
                List<Object> objects = new ArrayList<Object>(channelList.subList(15 * i, channelList.size() < 15 * (i + 1) ? channelList.size() : 15 * (i + 1)));
                listMaps.add(objects);
            }
        }
        // 广告列表 5代表推广页广告
        List<MallAd> adList = adService.queryIndex((byte) 5);

        // 推广页 新人专享
        List<MallGoods> newPersonGoods = goodsService.queryNewPerson(0, 3, industryId, "extendHome");

        // 推广页0元购购
        List<MallGoods> newGoodsList = goodsService.queryExtendByNew(0, 6, industryId);

        // 推广页会员权益200
        List<MallGoods> member = goodsService.queryExtendMember(0, 12, industryId);

        // 推广页获取品牌团
        String url = platFormServerproperty.getUrl(MallConstant.TAOBAO);
        Map<String, Object> mappMap = new HashMap<String, Object>();
        mappMap.put("plateform", "dtk");
        mappMap.put("page", 0);
        mappMap.put("size", 8);

        Header[] headers = new BasicHeader[] {new BasicHeader("method", "getBrandsByExtend"), new BasicHeader("version", "v1.0")};


        String result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);

        JSONObject object = JSONObject.parseObject(result);
        List<MallBrand> brands = new ArrayList<MallBrand>();
        if (object.getString("errno").equals("0")) {
            brands = JSONArray.parseArray(object.get("data").toString(), MallBrand.class);
        }


        data.put("brandList", brands);
        data.put("adList", adList);
        data.put("newPersonGoods", newPersonGoods);
        data.put("newGoodsList", newGoodsList);
        data.put("channelList", channelList);
        data.put("member", member);
        return ResponseUtil.ok(data);
    }

    /**
     * jd商品数据入库
     */
    @GetMapping("/jd")
    public Object jd() throws IOException {

        // 推广页获取品牌团
        String url = platFormServerproperty.getUrl(MallConstant.JINGDONG);
        Map<String, Object> mappMap = new HashMap<String, Object>();
        mappMap.put("plateform", "jd");
        mappMap.put("page", 0);
        mappMap.put("size", 8);

        Header[] headers = new BasicHeader[] {new BasicHeader("method", "jd.mall.list.get"), new BasicHeader("version", "v1.0")};
        String result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);

        JSONObject object = JSONObject.parseObject(result);
        return ResponseUtil.ok();
    }


    /**
     * 首页数据
     *
     * @param userId 当用户已经登录时，非空。为登录状态为null
     * @return 首页数据
     */
    @GetMapping("/index2")
    public Object index2(String industryId) {
        if (industryId == null) {
            industryId = "0";
        }

        Map<String, Object> data = new HashMap<>();
        try {
            // 广告列表
            // Callable<List<MallAd>> bannerListCallable = () -> adService.queryIndex();
            // 6代表K米商城首页广告
            List<MallAd> adList = adService.queryIndex((byte) 6);

            /**
             * 首页中间主推广告列表
             */
            List<MallAd> mainPushList = adService.queryIndex((byte) 8);

            // 查询产品目录
            // Callable<List<MallCategory>> channelListCallable = () -> categoryService.queryChannel();

            // 首页目录配置
            // List<MallCategory> channelList = categoryService.queryChannel(industryId,"home");


            // 首頁活动目录配置 本来就有
            // List<MallCategory> activitychannelList = categoryService.queryChannel(industryId,
            // "homeActivity");

            // 新品上市
            // List<MallGoods> newGoodsList = goodsService.queryByNew(0, SystemConfig.getNewLimit(),industryId);
            List<MallGoods> newGoodsList = goodsService.queryKmByNew(1, SystemConfig.getNewLimit(), industryId);

            // 热门商品
            // List<MallGoods> hotGoodsList = goodsService.queryByHot(0,
            // SystemConfig.getHotLimit(),industryId,null,null);


            // 今日推荐
            // List<MallTopic> topicList = topicService.queryList(0, SystemConfig.getTopicLimit());

            // 团购专区
            // List<Map<String, Object>> groupList = grouponRulesService.queryList(0, 5);


            List<MallGoods> hotGoodsList = goodsService.queryKmByHot(1, SystemConfig.getHotLimit(), industryId);
            List<MallCategory> channelList = categoryService.queryKmChannel(industryId, "categoryHome2");
            logger.info("获取到广告列表的数据 ： " + JacksonUtils.bean2Jsn(adList));

            // if(CollectionUtils.isNotEmpty(newGoodsList)){
            // for (int i = 0; i < newGoodsList.size(); i++) {
            // MallGoods myGoods = newGoodsList.get(i);
            // myGoods.setSpreads(GoodsUtils.getSpreads(myGoods));
            // }
            // }

            /*** 小花咪电商：需要计算返利金额，并且是根据不同的代理基本进行计算 */
            /*
             * Byte userLevl = 0; MallDistributionSettings setting =
             * distributionService.getlDistributionSetting(userLevl, "D00");
             *
             * List<Map<String, Object>> finalHotGoodsList = new ArrayList<Map<String, Object>>(); List<String>
             * hotGoodIds = new ArrayList<String>(); if (CollectionUtils.isNotEmpty(hotGoodsList)) { for (int i
             * = 0; i < hotGoodsList.size(); i++) { MallGoods myGoods = hotGoodsList.get(i);
             * hotGoodIds.add(myGoods.getId()); }
             *//**
                * 热门产品的优惠券
                *//*
                   * List<Map<String, Object>> couponList = couponService.getProductUnderCoupons(hotGoodIds); for (int
                   * i = 0; i < hotGoodsList.size(); i++) {
                   *
                   * Map<String, Object> hotGoodsData = new HashMap<String, Object>();
                   *
                   * MallGoods goods = hotGoodsList.get(i); String goodsId = goods.getId();
                   *
                   * goods.setSpreads(GoodsUtils.getSpreads(goods)); //计算返的金额 GoodsUtils.setRebateAmount(setting,
                   * goods);
                   *
                   * List<Map<String, Object>> couponCollect = new ArrayList<Map<String, Object>>();
                   * CollectionUtils.select(couponList, new Predicate() {
                   *
                   * @Override public boolean evaluate(Object object) {
                   *
                   * @SuppressWarnings("unchecked") Map<String, Object> coupon = (Map<String, Object>) object; return
                   * (goodsId != null && goodsId.equals(MapUtils.getString(coupon, "goodsId"))) ? true : false; } },
                   * couponCollect);
                   *
                   *
                   * hotGoodsData.put("goodsInfo", goods); hotGoodsData.put("coupons", couponCollect);
                   * finalHotGoodsList.add(hotGoodsData); }
                   *
                   * }
                   */

            // 目录分类
            data.put("channel", channelList);
            // 活动目录
            // data.put("activitychannel", activitychannelList);
            data.put("banner", adList);
            // 主推广告
            data.put("mainPushList", mainPushList);
            // 新品
            data.put("newGoodsList", newGoodsList);
            // 热卖产品
            data.put("hotGoodsList", hotGoodsList);
            // 品牌制造商
            // data.put("brandList", brandListTask.get());
            // 精选商品
            // data.put("topicList", topicList);
            // 团购专区
            // data.put("grouponList", groupList);
            // data.put("floorGoodsList", floorGoodsListTask.get());
            // 缓存数据
            // HomeCacheManager.loadData(HomeCacheManager.INDEX, data);

            // homeCache.put(key, data);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // executorService.shutdown();
        }
        return ResponseUtil.ok(data);
    }

    /**
     * 首页商品
     *
     * @param userId
     * @param industryId
     * @return
     */
    @GetMapping("/index/goods")
    public Object indexGoods(@LoginUser String userId, String industryId, String sort, String order) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        if (industryId == null) {
            industryId = "0";
        }

        Map<String, Object> data = new HashMap<>();

        String sortField = "sort_order";
        if ("0".equals(sort)) {
            sortField = "retail_price ,id ,itemsale";
        } else if ("1".equals(sort)) {
            sortField = "id";
        } else if ("2".equals(sort)) {
            sortField = "itemsale";
        } else if ("3".equals(sort)) {
            sortField = "retail_price";
        }


        try {
            // 热卖产品
            List<MallGoods> hotGoodsList = goodsService.queryByHot(0, SystemConfig.getHotLimit(), industryId, sortField, order);

            List<Map<String, Object>> finalHotGoodsList = new ArrayList<Map<String, Object>>();
            List<String> hotGoodIds = new ArrayList<String>();
            if (CollectionUtils.isNotEmpty(hotGoodsList)) {

                /*** 小花咪电商：需要计算返利金额，并且是根据不同的代理基本进行计算 */
                Byte userLevl = 0;
                if (userId != null) {
                    MallUser user = userService.findById(userId);
                    userLevl = user.getUserLevel();
                }

                MallDistributionSettings setting = distributionService.getlDistributionSetting(userLevl, "D00");
                GoodsUtils.setRebateAmount(setting, hotGoodsList);

                for (int i = 0; i < hotGoodsList.size(); i++) {
                    MallGoods myGoods = hotGoodsList.get(i);

                    hotGoodIds.add(myGoods.getId());
                    myGoods.setSpreads(GoodsUtils.getSpreads(myGoods));
                }

                /**
                 * 热门产品的优惠券
                 */
                List<Map<String, Object>> couponList = couponService.getProductUnderCoupons(hotGoodIds);
                for (int i = 0; i < hotGoodsList.size(); i++) {

                    Map<String, Object> hotGoodsData = new HashMap<String, Object>();

                    MallGoods goods = hotGoodsList.get(i);
                    String goodsId = goods.getId();

                    List<Map<String, Object>> couponCollect = new ArrayList<Map<String, Object>>();
                    CollectionUtils.select(couponList, new Predicate() {
                        @Override
                        public boolean evaluate(Object object) {
                            @SuppressWarnings("unchecked")
                            Map<String, Object> coupon = (Map<String, Object>) object;
                            return (goodsId != null && goodsId.equals(MapUtils.getInteger(coupon, "goodsId"))) ? true : false;
                        }
                    }, couponCollect);


                    hotGoodsData.put("goodsInfo", goods);
                    hotGoodsData.put("coupons", couponCollect);
                    finalHotGoodsList.add(hotGoodsData);
                }

            }
            // 热卖产品
            data.put("hotGoodsList", finalHotGoodsList);
        } catch (Exception e) {
            logger.error("商品查询异常");
        } finally {
            // executorService.shutdown();
        }
        return ResponseUtil.ok(data);
    }

    /**
     * 拼多多商品首页
     *
     * @return
     */
    @GetMapping("/index/pdd/goods")
    public Object indexPddGoods(String industryId) {
        String url = platFormServerproperty.getUrl(industryId);
        if (StringUtils.isBlank(industryId)) {
            industryId = "8";
        }
        Map<String, Object> mappMap = new HashMap<String, Object>();
        mappMap.put("plateform", "pdd");
        mappMap.put("industryId", industryId);

        Header[] headers = new BasicHeader[] {new BasicHeader("method", "pdd.get.home.list"), new BasicHeader("version", "v1.0")};

        String result = null;
        JSONObject qyDataList = null;
        try {
            result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);


        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 拼多多充值商品
     *
     * @return
     */
    @GetMapping("/index/pdd/rechargeGoods")
    public Object indexPddRechargeGoods(String industryId) {
        String url = platFormServerproperty.getUrl(MallConstant.SUNING);
        if (StringUtils.isBlank(industryId)) {
            industryId = "8";
        }
        Map<String, Object> mappMap = new HashMap<String, Object>();
        mappMap.put("plateform", "pdd");
        mappMap.put("industryId", industryId);

        Header[] headers = new BasicHeader[] {new BasicHeader("method", "select.goods.recharge"), new BasicHeader("version", "v1.0")};

        String result = null;
        try {
            result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseUtil.ok(result);

    }

    /**
     * 热搜关键词
     *
     * @return
     */
    @GetMapping("/queryHotKeyWords")
    public Object indexPddGoods() {
        String url = platFormServerproperty.getUrl(MallConstant.TAOBAO);

        Map<String, Object> mappMap = new HashMap<String, Object>();
        mappMap.put("plateform", "dtk");

        Header[] headers = new BasicHeader[] {new BasicHeader("method", "queryHotKeyWords"), new BasicHeader("version", "v1.0")};

        String result = null;
        try {
            result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);
            JSONObject object = JSONObject.parseObject(result);

            List<MallKeyword> keywords = new ArrayList<>(20);

            if (object.getString("errno").equals("0")) {
                keywords = JSONArray.parseArray(object.get("data").toString(), MallKeyword.class);
            }
            return ResponseUtil.ok(keywords);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("查询异常" + e.getMessage());
        }
        return ResponseUtil.fail();
    }

    @GetMapping("/indexBak")
    public Object indexBak(@LoginUser String userId, String industryId) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        // 优先从缓存中读取
        if (HomeCacheManager.hasData(HomeCacheManager.INDEX)) {
            return ResponseUtil.ok(HomeCacheManager.getCacheData(HomeCacheManager.INDEX));
        }


        Map<String, Object> data = new HashMap<>();

        // 广告列表
        Callable<List<MallAd>> bannerListCallable = () -> adService.queryIndex((byte) 1);

        // 查询产品目录
        Callable<List<MallCategory>> channelListCallable = () -> categoryService.queryChannel(industryId, "home");
        // 查询优惠券
        Callable<List<MallCoupon>> couponListCallable;
        // if(userId == null){
        // couponListCallable = () -> couponService.queryList(0, 3);
        // } else {
        couponListCallable = () -> couponService.queryAvailableList(userId, 0, 3);
        // }

        // 新品上市
        Callable<List<MallGoods>> newGoodsListCallable = () -> goodsService.queryByNew(0, SystemConfig.getNewLimit(), industryId);
        // 热卖产品
        Callable<List<MallGoods>> hotGoodsListCallable = () -> goodsService.queryByHot(0, SystemConfig.getHotLimit(), industryId, null, null);

        Callable<List<MallBrand>> brandListCallable = () -> brandService.queryVO(0, SystemConfig.getBrandLimit());

        Callable<List<MallTopic>> topicListCallable = () -> topicService.queryList(0, SystemConfig.getTopicLimit());

        // 团购专区
        Callable<List<Map<String, Object>>> grouponListCallable = () -> grouponRulesService.queryList(0, 5);

        Callable<List<Map<String, Object>>> floorGoodsListCallable = this::getCategoryList;

        FutureTask<List<MallAd>> bannerTask = new FutureTask<>(bannerListCallable);
        FutureTask<List<MallCategory>> channelTask = new FutureTask<>(channelListCallable);
        FutureTask<List<MallCoupon>> couponListTask = new FutureTask<>(couponListCallable);
        FutureTask<List<MallGoods>> newGoodsListTask = new FutureTask<>(newGoodsListCallable);
        FutureTask<List<MallGoods>> hotGoodsListTask = new FutureTask<>(hotGoodsListCallable);
        FutureTask<List<MallBrand>> brandListTask = new FutureTask<>(brandListCallable);
        FutureTask<List<MallTopic>> topicListTask = new FutureTask<>(topicListCallable);
        FutureTask<List<Map<String, Object>>> grouponListTask = new FutureTask<>(grouponListCallable);
        FutureTask<List<Map<String, Object>>> floorGoodsListTask = new FutureTask<>(floorGoodsListCallable);

        executorService.submit(bannerTask);
        executorService.submit(channelTask);
        executorService.submit(couponListTask);
        executorService.submit(newGoodsListTask);
        executorService.submit(hotGoodsListTask);
        executorService.submit(brandListTask);
        executorService.submit(topicListTask);
        executorService.submit(grouponListTask);
        executorService.submit(floorGoodsListTask);

        try {
            data.put("banner", bannerTask.get());

            logger.info("获取到广告列表的数据 ： " + JacksonUtils.bean2Jsn(bannerTask.get()));
            // 目录分类
            data.put("channel", channelTask.get());
            // 优惠券列表
            data.put("couponList", couponListTask.get());
            // 新品上市
            List<MallGoods> newGoodsList = newGoodsListTask.get();
            if (CollectionUtils.isNotEmpty(newGoodsList)) {
                for (int i = 0; i < newGoodsList.size(); i++) {
                    MallGoods myGoods = newGoodsList.get(i);
                    myGoods.setSpreads(GoodsUtils.getSpreads(myGoods));
                }
            }

            data.put("newGoodsList", newGoodsList);

            List<MallGoods> hotGoodsList = hotGoodsListTask.get();
            if (CollectionUtils.isNotEmpty(hotGoodsList)) {
                for (int i = 0; i < hotGoodsList.size(); i++) {
                    MallGoods myGoods = hotGoodsList.get(i);
                    myGoods.setSpreads(GoodsUtils.getSpreads(myGoods));
                }
            }

            // 热卖产品
            data.put("hotGoodsList", hotGoodsList);
            // 品牌制造商
            data.put("brandList", brandListTask.get());
            // 精选商品
            data.put("topicList", topicListTask.get());
            // 团购专区
            data.put("grouponList", grouponListTask.get());
            data.put("floorGoodsList", floorGoodsListTask.get());
            // 缓存数据
            HomeCacheManager.loadData(HomeCacheManager.INDEX, data);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            executorService.shutdown();
        }
        return ResponseUtil.ok(data);
    }

    @GetMapping("/qyIndex")
    public Object qyIndexBak(@LoginUser String userId) {
        log.debug("--------------------进来了qyIndexBak--------------------------------------------");
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        // if (HomeCacheManager.hasData(HomeCacheManager.QYINDEX)) {
        // return ResponseUtil.ok(HomeCacheManager.getCacheData(HomeCacheManager.QYINDEX));
        // }
        Map<String, Object> data = new HashMap<>();

        try {
            String url = platFormServerproperty.getUrl(MallConstant.SUNING);


            Map<String, Object> mappMap = new HashMap<String, Object>();
            mappMap.put("plateform", "fulu");
            mappMap.put("offset", 0);
            mappMap.put("pageSize", 100);
            mappMap.put("position_code", "categoryRights");

            Header[] headers = new BasicHeader[] {new BasicHeader("method", "getQYAllMallCategoryByFuLu"), new BasicHeader("version", "v1.0")};

            String result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);

            JSONObject object = JSONObject.parseObject(result);

            if (!object.getString("errno").equals("0")) {
                return ResponseUtil.fail();
            }

            JSON qyDataList = (JSON) JSON.parse(object.get("data").toString());

            data.put("qyDataList", qyDataList);
            List<MallAd> adList = adService.queryIndex((byte) 25);
            if (CollectionUtils.isNotEmpty(adList)) {
                for (int i = 0; i < adList.size(); i++) {
                    if (StringHelper.isNotNullAndEmpty(adList.get(i).getChannelCode())) {
                        adList.get(i).setBusinessId(adList.get(i).getChannelCode());
                    }
                }
            }

            if (CollectionUtils.isNotEmpty(adList)) {
                for (int i = 0; i < adList.size(); i++) {
                    if (StringHelper.isNotNullAndEmpty(adList.get(i).getChannelCode())) {
                        adList.get(i).setBusinessId(adList.get(i).getChannelCode());
                    }
                }
            }

            data.put("banner", adList);

            // 缓存数据
            // HomeCacheManager.loadData(HomeCacheManager.QYINDEX, data);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // executorService.shutdown();
        }
        return ResponseUtil.ok(data);
    }


    private List<Map<String, Object>> getCategoryList() {
        List<Map<String, Object>> categoryList = new ArrayList<>();
        List<MallCategory> catL1List = categoryService.queryL1WithoutRecommend(0, SystemConfig.getCatlogListLimit());
        for (MallCategory catL1 : catL1List) {
            List<MallCategory> catL2List = categoryService.queryByPid(catL1.getId());
            List<String> l2List = new ArrayList<String>();
            for (MallCategory catL2 : catL2List) {
                l2List.add(catL2.getId());
            }

            List<MallGoods> categoryGoods;
            if (l2List.size() == 0) {
                categoryGoods = new ArrayList<>();
            } else {
                categoryGoods = goodsService.queryByCategory(l2List, 0, SystemConfig.getCatlogMoreLimit());
            }

            Map<String, Object> catGoods = new HashMap<>();
            catGoods.put("id", catL1.getId());
            catGoods.put("name", catL1.getName());
            catGoods.put("goodsList", categoryGoods);
            categoryList.add(catGoods);
        }
        return categoryList;
    }

    /**
     * @return 广告和类目
     * @throws Exception
     */
    @PostMapping("/getHomeDataHSCard")
    public Object getCouponGoodsHSCard(@RequestBody String data, HttpServletRequest request) throws Exception {

        String sign = JacksonUtil.parseString(data, "sign");

        boolean flag = Md5Utils.verifySignature(null, sign);
        if (!flag) {
            return ResponseUtil.fail(403, "无效签名");
        }

        try {
            String url = platFormServerproperty.getUrl(MallConstant.TAOBAO);
            Map<String, Object> mappMap = new HashMap<String, Object>();
            mappMap.put("plateform", "dtk");

            Header[] headers = new BasicHeader[] {new BasicHeader("method", "getHomeDataByHsCard"), new BasicHeader("version", "v1.0")};
            String result = HttpRequestUtils.post(url, JacksonUtils.bean2Jsn(mappMap), headers);

            JSONObject object = JSONObject.parseObject(result);

            if (!"0".equals(object.getString("errno"))) {
                return ResponseUtil.fail();
            }

            return ResponseUtil.ok(JSON.parse(object.get("data").toString()));
        } catch (Exception e) {
            logger.error("商品查询异常");
            throw e;
        }
    }
}
