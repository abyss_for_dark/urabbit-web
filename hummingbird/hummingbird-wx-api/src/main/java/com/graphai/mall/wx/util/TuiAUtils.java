package com.graphai.mall.wx.util;


import com.alibaba.fastjson.JSON;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.mall.wx.vo.TuiAParams;
import com.graphai.mall.wx.vo.TuiAResult;
import com.graphai.mall.wx.vo.TuiAVo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.GZIPOutputStream;

//描述: 推啊广告位链接生成
public class TuiAUtils {

    public static void main(String[] args) {


//
//        TuiAVo tuiAVo = new TuiAVo();
//        TuiAParams params = new TuiAParams();
//
//        params.setImei("11");
//        params.setIdfa("12");
//        params.setOaid("13");
//        params.setDevice_id("14");
//        params.setApi_version("15");
//
//        tuiAVo.setTuiAParams(params);
//        tuiAVo.setGameName("0");
//
//
//        try {
//            generatorUrl(tuiAVo);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

    }

    public static TuiAResult generatorUrl(TuiAVo tuiAVo,String userId) throws Exception{

       /* 广告位 id
        用户设备 ID，Andriod： imei；iOS：idfa
        gameName：
​	        0：  种红包
​	        1： 寻宝
​	        2： 猜成语*/
        String slotId = "";


        if ("".equals(tuiAVo.getTuiAParams().getImei())){
            //iOS
                // 0：  种红包
            if (tuiAVo.getGameName().equals("0")){
                slotId = "377942";
            }
                //  1： 寻宝
            if (tuiAVo.getGameName().equals("1")){
                slotId = "378006";
            }
                //   2： 猜成语*/
            if (tuiAVo.getGameName().equals("2")){
                slotId = "378008";
            }
        }else {
            //安卓

                  // 0：  种红包
            if (tuiAVo.getGameName().equals("0")){
                slotId = "377940";
            }
                 //  1： 寻宝
            if (tuiAVo.getGameName().equals("1")){
                slotId = "378005";
            }
                 //  2： 猜成语*/
            if (tuiAVo.getGameName().equals("2")){
                slotId = "378007";
            }
        }

        String appKey = "4DUL3UGBCHGJb3VfDsoputpNmaax";
        String appSecret = "3XatvRj61wHapxrSS52tiZ61a2CCKtkCw43wAKx";
        Long timestamp = System.currentTimeMillis();

        String appInfo = JsonUtil.objectToJsonStr(tuiAVo.getTuiAParams());

        String md = Base64.getEncoder().encodeToString(zip(appInfo.getBytes()));

        System.out.println("加密后的md: " + URLEncoder.encode(md, "UTF-8"));

        String nonce = "456654";

        String signatureStr = "appSecret=" + appSecret + "&md=" + md + "&nonce=" + nonce + "&timestamp=" + timestamp;

        String signature = DigestUtils.sha1Hex(signatureStr);
//        System.out.println("wode签名"+signature);
//        System.out.println("签名signature: " + DigestUtils.sha1Hex(signature));
//        System.out.println("最终链接: " + "https://engine.lvehaisen.com/index/serving?appKey=" + appKey + "&adslotId=" + slotId + "&md=" + URLEncoder.encode(md, "UTF-8") + "&signature=" + signature + "&timestamp=" + timestamp + "&nonce=" + nonce);
//        System.out.println("最终链接: " + " https://engine.peonyta.com/index/serving?appKey=" + appKey + "&adslotId=" + slotId + "&md=" + URLEncoder.encode(md, "UTF-8") + "&signature=" + signature + "&timestamp=" + timestamp + "&nonce=" + nonce +"&isimageUrl=1");

      String finalUrl ="https://engine.peonyta.com/index/serving?appKey=" + appKey + "&adslotId=" + slotId + "&md=" + URLEncoder.encode(md, "UTF-8") + "&signature=" + signature + "&timestamp=" + timestamp + "&nonce=" + nonce +"&isimageUrl=0";
        System.out.println(finalUrl);

        String s = HttpClientUtils.doGet(finalUrl);

        TuiAResult tuiAResult = JsonUtil.jsonStrToObject(TuiAResult.class, s);
//        String encode = URLEncoder.encode(tuiAResult.getData().getActivityUrl(), "UTF-8");

        tuiAResult.getData().setActivityUrl(URLEncoder.encode(tuiAResult.getData().getActivityUrl()+"&userId="+userId, "UTF-8"));
        tuiAResult.getData().setReportClickUrl(URLEncoder.encode(tuiAResult.getData().getReportClickUrl(), "UTF-8"));
        tuiAResult.getData().setReportExposureUrl(URLEncoder.encode(tuiAResult.getData().getReportExposureUrl(), "UTF-8"));
        return tuiAResult;

    }




    private static byte[] zip(byte[] value) throws IOException {
        if (value == null || value.length == 0) {
            return new byte[0];
        }
        try (ByteArrayOutputStream byteOut = new ByteArrayOutputStream(); GZIPOutputStream gzipOut = new GZIPOutputStream(byteOut)) {
            gzipOut.write(value);
            gzipOut.finish();
            return byteOut.toByteArray();
        }
    }
}