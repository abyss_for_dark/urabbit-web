package com.graphai.mall.wx.util;

import java.util.*;
import java.util.Map.Entry;

/**
 * 字典排序
 * @author ymz
 * @date 2018/11/15 15:23
 */
public class SortUtils {

	/**
	 * 按字段名从小到大排序,获取key对于的值，通过“|”拼接成字符串
	 * @param param 参数
	 * @return string
	 * @since 1.6
	 */
	public static String sort(Map<String, String> param) {
		String content = "";
		try {
			List<Entry<String, String>> itmes = new ArrayList<Entry<String, String>>(param.entrySet());
			//对所有传入的参数按照字段名从小到大排序
			Collections.sort(itmes, new Comparator<Entry<String, String>>() {
				@Override
				public int compare(Entry<String, String> o1, Entry<String, String> o2) {
					return (o1.getKey().compareTo(o2.getKey()));
				}
			});
			//构造键值对的形式
			StringBuffer sb = new StringBuffer();
			for (Entry<String, String> item : itmes) {
				//忽略空值参数
				if(StringUtils.isNotBlank(item.getValue())){
					//sb.append(item.getKey()).append("=").append(item.getValue()).append("&");
					sb.append(item.getKey()).append("|");
				}
			}
			content = sb.toString();
			if (!content.isEmpty()&&content.endsWith("|")) {
				content = content.substring(0, content.length() - 1);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
		return content;
	}
}

