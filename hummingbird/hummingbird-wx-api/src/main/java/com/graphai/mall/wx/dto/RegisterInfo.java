package com.graphai.mall.wx.dto;

import java.util.Map;

import com.graphai.mall.tb.dto.TaoBaoBaiChuanUserInfo;

import lombok.Data;

@Data
public class RegisterInfo {

	// {
	// "username": "15920880905",
	// "password": "qwerty1234",
	// "invitationCode": "TDSQ",
	// "wxCode": "033yx5ae1WVtay0XP5be1Qsaae1yx5aR",
	// "valCode": "742648",
	// "registerChannel": "miniProgram",
	// "miniProgramUserInfo": {
	// "nickName": "biteam",
	// "gender": 1,
	// "language": "zh_CN",
	// "city": "Guangzhou",
	// "province": "Guangdong",
	// "country": "China",
	// "avatarUrl":
	// "https://wx.qlogo.cn/mmopen/vi_32/OO9iaoU3WyWKrU1FvuSkPEVBc9ia4Mu7FpaZbK4YdqiaXA7CdjicDuIAtODHmpDJgACTcEqAjBpe2Y6BXMdGSh66LA/132"
	// }
	
	/**是否淘宝授权**/  
	private String isTaobaoAuth;   //1重新授权

	/**用户ID**/
	private String userId;
	/**自定义注册的时候用到**/
	private String username;
	/**自定义注册的时候用到**/
	private String password;
	/**自定义注册的时候用到**/
	private String phone;
	/**自定义注册的时候用到**/
	private String rephone;
	/**自定义注册的时候用到**/
	private String province;
	/**自定义注册的时候用到**/
	private String city;
	/**自定义注册的时候用到**/
	private String district;
	/**自定义注册的时候用到**/
	private String detail;
	/**推荐人id,通过分享链接注册的时候用到**/
	private String firstLeaderId;
	/**渠道商id,通过渠道商分享的链接注册**/
	private String channelId;
	/**用户微信公众号授权openId**/
	private String weixinMpOpenid;

	/**分销系统验证码**/
	private String invitationCode;
	/**微信授权验证码**/
	private String wxCode;
	/**手机短信发送的验证码**/
	private String valCode;
	/**注册渠道**/
	private String registerChannel;
	/**微信公众号H5授权时带入的跳转链接**/
	private String redirectURI;
	/**是否开始账户(钱包)**/
	private String openAccount;
	/**分销员ID**/
	private String tpid;
	/**前端传入的路由参数数据* */
	private Map<String, Object> router;
	/**微信公众号登录的时候用到**/
	private UserInfo userInfo;
	/**APP授权登录微信**/
	private WxAppAuthInfo wxAppAuthInfo;
	/**小程序登录的时候用到**/
	private MiniProgramUserInfo miniProgramUserInfo;
	/**淘宝登录的时候用到**/
	private TaoBaoBaiChuanUserInfo taoBaoBaiChuanUserInfo;
}
