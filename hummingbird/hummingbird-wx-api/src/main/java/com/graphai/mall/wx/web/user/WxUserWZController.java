package com.graphai.mall.wx.web.user;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.mall.db.constant.user.MallUserEnum;
import com.graphai.mall.db.domain.MallUser;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.wx.annotation.LoginUser;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * 用户管理-网赚任务
 * @author Qxian
 * @date 2021-03-22
 */
@RestController
@RequestMapping("/wx/user/wz")
@Validated
public class WxUserWZController {

    private final Log logger = LogFactory.getLog(WxUserWZController.class);

    @Autowired
    private MallUserService userService;

    /**
     * 1、完成刷脸认证
     * @param userId 用户ID
     * @return Object
     */
    @PostMapping("finishFace")
    public Object finishedFace(@LoginUser String userId) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        MallUser user = userService.findById(userId);
        if (user == null) {
            return ResponseUtil.fail("认证失败");
        }
        if (MallUserEnum.IS_IDENTITY_1.strCode2Byte().equals(user.getIsIdentity())) {
            return ResponseUtil.fail("已认证过了");
        }
        user.setIsIdentity(MallUserEnum.IS_IDENTITY_1.strCode2Byte());
        userService.updateById(user);
        return ResponseUtil.ok("刷脸认证成功");
    }

    /**
     * 2、查询用户是否已完成刷脸认证
     * @param userId 用户ID
     * @return Object
     */
    @GetMapping("findFinishFace")
    public Object findFinishedFace(@LoginUser String userId) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        Map<String,Object> resultMap = new HashMap<String,Object>();
        resultMap.put("isIdentity","0");
        MallUser user = userService.findById(userId);
        if (MallUserEnum.IS_IDENTITY_1.strCode2Byte().equals(user.getIsIdentity())) {
            resultMap.put("isIdentity","1");
        }
        return ResponseUtil.ok(resultMap);
    }



}
