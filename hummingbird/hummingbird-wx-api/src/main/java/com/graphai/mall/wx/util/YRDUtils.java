package com.graphai.mall.wx.util;

import com.graphai.payment.utils.rsa.RSA;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

public class YRDUtils {

    private static final String url = "http://agrydapi.rydysc.deayou.com/";//测试接口地址
    private static final String urlZS ="http://agrydapi.rongyiduo.cn/";//正式接口地址
    private static final String mercId = "68439";//商户号
//    private static final String mercId = "66760";//测试商户号
    //正式私钥
    private static final String privateKey = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBALsDIpEL6GAyiVxVN70YW9bwyRMEgfwLSaMB9Bno4rXpWvV4Ebp96EZluq7i0fzKyJQxZZ5I+KrXT1MQgbqaLh8ZANfhhhxJEglHSB88pFVYxBqcmM5kL7vTsoQV2/0/HU0WUEcFeQr9SqRltHQegVK7oHBaEOyVcYH/bDnA5MutAgMBAAECgYB6/YPVOqyX34Sg+daPgR6dTHwhCrzl4xKJpvY/t/6pvo+4kj+uk7GX4r2U2M1bYI3rkFURdVI9IR8lNNzsuQuM/g/MiLZxoXNoC+B/SUkk2nSoWtUrTDYtWoOvS+y3O0466qfworlCleuhaoIJQ5D/b7skhDP/pZKHkpbDhVwjAQJBAOTiyNH3s3LYWwVTktrEWKKrtLnAO72tKuFKA2jMiUsrE0qMy7a3vhlfBorfsjoobi4PVMDPqxTgkmbM9qvKoW0CQQDRKnybRzFC3Xv3RK8C2IIpyFcQZi008XjJZT4ydNP6L5d8pNT5D1+tDjrxX/g5KYrzZAyLuj7BN4uGsdEGVqtBAkEAkqUub0BZi7pMzuPV0i48b7S9bMlaPVNmhsNJ3eo2CKAHkxsUJfQhatX0NiPOZ+6hcQdHkOU8wnqDsjOd00FXHQJAI+r91tRU2zyZaw66VuzC/VXPzZseLDdDHKJsGdICDLZbdyX/BH3lWhxVdtmaEIbXu4/0r5Hd8OQO66RhVotPgQJAQFIfzkNVF4vczFKFAi1aXm8SCp8qRGp9u+DFQTkEs0lP6n2chDxrH48lO65dHwX+8YxKeRsLUaZKkXGMtd9Z/Q==";
    //测试私钥
//    private static final String privateKey = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAMyIj2/Com5fdVjW6p2/qOhEvPnQdLd0WnZ5jVuthHa4PceNiSvL+gtODZ/BWxXD1H+6WPr83VYdV3nJZBIn9dYsA9XxLByPY/L22bvyWdmwWXHhZ0/qKvVn+z+ew6bbDAYpGhtBCDZTWkkKfo1FIFtCMlcdQcwp857L0wrhPEg7AgMBAAECgYEAwxu94HnFow4yjcWuVz7+bFst/gxpWpuGtB+txfeXNfz5CgQtBvd0lhYmDvBO6Es/yXxE52AR79TbnlSLvnoaGjidGHrVj0r/0429QGiQ9NHSN7CL8/iWs4r5STWsboq4E+P6cns02ze1QFxS/MSGf+BDguTCquehM9az8GcRNbkCQQDmV9YUCkXVdaQzblRNENIxLfcFjwUsk6PCNClZnD1J7Vniw/Bxp2fCuE56wyyYdAkRRuhnm5NqS7kR9Cn8L191AkEA41DE5MTcGcAwVPon9WiN4jZzvimbKGhUXztPt6s+BGNpS2JbFxcrwQtuE2nzUHrSB8KnqgrG7hl/9/Eaug5C7wJBAK6yxZmzZN2NeLjNH8DHVjOYgfZET49MdnHvL8FX/x/XlUmog+8fIVIMiPEQn/0MXGgvGEqkMvz2a2nTfhhXMZUCQFG5ljBRfnhbmoTSs08N2gT8Yrx5pOjwKXVbk5uLG27kdtS9ddZlZDuxak/chbfnkF9t4m3GFiNAU7vGmT9PJU0CQDWnOKXwOSOuFF4sV64ZPFz0zljXaFqH0J+9zh2F5IeqVtM5W18Bd5Nu9zC+bACGOfMW3x5aIVaQ0wrhkYH+nHk=";
    public static void main(String[] args) {
//        doBankList("product/bank/apply");//银行信用卡申请
//        doBankList("product/bank/list"); //银行卡列表
                doBankList("product/bank/apply","zhansan","13547854125","136"); //银行卡列表
    }
    /**
     * 申请银行信用卡
     * @param interfaceUrl 银行信用卡接口地址
     */
    public static Map<String,Object> doBankList(String interfaceUrl,String name,String phone,String productId) {
        Map<String, String> paramMap = new HashMap<String, String>();
        paramMap.put("channel_id", mercId); // 渠道id
        paramMap.put("version", "1");// 版本号
        paramMap.put("product_id",productId);//银行申卡产品id
        paramMap.put("name",name);//姓名
        paramMap.put("phone",phone);//手机号码
        // 按字母进行升序排序
        String sortStr = SortUtils.sort(paramMap);
        System.out.println("排序后：" + sortStr);
        // 签名
        String sign = RSA.sign(sortStr, privateKey, "utf-8");
        System.out.println("sign----"+sign);
        paramMap.put("check_value", sign); // RSA加密字符串

        String response = null;
        try {
            response = HttpClientUtils.doPost(urlZS + interfaceUrl, paramMap);
            System.out.println("返回结果：" + response);
        } catch (Exception e) {
            System.out.println("BankListDemo doBankList exception："+ e.getStackTrace());
            e.printStackTrace();
        }
        Map<String, Object> result = new HashMap<>();
        if(StringUtils.isNotBlank(response)){
             result = JsonUtil.JsonStrToMap(response);
            System.out.println("最终返回结果：" + result);
        }
      return result;
    }
    /**
     * 获取银行卡列表
     * @param interfaceUrl 银行卡列表接口地址
     */
    private static void doBankList(String interfaceUrl) {
        Map<String, String> paramMap = new HashMap<String, String>();
        paramMap.put("channel_id", mercId); // 渠道id
        paramMap.put("version", "1");// 版本号
        paramMap.put("product_id","136");//银行申卡产品id
        paramMap.put("name","张三");//姓名
        paramMap.put("phone","13612457845");//手机号码
        // 按字母进行升序排序
        String sortStr = SortUtils.sort(paramMap);
        System.out.println("排序后：" + sortStr);

        // 签名
        String sign = RSA.sign(sortStr, privateKey, "utf-8");
        paramMap.put("check_value", sign); // RSA加密字符串

        String response = null;
        try {
            response = HttpClientUtils.doPost(url + interfaceUrl, paramMap);
            System.out.println("返回结果：" + response);
        } catch (Exception e) {
            System.out.println("BankListDemo doBankList exception："+ e.getStackTrace());
            e.printStackTrace();
        }

        if(StringUtils.isNotBlank(response)){
            Map<String, Object> result = JsonUtil.JsonStrToMap(response);
            System.out.println("最终返回结果：" + result);
        }

    }




}
