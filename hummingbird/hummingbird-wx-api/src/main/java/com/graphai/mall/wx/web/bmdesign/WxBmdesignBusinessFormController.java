package com.graphai.mall.wx.web.bmdesign;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.mall.db.domain.BmdesignBusinessForm;
import com.graphai.mall.db.service.bmdesign.BmdesignBusinessFormService;
import com.graphai.validator.Order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 业务形态及规则配置
 */
@RestController
@RequestMapping("/wx/bmdesignBusinessForm")
@Validated
public class WxBmdesignBusinessFormController {

    @Autowired
    private BmdesignBusinessFormService bmdesignBusinessFormService;

    /**
     * 查询业务形态及规则配置
     */
    @GetMapping("/list")
    public Object list(@RequestParam(defaultValue = "id") String sort,
                       @Order @RequestParam(defaultValue = "asc") String order) {
        BmdesignBusinessForm bmdesignBusinessForm = new BmdesignBusinessForm();
        bmdesignBusinessForm.setFormStatus("1");    //上架
        List<BmdesignBusinessForm> list = bmdesignBusinessFormService.selectBmdesignBusinessFormList(bmdesignBusinessForm, null, null, sort, order);
        Map<String, Object> data = new HashMap<>();
        data.put("items", list);
        return ResponseUtil.ok(data);
    }

}
