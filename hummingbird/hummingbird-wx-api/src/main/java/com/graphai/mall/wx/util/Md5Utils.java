package com.graphai.mall.wx.util;


import java.util.Map;
import java.util.TreeMap;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import org.apache.commons.codec.digest.DigestUtils;
import cn.hutool.core.util.ObjectUtil;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Md5Utils {
	
	  /**
     * 签名验证 MD5
     *
     * @param string 签名的字符串对象jsonStr
     * @param sign   请求签名
     * @return  true通过  false不通过
     */
    public static boolean verifySignature(String string, String sign) {
        Map<String, Object> stringStringMap = JSONObject.parseObject(string, new TypeReference<Map<String, Object>>() {});
        StringBuffer sb = new StringBuffer();
        
        log.info("入参值{}", string);

        TreeMap<String, Object> objectObjectTreeMap = new TreeMap<>();
        if (ObjectUtil.isNotNull(stringStringMap)) {
        	objectObjectTreeMap.putAll(stringStringMap);
		}
        
//        logger.info("签名待处理json map{}", JSONObject.toJSONString(objectObjectTreeMap));

        for (String str : objectObjectTreeMap.keySet()) {
            Object v = objectObjectTreeMap.get(str);
            if (null != v && ! "".equals(v)) {
                sb.append(str + "=" + v + "&");
            }
        }
        //MD5加密,结果转换为大写字符
        String str = String.valueOf(sb.append("key=" + "b6a007a7cd7e77d13df3f2072aa44f4a"));
//        logger.info("进行签名的字符串{}", str);
        String s = DigestUtils.md5Hex(str).toUpperCase();
         log.info("签名值{}", s);
//        logger.info("系统签名{}", s);
//        logger.info("请求签名{}", sign);
        if (s.equals(sign)) {
//            logger.info("校验通过");
            return true;
        } else {
//            logger.info("校验失败");
            return false;
        }
    }
    
    public static void main(String[] args) {
    	
    	
             
             
           
   
	}

}
