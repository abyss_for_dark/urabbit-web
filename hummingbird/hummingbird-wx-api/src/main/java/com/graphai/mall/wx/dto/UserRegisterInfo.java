package com.graphai.mall.wx.dto;

import lombok.Data;

@Data
public class UserRegisterInfo{
    private String mobile;
    private String avatar;
    private String nickname;
    private String code;
    private String sessionKey;
    private String weixinMiniOpenid;
    private String weixinOpenid;
    private String invitationCode;
    private String unionid;
    private String encryptedData;
    private String iv;
    private String country;
    private String province;
    private String city;
    private String language;
    private Byte gender;
    private String wxMpCode;
    private String source;
}
