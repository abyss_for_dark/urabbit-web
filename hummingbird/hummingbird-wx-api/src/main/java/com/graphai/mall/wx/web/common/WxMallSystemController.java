package com.graphai.mall.wx.web.common;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.lang.StringUtils;
import com.graphai.mall.db.domain.MallSystem;
import com.graphai.mall.db.service.sysconfig.MallSystemConfigService;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * 配置表数据 《mall_system》
 */
@RestController
@RequestMapping("/wx/mallSystem")
@Validated
public class WxMallSystemController {

    @Autowired
    private MallSystemConfigService systemConfigService;

    /**
     * 查询分享页面配置参数
     */
    @GetMapping("/mallShareParam")
    public Object mallShareParam() {
        Map<String, String> data = systemConfigService.listShare();
        return ResponseUtil.ok(data);
    }

    /**
     * 查询代理端配置的参数信息(代理端文案说明、代理端上级名称、代理端上级电话、代理端首页图片、代理端提现文案说明)
     */
    @GetMapping("/mallAgentParam")
    public Object mallAgentParam() {
        Map<String, String> data = systemConfigService.listAgent();
        return ResponseUtil.ok(data);
    }

    /**
     * 获取配置表数据公共参数（单个）
     */
    @GetMapping("/getSystemConfSingle")
    public Object getSystemSingle(@Param("keyName") String keyName) {
        if (StringUtils.isBlank(keyName)) {
            return ResponseUtil.fail("参数不能为空");
        }
        Map<String, String> data = systemConfigService.getMallSystemCommonSingle(keyName);
        return ResponseUtil.ok(data);
    }

    /**
     * 获取配置表数据公共参数（多个）
     */
    @GetMapping("/getSystemConfMore")
    public Object getSystemMore(@Param("typeCode") String typeCode) {
        if (StringUtils.isBlank(typeCode)) {
            return ResponseUtil.fail("参数不能为空");
        }
        List<MallSystem> data = systemConfigService.getMallSystemCommonMore(typeCode);
        return ResponseUtil.ok(data);
    }

}
