package com.graphai.mall.wx.dto;

import com.graphai.mall.db.domain.MallAftersale;
import com.graphai.open.mallorder.entity.IMallOrder;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class AfterSaleInfo {

    private MallAftersale mallAftersale;

    private String applicationMethod;

    private String flag;

    private BigDecimal modifyPrice;

    private IMallOrder order;

    private Short OrderType;

}
