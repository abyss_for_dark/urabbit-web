package com.graphai.mall.wx.web.tripartite;



import com.alibaba.fastjson.JSONArray;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.json.JacksonUtil;
import com.graphai.id.GraphaiIdGenerator;
import com.graphai.mall.admin.service.IGameRedHourRuleService;
import com.graphai.mall.db.domain.*;
import com.graphai.mall.db.service.account.AccountService;
import com.graphai.mall.db.constant.account.AccountStatus;
import com.graphai.mall.db.service.account.FcAccountRechargeBillService;
import com.graphai.mall.db.service.order.MallOrderService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.service.wangzhuan.WangzhuanTaskService;
import com.graphai.mall.db.service.wangzhuan.WangzhuanUserTaskService;
import com.graphai.mall.db.util.account.AccountUtils;
import com.graphai.mall.db.constant.CommonConstant;
import com.graphai.mall.wx.annotation.LoginUser;

import com.suning.api.util.StringUtil;
import net.sf.json.JSONObject;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.time.LocalDateTime;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/wx/xinawan")
@Validated
public class WxXianWanController {
    private final Log logger = LogFactory.getLog(WxXianWanController.class);

    @Autowired
    private AccountService accountService;
    @Autowired
    private MallOrderService orderService;
    @Autowired
    private WangzhuanUserTaskService wangzhuanUserTaskService;
    @Resource
    private MallUserService  mallUserService;
    @Resource
    private FcAccountRechargeBillService fcAccountRechargeBillService;
    @Resource
    private WangzhuanTaskService wangzhuanTaskService;
    @Autowired
    private IGameRedHourRuleService redHourRuleService;

    //跳转闲玩
    @PostMapping("/receive")
    public Object getXianWanLink(@LoginUser String userId, @RequestBody String data) throws UnsupportedEncodingException {

//  加密顺序           appid+deviceid+msaoaid+androidosv+ptype+appsign+appsecret

        try {
         if(userId == null){
             return ResponseUtil.unlogin();
         }
        MallUser user =  mallUserService.findById(userId);


        String deviceid = JacksonUtil.parseString(data,"deviceid");
        String msaoaid = JacksonUtil.parseString(data,"msaoaid");
        String ptype = JacksonUtil.parseString(data,"ptype");
        String xwversion = JacksonUtil.parseString(data,"xwversion");
        String androidosv = JacksonUtil.parseString(data,"androidosv");
        String type = JacksonUtil.parseString(data,"type");

        if(null != user){
            //只有设备号
            if(!StringUtil.isNullOrEmpty(deviceid) && !"0".equals(deviceid)){
                user.setDeviceId(deviceid);
            }
            //只有soaid
            else if(!StringUtil.isNullOrEmpty(msaoaid) && !"0".equals(msaoaid) && StringUtil.isNullOrEmpty(deviceid) && "0".equals(deviceid)){
                user.setDeviceId(msaoaid);
            }
            //soaid和设备号都有
            else if(!StringUtil.isNullOrEmpty(msaoaid) && !"0".equals(msaoaid) && !StringUtil.isNullOrEmpty(deviceid) && !"0".equals(deviceid) ){
                user.setDeviceId(deviceid);
            }
            mallUserService.updateById(user);
        }
        String signString ="";
        //安卓
        if("2".equals(ptype)){
            if("1".equals(type)){
                // 生产
                signString = CommonConstant.Aappid+deviceid+msaoaid+androidosv+ptype+userId+CommonConstant.AappSecret;
            }else if("2".equals(type)){
                //生产备用
                signString = CommonConstant.ScAappid+deviceid+msaoaid+androidosv+ptype+userId+CommonConstant.ScAappSecret;
            }else if("3".equals(type)){
                //测试
                signString = CommonConstant.CsAappid+deviceid+msaoaid+androidosv+ptype+userId+CommonConstant.CsAappSecret;
            }
        }
        //ios
        else if("1".equals(ptype)){
            if("1".equals(type)){
                // 生产
                signString = CommonConstant.Iappid+deviceid+msaoaid+androidosv+ptype+userId+CommonConstant.IappSecret;
            }else if("2".equals(type)){
                //生产备用
                signString = CommonConstant.ScIappid+deviceid+msaoaid+androidosv+ptype+userId+CommonConstant.ScIappSecret;
            }else if("3".equals(type)){
                //测试
                signString = CommonConstant.CsIappid+deviceid+msaoaid+androidosv+ptype+userId+CommonConstant.CsIappSecret;
            }

        }
        String sign = DigestUtils.md5Hex(signString);
        String reqUrl="";
        //安卓
        if("2".equals(ptype)){
            if("1".equals(type)){
                // 生产
                reqUrl = CommonConstant.xwUrl+"?"+"appid="+CommonConstant.Aappid+"&deviceid="+deviceid+"&msaoaid="
                        +msaoaid+"&ptype="+ptype+"&androidosv="+androidosv+
                        "&keyCode="+sign+"&appsign="+userId+"&xwversion="+xwversion;
            }else if("2".equals(type)){
                //生产备用
                reqUrl = CommonConstant.xwUrl+"?"+"appid="+CommonConstant.ScAappid+"&deviceid="+deviceid+"&msaoaid="
                        +msaoaid+"&ptype="+ptype+"&androidosv="+androidosv+
                        "&keyCode="+sign+"&appsign="+userId+"&xwversion="+xwversion;
            }else if("3".equals(type)){
                //测试
                reqUrl = CommonConstant.xwUrl+"?"+"appid="+CommonConstant.CsAappid+"&deviceid="+deviceid+"&msaoaid="
                        +msaoaid+"&ptype="+ptype+"&androidosv="+androidosv+
                        "&keyCode="+sign+"&appsign="+userId+"&xwversion="+xwversion;
            }
        }
        //ios
        else if("1".equals(ptype)){

            if("1".equals(type)){
                // 生产
                reqUrl = CommonConstant.xwUrl+"?"+"appid="+CommonConstant.Iappid+"&deviceid="+deviceid+"&msaoaid="
                        +msaoaid+"&ptype="+ptype+"&androidosv="+androidosv+
                        "&keyCode="+sign+"&appsign="+userId+"&xwversion="+xwversion;
            }else if("2".equals(type)){
                //生产备用
                reqUrl = CommonConstant.xwUrl+"?"+"appid="+CommonConstant.ScIappid+"&deviceid="+deviceid+"&msaoaid="
                        +msaoaid+"&ptype="+ptype+"&androidosv="+androidosv+
                        "&keyCode="+sign+"&appsign="+userId+"&xwversion="+xwversion;
            }else if("3".equals(type)){
                //测试
                reqUrl = CommonConstant.xwUrl+"?"+"appid="+CommonConstant.CsIappid+"&deviceid="+deviceid+"&msaoaid="
                        +msaoaid+"&ptype="+ptype+"&androidosv="+androidosv+
                        "&keyCode="+sign+"&appsign="+userId+"&xwversion="+xwversion;
            }
        }

           Map<String,String> map  = new HashMap<>();

            map.put("url",URLEncoder.encode( reqUrl, "UTF-8" ));
            return ResponseUtil.ok(map);
        } catch (Exception e) {
            logger.error("领取任务异常：" +e.getMessage() ,e);
            throw e;
        }


    }



    @RequestMapping(value = "/order" ,produces = "application/json;charset=UTF-8;")
    public Object callBackXianWanLink(@RequestParam(defaultValue = "") String adid,@RequestParam (defaultValue = "") String adname,
                                      @RequestParam(defaultValue = "") String appid,@RequestParam(defaultValue = "") String ordernum,
                                      @RequestParam(defaultValue = "") String dlevel,@RequestParam(defaultValue = "") String pagename,
                                      @RequestParam(defaultValue = "") String atype,@RequestParam(defaultValue = "") String deviceid,
                                      @RequestParam(defaultValue = "") String simid,@RequestParam(defaultValue = "") String appsign,
                                      @RequestParam(defaultValue = "") String merid,@RequestParam(defaultValue = "") String event,
                                      @RequestParam(defaultValue = "") String adicon,@RequestParam(defaultValue = "") String price,
                                      @RequestParam(defaultValue = "") String money,@RequestParam(defaultValue = "") String itime,
                                      @RequestParam(defaultValue = "") String keycode)  {

        MallUser user =  mallUserService.findById(appsign);
        List<FcAccountRechargeBill> list = fcAccountRechargeBillService.selectRechargeBillByOrderSn1(ordernum,user.getId());
        Map map = new HashMap();
       if( null != user){
           Map<String,Object> paramsMap = new HashMap();
           paramsMap.put("dType",AccountStatus.DISTRIBUTION_TYPE_00);  //直接分润
           paramsMap.put("message",event);
           paramsMap.put("orderSn",ordernum);
           paramsMap.put("capitalTrend",AccountStatus.CAPITAL_TREND_01);
           paramsMap.put("changeType",AccountStatus.CHANGE_TYPE_01);
           paramsMap.put("changeAccountType",AccountStatus.CHANGE_ACCOUNT_TYPE_2);
           paramsMap.put("xianWanPrice",price);
           paramsMap.put("tradeType",AccountStatus.TRADE_TYPE_10);
           paramsMap.put("bid",AccountStatus.BID_32);

           Map<String,Object> paramsMapMaker = new HashMap();
           paramsMapMaker.put("dType",AccountStatus.DISTRIBUTION_TYPE_00);  //直接分润
           paramsMapMaker.put("message","粉丝完成:( "+event+" )收益");
           paramsMapMaker.put("orderSn",ordernum);
           paramsMapMaker.put("capitalTrend",AccountStatus.CAPITAL_TREND_01);
           paramsMapMaker.put("changeType",AccountStatus.CHANGE_TYPE_01);
           paramsMapMaker.put("changeAccountType",AccountStatus.CHANGE_ACCOUNT_TYPE_2);
           paramsMapMaker.put("xianWanPrice",price);
           paramsMapMaker.put("tradeType",AccountStatus.TRADE_TYPE_10);
           paramsMapMaker.put("bid",AccountStatus.BID_32);
           if(new BigDecimal(money).compareTo(new BigDecimal("0")) == 1  ){
//               BigDecimal gold = (BigDecimal) AccountUtils.exchangeGold(money,CommonConstant.TASK_REWARD_GOLD);
               if(list.size() == 0){
                   try {
                       //创建订单
                       MallOrder mallOrder = new MallOrder();
                       mallOrder.setId(GraphaiIdGenerator.nextId("xianwan"));
                       mallOrder.setUserId(user.getId());
                       mallOrder.setMessage(event);
                       mallOrder.setHuid(AccountStatus.BID_32);
                       mallOrder.setBid(AccountStatus.BID_32);
                       mallOrder.setOrderSn(ordernum);
                       mallOrder.setDeleted(false);
                       mallOrder.setPayTime(LocalDateTime.now());
                       mallOrder.setAddTime(LocalDateTime.now());
                       mallOrder.setUpdateTime(LocalDateTime.now());
                       mallOrder.setPubSharePreFee(money);
                       mallOrder.setActualPrice(new BigDecimal(price));
                       mallOrder.setOrderStatus((short)108);
                       orderService.add(mallOrder);
                       //创建任务记录
                       WangzhuanUserTask wangzhuanUserTask = new WangzhuanUserTask();
                       if(wangzhuanTaskService.selectTaskList().size()>0){
                           wangzhuanUserTask.setTaskId(wangzhuanTaskService.selectTaskList().get(0).getId());
                       }
                       wangzhuanUserTask.setId(GraphaiIdGenerator.nextId("wangzhaun"));
                       wangzhuanUserTask.setUserId(user.getId());
                       wangzhuanUserTask.setRemarks(event);
                       wangzhuanUserTask.setDeleted(false);
                       wangzhuanUserTask.setTstatus("UST104");
                       wangzhuanUserTask.setRewardType("JL20002");
                       wangzhuanUserTask.setReward(new BigDecimal(money));
                       wangzhuanUserTask.setCreateTime(LocalDateTime.now());
                       wangzhuanUserTask.setUpdateTime(LocalDateTime.now());
                       wangzhuanUserTask.setHuid(AccountStatus.BID_32);
                       wangzhuanUserTaskService.insertWangzhuanUserTask(wangzhuanUserTask);
                       if(null != mallOrder.getId() ){
                           paramsMap.put("orderId",mallOrder.getId());
                       }
                       /**用户收益*/
                       BigDecimal residuer = new BigDecimal(money);
                       //用户以及直推人充值
                       if( StringUtils.isNotBlank(user.getDirectLeader())){
                           MallUser recomUser = mallUserService.findById(user.getDirectLeader());
                           AccountUtils.accountRechargeSF(residuer,user,recomUser,paramsMap,paramsMapMaker);
                       }else{
                           AccountUtils.accountRechargeSF(residuer,user,null,paramsMap,paramsMapMaker);
                       }

                       //平台充值
                       AccountUtils.accountChangeByExchange("00", new BigDecimal(price).subtract(new BigDecimal(money)), CommonConstant.TASK_REWARD_CASH,AccountStatus.BID_32,paramsMap);
                   } catch (Exception e) {
                       e.printStackTrace();
                       map.put("success",0);
                       map.put("message",e);
                   }
                   map.put("success",1);
                   map.put("message","接收成功");
               }else if(list.size() >= 1){
                   map.put("success",1);
                   map.put("message","订单已接收");
               }
           }

       }



        return map;
    }

    /**
     * 聚享玩游戏
     * 返回跳转url，并绑定用户
     * @param userId
     * @param data
     * @return
     */
   @PostMapping("/jumpJXW")
    public Object jumpJuXiangWan(@LoginUser String userId, @RequestBody String data, HttpServletRequest request) throws UnsupportedEncodingException {

       // 说明：sign=md5(mid+resource_id+token)(符号+为连接符，实际不存在)

      // 接口示例：http://api.juxiangwan.com/?act=fast_reg&act_type=1&mid=5&resource_id=
      // 10001&sign=4f813943c2736f8054f9874935696c70
      // &device_code=868060033371878&from=h5android&version=2.0

       if(userId == null){
           return ResponseUtil.unlogin();
       }

       String mid ="";
       String token ="";
       String from = "";//
       String mobileTye = request.getHeader("User-Agent");
       if (mobileTye.contains("iPhone")) {
           from = "h5ios";
           mid ="1202";//ios 渠道mid
           token = "3d99680bbd5c1cb690ebf3bbd8feab61";//ios
       }else if (mobileTye.contains("Android")){
           from = "h5android";
           mid ="1201";//安卓 渠道mid
           token = "0b9fd4b8a3c53a1f687cdf422873f07b";//安卓
       }
       String act = "fast_reg";//固定值
       String act_type = "1";//固定值
       String resource_id = userId;//用户标识
       String device_code = JacksonUtil.parseString(data,"device_code");//用户手机标识码
       String oaid = JacksonUtil.parseString(data,"oaid");//oaid(仅安卓，ios 不需此参数)
       String sdkversion = JacksonUtil.parseString(data,"sdkversion");//sdkversion 大于等于 29 为安卓 10(仅安卓端，ios端 不需此参数)
       String version = "2.0";
       String sign =mid+resource_id+token;//签名
       String sign1 = DigestUtils.md5Hex(sign);
       String reqUrl ="";
       if(!StringUtil.isEmpty(device_code)){
           reqUrl = CommonConstant.jxwUrl+"act="+act+"&act_type="+act_type+"&mid="+mid+"&resource_id="+resource_id+"&sign="+sign1+"&device_code="+device_code+"&from="+from+"&version="+version ;
       }
       if(!StringUtil.isEmpty(oaid)){
           reqUrl = CommonConstant.jxwUrl+"act="+act+"&act_type="+act_type+"&mid="+mid+"&resource_id="+resource_id+"&sign="+sign1+"&oaid="+oaid+"&from="+from+"&version="+version ;
       }

         Map<String,String> map  = new HashMap<>();
         map.put("url",URLEncoder.encode( reqUrl, "UTF-8" ));

         return ResponseUtil.ok(map);

   }

   @RequestMapping(value = "/jxwOrder" ,produces = "application/json;charset=UTF-8;")
   public  Object getJxwOrder(@RequestParam Integer mid,@RequestParam (defaultValue = "") String resource_id,
                              @RequestParam Integer time,@RequestParam(defaultValue = "") String prize_info,
                              @RequestParam Integer adid,@RequestParam(defaultValue = "") String device_code,
                              @RequestParam(defaultValue = "") Integer field,@RequestParam(defaultValue = "") String icon,
                              @RequestParam(defaultValue = "") String sign) throws UnsupportedEncodingException {
        logger.info("聚享玩回调-----"+prize_info);
       MallUser user =  mallUserService.findById(resource_id);
       //       md5(prize_info+mid+time+resource_id+token)
       String tokenA="0b9fd4b8a3c53a1f687cdf422873f07b";
       String tokenI="3d99680bbd5c1cb690ebf3bbd8feab61";
       String signA=prize_info+mid+time+resource_id+tokenA;//安卓
       String signI=prize_info+mid+time+resource_id+tokenI;//ios
       String sign1 = DigestUtils.md5Hex(signA);
       JSONArray arr = JSONArray.parseArray(prize_info);
       for(int i=0;i<arr.size();i++){
          JSONObject str = JSONObject.fromObject(arr.get(i));
           String prizeId = str.get("prize_id").toString();//流水号
           String taskPrize = str.get("task_prize").toString();//用户奖励
           String dealPrize = str.get("deal_prize").toString();//平台利润
           String taskPrizeCoin = str.get("task_prize_coin").toString();//奖励金额

           String name = str.get("name").toString();
           if(null != user){
               List<FcAccountRechargeBill> list = fcAccountRechargeBillService.selectRechargeBillByOrderSn1(prizeId,user.getId());

               if (list.size() == 0) {
                   Map paramsMap = new HashMap();
                   //直接分润
                   paramsMap.put("dType", AccountStatus.DISTRIBUTION_TYPE_00);
                   paramsMap.put("message", name);
                   paramsMap.put("orderSn", prizeId);
                   paramsMap.put("capitalTrend", AccountStatus.CAPITAL_TREND_01);
                   paramsMap.put("changeType", AccountStatus.CHANGE_TYPE_01);
                   paramsMap.put("changeAccountType", AccountStatus.CHANGE_ACCOUNT_TYPE_2);
                   paramsMap.put("tradeType", AccountStatus.TRADE_TYPE_10);
                   paramsMap.put("bid", AccountStatus.BID_33);
                   Map<String,Object> paramsMapMaker = new HashMap();
                   //直接分润
                   paramsMapMaker.put("dType",AccountStatus.DISTRIBUTION_TYPE_00);
                   paramsMapMaker.put("message","粉丝完成:( "+name+" )收益");
                   paramsMapMaker.put("orderSn",prizeId);
                   paramsMapMaker.put("capitalTrend",AccountStatus.CAPITAL_TREND_01);
                   paramsMapMaker.put("changeType",AccountStatus.CHANGE_TYPE_01);
                   paramsMapMaker.put("changeAccountType",AccountStatus.CHANGE_ACCOUNT_TYPE_2);
                   paramsMapMaker.put("tradeType",AccountStatus.TRADE_TYPE_34);
                   paramsMapMaker.put("bid", AccountStatus.BID_33);
                   try {
                       //创建订单
                       MallOrder mallOrder = new MallOrder();
                       mallOrder.setId(GraphaiIdGenerator.nextId("xianwan"));
                       mallOrder.setUserId(user.getId());
                       mallOrder.setMessage(name);
                       mallOrder.setHuid(AccountStatus.BID_33);
                       mallOrder.setBid(AccountStatus.BID_33);
                       mallOrder.setOrderSn(prizeId);
                       mallOrder.setPayTime(LocalDateTime.now());
                       mallOrder.setAddTime(LocalDateTime.now());
                       mallOrder.setUpdateTime(LocalDateTime.now());
                       mallOrder.setPubSharePreFee(taskPrize);
                       mallOrder.setActualPrice(new BigDecimal(taskPrizeCoin));
                       mallOrder.setOrderStatus((short)108);
                       orderService.add(mallOrder);
                       //创建任务记录
                       WangzhuanUserTask wangzhuanUserTask = new WangzhuanUserTask();
                       if(wangzhuanTaskService.selectTaskList().size()>0){
                           wangzhuanUserTask.setTaskId(wangzhuanTaskService.selectTaskList().get(0).getId());
                       }
                       wangzhuanUserTask.setId(GraphaiIdGenerator.nextId("wangzhaun"));
                       wangzhuanUserTask.setUserId(user.getId());
                       wangzhuanUserTask.setRemarks(name);
                       wangzhuanUserTask.setDeleted(false);
                       wangzhuanUserTask.setTstatus("UST104");
                       wangzhuanUserTask.setRewardType("JL20002");
                       wangzhuanUserTask.setReward(new BigDecimal(taskPrize));
                       wangzhuanUserTask.setCreateTime(LocalDateTime.now());
                       wangzhuanUserTask.setUpdateTime(LocalDateTime.now());
                       wangzhuanUserTask.setHuid(AccountStatus.BID_33);
                       wangzhuanUserTaskService.insertWangzhuanUserTask(wangzhuanUserTask);
                       if(null != mallOrder.getId() ){
                           paramsMap.put("orderId",mallOrder.getId());
                       }
                       if(new BigDecimal(taskPrize).compareTo(new BigDecimal("0")) == 1  ) {

                           /**用户收益*/
                           BigDecimal residuer = new BigDecimal(taskPrize);

                            //用户以及直推人充值
                            if( StringUtils.isNotBlank(user.getDirectLeader())){
                                MallUser recomUser = mallUserService.findById(user.getDirectLeader());
                               AccountUtils.accountRechargeSF(residuer,user,recomUser,paramsMap,paramsMapMaker);

                            }else{
                                AccountUtils.accountRechargeSF(residuer,user,null,paramsMap,paramsMapMaker);
                            }


                           //平台充值
                           AccountUtils.accountChangeByExchange("00", new BigDecimal(dealPrize), CommonConstant.TASK_REWARD_CASH, AccountStatus.BID_33, paramsMap);
                       }
                   } catch (Exception e) {
                       e.printStackTrace();
                   }

               }
           }

       }

       return "success";
    }


    /**
     * 闲娱游戏
     * 返回跳转url，并绑定用户
     * @param userId
     * @param data
     * @return
     */
    @PostMapping("/jumpXY")
    public Object jumpXY(@LoginUser String userId, @RequestBody String data, HttpServletRequest request) throws UnsupportedEncodingException {
        // 说明：sign=MD5( base64( "device=864804031618796&id=12&phone=2&uid=12322&" ) + "c37e5e4685bee3564f" )

//        if(userId == null){
//            return ResponseUtil.unlogin();
//        }

        String id ="";
        String appKey ="";
        String phone ="";
        String mobileTye = request.getHeader("User-Agent");


//        if (mobileTye.contains("iPhone")) {
//            phone ="2";
//            id ="862";//ios 渠道mid
//            appKey = "dbaf161c13e9263615";//ios
//        }else if (mobileTye.contains("Android")){
//            phone ="1";
//            id ="861";//安卓 渠道mid
//            appKey = "9ccf927085787ae5cb";//安卓
//        }
        String device_code = JacksonUtil.parseString(data,"device_code");//用户手机标识码
        String resource_id = JacksonUtil.parseString(data,"phone");//用户手机做用户标识
//        String resource_id = user.getMobile();
        String oaid = JacksonUtil.parseString(data,"oaid");//oaid(仅安卓，ios 不需此参数)
        String ptype = JacksonUtil.parseString(data,"ptype");
        String type = JacksonUtil.parseString(data,"type");
        String sign ="";
//        if(!StringUtil.isEmpty(device_code)){
//            sign =  "device="+device_code+"&id="+id+"&phone="+phone+"&uid="+resource_id+"&";//签名
//        }else {
//            sign =  "device="+oaid+"&id="+id+"&phone="+phone+"&uid="+resource_id+"&";//签名
//        }

        String xyIappid= "862";//生产 苹果appid
        String xyIappSecret  ="dbaf161c13e9263615";//生产 苹果appSecret
        String xyCsIappid = "889";//ios-测试
        String xyCsIappSecret = "2af7d84800989e618a";//ios-测试

        String xyAappid= "861";//生产 安卓appid
        String xyAappSecret  ="9ccf927085787ae5cb";// 生产 安卓appSecret
        String xyCsAappid = "888";// android-测试
        String xyCsAappSecret = "c2d7d16c82e6059191";// android-测试

        String sign1 = "";
        //安卓
        if("2".equals(ptype)){
            phone ="1";
            if("1".equals(type)){
                // 生产
                if(!StringUtil.isEmpty(device_code)){
                    //签名
                    sign =  "device="+device_code+"&id="+xyAappid+"&phone="+phone+"&uid="+resource_id+"&";
                }else {
                    //签名
                    sign =  "device="+oaid+"&id="+xyAappid+"&phone="+phone+"&uid="+resource_id+"&";
                }
                // BASE64加密
                Base64.Encoder encoder = Base64.getEncoder();
                byte[] encoder1 = encoder.encode(sign.getBytes());
                sign1 = DigestUtils.md5Hex(new String(encoder1)+xyAappSecret);
            }else if("2".equals(type)){
                //测试
                if(!StringUtil.isEmpty(device_code)){
                    //签名
                    sign =  "device="+device_code+"&id="+xyCsAappid+"&phone="+phone+"&uid="+resource_id+"&";
                }else {
                    //签名
                    sign =  "device="+oaid+"&id="+xyCsAappid+"&phone="+phone+"&uid="+resource_id+"&";
                }
                // BASE64加密
                Base64.Encoder encoder = Base64.getEncoder();
                byte[] encoder1 = encoder.encode(sign.getBytes());
                sign1 = DigestUtils.md5Hex(new String(encoder1)+xyCsAappSecret);
            }

        }
        //ios
        else if("1".equals(ptype)){
            phone ="2";
            if("1".equals(type)){
                // 生产
                if(!StringUtil.isEmpty(device_code)){
                    //签名
                    sign =  "device="+device_code+"&id="+xyIappid+"&phone="+phone+"&uid="+resource_id+"&";
                }else {
                    //签名
                    sign =  "device="+oaid+"&id="+xyIappid+"&phone="+phone+"&uid="+resource_id+"&";
                }
                // BASE64加密
                Base64.Encoder encoder = Base64.getEncoder();
                byte[] encoder1 = encoder.encode(sign.getBytes());
                sign1 = DigestUtils.md5Hex(new String(encoder1)+xyIappSecret);
            }else if("2".equals(type)){
                //测试
                if(!StringUtil.isEmpty(device_code)){
                    //签名
                    sign =  "device="+device_code+"&id="+xyCsIappid+"&phone="+phone+"&uid="+resource_id+"&";
                }else {
                    //签名
                    sign =  "device="+oaid+"&id="+xyCsIappid+"&phone="+phone+"&uid="+resource_id+"&";
                }
                // BASE64加密
                Base64.Encoder encoder = Base64.getEncoder();
                byte[] encoder1 = encoder.encode(sign.getBytes());
                sign1 = DigestUtils.md5Hex(new String(encoder1)+xyCsIappSecret);
            }

        }



        String reqUrl ="";
        //安卓
        if("2".equals(ptype)){
            if("1".equals(type)){
                if(!StringUtil.isEmpty(device_code)){
                    reqUrl = CommonConstant.xyUrl+"id="+xyAappid+"&uid="+resource_id+"&device="+device_code+"&phone="+phone+"&sign="+sign1;
                }
                if(!StringUtil.isEmpty(oaid)){
                    reqUrl = CommonConstant.xyUrl+"id="+xyAappid+"&uid="+resource_id+"&device="+oaid+"&phone="+phone+"&sign="+sign1;
                }
            }else if("2".equals(type)){
                if(!StringUtil.isEmpty(device_code)){
                    reqUrl = CommonConstant.xyUrl+"id="+xyCsAappid+"&uid="+resource_id+"&device="+device_code+"&phone="+phone+"&sign="+sign1;
                }
                if(!StringUtil.isEmpty(oaid)){
                    reqUrl = CommonConstant.xyUrl+"id="+xyCsAappid+"&uid="+resource_id+"&device="+oaid+"&phone="+phone+"&sign="+sign1;
                }
            }
        }
        //ios
        else if("1".equals(ptype)){
            if("1".equals(type)){
                if(!StringUtil.isEmpty(device_code)){
                    reqUrl = CommonConstant.xyUrl+"id="+xyIappid+"&uid="+resource_id+"&device="+device_code+"&phone="+phone+"&sign="+sign1;
                }
                if(!StringUtil.isEmpty(oaid)){
                    reqUrl = CommonConstant.xyUrl+"id="+xyIappid+"&uid="+resource_id+"&device="+oaid+"&phone="+phone+"&sign="+sign1;
                }
            }else if("2".equals(type)){
                if(!StringUtil.isEmpty(device_code)){
                    reqUrl = CommonConstant.xyUrl+"id="+xyCsIappid+"&uid="+resource_id+"&device="+device_code+"&phone="+phone+"&sign="+sign1;
                }
                if(!StringUtil.isEmpty(oaid)){
                    reqUrl = CommonConstant.xyUrl+"id="+xyCsIappid+"&uid="+resource_id+"&device="+oaid+"&phone="+phone+"&sign="+sign1;
                }
            }
        }


        Map<String,String> map  = new HashMap<>();
        map.put("url",URLEncoder.encode( reqUrl, "UTF-8" ));

        return ResponseUtil.ok(map);

    }

    @RequestMapping(value = "/xyOrder" ,produces = "application/json;charset=UTF-8;")
    public  Object getXyOrder( @RequestParam Integer mediaId, @RequestParam  Double money,
                               @RequestParam Integer time,    @RequestParam Integer taskType,
                               @RequestParam Integer taskId,  @RequestParam(defaultValue = "") String device,
                               @RequestParam String userId,  @RequestParam Integer remainIdx,
                               @RequestParam(defaultValue = "") String title,  @RequestParam(defaultValue = "") String sign,
                               @RequestParam Integer mediaProfit)  {
        try {
        logger.info("闲娱订单回调参数----"+"device="+device+"--mediaId="+mediaId+"---mediaProfit="+mediaProfit+"---money="+money+"----remainIdx==="+remainIdx+"----taskId="+taskId+"--taskType="+taskType+"---time="+time+"---title="+title+"--userId="+userId);
        List<MallUser> user =  mallUserService.queryByMobile(userId);
        logger.info("任务用户-----"+user.get(0).toString());
        //       md5(prize_info+mid+time+resource_id+token)
            //用户奖励
            double taskPrize = money;
            /** money 如果是整数，闲娱只传整数位不传小数所以需要截取整数位加密，不能用小数加密*/
            String taskPrize1 = taskPrize+"";
            String[] split = taskPrize1.split("\\.");
//            double taskPrizeCoin = mediaProfit/100.00;//总奖励金额 (转换 元)
//             总奖励金额 (转换 元)
             BigDecimal taskPrizeCoin = BigDecimal.valueOf(Long.valueOf(mediaProfit)).divide(new BigDecimal(100));
            //平台金额
             BigDecimal platAmount = taskPrizeCoin.subtract(new BigDecimal(money));
            String appKey ="";

            if (mediaId == 862 ) {
                //ios
                appKey = "dbaf161c13e9263615";
            }else if (mediaId == 861){
                //安卓
                appKey = "9ccf927085787ae5cb";
            }else if (mediaId == 888){
                //安卓
                appKey = "c2d7d16c82e6059191";
            }else if (mediaId == 889){
                //IOS
                appKey = "2af7d84800989e618a";
            }

         String name ="闲娱试玩任务："+title;
            if(user.size()>0 && null != user.get(0)){
                List<FcAccountRechargeBill> list = fcAccountRechargeBillService.selectRechargeBillByOrderSn1(time+"",user.get(0).getId());
                logger.info("任务用户记录数量-------"+list.size());
                String sign1 = "";
                if(split[1].equals("0")){
                    sign1 = "device"+device+"mediaId"+mediaId+"mediaProfit"+mediaProfit+"money"+split[0]+"remainIdx"+remainIdx+"taskId"+taskId+"taskType"+taskType+"time"+time+"title"+title+"userId"+userId;
                }else {
                    sign1 = "device"+device+"mediaId"+mediaId+"mediaProfit"+mediaProfit+"money"+money+"remainIdx"+remainIdx+"taskId"+taskId+"taskType"+taskType+"time"+time+"title"+title+"userId"+userId;
                }
                logger.info("签名参数-------"+sign1);
                String sign2 = DigestUtils.md5Hex(appKey+sign1);
                logger.info("md5签名后参数-------"+sign2);
                // deviceABF6FF95-42F9-4F24-87C6-8E3F3B6A2422mediaId5mediaProfit199money999remainIdx1taskId666taskType0time1555308057title测试发放奖励userId1111
                if (list.size() == 0 && sign2.equals(sign)) {
                    Map paramsMap = new HashMap();
                    //直接分润
                    paramsMap.put("dType", AccountStatus.DISTRIBUTION_TYPE_00);
                    paramsMap.put("message", name);
                    paramsMap.put("orderSn", time+"");
                    paramsMap.put("capitalTrend", AccountStatus.CAPITAL_TREND_01);
                    paramsMap.put("changeType", AccountStatus.CHANGE_TYPE_01);
                    paramsMap.put("changeAccountType", AccountStatus.CHANGE_ACCOUNT_TYPE_2);
                    paramsMap.put("tradeType", AccountStatus.TRADE_TYPE_10);
                    paramsMap.put("bid",AccountStatus.BID_43);

                    Map<String,Object> paramsMapMaker = new HashMap();
                    //直接分润
                    paramsMapMaker.put("dType",AccountStatus.DISTRIBUTION_TYPE_00);
                    paramsMapMaker.put("message","粉丝完成:( "+name+" )收益");
                    paramsMapMaker.put("orderSn",time+"");
                    paramsMapMaker.put("capitalTrend",AccountStatus.CAPITAL_TREND_01);
                    paramsMapMaker.put("changeType",AccountStatus.CHANGE_TYPE_01);
                    paramsMapMaker.put("changeAccountType",AccountStatus.CHANGE_ACCOUNT_TYPE_2);
                    paramsMapMaker.put("tradeType",AccountStatus.TRADE_TYPE_34);
                    paramsMapMaker.put("bid",AccountStatus.BID_43);

                        //创建订单
                        MallOrder mallOrder = new MallOrder();
                        mallOrder.setId(GraphaiIdGenerator.nextId("xianyu"));
                        mallOrder.setUserId(user.get(0).getId());
                        mallOrder.setMessage(name);
                        //用户三方任务时间戳
                        mallOrder.setOrderSn(time+"");
                        mallOrder.setHuid(AccountStatus.BID_43);
                        mallOrder.setBid(AccountStatus.BID_43);
                        mallOrder.setAddTime(LocalDateTime.now());
                        mallOrder.setUpdateTime(LocalDateTime.now());
                        mallOrder.setPubSharePreFee(taskPrizeCoin+"");
                        mallOrder.setActualPrice(taskPrizeCoin);
                        mallOrder.setOrderStatus((short)108);
                        orderService.add(mallOrder);
                        //创建任务记录
                        WangzhuanUserTask wangzhuanUserTask = new WangzhuanUserTask();
                        if(wangzhuanTaskService.selectTaskList().size()>0){
                            wangzhuanUserTask.setTaskId(wangzhuanTaskService.selectTaskList().get(0).getId());
                        }
                        wangzhuanUserTask.setId(GraphaiIdGenerator.nextId("wangzhaun"));
                        wangzhuanUserTask.setUserId(user.get(0).getId());
                        wangzhuanUserTask.setRemarks(name);
                        wangzhuanUserTask.setDeleted(false);
                        wangzhuanUserTask.setTstatus("UST104");
                        wangzhuanUserTask.setRewardType("JL20002");
                        wangzhuanUserTask.setReward(new BigDecimal(taskPrize));
                        wangzhuanUserTask.setCreateTime(LocalDateTime.now());
                        wangzhuanUserTask.setUpdateTime(LocalDateTime.now());
                        wangzhuanUserTask.setHuid(AccountStatus.BID_43);
                        wangzhuanUserTaskService.insertWangzhuanUserTask(wangzhuanUserTask);
                        if(null != mallOrder.getId() ){
                            paramsMap.put("orderId",mallOrder.getId());
                        }
                        if(new BigDecimal(taskPrize).compareTo(new BigDecimal("0")) == 1  ) {

                            /**
                             * 用户充值 （如三方游戏）
                             *
                             * @param money 用户充值金额
                             * @param rule 规则
                             * @param user 用户
                             * @param recomUser 用户直推人
                             * @param paramsMap 用户充值参数
                             * @param paramsMapMaker 用户直推人充值参数
                             */
                            //用户以及直推人充值
                            if( StringUtils.isNotBlank(user.get(0).getDirectLeader())){
                                MallUser recomUser = mallUserService.findById(user.get(0).getDirectLeader());
                               AccountUtils.accountRechargeSF(new BigDecimal(taskPrize),user.get(0),recomUser,paramsMap,paramsMapMaker);
                            }else{
                                AccountUtils.accountRechargeSF(new BigDecimal(taskPrize),user.get(0),null,paramsMap,paramsMapMaker);
                            }
                            //平台充值
                            AccountUtils.accountChangeByExchange("00", platAmount, CommonConstant.TASK_REWARD_CASH, AccountStatus.BID_43, paramsMap);
                        }


                }
             }
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        return ResponseUtil.ok();
    }


}
