package com.graphai.mall.wx.web.bmdesign;

import java.awt.Color;
import java.awt.Font;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.httprequest.HttpUtil;
import com.graphai.commons.util.json.JacksonUtil;
import com.graphai.commons.util.obj.ObjectUtils;
import com.graphai.framework.storage.StorageService;
import com.graphai.mall.admin.domain.MallRedQrcode;
import com.graphai.mall.admin.service.IMallRedQrcodeService;
import com.graphai.mall.db.dao.MallOrderMapper;
import com.graphai.mall.db.domain.BmdesignTeam;
import com.graphai.mall.db.domain.BmdesignUserLevel;
import com.graphai.mall.db.domain.MallCategory;
import com.graphai.mall.db.domain.MallGoods;
import com.graphai.mall.db.domain.MallInvitationCode;
import com.graphai.mall.db.domain.MallOrder;
import com.graphai.mall.db.domain.MallOrderExample;
import com.graphai.mall.db.domain.MallStorage;
import com.graphai.mall.db.domain.MallUser;
import com.graphai.mall.db.service.MallInvitationCodeService;
import com.graphai.mall.db.service.bmdesign.BmdesignTeamService;
import com.graphai.mall.db.service.bmdesign.BmdesignUserLevelService;
import com.graphai.mall.db.service.category.MallCategoryService;
import com.graphai.mall.db.service.goods.MallGoodsService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.util.RandomUtils;
import com.graphai.mall.db.util.account.bobbi.AccountCKUtils;
import com.graphai.mall.db.util.account.AccountUtils;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.mall.wx.service.UserTokenManager;

import cn.hutool.core.img.ImgUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.extra.qrcode.QrCodeUtil;
import cn.hutool.extra.qrcode.QrConfig;
import cn.hutool.json.JSONUtil;

/**
 * 用户等级/段位
 */
@RestController
@RequestMapping("/wx/bmdesignUserLevel")
@Validated
public class WxBmdesignUserLevelController {
    private final Log logger = LogFactory.getLog(WxBmdesignUserLevelController.class);


    @Autowired
    private BmdesignUserLevelService bmdesignUserLevelService;
    @Autowired
    private MallUserService mallUserService;
    @Autowired
    private BmdesignTeamService bmdesignTeamService;
    @Autowired
    private MallCategoryService categoryService;
    @Autowired
    private MallGoodsService mallGoodsService;
    @Autowired
    private MallInvitationCodeService mallInvitationCodeService;
    @Autowired
    private IMallRedQrcodeService serviceImpl;
    @Value("${weiRed.qrcodeUrl}")
    private String requestUrl;
    @Autowired
    private StorageService storageService;
    @Value("${weiRed.wlsqUrl}")
    private String wlsqUrl;

    @Autowired
    private MallOrderMapper mallOrderMapper;


    /**
     * 会员主页数据
     */
    @GetMapping("/homePage")
    public Object homePage(@RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit) {
        BmdesignUserLevel bmdesignUserLevel = new BmdesignUserLevel();
        bmdesignUserLevel.setType("level");
        List<BmdesignUserLevel> levelList = bmdesignUserLevelService.selectBmdesignUserLevelList(bmdesignUserLevel, null, null, "level_num", "asc");
        List<MallCategory> levelPageList = categoryService.queryL1("levelPage");
        List<MallGoods> goodList = mallGoodsService.queryByMember(page, limit);

        Map<String, Object> data = new HashMap<>();
        data.put("levelList", levelList); // 会员等级list
        data.put("levelPageList", levelPageList); // 类目list
        data.put("goodList", goodList); // 商品list
        return ResponseUtil.ok(data);
    }

    /**
     * 查看等级/段位list
     */
    @GetMapping("/list")
    public Object list(@RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit) {
        BmdesignUserLevel bmdesignUserLevel = new BmdesignUserLevel();
        bmdesignUserLevel.setType("level");
        List<BmdesignUserLevel> levelList = bmdesignUserLevelService.selectBmdesignUserLevelList(bmdesignUserLevel, null, null, "level_num", "asc");
        BmdesignUserLevel level1 = new BmdesignUserLevel();
        level1.setId("10");
        level1.setLevelName("达人");
        level1.setLevelCode("010");
        BmdesignUserLevel level2 = new BmdesignUserLevel();
        level2.setId("11");
        level2.setLevelName("创客");
        level2.setLevelCode("011");
        levelList.add(level1);
        levelList.add(level2);
        Map<String, Object> data = new HashMap<>();
        data.put("levelList", levelList); // 会员等级list
        return ResponseUtil.ok(data);
    }


    /**
     * 查看等级/段位详情
     */
    @GetMapping("/find/{id}")
    public Object find(@PathVariable("id") String id) {
        BmdesignUserLevel bmdesignUserLevel = bmdesignUserLevelService.selectBmdesignUserLevelById(id);
        return ResponseUtil.ok(bmdesignUserLevel);
    }


    /**
     * 用户购买等级 id:等级id主键
     */
    @PostMapping("/buy/{id}")
    public Object buy(@LoginUser String userId, @PathVariable("id") String id) throws Exception {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        if ("0".equals(id)) {
            return ResponseUtil.badArgumentValue("请购买有效的等级！");
        }
        BmdesignUserLevel bmdesignUserLevel = bmdesignUserLevelService.selectBmdesignUserLevelById(id);
        if (null == bmdesignUserLevel) {
            return ResponseUtil.badArgumentValue("不存在该等级！");
        }
        Integer levelNum = bmdesignUserLevel.getLevelNum(); // 购买等级的优先级
        MallUser mallUser = mallUserService.findById(userId); // 查询当前用户的信息
        String levelId = mallUser.getLevelId(); // 当前用户的等级id
        BmdesignUserLevel userLevel = null;
        if (StringUtils.isNotEmpty(levelId)) {
            userLevel = bmdesignUserLevelService.selectBmdesignUserLevelById(levelId);
            if (ObjectUtils.isNotNull(userLevel)) {
                // 若 购买等级的优先级 小于或等于 当前用户等级的优先级，则提示错误
                if (levelNum <= userLevel.getLevelNum()) {
                    return ResponseUtil.badArgumentValue("请购买有效的等级！");
                }
            }
        }

        // todo 用户购买，扣除用户的金额
        BigDecimal price = bmdesignUserLevel.getPrice();
        /** 用户账户扣款 */



        // 若支付成功，则修改用户等级
        MallUser user = new MallUser();
        user.setId(userId);
        user.setLevelId(id);
        mallUserService.updateById(user);


        // todo 计算分销(目前只有等级)
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("orderId", null); // 订单ID
        paramsMap.put("orderSn", null); // 订单编号
        // 用户上级分润
        // AccountUtils.userProfitRecharge(userId, price, CommonConstant.TASK_REWARD_CASH,
        // AccountStatus.BID_12, paramsMap);



        // todo 重新配置 个人团队信息
        // 查询出等级小于当前用户等级的 下级用户信息(当前用户 发展的下级用户信息)
        List<MallUser> subordinateList = mallUserService.getMallUserByCondition(userId, levelNum, true); // todo 方法待完善
        // 判断是否存在下级
        if (subordinateList.size() > 0) {
            // 首先将属于我 未删除的直推数据 删除掉，主要是 怕后面的查询重复添加
            BmdesignTeam team = new BmdesignTeam();
            team.setTeamLeaderId(userId);
            // team.setFirstLeaderId(userId);
            team.setDeleted(BmdesignTeam.IS_DELETED);
            bmdesignTeamService.updateBmdesignTeamByLeaderId(team);
            // 添加新数据
            for (int i = 0; i < subordinateList.size(); i++) {
                BmdesignTeam bmdesignTeam = new BmdesignTeam();
                bmdesignTeam.setTeamLeaderId(userId);
                bmdesignTeam.setFirstLeaderId(subordinateList.get(i).getFirstLeader());
                bmdesignTeam.setUserId(subordinateList.get(i).getId());
                bmdesignTeam.setLevelId(subordinateList.get(i).getLevelId());
                // bmdesignTeam.setTeamLeaderLevelId(id);
                // bmdesignTeam.setOldLevelName();
                bmdesignTeam.setNickname(subordinateList.get(i).getNickname());
                bmdesignTeam.setAvatar(subordinateList.get(i).getAvatar());

                bmdesignTeamService.insertBmdesignTeam(bmdesignTeam);
            }
        }


        /** --------------------- */
        // 查询出等级小于等于当前用户等级的 上级用户信息
        List<BmdesignTeam> leaderList = bmdesignTeamService.selectBmdesignTeamListByCondition(userId, levelNum, true);


        List<String> leaderIdList = new ArrayList(); // 上级ids
        List<String> subordinateIdList = new ArrayList(); // 下级ids
        if (leaderList.size() > 0) {
            for (int i = 0; i < leaderList.size(); i++) {
                leaderIdList.add(leaderList.get(i).getTeamLeaderId());
            }

            if (subordinateList.size() > 0) {
                for (int i = 0; i < subordinateList.size(); i++) {
                    subordinateIdList.add(subordinateList.get(i).getId());
                }
            }
            // 将现在有关当前用户 的不符合条件的个人团队信息 改为删除状态
            int i = bmdesignTeamService.updateBmdesignTeamByCondition(leaderIdList, subordinateIdList, userId);
            logger.info("-----------修改成功我的BmdesignTeam数据条数： " + i);
        }
        /** --------------------- */


        // 修改团队长为当前用户的 团队长等级信息
        BmdesignTeam levelTeam = new BmdesignTeam();
        levelTeam.setTeamLeaderId(userId);
        levelTeam.setTeamLeaderLevelId(id);
        // 修改当前用户的旧等级信息
        if (null != userLevel) {
            levelTeam.setOldLevelName(userLevel.getLevelName());
        }
        bmdesignTeamService.updateBmdesignTeamByLeaderId(levelTeam);

        // 生成对应数量的激活码
        for (int i = 0; i < bmdesignUserLevel.getActivationCodeNum(); i++) {
            MallInvitationCode mallInvitationCode = new MallInvitationCode();
            mallInvitationCode.setUserId(userId);
            mallInvitationCodeService.insertMallInvitationCode(mallInvitationCode);
        }


        return ResponseUtil.ok();
    }



    /** 返佣分润测试 **/
    @PostMapping("/test")
    public Object test(@RequestBody String params) {

        Map<String, String> map = JacksonUtil.toMap(params);
        System.out.println(map);
        return AccountUtils.preCharge(map);
    }


    /** 手动创客分佣 **/
    @GetMapping("/testMaker")
    public Object testMaker(@RequestParam String mobile) {

        List<MallUser> mallUser = mallUserService.queryByMobile(mobile);

        MallOrderExample example = new MallOrderExample();
        example.createCriteria().andUserIdEqualTo(mallUser.get(0).getId()).andBidEqualTo("38").andOrderStatusEqualTo((short) 201);

        List<MallOrder> mallOrders = mallOrderMapper.selectByExample(example);
        if (mallOrders.size() > 0) {
            // 创客分润
            AccountCKUtils.preChargeCK(mallOrders.get(0), mallUser.get(0));
        }

        return null;
    }

    /**
     * 后台生成创客信息
     */
    @GetMapping("/createMaker")
    public Object createMaker(@RequestParam String mobile) throws IOException {
        List<MallUser> mallUser2 = mallUserService.queryByMobile(mobile);
        String qrCodeUrl = "";
        {
            // 生成注册邀请码
            String qrcode = RandomUtils.genRandomNum(6);
            logger.info("用户购买创客身份支付回调-------qrcode-----------" + qrcode);
            // QueryWrapper queryWrapper = ;
            // queryWrapper.eq("mall_red_qrcode.code",qrcode);
            MallRedQrcode one = serviceImpl.getOne(new QueryWrapper<MallRedQrcode>().eq("code", qrcode));
            logger.info("用户购买创客身份支付回调-------one-----------" + one);
            String number = "";
            if (null != one) {
                number = one.getCode();
            } else {
                MallRedQrcode mallRedQrcode = new MallRedQrcode();
                mallRedQrcode.setCode(qrcode);
                mallRedQrcode.setUsersId(mallUser2.get(0).getId());
                mallRedQrcode.setName("系统自动生成");
                mallRedQrcode.setTotalNumber(900000000); // 总次数（这个码总共能扫多少次） 功能做好后搞成前端变量转进来，不能写死
                mallRedQrcode.setSurplusNumber(900000000); // 剩余次数（这个码剩余能扫多少次） 功能做好后搞成前端变量转进来，不能写死
                mallRedQrcode.setStatus("0"); // 红包状态（0，有效，1 无效，2,禁用, 3 启用）
                mallRedQrcode.setUserNumber(5); // 个人领取记录 功能做好后搞成前端变量转进来，不能写死
                mallRedQrcode.setOneNum("0.01,0.05"); // 第一次区间 功能做好后搞成前端变量转进来，不能写死
                mallRedQrcode.setTwoNum("0.01,0.05");
                mallRedQrcode.setThreeNum("0.01,0.05");
                mallRedQrcode.setFourNum("0.01,0.05");
                mallRedQrcode.setFiveNum("0.01,0.05");
                mallRedQrcode.setSixNum("0.01,0.05");
                mallRedQrcode.setSevenNum("0.2"); // 创客所得比例 功能做好后搞成前端变量转进来，不能写死
                mallRedQrcode.setCreateTime(LocalDateTime.now());
                mallRedQrcode.setIsPrinted("0"); // 是否已印刷 默认0-否 1-是
                mallRedQrcode.setIsBind("0"); // 是否已绑定 默认0-否 1-是
                mallRedQrcode.setType("percent"); // 默认 0-创客使用
                mallRedQrcode.setEndTime(LocalDateTime.of(2100, 01, 01, 23, 59, 59));

                // mallRedQrcode.setRemark("商"); // 二维码备注
                serviceImpl.save(mallRedQrcode);
                // QueryWrapper queryWrapper1 = new QueryWrapper();
                // queryWrapper1.eq("mall_red_qrcode.code",qrcode);
                MallRedQrcode one2 = serviceImpl.getOne(new QueryWrapper<MallRedQrcode>().eq("code", qrcode));
                logger.info("用户购买创客身份支付回调-------one2-----------" + one2);
                number = one2.getCode();
            }
            QrConfig qrConfig = new QrConfig();
            qrConfig.setWidth(640); // 设置二维码宽度
            qrConfig.setHeight(640);// 设置二维码高度
            qrConfig.setMargin(1);// 设置边距
            File generate = QrCodeUtil.generate(requestUrl + "/wx/gamejackpot/qrCode?makerCode=" + qrcode, qrConfig, new File("C:\\Users\\Administrator.SC-202011302240\\Desktop\\红包码\\" + qrcode + ".png"));// 生成二维码存放地址
            /**
             * @param srcImageFile 源图像文件
             * @param destImageFile 目标图像文件
             * @param pressImg 水印图片
             * @param x 修正值。 默认在中间，偏移量相对于中间偏移
             * @param y 修正值。 默认在中间，偏移量相对于中间偏移
             * @param alpha 透明度：alpha 必须是范围 [0.0, 1.0] 之内（包含边界值）的一个浮点数字
             */
            ImgUtil.pressImage(
                            // FileUtil.file("/home/gxkj/redCode/red.png"),//源图像文件
                            FileUtil.file("C:\\Users\\Administrator.SC-202011302240\\Desktop\\红包码\\red.png"), // 源图像文件
                            FileUtil.file("C:\\Users\\Administrator.SC-202011302240\\Desktop\\红包码\\red1\\" + number + "." + qrcode + ".png"), // 目标图像文件，保存生成图片地址
                            ImgUtil.read(generate), -8, // x坐标修正值。 默认在中间，偏移量相对于中间偏移
                            30, // y坐标修正值。 默认在中间，偏移量相对于中间偏移
                            1f);

            String saveUrl = "C:\\Users\\Administrator.SC-202011302240\\Desktop\\红包码\\red2\\" + number + "." + qrcode + ".png";
            ImgUtil.pressText(FileUtil.file("C:\\Users\\Administrator.SC-202011302240\\Desktop\\红包码\\red1\\" + number + "." + qrcode + ".png"), /// 源图像文件，就是需要添加文字的源文件
                            FileUtil.file(saveUrl), // 目标图像文件，就是生成添加文字存放地址
                            qrcode, Color.BLACK, // 文字
                            new Font("黑体", Font.BOLD, 38), // 字体
                            -5, // x坐标修正值。 默认在中间，偏移量相对于中间偏移
                            383, // y坐标修正值。 默认在中间，偏移量相对于中间偏移
                            1f);// 透明度：alpha 必须是范围 [0.0, 1.0] 之内（包含边界值）的一个浮点数字


            File file = new File(saveUrl);
            InputStream inputStream = new FileInputStream(file);
            MultipartFile multipartFile = new MockMultipartFile(file.getName(), inputStream);

            String originalFilename = number + "." + qrcode + ".png";
            MallStorage MallStorage = storageService.store(multipartFile.getInputStream(), multipartFile.getSize(), multipartFile.getContentType(), originalFilename);
            qrCodeUrl = MallStorage.getUrl();

        }


        /** 购买成功给用户赋予创客权限 **/
        MallUser updateUser = new MallUser();
        updateUser.setId(mallUser2.get(0).getId());
        updateUser.setIsMaker("1");
        updateUser.setIsActivate((byte) 1);
        updateUser.setSource("手动生成创客");
        if ("0".equals(mallUser2.get(0).getLevelId())) {
            updateUser.setLevelId("1");
        }
        updateUser.setSessionKey(qrCodeUrl);
        // 修改用户信息，用户已经购购买创客身份
        mallUserService.updateById(updateUser);


        // TODO 同步用户信息给支付系统
        Map map = new HashMap<>();
        String url = wlsqUrl + "/api/wlsh/addUserInfo";
        // name str 名称
        map.put("name", updateUser.getNickname());
        // phone str手机号
        map.put("phone", updateUser.getMobile());
        // address str 地区
        map.put("address", updateUser.getProvince() + updateUser.getCity() + updateUser.getCounty());
        // userType str 用户类型 7
        if ("1".equals(updateUser.getIsMaker())) {
            map.put("userType", "7");
        }
        // 推荐人
        if (org.apache.commons.lang.StringUtils.isNotBlank(updateUser.getDirectLeader())) {
            MallUser mallUserD = mallUserService.findById(updateUser.getDirectLeader());
            // referrerPhone
            if (null != mallUserD) {
                map.put("referrerPhone", mallUserD.getMobile());
            }
        }

        try {
            String s = HttpUtil.sendPostJson(url, JSONUtil.toJsonStr(map));
            logger.info("用户购买创客身份同步支付系统------------------" + s);
        } catch (Exception e) {
            e.printStackTrace();
            logger.info("用户购买创客同步用户信息给支付系统失败：" + e.getMessage(), e);
        }

        return ResponseUtil.ok();
    }



    // 测试使用
    public static void main(String[] args) {

        // 生成JWT的tobken---------------

        String token = UserTokenManager.generateToken("5675003269619064832", "", "");
        System.out.println(token);



    }

}
