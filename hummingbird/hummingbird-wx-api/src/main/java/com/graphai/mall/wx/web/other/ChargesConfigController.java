package com.graphai.mall.wx.web.other;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.mall.db.domain.MallChargesConfig;
import com.graphai.mall.db.service.MallChargesConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/wx/chargesconfig")
public class ChargesConfigController {

	@Autowired
	private MallChargesConfigService chargesConfigService;
	
	@GetMapping("list")
	public Object list(HttpServletResponse response, HttpServletRequest request,
			@RequestParam(defaultValue = "HF") String changesType) {
		
		List<MallChargesConfig> chargesConfigList = chargesConfigService.getChargesConfigs(changesType);
		return ResponseUtil.ok(chargesConfigList);
	}
	
	
}
