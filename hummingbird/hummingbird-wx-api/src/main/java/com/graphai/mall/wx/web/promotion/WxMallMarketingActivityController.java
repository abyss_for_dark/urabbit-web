package com.graphai.mall.wx.web.promotion;


import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.json.JacksonUtil;
import com.graphai.mall.db.domain.MallMarketingActivity;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.mall.wx.service.WxMallMarketingActivityService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 998活动服务
 */
@RestController
@RequestMapping("/wx/act")
@Validated
public class WxMallMarketingActivityController {

    @Resource
    private WxMallMarketingActivityService wxMallMarketingActivityService;

    /**
     * @param userId
     * @return
     */
    @RequestMapping("/query")
    public Object query(@LoginUser String userId) {
        MallMarketingActivity mallMarketingActivity = wxMallMarketingActivityService.selectActivity(userId);
        mallMarketingActivity.setPassword("");
        return ResponseUtil.ok(mallMarketingActivity);
    }

    /**
     * @param
     * @return
     */
    @RequestMapping("/queryUser")
    public Object queryUser(@RequestBody String body) {
        String buserId = JacksonUtil.parseString(body, "buserId");
        if (!StringUtils.isEmpty(buserId)) {
            MallMarketingActivity mallMarketingActivity = wxMallMarketingActivityService.selectActivity(buserId);
            mallMarketingActivity.setPassword("");
            return ResponseUtil.ok(mallMarketingActivity);
        } else {
            return ResponseUtil.badArgumentValue();
        }
    }

    /**
     * 编辑记录
     *
     * @param userId
     * @param mallMarketingActivity
     * @return
     */
    @RequestMapping("/edit")
    public Object edit(@LoginUser String userId, @RequestBody MallMarketingActivity mallMarketingActivity) {
        mallMarketingActivity.setId(userId);
        return ResponseUtil.ok(wxMallMarketingActivityService.editActivity(mallMarketingActivity));
    }

    /**
     * 成交记录
     *
     * @param userId
     * @return
     */
    @RequestMapping("/list")
    public Object list(@LoginUser String userId) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        return ResponseUtil.ok(wxMallMarketingActivityService.find998Order(userId));
    }
}
