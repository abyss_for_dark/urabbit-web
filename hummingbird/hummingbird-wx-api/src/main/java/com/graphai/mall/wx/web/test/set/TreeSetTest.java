package com.graphai.mall.wx.web.test.set;

import java.util.Set;
import java.util.TreeSet;

/**
 * Set 测试类
 * @author Qxian
 * @date 2021-02-20
 */
public class TreeSetTest {
    public static void main(String[] args) {

        Set<Person> set = new TreeSet<Person>();

        //产生100以内的随机数
        int num = (int)(Math.random()*100+1);
        for(int i=num;i>0;i--){
            set.add(new Person(""+i,"张三","河南"));
        }
        set.add(new Person("81","李四","河南"));
        set.add(new Person("101","王五","河南"));
        set.add(new Person("888","赵六","河南"));
        set.add(new Person("600","田七","河南"));

        for(Object o : set){
            System.out.println(o);
        }
    }
}