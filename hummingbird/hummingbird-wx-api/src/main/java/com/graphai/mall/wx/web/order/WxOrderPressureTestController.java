package com.graphai.mall.wx.web.order;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;

import com.graphai.commons.util.ResponseUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.validator.internal.util.StringHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.graphai.commons.util.json.JacksonUtil;
import com.graphai.id.GraphaiIdGenerator;
import com.graphai.mall.db.domain.MallCart;
import com.graphai.mall.db.domain.MallGrouponRules;
import com.graphai.mall.db.domain.MallOrder;
import com.graphai.mall.db.domain.MallOrderGoods;
import com.graphai.mall.db.service.MallCartService;
import com.graphai.mall.db.constant.account.AccountStatus;
import com.graphai.mall.db.service.goods.MallGoodsService;
import com.graphai.mall.db.service.groupon.MallGrouponRulesService;
import com.graphai.mall.db.service.groupon.MallGrouponService;
import com.graphai.mall.db.service.order.MallOrderGoodsService;
import com.graphai.mall.db.service.order.MallOrderService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.constant.CommonConstant;
import com.graphai.mall.db.util.OrderUtil;
import com.graphai.mall.db.util.UserUtils;
import com.graphai.mall.db.util.account.bobbi.AccountZYUtils;
import com.graphai.open.mallorder.service.IMallOrderYLService;

@RestController
@RequestMapping("/wx/order")
@Validated
public class WxOrderPressureTestController {

    private final Log logger = LogFactory.getLog(WxOrderPressureTestController.class);

    @Autowired
    private MallUserService mallUserService;
    @Autowired
    private MallOrderGoodsService orderGoodsService;
    @Autowired
    private MallGoodsService goodsService;
    @Autowired
    IMallOrderYLService mallOrderYLService;
    @Autowired
    private MallGrouponRulesService grouponRulesService;
    @Autowired
    private MallGrouponService grouponService;
    @Autowired
    private MallOrderService orderService;

    @Autowired
    private MallCartService cartService;


    /**
     * 提交订单(插入大量测试数据)
     *
     * @param body 订单信息，{ cartId：xxx, addressId: xxx, couponId: xxx, message: xxx, grouponRulesId: xxx,
     *        grouponLinkId: xxx}
     * @return 提交订单操作结果
     */
    @PostMapping("SubmitTest")
    public Object hsCardSubmit(@RequestBody String body, HttpServletRequest request) {


//        try {
//
//
//            List<String> userIds = mallUserService.queryAllUserIds();
//
//            // for (int j = 0; j < 100; j++) {
//
//
//            ExecutorService executorService = Executors.newFixedThreadPool(10);
//
//            for (int i = 0; i < userIds.size(); i++) {
//                String userId = userIds.get(i);
//                executorService.execute(new Runnable() {
//                    @Override
//                    public void run() {
//                        hsCardSubmitTest(userId, body, IpUtils.getIpAddress(request));
//                    }
//                });
//
//            }
//            // }
//
//            System.out.println("1111111111111111");
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        return ResponseUtil.ok();

    }


    /**
     * 提交订单
     * <p>
     * 1. 创建订单表项和订单商品表项; 2. 购物车清空; 3. 优惠券设置已用; 4. 商品货品库存减少; 5. 如果是团购商品，则创建团购活动表项。
     *
     * @param body 订单信息，{ cartId：xxx, addressId: xxx, couponId: xxx, message: xxx, grouponRulesId: xxx,
     *        grouponLinkId: xxx}
     * @return 提交订单操作结果
     * @Param platFrom 数据来自哪个平台（1、大淘客 2、福禄 3、中闵在线 4、蓝色兄弟 5、优惠券商品）
     */
    @Transactional
    public void hsCardSubmitTest(String userId, String body, String clientIp) {
        String deptId = "";// 归属部门ID



        try {



            // 平台ID
            String platFrom = JacksonUtil.parseString(body, "platform");
            // 手机号
            String mobile = JacksonUtil.parseString(body, "mobile");



            if (null == userId) {
                Object result = UserUtils.getTokenUserId(mobile);
                if (!Objects.equals(result, null)) {
                    userId = String.valueOf(result);
                }
            }

            // 福禄商品下单
            // if (Objects.equals(platFrom, OrderUtil.PLATFROM_FULU)) {
            // return insertOrderByThreeV2(userId, body, clientIp, "fuluGoods", mobile, platFrom);
            // }
            // // 中闵在线
            // else if (Objects.equals(platFrom, OrderUtil.PLATFROM_ZM)) {
            // return insertOrderByThreeV2(userId, body, clientIp, "zmGoods", mobile, platFrom);
            // }
            // // 蓝色兄弟
            // else if (Objects.equals(platFrom, OrderUtil.PLATFROM_LSXD)) {
            // return insertOrderByThreeV2(userId, body, clientIp, "lsxdGoods", mobile, platFrom);
            // }
            // // 购买优惠券商品
            // else if (Objects.equals(platFrom, OrderUtil.PLATFROM_OTHER)) {
            // return insertOrderByOtherV2(userId, body, clientIp, "couponGoods", mobile, platFrom);
            // }


            if (Objects.equals(platFrom, OrderUtil.PLATFROM_LSXD)) {
                insertOrderByThreeTest(userId, body, clientIp, "lsxdGoods", mobile, platFrom);
            } else if (Objects.equals(platFrom, "0")) {
                // 购买自营商品
                buyZyGoodsV2(userId, body, clientIp, deptId);
            }



        } catch (Exception e) {
            logger.error("订单提交异常: " + e.getMessage(), e);
            throw e;
        }
        // return ResponseUtil.fail(501, "订单提交有误!");
    }

    @Transactional(rollbackFor = {Exception.class})
    private void insertOrderByThreeTest(String userId, String body, String clientIp, String type, String mobile,
                    String platFrom) {


        // 三方平台通用方
        String goods_name = JacksonUtil.parseString(body, "goods_name");
        String goods_sn = JacksonUtil.parseString(body, "goods_sn");
        String goods_id = JacksonUtil.parseString(body, "goods_id");
        String goods_product_id = JacksonUtil.parseString(body, "goods_product_id"); // 商品产品id
        String product_type = JacksonUtil.parseString(body, "product_type");
        // 销售价
        String goods_price = JacksonUtil.parseString(body, "goods_price");

        // 类目ID
        // String categoryId = JacksonUtil.parseString(body, "categoryId");
        String categoryPic = JacksonUtil.parseString(body, "categoryPic");

        // 手机号 \ qq号
        String charge_account = JacksonUtil.parseString(body, "charge_account");
        String buy_num = JacksonUtil.parseString(body, "buy_num");

        // 蓝色兄弟专用
        // 充值类型 （1手机 2 QQ）
        String account_type = JacksonUtil.parseString(body, "account_type");
        // String extend_parameter = JacksonUtil.parseString(body, "extend_parameter");



        if (StringHelper.isNullOrEmptyString(buy_num)) {
            buy_num = "1";
        }
        // todo 远程调用接口查询 商品产品信息,后面发版去除


        MallOrder order = new MallOrder();
        order.setUserId(userId);
        order.setOrderSn(GraphaiIdGenerator.nextId("OrderSN"));
        order.setOrderStatus(OrderUtil.STATUS_PAY);
        order.setMobile(charge_account);
        order.setMessage("购买权益商品");
        // 面值
        order.setGoodsPrice(new BigDecimal(goods_price));
        order.setFreightPrice(new BigDecimal(0.00));
        order.setCouponPrice(new BigDecimal(0.00));
        order.setIntegralPrice(new BigDecimal(0.00));
        // 订单总费用
        BigDecimal actualPrice = new BigDecimal(goods_price).multiply(new BigDecimal(buy_num));
        order.setActualPrice(actualPrice);
        order.setOrderPrice(actualPrice);
        order.setHuid(type);
        order.setClientIp(clientIp);
        order.setGrouponPrice(new BigDecimal(0.00)); // 团购价格
        order.setBid(AccountStatus.BID_9);

        // 添加订单表
        orderService.add(order);

        for (int i = 1; i <= Integer.parseInt(buy_num); i++) {
            // 添加订单商品表项
            // 订单商品
            MallOrderGoods orderGoods = new MallOrderGoods();
            orderGoods.setOrderId(order.getId());
            orderGoods.setGoodsId(goods_id);
            orderGoods.setGoodsSn(goods_sn);
            orderGoods.setProductId(goods_product_id);
            orderGoods.setSrcGoodId(goods_sn);
            orderGoods.setGoodsName(goods_name);
            orderGoods.setPrice(new BigDecimal(goods_price));
            orderGoods.setNumber(Short.valueOf("1"));
            orderGoods.setAddTime(LocalDateTime.now());
            // 临时用于存储商品类型
            orderGoods.setComment(product_type);
            // 设置类目的图片，当作权益的商品
            orderGoods.setChargeType(account_type);
            orderGoods.setPicUrl(categoryPic);
            orderGoods.setUserId(userId);
            orderGoodsService.add(orderGoods);
        }

        Map<String, Object> data = new HashMap<>();
        data.put("orderId", order.getId());

    }


    /**
     * 购买自营商品
     *
     */
    private void buyZyGoodsV2(String userId, String body, String clientIp, String deptId) {

        logger.info("参数body:" + body);
        String cartId = JacksonUtil.parseString(body, "cartId");


        String message = JacksonUtil.parseString(body, "message");

        // 团购专用
        String grouponRulesId = JacksonUtil.parseString(body, "grouponRulesId");
        String grouponLinkId = JacksonUtil.parseString(body, "grouponLinkId");

        String groupProductId = JacksonUtil.parseString(body, "groupProductId");

        String shareId = JacksonUtil.parseString(body, "shareId"); // 分享人id

        Integer buyNum = JacksonUtil.parseInteger(body, "buyNum");

        // 分销员的主键ID

        // 下单端口H5端默认写Customer，小程序目前是空，后续可以直接写MiniBusiness
        String submitClient = JacksonUtil.parseString(body, "submitClient");

        String sign = JacksonUtil.parseString(body, "sign");
        String name = JacksonUtil.parseString(body, "name"); // 收货人姓名
        String tel = JacksonUtil.parseString(body, "tel"); // 收货人电话
        String province = JacksonUtil.parseString(body, "province"); // 省
        String city = JacksonUtil.parseString(body, "city"); // 市
        String county = JacksonUtil.parseString(body, "county"); // 区
        String addressDetail = JacksonUtil.parseString(body, "addressDetail"); // 详细地址

        BigDecimal price = new BigDecimal(JacksonUtil.parseString(body, "price")); // 金额



        // 如果接口为空的话
        submitClient = StringUtils.isNotBlank(submitClient) ? submitClient : "MiniBusiness";

        // 如果是团购项目,验证活动是否有效
        MallGrouponRules rules = null;
        if (grouponRulesId != null && !"0".equals(grouponRulesId)) {
            rules = grouponRulesService.queryByIdV2(grouponRulesId);

        }

        // 查询团是否满人
        if (grouponLinkId != null) {
            // 查看参团成员
            List<Map<String, Object>> groupList = grouponService.queryGroupById(grouponLinkId);
            if (rules.getDiscountMember() <= groupList.size()) {

            }
        }



        BigDecimal checkedGoodsPrice = new BigDecimal(0.00);
        // 使用优惠券减免的金额
        BigDecimal couponPrice = new BigDecimal(0.00);
        // 根据订单商品总价计算运费，多个商品满88则免运费，否则8元；如果是单个商品则按商品的邮费计算
        BigDecimal freightPrice = new BigDecimal(0.00);
        // 总成本价
        BigDecimal costPrice = BigDecimal.ZERO;

        // 团购优惠
        BigDecimal grouponPrice = new BigDecimal(0.00);
        if (!Objects.equals(grouponRulesId, 0)) {
            if (rules != null) {
                grouponPrice = rules.getDiscount();
            }
        }

        List<MallCart> checkedGoodsList = null;
        // 团购商品不在购物车下单
        if (com.graphai.mall.db.util.StringHelper.isNotNullAndEmpty(groupProductId)) {
            JSONObject obj;
            try {
                obj = goodsService.queryGoodsByProductId(groupProductId);
                freightPrice = new BigDecimal(obj.getString("postage"));
                deptId = obj.getString("deptId");
                Integer stock = obj.getInteger("stock"); // 限购量
                Integer orderStock = orderGoodsService.selectOrderGoodsTotal(userId, obj.getString("gid"));
                if (!Objects.equals(null, stock)) {
                    if (orderStock >= stock) {
                        ResponseUtil.badArgumentValue("此商品已被限购!");

                    }
                }
                logger.info("单价" + obj.getString("price"));
                logger.info("数量" + buyNum);
                logger.info(groupProductId + "归属部门ID=" + deptId);
                logger.info("购买单价" + obj.getString("price"));
                logger.info("购买数量" + buyNum);
                checkedGoodsPrice = (new BigDecimal(obj.getString("price")).subtract(grouponPrice))
                                .multiply(new BigDecimal(buyNum));

                if (obj.getString("factory_price") != null) {
                    costPrice = new BigDecimal(obj.getString("factory_price"));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        if (cartId.equals(0) || "0".equals(cartId)) {
            logger.info("进入userid 查询购物车产品 ：" + userId);
            checkedGoodsList = cartService.queryByUidAndChecked(userId);
        } else {
            logger.info("进入cartId 查询购物车产品 ：" + cartId);
            MallCart cart = cartService.findById(cartId);
            checkedGoodsList = new ArrayList<>(1);
            checkedGoodsList.add(cart);
        }



        for (MallCart checkGoods : checkedGoodsList) {
            // 商品初始价格
            BigDecimal goodsPrice = checkGoods.getPrice();
            BigDecimal finalPrice = goodsPrice;
            // if(shId!=null){
            // MallRebateRecord rebate =
            // fenXiaoService.getMallRebateRecordById(shId);
            // finalPrice = GoodsUtils.getRetailPrice(rebate, goodsPrice);
            // }
            // 订单成交计算金额
            checkedGoodsPrice = checkedGoodsPrice.add(finalPrice.multiply(new BigDecimal(checkGoods.getNumber())));



        }



        // 可以使用的其他钱，例如用户积分
        BigDecimal integralPrice = new BigDecimal(0.00);

        // 订单费用
        BigDecimal orderTotalPrice = checkedGoodsPrice.add(freightPrice == null ? BigDecimal.ZERO : freightPrice)
                        .subtract(couponPrice);
        // 最终支付费用
        BigDecimal actualPrice = orderTotalPrice.subtract(integralPrice);
        BigDecimal finalActualPrice = actualPrice;

        // 分佣总金额
        BigDecimal commissionPrice = checkedGoodsPrice.subtract(costPrice);



        if (price.compareTo(finalActualPrice) != 0) {
            logger.info("金额不对!");
        }

        String orderId = null;
        MallOrder order = null;
        // 订单
        order = new MallOrder();
        order.setUserId(userId);
        // order.setOrderSn(orderService.generateOrderSn(userId));
        order.setOrderSn(GraphaiIdGenerator.nextId("OrderSN"));
        order.setOrderStatus(OrderUtil.STATUS_PAY);
        order.setConsignee(name);
        order.setMobile(tel);
        order.setMessage(message);
        String detailedAddress = province + city + county + " " + addressDetail;
        order.setAddress(detailedAddress);
        order.setGoodsPrice(checkedGoodsPrice);
        order.setFreightPrice(freightPrice);
        order.setCouponPrice(couponPrice);
        order.setIntegralPrice(integralPrice);
        order.setOrderPrice(orderTotalPrice);
        order.setActualPrice(finalActualPrice); // 订单实付金额
        order.setPubSharePreFee(String.valueOf(commissionPrice)); // 分佣总金额

        order.setSource(submitClient);
        order.setProvinceName(province);
        order.setCityName(city);
        order.setAreaName(county);
        order.setAddressDetail(addressDetail);
        order.setClientIp(clientIp);
        if (grouponRulesId != null && !Objects.equals(grouponRulesId, "0")) {
            order.setBid(AccountStatus.BID_36);
        } else {
            order.setBid(AccountStatus.BID_27);
        }

        if (com.graphai.mall.db.util.StringHelper.isNotNullAndEmpty(shareId)) {
            order.setShareId(shareId);
        }

        order.setHuid("zygoods");
        order.setGrouponPrice(grouponPrice); // 团购价
        List<MallOrderGoods> orderGoodsList = new ArrayList<MallOrderGoods>();
        Boolean becomeVip = false;

        order.setDeptId(deptId);// 订单加归属部门id
        // 添加订单表
        orderService.add(order);
        orderId = order.getId();

        String goodsId = "";
        // 添加订单商品表项(购物车)
        // 添加订单商品表项
        for (MallCart cartGoods : checkedGoodsList) {
            goodsId = cartGoods.getGoodsId();

            // 订单商品
            MallOrderGoods orderGoods = new MallOrderGoods();
            orderGoods.setOrderId(order.getId());
            orderGoods.setGoodsId(goodsId);
            orderGoods.setUserId(userId);
            orderGoods.setGoodsSn(cartGoods.getGoodsSn());
            orderGoods.setSrcGoodId(cartGoods.getSrcGoodId());
            orderGoods.setProductId(cartGoods.getProductId());
            orderGoods.setGoodsName(cartGoods.getGoodsName());

            orderGoods.setPicUrl(cartGoods.getPicUrl());
            orderGoods.setPrice(cartGoods.getPrice());
            orderGoods.setNumber(cartGoods.getNumber());
            orderGoods.setSpecifications(cartGoods.getSpecifications());
            orderGoods.setAddTime(LocalDateTime.now());
            orderGoodsService.add(orderGoods);
            orderGoodsList.add(orderGoods);
        }

        // submitOrderToWpc(order, orderGoodsList);

        // 删除购物车里面的商品信息
        // cartService.clearGoods(userId);



        /** 创建用户预估分润 **/
        Map<String, Object> map = new HashMap<>();
        map.put("userId", userId);
        map.put("bid", AccountStatus.BID_27);
        map.put("totalMoney", String.valueOf(order.getPubSharePreFee()));
        map.put("rechargeType", CommonConstant.TASK_REWARD_CASH);
        map.put("orderId", order.getId());
        map.put("orderSn", order.getOrderSn());
        if (StringUtils.isEmpty(shareId)) {
            shareId = "0";
        }
        map.put("shareId", shareId);
        map.put("goodsId", goodsId);
        map.put("flagZy", "0"); // flagZy=0:队列金额没有返回给用户 flagZy=1:队列金额已经返回给用户
        Object objResult = AccountZYUtils.preChargeZy(map);
        logger.info("自营商品分润结果：" + objResult);

    }

}
