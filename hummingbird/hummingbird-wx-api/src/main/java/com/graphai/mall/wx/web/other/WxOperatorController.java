package com.graphai.mall.wx.web.other;

import java.time.LocalDateTime;

import com.graphai.commons.constant.MallConstant;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.mall.db.domain.MallOperatorApply;
import com.graphai.mall.db.domain.MallUser;
import com.graphai.mall.db.service.MallOperatorService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.wx.annotation.LoginUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/wx/operator")
public class WxOperatorController {

	@Autowired
	private MallOperatorService operatorService;
	
	@Autowired
	private MallUserService userService;
	
	@PostMapping("apply")
	public Object getAppVersion(@LoginUser String userId,@RequestBody MallOperatorApply body) {
		if (userId == null) {
			return ResponseUtil.unlogin();
		}
		
		if (body==null) {
			return ResponseUtil.badArgument();
		}
		
		MallUser user = userService.findById(userId);
		if(user==null){
			return ResponseUtil.badArgumentValue("查询不到用户信息");
		}
		body.setCreateTime(LocalDateTime.now());
		body.setUpdateTime(LocalDateTime.now());
		body.setUserLevel(user.getUserLevel());
		body.setDeleted(false);
		body.setApplyStatus(MallConstant.APPLY_STATUS_APPLY);
		operatorService.applBecomeOperator(body);
		return ResponseUtil.ok("申请提交成功,等待专员联系");
	}
	
}
