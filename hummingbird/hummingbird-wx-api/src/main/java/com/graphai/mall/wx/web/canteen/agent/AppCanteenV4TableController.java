/*
package com.graphai.mall.wx.web.canteen.agent;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.generator.controller.IBaseController;
import com.graphai.id.GraphaiIdGenerator;
import com.graphai.mall.admin.domain.MallUserMerchant;
import com.graphai.mall.admin.service.IMallUserMerchantService;
import com.graphai.mall.canteen.domain.KndCanteenAreaTable;
import com.graphai.mall.canteen.domain.KndCanteenTable;
import com.graphai.mall.canteen.service.IKndCanteenAreaTableService;
import com.graphai.mall.canteen.service.IKndCanteenTableService;
import com.graphai.mall.wx.annotation.LoginUser;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;


*/
/**
 * 餐台Controller
 *
 * @author Qxian
 * @date 2020-03-26
 *//*

@RestController
@RequestMapping("/app/v4/canteenTable")
public class AppCanteenV4TableController extends IBaseController<KndCanteenTable,String> {

    @Autowired
    private IKndCanteenTableService kndCanteenTableService;

    @Autowired
    private IKndCanteenAreaTableService kndCanteenAreaTableService;

    @Autowired
    private IMallUserMerchantService userMerchantService;

    @Autowired
    private ServerConfig serverConfig;

    */
/**
     * 查询餐台列表
     *//*

    @PostMapping("/list")
    public Object list(@LoginUser String userId, @RequestBody KndCanteenTable kndCanteenTable) {
        */
/*if (!RoleEnum.CASHIER.getRoleKey().equals(tokenType)) {
            return AjaxResult.permissionError("该用户没有权限");
        }
        //通过userId获取商户信息
        KndMerchant kndMerchant = kndMerchantService.getKndMerchantInfo(userId, tokenType);
        kndCanteenTable.setMerchantId(kndMerchant.getId());
        kndCanteenTable.setStoreId((String) kndMerchant.getParams().get("storeId"));
        startPage();
        List<KndCanteenTable> list = kndCanteenTableService.selectKndCanteenTableList(kndCanteenTable);
        return AjaxResult.success("",getDataTable(list));*//*

        return ResponseUtil.ok(kndCanteenTableService.selectKndCanteenTableList(getPlusPage(),kndCanteenTable));
    }

    */
/**
     * 新增保存餐台
     *//*

    @PostMapping("/add")
    public Object addSave(@LoginUser String userId, @RequestBody KndCanteenTable kndCanteenTable) {

        Object ajaxResult = validateParam(kndCanteenTable);
        if (Objects.nonNull(ajaxResult)) {
            return ajaxResult;
        }
        //判断是否已经存在
        Page<KndCanteenTable> list = kndCanteenTableService.selectKndCanteenTableList(new Page<>(1,10),kndCanteenTable);
        if (null != list && list.getRecords().size() > 0) {
            return ResponseUtil.fail("添加失败，已存在相同桌台!");
        }
        //桌台码
        kndCanteenTable.setId(GraphaiIdGenerator.nextId("kndCanteenTable"));
        //kndCanteenTable.setCodeUrl("https://testfacepay.gzgxkj.com/profile/program/canteen?mid=" + kndMerchant.getId() + "&tid=" + kndCanteenTable.getId());
        //kndCanteenTable.setCodeUrl(serverConfig.getUrl() + "/profile/program/canteen?mid=" + kndCanteenTable.getMerchantId() + "&tid=" + kndCanteenTable.getId());
        //kndCanteenTable.setCodeUrl("");// 先设置为空
        kndCanteenTable.setCodeUrl(serverConfig.getUrl() + "/profile/program/canteen?tid=" + kndCanteenTable.getId());
        kndCanteenTable.setBound(1);
        return toAjax(kndCanteenTableService.insertKndCanteenTable(kndCanteenTable));

        */
/*if (!RoleEnum.CASHIER.getRoleKey().equals(tokenType)) {
            return AjaxResult.permissionError("该用户没有权限");
        }
        //通过userId获取商户信息
        KndMerchant kndMerchant = kndMerchantService.getKndMerchantInfo(userId, tokenType);
        kndCanteenTable.setMerchantId(kndMerchant.getId());
        kndCanteenTable.setStoreId((String) kndMerchant.getParams().get("storeId"));
        kndCanteenTable.setTableSn(kndCanteenTable.getTableSn());
        //判断是否已经存在
        List<KndCanteenTable> list = kndCanteenTableService.selectKndCanteenTableList(kndCanteenTable);
        if (null != list && list.size() > 0) {
            return error("添加失败，已存在相同桌台!");
        }
        //桌台码
        kndCanteenTable.setId(id.nextId(""));
        kndCanteenTable.setCodeUrl("https://testfacepay.gzgxkj.com/profile/program/canteen?mid=" + kndMerchant.getId() + "&tid=" + kndCanteenTable.getId());
        return toAjax(kndCanteenTableService.insertKndCanteenTable(kndCanteenTable));*//*


    }

    */
/**
     * 占桌
     *
     * @param id 桌台号ID
     * @param num 站桌人数
     * @return
     *//*

    @CrossOrigin
    @GetMapping("/lock")
    @ResponseBody
    public Object lock(@RequestParam String id, @RequestParam Integer num) {

        KndCanteenTable existTable = kndCanteenTableService.selectKndCanteenTableById(id);
        if (!StringUtils.isEmpty(existTable.getOrderId())) {
            return AjaxResult.error("请先清理桌台");
        }
        KndCanteenTable table = new KndCanteenTable();
        table.setId(id);
        table.setPeopleNum(num);
        int i = kndCanteenTableService.updateKndCanteenTable(table);

        return i > 0 ? AjaxResult.success() : AjaxResult.error();
    }

    */
/**
     * 清理桌台
     *
     * @param id
     * @return
     *//*

    @CrossOrigin
    @GetMapping("/clear")
    @ResponseBody
    public Object clear(@RequestParam String id) {
        return kndCanteenTableService.clearTable(id) > 0 ? AjaxResult.success() : AjaxResult.error();
    }


    */
/**
     * 修改餐台-回显
     *//*

    @CrossOrigin
    @GetMapping("/edit/{id}")
    @ResponseBody
    public Object edit(@LoginUser String userId,
                           @LoginUserType String tokenType,
                           @PathVariable("id") String id) {

        KndCanteenTable kndCanteenTable = kndCanteenTableService.selectKndCanteenTableById(id);
        if (Objects.isNull(kndCanteenTable))
            return AjaxResult.error("桌台不存在");
        KndMerchant kndMerchant = kndMerchantService.selectId(kndCanteenTable.getMerchantId());
        KndStore kndStore = new KndStore();
        kndStore.setMerchantId(kndMerchant.getId());
        kndStore.setId(kndCanteenTable.getStoreId());
        List<KndStore> kndStoreList = kndStoreService.selectKndStoreList(kndStore);
        List<KndStoreVo> kndStoreListVo = new ArrayList<KndStoreVo>();
        for (KndStore store : kndStoreList) {
            KndStoreVo storeVo = new KndStoreVo();
            storeVo.setId(store.getId());
            storeVo.setName(store.getName());
            storeVo.setMerchantId(store.getMerchantId());
            kndStoreListVo.add(storeVo);
        }
        Map<String, Object> map = new HashMap();
        map.put("kndMerchant", kndMerchant);
        map.put("kndStoreList", kndStoreListVo);
        map.put("kndCanteenTable", kndCanteenTable);
        return AjaxResult.success("查询成功", map);

        */
/*if (!RoleEnum.CASHIER.getRoleKey().equals(tokenType)) {
            return AjaxResult.permissionError("该用户没有权限");
        }
        //通过userId获取商户信息
        KndMerchant kndMerchant = kndMerchantService.getKndMerchantInfo(userId, tokenType);
        KndStore kndStore = new KndStore();
        kndStore.setMerchantId(kndMerchant.getId());
        kndStore.setId((String) kndMerchant.getParams().get("storeId"));
        List<KndStore> kndStoreList = kndStoreService.selectKndStoreList(kndStore);
        KndCanteenTable kndCanteenTable = kndCanteenTableService.selectKndCanteenTableById(id);
        Map map = new HashMap();
        map.put("kndStoreList", kndStoreList);
        map.put("kndCanteenTable", kndCanteenTable);
        return AjaxResult.success("", map);*//*

    }

    */
/**
     * 修改保存餐台
     *//*

    @PostMapping("/edit")
    public Object editSave(@LoginUser String userId,
                               @LoginUserType String tokenType,
                               @RequestBody KndCanteenTable kndCanteenTable) {

        KndCanteenTable kndCanteenTableValidate = kndCanteenTableService.selectKndCanteenTableById(kndCanteenTable.getId());
        if (Objects.isNull(kndCanteenTableValidate))
            return AjaxResult.error("桌台不存在");
        AjaxResult ajaxResult = validateParam(kndCanteenTable);
        if (Objects.nonNull(ajaxResult)) {
            return ajaxResult;
        }
        //判断是否已经存在
        List<KndCanteenTable> list = kndCanteenTableService.selectKndCanteenTableList(kndCanteenTable);
        if (null != list && list.size() > 1) {
            return error("修改失败，已存在相同桌台!");
        }
        return toAjax(kndCanteenTableService.updateKndCanteenTable(kndCanteenTable));

        */
/*if (!RoleEnum.CASHIER.getRoleKey().equals(tokenType)) {
            return AjaxResult.permissionError("该用户没有权限");
        }
        //通过userId获取商户信息
        KndMerchant kndMerchant = kndMerchantService.getKndMerchantInfo(userId, tokenType);
        kndCanteenTable.setMerchantId(kndMerchant.getId());
        kndCanteenTable.setStoreId((String) kndMerchant.getParams().get("storeId"));
        kndCanteenTable.setTableSn(kndCanteenTable.getTableSn());
        //判断是否已经存在
        List<KndCanteenTable> list = kndCanteenTableService.selectKndCanteenTableList(kndCanteenTable);
        if (null != list && list.size() > 1) {
            return error("修改失败，已存在相同桌台!");
        }
        //桌台码
//        kndCanteenTable.setCodeUrl("https://www?" + kndCanteenTable.getTableSn());
        return toAjax(kndCanteenTableService.updateKndCanteenTable(kndCanteenTable));*//*

    }

    */
/**
     * 删除餐台
     *//*

    @PostMapping("/remove")
    public Object remove(@LoginUser String userId, @RequestParam("ids") String ids) {
        */
/*if (!RoleEnum.CASHIER.getRoleKey().equals(tokenType)) {
            return AjaxResult.permissionError("该用户没有权限");
        }*//*

        return toAjax(kndCanteenTableService.deleteKndCanteenTableByIds(ids));
    }


    */
/**
     * 通过storeId查询桌台区域
     *//*

    @PostMapping("/getAreaList")
    public Object getAreaList(@LoginUser String userId,@RequestParam("storeId") String storeId) {
        if (!RoleEnum.CASHIER.getRoleKey().equals(tokenType)) {
            return AjaxResult.permissionError("该用户没有权限");
        }
        //通过userId获取商户信息
        KndMerchant kndMerchant = kndMerchantService.getKndMerchantInfo(userId, tokenType);

        KndCanteenAreaTable kndCanteenAreaTable = new KndCanteenAreaTable();
        kndCanteenAreaTable.setMerchantId(kndMerchant.getId());
        List<KndCanteenAreaTable> list = new ArrayList<>();
        if (storeId != null) {
            kndCanteenAreaTable.setStoreId(storeId);
            list = kndCanteenAreaTableService.selectKndCanteenAreaTableList(kndCanteenAreaTable);
        }
        return AjaxResult.success("", list);
    }

    */
/**
     * 绑定桌台码-回显
     *//*

    @GetMapping("/findBoundById/{id}")
    public Object boundFindById(@LoginUser String userId,@PathVariable("id") String id) {

        KndCanteenTable kndCanteenTable = kndCanteenTableService.selectKndCanteenTableById(id);
        if (Objects.isNull(kndCanteenTable))
            return AjaxResult.error("桌台不存在");
        KndMerchant kndMerchant = kndMerchantService.selectId(kndCanteenTable.getMerchantId());
        if (Objects.isNull(kndMerchant))
            return AjaxResult.error("该桌台码未绑定商户");
        KndStore kndStore = new KndStore();
        kndStore.setMerchantId(kndMerchant.getId());
        kndStore.setId(kndCanteenTable.getStoreId());
        List<KndStore> kndStoreList = kndStoreService.selectKndStoreList(kndStore);
        if (CollectionUtils.isEmpty(kndStoreList))
            return AjaxResult.error("该桌台码未绑定门店");
        List<KndStoreVo> kndStoreListVo = new ArrayList<KndStoreVo>();
        for (KndStore store : kndStoreList) {
            KndStoreVo storeVo = new KndStoreVo();
            storeVo.setId(store.getId());
            storeVo.setName(store.getName());
            storeVo.setMerchantId(store.getMerchantId());
            kndStoreListVo.add(storeVo);
        }
        Map<String, Object> map = new HashMap();
        map.put("kndMerchant", kndMerchant);
        map.put("kndStoreList", kndStoreListVo);
        map.put("kndCanteenTable", kndCanteenTable);
        return AjaxResult.success("查询成功", map);
    }

    @Autowired
    private AmKndTerminalBService kndTerminalBService;

    */
/**
     * 绑定桌台码
     *//*

    @PostMapping("/boundCommit")
    public Object boundCommit(@LoginUser String userId,@RequestBody KndCanteenTable kndCanteenTable) {

        KndCanteenTable kndCanteenTableValidate = kndCanteenTableService.selectKndCanteenTableById(kndCanteenTable.getId());
        if (Objects.isNull(kndCanteenTableValidate))
            return AjaxResult.error("桌台不存在");
        if (Objects.nonNull(kndCanteenTableValidate.getBound()))
            return AjaxResult.error("该桌台码已被绑定，不能再绑");
        AjaxResult ajaxResult = validateParam(kndCanteenTable);
        if (Objects.nonNull(ajaxResult)) {
            return ajaxResult;
        }
        kndCanteenTable.setBound(1);//已绑定

        if(StrUtil.isNotEmpty(kndCanteenTable.getCode())) {
            int i = kndTerminalBService.insertTerminalAndBindTable(kndCanteenTable.getStoreId(), kndCanteenTable.getMerchantId(),  kndCanteenTable.getCode(),kndCanteenTable.getId());
            if (i == 2) {
                return toAjax(kndCanteenTableService.updateKndCanteenTable(kndCanteenTable));
            } else if(i == -1){
                return AjaxResult.error("设备已绑定");
            } else{
                return AjaxResult.error("设备绑定失败");
            }
        }

        return toAjax(kndCanteenTableService.updateKndCanteenTable(kndCanteenTable));
    }

    */
/**
     * 参数校验
     *
     * @param kndCanteenTable
     * @return
     *//*

    private Object validateParam(KndCanteenTable kndCanteenTable) {
        if (StrUtil.isBlank(kndCanteenTable.getMerchantId()))
            return ResponseUtil.fail("商户ID不能为空");
        if (StrUtil.isBlank(kndCanteenTable.getCanteenAreaId()))
            return ResponseUtil.fail("所在区域不能为空");
        if (StrUtil.isBlank(kndCanteenTable.getTableSn()))
            return ResponseUtil.fail("桌号不能为空");
        MallUserMerchant userMerchant = userMerchantService.getOne(new QueryWrapper<MallUserMerchant>()
                .lambda().eq(MallUserMerchant::getUserId, kndCanteenTable.getMerchantId()));
        if (Objects.isNull(userMerchant)) {
            return ResponseUtil.badArgumentValue("商户信息不存在");
        }
        return null;
    }
}

*/
