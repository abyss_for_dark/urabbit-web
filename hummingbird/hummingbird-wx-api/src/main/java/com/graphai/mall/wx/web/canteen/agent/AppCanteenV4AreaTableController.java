package com.graphai.mall.wx.web.canteen.agent;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.generator.controller.IBaseController;
import com.graphai.mall.admin.domain.MallUserMerchant;
import com.graphai.mall.admin.service.IMallUserMerchantService;
import com.graphai.mall.canteen.domain.KndCanteenAreaTable;
import com.graphai.mall.canteen.service.IKndCanteenAreaTableService;
import com.graphai.mall.wx.annotation.LoginUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * 餐厅区域Controller
 *
 * @author Qxian
 * @date 2020-03-26
 */
@RestController
@RequestMapping("/wx/b/canteenAreaTable")
public class AppCanteenV4AreaTableController extends IBaseController<KndCanteenAreaTable, String> {

    @Autowired
    private IKndCanteenAreaTableService kndCanteenAreaTableService;
    @Autowired
    private IMallUserMerchantService userMerchantService;
    /**
     * 查询餐厅区域列表
     */
    @PostMapping("/list")
    @ResponseBody
    public Object list(@LoginUser String userId, @RequestBody KndCanteenAreaTable kndCanteenAreaTable) {
        /*if (!RoleEnum.CASHIER.getRoleKey().equals(tokenType)) {
            return AjaxResult.permissionError("该用户没有权限");
        }
        //通过userId获取商户信息
        KndMerchant kndMerchant = kndMerchantService.getKndMerchantInfo(userId, tokenType);
        kndCanteenAreaTable.setMerchantId(kndMerchant.getId());
        kndCanteenAreaTable.setStoreId((String) kndMerchant.getParams().get("storeId"));
        startPage();
        List<KndCanteenAreaTable> list = kndCanteenAreaTableService.selectKndCanteenAreaTableList(kndCanteenAreaTable);
        return AjaxResult.success("", getDataTable(list));*/

        if (StrUtil.isNotBlank(kndCanteenAreaTable.getMerchantId())) {
            MallUserMerchant userMerchant = userMerchantService.getOne(new QueryWrapper<MallUserMerchant>()
                    .lambda().eq(MallUserMerchant::getUserId, userId));
            if (Objects.isNull(userMerchant)) {
                return ResponseUtil.badArgumentValue("商户信息不存在");
            }
            kndCanteenAreaTable.setMerchantId(userMerchant.getMerchantId());
        }
        return ResponseUtil.ok(kndCanteenAreaTableService.selectKndCanteenAreaTableListPage(getPlusPage(),kndCanteenAreaTable));


    }

    /**
     * 新增保存餐厅区域
     */
    @PostMapping("/add")
    public Object addSave(@LoginUser String userId, @RequestBody KndCanteenAreaTable kndCanteenAreaTable) {
        /*if (!RoleEnum.CASHIER.getRoleKey().equals(tokenType)) {
            return AjaxResult.permissionError("该用户没有权限");
        }
        KndMerchant kndMerchant = kndMerchantService.getKndMerchantInfo(userId, tokenType);
        kndCanteenAreaTable.setMerchantId(kndMerchant.getId());
        kndCanteenAreaTable.setStoreId((String) kndMerchant.getParams().get("storeId"));
        kndCanteenAreaTable.setAreaName(kndCanteenAreaTable.getAreaName());*/

        if (StrUtil.isBlank(kndCanteenAreaTable.getAreaName()))
            return ResponseUtil.fail("区域名称不能为空");
        if (StrUtil.isBlank(kndCanteenAreaTable.getMerchantId()))
            return ResponseUtil.fail("商户ID不能为空");
        if (StrUtil.isBlank(kndCanteenAreaTable.getStoreId()))
            return ResponseUtil.fail("店铺ID不能为空");
        MallUserMerchant userMerchant = userMerchantService.getOne(new QueryWrapper<MallUserMerchant>()
                .lambda().eq(MallUserMerchant::getUserId, userId));
        if (Objects.isNull(userMerchant)) {
            return ResponseUtil.fail("商户信息不存在");
        }
        kndCanteenAreaTable.setMerchantId(userMerchant.getMerchantId());

        //判断是否已经存在
        List<KndCanteenAreaTable> list = kndCanteenAreaTableService.selectKndCanteenAreaTableList(kndCanteenAreaTable);
        if (null != list && list.size() > 0) {
            return ResponseUtil.fail("已存在相同区域名!");
        }
        if (kndCanteenAreaTableService.insertKndCanteenAreaTable(kndCanteenAreaTable) > 0)
             return ResponseUtil.ok();
       return ResponseUtil.fail();
    }

    /**
     * 修改餐厅区域-回显
     */
    @CrossOrigin
    @GetMapping("/edit/{id}")
    public Object edit(@LoginUser String userId, @PathVariable("id") String id) {
        /*//查询菜品分类
        if (!RoleEnum.CASHIER.getRoleKey().equals(tokenType)) {
            return AjaxResult.permissionError("该用户没有权限");
        }
        //通过userId获取商户信息
        KndMerchant kndMerchant = kndMerchantService.getKndMerchantInfo(userId, tokenType);
        KndStore kndStore = new KndStore();
        kndStore.setMerchantId(kndMerchant.getId());
        kndStore.setId((String) kndMerchant.getParams().get("storeId"));
        List<KndStore> kndStoreList = kndStoreService.selectKndStoreList(kndStore);
        KndCanteenAreaTable kndCanteenAreaTable = kndCanteenAreaTableService.selectKndCanteenAreaTableById(id);
        Map map = new HashMap();
        map.put("kndStoreList", kndStoreList);
        map.put("kndCanteenAreaTable", kndCanteenAreaTable);
        return AjaxResult.success("", map);*/

        KndCanteenAreaTable kndCanteenAreaTable = kndCanteenAreaTableService.selectKndCanteenAreaTableById(id);
        Map<String,Object> map = new HashMap();
        map.put("kndCanteenAreaTable", kndCanteenAreaTable);
        return ResponseUtil.ok(map);
    }

    /**
     * 修改保存餐厅区域
     */
    @CrossOrigin
    @PostMapping("/edit")
    public Object editSave(@LoginUser String userId,
                               @RequestBody KndCanteenAreaTable kndCanteenAreaTable) {
        /*if (!RoleEnum.CASHIER.getRoleKey().equals(tokenType)) {
            return AjaxResult.permissionError("该用户没有权限");
        }
        //通过userId获取商户信息
        KndMerchant kndMerchant = kndMerchantService.getKndMerchantInfo(userId, tokenType);

        kndCanteenAreaTable.setMerchantId(kndMerchant.getId());
        kndCanteenAreaTable.setStoreId((String) kndMerchant.getParams().get("storeId"));
        kndCanteenAreaTable.setAreaName(kndCanteenAreaTable.getAreaName());
        //判断是否已经存在
        List<KndCanteenAreaTable> list = kndCanteenAreaTableService.selectKndCanteenAreaTableList(kndCanteenAreaTable);
        if (null != list && list.size() > 0) {
            return error("已存在相同区域名!");
        }
        return toAjax(kndCanteenAreaTableService.updateKndCanteenAreaTable(kndCanteenAreaTable));*/

        if (StrUtil.isBlank(kndCanteenAreaTable.getAreaName()))
            return ResponseUtil.fail("区域名称不能为空");
        if (StrUtil.isBlank(kndCanteenAreaTable.getId()))
            return ResponseUtil.fail("区域ID不能为空");
        if (StrUtil.isNotBlank(kndCanteenAreaTable.getMerchantId())) {
            MallUserMerchant userMerchant = userMerchantService.getOne(new QueryWrapper<MallUserMerchant>()
                    .lambda().eq(MallUserMerchant::getUserId, userId));
            if (Objects.isNull(userMerchant)) {
                return ResponseUtil.fail("商户信息不存在");
            }
            kndCanteenAreaTable.setMerchantId(userMerchant.getMerchantId());
        }
        //判断是否已经存在
        List<KndCanteenAreaTable> list = kndCanteenAreaTableService.selectKndCanteenAreaTableList(kndCanteenAreaTable);
        if (null != list && list.size() > 0) {
            return ResponseUtil.fail("已存在相同区域名!");
        }
        return kndCanteenAreaTableService.updateKndCanteenAreaTable(kndCanteenAreaTable) > 0 ? ResponseUtil.fail() : ResponseUtil.ok();
    }

    /**
     * 删除餐厅区域
     */
    @CrossOrigin
    @PostMapping("/remove")
    @ResponseBody
    public Object remove(@LoginUser String userId, @RequestBody String ids) {
        /*if (!RoleEnum.CASHIER.getRoleKey().equals(tokenType)) {
            return AjaxResult.permissionError("该用户没有权限");
        }*/
        JSONObject jsonObject = JSONUtil.parseObj(ids);
        String removeIds =  jsonObject.getStr("ids");
        if (kndCanteenAreaTableService.deleteKndCanteenAreaTableByIds(removeIds) > 0){
            return ResponseUtil.ok();
        }
        return ResponseUtil.fail();
    }
}
