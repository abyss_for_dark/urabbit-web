package com.graphai.mall.wx.web.tripartite;


import com.graphai.commons.util.ResponseUtil;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.mall.wx.service.MallCaocaoOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;

@RestController
@RequestMapping("/wx/caocao")
public class WxCaoCaoController {


    @Autowired
    private MallCaocaoOrderService mallCaocaoOrderService;

    @GetMapping("/linkUrl")
    public Object index(@LoginUser String userId) throws UnsupportedEncodingException {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        return ResponseUtil.ok("/pages/web/webview?link=" + mallCaocaoOrderService.getLinkUrl(userId));
    }

    @PostMapping("/orderCallBack")
    public Object orderCallBack(@RequestParam String order_id,
                                @RequestParam String event) {
        int code = mallCaocaoOrderService.orderHandler(order_id, event);
        HashMap<String, Object> map = new HashMap<>();
        if (code > -1) {
            map.put("code",200);
            map.put("success",true);
        }else{
            map.put("code",-1);
            map.put("success",false);
        }
        return map;

    }

}
