package com.graphai.mall.wx.service;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.system.service.IGraphaiSystemRoleService;
import com.graphai.id.GraphaiIdGenerator;
import com.graphai.mall.admin.dao.MallGoodsCategoryMapper;
import com.graphai.mall.admin.domain.*;
import com.graphai.mall.admin.service.*;
import com.graphai.mall.admin.service.impl.MallUserMerchantServiceImpl;
import com.graphai.mall.core.qcode.QCodeService;
import com.graphai.mall.db.constant.coupon.CouponConstant;
import com.graphai.mall.db.dao.*;
import com.graphai.mall.db.domain.*;
import com.graphai.mall.db.service.MallAdminService;
import com.graphai.mall.db.service.advertisement.MallBrandService;
import com.graphai.mall.db.service.category.MallCategoryService;
import com.graphai.mall.db.service.coupon.MallCouponService;
import com.graphai.mall.db.service.goods.*;
import com.graphai.mall.db.service.groupon.MallGrouponRulesService;
import com.graphai.mall.db.util.StringHelper;
import com.graphai.mall.wx.dto.GoodsAllinone;
import com.graphai.mall.wx.vo.CatVo;
import com.graphai.open.mallcategory.entity.IMallCategory;
import com.graphai.open.mallcategory.service.IMallCategoryService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class WxGoodsService {
    private final Log logger = LogFactory.getLog(WxGoodsService.class);
    public static final Integer GOODS_NAME_EXIST = 611;
    @Autowired
    private MallGoodsService goodsService;
    @Autowired
    private MallGoodsSpecificationService specificationService;
    @Autowired
    private MallGoodsAttributeService attributeService;
    @Autowired
    private MallGoodsProductService productService;
    @Autowired
    private MallCategoryService categoryService;
    @Autowired
    private MallBrandService brandService;

    @Autowired
    private QCodeService qCodeService;
    @Resource
    private MallCouponMapper mallCouponMapper;
    @Resource
    private MallProductCouponMapper mallProductCouponMapper;
    @Resource
    private MallGoodsCouponCodeMapper mallGoodsCouponCodeMapper;
    @Autowired
    private MallCouponService mallCouponService;
    @Autowired
    private MallCategoryMapper mallCategoryMapper;
    @Autowired
    private IGraphaiSystemRoleService iGraphaiSystemRoleService;

    @Autowired
    private MallGoodsCouponCodeService mallGoodsCouponCodeService;

    @Autowired
    private MallGrouponRulesService rulesService;
    @Autowired
    private IMallUserProfitNewService userProfitNewService;
    @Autowired
    IMallGoodsPositionService goodsPositionService;
    @Autowired
    IMallLiveRoomProductService mallLiveRoomProductService;
    @Autowired
    IMallActivityGoodsProductService mallActivityGoodsProductService;
    @Autowired
    private MallAdminService adminService;

    @Autowired
    private MallUserMerchantServiceImpl userMerchantService;
    /**
     * 时间戳转换为日期字符串
     *
     * @param time
     * @return
     */
    private static String convertTimeToFormat(Long time) {
        Assert.notNull(time, "time is null");
        DateTimeFormatter ftf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return ftf.format(LocalDateTime.ofInstant(Instant.ofEpochMilli(time), ZoneId.systemDefault()));
    }




    @Resource
    private CustomGoodsMapper customGoodsMapper;
    @Resource
    private IMallMerchantService mallMerchantService;

    public Object list(String merchantId, Integer page, Integer limit, String sort, String order) {
//        MallGoodsExample example = new MallGoodsExample();
//        MallGoodsExample.Criteria criteria = example.createCriteria().andIsOnSaleEqualTo(true);
//
//        if (merchantId != null && !"".equals(merchantId)) {
//            criteria.andMerchantIdEqualTo(merchantId);
//        }
//        example.setOrderByClause(sort + " " + order);
//        PageHelper.startPage(page, limit);
//        List<MallGoods> result = goodsMapper.selectByExample(example);
//        PageInfo<MallGoods> pages = new PageInfo<>(result);
        List<MallGoods> pages = customGoodsMapper.queryPlantGoods(merchantId,(page - 1) * limit,limit);
        return ResponseUtil.ok(pages);
    }


    public Object list(String goodsSn, String name, Integer page, Integer limit, String sort, String order) {
        List<MallGoods> goodsList = goodsService.querySelective(goodsSn, name, page, limit, sort, order);
        long total = PageInfo.of(goodsList).getTotal();
        Map<String, Object> data = new HashMap<>();
        data.put("total", total);
        data.put("items", goodsList);

        return ResponseUtil.ok(data);
    }

    public Object list(String merchantNoSale,String goodsSn, String goodsName, Integer page, Integer limit, String sort, String order,
                    String brandName, Integer priceMin, Integer platform, Integer priceMax, Boolean isHot,
                    Boolean isNew, Boolean isOnSale, Integer categorypid, Integer industryId, Integer goodsType,
                    Boolean isRightGoods,Integer placement) {
        // 获取当前用户
        MallAdmin adminUser = (MallAdmin) SecurityUtils.getSubject().getPrincipal();
        String deptId = "";
        if (adminUser != null) {
            deptId = adminUser.getDeptId();
        }
        List<MallRole> mallRoleList = iGraphaiSystemRoleService.selectRoleListMMByUserId(adminUser.getId());
        MallRole mallRole = null;
        if (mallRoleList.size() > 0) {
            mallRole = mallRoleList.get(0);
        }

        logger.info("第一次时间戳" + convertTimeToFormat(System.currentTimeMillis()));
        List<MallGoods> goodsList = goodsService.querySelective(merchantNoSale,goodsSn, goodsName, page, limit, sort, order, brandName,
                        priceMin, priceMax, platform, isHot, isNew, isOnSale, categorypid, industryId, goodsType,
                        isRightGoods, mallRole, deptId, adminUser, placement);
        Long totalNum = new Long(0);
        Long buyNum = new Long(0);
        Long useNum = new Long(0);
        logger.info("第二次时间戳" + convertTimeToFormat(System.currentTimeMillis()));
        // 淘宝商品雷米idList
        List<String> tbIdList = new ArrayList<String>();
        List<String> couponGoodsIdList = new ArrayList<String>();
        // 其他平台类目idList
        List<String> otherIdList = new ArrayList<String>();
        // 淘宝List
        List<Map<String, Object>> tbCaList = new ArrayList<Map<String, Object>>();
        // 其他平台List
        List<Map<String, Object>> otherCaList = new ArrayList<Map<String, Object>>();


        List<Map<String, Object>> totalCount = new ArrayList<Map<String, Object>>();// 优惠券商品总库存
        List<Map<String, Object>> buyCount = new ArrayList<Map<String, Object>>(); // 优惠券商品总已售

        if (Objects.equals(1, platform)) {
            if (CollectionUtils.isNotEmpty(goodsList)) {
                tbIdList = goodsList.stream().map(MallGoods::getCategoryId).collect(Collectors.toList());
                tbCaList = categoryService.findTbCategoryName(tbIdList);
            }
        } else {
            if (CollectionUtils.isNotEmpty(goodsList)) {
                for (MallGoods goods : goodsList) {
                    if (Objects.equals(1, goods.getPlatform())) {
                        tbIdList.add(goods.getCategoryId());
                    } else if (2 == goods.getVirtualGood()) {
                        couponGoodsIdList.add(goods.getId());
                    } else {
                        otherIdList.add(goods.getCategoryId());
                    }
                }
            }

            if (CollectionUtils.isNotEmpty(tbIdList)) {
                tbCaList = categoryService.findTbCategoryName(tbIdList);
            }

            if (CollectionUtils.isNotEmpty(otherIdList)) {
                otherCaList = categoryService.findOtherCategoryName(otherIdList);
            }

        }


        // 优惠券商品总库存
        // if (CollectionUtils.isNotEmpty(couponGoodsIdList)) {
        // totalCount = mallGoodsCouponCodeService.getGoodsCouponTotalCount(couponGoodsIdList);
        // buyCount = mallGoodsCouponCodeService.getGoodsCouponBuyCount(couponGoodsIdList);
        // }


        System.out.println("total" + totalCount.toString());
        List<String> merchantIds = new ArrayList<>();
        for (int i = 0; i < goodsList.size(); i++) {
            // 自营优惠券才有兑换量 库存 销售量
            if (StringHelper.isNotNullAndEmpty(goodsType) && 2 == goodsType) {
                MallGoodsCouponCodeExample totalExample = new MallGoodsCouponCodeExample();
                totalExample.or().andGoodsIdEqualTo(goodsList.get(i).getId()).andDeletedEqualTo(false);
                totalNum = mallGoodsCouponCodeMapper.countByExample(totalExample);
                MallGoodsCouponCodeExample buyExample = new MallGoodsCouponCodeExample();
                buyExample.or().andGoodsIdEqualTo(goodsList.get(i).getId()).andSaleStatusEqualTo(true)
                                .andDeletedEqualTo(false);
                buyNum = mallGoodsCouponCodeMapper.countByExample(buyExample);
                MallGoodsCouponCodeExample useExample = new MallGoodsCouponCodeExample();
                useExample.or().andGoodsIdEqualTo(goodsList.get(i).getId()).andSaleStatusEqualTo(true)
                                .andUseStatusEqualTo(true).andDeletedEqualTo(false);
                useNum = mallGoodsCouponCodeMapper.countByExample(useExample);

                Long num = totalNum - buyNum;
                goodsList.get(i).setShopname((num <= 0 ? "0" : num.toString()) + "/" + totalNum.toString()); // 借用字段

                // 填充剩余库存/总库存
                goodsList.get(i).setItemsale(buyNum.intValue()); // 借用字段 填充销售量
                goodsList.get(i).setItemsale2(useNum.intValue()); // 借用字段 填充兑换量


            }

            if (Objects.equals(goodsList.get(i).getPlatform(), 1)) {
                if (CollectionUtils.isNotEmpty(tbCaList)) {
                    for (Map<String, Object> map : tbCaList) {
                        if (Objects.equals(String.valueOf(map.get("tbCateId")), goodsList.get(i).getCategoryId())) {
                            goodsList.get(i).setSellerName(String.valueOf(map.get("name")));
                        }
                    }
                }
            } else {
                if (CollectionUtils.isNotEmpty(otherCaList)) {
                    for (Map<String, Object> map : otherCaList) {
                        if (Objects.equals(String.valueOf(map.get("id")), goodsList.get(i).getCategoryId())) {
                            goodsList.get(i).setSellerName(String.valueOf(map.get("name")));
                        }
                    }
                }
            }
            merchantIds.add(goodsList.get(i).getMerchantId());
        }
        if(merchantIds.size() > 0) {
            QueryWrapper<MallMerchant> queryWrapper = new QueryWrapper<>();
            queryWrapper.in("id", merchantIds);
            List<MallMerchant> mallMerchants = mallMerchantService.list(queryWrapper);
            for(MallGoods goods : goodsList){
                for(MallMerchant mallMerchant : mallMerchants){
                    if(mallMerchant.getId().equals(goods.getMerchantId())){
                        goods.setMerchantName(mallMerchant.getTitle());
                        goods.setJumpUrl(mallMerchant.getLog());
                    }
                }
            }
        }

            long total = PageInfo.of(goodsList).getTotal();
        Map<String, Object> data = new HashMap<>();
        data.put("total", total);
        data.put("items", goodsList);
        if(ObjectUtil.isNotNull(mallRole) && ObjectUtil.isNotEmpty(mallRole)) {
            data.put("login", mallRole.getRoleKey());
        }else{
            data.put("login", "admin");
        }
        return ResponseUtil.ok(data);
    }


    private Object validate(GoodsAllinone goodsAllinone) {
        MallGoods goods = goodsAllinone.getGoods();
        String name = goods.getName();
        if (StringUtils.isEmpty(name)) {
            return ResponseUtil.badArgumentValue("商品名称不能为空！");
        }
        String brief = goods.getBrief();
        if (StringUtils.isEmpty(brief)) {
            return ResponseUtil.badArgumentValue("请输入商品简介！");
        }

//        // 分类可以不设置，如果设置则需要验证分类存在
//        String categoryId = goods.getCategoryId();
//        if (StringUtils.isEmpty(categoryId)) {
//            return ResponseUtil.badArgumentValue("请选择所属类目！");
//        }
//        BigDecimal retailPrice = goods.getRetailPrice();
//        if (retailPrice == null) {
//            return ResponseUtil.badArgumentValue("销售价格不能为空！");
//        }

        // 品牌商可以不设置，如果设置则需要验证品牌商存在
        String brandId = goods.getBrandId();
        if (brandId != null && !"0".equals(brandId)) {
            if (brandService.findById(brandId) == null) {
                return ResponseUtil.badArgumentValue("品牌商不存在！");
            }
        }


//        if (categoryId != null && !"0".equals(categoryId)) {
//            if (categoryService.findById(categoryId) == null) {
//                return ResponseUtil.badArgumentValue("类目不存在！");
//            }
//        }

        MallGoodsAttribute[] attributes = goodsAllinone.getAttributes();
        for (MallGoodsAttribute attribute : attributes) {
            String attr = attribute.getAttribute();
            if (StringUtils.isEmpty(attr)) {
                return ResponseUtil.badArgument();
            }
            String value = attribute.getValue();
            if (StringUtils.isEmpty(value)) {
                return ResponseUtil.badArgument();
            }
        }

        MallGoodsSpecification[] specifications = goodsAllinone.getSpecifications();
        for (MallGoodsSpecification specification : specifications) {
            String spec = specification.getSpecification();
            if (StringUtils.isEmpty(spec)) {
                return ResponseUtil.badArgument();
            }
            String value = specification.getValue();
            if (StringUtils.isEmpty(value)) {
                return ResponseUtil.badArgument();
            }
        }

        MallGoodsProduct[] products = goodsAllinone.getProducts();
        for (MallGoodsProduct product : products) {
            Integer number = product.getNumber();
            if (number == null || number < 0) {
                return ResponseUtil.badArgument();
            }

            BigDecimal price = product.getPrice();
            if (price == null) {
                return ResponseUtil.badArgument();
            }

            String[] productSpecifications = product.getSpecifications();
            if (productSpecifications.length == 0) {
                return ResponseUtil.badArgument();
            }
        }

        return null;
    }

    /**
     * 编辑商品
     * <p>
     * TODO 目前商品修改的逻辑是 1. 更新Mall_goods表 2.
     * 逻辑删除Mall_goods_specification、Mall_goods_attribute、Mall_goods_product 3.
     * 添加Mall_goods_specification、Mall_goods_attribute、Mall_goods_product
     * <p>
     * 这里商品三个表的数据采用删除再添加的策略是因为 商品编辑页面，支持管理员添加删除商品规格、添加删除商品属性，因此这里仅仅更新是不可能的， 只能删除三个表旧的数据，然后添加新的数据。
     * 但是这里又会引入新的问题，就是存在订单商品货品ID指向了失效的商品货品表。 因此这里会拒绝管理员编辑商品，如果订单或购物车中存在商品。 所以这里可能需要重新设计。
     */
    @Transactional
    public Object update(GoodsAllinone goodsAllinone) {
        Object error = validate(goodsAllinone);
        if (error != null) {
            return error;
        }

        MallGoods goods = goodsAllinone.getGoods();
        MallGoodsAttribute[] attributes = goodsAllinone.getAttributes();
        MallGoodsSpecification[] specifications = goodsAllinone.getSpecifications();
        MallGoodsProduct[] products = goodsAllinone.getProducts();


        if (CollectionUtils.isNotEmpty(goodsAllinone.getCategoryIds())){
            List<String> categoryIds = goodsAllinone.getCategoryIds();
            //删除商品分类
            QueryWrapper<MallGoodsCategory> que = new QueryWrapper<>();
            que.eq("goods_id",goods.getId());
            mallGoodsCategoryService.remove(que);

            //添加商品分类
            for(String categoryId : categoryIds){
                MallGoodsCategory mallGoodsCategory = new MallGoodsCategory();
                mallGoodsCategory.setGoodsId(goods.getId());
                mallGoodsCategory.setCategoryId(categoryId);
                mallGoodsCategory.setId(GraphaiIdGenerator.nextId("MallGoodsCategory"));
                mallGoodsCategoryService.save(mallGoodsCategory);
            }
        }

        // 将生成的分享图片地址写入数据库
        String url = qCodeService.createGoodShareImage(goods.getId().toString(), goods.getPicUrl(), goods.getName());
        goods.setShareUrl(url);

        if (Objects.equals(null, goods.getcRetailPrice())) {
            goods.setcRetailPrice(BigDecimal.ZERO);
        }

        // 商品基本信息表Mall_goods
        if (goodsService.updateById(goods) == 0) {
            return ResponseUtil.fail(500, "更新商品数据失败");
        }

        // 优惠券更新/添加
        MallCoupon coupon = goodsAllinone.getCoupon();
        if (null != coupon) {
            coupon.setStatus(goods.getIsOnSale() ? new Short("1") : new Short("0"));
            coupon.setUpdateTime(LocalDateTime.now());
            if (null == coupon.getId()) {
                coupon.setId(GraphaiIdGenerator.nextId("MallCoupon"));
                mallCouponMapper.insertSelective(coupon);
                MallProductCoupon productCoupon = new MallProductCoupon();
                productCoupon.setId(GraphaiIdGenerator.nextId("MallProductCoupon"));
                productCoupon.setGoodsId(goods.getId());
                productCoupon.setCouponId(coupon.getId());
                productCoupon.setStatus("1");
                productCoupon.setDeleted(false);
                productCoupon.setAddTime(LocalDateTime.now());
                productCoupon.setUpdateTime(LocalDateTime.now());
                mallProductCouponMapper.insertSelective(productCoupon);
            } else {
                mallCouponMapper.updateByPrimaryKey(coupon);
            }
        }

        String gid = goods.getId();
        specificationService.deleteByGid(gid);
        attributeService.deleteByGid(gid);
        productService.deleteByGid(gid);

        // 商品规格表Mall_goods_specification
        for (MallGoodsSpecification specification : specifications) {
            specification.setGoodsId(goods.getId());
            specificationService.add(specification);
        }

        // 商品参数表Mall_goods_attribute
        for (MallGoodsAttribute attribute : attributes) {
            attribute.setGoodsId(goods.getId());
            attributeService.add(attribute);
        }

        // 商品货品表Mall_product
        for (MallGoodsProduct product : products) {
            product.setGoodsId(goods.getId());
            productService.add(product);
        }
        //修改 团购规则
        if ( goodsAllinone.getGrouponRules() != null &&  goodsAllinone.getGrouponRules().getId() != null){
            goodsAllinone.getGrouponRules().setGoodsId(null);
            goodsAllinone.getGrouponRules().setReturnRatio(goodsAllinone.getUserProfitNew().getTwoMerProfit().divide(new BigDecimal(100)));
            if(goodsAllinone.getGrouponRules().getPaymode() == 1){
                goodsAllinone.getGrouponRules().setPrepayments(new BigDecimal(1));
            }else{
                goodsAllinone.getGrouponRules().setPrepayments(goodsAllinone.getGrouponRules().getPrepayments().divide(new BigDecimal(100)));
            }
            rulesService.updateById(goodsAllinone.getGrouponRules());
        }
        //修改 分润比例
        if (goodsAllinone.getUserProfitNew() !=null && goodsAllinone.getUserProfitNew().getId() != null){
            if (!StringUtils.isEmpty(goodsAllinone.getUserProfitNew().getOrdinaryUsersProfit())){
                goodsAllinone.getUserProfitNew().setOrdinaryUsersProfit(goodsAllinone.getUserProfitNew().getOrdinaryUsersProfit().divide(new BigDecimal(100)));
            }
            if (!StringUtils.isEmpty(goodsAllinone.getUserProfitNew().getOneMerProfit())){
                goodsAllinone.getUserProfitNew().setOneMerProfit(goodsAllinone.getUserProfitNew().getOneMerProfit().divide(new BigDecimal(100)));
            }
            if (!StringUtils.isEmpty(goodsAllinone.getUserProfitNew().getUserProfit())){
                goodsAllinone.getUserProfitNew().setUserProfit(goodsAllinone.getUserProfitNew().getUserProfit().divide(new BigDecimal(100)));
            }
            if (!StringUtils.isEmpty(goodsAllinone.getUserProfitNew().getTwoMerProfit())){
                goodsAllinone.getUserProfitNew().setTwoMerProfit(goodsAllinone.getUserProfitNew().getTwoMerProfit().divide(new BigDecimal(100)));
            }
            userProfitNewService.updateById(goodsAllinone.getUserProfitNew());
        }
        return ResponseUtil.ok();
    }

    @Transactional
    public Object delete(MallGoods goods) {
        String id = goods.getId();
        if (id == null) {
            return ResponseUtil.badArgument();
        }

        String gid = goods.getId();
        goodsService.deleteById(gid);
        specificationService.deleteByGid(gid);
        attributeService.deleteByGid(gid);
        productService.deleteByGid(gid);
        return ResponseUtil.ok();
    }

    @Autowired
    private MallGoodsCategoryMapper mallGoodsCategoryMapper;

    @Transactional
    public Object create(String userId,GoodsAllinone goodsAllinone) {
        // 获取当前用户
        MallUserMerchant userMerchant = userMerchantService.getOne(new QueryWrapper<MallUserMerchant>().lambda().eq(MallUserMerchant::getUserId, userId));
//        Object error = validate(goodsAllinone);
//        if (error != null) {
//            return error;
//        }
        MallGoods goods = goodsAllinone.getGoods();

        MallGoodsAttribute[] attributes = goodsAllinone.getAttributes();
        MallGoodsSpecification[] specifications = goodsAllinone.getSpecifications();
        MallGoodsProduct[] products = goodsAllinone.getProducts();

        if(CollectionUtils.isNotEmpty(goodsAllinone.getCategoryIds())){
            List<String> ids = goodsAllinone.getCategoryIds();
            //兼容未来生活单分类，将多分类的第一个分类补充再商品表的categoryId中
            goods.setCategoryId(ids.get(0));
            for(String id : ids){
                MallGoodsCategory mallGoodsCategory = new MallGoodsCategory();
                mallGoodsCategory.setCategoryId(id);
                mallGoodsCategory.setGoodsId(goods.getId());
                mallGoodsCategory.setId(GraphaiIdGenerator.nextId("MallGoodsCategory"));
                mallGoodsCategoryService.save(mallGoodsCategory);
            }
        }


        MallProductCoupon productCoupon = new MallProductCoupon();

        goods.setVirtualGood(null != goods.getVirtualGood() ? goods.getVirtualGood() : 0); // 空或0是实体商品，1是虚拟商品,2优惠券商品

        if(goods.getVirtualGood() == 0 || goods.getVirtualGood() == 7){
            goods.setCommissionType(0);
        }

//        goods.setCategoryId(null);
        // 商品基本信息表Mall_goods
        goods.setMerchantId(userMerchant.getMerchantId());
        goods.setIsOnSale(false);
        goodsService.add(goods);

        if (CollectionUtils.isNotEmpty(goodsAllinone.getCategoryIds())){
            List<String> categoryIds = goodsAllinone.getCategoryIds();
            for(String id : categoryIds){
                MallGoodsCategory mallGoodsCategory = new MallGoodsCategory();
                mallGoodsCategory.setId(GraphaiIdGenerator.nextId("MallGoods"));
                mallGoodsCategory.setCategoryId(id);
                mallGoodsCategory.setGoodsId(goods.getId());
                mallGoodsCategoryMapper.insert(mallGoodsCategory);
            }
        }

        if (null == goods || null == goods.getId()) {
            return ResponseUtil.fail(-1, "商品添加逻辑失败,商品id获取不到");
        } else {
            productCoupon.setGoodsId(goods.getId());
        }

        String goodsId = goods.getId();
        //判断 团购规则对象是否有值 有则新增
        if (!StringUtils.isEmpty(goodsAllinone.getGrouponRules()) && !StringUtils.isEmpty(goodsAllinone.getGrouponRules().getGroupPrice())){
            MallGoods goodsV1 = goodsService.findById(goodsId);
            if (goodsV1 == null) {
                return ResponseUtil.badArgumentValue();
            }
            goodsAllinone.getGrouponRules().setGoodsId(goodsV1.getId());
            goodsAllinone.getGrouponRules().setGoodsName(goodsV1.getName());
            goodsAllinone.getGrouponRules().setPicUrl(goodsV1.getPicUrl());
            if (!StringUtils.isEmpty(goodsAllinone.getUserProfitNew().getTwoMerProfit())){
                goodsAllinone.getGrouponRules().setReturnRatio(goodsAllinone.getUserProfitNew().getTwoMerProfit().divide(new BigDecimal(100)));
            }
            //判断支付模式 全款还是 预付款+尾款 全款时 预付款则为100% 否则根据所传入值
            if (goodsAllinone.getGrouponRules().getPaymode() == 1){
                goodsAllinone.getGrouponRules().setPrepayments(new BigDecimal(1));
            }else{
                BigDecimal num = goodsAllinone.getGrouponRules().getPrepayments();
                goodsAllinone.getGrouponRules().setPrepayments(num.divide(new BigDecimal(100)));
            }
            int createRulesNum = rulesService.createRules(goodsAllinone.getGrouponRules());
            if (createRulesNum<=0){
                return ResponseUtil.fail(-1, "添加团购规则失败");
            }
        }

        //判断 分润对象是否有值 有则新增
        if (!StringUtils.isEmpty(goodsAllinone.getUserProfitNew()) && !StringUtils.isEmpty(goodsAllinone.getUserProfitNew().getOrdinaryUsersProfit())){
            goodsAllinone.getUserProfitNew().setId(GraphaiIdGenerator.nextId("UserProfitNew"));
            goodsAllinone.getUserProfitNew().setAddTime(LocalDateTime.now());
            goodsAllinone.getUserProfitNew().setUpdateTime(LocalDateTime.now());
            //总佣金转换
            if (!StringUtils.isEmpty(goodsAllinone.getUserProfitNew().getOrdinaryUsersProfit())){
                goodsAllinone.getUserProfitNew().setOrdinaryUsersProfit(goodsAllinone.getUserProfitNew().getOrdinaryUsersProfit().divide(new BigDecimal(100)));
            }
            //平台佣金转换
            if (!StringUtils.isEmpty(goodsAllinone.getUserProfitNew().getUserProfit())){
                goodsAllinone.getUserProfitNew().setUserProfit(goodsAllinone.getUserProfitNew().getUserProfit().divide(new BigDecimal(100)));
            }
            //未中奖用户奖励佣金转换
            if (!StringUtils.isEmpty(goodsAllinone.getUserProfitNew().getTwoMerProfit())){
                goodsAllinone.getUserProfitNew().setTwoMerProfit(goodsAllinone.getUserProfitNew().getTwoMerProfit().divide(new BigDecimal(100)));
            }
            //参团用户直推人佣金转换
            if (!StringUtils.isEmpty(goodsAllinone.getUserProfitNew().getOneMerProfit())){
                goodsAllinone.getUserProfitNew().setOneMerProfit(goodsAllinone.getUserProfitNew().getOneMerProfit().divide(new BigDecimal(100)));
            }
            goodsAllinone.getUserProfitNew().setGoodsId(goodsId);
            //设置分类 7为团购
            goodsAllinone.getUserProfitNew().setProfitType(7);
            boolean userProfitNewSave = userProfitNewService.save(goodsAllinone.getUserProfitNew());
            if (!userProfitNewSave){
                return ResponseUtil.fail(-1, "添加分润比例失败！");
            }
        }

        //判断所选商品位置是否为空
        if(ObjectUtil.isNotEmpty(goodsAllinone.getPlacement()) && ObjectUtil.isNotNull(goodsAllinone.getPlacement())) {
            if (goodsAllinone.getPlacement().length > 0) {
                for (int i = 0; i < goodsAllinone.getPlacement().length; i++) {
                    MallGoodsPosition goodsPosition = new MallGoodsPosition();
                    goodsPosition.setId(GraphaiIdGenerator.nextId("GoodsPosition"));
                    goodsPosition.setCreateTime(LocalDateTime.now());
                    goodsPosition.setUpdateTime(LocalDateTime.now());
                    goodsPosition.setGoodsId(goodsId);
                    goodsPosition.setType(goodsAllinone.getPlacement()[i]);
                    boolean goodsPositionSave = goodsPositionService.save(goodsPosition);
                    if (!goodsPositionSave) {
                        return ResponseUtil.fail(-1, "添加商品位置失败");
                    }
                }
            }
        }
        // 优惠券添加
        MallCoupon coupon = goodsAllinone.getCoupon();
        if (null != coupon) {
            // 如果是兑换码类型，则这里需要生成一个兑换码
            if (coupon.getType().equals(CouponConstant.TYPE_CODE)) {
                String code = mallCouponService.generateCode();
                coupon.setCode(code);
            }
            coupon.setId(GraphaiIdGenerator.nextId("MallCoupon"));
            coupon.setStatus(new Short("1"));
            coupon.setAddTime(LocalDateTime.now());
            coupon.setUpdateTime(LocalDateTime.now());
            mallCouponMapper.insertSelective(coupon);

            if (null == coupon || null == coupon.getId()) {
                return ResponseUtil.fail(-1, "优惠券添加逻辑失败,优惠券id获取不到");
            } else {
                productCoupon.setCouponId(coupon.getId());
            }
            productCoupon.setId(GraphaiIdGenerator.nextId("MallProductCoupon"));
            productCoupon.setStatus("1");
            productCoupon.setDeleted(false);
            productCoupon.setAddTime(LocalDateTime.now());
            productCoupon.setUpdateTime(LocalDateTime.now());
            mallProductCouponMapper.insertSelective(productCoupon);
        }
        // 将生成的分享图片地址写入数据库
        String url = qCodeService.createGoodShareImage(goods.getId().toString(), goods.getPicUrl(), goods.getName());
        if (!StringUtils.isEmpty(url)) {
            goods.setShareUrl(url);
            if (goodsService.updateById(goods) == 0) {
                throw new RuntimeException("更新数据失败");
            }
        }

        // 商品规格表Mall_goods_specification
        for (MallGoodsSpecification specification : specifications) {
            specification.setGoodsId(goods.getId());
            specificationService.add(specification);
        }

        // 商品参数表Mall_goods_attribute
        for (MallGoodsAttribute attribute : attributes) {
            attribute.setGoodsId(goods.getId());
            attributeService.add(attribute);
        }

        // 商品货品表Mall_product
        for (MallGoodsProduct product : products) {
            product.setGoodsId(goods.getId());
            productService.add(product);
        }
        return ResponseUtil.ok();
    }

    /**
     * 商品上架内容 类目信息查询
     */
    public Object selectCategoryList() {
        // http://element-cn.eleme.io/#/zh-CN/component/cascader
        List<CatVo> categoryVoList = new ArrayList<>();
        // 获取所有类目
        List<MallCategory> allCategory = categoryService.getAllCategory();

        // 存放子类的集合
        List<MallCategory> subList = new ArrayList<>();

        // pidMaps 用于存放所有pid的值，用于通过 map的数据寻找下面的值
        Map<String, List<MallCategory>> pidMaps = new HashMap<>();

        for (MallCategory mallCategory : allCategory) {

            // 实现，通过id 找到id下面的所有子类目
            if (!pidMaps.containsKey(mallCategory.getPid())) {
                subList = new ArrayList<>();
            } else {
                // 获取有pid的key，进行继续追加
                if (pidMaps.get(mallCategory.getPid()) != null) {
                    subList = pidMaps.get(mallCategory.getPid());
                }
            }
            subList.add(mallCategory);
            pidMaps.put(mallCategory.getPid(), subList);
        }
        if (!pidMaps.isEmpty()) {
            getCategoryList(pidMaps, categoryVoList, null, pidMaps.get("0"));
        }

        // http://element-cn.eleme.io/#/zh-CN/component/select
        // 管理员设置“所属品牌商”
        // List<MallBrand> list3 = brandService.selectBrandList(null,null,null);
        // List<Map<String, Object>> brandList = new ArrayList<>();
        // for (MallBrand brand : list3) {
        // Map<String, Object> b = new HashMap<>(2);
        // b.put("value", brand.getId());
        // b.put("label", brand.getName());
        // brandList.add(b);
        // }

        Map<String, Object> data = new HashMap<>();
        data.put("categoryList", categoryVoList);
        // data.put("brandList", brandList);
        return data;
    }

    /**
     * 获取所有类目
     *
     * @param pidMaps key=pid value=MallCategory 第一次: level=L1 代表一级目录
     * @param finallyList 返回最终的结果list
     * @param pidCategory 存放当前父类，用于pidCategory.setChildren
     * @param categoryList 获取当前类目所有的子类
     * @return
     */
    private List<CatVo> getCategoryList(Map<String, List<MallCategory>> pidMaps, List<CatVo> finallyList,
                    CatVo pidCategory, List<MallCategory> categoryList) {
        // 引用对象，减少内存开支
        CatVo categoryVO;
        List<CatVo> childrenList = null;
        List<MallCategory> mallCategoriesList;

        for (MallCategory category : categoryList) {
            categoryVO = new CatVo();
            categoryVO.setValue(category.getId());
            categoryVO.setLabel(category.getName());

            // 获取当前类目的子类
            mallCategoriesList = pidMaps.get(categoryVO.getValue());
            if (CollectionUtils.isNotEmpty(mallCategoriesList)) {
                // 进行递归调用，返回子类的结果集，赋值给父类的children 属性
                childrenList = getCategoryList(pidMaps, new ArrayList<>(), categoryVO, mallCategoriesList);
            } else {
                childrenList = new ArrayList<>();
            }

            if (CollectionUtils.isNotEmpty(childrenList)) {
                // 添加当前类目的子类 = 空/childrenList
                categoryVO.setChildren(childrenList);
            }
            finallyList.add(categoryVO);
        }
        return finallyList;
    }


    /*
     * public Object list2() { // http://element-cn.eleme.io/#/zh-CN/component/cascader
     * List<MallCategory> allCategory = categoryService.getAllCategory();
     *
     * List<CatVo> categoryVoList = new ArrayList<>();
     *
     * List<MallCategory> list = new ArrayList<>(); List<MallCategory> list2 = new ArrayList<>();
     *
     * //map 存放level = L1的数据 Map<String, List<MallCategory>> map = new HashMap<>(); //map2
     * 用于存放所有pid的值，用于通过 map的数据寻找下面的值 Map<String, List<MallCategory>> map2 = new HashMap<>();
     *
     * for (MallCategory mallCategory : allCategory) {
     *
     * //只获取L1的类别 if (Objects.equals(mallCategory.getLevel(), "L1")) { if
     * (!map.containsKey(mallCategory.getLevel())) { list = new ArrayList<>(); } list.add(mallCategory);
     * map.put(mallCategory.getLevel(), list); }
     *
     * //实现，通过id 找到id下面的所有子类目 if (!map2.containsKey(mallCategory.getPid())) { list2 = new ArrayList<>();
     * } else { //获取有pid的key，进行继续追加 if (map2.get(mallCategory.getPid()) != null) { list2 =
     * map2.get(mallCategory.getPid()); } } list2.add(mallCategory); map2.put(mallCategory.getPid(),
     * list2); }
     *
     * //进行对象引用 CatVo categoryVO = null; CatVo subCategoryVo = null; CatVo thirdCategoryVo = null; CatVo
     * fourCategoryVo = null;
     *
     * List<CatVo> children = null; List<CatVo> thirdChildren = null; List<MallCategory>
     * thirdCategoryList = null; List<CatVo> fourChildren = null;
     *
     * //暂时只支持四级类目 //循环逻辑： 拿到所有一级类目，下面的二级类目，下面的三级类目，下面的四级类目，合并返回给前端 List<MallCategory> categoryList =
     * map.get("L1"); for (MallCategory category : categoryList) { categoryVO = new CatVo();
     * categoryVO.setValue(category.getId()); categoryVO.setLabel(category.getName());
     *
     * children = new ArrayList<>();
     *
     * List<MallCategory> subCategoryList = map2.get(categoryVO.getValue()); if
     * (CollectionUtils.isEmpty(subCategoryList)) { continue; } for (MallCategory subCategory :
     * subCategoryList) { subCategoryVo = new CatVo(); subCategoryVo.setValue(subCategory.getId());
     * subCategoryVo.setLabel(subCategory.getName());
     *
     * children.add(subCategoryVo);
     *
     *
     * thirdCategoryList = map2.get(subCategory.getId()); if
     * (CollectionUtils.isEmpty(thirdCategoryList)) { continue; }
     *
     * thirdChildren = new ArrayList<>(); for (MallCategory mallCategory : thirdCategoryList) {
     * thirdCategoryVo = new CatVo(); thirdCategoryVo.setValue(mallCategory.getId());
     * thirdCategoryVo.setLabel(mallCategory.getName());
     *
     * thirdChildren.add(thirdCategoryVo);
     *
     *
     * List<MallCategory> fourCategoryList = map2.get(thirdCategoryVo.getValue()); if
     * (CollectionUtils.isEmpty(fourCategoryList)) { continue; } fourChildren = new ArrayList<>();
     *
     * for (MallCategory mallCategory1 : fourCategoryList) { fourCategoryVo = new CatVo();
     * fourCategoryVo.setValue(mallCategory1.getId()); fourCategoryVo.setLabel(mallCategory1.getName());
     *
     * fourChildren.add(fourCategoryVo); }
     *
     * thirdCategoryVo.setChildren(fourChildren);
     *
     * } subCategoryVo.setChildren(thirdChildren);
     *
     * }
     *
     * categoryVO.setChildren(children); categoryVoList.add(categoryVO); }
     *
     *
     * // http://element-cn.eleme.io/#/zh-CN/component/select // 管理员设置“所属品牌商” List<MallBrand> list3 =
     * brandService.all(); List<Map<String, Object>> brandList = new ArrayList<>(); for (MallBrand brand
     * : list3) { Map<String, Object> b = new HashMap<>(2); b.put("value", brand.getId());
     * b.put("label", brand.getName()); brandList.add(b); }
     *
     * Map<String, Object> data = new HashMap<>(); data.put("categoryList", categoryVoList);
     * data.put("brandList", brandList); return ResponseUtil.ok(data); }
     */
    /*
     * public Object list2() { // http://element-cn.eleme.io/#/zh-CN/component/cascader // 管理员设置“所属分类”
     * List<MallCategory> l1CatList = categoryService.queryL1(null); List<CatVo> categoryList = new
     * ArrayList<>(l1CatList.size());
     *
     * for (MallCategory l1 : l1CatList) { CatVo l1CatVo = new CatVo(); l1CatVo.setValue(l1.getId());
     * l1CatVo.setLabel(l1.getName());
     *
     * List<MallCategory> l2CatList = categoryService.queryByPid(l1.getId()); List<CatVo> children = new
     * ArrayList<>(l2CatList.size()); for (MallCategory l2 : l2CatList) { CatVo l2CatVo = new CatVo();
     * l2CatVo.setValue(l2.getId()); l2CatVo.setLabel(l2.getName()); children.add(l2CatVo); }
     * l1CatVo.setChildren(children);
     *
     * categoryList.add(l1CatVo); }
     *
     * // http://element-cn.eleme.io/#/zh-CN/component/select // 管理员设置“所属品牌商” List<MallBrand> list =
     * brandService.all(); List<Map<String, Object>> brandList = new ArrayList<>(l1CatList.size()); for
     * (MallBrand brand : list) { Map<String, Object> b = new HashMap<>(2); b.put("value",
     * brand.getId()); b.put("label", brand.getName()); brandList.add(b); }
     *
     * Map<String, Object> data = new HashMap<>(); data.put("categoryList", categoryList);
     * data.put("brandList", brandList); return ResponseUtil.ok(data); }
     */

    @Autowired
    private IMallGoodsCategoryService mallGoodsCategoryService;
    @Autowired
    private IMallCategoryService mallCategoryService;

    public Object detail(String id) {
        MallGoods goods = goodsService.findById(id);
        List<MallGoodsProduct> products = productService.queryByGid(id);
        List<MallGoodsSpecification> specifications = specificationService.queryByGid(id);
        List<MallGoodsAttribute> attributes = attributeService.queryByGid(id);

        MallProductCouponExample example = new MallProductCouponExample();
        example.or().andGoodsIdEqualTo(goods.getId()).andDeletedEqualTo(false);
        List<MallProductCoupon> mallProductCouponList = mallProductCouponMapper.selectByExample(example);
        MallCoupon coupon = new MallCoupon();
        if (mallProductCouponList.size() > 0) {
            coupon = mallCouponService.findById(mallProductCouponList.get(0).getCouponId());
        }
        // 类目
//        String categoryId = goods.getCategoryId();
//        MallCategory category = categoryService.findById(categoryId);

        QueryWrapper<MallGoodsCategory> que = new QueryWrapper<>();
        que.eq("goods_id",goods.getId());
        List<MallGoodsCategory> list = mallGoodsCategoryService.list(que);

        List<List> categoryIds = new ArrayList<>();
        if (ObjectUtil.isNotNull(list) && ObjectUtil.isNotEmpty(list)) {
            for(MallGoodsCategory item : list) {
                List<String> ca = new ArrayList<>();
                IMallCategory three = mallCategoryService.getById(item.getCategoryId());
                IMallCategory two = mallCategoryService.getById(three.getPid());
                ca.add(two.getPid());
                ca.add(two.getId());
                ca.add(three.getId());
                categoryIds.add(ca);
            }
        }
        // 品牌商
        String brandId = goods.getBrandId();
        MallBrand brand = null;
        if (!StringUtils.isEmpty(brandId)) {
            brand = brandService.findById(brandId);
        }

        List<Integer> placement = new ArrayList<Integer>();
        QueryWrapper<MallGoodsPosition> gpWrapper = new QueryWrapper<>();
        gpWrapper.eq("goods_id",goods.getId());
        List<MallGoodsPosition> gplist = goodsPositionService.list(gpWrapper);
        for (int i = 0; i<gplist.size(); i++) {
            placement.add(gplist.get(i).getType());
        }
        QueryWrapper<MallUserProfitNew> upnWrapper = new QueryWrapper<>();
        upnWrapper.eq("goods_Id", goods.getId());
        //.eq("profit_type",goods.getVirtualGood())
        MallUserProfitNew userProfitNew = userProfitNewService.getOne(upnWrapper);

        List<MallGrouponRules> grouponRulesList = rulesService.queryByGoodsId(goods.getId());
        Map<String, Object> data = new HashMap<>();
        if (grouponRulesList.size()>0){
            data.put("grouponRules",grouponRulesList.get(0));
        }else{
            data.put("grouponRules",null);
        }
        data.put("goods", goods);
        data.put("specifications", specifications);
        data.put("products", products);
        data.put("attributes", attributes);
        data.put("categoryIds", categoryIds);
        data.put("coupon", coupon);
        data.put("brand", brand);
        data.put("placement", placement);
        data.put("userProfitNew", userProfitNew);
        return ResponseUtil.ok(data);
    }

    public Object disableGoods(MallGoods goods) {
        // 商品基本信息表Mall_goods
        if (goodsService.updateById(goods) == 0) {
            throw new RuntimeException("更新数据失败");
        }
        // 如果活动商品存在商品则清除
        // 直播间商品
        QueryWrapper<MallLiveRoomProduct> mallLiveRoomProductQueryWrapper = new QueryWrapper<>();
        mallLiveRoomProductQueryWrapper.eq("goods_id", goods.getId());
        mallLiveRoomProductService.remove(mallLiveRoomProductQueryWrapper);

        // 活动关联产品规格
        QueryWrapper<MallActivityGoodsProduct> mallActivityGoodsProductQueryWrapper = new QueryWrapper<>();
        mallActivityGoodsProductQueryWrapper.eq("goods_id", goods.getId());
        mallActivityGoodsProductService.remove(mallActivityGoodsProductQueryWrapper);
        return ResponseUtil.ok();
    }
}
