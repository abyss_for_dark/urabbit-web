package com.graphai.mall.wx.web.canteen;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.generator.controller.IBaseController;
import com.graphai.id.GraphaiIdGenerator;
import com.graphai.mall.admin.service.IMallUserMerchantService;
import com.graphai.mall.canteen.domain.*;
import com.graphai.mall.canteen.form.AppCanteenCartForm;
import com.graphai.mall.canteen.service.*;
import com.graphai.mall.db.vo.KndCanteenFoodSpecificationVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 小程序-堂食
 * app/小程序-餐饮---c端用户专用 -----cyf
 *
 * @author Administrator
 */
@RestController
@RequestMapping("/wx/c/canteen/")
public class AppCanteenV3Controller {

    protected final Logger log = LoggerFactory.getLogger(AppCanteenV3Controller.class);

//    @Autowired
//    private DoPayService doPayServiceUtil;
    @Autowired
    private IKndCanteenTableService kndCanteenTableService;
    @Autowired
    private IKndCanteenFoodService kndCanteenFoodService;
    @Autowired
    private IKndCanteenCategoryService kndCanteenCategoryService;
    @Autowired
    private IKndCanteenFoodSpecificationService kndCanteenFoodSpecificationService;
    @Autowired
    private IKndCanteenFoodProductService kndCanteenFoodProductService;
    @Autowired
    private IKndCanteenCartService kndCanteenCartService;
    @Autowired
    private IKndCanteenOrderService kndCanteenOrderService;
    @Autowired
    private IKndCanteenOrderFoodService kndCanteenOrderFoodService;

//    @Autowired
//    private IKndOrderLogService kndOrderLogService;
//    @Autowired
//    private IKndUserDeliveryAddressService kndUserDeliveryAddressService;
    @Autowired
    private IMallUserMerchantService userMerchantService;

    /** ------------------------------ 菜品列表 （KndCanteenFood） ------------------------------ **/
    /**
     * 查询菜品分类列表、商户名称、店铺名称
     */
    @CrossOrigin
    @PostMapping("/selectCanteenCategoryList")
    public Object selectCanteenCategoryList(@RequestParam(value = "merchantId") String merchantId,
                                            @RequestParam(value = "tableId", required = false) String tableId, @RequestParam(value = "storeId", required = false) String storeId) {
        if (merchantId == null) {
            return ResponseUtil.fail("未获取到商户Id！");
        }

        //通过桌号查询 租户信息
        KndCanteenTable kndCanteenTable = kndCanteenTableService.selectKndCanteenTableById(tableId);
        if (ObjectUtil.isNotEmpty(kndCanteenTable)) {
            storeId = kndCanteenTable.getStoreId();
            merchantId = kndCanteenTable.getMerchantId();
            if (StrUtil.isEmpty(kndCanteenTable.getStoreId()) || StrUtil.isEmpty(kndCanteenTable.getMerchantId())) {
                return ResponseUtil.fail("该桌台码暂未绑定，请绑定后重新扫码！");
            }

        }
        KndCanteenCategory kndCanteenCategory = new KndCanteenCategory();
        kndCanteenCategory.setMerchantId(merchantId);
        kndCanteenCategory.setStoreId(storeId);
        List<KndCanteenCategory> kndCanteenCategoryList = kndCanteenCategoryService.getKndCanteenCategoryList(kndCanteenCategory);  //分类信息
        Map<String,Object> map = new HashMap<>();

        map.put("storeId", storeId);
        if (ObjectUtil.isNotEmpty(kndCanteenTable)) {
            map.put("tableSn", kndCanteenTable.getTableSn());
        } else {
            map.put("tableSn", "");
        }
        map.put("kndCanteenCategoryList", kndCanteenCategoryList);
        return ResponseUtil.ok("操作成功！", map);
    }

    /**
     * 查询菜品列表
     */
    @PostMapping("/selectCanteenFoodList")
    public Object selectCanteenFoodList(
            @RequestParam(value = "merchantId") String merchantId,
            @RequestParam(value = "storeId") String storeId,
            String categoryId,
            String foodName) {
        if (merchantId == null) {
            return ResponseUtil.fail("未获取到商户Id！");
        }
        if (storeId == null) {
            return ResponseUtil.fail("未获取到店铺Id！");
        }

        KndCanteenFood kndCanteenFood = new KndCanteenFood();
        kndCanteenFood.setMerchantId(merchantId);
        kndCanteenFood.setStoreId(storeId);
        kndCanteenFood.setIsOnSale(0);
        if (StrUtil.isNotEmpty(categoryId)) {
            kndCanteenFood.setCategoryId(categoryId);
        }
        if (StrUtil.isNotEmpty(foodName)) {
            kndCanteenFood.setFoodName(foodName);
        }
        List<KndCanteenFood> kndCanteenFoodList = kndCanteenFoodService.selectKndCanteenFoodList(kndCanteenFood);
        return ResponseUtil.fail("操作成功！", kndCanteenFoodList);
    }

    /**
     * 选择菜品产品
     */
    @CrossOrigin
    @PostMapping("/selectCanteenFoodProductList")
    public Object selectCanteenFoodProductList(@RequestParam(value = "foodId") String foodId) {
        if (StrUtil.isEmpty(foodId)) {
            return ResponseUtil.fail("未获取到菜品Id！");
        }
        //查询菜品规格list
        KndCanteenFoodSpecification kndCanteenFoodSpecification = new KndCanteenFoodSpecification();
        kndCanteenFoodSpecification.setFoodId(foodId);
        kndCanteenFoodSpecification.setDeleted(0);
        List<KndCanteenFoodSpecification> canteenFoodSpecificationList = kndCanteenFoodSpecificationService.selectKndCanteenFoodSpecificationList(kndCanteenFoodSpecification);
        List<KndCanteenFoodSpecificationVo> kndCanteenFoodSpecificationList = new ArrayList<>();
        if (canteenFoodSpecificationList.size() > 0) {
            //同 Specification的菜品产品分类
            Map<String, List<KndCanteenFoodSpecification>> collectList = canteenFoodSpecificationList.stream().collect(Collectors.groupingBy(KndCanteenFoodSpecification::getSpecification));
            //获取所有 Specification
            List<String> tableNames = canteenFoodSpecificationList.stream().map(KndCanteenFoodSpecification::getSpecification).collect(Collectors.toList());
            //同 Specification放入一个list
            List<String> ketList = tableNames.stream().distinct().collect(Collectors.toList());
            //将 Specification放入list中重组
            for (int i = 0; i < ketList.size(); i++) {
                KndCanteenFoodSpecificationVo kndCanteenFoodSpecificationVo = new KndCanteenFoodSpecificationVo();
                kndCanteenFoodSpecificationVo.setName(ketList.get(i));
                kndCanteenFoodSpecificationVo.setValueList(collectList.get(ketList.get(i)));
                kndCanteenFoodSpecificationList.add(kndCanteenFoodSpecificationVo);
            }
        }

        //菜品规格
        KndCanteenFoodProduct kndCanteenFoodProduct = new KndCanteenFoodProduct();
        kndCanteenFoodProduct.setFoodId(foodId);
        kndCanteenFoodProduct.setDeleted(0);
        List<KndCanteenFoodProduct> kndCanteenFoodProductList = kndCanteenFoodProductService.selectKndCanteenFoodProductList(kndCanteenFoodProduct);

        KndCanteenFood kndCanteenFood = kndCanteenFoodService.selectKndCanteenFoodById(foodId);

        Map map = new HashMap();
        map.put("kndCanteenFoodSpecificationList", kndCanteenFoodSpecificationList);
        map.put("kndCanteenFoodProductList", kndCanteenFoodProductList);
        map.put("packPrice", kndCanteenFood.getPackPrice());
        return ResponseUtil.ok("操作成功！", map);
    }


    /** ------------------------------ 购物车 （KndCanteenCart） ------------------------------ **/
    /**
     * 查询用户购物车的菜品信息
     */
    @PostMapping("/selectCanteenCart")
    public Object selectCanteenCart(@RequestParam("merchantId") String merchantId,
                                        @RequestParam("userOpenId") String userOpenId,
                                        @RequestParam(value = "deliveryAddressId", required = false) String deliveryAddressId) {
        if (merchantId == null) {
            return ResponseUtil.fail("未获取到商户Id！");
        }
        if (StrUtil.isEmpty(userOpenId)) {
            return ResponseUtil.fail("未获取到用户openId！");
        }
        KndCanteenCart kndCanteenCart = new KndCanteenCart();
        kndCanteenCart.setMerchantId(merchantId);
        kndCanteenCart.setUserOpenId(userOpenId);
        List<KndCanteenCart> kndCanteenCartList = kndCanteenCartService.selectKndCanteenCartListByUser(kndCanteenCart);
        //返回购物车的菜品产品信息是否失效
        if (kndCanteenCartList.size() > 0) {
            for (int i = 0; i < kndCanteenCartList.size(); i++) {
                if (StrUtil.isNotEmpty(kndCanteenCartList.get(i).getFoodProductId())) {
                    kndCanteenCartList.get(i).setInvalid(false);
                } else {
                    kndCanteenCartList.get(i).setInvalid(true);
                }
            }
        }
        //暂定配送费和包装费0元
        return ResponseUtil.ok("操作成功！", kndCanteenCartList);
    }

    /**
     * 添加菜品到购物车
     */
    @PostMapping("/updateCanteenCart")
    public Object updateCanteenCart(@RequestBody @Validated AppCanteenCartForm appCanteenCartForm) {
        List<KndCanteenFoodProduct> foodProductList = appCanteenCartForm.getFoodProductList();
        if (foodProductList.size() == 0) {
            return ResponseUtil.fail("请选择菜品！");
        }

        //循环菜品产品信息
        for (int i = 0; i < foodProductList.size(); i++) {
            //判断购物车里面是否已经存在 指定prodtcuId 的菜品产品信息
            KndCanteenFoodProduct kndCanteenFoodProduct = foodProductList.get(i);
            //判断菜品是否下架
            KndCanteenFoodProduct foodProduct = kndCanteenFoodProductService.selectKndCanteenFoodProductById(kndCanteenFoodProduct.getId());    //菜品产品
            if (ObjectUtil.isEmpty(foodProduct)) {
                return ResponseUtil.fail("存在下架菜品，请先清空下架菜品！");
            }
            KndCanteenFood food = kndCanteenFoodService.selectKndCanteenFoodById(foodProduct.getFoodId());      //菜品

            //若购物车已存在菜品产品信息 则修改，若不存在则添加
            KndCanteenCart cart = new KndCanteenCart();
            cart.setMerchantId(appCanteenCartForm.getMerchantId());
            cart.setStoreId(food.getStoreId());
            cart.setUserOpenId(appCanteenCartForm.getUserOpenId());
            cart.setProductId(foodProduct.getId());     //菜品产品id主键
            List<KndCanteenCart> kndCanteenCartList = kndCanteenCartService.selectKndCanteenCartList(cart);
            if (kndCanteenCartList.size() == 0) {
                if (kndCanteenFoodProduct.getNumber() > 0) {
                    KndCanteenCart kndCanteenCart = new KndCanteenCart();
                    kndCanteenCart.setMerchantId(appCanteenCartForm.getMerchantId());
                    kndCanteenCart.setStoreId(food.getStoreId());
                    kndCanteenCart.setUserOpenId(appCanteenCartForm.getUserOpenId());
                    kndCanteenCart.setUserName(appCanteenCartForm.getUserName());

                    kndCanteenCart.setFoodId(foodProduct.getFoodId());
                    kndCanteenCart.setFoodName(food.getFoodName());
                    kndCanteenCart.setProductId(foodProduct.getId());
                    kndCanteenCart.setPrice(foodProduct.getPrice());
                    kndCanteenCart.setNumber(kndCanteenFoodProduct.getNumber());
                    kndCanteenCart.setSpecifications(foodProduct.getSpecifications());
                    kndCanteenCart.setPicUrl(food.getPicUrl());

                    kndCanteenCartService.insertKndCanteenCart(kndCanteenCart);
                }
            } else {
                if (kndCanteenCartList.size() > 1) {
                    return ResponseUtil.fail("购物车添加失败，找到多条数据！");
                }
                KndCanteenCart kndCanteenCart = kndCanteenCartList.get(0);

                if (kndCanteenFoodProduct.getNumber() != 0) {
                    KndCanteenCart cartUpdate = new KndCanteenCart();
                    cartUpdate.setId(kndCanteenCart.getId());
                    cartUpdate.setNumber(kndCanteenFoodProduct.getNumber());
                    kndCanteenCartService.updateKndCanteenCart(cartUpdate);
                } else {
                    //删除购物车中的 数量为 0的菜品信息
                    kndCanteenCartService.deleteKndCanteenCartById(kndCanteenCart.getId());
                }
            }

        }

        return ResponseUtil.ok("操作成功！");
    }

    /**
     * 清空购物车
     */
    @CrossOrigin
    @PostMapping("/clearCanteenCart")
    public Object clearCanteenCart(@RequestParam("ids") String ids) {
        return kndCanteenCartService.deleteKndCanteenCartByIds(ids) > 0 ? ResponseUtil.ok() : ResponseUtil.fail();
    }

    /**
     * 清空购物车
     */
    @PostMapping("/newClearCanteenCart")
    public Object v2clearCanteenCart(@RequestParam("userOpenId") String userOpenId) {
        KndCanteenCart kndCanteenCart = new KndCanteenCart();
        kndCanteenCart.setUserOpenId(userOpenId);
        List<KndCanteenCart> kndCanteenCartList = kndCanteenCartService.selectKndCanteenCartList(kndCanteenCart);
        for (KndCanteenCart canteenCart : kndCanteenCartList) {
            kndCanteenCartService.deleteKndCanteenCartById(canteenCart.getId());
        }
        return ResponseUtil.ok();
    }

    /** ------------------------------ 菜品订单 （KndCanteenOrder） ------------------------------ **/
    /**
     * 查询订单信息
     */
    @PostMapping("/selectCanteenOrderList")
    public Object selectCanteenOrderList(@RequestParam("userOpenId") String userOpenId) {
        if (StrUtil.isEmpty(userOpenId)) {
            return ResponseUtil.fail("未获取到用户openId！");
        }
        KndCanteenOrder kndCanteenOrder = new KndCanteenOrder();
        kndCanteenOrder.setUserOpenId(userOpenId);
        List<KndCanteenOrder> kndCanteenOrderList = kndCanteenOrderService.selectKndCanteenOrderListInfo(kndCanteenOrder);
        return ResponseUtil.ok("操作成功！", kndCanteenOrderList);
    }

    /**
     * 提交购物车里面的订单（添加 knd_canteen_order、knd_canteen_order_food 表数据），同时删除购物车里面的数据(删除knd_canteen_cart表数据)
     */
    @Transactional
    @PostMapping("/submitCanteenOrder")
    public Object submitCanteenOrder(@RequestBody @Validated AppCanteenCartForm appCanteenCartForm) {
      /*  if (StringUtils.isEmpty(appCanteenCartForm.getTableId())) {
            return error("未获取到桌台号！");
        }*/
        if (appCanteenCartForm.getPeopleNumber() == null) {
            appCanteenCartForm.setPeopleNumber(1);
        }
        KndCanteenOrder kndCanteenOrder = new KndCanteenOrder();
        kndCanteenOrder.setIsShip(appCanteenCartForm.getIsShip());

        BigDecimal peisongPrice = new BigDecimal("0");//外卖撇送费
        //外卖配送 判断
//        if (null != appCanteenCartForm.getIsShip() && appCanteenCartForm.getIsShip() == 2) {
//            if (null == appCanteenCartForm.getDeliveryAddressId()) {
//                return  ResponseUtil.fail("请填写收货地址");
//            }
//            KndUserDeliveryAddress kndUserDeliveryAddress = kndUserDeliveryAddressService.selectKndUserDeliveryAddressById((appCanteenCartForm.getDeliveryAddressId()));
//            kndCanteenOrder.setConsigneeMobile(kndUserDeliveryAddress.getReceiverPhone());
//            kndCanteenOrder.setConsigneeAddress(kndUserDeliveryAddress.getLocationAddress() + kndUserDeliveryAddress.getReceiverAddress());
//            kndCanteenOrder.setCity(kndUserDeliveryAddress.getCity());
//            kndCanteenOrder.setLatitude(kndUserDeliveryAddress.getLatitude());
//            kndCanteenOrder.setLongitude(kndUserDeliveryAddress.getLongitude());
//            String receiverName = kndUserDeliveryAddress.getReceiverName();
//            if (kndUserDeliveryAddress.getGender() != null) {
//                receiverName += kndUserDeliveryAddress.getGender().equals("0") ? "(女士)" : "(先生)";
//            }
//            kndCanteenOrder.setConsignee(receiverName);
//
//            kndCanteenOrder.setMtpsPrice(meituanPeisongopenUtil.getDeliveryPrice(appCanteenCartForm.getDeliveryAddressId(), appCanteenCartForm.getStoreId()));
//            peisongPrice = kndMerchant.getCustomDeliveryFee();
//            if (kndMerchant.getDeliveryFeePayer() != null) {
//                if (kndMerchant.getDeliveryFeePayer() == 1) {
//                    peisongPrice = new BigDecimal("0");
//                }
//                if (kndMerchant.getDeliveryFeePayer() == 0) {
//                    if (peisongPrice == null || peisongPrice.equals(new BigDecimal(0))) {
//                        peisongPrice = kndCanteenOrder.getMtpsPrice();
//                    }
//                }
//            }
//            kndCanteenOrder.setPeisongPrice(peisongPrice);
//        }
        //计算配送费

        BigDecimal tableFee = new BigDecimal(0);//餐位费
        //计算菜品总费用
        List<KndCanteenFoodProduct> foodProductList = appCanteenCartForm.getFoodProductList();
        if (foodProductList.size() == 0) {
            return ResponseUtil.fail("请选择菜品！");
        }
        BigDecimal totalMoney = new BigDecimal(0).add(peisongPrice);
        BigDecimal totalPackPrice = new BigDecimal(0);
        for (int i = 0; i < foodProductList.size(); i++) {
            KndCanteenFoodProduct kndCanteenFoodProduct = foodProductList.get(i);

            KndCanteenFoodProduct foodProduct = kndCanteenFoodProductService.selectKndCanteenFoodProductById(kndCanteenFoodProduct.getId());    //菜品产品
            if (ObjectUtil.isEmpty(foodProduct)) {
                return ResponseUtil.fail("存在下架菜品，请先清空下架菜品！");
            }
            KndCanteenFood food = kndCanteenFoodService.selectKndCanteenFoodById(foodProduct.getFoodId());  //菜品
            //判断菜品产品数量 是否大于 菜品剩余数量, -1代表无限库存
            if (food.getSurplusNumber() != null && food.getSurplusNumber() != -1 && kndCanteenFoodProduct.getNumber() > food.getSurplusNumber()) {
                return ResponseUtil.fail("请选择正确的数量！");
            }
            if (foodProduct.getNumber() != null && foodProduct.getNumber() != -1 && kndCanteenFoodProduct.getNumber() > foodProduct.getNumber()) {
                return ResponseUtil.fail("请选择正确的数量！");
            }
            BigDecimal packPrice = food.getPackPrice().multiply(new BigDecimal(kndCanteenFoodProduct.getNumber()));
            totalPackPrice = totalPackPrice.add(packPrice);
            totalMoney = totalMoney.add(foodProduct.getPrice().multiply(new BigDecimal(kndCanteenFoodProduct.getNumber())));
        }

        kndCanteenOrder.setId(GraphaiIdGenerator.nextId("kndCanteenOrder"));
        kndCanteenOrder.setMerchantId(appCanteenCartForm.getMerchantId());
        kndCanteenOrder.setUserOpenId(appCanteenCartForm.getUserOpenId());
        //kndCanteenOrder.setUserName(appCanteenCartForm.getUserName());
        kndCanteenOrder.setTableId(appCanteenCartForm.getTableId());

        //通过桌台id查询 店铺id
        KndCanteenTable kndCanteenTable = kndCanteenTableService.selectKndCanteenTableById(appCanteenCartForm.getTableId());
        String storeId = appCanteenCartForm.getStoreId();
        if (ObjectUtil.isNotEmpty(kndCanteenTable)) {
            tableFee = new BigDecimal(appCanteenCartForm.getPeopleNumber()).multiply(kndCanteenTable.getTableFee());
            storeId = kndCanteenTable.getStoreId();
        }
        if (storeId == null) {
            return ResponseUtil.fail("没有找到门店");
        }
        kndCanteenOrder.setStoreId(storeId);
        //生成8位 订单编码orderSn
        String orderSn = kndCanteenOrderService.selectUniqueNumber();
        kndCanteenOrder.setOrderSn(orderSn);
        kndCanteenOrder.setOrderStatus(0);  //订单状态为 未支付
        kndCanteenOrder.setMessage(StrUtil.isNotEmpty(appCanteenCartForm.getMessage()) ? appCanteenCartForm.getMessage() : "用户提交订单！");
        kndCanteenOrder.setPeopleNumber(appCanteenCartForm.getPeopleNumber());
        //todo 金额方面 暂时不计算 配送费用，所以未支付订单目前只存在 菜品总费用

        //todo 订单总金额=（菜品产品金额*数量）+（用餐人数*单个人数的茶位费） +  配送费 + 包装费
        BigDecimal totalPrice;
        if (null != appCanteenCartForm.getIsShip() && appCanteenCartForm.getIsShip() == 0) {
            totalPrice = totalMoney.add(tableFee);
        } else {
            totalPrice = totalMoney.add(tableFee).add(totalPackPrice);
        }
        kndCanteenOrder.setFoodPrice(totalMoney);
        kndCanteenOrder.setFreightPrice(tableFee);
//        kndCanteenOrder.setCouponPrice(new BigDecimal(0.00));
        kndCanteenOrder.setIntegralPrice(new BigDecimal(0));
        kndCanteenOrder.setOrderPrice(totalPrice);
        kndCanteenOrder.setPackPrice(totalPackPrice);
//        kndCanteenOrder.setActualPrice(totalMoney);

        //创建未支付订单
        kndCanteenOrderService.insertKndCanteenOrder(kndCanteenOrder);
        //添加 knd_canteen_order_food 表数据
        for (int i = 0; i < foodProductList.size(); i++) {
            KndCanteenFoodProduct kndCanteenFoodProduct = foodProductList.get(i);

            KndCanteenFoodProduct foodProduct = kndCanteenFoodProductService.selectKndCanteenFoodProductById(kndCanteenFoodProduct.getId());    //菜品产品
            if (ObjectUtil.isEmpty(foodProduct)) {
                return ResponseUtil.fail("未查询到上架的菜品产品信息！");
            }
            KndCanteenFood food = kndCanteenFoodService.selectKndCanteenFoodById(foodProduct.getFoodId());  //菜品
            if (ObjectUtil.isEmpty(food)) {
                return ResponseUtil.fail("未查询到菜品信息！");
            }
            //添加数据
            KndCanteenOrderFood kndCanteenOrderFood = new KndCanteenOrderFood();
            kndCanteenOrderFood.setMerchantId(foodProduct.getMerchantId());
            kndCanteenOrderFood.setStoreId(storeId);
            kndCanteenOrderFood.setOrderId(kndCanteenOrder.getId());
            kndCanteenOrderFood.setFoodId(foodProduct.getFoodId());
            kndCanteenOrderFood.setFoodSn(food.getFoodSn());
            kndCanteenOrderFood.setProductId(foodProduct.getId());
            kndCanteenOrderFood.setNumber(kndCanteenFoodProduct.getNumber());
            kndCanteenOrderFood.setPrice(foodProduct.getPrice());
            kndCanteenOrderFood.setSpecifications(foodProduct.getSpecifications());
            kndCanteenOrderFood.setPicUrl(food.getPicUrl());
            kndCanteenOrderFoodService.insertKndCanteenOrderFood(kndCanteenOrderFood);

            //创建未支付订单之后，菜品数量减少, 有些餐饮项目没有总库存的概念, 有规格库存概念
            // -1 代表无限库存
            if (food.getSurplusNumber() != null && food.getSurplusNumber() != -1) {
                KndCanteenFood foodUpdate = new KndCanteenFood();
                foodUpdate.setId(food.getId());
                foodUpdate.setSurplusNumber(food.getSurplusNumber() - kndCanteenFoodProduct.getNumber());   //菜品剩余数量减少
                kndCanteenFoodService.updateKndCanteenFood(foodUpdate);
            }
            if (kndCanteenFoodProduct.getNumber() != null && foodProduct.getNumber() != null && foodProduct.getNumber() != -1) {
                KndCanteenFoodProduct foodProduct1 = new KndCanteenFoodProduct();
                foodProduct1.setId(kndCanteenFoodProduct.getId());
                foodProduct1.setNumber(foodProduct.getNumber() - kndCanteenFoodProduct.getNumber());
                kndCanteenFoodProductService.updateKndCanteenFoodProduct(foodProduct1);
            }

            //todo 删除购物车里面的商品信息
            KndCanteenCart kndCanteenCart = new KndCanteenCart();
            kndCanteenCart.setMerchantId(appCanteenCartForm.getMerchantId());
            kndCanteenCart.setUserOpenId(appCanteenCartForm.getUserOpenId());
            kndCanteenCart.setProductId(foodProduct.getId());
            List<KndCanteenCart> kndCanteenCartList = kndCanteenCartService.selectKndCanteenCartList(kndCanteenCart);
            if (kndCanteenCartList.size() > 0) {
                KndCanteenCart cart = kndCanteenCartList.get(0);
                kndCanteenCartService.deleteKndCanteenCartById(cart.getId());
            }
        }
        //提交订单之后，返回前端 订单id，后续操作可支付订单、取消
        Map map = new HashMap();
        map.put("orderId", kndCanteenOrder.getId());
        map.put("orderPrice", kndCanteenOrder.getOrderPrice());
        map.put("peisongPrice", peisongPrice);
        map.put("packPrice", totalPackPrice);
        map.put("peisongTime", DateUtil.offsetMinute(new Date(), 60));
        return ResponseUtil.ok("操作成功！", map);

    }


    /**
     * 计算订单的支付金额、优惠金额(立即支付)——返回前端显示
     */


    /**
     * (1)修改订单状态: 立即支付          ——订单状态（0:未支付 1:已支付 2:已取消 3:已完成）
     */
/*    @PostMapping("/payCanteenOrder")
    @Transactional(rollbackFor = Exception.class)
    public Object payCanteenOrder(@RequestParam("orderId") String orderId,
                                      @RequestParam("type") Integer type,
                                      @RequestParam(value = "mobile", required = false) String mobile,
                                      HttpServletRequest request) {
        try {
            if (StrUtil.isEmpty(mobile)) {
                mobile = "";
            }
            if (StrUtil.isEmpty(orderId)) {
                return ResponseUtil.fail("未获取到订单号！");
            }
            if (type == null) {
                return ResponseUtil.fail("请选择支付方式！");
            }
            //判断是否存在订单
            log.info("------------订单：立即支付-----------------：{}", orderId);
            KndCanteenOrder order = kndCanteenOrderService.selectKndCanteenOrderById(orderId);
            if (ObjectUtil.isEmpty(order)) {
                return ResponseUtil.fail("未查询到订单信息！");
            }
            if (order.getOrderStatus() != 0) {
                return ResponseUtil.fail("订单状态错误！");
            }

            //todo 创建支付订单
            String userOpenId = order.getUserOpenId();  //用户openId
            BigDecimal foodPrice = order.getOrderPrice();    //实际支付金额
            String merchantId = order.getMerchantId();
            String storeId = order.getStoreId();   //店铺id

            //支付数据
            String orderIdByUUId = null;
            AjaxResult ajaxResult = null;
            Map<String, Object> mp = new HashMap<>();
            String userId = null;
            String ipAddr = IpUtils.getIpAddr(request);
            String qrCode = merPayInfoBo.getPayCode();

            if (type == 1) {
                log.info("微信支付(type=1)——创建未支付订单");
//            String opendId = wxPayServicesUtil.getUserOpendId(userOpenId);

                //微信支付(type=1)——创建未支付订单
                //String ipAddr = IpUtils.getIpAddr(request);
                Integer status = merPayInfoBo.getStatus();
                String linkType = merPayInfoBo.getLinkType();
                String payCode = merPayInfoBo.getPayCode();

                //Map<String, Object> mp = new HashMap<>();

                if (Constants.KND_MER_STATUS_NO.equals(status)) {
                    return AjaxResult.warn("该商户已下架");
                }
                //MerchantMchIdBo merchantMchIdBo = kndMerchantService.getPayInfoByMerId(merchantId);
                mp.put("mchId", merchantMchIdBo.getMchId());
                mp.put("userId", userOpenId);
                mp.put("payType", WX_PAY);
                mp.put("sysUserId", merPayInfoBo.getSysUserId());
                mp.put("mobile", mobile);
                merPayInfoBo.setStoreId(storeId);
                //创建未支付订单
                AjaxResult result = wxMiniPayServicesUtil.getPayAjaxResult(String.valueOf(foodPrice), String.valueOf(merchantId),
                        merPayInfoBo, linkType, Constants.WX_ROAL, merchantMchIdBo.getFeesRate(), ipAddr, mp, null,
                        "餐饮订单支付", Constants.ORDER_TYPE_CANTEEN, Constants.ORDER_TYPE_RECEIVE_H5, Constants.ORDER_TERMINAL_TYPE_APP, orderId);
                //判断是否创建成功 未支付订单
                if (ObjectUtil.isNotEmpty(result) && (Integer) result.get("code") == 0) {
                    Map<String, String> map = (Map<String, String>) result.get("data");
                    log.info("map：{}", map);
                    String orderCode = map.get("orderCode");
                    //更新垂直方案订单状态
                    doPayServiceUtil.bindOrder(1, orderId, orderCode);
                }
                return result;
            } else if (3 == type) {//支付宝支付
                log.info("支付宝支付");
                KndMerchantAli kndMerchantAli = kndMerchantAliService.selectKndMerchantAliByMerId(merchantId);
                if (null == kndMerchantAli || Constants.KND_MER_PASS != kndMerchantAli.getCheckStatus()) {
                    return AjaxResult.warn("该商户尚未开通支付宝支付,请使用其他方式付款");
                }
                orderIdByUUId = sysConfigService.selectConfigByKey(Constants.SYS_PLATFORM_COE) + "a0" + OrderUtils.getOrderIdByUUId();

                Map<String, Object> paymap = new HashMap<>();
            *//*AjaxResult ajaxResult = null;
            String userId = null;*//*
                //用户付款app缩写 ali(支付宝付款) wx(微信付款)
                String appPayStr = "ali";


                // 操作
                String agentId = merPayInfoBo.getAgentId();
                String assistantId = merPayInfoBo.getAssistantId();
                String agentSellId = merPayInfoBo.getAgentSellId();
                String linkType = merPayInfoBo.getLinkType();
                BigDecimal feesRate = null;
                if ("0".equals(linkType)) {
                    BigDecimal agBaseRate = new BigDecimal("0.00");
                    KndClientFee kndClientFeeAg = kndClientFeeService.selectKndClientFeeByClientIdAndType(agentId, RoleEnum.AGENT.getUserType(), linkType, appPayStr);
                    if (null != kndClientFeeAg && null != kndClientFeeAg.getFee()) {
                        agBaseRate = kndClientFeeAg.getFee();
                    }
                    BigDecimal aggBaseRate = new BigDecimal("0.00");
                    KndClientFee kndClientFeeAgg = kndClientFeeService.selectKndClientFeeByClientIdAndType(agentSellId, RoleEnum.DISTRIBUTOR.getUserType(), linkType, appPayStr);
                    if (null != kndClientFeeAgg && null != kndClientFeeAgg.getFee()) {
                        agBaseRate = kndClientFeeAgg.getFee();
                    }
                    BigDecimal assBaseRate = new BigDecimal("0.00");
                    KndClientFee kndClientFeeAss = kndClientFeeService.selectKndClientFeeByClientIdAndType(assistantId, RoleEnum.ASSISTANT.getUserType(), linkType, appPayStr);
                    if (null != kndClientFeeAss && null != kndClientFeeAss.getFee()) {
                        assBaseRate = kndClientFeeAss.getFee();
                    }

                    KndClientFee kndClientFeesRate = kndClientFeeService.selectKndClientFeeByClientIdAndType(merchantId, RoleEnum.MERCHANT.getUserType(), linkType, appPayStr);
                    if (null != kndClientFeesRate && null != kndClientFeesRate.getFee()) {
                        feesRate = kndClientFeesRate.getFee();
                    }
                    merPayInfoBo.setAgBaseRate(agBaseRate);
                    merPayInfoBo.setAssBaseRate(assBaseRate);
                    merPayInfoBo.setAggBaseRate(aggBaseRate);
                } else {
                    BigDecimal agBaseRate = new BigDecimal("0.00");
                    KndClientFee kndClientFeeAg = kndClientFeeService.selectKndClientFeeByClientIdAndType(agentId, RoleEnum.AGENT.getUserType(), linkType, appPayStr);
                    if (null != kndClientFeeAg && null != kndClientFeeAg.getFee()) {
                        agBaseRate = kndClientFeeAg.getFee();
                    }
                    BigDecimal aggBaseRate = new BigDecimal("0.00");
                    KndClientFee kndClientFeeAgg = kndClientFeeService.selectKndClientFeeByClientIdAndType(agentSellId, RoleEnum.DISTRIBUTOR.getUserType(), linkType, appPayStr);
                    if (null != kndClientFeeAgg && null != kndClientFeeAgg.getFee()) {
                        agBaseRate = kndClientFeeAgg.getFee();
                    }
                    BigDecimal assBaseRate = new BigDecimal("0.00");
                    KndClientFee kndClientFeeAss = kndClientFeeService.selectKndClientFeeByClientIdAndType(assistantId, RoleEnum.ASSISTANT.getUserType(), linkType, appPayStr);
                    if (null != kndClientFeeAss && null != kndClientFeeAss.getFee()) {
                        assBaseRate = kndClientFeeAss.getFee();
                    }
                    KndClientFee kndClientFeesRate = kndClientFeeService.selectKndClientFeeByClientIdAndType(merchantId, RoleEnum.MERCHANT.getUserType(), linkType, appPayStr);
                    if (null != kndClientFeesRate && null != kndClientFeesRate.getFee()) {
                        feesRate = kndClientFeeAss.getFee();
                    }
                    merPayInfoBo.setAgBaseRate(agBaseRate);
                    merPayInfoBo.setAssBaseRate(assBaseRate);
                    merPayInfoBo.setAggBaseRate(aggBaseRate);

                }

                String agentName = merPayInfoBo.getAgentName();
                BigDecimal agBaseRate = merPayInfoBo.getAgBaseRate();
                String assistantName = merPayInfoBo.getAssistantName();
                BigDecimal assBaseRate = merPayInfoBo.getAssBaseRate();

                String agentSellName = merPayInfoBo.getAgentSellName();
                BigDecimal aggBaseRate = merPayInfoBo.getAggBaseRate();
                String merchantName = merPayInfoBo.getMerchantName();

                String sellerId = kndMerchantAli.getSellerId();
                String appAuthToken = kndMerchantAli.getAppAuthToken();

                // 优惠金额
                BigDecimal freeAmount = new BigDecimal("0.00");

                String id = doPayServiceUtil.createKndOrder(null, orderIdByUUId, storeId, agentId, agentName, agBaseRate, assistantId,
                        assistantName, assBaseRate, agentSellId, agentSellName, aggBaseRate, merchantId, merchantName, feesRate, userId, "", mobile,
                        linkType, appPayStr, foodPrice, freeAmount, null, Constants.ORDER_TYPE_CANTEEN, Constants.ORDER_TYPE_RECEIVE_H5,
                        Constants.ORDER_TERMINAL_TYPE_APP, userId);

                AjaxResult result = aliPayServicesUtil.alipayTradeCreate(orderIdByUUId, sellerId, appAuthToken, "", String.valueOf(foodPrice), userOpenId, 1);
                //更新垂直方案订单状态
                doPayServiceUtil.bindOrder(1, order.getId(), orderIdByUUId);
                return result;
            } else if (2 == type) {//头条支付

                log.info("头条支付支付");
                orderIdByUUId = sysConfigService.selectConfigByKeyOrTenantId(Constants.SYS_PLATFORM_COE, TenantParametersUtils.getTenantId()) + "t2" + OrderUtils.getOrderIdByUUId();
                String mchId = merchantMchIdBo.getMchId();
                userId = userOpenId;

                // 创建订单
                Integer status = merPayInfoBo.getStatus();
                String linkType = merPayInfoBo.getLinkType();
                if (Constants.KND_MER_STATUS_NO.equals(status)) {
                    return AjaxResult.warn("该商户已下架");
                }
                mp.put("mchId", merchantMchIdBo.getMchId());
                mp.put("userId", userOpenId);
                mp.put("payType", WX_PAY);
                mp.put("sysUserId", merPayInfoBo.getSysUserId());
                mp.put("mobile", mobile);
                merPayInfoBo.setStoreId(storeId);

                // 创建未支付订单
                String orderIdStr = wxMiniPayServicesUtil.createCYKndOrder(String.valueOf(foodPrice), String.valueOf(merchantId),
                        merPayInfoBo, linkType, Constants.WX_ROAL, merchantMchIdBo.getFeesRate(), ipAddr, mp, null,
                        "餐饮订单支付", Constants.ORDER_TYPE_CANTEEN, Constants.ORDER_TYPE_RECEIVE_H5, Constants.ORDER_TERMINAL_TYPE_APP, orderId, orderIdByUUId);
                log.info("创建未支付订单orderIdStr:{}", orderIdStr);

                // 唤醒预支付
                ajaxResult = wxMiniPayServicesUtil.wxpayTradeCreateMWEB(orderIdByUUId, mchId, qrCode, String.valueOf(foodPrice), userId, "", ipAddr, merPayInfoBo.getCashbackRate(), Constants.WEILAISHENGHUO + TenantParametersUtils.getTenantId());

                //判断是否创建成功 未支付订单
                log.info("ajaxResult：{}", ajaxResult);
                if (ObjectUtil.isNotEmpty(ajaxResult)) {
                    Map<String, String> map = (Map<String, String>) ajaxResult.get("data");
                    log.info("头条支付map：{}", map);// {mwebUrl=https://wx.tenpay.com/cgi-bin/mmpayweb-bin/checkmweb?prepay_id=wx30163737767299a196c72755cfe7820000&package=1105257472, orderInfo={"trade_time":"1617093457","wx_type":"MWEB","subject":"抖音下单","sign":"de37fae895b8e90cf758866fad0880a4","out_order_no":"22t22021033016373700443532004","wx_url":"https://wx.tenpay.com/cgi-bin/mmpayweb-bin/checkmweb?prepay_id\u003dwx30163737767299a196c72755cfe7820000\u0026package\u003d1105257472","merchant_id":"1900053467","product_code":"pay","notify_url":"https://tp-pay.snssdk.com/paycallback","body":"抖音下单","version":"2.0","uid":"111313185055","payment_type":"direct","total_amount":"1","trade_type":"H5","currency":"CNY","risk_info":"{\"ip\":\"112.94.7.29\"}","valid_time":"36000000","app_id":"800534679655","sign_type":"MD5","alipay_url":"","timestamp":"1617093457"}}
                    if (StringUtils.isNotEmpty(map)) {
                        String orderInfo = map.get("orderInfo");
                        if (StringUtils.isNotBlank(orderInfo)) {
                            cn.hutool.json.JSONObject regionObject = JSONUtil.parseObj(orderInfo);
                            String out_order_no = String.valueOf(regionObject.get("out_order_no"));
                            log.info("out_order_no：{}", out_order_no);
                            doPayServiceUtil.bindOrder(1, orderId, out_order_no);//更新垂直方案订单状态
                        }
                    }
                }
                return ajaxResult;
            }

            return error("下单失败，请稍后重试");
        } catch (Exception ex) {
            log.info("下单失败，失败原因：" + ex.getMessage(), ex);
            ex.printStackTrace();
            return error("下单失败，请稍后重试");
        }
    }*/

    /**
     * (2)修改订单状态: 取消订单          ——订单状态（0:未支付 1:已支付 2:已取消 3:已完成）
     */
    @PostMapping("/cancelCanteenOrder")
    public Object cancelCanteenOrder(@RequestParam("orderId") String orderId) {
        if (StrUtil.isEmpty(orderId)) {
            return ResponseUtil.fail("未获取到订单号！");
        }

        //判断是否存在订单
        KndCanteenOrder order = kndCanteenOrderService.selectKndCanteenOrderById(orderId);
        if (ObjectUtil.isEmpty(order)) {
            return ResponseUtil.fail("未查询到订单信息！");
        }
        if (order.getOrderStatus() != 0) {
            return ResponseUtil.fail("订单状态错误！");
        }
        //取消订单
        KndCanteenOrder kndCanteenOrder = new KndCanteenOrder();
        kndCanteenOrder.setId(orderId);
        kndCanteenOrder.setOrderStatus(2);

        //todo 如果取消订单，需将 购买数量返回到 菜品数量，目前不处理 退还菜品数量
        kndCanteenOrderService.updateKndCanteenOrder(kndCanteenOrder);
        return ResponseUtil.ok();
    }

    /**
     * (2)修改订单状态: 完成订单          ——订单状态（0:未支付 1:已支付 2:已取消 3:已完成）
     */
    @PostMapping("/finishCanteenOrder")
    public Object finishCanteenOrder(@RequestParam("orderId") String orderId) {
        if (StrUtil.isEmpty(orderId)) {
            return ResponseUtil.fail("未获取到订单号！");
        }

        //判断是否存在订单
        KndCanteenOrder order = kndCanteenOrderService.selectKndCanteenOrderById(orderId);
        if (ObjectUtil.isEmpty(order)) {
            return ResponseUtil.fail("未查询到订单信息！");
        }
        if (order.getOrderStatus() != 1) {
            return ResponseUtil.fail("订单状态错误！");
        }
        if (("TEST".equalsIgnoreCase(order.getDeliveryId()) || "MTPS".equalsIgnoreCase(order.getDeliveryId()))
                && !"302".equalsIgnoreCase(order.getPeisongStatus())) {
            return ResponseUtil.fail("美团配送确认收货失败，请确认货物是否送达！");
        }
        //取消订单
        KndCanteenOrder kndCanteenOrder = new KndCanteenOrder();
        kndCanteenOrder.setId(orderId);
        kndCanteenOrder.setOrderStatus(3);

        kndCanteenOrderService.updateKndCanteenOrder(kndCanteenOrder);
        return ResponseUtil.fail();
    }

    /**
     * 通过订单id查询 订单食品信息
     */
    @PostMapping("/selectCanteenOrderById")
    public Object selectCanteenOrderById(@RequestParam("orderId") String orderId, @RequestParam(value = "deliveryAddressId", required = false) String deliveryAddressId) {
        if (StrUtil.isEmpty(orderId)) {
            return ResponseUtil.fail("未获取到订单orderId！");
        }
        //订单信息
        KndCanteenOrder kndCanteenOrder = kndCanteenOrderService.selectKndCanteenOrderById(orderId);
        //订单食品list信息
        KndCanteenOrderFood kndCanteenOrderFood = new KndCanteenOrderFood();
        kndCanteenOrderFood.setOrderId(orderId);
        List<KndCanteenOrderFood> kndCanteenOrderFoodList = kndCanteenOrderFoodService.selectKndCanteenOrderFoodListInfo(kndCanteenOrderFood);

        Map map = new HashMap();
        String storeId = kndCanteenOrder.getStoreId();
        Map<String, Object> reqmap = new HashMap<>();
        if (kndCanteenOrder.getIsShip() == 2 && kndCanteenOrder.getOrderStatus() != 0 &&
                null != kndCanteenOrder.getPeisongStatus() && ("TEST".equalsIgnoreCase(kndCanteenOrder.getDeliveryId())
                || "MTPS".equalsIgnoreCase(kndCanteenOrder.getDeliveryId()))) {
            String longitude = kndCanteenOrder.getLongitude();
            String latitude = kndCanteenOrder.getLatitude();
        }
        map.put("kndCanteenOrder", kndCanteenOrder);
        map.put("kndCanteenOrderFoodList", kndCanteenOrderFoodList);
        if (null != kndCanteenOrderFoodList) {
            for (KndCanteenOrderFood canteenOrderFood : kndCanteenOrderFoodList) {
                String[] specifications = canteenOrderFood.getSpecifications();
                String specification = "";
                if (null != specifications) {
                    for (String s : specifications) {
                        specification += "," + s;
                    }
                    canteenOrderFood.setSpecificationsStr(StrUtil.isNotEmpty(specification) ? specification.substring(1) : "");
                }
            }
        }
        if (0 == kndCanteenOrder.getOrderStatus()) {
            map.put("packPrice", 0);
            map.put("peisongTime", DateUtil.offsetMinute(new Date(), 60));
        }
        map.put("peisongPrice", kndCanteenOrder.getFreightPrice());
        return ResponseUtil.ok("操作成功！", map);
    }

    /**
     * 通过桌台id查询 茶位费
     */
    @PostMapping("/selectCanteenTableFeeByTableId")
    public Object selectCanteenTableFeeByTableId(@RequestParam("tableId") String tableId) {
        if (StrUtil.isEmpty(tableId)) {
            return ResponseUtil.fail("没有查询到桌台号信息");
        }
        //桌台信息
        KndCanteenTable kndCanteenTable = kndCanteenTableService.selectKndCanteenTableById(tableId);
        return ResponseUtil.ok("查询成功！", kndCanteenTable);
    }

    /**
     * 通过桌台id查询 茶位费
     */
    @PostMapping("/getTableInfo")
    public Object getTableInfo(@RequestParam("tableId") String tableId) {
        if (StrUtil.isEmpty(tableId)) {
            return ResponseUtil.fail("没有查询到桌台号信息");
        }
        //桌台信息
        KndCanteenTable kndCanteenTable = kndCanteenTableService.selectKndCanteenTableById(tableId);
        if ((null == kndCanteenTable.getBound() || 0 == kndCanteenTable.getBound()) && StrUtil.isEmpty(kndCanteenTable.getCodeUrl())) {
            return ResponseUtil.fail("桌台码暂未绑定门店");
        }

        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("storeId", kndCanteenTable.getStoreId());

        Map<String, Object> map = new HashMap<>();
        map.put("kndCanteenTable", kndCanteenTable);
        return ResponseUtil.ok("查询成功！", map);
    }

    /**
     * 获取微信用户的openId（——餐饮小程序使用）
     */
/*    @GetMapping("/getUserOpendId")
    public Object getUserOpendId(@RequestParam("code") String code, @RequestParam(value = "merchantId", required = false) Integer
            merchantId, @RequestParam(value = "authType", defaultValue = "wx") String authType, @RequestParam(value = "appId", required = false) String appId) {
        log.info("-------------------------获取微信用户的openId code----------------------" + JSONObject.toJSONString(code) + "-------------------------------------");
        if (null == code) {
            throw new RuntimeException("参数缺失");
        }
        String miniappAppid;
        String miniAppSecret;
*//*        if (Constants.WX_ROAL.equalsIgnoreCase(authType)) {
//            miniappAppid = "wxa93009bce87bd1fd";//未莱卡appid
//            miniAppSecret = "3ee32af21fd65cead0ee021684d3ccab";//未莱卡秘钥
            String url = "https://api.weixin.qq.com/sns/jscode2session"
                    + "?appid=AppId"
                    + "&secret=AppSecret"
                    + "&js_code=CODE"
                    + "&grant_type=authorization_code";
            url = url.replace("AppId", miniappAppid)
                    .replace("AppSecret", miniAppSecret)
                    .replace("CODE", code);*//*
            // 根据地址获取请求
//            HttpGet request = new HttpGet(url);//这里发送get请求
            // 获取当前客户端对象
            HttpClient httpClient = HttpClientBuilder.create().build();
            // 通过请求对象获取响应对象
            String openid = "";
            try {
                HttpResponse response = httpClient.execute(request);

                String jsonStr = EntityUtils.toString(response.getEntity(), "utf-8");

                log.info(jsonStr);

                JSONObject jsonTexts = (JSONObject) JSON.parse(jsonStr);
                log.info(jsonTexts.toJSONString());
                if (jsonTexts.get("openid") != null) {
                    openid = jsonTexts.get("openid").toString();
                }
                log.info("-------------------------获取微信用户的openId openid----------------------" + JSONObject.toJSONString(openid) + "-------------------------------------");
            } catch (IOException e) {
                e.printStackTrace();
                log.error("获取微信用户openIdId异常：");
                //增加日志
                kndOrderLogService.insertKndOrderLog(null, Constants.LOGIN_FAIL, "获取jsapi微信open失败",
                        "支付授权", url, null, JSONUtil.toJsonStr(e), TenantParametersUtils.getTenantId());
                return AjaxResult.error("获取失败", e.getMessage());
            }
            return ResponseUtil.ok("获取成功", openid);
        } else if ("test".equalsIgnoreCase(authType)) {
            return AjaxResult.success("获取测试用戶ID成功", System.currentTimeMillis());
        }
        return AjaxResult.success("获取支付宝小程序用戶ID成功", tradeUserId);
    }*/


    //--------------------------------------------------------------外卖配送新增接口------------------------------------------------------------------------

/*    @PostMapping("/addDeliveryAddress")
    public AjaxResult addDeliveryAddress(@RequestBody KndUserDeliveryAddress address) {
        if (validateAddressObj(address)) {
            Object mobile = address.getParams().get("mobile");
            if (mobile != null) {
                this.syncSaveAddress(address, (String) mobile);
            }
            return toAjax(kndUserDeliveryAddressService.insertKndUserDeliveryAddress(address));
        }
        return error("缺少必要参数");
    }

    @PostMapping("/delDeliveryAddress")
    public AjaxResult delDeliveryAddress(@RequestParam String ids, @RequestParam String mobile) {
        String[] split = ids.split(",");
        for (int i = 0; i < split.length; i++) {
            KndUserDeliveryAddress addressById = kndUserDeliveryAddressService.selectKndUserDeliveryAddressById((split[i]));
            if (addressById != null && addressById.getWlshAddressId() != null) {
                this.syncDelAddress(addressById, mobile);
            }
        }
        return toAjax(kndUserDeliveryAddressService.deleteKndUserDeliveryAddressByIds(ids));
    }

    @PostMapping("/updateDeliveryAddress")
    public AjaxResult updateDeliveryAddress(@RequestBody KndUserDeliveryAddress address) {
        if (address.getId() == null) return error("参数错误");
        if (validateAddressObj(address)) {
            Object mobile = address.getParams().get("mobile");
            if (mobile != null) {
                this.syncSaveAddress(address, (String) mobile);
            }
            return toAjax(kndUserDeliveryAddressService.updateKndUserDeliveryAddress(address));
        }
        return AjaxResult.error("缺少必要参数");
    }

    @GetMapping("/deliveryAddressList")
    public AjaxResult list(String openId) {
        if (StringUtils.isEmpty(openId)) return error("参数错误");
        KndUserDeliveryAddress address = new KndUserDeliveryAddress();
        address.setOutUserId(openId);
        return AjaxResult.success(kndUserDeliveryAddressService.selectKndUserDeliveryAddressList(address));
    }

    // 同步未莱生活的收货地址
    private void syncSaveAddress(KndUserDeliveryAddress ad, String userPhone) {
        try {
            Map<String, Object> map = new HashMap<>();
            map.put("id", ad.getWlshAddressId());
            map.put("mobile", userPhone);
            map.put("province", ad.getProvince());
            map.put("city", ad.getCity());
            map.put("county", ad.getArea());
            map.put("addressDetail", ad.getReceiverAddress());
            map.put("isDefault", ad.getIsDef() == 1 ? "true" : "false");
            map.put("areaCode", 0);
            map.put("name", ad.getReceiverName());
            map.put("tel", ad.getReceiverPhone());
            String ret = HttpClientUtil.httpPostWithJson(JSONObject.parseObject(JSONUtils.toJSONString(map)), TenantParametersUtils.getRealmUrl() + "/wx/address/integral/save");
            log.info("请求同步未莱生活收货地址: {}", JSONUtils.toJSONString(map));
            log.info("返回结果: {}", ret);
            JSONObject obj = JSONObject.parseObject(ret);
            if (obj.getInteger("errno") != null && obj.getInteger("errno").equals(0)) {
                ad.setWlshAddressId(obj.getString("data"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void syncDelAddress(KndUserDeliveryAddress ad, String userPhone) {
        try {
            Map<String, Object> map = new HashMap<>();
            map.put("id", ad.getWlshAddressId());
            map.put("mobile", userPhone);
            String ret = HttpClientUtil.httpPostWithJson(JSONObject.parseObject(JSONUtils.toJSONString(map)), TenantParametersUtils.getRealmUrl() + "/wx/address/integral/delete");
            log.info("同步删除未莱生活收货地址: {}", JSONUtils.toJSONString(map));
            log.info("返回结果: {}", ret);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @GetMapping("/getDeliverySetting")
    public Object getSetting(@RequestParam("storeId") String storeId,
                                 @RequestParam(value = "deliveryAddressId", required = false) String deliveryAddressId) {

        KndMerchant km = kndMerchantService.selectKndMerchantById(kndStore.getMerchantId());

        Map<String, Object> map = new HashMap<>();
        map.put("startPrice", km.getStartPrice());
        map.put("maxDeliveryDistance", km.getMaxDeliveryDistance());
        map.put("customDeliveryFee", km.getCustomDeliveryFee());
        map.put("deliveryFeePayer", km.getDeliveryFeePayer());
        map.put("mtpsPrice", meituanPeisongopenUtil.getDeliveryPrice(deliveryAddressId, storeId));//美团配送费

        KndUserDeliveryAddress address = kndUserDeliveryAddressService.selectKndUserDeliveryAddressById((deliveryAddressId));
        BigDecimal distance = LatLngUtil.getDistance(kndStore.getLongitude() + "," + kndStore.getLatitude(),
                address.getLongitude() + "," + address.getLatitude());
        if (null == distance) {
            distance = new BigDecimal("0");
        }
        map.put("distance", distance);

        return AjaxResult.success(map);
    }

    private boolean validateAddressObj(KndUserDeliveryAddress address) {
        if (
                StrUtil.isBlank(address.getReceiverName()) ||
                        StrUtil.isBlank(address.getReceiverAddress()) ||
                        StrUtil.isBlank(address.getReceiverPhone()) ||
                        StrUtil.isBlank(address.getOutUserId()) ||
                        address.getProvince() == null ||
                        address.getCity() == null ||
                        address.getArea() == null ||
                        address.getIsDef() == null ||
                        address.getGender() == null) {
            return false;
        }
        return true;
    }*/


}
