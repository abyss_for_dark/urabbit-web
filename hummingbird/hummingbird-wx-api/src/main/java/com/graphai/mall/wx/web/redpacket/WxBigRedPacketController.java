package com.graphai.mall.wx.web.redpacket;

import static com.graphai.commons.util.servlet.ServletUtils.getRequest;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.graphai.commons.constant.JetcacheConstant;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.ip.IpUtils;
import com.graphai.commons.util.json.JacksonUtils;
import com.graphai.commons.util.lock.MutexLockUtils;
import com.graphai.id.GraphaiIdGenerator;
import com.graphai.mall.admin.domain.MallRedQrcodeRecord;
import com.graphai.mall.admin.domain.MallSystemRule;
import com.graphai.mall.admin.service.IGameRedHourRuleService;
import com.graphai.mall.admin.service.IMallRedQrcodeRecordService;
import com.graphai.mall.db.domain.MallUser;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.nosql.redis.NewCacheInstanceServiceLoader;
import com.graphai.spi.mq.BasedMQProducer;
import com.graphai.spi.mq.MQBaseConstant;

/**
 * 大额红包
 * 
 * @author biteam 整理
 *
 */
@RestController
@RequestMapping("/wx/gamejackpot")
@Validated
public class WxBigRedPacketController {
    private final Log logger = LogFactory.getLog(WxBigRedPacketController.class);
    @Autowired
    private BasedMQProducer _basedMQProducer;
    @Autowired
    private MallUserService mallUserService;
    @Autowired
    private IMallRedQrcodeRecordService iMallRedQrcodeRecordService;

    @Autowired
    private IGameRedHourRuleService redHourRuleService;

    /**
     * 查看当前整点红包领取总量是否满足触发大额红包，以及当前用户是否已经领取过该档次的大额红包
     *
     * @return
     */
    @GetMapping("/receiveStatus")
    public Object receiveStatus(@LoginUser String userId) throws ExceptionInInitializerError {

        if (null == userId) {
            return ResponseUtil.unlogin();
        }

        MallUser user = mallUserService.findById(userId);

        if (user == null) {
            return ResponseUtil.fail("该用户不存在！");

        }

        return getReceiveStatus(userId);

    }

    /**
     * 判断抽取（弹窗）
     *
     * @param userId
     * @return
     */
    private Object getReceiveStatus(String userId) {
        // 查询当天整点红包领取个数
        Integer redSum = iMallRedQrcodeRecordService.getRedSum();
        // 查询所有大额红包规则(降序查询，保证领取最新档)
        List<MallSystemRule> mallSystemRules = redHourRuleService.list(new QueryWrapper<MallSystemRule>().eq("rule_id", 3).last("ORDER BY (reserve_one +0) desc"));

        for (MallSystemRule rule : mallSystemRules) {
            // 规则中 触发大额红包的整点红包阈值
            Integer ruleSum = Integer.valueOf(rule.getReserveOne());
            // 达到阈值，触发
            if (redSum >= ruleSum) {

                String cacheKey = JetcacheConstant.bigRedItemCacheConstant(userId, rule.getId());
                Map<String, Object> recordmap = NewCacheInstanceServiceLoader.bizCache().get(cacheKey);

                if (MapUtils.isNotEmpty(recordmap)) {

                    return ResponseUtil.fail("当前不能领取大额红包");
                }

                // 判断该用户当天有无领取过同档次大额红包，
                boolean getStatus = iMallRedQrcodeRecordService.getStatusByUseId(rule.getId(), userId);
                if (getStatus) {
                    // 判断该档次大额红包当天份额有无领取完
                    // 1，查出该档次被领取数量
                    Integer receiveCount = iMallRedQrcodeRecordService.getReceiveCount(rule.getId());
                    // 2,大额红包当天可领取份额 > 已领取数量
                    if (Integer.valueOf(rule.getReserveTwo()) > receiveCount) {


                        return ResponseUtil.ok();
                    }
                }
            }
        }

        return ResponseUtil.fail("当前不能领取大额红包");
    }


    /**
     * 判断抽取（领取）
     *
     * @param userId
     * @return
     */
    private HashMap<String, String> getReceiveStatus2(String userId) {
        HashMap<String, String> map = new HashMap<>();

        // 查询当天整点红包领取个数
        Integer redSum = iMallRedQrcodeRecordService.getRedSum();
        // 查询所有大额红包规则(降序查询，保证领取最新档)
        List<MallSystemRule> mallSystemRules = redHourRuleService.list(new QueryWrapper<MallSystemRule>().eq("rule_id", 3).last("ORDER BY (reserve_one +0) desc"));


        for (MallSystemRule rule : mallSystemRules) {
            // 规则中 触发大额红包的整点红包阈值
            Integer ruleSum = Integer.valueOf(rule.getReserveOne());
            // 达到阈值，触发
            if (redSum >= ruleSum) {



                // 判断该用户当天有无领取过同档次大额红包，
                boolean getStatus = iMallRedQrcodeRecordService.getStatusByUseId(rule.getId(), userId);
                if (getStatus) {
                    // 判断该档次大额红包当天份额有无领取完
                    // 1，查出该档次被领取数量
                    Integer receiveCount = iMallRedQrcodeRecordService.getReceiveCount(rule.getId());

                    if (receiveCount == 0) {
                        logger.warn("****************系统全局消息推送****************** 第一个领取的用户：" + userId);

                        // todo 每个阈值发一次消息推送
                        //
                        // String title ="大额红包开抢啦";
                        // String content = "本轮大额红包开启啦，速来领取最高2021元现金大额红包，无门槛，数量有限，先到先得，领完即止，错过本场，再等一轮~";
                        // String linkUrl = "/pages/game/red-bad-game";
                        // String type = "";
                        // String logoUrl= "";
                        // Integer noticeType= 0;
                        //
                        // NotificationTemplate template = PushUtil.getNotificationTemplate(title, content, linkUrl, type,
                        // logoUrl, noticeType);
                        // PushUtil.pushToSingle("fa4913a592192f5d8e2398b2a591ffef", "嘉华帅嘉华帅嘉华帅嘉华帅", "内容内容内容",
                        // "/pages/game/red-bad-game", null, 4, null);
                        // System.out.println("消息发送了*********************************************");
                        //
                    }


                    // 2,大额红包当天份额的最大数量 > 已领取数量
                    if (Integer.valueOf(rule.getReserveTwo()) > receiveCount) {


                        map.put("id", rule.getId());
                        map.put("startSection", rule.getStartSection().toString());
                        map.put("endSection", rule.getEndSection().toString());
                        map.put("status", "500");
                        map.put("proportion", rule.getProportion().toString());

                        return map;
                    } else {
                        map.put("status", "400");
                        return map;
                    }


                } else {

                    map.put("status", "401");
                    return map;
                }
            }
        }

        return null;

    }


    /**
     * 判断抽取（返回当前大额红包档次）
     *
     * @param userId
     * @return
     */
    private MallSystemRule getCurrentRule(String userId) {
        // 查询当天整点红包领取个数
        Integer redSum = iMallRedQrcodeRecordService.getRedSum();
        // 查询所有大额红包规则(降序查询，保证领取最新档)
        List<MallSystemRule> mallSystemRules = redHourRuleService.list(new QueryWrapper<MallSystemRule>().eq("rule_id", 3).last("ORDER BY (reserve_one +0) desc"));

        for (MallSystemRule rule : mallSystemRules) {
            // 规则中 触发大额红包的整点红包阈值
            Integer ruleSum = Integer.valueOf(rule.getReserveOne());
            // 达到阈值，触发
            if (redSum >= ruleSum) {

                // 判断该用户当天有无领取过同档次大额红包，
                boolean getStatus = iMallRedQrcodeRecordService.getStatusByUseId(rule.getId(), userId);
                if (getStatus) {
                    // 判断该档次大额红包当天份额有无领取完
                    // 1，查出该档次被领取数量
                    Integer receiveCount = iMallRedQrcodeRecordService.getReceiveCount(rule.getId());
                    // 2,大额红包当天份额的最大数量 > 已领取数量
                    if (Integer.valueOf(rule.getReserveTwo()) > receiveCount) {


                        return rule;
                    }
                }
            }
        }

        return null;
    }

    /**
     * 领取大額红包
     */
    @GetMapping("/receiveBig")
    public Object receiveBigRedpacket(@LoginUser String userId, HttpServletRequest request) throws Exception {
        if (null == userId) {
            return ResponseUtil.unlogin();
        }

        /** 业务执行所需传递必要参数 **/
        Map<String, Object> bizmap = new HashMap<>();
        // 返回参数
        Map<String, Object> datamap = new HashMap<>();
        try {
            synchronized (MutexLockUtils.getMutexLock(userId)) {
                String device = "";
                String userAgent = getRequest().getHeader("user-agent");
                if (null != userAgent && userAgent.contains("iPhone")) {
                    device = "iPhone";
                } else {
                    device = "Android";
                }
                /** 判断是否有资格领取大额红包-获取大额红包配置规则 -返回异常信息 **/
                HashMap<String, String> mallSystemRule = getReceiveStatus2(userId);

                if ("400".equals(mallSystemRule.get("status"))) {
                    datamap.put("type", "01");
                    datamap.put("msg", "抢光啦");
                    return ResponseUtil.ok(datamap);
                }
                if ("401".equals(mallSystemRule.get("status"))) {
                    datamap.put("type", "02");
                    datamap.put("msg", "请勿重复领取");
                    return ResponseUtil.ok(datamap);
                }


                // 获取用户领取的对应随机红包记录
                MallRedQrcodeRecord received = iMallRedQrcodeRecordService.getOneRandom();

                /** 通过红包规则段获取最终领取金额 **/


                BigDecimal residuer = received.getAmount().setScale(2, BigDecimal.ROUND_DOWN);

                // MallUser user = mallUserService.findById(userId);

                /** 翻倍后金额 */
                // BigDecimal residuerTal = residuer;
                //
                // //属于普通用户还是达人，0 普通。 1 达人(乘以倍率)
                // if (user.getIsTalent().equals("1")) {
                // residuerTal = residuer.multiply(new BigDecimal(mallSystemRule.get("proportion"))).setScale(2,
                // BigDecimal.ROUND_DOWN);
                // }



                String cacheKey = MQBaseConstant.RED_BIG_KEY_PREFIX + GraphaiIdGenerator.nextId("");
                String recordCahceKey = JetcacheConstant.bigRedItemCacheConstant(userId, mallSystemRule.get("id"));
                // "wreditem" + userId + ":" + LocalDate.now().toString();
                bizmap.put("device", device);
                bizmap.put("version", getRequest().getHeader("version"));
                bizmap.put("ip", IpUtils.getIpAddress(getRequest()));
                bizmap.put("userId", userId);
                // 原金额
                bizmap.put("residuerStr", residuer.toString());
                // 翻倍金额
                // bizmap.put("residuerTal", residuerTal.toString());
                bizmap.put("ruleId", mallSystemRule.get("id"));

                // 该用户领取的对应红包记录
                bizmap.put("received", received.getId());

                /** 将参数投递入缓存 **/
                // NewCacheInstanceServiceLoader.bizCache().put(cacheKey, bizmap);

                MallUser mallUser = mallUserService.findById(userId);
                Map<String, Object> recordmap = new HashMap<>();
                // recordmap.put("amount", residuerTal);
                recordmap.put("amount", residuer);
                recordmap.put("create_time", LocalDateTime.now());
                recordmap.put("to_user_id", userId);
                recordmap.put("nickname", mallUser.getNickname());
                recordmap.put("avatar", mallUser.getAvatar());
                //NewCacheInstanceServiceLoader.bizCache().put(recordCahceKey, recordmap);
                
                Map<String, Object> mqParams = new HashMap<>();
                mqParams.put("param", JacksonUtils.bean2Jsn(recordmap));
                mqParams.put("bizKey", cacheKey);
                mqParams.put("tags", MQBaseConstant.RED_BIG_ENVELOPE_TAG);
                _basedMQProducer.send(mqParams);
                /** 将缓存ID投递入MQ队列 **/
                //wxProducerService.produceMsg(JacksonUtils.bean2Jsn(bizmap), cacheKey, MQBaseConstant.RED_BIG_ENVELOPE_TAG);
                // wxProducerService.produceMsg(cacheKey, cacheKey, MQBaseConstant.RED_BIG_ENVELOPE_TAG);
                /** 同步触发红包领取逻辑 做实际的入库操作 **/
                // gameRedComponent.receive(userId, makerId, mallSystemRule, residuer, grain, map);
                /** 移除mutexlock **/
                MutexLockUtils.remove(userId);


                datamap.put("type", "00");
                datamap.put("msg", "领取成功");
                // datamap.put("money", residuerTal);
                datamap.put("money", residuer);
                datamap.put("reminder", "恭喜，领取成功");
                return ResponseUtil.ok(datamap);
            }
        } catch (Exception e) {
            logger.error("领取红包异常：" + e.getMessage(), e);

            return ResponseUtil.fail("红包领取失败");
        }
    }


    /**
     * 大額红包领取进度
     */
    @GetMapping("/bigDetail")
    public Object getBigRedpacketDetail(@LoginUser String userId, HttpServletRequest request) {

        if (null == userId) {
            return ResponseUtil.unlogin();
        }

        // 1.当天所有整点红包领取个数
        Integer redHourCount = iMallRedQrcodeRecordService.getRedSum();

        // 2.将要触发的档次总共红包个数
        // Integer ruleCount = 0;


        // 当前档次的阈值
        Integer currentThreshold = 0;

        Integer nextThreshold = 0;


        // 当前档次索引
        int flag = -1;

        // 查询所有大额红包规则(降序查询，保证领取最新档)
        List<MallSystemRule> mallSystemRules = redHourRuleService.list(new QueryWrapper<MallSystemRule>().eq("rule_id", 3).last("ORDER BY (reserve_one +0) desc"));

        if (mallSystemRules == null || mallSystemRules.size() == 0) {
            return null;
        }



        for (int i = 0; i < mallSystemRules.size(); i++) {
            // 规则中 触发大额红包的整点红包阈值
            Integer ruleSum = Integer.valueOf(mallSystemRules.get(i).getReserveOne());
            // 达到阈值，触发
            if (redHourCount >= ruleSum) {
                flag = i;
                break;
            }
        }


        if (flag == -1) {

            nextThreshold = Integer.valueOf(mallSystemRules.get(mallSystemRules.size() - 1).getReserveOne());

        } else {
            nextThreshold = Integer.valueOf(mallSystemRules.get(mallSystemRules.size() - 1).getReserveOne());


            // 保留多档位
            // ruleCount = Integer.valueOf(mallSystemRules.get(flag - 1).getReserveTwo());
            // currentThreshold = Integer.valueOf(mallSystemRules.get(flag).getReserveOne());
            // nextThreshold = Integer.valueOf(mallSystemRules.get(flag - 1).getReserveOne());

        }

        // 3.距离下一档还差多少个整点红包
        // int difference = threshold - redHourCount;


        HashMap<String, Integer> map = new HashMap<>();

        // 1.当天所有整点红包领取个数
        map.put("redHourCount", redHourCount);

        // // 2.将要触发的档次总共红包个数
        // map.put("ruleCount",ruleCount);
        //
        // // 3.距离下一档还差多少个整点红包
        // map.put("difference",difference);
        // 2.将要触发的档次总共红包个数


        // 当前档阈值
        map.put("currentThreshold", currentThreshold);

        // 下一档阈值
        map.put("nextThreshold", nextThreshold);

        return ResponseUtil.ok(map);

    }


    /**
     * 大额红包领取记录
     *
     * @param userId
     * @param
     * @return
     */
    @PostMapping("/bigReceiveDetail")
    public Object getBigReceiveDetail(@LoginUser String userId, @RequestBody Map<String, String> map) {
        if (null == userId) {
            return ResponseUtil.unlogin();
        }


        return iMallRedQrcodeRecordService.getBigReceiveDetail(map, userId);


    }

}
