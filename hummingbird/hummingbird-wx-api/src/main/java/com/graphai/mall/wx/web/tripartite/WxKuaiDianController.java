package com.graphai.mall.wx.web.tripartite;


import com.alibaba.fastjson.JSONObject;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.mall.db.dao.MallUserMapper;
import com.graphai.mall.db.domain.MallUser;
import com.graphai.mall.db.util.RandomUtils;
import com.graphai.mall.db.util.phone.RSAUtils;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.mall.wx.util.HttpClientUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;
import java.net.URLEncoder;
import java.util.*;

@Validated
@RestController
@RequestMapping("/wx/kuaiDian")
public class WxKuaiDianController {
    private final Log logger = LogFactory.getLog(WxKuaiDianController.class);

//    private static final String API_URL = "http://test-tchws.gokuaidian.cn/api/v1"; // 测试环境
    private static final String API_URL = "https://tch.fleetingpower.com/api/v1"; // 正式环境
    private static final String STATION_LIST_URL = "/getStationListUrl";//获取快电的h5列表页接口
    private static final String GET_USER_TOKEN = "/queryUserToken";//获取快电的token接口

    private static final String PRIVATE_KEY = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAJjGFj7Lj1nMlJh0w5TiQrhRFqOCGPQ7IyP8O3y+qaxKiAe/9PVjBLW0TcB4rspv8Fn7QmDsROPZ+WNZSMJJsJjz66rLgq/86nTsU8XL0OXoT7iqCUSdpqpgZu/oSmTlZ6D0xM58rvN6JEIDF4VGnDIjqIA0q4b9VuN/7kKh6o4BAgMBAAECgYBrNSbLLkIMfDoSum29fwHfIlhGqbclPJKuYATGx21+EFdhSN13jeLyN/MQDCBswxzhqCauqV62nnIZu8AJR3j65yZ0aLXEGxEWMPOWXXpBbUBou8QvX/UYowdETqF1iHezK3ME9VRI02cUDAwIpmiuWilMrcUO9Qy7uPdhiplKAQJBAMlI4pC7eg3NZ1O1Nt4hMmOGBWiKnxyjMGhum6L0HLGiqf3MMRIYXa/UZtIz+4pjO2ORJAfT5du8/6HD1WrIg9ECQQDCTWVGOMW7hGdN89nV5Ffuc2MkkQJlTSDLNJKgZNqiER2OUj5F5gc/N/UAdeAihqq93dqr/fph+/GpwdpW9+MxAkEAoXyTIPD4TEiF5HeXjIDvrz8f6a6FVvFp7e8HeKYGu1E4tsDygYUA7Qcocu53maBhTlFsqccMO84W/U5jU6eJcQJAHYUuvtV/TTcXjuNelD2mEmdbh8G0UHnAYr8xC8dm5Dqfd/EM3wfM0BELWy+NhxFYvZIIWBs1QE/L7pOyPYZZ8QJBAJSXQIJg8jCyEyjqEi5EaDc2R742vSyT9JSR4gvkuWkYbXMwE4UvBS5f4hGxascxHb6n2sP5D1rmNWyTp3TPEAo=";
    private static final String PUBLIC_KEY = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCYxhY+y49ZzJSYdMOU4kK4URajghj0OyMj/Dt8vqmsSogHv/T1YwS1tE3AeK7Kb/BZ+0Jg7ETj2fljWUjCSbCY8+uqy4Kv/Op07FPFy9Dl6E+4qglEnaaqYGbv6Epk5Weg9MTOfK7zeiRCAxeFRpwyI6iANKuG/Vbjf+5CoeqOAQIDAQAB";
    private static final String PLATFORMCODE = "920072201";


    @Resource
    private MallUserMapper mallUserMapper;

    @GetMapping("/linkUrl")
    public Object index(@LoginUser String userId, @NotNull String userLatStr,@NotNull String userLngStr){
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        try {
            String token = getToken(userId);
            if (token == null) return ResponseUtil.fail();
            Map<String, String> signParams = getBaseMap();
            signParams.put("token", token);
            signParams.put("userLatStr", userLatStr);
            signParams.put("userLngStr", userLngStr);
            Map<String, String> params = generateRequestParams(signParams);
            String result = HttpClientUtils.doPost(API_URL + STATION_LIST_URL, params);
            JSONObject obj = JSONObject.parseObject(result);
            if (obj != null && obj.get("data") != null && (int) obj.get("ret") == 0) {
                JSONObject data = (JSONObject) obj.get("data");
                Object url = data.get("url");
                if (url != null) {
                    String[] split = url.toString().split("\\?");
                    String linkUrl = "/pages/web/webview-kd?seq=" + data.get("seq") + "&link=" + split[0] + "?" + URLEncoder.encode(split[1], "UTF-8");
                    return ResponseUtil.ok(linkUrl);
                }
            }
            logger.info("-------调用获取快电h5url接口异常: "+result);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseUtil.fail();
    }

    private String getToken(String userId) throws Exception {
        MallUser user = mallUserMapper.selectByPrimaryKeySelective(userId, MallUser.Column.mobile);
        if (user == null || user.getMobile() == null) return null;
        Map<String, String> signParams = getBaseMap();
        signParams.put("phone", user.getMobile());
        Map<String, String> requestParams = generateRequestParams(signParams);
        String result = HttpClientUtils.doPost(API_URL + GET_USER_TOKEN, requestParams);
        JSONObject obj = JSONObject.parseObject(result);
        if (obj != null && obj.get("data") != null && (int) obj.get("ret") == 0) {
            JSONObject data = (JSONObject) obj.get("data");
            return data.get("token").toString();
        }
        logger.info("-------调用获取快电token接口异常: "+result);
        return null;
    }

    private Map<String, String> generateRequestParams(Map<String, String> signParams) throws Exception {
        List<String> keys = new ArrayList<>(signParams.size());
        keys.addAll(signParams.keySet());
        Collections.sort(keys);
        StringBuilder params = new StringBuilder();
        for (String key : keys) {
            params.append(key).append("=").append(signParams.get(key)).append("&");
        }
        String sign = RSAUtils.sign(params.toString().substring(0, params.length() - 1).getBytes(), PRIVATE_KEY);
        signParams.put("sig", sign);
        return signParams;
    }

    private Map<String, String> getBaseMap() {
        Map<String, String> map = new HashMap<>();
        map.put("platformCode", PLATFORMCODE);
        map.put("seq", RandomUtils.genRandomNum(30));
        map.put("timestamp", System.currentTimeMillis() + "");
        return map;
    }

}
