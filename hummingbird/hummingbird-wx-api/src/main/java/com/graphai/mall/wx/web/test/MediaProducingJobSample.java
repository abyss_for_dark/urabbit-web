package com.graphai.mall.wx.web.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

import org.apache.http.entity.ContentType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import com.aliyun.ice20201109.Client;
import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.teaopenapi.models.Config;

/**
 * @author ：xxxxx
 * @date ：Created in 2021/1/12
 */
public class MediaProducingJobSample {
    
    public static void main(String[] args) {
        try {
            
            File pdf =  new File("C:\\Users\\Administrator\\Documents\\WeChat Files\\wxid_vhvby512g1l312\\FileStorage\\Video\\2021-03\\bb571f8ab51586887b2169f2aec32eb4.mp4");
            
            FileInputStream fileInputStream = null;
            fileInputStream = new FileInputStream(pdf);
            MultipartFile multipartFile = new MockMultipartFile(pdf.getName(), pdf.getName(),
                    ContentType.APPLICATION_OCTET_STREAM.toString(), fileInputStream);
            
            
            ///String url = ossFileUtils.upload(multipartFile.getOriginalFilename(), multipartFile);
            
            
            Config config =  new Config();
            // Endpoint以上海为例，其它Region请按实际情况填写。
            config.endpoint = "ice.cn-shanghai.aliyuncs.com";
            // 阿里云子账号AccessKey， 需要添加 AliyunICEFullAccess 权限。强烈建议您创建并使用RAM账号进行API访问或日常运维，请登录 https://ram.console.aliyun.com 创建RAM账号。
            config.accessKeyId = "LTAIAYqztsGYLpCv";
            config.accessKeySecret = "PwMG4dk9OXoBn4csOR5G72XpT63XJI";
            Client iceClient = new Client(config);
            
            
         // Endpoint以杭州为例，其它Region请按实际情况填写。
            String endpoint = "oss-cn-shanghai.aliyuncs.com";
            // 阿里云主账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM账号进行API访问或日常运维，请登录RAM控制台创建RAM账号。
            String accessKeyId = "LTAIAYqztsGYLpCv";
            String accessKeySecret = "PwMG4dk9OXoBn4csOR5G72XpT63XJI";
            String bucketName = "gx-shanghai-video";
            // <yourObjectName>上传文件到OSS时需要指定包含文件后缀在内的完整路径，例如abc/efg/123.jpg。
            String objectName = "mete123123st.mp4";

            
            
            // 创建OSSClient实例。
            OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);

            // 上传文件到指定的存储空间（bucketName）并将其保存为指定的文件名称（objectName）。
            ossClient.putObject(bucketName, objectName,  multipartFile.getInputStream());
            // 关闭OSSClient。
            ossClient.shutdown();  
            
//            // 注册媒资内容
//            RegisterMediaInfoRequest registerMediaInfoRequest = new RegisterMediaInfoRequest();
//            registerMediaInfoRequest.setInputURL("https://gx-shanghai-video.oss-cn-shanghai.aliyuncs.com/video/WeChat_20210324193326.mp4?Expires=1616587820&OSSAccessKeyId=TMP.3KiwjgppeRVWSFstra2fkmr1pi5NfdXJMKE9dVwef9qQFqvm3YqSUqFicLcbpgU2vMaXzEGiuy6EacEDNq6YmeZupwe9yw&Signature=iQHrvAJYR%2FpFGOQVYfydz7h78u4%3D");
//            registerMediaInfoRequest.setMediaType("video");
//            //RegisterMediaInfoResponse registerMediaInfoResponse = iceClient.registerMediaInfo(registerMediaInfoRequest);
//            String mediaId = "c";// registerMediaInfoResponse.getBody().getMediaId();
//            
//            RegisterMediaInfoRequest registerMediaInfoRequest1 = new RegisterMediaInfoRequest();
//            registerMediaInfoRequest1.setInputURL("https://gx-shanghai-video.oss-cn-shanghai.aliyuncs.com/video/WeChat_20210324193350.mp4?Expires=1616588175&OSSAccessKeyId=TMP.3KiwjgppeRVWSFstra2fkmr1pi5NfdXJMKE9dVwef9qQFqvm3YqSUqFicLcbpgU2vMaXzEGiuy6EacEDNq6YmeZupwe9yw&Signature=hx9KrkoZtUCBtDsT8%2BfLDFM8r%2F8%3D");
//            registerMediaInfoRequest1.setMediaType("video");
//            ///RegisterMediaInfoResponse registerMediaInfoResponse1 = iceClient.registerMediaInfo(registerMediaInfoRequest1);
//            String mediaId1 = "0a5eacad75cb4855a12990f3503802bb" ;// registerMediaInfoResponse1.getBody().getMediaId();
//            
//            System.out.println("mediaId : " + mediaId);
//            System.out.println("mediaId1 : " + mediaId1);
//            
//            // 通过timeline创建合成任务, mediaId 是通过内容库的注册接口注册得到的媒资ID
//            SubmitMediaProducingJobRequest request1 = new SubmitMediaProducingJobRequest();
//            request1.setTimeline("{\"VideoTracks\":[{\"VideoTrackClips\":[{\"MediaId\":\""+mediaId+"\"},{\"MediaId\":\""+mediaId1+"\"}]}]}");
//
//            // 输出的媒体文件 URL: 指定为所在 Region 的 OSS Bucket 下面的文件, bucketxxx替换成真实的 Bucket 名称
//            request1.setOutputMediaConfig("{ \"mediaURL\": \"https://gx-shanghai-video.oss-cn-shanghai.aliyuncs.com/video/hecheng123113-test.mp4\", \"type\": \"oss-object\" }");
//
//            SubmitMediaProducingJobResponse submitMediaProducingJobResponse = iceClient.submitMediaProducingJob(
//                request1);
//
//            SubmitMediaProducingJobResponseBody body = submitMediaProducingJobResponse.getBody();
//            System.out.println("requestId:" + body.getRequestId() + " jobId:" + body.getJobId());
//            
            
            
            
//            GetMediaProducingJobRequest request = new GetMediaProducingJobRequest();
//            request.setJobId("785d363756c24bf69963089752ef89ee");
//            GetMediaProducingJobResponse reqponse = iceClient.getMediaProducingJob(request);
//            System.out.println("jobId : " + reqponse.getBody().getMediaProducingJob().getJobId());
//            System.out.println("status : " + reqponse.getBody().getMediaProducingJob().getStatus());
//            System.out.println(JacksonUtils.bean2Jsn(reqponse.getBody().getMediaProducingJob()));
            //requestId:582C3AD9-8883-41BB-A4B6-813BF8F7FF7D jobId:d9d092eb296e45a9af6756f9e5dcfcb7
            
//            File file =  getFile(reqponse.getBody().getMediaProducingJob().getMediaURL());
//            System.out.println(file.getTotalSpace());
            //https://gx-shanghai-video.oss-cn-shanghai.aliyuncs.com/video/hecheng123113-test.mp4
//            GetMediaInfoRequest getMediaInfoRequest = new GetMediaInfoRequest();
//            getMediaInfoRequest.setMediaId("e9ea3e2fa3d943d9868c49656dc7427d");
//            GetMediaInfoResponse getMediaInfoResponse = iceClient.getMediaInfo(getMediaInfoRequest);
//            System.out.println("mediaId : " + JacksonUtils.bean2Jsn(getMediaInfoResponse.getBody().getMediaInfo())  );
            
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static File getFile(String url) throws Exception {
        //对本地文件命名
        String fileName = url.substring(url.lastIndexOf("."),url.length());
        File file = null;
 
        URL urlfile;
        InputStream inStream = null;
        OutputStream os = null;
        try {
            file = File.createTempFile("net_url", fileName);
            //下载
            urlfile = new URL(url);
            inStream = urlfile.openStream();
            os = new FileOutputStream(file);
 
            int bytesRead = 0;
            byte[] buffer = new byte[8192];
            while ((bytesRead = inStream.read(buffer, 0, 8192)) != -1) {
                os.write(buffer, 0, bytesRead);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (null != os) {
                    os.close();
                }
                if (null != inStream) {
                    inStream.close();
                }
 
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
 
        return file;
    }

}
