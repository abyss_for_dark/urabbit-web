package com.graphai.mall.wx.web.goods;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.obj.ObjectUtils;
import com.graphai.mall.db.domain.MallKeyword;
import com.graphai.mall.db.domain.MallSearchHistory;
import com.graphai.mall.db.service.goods.WxGoodsSearchAllService;
import com.graphai.mall.db.service.search.MallKeywordService;
import com.graphai.mall.db.service.search.MallSearchHistoryService;
import com.graphai.mall.wx.annotation.LoginUser;
import org.apache.ibatis.annotations.Param;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.time.LocalDateTime;

@RestController
@RequestMapping("/wx/goods/all")
@Validated
public class WxGoodsSearchAllController {
    @Resource
    private WxGoodsSearchAllService wxGoodsSearchAllService;
    @Resource
    private MallSearchHistoryService searchHistoryService;
    @Resource
    private MallKeywordService mallKeywordService;
    /**
     *
     * @param q 关键字
     * @param material   材质
     * @param minSize    最小尺寸
     * @param maxSize    最大尺寸
     * @param technology 工艺
     * @param minPrice   最低价格
     * @param maxPrice   最高价格
     * @param place      产地
     * @param userId     当前用户id
     * @param page       当前页
     * @param limit      每页展示数量
     * @param sortName   排序的类型  price/salesNum   : 金额 / 销量
     * @param sort       排序的方式  desc/asc ： 倒叙/正序
     * @return
     * @throws Exception
     */
    @GetMapping("/search")
    public Object getSearchText(@Param("q") String q, @RequestParam(required = false)String material,@RequestParam(required = false)String minSize,
                                @RequestParam(required = false)String maxSize,@RequestParam(required = false)String technology,@RequestParam(required = false)Double minPrice,
                                @RequestParam(required = false)Double maxPrice,@RequestParam(required = false)String place,@LoginUser String userId,@RequestParam(defaultValue = "1") Integer page,
                                @RequestParam(defaultValue = "16") Integer limit,@RequestParam(defaultValue = "price") String sortName,
                                @RequestParam(defaultValue = "true") Boolean sort,@RequestParam(required = false)String categoryId,@RequestParam(required = false)String tag) throws Exception {
        // 添加到搜索历史
        if (userId != null && !org.apache.commons.lang3.StringUtils.isEmpty(q)) {
            MallSearchHistory searchHistoryVo = new MallSearchHistory();
            searchHistoryVo.setKeyword(q);
            searchHistoryVo.setUserId(userId);
            searchHistoryVo.setFrom("wx");
            searchHistoryService.save(searchHistoryVo);

            // 新增关键字
            MallKeyword mallKeyword = mallKeywordService.queryByKeyword(q);
            if(mallKeyword == null){
                mallKeyword = new MallKeyword();
                mallKeyword.setKeyword(q);
                mallKeyword.setAddTime(LocalDateTime.now());
                mallKeyword.setUrl("#");
                mallKeyword.setIsHot(false);
                mallKeyword.setDeleted(false);
                mallKeyword.setIsDefault(false);
                mallKeywordService.add(mallKeyword);
            }else{
                mallKeyword.setSortOrder(mallKeyword.getSortOrder()+1);
                mallKeyword.setUpdateTime(LocalDateTime.now());
                mallKeywordService.updateById(mallKeyword);
            }
        }

        return wxGoodsSearchAllService.getGoodsSearch(q,material,minSize,maxSize,technology,minPrice,maxPrice,place,userId,page,limit,sortName,sort,categoryId,tag);
    }

    @GetMapping("/searchByName")
    public Object getSearchByName(@Param("q")String q,@Param("page")Integer page,@Param("limit")Integer limit,@Param("name")String name,@LoginUser String userId) throws Exception {
        if(ObjectUtils.isEmpty(userId)){
            return ResponseUtil.unlogin();
        }
        return wxGoodsSearchAllService.getGoodsSearchByName(q,page,limit,name);
    }
}
