package com.graphai.mall.wx.web.home;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.graphai.mall.db.domain.MallActivity;
import com.graphai.mall.db.service.promotion.ActivityService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.json.JacksonUtils;
import com.graphai.mall.db.domain.MallAd;
import com.graphai.mall.db.service.advertisement.MallAdService;
import com.graphai.mall.db.service.category.MallCategoryService;
import com.graphai.mall.db.util.RetrialUtils;
import com.graphai.mall.db.util.StringHelper;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.mall.wx.web.common.MyBaseController;
import com.graphai.open.mallindustry.service.IMallIndustryService;

import lombok.extern.slf4j.Slf4j;

/**
 * 首页服务
 */
@RestController
@RequestMapping("/wx/home")
@Validated
@Slf4j
public class WxHomeV3Controller extends MyBaseController {
    @Autowired
    private MallAdService adService;
    @Autowired
    private MallCategoryService categoryService;

    @Autowired
    private IMallIndustryService industryService;
    @Autowired
    private ActivityService activityService;

    /**
     * 首页数据
     *
     * @param userId 当用户已经登录时，非空。为登录状态为null
     * @return 首页数据
     */
    @GetMapping("/indexV3")
    public Object indexV3(String industryId, @LoginUser String userId, HttpServletRequest request,
                    @RequestParam(defaultValue = "1.6.6") String version) {

        if (industryId == null) {
            industryId = "0";
        }


        List<MallAd> adList = null;

        try {

            Map<String, Object> data = new HashMap<>();

            adList = adService.queryIndex((byte) 1);
            if (CollectionUtils.isNotEmpty(adList)) {
                for (int i = 0; i < adList.size(); i++) {
                    if (StringHelper.isNotNullAndEmpty(adList.get(i).getChannelCode())) {
                        adList.get(i).setBusinessId(adList.get(i).getChannelCode());
                    }
                }
            }
            String industryType = "dtkcategory";
            Object objIndustry = industryService.getIndustryByType(industryType);
            JSONObject object = JSONObject.parseObject(JacksonUtils.bean2Jsn(objIndustry));
            if (object.getString("errno").equals("0")) {
                data.put("centerIndustryList", JSON.parseObject(object.get("data").toString(), List.class));
            }

            Object obj = null;
            // 首页行业频道放入缓存
            // Object indexCate = homeCache.get("indexcate");
            // if (null == indexCate) {
            // obj = categoryService.queryIndustryAndCategoryV2("head", "home",
            // RetrialUtils.examine(mobileTye, version));
            // homeCache.put("indexcate", obj);
            // } else {
            // obj = indexCate;
            // }


            obj = categoryService.queryIndustryAndCategoryV2("head", "home", RetrialUtils.examine(request, version));

            data.put("banner", adList);
            if (RetrialUtils.examine(request, version)) {
                List<MallAd> mallAds = new ArrayList<MallAd>();
                adList.get(0).setLink("/pages/brand-specials/list?");
                mallAds.add(adList.get(0));

                data.put("banner", mallAds);
            }

            data.put("channelListAndIndustry", obj);

            // 首页广告
            List<MallActivity> activityList = activityService.selectConductActivityList(null,(byte)1);
            data.put("activityList", activityList);

            // 品牌制造商
            return ResponseUtil.ok(data);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {

        }

        return ResponseUtil.ok();
    }
}
