package com.graphai.mall.tb.dto;

public class TopAuthToken {

	private TopAuthTokenCreateResponse topAuthTokenCreateResponse;

	public TopAuthTokenCreateResponse getTopAuthTokenCreateResponse() {
		return topAuthTokenCreateResponse;
	}

	public void setTopAuthTokenCreateResponse(TopAuthTokenCreateResponse topAuthTokenCreateResponse) {
		this.topAuthTokenCreateResponse = topAuthTokenCreateResponse;
	}
	
	
}
