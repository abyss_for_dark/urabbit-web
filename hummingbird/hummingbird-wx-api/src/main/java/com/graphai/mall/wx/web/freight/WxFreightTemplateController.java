package com.graphai.mall.wx.web.freight;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.mall.admin.domain.MallUserMerchant;
import com.graphai.mall.admin.service.impl.MallUserMerchantServiceImpl;
import com.graphai.mall.setting.domain.FreightTemplate;
import com.graphai.mall.setting.service.FreightTemplateService;
import com.graphai.mall.wx.annotation.LoginUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/wx/freightTemplate")
public class WxFreightTemplateController {
    @Autowired
    private FreightTemplateService freightTemplateService;
    @Autowired
    private MallUserMerchantServiceImpl userMerchantService;

    @GetMapping("/queryMerchantTemplate")
    public Object queryMerchantTemplate(@LoginUser String userId) {
        MallUserMerchant userMerchant = userMerchantService.getOne(new QueryWrapper<MallUserMerchant>().lambda().eq(MallUserMerchant::getUserId, userId));
        List<FreightTemplate> list = freightTemplateService.list(new QueryWrapper<FreightTemplate>().lambda()
                .eq(FreightTemplate::getMerchantId, userMerchant.getMerchantId()));
        return ResponseUtil.ok(list);
    }
}
