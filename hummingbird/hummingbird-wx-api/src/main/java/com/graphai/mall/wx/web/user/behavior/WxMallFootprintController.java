package com.graphai.mall.wx.web.user.behavior;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.generator.controller.IBaseController;
import com.graphai.mall.admin.domain.WxMallFootprint;
import com.graphai.mall.admin.service.IMallFootprintService;
import com.graphai.mall.db.vo.WxMallFootprintVo;
import com.graphai.mall.wx.annotation.LoginUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;


/**
 * <p>
 * 用户浏览足迹表 前端控制器
 * </p>
 *
 * @author biteam
 * @since 2021-06-30
 */
@RestController
@RequestMapping("/wx/getFootprint")
@Validated
public class WxMallFootprintController extends IBaseController<WxMallFootprint,  String >  {

	@Autowired
    private IMallFootprintService serviceImpl;


	/**
	 * 用户足迹列表
	 *
	 * @param page 分页页数
	 * @param limit 分页大小
	 * @return 用户足迹列表
	 */
	@GetMapping("/list")
	public Object list(@LoginUser String userId,
					   @RequestParam(defaultValue = "1") Integer page,
					   @RequestParam(defaultValue = "10") Integer limit) {
		if (userId == null) {
			return ResponseUtil.unlogin();
		}
		Map<String,Object> footprintVo = serviceImpl.queryByBrowse(userId, page, limit);
		return ResponseUtil.ok(footprintVo);
	}

}

