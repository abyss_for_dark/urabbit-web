package com.graphai.mall.wx.web.redpacket;


import static com.graphai.commons.util.servlet.ServletUtils.getRequest;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.graphai.commons.constant.JetcacheConstant;
import com.graphai.commons.util.NameUtil;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.date.DateUtils;
import com.graphai.commons.util.ip.IpUtils;
import com.graphai.commons.util.json.JacksonUtil;
import com.graphai.commons.util.json.JacksonUtils;
import com.graphai.commons.util.lock.MutexLockUtils;
import com.graphai.commons.util.rsa.RSA;
import com.graphai.id.GraphaiIdGenerator;
import com.graphai.mall.admin.domain.MallRedQrcodeRecord;
import com.graphai.mall.admin.domain.MallSystemRule;
import com.graphai.mall.admin.service.IGameRedHourRuleService;
import com.graphai.mall.admin.service.IMallRedQrcodeRecordService;
import com.graphai.mall.db.domain.GameJackpotRedpacket;
import com.graphai.mall.db.domain.GameRedpacketRain;
import com.graphai.mall.db.domain.MallUser;
import com.graphai.mall.db.service.WxEnterpriseTransferService;
import com.graphai.mall.db.service.game.GameJackpotRedpacketService;
import com.graphai.mall.db.service.game.GameRedPacketRecordService;
import com.graphai.mall.db.service.game.GameRedpacketRainService;
import com.graphai.mall.db.service.red.WxRedService;
import com.graphai.mall.db.service.sysconfig.MallSystemConfigService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.util.RandomUtils;
import com.graphai.mall.db.util.redpacket.RedPacketBizUtils;
import com.graphai.mall.db.vo.RedPacketDetailsVo;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.mall.wx.util.AESUtils;
import com.graphai.nosql.redis.NewCacheInstanceServiceLoader;
import com.graphai.spi.mq.BasedMQProducer;
import com.graphai.spi.mq.MQBaseConstant;
import com.graphai.validator.Order;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import cn.hutool.core.util.StrUtil;

@RestController
@RequestMapping("/wx/gamejackpot")
@Validated
public class WxJackpotRedPacketController {
    private final Log logger = LogFactory.getLog(WxJackpotRedPacketController.class);
    @Autowired
    private GameRedpacketRainService gameRedpacketRainService;
    @Autowired
    private GameRedPacketRecordService gameRedPacketRecordService;
    @Autowired
    private MallUserService mallUserService;
    @Autowired
    private GameJackpotRedpacketService gameJackpotRedpacketService;
    @Autowired
    private MallSystemConfigService mallSystemConfigService;
    @Autowired
    private WxEnterpriseTransferService wxEnterpriseTransferService;
    @Autowired
    private IMallRedQrcodeRecordService qrcodeRecordService;
    @Autowired
    private BasedMQProducer _baseMQProducer;
    @Autowired
    private IGameRedHourRuleService redHourRuleService;
    @Autowired
    private WxRedService wxRedService;


    @Value("${mall.wx.mpappid}")
    private String appid;
    @Value("${weiRed.app}")
    private String app;
    @Value("${weiRed.privateKey}")
    private String privateKey;
    @Value("${weiRed.publicKey}")
    private String publicKey;
    @Value("${weiRed.qrcodeUrl}")
    private String requestUrl;

    /**
     * 查询未分配的红包信息
     */
    @GetMapping("/redpackt")
    public Object getGameJackpotRedpackt(@RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit, @RequestParam(defaultValue = "id") String sort,
                    @Order @RequestParam(defaultValue = "desc") String order) {
        GameJackpotRedpacket gameJackpotRedpacket = new GameJackpotRedpacket();
        gameJackpotRedpacket.setGjrStatus("0"); // 红包状态(0：未发送 1：已发送)
        List<GameJackpotRedpacket> gameJackpotRedpacketList = gameJackpotRedpacketService.selectGameJackpotRedpacketList(gameJackpotRedpacket, page, limit, sort, order);
        return ResponseUtil.ok(gameJackpotRedpacketList);
    }

    /**
     * 查询红包雨游戏列表
     *
     * @return
     */
    // @GetMapping("/rains")
    // public Object getGameRedpacketList(@LoginUser String userId) {
    // if (null == userId) {
    // return ResponseUtil.unlogin();
    // }
    //
    // try {
    // Map<Integer, Map<String, String>> map1 = new HashMap<>();
    // Map<String, String> map2 = new HashMap<>();
    // map2.put("next", "4425.65");
    // map2.put("teday", "13453");
    // Map<String, String> map3 = new HashMap<>();
    // map3.put("next", "3006.45");
    // map3.put("teday", "10243");
    // Map<String, String> map4 = new HashMap<>();
    // map4.put("next", "6026.45");
    // map4.put("teday", "10243");
    // Map<String, String> map5 = new HashMap<>();
    // map5.put("next", "4645.48");
    // map5.put("teday", "15246");
    // Map<String, String> map6 = new HashMap<>();
    // map6.put("next", "5645.48");
    // map6.put("teday", "17243");
    // Map<String, String> map7 = new HashMap<>();
    // map7.put("next", "8645.48");
    // map7.put("teday", "32243");
    // Map<String, String> map8 = new HashMap<>();
    // map8.put("next", "10645.48");
    // map8.put("teday", "20243");
    // map1.put(0, map2);
    // map1.put(1, map3);
    // map1.put(2, map4);
    // map1.put(3, map5);
    // map1.put(4, map6);
    // map1.put(5, map7);
    // map1.put(6, map8);
    //
    //
    // /** 查询在当前的这个时间段用户是否已经领取过整点红包了,主要考虑是领取完后数据在MQ端还没有消费，但是前段页面还可以继续再领取 **/
    // Map<String, Object> nowTimeRedRecord =
    // NewCacheInstanceServiceLoader.bizCache().get(JetcacheConstant.redItemCacheConstant(userId, ""));
    //
    // logger.info("查看缓存Key :" + JetcacheConstant.redItemCacheConstant(userId, ""));
    // String redpacketTime = LocalDate.now().toString();
    // List<GameRedpacketRain> list = gameRedpacketRainService.selectRedpacketRains(userId,
    // redpacketTime);
    // List<Map<String, Object>> lists = new ArrayList<>();
    // Map<String, Object> datamap = new HashMap<>();
    // final double d = Math.random();
    // final int i = (int) (d * 100);
    // for (GameRedpacketRain grain : list) {
    // Map<String, Object> map = new HashMap<>();
    // String endtime = grain.getEndTime();// 红包结束时间
    // LocalDate now = LocalDate.now();// 当前日期
    // String end = now.toString() + " " + endtime;
    //
    // DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    // LocalDateTime nowtime = LocalDateTime.now();// 当前时间
    // LocalDateTime endldt = LocalDateTime.parse(end, df);// 红包结束时间
    // // 红包未过期
    // // if (-1 == nowtime.compareTo(endldt)) {
    //
    // if (null == grain.getAmount()) {
    // map.put("money", 0);
    // map.put("status", 0);
    // } else {
    // map.put("money", grain.getAmount());
    // map.put("status", 1);
    // }
    //
    // /** redis中缓存的数据不为空，说明他实际上已经领取过了只是在MySQL数据库中没有入库数据，那么就使用redis的数据顶上 **/
    // if (MapUtils.isNotEmpty(nowTimeRedRecord)) {
    // /** 这里的判断逻辑主要是当redis中有数据，但是MySQL查询出来的数据为空的情况下，将上面的数据进行一个替换的动作 **/
    // String redpacketId = MapUtils.getString(nowTimeRedRecord, "redpacket_id");
    //
    // if (StringUtils.isNotBlank(redpacketId) && redpacketId.equals(grain.getId()) && null ==
    // grain.getAmount()) {
    // logger.info("~~~查看下缓存内的真实数据nowTimeRedRecord：" + JacksonUtils.bean2Jsn(nowTimeRedRecord));
    // logger.info("~~~查看下对比的数据grain：" + JacksonUtils.bean2Jsn(grain));
    // map.put("money", MapUtils.getString(nowTimeRedRecord, "amount"));
    // map.put("status", 1);
    // }
    // }
    // map.put("starttime", grain.getStartTime().substring(0, 5));
    // lists.add(map);
    // datamap.put("items", lists);
    // datamap.put("record", map1);
    //
    // // }
    // // 红包过期
    // // else {
    // // if (null == grain.getAmount()) {
    // // map.put("money", 0);
    // // map.put("status", 0);
    // // } else {
    // // map.put("money", grain.getAmount());
    // // map.put("status", 1);
    // // }
    // // map.put("starttime", grain.getStartTime().substring(0, 5));
    // //
    // // lists.add(map);
    // // datamap.put("items", lists);
    // // datamap.put("record", map1);
    // //
    // // }
    // }
    //
    // return ResponseUtil.ok(datamap);
    // } catch (Exception e) {
    // logger.error("整点红包查询异常: " + e.getMessage(), e);
    // return ResponseUtil.fail();
    // }
    // }

    /**
     * 查询红包雨游戏列表
     *
     * @return
     */
    @GetMapping("/rains")
    public Object getGameRedpacketList(@LoginUser String userId) {
        if (null == userId) {
            return ResponseUtil.unlogin();
        }

        try {
            Map<Integer, Map<String, String>> map1 = RedPacketBizUtils.build24RainsParamsMap();
            /** 查询在当前的这个时间段用户是否已经领取过整点红包了,主要考虑是领取完后数据在MQ端还没有消费，但是前段页面还可以继续再领取 **/
            Map<String, Object> nowTimeRedRecord = NewCacheInstanceServiceLoader.bizCache().get(JetcacheConstant.redItemCacheConstant(userId, ""));

            logger.info("查看缓存Key :" + JetcacheConstant.redItemCacheConstant(userId, ""));

            LocalDateTime startTime = LocalDateTime.of(LocalDate.now(), LocalTime.of(00, 00, 00));
            LocalDateTime endTime = LocalDateTime.of(LocalDate.now(), LocalTime.of(23, 59, 59));
            /** 红包列表 */
            List<GameRedpacketRain> rainList = gameRedpacketRainService.selectGameRedpacketList();
            /** 查询个人当天领取记录 */
            List<MallRedQrcodeRecord> list1 = qrcodeRecordService.selectUserList(userId, startTime.toString(), endTime.toString());
            List<Map<String, Object>> lists = new ArrayList<>();
            Map<String, Object> datamap = new HashMap<>();
            for (int i = 0; i < rainList.size(); i++) {
                Map<String, Object> redMap = new HashMap<>();
                String redId = rainList.get(i).getId();
                /** 有领取 */
                List<MallRedQrcodeRecord> collect = list1.stream().filter(mallRedQrcodeRecord -> mallRedQrcodeRecord.getRedpacketId().equals(redId)).collect(Collectors.toList());
                if (null != list1 && list1.size() > 0) {

                    /** 当前有领取 */
                    if (null != collect && collect.size() > 0) {
                        redMap.put("money", collect.get(0).getAmount());
                        redMap.put("status", 1);
                    } /** 当前没有领取 */
                    else {
                        redMap.put("money", 0);
                        redMap.put("status", 0);
                    }
                } /** 一次都没有领取 */
                else {
                    redMap.put("money", 0);
                    redMap.put("status", 0);
                }
                /** redis中缓存的数据不为空，说明他实际上已经领取过了只是在MySQL数据库中没有入库数据，那么就使用redis的数据顶上 **/
                if (MapUtils.isNotEmpty(nowTimeRedRecord)) {
                    /** 这里的判断逻辑主要是当redis中有数据，但是MySQL查询出来的数据为空的情况下，将上面的数据进行一个替换的动作 **/
                    String redpacketId = MapUtils.getString(nowTimeRedRecord, "redpacket_id");

                    if (StringUtils.isNotBlank(redpacketId) && redpacketId.equals(rainList.get(i).getId())) {
                        logger.info("~~~查看下缓存内的真实数据nowTimeRedRecord：" + JacksonUtils.bean2Jsn(nowTimeRedRecord));
                        logger.info("~~~查看下对比的数据grain：" + JacksonUtils.bean2Jsn(rainList.get(i)));
                        redMap.put("money", MapUtils.getString(nowTimeRedRecord, "amount"));
                        redMap.put("status", 1);
                    }
                }
                redMap.put("starttime", rainList.get(i).getStartTime().substring(0, 5));
                lists.add(redMap);
            }
            datamap.put("items", lists);
            datamap.put("record", map1);
            return ResponseUtil.ok(datamap);
        } catch (Exception e) {
            logger.error("整点红包查询异常: " + e.getMessage(), e);
            return ResponseUtil.fail();
        }
    }

    /**
     * 添加红包
     *
     * @param
     * @throws IOException
     */
    @PostMapping("/add")
    public Object addGameRedpacketRain(@RequestBody GameRedpacketRain gameRedpacketRain) {
        int redpacket = gameRedpacketRainService.insertGameRedpacket(gameRedpacketRain);

        if (redpacket > 0) {
            return ResponseUtil.ok();
        }

        return ResponseUtil.fail(403, "添加失败！");
    }

    /**
     * 修改红包
     *
     * @param @throws
     */
    @PostMapping("/edit")
    public Object editRedpacketRain(@RequestBody GameRedpacketRain gameRedpacketRain) {
        int redpacket = gameRedpacketRainService.editGameRedpacket(gameRedpacketRain);
        if (redpacket > 0) {
            return ResponseUtil.ok();
        }

        return ResponseUtil.fail(403, "修改失败！");

    }

    /**
     * 删除红包
     *
     * @param
     * @throws IOException
     */
    @GetMapping("/delete/{redId}")
    public Object deleteRedpacket(@PathVariable("redId") String redId) {
        int redpacket = gameRedpacketRainService.deleteGameRedpacket(redId);
        if (redpacket > 0) {
            return ResponseUtil.ok();
        }

        return ResponseUtil.fail(403, "删除失败！");
    }

    /**
     * 整点红包弹幕
     *
     * @param
     * @throws IOException
     */
    @GetMapping("/barrage")
    public Object barrageList() {
        return ResponseUtil.ok(gameRedpacketRainService.barrageList());
    }

    /**
     * 整点红包弹幕+疯抢排行
     *
     * @param
     * @throws IOException
     */
    @GetMapping("/barrage/V2")
    public Object barrageList(@LoginUser String userId) {
        if (null == userId) {
            return ResponseUtil.unlogin();
        }
        /** 弹幕列表 */
        List<Map<String, Object>> maps = gameRedpacketRainService.barrageList();
        /** 排行榜 */
        List<Map<String, Object>> list1 = gameRedpacketRainService.barrageList1();
        /** 当日累计领取 */
        BigDecimal totalAmountByUser = qrcodeRecordService.getTotalAmountByUser(userId);
        if (null != maps && maps.size() > 0) {
            for (Map<String, Object> map : maps) {
                String nickname = NameUtil.nameDesensitizationDef(String.valueOf(map.get("nickname")));
                if ("".equals(nickname)) {
                    map.put("nickname", NameUtil.nameDesensitizationDef(String.valueOf(map.get("mobile"))));
                } else {
                    map.put("nickname", nickname);
                }
            }
        }
        if (null != list1 && list1.size() > 0) {
            for (Map<String, Object> map : list1) {
                String nickname = NameUtil.nameDesensitizationDef(String.valueOf(map.get("nickname")));
                if ("".equals(nickname)) {
                    map.put("nickname", NameUtil.nameDesensitizationDef(String.valueOf(map.get("mobile"))));
                } else {
                    map.put("nickname", nickname);
                }
            }
        }
        Map<String, Object> map = new HashMap<>();
        map.put("barrage", maps);
        map.put("list", list1);
        map.put("totalAmount", totalAmountByUser);
        return ResponseUtil.ok(map);
    }

    /**
     * 领取整点红包
     */
    @GetMapping("/receive008ii")
    public Object receive(@LoginUser String userId, @RequestParam String time, @RequestParam(defaultValue = "") String makerId, HttpServletRequest request) throws Exception {

        if (null == userId) {
            return ResponseUtil.unlogin();
        }


        try {
            synchronized (MutexLockUtils.getMutexLock(userId)) {
                String device = "";
                String userAgent = getRequest().getHeader("user-agent");
                if (null != userAgent && userAgent.contains("iPhone")) {
                    device = "iPhone";
                } else {
                    device = "Android";
                }
                /** 查询当前时间段的红包总金额 **/
                String time1 = time + ":00";
                GameRedpacketRain grain = gameRedpacketRainService.selectGameRedpacket(time1);
                /** 当前时间段有没有领取过 **/
                /**
                 * 查询该时间段十分已经领取过红包
                 */
                MallRedQrcodeRecord qrRedRecord = qrcodeRecordService.getOne(new QueryWrapper<MallRedQrcodeRecord>().eq("to_user_id", userId).eq("type_of_claim", "01").eq("type", "02").eq("redpacket_id", grain.getId())
                                 .eq("time_mark", DateUtils.getNowTimeHour(time)));
                
                
//                int receiveNumber = qrcodeRecordService.count(new QueryWrapper<MallRedQrcodeRecord>().eq("to_user_id", userId).eq("type_of_claim", "01").eq("type", "02").eq("redpacket_id", grain.getId())
//                                .between("create_time", LocalDateTime.of(LocalDateTime.now().toLocalDate(), LocalTime.MIN), LocalDateTime.of(LocalDateTime.now().toLocalDate(), LocalTime.MAX)));

                /** 说明该时间段已经领取过红包 **/
                if (qrRedRecord!=null) {
                    return ResponseUtil.fail(403, "您在该时间段已经领取过红包！等待下一个时间段再次领取吧");
                }

                /** 获取整点红包配置规则 **/
                List<MallSystemRule> listRule = redHourRuleService.list(new QueryWrapper<MallSystemRule>().eq("rule_id", 1).eq("status", 0));
                /** 通过规则随机获取红包点中的段位 **/
                int redIndex = RandomUtils.getRedIndex(listRule);
                /** 取出具体的配置段 **/
                MallSystemRule mallSystemRule = listRule.get(redIndex);

                /** 为了兼容老规则 **/
                Map<String, String> map = mallSystemConfigService.redPacketRule();
                /** 整点红包区间1 给一个初始值防止出错后没有值 **/
                String packet1 = "0.01";
                /** 整点红包区间2 给一个初始值防止出错后没有值 **/
                String packet2 = "0.01";
                String rule = map.get("mall_red_rule");

                /** 看当前启用的是新规则还是旧规则 1 用配置表 2 新的规则 **/
                if ("1".equals(rule)) {
                    packet1 = map.get("mall_red_packet1");
                    packet2 = map.get("mall_red_packet2");
                } else {
                    packet1 = mallSystemRule.getStartSection().toString();
                    packet2 = mallSystemRule.getEndSection().toString();
                }
                MallUser mallUser = mallUserService.findById(userId);
                /** 通过红包规则段获取最终领取金额 **/
                Random random = new Random();
                DecimalFormat df1 = new DecimalFormat("0.00");
                // 原金额
                BigDecimal residuer = new BigDecimal(df1.format(random.nextDouble() * (Double.parseDouble(packet2) - Double.parseDouble(packet1)) + Double.parseDouble(packet1)));
                // 达人金额
                BigDecimal talResiduer = residuer;
                /** 判断是否达人 */
                if ("1".equals(mallUser.getIsTalent())) {
                    // （新：新的规则表）(保留两位)
                    talResiduer = residuer.multiply(new BigDecimal(mallSystemRule.getReserveTwo())).setScale(2, BigDecimal.ROUND_DOWN);
                }
                Map<String, Object> datamap = new HashMap<>();
                String endtime = grain.getEndTime();// 红包结束时间
                LocalDate now = LocalDate.now();// 当前日期
                String end = now.toString() + " " + endtime;

                DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                LocalDateTime nowtime = LocalDateTime.now();// 当前时间

                String time2 = now.toString() + " " + time + ":00";
                LocalDateTime endldt1 = LocalDateTime.parse(time2, df);// 红包时间

                /** 判断当前时间段是否超时 **/
                if (nowtime.isBefore(endldt1)) {
                    return ResponseUtil.fail(403, "已超过领取时间！");
                }

                String cacheKey = JetcacheConstant.redCacheConstant();
                // "wred" + GraphaiIdGenerator.nextId("");
                String recordCahceKey = JetcacheConstant.redItemCacheConstant(userId, time);

                /** 业务执行所需传递必要参数 **/
                Map<String, Object> bizmap = new HashMap<>(10);
                bizmap.put("device", device);
                //bizmap.put("mall_red_rule", map.get("mall_red_rule"));
                bizmap.put("version", getRequest().getHeader("version"));
                bizmap.put("ip", IpUtils.getIpAddress(getRequest()));
                bizmap.put("userId", userId);
                bizmap.put("makerId", makerId);
                // 原金额 用来直推人收益计算用
                bizmap.put("residuerStr", residuer.toString());
                // 乘达人倍率后的金额
                bizmap.put("talResiduer", talResiduer.toString());
                bizmap.put("grainId", grain.getId());
                bizmap.put("ruleId", mallSystemRule.getId());
                bizmap.put("timeMark", DateUtils.getNowTimeHour(time));
                
                /** 将参数投递入缓存 **/

                logger.info("投递业务缓存的key : " + cacheKey);
                NewCacheInstanceServiceLoader.bizCache().put(cacheKey, bizmap);


                Map<String, Object> recordmap = new HashMap<>();
                recordmap.put("redpacket_id", grain.getId());
                recordmap.put("amount", talResiduer);
                recordmap.put("create_time", LocalDateTime.now());
                recordmap.put("to_user_id", userId);
                recordmap.put("nickname", mallUser.getNickname());
                recordmap.put("avatar", mallUser.getAvatar());
                NewCacheInstanceServiceLoader.bizCache().put(recordCahceKey, recordmap);
                /** 将缓存ID投递入MQ队列 **/
                // wxProducerService.produceMsg(JacksonUtils.bean2Jsn(bizmap), cacheKey,
                // MQBaseConstant.RED_WHOLE_ENVELOPE_TAG);

                Map<String, Object> mqParams = new HashMap<>();
                mqParams.put("param", JacksonUtils.bean2Jsn(bizmap));
                mqParams.put("bizKey", cacheKey);
                mqParams.put("tags", MQBaseConstant.RED_WHOLE_ENVELOPE_TAG);
                _baseMQProducer.send(mqParams);
                // wxProducerService.produceMsg(cacheKey, cacheKey, MQBaseConstant.RED_WHOLE_ENVELOPE_TAG);
                /** 触发红包领取逻辑 做实际的入库操作 **/
                // gameRedComponent.receive(userId, makerId, mallSystemRule, residuer, grain, map);
                /** 移除mutexlock **/
                MutexLockUtils.remove(userId);
                datamap.put("status", 1);
                datamap.put("money", talResiduer);
                return ResponseUtil.ok(datamap);
            }
        } catch (Exception e) {
            logger.error("领取红包异常：" + e.getMessage(), e);

            return ResponseUtil.fail("红包领取失败");
        }

    }



    /**
     * 领取整点红包
     */
    @PostMapping("/receive/V2")
    public Object receiveV2(@LoginUser String userId, @RequestBody String body) throws Exception {

        if (null == userId) {
            return ResponseUtil.unlogin();
        }
        
        String params = JacksonUtil.parseString(body, "params");
        try {
            synchronized (MutexLockUtils.getMutexLock(userId)) {
                Map<String, String> mapRequest = RSA.getSignAlgorithms(params);
                String device = "";
                String userAgent = getRequest().getHeader("user-agent");
                String xMallAd = getRequest().getHeader("X-Mall-Ad");
                String markerId = mapRequest.get("makerId");
                if (null != userAgent && userAgent.contains("iPhone")) {
                    device = "iPhone";
                } else {
                    device = "Android";
                }
                /** 查询当前时间段的红包总金额 **/
                String time = mapRequest.get("time");
                String time1 = time + ":00";
                /** 时间戳用来作为 AES密码 */
                String timestamp = mapRequest.get("timestamp");
                String param = "xxx" + timestamp;
                String string = AESUtils.decodeFromBase64String(xMallAd, param.getBytes(), null);
                if (!StrUtil.isBlank(string) && string.contains("WLSH")) {

                    GameRedpacketRain grain = gameRedpacketRainService.selectGameRedpacket(time1);
                    /** 当前时间段有没有领取过 **/
//                    int receiveNumber = qrcodeRecordService.count(new QueryWrapper<MallRedQrcodeRecord>().eq("to_user_id", userId).eq("type_of_claim", "01").eq("type", "02").eq("redpacket_id", grain.getId())
//                                    .between("create_time", LocalDateTime.of(LocalDateTime.now().toLocalDate(), LocalTime.MIN), LocalDateTime.of(LocalDateTime.now().toLocalDate(), LocalTime.MAX)));
//                    
                    MallRedQrcodeRecord qrRedRecord = qrcodeRecordService.getOne(new QueryWrapper<MallRedQrcodeRecord>().eq("to_user_id", userId).eq("type_of_claim", "01").eq("type", "02").eq("redpacket_id", grain.getId())
                                    .eq("time_mark", DateUtils.getNowTimeHour(time)));
                    
                    
                    logger.info("调用的V2版本接口.................");
                    /** 说明该时间段已经领取过红包 **/
                    if (qrRedRecord != null) {
                        return ResponseUtil.fail(403, "您在该时间段已经领取过红包！等待下一个时间段再次领取吧");
                    }
                    
                    /** 当天领取多少次 **/
                    int receiveDayNumber = qrcodeRecordService.count(new QueryWrapper<MallRedQrcodeRecord>().eq("to_user_id", userId).eq("type_of_claim", "01").eq("type", "02").between("create_time",
                                    LocalDateTime.of(LocalDateTime.now().toLocalDate(), LocalTime.MIN), LocalDateTime.of(LocalDateTime.now().toLocalDate(), LocalTime.MAX)));
                    
                    MallUser mallUser = mallUserService.findById(userId);
                    /** 获取整点红包配置规则 **/
                    List<MallSystemRule> listRule = redHourRuleService.list(new QueryWrapper<MallSystemRule>().eq("rule_id", 1).eq("status", 0));
                    /** 通过规则随机获取红包点中的段位 **/
                    MallSystemRule mallSystemRule = wxRedService.getRedIndex(listRule, mallUser.getIsTalent(), receiveDayNumber);

                    /** 为了兼容老规则 **/
                    Map<String, String> map = mallSystemConfigService.redPacketRule();
                    /** 整点红包区间1 给一个初始值防止出错后没有值 **/
                    String packet1 = "0.01";
                    /** 整点红包区间2 给一个初始值防止出错后没有值 **/
                    String packet2 = "0.01";
                    String rule = map.get("mall_red_rule");

                    /** 看当前启用的是新规则还是旧规则 1 用配置表 2 新的规则 **/
                    if ("1".equals(rule)) {
                        packet1 = map.get("mall_red_packet1");
                        packet2 = map.get("mall_red_packet2");
                    } else {
                        packet1 = mallSystemRule.getStartSection().toString();
                        packet2 = mallSystemRule.getEndSection().toString();
                    }
                    /** 通过红包规则段获取最终领取金额 **/
                    Random random = new Random();
                    DecimalFormat df1 = new DecimalFormat("0.00");
                    // 原金额
                    BigDecimal residuer = new BigDecimal(df1.format(random.nextDouble() * (Double.parseDouble(packet2) - Double.parseDouble(packet1)) + Double.parseDouble(packet1)));
                    // 达人金额
                    BigDecimal talResiduer = residuer;
                    /** 判断是否达人 */
                    if (StringUtils.isNotBlank(mallUser.getIsTalent()) && "1".equals(mallUser.getIsTalent())) {
                        // （新：新的规则表）(保留两位)
                        // talResiduer = residuer.multiply(new BigDecimal(mallSystemRule.getReserveTwo())).setScale(2,
                        // BigDecimal.ROUND_DOWN);
                        talResiduer = residuer.setScale(2, BigDecimal.ROUND_DOWN);
                    }
                    Map<String, Object> datamap = new HashMap<>();
                    String endtime = grain.getEndTime();// 红包结束时间
                    LocalDate now = LocalDate.now();// 当前日期
                    String end = now.toString() + " " + endtime;

                    DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                    LocalDateTime nowtime = LocalDateTime.now();// 当前时间

                    String time2 = now.toString() + " " + time + ":00";
                    LocalDateTime endldt1 = LocalDateTime.parse(time2, df);// 红包时间

                    /** 判断当前时间段是否超时 **/
                    if (nowtime.isBefore(endldt1)) {
                        return ResponseUtil.fail(403, "已超过领取时间！");
                    }
                    
                    /** 获取直推人信息，如果没有直推人那么就将红包分享人绑定成为直推人，如果没有分享人则直接省略 **/
                    MallUser user1 = mallUserService.bindUserDirectLeader(mallUser, markerId);
                    BigDecimal makerRes = RedPacketBizUtils.getDirectLeaderProfit(user1, mallSystemRule, residuer);
                    
                    String cacheKey = JetcacheConstant.redCacheConstant();
                    String recordCahceKey = JetcacheConstant.redItemCacheConstant(userId, time);
                    /** 业务执行所需传递必要参数 **/
                    Map<String, Object> bizmap = new HashMap<>(10);
                    bizmap.put("device", device);
                    //bizmap.put("mall_red_rule", map.get("mall_red_rule"));
                    bizmap.put("version", getRequest().getHeader("version"));
                    bizmap.put("ip", IpUtils.getIpAddress(getRequest()));
                    bizmap.put("userId", userId);
                    bizmap.put("makerId", markerId);
                    // 原金额 用来直推人收益计算用
                    bizmap.put("residuerStr", residuer.toString());
                    // 乘达人倍率后的金额
                    bizmap.put("talResiduer", talResiduer.toString());
                    bizmap.put("grainId", grain.getId());
                    bizmap.put("ruleId", mallSystemRule.getId());
                    bizmap.put("timeMark", DateUtils.getNowTimeHour(time));
                    bizmap.put("xMallAd", string);
                    bizmap.put("fansName", StringUtils.isNotBlank(mallUser.getNickname()) ? mallUser.getNickname() : mallUser.getMobile());
                    bizmap.put("makerRes", makerRes.toString());
                    bizmap.put("msgToUserId",user1!=null? user1.getId(): "0");
                    
                    /** 将参数投递入缓存 **/
                    Map<String, Object> recordmap = new HashMap<>();
                    recordmap.put("redpacket_id", grain.getId());
                    recordmap.put("amount", talResiduer);
                    recordmap.put("create_time", LocalDateTime.now());
                    recordmap.put("to_user_id", userId);
                    recordmap.put("nickname", mallUser.getNickname());
                    recordmap.put("avatar", mallUser.getAvatar());
                    NewCacheInstanceServiceLoader.bizCache().put(recordCahceKey, recordmap);
                    /** 将缓存ID投递入MQ队列 **/
                    // wxProducerService.produceMsg(JacksonUtils.bean2Jsn(bizmap), cacheKey,
                    // MQBaseConstant.RED_WHOLE_ENVELOPE_TAG);

                    Map<String, Object> mqParams = new HashMap<>();
                    mqParams.put("param", JacksonUtils.bean2Jsn(bizmap));
                    mqParams.put("bizKey", cacheKey);
                    mqParams.put("tags", MQBaseConstant.RED_WHOLE_ENVELOPE_TAG);
                    _baseMQProducer.send(mqParams);
                   
                    /**将其他事务进行拆离**/
                    Map<String, Object> mqPvParams = new HashMap<>();
                    mqPvParams.put("param", JacksonUtils.bean2Jsn(bizmap));
                    mqPvParams.put("bizKey", GraphaiIdGenerator.nextId(""));
                    mqPvParams.put("tags", MQBaseConstant.COMMON_PAGE_VIEW_TAG);
                    _baseMQProducer.send(mqPvParams);
                    
                    // wxProducerService.produceMsg(cacheKey, cacheKey, MQBaseConstant.RED_WHOLE_ENVELOPE_TAG);
                    /** 触发红包领取逻辑 做实际的入库操作 **/
                    // gameRedComponent.receive(userId, makerId, mallSystemRule, residuer, grain, map);
                    /** 移除mutexlock **/
                    MutexLockUtils.remove(userId);
                    datamap.put("status", 1);
                    datamap.put("money", talResiduer);
                    return ResponseUtil.ok(datamap);
                } else {
                    return ResponseUtil.fail("红包领取失败");
                }
            }
        } catch (Exception e) {
            logger.error("领取红包异常：" + e.getMessage(), e);

            return ResponseUtil.fail("红包领取失败");
        }

    }

    /**
     * @note 红包详情
     *
     */
    @SuppressWarnings("unchecked")
    @GetMapping("/detail")
    public Object getRedpacketDetail(@LoginUser String userId, @RequestParam String time, @RequestParam(defaultValue = "") String createTime, @RequestParam(defaultValue = "1") String page,
                    @RequestParam(defaultValue = "50") String limit) {
        try {
            if (null == userId) {
                return ResponseUtil.unlogin();
            }
            int page1 = Integer.parseInt(page);
            int limit1 = Integer.parseInt(limit);
            LocalDate local = LocalDate.now();
            String time1 = time + ":00";
            GameRedpacketRain grain = gameRedpacketRainService.selectGameRedpacket(time1);
            String redId = "";
            if (null != grain) {
                redId = grain.getId();
            }
            Map<String, String> mapRule = mallSystemConfigService.queryMaker();
            // 红包开始时间
            String redpacketsTime = local.toString() + " " + time + ":00";
            // 红包结束时间
            String redpacketeTime = local.toString() + " " + grain.getEndTime();
            /** 当前时间所有领取记录 */
//            List<RedPacketDetailsVo> list = gameRedPacketRecordService.selectGameRedpacketDetail(redId, redpacketsTime, redpacketeTime, page1, limit1, createTime, "");
            
            Map<String,Object> objmap = gameRedPacketRecordService.selectGameRedpacketDetailV2(redId, redpacketsTime, redpacketeTime, page1, limit1, "", "",createTime);
            long total = MapUtils.getLongValue(objmap, "total");
            List<RedPacketDetailsVo> list = (List<RedPacketDetailsVo>) objmap.get("voList");
            /**倒序*/
            List<RedPacketDetailsVo> collect = list.stream().sorted(Comparator.comparing(RedPacketDetailsVo::getCreateTime).reversed()).collect(Collectors.toList());
            /** 当前自己领取记录 */
            //List<RedPacketDetailsVo> listOwn = gameRedPacketRecordService.selectGameRedpacketDetail(redId, redpacketsTime, redpacketeTime, page1, limit1, createTime, userId);
            String nowDate = DateUtils.getNowTimeHour(time);
            Map<String,Object> objmap1 =  gameRedPacketRecordService.selectGameRedpacketDetailV2(redId, redpacketsTime, redpacketeTime, null, null, nowDate, userId,null);
            List<RedPacketDetailsVo> listOwn = (List<RedPacketDetailsVo>) objmap1.get("voList");
            
//            List<RedPacketDetailsVo> list2 = new ArrayList<>();
            Map<String, Object> map = new HashMap<>();
//            if (CollectionUtils.isNotEmpty(list)) {
//                for (RedPacketDetailsVo record : list) {
//                    String nick = record.getNickname();
//                    record.setNickname(NameUtil.nicknameDesensitization(nick));
//                    record.setAmount(record.getAmount().setScale(2, BigDecimal.ROUND_DOWN));
//                    list2.add(record);
//                }
//            }
            // 这个list只是为了兼容app数据格式
            List<RedPacketDetailsVo> list3 = new ArrayList<>();
            if (null != listOwn && listOwn.size() > 0) {
                listOwn.get(0).setAmount(listOwn.get(0).getAmount().setScale(2, BigDecimal.ROUND_DOWN));
                list3.add(listOwn.get(0));
            } else {
                String recordCahceKey = JetcacheConstant.redItemCacheConstant(userId, time);
                /** 这里在抢完红包后,如果缓存也没有数据该怎么办?? 暂时先不考虑吧 **/

                Map<String, Object> stringObjectMap = NewCacheInstanceServiceLoader.bizCache().get(recordCahceKey);
                if (null != stringObjectMap) {
                    RedPacketDetailsVo redPacketDetailsVo = BeanUtil.mapToBean(stringObjectMap, RedPacketDetailsVo.class, true, CopyOptions.create());
                    redPacketDetailsVo.setAmount(redPacketDetailsVo.getAmount().setScale(2, BigDecimal.ROUND_DOWN));
                    list3.add(redPacketDetailsVo);
                }
                logger.info("红包item:来查询缓存内的数据.........");
            }
            map.put("total", total);
            map.put("own", list3);
            map.put("other", collect);
            map.put("isShare", mapRule.get("mall_maker_isShare"));
            return ResponseUtil.ok(map);
        } catch (Exception ex) {
            logger.error("红包详情报错信息：" + ex.getMessage(), ex);
            return ResponseUtil.badArgument();
        }
    }


    /**
     * 红包领取记录
     */
    @RequestMapping("/relist")
    public Object getRedpacketDetail(@LoginUser String userId) {
        if (null == userId) {
            return ResponseUtil.unlogin();
        }
        List<Map> list = gameRedPacketRecordService.selectByRedpacketlists(userId);
        Map map = gameRedPacketRecordService.selectByRedpacketSum(userId);
        Map dataMap = new HashMap();
        if (null != map && !map.isEmpty()) {
            dataMap.put("sum", map.get("amount"));
        } else {
            dataMap.put("sum", "0.00");
        }
        dataMap.put("list", list);
        return ResponseUtil.ok(dataMap);
    }



    @PostMapping("wxTest")
    public Object wxTest(@RequestBody String body) {
        String appId = JacksonUtil.parseString(body, "appId");
        String openId = JacksonUtil.parseString(body, "openId");
        String orderId = JacksonUtil.parseString(body, "orderId");
        String amount = JacksonUtil.parseString(body, "amount");
        String reason = JacksonUtil.parseString(body, "reason");
        return wxEnterpriseTransferService.transfer2SmallChange(appId, openId, orderId, amount, reason);
    }



}
