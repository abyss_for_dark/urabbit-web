package com.graphai.mall.wx.vo;

import lombok.Data;

/**
 * @Author: jw
 * @Date: 2021/1/30 16:06
 */
 @Data
public class TuiAResultParams {
     private String activityUrl ;
     private String imageUrl ;
     private Long sckId ;
     private String reportClickUrl ;
     private String reportExposureUrl ;
     private String extTitle ;
     private String extDesc ;
     private String size ;
     private Boolean isVisibleOfIcon ;
     private Boolean isVisibleOfCloseButton ;
     private Boolean isSdkType ;

}
