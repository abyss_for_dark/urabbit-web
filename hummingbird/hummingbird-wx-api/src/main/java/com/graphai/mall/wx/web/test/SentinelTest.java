package com.graphai.mall.wx.web.test;

import com.alibaba.csp.sentinel.Entry;
import com.alibaba.csp.sentinel.SphO;
import com.alibaba.csp.sentinel.SphU;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.alibaba.csp.sentinel.slots.block.RuleConstant;
import com.alibaba.csp.sentinel.slots.block.flow.FlowRule;
import com.alibaba.csp.sentinel.slots.block.flow.FlowRuleManager;
import com.graphai.commons.util.ResponseUtil;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/Sentinel/test")
@Validated
public class SentinelTest {
    public static void main(String[] args) {
//        // 配置规则
//        List<FlowRule> rules = new ArrayList<>();
//        FlowRule rule = new FlowRule();
//        rule.setResource("tutorial");
//        // QPS 不得超出 1
//        rule.setCount(2);
//        rule.setGrade(RuleConstant.FLOW_GRADE_QPS);
//        rule.setLimitApp("default");
//        rules.add(rule);
//        // 加载规则
//        FlowRuleManager.loadRules(rules);
//        // 下面开始运行被限流作用域保护的代码
//        while (true) {
//            Entry entry = null;
//            try {
//                entry = SphU.entry("tutorial");
//                System.out.println("hello world");
//            } catch (BlockException e) {
//                System.out.println("blocked");
//            } finally {
//                if (entry != null) {
//                    entry.exit();
//                }
//            }
//            try {
//                Thread.sleep(300);
//            } catch (InterruptedException e) {}
//        }

//        // 配置规则
//        List<FlowRule> rules = new ArrayList<>();
//        FlowRule rule = new FlowRule();
//        rule.setResource("tutorial");
//        // QPS 不得超出 1
//        rule.setCount(40);
//        rule.setGrade(RuleConstant.FLOW_GRADE_QPS);
//        rule.setLimitApp("default");
//        rules.add(rule);
//        FlowRuleManager.loadRules(rules);
//        // 运行被限流作用域保护的代码
//        while (true) {
//            if (SphO.entry("tutorial")) {
//                try {
//                    System.out.println("hello world");
//                } finally {
//                    SphO.exit();
//                }
//            } else {
//                System.out.println("blocked");
//            }
//            try {
//                Thread.sleep(10);
//            } catch (InterruptedException e) {}
//        }


    }

    @RequestMapping("/sen")
    public Object testSentinel(){
        // 配置规则
        List<FlowRule> rules = new ArrayList<>();
        FlowRule rule = new FlowRule();
        rule.setResource("tutorial");
        // QPS 不得超出 1
        rule.setCount(400);
        rule.setGrade(RuleConstant.FLOW_GRADE_QPS);
        rule.setLimitApp("default");
        rules.add(rule);
        FlowRuleManager.loadRules(rules);
        if (SphO.entry("tutorial")) {
            try {
                System.out.println("hello world");
            } finally {
                SphO.exit();
            }
        } else {
            System.out.println("blocked");
            return ResponseUtil.fail("活动太火爆，请稍后重试！");
        }
        return ResponseUtil.ok();
    }
}
