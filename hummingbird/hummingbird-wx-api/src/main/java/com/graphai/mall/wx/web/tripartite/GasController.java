package com.graphai.mall.wx.web.tripartite;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.graphai.commons.constant.MallConstant;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.SecurityUtil;
import com.graphai.commons.util.httprequest.HttpUtil;
import com.graphai.commons.util.httprequest.LSXDUtils;
import com.graphai.id.GraphaiIdGenerator;
import com.graphai.mall.core.qianzhu.kfc.boot.util.Md5Utils;
import com.graphai.mall.db.dao.CustomMallGasMapper;
import com.graphai.mall.db.dao.MallGasMapper;
import com.graphai.mall.db.dao.MallOliMapper;
import com.graphai.mall.db.domain.MallGas;
import com.graphai.mall.db.domain.MallOli;
import com.graphai.mall.db.domain.MallOliExample;
import com.graphai.mall.db.domain.MallUser;
import com.graphai.mall.db.service.gasoline.MallGasService;
import com.graphai.mall.db.service.gasoline.MallOliService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.util.StringHelper;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.nosql.redis.NewCacheInstanceServiceLoader;

import cn.hutool.core.util.ObjectUtil;
import lombok.extern.slf4j.Slf4j;

@Slf4j(topic = "GasController")
@RestController
@RequestMapping("/wx/gas")
public class GasController {
    @Autowired
    private MallUserService userService;
    @Autowired
    private MallGasMapper mallGasMapper;
    @Autowired
    private MallOliMapper mallOliMapper;

    // @Autowired
    // private MongoTemplate mongoTemplate;
    @Autowired
    private CustomMallGasMapper customMallGasMapper;

    @Autowired
    private MallGasService mallGasService;

    @Autowired
    private MallOliService mallOliService;
    

    public static void main(String[] args) {


    }



    /**
     * 获取小桔token
     *
     * @return token
     */
    public String getXiaoJuToken() {

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("appSecret", SecurityUtil.appSecret);

        try {

            String jsonParam = SecurityUtil.encryptSignRequest(map);

            String result = HttpUtil.sendPostJson(SecurityUtil.requestUrl + "/queryToken", jsonParam);

            JSONObject object = JSONObject.parseObject(result);
            if (Objects.equals("0", object.get("code"))) {
                String respoString = SecurityUtil.decryptResponse(object.getString("data"));
                if (StringHelper.isNotNullAndEmpty(respoString)) {
                    JSONObject obj = JSONObject.parseObject(respoString);
                    if (!Objects.equals(null, obj.getString("accessToken"))) {
                        NewCacheInstanceServiceLoader.xiaojuCache().put("xiaojuToken", obj.getString("accessToken"));
                        return obj.getString("accessToken");
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }



    /**
     * 用户授权登录
     *
     * @param userId 用户id
     * @return token
     */
    public boolean getToken(LocalDateTime tokenTime, String mobile, String userId) {


        // 若token生成的时间小于当前时间21天则重新授权
        Date nowDate = new Date();

        int days = 0;
        // 将LocalDateTime转成date
        if (tokenTime != null) {
            ZoneId zoneId = ZoneId.systemDefault();
            ZonedDateTime zdt = tokenTime.atZone(zoneId);
            Date tokenDate = Date.from(zdt.toInstant());
            days = (int) ((nowDate.getTime() - tokenDate.getTime()) / (1000 * 3600 * 24));
        }


        try {

            if (days >= 21 || null == tokenTime) {
                Map<String, String> map = new HashMap<String, String>();
                map.put("app_key", MallConstant.GAS_KEY);
                map.put("platformType", MallConstant.GAS_platform_TYPE);
                map.put("platformCode", mobile);
                map.put("timestamp", String.valueOf(System.currentTimeMillis()));

                String signString = MallConstant.GAS_SECRET + "app_key" + map.get("app_key") + "platformType" + map.get("platformType") + "platformCode" + map.get("platformCode") + "timestamp" + map.get("timestamp")
                                + MallConstant.GAS_SECRET;

                map.put("sign", Md5Utils.md5(signString));

                String result = HttpUtil.sendPost("https://mcs.czb365.com/services/v3/begin/platformLoginSimpleAppV4", map);

                JSONObject object = JSONObject.parseObject(result);
                if (Objects.equals(object.getString("code"), "200")) {
                    MallUser user = userService.findById(userId);
                    if (ObjectUtil.isNotNull(user)) {
                        user.setGasTokenAddTime(LocalDateTime.now());
                        user.setUpdateTime(LocalDateTime.now());
                        userService.updateById(user);
                        return true;
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;

    }

    /**
     * 初始化油站数据
     *
     * @param userId 用户id
     * @return token
     */
    @GetMapping("/getGasList")
    public Object getGasListData() {

        try {

            Map<String, String> map = new LinkedHashMap<String, String>();
            map.put("app_key", MallConstant.GAS_KEY);
            map.put("channelId", MallConstant.GAS_platform_TYPE);
            map.put("timestamp", String.valueOf(System.currentTimeMillis()));

            String signString = MallConstant.GAS_SECRET + "app_key" + map.get("app_key") + "channelId" + map.get("channelId") + "timestamp" + map.get("timestamp") + MallConstant.GAS_SECRET;

            map.put("sign", DigestUtils.md5Hex(signString));

            // String result =
            // HttpUtils.post("https://test-mcs.czb365.com/services/v3/gas/queryGasInfoListOilNoNew?app_key="+map.get("app_key")+"&channelId="+
            // map.get("channelId")+"&timestamp="+map.get("timestamp")+"&sign="+map.get("sign"),
            // null, null);

            String result = HttpUtil.sendPost("https://mcs.czb365.com/services/v3/gas/queryGasInfoListOilNoNew", map);
            // String result =
            // HttpUtils.post("https://test-mcs.czb365.com/services/v3/gas/queryGasInfoListOilNoNew",
            // map,null);
            // String result =
            // LSXDUtils.sendPost("https://test-mcs.czb365.com/services/v3/gas/queryGasInfoListOilNoNew",
            // map);
            // String result2 =
            // HttpPostUtil.postForward("https://test-mcs.czb365.com/services/v3/gas/queryGasInfoListOilNoNew",
            // map);

            JSONObject object = JSONObject.parseObject(result);

            if (Objects.equals(object.getString("code"), "200")) {

                JSONArray array = JSONArray.parseArray(object.get("result").toString());
                if (CollectionUtils.isNotEmpty(array)) {
                    for (int i = 0; i < array.size(); i++) {
                        JSONObject ob = (JSONObject) array.get(i);

                        // 查询是否已存在油站记录
                        // MallGasExample example = new MallGasExample();
                        // example.or().andGasIdEqualTo(ob.getString("gasId")).andDeletedEqualTo(false);
                        // List<MallGas> gaslList = mallGasMapper.selectByExample(example);
                        //
                        // if (CollectionUtils.isEmpty(gaslList)) {
                        MallGas gas = new MallGas();
                        gas.setId(GraphaiIdGenerator.nextId("MallGas"));
                        gas.setGasId(ob.getString("gasId"));
                        gas.setGasName(ob.getString("gasName"));
                        gas.setGasType(ob.getInteger("gasType"));
                        gas.setProvinceCode(ob.getInteger("provinceCode"));
                        gas.setProvinceName(ob.getString("provinceName"));
                        gas.setCityCode(ob.getInteger("cityCode"));
                        gas.setCityName(ob.getString("cityName"));
                        if (ObjectUtil.isNotNull(ob.getInteger("countyCode"))) {
                            gas.setCountyCode(ob.getInteger("countyCode"));
                        }

                        if (StringUtils.isNotEmpty(ob.getString("countyName"))) {
                            gas.setCountyName(ob.getString("countyName"));
                        }

                        gas.setGasLogoBig(ob.getString("gasLogoBig"));
                        gas.setGasLogoSmall(ob.getString("gasLogoSmall"));
                        gas.setGasAddress(ob.getString("gasAddress"));
                        gas.setGasLongitude(ob.getDouble("gasAddressLongitude"));
                        gas.setGasLatitude(ob.getDouble("gasAddressLatitude"));
                        List<Double> doubles = new ArrayList<Double>(2);
                        doubles.add(Double.parseDouble(ob.getString("gasAddressLongitude")));
                        doubles.add(Double.parseDouble(ob.getString("gasAddressLatitude")));
                        gas.setLocation(doubles);
                        gas.setDeleted(false);
                        gas.setAddTime(LocalDateTime.now());
                        if (0 == ob.getInteger("isInvoice")) {
                            gas.setIsInvoice(false);
                        } else {
                            gas.setIsInvoice(true);
                        }
                        // mongoTemplate.insert(gas,"gas");
                        // mallGasMapper.insert(gas);
                        JSONArray arr = JSONArray.parseArray(ob.get("oilPriceList").toString());
                        if (CollectionUtils.isNotEmpty(arr)) {
                            for (int j = 0; j < arr.size(); j++) {
                                JSONObject ob2 = (JSONObject) arr.get(j);
                                MallOli oli = new MallOli();
                                oli.setGasId(gas.getId());
                                oli.setId(GraphaiIdGenerator.nextId("MallOli"));
                                oli.setOliNo(ob2.getString("oilNo"));
                                oli.setOliName(ob2.getString("oilName"));
                                oli.setYfqPrice(new BigDecimal(ob2.getString("priceYfq")));
                                oli.setGunPrice(new BigDecimal(ob2.getString("priceGun")));
                                oli.setOfficialPrice(new BigDecimal(ob2.getString("priceOfficial")));
                                oli.setOilType(ob2.getInteger("oilType"));
                                oli.setDeleted(false);
                                oli.setAddTime(LocalDateTime.now());
                                // mallOliMapper.insert(oli);
                                // mongoTemplate.insert(oli, "oli");
                            }
                        }
                        // } else {
                        // if (0 == ob.getInteger("isInvoice")) {
                        // gaslList.get(0).setIsInvoice(false);
                        // } else {
                        // gaslList.get(0).setIsInvoice(true);
                        // }
                        //
                        //
                        // gaslList.get(0).setGasId(ob.getString("gasId"));
                        // gaslList.get(0).setGasName(ob.getString("gasName"));
                        // gaslList.get(0).setGasType(ob.getInteger("gasType"));
                        // gaslList.get(0).setProvinceCode(ob.getInteger("provinceCode"));
                        // gaslList.get(0).setProvinceName(ob.getString("provinceName"));
                        // gaslList.get(0).setCityCode(ob.getInteger("cityCode"));
                        // gaslList.get(0).setCityName(ob.getString("cityName"));
                        // if (ObjectUtil.isNotNull(ob.getInteger("countyCode"))) {
                        // gaslList.get(0).setCountyCode(ob.getInteger("countyCode"));
                        // }
                        //
                        // if (StringUtils.isNotEmpty(ob.getString("countyName"))) {
                        // gaslList.get(0).setCountyName(ob.getString("countyName"));
                        // }
                        //
                        // gaslList.get(0).setGasLogoBig(ob.getString("gasLogoBig"));
                        // gaslList.get(0).setGasLogoSmall(ob.getString("gasLogoSmall"));
                        // gaslList.get(0).setGasAddress(ob.getString("gasAddress"));
                        // gaslList.get(0).setGasLongitude(ob.getDouble("gasAddressLongitude"));
                        // gaslList.get(0).setGasLatitude(ob.getDouble("gasAddressLatitude"));
                        // List<Double> doubles=new ArrayList<Double>(2);
                        // doubles.add(Double.parseDouble(ob.getString("gasAddressLongitude")));
                        // doubles.add(Double.parseDouble(ob.getString("gasAddressLatitude")));
                        // gaslList.get(0).setLocation(doubles);
                        //
                        // //mallGasMapper.updateByPrimaryKeySelective(gaslList.get(0));
                        // mongoTemplate.update(gaslList.get(0).getClass());
                        //
                        // JSONArray arr = JSONArray.parseArray(ob.get("oilPriceList").toString());
                        // if (CollectionUtils.isNotEmpty(arr)) {
                        // for (int j = 0; j < arr.size(); j++) {
                        // JSONObject ob2 = (JSONObject) arr.get(j);
                        // // 查询油站id和油号是否存在
                        // MallOliExample oliExample = new MallOliExample();
                        // oliExample.or().andGasIdEqualTo(gaslList.get(0).getId())
                        // .andOliNoEqualTo(ob2.getString("oilNo")).andDeletedEqualTo(false);
                        // List<MallOli> oList = mallOliMapper.selectByExample(oliExample);
                        // if (CollectionUtils.isEmpty(oList)) {
                        // MallOli oli = new MallOli();
                        // oli.setGasId(gaslList.get(0).getId());
                        // oli.setId(GraphaiIdGenerator.nextId("MallOli"));
                        // oli.setOliNo(ob2.getString("oilNo"));
                        // oli.setOliName(ob2.getString("oilName"));
                        // oli.setYfqPrice(new BigDecimal(ob2.getString("priceYfq")));
                        // oli.setGunPrice(new BigDecimal(ob2.getString("priceGun")));
                        // oli.setOfficialPrice(new BigDecimal(ob2.getString("priceOfficial")));
                        // oli.setOilType(ob2.getInteger("oilType"));
                        // oli.setDeleted(false);
                        // oli.setAddTime(LocalDateTime.now());
                        // //mallOliMapper.insert(oli);
                        // mongoTemplate.insert(oli, "oli");
                        // } else {
                        // oList.get(0).setOliName(ob2.getString("oilName"));
                        // oList.get(0).setYfqPrice(new BigDecimal(ob2.getString("priceYfq")));
                        // oList.get(0).setGunPrice(new BigDecimal(ob2.getString("priceGun")));
                        // oList.get(0).setOfficialPrice(new BigDecimal(ob2.getString("priceOfficial")));
                        // oList.get(0).setOilType(ob2.getInteger("oilType"));
                        // oList.get(0).setUpdateTime(LocalDateTime.now());
                        // //mallOliMapper.updateByPrimaryKeySelective(oList.get(0));
                        // mongoTemplate.update(oList.get(0).getClass());
                        // }
                        //
                        // }
                        // }
                        //
                        // }

                    }
                }
                return ResponseUtil.ok(array);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseUtil.fail();
    }



    /**
     * 根据用户查询油站状态和油价(mongodb版本)
     *
     * @param userId 用户id
     * @return token
     */
    // @GetMapping("/getGasByUser")
    // public Object getGasByUser( @LoginUser String userId, @NotEmpty String longitude, @NotEmpty
    // String latitude, @RequestParam(defaultValue = "1") double radius,
    // @RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "5") Integer limit,
    // @RequestParam(defaultValue = "92") String oliNo) {
    //
    // if (userId == null) {
    // return ResponseUtil.unlogin();
    // }
    // String gunNos = "";
    //
    // MallUser user = userService.findById(userId);
    // if (ObjectUtil.isNotNull(user) && StringUtils.isNotBlank(user.getMobile())) {
    // if (getToken(user.getGasTokenAddTime(), user.getMobile(), userId) == false) {
    // return ResponseUtil.fail(508, "团油用户授权失败!");
    // }
    // }
    //
    // Map<String, Object> dataMap = findNearbyGas(longitude, latitude, radius, page, limit);
    //
    // List<String> diList = new ArrayList<>();
    // if (dataMap.get("distanceList") instanceof ArrayList<?>) {
    // for (Object o : (List<?>) dataMap.get("distanceList")) {
    // diList.add(String.class.cast(o));
    // }
    // }
    //
    // List<Map<String, Object>> resuList = new ArrayList<Map<String, Object>>(10);
    // try {
    //
    // Map<String, String> map = new LinkedHashMap<String, String>();
    // map.put("app_key", MallConstant.GAS_KEY);
    //// if (CollectionUtils.isNotEmpty(dataMap.get("gasId"))) {
    //// String arr = String.join(",",gasIdList);
    ////
    //// }
    // map.put("gasIds", String.valueOf(dataMap.get("gasId")));
    // map.put("platformType", MallConstant.GAS_platform_TYPE);
    // if (StringUtils.isBlank(user.getMobile())) {
    // return ResponseUtil.fail(507, "获取用户手机号异常!");
    // }
    // map.put("phone", user.getMobile());
    // map.put("timestamp", String.valueOf(System.currentTimeMillis()));
    //
    // String signString = MallConstant.GAS_SECRET + "app_key" + map.get("app_key") + "gasIds" +
    // map.get("gasIds")
    // + "phone" + map.get("phone") + "platformType" + map.get("platformType") + "timestamp"
    // + map.get("timestamp") + MallConstant.GAS_SECRET;
    //
    // map.put("sign", DigestUtils.md5Hex(signString));
    //
    // // String result =
    // // HttpUtils.post("https://test-mcs.czb365.com/services/v3/gas/queryPriceByPhone",
    // // map, null);
    //
    // // String result =
    // // HttpUtil.sendPost("https://test-mcs.czb365.com/services/v3/gas/queryPriceByPhone",
    // // map);
    // String result = LSXDUtils.sendPost("https://mcs.czb365.com/services/v3/gas/queryPriceByPhone",
    // map);
    // JSONObject object = JSONObject.parseObject(result);
    // if (Objects.equals(object.getString("code"), "200")) {
    //
    // // return ResponseUtil.ok(object);
    // JSONArray array = JSONArray.parseArray(object.get("result").toString());
    // if (CollectionUtils.isNotEmpty(array)) {
    // for (int i = 0; i < array.size(); i++) {
    //
    // // 查询是否已存在油站记录
    // JSONObject ob = (JSONObject) array.get(i);
    // Map<String, Object> gas = customMallGasMapper.findGasByGasId(ob.getString("gasId"), oliNo);
    //
    // if (ObjectUtil.isNull(gas)) {
    // continue;
    // }
    // gas.put("distance", diList.get(i));
    // JSONArray arr = JSONArray.parseArray(ob.get("oilPriceList").toString());
    // if (CollectionUtils.isNotEmpty(arr)) {
    // for (int j = 0; j < arr.size(); j++) {
    //
    // JSONObject ob2 = (JSONObject) arr.get(j);
    // JSONArray gunArray = JSONArray.parseArray(ob2.get("gunNos").toString());
    // if (CollectionUtils.isNotEmpty(gunArray)) {
    // // JSONArray js = new JSONArray();
    // List<String> js = new ArrayList<String>();
    // for (int k = 0; k < gunArray.size(); k++) {
    //
    // JSONObject ob3 = (JSONObject) gunArray.get(k);
    // js.add(ob3.getString("gunNo"));
    //
    // }
    // gunNos = String.join(",", js);
    // }
    // String oNo = ob2.getString("oilNo");
    // MallOli oli = new MallOli();
    // oli.setGunNo(gunNos);
    // MallOliExample oliExample = new MallOliExample();
    // oliExample.or().andGasIdEqualTo(String.valueOf(gas.get("id"))).andOliNoEqualTo(oNo);
    // mallOliMapper.updateByExampleSelective(oli, oliExample);
    // }
    // }
    // resuList.add(gas);
    //
    // }
    // }
    // return ResponseUtil.ok(resuList);
    // }
    // } catch (Exception e) {
    // e.printStackTrace();
    // }
    // return ResponseUtil.fail();
    // }
    //


    /**
     * 根据用户查询油站状态和油价(mysql版本)
     *
     * @param userId 用户id
     * @return token
     */
    @GetMapping("/getGasByUser")
    public Object getGasByUser(@LoginUser String userId, @NotEmpty String longitude, @NotEmpty String latitude, @RequestParam(defaultValue = "1") double radius, @RequestParam(defaultValue = "1") Integer page,
                    @RequestParam(defaultValue = "5") Integer limit, @RequestParam(defaultValue = "92") String oliNo) {

        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        String gunNos = "";

        MallUser user = userService.findById(userId);
        if (ObjectUtil.isNotNull(user) && StringUtils.isNotBlank(user.getMobile())) {
            if (getToken(user.getGasTokenAddTime(), user.getMobile(), userId) == false) {
                return ResponseUtil.fail(508, "团油用户授权失败!");
            }
        }

        StringBuilder sb = new StringBuilder();
        List<String> gasIdList = new ArrayList<>();
        Double longi = 0.0;
        Double lati = 0.0;
        if (StringHelper.isNotNullAndEmpty(longitude) && StringHelper.isNotNullAndEmpty(latitude)) {
            Double gitude = Double.parseDouble(longitude);
            Double titude = Double.parseDouble(latitude);

            BigDecimal bd = new BigDecimal(gitude);
            longi = bd.setScale(8, BigDecimal.ROUND_HALF_UP).doubleValue();
            BigDecimal bd2 = new BigDecimal(titude);
            lati = bd2.setScale(8, BigDecimal.ROUND_HALF_UP).doubleValue();

            List<Map<String, Object>> dataMap = findNearbyGasV2(longi, lati, radius, page, limit);



            if (CollectionUtils.isNotEmpty(dataMap)) {
                for (Map<String, Object> map : dataMap) {
                    if (!Objects.equals(null, map.get("gasId")) && Objects.equals("1", String.valueOf(map.get("platForm")))) {
                        sb.append(String.valueOf(map.get("gasId")) + ",");
                        gasIdList.add(String.valueOf(map.get("gasId")));
                    }

                }
            }
        }



        List<Map<String, Object>> resuList = new ArrayList<Map<String, Object>>(10);
        try {

            Map<String, String> map = new LinkedHashMap<String, String>();
            map.put("app_key", MallConstant.GAS_KEY);
            map.put("gasIds", sb.toString() != null ? sb.toString() : null);
            map.put("platformType", MallConstant.GAS_platform_TYPE);
            if (StringUtils.isBlank(user.getMobile())) {
                return ResponseUtil.fail(507, "获取用户手机号异常!");
            }
            map.put("phone", user.getMobile());
            map.put("timestamp", String.valueOf(System.currentTimeMillis()));

            String signString = MallConstant.GAS_SECRET + "app_key" + map.get("app_key") + "gasIds" + map.get("gasIds") + "phone" + map.get("phone") + "platformType" + map.get("platformType") + "timestamp"
                            + map.get("timestamp") + MallConstant.GAS_SECRET;

            map.put("sign", DigestUtils.md5Hex(signString));

            String result = LSXDUtils.sendPost("https://mcs.czb365.com/services/v3/gas/queryPriceByPhone", map);
            JSONObject object = JSONObject.parseObject(result);
            log.info("----获取团油数据结果------------------" + object);
            if (Objects.equals(object.getString("code"), "200")) {

                // return ResponseUtil.ok(object);
                JSONArray array = JSONArray.parseArray(object.get("result").toString());

                for (int a = 0; a < gasIdList.size(); a++) {
                    if (CollectionUtils.isNotEmpty(array)) {
                        for (int i = 0; i < array.size(); i++) {

                            // 查询是否已存在油站记录
                            JSONObject ob = (JSONObject) array.get(i);
                            Map<String, Object> gas = new HashMap<String, Object>();

                            if (!Objects.equals(gasIdList.get(a), ob.getString("gasId"))) {
                                continue;
                            }

                            gas = mallGasService.queryByGasIdAndOliNoV2(longi, lati, ob.getString("gasId"), oliNo);
                            if (ObjectUtil.isNull(gas)) {
                                continue;
                            }

                            JSONArray arr = JSONArray.parseArray(ob.get("oilPriceList").toString());
                            if (CollectionUtils.isNotEmpty(arr)) {
                                for (int j = 0; j < arr.size(); j++) {

                                    JSONObject ob2 = (JSONObject) arr.get(j);
                                    JSONArray gunArray = JSONArray.parseArray(ob2.get("gunNos").toString());
                                    if (CollectionUtils.isNotEmpty(gunArray)) {
                                        // JSONArray js = new JSONArray();
                                        List<String> js = new ArrayList<String>();
                                        for (int k = 0; k < gunArray.size(); k++) {

                                            JSONObject ob3 = (JSONObject) gunArray.get(k);
                                            js.add(ob3.getString("gunNo"));

                                        }
                                        gunNos = String.join(",", js);
                                    }
                                    String oNo = ob2.getString("oilNo");
                                    MallOli oli = new MallOli();
                                    oli.setGunNo(gunNos);
                                    oli.setYfqPrice(ob2.getBigDecimal("priceYfq"));
                                    oli.setGunPrice(ob2.getBigDecimal("priceGun"));
                                    oli.setOfficialPrice(ob2.getBigDecimal("priceOfficial"));
                                    MallOliExample oliExample = new MallOliExample();
                                    oliExample.or().andGasIdEqualTo(String.valueOf(gas.get("id"))).andOliNoEqualTo(oNo);
                                    mallOliMapper.updateByExampleSelective(oli, oliExample);
                                }
                            }
                            resuList.add(gas);

                        }
                    }
                }
                return ResponseUtil.ok(resuList);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        log.info("----获取团油数据失败------------------");
        return ResponseUtil.ok(resuList);
    }



    /**
     * 根据用户查询油站状态和油价(mysql版本)
     *
     * @param userId 用户id
     * @return token
     */
    @GetMapping("/getGasByUserV2")
    public Object getGasByUserV2(@LoginUser String userId, @NotEmpty String longitude, @NotEmpty String latitude, @RequestParam(defaultValue = "1") double radius, @RequestParam(defaultValue = "1") Integer page,
                    @RequestParam(defaultValue = "5") Integer limit, @RequestParam(defaultValue = "92") String oliNo) {

        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        String gunNos = "";

        MallUser user = userService.findById(userId);
        if (ObjectUtil.isNotNull(user) && StringUtils.isNotBlank(user.getMobile())) {
            if (getToken(user.getGasTokenAddTime(), user.getMobile(), userId) == false) {
                return ResponseUtil.fail(508, "团油用户授权失败!");
            }
        }

        StringBuilder sb = new StringBuilder();
        List<String> gasIdList = new ArrayList<>();
        Double longi = 0.0;
        Double lati = 0.0;
        if (StringHelper.isNotNullAndEmpty(longitude) && StringHelper.isNotNullAndEmpty(latitude)) {
            Double gitude = Double.parseDouble(longitude);
            Double titude = Double.parseDouble(latitude);

            BigDecimal bd = new BigDecimal(gitude);
            longi = bd.setScale(8, BigDecimal.ROUND_HALF_UP).doubleValue();
            BigDecimal bd2 = new BigDecimal(titude);
            lati = bd2.setScale(8, BigDecimal.ROUND_HALF_UP).doubleValue();

            List<Map<String, Object>> dataMap = findNearbyGasV2(longi, lati, radius, page, limit);



            if (CollectionUtils.isNotEmpty(dataMap)) {
                for (Map<String, Object> map : dataMap) {
                    if (!Objects.equals(null, map.get("gasId"))) {
                        sb.append(String.valueOf(map.get("gasId")) + ",");
                        gasIdList.add(String.valueOf(map.get("gasId")));
                    }

                }
            }
        }



        List<Map<String, Object>> resuList = new ArrayList<Map<String, Object>>(10);
        try {

            Map<String, String> map = new LinkedHashMap<String, String>();
            map.put("app_key", MallConstant.GAS_KEY);
            map.put("gasIds", sb.toString() != null ? sb.toString() : null);
            map.put("platformType", MallConstant.GAS_platform_TYPE);
            if (StringUtils.isBlank(user.getMobile())) {
                return ResponseUtil.fail(507, "获取用户手机号异常!");
            }
            map.put("phone", user.getMobile());
            map.put("timestamp", String.valueOf(System.currentTimeMillis()));

            String signString = MallConstant.GAS_SECRET + "app_key" + map.get("app_key") + "gasIds" + map.get("gasIds") + "phone" + map.get("phone") + "platformType" + map.get("platformType") + "timestamp"
                            + map.get("timestamp") + MallConstant.GAS_SECRET;

            map.put("sign", DigestUtils.md5Hex(signString));

            String result = LSXDUtils.sendPost("https://mcs.czb365.com/services/v3/gas/queryPriceByPhone", map);
            JSONObject object = JSONObject.parseObject(result);
            log.info("----获取团油数据结果------------------" + object);
            if (Objects.equals(object.getString("code"), "200")) {

                // return ResponseUtil.ok(object);
                JSONArray array = JSONArray.parseArray(object.get("result").toString());

                for (int a = 0; a < gasIdList.size(); a++) {
                    if (CollectionUtils.isNotEmpty(array)) {
                        for (int i = 0; i < array.size(); i++) {

                            // 查询是否已存在油站记录
                            JSONObject ob = (JSONObject) array.get(i);
                            Map<String, Object> gas = new HashMap<String, Object>();

                            if (!Objects.equals(gasIdList.get(a), ob.getString("gasId"))) {
                                continue;
                            }

                            gas = mallGasService.queryByGasIdAndOliNoV2(longi, lati, ob.getString("gasId"), oliNo);
                            if (ObjectUtil.isNull(gas)) {
                                continue;
                            }

                            JSONArray arr = JSONArray.parseArray(ob.get("oilPriceList").toString());
                            if (CollectionUtils.isNotEmpty(arr)) {
                                for (int j = 0; j < arr.size(); j++) {

                                    JSONObject ob2 = (JSONObject) arr.get(j);
                                    JSONArray gunArray = JSONArray.parseArray(ob2.get("gunNos").toString());
                                    if (CollectionUtils.isNotEmpty(gunArray)) {
                                        // JSONArray js = new JSONArray();
                                        List<String> js = new ArrayList<String>();
                                        for (int k = 0; k < gunArray.size(); k++) {

                                            JSONObject ob3 = (JSONObject) gunArray.get(k);
                                            js.add(ob3.getString("gunNo"));

                                        }
                                        gunNos = String.join(",", js);
                                    }
                                    String oNo = ob2.getString("oilNo");
                                    MallOli oli = new MallOli();
                                    oli.setGunNo(gunNos);
                                    MallOliExample oliExample = new MallOliExample();
                                    oliExample.or().andGasIdEqualTo(String.valueOf(gas.get("id"))).andOliNoEqualTo(oNo);
                                    mallOliMapper.updateByExampleSelective(oli, oliExample);
                                }
                            }
                            resuList.add(gas);

                        }
                    }
                }
                return ResponseUtil.ok(resuList);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        log.info("----获取团油数据失败------------------");
        return ResponseUtil.ok(resuList);
    }



    /**
     * 根据用户查询油站状态和油价(mysql版本)
     *
     * @param userId 用户id
     * @return token
     */
    @GetMapping("/getNewGasByUser")
    public Object getNewGasByUser(@LoginUser String userId, @NotEmpty String longitude, @NotEmpty String latitude, @RequestParam(defaultValue = "1") double radius, @RequestParam(defaultValue = "1") Integer page,
                    @RequestParam(defaultValue = "5") Integer limit, @RequestParam(defaultValue = "92") String oliNo) {

        if (userId == null) {
            return ResponseUtil.unlogin();
        }



        MallUser user = userService.findById(userId);
        if (ObjectUtil.isNotNull(user) && StringUtils.isNotBlank(user.getMobile())) {
            if (getToken(user.getGasTokenAddTime(), user.getMobile(), userId) == false) {
                return ResponseUtil.fail(508, "团油用户授权失败!");
            }
        }

        StringBuilder sb = new StringBuilder();
        // 小桔油站id集合
        List<String> gasIdList = new ArrayList<>();
        Double longi = 0.0;
        Double lati = 0.0;
        if (StringHelper.isNotNullAndEmpty(longitude) && StringHelper.isNotNullAndEmpty(latitude)) {
            Double gitude = Double.parseDouble(longitude); // longitude
            Double titude = Double.parseDouble(latitude); // latitude

            BigDecimal bd = new BigDecimal(gitude);
            longi = bd.setScale(8, BigDecimal.ROUND_HALF_UP).doubleValue();
            BigDecimal bd2 = new BigDecimal(titude);
            lati = bd2.setScale(8, BigDecimal.ROUND_HALF_UP).doubleValue();

            List<Map<String, Object>> dataMap = findNearbyGasV2(longi, lati, radius, page, limit);



            // 获取团油油站id集合
            if (CollectionUtils.isNotEmpty(dataMap)) {
                for (Map<String, Object> map : dataMap) {
                    if (!Objects.equals(null, map.get("gasId")) && Objects.equals("1", String.valueOf(map.get("platForm")))) {
                        sb.append(String.valueOf(map.get("gasId")) + ",");
                    }

                    if (Objects.equals(String.valueOf(map.get("platForm")), "2")) {
                        gasIdList.add(String.valueOf(map.get("gasId")));
                    }

                }
            }
        }



        // List<Map<String, Object>> resuList = new ArrayList<Map<String, Object>>(10);
        try {
            // 修改团油实时油号价格(调用第三方平台)
            updateGroupGasPrice(sb.toString(), user.getMobile());
            // 修改小桔实时油号价格(调用第三方平台)
            updateXiaoJuGasPrice(gasIdList, user.getMobile(), longitude, latitude, oliNo + "#");

            List<Map<String, Object>> resuList = mallGasService.queryGasorderByDistancePage(longitude, latitude, oliNo, oliNo + "#", (page - 1) * limit, limit);

            return ResponseUtil.ok(resuList);

        } catch (Exception e) {
            e.printStackTrace();
        }
        log.info("----获取团油数据失败------------------");
        return ResponseUtil.ok();
    }


    /**
     * 静默登录获取授权码跳转到支付页面
     */
    @GetMapping("/getPayUrl")
    public Object getPayUrl(@LoginUser String userId, @NotNull String gasId, @NotNull String gunNo) {

        if (userId == null) {
            return ResponseUtil.unlogin();
        }

        MallUser user = userService.findById(userId);

        try {
            Map<String, String> map = new LinkedHashMap<String, String>();
            map.put("app_key", MallConstant.GAS_KEY);
            map.put("platformId", MallConstant.GAS_platform_TYPE);

            if (StringUtils.isBlank(user.getMobile())) {
                return ResponseUtil.fail(507, "获取用户手机号异常!");
            }
            map.put("phone", user.getMobile());
            map.put("timestamp", String.valueOf(System.currentTimeMillis()));

            String signString = MallConstant.GAS_SECRET + "app_key" + map.get("app_key") + "phone" + map.get("phone") + "platformId" + map.get("platformId") + "timestamp" + map.get("timestamp") + MallConstant.GAS_SECRET;

            map.put("sign", DigestUtils.md5Hex(signString));

            // String result =
            // HttpUtils.post("https://test-mcs.czb365.com/services/v3/begin/getSecretCode",
            // map, null);
            String result = HttpUtil.sendPost("https://mcs.czb365.com/services/v3/begin/getSecretCode", map);
            JSONObject object = JSONObject.parseObject(result);
            if (Objects.equals(object.getString("code"), "200")) {
                String code = object.getString("result");
                return ResponseUtil.ok(String.format("https://open.czb365.com/redirection/todo/?platformType=%s&authCode=%s&gasId=%s&gunNo=%s", MallConstant.GAS_platform_TYPE, code, gasId, gunNo));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseUtil.fail();
    }

    /**
     * 团油详情页
     */
    @GetMapping("/detail")
    public Object detail(@LoginUser String userId, @NotNull String aId, @NotNull String oliNo) {

        if (userId == null) {
            return ResponseUtil.unlogin();
        }

        Map<String, Object> data = new HashMap<String, Object>();
        List<MallOli> olis = new ArrayList<MallOli>();
        // String gunNos="";
        try {
            Map<String, Object> map = customMallGasMapper.findGasOliByIdAndOliNo(aId, oliNo);
            if (ObjectUtil.isNotNull(map)) {
                MallOliExample oliExample = new MallOliExample();
                oliExample.or().andGasIdEqualTo(aId).andDeletedEqualTo(false).andGunNoIsNotNull();
                olis = mallOliMapper.selectByExampleSelective(oliExample, MallOli.Column.gunNo, MallOli.Column.oliName, MallOli.Column.oliNo);
                for (MallOli oli : olis) {
                    if (oli.getGunNo().contains(",")) {
                        List<String> gunList = Arrays.asList(oli.getGunNo().split(","));
                        oli.setGunList(gunList);
                    } else {
                        List<String> oList = new ArrayList<String>();
                        oList.add(oli.getGunNo());
                        oli.setGunList(oList);
                    }

                }
            }

            data.put("info", map);
            data.put("oliGunList", olis);
            return ResponseUtil.ok(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseUtil.fail();
    }


    /**
     * 查询附近的加油站(mongodb版本)
     */
    // public Map<String, Object> findNearbyGas(String longitude, String latitude, double radius,
    // Integer page,
    // Integer limit) {
    //
    //
    // List<String> list = new ArrayList<String>();
    // List<String> distanceList = new ArrayList<String>();
    // List<MallGas> lbses = new ArrayList<MallGas>();
    // Map<String, Object> data = new HashMap<String, Object>();
    // try {
    // // 分页
    // Pageable pageable = PageRequest.of(page - 1, limit);
    //
    // Double gitude = Double.parseDouble(longitude);
    // Double titude = Double.parseDouble(latitude);
    //
    // BigDecimal bd = new BigDecimal(gitude);
    // Double longi = bd.setScale(8, BigDecimal.ROUND_HALF_UP).doubleValue();
    // BigDecimal bd2 = new BigDecimal(titude);
    // Double lati = bd2.setScale(8, BigDecimal.ROUND_HALF_UP).doubleValue();
    //
    // // mongo的方法，传入经纬度
    // Point point = new Point(longi, lati);
    // // 按条件查询
    //// Criteria criteria = Criteria.where("gasId").is("WC000019305");
    //// Query query1 = new Query(criteria);
    // NearQuery nearQuery = NearQuery.near(point, Metrics.KILOMETERS).with(pageable).spherical(true);
    // //NearQuery nearQuery = NearQuery.near(point, Metrics.KILOMETERS).query(query1).spherical(true);
    // GeoResults<MallGas> geoResults = mongoTemplate.geoNear(nearQuery, MallGas.class);
    // List<GeoResult<MallGas>> list2 = geoResults.getContent();
    // if (CollectionUtils.isNotEmpty(list2)) {
    // for (int i = 0; i < list2.size(); i++) {
    // GeoResult<MallGas> sceneryResultGeoResult = list2.get(i);
    // lbses.add(sceneryResultGeoResult.getContent());
    // list.add(sceneryResultGeoResult.getContent().getGasId());
    // String distance = String.valueOf(sceneryResultGeoResult.getDistance());
    // if (distance.length() > 6) {
    // distance = distance.substring(0, 4);
    // }
    // // 打印出距离
    // distanceList.add(distance);
    // }
    // }
    //
    // if (CollectionUtils.isNotEmpty(list)) {
    // String arr = String.join(",", list);
    // data.put("gasId", arr);
    // }
    //
    // data.put("distanceList", distanceList);
    //
    // return data;
    // } catch (Exception e) {
    // e.printStackTrace();
    // }
    // return new HashMap<String, Object>();
    // }



    /**
     * 查询附近的加油站(mysql版本)
     */
    public List<Map<String, Object>> findNearbyGasV2(Double longitude, Double latitude, double radius, Integer page, Integer limit) {

        Map<String, Object> data = new HashMap<String, Object>();
        try {
            if (page <= 0) {
                page = 1;
            }

            List<Map<String, Object>> reList = mallGasService.queryGasorderByDistance(longitude, latitude, (page - 1) * limit, limit);
            if (CollectionUtils.isNotEmpty(reList)) {
                return reList;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ArrayList<Map<String, Object>>();
    }



    /**
     * 修改团油实时油价
     * 
     * @param gasIdList 油站id集合
     * @param mobile 用户手机号
     * @return
     */
    public boolean updateGroupGasPrice(String gasIdList, String mobile) {


        String gunNos = "";
        try {
            Map<String, String> map = new LinkedHashMap<String, String>();
            map.put("app_key", MallConstant.GAS_KEY);
            map.put("gasIds", gasIdList);
            map.put("platformType", MallConstant.GAS_platform_TYPE);

            map.put("phone", mobile);
            map.put("timestamp", String.valueOf(System.currentTimeMillis()));

            String signString = MallConstant.GAS_SECRET + "app_key" + map.get("app_key") + "gasIds" + map.get("gasIds") + "phone" + map.get("phone") + "platformType" + map.get("platformType") + "timestamp"
                            + map.get("timestamp") + MallConstant.GAS_SECRET;

            map.put("sign", DigestUtils.md5Hex(signString));

            String result = LSXDUtils.sendPost("https://mcs.czb365.com/services/v3/gas/queryPriceByPhone", map);
            JSONObject object = JSONObject.parseObject(result);
            log.info("----获取团油数据结果------------------" + object);
            if (Objects.equals(object.getString("code"), "200")) {

                JSONArray array = JSONArray.parseArray(object.get("result").toString());

                if (CollectionUtils.isNotEmpty(array)) {
                    for (int i = 0; i < array.size(); i++) {

                        // 查询是否已存在油站记录
                        JSONObject ob = (JSONObject) array.get(i);

                        MallGas gas = mallGasService.queryGasByGasId(ob.getString("gasId"), 1);
                        if (null == gas) {
                            continue;
                        }

                        JSONArray arr = JSONArray.parseArray(ob.get("oilPriceList").toString());
                        if (CollectionUtils.isNotEmpty(arr)) {
                            for (int j = 0; j < arr.size(); j++) {

                                JSONObject ob2 = (JSONObject) arr.get(j);
                                JSONArray gunArray = JSONArray.parseArray(ob2.get("gunNos").toString());
                                if (CollectionUtils.isNotEmpty(gunArray)) {
                                    // JSONArray js = new JSONArray();
                                    List<String> js = new ArrayList<String>();
                                    for (int k = 0; k < gunArray.size(); k++) {

                                        JSONObject ob3 = (JSONObject) gunArray.get(k);
                                        js.add(ob3.getString("gunNo"));

                                    }
                                    gunNos = String.join(",", js);
                                }
                                String oNo = ob2.getString("oilNo");
                                MallOli oli = new MallOli();
                                oli.setGunNo(gunNos);
                                oli.setYfqPrice(ob2.getBigDecimal("priceYfq"));
                                oli.setGunPrice(ob2.getBigDecimal("priceGun"));
                                oli.setOfficialPrice(ob2.getBigDecimal("priceOfficial"));
                                MallOliExample oliExample = new MallOliExample();
                                oliExample.or().andGasIdEqualTo(gas.getId()).andOliNoEqualTo(oNo);
                                mallOliMapper.updateByExampleSelective(oli, oliExample);
                            }
                        }


                    }
                }
                return true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


    /**
     * 修改实时小桔油价
     * 
     * @param gasIdList 油站id集合
     * @param mobile 用户手机号
     * @param lon 经度
     * @param lat 纬度
     * @param oliName 油号名字
     * @param token 小桔token
     * @return
     */
    public boolean updateXiaoJuGasPrice(List<String> gasIdList, String mobile, String lon, String lat, String oliName) {

        String token = "";
        // 小桔token缓存redis两小时自动失效
        Object home = NewCacheInstanceServiceLoader.xiaojuCache().get("xiaojuToken");
        if (home == null) {
            token = getXiaoJuToken();
        } else {
            token = String.valueOf(NewCacheInstanceServiceLoader.xiaojuCache().get("xiaojuToken"));
        }


        Map<String, Object> map = new LinkedHashMap<String, Object>();
        map.put("lon", lon);
        map.put("lat", lat);
        map.put("mobile", mobile);
        map.put("openChannel", 1);
        map.put("itemName", oliName);
        // List<String> list = Arrays.asList("5165633531352193403", "5202188195790855162");
        map.put("storeIdList", gasIdList);

        JSONObject object = null;
        String jsonParam;

        try {

            jsonParam = SecurityUtil.encryptSignRequest(map);
            String result = HttpUtil.sendPostJson(SecurityUtil.requestUrl + "/queryStorePrice", jsonParam, token);
            object = JSONObject.parseObject(result);
            log.info("-----------获取小桔实时油价" + object);
            if (Objects.equals("0", object.get("code"))) {
                String respoString = SecurityUtil.decryptResponse(object.getString("data"));
                JSONObject obj = JSONObject.parseObject(respoString);

                JSONArray array = JSONArray.parseArray(obj.get("itemInfoList").toString());
                if (CollectionUtils.isNotEmpty(array)) {
                    for (int i = 0; i < array.size(); i++) {
                        JSONObject ob = (JSONObject) array.get(i);
                        if (StringHelper.isNotNullAndEmpty(ob.getString("itemName")) && StringHelper.isNotNullAndEmpty(ob.getString("storeId"))) {
                            MallOli oli = mallOliService.queryOliByGasIdAndOliName(ob.getString("storeId"), ob.getString("itemName"));
                            if (null != oli) {
                                if (StringHelper.isNotNullAndEmpty(ob.getString("cityPrice"))) {
                                    oli.setOfficialPrice(new BigDecimal(ob.getString("cityPrice")).divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP));
                                }

                                if (StringHelper.isNotNullAndEmpty(ob.getString("storePrice"))) {
                                    oli.setGunPrice(new BigDecimal(ob.getString("storePrice")).divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP));
                                }

                                if (StringHelper.isNotNullAndEmpty(ob.getString("vipPrice"))) {
                                    oli.setYfqPrice(new BigDecimal(ob.getString("vipPrice")).divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP));
                                }

                                mallOliService.updateOli(oli);
                            }
                        }
                    }
                }

                return true;

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

}
