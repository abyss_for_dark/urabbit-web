package com.graphai.mall.wx.web.test;

public class Test36 {

    public static void main(String[] args) {

//        {
//            "longUrl": "https://app.weilaylife.com/common/h5/#/pages/download/index2?goodsId=5357851030366777344&groupId=5357851030366777344&userId=5357851030366777344",
//                "type":"x-i-n"
//        }

        // 。 邀请你参加拼团，点击 http://sd.uulucky.com/xQE8lk1i 下载APP 复制这段文字T#15ulu71dh28w0,16660FY77I2GW,2cfp9v1ags2sT#，来未莱生活APP拼团吧~
        // id都是36进制的数组
//        36–> 10
        System.out.println(Long.valueOf("15ulu71dh28w0", 36));
        System.out.println(Long.valueOf("165cnvc62j5s0", 36));
        System.out.println(Long.valueOf("2cfp9v1ags2s", 36));

//        10 –> 36
        long longUserId = Long.parseLong("5550659121664532480");
        System.out.print(Long.toString(longUserId, 36).toUpperCase());

        //        36–> 10
//        System.out.println(String.valueOf("15ulu71dh28w0", 36));
//        System.out.println(String.valueOf("165cnvc62j5s0", 36));
//        System.out.println(String.valueOf("2cfp9v1ags2s", 36));
        //        10 –> 36
//        long longUserId2 = Long.parseLong("5550659121664532480");
//        System.out.print(Long.toString(longUserId, 36).toUpperCase());

    }
}
