package com.graphai.mall.wx.web.order;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.json.JacksonUtil;
import com.graphai.mall.db.domain.MallUser;
import com.graphai.mall.db.domain.MobileCardForm;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.util.MobileCardUtils;
import com.graphai.mall.wx.annotation.LoginUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 联通王卡接口
 *
 * @author Administrator
 */
@RestController
@RequestMapping("/wx/mobileCard")
@Validated
public class WxMobileCardController {

    @Autowired
    private MallUserService mallUserService;

    /**
     * 提交订单
     *
     * @param userId
     * @param mobileCardForm
     * @return
     */
    @PostMapping("/doCardOrder")
    public Object doCardOrder(@LoginUser String userId, @RequestBody MobileCardForm mobileCardForm) {
        MallUser user = mallUserService.findById(userId);
        if (null == user) {
            return ResponseUtil.unlogin();
        }
        try {
            return MobileCardUtils.doCardOrder(mobileCardForm, user);
        } catch (Exception e) {
            return ResponseUtil.fail(e.getMessage());
        }
    }

    /**
     * 获取验证码
     *
     * @param userId
     * @param data
     * @return
     */
    @PostMapping("/kcMessageSend")
    public Object kcMessageSend(@LoginUser String userId, @RequestBody String data) {
        MallUser user = mallUserService.findById(userId);
        if (null == user) {
            return ResponseUtil.unlogin();
        }
        String certId = JacksonUtil.parseString(data, "certId");
        String contactNum = JacksonUtil.parseString(data, "contactNum");
        try {
            return MobileCardUtils.kcMessageSend(certId, contactNum);
        } catch (Exception e) {
            return ResponseUtil.fail(e.getMessage());
        }
    }

    /**
     * 选号服务（精简版）
     *
     * @param userId
     * @param data
     * @return
     */
    @PostMapping("/numSelect")
    public Object numSelect(@LoginUser String userId, @RequestBody String data) {
        MallUser user = mallUserService.findById(userId);
        if (null == user) {
            return ResponseUtil.unlogin();
        }
        try {
            return MobileCardUtils.numSelect(data);
        } catch (Exception e) {
            return ResponseUtil.fail(e.getMessage());
        }
    }

}
