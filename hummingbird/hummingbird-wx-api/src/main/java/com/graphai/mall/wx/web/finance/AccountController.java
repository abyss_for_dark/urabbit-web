package com.graphai.mall.wx.web.finance;

import static com.graphai.mall.wx.util.WxResponseCode.ORDER_UNKNOWN;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.graphai.commons.util.obj.ObjectUtils;
import com.graphai.mall.admin.domain.MallMerchant;
import com.graphai.mall.admin.domain.MallPurchaseOrder;
import com.graphai.mall.admin.domain.MallUserMerchant;
import com.graphai.mall.admin.service.IMallMerchantService;
import com.graphai.mall.admin.service.IMallPurchaseOrderService;
import com.graphai.mall.admin.service.IMallUserMerchantService;
import com.graphai.mall.db.constant.order.PurchaseEnum;
import com.graphai.mall.db.constant.user.MallUserEnum;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.collections.PredicateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.houbb.sensitive.core.api.SensitiveUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.graphai.commons.constant.MallConstant;
import com.graphai.commons.util.NameUtil;
import com.graphai.commons.util.RegexUtil;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.json.JacksonUtil;
import com.graphai.commons.util.json.JacksonUtils;
import com.graphai.mall.db.domain.BmdesignUserLevel;
import com.graphai.mall.db.domain.FcAccount;
import com.graphai.mall.db.domain.FcAccountBindCard;
import com.graphai.mall.db.domain.FcAccountFreezeBill;
import com.graphai.mall.db.domain.FcAccountPrechargeBill;
import com.graphai.mall.db.domain.FcAccountRechargeBill;
import com.graphai.mall.db.domain.FcAccountWithdrawBill;
import com.graphai.mall.db.domain.MallAd;
import com.graphai.mall.db.domain.MallCategory;
import com.graphai.mall.db.domain.MallOrder;
import com.graphai.mall.db.domain.MallOrderGoods;
import com.graphai.mall.db.domain.MallUser;
import com.graphai.mall.db.service.account.AccountService;
import com.graphai.mall.db.constant.account.AccountStatus;
import com.graphai.mall.db.service.account.CapitalAccountUtils;
import com.graphai.mall.db.service.account.FcAccountBindCardService;
import com.graphai.mall.db.service.account.FcAccountFreezeService;
import com.graphai.mall.db.service.account.FcAccountPrechargeBillService;
import com.graphai.mall.db.service.account.FcAccountRechargeBillService;
import com.graphai.mall.db.service.account.FcAccountWithdrawBillService;
import com.graphai.mall.db.service.advertisement.MallAdService;
import com.graphai.mall.db.service.bmdesign.BmdesignUserLevelService;
import com.graphai.mall.db.service.category.MallCategoryService;
import com.graphai.mall.db.service.common.AppVersionService;
import com.graphai.mall.db.service.common.MallPhoneValidationService;
import com.graphai.mall.db.service.order.MallOrderGoodsService;
import com.graphai.mall.db.service.order.MallOrderService;
import com.graphai.mall.db.service.sysconfig.MallSystemConfigService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.constant.CommonConstant;
import com.graphai.mall.db.util.OrderUtil;
import com.graphai.mall.db.util.RetrialUtils;
import com.graphai.mall.db.util.account.AccountUtils;
import com.graphai.mall.db.vo.BmdesignUserLevelVo;
import com.graphai.mall.db.vo.FcAccountRechargeBill2Vo;
import com.graphai.mall.db.vo.FcAccountRechargeBillVo;
import com.graphai.mall.db.vo.WxMaProperties;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.mall.wx.dto.WxLoginInfo;
import com.graphai.mall.wx.service.UserTokenManager;
import com.graphai.mall.wx.service.WxOrderService;
import com.graphai.properties.WxProperties;
import com.vdurmont.emoji.EmojiParser;

import cn.hutool.core.date.LocalDateTimeUtil;
import cn.hutool.json.JSONUtil;

@RestController
// @RequestMapping(value = "/api_gateway", consumes = {
@RequestMapping(value = "/wx/capital",
                consumes = {MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.APPLICATION_ATOM_XML_VALUE,
                        MediaType.APPLICATION_FORM_URLENCODED_VALUE, MediaType.APPLICATION_JSON_VALUE,
                        MediaType.TEXT_PLAIN_VALUE, MediaType.TEXT_HTML_VALUE, MediaType.TEXT_XML_VALUE,
                        MediaType.APPLICATION_OCTET_STREAM_VALUE})
// html直接请求
public class AccountController {
    private final Log log = LogFactory.getLog(AccountController.class);

    // @Value("${mall.wx.mini-agent-app-id}")
    // private String miniAgentAppId;
    // @Value("${mall.wx.mini-agent-secret}")
    // private String miniAgentAppSecret;
    // @Value("${mall.wx.mini-activity-app-id}")
    // private String miniActivityAppId;
    // @Value("${mall.wx.mini-activity-secret}")
    // private String miniActivityAppSecret;
    @Autowired
    private WxProperties wxProperties;

    @Autowired
    private AccountService accountManager;

    @Autowired
    private MallOrderService orderService;
    @Autowired
    private MallOrderGoodsService orderGoodsService;
    @Autowired
    private MallPhoneValidationService phoneService;
    @Autowired
    private MallUserService userService;
    @Autowired
    private AccountService accountService;
    @Autowired
    private WxOrderService wxOrderService;

    @Autowired
    private MallCategoryService categoryService;

    @Autowired
    private MallAdService adService;

    @Autowired
    private FcAccountRechargeBillService fcAccountRechargeBillService;
    @Autowired
    private FcAccountPrechargeBillService fcAccountPrechargeBillService;

    @Autowired
    private BmdesignUserLevelService bmdesignUserLevelService;
    @Autowired
    private FcAccountWithdrawBillService fcAccountWithdrawBillService;
    @Autowired
    private FcAccountBindCardService fcAccountBindCardService;
    @Autowired
    private FcAccountFreezeService fcAccountFreezeService;

    @Autowired
    private MallSystemConfigService mallSystemConfigService;

    @Autowired
    private AppVersionService appVersionService;
    @Autowired
    private WxMaProperties wxMaProperties;
    @Autowired
    private IMallPurchaseOrderService mallPurchaseOrderService;
    @Autowired
    private IMallUserMerchantService mallUserMerchantService;
    @Autowired
    private IMallMerchantService mallMerchantService;

    /**
     * 收益明细-总金额合计
     *
     * @param userId 用户id
     */
    @GetMapping(value = "/incomeAccountSum")
    public Object incomeAccountSum(@LoginUser String userId) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        FcAccount fcAccount = accountManager.getAccountByKey(userId);
        BigDecimal withdrawableMoney = new BigDecimal(0.00); // 可提现余额
        BigDecimal totalRecharge = new BigDecimal(0.00);     // 累计入账
        BigDecimal totalPrecharge = new BigDecimal(0.00);    // 累计待入账
        List<String> bidList = AccountStatus.BID_LIST_FANGTAO;
        BigDecimal totalRechargeSum = fcAccountRechargeBillService.selectRechargeAmountByCondition2(userId, bidList, AccountStatus.DISTRIBUTION_TYPE_00, CommonConstant.TASK_REWARD_CASH, null, null);
        BigDecimal totalPrechargeSum = fcAccountPrechargeBillService.selectPrechargeAmountByCondition2(userId, bidList, AccountStatus.DISTRIBUTION_TYPE_00, CommonConstant.TASK_REWARD_CASH, null, null);
        if (fcAccount != null) {
            withdrawableMoney = fcAccount.getAmount();
        }
        if (Objects.nonNull(totalRechargeSum))
            totalRecharge = totalRechargeSum;
        if (Objects.nonNull(totalPrechargeSum))
            totalPrecharge = totalPrechargeSum;

        BigDecimal turnover=BigDecimal.ZERO;
        LocalDate now = LocalDate.now();
        LocalDateTime start = LocalDateTime.of(now, LocalTime.of(00, 00, 00));
        LocalDateTime end = LocalDateTime.of(now, LocalTime.of(23, 59, 59));

        MallUser mallUser = userService.queryByUserId(userId);
        if(mallUser.getLevelId().equals("98")){
            // 厂家
            bidList = new ArrayList<>();
            bidList.add(PurchaseEnum.BID_1.getCode());
            bidList.add(PurchaseEnum.BID_2.getCode());
            QueryWrapper<MallPurchaseOrder> mallPurchaseOrderQueryWrapper = new QueryWrapper<MallPurchaseOrder>();
            mallPurchaseOrderQueryWrapper.eq("factory_user_id", userId);// 正常营业
            mallPurchaseOrderQueryWrapper.between("add_time",start,end);
            mallPurchaseOrderQueryWrapper.in("bid", bidList);
            mallPurchaseOrderQueryWrapper.notIn("order_status", OrderUtil.STATUS_CREATE, OrderUtil.STATUS_CANCEL);
            mallPurchaseOrderQueryWrapper.select("SUM(actual_price) totalActualPrice");
            Map<String, Object> purchaseOrderServiceMap = mallPurchaseOrderService.getMap(mallPurchaseOrderQueryWrapper);
            if (null != purchaseOrderServiceMap) {
                turnover = turnover.add(new BigDecimal(String.valueOf(purchaseOrderServiceMap.get("totalActualPrice"))));
            }
        }

        MallUserMerchant merchant = mallUserMerchantService.getOne(new LambdaQueryWrapper<MallUserMerchant>().eq(MallUserMerchant::getUserId, userId));
        // 商户
        if (null != merchant) {
            Map<String, Object> objectMap = orderService.getTradeStatisticByMerchant(start, end, bidList, merchant.getMerchantId());
            if (ObjectUtils.isNotEmpty(objectMap) && objectMap.containsKey("actualPrice")) {
                turnover = turnover.add(new BigDecimal(String.valueOf(objectMap.get("actualPrice"))));
            }
        }

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("withdrawableMoney", withdrawableMoney); // 可提现余额
        map.put("totalRecharge", totalRecharge);         // 累计入账
        map.put("totalPrecharge", totalPrecharge);       // 累计待入账
        map.put("turnover", turnover);       // 营业额

        return ResponseUtil.ok("返利数据查询成功", map);
    }

    /**
     * 开户接口
     *
     * @param response
     * @param request
     * @param paramJson
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/account", method = RequestMethod.POST)
    public Object createNewCapitalAccount(HttpServletResponse response, HttpServletRequest request,
                    @RequestBody String paramJson) throws Exception {
        Map<String, Object> paramsMap = JacksonUtils.jsn2map(paramJson, String.class, Object.class);
        String apsKey = MapUtils.getString(paramsMap, "apsKey");
        log.info("~~开户参数apsKey:" + apsKey);
        FcAccount customerAccount = accountManager.getAccountByKey(apsKey);

        if (customerAccount != null) {
            return ResponseUtil.ok("您已经开户", customerAccount);
        }

        // 开户
        customerAccount = accountManager.accountTradeCreateAccount(apsKey);

        return ResponseUtil.ok("开户成功", customerAccount);
    }

    @RequestMapping(value = "/account/info", method = RequestMethod.POST)
    public Object getCustomerAccount(HttpServletResponse response, HttpServletRequest request,
                    @RequestBody String paramJson, @LoginUser String userId) throws Exception {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }

        FcAccount customerAccount = accountManager.getAccountByKey(userId.toString());
        Object obj = CapitalAccountUtils.validationAccount(customerAccount);
        if (obj != null) {
            return obj;
        }

        return ResponseUtil.ok("账户查询成功", customerAccount);
    }

    /**
     * 获取分销分润充值记录
     *
     * @param response
     * @param request
     * @param queryType
     * @param bid
     * @param userId
     * @param page
     * @param limit
     * @return
     * @throws Exception
     */
    @GetMapping(value = "/recharge/list")
    public Object getRechargeList(HttpServletResponse response, HttpServletRequest request,
                    @RequestParam(defaultValue = "self") String queryType, @RequestParam(required = false) String bid,
                    @LoginUser String userId, @RequestParam(defaultValue = "1") Integer page,
                    @RequestParam(defaultValue = "10") Integer limit) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        Integer start = (page - 1) * limit;
        Map<String, Object> result = new HashMap<String, Object>();
        // 查询自己的数据
        List<FcAccountRechargeBillVo> list = null;
        List<FcAccountPrechargeBill> prechargelist = null;
        Long total = 0l;
        int totalPages = 0;
        List<String> userIds = new ArrayList<String>();
        if (MallConstant.QUERY_TYPE_SELF.equals(queryType)) {
            userIds.add(userId.toString());

            list = fcAccountRechargeBillService.getAccountRechargeBillListByUserId(userId, true, bid,
                            AccountStatus.DATA_STATUS_1, AccountStatus.DISTRIBUTION_TYPE_00,
                            CommonConstant.TASK_REWARD_CASH, start, limit);
            total = accountManager.getAccountRechargeBillCount(userId);
            totalPages = (int) Math.ceil((double) Integer.parseInt(total.toString()) / limit);

        } else if (MallConstant.QUERY_TYPE_SUB.equals(queryType)) {

            total = accountManager.getAccountFansRechargeBillCount(userId.toString());
            totalPages = (int) Math.ceil((double) Integer.parseInt(total.toString()) / limit);
            list = fcAccountRechargeBillService.getAccountRechargeBillListByUserId(userId, true, bid,
                            AccountStatus.DATA_STATUS_1, AccountStatus.DISTRIBUTION_TYPE_00,
                            CommonConstant.TASK_REWARD_CASH, start, limit);
        } else {
            userIds.add(userId.toString());
            prechargelist = accountManager.getAccountPrechargeBillList(userIds, start, limit);

            total = accountManager.getAccountPrechargeBillCount(userIds);
            totalPages = (int) Math.ceil((double) Integer.parseInt(total.toString()) / limit);
        }

        List<Object> resultList = new ArrayList<Object>();

        List<String> fansUserIds = new ArrayList<String>();

        if (CollectionUtils.isNotEmpty(list)) {
            for (int i = 0; i < list.size(); i++) {
                FcAccountRechargeBillVo bill = list.get(i);
                if (!fansUserIds.contains(bill.getTpid())) {
                    fansUserIds.add(bill.getTpid());
                }
            }

            // 查询粉丝的用户信息
            List<MallUser> fansUserList = userService.findByIds(fansUserIds);
            for (int i = 0; i < list.size(); i++) {
                FcAccountRechargeBillVo bill = list.get(i);
                String tpid = bill.getTpid();

                // CollectionUtils.find(fansUserList, PredicateUtils.equalPredicate(tpid));
                Object userInfo = CollectionUtils.find(fansUserList, new Predicate() {

                    @Override
                    public boolean evaluate(Object object) {
                        MallUser user = (MallUser) object;
                        if (tpid != null && tpid.equals(user.getId())) {
                            return true;
                        }
                        return false;
                    }
                });

                Map<String, Object> results = new HashMap<>();
                results.put("bill", bill);
                results.put("userInfo", userInfo);
                resultList.add(results);
            }

            result.put("billList", resultList);
            result.put("totalPage", totalPages);
        }

        try {
            log.info("查询出订单数据 ： " + JacksonUtils.bean2Jsn(prechargelist));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (CollectionUtils.isNotEmpty(prechargelist)) {
            for (int i = 0; i < prechargelist.size(); i++) {
                FcAccountPrechargeBill bill = prechargelist.get(i);
                if (!fansUserIds.contains(bill.getTpid())) {
                    fansUserIds.add(bill.getTpid());
                }

            }

            // 查询粉丝的用户信息
            List<MallUser> fansUserList = userService.findByIds(fansUserIds);
            for (int i = 0; i < prechargelist.size(); i++) {
                FcAccountPrechargeBill bill = prechargelist.get(i);
                String tpid = bill.getTpid();

                Object userInfo = CollectionUtils.find(fansUserList, new Predicate() {

                    @Override
                    public boolean evaluate(Object object) {
                        MallUser user = (MallUser) object;
                        if (tpid != null && tpid.equals(user.getId())) {
                            return true;
                        }
                        return false;
                    }
                });

                Map<String, Object> results = new HashMap<>();
                results.put("bill", bill);
                results.put("userInfo", userInfo);
                resultList.add(results);
            }

            result.put("billList", resultList);
            result.put("totalPage", totalPages);
        }


        return ResponseUtil.ok("分润数据查询成功", result);
    }


    /**
     * 获取分销分润充值记录2(原始)
     *
     * @param response
     * @param request
     * @param userId
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/recharge/list2")
    public Object getRechargeList2(HttpServletResponse response, HttpServletRequest request,
                    @RequestParam(defaultValue = "self") String queryType, @LoginUser String userId,
                    @RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit)
                    throws Exception {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        Integer start = (page - 1) * limit;
        Map<String, Object> result = new HashMap<String, Object>();
        // 查询自己的数据
        List<FcAccountRechargeBill> list = null;
        List<FcAccountPrechargeBill> prechargelist = null;
        Long total = 0l;
        int totalPages = 0;
        List<String> userIds = new ArrayList<String>();
        if (MallConstant.QUERY_TYPE_SELF.equals(queryType)) {
            userIds.add(userId.toString());
            list = accountManager.getAccountRechargeBillList(userIds, start, limit);

            total = accountManager.getAccountRechargeBillCount(userId);
            totalPages = (int) Math.ceil((double) Integer.parseInt(total.toString()) / limit);

        } else if (MallConstant.QUERY_TYPE_SUB.equals(queryType)) {

            // total = accountManager.getAccountRechargeBillCount(userIds);
            // totalPages = (int) Math.ceil((double) Integer.parseInt(total.toString()) / limit);

            total = accountManager.getAccountFansRechargeBillCount(userId.toString());
            totalPages = (int) Math.ceil((double) Integer.parseInt(total.toString()) / limit);
            list = accountManager.getAccountFansRechargeBillList(userId.toString(), start, limit);
        } else {
            userIds.add(userId.toString());
            prechargelist = accountManager.getAccountPrechargeBillList(userIds, start, limit);

            total = accountManager.getAccountPrechargeBillCount(userIds);
            totalPages = (int) Math.ceil((double) Integer.parseInt(total.toString()) / limit);
        }

        List<Object> resultList = new ArrayList<Object>();
        List<String> orderIds = new ArrayList<String>();

        List<String> fansUserIds = new ArrayList<String>();

        if (CollectionUtils.isNotEmpty(list)) {
            for (int i = 0; i < list.size(); i++) {
                FcAccountRechargeBill bill = list.get(i);
                String orderId = bill.getOrderId();
                if (!orderIds.contains(orderId)) {
                    orderIds.add(orderId);
                }

                if (!fansUserIds.contains(bill.getTpid())) {
                    fansUserIds.add(bill.getTpid());
                }
            }

            if (CollectionUtils.isEmpty(orderIds)) {
                return ResponseUtil.fail(ORDER_UNKNOWN, "订单不存在");
            }

            // 查询粉丝的用户信息
            List<MallUser> fansUserList = userService.findByIds(fansUserIds);
            // 查询充值流水下的订单信息
            List<MallOrder> orderList = orderService.findByIds(orderIds);
            for (int i = 0; i < list.size(); i++) {
                FcAccountRechargeBill bill = list.get(i);
                String orderId = bill.getOrderId();
                String tpid = bill.getTpid();

                CollectionUtils.find(fansUserList, PredicateUtils.equalPredicate(tpid));


                Object userInfo = CollectionUtils.find(fansUserList, new Predicate() {

                    @Override
                    public boolean evaluate(Object object) {
                        MallUser user = (MallUser) object;
                        if (tpid != null && tpid.equals(user.getId())) {
                            return true;
                        }
                        return false;
                    }
                });

                for (int j = 0; j < orderList.size(); j++) {
                    MallOrder order = orderList.get(j);
                    if (orderId != null && orderId.equals(order.getId())) {
                        Map<String, Object> orderVo = new HashMap<String, Object>();
                        orderVo.put("id", order.getId());
                        orderVo.put("orderSn", order.getOrderSn());
                        orderVo.put("addTime", order.getAddTime());
                        orderVo.put("consignee", order.getConsignee());
                        orderVo.put("mobile", order.getMobile());
                        orderVo.put("address", order.getAddress());
                        orderVo.put("goodsPrice", order.getGoodsPrice());
                        orderVo.put("couponPrice", order.getCouponPrice());
                        orderVo.put("freightPrice", order.getFreightPrice());
                        orderVo.put("actualPrice", order.getActualPrice());
                        orderVo.put("orderStatusText", OrderUtil.orderStatusText(order));
                        orderVo.put("handleOption", OrderUtil.build(order));
                        orderVo.put("expCode", order.getShipChannel());
                        orderVo.put("expNo", order.getShipSn());

                        List<MallOrderGoods> orderGoodsList = orderGoodsService.queryByOid(order.getId());

                        Map<String, Object> results = new HashMap<>();
                        results.put("orderInfo", orderVo);
                        results.put("orderGoods", orderGoodsList);
                        results.put("bill", bill);
                        results.put("userInfo", userInfo);

                        resultList.add(results);
                    }
                }
            }



            result.put("billList", resultList);
            result.put("totalPage", totalPages);
        }

        try {
            log.info("查询出订单数据 ： " + JacksonUtils.bean2Jsn(prechargelist));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (CollectionUtils.isNotEmpty(prechargelist)) {
            for (int i = 0; i < prechargelist.size(); i++) {
                FcAccountPrechargeBill bill = prechargelist.get(i);
                String orderId = bill.getOrderId();
                if (!orderIds.contains(orderId)) {
                    orderIds.add(orderId);
                }

                if (!fansUserIds.contains(bill.getTpid())) {
                    fansUserIds.add(bill.getTpid());
                }

            }

            if (CollectionUtils.isEmpty(orderIds)) {
                return ResponseUtil.fail(ORDER_UNKNOWN, "订单不存在");
            }
            // 查询粉丝的用户信息
            List<MallUser> fansUserList = userService.findByIds(fansUserIds);
            // 查询订单
            List<MallOrder> orderList = orderService.findByIds(orderIds);
            for (int i = 0; i < prechargelist.size(); i++) {
                FcAccountPrechargeBill bill = prechargelist.get(i);
                String orderId = bill.getOrderId();
                String tpid = bill.getTpid();

                Object userInfo = CollectionUtils.find(fansUserList, new Predicate() {

                    @Override
                    public boolean evaluate(Object object) {
                        MallUser user = (MallUser) object;
                        if (tpid != null && tpid.equals(user.getId())) {
                            return true;
                        }
                        return false;
                    }
                });

                for (int j = 0; j < orderList.size(); j++) {

                    MallOrder order = orderList.get(j);


                    if (orderId != null && orderId.equals(order.getId())) {
                        Map<String, Object> orderVo = new HashMap<String, Object>();
                        orderVo.put("id", order.getId());
                        orderVo.put("orderSn", order.getOrderSn());
                        orderVo.put("addTime", order.getAddTime());
                        orderVo.put("consignee", order.getConsignee());
                        orderVo.put("mobile", order.getMobile());
                        orderVo.put("address", order.getAddress());
                        orderVo.put("goodsPrice", order.getGoodsPrice());
                        orderVo.put("couponPrice", order.getCouponPrice());
                        orderVo.put("freightPrice", order.getFreightPrice());
                        orderVo.put("actualPrice", order.getActualPrice());
                        orderVo.put("orderStatusText", OrderUtil.orderStatusText(order));
                        orderVo.put("handleOption", OrderUtil.build(order));
                        orderVo.put("expCode", order.getShipChannel());
                        orderVo.put("expNo", order.getShipSn());

                        List<MallOrderGoods> orderGoodsList = orderGoodsService.queryByOid(order.getId());

                        Map<String, Object> results = new HashMap<>();
                        results.put("orderInfo", orderVo);
                        results.put("orderGoods", orderGoodsList);
                        results.put("bill", bill);
                        results.put("userInfo", userInfo);
                        resultList.add(results);
                    }
                }
            }

            result.put("billList", resultList);
            result.put("totalPage", totalPages);
        }


        return ResponseUtil.ok("分润数据查询成功", result);
    }


    @RequestMapping(value = "/recharge/detail")
    public Object getRechargeDetail(HttpServletResponse response, HttpServletRequest request, @LoginUser String userId,
                    @RequestParam(defaultValue = "recharge") String revenueType, @RequestParam String rechargeBillId)
                    throws Exception {
        Map<String, Object> result = new HashMap<String, Object>();

        if ("recharge".equalsIgnoreCase(revenueType)) {
            FcAccountRechargeBill rechargeBill = accountManager.getAccountRechargeBill(rechargeBillId);


            if (rechargeBill != null) {

                String customerId = rechargeBill.getCustomerId();
                String tpid = rechargeBill.getTpid();
                // 说明是他人分销分润给自己
                if (customerId != null && tpid != null && customerId.equals(userId.toString())
                                && !customerId.equals(tpid.toString())) {

                    MallUser fansUserInfo = userService.findById(tpid);
                    Map<String, Object> fansCon = accountManager.getFansContribution(userId.toString(), tpid);
                    result.put("fansCon", fansCon);
                    result.put("fansUserInfo", fansUserInfo);
                }
                Object orderDetail = wxOrderService.detail(userId, rechargeBill.getOrderId());

                result.put("bill", rechargeBill);
                result.put("orderDetail", orderDetail);

                return ResponseUtil.ok("分润数据查询成功", result);
            }
        } else {
            FcAccountPrechargeBill rechargeBill = accountManager.getAccountPrechargeBill(rechargeBillId);
            if (rechargeBill != null) {
                Object orderDetail = wxOrderService.detail(userId, rechargeBill.getOrderId());

                String customerId = rechargeBill.getCustomerId();
                String tpid = rechargeBill.getTpid();
                // 说明是他人分销分润给自己
                if (customerId != null && tpid != null && customerId.equals(userId.toString())
                                && !customerId.equals(tpid.toString())) {

                    MallUser fansUserInfo = userService.findById(tpid);
                    Map<String, Object> fansCon = accountManager.getFansContribution(userId.toString(), tpid);
                    result.put("fansCon", fansCon);
                    result.put("fansUserInfo", fansUserInfo);
                }

                result.put("bill", rechargeBill);
                result.put("orderDetail", orderDetail);

                return ResponseUtil.ok("分润数据查询成功", result);
            }
        }


        return ResponseUtil.fail(-1, "充值详情查询为空");
    }

    /**
     * 获取绩效
     *
     * @param response
     * @param request
     * @param paramJson
     * @param userId
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/achievement", method = RequestMethod.POST)
    public Object getCustomerAchievement(HttpServletResponse response, HttpServletRequest request,
                    @RequestBody String paramJson, @LoginUser String userId) throws Exception {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        log.info("接口调用过来了....." + userId);
        Map<String, Object> map = accountManager.getCustomerAchievement(userId.toString());

        return ResponseUtil.ok("收益查询成功", map);
    }

    /**
     * 发起提现
     *
     * @param response
     * @param request
     * @param bill
     * @param userId
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/apply/cashwithdrawal", method = RequestMethod.POST)
    public Object applyCashWithdrawal(HttpServletResponse response, HttpServletRequest request,
                    @RequestBody FcAccountWithdrawBill bill, @LoginUser String userId) throws Exception {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        log.info("接口调用过来了....." + userId);
        FcAccount account = accountManager.getAccountByKey(userId.toString());

        // 获取是当前的几号
        int dayCal = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);

        // if (dayCal != 10 && dayCal != 20 && dayCal != 30) {
        // return ResponseUtil.fail(-1, "当前不是提现日期");
        // }

        String smsType = "WITHDRAW";
        try {
            if (account != null && bill != null) {
                // MallPhoneValidation val = phoneService.getPhoneValidationByCode(bill.getMobile(),
                // bill.getValCode(), smsType);
                //
                // if (val == null) {
                // return ResponseUtil.fail(AUTH_NAME_REGISTERED, "验证码有误");
                // }

                bill.setCustomerId(userId.toString());
                bill.setAccountId(account.getAccountId());
                // 返回值 -1 小于 0 等于 1 大于
                if (bill.getAmt().compareTo(account.getAmount()) == -1) {
                    log.info("提现金额和账户有的金额 getAmt： " + bill.getAmt().toString());
                    log.info("提现金额和账户有的金额getAmount ： " + account.getAmount().toString());

                    accountManager.addWithdraw(bill, account);

                    // MallPhoneValidation updateVal = new MallPhoneValidation();
                    // updateVal.setId(val.getId());
                    // updateVal.setValState("00");
                    // phoneService.update(updateVal);

                    return ResponseUtil.ok();
                } else {
                    return ResponseUtil.fail(-1, "账户金额少于提现金额");
                }
            } else {
                return ResponseUtil.fail(-1, "账户不存在");
            }
        } catch (Exception e) {
            log.error("发起提现异常:" + e.getMessage(), e);
        }

        return ResponseUtil.fail(-1, "账户不存在");

    }

    @RequestMapping(value = "/withdrawal/bill")
    public Object getWithdrawBill(HttpServletResponse response, HttpServletRequest request,
                    @RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "30") Integer size,
                    @LoginUser String userId) throws Exception {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }

        Map<String, Object> dataList = new HashMap<String, Object>();
        log.info("接口调用过来了....." + userId);
        List<FcAccountWithdrawBill> list = accountManager.getWithdrawBill(page, size, userId.toString());
        Long total = accountManager.getWithdrawBillCount(userId.toString());

        Integer totalPages = (int) Math.ceil((double) Integer.parseInt(total.toString()) / size);


        dataList.put("billList", list);
        dataList.put("totalPage", totalPages);

        return ResponseUtil.ok("查询账户申请提现流水成功", dataList);
    }


    /**
     * 获取个人中心的广告和类目图
     */
    @GetMapping("/getadsAndCategorys")
    public Object getadsAndCategorys(@LoginUser String userId, HttpServletRequest request,
                    @RequestParam(defaultValue = "1.6.6") String version) {
        // if (userId == null) {
        // return ResponseUtil.unlogin();
        // }

        List<MallCategory> secondCategories = null;
        Map<String, Object> map = new HashMap<String, Object>();
        List<MallCategory> firstCategories = categoryService.queryL1("firstpersoncenter");
        secondCategories = categoryService.queryL1("secondpersoncenter");
        List<MallCategory> underCategories = categoryService.queryL1("personundericon");
        // 个人中心切换成过审类目

        if (RetrialUtils.examine(request, version)) {

            secondCategories = new ArrayList<>();

            // firstCategories.stream().filter(bean -> {
            // if (Objects.equals("邀请好友", bean.getName()))
            // bean.setName("我的特权");
            // bean.setLinkUrl(null);
            // return true;
            // }).collect(Collectors.toList());

            for (int i = 0; i < firstCategories.size(); i++) {
                if (Objects.equals("我的粉丝", firstCategories.get(i).getName())) {
                    firstCategories.get(i).setName("出行订单");
                    firstCategories.get(i).setLinkUrl("/pages/user/order-tracking/gas");
                }

                if (Objects.equals("邀请好友", firstCategories.get(i).getName())) {
                    firstCategories.get(i).setName("kfc订单");
                    firstCategories.get(i).setLinkUrl("/pages/user/order-tracking/KFC");
                }

                if (Objects.equals("我的优惠券", firstCategories.get(i).getName())) {
                    firstCategories.get(i).setName("美食订单");
                    firstCategories.get(i).setLinkUrl("/pages/user/order-tracking/meituan");
                }

                if (Objects.equals("我的订单", firstCategories.get(i).getName())) {
                    firstCategories.get(i).setLinkUrl("/pages/order/order-list");
                }

            }

            for (MallCategory mallCategory : underCategories) {
                if (Objects.equals("充值中心", mallCategory.getName())) {
                    mallCategory.setName("万款好礼");
                    mallCategory.setLinkUrl("/pages/brand-specials/list");
                    mallCategory.setIconUrl(
                                    "https://gxfacepay.oss-cn-shenzhen.aliyuncs.com/hummingbird/Examine/wankuanhaoli.png");
                }

                if (Objects.equals("天天补贴", mallCategory.getName())) {
                    mallCategory.setName("皮具箱包");
                    mallCategory.setLinkUrl("/pages/index/IndustryDetails?cateId=11");
                    mallCategory.setIconUrl(
                                    "https://ddangxiaobao.oss-cn-shenzhen.aliyuncs.com/mall/upload/y6jefyanu36xt9t2jlha.png");
                }

                if (Objects.equals("在线游戏", mallCategory.getName())) {
                    mallCategory.setName("在线购物");
                    mallCategory.setLinkUrl("/pages/crazy/list");
                    mallCategory.setIconUrl(
                                    "https://gxfacepay.oss-cn-shenzhen.aliyuncs.com/hummingbird/person/shopping.png");
                }

                if (Objects.equals("畅销好书", mallCategory.getName())) {
                    mallCategory.setLinkUrl(
                                    "/pages/web/webview?link=https://3.cn/14Q-ZR3Z?&utm_source=iosapp&utm_medium=appshare&utm_campaign=t_335139774&utm_term=CopyURL&ad_od=share&utm_user=plusmember");
                }

                if (Objects.equals("优选好课", mallCategory.getName())) {
                    mallCategory.setLinkUrl("/pages/strictSelection/index");
                }

                if (Objects.equals("加油特惠", mallCategory.getName())) {
                    mallCategory.setName("9.9包邮");
                    mallCategory.setLinkUrl("/pages/nine/list");
                }


                if (Objects.equals("肯德基", mallCategory.getName())) {
                    mallCategory.setName("整点秒杀");
                    mallCategory.setLinkUrl("/pages/seckill-time/list");
                    mallCategory.setIconUrl(
                                    "https://gxfacepay.oss-cn-shenzhen.aliyuncs.com/hummingbird/Examine/skill.png");
                }


                if (Objects.equals("外卖中心", mallCategory.getName())) {
                    mallCategory.setName("配饰");
                    mallCategory.setLinkUrl(
                                    "/pages/kmi/qs-product/product?platform=1&industryId=4&position_code=dtkCategory&is_select_category=0&categoryId=1257549570755104769");
                    mallCategory.setIconUrl(
                                    "https://gxfacepay.oss-cn-shenzhen.aliyuncs.com/hummingbird/index/newicon/1%20(9).png");

                }

                if (Objects.equals("大牌返利", mallCategory.getName())) {
                    mallCategory.setLinkUrl("/pages/brand-specials/list");
                }

            }

        }

        List<MallAd> ads = adService.queryIndex((byte) 4);

        map.put("firstCategories", firstCategories);
        map.put("secondCategories", secondCategories);
        map.put("underCategories", underCategories);
        map.put("ads", ads);
        return ResponseUtil.ok(map);
    }

    /**
     * 获取用户的预估收益
     */
    @GetMapping("/getPreProfit")
    public Object getPreProfit(@LoginUser String userId) {
        // if (userId == null) {
        // return ResponseUtil.unlogin();
        // }

        if (userId == null) {
            Map<String, Object> map = new HashMap<>();
            map.put("withdrawableMoney", new BigDecimal("0.00")); // 可提现余额
            map.put("recharge", new BigDecimal("0.00")); // 累计总收益
            map.put("todayPrecharge", new BigDecimal("0.00")); // 今日预估收益
            map.put("thisMonthPrecharge", new BigDecimal("0.00")); // 本月预估收益
            return ResponseUtil.ok(map);
        }


        MallUser user = userService.findById(userId);
        if (Objects.equals(user, null)) {
            return ResponseUtil.badArgumentValue("查询用户信息异常!");
        }

        FcAccount fcAccount = accountManager.getAccountByKey(userId);
        // 可提现余额
        BigDecimal withdrawableMoney = new BigDecimal(0.00);

        if (fcAccount != null) {
            withdrawableMoney = fcAccount.getAmount();
        }

        BigDecimal totalPrecharge = fcAccountPrechargeBillService.selectPrechargeAmountByCondition(userId, true, null,
                        null, CommonConstant.TASK_REWARD_CASH, null, null); // 总的预估收益
        BigDecimal totalRecharge = fcAccountRechargeBillService.selectRechargeAmountByCondition(userId, true, null,
                        null, CommonConstant.TASK_REWARD_CASH, null, null); // 总的收益

        if (null == totalPrecharge) {
            totalPrecharge = BigDecimal.ZERO;
        }

        if (null == totalRecharge) {
            totalRecharge = BigDecimal.ZERO;
        }

        LocalDateTime today = LocalDateTime.of(LocalDate.now(), LocalTime.of(0, 0, 0)); // 今天
        LocalDateTime tomorrow = today.plusDays(1); // 明天

        int year = LocalDate.now().getYear(); // 年
        int month = LocalDate.now().getMonthValue(); // 月
        LocalDate date = LocalDate.of(year, month, 1); // 本月1号
        LocalDateTime thisMonth = LocalDateTime.of(date, LocalTime.of(0, 0, 0)); // 本月
        LocalDateTime nextMonth = LocalDateTime.of(date.plusMonths(1), LocalTime.of(0, 0, 0)); // 下月

        // 累计收益
        BigDecimal recharge = totalPrecharge.add(totalRecharge);
        // 今日预估收益
        BigDecimal todayPrecharge = fcAccountPrechargeBillService.selectPrechargeAmountByCondition(userId, true, null,
                        null, CommonConstant.TASK_REWARD_CASH, today, tomorrow);
        // 本月预估收益
        BigDecimal thisMonthPrecharge = fcAccountPrechargeBillService.selectPrechargeAmountByCondition(userId, true,
                        null, null, CommonConstant.TASK_REWARD_CASH, thisMonth, nextMonth);

        Map<String, Object> map = new HashMap<>();
        map.put("withdrawableMoney", withdrawableMoney); // 可提现余额
        map.put("recharge", recharge); // 累计总收益
        map.put("todayPrecharge", todayPrecharge == null ? BigDecimal.ZERO : todayPrecharge); // 今日预估收益
        map.put("thisMonthPrecharge", thisMonthPrecharge == null ? BigDecimal.ZERO : thisMonthPrecharge); // 本月预估收益
        return ResponseUtil.ok(map);
    }


    /**
     * 收益明细列表-代理端
     *
     * @param userId 用户id
     * @param type (1:全部返利 2:待入账 3:已入账 4:已失效)
     */
    @GetMapping(value = "/incomeDetailList")
    public Object incomeDetailList(@LoginUser String userId, @RequestParam(defaultValue = "1") Integer type, @RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        Integer start = (page - 1) * limit;
        List<FcAccountRechargeBillVo> rechargeList = null;
        List<FcAccountRechargeBillVo> prechargeList = null;
        List<String> bidList = AccountStatus.BID_LIST_FANGTAO;
        Map<String, Object> map = new HashMap<String, Object>();
        Map<String, Object> listMap = new HashMap<String, Object>();
        if (type.intValue()==1) {
            // 全部返利
            // (已入账、失效)
            rechargeList = fcAccountRechargeBillService.getAccountRechargeBillListByUserId2(userId, bidList, AccountStatus.DATA_STATUS_1, null, CommonConstant.TASK_REWARD_CASH, start, limit);
            // (待入账、失效)
            prechargeList = fcAccountPrechargeBillService.getAccountPrechargeBillListByUserId2(null, userId, bidList, AccountStatus.DATA_STATUS_1, null, CommonConstant.TASK_REWARD_CASH, start, limit);
        } else if (type.intValue() == 2) {
            // 待入账
            prechargeList = fcAccountPrechargeBillService.getAccountPrechargeBillListByUserId2(null, userId, bidList, AccountStatus.DATA_STATUS_1, null, CommonConstant.TASK_REWARD_CASH, start, limit);
        } else if (type.intValue() == 3) {
            // 已入账
            rechargeList = fcAccountRechargeBillService.getAccountRechargeBillListByUserId2(userId, bidList, AccountStatus.DATA_STATUS_1, null, CommonConstant.TASK_REWARD_CASH, start, limit);
        } else {
            // 已失效(删除的订单信息)
            rechargeList = fcAccountRechargeBillService.getAccountRechargeBillListByUserId2(userId, bidList, AccountStatus.DATA_STATUS_0, null, CommonConstant.TASK_REWARD_CASH, start, limit);
            // 已失效(删除的订单信息)
            prechargeList = fcAccountPrechargeBillService.getAccountPrechargeBillListByUserId2("1", userId, bidList, AccountStatus.DATA_STATUS_0, null, CommonConstant.TASK_REWARD_CASH, start, limit);
        }

        listMap.put("rechargeList", rechargeList);
        listMap.put("prechargeList", prechargeList);
        map.put("list", listMap);
        return ResponseUtil.ok("返利数据查询成功", map);
    }


    /**
     * 钱包流水--未莱生活&IOT代理端
     */
    @GetMapping("/streamList")
    public Object getStreamList(@LoginUser String userId, @RequestParam(defaultValue = "1") Integer page,
                                @RequestParam(defaultValue = "10") Integer limit, @RequestParam(defaultValue = "") String startTime,
                                @RequestParam(defaultValue = "") String endTime, @RequestParam(required = false) String tradeType,
                                @RequestParam(defaultValue = "1") String accountType) {

        try {

            log.info("page=" + page + "；limit=" + limit + "；startTime=" + startTime + "；endTime=" + endTime + "；tradeType=" + tradeType + "； accountType=" + accountType);

            List<String> typeList = new ArrayList<>();
            if ("1".equals(accountType)) {
                // 零钱钱包
                typeList = AccountStatus.BID_LIST_IOT_TRADE;
            } else {
                return ResponseUtil.fail("任务钱包类型目前不存在");
            }

            LocalDateTime beginDate = null;
            LocalDateTime finalDate = null;
            if (!"".equals(startTime)){
                beginDate = LocalDateTimeUtil.parse(startTime+" 00:00:00","yyyy-MM-dd HH:mm:ss");
                finalDate = LocalDateTimeUtil.parse(endTime+" 23:59:59","yyyy-MM-dd HH:mm:ss");
            }
            Map<String, Object> mapdata = new HashMap<>();
            if("0".equals(tradeType)){
                PageHelper.startPage(page,limit);
                List<Map<String,Object>> list= accountManager.queryAccountStreamListAll(userId,beginDate,finalDate);
                PageInfo<Map<String,Object>> pageInfo=new PageInfo<>(list);
                mapdata.put("total", pageInfo.getTotal());
                mapdata.put("items", list);
            }else if("07".equals(tradeType)){
                PageHelper.startPage(page,limit);
                List<Map<String,Object>> list= accountManager.queryAccountStreamListByTradeType(userId,AccountStatus.TRADE_TYPE_07,beginDate,finalDate,null);
                PageInfo<Map<String,Object>> pageInfo=new PageInfo<>(list);
                mapdata.put("total", pageInfo.getTotal());
                mapdata.put("items", list);
            } else {

                if("09".equals(tradeType)){
                    String note="商家推荐";
                    PageHelper.startPage(page,limit);
                    List<Map<String,Object>> list= accountManager.queryAccountStreamListByTradeType(userId,AccountStatus.TRADE_TYPE_37,beginDate,finalDate,note);
                    PageInfo<Map<String,Object>> pageInfo=new PageInfo<>(list);
                    mapdata.put("total", pageInfo.getTotal());
                    mapdata.put("items", list);
                }else if("04".equals(tradeType)){
                    String note="服务商推荐";
                    PageHelper.startPage(page,limit);
                    List<Map<String,Object>> list= accountManager.queryAccountStreamListByTradeType(userId,AccountStatus.TRADE_TYPE_37,beginDate,finalDate,note);
                    PageInfo<Map<String,Object>> pageInfo=new PageInfo<>(list);
                    mapdata.put("total", pageInfo.getTotal());
                    mapdata.put("items", list);
                }else if("05".equals(tradeType)){
                    String note="区代推荐";
                    PageHelper.startPage(page,limit);
                    List<Map<String,Object>> list= accountManager.queryAccountStreamListByTradeType(userId,AccountStatus.TRADE_TYPE_37,beginDate,finalDate,note);
                    PageInfo<Map<String,Object>> pageInfo=new PageInfo<>(list);
                    mapdata.put("total", pageInfo.getTotal());
                    mapdata.put("items", list);
                }else if("06".equals(tradeType)){
                    String note="市代推荐";
                    PageHelper.startPage(page,limit);
                    List<Map<String,Object>> list= accountManager.queryAccountStreamListByTradeType(userId,AccountStatus.TRADE_TYPE_37,beginDate,finalDate,note);
                    PageInfo<Map<String,Object>> pageInfo=new PageInfo<>(list);
                    mapdata.put("total", pageInfo.getTotal());
                    mapdata.put("items", list);
                }else if("10".equals(tradeType)){
                    String note="得分润";
                    PageHelper.startPage(page,limit);
                    List<Map<String,Object>> list= accountManager.queryAccountStreamListByTradeType(userId,AccountStatus.TRADE_TYPE_37,beginDate,finalDate,note);
                    PageInfo<Map<String,Object>> pageInfo=new PageInfo<>(list);
                    mapdata.put("total", pageInfo.getTotal());
                    mapdata.put("items", list);
                }else {
                    return ResponseUtil.ok();
                }
            }

            return ResponseUtil.ok(mapdata);

        } catch (Exception ex) {
            log.error("报错信息： " + ex.getMessage(),ex);
            ex.printStackTrace();
            return ResponseUtil.badArgumentValue();
        }
    }

    /**
     * 积分流水
     * @param userId
     * @param page
     * @param limit
     * @param startTime
     * @param endTime
     * @return
     */
    @GetMapping("getIntegralStreamList")
    public Object getIntegralStreamList(@LoginUser String userId, @RequestParam(defaultValue = "1") Integer page,
                                        @RequestParam(defaultValue = "10") Integer limit, @RequestParam(defaultValue = "") String startTime,
                                        @RequestParam(defaultValue = "") String endTime) {
        LocalDateTime beginDate = null;
        LocalDateTime finalDate = null;

        if (!"".equals(startTime)){
            beginDate = LocalDateTimeUtil.parse(startTime+" 00:00:00","yyyy-MM-dd HH:mm:ss");
            finalDate = LocalDateTimeUtil.parse(endTime+" 23:59:59","yyyy-MM-dd HH:mm:ss");
        }

        List<Map> maps = accountManager.queryIntegralAccountStreamList(userId, page, limit, beginDate, finalDate);
        long total = PageInfo.of(maps).getTotal(); // 总数

        Map<String, Object> mapdata = new HashMap<>();
        mapdata.put("total", total);
        mapdata.put("items", maps);
        return ResponseUtil.ok(mapdata);
    }

    /**
     * 1.数据报表——列表统计 time格式"2020-04-03"
     */
    @GetMapping("/getBillStatistics")
    public Object getRechargeBillStatistics(@LoginUser String userId,
                    @RequestParam(value = "time", required = false) String time) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        LocalDateTime beginTime = null;
        LocalDateTime endTime = null;
        if (time != null) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            beginTime = LocalDateTime.parse(time + " 00:00:00", formatter);
            endTime = beginTime.plusDays(1);
        }

        // 付款单数
        int totalCount = orderService.count(userId, null, null, beginTime, endTime);
        // 销售额
        BigDecimal totalAmount = orderService.selectAmountByCondition(userId, null, beginTime, endTime);
        // 预估收益
        BigDecimal totalPreprofit = fcAccountPrechargeBillService.selectPrechargeAmountByCondition(userId, true, null,
                        AccountStatus.DISTRIBUTION_TYPE_00, CommonConstant.TASK_REWARD_CASH, beginTime, endTime);
        Boolean extensionFlag = true;
        Boolean shopFlag = true;
        Boolean equityFlag = true;
        Boolean taskFlag = true;
        Boolean gameFlag = true;
        Boolean contentFlag = true;
        Boolean advertFlag = true;

        List<Object> profitList = new ArrayList<>();
        // todo 判断用户等级，根据等级展示 页面可以显示的数据信息
        MallUser mallUser = userService.findById(userId);
        BmdesignUserLevel bmdesignUserLevel =
                        bmdesignUserLevelService.selectBmdesignUserLevelById(mallUser.getLevelId());

        // todo 查询等级应该展示的数据信息



        /** (1)推广收益 **/
        if (extensionFlag) {
            Map<String, Object> extensionProfit = new HashMap<>();
            List<MallUser> mallUserList = userService.getUserByCondition(userId, beginTime, endTime);
            List<String> bidList = new ArrayList<>();
            bidList.add(AccountStatus.BID_11);
            BigDecimal extensionMoney = fcAccountRechargeBillService.selectRechargeAmountByCondition(userId, true,
                            bidList, AccountStatus.DISTRIBUTION_TYPE_00, CommonConstant.TASK_REWARD_CASH, beginTime,
                            endTime);
            BigDecimal extensionPremoney = fcAccountPrechargeBillService.selectPrechargeAmountByCondition(userId, true,
                            bidList, AccountStatus.DISTRIBUTION_TYPE_00, CommonConstant.TASK_REWARD_CASH, beginTime,
                            endTime);

            List<BmdesignUserLevelVo> extensionDetail = new ArrayList<>();
            // 直推的用户信息
            List<BmdesignUserLevel> bmdesignUserLevelList = bmdesignUserLevelService.selectBmdesignUserLevelList();
            if (bmdesignUserLevelList.size() > 0) {
                for (BmdesignUserLevel level : bmdesignUserLevelList) {
                    int levelCount = 0;
                    BigDecimal premoney = new BigDecimal(0.00);
                    if (mallUserList.size() > 0) {
                        for (MallUser user : mallUserList) {
                            if (level.getId().equals(user.getLevelId())) {
                                levelCount++;
                            }
                        }
                    }
                    if (levelCount > 0) {
                        // todo 暂时写死，后面在level表里面增加字段，定义推荐 每个等级段应该获取的奖励金额-------------
                        premoney = new BigDecimal(levelCount).multiply(level.getPrice());
                    }
                    BmdesignUserLevelVo bmdesignUserLevelVo = new BmdesignUserLevelVo();
                    bmdesignUserLevelVo.setId(level.getId());
                    bmdesignUserLevelVo.setDetailName(level.getLevelName());
                    bmdesignUserLevelVo.setDetailPremoney(premoney);
                    bmdesignUserLevelVo.setDetailCount(levelCount);
                    bmdesignUserLevelVo.setLevelNum(level.getLevelNum());
                    bmdesignUserLevelVo.setLevelLogo(level.getLevelLogo());
                    extensionDetail.add(bmdesignUserLevelVo);
                }
            }
            extensionProfit.put("name", "推广收益");
            extensionProfit.put("totalNum", mallUserList.size()); // 推广人数(人)
            extensionProfit.put("money", extensionMoney); // 返佣金额
            extensionProfit.put("premoney", extensionPremoney); // 预估收益
            extensionProfit.put("detail", extensionDetail);
            profitList.add(extensionProfit);
        }

        /** (2)购物返佣 **/
        if (shopFlag) {
            Map<String, Object> shopProfit = new HashMap<>();
            // 淘宝
            List<String> bidList = new ArrayList<>();
            bidList.add(AccountStatus.BID_4);
            int tbCount = orderService.count(userId, AccountStatus.BID_4, null, beginTime, endTime);
            BigDecimal tbMoney = fcAccountRechargeBillService.selectRechargeAmountByCondition(userId, true, bidList,
                            AccountStatus.DISTRIBUTION_TYPE_00, CommonConstant.TASK_REWARD_CASH, beginTime, endTime);
            BigDecimal tbPremoney = fcAccountPrechargeBillService.selectPrechargeAmountByCondition(userId, true,
                            bidList, AccountStatus.DISTRIBUTION_TYPE_00, CommonConstant.TASK_REWARD_CASH, beginTime,
                            endTime);
            // 京东
            List<String> bidList2 = new ArrayList<>();
            bidList2.add(AccountStatus.BID_5);
            int jdCount = orderService.count(userId, AccountStatus.BID_5, null, beginTime, endTime);
            BigDecimal jdMoney = fcAccountRechargeBillService.selectRechargeAmountByCondition(userId, true, bidList2,
                            AccountStatus.DISTRIBUTION_TYPE_00, CommonConstant.TASK_REWARD_CASH, beginTime, endTime);
            BigDecimal jdPremoney = fcAccountPrechargeBillService.selectPrechargeAmountByCondition(userId, true,
                            bidList2, AccountStatus.DISTRIBUTION_TYPE_00, CommonConstant.TASK_REWARD_CASH, beginTime,
                            endTime);
            // 拼多多
            List<String> bidList3 = new ArrayList<>();
            bidList3.add(AccountStatus.BID_8);
            int pddCount = orderService.count(userId, AccountStatus.BID_8, null, beginTime, endTime);
            BigDecimal pddMoney = fcAccountRechargeBillService.selectRechargeAmountByCondition(userId, true, bidList3,
                            AccountStatus.DISTRIBUTION_TYPE_00, CommonConstant.TASK_REWARD_CASH, beginTime, endTime);
            BigDecimal pddPremoney = fcAccountPrechargeBillService.selectPrechargeAmountByCondition(userId, true,
                            bidList3, AccountStatus.DISTRIBUTION_TYPE_00, CommonConstant.TASK_REWARD_CASH, beginTime,
                            endTime);
            // todo 苏宁 待续-----------
            int snCount = 0;
            BigDecimal snMoney = new BigDecimal(0.00);
            BigDecimal snPremoney = new BigDecimal(0.00);


            List<Object> shopdetail = new ArrayList<>();
            Map<String, Object> tbDetail = new HashMap<>();
            tbDetail.put("detailName", "淘宝");
            tbDetail.put("detailCount", tbCount);
            tbDetail.put("detailPremoney", tbPremoney);
            Map<String, Object> jdDetail = new HashMap<>();
            jdDetail.put("detailName", "京东");
            jdDetail.put("detailCount", jdCount);
            jdDetail.put("detailPremoney", jdPremoney);
            Map<String, Object> pddDetail = new HashMap<>();
            pddDetail.put("detailName", "拼多多");
            pddDetail.put("detailCount", pddCount);
            pddDetail.put("detailPremoney", pddPremoney);
            Map<String, Object> snDetail = new HashMap<>();
            snDetail.put("detailName", "苏宁");
            snDetail.put("detailCount", snCount);
            snDetail.put("detailPremoney", snPremoney);

            shopdetail.add(tbDetail);
            shopdetail.add(jdDetail);
            shopdetail.add(pddDetail);
            shopdetail.add(snDetail);

            int shopTotalNum = tbCount + jdCount + pddCount + snCount;
            BigDecimal shopMoney = tbMoney.add(jdMoney).add(pddMoney).add(snMoney);
            BigDecimal shopPremoney = tbPremoney.add(jdPremoney).add(pddPremoney).add(snPremoney);
            shopProfit.put("name", "购物返佣");
            shopProfit.put("totalNum", shopTotalNum); // 付款订单数
            shopProfit.put("money", shopMoney); // 返佣金额
            shopProfit.put("premoney", shopPremoney); // 预估收益
            shopProfit.put("detail", shopdetail);
            profitList.add(shopProfit);
        }

        /** (3)权益返佣 **/
        if (equityFlag) {
            Map<String, Object> equityProfit = new HashMap<>();
            // 充值
            int rechargeCount = 0;
            BigDecimal rechargeMoney = new BigDecimal(0.00);
            BigDecimal rechargePremoney = new BigDecimal(0.00);
            // 影音
            List<String> bidList = new ArrayList<>();
            bidList.add(AccountStatus.BID_2);
            int videoCount = orderService.count(userId, AccountStatus.BID_2, null, beginTime, endTime);
            BigDecimal videoMoney = fcAccountRechargeBillService.selectRechargeAmountByCondition(userId, true, bidList,
                            AccountStatus.DISTRIBUTION_TYPE_00, CommonConstant.TASK_REWARD_CASH, beginTime, endTime);
            BigDecimal videoPremoney = fcAccountPrechargeBillService.selectPrechargeAmountByCondition(userId, true,
                            bidList, AccountStatus.DISTRIBUTION_TYPE_00, CommonConstant.TASK_REWARD_CASH, beginTime,
                            endTime);
            // 生活
            int lifeCount = 0;
            BigDecimal lifeMoney = new BigDecimal(0.00);
            BigDecimal lifePremoney = new BigDecimal(0.00);
            // 加油
            int gasolineCount = 0;
            BigDecimal gasolineMoney = new BigDecimal(0.00);
            BigDecimal gasolinePremoney = new BigDecimal(0.00);

            List<Object> equityDetail = new ArrayList<>();
            Map<String, Object> rechargeDetail = new HashMap<>();
            rechargeDetail.put("detailName", "充值");
            rechargeDetail.put("detailCount", rechargeCount);
            rechargeDetail.put("detailPremoney", rechargePremoney);
            Map<String, Object> videoDetail = new HashMap<>();
            videoDetail.put("detailName", "影音");
            videoDetail.put("detailCount", videoCount);
            videoDetail.put("detailPremoney", videoPremoney);
            Map<String, Object> lifeDetail = new HashMap<>();
            lifeDetail.put("detailName", "生活");
            lifeDetail.put("detailCount", lifeCount);
            lifeDetail.put("detailPremoney", lifePremoney);
            Map<String, Object> gasolineDetail = new HashMap<>();
            gasolineDetail.put("detailName", "充值");
            gasolineDetail.put("detailCount", gasolineCount);
            gasolineDetail.put("detailPremoney", gasolinePremoney);

            equityDetail.add(rechargeDetail);
            equityDetail.add(videoDetail);
            equityDetail.add(lifeDetail);
            equityDetail.add(gasolineDetail);

            int equityTotalNum = rechargeCount + videoCount + lifeCount + gasolineCount;
            BigDecimal equityMoney = rechargeMoney.add(videoMoney).add(lifeMoney).add(gasolineMoney);
            BigDecimal equityPremoney = rechargePremoney.add(videoPremoney).add(lifePremoney).add(gasolinePremoney);
            equityProfit.put("name", "权益返佣");
            equityProfit.put("totalNum", equityTotalNum); // 付款订单数
            equityProfit.put("money", equityMoney); // 返佣金额
            equityProfit.put("premoney", equityPremoney); // 预估收益
            equityProfit.put("detail", equityDetail);
            profitList.add(equityProfit);
        }

        /** (4)任务返佣 **/
        if (taskFlag) {
            List<String> bidList = new ArrayList<>();
            bidList.add(AccountStatus.BID_6);
            Map<String, Object> taskProfit = new HashMap<>();
            int taskConut = orderService.count(userId, AccountStatus.BID_6, null, beginTime, endTime);
            BigDecimal taskMoney = fcAccountRechargeBillService.selectRechargeAmountByCondition(userId, true, bidList,
                            AccountStatus.DISTRIBUTION_TYPE_00, CommonConstant.TASK_REWARD_CASH, beginTime, endTime);
            BigDecimal taskPremoney = fcAccountPrechargeBillService.selectPrechargeAmountByCondition(userId, true,
                            bidList, AccountStatus.DISTRIBUTION_TYPE_00, CommonConstant.TASK_REWARD_CASH, beginTime,
                            endTime);

            taskProfit.put("name", "任务返佣");
            taskProfit.put("totalNum", taskConut); // 付款订单数
            taskProfit.put("money", taskMoney); // 返佣金额
            taskProfit.put("premoney", taskPremoney); // 预估收益
            // taskProfit.put("detail",taskDetail);

            profitList.add(taskProfit);
        }

        /** (5)游戏返佣 **/
        if (gameFlag) {
            List<String> bidList = new ArrayList<>();
            bidList.add(AccountStatus.BID_7);
            Map<String, Object> gameProfit = new HashMap<>();
            int gameConut = orderService.count(userId, AccountStatus.BID_7, null, beginTime, endTime);
            BigDecimal gameMoney = fcAccountRechargeBillService.selectRechargeAmountByCondition(userId, true, bidList,
                            AccountStatus.DISTRIBUTION_TYPE_00, CommonConstant.TASK_REWARD_CASH, beginTime, endTime);
            BigDecimal gamePremoney = fcAccountPrechargeBillService.selectPrechargeAmountByCondition(userId, true,
                            bidList, AccountStatus.DISTRIBUTION_TYPE_00, CommonConstant.TASK_REWARD_CASH, beginTime,
                            endTime);

            gameProfit.put("name", "游戏返佣");
            gameProfit.put("totalNum", gameConut); // 付款订单数
            gameProfit.put("money", gameMoney); // 返佣金额
            gameProfit.put("premoney", gamePremoney); // 预估收益
            profitList.add(gameProfit);
        }

        /** (6)内容返佣(资讯) **/
        if (contentFlag) {
            List<String> bidList = new ArrayList<>();
            bidList.add(AccountStatus.BID_3);
            Map<String, Object> contentProfit = new HashMap<>();
            int contentConut = orderService.count(userId, AccountStatus.BID_3, null, beginTime, endTime);
            BigDecimal contentMoney = fcAccountRechargeBillService.selectRechargeAmountByCondition(userId, true,
                            bidList, AccountStatus.DISTRIBUTION_TYPE_00, CommonConstant.TASK_REWARD_CASH, beginTime,
                            endTime);
            BigDecimal contentPremoney = fcAccountPrechargeBillService.selectPrechargeAmountByCondition(userId, true,
                            bidList, AccountStatus.DISTRIBUTION_TYPE_00, CommonConstant.TASK_REWARD_CASH, beginTime,
                            endTime);

            contentProfit.put("name", "内容返佣");
            contentProfit.put("totalNum", contentConut); // 付款订单数
            contentProfit.put("money", contentMoney); // 返佣金额
            contentProfit.put("premoney", contentPremoney); // 预估收益
            profitList.add(contentProfit);
        }

        /** (7)广告返佣 **/
        if (advertFlag) {
            List<String> bidList = new ArrayList<>();
            bidList.add(AccountStatus.BID_1);
            Map<String, Object> advertProfit = new HashMap<>();
            int advertConut = orderService.count(userId, AccountStatus.BID_1, null, beginTime, endTime);
            BigDecimal advertMoney = fcAccountRechargeBillService.selectRechargeAmountByCondition(userId, true, bidList,
                            AccountStatus.DISTRIBUTION_TYPE_00, CommonConstant.TASK_REWARD_CASH, beginTime, endTime);
            BigDecimal advertPremoney = fcAccountPrechargeBillService.selectPrechargeAmountByCondition(userId, true,
                            bidList, AccountStatus.DISTRIBUTION_TYPE_00, CommonConstant.TASK_REWARD_CASH, beginTime,
                            endTime);

            advertProfit.put("name", "广告返佣");
            advertProfit.put("totalNum", advertConut); // 付款订单数
            advertProfit.put("money", advertMoney); // 返佣金额
            advertProfit.put("premoney", advertPremoney); // 预估收益
            profitList.add(advertProfit);
        }

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("totalCount", totalCount);
        map.put("totalAmount", totalAmount);
        map.put("totalPreprofit", totalPreprofit);
        map.put("profitList", profitList);
        return ResponseUtil.ok(map);
    }


    /**
     * 2.数据报表——图表报告统计 choose：1、今日 2、昨日 3、最近7天
     */
    @GetMapping("/getBillReport")
    public Object getBillReport(@LoginUser String userId,
                    @RequestParam(value = "choose", defaultValue = "1") String choose) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        LocalDateTime beginTime = null;
        LocalDateTime endTime = null;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String time;

        Boolean extensionFlag = true;
        Boolean shopFlag = true;
        Boolean equityFlag = true;
        Boolean taskFlag = true;
        Boolean gameFlag = true;
        Boolean contentFlag = true;
        Boolean advertFlag = true;

        List<Object> profitList = new ArrayList<>();
        List<Object> detailProfitList = new ArrayList<>();
        // todo 判断用户等级，根据等级展示 页面可以显示的数据信息
        MallUser mallUser = userService.findById(userId);
        BmdesignUserLevel bmdesignUserLevel =
                        bmdesignUserLevelService.selectBmdesignUserLevelById(mallUser.getLevelId());
        // todo 查询等级应该展示的数据信息



        /** 1.提现报告 **/
        Map<String, Object> withdrawMap = new HashMap<String, Object>();
        int year = LocalDate.now().getYear(); // 年
        int month = LocalDate.now().getMonthValue(); // 月
        LocalDate date = LocalDate.of(year, month, 1);
        LocalDateTime beginDate = LocalDateTime.of(date, LocalTime.of(0, 0, 0)); // 开始时间
        LocalDateTime endDate = beginDate.plusMonths(1); // 结束时间


        // todo 本月可提现，暂时写死
        BigDecimal withdrawableMoney = new BigDecimal(0.00).setScale(2, BigDecimal.ROUND_DOWN);
        // 累计提现金额
        BigDecimal withdrawnMoney = fcAccountWithdrawBillService.selectWithdrawBillAmountByCondition(userId, true, null,
                        AccountStatus.WITHDRAW_STATUS_FINISH, null, null);
        withdrawMap.put("withdrawableMoney", withdrawableMoney);
        withdrawMap.put("withdrawnMoney", withdrawnMoney);

        /** 2.收益报告(充值、预充值) **/
        LocalDateTime begin = LocalDateTime.of(LocalDate.now(), LocalTime.of(0, 0, 0));
        LocalDateTime end = null;
        if (choose.equals("1")) {
            beginTime = begin;
            endTime = begin.plusDays(1);
            time = LocalDate.parse(LocalDate.now().toString(), formatter).toString();
            // 今日(总共24个小时,每3个小时为一段,共8段)
            for (int i = 0; i < 8; i++) {
                Map<String, Object> detailMap = new HashMap<String, Object>();
                end = begin.plusHours(3);
                BigDecimal rechargeMoney = fcAccountRechargeBillService.selectRechargeAmountByCondition(userId, true,
                                null, AccountStatus.DISTRIBUTION_TYPE_00, CommonConstant.TASK_REWARD_CASH, begin, end);
                BigDecimal prechargeMoney = fcAccountPrechargeBillService.selectPrechargeAmountByCondition(userId, true,
                                null, AccountStatus.DISTRIBUTION_TYPE_00, CommonConstant.TASK_REWARD_CASH, begin, end);
                BigDecimal money = rechargeMoney.add(prechargeMoney);
                detailMap.put("name", i * 3);
                detailMap.put("value", money);
                profitList.add(detailMap);
                begin = end;
            }
        } else if (choose.equals("2")) {
            beginTime = begin.minusDays(1);
            endTime = begin;
            time = LocalDate.parse(LocalDate.now().minusDays(1).toString(), formatter).toString();
            // 昨日(总共24个小时,每3个小时为一段,共8段)
            begin = begin.minusDays(1);
            for (int i = 0; i < 8; i++) {
                Map<String, Object> detailMap = new HashMap<String, Object>();
                end = begin.plusHours(3);
                BigDecimal rechargeMoney = fcAccountRechargeBillService.selectRechargeAmountByCondition(userId, true,
                                null, AccountStatus.DISTRIBUTION_TYPE_00, CommonConstant.TASK_REWARD_CASH, begin, end);
                BigDecimal prechargeMoney = fcAccountPrechargeBillService.selectPrechargeAmountByCondition(userId, true,
                                null, AccountStatus.DISTRIBUTION_TYPE_00, CommonConstant.TASK_REWARD_CASH, begin, end);
                BigDecimal money = rechargeMoney.add(prechargeMoney);
                detailMap.put("name", i * 3);
                detailMap.put("value", money);
                profitList.add(detailMap);
                begin = end;
            }
        } else {
            beginTime = begin.minusDays(6);
            endTime = begin;
            time = LocalDate.parse(LocalDate.now().minusDays(7).toString(), formatter) + "至"
                            + LocalDate.parse(LocalDate.now().toString(), formatter);
            // 最近7天(最近7天的数据)
            begin = begin.minusDays(6);
            for (int i = 0; i < 7; i++) {
                Map<String, Object> detailMap = new HashMap<String, Object>();
                end = begin.plusDays(1);
                BigDecimal rechargeMoney = fcAccountRechargeBillService.selectRechargeAmountByCondition(userId, true,
                                null, AccountStatus.DISTRIBUTION_TYPE_00, CommonConstant.TASK_REWARD_CASH, begin, end);
                BigDecimal prechargeMoney = fcAccountPrechargeBillService.selectPrechargeAmountByCondition(userId, true,
                                null, AccountStatus.DISTRIBUTION_TYPE_00, CommonConstant.TASK_REWARD_CASH, begin, end);
                BigDecimal money = rechargeMoney.add(prechargeMoney);
                detailMap.put("name", begin.toLocalDate());
                detailMap.put("value", money);
                profitList.add(detailMap);
                begin = end;
            }
        }

        /** 3.收益明细(充值、预充值) **/
        /** (1)推广收益 **/
        if (extensionFlag) {
            List<String> bidList = new ArrayList<>();
            bidList.add(AccountStatus.BID_11);
            Map<String, Object> extensionProfit = new HashMap<>();
            BigDecimal extensionRechargeMoney = fcAccountRechargeBillService.selectRechargeAmountByCondition(userId,
                            true, bidList, AccountStatus.DISTRIBUTION_TYPE_00, CommonConstant.TASK_REWARD_CASH,
                            beginTime, endTime);
            BigDecimal extensionPrechargeMoney = fcAccountPrechargeBillService.selectPrechargeAmountByCondition(userId,
                            true, bidList, AccountStatus.DISTRIBUTION_TYPE_00, CommonConstant.TASK_REWARD_CASH,
                            beginTime, endTime);
            BigDecimal extensionMoney = extensionRechargeMoney.add(extensionPrechargeMoney);
            extensionProfit.put("name", "推广收益");
            extensionProfit.put("money", extensionMoney); // 返佣金额
            detailProfitList.add(extensionProfit);
        }
        /** (2)购物返佣 **/
        if (shopFlag) {
            Map<String, Object> shopProfit = new HashMap<>();
            // 淘宝
            List<String> bidList = new ArrayList<>();
            bidList.add(AccountStatus.BID_4);
            BigDecimal tbRechargeMoney = fcAccountRechargeBillService.selectRechargeAmountByCondition(userId, true,
                            bidList, AccountStatus.DISTRIBUTION_TYPE_00, CommonConstant.TASK_REWARD_CASH, beginTime,
                            endTime);
            BigDecimal tbPrechargeMoney = fcAccountPrechargeBillService.selectPrechargeAmountByCondition(userId, true,
                            bidList, AccountStatus.DISTRIBUTION_TYPE_00, CommonConstant.TASK_REWARD_CASH, beginTime,
                            endTime);
            // 京东
            List<String> bidList2 = new ArrayList<>();
            bidList2.add(AccountStatus.BID_5);
            BigDecimal jdRechargeMoney = fcAccountRechargeBillService.selectRechargeAmountByCondition(userId, true,
                            bidList2, AccountStatus.DISTRIBUTION_TYPE_00, CommonConstant.TASK_REWARD_CASH, beginTime,
                            endTime);
            BigDecimal jdPrechargeMoney = fcAccountPrechargeBillService.selectPrechargeAmountByCondition(userId, true,
                            bidList2, AccountStatus.DISTRIBUTION_TYPE_00, CommonConstant.TASK_REWARD_CASH, beginTime,
                            endTime);

            // 拼多多
            List<String> bidList3 = new ArrayList<>();
            bidList3.add(AccountStatus.BID_8);
            BigDecimal pddRechargeMoney = fcAccountRechargeBillService.selectRechargeAmountByCondition(userId, true,
                            bidList3, AccountStatus.DISTRIBUTION_TYPE_00, CommonConstant.TASK_REWARD_CASH, beginTime,
                            endTime);
            BigDecimal pddPrechargeMoney = fcAccountPrechargeBillService.selectPrechargeAmountByCondition(userId, true,
                            bidList3, AccountStatus.DISTRIBUTION_TYPE_00, CommonConstant.TASK_REWARD_CASH, beginTime,
                            endTime);
            // todo 苏宁 待续-----------
            BigDecimal snRechargeMoney = new BigDecimal(0.00);
            BigDecimal snPrechargeMoney = new BigDecimal(0.00);


            BigDecimal shopMoney = tbRechargeMoney.add(jdRechargeMoney).add(pddRechargeMoney).add(snRechargeMoney)
                            .add(tbPrechargeMoney).add(jdPrechargeMoney).add(pddPrechargeMoney).add(snPrechargeMoney);
            shopProfit.put("name", "购物返佣");
            shopProfit.put("money", shopMoney); // 返佣金额
            detailProfitList.add(shopProfit);
        }
        /** (3)权益返佣 **/
        if (equityFlag) {
            Map<String, Object> equityProfit = new HashMap<>();
            // 充值
            BigDecimal rechargeMoney = new BigDecimal(0.00);
            BigDecimal prechargeMoney = new BigDecimal(0.00);
            // 影音
            List<String> bidList = new ArrayList<>();
            bidList.add(AccountStatus.BID_2);
            BigDecimal videoRechargeMoney = fcAccountRechargeBillService.selectRechargeAmountByCondition(userId, true,
                            bidList, AccountStatus.DISTRIBUTION_TYPE_00, CommonConstant.TASK_REWARD_CASH, beginTime,
                            endTime);
            BigDecimal videoPrechargeMoney = fcAccountPrechargeBillService.selectPrechargeAmountByCondition(userId,
                            true, bidList, AccountStatus.DISTRIBUTION_TYPE_00, CommonConstant.TASK_REWARD_CASH,
                            beginTime, endTime);
            // 生活
            BigDecimal lifeRechargeMoney = new BigDecimal(0.00);
            BigDecimal lifePrechargeMoney = new BigDecimal(0.00);
            // 加油
            BigDecimal gasolineRechargeMoney = new BigDecimal(0.00);
            BigDecimal gasolinePrechargeMoney = new BigDecimal(0.00);

            BigDecimal equityMoney = rechargeMoney.add(videoRechargeMoney).add(lifeRechargeMoney)
                            .add(gasolineRechargeMoney).add(prechargeMoney).add(videoPrechargeMoney)
                            .add(lifePrechargeMoney).add(gasolinePrechargeMoney);
            equityProfit.put("name", "权益返佣");
            equityProfit.put("money", equityMoney); // 返佣金额
            detailProfitList.add(equityProfit);
        }
        /** (4)任务返佣 **/
        if (taskFlag) {
            List<String> bidList = new ArrayList<>();
            bidList.add(AccountStatus.BID_6);
            Map<String, Object> taskProfit = new HashMap<>();
            BigDecimal taskRechargeMoney = fcAccountRechargeBillService.selectRechargeAmountByCondition(userId, true,
                            bidList, AccountStatus.DISTRIBUTION_TYPE_00, CommonConstant.TASK_REWARD_CASH, beginTime,
                            endTime);
            BigDecimal taskPrechargeMoney = fcAccountPrechargeBillService.selectPrechargeAmountByCondition(userId, true,
                            bidList, AccountStatus.DISTRIBUTION_TYPE_00, CommonConstant.TASK_REWARD_CASH, beginTime,
                            endTime);
            BigDecimal taskMoney = taskRechargeMoney.add(taskPrechargeMoney);
            taskProfit.put("name", "任务返佣");
            taskProfit.put("money", taskMoney); // 返佣金额
            detailProfitList.add(taskProfit);
        }
        /** (5)游戏返佣 **/
        if (gameFlag) {
            List<String> bidList = new ArrayList<>();
            bidList.add(AccountStatus.BID_7);
            Map<String, Object> gameProfit = new HashMap<>();
            BigDecimal gameRechargeMoney = fcAccountRechargeBillService.selectRechargeAmountByCondition(userId, true,
                            bidList, AccountStatus.DISTRIBUTION_TYPE_00, CommonConstant.TASK_REWARD_CASH, beginTime,
                            endTime);
            BigDecimal gamePrechargeMoney = fcAccountPrechargeBillService.selectPrechargeAmountByCondition(userId, true,
                            bidList, AccountStatus.DISTRIBUTION_TYPE_00, CommonConstant.TASK_REWARD_CASH, beginTime,
                            endTime);
            BigDecimal gameMoney = gameRechargeMoney.add(gamePrechargeMoney);
            gameProfit.put("name", "游戏返佣");
            gameProfit.put("money", gameMoney); // 返佣金额
            detailProfitList.add(gameProfit);
        }
        /** (6)内容返佣(资讯) **/
        if (contentFlag) {
            List<String> bidList = new ArrayList<>();
            bidList.add(AccountStatus.BID_3);
            Map<String, Object> contentProfit = new HashMap<>();
            BigDecimal contentRechargeMoney = fcAccountRechargeBillService.selectRechargeAmountByCondition(userId, true,
                            bidList, AccountStatus.DISTRIBUTION_TYPE_00, CommonConstant.TASK_REWARD_CASH, beginTime,
                            endTime);
            BigDecimal contentPrechargeMoney = fcAccountPrechargeBillService.selectPrechargeAmountByCondition(userId,
                            true, bidList, AccountStatus.DISTRIBUTION_TYPE_00, CommonConstant.TASK_REWARD_CASH,
                            beginTime, endTime);
            BigDecimal contentMoney = contentRechargeMoney.add(contentPrechargeMoney);
            contentProfit.put("name", "内容返佣");
            contentProfit.put("money", contentMoney); // 返佣金额
            detailProfitList.add(contentProfit);
        }
        /** (7)广告返佣 **/
        if (advertFlag) {
            List<String> bidList = new ArrayList<>();
            bidList.add(AccountStatus.BID_1);
            Map<String, Object> advertProfit = new HashMap<>();
            BigDecimal advertRechargeMoney = fcAccountRechargeBillService.selectRechargeAmountByCondition(userId, true,
                            bidList, AccountStatus.DISTRIBUTION_TYPE_00, CommonConstant.TASK_REWARD_CASH, beginTime,
                            endTime);
            BigDecimal advertPrechargeMoney = fcAccountPrechargeBillService.selectPrechargeAmountByCondition(userId,
                            true, bidList, AccountStatus.DISTRIBUTION_TYPE_00, CommonConstant.TASK_REWARD_CASH,
                            beginTime, endTime);
            BigDecimal advertMoney = advertRechargeMoney.add(advertPrechargeMoney);
            advertProfit.put("name", "广告返佣");
            advertProfit.put("money", advertMoney); // 返佣金额
            detailProfitList.add(advertProfit);
        }


        Map<String, Object> map = new HashMap<String, Object>();
        map.put("withdrawMap", withdrawMap); // 提现数据
        map.put("profitList", profitList); // 收益数据
        map.put("detailProfitList", detailProfitList); // 收益明细数据
        map.put("time", time); // 时间
        return ResponseUtil.ok(map);

    }



    /** -------------------------------代理端接口------------------------------- **/
    /**
     * 代理端小程序_统计数据——首页 (服务商：level_id=4，区级代理：level_id=5，市级代理：level_id=6)
     */
    @GetMapping("/getAgentStatistics")
    public Object getAgentStatistics(@LoginUser String userId) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        /** 判断今日 **/
        LocalDate today = LocalDate.now();
        LocalDateTime beginTime = LocalDateTime.of(today, LocalTime.of(0, 0, 0)); // 今日
        LocalDateTime endTime = beginTime.plusDays(1); // 明日
        /** 判断今月 **/
        int year = LocalDate.now().getYear(); // 年
        int month = LocalDate.now().getMonthValue(); // 月
        LocalDate date = LocalDate.of(year, month, 1); // 本月1号
        LocalDateTime thisMonth = LocalDateTime.of(date, LocalTime.of(0, 0, 0)); // 本月
        LocalDateTime nextMonth = LocalDateTime.of(date.plusMonths(1), LocalTime.of(0, 0, 0)); // 下月


        MallUser mallUser = userService.findById(userId);
        if (mallUser == null) {
            return ResponseUtil.badArgumentValue("用户不存在！");
        }
        Boolean isTwo = true;
        Boolean isThree = true;
        String province = mallUser.getProvince();
        String city = mallUser.getCity();
        String county = "";
        if (mallUser.getLevelId().equals(AccountStatus.LEVEL_4)) {
            // 服务商
            isTwo = false;
            isThree = false;
            county = mallUser.getCounty();
        } else if (mallUser.getLevelId().equals(AccountStatus.LEVEL_5)) {
            // 区级代理
            isThree = false;
            county = mallUser.getCounty();
        }

        FcAccount fcAccount = accountManager.getAccountByKey(userId);
        // 可提现余额
        BigDecimal withdrawableMoney = new BigDecimal(0.00);
        if (fcAccount != null) {
            withdrawableMoney = fcAccount.getAgentAmount();
        }

        Map<String, Object> map = new HashMap<>();

        // 查询分润计算方式(key_value 0:服务商、区代、市代方式 1:用户firstLeader、secondLeader、thirderLeader方式 )
        Map<String, String> profitTypeMap = mallSystemConfigService.listProfitType();
        String profitType = profitTypeMap.get("Mall_profit_type");
        Integer uTotal = 0;
        if (Objects.equals(profitType, "0")) {
            /** 方式一(根据省市区) **/
            uTotal = userService.getUserListByType(false, userId, "1", province, city, county, null, null, null).size();
            map.put("uTotal", uTotal); // 用户总数
            if (isTwo) {
                Integer fTotal = userService
                                .getCountByCondition(false, null, province, city, county, AccountStatus.LEVEL_4).size();
                map.put("fTotal", fTotal); // 服务商代总数
            }
            if (isThree) {
                Integer qTotal = userService
                                .getCountByCondition(false, null, province, city, county, AccountStatus.LEVEL_5).size();
                map.put("qTotal", qTotal); // 区代总数
            }
        } else {
            /** 方式二(根据userId) **/
            uTotal = userService.getUserListByType(true, userId, "1", null, null, null, null, null, null).size();
            map.put("uTotal", uTotal); // 用户总数
            if (isTwo) {
                Integer fTotal = userService
                                .getCountByCondition(true, userId, province, city, county, AccountStatus.LEVEL_4)
                                .size();
                map.put("fTotal", fTotal); // 服务商代总数
            }
            if (isThree) {
                Integer qTotal = userService
                                .getCountByCondition(true, userId, province, city, county, AccountStatus.LEVEL_5)
                                .size();
                map.put("qTotal", qTotal); // 区代总数
            }
        }


        // 代理端所有订单指定的业务形态
        List<String> allOrder = AccountStatus.BID_LIST_AGENT;
        // 今日预估订单分润
        BigDecimal todayPrecharge = fcAccountPrechargeBillService.selectPrechargeAmountByCondition(userId, false,
                        allOrder, AccountStatus.DISTRIBUTION_TYPE_10, CommonConstant.TASK_REWARD_CASH, beginTime,
                        endTime);
        // 今日预估报单奖励
        // todo ----------暂时屏蔽----------
        // BigDecimal todayReward =
        // fcAccountPrechargeBillService.selectPrechargeAmountByCondition(userId,false,
        // AccountStatus.BID_30, AccountStatus.DISTRIBUTION_TYPE_00, CommonConstant.TASK_REWARD_CASH,
        // beginTime,endTime);
        BigDecimal todayReward = new BigDecimal(0);
        // 本月预估订单分润
        BigDecimal monthProfite = fcAccountPrechargeBillService.selectPrechargeAmountByCondition(userId, false,
                        allOrder, AccountStatus.DISTRIBUTION_TYPE_10, CommonConstant.TASK_REWARD_CASH, thisMonth,
                        nextMonth);
        // 本月预估报单奖励
        // todo ----------暂时屏蔽----------
        // BigDecimal monthReward = fcAccountPrechargeBillService.selectPrechargeAmountByCondition(userId,
        // false,AccountStatus.BID_30, AccountStatus.DISTRIBUTION_TYPE_00, CommonConstant.TASK_REWARD_CASH,
        // thisMonth,nextMonth);
        BigDecimal monthReward = new BigDecimal(0);
        // 本月预估总收益(本月预估订单分润 + 本月预估报单奖励)
        BigDecimal monthPrecharge = monthProfite.add(monthReward);

        // ----上
        map.put("withdrawableMoney", withdrawableMoney); // 可提现余额
        // ----下
        map.put("todayPrecharge", todayPrecharge); // 今日预估订单分润
        map.put("todayReward", todayReward); // 今日预估报单奖励
        map.put("monthPrecharge", monthPrecharge); // 本月预估总收益
        map.put("monthProfite", monthProfite); // 本月预估订单分润
        map.put("monthReward", monthReward); // 本月预估报单奖励
        return ResponseUtil.ok(map);
    }


    // /**
    // * 代理小程序报表统计数据——首页
    // */
    // @GetMapping("/getAgentReport")
    // public Object getAgentReport(@LoginUser String userId){
    // return ResponseUtil.ok();
    // }



    /**
     * 代理端小程序_收益明细——收益页面
     *
     * @param choose 1、订单分润(预充值) 2、订单分润(已结算) 3、报单奖励(预充值) 3、报单奖励(已结算)
     * @param orderType 1、购物订单 2、权益订单 3、美食订单 4、出行 5、游戏
     */
    @GetMapping("/getAgentRechargeBillList")
    public Object getAgentRechargeBillList(@LoginUser String userId, @RequestParam(defaultValue = "1") String choose,
                    @RequestParam(required = false) String orderType, @RequestParam(required = false) String beginDate,
                    @RequestParam(required = false) String endDate, @RequestParam(defaultValue = "1") Integer page,
                    @RequestParam(defaultValue = "10") Integer limit) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }

        LocalDateTime beginTime = null;
        LocalDateTime endTime = null;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        if (!StringUtils.isEmpty(beginDate)) {
            beginTime = LocalDateTime.parse(beginDate + " 00:00:00", formatter);
        }
        if (!StringUtils.isEmpty(endDate)) {
            endTime = LocalDateTime.parse(endDate + " 23:59:59", formatter);
            // .plusDays(1);
        }

        List<String> allOrder = AccountStatus.BID_LIST_AGENT; // 购物、权益、美食
        List<String> shoppingOrder = AccountStatus.BID_LIST_SHOPPING; // 购物
        List<String> equityOrder = AccountStatus.BID_LIST_EQUITY; // 权益
        List<String> foodOrder = AccountStatus.BID_LIST_FOOD; // 美食
        List<String> walkOrder = AccountStatus.BID_LIST_WALK; // 出行
        List<String> gameOrder = AccountStatus.BID_LIST_GAME; // 游戏
        List<String> makerOrder = AccountStatus.BID_LIST_MAKER; // 创客
        List<String> zyOrder = AccountStatus.BID_LIST_ZY; // 自营

        // 订单分润金额(已结算)
        BigDecimal rechargeCommissionPrice = fcAccountRechargeBillService.selectAgentRechargeAmountByCondition(userId,
                        false, allOrder, AccountStatus.DISTRIBUTION_TYPE_10, CommonConstant.TASK_REWARD_CASH, beginTime,
                        endTime);
        // 订单分润金额(预充值)
        BigDecimal prechargeCommissionPrice = fcAccountPrechargeBillService.selectAgentPrechargeAmountByCondition(
                        userId, false, allOrder, AccountStatus.DISTRIBUTION_TYPE_10, CommonConstant.TASK_REWARD_CASH,
                        beginTime, endTime);

        if (null == rechargeCommissionPrice) {
            rechargeCommissionPrice = BigDecimal.ZERO;
        }

        if (null == prechargeCommissionPrice) {
            prechargeCommissionPrice = BigDecimal.ZERO;
        }
        // 总的订单分润金额
        BigDecimal totalCommissionPrice = rechargeCommissionPrice.add(prechargeCommissionPrice);

        Map<String, Object> map = new HashMap<>();
        map.put("rechargeCommissionPrice", rechargeCommissionPrice); // 订单分润金额(已结算)
        map.put("prechargeCommissionPrice", prechargeCommissionPrice); // 订单分润金额(预充值)
        map.put("totalCommissionPrice", totalCommissionPrice); // 总的订单分润金额

        PageHelper.startPage(page, limit);
        long total = 0;
        /** 订单分润(已结算) **/
        List<FcAccountRechargeBill2Vo> rechargeBill2VoList = null;
        /** 订单分润(预充值) **/
        List<FcAccountRechargeBill2Vo> prechargeBill2VoList = null;
        if ("1".equals(choose)) {
            if ("1".equals(orderType)) {
                /** 购物订单 **/
                rechargeBill2VoList = fcAccountRechargeBillService.getAccountRechargeBill2ListByUserId(userId,
                                shoppingOrder, AccountStatus.DATA_STATUS_1, AccountStatus.DISTRIBUTION_TYPE_10,
                                CommonConstant.TASK_REWARD_CASH, beginDate, endDate);
            } else if ("2".equals(orderType)) {
                /** 权益订单 **/
                rechargeBill2VoList = fcAccountRechargeBillService.getAccountRechargeBill2ListByUserId(userId,
                                equityOrder, AccountStatus.DATA_STATUS_1, AccountStatus.DISTRIBUTION_TYPE_10,
                                CommonConstant.TASK_REWARD_CASH, beginDate, endDate);
            } else if ("3".equals(orderType)) {
                /** 美食订单 **/
                rechargeBill2VoList = fcAccountRechargeBillService.getAccountRechargeBill2ListByUserId(userId,
                                foodOrder, AccountStatus.DATA_STATUS_1, AccountStatus.DISTRIBUTION_TYPE_10,
                                CommonConstant.TASK_REWARD_CASH, beginDate, endDate);
            } else if ("4".equals(orderType)) {
                /** 出行订单 **/
                rechargeBill2VoList = fcAccountRechargeBillService.getAccountRechargeBill2ListByUserId(userId,
                                walkOrder, AccountStatus.DATA_STATUS_1, AccountStatus.DISTRIBUTION_TYPE_10,
                                CommonConstant.TASK_REWARD_CASH, beginDate, endDate);
            } else if ("5".equals(orderType)) {
                /** 游戏订单 **/
                rechargeBill2VoList = fcAccountRechargeBillService.getAccountRechargeBill2ListByUserId(userId,
                                gameOrder, AccountStatus.DATA_STATUS_1, AccountStatus.DISTRIBUTION_TYPE_10,
                                CommonConstant.TASK_REWARD_CASH, beginDate, endDate);
            } else if ("6".equals(orderType)) {
                /** 自营订单 **/
                rechargeBill2VoList = fcAccountRechargeBillService.getAccountRechargeBill2ListByUserId(userId, zyOrder,
                                AccountStatus.DATA_STATUS_1, AccountStatus.DISTRIBUTION_TYPE_10,
                                CommonConstant.TASK_REWARD_CASH, beginDate, endDate);
            } else if ("7".equals(orderType)) {
                /** 创客订单 **/
                rechargeBill2VoList = fcAccountRechargeBillService.getAccountRechargeBill2ListByUserId(userId,
                                makerOrder, AccountStatus.DATA_STATUS_1, AccountStatus.DISTRIBUTION_TYPE_10,
                                CommonConstant.TASK_REWARD_CASH, beginDate, endDate);
            } else {
                /** 所有 **/
                rechargeBill2VoList = fcAccountRechargeBillService.getAccountRechargeBill2ListByUserId(userId, allOrder,
                                AccountStatus.DATA_STATUS_1, AccountStatus.DISTRIBUTION_TYPE_10,
                                CommonConstant.TASK_REWARD_CASH, beginDate, endDate);
            }
            total = PageInfo.of(rechargeBill2VoList).getTotal();
            if (rechargeBill2VoList.size() > 0) {
                // 用户昵称脱敏
                for (int i = 0; i < rechargeBill2VoList.size(); i++) {
                    String nickname = rechargeBill2VoList.get(i).getNickname();
                    if (!StringUtils.isEmpty(nickname)) {
                        // 判断用户昵称类型
                        if (!RegexUtil.isMobileExact(nickname)) {
                            // 不是手机号
                            nickname = NameUtil.nameDesensitizationDef(nickname);
                        } else {
                            // 是手机号
                            nickname = NameUtil.mobileEncrypt(nickname);
                        }
                        rechargeBill2VoList.get(i).setNickname(nickname);
                    }
                }
                // 手机号脱敏
                rechargeBill2VoList = SensitiveUtil.desCopyCollection(rechargeBill2VoList);
            }
            map.put("list", rechargeBill2VoList);
        } else if (choose == "2" || "2".equals(choose)) {
            if ("1".equals(orderType)) {
                /** 购物订单 **/
                prechargeBill2VoList = fcAccountRechargeBillService.getAccountPrechargeBill2ListByUserId(userId,
                                shoppingOrder, AccountStatus.DATA_STATUS_1, AccountStatus.DISTRIBUTION_TYPE_10,
                                CommonConstant.TASK_REWARD_CASH, beginDate, endDate);
            } else if ("2".equals(orderType)) {
                /** 权益订单 **/
                prechargeBill2VoList = fcAccountRechargeBillService.getAccountPrechargeBill2ListByUserId(userId,
                                equityOrder, AccountStatus.DATA_STATUS_1, AccountStatus.DISTRIBUTION_TYPE_10,
                                CommonConstant.TASK_REWARD_CASH, beginDate, endDate);
            } else if ("3".equals(orderType)) {
                /** 美食订单 **/
                prechargeBill2VoList = fcAccountRechargeBillService.getAccountPrechargeBill2ListByUserId(userId,
                                foodOrder, AccountStatus.DATA_STATUS_1, AccountStatus.DISTRIBUTION_TYPE_10,
                                CommonConstant.TASK_REWARD_CASH, beginDate, endDate);
            } else if ("4".equals(orderType)) {
                /** 出行订单 **/
                prechargeBill2VoList = fcAccountRechargeBillService.getAccountPrechargeBill2ListByUserId(userId,
                                walkOrder, AccountStatus.DATA_STATUS_1, AccountStatus.DISTRIBUTION_TYPE_10,
                                CommonConstant.TASK_REWARD_CASH, beginDate, endDate);
            } else if ("5".equals(orderType)) {

                /** 游戏订单 **/
                prechargeBill2VoList = fcAccountRechargeBillService.getAccountPrechargeBill2ListByUserId(userId,
                                gameOrder, AccountStatus.DATA_STATUS_1, AccountStatus.DISTRIBUTION_TYPE_10,
                                CommonConstant.TASK_REWARD_CASH, beginDate, endDate);
            } else if ("6".equals(orderType)) {

                /** 自营订单 **/
                prechargeBill2VoList = fcAccountRechargeBillService.getAccountPrechargeBill2ListByUserId(userId,
                                zyOrder, AccountStatus.DATA_STATUS_1, AccountStatus.DISTRIBUTION_TYPE_10,
                                CommonConstant.TASK_REWARD_CASH, beginDate, endDate);
            } else if ("7".equals(orderType)) {

                /** 创客订单 **/
                prechargeBill2VoList = fcAccountRechargeBillService.getAccountPrechargeBill2ListByUserId(userId,
                                makerOrder, AccountStatus.DATA_STATUS_1, AccountStatus.DISTRIBUTION_TYPE_10,
                                CommonConstant.TASK_REWARD_CASH, beginDate, endDate);
            } else {
                /** 所有 **/
                prechargeBill2VoList = fcAccountRechargeBillService.getAccountPrechargeBill2ListByUserId(userId,
                                allOrder, AccountStatus.DATA_STATUS_1, AccountStatus.DISTRIBUTION_TYPE_10,
                                CommonConstant.TASK_REWARD_CASH, beginDate, endDate);
            }
            total = PageInfo.of(prechargeBill2VoList).getTotal();
            if (prechargeBill2VoList.size() > 0) {
                // 用户昵称脱敏
                for (int i = 0; i < prechargeBill2VoList.size(); i++) {
                    String nickname = prechargeBill2VoList.get(i).getNickname();
                    if (!StringUtils.isEmpty(nickname)) {
                        // 判断用户昵称类型
                        if (!RegexUtil.isMobileExact(nickname)) {
                            // 不是手机号
                            nickname = NameUtil.nameDesensitizationDef(nickname);
                        } else {
                            // 是手机号
                            nickname = NameUtil.mobileEncrypt(nickname);
                        }
                        prechargeBill2VoList.get(i).setNickname(nickname);
                    }
                }
                // 手机号脱敏
                prechargeBill2VoList = SensitiveUtil.desCopyCollection(prechargeBill2VoList);
            }
            map.put("list", prechargeBill2VoList);
        }
        List list = new ArrayList();
        // 查所有
        if (choose == "0" || "0".equals(choose)) {
            if ("0".equals(orderType)) {
                /** 已结算所有订单 **/
                rechargeBill2VoList = fcAccountRechargeBillService.getAccountRechargeBill2ListByUserId(userId, allOrder,
                                AccountStatus.DATA_STATUS_1, AccountStatus.DISTRIBUTION_TYPE_10,
                                CommonConstant.TASK_REWARD_CASH, beginDate, endDate);
                /** 预充值所有订单 **/
                prechargeBill2VoList = fcAccountRechargeBillService.getAccountPrechargeBill2ListByUserId(userId,
                                allOrder, AccountStatus.DATA_STATUS_1, AccountStatus.DISTRIBUTION_TYPE_10,
                                CommonConstant.TASK_REWARD_CASH, beginDate, endDate);

            } else if ("1".equals(orderType)) {
                /** 购物订单 **/
                rechargeBill2VoList = fcAccountRechargeBillService.getAccountRechargeBill2ListByUserId(userId,
                                shoppingOrder, AccountStatus.DATA_STATUS_1, AccountStatus.DISTRIBUTION_TYPE_10,
                                CommonConstant.TASK_REWARD_CASH, beginDate, endDate);
                /** 购物订单 **/
                prechargeBill2VoList = fcAccountRechargeBillService.getAccountPrechargeBill2ListByUserId(userId,
                                shoppingOrder, AccountStatus.DATA_STATUS_1, AccountStatus.DISTRIBUTION_TYPE_10,
                                CommonConstant.TASK_REWARD_CASH, beginDate, endDate);
            } else if ("2".equals(orderType)) {
                /** 权益订单 **/
                rechargeBill2VoList = fcAccountRechargeBillService.getAccountRechargeBill2ListByUserId(userId,
                                equityOrder, AccountStatus.DATA_STATUS_1, AccountStatus.DISTRIBUTION_TYPE_10,
                                CommonConstant.TASK_REWARD_CASH, beginDate, endDate);
                /** 权益订单 **/
                prechargeBill2VoList = fcAccountRechargeBillService.getAccountPrechargeBill2ListByUserId(userId,
                                equityOrder, AccountStatus.DATA_STATUS_1, AccountStatus.DISTRIBUTION_TYPE_10,
                                CommonConstant.TASK_REWARD_CASH, beginDate, endDate);
            } else if ("3".equals(orderType)) {
                /** 美食订单 **/
                rechargeBill2VoList = fcAccountRechargeBillService.getAccountRechargeBill2ListByUserId(userId,
                                foodOrder, AccountStatus.DATA_STATUS_1, AccountStatus.DISTRIBUTION_TYPE_10,
                                CommonConstant.TASK_REWARD_CASH, beginDate, endDate);
                /** 美食订单 **/
                prechargeBill2VoList = fcAccountRechargeBillService.getAccountPrechargeBill2ListByUserId(userId,
                                foodOrder, AccountStatus.DATA_STATUS_1, AccountStatus.DISTRIBUTION_TYPE_10,
                                CommonConstant.TASK_REWARD_CASH, beginDate, endDate);
            } else if ("4".equals(orderType)) {
                /** 出行订单 **/
                rechargeBill2VoList = fcAccountRechargeBillService.getAccountRechargeBill2ListByUserId(userId,
                                walkOrder, AccountStatus.DATA_STATUS_1, AccountStatus.DISTRIBUTION_TYPE_10,
                                CommonConstant.TASK_REWARD_CASH, beginDate, endDate);
                /** 出行订单 **/
                prechargeBill2VoList = fcAccountRechargeBillService.getAccountPrechargeBill2ListByUserId(userId,
                                walkOrder, AccountStatus.DATA_STATUS_1, AccountStatus.DISTRIBUTION_TYPE_10,
                                CommonConstant.TASK_REWARD_CASH, beginDate, endDate);
            } else if ("5".equals(orderType)) {
                /** 游戏订单 **/
                rechargeBill2VoList = fcAccountRechargeBillService.getAccountRechargeBill2ListByUserId(userId,
                                gameOrder, AccountStatus.DATA_STATUS_1, AccountStatus.DISTRIBUTION_TYPE_10,
                                CommonConstant.TASK_REWARD_CASH, beginDate, endDate);
                /** 游戏订单 **/
                prechargeBill2VoList = fcAccountRechargeBillService.getAccountPrechargeBill2ListByUserId(userId,
                                gameOrder, AccountStatus.DATA_STATUS_1, AccountStatus.DISTRIBUTION_TYPE_10,
                                CommonConstant.TASK_REWARD_CASH, beginDate, endDate);
            } else if ("6".equals(orderType)) {
                /** 自营订单 **/
                rechargeBill2VoList = fcAccountRechargeBillService.getAccountRechargeBill2ListByUserId(userId, zyOrder,
                                AccountStatus.DATA_STATUS_1, AccountStatus.DISTRIBUTION_TYPE_10,
                                CommonConstant.TASK_REWARD_CASH, beginDate, endDate);
                /** 自营订单 **/
                prechargeBill2VoList = fcAccountRechargeBillService.getAccountPrechargeBill2ListByUserId(userId,
                                zyOrder, AccountStatus.DATA_STATUS_1, AccountStatus.DISTRIBUTION_TYPE_10,
                                CommonConstant.TASK_REWARD_CASH, beginDate, endDate);
            } else if ("7".equals(orderType)) {
                /** 创客订单 **/
                rechargeBill2VoList = fcAccountRechargeBillService.getAccountRechargeBill2ListByUserId(userId,
                                makerOrder, AccountStatus.DATA_STATUS_1, AccountStatus.DISTRIBUTION_TYPE_10,
                                CommonConstant.TASK_REWARD_CASH, beginDate, endDate);
                /** 创客订单 **/
                prechargeBill2VoList = fcAccountRechargeBillService.getAccountPrechargeBill2ListByUserId(userId,
                                makerOrder, AccountStatus.DATA_STATUS_1, AccountStatus.DISTRIBUTION_TYPE_10,
                                CommonConstant.TASK_REWARD_CASH, beginDate, endDate);
            }

            if (prechargeBill2VoList.size() > 0) {
                // 用户昵称脱敏
                for (int i = 0; i < prechargeBill2VoList.size(); i++) {
                    String nickname = prechargeBill2VoList.get(i).getNickname();
                    if (!StringUtils.isEmpty(nickname)) {
                        // 判断用户昵称类型
                        if (!RegexUtil.isMobileExact(nickname)) {
                            // 不是手机号
                            nickname = NameUtil.nameDesensitizationDef(nickname);
                        } else {
                            // 是手机号
                            nickname = NameUtil.mobileEncrypt(nickname);
                        }
                        prechargeBill2VoList.get(i).setNickname(nickname);
                    }
                }
                // 手机号脱敏
                prechargeBill2VoList = SensitiveUtil.desCopyCollection(prechargeBill2VoList);
            }
            if (rechargeBill2VoList.size() > 0) {
                // 用户昵称脱敏
                for (int i = 0; i < rechargeBill2VoList.size(); i++) {
                    String nickname = rechargeBill2VoList.get(i).getNickname();
                    if (!StringUtils.isEmpty(nickname)) {
                        // 判断用户昵称类型
                        if (!RegexUtil.isMobileExact(nickname)) {
                            // 不是手机号
                            nickname = NameUtil.nameDesensitizationDef(nickname);
                        } else {
                            // 是手机号
                            nickname = NameUtil.mobileEncrypt(nickname);
                        }
                        rechargeBill2VoList.get(i).setNickname(nickname);
                    }
                }
                // 手机号脱敏
                rechargeBill2VoList = SensitiveUtil.desCopyCollection(rechargeBill2VoList);
            }

            for (FcAccountRechargeBill2Vo re : rechargeBill2VoList) {
                re.setStatus("1");
                list.add(re);
            }
            for (FcAccountRechargeBill2Vo pre : prechargeBill2VoList) {
                pre.setStatus("2");
                list.add(pre);
            }
            // 按日期排序
            Object collect = list.stream()
                            .sorted(Comparator.comparing(FcAccountRechargeBill2Vo::getCreateDate).reversed())
                            .collect(Collectors.toList());
            map.put("list", collect);
            total = PageInfo.of(prechargeBill2VoList).getTotal() + PageInfo.of(rechargeBill2VoList).getTotal();
        }
        map.put("total", total);
        return ResponseUtil.ok(map);
    }

    // public Object getAgentRechargeBillList(@LoginUser String userId,
    // @RequestParam(defaultValue = "1") String choose,
    // @RequestParam String beginDate,
    // @RequestParam String endDate,
    // @RequestParam(defaultValue = "1") Integer page,
    // @RequestParam(defaultValue = "10") Integer limit){
    // if (userId == null) {
    // return ResponseUtil.unlogin();
    // }
    // DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    // LocalDateTime beginTime = LocalDateTime.parse(beginDate + " 00:00:00", formatter);
    // LocalDateTime endTime = LocalDateTime.parse(endDate + " 00:00:00", formatter);
    // endTime = endTime.plusDays(1);
    //
    // //订单分润金额(已结算)
    // BigDecimal rechargeCommissionPrice =
    // fcAccountRechargeBillService.selectRechargeAmountByCondition(userId,false,null,
    // AccountStatus.DISTRIBUTION_TYPE_10, CommonConstant.TASK_REWARD_CASH, beginTime, endTime);
    // //订单分润金额(预充值)
    // BigDecimal prechargeCommissionPrice =
    // fcAccountPrechargeBillService.selectPrechargeAmountByCondition(userId,false,null,
    // AccountStatus.DISTRIBUTION_TYPE_10, CommonConstant.TASK_REWARD_CASH, beginTime, endTime);
    // //总的订单分润金额
    // BigDecimal totalCommissionPrice = rechargeCommissionPrice.add(prechargeCommissionPrice);
    //
    //
    // //todo ----------暂时屏蔽----------
    //// //报单奖励金额(已结算)
    //// BigDecimal rechargeRewardPrice =
    // fcAccountRechargeBillService.selectRechargeAmountByCondition(userId,false, AccountStatus.BID_30,
    // AccountStatus.DISTRIBUTION_TYPE_00, CommonConstant.TASK_REWARD_CASH, beginTime, endTime);
    //// //报单奖励金额(预充值)
    //// BigDecimal prechargeRewardPrice =
    // fcAccountPrechargeBillService.selectPrechargeAmountByCondition(userId,false,
    // AccountStatus.BID_30, AccountStatus.DISTRIBUTION_TYPE_00, CommonConstant.TASK_REWARD_CASH,
    // beginTime, endTime);
    //// //总的报单奖励
    //// BigDecimal totalRewardPrice = rechargeRewardPrice.add(prechargeRewardPrice);
    // //todo ----------暂时屏蔽----------
    // BigDecimal price = new BigDecimal(0);
    // List<FcAccountRechargeBill3Vo> defaultList = new ArrayList<>();
    // long defaultTotal = 0;
    //
    //
    // Map<String,Object> map = new HashMap<>();
    // map.put("rechargeCommissionPrice",rechargeCommissionPrice); //订单分润金额(已结算)
    // map.put("prechargeCommissionPrice",prechargeCommissionPrice); //订单分润金额(预充值)
    // map.put("totalCommissionPrice",totalCommissionPrice); //总的订单分润金额
    //
    // map.put("rechargeRewardPrice",price); //报单奖励金额(已结算)
    // map.put("prechargeRewardPrice",price); //报单奖励金额(预充值)
    // map.put("totalRewardPrice",price); //总的报单奖励
    //
    // PageHelper.startPage(page, limit);
    // long total = 0;
    // if(choose == "1" || "1".equals(choose)){
    // /** 订单分润(已结算) **/
    // List<FcAccountRechargeBill2Vo> rechargeBill2VoList =
    // fcAccountRechargeBillService.getAccountRechargeBill2ListByUserId(userId, null,
    // AccountStatus.DATA_STATUS_1, AccountStatus.DISTRIBUTION_TYPE_10, CommonConstant.TASK_REWARD_CASH,
    // beginDate, endDate);
    // total = PageInfo.of(rechargeBill2VoList).getTotal();
    // map.put("list",rechargeBill2VoList);
    // }else if(choose == "2" || "2".equals(choose)){
    // /** 订单分润(预充值) **/
    // List<FcAccountRechargeBill2Vo> prechargeBill2VoList =
    // fcAccountRechargeBillService.getAccountPrechargeBill2ListByUserId(userId, null,
    // AccountStatus.DATA_STATUS_1, AccountStatus.DISTRIBUTION_TYPE_10, CommonConstant.TASK_REWARD_CASH,
    // beginDate, endDate);
    // total = PageInfo.of(prechargeBill2VoList).getTotal();
    // map.put("list",prechargeBill2VoList);
    // }else if(choose == "3" || "3".equals(choose)){
    // /** 报单奖励(已结算) **/
    // List<FcAccountRechargeBill3Vo> rechargeBill3VoList =
    // fcAccountRechargeBillService.getAccountRechargeBill3ListByUserId(userId, AccountStatus.BID_30,
    // AccountStatus.DATA_STATUS_1, null, CommonConstant.TASK_REWARD_CASH, beginDate, endDate);
    // total = defaultTotal;
    // map.put("list",defaultList);
    // }else{
    // /** 报单奖励(预充值) **/
    // List<FcAccountRechargeBill3Vo> prechargeBill3VoList =
    // fcAccountRechargeBillService.getAccountPrechargeBill3ListByUserId(userId, AccountStatus.BID_30,
    // AccountStatus.DATA_STATUS_1, null, CommonConstant.TASK_REWARD_CASH, beginDate, endDate);
    // total = defaultTotal;
    // map.put("list",defaultList);
    // }
    // map.put("total", total);
    // return ResponseUtil.ok(map);
    // }

    @GetMapping("/myWallet")
    public Object myWallet(@LoginUser String userId, @RequestParam(defaultValue = "1") Integer page,
                    @RequestParam(defaultValue = "10") Integer limit) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        FcAccount fcAccount = accountManager.getAccountByKey(userId);
        if (fcAccount == null) {
            return ResponseUtil.badArgumentValue("账户不存在！");
        }
        // 可提现(代理账户金额)
        BigDecimal withdrawableMoney = fcAccount.getAgentAmount();

        // 代理端所有订单指定的业务形态
        List<String> allOrder = AccountStatus.BID_LIST_AGENT;
        // 提现中
        BigDecimal waitAuditMoney = fcAccountWithdrawBillService.selectWithdrawBillAmountByCondition(userId, false,
                        null, AccountStatus.WITHDRAW_STATUS_WAIT_AUDIT, null, null); // 待审核
        BigDecimal waitPayMoney = fcAccountWithdrawBillService.selectWithdrawBillAmountByCondition(userId, false, null,
                        AccountStatus.WITHDRAW_STATUS_WAIT_PAY, null, null); // 审核完成，待提现

        if (null == waitAuditMoney) {
            waitAuditMoney = BigDecimal.ZERO;
        }

        if (null == waitPayMoney) {
            waitPayMoney = BigDecimal.ZERO;
        }


        BigDecimal withdrawalMoney = waitAuditMoney.add(waitPayMoney);
        // 已提现
        BigDecimal withdrawnMoney = fcAccountWithdrawBillService.selectWithdrawBillAmountByCondition(userId, false,
                        null, AccountStatus.WITHDRAW_STATUS_FINISH, null, null);
        // 已结算金额
        BigDecimal rechargeMoney = fcAccountRechargeBillService.selectRechargeAmountByCondition(userId, false, allOrder,
                        AccountStatus.DISTRIBUTION_TYPE_10, CommonConstant.TASK_REWARD_CASH, null, null);
        // 待结算金额
        BigDecimal prechargeMoney = fcAccountPrechargeBillService.selectPrechargeAmountByCondition(userId, false,
                        allOrder, AccountStatus.DISTRIBUTION_TYPE_10, CommonConstant.TASK_REWARD_CASH, null, null);

        /** 查询 存在、收益提现的 提现记录 **/
        FcAccountWithdrawBill fcAccountWithdrawBill = new FcAccountWithdrawBill();
        // fcAccountWithdrawBill.setWithdrawType("03");
        fcAccountWithdrawBill.setCustomerId(userId);
        List<FcAccountWithdrawBill> withdrawalList = fcAccountWithdrawBillService
                        .selectFcAccountWithdrawBillList(fcAccountWithdrawBill, page, limit, "create_date", "desc");
        long total = PageInfo.of(withdrawalList).getTotal(); // 总数

        Map<String, Object> map = new HashMap<>();
        // ----上
        map.put("withdrawableMoney", withdrawableMoney); // 可提现余额
        // ----中
        map.put("withdrawalMoney", withdrawalMoney); // 提现中
        map.put("withdrawnMoney", withdrawnMoney == null ? BigDecimal.ZERO : withdrawnMoney); // 已提现
        map.put("prechargeMoney", prechargeMoney == null ? BigDecimal.ZERO : prechargeMoney); // 待结算金额
        map.put("rechargeMoney", rechargeMoney == null ? BigDecimal.ZERO : rechargeMoney); // 已结算金额
        // ----下
        map.put("withdrawalList", withdrawalList); // 提现记录
        map.put("total", total);
        return ResponseUtil.ok(map);
    }

    @GetMapping("/agentInfo")
    public Object agentInfo(@LoginUser String userId) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        MallUser user = userService.findById(userId);
        if (user == null) {
            return ResponseUtil.badArgumentValue("用户不存在！");
        }
        BmdesignUserLevel bmdesignUserLevel = bmdesignUserLevelService.selectBmdesignUserLevelById(user.getLevelId());
        Map<Object, Object> result = new HashMap<Object, Object>();
        result.put("userInfo", user);
        result.put("userLevel", bmdesignUserLevel);
        return ResponseUtil.ok(result);
    }

    /**
     * 代理端微信小程序授权
     */
    @PostMapping("/authByMiniAgent")
    public Object authByMiniAgent(@LoginUser String userId, @RequestBody String body) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        String code = JacksonUtil.parseString(body, "code"); // 授权code
        String nickName = JacksonUtil.parseString(body, "nickName"); // 微信名称
        String avatarUrl = JacksonUtil.parseString(body, "avatarUrl"); // 微信头像
        String appid = JacksonUtil.parseString(body, "appid"); // 微信appid

        log.info("-------------------------获取微信用户的openId body参数----------------------" + body
                        + "-------------------------------------");
        String url = "https://api.weixin.qq.com/sns/jscode2session" + "?appid=AppId" + "&secret=AppSecret"
                        + "&js_code=CODE" + "&grant_type=authorization_code";
        String secret = "";
        List<Map<String, String>> miniapp = wxMaProperties.getMiniapp();
        for (Map<String, String> map : miniapp) {
            if (map.get("appid").equals(appid)) {
                secret = map.get("secret");
                break;
            }
        }

        // url = url.replace("AppId", wxProperties.getMiniAgentAppId())
        // .replace("AppSecret", wxProperties.getMiniAgentSecret()).replace("CODE", code);
        url = url.replace("AppId", appid).replace("AppSecret", secret).replace("CODE", code);
        // 根据地址获取请求
        HttpGet request = new HttpGet(url);// 这里发送get请求
        // 获取当前客户端对象
        HttpClient httpClient = HttpClientBuilder.create().build();
        // 通过请求对象获取响应对象
        String openid = "";
        try {
            HttpResponse response = httpClient.execute(request);
            String jsonStr = EntityUtils.toString(response.getEntity(), "utf-8");
            com.alibaba.fastjson.JSONObject jsonTexts = (com.alibaba.fastjson.JSONObject) JSON.parse(jsonStr);
            log.info("jsonTexts：" + jsonTexts);
            if (jsonTexts.get("openid") != null) {
                openid = jsonTexts.get("openid").toString();
            }
            log.info("-------------------------获取微信用户的openId openid----------------------"
                            + JSONObject.toJSONString(openid) + "-------------------------------------");
        } catch (IOException e) {
            e.printStackTrace();
            log.error("获取微信用户openIdId异常：");
        }
        // 授权用户openId
        if (!StringUtils.isEmpty(openid)) {
            MallUser user = new MallUser();
            user.setId(userId);
            user.setNickname(nickName);
            user.setAvatar(avatarUrl);
            user.setMiniProgramOpenid(openid);
            userService.updateById(user);
        }
        return ResponseUtil.ok();
    }

    /**
     * 活动微信小程序授权
     */
    @PostMapping("/authByMiniActivity")
    public Object authByMiniActivity(@RequestBody WxLoginInfo wxLoginInfo) throws Exception {
        String code = wxLoginInfo.getCode(); // 授权code

        log.info("-------------------------获取微信用户的openId body参数----------------------" + JSONUtil.toJsonStr(wxLoginInfo)
                        + "-------------------------------------");
        String url = "https://api.weixin.qq.com/sns/jscode2session" + "?appid=AppId" + "&secret=AppSecret"
                        + "&js_code=CODE" + "&grant_type=authorization_code";
        url = url.replace("AppId", wxProperties.getMiniActivityAppId())
                        .replace("AppSecret", wxProperties.getMiniActivitySecret()).replace("CODE", code);
        // 根据地址获取请求
        HttpGet request = new HttpGet(url);// 这里发送get请求
        // 获取当前客户端对象
        HttpClient httpClient = HttpClientBuilder.create().build();
        // 通过请求对象获取响应对象
        String openid = "";
        try {
            HttpResponse response = httpClient.execute(request);
            String jsonStr = EntityUtils.toString(response.getEntity(), "utf-8");
            com.alibaba.fastjson.JSONObject jsonTexts = (com.alibaba.fastjson.JSONObject) JSON.parse(jsonStr);
            log.info("jsonTexts：" + jsonTexts);
            if (jsonTexts.get("openid") != null) {
                openid = jsonTexts.get("openid").toString();
            }
            log.info("-------------------------获取微信用户的openId openid----------------------"
                            + JSONObject.toJSONString(openid) + "-------------------------------------");
        } catch (IOException e) {
            e.printStackTrace();
            log.error("获取微信用户openIdId异常：");
        }
        if (StringUtils.isEmpty(openid)) {
            return ResponseUtil.badArgumentValue("授权失败!");
        }
        List<MallUser> mallUserList = userService.queryByWeixinMiniOpenid(openid);
        MallUser mallUser = new MallUser();
        if (mallUserList.size() == 0) {
            // 若用户不存在，则注册成新用户
            mallUser.setLevelId("0");
            mallUser.setWeixinMiniOpenid(openid);
            mallUser.setSource("用户通过微信小程序购买998特权活动创建！");
            mallUser.setChannelId(MallUserEnum.CHANNEL_ID_2.getCode()); // 活动注册
            userService.add(mallUser);
            // 开户
            accountService.accountTradeCreateAccount(mallUser.getId());
        } else if (mallUserList.size() == 1) {
            mallUser = mallUserList.get(0);
        } else {
            return ResponseUtil.badArgumentValue("存在多个相同用户！");
        }
        /** 生成token返回给前端 **/
        String token = UserTokenManager.generateToken(mallUser.getId(), mallUser.getTbkRelationId(),
                        mallUser.getTbkSpecialId());
        Map<Object, Object> result = new HashMap<Object, Object>();
        result.put("token", token);
        result.put("user", mallUser);
        return ResponseUtil.ok(result);
    }



    /**
     * 代理端用户提现 {"withdrawAccountType":"","custName":"","withdrawAccount":"","amt":""}
     */
    @PostMapping("/agentWithdraw")
    @Transactional(rollbackFor = {Exception.class})
    public Object agentWithdraw(@LoginUser String userId, @RequestBody FcAccountWithdrawBill fcAccountWithdrawBill)
                    throws Exception {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        MallUser user = userService.findById(userId); // 用户信息
        FcAccount fcAccount = accountManager.getAccountByKey(userId); // 用户账户信息

        if (fcAccount == null) {
            return ResponseUtil.badArgumentValue("账户不存在！");
        }
        // 可提现金额(余额)
        BigDecimal amount = fcAccount.getAmount();

        if (StringUtils.isEmpty(fcAccountWithdrawBill.getWithdrawAccountType())) {
            return ResponseUtil.badArgumentValue("请选择提现账户类型！");
        }

        if (fcAccountWithdrawBill.getAmt() == null) {
            return ResponseUtil.badArgumentValue("请填写提现金额！");
        }
        if (fcAccountWithdrawBill.getAmt().compareTo(new BigDecimal(0.3)) < 0) {
            return ResponseUtil.badArgumentValue("提现金额最少为0.3元！");
        }
        // 验证用户打款账户的金额是否符合提现条件
        if (amount.compareTo(fcAccountWithdrawBill.getAmt()) < 0) {
            return ResponseUtil.badArgumentValue("提现错误,账户余额不足！");
        }
        if (!"3".equals(fcAccountWithdrawBill.getWithdrawAccountType())) {
            if (StringUtils.isEmpty(fcAccountWithdrawBill.getCustName())) {
                return ResponseUtil.badArgumentValue("请填写账户名！");
            }
            if (StringUtils.isEmpty(fcAccountWithdrawBill.getWithdrawAccount())) {
                return ResponseUtil.badArgumentValue("请填写提现账户！");
            }
        }

        // 判断提现账户类型
        if ("1".equals(fcAccountWithdrawBill.getWithdrawAccountType())) {
            // 银行卡提现
            fcAccountWithdrawBill.setNote("代理端银行卡提现");
        } else if ("2".equals(fcAccountWithdrawBill.getWithdrawAccountType())) {
            // 支付宝提现
            fcAccountWithdrawBill.setMobile(fcAccountWithdrawBill.getWithdrawAccount()); // 手机号
            fcAccountWithdrawBill.setNote("代理端支付宝提现");
        } else if ("3".equals(fcAccountWithdrawBill.getWithdrawAccountType())) {
            // 查询用户openId是否授权微信小程序
            if (user == null || StringUtils.isEmpty(user.getWeixinMiniOpenid())) {
                return ResponseUtil.badArgumentValue("请先授权微信小程序！");
            }
            fcAccountWithdrawBill.setWithdrawAccount(user.getWeixinMiniOpenid());
            // 微信提现
            fcAccountWithdrawBill.setNote("代理端微信提现");
        }

        /** (1)用户提现，冻结账户金额 **/
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("dType", AccountStatus.DISTRIBUTION_TYPE_00); // 间接分润
        paramsMap.put("message", "代理端用户提现");
        paramsMap.put("capitalTrend", AccountStatus.CAPITAL_TREND_02);
        paramsMap.put("changeType", AccountStatus.CHANGE_TYPE_03);
        paramsMap.put("changeAccountType", AccountStatus.CHANGE_ACCOUNT_TYPE_3);
        paramsMap.put("tradeType", AccountStatus.TRADE_TYPE_08);
        if (amount.compareTo(fcAccountWithdrawBill.getAmt()) >= 0) {
            AccountUtils.accountChange(userId, fcAccountWithdrawBill.getAmt(), CommonConstant.TASK_REWARD_CASH,
                            AccountStatus.BID_7, paramsMap);
        } else {
            return ResponseUtil.badArgumentValue("账户余额不足！");
        }

        /** (2)用户提现，增加冻结流水 **/
        FcAccountFreezeBill fcAccountFreezeBill = new FcAccountFreezeBill();
        fcAccountFreezeBill.setCapitalTrend(AccountStatus.CAPITAL_TREND_02);
        fcAccountFreezeBill.setAccountId(fcAccount.getAccountId());
        fcAccountFreezeBill.setFreezeAccountId(fcAccount.getAccountId());
        fcAccountFreezeBill.setCustomerId(userId);
        // fcAccountFreezeBill.setCustomerName("");
        fcAccountFreezeBill.setCustomerType("CUST");
        fcAccountFreezeBill.setFreezeCashAmt(fcAccountWithdrawBill.getAmt());
        fcAccountFreezeBill.setChangeType(AccountStatus.CHANGE_TYPE_03);
        fcAccountFreezeBill.setNote("代理端用户提现");
        fcAccountFreezeService.insertFcAccountFreezeBill(fcAccountFreezeBill);

        /** (3)增加用户提现记录 **/
        fcAccountWithdrawBill.setFreezeAcBillId(fcAccountFreezeBill.getFreezeAcBillId()); // 冻结流水id
        fcAccountWithdrawBill.setAccountId(fcAccount.getAccountId());
        fcAccountWithdrawBill.setCustomerId(userId);
        fcAccountWithdrawBill.setWithdrawType("01");// 01 --收益（零钱）提现 02 --任务奖励提现 03 --代理端提现
        fcAccountWithdrawBill.setCreateDate(LocalDateTime.now());
        fcAccountWithdrawBill.setUpdateDate(LocalDateTime.now());
        fcAccountWithdrawBill.setWithdrawDate(LocalDateTime.now());
        fcAccountWithdrawBill.setCheckStatus("00C");// 00C 未对账 01C 已对账
        fcAccountWithdrawBill.setCheckDate(LocalDateTime.now());

        fcAccountWithdrawBill.setUserName(EmojiParser.parseToAliases(user.getNickname())); // 用户昵称
        // fcAccountWithdrawBill.setCustName(fcAccountWithdrawBill.getUserName()); //（支付宝，微信）账户名称
        // fcAccountWithdrawBill.setReceiveBankCustname('');
        fcAccountWithdrawBill.setWithdrawState("WAIT_AUDIT"); // 提现状态:WAIT_AUDIT等待审核 WAIT_PAY:审核成功，等待提现 FINISH:提现完成
                                                              // REJECT：已驳回
        fcAccountWithdrawBill.setAuditFlag((short) 1);
        // fcAccountWithdrawBill.setPayOrderNum("支付宝订单流水"); //支付宝订单流水
        // fcAccountWithdrawBill.setWithdrawAccountType("02"); //提现账户类型（01 微信 02 支付宝 03 银行卡）
        fcAccountWithdrawBillService.add(fcAccountWithdrawBill);
        return ResponseUtil.ok();
    }

    /**
     * 查询默认收款账户
     */
    @GetMapping("/findBindCard")
    public Object findBindCard(@LoginUser String userId) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        FcAccount fcAccount = accountManager.getAccountByKey(userId);
        if (fcAccount == null) {
            return ResponseUtil.badArgumentValue("账户不存在！");
        }
        FcAccountBindCard fcAccountBindCard = new FcAccountBindCard();
        fcAccountBindCard.setAccountId(fcAccount.getAccountId());
        fcAccountBindCard.setIsApplyDefault(true);

        FcAccountBindCard bindCard = null;
        List<FcAccountBindCard> fcAccountBindCardList =
                        fcAccountBindCardService.selectFcAccountBindCardList(fcAccountBindCard, null, null, null, null);
        if (fcAccountBindCardList.size() > 0) {
            bindCard = fcAccountBindCardList.get(0);
        }
        return ResponseUtil.ok(bindCard);
    }

    /**
     * 查询收款账户list
     */
    @GetMapping("/findBindCardList")
    public Object findBindCardList(@LoginUser String userId, @RequestParam Short accountType) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        FcAccount fcAccount = accountManager.getAccountByKey(userId);
        if (fcAccount == null) {
            return ResponseUtil.badArgumentValue("账户不存在！");
        }
        FcAccountBindCard fcAccountBindCard = new FcAccountBindCard();
        fcAccountBindCard.setAccountId(fcAccount.getAccountId());
        fcAccountBindCard.setAccountType(accountType);
        List<FcAccountBindCard> fcAccountBindCardList =
                        fcAccountBindCardService.selectFcAccountBindCardList(fcAccountBindCard, null, null, null, null);
        return ResponseUtil.ok(fcAccountBindCardList);
    }

    /**
     * 保存收款账户_银行卡、支付宝
     */
    @PostMapping("/addBindCard")
    public Object addBindCard(@LoginUser String userId, @RequestBody FcAccountBindCard fcAccountBindCard) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        MallUser mallUser = userService.findById(userId);
        if (mallUser == null) {
            return ResponseUtil.badArgumentValue("用户不存在！");
        }
        FcAccount fcAccount = accountManager.getAccountByKey(userId);
        if (fcAccount == null) {
            return ResponseUtil.badArgumentValue("账户不存在！");
        }
        // if(StringUtils.isEmpty(fcAccountBindCard.getBankName())){
        // return ResponseUtil.badArgumentValue("请选择银行名称！");
        // }

        // fcAccountBindCard.setUserName(mallUser.getNickname());
        fcAccountBindCard.setAccountId(fcAccount.getAccountId());
        fcAccountBindCard.setBindTime(LocalDateTime.now());
        fcAccountBindCard.setLastUpdateTime(LocalDateTime.now());
        if (fcAccountBindCard.getAccountType() == 1 || "1".equals(fcAccountBindCard.getAccountType())) {
            // 绑定银行卡
            fcAccountBindCard.setNote("绑定银行卡");
        } else {
            // 绑定支付宝
            fcAccountBindCard.setNote("绑定支付宝");
        }
        fcAccountBindCard.setIsApplyDefault(false);
        fcAccountBindCardService.add(fcAccountBindCard);
        return ResponseUtil.ok(fcAccountBindCard.getBindCardId());
    }

    /**
     * 修改收款账户__银行卡、支付宝
     */
    @PostMapping("/updateBindCard")
    public Object updateBindCard(@LoginUser String userId, @RequestBody FcAccountBindCard fcAccountBindCard) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        MallUser mallUser = userService.findById(userId);
        if (mallUser == null) {
            return ResponseUtil.badArgumentValue("用户不存在！");
        }
        FcAccount fcAccount = accountManager.getAccountByKey(userId);
        if (fcAccount == null) {
            return ResponseUtil.badArgumentValue("账户不存在！");
        }
        fcAccountBindCard.setBindTime(LocalDateTime.now());
        fcAccountBindCard.setLastUpdateTime(LocalDateTime.now());
        fcAccountBindCardService.update(fcAccountBindCard);
        return ResponseUtil.ok();
    }

    /**
     * 设置默认收款账户
     */
    @PostMapping("/setBindCard")
    public Object setBindCard(@LoginUser String userId, @RequestBody FcAccountBindCard card) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        MallUser mallUser = userService.findById(userId);
        if (mallUser == null) {
            return ResponseUtil.badArgumentValue("用户不存在！");
        }
        FcAccount fcAccount = accountManager.getAccountByKey(userId);
        if (fcAccount == null) {
            return ResponseUtil.badArgumentValue("账户不存在！");
        }
        if (null == card || null == card.getBindCardId()) {
            return ResponseUtil.badArgument();
        }
        /** 修改默认绑定 旧的账户卡号 **/
        FcAccountBindCard fcAccountBindCard = new FcAccountBindCard();
        fcAccountBindCard.setIsApplyDefault(false);
        fcAccountBindCardService.updateOther(fcAccount, fcAccountBindCard);

        /** 设置默认绑定 新的账户卡号 **/
        fcAccountBindCard.setBindCardId(card.getBindCardId());
        fcAccountBindCard.setIsApplyDefault(true);
        fcAccountBindCardService.update(fcAccountBindCard);
        return ResponseUtil.ok();
    }


    /** -------------------------------代理端接口------------------------------- **/
    @RequestMapping("/miniApp")
    public Object getWxMiNi(@RequestParam String appid) {

        List<Map<String, String>> miniapp = wxMaProperties.getMiniapp();
        for (Map<String, String> map : miniapp) {
            // ifmap.get("appid")
        }
        System.out.println(miniapp);
        return ResponseUtil.ok();
    }

    /**
     * 查询用户信用分
     * @param userId
     * @return
     */
    @GetMapping("/findCreditScore")
    public Object findCreditScore(@LoginUser String userId){
        if( null == userId ){
            return ResponseUtil.unlogin();
        }
        Map map = new HashMap();
        FcAccount userAccount = accountService.getUserAccount(userId);
        if(null != userAccount){
            BigDecimal creditScore = userAccount.getCreditScore();
            map.put("creditScore",creditScore);
        }
        Map<String, String> Maprule = mallSystemConfigService.getMallSystemCommonSingle("credit_score_rule1");
        Map<String, String> Maprule2 = mallSystemConfigService.getMallSystemCommonSingle("credit_score_rule2");
        Map<String, String> Maprule3 = mallSystemConfigService.getMallSystemCommonSingle("credit_score_rule3");

        StringBuilder sb = new StringBuilder();
        if (null != Maprule && null!= Maprule2 && null!=Maprule3){
            sb.append(Maprule.get("credit_score_rule1"));
            sb.append(Maprule2.get("credit_score_rule2"));
            sb.append(Maprule3.get("credit_score_rule3"));
        }
        String rule = sb.toString();
        map.put("rule",rule);
        return ResponseUtil.ok(map);
    }
}
