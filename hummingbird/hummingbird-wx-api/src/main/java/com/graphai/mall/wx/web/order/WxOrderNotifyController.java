package com.graphai.mall.wx.web.order;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.graphai.commons.util.json.JacksonUtils;
import com.graphai.mall.wx.service.WxOrderService;
import com.graphai.open.mallorder.service.IMallOrderYLService;

@RestController
@RequestMapping("/wx/order")
@Validated
public class WxOrderNotifyController {


    @Autowired
    private WxOrderService wxOrderService;
    @Autowired
    IMallOrderYLService mallOrderYLService;



    /**
     * 微信付款成功或失败回调接口-未莱生活回调
     * <p>
     * TODO 注意，这里pay-notify是示例地址，建议开发者应该设立一个隐蔽的回调地址
     *
     * @param request 请求内容
     * @param response 响应内容
     * @return 操作结果
     */
    @PostMapping("pay-notify")
    public Object payNotify(HttpServletRequest request, HttpServletResponse response) {
        return wxOrderService.payNotify(request, response);
    }

    /**
     * 商户下单回调
     * @param request
     * @param response
     * @return
     */
    @PostMapping("pay-pubNotify")
    public Object pubNotify(HttpServletRequest request, HttpServletResponse response) {
        return wxOrderService.pubNotify(request, response);
    }

    /**
     * 汇付付款成功或失败回调接口
     * <p>
     *
     * @param request 请求内容
     * @return 操作结果
     * @throws Exception
     */
    @PostMapping("huifunotify")
    public Object huifunotify(HttpServletRequest request) throws Exception {
        return wxOrderService.huifuNotify(request);
    }


    /**
     * 微信付款成功或失败回调接口---未莱商圈回调--包含自营
     * <p>
     * TODO 注意，这里pay-notify是示例地址，建议开发者应该设立一个隐蔽的回调地址
     *
     * @param request 请求内容
     * @param response 响应内容
     * @return 操作结果
     */
    @PostMapping("pay-hsNotify")
    public Object hsnotify(HttpServletRequest request, HttpServletResponse response) {
        return wxOrderService.payHsNotify(request, response);
    }

    /**
     * 活动小程序支付回调(998活动支付)
     */
    @PostMapping("pay-activityNotify")
    public Object activityNotify(HttpServletRequest request, HttpServletResponse response) {
        return wxOrderService.payActivityNotify(request, response);
    }

    /**
     * 代理段支付回调
     */
    @PostMapping("pay-agentNotify")
    public Object agentNotify(HttpServletRequest request, HttpServletResponse response) {
        return wxOrderService.agentNotify(request, response);
    }

    /**
     * 支付宝支付授权回调
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("alipaynotify")
    public String alipayNotify(HttpServletRequest request, HttpServletResponse response) throws Exception {
        return wxOrderService.alipayNotify(request, response);
    }

    /**
     * 支付宝网页支付授权回调
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("alipayPubNotify")
    public String alipayPubNotify(HttpServletRequest request, HttpServletResponse response) throws Exception {
        return wxOrderService.alipayPubNotify(request, response);
    }

    /**
     * 福禄充值结果回调
     *
     * @param reqjsn
     * @return
     */
    @PostMapping("/pay-notify-fulu")
    public Object payNotifyByFuLu(@RequestBody String reqjsn) {
        Map<String, Object> reqmap = new HashMap<>();
        reqmap = JacksonUtils.jsn2map(reqjsn, String.class, Object.class);
        return wxOrderService.payNotifyByFuLu(reqmap);
    }

    /**
     * 中闵充值结果回调
     *
     * @param reqjsn
     * @return
     */
    @PostMapping("/pay-notify-zm")
    public Object payNotifyByZM(@RequestBody String reqjsn) {
        Map<String, Object> request = null;
        request = JacksonUtils.jsn2map(reqjsn, String.class, Object.class);
        return wxOrderService.payNotifyByZM(request);
    }

    /**
     * 蓝色兄弟充值结果回调
     *
     * @param reqjsn
     * @return
     */
    @PostMapping("/pay-notify-lxsd")
    public Object payNotifyByLSXD(@RequestBody String reqjsn) {
        Map<String, Object> request = null;
        request = JacksonUtils.jsn2map(reqjsn, String.class, Object.class);
        return wxOrderService.payNotifyByLSXD(request);
    }

    /**
     * 壹林话费快充订单充值结果回调
     *
     * @return
     */
    @PostMapping("/pay-notify-yl")
    public Object callBackMallOrderInfoByYl(@RequestBody Map<String, String> map) {
        return wxOrderService.payNotifyByYl(map);
    }


    /**
     * 话费充值结果回调
     *
     * @param request
     * @return
     */
    @GetMapping("/pay-notify-cost")
    public Object payNotifyByCost(HttpServletRequest request) {
        return wxOrderService.payNotifyByCost(request);
    }



    @PostMapping("pay-groupHsnotify")
    @ResponseBody
    public Object groupHsnotify(HttpServletRequest request, HttpServletResponse response) {
        return wxOrderService.groupHsnotify(request, response);
    }

    @PostMapping("pay-appGroupHsnotify")
    @ResponseBody
    public Object appGroupHsnotify(HttpServletRequest request, HttpServletResponse response) {
        return wxOrderService.appGroupHsnotify(request, response);
    }


    /**
     * 汇付付款成功或失败回调接口
     * <p>
     *
     * @param request 请求内容
     * @return 操作结果
     * @throws Exception
     */
    @PostMapping("grouphuifunotify")
    public Object grouphuifunotify(HttpServletRequest request) throws Exception {
        return wxOrderService.grouphuifunotify(request);
    }
}
