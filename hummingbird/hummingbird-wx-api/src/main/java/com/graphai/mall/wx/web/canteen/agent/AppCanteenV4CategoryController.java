package com.graphai.mall.wx.web.canteen.agent;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.mall.admin.domain.MallUserMerchant;
import com.graphai.mall.admin.service.IMallUserMerchantService;
import com.graphai.mall.canteen.domain.KndCanteenCategory;
import com.graphai.mall.canteen.service.IKndCanteenCategoryService;
import com.graphai.mall.wx.annotation.LoginUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

/**
 * 餐品目录Controller
 *
 * @author author
 * @date 2020-03-26
 */
@RestController
@RequestMapping("/wx/b/canteenCategory")
public class AppCanteenV4CategoryController{

    @Autowired
    private IKndCanteenCategoryService kndCanteenCategoryService;

    @Autowired
    private IMallUserMerchantService userMerchantService;

    /**
     * 查询餐品目录列表
     */
    @PostMapping("/list")
    public Object list(@LoginUser String userId, KndCanteenCategory kndCanteenCategory,
                       @RequestParam(defaultValue = "1") Integer page,@RequestParam(defaultValue = "17") Integer size) {
        //通过userId获取商户信息
        MallUserMerchant userMerchant = userMerchantService.getOne(new QueryWrapper<MallUserMerchant>()
                .lambda().eq(MallUserMerchant::getUserId, userId));
        if (Objects.isNull(userMerchant)) {
            return ResponseUtil.badArgumentValue("商户信息不存在");
        }
        kndCanteenCategory.setMerchantId(userMerchant.getMerchantId());
        return ResponseUtil.ok(kndCanteenCategoryService.selectKndCanteenCategoryList(new Page<>(page,size),kndCanteenCategory));
    }


    /**
     * 查询餐品目录列表(包含所有食品)
     */
    @PostMapping("/getCategoryFoodList")
    public Object getCategoryFoodList(@LoginUser String userId,KndCanteenCategory kndCanteenCategory,
                                      @RequestParam(defaultValue = "1") Integer page,@RequestParam(defaultValue = "17") Integer size) {
        //通过userId获取商户信息
        MallUserMerchant userMerchant = userMerchantService.getOne(new QueryWrapper<MallUserMerchant>()
                .lambda().eq(MallUserMerchant::getUserId, userId));
        if (Objects.isNull(userMerchant)) {
            return ResponseUtil.badArgumentValue("商户信息不存在");
        }
        kndCanteenCategory.setMerchantId(userMerchant.getMerchantId());


        return ResponseUtil.ok(kndCanteenCategoryService.selectKndCanteenCategoryFoodList(new Page<>(page, size),kndCanteenCategory));
    }

    /**
     * 新增保存餐品目录
     */
    @PostMapping("/add")
    public Object addSave(@LoginUser String userId,@RequestBody KndCanteenCategory kndCanteenCategory) {
        //通过userId获取商户信息
        //通过userId获取商户信息
        MallUserMerchant userMerchant = userMerchantService.getOne(new QueryWrapper<MallUserMerchant>()
                .lambda().eq(MallUserMerchant::getUserId, userId));
        if (Objects.isNull(userMerchant)) {
            return ResponseUtil.badArgumentValue("商户信息不存在");
        }
        kndCanteenCategory.setMerchantId(userMerchant.getMerchantId());
        //判断是否已经存在
        KndCanteenCategory category = new KndCanteenCategory();
        category.setMerchantId(userMerchant.getMerchantId());
        category.setCategoryName(kndCanteenCategory.getCategoryName());
        List<KndCanteenCategory> list = kndCanteenCategoryService.getKndCanteenCategoryList(category);

        if (null != list && list.size() > 0) {
            return ResponseUtil.fail("添加失败，已存在相同分类名！");
        }
        kndCanteenCategory.setMerchantId(userMerchant.getMerchantId());
        return ResponseUtil.ok(kndCanteenCategoryService.insertKndCanteenCategory(kndCanteenCategory));
    }

    /**
     * 修改餐品目录
     */
    @GetMapping("/edit/{id}")
    public Object edit(@LoginUser String userId,@PathVariable("id") String id) {
        return ResponseUtil.ok(kndCanteenCategoryService.selectKndCanteenCategoryById(id));
    }

    /**
     * 修改保存餐品目录
     */
    @PostMapping("/edit")
    public Object editSave(@LoginUser String userId,@RequestBody KndCanteenCategory kndCanteenCategory) {
        //通过userId获取商户信息
        MallUserMerchant userMerchant = userMerchantService.getOne(new QueryWrapper<MallUserMerchant>()
                .lambda().eq(MallUserMerchant::getUserId, userId));
        if (Objects.isNull(userMerchant)) {
            return ResponseUtil.badArgumentValue("商户信息不存在");
        }
        KndCanteenCategory category = new KndCanteenCategory();
        category.setCategoryName(kndCanteenCategory.getCategoryName());
        category.setMerchantId(userMerchant.getMerchantId());
        category.setStoreId(kndCanteenCategory.getStoreId());
        List<KndCanteenCategory> list = kndCanteenCategoryService.getKndCanteenCategoryList(category);
        if (null != list && list.size() > 1) {
            return ResponseUtil.fail("修改失败，已存在相同分类名！");
        }
        return kndCanteenCategoryService.updateKndCanteenCategory(kndCanteenCategory)>0 ? ResponseUtil.ok() : ResponseUtil.fail();
    }

    /**
     * 删除餐品目录
     */
    @PostMapping("/remove")
    public Object remove(String ids) {
        kndCanteenCategoryService.deleteKndCanteenCategoryByIds(ids);
        return ResponseUtil.ok();
    }


    /**
     * 通过merchantId查询 菜品分类
     */
    @PostMapping("/getCategoryList")
    public Object getCategoryList(@LoginUser String userId, String merchantId,
                                  @RequestParam(defaultValue = "1") Integer page,@RequestParam(defaultValue = "17") Integer size) {
        KndCanteenCategory kndCanteenCategory = new KndCanteenCategory();
        kndCanteenCategory.setMerchantId(merchantId);
        return kndCanteenCategoryService.selectKndCanteenCategoryList(new Page<>(page,size),kndCanteenCategory);
    }
}
