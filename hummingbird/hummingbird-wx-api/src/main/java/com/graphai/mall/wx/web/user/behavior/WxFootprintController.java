package com.graphai.mall.wx.web.user.behavior;

import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.json.JacksonUtil;
import com.graphai.mall.db.domain.MallCollect;
import com.graphai.mall.db.domain.MallFootprint;
import com.graphai.mall.db.domain.MallGoods;
import com.graphai.mall.db.domain.MallKeyword;
import com.graphai.mall.db.service.goods.MallGoodsService;
import com.graphai.mall.db.service.search.MallKeywordService;
import com.graphai.mall.db.service.user.behavior.MallFootprintService;
import com.graphai.mall.wx.annotation.LoginUser;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 用户访问足迹服务
 */
@RestController
@RequestMapping("/wx/footprint")
@Validated
public class WxFootprintController {
    private final Log logger = LogFactory.getLog(WxFootprintController.class);

    @Autowired
    private MallFootprintService footprintService;
    @Autowired
    private MallGoodsService goodsService;
    @Autowired
    private MallKeywordService mallKeywordService;
    /**
     * 删除用户足迹
     *
     * @param userId 用户ID
     * @param body   请求内容， { id: xxx }
     * @return 删除操作结果
     */
    @PostMapping("delete")
    public Object delete(@LoginUser String userId, @RequestBody String body) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        if (body == null) {
            return ResponseUtil.badArgument();
        }

        String footprintId = JacksonUtil.parseString(body, "id");
        if (footprintId == null) {
            return ResponseUtil.badArgument();
        }
        MallFootprint footprint = footprintService.findById(footprintId);

        if (footprint == null) {
            return ResponseUtil.badArgumentValue();
        }
        if (!footprint.getUserId().equals(userId)) {
            return ResponseUtil.badArgumentValue();
        }

        footprintService.deleteById(footprintId);
        return ResponseUtil.ok();
    }

    /**
     * 批量删除用户足迹
     *
     * @param userId 用户ID
     * @param body   请求内容， { id: xxx }
     * @return 删除操作结果
     */
    @PostMapping("deleteMore")
    public Object deleteMore(@LoginUser String userId, @RequestBody String body) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        if (body == null) {
            return ResponseUtil.badArgument();
        }

        List<String> footprintIds = JacksonUtil.parseStringList(body, "ids");
        if(footprintIds.size() > 0){
            for (String temp: footprintIds) {
                footprintService.deleteById(temp);
            }
        }

        Boolean isClear = JacksonUtil.parseBoolean(body, "isClear");
        if(isClear){
            footprintService.clear(userId);
        }
        return ResponseUtil.ok();
    }

    /**
     * 获取热搜词
     * @return
     */
    @GetMapping("getHotKeyWord")
    public Object getHotKeyWord(@RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer limit){
        List<MallKeyword> mallKeywords = mallKeywordService.getHotKeyWord(page,limit);
        return ResponseUtil.ok(mallKeywords);
    }

    /**
     * 用户足迹列表
     *
     * @param page 分页页数
     * @param limit 分页大小
     * @return 用户足迹列表
     */
    @GetMapping("list")
    public Object list(@LoginUser String userId,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }

        List<MallFootprint> footprintList = footprintService.queryByAddTime(userId, page, limit);
        long count = PageInfo.of(footprintList).getTotal();
        int totalPages = (int) Math.ceil((double) count / limit);

        List<Object> footprintVoList = new ArrayList<>(footprintList.size());
        for (MallFootprint footprint : footprintList) {
            Map<String, Object> c = new HashMap<String, Object>();
            c.put("id", footprint.getId());
            c.put("goodsId", footprint.getGoodsId());
            c.put("addTime", footprint.getAddTime());

            MallGoods goods = goodsService.findById(footprint.getGoodsId());
            
            if(goods!=null){
            	 c.put("name", goods.getName());
                 c.put("brief", goods.getBrief());
                 c.put("picUrl", goods.getPicUrl());
                 c.put("retailPrice", goods.getRetailPrice());
                 footprintVoList.add(c);
            }
        }


        Map<String, Object> result = new HashMap<>();
        result.put("footprintList", footprintVoList);
        result.put("totalPages", totalPages);
        return ResponseUtil.ok(result);
    }

/*    *//**
     * 房淘B端的用户足迹列表
     *
     * @param page 分页页数
     * @param limit 分页大小
     * @return 用户足迹列表
     *//*
    @GetMapping("/fanTao/list")
    public Object fanTaoList(@RequestParam String userId,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }

        List<MallFootprintVo2> footprintList = footprintService.queryByBrowse(userId, page, limit);
        long count = PageInfo.of(footprintList).getTotal();
        int totalPages = (int) Math.ceil((double) count / limit);

*//*        List<Object> footprintVoList = new ArrayList<>(footprintList.size());
        for (MallFootprint footprint : footprintList) {
            Map<String, Object> c = new HashMap<String, Object>();
            c.put("id", footprint.getId());
            c.put("goodsId", footprint.getGoodsId());
            c.put("addTime", footprint.getAddTime());

            MallGoods goods = goodsService.findById(footprint.getGoodsId());

            if(goods!=null){
            	 c.put("name", goods.getName());
                 c.put("brief", goods.getBrief());
                 c.put("picUrl", goods.getPicUrl());
                 c.put("retailPrice", goods.getRetailPrice());
                 footprintVoList.add(c);
            }
        }*//*


        Map<String, Object> result = new HashMap<>();
        result.put("footprintList", footprintList);
        result.put("totalPages", totalPages);
        return ResponseUtil.ok(result);
    }*/

    /**
     * 用户足迹添加
     * <p>
     *
     *
     * @param userId 用户ID
     * @param body 请求内容，{ goodsId: xxx ,merchantId xxx}
     * @return 操作结果
     */
    @PostMapping("add")
    public Object add(@LoginUser String userId, @RequestBody String body) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        //String userId = "5199692295660396544";

        String goodsId = JacksonUtil.parseString(body, "goodsId");
        String merchantId = JacksonUtil.parseString(body, "merchantId");
        if (!ObjectUtils.allNotNull(goodsId, merchantId)) {
            return ResponseUtil.badArgument();
}

        MallFootprint footprint = new MallFootprint();
        footprint.setUserId(userId);
        footprint.setGoodsId(goodsId);
        footprint.setMerchantId(merchantId);
        footprintService.add(footprint);

        return ResponseUtil.ok();
    }

}
