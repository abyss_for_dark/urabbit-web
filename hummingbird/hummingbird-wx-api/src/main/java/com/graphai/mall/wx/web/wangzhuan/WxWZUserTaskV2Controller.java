package com.graphai.mall.wx.web.wangzhuan;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.lang.StringUtils;
import com.graphai.mall.admin.domain.MallTag;
import com.graphai.mall.admin.domain.MallUserProfitNew;
import com.graphai.mall.admin.service.IMallTagMallUserService;
import com.graphai.mall.db.domain.FcAccountRechargeBill;
import com.graphai.mall.db.domain.MallOrder;
import com.graphai.mall.db.domain.MallUser;
import com.graphai.mall.db.domain.WangzhuanUserTask;
import com.graphai.mall.db.constant.account.AccountStatus;
import com.graphai.mall.db.service.account.FcAccountRechargeBillService;
import com.graphai.mall.db.service.order.MallOrderService;
import com.graphai.mall.db.service.sysconfig.MallSystemConfigService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.service.wangzhuan.WangzhuanTaskService;
import com.graphai.mall.db.service.wangzhuan.WangzhuanTaskStepService;
import com.graphai.mall.db.service.wangzhuan.WangzhuanUserTaskService;
import com.graphai.mall.db.service.wangzhuan.WxWangzhuanTaskRedisService;
import com.graphai.mall.db.constant.CommonConstant;
import com.graphai.mall.db.util.account.AccountUtils;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.open.utils.lock.MutexLock;
import com.graphai.open.utils.lock.MutexLockUtils;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;

/**
 * (网赚)领取个人任务记录Controller-V2
 *
 * @author Qxian
 * @date 2021-03-22
 */
@RestController
@RequestMapping("/wx/userTask/v2")
@Validated
public class WxWZUserTaskV2Controller {

    private final Log logger = LogFactory.getLog(WxWZUserTaskV2Controller.class);

    @Resource
    private WangzhuanTaskService wangzhuanTaskService;
    @Resource
    private WxWangzhuanTaskRedisService wxWangzhuanTaskRedisService;
    @Autowired
    private WangzhuanUserTaskService wangzhuanUserTaskService;
    @Resource
    private WangzhuanTaskStepService wangzhuanTaskStepService;
//    @Autowired
//    private MallUserProfitNewMapper mallUserProfitNewMapper;
    @Autowired
    private MallUserService mallUserService;
    @Autowired
    private FcAccountRechargeBillService fcAccountRechargeBillService;
    @Autowired
    private MallOrderService mallOrderService;
    @Autowired
    private IMallTagMallUserService mallTagMallUserService;
    @Autowired
    private MallSystemConfigService mallSystemConfigService;

    private Object lock2 = new Object();
    /**
     * 获取 我的工作台 - 各个title的count
     *
     * @param userId
     *
     */
    @GetMapping("/task/count")
    public Object getTaskCount(@LoginUser String userId){
        if (null == userId) {
            return ResponseUtil.unlogin();
        }
        return wangzhuanTaskService.getTaskCount(userId);
    }

    /**
     *  获取   完成任务的弹幕列表。
     * @param redisKey
     * @return
     */
    @GetMapping("/getTaskBarrageList")
    public Object getTaskRankingBarrage(@Param("redisKey") String redisKey){
        return wxWangzhuanTaskRedisService.getTaskRankingBarrage(redisKey);
    }



    /**
     * 获取任务详情 - 顶部部分的数据。
     * @param userId
     * @param id
     * @return
     */
    @GetMapping("/getTaskDetailsTop")
    public Object getTaskDetailsTop(@LoginUser String userId,@Param("id") String id){
        if (null == userId) {
            return ResponseUtil.unlogin();
        }
        return wangzhuanTaskService.getTaskDetailsTop(userId,id);

    }

    /**
     *  获取任务详情 - 任务步骤。
     * @param id
     * @return
     */
    @GetMapping("/getTaskStepDetails")
    public Object getTaskStepDetails(@Param("id")String id){
        return wangzhuanTaskStepService.getTaskStepDetails(id);
    }

    /**
     *  根据用户Id去查找任务列表
     *  任务管理
     * @param page
     * @param limit
     * @param userId
     * @param tStatus 状态( ST100-未上架(审核中)、ST101-已上架、ST102-已下架、ST103-已驳回)
     * @return
     */
    @GetMapping("/getTaskManageList")
    public Object getTaskManageList(@RequestParam(defaultValue = "0") Integer page, @RequestParam(defaultValue = "10") Integer limit,
                                    @LoginUser String userId ,@Param("tStatus") String tStatus){
        if (null == userId) {
            return ResponseUtil.unlogin();
        }
        return wangzhuanTaskService.selectTaskManageList(userId,(page-1)*limit,limit,tStatus);
    }

    /**
     *  根据用户Id和任务是否为审核中的状态去查找任务列表
     *  订单审核
     * @param page
     * @param limit
     * @param userId
     * @param tStatus 状态(UST100 进行中、UST101 已取消、UST102 审核中(完成)、UST103 审核不通过、UST104 审核通过)
     * @return
     */
    @GetMapping("/getOrderExamineList")
    public Object getOrderExamineList(@RequestParam(defaultValue = "0") Integer page, @RequestParam(defaultValue = "10") Integer limit,
                                      @LoginUser String userId ,@Param("tStatus") String tStatus){
        if (null == userId) {
            return ResponseUtil.unlogin();
        }
        return  wangzhuanTaskService.selectOrderExamineList(userId,(page-1)*limit,limit,tStatus);
    }

    /**
     *  根据用户ID和任务状态去查找，我接了的任务
     *  我的订单
     * @param page
     * @param limit
     * @param userId
     * @param tStatus 状态(UST100 进行中（待提交）、UST101 已取消（）、UST102 审核中(完成)、UST103 审核不通过（已驳回）、UST104 审核通过)
     * @return
     */
    @GetMapping("/getMyOrderList")
    public Object getMyOrderList(@RequestParam(defaultValue = "0") Integer page, @RequestParam(defaultValue = "10") Integer limit,
                                 @LoginUser String userId ,@Param("tStatus") String tStatus){
        if (null == userId) {
            return ResponseUtil.unlogin();
        }
        return wangzhuanTaskService.selectMyOrderList(userId,(page-1)*limit,limit,tStatus);
    }

    /**
     *  接单审核- 详情，  根据 任务id以及 领取者id去查找数据
     * @param taskId,userId
     * @return
     */
    @GetMapping("/getOrderExamineByIdAndUserId")
    public Object getOrderExamineByIdAndUserId(@Param("taskId")String taskId,@Param("drawUserId")String drawUserId){
        return wangzhuanTaskService.selectOrderExamineByIdAndUserId(taskId,drawUserId);
    }

    /**
     * 新增保存(网赚)-领取任务
     */
    @PostMapping("/receiveTask")
    public Object receiveTask(@LoginUser String userId, @RequestBody Map map) {
        try {
            if (StringUtils.isBlank(userId)) {
                return ResponseUtil.unlogin();
            }
            String taskId = (String) map.get("taskId");
            WangzhuanUserTask wangzhuanUserTask = new WangzhuanUserTask();
            wangzhuanUserTask.setTaskId(taskId);
            Object error = validate(wangzhuanUserTask);
            if (error != null) {
                return error;
            }
            MutexLock mutexLock = MutexLockUtils.getMutexLock(taskId);
            synchronized (mutexLock) {
                try {
                    // 处理业务
                    Object obj = wangzhuanUserTaskService.receiveTask(userId, wangzhuanUserTask, taskId);
                    MutexLockUtils.remove(taskId);
                    return obj;
                }catch(Exception e){
                    MutexLockUtils.remove(taskId);
                    logger.info("领取任务出现异常"+e.getMessage() , e);
                    return ResponseUtil.badArgument();
                }
            }
        } catch (Exception ex) {
            logger.info("领取任务出现异常"+ex.getMessage() , ex);
            return ResponseUtil.badArgument();
        }
    }

    /**
     * 校验数据(添加)
     */
    private Object validate(WangzhuanUserTask wangzhuanUserTask) {
        if (StringUtils.isBlank(wangzhuanUserTask.getTaskId())) {
            return ResponseUtil.badArgument();
        }
        return null;
    }


    /*  任务管理-任务下架操作。
     * @param taskId
     * @return
     */
    @GetMapping("/taskRemoveFormMyTask")
    public Object taskRemoveFormMyTask(@Param("taskId")String taskId,@LoginUser String userId){
        if (null == userId) {
            return ResponseUtil.unlogin();
        }
        return wangzhuanTaskService.taskRemoveFormMyTask(taskId);
    }

    /**
     *  任务管理 -  任务取消上架申请。
     * @param taskId
     * @return
     */
    @GetMapping("/taskCancelFormMyTask")
    public Object taskCancelFormMyTask(@Param("taskId")String taskId,@LoginUser String userId){
        if (null == userId) {
            return ResponseUtil.unlogin();
        }
        return wangzhuanTaskService.taskCancelFormMyTask(taskId);
    }

    /**
     * 订单审核 - 快速审核， userTaskId  有多个，以  '，' 做间隔
     * @param userTaskId wangzhuan_user_task 的主键id
     * @return
     */
    @GetMapping("/easyExaminedFormMyOrder")
    public Object easyExaminedFormMyOrder(@Param("userTaskId")String userTaskId,@LoginUser String userId){
        if (null == userId) {
            return ResponseUtil.unlogin();
        }
        Object obj= wangzhuanUserTaskService.easyExaminedFormMyOrder(userTaskId);
        return obj;
    }

    /**
     *  接单审核 - 全部审核
     *  逻辑： 根据当前登录的用户Id,获取它全部的待审核订单。
     * @param userId 注意，这个userId,相当于从表的  publish_user_id
     * @return
     */
    @GetMapping("/examinedFormMyAllOrder")
    public Object examinedFormMyAllOrder(@LoginUser String userId){
        if (null == userId) {
            return ResponseUtil.unlogin();
        }

       return wangzhuanUserTaskService.examinedFormMyAllOrder(userId);
    }

    /**
     *  接单审核 - 单个审核 可能会提交相关数据的。
     * @param userTaskId wangzhuan_user_task 的主键Id
     * @param tStatus 状态：UST103：审核不通过
     *                     UST104: 审核通过
     * @param remarks 审核通过或不通过的原因
     * @return
     */
    @GetMapping("/examinedFormMyOrderByOne")
    public Object examinedFormMyOrderByOne(@Param("userTaskId")String userTaskId,@Param("tStatus")String tStatus,@Param("remarks")String remarks,@LoginUser String userId){
        logger.info("userTaskIdtStatus"+userTaskId+"tStatus:"+tStatus+"remarks:"+remarks+"userId:"+userId);
        if (null == userId) {
            return ResponseUtil.unlogin();
        }
       return wangzhuanUserTaskService.examinedFormMyOrderByOne(userTaskId,tStatus,remarks);
    }

    /**
     *  我的订单 - 去完成
     * @RequestBody json数据
     * @Param userId
     * @return
     */
    @PostMapping("/goToFinishMyDrawOrder")
    public Object goToFinishMyDrawOrder(@RequestBody String data,@LoginUser String userId){
        if (null == userId) {
            return ResponseUtil.unlogin();
        }
        JSONObject jsonObject = JSONUtil.parseObj(data);
        String taskId=jsonObject.getStr("taskId");
        String screenshotImages=jsonObject.getStr("screenshotImages");
        return wangzhuanUserTaskService.goToFinishMyDrawOrder(taskId,screenshotImages,userId);
    }
    /**
     *  逻辑
     *  1. 这个任务，我没有领取过，也不是我发布的：  status：TAUR101
     *  2. 这个任务，是我发布的 ： status：TAUR105
     *  3. 这个任务是我领取的，但未完成 ： status: TAUR100
     *  4. 这个任务是我领取的，但已经完成了： status：TAUR102
     *  5. 这个任务是我领取的，已经完成了，且审核通过了 ： status: TAUR103
     *  6. 这个任务是我领取的，已经完成了，且审核未通过 ： status: TAUR104
     */
    @GetMapping("/taskAndUserRelationship")
    public Object taskAndUserRelationship(@Param("taskId")String taskId,@LoginUser String userId){
        if (null == userId) {
            return ResponseUtil.unlogin();
        }
        return wangzhuanTaskService.taskAndUserRelationship(taskId,userId);
    }

    /**
     *  通过 taskId 以及 drawUserId 获取数据。
     * @param taskId
     * @param drawUserId
     * @return
     */
    @GetMapping("/getTaskAndUserData")
    public Object getTaskAndUserData(@Param("taskId")String taskId,@Param("drawUserId")String drawUserId,@LoginUser String userId){
        if (null == userId) {
            return ResponseUtil.unlogin();
        }
        return wangzhuanUserTaskService.getTaskAndUserData(taskId,drawUserId);
    }

    @GetMapping("/getCountByOrderAtAll")
    public Object getCountByOrderAtAll(@LoginUser String userId){
        return wangzhuanUserTaskService.getCountByOrderAtAll(userId);
    }

    /**
     *   领取者，取消任务。
     * @param taskId
     * @param userId
     * @return
     */
    @GetMapping("/cancelTaskByDrawUserId")
    public Object cancelTaskByDrawUserId(@Param("taskId")String taskId,@LoginUser String userId){
        if (null == userId) {
            return ResponseUtil.unlogin();
        }
        MutexLock mutexLock = MutexLockUtils.getMutexLock(taskId);
        synchronized (mutexLock) {
            Object obj = null;
            try {
                obj = wangzhuanUserTaskService.cancelTaskByDrawUserId(taskId, userId);
                MutexLockUtils.remove(taskId);
            }catch (Exception e){
                MutexLockUtils.remove(taskId);
            }
            return obj;
        }
    }

    @GetMapping("/getRateByType")
    public Object getRateByType(@Param("type")String type){
        List<MallUserProfitNew> list = wangzhuanTaskService.getRateByType(type);
        Map<String,Object> map = new HashMap<>();
        if(CollectionUtils.isNotEmpty(list)){
           map.put("item", list.get(0));
        }else{
            map.put("item",list);
        }
        return ResponseUtil.ok(map);
    }



    /**
     * V2 新增 爱好/生日/性别任务
     * 查询新人任务(0:未完成 1:已完成)
     */
    @GetMapping("/newTask")
    public Object newTask(@LoginUser String userId) throws Exception {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }

        String isNew = "1";
        String isTaobao = "0";
        String isFirstOrderByFans = "0";
        String isFirstOrder = "0";
        String isWx = "0";
        String isIOS = "0";
        String isPe = "0";

        String isBirthday= "0";
        String isHobby= "0";
        String isGender= "0";

        MallUser mallUser = mallUserService.findById(userId);
        if (mallUser == null) {
            return ResponseUtil.badArgumentValue("用户不存在！");
        }
        // 淘宝授权
        if (!org.springframework.util.StringUtils.isEmpty(mallUser.getTbkRelationId())) {
            isTaobao = "1";
            String str = "淘宝授权";
            List<FcAccountRechargeBill> list = fcAccountRechargeBillService.listFcAccountBillListUserId(userId, str);
            if (list.size() == 0) {
                Map<String, Object> paramsMap1 = new HashMap();
                paramsMap1.put("dType", AccountStatus.DISTRIBUTION_TYPE_00); // 直接分润
                paramsMap1.put("message", "淘宝授权奖励");
                paramsMap1.put("capitalTrend", AccountStatus.CAPITAL_TREND_01);
                paramsMap1.put("changeType", AccountStatus.CHANGE_TYPE_01);
                paramsMap1.put("changeAccountType", AccountStatus.CHANGE_ACCOUNT_TYPE_2);
                AccountUtils.accountChange(userId, new BigDecimal(300), CommonConstant.TASK_REWARD_GOLD,
                        AccountStatus.BID_6, paramsMap1);
            }

        } else {
            String str = "淘宝授权";
            List<FcAccountRechargeBill> list = fcAccountRechargeBillService.listFcAccountBillListUserId(userId, str);
            if (list.size() > 0) {
                isTaobao = "1";
            }
        }
        // IOS授权
        if (!org.springframework.util.StringUtils.isEmpty(mallUser.getIosUserId())) {
            isIOS = "1";
            String str = "Apple授权";
            List<FcAccountRechargeBill> list = fcAccountRechargeBillService.listFcAccountBillListUserId(userId, str);
            if (list.size() == 0) {
                Map<String, Object> paramsMap1 = new HashMap();
                paramsMap1.put("dType", AccountStatus.DISTRIBUTION_TYPE_00); // 直接分润
                paramsMap1.put("message", "Apple授权奖励");
                paramsMap1.put("capitalTrend", AccountStatus.CAPITAL_TREND_01);
                paramsMap1.put("changeType", AccountStatus.CHANGE_TYPE_01);
                paramsMap1.put("changeAccountType", AccountStatus.CHANGE_ACCOUNT_TYPE_2);
                AccountUtils.accountChange(userId, new BigDecimal(200), CommonConstant.TASK_REWARD_GOLD,
                        AccountStatus.BID_6, paramsMap1);
            }

        }

        // 绑定微信
        if (!org.springframework.util.StringUtils.isEmpty(mallUser.getWeixinOpenid())) {
            isWx = "1";
            String str = "绑定微信";
            List<FcAccountRechargeBill> list = fcAccountRechargeBillService.listFcAccountBillListUserId(userId, str);
            if (list.size() < 1) {
                Map<String, Object> paramsMap1 = new HashMap();
                paramsMap1.put("dType", AccountStatus.DISTRIBUTION_TYPE_00); // 直接分润
                paramsMap1.put("message", "绑定微信奖励");
                paramsMap1.put("capitalTrend", AccountStatus.CAPITAL_TREND_01);
                paramsMap1.put("changeType", AccountStatus.CHANGE_TYPE_01);
                paramsMap1.put("changeAccountType", AccountStatus.CHANGE_ACCOUNT_TYPE_2);
                AccountUtils.accountChange(userId, new BigDecimal(200), CommonConstant.TASK_REWARD_GOLD,
                        AccountStatus.BID_6, paramsMap1);
            }
        } else {
            String str = "绑定微信";
            List<FcAccountRechargeBill> list = fcAccountRechargeBillService.listFcAccountBillListUserId(userId, str);
            if (list.size() > 0) {
                isWx = "1";
            }
        }
        // 粉丝首单
        List<String> userIds = mallUserService.getFansIdByUserId(userId);
        if (userIds.size() > 0) {
            List<MallOrder> fanMallOrderList = mallOrderService.findByUserIds(userIds);
            if (fanMallOrderList.size() > 0) {
                isFirstOrderByFans = "1";
                String str = "粉丝首单";
                List<FcAccountRechargeBill> list =
                        fcAccountRechargeBillService.listFcAccountBillListUserId(userId, str);
                if (list.size() < 1) {

                    Map<String, Object> paramsMap1 = new HashMap();
                    paramsMap1.put("dType", AccountStatus.DISTRIBUTION_TYPE_00); // 直接分润
                    paramsMap1.put("message", "粉丝首单奖励");
                    paramsMap1.put("capitalTrend", AccountStatus.CAPITAL_TREND_01);
                    paramsMap1.put("changeType", AccountStatus.CHANGE_TYPE_01);
                    paramsMap1.put("changeAccountType", AccountStatus.CHANGE_ACCOUNT_TYPE_2);
                    AccountUtils.accountChange(userId, new BigDecimal(300), CommonConstant.TASK_REWARD_GOLD,
                            AccountStatus.BID_6, paramsMap1);
                }
            }
        }
        // 首次出单
        MallOrder mallOrder = new MallOrder();
        mallOrder.setUserId(userId);
        List<MallOrder> mallOrderList = mallOrderService.findByCondition(mallOrder);
        if (mallOrderList.size() > 0) {
            isFirstOrder = "1";
            String strName = "首次出单奖励";
            List<FcAccountRechargeBill> list =
                    fcAccountRechargeBillService.listFcAccountBillListUserId(userId, strName);
            if (list.size() < 1) {
                Map<String, Object> paramsMap1 = new HashMap();
                paramsMap1.put("dType", AccountStatus.DISTRIBUTION_TYPE_00); // 直接分润
                paramsMap1.put("message", "首次出单奖励");
                paramsMap1.put("capitalTrend", AccountStatus.CAPITAL_TREND_01);
                paramsMap1.put("changeType", AccountStatus.CHANGE_TYPE_01);
                paramsMap1.put("changeAccountType", AccountStatus.CHANGE_ACCOUNT_TYPE_2);
                AccountUtils.accountChange(userId, new BigDecimal(500), CommonConstant.TASK_REWARD_GOLD,
                        AccountStatus.BID_6, paramsMap1);
            }
        }

        // 绑定地理位置
        if (!org.springframework.util.StringUtils.isEmpty(mallUser.getProvince()) && !org.springframework.util.StringUtils.isEmpty(mallUser.getCity())
                && !org.springframework.util.StringUtils.isEmpty(mallUser.getCounty())) {
            isPe = "1";
            String str = "绑定地理位置";
            List<FcAccountRechargeBill> list = fcAccountRechargeBillService.listFcAccountBillListUserId(userId, str);
            synchronized (lock2) {
                if (list.size() == 0) {
                    Map<String, Object> paramsMap1 = new HashMap();
                    paramsMap1.put("dType", AccountStatus.DISTRIBUTION_TYPE_00); // 直接分润
                    paramsMap1.put("message", "绑定地理位置奖励");
                    paramsMap1.put("capitalTrend", AccountStatus.CAPITAL_TREND_01);
                    paramsMap1.put("changeType", AccountStatus.CHANGE_TYPE_01);
                    paramsMap1.put("changeAccountType", AccountStatus.CHANGE_ACCOUNT_TYPE_2);
                    AccountUtils.accountChange(userId, new BigDecimal(100), CommonConstant.TASK_REWARD_GOLD,
                            AccountStatus.BID_6, paramsMap1);
                }
            }
        }


        // 新人注册
        String strNew = "新人注册奖励";
        List<FcAccountRechargeBill> list1 = fcAccountRechargeBillService.listFcAccountBillListUserId(userId, strNew);
        if (list1.size() < 1) {
            Map<String, Object> paramsMap1 = new HashMap();
            paramsMap1.put("dType", AccountStatus.DISTRIBUTION_TYPE_00); // 直接分润
            paramsMap1.put("message", "新人注册奖励");
            paramsMap1.put("capitalTrend", AccountStatus.CAPITAL_TREND_01);
            paramsMap1.put("changeType", AccountStatus.CHANGE_TYPE_01);
            paramsMap1.put("changeAccountType", AccountStatus.CHANGE_ACCOUNT_TYPE_2);
            AccountUtils.accountChange(userId, new BigDecimal(200), CommonConstant.TASK_REWARD_GOLD,
                    AccountStatus.BID_6, paramsMap1);
        }

        //生日信息
        MallUser user = mallUserService.findById(userId);
        if (user.getBirthday()!=null){
            isBirthday="1";
        }
        //性别信息
        if (user.getGender()!=null || user.getGender() != "0"){
            isGender="1";
        }
        //爱好信息
        List<MallTag> mallTags = mallTagMallUserService.listUserTag(userId);
        if (mallTags!=null && mallTags.size()>0){
            isHobby="1";
        }


        // 新人注册
        Map<String, Object> isNewMap = new HashMap<>();
        isNewMap.put("icon", "https://gxfacepay.oss-cn-shenzhen.aliyuncs.com/hummingbird/level/1.png");
        isNewMap.put("name", "新人注册");
        isNewMap.put("gold", "200");
        isNewMap.put("desc", "在APP内完成绑定注册");
        isNewMap.put("status", isNew);

        // 淘宝授权
        Map<String, Object> isTaobaoMap = new HashMap<>();
        isTaobaoMap.put("icon", "https://gxfacepay.oss-cn-shenzhen.aliyuncs.com/hummingbird/level/2.png");
        isTaobaoMap.put("name", "淘宝授权");
        isTaobaoMap.put("gold", "300");
        isTaobaoMap.put("desc", "成功完成淘宝授权");
        isTaobaoMap.put("status", isTaobao);
        // 粉丝首单
        Map<String, Object> isFirstOrderByFansMap = new HashMap<>();
        isFirstOrderByFansMap.put("icon", "https://gxfacepay.oss-cn-shenzhen.aliyuncs.com/hummingbird/level/3.png");
        isFirstOrderByFansMap.put("name", "粉丝首单");
        isFirstOrderByFansMap.put("gold", "100");
        isFirstOrderByFansMap.put("desc", "完成查询粉丝首次出单");
        isFirstOrderByFansMap.put("status", isFirstOrderByFans);
        // 首次出单
        Map<String, Object> isFirstOrderMap = new HashMap<>();
        isFirstOrderMap.put("icon", "https://gxfacepay.oss-cn-shenzhen.aliyuncs.com/hummingbird/level/4.png");
        isFirstOrderMap.put("name", "首次出单");
        isFirstOrderMap.put("gold", "500");
        isFirstOrderMap.put("desc", "完成首次出单");
        isFirstOrderMap.put("status", isFirstOrder);

        // //绑定IOS
        // Map<String,Object> isIOSMap = new HashMap<>();
        // isIOSMap.put("icon","http://gxfacepay.oss-cn-shenzhen.aliyuncs.com/hummingbird/mipmap/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20200703163554.png");
        // isIOSMap.put("name","Apple授权");
        // isIOSMap.put("gold","200");
        // isIOSMap.put("desc","完成Apple授权登录");
        // isIOSMap.put("status",isIOS);

        // 绑定微信
        Map<String, Object> isWxMap = new HashMap<>();
        isWxMap.put("icon", "https://gxfacepay.oss-cn-shenzhen.aliyuncs.com/hummingbird/level/5.png");
        isWxMap.put("name", "绑定微信");
        isWxMap.put("gold", "200");
        isWxMap.put("desc", "完成微信号填写");
        isWxMap.put("status", isWx);

        // 地理位置授权
        Map<String, Object> isPlaceEmpower = new HashMap<>();
        isPlaceEmpower.put("icon",
                "http://gxfacepay.oss-cn-shenzhen.aliyuncs.com/hummingbird/mipmap/%E4%BD%8D%E7%BD%AE_20200709155627.png");
        isPlaceEmpower.put("name", "地理位置授权");
        isPlaceEmpower.put("gold", "100");
        isPlaceEmpower.put("desc", "完成地理位置授权");
        isPlaceEmpower.put("status", isPe);// 地理位置授权

        Map<String, String> creditScoreLimitMap = mallSystemConfigService.getMallSystemCommonSingle("new_credit_score");
        String newScore = creditScoreLimitMap.get("new_credit_score");
        //设置生日信息
        Map<String, Object> isBirthdayMap = new HashMap<>();
        isBirthdayMap.put("icon",
                "https://gxfacepay.oss-cn-shenzhen.aliyuncs.com/hummingbird/level/%E5%87%BA%E7%94%9F%E5%B9%B4%E6%9C%88.png");
        isBirthdayMap.put("name", "用户生日设置");
        isBirthdayMap.put("score", newScore);
        isBirthdayMap.put("desc", "完成用户生日设置");
        isBirthdayMap.put("status", isBirthday);

        //设置爱好信息
        Map<String, Object> isHobbyMap = new HashMap<>();
        isHobbyMap.put("icon",
                "https://gxfacepay.oss-cn-shenzhen.aliyuncs.com/hummingbird/level/%E5%BF%83.png");
        isHobbyMap.put("name", "用户爱好选择");
        isHobbyMap.put("score", newScore);
        isHobbyMap.put("desc", "完成用户爱好选择");
        isHobbyMap.put("status", isHobby);

        //设置性别信息
        Map<String, Object> isGenderMap = new HashMap<>();
        isGenderMap.put("icon",
                "https://gxfacepay.oss-cn-shenzhen.aliyuncs.com/hummingbird/level/%E6%80%A7%E5%88%AB.png");
        isGenderMap.put("name", "用户性别设置");
        isGenderMap.put("score", newScore);
        isGenderMap.put("desc", "完成用户性别设置");
        isGenderMap.put("status", isGender);



        List<Map<String, Object>> list = new ArrayList<>();

        list.add(isNewMap);
        list.add(isTaobaoMap);
        list.add(isFirstOrderByFansMap);
        list.add(isFirstOrderMap);
        // list.add(isIOSMap);
        list.add(isWxMap);
        list.add(isPlaceEmpower);
        list.add(isBirthdayMap);
        list.add(isHobbyMap);
        list.add(isGenderMap);

        return ResponseUtil.ok(list);
    }
}
