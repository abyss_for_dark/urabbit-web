package com.graphai.mall.wx.web.wangzhuan;

import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.ip.IpUtils;
import com.graphai.commons.util.lang.StringUtils;
import com.graphai.commons.util.obj.ObjectUtils;
import com.graphai.id.GraphaiIdGenerator;
import com.graphai.mall.db.bo.ChangeAboutTaskBo;
import com.graphai.mall.db.constant.wangzhuan.ComplainEnum;
import com.graphai.mall.db.constant.wangzhuan.WangzhuanTaskEnum;
import com.graphai.mall.db.domain.MallComplain;
import com.graphai.mall.db.domain.WangzhuanTask;
import com.graphai.mall.db.domain.WangzhuanTaskType;
import com.graphai.mall.db.service.complain.IMallComplainService;
import com.graphai.mall.db.service.wangzhuan.WangZhuangTaskComplainService;
import com.graphai.mall.db.service.wangzhuan.WangzhuanTaskService;
import com.graphai.mall.db.service.wangzhuan.WangzhuanTaskServiceV2;
import com.graphai.mall.db.vo.wangzhuan.WangzhuanTaskCreateDetailVo;
import com.graphai.mall.db.vo.wangzhuan.WangzhuanTaskCreateDetailVoV2;
import com.graphai.mall.db.vo.wangzhuan.WzTaskSetBiddingVo;
import com.graphai.mall.db.vo.wangzhuan.WzTaskSetTopOrRecommendVo;
import com.graphai.mall.wx.annotation.LoginUser;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/wx/task/v2")
@Validated
public class WxWangzhuanTaskControllerV2 {
    private final Log logger = LogFactory.getLog(WxWangzhuanTaskControllerV2.class);

    @Resource
    private WangZhuangTaskComplainService wangZhuangTaskComplainService;

    @Resource
    private WangzhuanTaskService wangzhuanTaskService;

    @Resource
    private WangzhuanTaskServiceV2 wangzhuanTaskServiceV2;

    @Autowired
    private IMallComplainService mallComplainService;


    /**
     * 获取 任务类目
     *
     * @param taskType 类目类型
     * @return
     */
    @GetMapping("/taskCategory")
    public Object taskCategory(@Param("taskType") String taskType) {
        List<WangzhuanTaskType> list = wangzhuanTaskService.getTaskCategory(taskType);
        Map<String, Object> map = new HashMap<>();
        map.put("items", list);
        return ResponseUtil.ok(map);
    }
    /**
     * 发布任务
     *
     * @param userId 发布者id
     * @param data   发布参数
     * @return Object
     */
    @PostMapping("/createTask")
    public Object addSave(@LoginUser String userId, @RequestBody String data, HttpServletRequest request) {
        try {
            // 参数验证
            Object error = wangzhuanTaskService.validatereateTaskV2(data);
            if (error != null) {
                return error;
            }
            JSONObject jsonObject = JSONUtil.parseObj(data);
            WangzhuanTask wangzhuanTask = new WangzhuanTask();
            wangzhuanTask.setAmount(jsonObject.getBigDecimal("amount"));//任务单价
            wangzhuanTask.setTaskType(jsonObject.getStr("taskType"));//投放类型
            wangzhuanTask.setRemarks(jsonObject.getStr("remark"));
            wangzhuanTask.setTstatus(WangzhuanTaskEnum.TSTATUS_ST105.getCode()); //任务状态
            wangzhuanTask.setTaskName(jsonObject.getStr("taskName"));//任务名称
            wangzhuanTask.setSubName(jsonObject.getStr("subName"));//副标题
            wangzhuanTask.setDeleted(false);//是否禁用
            wangzhuanTask.setTaskCategory(jsonObject.getStr("taskCategory"));//任务类型
            String imgUrl = jsonObject.getStr("imageUrl");
            wangzhuanTask.setImageUrl(imgUrl);
            wangzhuanTask.setUnit(WangzhuanTaskEnum.UNIT_US102.getCode());//完成时间单位
            wangzhuanTask.setDuration(jsonObject.getStr("duration"));//任务完成时长
            wangzhuanTask.setAuditDuration(jsonObject.getStr("auditDuration")); // 审核时长，单位：小时
            wangzhuanTask.setStockNumber(Integer.parseInt(jsonObject.get("stockNumber").toString()));//任务数量-这个是会减少的。
            wangzhuanTask.setSumStockNumber(Integer.parseInt(jsonObject.get("stockNumber").toString()));//任务数量-这个是一直不变化的。
            String id = GraphaiIdGenerator.nextId("WangzhuanTask");
            wangzhuanTask.setId(id);//id
            wangzhuanTask.setUserId(userId); // 发布者用户ID

            // ----  V.2.0 ----- 才有的下列字段 -beigin
            wangzhuanTask.setProductKey(jsonObject.getStr("productKey")); // 产品Key
            wangzhuanTask.setProductName(jsonObject.getStr("productName")); //产品 Name

            LocalDateTime dateTime = jsonObject.get("cycleDate", LocalDateTime.class);
            wangzhuanTask.setCycleDate(dateTime); //  任务周期结束时间
            if (StringUtils.isNotBlank(jsonObject.getStr("sex"))) {
                wangzhuanTask.setSex(jsonObject.getStr("sex")); // 性别限制
            }
            if (StringUtils.isNotBlank(jsonObject.getStr("beginAge"))) {
                wangzhuanTask.setBeginAge(jsonObject.getStr("beginAge")); // 限制年龄开始
            }
            if (StringUtils.isNotBlank(jsonObject.getStr("endAge"))) {
                wangzhuanTask.setEndAge(jsonObject.getStr("endAge")); // 限制年龄结束
            }

            // ----  V.2.0 ----- 才有的下列字段 -end

            // 省市区
            JSONArray regin = new JSONArray(jsonObject.getStr("region"));
            if (CollectionUtils.isNotEmpty(regin)) {
                String provinceCode = "";
                String cityCode = "";
                String areaCode = "";
                String province = "";
                String city = "";
                String county = "";
                for (int i = 0; regin.size() > i; i++) {
                    JSONObject regionObject = JSONUtil.parseObj(regin.get(i));
                    if (ObjectUtils.isNotNull(regionObject.get("provinceCode"))) {
                        provinceCode = provinceCode.concat(StringUtils.isNotBlank(provinceCode) ? "," + regionObject.get("provinceCode").toString() : regionObject.get("provinceCode").toString());
                    }
                    if (ObjectUtils.isNotNull(regionObject.get("cityCode"))) {
                        cityCode = cityCode.concat(StringUtils.isNotBlank(cityCode) ? "," + regionObject.get("cityCode").toString() : regionObject.get("cityCode").toString());
                    }
                    if (ObjectUtils.isNotNull(regionObject.get("province"))) {
                        province = province.concat(StringUtils.isNotBlank(province) ? "," + regionObject.get("province").toString() : regionObject.get("province").toString());
                    }
                    if (ObjectUtils.isNotNull(regionObject.get("city"))) {
                        city = city.concat(StringUtils.isNotBlank(city) ? "," + regionObject.get("city").toString() : regionObject.get("city").toString());
                    }
                    if (ObjectUtils.isNotNull(regionObject.get("areaCode"))) {
                        areaCode = areaCode.concat(StringUtils.isNotBlank(areaCode) ? "," + regionObject.get("areaCode").toString() : regionObject.get("areaCode").toString());
                    }
                    if (ObjectUtils.isNotNull(regionObject.get("county"))) {
                        county = county.concat(StringUtils.isNotBlank(county) ? "," + regionObject.get("county").toString() : regionObject.get("county").toString());
                    }
                }
                wangzhuanTask.setProvinceCode(provinceCode);
                wangzhuanTask.setCityCode(cityCode);
                wangzhuanTask.setAreaCode(areaCode);
                wangzhuanTask.setProvince(province);
                wangzhuanTask.setCity(city);
                wangzhuanTask.setCounty(county);
            }
            BigDecimal amount = jsonObject.getBigDecimal("amount"); // 任务单价
            String stockNumber = jsonObject.getStr("stockNumber");  // 任务数量
            wangzhuanTask = wangzhuanTaskService.chooseAmountV2(wangzhuanTask, amount, stockNumber);
            // 业务处理
            return wangzhuanTaskService.addSave(wangzhuanTask, jsonObject, IpUtils.getIpAddress(request));

        } catch (Exception ex) {
            logger.info("发布任务出现异常" + ex.getMessage(), ex);
            return ResponseUtil.badArgument();
        }
    }

    /**
     * 获取 同款发布 的详情
     *
     * @param taskId
     * @return
     */
    @GetMapping("/getCreateTask")
    public Object getCreateTask(@Param("taskId") String taskId) {
        return wangzhuanTaskService.getCreateTask(taskId);
    }

    /**
     * 修改相关资料
     *
     * @param userId
     * @param changeAboutTaskBo
     * @return
     */
    @PostMapping("/changeAboutTaskById")
    public Object changeAboutTaskById(@LoginUser String userId, @RequestBody ChangeAboutTaskBo changeAboutTaskBo, HttpServletRequest request) {
        if (StringUtils.isBlank(userId)) {
            return ResponseUtil.unlogin();
        }
        if (StringUtils.isBlank(changeAboutTaskBo.getType())) {
            return ResponseUtil.fail("无法识别操作！");
        }
        return wangzhuanTaskService.changeAboutTaskById(userId, changeAboutTaskBo, IpUtils.getIpAddress(request));
    }

    /**
     * 修改 任务的 全部
     *
     * @param userId
     * @param request
     * @return

    @PostMapping("/changeAllAboutTaskById")
    public Object changeAllAboutTaskById(@LoginUser String userId, @RequestBody WangzhuanTaskCreateDetailVoV2 wangzhuanTaskCreateDetailVo, HttpServletRequest request) {
        if (StringUtils.isBlank(userId)) {
            return ResponseUtil.unlogin();
        }
        // 参数验证
        Object error = wangzhuanTaskService.validatereateTaskV2(wangzhuanTaskCreateDetailVo);
        if (error != null) {
            return error;
        }
        return wangzhuanTaskService.changeAllAboutTaskById(userId, wangzhuanTaskCreateDetailVo, IpUtils.getIpAddress(request));
    }*/

    @PostMapping("/changeAllAboutTaskById")
    public Object changeAllAboutTaskById(@LoginUser String userId, @RequestBody String data, HttpServletRequest request){
        // 参数验证
        Object error = wangzhuanTaskService.validatereateTaskV2(data);
        if (error != null) {
            return error;
        }
        JSONObject jsonObject = JSONUtil.parseObj(data);
        WangzhuanTask wangzhuanTask = new WangzhuanTask();
        wangzhuanTask.setAmount(jsonObject.getBigDecimal("amount"));//任务单价
        wangzhuanTask.setTaskType(jsonObject.getStr("taskType"));//投放类型
        wangzhuanTask.setRemarks(jsonObject.getStr("remark"));
        wangzhuanTask.setTstatus(WangzhuanTaskEnum.TSTATUS_ST105.getCode()); //任务状态
        wangzhuanTask.setTaskName(jsonObject.getStr("taskName"));//任务名称
        wangzhuanTask.setSubName(jsonObject.getStr("subName"));//副标题
        wangzhuanTask.setDeleted(false);//是否禁用
        wangzhuanTask.setTaskCategory(jsonObject.getStr("taskCategory"));//任务类型
        String imgUrl = jsonObject.getStr("imageUrl");
        wangzhuanTask.setImageUrl(imgUrl);
        wangzhuanTask.setUnit(WangzhuanTaskEnum.UNIT_US102.getCode());//完成时间单位
        wangzhuanTask.setDuration(jsonObject.getStr("duration"));//任务完成时长
        wangzhuanTask.setAuditDuration(jsonObject.getStr("auditDuration")); // 审核时长，单位：小时
        wangzhuanTask.setStockNumber(Integer.parseInt(jsonObject.get("stockNumber").toString()));//任务数量-这个是会减少的。
        wangzhuanTask.setSumStockNumber(Integer.parseInt(jsonObject.get("stockNumber").toString()));//任务数量-这个是一直不变化的。
        wangzhuanTask.setUserId(userId); // 发布者用户ID
        if (ObjectUtils.isNotNull(jsonObject.get("id"))){
            wangzhuanTask.setId(jsonObject.get("id").toString());
        }
        // ----  V.2.0 ----- 才有的下列字段 -beigin
        wangzhuanTask.setProductKey(jsonObject.getStr("productKey")); // 产品Key
        wangzhuanTask.setProductName(jsonObject.getStr("productName")); //产品 Name

        LocalDateTime dateTime = jsonObject.get("cycleDate", LocalDateTime.class);
        wangzhuanTask.setCycleDate(dateTime); //  任务周期结束时间
        if (StringUtils.isNotBlank(jsonObject.getStr("sex"))) {
            wangzhuanTask.setSex(jsonObject.getStr("sex")); // 性别限制
        }
        if (StringUtils.isNotBlank(jsonObject.getStr("beginAge"))) {
            wangzhuanTask.setBeginAge(jsonObject.getStr("beginAge")); // 限制年龄开始
        }
        if (StringUtils.isNotBlank(jsonObject.getStr("endAge"))) {
            wangzhuanTask.setEndAge(jsonObject.getStr("endAge")); // 限制年龄结束
        }

        // ----  V.2.0 ----- 才有的下列字段 -end

        // 省市区
        JSONArray regin = new JSONArray(jsonObject.getStr("region"));
        if (CollectionUtils.isNotEmpty(regin)) {
            String provinceCode = "";
            String cityCode = "";
            String areaCode = "";
            String province = "";
            String city = "";
            String county = "";
            for (int i = 0; regin.size() > i; i++) {
                JSONObject regionObject = JSONUtil.parseObj(regin.get(i));
                if (ObjectUtils.isNotNull(regionObject.get("provinceCode"))) {
                    provinceCode = provinceCode.concat(StringUtils.isNotBlank(provinceCode) ? "," + regionObject.get("provinceCode").toString() : regionObject.get("provinceCode").toString());
                }
                if (ObjectUtils.isNotNull(regionObject.get("cityCode"))) {
                    cityCode = cityCode.concat(StringUtils.isNotBlank(cityCode) ? "," + regionObject.get("cityCode").toString() : regionObject.get("cityCode").toString());
                }
                if (ObjectUtils.isNotNull(regionObject.get("province"))) {
                    province = province.concat(StringUtils.isNotBlank(province) ? "," + regionObject.get("province").toString() : regionObject.get("province").toString());
                }
                if (ObjectUtils.isNotNull(regionObject.get("city"))) {
                    city = city.concat(StringUtils.isNotBlank(city) ? "," + regionObject.get("city").toString() : regionObject.get("city").toString());
                }
                if (ObjectUtils.isNotNull(regionObject.get("areaCode"))) {
                    areaCode = areaCode.concat(StringUtils.isNotBlank(areaCode) ? "," + regionObject.get("areaCode").toString() : regionObject.get("areaCode").toString());
                }
                if (ObjectUtils.isNotNull(regionObject.get("county"))) {
                    county = county.concat(StringUtils.isNotBlank(county) ? "," + regionObject.get("county").toString() : regionObject.get("county").toString());
                }
            }
            wangzhuanTask.setProvinceCode(provinceCode);
            wangzhuanTask.setCityCode(cityCode);
            wangzhuanTask.setAreaCode(areaCode);
            wangzhuanTask.setProvince(province);
            wangzhuanTask.setCity(city);
            wangzhuanTask.setCounty(county);
        }
        BigDecimal amount = jsonObject.getBigDecimal("amount"); // 任务单价
        String stockNumber = jsonObject.getStr("stockNumber");  // 任务数量
        wangzhuanTask = wangzhuanTaskService.chooseAmountV2(wangzhuanTask, amount, stockNumber);

        return wangzhuanTaskService.changeAllAboutTaskById(userId, wangzhuanTask,jsonObject, IpUtils.getIpAddress(request));
    }

    /**
     * 用户申述接口
     *
     * @param userId
     * @param data
     * @return
     */
    @PostMapping("/complaint")
    public Object complaintUser(@LoginUser String userId, @RequestBody String data) {
        try {
            // 参数验证
            Object error = wangZhuangTaskComplainService.validatereateComlpain(data);
            if (error != null) {
                return error;
            }
            JSONObject jsonObject = JSONUtil.parseObj(data);

            MallComplain mallComplain = new MallComplain();

            String id = GraphaiIdGenerator.nextId("MallComplain");
            mallComplain.setId(id);
            //投诉所属任务ID
            mallComplain.setCategoryId(jsonObject.getStr("categoryId"));
            //申诉类型
            Integer complainIntType = jsonObject.getInt("complainType");
            String complainStrType = jsonObject.getStr("complainType");
            if (complainIntType!=null){
                mallComplain.setComplainType(complainIntType.toString());
            }
            if(complainStrType!=null){
                mallComplain.setComplainType(complainStrType);
            }
            //申诉原因
            mallComplain.setComplainText(jsonObject.getStr("complainText"));
            //申诉图片url
            mallComplain.setComplainImgs(jsonObject.getStr("complainImgs"));
            WangzhuanTask wangzhuangTask = wangzhuanTaskService.getWangzhuangTask(jsonObject.getStr("categoryId"));
            if (wangzhuangTask == null) {
                return ResponseUtil.fail("任务不存在,请联系管理员.");
            }
            //任务名
            mallComplain.setTitle(wangzhuangTask.getTaskName());
            //副标题
            mallComplain.setSubTitle(wangzhuangTask.getSubName());
            //被投诉人
            mallComplain.setComplainUser(wangzhuangTask.getUserId());
            mallComplain.setComplainNumber(id);
            mallComplain.setComplainCreateUser(userId);
            mallComplain.setCreateTime(LocalDateTime.now());
            //设置申述初始处理状态为待处理
            mallComplain.setComplainStatus(ComplainEnum.PROCESSED_2.getCode());
            //设置投诉类目为任务
            mallComplain.setCategory(ComplainEnum.CATEGORY_1.getCode());
            //设置处罚类型为信用分处罚
            mallComplain.setComplainPunishType(ComplainEnum.PUNISH_TYPE_1.getCode());
            //业务处理
            wangZhuangTaskComplainService.complaintUser(mallComplain);
            return ResponseUtil.ok();
        } catch (Exception ex) {
            logger.info("进行申述出现异常" + ex.getMessage(), ex);
            return ResponseUtil.badArgument();
        }
    }

    /**
     * 获取当前用户的申诉信息列表
     */
    @GetMapping("/getComplainList")
    public Object getComplainList(@LoginUser String userId) {
        if (StringUtils.isBlank(userId)) {
            return ResponseUtil.unlogin();
        }
        return mallComplainService.queryMallComplain(userId);
    }

    /**
     * 获取当前用户的申诉信息详情
     */
    @GetMapping("/getComplain")
    public Object getComplain(@RequestParam String complainId) {
        if (StringUtils.isEmpty(complainId)) {
            return ResponseUtil.fail();
        }
        MallComplain complain = mallComplainService.getComplain(complainId);
        return ResponseUtil.ok(complain);
    }

    /**
     * 立刻退款
     * @param userId
     * @param taskId
     * @return
     */
    @GetMapping("/rightNowBackMoney")
    public Object rightNowBackMoney(@LoginUser String userId,String taskId){
        if (StringUtils.isBlank(userId)) {
            return ResponseUtil.unlogin();
        }
        return wangzhuanTaskService.rightNowBackMoney(userId,taskId);
    }

    /**
     * 购买 置顶或者推荐的接口
     * @param userId 用户id
     * @param wzTaskSetTopOrRecommendVo 对象，具体里面有啥，点进去看啊，懒鬼
     * @param request
     * @return
     */
    @PostMapping("/setTopOrRecommend")
    public Object setTopOrRecommend(@LoginUser String userId,@RequestBody WzTaskSetTopOrRecommendVo wzTaskSetTopOrRecommendVo,HttpServletRequest request){
        if(StringUtils.isBlank(userId)){
            return ResponseUtil.unlogin();
        }
        return wangzhuanTaskServiceV2.setTopOrRecommend(userId,wzTaskSetTopOrRecommendVo, IpUtils.getIpAddress(request));
    }

    /**
     * 参与竞价， 出价或者加价
     * @param userId 用户id
     * @param wzTaskSetBiddingVo 对象，具体里面有啥，点进去看
     * @param request
     * @return
     */
    @PostMapping("/setBiddingCreateOrUpdate")
    public Object setBiddingCreateOrUpdate(@LoginUser String userId,@RequestBody WzTaskSetBiddingVo wzTaskSetBiddingVo,HttpServletRequest request){
        if(StringUtils.isBlank(userId)){
            return ResponseUtil.unlogin();
        }
        return wangzhuanTaskServiceV2.setBiddingCreateOrUpdate(userId,wzTaskSetBiddingVo,IpUtils.getIpAddress(request));
    }

    @GetMapping("/getDiddingTask")
    public Object getDiddingTask(){
        return wangzhuanTaskServiceV2.getDiddingTask();
    }

    @GetMapping("/getTaskAndUserDataV2")
    public Object getTaskAndUserDataV2(@Param("userTaskId")String userTaskId){
        return wangzhuanTaskServiceV2.getTaskAndUserDataV2(userTaskId);
    }

    @GetMapping("/getYesterdayBiddingAmount")
    public Object getYesterdayBiddingAmount(@Param("taskId") String taskId){
        return wangzhuanTaskServiceV2.getYesterdayBiddingAmount(taskId);
    }

}
