package com.graphai.mall.wx.web.test.account;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.json.JacksonUtil;
import com.graphai.mall.db.util.account.fangtao.AccountFangtaoCommonUtils;
import com.graphai.mall.wx.util.JsonUtil;
import com.graphai.mall.wx.web.common.MyBaseController;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * 账户测试类
 *
 * @author Qxian
 * @date 2021-08-06
 */
@RestController
@RequestMapping("/wx/test/account")
@Validated
public class TestAccountController extends MyBaseController {

    private final Log log = LogFactory.getLog(TestAccountController.class);


    /**
     * 预充值-分佣测试-测试后记得注释掉方法【因为账户比较敏感！！！】
     * body {"amount":"充值金额","userId":"用户id","orderId":"订单Id"}
     * @return
     */
    @PostMapping("precharge")
    public Object setRedRanking(@RequestBody String body) {

        String type = JacksonUtil.parseString(body, "type");
        String orderType = JacksonUtil.parseString(body, "orderType");
        String orderId = JacksonUtil.parseString(body, "orderId");
        Map<String, Object> reqMap = new HashMap<String, Object>();
        reqMap.put("type", type);
        reqMap.put("orderType", orderType);
        reqMap.put("orderId", orderId);

        log.info("预充值请求参数：" +reqMap);
        Map<String, Object> resultMap = AccountFangtaoCommonUtils.preChargeFangtaoStart(reqMap);
        log.info("预充值返回结果：" + JsonUtil.objectToJsonStr(resultMap));
        return ResponseUtil.ok(resultMap);
    }

    /**
     * 部分退款补分润-测试后记得注释掉方法【因为账户比较敏感！！！】
     * body {"amount":"充值金额","userId":"用户id","orderId":"订单Id"}
     * @return
     */
//    @PostMapping("refundRecharge")
//    public Object refundRecharge(@RequestBody String body) {
//
//        String amount = JacksonUtil.parseString(body, "amount");
//        String userId = JacksonUtil.parseString(body, "userId");
//        String orderId = JacksonUtil.parseString(body, "orderId");
//        String type = JacksonUtil.parseString(body, "type");
//        Map<String, String> reqMap = new HashMap<String, String>();
//        reqMap.put("amount", amount);
//        reqMap.put("userId", userId);
//        reqMap.put("orderId", orderId);
//        reqMap.put("type", type);
//
//        log.info("部分退款补分润请求参数：" +reqMap );
//        Map<String, String> resultMap = AccountFangtaoCommonUtils.refundPreChargeCommonStart(reqMap);
//        log.info("部分退款补分润返回结果" + resultMap);
//
//        return ResponseUtil.ok(resultMap);
//
//    }


}
