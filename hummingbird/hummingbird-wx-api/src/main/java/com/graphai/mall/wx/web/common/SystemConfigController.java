package com.graphai.mall.wx.web.common;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;

import com.graphai.mall.db.domain.MallUser;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.wx.annotation.LoginUser;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.github.pagehelper.PageHelper;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.json.JacksonUtil;
import com.graphai.commons.util.lang.StringHelper;
import com.graphai.mall.db.dao.MallAppVersionMapper;
import com.graphai.mall.db.domain.MallAppVersion;
import com.graphai.mall.db.domain.MallAppVersionExample;
import com.graphai.mall.db.domain.MallSystem;
import com.graphai.mall.db.service.common.AppVersionService;
import com.graphai.mall.db.service.sysconfig.MallSystemConfigService;
import com.graphai.mall.db.util.RetrialUtils;
import com.graphai.mall.wx.service.UserTokenManager;

/**
 * 系统配置
 */
@RestController
@RequestMapping("/wx/sysconfig")
@Validated
public class SystemConfigController {

    private final Log logger = LogFactory.getLog(SystemConfigController.class);

    @Autowired
    private MallSystemConfigService mallSystemConfigService;

    @Autowired
    private AppVersionService appVersionService;

    @Autowired
    private MallAppVersionMapper appVersionMapper;

    @Autowired
    private MallUserService mallUserService;


    /**
     * 获取过审状态 return true 过审 false 不过审
     */
    @GetMapping("/getExamineStatus")
    public Object getExamineStatus( @LoginUser String userId, HttpServletRequest request,
                    @RequestParam(value = "version", required = false) String version) {

        if (!StringUtils.isEmpty(userId)) {
            mallUserService.updateLastLoginTime(userId);
        }


        Map<String, Object> map = new HashMap<String, Object>();
        String mobileTye = request.getHeader("User-Agent");

        String iosPayType = null;
        MallSystem mallSystem = new MallSystem();
        String type = "android";
        // mallSystem.setKeyName("mall_app_ad%");
        List<MallSystem> appAd = mallSystemConfigService.getSystemList(mallSystem);// 查询app广告

        if (CollectionUtils.isNotEmpty(appAd)) {
            for (MallSystem mallSystem2 : appAd) {
                if (Objects.equals(mallSystem2.getKeyName(), "mall_app_ad")) {
                    map.put("appAd", mallSystem2.getKeyValue());
                }
                if (Objects.equals(mallSystem2.getKeyName(), "mall_app_pay_type")) {
                    map.put("payType", mallSystem2.getKeyValue());
                }
                if (Objects.equals(mallSystem2.getKeyName(), "mall_ios_pay_type")) {
                    iosPayType = mallSystem2.getKeyValue();
                }
            }

        }


        if (StringHelper.isNotNullAndEmpty(mobileTye)) {
            if (mobileTye.toUpperCase().contains("mac".toUpperCase())
                            || mobileTye.toUpperCase().contains("ios".toUpperCase())
                            || mobileTye.toUpperCase().contains("iphone".toUpperCase())
                            || mobileTye.toUpperCase().contains("ipad".toUpperCase())) {
                map.put("payType", iosPayType);
                type = "ios";
            }
        }

        MallAppVersion appVersion = appVersionService.queryAppVersionListByType(type);
        if (!Objects.equals(appVersion, null)) {
            map.put("downloadLink", appVersion.getUpdateUrl());
        }

        try {

            if (RetrialUtils.examine(request, version)) {
                // 生成JWT的tobken---------------
                String token = UserTokenManager.generateToken("336830247180644352", "", "");
                if (null != token) {
                    map.put("token", token);
                }

                MallAppVersionExample example = new MallAppVersionExample();
                example.createCriteria().andTypeEqualTo("ios");
                example.setOrderByClause("create_time DESC");
                PageHelper.startPage(1, 1);
                List<MallAppVersion> list = appVersionMapper.selectByExample(example);
                if (CollectionUtils.isNotEmpty(list)) {
                    map.put("version", list.get(0).getNumber());
                }
                map.put("examine", true);
            } else {
                map.put("examine", false);
            }

            return ResponseUtil.ok(map);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return ResponseUtil.fail();
    }

    /**
     * 查询卡金额
     */
    @GetMapping("/cardPrice")
    public Object cardPrice() {
        Map<String, String> cardPriceMap = mallSystemConfigService.listCard();// 查询购卡价格
        return ResponseUtil.ok(cardPriceMap);
    }

    /**
     * 查询卡金额
     */
    @GetMapping("/cardList")
    public Object cardList() {
        MallSystem mallSystem = new MallSystem();
        mallSystem.setKeyName("Mall_card_%");
        List<MallSystem> cardList = mallSystemConfigService.getSystemList(mallSystem);// 查询购卡价格
        return ResponseUtil.ok(cardList);
    }
}
