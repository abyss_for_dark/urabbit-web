package com.graphai.mall.wx.web.canteen.agent;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.mall.admin.domain.MallUserMerchant;
import com.graphai.mall.admin.service.IMallUserMerchantService;
import com.graphai.mall.canteen.domain.KndCanteenOrder;
import com.graphai.mall.canteen.domain.KndCanteenOrderFood;
import com.graphai.mall.canteen.service.IKndCanteenOrderFoodService;
import com.graphai.mall.canteen.service.IKndCanteenOrderService;
import com.graphai.mall.canteen.service.IKndCanteenRefundFoodService;
import com.graphai.mall.canteen.service.IKndCanteenRefundOrderService;
import com.graphai.mall.wx.annotation.LoginUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * 餐饮订单Controller
 *
 * @author author
 * @date 2020-03-26
 */
@Slf4j
@RestController
@RequestMapping("/wx/b/canteenOrder")
public class AppCanteenV4OrderController {

    public static final Integer ORDER_PAID = 1;    //已支付
    public static final Integer ORDER_COMPLETED = 3;    //已完成

    @Autowired
    private IKndCanteenOrderService kndCanteenOrderService;
    @Autowired
    private IKndCanteenOrderFoodService kndCanteenOrderFoodService;
    @Autowired
    private IKndCanteenRefundOrderService kndCanteenRefundOrderService;
    @Autowired
    private IKndCanteenRefundFoodService kndCanteenRefundFoodService;
//    @Autowired
//    private IKndOrderService kndOrderService;
//    @Autowired
//    private DoPayServiceUtil doPayServiceUtil;

    @Autowired
    private IMallUserMerchantService userMerchantService;
    /**
     * 查询餐饮订单列表
     */
    @PostMapping("/orderList")
    public Object orderList(@LoginUser String userId,KndCanteenOrder kndCanteenOrder) {
        //通过userId获取商户信息

        MallUserMerchant userMerchant = userMerchantService.getOne(new QueryWrapper<MallUserMerchant>()
                .lambda().eq(MallUserMerchant::getUserId, userId));
        if (Objects.isNull(userMerchant)) {
            return ResponseUtil.badArgumentValue("商户信息不存在");
        }
        kndCanteenOrder.setMerchantId(userMerchant.getMerchantId());
        List<KndCanteenOrder> list = kndCanteenOrderService.selectKndCanteenOrderListInfo(kndCanteenOrder);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                list.get(i).setReceivePrice(list.get(i).getActualPrice().subtract(list.get(i).getReturnPrice()));
            }
        }
        return ResponseUtil.ok(list);
    }

    /**
     * 完成/取消订单
     */
/*    @PostMapping("/edit")
    public Object editSave(HttpServletRequest request, HttpServletResponse response, @LoginUser String userId,
                               KndCanteenOrder kndCanteenOrder) throws AlipayApiException {
        if (ObjectUtil.isNotEmpty(kndCanteenOrder.getOrderStatus())) {
            //取消订单
            //查询该订单状态是不是 未支付状态、已支付状态
            if (kndCanteenOrder.getOrderStatus() == 2) {
                KndCanteenOrder order = kndCanteenOrderService.selectKndCanteenOrderById(kndCanteenOrder.getId());
                //如果是已支付的订单，则需要退还所有到用户账户
                if (order.getOrderStatus() == 1) {
                    KndCanteenOrderFood kndCanteenOrderFood = new KndCanteenOrderFood();
                    kndCanteenOrderFood.setOrderId(kndCanteenOrder.getId());
                    kndCanteenOrderFood.setRefundableNumber(0);
                    List<KndCanteenOrderFood> kndCanteenOrderFoodList = kndCanteenOrderFoodService.selectKndCanteenOrderFoodListInfo(kndCanteenOrderFood);
                    if (kndCanteenOrderFoodList.size() == 0) {
                        kndCanteenOrderService.updateKndCanteenOrder(kndCanteenOrder);
                        return ResponseUtil.fail("取消订单成功，但未发现可退款的菜品信息！");
                    }

                    //计算退款金额，全部退款
                    BigDecimal refundMoney = new BigDecimal(0.00);
                    for (int i = 0; i < kndCanteenOrderFoodList.size(); i++) {
                        KndCanteenOrderFood food = kndCanteenOrderFoodList.get(i);
                        int number = food.getRefundableNumber();
                        if (number > 0) {
                            refundMoney = refundMoney.add(food.getPrice().multiply(new BigDecimal(number)));
                        }
                    }

                    //todo -----将退款金额打入用户账户
                    OrderRefundForm orderRefundForm = new OrderRefundForm();
                    KndOrder kndOrder = kndOrderService.selectKndOrderById(kndCanteenOrder.getPayOrderId());
                    orderRefundForm.setOrderCode(kndOrder.getOrderCode());
                    orderRefundForm.setPrice(refundMoney);
                    AjaxResult refund = refund(request, response, userId, tokenType, orderRefundForm);
                    //退款失败
                    if (refund.getCode() != 0) {
                        return refund;
                    }
                    //退款成功返回信息
                    BoxRefundVo boxRefundVo = (BoxRefundVo) refund.get("boxRefundVo");
                    if(null == boxRefundVo){
                        log.info("-----------退款返回信息boxRefundVo为空--------" );
                        boxRefundVo = (BoxRefundVo)refund.get("data");
                    }
                    log.info("-----------退款返回信息--------" + boxRefundVo.toString());

                    //(1)添加 knd_canteen_refund_order表数据
                    KndCanteenRefundOrder kndCanteenRefundOrder = new KndCanteenRefundOrder();
                    kndCanteenRefundOrder.setId(GraphaiIdGenerator.nextId("kndCanteenRefundFood"));
                    kndCanteenRefundOrder.setMerchantId(order.getMerchantId());
                    kndCanteenRefundOrder.setOrderId(order.getId());
                    kndCanteenRefundOrder.setRUserOpenId(kndCanteenOrder.getUserOpenId());     //todo 退款人
                    kndCanteenRefundOrder.setRUserName(kndCanteenOrder.getUserName());

                    kndCanteenRefundOrder.setOUserId(userId); //todo 操作人
                    kndCanteenRefundOrder.setOrderPrice(order.getActualPrice());  //订单金额(实际支付金额)
                    kndCanteenRefundOrder.setFee(refundMoney);    //总退款金额 = 退款数量 * 售价
                    kndCanteenRefundOrder.setRemarks("取消订单，订单食品退款");     //todo 备注
                    //todo 退款之后的接口返回信息
                    kndCanteenRefundOrder.setMethod(boxRefundVo.getPay_type());    //退款方式
                    kndCanteenRefundOrder.setApiReturnCode("SUCCESS");   //退款接口返回状态码
                    kndCanteenRefundOrder.setTransactionId(boxRefundVo.getOut_refund_no());   //第三方退款交易订单ID（退款单号——knd_refund表）
                    kndCanteenRefundOrder.setPayChannel(boxRefundVo.getChannel_type());  //第三方支付通道

                    kndCanteenRefundOrderService.insertKndCanteenRefundOrder(kndCanteenRefundOrder);
                    //todo -----将退款金额打入用户账户

                    //修改 knd_canteen_order_food表数量、 添加 knd_canteen_refund_food表记录
                    for (int i = 0; i < kndCanteenOrderFoodList.size(); i++) {
                        KndCanteenOrderFood orderFood = kndCanteenOrderFoodList.get(i);
                        Integer number = orderFood.getRefundableNumber();
                        if (number > 0) {
                            Integer newNumber = orderFood.getRefundableNumber() - number;
                            KndCanteenOrderFood food = new KndCanteenOrderFood();
                            food.setId(orderFood.getId());
                            food.setRefundableNumber(newNumber);
                            //(2)修改 knd_canteen_order_food表 可退数量
                            kndCanteenOrderFoodService.updateKndCanteenOrderFood(food);

                            //(3)添加 knd_canteen_refund_food表数据
                            KndCanteenRefundFood kndCanteenRefundFood = new KndCanteenRefundFood();
                            kndCanteenRefundFood.setId(GraphaiIdGenerator.nextId("kndCanteenRefundFood"));
                            kndCanteenRefundFood.setRefundOrderId(kndCanteenRefundOrder.getId());
                            kndCanteenRefundFood.setOrderId(kndCanteenOrder.getId());
                            kndCanteenRefundFood.setFoodId(orderFood.getFoodId());
                            kndCanteenRefundFood.setFoodSn(orderFood.getFoodSn());
                            kndCanteenRefundFood.setProductId(orderFood.getProductId());
                            kndCanteenRefundFood.setNumber(number);
                            kndCanteenRefundFood.setPrice(orderFood.getPrice());
                            kndCanteenRefundFood.setSpecifications(orderFood.getSpecifications());
                            kndCanteenRefundFood.setPicUrl(orderFood.getPicUrl());
                            kndCanteenRefundFood.setMerchantId(order.getMerchantId());
                            kndCanteenRefundFoodService.insertKndCanteenRefundFood(kndCanteenRefundFood);
                        }
                    }
                }

            }

        }
        return kndCanteenOrderService.updateKndCanteenOrder(kndCanteenOrder)>0 ? ResponseUtil.ok() : ResponseUtil.fail();
    }*/

    /**
     * 订单列表统计数据 ( value = "期限类型(值：today=今天统计，week=近七天统计，month=月统计)")
     */
    @PostMapping("/orderStatistics")
    public Object orderStatistics(@LoginUser String userId,@RequestParam("param") String param) {
        MallUserMerchant userMerchant = userMerchantService.getOne(new QueryWrapper<MallUserMerchant>()
                .lambda().eq(MallUserMerchant::getUserId, userId));
        if (Objects.isNull(userMerchant)) {
            return ResponseUtil.badArgumentValue("商户信息不存在");
        }

        String merchantId = userMerchant.getMerchantId();

        Map<String, String> kndCanteenOrderStatistics = kndCanteenOrderService.getKndCanteenOrderStatistics(merchantId, param);
        BigDecimal refundMoney = kndCanteenRefundOrderService.getKndCanteenRefundOrderStatistics(merchantId, param);
        int refundFoodNum = kndCanteenRefundFoodService.getKndCanteenRefundFoodStatistics(merchantId, param);

        Map map = new HashMap();
        map.put("orderNum", kndCanteenOrderStatistics.get("orderNum"));
        map.put("totalMoney", kndCanteenOrderStatistics.get("totalMoney"));
        map.put("refundMoney", refundMoney);
        map.put("refundFoodNum", refundFoodNum);
        return ResponseUtil.ok(map);
    }

    /**
     * 详情
     */
    @GetMapping("/detail/{id}")
    public Object detail(@PathVariable("id") String id) {
        KndCanteenOrder kndCanteenOrder = kndCanteenOrderService.selectKndCanteenOrderById(id);
        //查询菜品信息
        KndCanteenOrderFood kndCanteenOrderFood = new KndCanteenOrderFood();
        kndCanteenOrderFood.setOrderId(kndCanteenOrder.getId());
        List<KndCanteenOrderFood> kndCanteenFoodOrderList = kndCanteenOrderFoodService.selectKndCanteenOrderFoodListInfo(kndCanteenOrderFood);
        if (kndCanteenFoodOrderList.size() > 0) {
            for (int i = 0; i < kndCanteenFoodOrderList.size(); i++) {
                //已支付、已完成订单
                if (kndCanteenOrder.getOrderStatus().equals(ORDER_PAID) || kndCanteenOrder.getOrderStatus().equals(ORDER_COMPLETED)) {
                    //计算菜品实收金额 (已售菜品数量 * 售价)
                    Integer num = kndCanteenFoodOrderList.get(i).getNumber() - kndCanteenFoodOrderList.get(i).getRefundableNumber();    //已退数量 = 总数 - 可退数量
                    kndCanteenFoodOrderList.get(i).setReturnedFoodNum(num);
                    //退款金额
                    kndCanteenFoodOrderList.get(i).setReturnedMoney(kndCanteenFoodOrderList.get(i).getPrice().multiply(new BigDecimal(num)));
                    //实收金额
                    kndCanteenFoodOrderList.get(i).setReceiveMoney(kndCanteenFoodOrderList.get(i).getPrice().multiply(new BigDecimal(kndCanteenFoodOrderList.get(i).getRefundableNumber())));
                } else {
                    kndCanteenFoodOrderList.get(i).setReturnedFoodNum(0);
                    //退款金额
                    kndCanteenFoodOrderList.get(i).setReturnedMoney(new BigDecimal(0.00));
                    //实收金额
                    kndCanteenFoodOrderList.get(i).setReceiveMoney(new BigDecimal(0.00));
                }
            }
        }
        Map map = new HashMap();
        map.put("kndCanteenFoodOrderList", kndCanteenFoodOrderList);
        map.put("kndCanteenOrder", kndCanteenOrder);
        return ResponseUtil.ok(map);
    }

    /**
     * 查询可退款的商品信息
     */
    @GetMapping("/refund/{id}")
    public Object refund(@PathVariable("id") String id) {
        //KndCanteenOrder kndCanteenOrder = kndCanteenOrderService.selectKndCanteenOrderById(id);
        //通过订单id，查询可退款的商品信息
        KndCanteenOrderFood kndCanteenOrderFood = new KndCanteenOrderFood();
        kndCanteenOrderFood.setOrderId(id);
        kndCanteenOrderFood.setRefundableNumber(0);
        List<KndCanteenOrderFood> kndCanteenOrderFoodList = kndCanteenOrderFoodService.selectKndCanteenOrderFoodListInfo(kndCanteenOrderFood);
        Map map = new HashMap();
        map.put("orderId", id);
        map.put("list", kndCanteenOrderFoodList);
        return ResponseUtil.ok(map);
    }

    /**
     * 退款
     */
 /*    @PostMapping("/refundFood")
   public Object refundFood(HttpServletRequest request, HttpServletResponse response, @LoginUser String userId, @Validated RefundOrderFrom refundOrderFrom) throws AlipayApiException {

        if (ObjectUtil.isEmpty(refundOrderFrom)) {
            return ResponseUtil.fail("退款失败！");
        }
        if (StrUtil.isEmpty(refundOrderFrom.getOrderId())) {
            return ResponseUtil.fail("未获取到订单号！");
        }
        if (ObjectUtil.isEmpty(refundOrderFrom.getOrderFoodList())) {
            return ResponseUtil.fail("请完善退款信息！");
        }
        //通过id查询退款订单数据
        KndCanteenOrder kndCanteenOrder = kndCanteenOrderService.selectKndCanteenOrderById(refundOrderFrom.getOrderId());

        //退款菜品信息
        List<KndCanteenOrderFood> orderFoodList = refundOrderFrom.getOrderFoodList();
        if (orderFoodList.size() == 0) {
            return ResponseUtil.fail("请选择退款商品信息！");
        }
        if (orderFoodList.size() > 0) {
            //计算退款金额
            BigDecimal refundMoney = new BigDecimal(0.00);
            for (int i = 0; i < orderFoodList.size(); i++) {
                String id = orderFoodList.get(i).getId();
                Integer number = orderFoodList.get(i).getNumber();  //退款数量
                //通过id查询 knd_canteen_order_food表数据
                KndCanteenOrderFood kndCanteenOrderFood = kndCanteenOrderFoodService.selectKndCanteenOrderFoodById(id);
                if (kndCanteenOrderFood.getRefundableNumber() < number) {
                    return ResponseUtil.fail("请输入正确的退款数量！");
                }
                refundMoney = refundMoney.add(kndCanteenOrderFood.getPrice().multiply(new BigDecimal(number)));
            }
            if (!(refundMoney.compareTo(new BigDecimal(0)) == 1)) {
                return ResponseUtil.fail("请确认正确的退款数量!");
            }
            //todo -----将退款金额打入用户账户
            OrderRefundForm orderRefundForm = new OrderRefundForm();
            KndOrder kndOrder = kndOrderService.selectKndOrderById(kndCanteenOrder.getPayOrderId());
            orderRefundForm.setOrderCode(kndOrder.getOrderCode());
            orderRefundForm.setPrice(refundMoney);
            AjaxResult refund = refund(request, response, userId, tokenType, orderRefundForm);
            log.info("-----------退款返回信息refund--------" + JSONUtil.toJsonStr(refund));
            //退款失败
            if (!"0".equals(refund.get("code").toString())) {
                return refund;
            }
            //退款成功返回信息
            BoxRefundVo boxRefundVo = (BoxRefundVo) refund.get("boxRefundVo");
            if(null == boxRefundVo){
                log.info("-----------退款返回信息boxRefundVo为空--------" );
                boxRefundVo = (BoxRefundVo)refund.get("data");
            }
            log.info("-----------退款返回信息boxRefundVo--------" + String.valueOf(boxRefundVo));

            //(1)添加 knd_canteen_refund_order表数据
            KndCanteenRefundOrder kndCanteenRefundOrder = new KndCanteenRefundOrder();
            kndCanteenRefundOrder.setId(generatorId.nextId(""));
            kndCanteenRefundOrder.setMerchantId(kndCanteenOrder.getMerchantId());
            kndCanteenRefundOrder.setOrderId(refundOrderFrom.getOrderId());
            kndCanteenRefundOrder.setRUserOpenId(kndCanteenOrder.getUserOpenId());     //todo 退款人
            kndCanteenRefundOrder.setRUserName(kndCanteenOrder.getUserName());

            kndCanteenRefundOrder.setOUserId(userId); //todo 操作人
            kndCanteenRefundOrder.setOrderPrice(kndCanteenOrder.getActualPrice());  //订单金额(实际支付金额)
            kndCanteenRefundOrder.setFee(refundMoney);    //总退款金额 = 退款数量 * 售价
            kndCanteenRefundOrder.setRemarks("订单食品退款");     //todo 备注
            //todo 退款之后的接口返回信息
            kndCanteenRefundOrder.setMethod(boxRefundVo.getPay_type());    //退款方式
            kndCanteenRefundOrder.setApiReturnCode("SUCCESS");   //退款接口返回状态码
            kndCanteenRefundOrder.setTransactionId(boxRefundVo.getOut_refund_no());   //第三方退款交易订单ID（退款单号——knd_refund表）
            kndCanteenRefundOrder.setPayChannel(boxRefundVo.getChannel_type());  //第三方支付通道
            kndCanteenRefundOrderService.insertKndCanteenRefundOrder(kndCanteenRefundOrder);


            //修改 knd_canteen_order_food表数量、 添加 knd_canteen_refund_food表记录
            for (int i = 0; i < orderFoodList.size(); i++) {
                String id = orderFoodList.get(i).getId();
                Integer number = orderFoodList.get(i).getNumber();
                if (number > 0) {
                    //通过id查询 knd_canteen_order_food表数据
                    KndCanteenOrderFood kndCanteenOrderFood = kndCanteenOrderFoodService.selectKndCanteenOrderFoodById(id);
                    if (kndCanteenOrderFood.getRefundableNumber() < number) {
                        return ResponseUtil.fail("请输入正确的退款数量！");
                    }
                    Integer newNumber = kndCanteenOrderFood.getRefundableNumber() - number;
                    KndCanteenOrderFood food = new KndCanteenOrderFood();
                    food.setId(kndCanteenOrderFood.getId());
                    food.setRefundableNumber(newNumber);
                    //(2)修改 knd_canteen_order_food表 可退数量
                    kndCanteenOrderFoodService.updateKndCanteenOrderFood(food);

                    //(3)添加 knd_canteen_refund_food表数据
                    KndCanteenRefundFood kndCanteenRefundFood = new KndCanteenRefundFood();
                    kndCanteenRefundFood.setId(GraphaiIdGenerator.nextId("kndCanteenRefundFood"));
                    kndCanteenRefundFood.setRefundOrderId(kndCanteenRefundOrder.getId());
                    kndCanteenRefundFood.setOrderId(refundOrderFrom.getOrderId());
                    kndCanteenRefundFood.setFoodId(kndCanteenOrderFood.getFoodId());
                    kndCanteenRefundFood.setFoodSn(kndCanteenOrderFood.getFoodSn());
                    kndCanteenRefundFood.setProductId(kndCanteenOrderFood.getProductId());
                    kndCanteenRefundFood.setNumber(number);
                    kndCanteenRefundFood.setPrice(kndCanteenOrderFood.getPrice());
                    kndCanteenRefundFood.setSpecifications(kndCanteenOrderFood.getSpecifications());
                    kndCanteenRefundFood.setPicUrl(kndCanteenOrderFood.getPicUrl());
                    kndCanteenRefundFood.setMerchantId(kndCanteenOrder.getMerchantId());
                    kndCanteenRefundFoodService.insertKndCanteenRefundFood(kndCanteenRefundFood);
                }
            }

        }
        return ResponseUtil.ok("退款成功！");
    }
*/

    /**
     * 退款通用方法
     */
/*    @ResponseBody
    public Object refund(HttpServletRequest request, HttpServletResponse response, @LoginUser String userId,
                             @Validated OrderRefundForm orderRefundForm) throws AlipayApiException {
        //通过userId获取商户信息
        KndMerchant kndMerchant = kndMerchantService.getKndMerchantInfo(userId, tokenType);

        KndOrder kndOrder = new KndOrder();
        BeanUtil.copyProperties(orderRefundForm, kndOrder);
        kndOrder.setOrderCode(orderRefundForm.getOrderCode());
        kndOrder.setMerchantId(kndMerchant.getId());
        kndOrder.setStoreId((String) kndMerchant.getParams().get("storeId"));
        KndOrderVo kndOrderVo = kndOrderService.selectKndOrderByMerOrOrderCode(kndOrder);
        if (kndOrderVo == null) {
            return  ResponseUtil.fail("没有权限退款该订单！");
        }
        if (kndOrderVo.getLinkType().equals(0)) {
            if (kndOrderVo.getStatus().equals(Constants.YES)) {
                String money = String.valueOf(orderRefundForm.getPrice().multiply(new BigDecimal(100)).setScale(0, BigDecimal.ROUND_DOWN).toString());
                //微信订单
                if (kndOrderVo.getPayType().equals(Constants.GALLERY_SIGN_WX)) {
                    AjaxResult ajaxResult = wxMiniPayServicesUtil.refund(orderRefundForm.getOrderCode(), money, null);
                    return ajaxResult;
                } else if (kndOrderVo.getPayType().equals(Constants.GALLERY_SIGN_ZFB)) {
                    AjaxResult ajaxResult = aliPayServicesUtil.refund(orderRefundForm.getOrderCode(), money, null);
                    return ajaxResult;
                } else if (kndOrderVo.getPayType().equals(Constants.GALLERY_SIGN_MEMBER)) {
                    return doPayServiceUtil.mbrRefund(orderRefundForm.getOrderCode(), money, null);
                } else if (kndOrderVo.getPayType().equals(Constants.GALLERY_SIGN_XJ)) {
                    return doPayServiceUtil.xjRefund(orderRefundForm.getOrderCode(), money, null);
                } else {
                    return  ResponseUtil.fail("未知支付类型，请联系管理员！");
                }
            }
        } else {
            if (kndOrderVo.getStatus().equals(Constants.YES)) {
                String money = String.valueOf(orderRefundForm.getPrice().multiply(new BigDecimal(100)).setScale(0, BigDecimal.ROUND_DOWN).toString());
                return jlPayServicesUtil.refund(orderRefundForm.getOrderCode(), money, null, IpUtils.getIpAddr(request));
            }
        }

        return ResponseUtil.fail("订单未支付/撤销");
    }*/
}
