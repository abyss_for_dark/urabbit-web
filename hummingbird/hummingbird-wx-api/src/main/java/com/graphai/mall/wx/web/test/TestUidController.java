package com.graphai.mall.wx.web.test;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.graphai.id.GraphaiIdGenerator;

@RestController
@RequestMapping("/wx/uid")
@Validated
public class TestUidController {

    @GetMapping("run")
    public String uid() {

        for (int i = 0; i < 100; i++) {
            System.out.println(GraphaiIdGenerator.nextId(""));
        }
        return "success";
    }

    public static void main(String[] args) {

        System.out.println(~(-1L << 23));

    }
}
