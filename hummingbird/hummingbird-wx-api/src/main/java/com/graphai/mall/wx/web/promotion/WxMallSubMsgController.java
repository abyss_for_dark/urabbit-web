package com.graphai.mall.wx.web.promotion;

import cn.hutool.json.JSONUtil;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.mall.db.domain.MallSubMsg;
import com.graphai.mall.db.service.promotion.MallSubMsgService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * 商城留言
 */
@RestController
@RequestMapping("/wx/mall/subMsg")
@Validated
public class WxMallSubMsgController {
    private final Log logger = LogFactory.getLog(WxMallSubMsgController.class);

    @Autowired
    private MallSubMsgService mallSubMsgService;

    @PostMapping("/create")
    public Object create(@RequestBody MallSubMsg mallSubMsg,
                         HttpServletRequest request) {
        logger.info("请求域名： " + JSONUtil.toJsonStr(request.getHeader("host")));
        mallSubMsg.setMethodUrl(request.getHeader("host"));
        mallSubMsgService.insertMallSubMsg(mallSubMsg);
        return ResponseUtil.ok();
    }


}