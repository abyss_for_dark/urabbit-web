package com.graphai.mall.wx.service;

import static com.graphai.mall.wx.util.WxResponseCode.AUTH_OPENID_UNACCESS;
import static com.graphai.mall.wx.util.WxResponseCode.ORDER_COMMENTED;
import static com.graphai.mall.wx.util.WxResponseCode.ORDER_COMMENT_EXPIRED;
import static com.graphai.mall.wx.util.WxResponseCode.ORDER_INVALID;
import static com.graphai.mall.wx.util.WxResponseCode.ORDER_INVALID_OPERATION;
import static com.graphai.mall.wx.util.WxResponseCode.ORDER_PAY_FAIL;
import static com.graphai.mall.wx.util.WxResponseCode.ORDER_UNKNOWN;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;

import com.graphai.mall.admin.service.IGameRedHourRuleService;
import com.graphai.mall.db.constant.CommonConstant;
import com.graphai.mall.db.domain.*;
import com.graphai.mall.db.util.account.AccountUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.alibaba.fastjson.JSONObject;
import com.github.binarywang.wxpay.bean.notify.WxPayNotifyResponse;
import com.github.binarywang.wxpay.bean.notify.WxPayOrderNotifyResult;
import com.github.binarywang.wxpay.bean.order.WxPayMpOrderResult;
import com.github.binarywang.wxpay.bean.request.WxPayUnifiedOrderRequest;
import com.github.binarywang.wxpay.bean.result.BaseWxPayResult;
import com.github.binarywang.wxpay.config.WxPayConfig;
import com.github.binarywang.wxpay.constant.WxPayConstants;
import com.github.binarywang.wxpay.exception.WxPayException;
import com.github.binarywang.wxpay.service.WxPayService;
import com.github.pagehelper.PageInfo;
import com.graphai.commons.constant.MallConstant;
import com.graphai.commons.util.IpUtil;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.json.JacksonUtil;
import com.graphai.commons.util.json.JacksonUtils;
import com.graphai.commons.util.sms.TecentSmsUtil;
import com.graphai.express.ExpressService;
import com.graphai.mall.core.notify.NotifyService;
import com.graphai.mall.core.qcode.QCodeService;
import com.graphai.mall.core.system.SystemConfig;
import com.graphai.mall.db.dao.FcAccountCapitalBillMapper;
import com.graphai.mall.db.dao.FcAccountMapper;
import com.graphai.mall.db.service.MallCartService;
import com.graphai.mall.db.service.WpcApiService;
import com.graphai.mall.db.service.account.AccountService;
import com.graphai.mall.db.constant.account.AccountStatus;
import com.graphai.mall.db.service.common.MallRegionService;
import com.graphai.mall.db.service.common.ShipService;
import com.graphai.mall.db.service.coupon.CouponVerifyService;
import com.graphai.mall.db.service.coupon.MallCouponService;
import com.graphai.mall.db.service.coupon.MallCouponUserService;
import com.graphai.mall.db.service.distribution.FenXiaoService;
import com.graphai.mall.db.service.goods.MallGoodsProductService;
import com.graphai.mall.db.service.goods.MallGoodsService;
import com.graphai.mall.db.service.groupon.MallGrouponRulesService;
import com.graphai.mall.db.service.groupon.MallGrouponService;
import com.graphai.mall.db.service.integral.MallGoodsIntegralService;
import com.graphai.mall.db.service.integral.PayAsUserAndMerchantService;
import com.graphai.mall.db.service.order.MallOrderGoodsService;
import com.graphai.mall.db.service.order.MallOrderNotifyLogService;
import com.graphai.mall.db.service.order.MallOrderService;
import com.graphai.mall.db.service.user.MallUserFormIdService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.service.user.behavior.MallAddressService;
import com.graphai.mall.db.service.user.behavior.MallCommentService;
import com.graphai.mall.db.util.OrderHandleOption;
import com.graphai.mall.db.util.OrderUtil;
import com.graphai.mall.wx.util.GoodsUtils;
import com.graphai.mall.wx.util.OrderCalculationUtil;
import com.graphai.payment.enumerate.Channel;
import com.graphai.properties.WxProperties;

/**
 * 订单服务-积分商城
 *
 * <p>
 * 订单状态： 101 订单生成，未支付；102，下单后未支付用户取消；103，下单后未支付超时系统自动取消 201
 * 支付完成，商家未发货；202，订单生产，已付款未发货，但是退款取消； 301 商家发货，用户未确认； 401 用户确认收货； 402
 * 用户没有确认收货超过一定时间，系统自动确认收货；
 *
 * <p>
 * 用户操作： 当101用户未付款时，此时用户可以进行的操作是取消订单，或者付款操作 当201支付完成而商家未发货时，此时用户可以取消订单并申请退款
 * 当301商家已发货时，此时用户可以有确认收货的操作 当401用户确认收货以后，此时用户可以进行的操作是删除订单，评价商品，或者再次购买
 * 当402系统自动确认收货以后，此时用户可以删除订单，评价商品，或者再次购买
 *
 * <p>
 * 注意：目前不支持订单退货和售后服务
 */
@Service
public class WxOrderIntegralService {
	private final Log logger = LogFactory.getLog(WxOrderIntegralService.class);

	@Autowired
	private WxProperties properties;

	@Autowired
	private MallOrderNotifyLogService logService;

	@Autowired
	private FenXiaoService fenXiaoService;
	@Autowired
	private AccountService accountService;
	@Autowired
	private MallUserService userService;
	@Autowired
	private MallOrderService orderService;
	@Autowired
	private MallOrderGoodsService orderGoodsService;
	@Autowired
	private MallGoodsService goodsService;
	@Autowired
	private MallGoodsIntegralService goodsIntegralService;
	@Autowired
	private MallAddressService addressService;
	@Autowired
	private MallCartService cartService;
	@Autowired
	private MallRegionService regionService;
	@Autowired
	private MallGoodsProductService productService;
	@Autowired
	private WxPayService wxPayService;
	@Autowired
	private WxPayConfig wxPayConfig;
	@Autowired
	private NotifyService notifyService;
	@Autowired
	private MallUserFormIdService formIdService;
	@Autowired
	private MallGrouponRulesService grouponRulesService;
	@Autowired
	private MallGrouponService grouponService;
	@Autowired
	private QCodeService qCodeService;
	@Autowired
	private ExpressService expressService;
	@Autowired
	private MallCommentService commentService;
	@Autowired
	private MallCouponService couponService;
	@Autowired
	private MallCouponUserService couponUserService;
	@Autowired
	private CouponVerifyService couponVerifyService;
	@Autowired
	private ShipService shipService;
	@Resource
	private FcAccountMapper fcAccountMapper;
	@Autowired
	private IGameRedHourRuleService redHourRuleService;
//	@Autowired
//	private PayAsUserAndMerchantService payAsUserAndMerchantService;

	@Autowired
	private WpcApiService wpcApiService;

	@Resource
	private FcAccountCapitalBillMapper fcAccountCapitalBillMapper;

	@Resource(name="hsWxPayService")
	private WxPayService hsWxPayService;
	@Resource(name="jfWxPayService")
	private WxPayService jfWxPayService;

	@Autowired
	private PayAsUserAndMerchantService payAsUserAndMerchantService;

	@Autowired
	private MallOrderGoodsService mallOrderGoodsService;
//	@Value("${litemall.wx.wap-name}")
//	private String wapName;
//	@Value("${litemall.wx.wap-url}")
//	private String wapUrl;

//	@Value("${litemall.wx.sub-app-id}")
//	private String subAppId;
//
//	@Value("${litemall.wx.app-id}")
//	private String appId;
//
//	@Value("${litemall.wx.mch-id}")
//	private String mchId;
//
//	@Value("${litemall.wx.sub-mch-id}")
//	private String subMchId;
//
//	@Value("${litemall.wx.mch-key}")
//	private String mchKey;
//
//	@Value("${litemall.wx.notify-url}")
//	private String notifyUrl;
//
//	@Value("${litemall.wx.key-path}")
//	private String keyPath;



	/**
	 * 订单列表
	 *
	 * @param userId
	 *            用户ID
	 * @param showType
	 *            订单信息： 0，全部订单； 1，待付款； 2，待发货； 3，待收货； 4，待评价。
	 * @param page
	 *            分页页数
	 * @param limit
	 *            分页大小
	 * @return 订单列表
	 */
	public Object list(String userId, Integer showType, Integer page, Integer limit) {
		if (userId == null) {
			return ResponseUtil.unlogin();
		}

		List<Short> orderStatus = OrderUtil.orderStatus(showType);
		List<MallOrder> orderList = orderService.queryByOrderStatus(userId,null, orderStatus,null, page, limit,showType);
		long count = PageInfo.of(orderList).getTotal();
		int totalPages = (int) Math.ceil((double) count / limit);

		List<Map<String, Object>> orderVoList = new ArrayList<>(orderList.size());
		for (MallOrder order : orderList) {
			Map<String, Object> orderVo = new HashMap<>();
			orderVo.put("id", order.getId());
			orderVo.put("orderSn", order.getOrderSn());
			orderVo.put("actualPrice", order.getActualPrice());
			orderVo.put("orderStatusText", OrderUtil.orderStatusText(order));
			orderVo.put("orderStatus", order.getOrderStatus());
			orderVo.put("handleOption", OrderUtil.build(order));
			orderVo.put("orderTime", order.getAddTime());

			MallGroupon groupon = grouponService.queryByOrderId(order.getId());
			if (groupon != null) {
				orderVo.put("isGroupin", true);
			} else {
				orderVo.put("isGroupin", false);
			}

			List<MallOrderGoods> orderGoodsList = orderGoodsService.queryByOid(order.getId());
			orderVo.put("goodsNumber", orderGoodsList.size());
			List<Map<String, Object>> orderGoodsVoList = new ArrayList<>(orderGoodsList.size());
			for (MallOrderGoods orderGoods : orderGoodsList) {
				Map<String, Object> orderGoodsVo = new HashMap<>();
				orderGoodsVo.put("id", orderGoods.getId());
				orderGoodsVo.put("goodsName", orderGoods.getGoodsName());
				orderGoodsVo.put("number", orderGoods.getNumber());
				orderGoodsVo.put("picUrl", orderGoods.getPicUrl());
				orderGoodsVo.put("price", orderGoods.getPrice());
				orderGoodsVo.put("specifications", orderGoods.getSpecifications());
				orderGoodsVoList.add(orderGoodsVo);
			}
			orderVo.put("goodsList", orderGoodsVoList);

			orderVoList.add(orderVo);
		}

		Map<String, Object> result = new HashMap<>();
		result.put("count", count);
		result.put("data", orderVoList);
		result.put("totalPages", totalPages);

		return ResponseUtil.ok(result);
	}


	public Object fenxiaoList(Integer showBelong,String mobile, Integer showType, Integer page, Integer limit) {

		//判断用户信息存在
		//String mobile = JacksonUtil.parseString(body, "mobile");
		if (mobile == null || "".equals(mobile)) {
			return ResponseUtil.fail(501,"手机号码不能为空!");
		}
		String userId="";
		List<MallUser> users = userService.queryByMobile(mobile);
		if (CollectionUtils.isNotEmpty(users)) {
			if (org.apache.commons.lang3.StringUtils.isNotEmpty(users.get(0).getId())) {
				userId = users.get(0).getId();
			}
		}else {
			return ResponseUtil.fail(501,"用户不存在!");
		}
		if (userId == null || userId.equals("")) {
			return ResponseUtil.unlogin();
		}

		System.out.println("要查询的数据是 :" + showBelong);
		/**
		 * 不查询的订单状态
		 */
		List<Short> notQueryOrderStatus = null;
		List<String> userIds = new ArrayList<String>();
		//获取发展的下线
		List<MallUser> userList = userService.getFenxiaoSubordinate(userId);
		if(showBelong==0){
			//看全部
			for (int i = 0; i < userList.size(); i++) {
				userIds.add(userList.get(i).getId());
			}
			//自己的订单也要查询出来
			userIds.add(userId);
		}else if(showBelong==1){
			//看分销
			for (int i = 0; i < userList.size(); i++) {
				userIds.add(userList.get(i).getId());
			}
		}else if(showBelong==2){
			//看自己
			userIds.add(userId);
		}else{
			//看全部
			for (int i = 0; i < userList.size(); i++) {
				userIds.add(userList.get(i).getId());
			}
			//自己的订单也要查询出来
			userIds.add(userId);

			notQueryOrderStatus = OrderUtil.orderStatus(5);
		}


//		for (int i = 0; i < userList.size(); i++) {
//			userIds.add(userList.get(i).getId());
//		}
		//自己的订单也要查询出来
		//userIds.add(userId);
		List<Short> orderStatus = OrderUtil.orderStatus(showType);
		//List<MallOrder> orderList = orderService.queryByOrderStatusFenxiaoList(userId, orderStatus, page, limit);
		List<MallOrder> orderList = orderService.queryByOrderStatusaUserIdsLitemall(userIds, orderStatus,notQueryOrderStatus, page, limit);

		long count = PageInfo.of(orderList).getTotal();
		int totalPages = (int) Math.ceil((double) count / limit);

		List<Map<String, Object>> orderVoList = new ArrayList<>(orderList.size());
		for (MallOrder order : orderList) {
			String orderId = order.getId();

			Map<String, Object> orderVo = new HashMap<>();
			orderVo.put("id", orderId);
			orderVo.put("orderSn", order.getOrderSn());
			orderVo.put("actualPrice", order.getActualPrice());
			orderVo.put("orderPrice", order.getOrderPrice());
			orderVo.put("orderStatusText", OrderUtil.orderStatusText(order));
			orderVo.put("orderStatus", order.getOrderStatus());
			orderVo.put("handleOption", OrderUtil.build(order));
			orderVo.put("orderTime", order.getAddTime());
			///分销相关的数据
			orderVo.put("isCal", order.getIsCal());//是否已经计算
			orderVo.put("primaryDividend", order.getPrimaryDividend());
			orderVo.put("secondaryDividend", order.getSecondaryDividend());
			orderVo.put("markupDividend", order.getMarkupDividend());
			orderVo.put("consignee",order.getConsignee());
			orderVo.put("goodsPrice",order.getGoodsPrice());      //'商品总费用',【销售价格】
			orderVo.put("integralPrice",order.getIntegralPrice());//'用户积分减免',【兑换积分】
			orderVo.put("freightPrice",order.getFreightPrice());  // '配送费用',【配送费用】
			orderVo.put("couponPrice",order.getCouponPrice());    //'优惠券减免',【商品原价】

			//查询出当前人的分润充值记录
			//FcAccountRechargeBill reBill = accountService.getAccountRechargeBillByUserId(userId, orderId);

			if (!order.getUserId().equals(userId)) {
				//orderVo.put("tpModel", "ZJFR");
				///MallUser  user = userService.findById(userId);
				//orderVo.put("buyer", user.getNickname());

				if(userId!=null && userId.toString().equals(order.getTpid())){
					orderVo.put("tpModel", "ZJFR");
				}else{
					orderVo.put("tpModel", "JJFR");
				}

			}else{
				orderVo.put("tpModel", "ZJFR");
				orderVo.put("buyer", "本人购买");
			}


			if(order.getTpid()!=null){
				orderVo.put("tpUserName", order.getTpUserName());
			}else{
				String myOrderUserId = order.getUserId();
				MallUser tpUser = null;
				if(CollectionUtils.isNotEmpty(userList)){
					for (int i = 0; i < userList.size(); i++) {
						if(myOrderUserId==userList.get(i).getId()){
							//取出当前订单的分销人员
							tpUser = userList.get(i);
							break;
						}
					}
				}
				if(tpUser!=null){
					orderVo.put("tpUserName", tpUser.getNickname());
				}
			}

			MallGroupon groupon = grouponService.queryByOrderId(order.getId());
			if (groupon != null) {
				orderVo.put("isGroupin", true);
			} else {
				orderVo.put("isGroupin", false);
			}

			List<MallOrderGoods> orderGoodsList = orderGoodsService.queryByOid(order.getId());
			orderVo.put("goodsNumber", orderGoodsList.size());
			List<Map<String, Object>> orderGoodsVoList = new ArrayList<>(orderGoodsList.size());
			for (MallOrderGoods orderGoods : orderGoodsList) {
				Map<String, Object> orderGoodsVo = new HashMap<>();
				orderGoodsVo.put("id", orderGoods.getId());
				orderGoodsVo.put("goodId", orderGoods.getGoodsId());
				orderGoodsVo.put("goodsName", orderGoods.getGoodsName());
				orderGoodsVo.put("number", orderGoods.getNumber());
				orderGoodsVo.put("picUrl", orderGoods.getPicUrl());
				orderGoodsVo.put("price", orderGoods.getPrice());
				orderGoodsVo.put("specifications", orderGoods.getSpecifications());
				orderGoodsVoList.add(orderGoodsVo);
			}
			orderVo.put("goodsList", orderGoodsVoList);

			orderVoList.add(orderVo);
		}

		Map<String, Object> result = new HashMap<>();
		result.put("count", count);
		result.put("data", orderVoList);
		result.put("totalPages", totalPages);

		return ResponseUtil.ok(result);
	}


	/**
	 * 订单详情
	 *
	 * @param mobile
	 *            用户手机号码
	 * @param orderId
	 *            订单ID
	 * @return 订单详情
	 */
	public Object detail(@NotNull String mobile, @NotNull String orderId) {

		//判断用户信息存在
		//String mobile = JacksonUtil.parseString(body, "mobile");
		String userId="";
		List<MallUser> users = userService.queryByMobile(mobile);
		if (CollectionUtils.isNotEmpty(users)) {
			if (org.apache.commons.lang3.StringUtils.isNotEmpty(users.get(0).getId())) {
				userId = users.get(0).getId();
			}
		}else {
			return ResponseUtil.fail(501,"用户不存在!");
			//return ResponseUtil.unlogin();
		}

		//String orderId = JacksonUtil.parseString(body, "orderId");
		// 订单信息
		MallOrder order = orderService.findById(orderId);
		if (null == order) {
			return ResponseUtil.fail(ORDER_UNKNOWN, "订单不存在");
		}
		if (!order.getUserId().equals(userId)) {
			//return ResponseUtil.fail(ORDER_INVALID, "不是当前用户的订单");
		}
		Map<String, Object> orderVo = new HashMap<String, Object>();
		orderVo.put("id", order.getId());
		orderVo.put("orderSn", order.getOrderSn());
		orderVo.put("addTime", order.getAddTime());
		orderVo.put("consignee", order.getConsignee());
		orderVo.put("mobile", order.getMobile());
		orderVo.put("address", order.getAddress());
		orderVo.put("goodsPrice", order.getGoodsPrice());
		orderVo.put("couponPrice", order.getCouponPrice());
		orderVo.put("freightPrice", order.getFreightPrice());
		orderVo.put("actualPrice", order.getActualPrice());
		orderVo.put("orderPrice", order.getOrderPrice());
		orderVo.put("integralPrice",order.getIntegralPrice());
		orderVo.put("orderStatusText", OrderUtil.orderStatusText(order));
		orderVo.put("handleOption", OrderUtil.build(order));
		orderVo.put("expCode", order.getShipChannel());
		orderVo.put("expNo", order.getShipSn());
		orderVo.put("expressOrderNumber", order.getShipSn());  //快递订单号
		orderVo.put("expressOrderName",order.getShipChannel());//快递公司
		orderVo.put("expressOrderNo",order.getShipCode());     //快递公司编号
		orderVo.put("payTime",order.getPayTime());        //支付时间
		orderVo.put("confirmTime",order.getConfirmTime());//确认收货时间
		orderVo.put("orderStatus",order.getOrderStatus());//订单状态

		List<MallOrderGoods> orderGoodsList = orderGoodsService.queryByOid(order.getId());

		Map<String, Object> result = new HashMap<>();
		result.put("orderInfo", orderVo);
		result.put("orderGoods", orderGoodsList);

		// 订单状态为已发货且物流信息不为空
		// "YTO", "800669400640887922"
		 if (order.getOrderStatus().equals(OrderUtil.STATUS_SHIP)) {
             //ExpressInfo ei = expressService.getExpressInfo(order.getShipChannel(), order.getShipSn());
             Object object = shipService.getLogisticsInfo(order.getShipSn(), order.getShipCode());
             logger.info("查询订单物流数据 :"+ JacksonUtils.bean2Jsn(object));
             result.put("expressInfo", object);
         }
//		payType=3，对应支付接口：channel=balance_payment  余额支付
//		payType=4，对应支付接口：channel=wx_nimiprog  小程序支付
		int payType = 4;
//		for (MallOrderGoods MallOrderGoods : orderGoodsList){
//			MallGoods MallGoods = goodsService.selectOneById(MallOrderGoods.getGoodsId());
//			MallCoupon MallCoupon = couponService.findById(MallGoods.getCouponId());
			//`coupon_type` bigint(2) DEFAULT '0' COMMENT '默认 0优惠券  1折扣卷  2礼品  3兑换券',
//			if (2 == MallCoupon.getCouponType()){
//				payType = 4;
//				break;
//			}
//		}
		if("3".equals(order.getActivityType())){
			payType = 3;//特别活动标识（0元购 zero  3-积分商城余额支付  4-积分商城小程序支付）
		}

		BigDecimal orderExchangeAmount = SystemConfig.getLitemallOrderExchangeAmount();//积分兑换金额比例
		result.put("payType", 3);//说明支付方式只支持某种方式
		result.put("orderExchangeAmount", orderExchangeAmount);//
		return ResponseUtil.ok(result);
	}

	/**
	 * 提交订单
	 * <p>
	 * 1. 创建订单表项和订单商品表项;
	 * 2. 购物车清空;
	 * 3. 优惠券设置已用;
	 * 4. 商品货品库存减少;
	 * 5.如果是团购商品，则创建团购活动表项。
	 *
	 * @param body
	 *            订单信息，{ userId，xxx，cartId：xxx, addressId: xxx, couponId: xxx, message:xxx, grouponRulesId: xxx, grouponLinkId: xxx}
	 * @return 提交订单操作结果
	 */
	@Transactional
	public Object submit(String body, String clientIp) {

		Map<String, Object> data = null;
		String deptId = ""; //订单归属部门ID
		try {
			//判断用户信息存在
			String mobile = JacksonUtil.parseString(body, "mobile");
			String userId="";
			List<MallUser> users = userService.queryByMobile(mobile);
			if (CollectionUtils.isNotEmpty(users)) {
				if (org.apache.commons.lang3.StringUtils.isNotEmpty(users.get(0).getId())) {
					userId = users.get(0).getId();
				}
			}else {
				return ResponseUtil.fail(501,"用户不存在!");
				//return ResponseUtil.unlogin();
			}
			if (body == null) {
				return ResponseUtil.badArgument();
			}

			logger.info("1、参数body:" + body);
			MallCoupon coupon = null;
			MallCouponUser couponUser = null;
			String cartId = JacksonUtil.parseString(body, "cartId");
			String addressId = JacksonUtil.parseString(body, "addressId");
			String couponId = JacksonUtil.parseString(body, "couponId");
			//优惠券编码
			String couponCode = JacksonUtil.parseString(body, "couponCode");
			//优惠券信息
			String message = JacksonUtil.parseString(body, "message");
			Integer grouponRulesId = JacksonUtil.parseInteger(body, "grouponRulesId");
			Integer grouponLinkId = JacksonUtil.parseInteger(body, "grouponLinkId");
			// 分销员的主键ID
			String tpid = JacksonUtil.parseString(body, "tpid");
			Boolean addressShow = JacksonUtil.parseBoolean(body, "addressShow");
			//下单端口H5端默认写Customer，小程序目前是空，后续可以直接写MiniBusiness
			String submitClient = JacksonUtil.parseString(body, "submitClient");
			//如果接口为空的话
			submitClient = StringUtils.isNotBlank(submitClient) ? submitClient : "MiniBusiness";
			// 当前购买商品的是什么用户
			MallUser myUser = users.get(0);
			if (myUser == null) {
				return ResponseUtil.badArgument();
			}
			if (cartId == null || couponId == null || cartId.equals("0")) {
				return ResponseUtil.badArgument();
			}
			// 收货地址
			MallAddress checkedAddress = addressService.query(userId, addressId);
			if (addressShow && checkedAddress == null) {
				return ResponseUtil.badArgument();
			}

			// 货品价格
			List<MallCart> checkedGoodsList = null;
			if (cartId.equals("0") || cartId==null) {
				logger.info("2、进入userid 查询购物车产品 ：" + userId);
				checkedGoodsList = cartService.queryByUidAndChecked(userId);
			} else {
				logger.info("3、进入cartId 查询购物车产品 ：" + cartId);
				MallCart cart = cartService.findById(cartId);
				// 设置归属部门编号
				String deptIdTemp = cart.getDeptId();
				if(deptIdTemp != null && !"".equals(deptIdTemp)){
					deptId = deptIdTemp;
				}
				checkedGoodsList = new ArrayList<>(1);
				checkedGoodsList.add(cart);
			}
			if (checkedGoodsList.size() == 0) {
				return ResponseUtil.badArgumentValue("选择商品数据不正确");
			}
			BigDecimal checkedGoodsPrice = new BigDecimal(0.00);
			// 使用优惠券减免的金额
			BigDecimal couponPrice = new BigDecimal(0.00);
			//礼品需支付的运费
			BigDecimal finalActual = new BigDecimal(0.00);
			//商品原价
			BigDecimal factoryPrice = new BigDecimal(0.00);

			for (MallCart checkGoods : checkedGoodsList) {
					// 商品初始价格
					BigDecimal goodsPrice = checkGoods.getPrice();
					BigDecimal finalPrice = goodsPrice;

					//通过购物车商品id获取商品对象
					//MallGoods cartMallGoods = goodsService.findById(checkGoods.getGoodsId());
					JSONObject obj = goodsService.queryGoodsByProductId(checkGoods.getProductId());
					if (Objects.equals(null, obj)) {
						return ResponseUtil.fail(507, "商品信息有误!");
					}

					//商品原价
					if(obj.getString("gFactoryPrice") != null){
						factoryPrice = factoryPrice.add(new BigDecimal(obj.getString("gFactoryPrice")));
					}

					//运费
					BigDecimal yprice = new BigDecimal(0.00);
					String gCRetailPrice = obj.getString("gCRetailPrice");
					if(gCRetailPrice != null){
						yprice = new BigDecimal(gCRetailPrice);
					}
					finalActual = finalActual.add((null == yprice || yprice.compareTo(BigDecimal.ZERO)==0)? SystemConfig.getFreightLitemall():yprice);

					// 订单成交计算金额
					checkedGoodsPrice = checkedGoodsPrice.add(finalPrice.multiply(new BigDecimal(checkGoods.getNumber())));

					// 设置归属部门编号
					String deptIdTemp = obj.getString("deptId");
					if(deptIdTemp != null && !"".equals(deptIdTemp)){
						deptId = deptIdTemp;
					}
			}

			// 根据订单商品总价计算运费，满足条件（例如88元）则免运费，否则需要支付运费（例如8元）；
			BigDecimal freightPrice = new BigDecimal(0.00);

			// 可以使用的其他钱，例如用户积分
			BigDecimal integralPrice = new BigDecimal(0.00);

			// 订单费用
			BigDecimal orderTotalPrice = checkedGoodsPrice.add(freightPrice).subtract(couponPrice);

			//订单金额
			FcAccountExample fcAccountMallExample = new FcAccountExample();
//			fcAccountMallExample.or().andCustomerIdEqualTo(myUser.getId()).andAccountTypeEqualTo(false).andStatusEqualTo(new Short("1"));
			fcAccountMallExample.or().andCustomerIdEqualTo(myUser.getId()).andStatusEqualTo(new Short("1"));
			FcAccount fcAccountMall=  fcAccountMapper.selectOneByExample(fcAccountMallExample);
			if (null == fcAccountMall || null == fcAccountMall.getAccountId()){
				return ResponseUtil.fail("未来卡没绑定有注册账户，请联系客服");
				//特殊情况，未来卡要是没绑定没有注册账户，这里注册一下
				//fcAccountMall = accountService.accountTradeCreateAccountIntegral(myUser.getId());
			}
			BigDecimal integralAmount = fcAccountMall.getIntegralAmount();  // 用户积分商城余额
			BigDecimal exchangePrice = new BigDecimal(0.00);// 积分兑换金额
			BigDecimal redeemPoints = new BigDecimal(0.00); // 兑换积分
			if(integralAmount == null){
				integralAmount = BigDecimal.ZERO;
			}
			if(orderTotalPrice == null){
				orderTotalPrice = BigDecimal.ZERO;
			}
			if (integralAmount.compareTo(orderTotalPrice) == -1){
				return ResponseUtil.badArgumentValue("积分不足");
			}

			// 最终支付费用 = 配送费 + 商品原价 + 积分兑换金额
			BigDecimal finalActualPrice = new BigDecimal(0.00);
			if (addressShow){
				finalActualPrice = finalActual;
			}
			finalActualPrice = (finalActualPrice.add(factoryPrice)).add(exchangePrice);

			String orderId = null;
			MallOrder order = null;
			// 订单
			order = new MallOrder();
			order.setUserId(userId);
			order.setOrderSn(orderService.generateOrderSn(userId));
			order.setOrderStatus(OrderUtil.STATUS_CREATE);
			order.setMessage(message);
			order.setHuid("litemall");//积分商城-订单来源
			if (null != checkedAddress){
				order.setConsignee(checkedAddress.getName());
				order.setMobile(checkedAddress.getTel());
				String detailedAddress = checkedAddress.getProvince() + checkedAddress.getCity() + checkedAddress.getCounty() + " " + checkedAddress.getAddressDetail();
				order.setAddress(detailedAddress);
			}
			order.setGoodsPrice(checkedGoodsPrice);
			freightPrice = finalActual; //配送费赋值
			order.setFreightPrice(BigDecimal.ZERO); //'配送费用',【配送费用】
			order.setCouponPrice(BigDecimal.ZERO);  //'优惠券减免',【商品原价】
			order.setIntegralPrice(checkedGoodsPrice);//'用户积分减免',【兑换积分】
			order.setOrderPrice(BigDecimal.ZERO);   //订单费用 = '用户积分减免',【含积分不足抵扣】
			order.setActualPrice(BigDecimal.ZERO);
			order.setBid(AccountStatus.BID_54);
			//判断用商城积分余额支付，还是用微信小程序+积分支付
			if(finalActualPrice.compareTo(new BigDecimal("0")) == 0){
				order.setActivityType("3");//特别活动标识（0元购 zero  3-积分商城余额支付  4-积分商城小程序支付）
			}else{
				order.setActivityType("4");//特别活动标识（0元购 zero  3-积分商城余额支付  4-积分商城小程序支付）
			}

			// 如果使用了优惠券，设置优惠券使用状态
			//if (couponId != 0 && couponId != -1 && StringUtils.isNotBlank(couponCode)) {
			if ( !"0".equals(couponId) &&  !"-1".equals(couponId) && StringUtils.isNotBlank(couponCode)) {
				couponUser =  couponUserService.findCouponUserByCouponCode(couponCode);
				order.setCouponId(couponUser.getId());
			}

			order.setSource(submitClient);
			if (null != checkedAddress) {
				order.setProvinceName(checkedAddress.getProvince());
				order.setCityName(checkedAddress.getCity());
				order.setAreaName(checkedAddress.getCounty());
				order.setAddressDetail(checkedAddress.getAddressDetail());
			}
			order.setClientIp(clientIp);

			// 1代表为分销商用户购买商品,2代表普通用户购买，普通用户购买的情况下要计算分销商收益，真正的收益计算在支付后
			if ("1".equals(myUser.getIsDistribut())) {
				order.setTpid(userId.toString());
				order.setTpUserName(myUser.getNickname());
			} else if(!"1".equals(myUser.getIsDistribut()) && StringUtils.isNotBlank(tpid)) {
				//分销员信息
				MallUser fenXiaoUser = userService.findById(tpid);
				order.setTpid(tpid);
				order.setTpUserName(fenXiaoUser.getNickname());
			}

			order.setGrouponPrice(new BigDecimal(0.00)); // 团购价格

			List<MallOrderGoods> orderGoodsList = new ArrayList<MallOrderGoods>();
			order.setDeptId(deptId);
			// 添加订单表
			orderService.add(order);
			orderId = order.getId();

			// 添加订单商品表项
			for (MallCart cartGoods : checkedGoodsList) {
				String  goodsId = cartGoods.getGoodsId() ;

				// 订单商品
				MallOrderGoods orderGoods = new MallOrderGoods();
				orderGoods.setOrderId(order.getId());
				orderGoods.setGoodsId(goodsId);
				orderGoods.setGoodsSn(cartGoods.getGoodsSn());
				orderGoods.setSrcGoodId(cartGoods.getSrcGoodId());
				orderGoods.setProductId(cartGoods.getProductId());
				orderGoods.setGoodsName(cartGoods.getGoodsName());

				if(coupon!=null && couponUser!=null){
					String[] goodsIds = coupon.getGoodsValue();
					if(ArrayUtils.contains(goodsIds, goodsId)){
						orderGoods.setCouponUserId(couponUser.getId());
						orderGoods.setCouponPrice(coupon.getDiscount());
					}
				}

				orderGoods.setPicUrl(cartGoods.getPicUrl());
				orderGoods.setPrice(cartGoods.getPrice());
				orderGoods.setNumber(cartGoods.getNumber());
				orderGoods.setSpecifications(cartGoods.getSpecifications());
				orderGoods.setAddTime(LocalDateTime.now());

				orderGoodsService.add(orderGoods);

				orderGoodsList.add(orderGoods);
			}

			// 删除购物车里面的商品信息
			cartService.clearGoods(userId);

			// 商品货品数量减少
			for (MallCart checkGoods : checkedGoodsList) {
				String productId = checkGoods.getProductId();
//				MallGoodsProduct product = productService.findById(productId);
//				Integer remainNumber = product.getNumber() - checkGoods.getNumber();
//				if (remainNumber < 0) {
//					throw new RuntimeException("下单的商品货品数量大于库存量");
//				}
//				if (productService.reduceStock(productId, checkGoods.getNumber()) == 0) {
//					throw new RuntimeException("商品货品库存减少失败");
//				}
				JSONObject product=null;
				try {
					product = goodsService.queryGoodsByProductId(productId);
					if (!Objects.equals(null, product)) {

						//判断库存充足
						Integer remainNumber = product.getShort("number") - checkGoods.getNumber();
						if (remainNumber < 0) {
							throw new RuntimeException("下单的商品货品数量大于库存量");
						}

						logger.info("4、--------修改积分商城商品库存-----------------");
						//在开放平台goods表添加销量和product表减去库存
						//该商品一定要属于积分商城商品
						if (Objects.equals("3",product.get("virtualGood") )|| 3==Integer.parseInt(String.valueOf(product.get("virtualGood")))) {
							Integer buyNum =Integer.parseInt(String.valueOf(checkGoods.getNumber()));
							logger.info("5、购买数量："+buyNum);
							if (!goodsIntegralService.updateZyGoodsIntegeral(productId, buyNum)) {
								logger.info("6、修改积分商城商品库存失败");
							}
						}
					}else{
						return ResponseUtil.fail(507, "商品信息有误!");
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

//			 如果使用了优惠券，设置优惠券使用状态
//			if (couponId != 0 && couponId != -1 && StringUtils.isNotBlank(couponCode)) {
//			if ( !"0".equals(couponId) &&  !"-1".equals(couponId) && StringUtils.isNotBlank(couponCode)) {
//				couponUser =  couponUserService.findCouponUserByCouponCode(couponCode);
//				couponUser.setUsedTime(LocalDateTime.now());
//				couponUser.setOrderId(orderId);
//				couponUserService.update(couponUser);
//			}

			data = new HashMap<>();
			data.put("orderId", orderId);
		} catch (Exception e) {
			logger.error("7、订单提交异常: "+e.getMessage(),e);
			return ResponseUtil.fail(-1,"订单提交异常");
		}
		return ResponseUtil.ok(data);
	}

	/**
	 * 取消订单
	 * <p>
	 * 1. 检测当前订单是否能够取消； 2. 设置订单取消状态； 3. 商品货品库存恢复； 4. TODO 优惠券； 5. TODO 团购活动。
	 *
	 * @param body
	 *            订单信息，{ orderId：xxx，mobiel：xxx}
	 * @return 取消订单操作结果
	 */
	@Transactional
	public Object cancel( String body) {

		//判断用户信息存在
		String mobile = JacksonUtil.parseString(body, "mobile");
		if(mobile == null || "".equals(mobile)){
			return ResponseUtil.fail(402,"手机号码不能为空!");
		}
		String userId="";
		List<MallUser> users = userService.queryByMobile(mobile);
		if (CollectionUtils.isNotEmpty(users)) {
			if (org.apache.commons.lang3.StringUtils.isNotEmpty(users.get(0).getId())) {
				userId = users.get(0).getId();
			}
		}else {
			return ResponseUtil.fail(501,"用户不存在!");
		}

		String orderId = JacksonUtil.parseString(body, "orderId");
		if (orderId == null || "".equals(orderId)) {
			return ResponseUtil.fail(402,"订单编号不能为空!");
		}

		MallOrder order = orderService.findById(orderId);
		if (order == null) {
			return ResponseUtil.badArgumentValue();
		}
		if (!order.getUserId().equals(userId)) {
			return ResponseUtil.badArgumentValue("订单不属于本人,不能取消");
		}

		LocalDateTime preUpdateTime = order.getUpdateTime();

		// 检测是否能够取消
		OrderHandleOption handleOption = OrderUtil.build(order);
		if (!handleOption.isCancel()) {
			return ResponseUtil.fail(ORDER_INVALID_OPERATION, "订单不能取消");
		}

		// 设置订单已取消状态
		order.setOrderStatus(OrderUtil.STATUS_CANCEL);
		order.setEndTime(LocalDateTime.now());
		if (orderService.updateWithOptimisticLocker(order) == 0) {
			throw new RuntimeException("更新数据已失效");
		}

		// 商品货品数量增加
//		List<MallOrderGoods> orderGoodsList = orderGoodsService.queryByOid(orderId);
//		for (MallOrderGoods orderGoods : orderGoodsList) {
//			String productId = orderGoods.getProductId();
//			Short number = orderGoods.getNumber();
//			if (productService.addStock(productId, number) == 0) {
//				throw new RuntimeException("商品货品库存增加失败");
//			}
//		}
		try {
			logger.info("4、--------修改积分商城商品库存-----------------");
			List<MallOrderGoods> orderGoodsList = orderGoodsService.queryByOid(orderId);
			for (MallOrderGoods orderGoods : orderGoodsList) {
				String productId = orderGoods.getProductId();
				Short number = orderGoods.getNumber();
				int buyNum = number.intValue();
				if (!goodsIntegralService.updateZyGoodsIntegeral(productId, -buyNum)) {
					throw new RuntimeException("商品货品库存增加失败");
				}
			}
		}catch (Exception e){
			logger.error("修改积分商城商品库存-异常: "+e.getMessage(),e);
		}


		return ResponseUtil.ok();
	}

	/**
	 * 付款订单的预支付会话标识
	 * <p>
	 * 1. 检测当前订单是否能够付款
	 * 2. 微信商户平台返回支付订单ID
	 * 3. 设置订单付款状态
	 *
	 * @param body-userId
	 *            用户ID
	 * @param body
	 *            订单信息，{ orderId：xxx }
	 * @return 支付订单ID
	 */
	@Transactional
	public Object prepay(String body, HttpServletRequest request) {

		//判断用户信息
		String mobile = JacksonUtil.parseString(body, "mobile");
		if (mobile == null || "".equals(mobile)) {
			return ResponseUtil.fail(501,"手机号码不能为空!");
		}
		String userId="";
		List<MallUser> users = userService.queryByMobile(mobile);
		if (CollectionUtils.isNotEmpty(users)) {
			if (org.apache.commons.lang3.StringUtils.isNotEmpty(users.get(0).getId())) {
				userId = users.get(0).getId();
			}
		}else {
			logger.info("1、用户不存在!");
			return ResponseUtil.fail(501,"用户不存在!");
		}
		if (userId == null || userId.equals("")) {
			logger.info("2、用户不存在!userId为空！");
			return ResponseUtil.unlogin();
		}

		//判断订单信息
		String orderId = JacksonUtil.parseString(body, "orderId");
		String channel = JacksonUtil.parseString(body, "channel");
		if (orderId == null || "".equals(orderId)) {
			return ResponseUtil.badArgument();
		}
		MallOrder order = orderService.findById(orderId);
		if (order == null) {
			return ResponseUtil.badArgumentValue();
		}
		if (!order.getUserId().equals(userId)) {
			return ResponseUtil.badArgumentValue();
		}
		// 检测是否能够取消
		OrderHandleOption handleOption = OrderUtil.build(order);
		if (!handleOption.isPay()) {
			logger.info("3、订单不能支付，原因：handleOption.isPay()="+handleOption.isPay());
			return ResponseUtil.fail(ORDER_INVALID_OPERATION, "订单不能支付");
		}

		//MallUser user = userService.findById(userId);
		MallUser user = users.get(0);

//		WxPayMpOrderResult result = null;
		ResponseUtil result = null;
		String prepayId = null;

		MallOrderNotifyLog log = new MallOrderNotifyLog();
		//订单金额
//		BigDecimal orderPrice = order.getOrderPrice();//含积分不足抵扣
		BigDecimal orderPrice = order.getIntegralPrice();//兑换积分
		FcAccountExample fcAccountMallExample = new FcAccountExample();
		fcAccountMallExample.or().andCustomerIdEqualTo(user.getId()).andStatusEqualTo(new Short("1"));
		FcAccount fcAccountMall=  fcAccountMapper.selectOneByExample(fcAccountMallExample);
		if (null == fcAccountMall || null == fcAccountMall.getAccountId()){
			return ResponseUtil.badArgumentValue();
		}
//		BigDecimal mallAmount = fcAccountMall.getAmount();
		BigDecimal mallAmount = fcAccountMall.getIntegralAmount();
		if(mallAmount == null){
			mallAmount = BigDecimal.ZERO;
		}
		if(orderPrice == null){
			orderPrice = BigDecimal.ZERO;
		}
		if (mallAmount.compareTo(orderPrice) < 0){
			logger.info("4、积分不足");
			return ResponseUtil.badArgumentValue("积分不足");
		}
		// 检查这个订单是否已经处理过
		if (OrderUtil.isPayStatus(order) && order.getPayId() != null) {
			try {
				log.setErrMsg("5、订单已经处理过");
				log.setIsSuccess("0F");
				logService.add(log);
			} catch (Exception e1) {
				logger.error("6、日志添加异常: "+e1.getMessage(),e1);
			}
			return WxPayNotifyResponse.success("订单已经处理成功!");
		}
		try {
			//目前只用到这个
			//（5）微信小程序支付先获取小程序Openid
			//String subOpenid = user.getWeixinOpenid();//前端转过来  subOpenid
//			String subOpenid = JacksonUtil.parseString(body, "subOpenid");
//			if (subOpenid == null || "".equals(subOpenid)) {
//				logger.info("7、订单不能支付，subOpenid参数为空！subOpenid="+subOpenid);
//				return ResponseUtil.fail(AUTH_OPENID_UNACCESS, "订单不能支付，subOpenid参数为空！");
//			}

			// 积分商城全积分支付
			Map merchantParamsMap = new HashMap();
			merchantParamsMap.put("dType", AccountStatus.DISTRIBUTION_TYPE_00);  //直接分润
			merchantParamsMap.put("capitalTrend", AccountStatus.CAPITAL_TREND_02);
			merchantParamsMap.put("changeType", AccountStatus.CHANGE_TYPE_03);
			merchantParamsMap.put("changeAccountType", AccountStatus.CHANGE_ACCOUNT_TYPE_2);
			merchantParamsMap.put("message", "购买积分商品");
			AccountUtils.accountChange(userId, order.getIntegralPrice(), CommonConstant.TASK_REWARD_INTEGRAL, AccountStatus.BID_10, merchantParamsMap);

			// 修改订单状态
			order.setPayTime(LocalDateTime.now());
			order.setUpdateTime(LocalDateTime.now());
			order.setOrderStatus(OrderUtil.STATUS_PAY);
			orderService.updateWithOptimisticLocker(order);
//				WxPayUnifiedOrderRequest orderRequest = new WxPayUnifiedOrderRequest();
//				orderRequest.setOutTradeNo(order.getOrderSn());
//				orderRequest.setSubOpenid(subOpenid);
//				//orderRequest.setSubAppId(properties.getSubAppId());
//				//orderRequest.setSubAppId(subAppId);
//				orderRequest.setBody("订单：" + order.getOrderSn());
//				// 元转成分
//				int fee = 0;
//				BigDecimal actualPrice = order.getActualPrice();
//				fee = actualPrice.multiply(new BigDecimal(100)).intValue();
//				orderRequest.setTotalFee(fee);
//				orderRequest.setSpbillCreateIp(IpUtil.getIpAddr(request));

			/*WxPayConfig payConfig = new WxPayConfig();
//				payConfig.setAppId(properties.getAppId());
//				payConfig.setMchId(properties.getMchId());
			payConfig.setAppId(appId);
			payConfig.setMchId(mchId);
			//子商户信息
//				payConfig.setSubAppId(properties.getSubAppId());
//				payConfig.setSubMchId(properties.getSubMchId());
//				payConfig.setMchKey(properties.getMchKey());
//				payConfig.setNotifyUrl(properties.getNotifyUrl());
//				payConfig.setKeyPath(properties.getKeyPath());
			payConfig.setSubAppId(subAppId);
			payConfig.setSubMchId(subMchId);
			payConfig.setMchKey(mchKey);
			payConfig.setNotifyUrl(notifyUrl);
			payConfig.setKeyPath(keyPath);
			//新加4个属性
			payConfig.setPrivateKeyPath(keyPath);
			payConfig.setPrivateCertPath(keyPath);
			payConfig.setCertSerialNo(keyPath);
			payConfig.setApiV3Key(keyPath);
			payConfig.setTradeType("JSAPI");
			payConfig.setSignType("MD5");
			wxPayService.setConfig(payConfig);

			logger.error("8、-------------------------------------payConfig="+payConfig);
			result = wxPayService.createOrder(orderRequest);
			logger.error("9、-------------------------------------result="+result);
			*/

			//自营商城支付参数
//				WxPayConfig payConfig = new WxPayConfig();
//				payConfig.setAppId(properties.getHsAppId());
//				payConfig.setMchId(properties.getHsMchId());
//				payConfig.setSubAppId(properties.getHsAppletsAppId());
//				payConfig.setSubMchId(properties.getHsChildMchId());
//				payConfig.setMchKey(properties.getHsMchKey());
//				payConfig.setNotifyUrl(properties.getJfNotifyUrl());
//				payConfig.setKeyPath(properties.getKeyPath());
//				payConfig.setTradeType("JSAPI");
//				payConfig.setSignType("MD5");
//				hsWxPayService.setConfig(payConfig);
//				result = hsWxPayService.createOrder(orderRequest);

			//积分商城支付参数
			/*WxPayConfig payConfig = new WxPayConfig();
			payConfig.setAppId(properties.getJfAppId());
			payConfig.setMchId(properties.getJfMchId());
			payConfig.setSubAppId(properties.getJfAppletsAppId());
			payConfig.setSubMchId(properties.getJfChildMchId());
			payConfig.setMchKey(properties.getJfMchKey());
			payConfig.setNotifyUrl(properties.getJfNotifyUrl());
			payConfig.setKeyPath(properties.getKeyPath());
			payConfig.setTradeType("JSAPI");
			payConfig.setSignType("MD5");
			jfWxPayService.setConfig(payConfig);
			logger.error("8、-------------------------------------payConfig="+payConfig);
			result = jfWxPayService.createOrder(orderRequest);
			logger.error("9、-------------------------------------result="+result);*/

			// 缓存prepayID用于后续模版通知
//				prepayId = result.getPackageValue();
//				prepayId = prepayId.replace("prepay_id=", "");
//
//				MallUserFormid userFormid = new MallUserFormid();
//				userFormid.setOpenid(subOpenid);
//				userFormid.setFormid(prepayId);
//				userFormid.setIsprepay(true);
//				userFormid.setUseamount(3);
//				userFormid.setExpireTime(LocalDateTime.now().plusDays(7));
//				formIdService.addUserFormid(userFormid);
//				logger.info("10、预支付请求成功。");

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("11、订单不能支付，原因："+e.getMessage());
			return ResponseUtil.fail(ORDER_PAY_FAIL, "订单不能支付");
		}

//		if (orderService.updateWithOptimisticLocker(order) == 0) {
//			logger.error("12、更新数据已经失效。");
//			return ResponseUtil.updatedDateExpired();
//		}
		logger.info("13、预支付请求成功。返回结果："+result);
		return ResponseUtil.ok("支付成功");
	}

	/**
	 * 微信付款成功或失败回调接口
	 * <p>
	 * 1. 检测当前订单是否是付款状态; 2. 设置订单付款成功状态相关信息; 3. 响应微信商户平台.
	 *
	 * @param request
	 *            请求内容
	 * @param response
	 *            响应内容
	 * @return 操作结果
	 */
	@Transactional
	public Object payNotify(HttpServletRequest request, HttpServletResponse response) {

		logger.error("-------------------积分商城微信付款成功或失败回调接口------------------------");

		MallOrderNotifyLog log = new MallOrderNotifyLog();
		String xmlResult = null;
		try {
			xmlResult = IOUtils.toString(request.getInputStream(), request.getCharacterEncoding());
			log.setWxParams(xmlResult);

		} catch (IOException e) {
			e.printStackTrace();
			try {
				log.setErrMsg("微信通知参数解析异常：" + e.getMessage());
				log.setIsSuccess("0F");
				logService.add(log);
			} catch (Exception e1) {
				logger.error("日志添加异常: "+e1.getMessage(),e1);
			}
			return WxPayNotifyResponse.fail(e.getMessage());
		}

		WxPayOrderNotifyResult result = null;
		try {
//			result = wxPayService.parseOrderNotifyResult(xmlResult);
			result = hsWxPayService.parseOrderNotifyResult(xmlResult);

			if (!WxPayConstants.ResultCode.SUCCESS.equals(result.getResultCode())) {
				logger.error(xmlResult);
				throw new WxPayException("微信通知支付失败！");
			}
			if (!WxPayConstants.ResultCode.SUCCESS.equals(result.getReturnCode())) {
				logger.error(xmlResult);
				throw new WxPayException("微信通知支付失败！");
			}
		} catch (WxPayException e) {
			try {
				log.setErrMsg("微信通知参数解析异常：" + e.getMessage());
				log.setIsSuccess("0F");
				logService.add(log);
			} catch (Exception e1) {
				logger.error("日志添加异常: "+e1.getMessage(),e1);
			}
			e.printStackTrace();
			return WxPayNotifyResponse.fail(e.getMessage());
		}

		logger.error("处理腾讯支付平台的订单支付");
		logger.error(result);

		String orderSn = result.getOutTradeNo();
		String payId = result.getTransactionId();
		log.setOrderSn(orderSn);

		// 分转化成元
		String totalFee = BaseWxPayResult.fenToYuan(result.getTotalFee());
		MallOrder order = orderService.findBySn(orderSn);
		try {
			if (order == null) {
				try {
					log.setErrMsg("订单不存在 sn=" + orderSn);
					log.setIsSuccess("0F");
					logService.add(log);
				} catch (Exception e1) {
					logger.error("日志添加异常: "+e1.getMessage(),e1);
				}
				return WxPayNotifyResponse.fail("订单不存在 sn=" + orderSn);
			}

			// 检查这个订单是否已经处理过
			if (OrderUtil.isPayStatus(order) && order.getPayId() != null) {
				try {
					log.setErrMsg("订单已经处理过");
					log.setIsSuccess("0F");
					logService.add(log);
				} catch (Exception e1) {
					logger.error("日志添加异常: "+e1.getMessage(),e1);
				}
				return WxPayNotifyResponse.success("订单已经处理成功!");
			}

			// 检查支付订单金额
			if (!totalFee.equals(order.getActualPrice().toString())) {
				try {
					log.setErrMsg(order.getOrderSn() + " : 支付金额不符合 totalFee=" + totalFee);
					log.setIsSuccess("0F");
					logService.add(log);
				} catch (Exception e1) {
					logger.error("日志添加异常: "+e1.getMessage(),e1);
				}
				return WxPayNotifyResponse.fail(order.getOrderSn() + " : 支付金额不符合 totalFee=" + totalFee);
			}

//			this.calFenXiao(order,true,true);
			//如果订单是购买会员产品，那么成为会员   这里没有这个业务，先注释
//			userService.becomeVip(order);


			order.setPayId(payId);
			order.setPayTime(LocalDateTime.now());
			order.setOrderStatus(OrderUtil.STATUS_PAY);
			if (orderService.updateWithOptimisticLocker(order) == 0) {
				// 这里可能存在这样一个问题，用户支付和系统自动取消订单发生在同时
				// 如果数据库首先因为系统自动取消订单而更新了订单状态；
				// 此时用户支付完成回调这里也要更新数据库，而由于乐观锁机制这里的更新会失败
				// 因此，这里会重新读取数据库检查状态是否是订单自动取消，如果是则更新成支付状态。
				order = orderService.findBySn(orderSn);
				int updated = 0;

				if (OrderUtil.isAutoCancelStatus(order)) {
					order.setPayId(payId);
					order.setPayTime(LocalDateTime.now());
					order.setOrderStatus(OrderUtil.STATUS_PAY);

					updated = orderService.updateWithOptimisticLocker(order);
				}

				try {
					log.setRemark("订单支付通知计算完成");
					log.setIsSuccess("1F");
					logService.add(log);
				} catch (Exception e1) {
					logger.error("日志添加异常: "+e1.getMessage(),e1);
				}

				// 如果updated是0，那么数据库更新失败
				if (updated == 0) {
					return WxPayNotifyResponse.fail("更新数据已失效");
				}
			}

			//订单金额
//			BigDecimal orderPrice = order.getOrderPrice();//含积分不足抵扣
			BigDecimal orderPrice = order.getIntegralPrice();//兑换积分
			FcAccountExample fcAccountMallExample = new FcAccountExample();
			fcAccountMallExample.or().andCustomerIdEqualTo(order.getUserId().toString()).andAccountTypeEqualTo(false).andStatusEqualTo(new Short("1"));
			FcAccount fcAccountMall=  fcAccountMapper.selectOneByExample(fcAccountMallExample);
			if (null == fcAccountMall || null == fcAccountMall.getAccountId()){
				return ResponseUtil.badArgumentValue();
			}
			BigDecimal mallAmount = fcAccountMall.getIntegralAmount();//积分余额
			if(mallAmount == null){
				mallAmount = BigDecimal.ZERO;
			}
			if(orderPrice == null){
				orderPrice = BigDecimal.ZERO;
			}
			if (mallAmount.compareTo(orderPrice) < 0){
				return ResponseUtil.badArgumentValue("商城积分余额不足");
			}
			payAsUserAndMerchantService.insert(fcAccountMall,orderPrice.negate(),order.getId());//新增积分记录
			fcAccountMall.setIntegralAmount(mallAmount.subtract(orderPrice));//积分余额 = 原有积分余额 - 积分兑换金额
			//更新商户商城余额
			if (fcAccountMapper.updateByPrimaryKey(fcAccountMall)== 0) {
				return ResponseUtil.badArgumentValue("商城积分余额处理失败");
			}else{
				logger.info("-----------------------商城积分余额处理成功--------------------");
			}
//			List<MallOrderGoods> orderGoodsList = orderGoodsService.queryByOid(order.getId());
//			List<String> goodsIdList = new ArrayList<String>();
//			for (MallOrderGoods orderGoods : orderGoodsList){
//				goodsIdList.add(orderGoods.getGoodsId());
//			}
//			List<MallGoods> goodsList = goodsService.findByIdList(goodsIdList);
//			List<String> couponIdList = new ArrayList<String>();
//			for (MallGoods goods : goodsList){
//				couponIdList.add(goods.getCouponId());
//			}
//			List<MallCoupon> couponList = couponService.findByIdList(couponIdList);
//			for (MallCoupon coupon : couponList){
//				MallCouponUser couponUser = new MallCouponUser();
//				couponUser.setCouponId(coupon.getId());
//				couponUser.setUserId(order.getUserId());
//				Short timeType = coupon.getTimeType();
//				if (timeType.equals(CouponConstant666.TIME_TYPE_TIME)) {
//					couponUser.setStartTime(coupon.getStartTime());
//					couponUser.setEndTime(coupon.getEndTime());
//				}
//				else{
//					LocalDateTime now = LocalDateTime.now();
//					couponUser.setStartTime(now);
//					couponUser.setEndTime(now.plusDays(coupon.getDays()));
//				}
//				couponUser.setCouponCode(AppIdAlgorithm.generateCode(12));
//				couponUser.setMerchantId(coupon.getMerchantId());
//				couponUserService.add(couponUser);
//			}

//			logger.info("4、--------修改积分商城商品销量和库存-------begin----------");
//			//通过orderId 查询订单商品
//			List<MallOrderGoods> orderGoodsList = orderGoodsService.queryByOid(order.getId());
//			if(orderGoodsList.size() > 0){
//				for (MallOrderGoods mallOrderGoods:orderGoodsList){
//					String productId = mallOrderGoods.getProductId();
//					try {
//						JSONObject obj = goodsService.queryGoodsByProductId(productId);
//						if (!Objects.equals(null, obj)) {
//							logger.info("5、--------修改自营商品销量和库存-----------------");
//							//在开放平台goods表添加销量和product表减去库存
//							//该商品一定要属于积分商城商品
//							if (Objects.equals("3",obj.get("virtualGood") ) || 3==Integer.parseInt(String.valueOf(obj.get("virtualGood")))) {
//								Integer buyNum =Integer.parseInt(String.valueOf(mallOrderGoods.getNumber()));
//								if (!goodsService.updateZyOpenGoods(productId, buyNum)) {
//									logger.info("6、修改自营商品销量和库存失败");
//								}
//								logger.info("7、修改自营商品销量和库存成功");
//							}
//						}
//					} catch (Exception e) {
//						logger.info("8、远程修改库存和销量出现异常!");
//						e.printStackTrace();
//					}
//				}
//			}
//			logger.info("4、--------修改积分商城商品销量和库存--------end---------");

		} catch (Exception e) {
			try {
				log.setRemark("9、订单支付发生异常: " +e.getMessage());
				log.setIsSuccess("0F");
				logService.add(log);
			} catch (Exception e1) {
				logger.error("10、日志添加异常: "+e1.getMessage(),e1);
			}
			logger.error("11、支付订单通知处理异常 :" + e.getMessage(), e);
		}

		logger.info("12、积分商城异步通知业务全部处理完成！");
		return WxPayNotifyResponse.success("处理成功!");
	}


	public String checkProfit(MallOrder order){


		return null;
	}


//	public InternalProtocolMsg<Object> sheet(List<String> billIds) throws Exception{
//		InternalProtocolMsg<Object> msg =new InternalProtocolMsg<Object>();
//
//		List<FcAccountCapitalBill> bills = accountService.getCapitalBillList(billIds);
//
//		List<Integer> orderIds = new ArrayList<Integer>();
//		for (int i = 0; i < bills.size(); i++) {
//			FcAccountCapitalBill bill = bills.get(i);
//			if(bill.getOrderId()!=null && !orderIds.contains(Integer.parseInt(bill.getOrderId()))){
//				orderIds.add(Integer.parseInt(bill.getOrderId()));
//			}else{
//				System.out.println("这个没有订单啊: "+ bill.getChangeBillId());
//			}
//		}
//
//		if(CollectionUtils.isNotEmpty(orderIds)){
//			List<String> billIdList = new ArrayList<String>();
//			List<FcAccountRechargeBill> billList = accountService.getAccountRechargeBill(orderIds);
//			for (int i = 0; i < billList.size(); i++) {
//				billIdList.add(billList.get(i).getBillId());
//			}
//
//			if (CollectionUtils.isNotEmpty(billIdList)) {
//				accountService.updateAccountRechargeBillStatus(billIdList, AccountStatus.DATA_STATUS_0);
//				for (int i = 0; i < bills.size(); i++) {
//					FcAccountCapitalBill bill = bills.get(i);
//					FcAccount primaryUserAccount = accountService.getAccount(bill.getAccountId());
//					accountService.refundAccountOperation(primaryUserAccount, Integer.parseInt(bill.getOrderId()), bill.getAtlTradeAmount());
//				}
//
//				for (int i = 0; i < orderIds.size(); i++) {
//					Integer orderId = orderIds.get(i);
//					MallOrder order = orderService.findById(orderId);
////					this.calFenXiao(order,false,true);
//				}
//			}else{
//				System.out.println("不能退账,充值记录不存在，orderId：");
//			}
//
//		}else{
//			System.out.println("订单是个空的.....");
//		}
//
//		msg.setErrno(ResponseUtil.SUCCESS);
//		msg.setErrmsg("计算成功....");
//		return msg;
//	}

//	public void supplementSheet(String body){
//    	String orderSn = JacksonUtil.parseString(body, "orderSn");
//    	Integer userId = JacksonUtil.parseInteger(body, "userId");
//
////		String channel = JacksonUtil.parseString(body, "channel");
//
//    	List<Short> status = new ArrayList<Short>();
//		status.add(OrderUtil.STATUS_PAY);
//		status.add(OrderUtil.STATUS_CONFIRM);
//		status.add(OrderUtil.STATUS_SHIP);
//		status.add(OrderUtil.STATUS_AUTO_CONFIRM);
//
//		List<MallOrder> orders = null;
//
//		if(StringUtils.isNotBlank(orderSn)){
//			orders = new ArrayList<MallOrder>();
//			MallOrder order = orderService.findBySn(orderSn);
//			orders.add(order);
//		}else{
//			//orders  = orderService.queryByOrderStatus(userId, status, null, null);
//		}
//
//		try {
//			System.out.println("查询出所有的数据 orders:" + JacksonUtils.bean2Jsn(orders));
//			if (CollectionUtils.isNotEmpty(orders)) {
//				for (int ix = 0; ix < orders.size(); ix++) {
//					MallOrder order = orders.get(ix);
//					//订单状态在可以发起退账的状态内
//					if(status.contains(order.getOrderStatus())){
//						System.out.println("order.getOrderStatus() :" + order.getOrderStatus());
//						//先退账
//						InternalProtocolMsg<Object>  msg = this.manualRefund(order.getId());
//						if(ResponseUtil.SUCCESS.equals(msg.getErrno())){
//							//重新计算订单分润金额  第二个true 代表是否充值
////							this.calFenXiao(order,false,true);
//						}else{
//							System.out.println("退账结果： " + JacksonUtils.bean2Jsn(msg));
//						}
//					}else{
//						System.out.println("订单状态不满足发起手动退账数据...");
//					}
//
//
//				}
//			}
//		} catch (JsonParseException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (JsonMappingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}


	public void sendSms(MallOrder order, String body,
			MallUser fenXiaoUser, String smsContent2fenxiao) {
		//body+= "【收件人: "+order.getConsignee() + ",电话: "+ order.getMobile() + ",地址: "+ order.getAddress()+"】";
		String[] params = { order.getOrderSn(), order.getActualPrice().toString(), body };

		int templateId = 353450;
		// 需要发送短信的手机号码
		String[] phoneNumbers = { "13924172773", "18905740203" };
		//String[] phoneNumbers = { "15920880905" ,"18998828258" };
		// 订单支付成功通知 有新的支付订单来了！ 单号：{1} 金额：{2} 商品：{3}
		// 参数1 订单
		// 参数2 价格
		// 参数2 【商品名称;商品规格数组;数量;原系统产品ID】【收件人;电话;地址】
		TecentSmsUtil.sendForTemplate(phoneNumbers, templateId, params);

		if(fenXiaoUser!=null){
			String[] fenxiaoPhoneNumbers = { fenXiaoUser.getMobile() };
			String[] params2 = { order.getOrderSn(), order.getActualPrice().toString(),order.getConsignee(), smsContent2fenxiao };
			int noteSaleMantemplateId = 364271;
			//有新的支付订单来了！ 订单号：{1}金额：{2} 收件人 ：{3} 商品：{4}
			TecentSmsUtil.sendForTemplate(fenxiaoPhoneNumbers, noteSaleMantemplateId, params2);
		}
	}

	private void submitOrderToWpc(MallOrder order, List<MallOrderGoods> orderGoodsList){
		boolean isWpcGoodsFlag = false; //是否唯品仓商品

		Map<String,Integer> sizeInfo = new HashMap<String,Integer>();
		// 添加订单商品表项
		for (MallOrderGoods orderGoods : orderGoodsList) {
			// 订单商品
			MallGoods goods = goodsService.selectOneById(orderGoods.getGoodsId());
			if("WPC".equals(goods.getChannelCode())){
				isWpcGoodsFlag = true;
				MallGoodsProduct product = productService.findById(orderGoods.getProductId());
				sizeInfo.put(product.getSrcSizeId(), (int)orderGoods.getNumber()); //key为唯品仓对应的sizeId
			}
		}
		if(isWpcGoodsFlag){
			WpcOrderReqVo vo = new WpcOrderReqVo();

			vo.setSizeInfo(sizeInfo);

			vo.setProvinceName(order.getProvinceName());
			vo.setCityName(order.getCityName());
			vo.setAreaName(order.getAreaName());
//			vo.setTownName(null);
			vo.setAddress(order.getAddressDetail());
			vo.setConsignee(order.getConsignee());
			vo.setMobile(order.getMobile());
			vo.setTraceId(order.getId().toString());
			vo.setClientIp(order.getClientIp());

			try {
				WpcApiRespDto respDto = wpcApiService.createOrder(vo);
				if("200".equals(respDto.getCode())){
					//提交订单成功
					if(respDto.getData()!=null && respDto.getData() instanceof Map){
						Map<String,Object> retData = (Map)respDto.getData();
						if(retData.containsKey("orders")){
							List<Map<String,Object>> srcOrders =
									(List<Map<String,Object>>)retData.get("orders");
							if(srcOrders!=null&&srcOrders.size()>0){
								StringBuffer srcOrdersStr = new StringBuffer();
								for(Map<String,Object> srcOrder:srcOrders){
									srcOrdersStr.append(MapUtils.getString(srcOrder, "orderSn"));
								}

								MallOrder updateOrdervo = new MallOrder();
								updateOrdervo.setSrcOrderIds(srcOrdersStr.toString());
								updateOrdervo.setChannelCode("WPC");
								updateOrdervo.setId(order.getId());
								orderService.updateByPrimaryKeySelective(updateOrdervo);
							}
						}
					}
				}else if("600008".equals(respDto.getCode())){
					logger.info("唯品仓接口调用失败,"+respDto.getMsg()+";订单ID:"+order.getId());
					throw new RuntimeException("库存不足!");
				}else{
					logger.info("唯品仓接口调用结果:"+ JacksonUtils.bean2Jsn(respDto));
					throw new RuntimeException("库存不足!");
				}
//			} catch (ServiceException e) {
//				logger.error("提交唯品仓接口报错:"+body, e);
//				throw new RuntimeException("库存不足");
			} catch (Exception e) {
				logger.error("提交唯品仓接口报错:orderId:"+order.getId(), e);
				throw new RuntimeException("库存不足!");
			}
		}
	}

	/**
	 * @param order
	 * @param isSend
	 * @throws Exception
	 */
	public void calFenXiaoBak(MallOrder order,boolean isSend,boolean isRecharge) throws Exception {

		/**直接分润最终累计值**/
		BigDecimal primaryDividend = new BigDecimal(0);
		/**间接分润最终累计值**/
		BigDecimal secondaryDividend= new BigDecimal(0);
		/**订单加价分润最终累计值**/
		BigDecimal markupDividend = new BigDecimal(0);

		MallRebateRecord rebat = null;
		/**订单加价金额分利**/
		BigDecimal shAddAmount = null;
		/**订单直接分利**/
		BigDecimal getR1amount	= null;
		/**订单间接分利**/
		BigDecimal getR2amount	= null;
		// 有分销员ID的时候订单需要计算分销金额
		String tpid = order.getTpid();
		String shId = order.getShId();

		String body = "";
		String smsContent2fenxiao = "";
		MallUser fenXiaoUser = null;
		/** 先查询商品 **/
		List<MallOrderGoods> orderGoods = orderGoodsService.queryByOid(order.getId());

		/**查询商品购买人**/
		MallUser user = userService.findById(order.getUserId());
		int loop = CollectionUtils.isNotEmpty(orderGoods) ? orderGoods.size() : 0;
		List<String> goodIds = new ArrayList<String>();
		//购买的产品Id集合
		List<String> productIds = new ArrayList<String>();

		for (int i = 0; i < loop; i++) {
			MallOrderGoods orderGood = orderGoods.get(i);


			body += "【商品: " + orderGood.getGoodsName() + ",数量: " + orderGood.getNumber() +
					",规格: " +Arrays.toString(orderGood.getSpecifications())+ ",原系统产品ID: "
					+ orderGood.getSrcGoodId()+"】";
			smsContent2fenxiao += "【商品: " + orderGood.getGoodsName() + ",数量: " + orderGood.getNumber() +
					",规格: " +Arrays.toString(orderGood.getSpecifications())+"】";

			String goodId = orderGood.getGoodsId() != null ? orderGood.getGoodsId() : "0";

			String productId = orderGood.getProductId() != null ? orderGood.getProductId() :"0" ;
			productIds.add(productId);
			goodIds.add(goodId);
		}

		/*** 取出订单的所有产品 **/
		List<MallGoods> goods = goodsService.findByIdList(goodIds);

		//List<MallGoodsProduct> products = productService.findByIds(productIds);
		int _loop = CollectionUtils.isNotEmpty(goods) ? goods.size() : 0;
		Short number = 1;
		FcAccount fenXiaoUserAccount = null;

		if (StringUtils.isBlank(tpid)) {
			tpid = order.getUserId().toString();
		}

		/********************************* 计算单个商品的返利数据 *********************************************/
		for (int i = 0; i < _loop; i++) {
			//MallGoodsProduct _product = products.get(i);

			MallGoods _goods = goods.get(i);
			/** 单个商品如果没有设置分润比例则默认给 ***/
			BigDecimal tcRetailPrice = _goods.getRetailPrice();
			BigDecimal firstRatio = _goods.getFirstRatio() != null ? _goods.getFirstRatio() : new BigDecimal(0.1);
			BigDecimal secondRatio = _goods.getSecondRatio() != null ? _goods.getSecondRatio() : new BigDecimal(0.05);
			String calModel = _goods.getCalModel() != null ? _goods.getCalModel() : "1R";

			String productId = "0";
			BigDecimal couponPrice = null;
			// 分润比例不为0的时候才记入充值
			if (firstRatio.compareTo(new BigDecimal(0)) != 0) {
				//寻找订单购物的数量
				for (int j = 0; j < loop; j++) {
					MallOrderGoods orderGood = orderGoods.get(j);
					if(_goods.getId()!=null && _goods.getId().equals(orderGood.getGoodsId())){
						number = orderGood.getNumber();
						productId = orderGood.getProductId();

						//优惠金额
						couponPrice =  orderGood.getCouponPrice();
					}
				}

				if(!"0".equals(productId)){
					MallGoodsProduct product = productService.findById(productId);
					//实际要取产品的金额
					tcRetailPrice = product.getPrice();
					if(couponPrice!=null){
						tcRetailPrice = tcRetailPrice.subtract(couponPrice);
					}

					/*************计算订单加价部分金额*************/
					if (shId != null) {
						// 取到加价规则
						if(rebat==null){
							rebat = fenXiaoService.getMallRebateRecordById(shId);
						}

						//说明用户是会员，那么支付的金额打折(9.5折)同样订单加价部分分润9.5折
						if(MallConstant.USER_LEVEL_VIP==user.getUserLevel()){
							shAddAmount = GoodsUtils.getAddAmount(rebat, tcRetailPrice).multiply(new BigDecimal(0.95)).setScale(2, BigDecimal.ROUND_HALF_UP);
						}else{
							shAddAmount = GoodsUtils.getAddAmount(rebat, tcRetailPrice);
						}
						// 如果非等于0的话就说明加价了，需要写入加价部分给分销商
						//订单加价累计
						markupDividend = markupDividend.add(shAddAmount);
					}

				}

				// 通过比例的方式计算分润
				if ("1R".equals(calModel)) {
					// 计算比例
					BigDecimal calamount1 = tcRetailPrice.multiply(firstRatio).setScale(2, BigDecimal.ROUND_HALF_UP);
					BigDecimal calamount2	= tcRetailPrice.multiply(secondRatio).setScale(2, BigDecimal.ROUND_HALF_UP);
					 getR1amount = calamount1.multiply(new BigDecimal(number)).setScale(2, BigDecimal.ROUND_HALF_UP);
					 getR2amount = calamount2.multiply(new BigDecimal(number)).setScale(2, BigDecimal.ROUND_HALF_UP);
				} else {
					// 固定值
					BigDecimal calamount1 = firstRatio;
					BigDecimal calamount2 = secondRatio;
					getR1amount = calamount1.multiply(new BigDecimal(number)).setScale(2, BigDecimal.ROUND_HALF_UP);
					getR2amount = calamount2.multiply(new BigDecimal(number)).setScale(2, BigDecimal.ROUND_HALF_UP);
				}

				//如果分销id为空那么自己就是分销员


				if (fenXiaoUser == null) {
					fenXiaoUser = userService.findById(tpid);
				}

				// 上级分销员
				String firstLeader = fenXiaoUser.getFirstLeader();
				Map<String, Object> paramsMap = new HashMap<String, Object>();
				paramsMap.put("channel", "wx");
				paramsMap.put("orderId", order.getId());
				paramsMap.put("orderSn", order.getOrderSn());
				paramsMap.put("buyerId", order.getUserId());
				paramsMap.put("goodsId", _goods.getId());
				/** 计算一级分销的分润金额 计算本分销员的收益 **/
				fenXiaoUserAccount = accountService.getAccountByKey(tpid);
				if (fenXiaoUserAccount != null) {
					paramsMap.put("dType", MallConstant.DISTRIBUTION_TYPE_00);
					FcAccount rechargeAccount1 = new FcAccount();
					rechargeAccount1.setAccountId(fenXiaoUserAccount.getAccountId());
					rechargeAccount1.setAmount(getR1amount);

					primaryDividend = primaryDividend.add(getR1amount);
					if(isRecharge){
						accountService.accountTradeRecharge(fenXiaoUserAccount, rechargeAccount1, paramsMap,"");
					}

				}

				/** 计算二级分销的分润金额 **/
				FcAccount account2 = accountService.getAccountByKey(firstLeader);
				if (account2 != null) {
					paramsMap.put("dType", MallConstant.DISTRIBUTION_TYPE_01);
					FcAccount rechargeAccount2 = new FcAccount();
					rechargeAccount2.setAccountId(account2.getAccountId());
					rechargeAccount2.setAmount(getR2amount);

					secondaryDividend = secondaryDividend.add(getR2amount);
					if(isRecharge){
						accountService.accountTradeRecharge(account2, rechargeAccount2, paramsMap,"");
					}
				}
				logger.info("计算商品分销金额成功...................");
			}

		}

		/***************************计算订单加价分润*********************************/
		for (int i = 0; i < _loop; i++) {
			//MallGoodsProduct _product = products.get(i);
			MallGoods _goods = goods.get(i);
			//BigDecimal tcRetailPrice = _goods.getRetailPrice();

		}

		System.out.println("markupDividend计算出的订单加价部分 :" + markupDividend.toString());

		if (markupDividend.compareTo(new BigDecimal(0)) != 0) {
			// 加价部分全部算给一级分销商自己
			fenXiaoUserAccount = accountService.getAccountByKey(tpid);
			Map<String, Object> paramsMap = new HashMap<String, Object>();
			paramsMap.put("channel", "wx");
			paramsMap.put("orderId", order.getId());
			paramsMap.put("orderSn", order.getOrderSn());
			paramsMap.put("buyerId", order.getUserId());
			paramsMap.put("dType", "D11");

			System.out.println("查询出账户fenXiaoUserAccount "+ JacksonUtils.bean2Jsn(fenXiaoUserAccount));
			if (fenXiaoUserAccount != null) {
				FcAccount rechargeAccount1 = new FcAccount();
				rechargeAccount1.setAccountId(fenXiaoUserAccount.getAccountId());
				rechargeAccount1.setAmount(markupDividend);
				if(isRecharge){
					accountService.accountTradeRecharge(fenXiaoUserAccount, rechargeAccount1, paramsMap,"");
				}
			}
			logger.info("计算订单分销金额成功...................");
		}

		/********************************* 每个商品的返利金额计算结束 *********************************************/

		/********************************* 计算整个订单的加价部分 (2019/7/4 重写订单分润逻辑，从产品中计算，设计到会员折扣的问题)***************************************
		if (shId != null) {
			BigDecimal actualPrice = order.getActualPrice();
			// 取到加价规则
			MallRebateRecord rebat = fenXiaoService.getMallRebateRecordById(shId);
			shAddAmount = GoodsUtils.getAddAmount(rebat, actualPrice);
			// 如果非等于0的话就说明加价了，需要写入加价部分给分销商
			if (shAddAmount.compareTo(new BigDecimal(0)) != 0) {
				// 加价部分全部算给一级分销商自己
				fenXiaoUserAccount = accountService.getAccountByKey(tpid);
				Map<String, Object> paramsMap = new HashMap<String, Object>();
				paramsMap.put("channel", "wx");
				paramsMap.put("orderId", order.getId());
				paramsMap.put("orderSn", order.getOrderSn());
				paramsMap.put("buyerId", order.getUserId());
				paramsMap.put("dType", "D11");

				if (fenXiaoUserAccount != null) {
					FcAccount rechargeAccount1 = new FcAccount();
					rechargeAccount1.setAccountId(fenXiaoUserAccount.getAccountId());
					rechargeAccount1.setAmount(shAddAmount);
					accountService.accountTradeRecharge(fenXiaoUserAccount, rechargeAccount1, paramsMap);
				}
				logger.info("计算订单分销金额成功...................");
			}
		}******/

		/********************************* 计算整个订单的加价部分结束 *********************************************/


		/********************************* 计算完成将计算后的金额写入订单中 *********************************************/
		MallOrder uOrder = new MallOrder();
		uOrder.setId(order.getId());
		uOrder.setPrimaryDividend(primaryDividend);
		uOrder.setSecondaryDividend(secondaryDividend);
		uOrder.setMarkupDividend(markupDividend);
		uOrder.setIsCal("1");
		orderService.updateByPrimaryKeySelective(uOrder);

		//是否发送通知
		if(isSend){
			//支付订单通知
			try {
				sendSms(order, body, fenXiaoUser, smsContent2fenxiao);
			} catch (Exception e) {
				logger.error("",e);
			}
			try {
				submitOrderToWpc(order, orderGoods);
			} catch (Exception e) {
				logger.error("",e);
			}
		}
	}


	private static BigDecimal ZERO = new BigDecimal(0);
	/**
	 * @param order
	 * @param isSend
	 * @throws Exception
	 */
	public void calFenXiao(MallOrder order,boolean isSend,boolean isRecharge) throws Exception {
		/**直接分润最终累计值**/
		BigDecimal primaryDividend = new BigDecimal(0);
		/**间接分润最终累计值**/
		BigDecimal secondaryDividend= new BigDecimal(0);
		/**订单加价分润最终累计值**/
		BigDecimal markupDividend = new BigDecimal(0);

		/**分销员账户**/
		FcAccount primaryUserAccount = null;
		FcAccount secondaryUserAccount = null;

		MallRebateRecord rebat = null;

		// 有分销员ID的时候订单需要计算分销金额
		String shId = order.getShId();

		String body = "";
		String smsContent2fenxiao = "";
		MallUser fenXiaoUser = null;
		/** 先查询商品 **/
		List<MallOrderGoods> orderGoods = orderGoodsService.queryByOid(order.getId());

		/**查询商品购买人**/
		MallUser user = userService.findById(order.getUserId());

		int loop = CollectionUtils.isNotEmpty(orderGoods) ? orderGoods.size() : 0;
		List<String> goodIds = new ArrayList<String>();
		//购买的产品Id集合
		List<String> productIds = new ArrayList<String>();

		for (int i = 0; i < loop; i++) {
			MallOrderGoods orderGood = orderGoods.get(i);
			String goodId = orderGood.getGoodsId() != null ? orderGood.getGoodsId() : "0";
			String productId = orderGood.getProductId() != null ? orderGood.getProductId() :"0" ;
			productIds.add(productId);
			goodIds.add(goodId);
		}

		/*** 取出订单的所有产品 **/
		List<MallGoods> goods = goodsService.findByIdList(goodIds);
		/**获取所有的SKU数据***/
		List<MallGoodsProduct> products = productService.findByIds(productIds);

		int _loop = CollectionUtils.isNotEmpty(products) ? products.size() : 0;
		String tpid = StringUtils.isBlank(order.getTpid()) ? order.getUserId().toString() : order.getTpid() ;


		/********************************* 计算单个商品的返利数据 *********************************************/
		for (int i = 0; i < _loop; i++) {
			MallGoodsProduct _product = products.get(i);
			MallGoods _goods = OrderCalculationUtil.getGoods(goods, _product);

			/** 单个商品如果没有设置分润比例则默认给0.1和0.05 ***/
			BigDecimal tcRetailPrice = _product.getPrice();
			BigDecimal firstRatio = _goods.getFirstRatio() != null ? _goods.getFirstRatio() : new BigDecimal(0.1);
			BigDecimal secondRatio = _goods.getSecondRatio() != null ? _goods.getSecondRatio() : new BigDecimal(0.05);
			String calModel = _goods.getCalModel() != null ? _goods.getCalModel() : "1R";

			// 分润比例不为0的时候才记入充值
			if (firstRatio.compareTo(ZERO) != 0 && secondRatio.compareTo(ZERO) != 0) {
				/**获取sku购买数量**/
				Short number = OrderCalculationUtil.getBuyNumber(orderGoods, _product);
				/**计算优惠后的金额**/
				tcRetailPrice = OrderCalculationUtil.getAmountAfterPreference(tcRetailPrice, orderGoods, _product);

				/**直接分润**/
				BigDecimal getR1amount = OrderCalculationUtil.getAmountOfShare(calModel, tcRetailPrice, number, firstRatio);
				/**间接分润**/
				BigDecimal getR2amount = OrderCalculationUtil.getAmountOfShare(calModel, tcRetailPrice, number, secondRatio);

				/**累加直接分润总金额**/
				primaryDividend = primaryDividend.add(getR1amount);
				/**累加间接分润总金额**/
				secondaryDividend = secondaryDividend.add(getR2amount);

				/**直接分销员**/
				fenXiaoUser = fenXiaoUser!=null ? fenXiaoUser : userService.findById(tpid);
				/**间接分销员**/
				String firstLeader = fenXiaoUser.getFirstLeader();

				/** 直接分润账户产品分润充值 **/
				primaryUserAccount = accountService.getAccountByKey(tpid);
				OrderCalculationUtil.shareAmountRecharge(MallConstant.DISTRIBUTION_TYPE_00, accountService,
						primaryUserAccount, order, _goods, _product, getR1amount, isRecharge);

				/** 间接分润账户充值 **/
				secondaryUserAccount = accountService.getAccountByKey(firstLeader);
				OrderCalculationUtil.shareAmountRecharge(MallConstant.DISTRIBUTION_TYPE_01, accountService,
						secondaryUserAccount, order, _goods, _product, getR2amount, isRecharge);
			}

			/**自定义加价分润部分,里面包含扣除VIP折扣部分**/
			rebat = rebat!=null ? rebat : fenXiaoService.getMallRebateRecordById(shId);
			BigDecimal getShAmount = OrderCalculationUtil.getCustomerAddAmount(fenXiaoService, tcRetailPrice, rebat, user);
			/**累加自定义加价总金额**/
			markupDividend = markupDividend.add(getShAmount);

			/** 直接分润账户自定义加价部分充值 **/
			primaryUserAccount = primaryUserAccount!=null ? primaryUserAccount : accountService.getAccountByKey(tpid);
			OrderCalculationUtil.shareAmountRecharge(MallConstant.DISTRIBUTION_TYPE_11, accountService,
					primaryUserAccount, order, _goods, _product, getShAmount, isRecharge);
		}
		/********************************* 计算整个订单的加价部分结束 *********************************************/


		/********************************* 计算完成将计算后的金额写入订单中 *********************************************/
		MallOrder uOrder = new MallOrder();
		uOrder.setId(order.getId());
		uOrder.setPrimaryDividend(primaryDividend);
		uOrder.setSecondaryDividend(secondaryDividend);
		uOrder.setMarkupDividend(markupDividend);
		uOrder.setIsCal("1");
		orderService.updateByPrimaryKeySelective(uOrder);

		//是否发送通知
		if(isSend){
			for (int i = 0; i < loop; i++) {
				MallOrderGoods orderGood = orderGoods.get(i);
				body += "【商品: " + orderGood.getGoodsName() + ",数量: " + orderGood.getNumber() +
						",规格: " +Arrays.toString(orderGood.getSpecifications())+ ",原系统产品ID: "
						+ orderGood.getSrcGoodId()+"】";
				smsContent2fenxiao += "【商品: " + orderGood.getGoodsName() + ",数量: " + orderGood.getNumber() +
						",规格: " +Arrays.toString(orderGood.getSpecifications())+"】";
			}
			//支付订单通知
			try {
				sendSms(order, body, fenXiaoUser, smsContent2fenxiao);
			} catch (Exception e) {
				logger.error("",e);
			}
			try {
				submitOrderToWpc(order, orderGoods);
			} catch (Exception e) {
				logger.error("",e);
			}
		}
	}

	public static void main(String[] args) {
		BigDecimal secondRatio = new BigDecimal(0);
		System.out.println(secondRatio.compareTo(ZERO) != 0);
	}
	/**
	 * 订单申请退款
	 * <p>
	 * 1. 检测当前订单是否能够退款； 2. 设置订单申请退款状态。
	 *
	 * @param userId
	 *            用户ID
	 * @param body
	 *            订单信息，{ orderId：xxx }
	 * @return 订单退款操作结果
	 * @throws Exception
	 */
	public Object refund(String userId, String body) throws Exception {
		if (userId == null) {
			return ResponseUtil.unlogin();
		}
		String orderId = JacksonUtil.parseString(body, "orderId");
		if (orderId == null) {
			return ResponseUtil.badArgument();
		}

		MallOrder order = orderService.findById(orderId);
		if (order == null) {
			return ResponseUtil.badArgument();
		}
		if (!order.getUserId().equals(userId)) {
			return ResponseUtil.badArgumentValue();
		}

		OrderHandleOption handleOption = OrderUtil.build(order);
		if (!handleOption.isRefund()) {
			return ResponseUtil.fail(ORDER_INVALID_OPERATION, "订单不能取消");
		}

		// TODO 发送邮件和短信通知，这里采用异步发送
		// 有用户申请退款，邮件通知运营人员
		//notifyService.notifyMail("退款申请", order.toString());


		/*********上面部分完全可以为C端电商退单逻辑，下面增加分销商退款逻辑*********/
		accountService.refund(MallConstant.REFUND_TYPE_ALL,userId,null, orderId, order);


		// 设置订单申请退款状态
		order.setOrderStatus(OrderUtil.STATUS_REFUND);
		if (orderService.updateWithOptimisticLocker(order) == 0) {
			return ResponseUtil.updatedDateExpired();
		}

		return ResponseUtil.ok();
	}

//	public InternalProtocolMsg<Object> manualRefund(Integer orderId) throws Exception {
//		InternalProtocolMsg<Object> msg = new InternalProtocolMsg<Object>();
//		MallOrder order = orderService.findById(orderId);
//		if (order == null) {
//			//return ResponseUtil.badArgument();
//			msg.setErrno(ResponseUtil.FAIL);
//			msg.setErrmsg("订单查询不到");
//			return msg;
//		}
//
//		/*********上面部分完全可以为C端电商退单逻辑，下面增加分销商退款逻辑*********/
//		msg = accountService.refund(MallConstant.REFUND_TYPE_ALL, null,null, orderId, order);
//		// 设置订单申请退款状态
//		order.setOrderStatus(OrderUtil.STATUS_REFUND);
//		if (orderService.updateWithOptimisticLocker(order) == 0) {
//			msg.setErrno(ResponseUtil.FAIL);
//			msg.setErrmsg("更新数据失效");
//			return msg;
//		}
//		return msg;
//	}

	/**
	 * 确认收货
	 * <p>
	 * 1. 检测当前订单是否能够确认收货； 2. 设置订单确认收货状态。
	 *
	 * @param body
	 *            订单信息，{ orderId：xxx, mobile:xxx }
	 * @return 订单操作结果
	 */
	public Object confirm(String body) {

		//判断用户信息
		String mobile = JacksonUtil.parseString(body, "mobile");
		if (mobile == null || "".equals(mobile)) {
			return ResponseUtil.fail(501,"手机号码不能为空!");
		}
		String userId="";
		List<MallUser> users = userService.queryByMobile(mobile);
		if (CollectionUtils.isNotEmpty(users)) {
			if (org.apache.commons.lang3.StringUtils.isNotEmpty(users.get(0).getId())) {
				userId = users.get(0).getId();
			}
		}else {
			logger.info("1、用户不存在!");
			return ResponseUtil.fail(501,"用户不存在!");
		}
		if (userId == null || userId.equals("")) {
			logger.info("2、用户不存在!userId为空！");
			return ResponseUtil.unlogin();
		}

		String orderId = JacksonUtil.parseString(body, "orderId");
		if (orderId == null || "".equals(orderId)) {
			//return ResponseUtil.badArgument();
			logger.info("3、订单编号不能为空!");
			return ResponseUtil.fail(601,"订单编号不能为空!");
		}

		MallOrder order = orderService.findById(orderId);
		if (order == null) {
			return ResponseUtil.badArgument();
		}
		if (!order.getUserId().equals(userId)) {
			return ResponseUtil.badArgumentValue();
		}

		OrderHandleOption handleOption = OrderUtil.build(order);
		if (!handleOption.isConfirm()) {
			return ResponseUtil.fail(ORDER_INVALID_OPERATION, "订单不能确认收货");
		}

		Short comments = orderGoodsService.getComments(orderId);
		order.setComments(comments);

		order.setOrderStatus(OrderUtil.STATUS_CONFIRM);
		order.setConfirmTime(LocalDateTime.now());
		if (orderService.updateWithOptimisticLocker(order) == 0) {
			return ResponseUtil.updatedDateExpired();
		}
		return ResponseUtil.ok();
	}

	/**
	 * 删除订单
	 * <p>
	 * 1. 检测当前订单是否可以删除； 2. 删除订单。
	 *
	 * @param userId
	 *            用户ID
	 * @param body
	 *            订单信息，{ orderId：xxx }
	 * @return 订单操作结果
	 */
	public Object delete(String userId, String body) {
		if (userId == null) {
			return ResponseUtil.unlogin();
		}
		String orderId = JacksonUtil.parseString(body, "orderId");
		if (orderId == null) {
			return ResponseUtil.badArgument();
		}

		MallOrder order = orderService.findById(orderId);
		if (order == null) {
			return ResponseUtil.badArgument();
		}
		if (!order.getUserId().equals(userId)) {
			return ResponseUtil.badArgumentValue();
		}

		OrderHandleOption handleOption = OrderUtil.build(order);
		if (!handleOption.isDelete()) {
			return ResponseUtil.fail(ORDER_INVALID_OPERATION, "订单不能删除");
		}

		// 订单order_status没有字段用于标识删除
		// 而是存在专门的delete字段表示是否删除
		orderService.deleteById(orderId);

		return ResponseUtil.ok();
	}

	/**
	 * 待评价订单商品信息
	 *
	 * @param userId
	 *            用户ID
	 * @param orderId
	 *            订单ID
	 * @param goodsId
	 *            商品ID
	 * @return 待评价订单商品信息
	 */
	public Object goods(String userId, String orderId, String goodsId) {
		if (userId == null) {
			return ResponseUtil.unlogin();
		}

		List<MallOrderGoods> orderGoodsList = orderGoodsService.findByOidAndGid(orderId, goodsId);
		int size = orderGoodsList.size();

		Assert.state(size < 2, "存在多个符合条件的订单商品");

		if (size == 0) {
			return ResponseUtil.badArgumentValue();
		}

		MallOrderGoods orderGoods = orderGoodsList.get(0);
		return ResponseUtil.ok(orderGoods);
	}

	/**
	 * 评价订单商品
	 * <p>
	 * 确认商品收货或者系统自动确认商品收货后7天内可以评价，过期不能评价。
	 *
	 * @param userId
	 *            用户ID
	 * @param body
	 *            订单信息，{ orderId：xxx }
	 * @return 订单操作结果
	 */
	public Object comment(String userId, String body) {
		if (userId == null) {
			return ResponseUtil.unlogin();
		}

		String orderGoodsId = JacksonUtil.parseString(body, "orderGoodsId");
		if (orderGoodsId == null) {
			return ResponseUtil.badArgument();
		}
		MallOrderGoods orderGoods = orderGoodsService.findById(orderGoodsId);
		if (orderGoods == null) {
			return ResponseUtil.badArgumentValue();
		}
		String orderId = orderGoods.getOrderId();
		MallOrder order = orderService.findById(orderId);
		if (order == null) {
			return ResponseUtil.badArgumentValue();
		}
		Short orderStatus = order.getOrderStatus();
		if (!OrderUtil.isConfirmStatus(order) && !OrderUtil.isAutoConfirmStatus(order)) {
			return ResponseUtil.fail(ORDER_INVALID_OPERATION, "当前商品不能评价");
		}
		if (!order.getUserId().equals(userId)) {
			return ResponseUtil.fail(ORDER_INVALID, "当前商品不属于用户");
		}
		String commentId = orderGoods.getComment();
		if ("-1".equals(commentId)) {
			return ResponseUtil.fail(ORDER_COMMENT_EXPIRED, "当前商品评价时间已经过期");
		}
		if (!"0".equals(commentId)) {
			return ResponseUtil.fail(ORDER_COMMENTED, "订单商品已评价");
		}

		String content = JacksonUtil.parseString(body, "content");
		Integer star = JacksonUtil.parseInteger(body, "star");
		if (star == null || star < 0 || star > 5) {
			return ResponseUtil.badArgumentValue();
		}
		Boolean hasPicture = JacksonUtil.parseBoolean(body, "hasPicture");
		List<String> picUrls = JacksonUtil.parseStringList(body, "picUrls");
		if (hasPicture == null || !hasPicture) {
			picUrls = new ArrayList<>(0);
		}

		// 1. 创建评价
		MallComment comment = new MallComment();
		comment.setUserId(userId);
		comment.setType((byte) 0);
		comment.setValueId(orderGoods.getGoodsId());
		comment.setStar((byte) star.shortValue());
		comment.setContent(content);
		comment.setHasPicture(hasPicture);
		comment.setPicUrls(picUrls.toArray(new String[] {}));
		commentService.save(comment);

		// 2. 更新订单商品的评价列表
		orderGoods.setComment(comment.getId());
		orderGoodsService.updateById(orderGoods);

		// 3. 更新订单中未评价的订单商品可评价数量
		Short commentCount = order.getComments();
		if (commentCount > 0) {
			commentCount--;
		}
		order.setComments(commentCount);
		orderService.updateWithOptimisticLocker(order);

		return ResponseUtil.ok();
	}

	/**
	 * 支付成功充值积分接口
	 * @param body 请求参数{"mobile":"13128572588","orderId":"20200818081878","amount":"26.00","key":"B50C090C44E735AD7DF4B220DD540873"}
	 *             判断订单号ID是否重复
	 * @return Object
	 */
	public Object pointsRechargeNotify(String body) {

		logger.error("---------------支付系统-支付成功充值积分接口请求参数："+body);
		String mobile = JacksonUtil.parseString(body, "mobile");
		String orderId = JacksonUtil.parseString(body, "orderId");
		String amount = JacksonUtil.parseString(body,"amount");
		String key = JacksonUtil.parseString(body,"key");
		if (StringUtils.isEmpty(mobile)) {
			logger.error("1、支付成功充值积分接口失败，手机号码为空");
			return ResponseUtil.badArgument();
		}
		if (StringUtils.isEmpty(amount)) {
			logger.error("2、支付成功充值积分接口失败，充值金额为空");
			return ResponseUtil.badArgument();
		}
		if(!isNumber(amount)){
			logger.error("3、支付成功充值积分接口失败，充值金额不是数字");
			return ResponseUtil.badArgument();
		}
		List<MallUser> userList = userService.queryByMobile(mobile);
		if (userList == null) {
			logger.error("4、支付成功充值积分接口失败，用户为空");
			return ResponseUtil.badArgument();
		}
		if (userList.size() <= 0) {
			logger.error("4、支付成功充值积分接口失败，用户为空");
			return ResponseUtil.badArgument();
		}
		String orderIdTemp = null;
		if (StringUtils.isNotEmpty(orderId)) {
			orderIdTemp = orderId;
		}
		if (!"B50C090C44E735AD7DF4B220DD540873".equals(key)) {
			logger.error("5、支付成功充值积分接口失败，key值不对");
			return ResponseUtil.badArgument();
		}
		BigDecimal orderBonusPoints = SystemConfig.getLitemallOrderBonusPoints();//支付返积分比例
		if (orderBonusPoints == null) {
			logger.error("6、支付成功充值积分接口失败，后台支付返积分比例未设置");
			return ResponseUtil.badArgument();
		}
		BigDecimal orderAmount = new BigDecimal(amount);

		//判断订单ID重复就拦截
		FcAccountCapitalBillExample FcAccountCapitalBillExample = new FcAccountCapitalBillExample();
		FcAccountCapitalBillExample.or().andOrderIdEqualTo(orderId).andStatusEqualTo(new Short("1")).andRechargeTypeEqualTo(AccountStatus.RECHARGE_TYPE_03);
		FcAccountCapitalBill fcAccountCapital=  fcAccountCapitalBillMapper.selectOneByExample(FcAccountCapitalBillExample);
		if (fcAccountCapital != null) {
			logger.error("7、支付成功充值积分接口失败，orderId已经重复充值"+orderId);
			return ResponseUtil.badArgument();
		}

		//订单金额
		FcAccountExample fcAccountMallExample = new FcAccountExample();
		fcAccountMallExample.or().andCustomerIdEqualTo(String.valueOf(userList.get(0).getId())).andStatusEqualTo(new Short("1"));
		FcAccount fcAccountMall=  fcAccountMapper.selectOneByExample(fcAccountMallExample);
		if (null == fcAccountMall || null == fcAccountMall.getAccountId()){
			logger.error("8、支付系统-支付成功充值积分失败，原因找不到用户："+userList.get(0).getId());
			return ResponseUtil.badArgumentValue();
//			try{
//				//特殊情况，未来卡要是没绑定没有注册账户，这里注册一下
//				fcAccountMall = accountService.accountTradeCreateAccount(String.valueOf(userList.get(0).getId()));
//			}catch (Exception ex){
//				logger.error("8、注册账户报错信息"+ex.getMessage());
//			}
		}
		//购买金额大于或等于1元才赠送金额，积分向下取整
		Map<String,Object> resultMap = new HashMap<>();
		logger.error("9.1、兑换比例前orderAmount="+orderAmount);
		if(orderAmount.compareTo(new BigDecimal("1")) == 1 || orderAmount.compareTo(new BigDecimal("1")) == 0){
			orderAmount = orderAmount.multiply(orderBonusPoints);
			logger.error("9.2、支付成功充值积分接口充值比例："+orderBonusPoints);
			logger.error("9.3、支付成功充值计算比例后："+orderAmount);
			orderAmount = orderAmount.setScale(0,BigDecimal.ROUND_DOWN);//向下取整
			logger.error("9.4、向下取整orderAmount="+orderAmount);
			BigDecimal mallAmount = fcAccountMall.getIntegralAmount(); //BigDecimal mallAmount = fcAccountMall.getAmount();
			payAsUserAndMerchantService.insert(fcAccountMall,orderAmount,orderIdTemp);
			if(mallAmount == null){
				mallAmount = BigDecimal.ZERO;
			}
			fcAccountMall.setIntegralAmount(mallAmount.add(orderAmount));//fcAccountMall.setAmount(mallAmount.add(orderAmount));
			//更新商户商城余额
			if (fcAccountMapper.updateByPrimaryKey(fcAccountMall)== 0) {
				logger.error("10、支付成功充值积分接口失败，余额处理失败");
				return ResponseUtil.badArgumentValue("余额处理失败");
			}
			logger.error("11、支付成功充值积分接口成功，充值前="+mallAmount+",充值后="+mallAmount.add(orderAmount));
			resultMap.put("orderAmount",orderAmount);
		}else{
			logger.error("12、支付成功充值积分接口失败，支付金额是"+orderAmount+"支付金额要>=1元才能送积分。");
			resultMap.put("orderAmount",0);
		}

		return ResponseUtil.ok(resultMap);
	}

	//方法一：用JAVA自带的函数[注意这里是数字int，不能判断出小数点]
	public boolean isNumeric(String str){
		for (int i = str.length();--i>=0;){
			if (!Character.isDigit(str.charAt(i))){
				return false;
			}
		}
		return true;
	}
	/*方法二：推荐，速度最快
	 * 判断是否为整数
	 * @param str 传入的字符串
	 * @return 是整数返回true,否则返回false
	 */

	public static boolean isInteger(String str) {
		Pattern pattern = Pattern.compile("^[-\\+]?[\\d]*$");
		return pattern.matcher(str).matches();
	}
	/**
	 * 判断字符串是否是金额
	 * @param str
	 * @return
	 */
	public static boolean isNumber(String str){
		Pattern pattern= Pattern.compile("^(([1-9]{1}\\d*)|([0]{1}))(\\.(\\d){0,2})?$"); // 判断小数点后2位的数字的正则表达式
		java.util.regex.Matcher match=pattern.matcher(str);
		if(match.matches()==false){
			return false;
		}
		else{
			return true;
		}
	}
}
