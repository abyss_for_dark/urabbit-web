package com.graphai.mall.wx.web.other;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.mall.db.domain.MallGoods;
import com.graphai.mall.db.domain.MallGoodsProduct;
import com.graphai.mall.db.domain.MallGoodsSpecification;
import com.graphai.mall.db.domain.MallGrouponRules;
import com.graphai.mall.db.domain.mall.GroupProductDTO;
import com.graphai.mall.db.service.goods.MallGoodsAttributeService;
import com.graphai.mall.db.service.goods.MallGoodsProductService;
import com.graphai.mall.db.service.goods.MallGoodsService;
import com.graphai.mall.db.service.goods.MallGoodsSpecificationService;
import com.graphai.mall.db.service.groupon.MallGrouponRulesService;

@RestController
@RequestMapping("/wx/groupRule")
@Validated
public class WxGroupRulesController {
    
    @Autowired
    private MallGrouponRulesService rulesService;
    
    @Autowired
    private MallGoodsService goodsService;
    
    
    @Autowired
    private MallGoodsSpecificationService specificationService;
    @Autowired
    private MallGoodsAttributeService attributeService;
    @Autowired
    private MallGoodsProductService productService;
    
    
    @PostMapping("/create")
    public Object create(@RequestBody GroupProductDTO groupProductDTO) {
        try {
            
            MallGrouponRules  grouponRules=groupProductDTO.getMallGrouponRules();
            MallGoodsSpecification[] specifications = groupProductDTO.getSpecifications();
            MallGoodsProduct[] products = groupProductDTO.getProducts();
            String goodsId = grouponRules.getGoodsId();
            MallGoods goods = goodsService.findById(goodsId);
            if (goods == null) {
                return ResponseUtil.badArgumentValue();
            }
            grouponRules.setReturnRatio(grouponRules.getReturnRatio().divide(new BigDecimal(100)));
            grouponRules.setGoodsName(goods.getName());
            grouponRules.setPicUrl(goods.getPicUrl());
            rulesService.createRules(grouponRules);
            
            // 商品规格表Mall_goods_specification
            for (MallGoodsSpecification specification : specifications) {
                specification.setGoodsId(goods.getId());
                specificationService.add(specification);
            }

            // 商品货品表Mall_product
            for (MallGoodsProduct product : products) {
                product.setGoodsId(goods.getId());
                productService.add(product);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseUtil.ok();
    }
    
    
    
    @PostMapping("/update")
    public Object update(@RequestBody GroupProductDTO groupProductDTO) {
        try {
            
            MallGrouponRules  grouponRules=groupProductDTO.getMallGrouponRules();
            MallGoodsSpecification[] specifications = groupProductDTO.getSpecifications();
            MallGoodsProduct[] products = groupProductDTO.getProducts();
            
            String goodsId = grouponRules.getGoodsId();
            MallGoods goods = goodsService.findById(goodsId);
            if (goods == null) {
                return ResponseUtil.badArgumentValue();
            }

            grouponRules.setReturnRatio(grouponRules.getReturnRatio().divide(new BigDecimal(100)));
            grouponRules.setGoodsName(goods.getName());
            grouponRules.setPicUrl(goods.getPicUrl());
          

            // 商品基本信息表Mall_goods
            if (rulesService.updateById(grouponRules)== 0) {
                return ResponseUtil.fail(500, "更新团购规则失败");
            }

            String gid = goods.getId();
            specificationService.deleteByGid(gid);
            attributeService.deleteByGid(gid);
            productService.deleteByGid(gid);
            
            
            // 商品规格表Mall_goods_specification
            for (MallGoodsSpecification specification : specifications) {
                specification.setGoodsId(goods.getId());
                specificationService.add(specification);
            }

            // 商品货品表Mall_product
            for (MallGoodsProduct product : products) {
                product.setGoodsId(goods.getId());
                productService.add(product);
            }
         
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseUtil.ok();
    }
    
    
    /**
     * 团购详情
     */
    @GetMapping("/detail")
    public Object detail(@NotNull String goodsId) {
        MallGrouponRules mallGrouponRules=null;
        Map<String, Object> data = new HashMap<>();
        try {
            List<MallGrouponRules> rules=  rulesService.queryByGoodsId(goodsId);  
            if (CollectionUtils.isNotEmpty(rules)) {
               if (!Objects.equals(null, rules.get(0).getReturnRatio())) {
                   rules.get(0).setReturnRatio(rules.get(0).getReturnRatio().multiply(new BigDecimal(100)));
             }
               mallGrouponRules= rules.get(0);
          }
            
            List<MallGoodsProduct> products = productService.queryByGid(goodsId);
            List<MallGoodsSpecification> specifications = specificationService.queryByGid(goodsId);
            
            data.put("mallGrouponRules", mallGrouponRules);
            data.put("specifications", specifications);
            data.put("products", products);
           
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseUtil.ok(data);
    }
    

}
