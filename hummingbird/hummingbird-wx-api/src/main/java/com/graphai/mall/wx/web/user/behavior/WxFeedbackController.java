package com.graphai.mall.wx.web.user.behavior;

import com.graphai.commons.util.json.JacksonUtil;
import com.graphai.id.GraphaiIdGenerator;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.graphai.commons.util.RegexUtil;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.mall.db.domain.MallFeedback;
import com.graphai.mall.db.domain.MallUser;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.service.user.behavior.MallFeedbackService;
import com.graphai.mall.wx.annotation.LoginUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 意见反馈服务
 *
 * @author Yogeek
 * @date 2018/8/25 14:10
 */
@RestController
@RequestMapping("/wx/feedback")
@Validated
public class WxFeedbackController {
    private final Log logger = LogFactory.getLog(WxFeedbackController.class);

    @Autowired
    private MallFeedbackService feedbackService;
    @Autowired
    private MallUserService userService;

    private Object validate(MallFeedback feedback) {
        String content = feedback.getContent();
        if (StringUtils.isEmpty(content)) {
            return ResponseUtil.badArgument();
        }

        String type = feedback.getFeedType();
        if (StringUtils.isEmpty(type)) {
            return ResponseUtil.badArgument();
        }

        Boolean hasPicture = feedback.getHasPicture();
        if (hasPicture == null || !hasPicture) {
            feedback.setPicUrls(new String[0]);
        }

        // 测试手机号码是否正确
        String mobile = feedback.getMobile();
        if (StringUtils.isEmpty(mobile)) {
            return ResponseUtil.badArgument();
        }
        if (!RegexUtil.isMobileExact(mobile)) {
            return ResponseUtil.badArgument();
        }
        return null;
    }

    /**
     * 添加意见反馈
     *
     * @param userId   用户ID
     * @param content 意见反馈
     * @return 操作结果
     */
    @PostMapping("/submit")
    public Object submit(@LoginUser String userId, @RequestBody String content) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }

        // 邀请人userId
        String contentStr = JacksonUtil.parseString(content, "content");

        MallFeedback feedback = new MallFeedback();
        MallUser user = userService.findById(userId);
        feedback.setId(GraphaiIdGenerator.nextId("MallFeedback"));
        feedback.setUserId(userId);
        feedback.setUsername(user.getUsername());
        feedback.setMobile(user.getMobile());
        feedback.setContent(contentStr);
        feedbackService.add(feedback);

        return ResponseUtil.ok();
    }

}
