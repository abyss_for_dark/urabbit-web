package com.graphai.mall.wx.web.other;


import static com.graphai.commons.util.servlet.ServletUtils.getRequest;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.graphai.commons.constant.JetcacheConstant;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.ip.IpUtils;
import com.graphai.commons.util.json.JacksonUtil;
import com.graphai.commons.util.json.JacksonUtils;
import com.graphai.commons.util.lock.MutexLockUtils;
import com.graphai.commons.util.rsa.RSA;
import com.graphai.mall.admin.domain.MallSystemRule;
import com.graphai.mall.admin.service.IGameRedHourRuleService;
import com.graphai.mall.admin.service.IMallRedQrcodeRecordService;
import com.graphai.mall.db.domain.FcAccount;
import com.graphai.mall.db.domain.FcAccountRechargeBill;
import com.graphai.mall.db.domain.GameRedpacketRain;
import com.graphai.mall.db.domain.GoldExchangeRecord;
import com.graphai.mall.db.domain.MallUser;
import com.graphai.mall.db.domain.WangzhuanTaskWithdrawaChancel;
import com.graphai.mall.db.domain.WangzhuanTaskWithdrawaChancelSum;
import com.graphai.mall.db.service.account.AccountService;
import com.graphai.mall.db.constant.account.AccountStatus;
import com.graphai.mall.db.service.account.FcAccountRechargeBillService;
import com.graphai.mall.db.service.game.GameRedpacketRainService;
import com.graphai.mall.db.service.game.GoldDetiledSerivce;
import com.graphai.mall.db.service.sysconfig.MallSystemConfigService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.service.wangzhuan.WangZhuanTaskChanceService;
import com.graphai.mall.db.constant.CommonConstant;
import com.graphai.mall.db.util.account.AccountUtils;
import com.graphai.mall.db.util.phone.PushUtils;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.mall.wx.util.AESUtils;
import com.graphai.nosql.redis.NewCacheInstanceServiceLoader;
import com.graphai.spi.mq.BasedMQProducer;
import com.graphai.spi.mq.MQBaseConstant;

import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;

@RestController
@RequestMapping("/wx/gold")
@Validated
public class WxGoldDetailedController {
    private final Log logger = LogFactory.getLog(WxGoldDetailedController.class);

    @Autowired
    private GoldDetiledSerivce goldDetiledSerivce;
    @Autowired
    private AccountService accountService;
    @Autowired
    private FcAccountRechargeBillService fcAccountRechargeBillService;
    @Autowired
    private WangZhuanTaskChanceService wangZhuanTaskChanceService;
    @Autowired
    private GameRedpacketRainService gameRedpacketRainService;
    @Autowired
    private IGameRedHourRuleService redHourRuleService;
    @Autowired
    private BasedMQProducer _basedMQProducer;
    @Autowired
    private MallUserService mallUserService;
    @Autowired
    private IMallRedQrcodeRecordService iMallRedQrcodeRecordService;
    @Autowired
    private MallSystemConfigService mallSystemConfigService;

    /**
     * 金币明细
     */
    @RequestMapping("/detailed")
    public Object goldDetailed(@LoginUser String userId) {
        if (null == userId) {
            Map map1 = new HashMap();
            Map map2 = new HashMap();
            map2.put("exchangeAmount", 0);
            map2.put("exchangeGold", 0);
            map1.put("gold", map2);
            return ResponseUtil.ok(map1);
        }
        LocalDate now1 = LocalDate.now();
        LocalDate already = now1.minusDays(7);
        LocalDateTime noww = LocalDateTime.now();
        // 7天流水
        List<FcAccountRechargeBill> list = goldDetiledSerivce.getGoldDetailed(userId, noww, already);
        // List<FcAccountRechargeBill> list = bills.stream().filter(obj ->
        // obj.getBid().equals(AccountStatus.BID_10) ||
        // obj.getBid().equals(AccountStatus.BID_21)).collect(Collectors.toList());
//        List<FcAccountRechargeBill> list = bills.stream().filter(obj -> obj.getMessage().contains("金币")).collect(Collectors.toList());
        HashSet<String> listS = new HashSet<>();
        Map map = new HashMap<>();
        Map<String, Object> map1 = new HashMap<>();
        Map datamap = new HashMap<>();

        List listdata1 = new ArrayList();
        if (list.size() > 0) {
            for (FcAccountRechargeBill fca : list) {
                LocalDate now = LocalDate.from(fca.getCreateDate());
                listS.add(now.toString());

            }
            for (String time : listS) {

                List listdata = new ArrayList();

                for (FcAccountRechargeBill fca : list) {
                    // String s=LocalDate.from(fca.getCreateDate()).toString();
                    Map mapdata = new HashMap<>();
                    if (time.equals(LocalDate.from(fca.getCreateDate()).toString())) {
                        String Hour = "";
                        String Minute = "";
                        String Second = "";

                        if (fca.getCreateDate().getHour() < 10) {
                            Hour = "0" + fca.getCreateDate().getHour();
                        } else {
                            Hour = fca.getCreateDate().getHour() + "";
                        }
                        if (fca.getCreateDate().getMinute() < 10) {
                            Minute = "0" + fca.getCreateDate().getMinute();
                        } else {
                            Minute = fca.getCreateDate().getMinute() + "";
                        }
                        if (fca.getCreateDate().getSecond() < 10) {
                            Second = "0" + fca.getCreateDate().getSecond();
                        } else {
                            Second = fca.getCreateDate().getSecond() + "";
                        }


                        String timed = Hour + ":" + Minute + ":" + Second;
//                        mapdata.put("message", fca.getBid().equals(AccountStatus.BID_21) ? "-" + (fca.getAmt().multiply(new BigDecimal(10000))).setScale(0) : fca.getAmt());
                        mapdata.put("message",fca.getMessage());
                        mapdata.put("amt", fca.getAmt());
                        mapdata.put("time", timed);
                        listdata.add(mapdata);
                    }
                }

                Collections.reverse(listdata);
                map1.put(time, listdata);

            }

            // listdata1.add(map1);

        } else {
            LocalDate now = LocalDate.now();
            map.put(now.toString(), listdata1);
        }


        /** 今日金币 */
        List<FcAccountRechargeBill> lists = goldDetiledSerivce.getDayGoldDetailed(userId);
        List<BigDecimal> decimalList = new ArrayList<>();
        if (lists.size() > 0) {
            for (FcAccountRechargeBill bill : lists) {
                decimalList.add(bill.getAmt());
            }
        }
        BigDecimal bigd = new BigDecimal("0.00");
        if (decimalList.size() > 0 && decimalList.size() <= 1) {
            map.put("todayGold", decimalList.get(0));
        } else if (decimalList.size() > 1) {
            for (int i = 0; i < decimalList.size(); i++) {
                bigd = bigd.add(decimalList.get(i));
                map.put("todayGold", bigd);
            }
        } else {
            map.put("todayGold", bigd);
        }
        /***
         * 累计收益金币
         */
        List<GoldExchangeRecord> listr = goldDetiledSerivce.selectGoldExchange(userId);
        int gold = 0;
        BigDecimal gold1 = new BigDecimal("0");// 累计收益金币
        if (listr.size() > 0) {
            for (GoldExchangeRecord record : listr) {
                gold += record.getGold();
            }
        }
        FcAccount fc = accountService.getUserAccount(userId);
        /** 可兑换金币 */
        if (null != fc && null != fc.getGold()) {
            map.put("exchangeGold", fc.getGold());
            gold1 = fc.getGold().add(new BigDecimal(gold + ""));
        }
        /** 可提现金额 */
        if (null != fc && null != fc.getExchangeAmount()) {
            map.put("exchangeAmount", fc.getExchangeAmount());
        }

        map.put("recordGold", gold1);

        datamap.put("gold", map);
        Map<String, Object> finalMap = new LinkedHashMap<>();

        map1.entrySet().stream().sorted(Map.Entry.<String, Object>comparingByKey().reversed()).forEachOrdered(x -> finalMap.put(x.getKey(), x.getValue()));
        datamap.put("goldRecord", finalMap);
        return ResponseUtil.ok(datamap);
    }

    /**
     * 金币兑换现金
     *
     */
    @RequestMapping("/exchange")
    public Object goldExchange(@LoginUser String userId, @RequestParam String gold) throws Exception {
        if (null == userId) {
            return ResponseUtil.unlogin();
        }
        FcAccount fc = accountService.getUserAccount(userId);
        if (null != fc && null != fc.getGold()) {
            if (fc.getGold().compareTo(new BigDecimal(gold)) < 0) {
                return ResponseUtil.fail(403, "传入金币数大于实际金币数！");
            }
        }

        String[] str = gold.split("\\.");



        Long golds = Long.parseLong(str[0]) - Long.parseLong(str[0]) % 10000; // 兑换金币数
        Long goldst = Long.parseLong(str[0]) / 10000;// 金额

        // todo 兑换比例暂时写死10000:1，以后从形态表中获取
        // BigDecimal big = new BigDecimal(golds/10000).setScale(2,BigDecimal.ROUND_UP) ;
        if (goldst > 0) {
            Map paramsMap = new HashMap();
            paramsMap.put("dType", AccountStatus.DISTRIBUTION_TYPE_00); // 直接分润
            paramsMap.put("message", "金币兑换现金充值现金");
            paramsMap.put("capitalTrend", AccountStatus.CAPITAL_TREND_01);
            paramsMap.put("changeType", AccountStatus.CHANGE_TYPE_01);
            paramsMap.put("changeAccountType", AccountStatus.CHANGE_ACCOUNT_TYPE_2);
            paramsMap.put("tradeType", AccountStatus.TRADE_TYPE_02);
            AccountUtils.accountChangeByExchange(userId, new BigDecimal(goldst), CommonConstant.TASK_REWARD_CASH, AccountStatus.BID_21, paramsMap);
            // 消息推送
            String content = "成功兑换" + gold + "个金币，获得金额¥" + new BigDecimal(goldst) + "已转至您的可提现账户.";
            PushUtils.pushMessage(userId, "金币兑换成功", content, 3, 1);
        }


        GoldExchangeRecord record = new GoldExchangeRecord();
        record.setUserId(userId);
        record.setGold(Integer.parseInt(golds + ""));
        record.setJvalue(new BigDecimal(goldst));
        record.setRemarks("金币对换现金");



        Map paramsMap1 = new HashMap();
        paramsMap1.put("dType", AccountStatus.DISTRIBUTION_TYPE_00); // 直接分润
        paramsMap1.put("message", "金币对换现金减金币");
        paramsMap1.put("capitalTrend", AccountStatus.CAPITAL_TREND_02);
        paramsMap1.put("changeType", AccountStatus.CHANGE_TYPE_03);
        paramsMap1.put("changeAccountType", AccountStatus.CHANGE_ACCOUNT_TYPE_2);
        AccountUtils.accountChangeByExchange(userId, new BigDecimal(golds), CommonConstant.TASK_REWARD_GOLD, AccountStatus.BID_21, paramsMap1);
        int i = goldDetiledSerivce.insertGoldDetailed(record);
        if (i > 0) {
            return ResponseUtil.ok();
        } else {
            return ResponseUtil.fail(403, "金币兑换失败！");
        }


    }

    /**
     * 金币兑换现金记录list
     */
    @RequestMapping("/exchangeList")
    public Object getExchangeList(@LoginUser String userId) {
        if (null == userId) {
            return ResponseUtil.unlogin();
        }
        LocalDate now = LocalDate.now();
        LocalDate already = now.minusDays(7);

        List<Map<String, Object>> list = new ArrayList<>();
        // List<GoldExchangeRecord> record = goldDetiledSerivce.selectGoldExchange7Day(userId,already,now);
        List<FcAccountRechargeBill> fc = fcAccountRechargeBillService.listFcAccountBill(userId, already, now);
        // if(record.size()>0){
        // for(GoldExchangeRecord rec : record){
        // Map<String,Object> map = new HashMap<>();
        // map.put("amt",rec.getJvalue());
        // map.put("message",rec.getRemarks());
        // map.put("time",rec.getCreateTime().toLocalDate().toString());
        // list.add(map);
        // }
        // }
        if (fc.size() > 0) {
            for (FcAccountRechargeBill rec : fc) {
                Map<String, Object> map = new HashMap<>();
                map.put("amt", rec.getAmt());
                map.put("message", rec.getMessage());
                map.put("time", rec.getCreateDate().toLocalDate().toString());
                list.add(map);
            }
        }



        // if (!StringUtils.isEmpty(cloudRInfoPo.getDuration()) || "desc".equals(cloudRInfoPo.getSort())) {
        // record.sort(Comparator.comparing((Map<String, Object> h) -> ((String) h.get("time")))); //正序
        // }
        // if (!StringUtils.isEmpty(cloudRInfoPo.getDuration()) || "asc".equals(cloudRInfoPo.getSort())) {
        list.sort(Comparator.comparing((Map<String, Object> h) -> ((String) h.get("time"))).reversed()); // 倒叙
        // }


        return ResponseUtil.ok(list);
    }

    /**
     * 领取红包（抖音）(该接口暂时不用，防止恶意刷接口修改接口路径 /receiveGold 修改前 ，/receiveGoldDy 修改后)
     */
    @RequestMapping("/receiveGoldDy")
    public Object receive(@LoginUser String userId, HttpServletRequest request) throws Exception {

        if (null == userId) {
            return ResponseUtil.unlogin();
        }

        // 领取抖货红包那里改成每天只能看十次，如果超过十次返回今天辛苦了明天再来
        String num = "9"; // 默认10，因为比较，所以减1
        Map<String, String> map = mallSystemConfigService.redPacketDHRule();
        if (MapUtil.isNotEmpty(map)) {
            String numHoursStr = map.get("mall_group_douhuo_num");
            if (StringUtils.isNotBlank(numHoursStr)) {
                num = numHoursStr;
            }
            logger.info("numHoursStr=" + numHoursStr + "; num=" + num);
        }
        logger.info("num=" + num);
        Integer count = iMallRedQrcodeRecordService.getBigRedCurrentCount(userId);
        if (count != null) {
            BigDecimal countBig = new BigDecimal(String.valueOf(count));
            if (countBig.compareTo(new BigDecimal(num)) == 1) {
                return ResponseUtil.fail("今天的抖货红包次数已用完，欢迎明天再来。");
            }
        }

        /** 业务执行所需传递必要参数 **/
        Map<String, Object> bizmap = new HashMap<>();
        try {
            synchronized (MutexLockUtils.getMutexLock(userId)) {
                String device = "";
                String userAgent = getRequest().getHeader("user-agent");
                if (null != userAgent && userAgent.contains("iPhone")) {
                    device = "iPhone";
                } else {
                    device = "Android";
                }
                /** 查询红包 **/
                String actId = "401";
                GameRedpacketRain grain = gameRedpacketRainService.selectRedpacket(actId);

                /** 获取整点红包配置规则 **/
                List<MallSystemRule> listRule = redHourRuleService.list(new QueryWrapper<MallSystemRule>().eq("rule_id", 4).eq("status", 0));

                /** 整点红包区间1 给一个初始值防止出错后没有值 **/
                String packet1 = "0.01";
                /** 整点红包区间2 给一个初始值防止出错后没有值 **/
                String packet2 = "0.01";

                packet1 = listRule.get(0).getStartSection().toString();
                packet2 = listRule.get(0).getEndSection().toString();
                MallUser mallUser = mallUserService.findById(userId);

                /** 通过红包规则段获取最终领取金额 **/
                Random random = new Random();
                DecimalFormat df1 = new DecimalFormat("0.00");
                BigDecimal residuer = new BigDecimal(df1.format(random.nextDouble() * (Double.parseDouble(packet2) - Double.parseDouble(packet1)) + Double.parseDouble(packet1))).setScale(2, BigDecimal.ROUND_DOWN);
                BigDecimal residuerTal = residuer;
                /** 判断是否达人 */
                if ("1".equals(mallUser.getIsTalent())) {
                    // （新：新的规则表）
                    residuerTal = residuer.multiply(new BigDecimal(listRule.get(0).getReserveTwo())).setScale(2, BigDecimal.ROUND_DOWN);
                }
                Map<String, Object> datamap = new HashMap<>();

                /** 抖音红包缓存key **/
                String cacheKey = JetcacheConstant.redCacheConstantDy();
                bizmap.put("device", device);
                bizmap.put("version", getRequest().getHeader("version"));
                bizmap.put("ip", IpUtils.getIpAddress(getRequest()));
                bizmap.put("userId", userId);
                // 原金额
                bizmap.put("residuerStr", residuer.toString());
                // 达人金额
                bizmap.put("residuerTal", residuerTal.toString());
                bizmap.put("grainId", grain.getId());
                bizmap.put("ruleId", listRule.get(0).getId());
                /** 将参数投递入缓存 
                NewCacheInstanceServiceLoader.bizCache().put(cacheKey, bizmap);**/
                
                Map<String, Object> mqParams = new HashMap<>();
                mqParams.put("param", JacksonUtils.bean2Jsn(bizmap));
                mqParams.put("bizKey", cacheKey);
                mqParams.put("tags", MQBaseConstant.RED_DY_ENVELOPE_TAG);
                _basedMQProducer.send(mqParams);
                /** 将缓存ID投递入MQ队列 **/
                //wxProducerService.produceMsg(JacksonUtils.bean2Jsn(bizmap), cacheKey, MQBaseConstant.RED_DY_ENVELOPE_TAG);
                /** 触发红包领取逻辑 做实际的入库操作 **/
                // gameRedComponent.receive(userId, makerId, mallSystemRule, residuer, grain, map);
                /** 移除mutexlock **/
                MutexLockUtils.remove(userId);
                datamap.put("money", residuerTal);
                return ResponseUtil.ok(datamap);
            }
        } catch (Exception e) {
            logger.error("领取红包异常：" + e.getMessage(), e);

            return ResponseUtil.fail("红包领取失败");
        }

    }


    /**
     * 领取红包（抖音）
     */
    @PostMapping("/receiveGold/V2")
    public Object receiveGoldv2(@LoginUser String userId, HttpServletRequest request, @RequestBody String body) throws Exception {

        if (null == userId) {
            return ResponseUtil.unlogin();
        }

        // 领取抖货红包那里改成每天只能看十次，如果超过十次返回今天辛苦了明天再来
        String num = "9"; // 默认10，因为比较，所以减1
        Map<String, String> map = mallSystemConfigService.redPacketDHRule();
        if (MapUtil.isNotEmpty(map)) {
            String numHoursStr = map.get("mall_group_douhuo_num");
            if (StringUtils.isNotBlank(numHoursStr)) {
                num = numHoursStr;
            }
            logger.info("numHoursStr=" + numHoursStr + "; num=" + num);
        }
        logger.info("num=" + num);
        Integer count = iMallRedQrcodeRecordService.getBigRedCurrentCount(userId);
        if (count != null) {
            BigDecimal countBig = new BigDecimal(String.valueOf(count));
            if (countBig.compareTo(new BigDecimal(num)) == 1) {
                return ResponseUtil.fail("今天的抖货红包次数已用完，欢迎明天再来。");
            }
        }

        // 更新用户达人状态
        mallUserService.updateUserTalentStatus(userId);
        String params = JacksonUtil.parseString(body, "params");
        /** 业务执行所需传递必要参数 **/
        Map<String, Object> bizmap = new HashMap<>();
        try {
            synchronized (MutexLockUtils.getMutexLock(userId)) {
                Map<String, String> mapRequest = RSA.getSignAlgorithms(params);
                /** 时间戳用来作为 AES密码 */
                String timestamp = mapRequest.get("timestamp");
                String param = "xxx" + timestamp;
                String xMallAd = getRequest().getHeader("X-Mall-Ad");
                String aesBase64Decode = AESUtils.decodeFromBase64String(xMallAd, param.getBytes(), null);
                String device = "";
                String userAgent = getRequest().getHeader("user-agent");
                if (null != userAgent && userAgent.contains("iPhone")) {
                    device = "iPhone";
                } else {
                    device = "Android";
                }
                if (!StrUtil.isBlank(aesBase64Decode) && aesBase64Decode.contains("WLSH")) {
                    /** 查询红包 **/
                    String actId = "401";
                    GameRedpacketRain grain = gameRedpacketRainService.selectRedpacket(actId);

                    /** 获取整点红包配置规则 **/
                    List<MallSystemRule> listRule = redHourRuleService.list(new QueryWrapper<MallSystemRule>().eq("rule_id", 4).eq("status", 0));

                    /** 整点红包区间1 给一个初始值防止出错后没有值 **/
                    String packet1 = "0.01";
                    /** 整点红包区间2 给一个初始值防止出错后没有值 **/
                    String packet2 = "0.01";

                    packet1 = listRule.get(0).getStartSection().toString();
                    packet2 = listRule.get(0).getEndSection().toString();
                    MallUser mallUser = mallUserService.findById(userId);

                    /** 通过红包规则段获取最终领取金额 **/
                    Random random = new Random();
                    DecimalFormat df1 = new DecimalFormat("0.00");
                    BigDecimal residuer = new BigDecimal(df1.format(random.nextDouble() * (Double.parseDouble(packet2) - Double.parseDouble(packet1)) + Double.parseDouble(packet1))).setScale(2, BigDecimal.ROUND_DOWN);
                    BigDecimal residuerTal = residuer;
                    /** 判断是否达人 */
                    if ("1".equals(mallUser.getIsTalent())) {
                        // （新：新的规则表）
                        // residuerTal = residuer.multiply(new BigDecimal(listRule.get(0).getReserveTwo())).setScale(2,
                        // BigDecimal.ROUND_DOWN);
                        residuerTal = RandomUtil.randomBigDecimal(new BigDecimal("0.06"), new BigDecimal("0.13")).setScale(2, BigDecimal.ROUND_DOWN);

                    }
                    Map<String, Object> datamap = new HashMap<>();

                    /** 抖音红包缓存key **/
                    String cacheKey = JetcacheConstant.redCacheConstantDy();
                    bizmap.put("device", device);
                    bizmap.put("version", getRequest().getHeader("version"));
                    bizmap.put("ip", IpUtils.getIpAddress(getRequest()));
                    bizmap.put("userId", userId);
                    // 原金额
                    bizmap.put("residuerStr", residuer.toString());
                    // 达人金额
                    bizmap.put("residuerTal", residuerTal.toString());
                    bizmap.put("grainId", grain.getId());
                    bizmap.put("ruleId", listRule.get(0).getId());
                    bizmap.put("xMallAd", aesBase64Decode);
                    /** 将参数投递入缓存 **/
                    NewCacheInstanceServiceLoader.bizCache().put(cacheKey, bizmap);
                    
                    Map<String, Object> mqParams = new HashMap<>();
                    mqParams.put("param", JacksonUtils.bean2Jsn(bizmap));
                    mqParams.put("bizKey", cacheKey);
                    mqParams.put("tags", MQBaseConstant.RED_DY_ENVELOPE_TAG);
                    _basedMQProducer.send(mqParams);
                    
                    /** 将缓存ID投递入MQ队列 **/
                    //wxProducerService.produceMsg(JacksonUtils.bean2Jsn(bizmap), cacheKey, MQBaseConstant.RED_DY_ENVELOPE_TAG);
                    /** 触发红包领取逻辑 做实际的入库操作 **/
                    // gameRedComponent.receive(userId, makerId, mallSystemRule, residuer, grain, map);
                    /** 移除mutexlock **/
                    MutexLockUtils.remove(userId);
                    datamap.put("money", residuerTal);
                    return ResponseUtil.ok(datamap);
                } else {
                    return ResponseUtil.fail("红包领取失败");
                }
            }
        } catch (Exception e) {
            logger.error("领取红包异常：" + e.getMessage(), e);

            return ResponseUtil.fail("红包领取失败");
        }

    }

    /**
     * 查询今日已领取红包个数和是否已领取（抖音）
     */
    @GetMapping("/receiveGold/findNum")
    public Object receiveGoldv2(@LoginUser String userId) {

        /*
         * if (null == userId) { return ResponseUtil.unlogin(); }
         * 
         * Map<String, Object> result = new HashMap<String, Object>(); result.put("flag", false);
         * result.put("msg", "今天的抖货红包次数未领完。");
         * 
         * // 领取抖货红包那里改成每天只能看十次，如果超过十次返回今天辛苦了明天再来 String num = "9"; // 默认10，因为比较，所以减1 Map<String, String>
         * map = mallSystemConfigService.redPacketDHRule(); if (MapUtil.isNotEmpty(map)) { String
         * numHoursStr = map.get("mall_group_douhuo_num"); if (StringUtils.isNotBlank(numHoursStr)) { num =
         * numHoursStr; } logger.info("numHoursStr="+numHoursStr+"; num="+num); } logger.info("num="+num);
         * 
         * Integer count = iMallRedQrcodeRecordService.getBigRedCurrentCount(userId); if (count != null) {
         * BigDecimal countBig = new BigDecimal(String.valueOf(count)); if (countBig.compareTo(new
         * BigDecimal(num)) == 1) { result.put("flag", true); result.put("msg", "今天的抖货红包次数已用完，欢迎明天再来。"); } }
         * 
         * return ResponseUtil.ok(result);
         */

        Map<String, Object> result = new HashMap<String, Object>();
        result.put("flag", false);
        result.put("msg", "今天的抖货红包次数未领完。");
        return ResponseUtil.ok(result);

    }



    /**
     * 任务中心看广告抽取提现机会
     */
    @GetMapping("/chance")
    public Object getChance(@LoginUser String userId, /* @RequestParam String chance, */@RequestParam String number) {

        if (null == userId) {
            return ResponseUtil.unlogin();
        }
        if (null == number) {
            return ResponseUtil.badArgument();
        }
        Map<String, Object> datamap = new HashMap<>();
        Map map = new HashMap();

        List<LocalDate> listd = new ArrayList();
        // 随机当前周获得两天提现机会
        for (int i = 0; i < 2; i++) {
            int m = (int) (Math.random() * 8);
            LocalDate lastMonday = null;
            if (m == 0) {
                lastMonday = LocalDate.now().minusDays(LocalDate.now().getDayOfWeek().getValue() - 1);// 算出日期
            } else {
                lastMonday = LocalDate.now().minusDays(LocalDate.now().getDayOfWeek().getValue() - m);// 算出日期
            }
            listd.add(lastMonday);
        }

        List<LocalDate> listdata = new ArrayList();
        if (listd.get(0).equals(listd.get(1))) {
            int day = listd.get(0).getDayOfWeek().getValue();
            if (day > 1 && day < 6) {
                LocalDate now = listd.get(1).plusDays(2);
                listdata.add(listd.get(0));
                listdata.add(now);
            } else if (day == 6) {
                LocalDate now = listd.get(1).plusDays(1);
                listdata.add(listd.get(0));
                listdata.add(now);
            }
        } else {
            listdata = listd;
        }


        if (null != wangZhuanTaskChanceService.selectWithdrawaChancel(userId, number)) {

        } else if (!"".equals(number)) {
            wangZhuanTaskChanceService.insertWithdrawaChancel(userId, number);
        }

        List<WangzhuanTaskWithdrawaChancel> list = wangZhuanTaskChanceService.wangzhuanTaskWithdrawaChancelList(userId);
        List<String> list1 = new ArrayList<>();
        if (list.size() > 0 && list.size() <= 4) {
            for (WangzhuanTaskWithdrawaChancel wz : list) {
                list1.add(wz.getPlace());
            }
        } else {
            list1.add(number);
        }

        List<WangzhuanTaskWithdrawaChancelSum> wlist1 = wangZhuanTaskChanceService.selectWithdrawaChancelSumByWeek(userId);
        for (int i = 0; i < listdata.size(); i++) {
            // 随机给提现机会在那个广告位置
            int m = (int) (Math.random() * 5);

            WangzhuanTaskWithdrawaChancelSum wzt = new WangzhuanTaskWithdrawaChancelSum();
            if (m == 0) {
                wzt.setChancePlace("1");
            } else {
                wzt.setChancePlace(m + "");
            }
            LocalDateTime stime = LocalDateTime.of(listdata.get(i), LocalTime.of(00, 00, 00));
            LocalDateTime etime = LocalDateTime.of(listdata.get(i), LocalTime.of(23, 59, 59));
            wzt.setStartTime(stime);
            wzt.setEndTime(etime);
            wzt.setUserId(userId);
            // 最小0.3
            wzt.setChanceNum(new BigDecimal(Math.random() * 0.1).add(new BigDecimal(0.3)).setScale(2, RoundingMode.HALF_UP));
            if (wlist1.size() > 0) {
            } else {
                wangZhuanTaskChanceService.insertWithdrawaChancelSum(wzt);
            }
        }

        for (int i = 1; i <= 4; i++) {
            map.put(i + "", 0);
        }
        for (String str : list1) {
            switch (str) {
                case "1":
                    map.put("1", 1);
                    break;
                case "2":
                    map.put("2", 2);
                    break;
                case "3":
                    map.put("3", 3);
                    break;
                case "4":
                    map.put("4", 4);
                    break;
            }
        }
        // for(String str :list1){
        // switch (str){
        // case "1":
        // map.put("1",1);
        // break;
        // case "2":
        // map.put("2",1);
        // break;
        // case "3":
        // map.put("3",1);
        // break;
        // case "4":
        // map.put("4",1);
        // break;
        // }
        // }

        Map<String, Object> map1 = new HashMap<>();
        List<WangzhuanTaskWithdrawaChancelSum> wlist = wangZhuanTaskChanceService.selectWithdrawaChancelSum(userId);
        for (WangzhuanTaskWithdrawaChancelSum withdrawaChancelSum : wlist) {
            LocalDate now = LocalDate.now();
            if (now.isEqual(withdrawaChancelSum.getStartTime().toLocalDate())) {
                String place = withdrawaChancelSum.getChancePlace();
                switch (place) {
                    case "1":
                        map1.put("num", withdrawaChancelSum.getChanceNum());
                        map1.put("place", "1");
                        map1.put("end", withdrawaChancelSum.getEndTime());
                        map1.put("status", withdrawaChancelSum.getStatus());
                        break;
                    case "2":
                        map1.put("num", withdrawaChancelSum.getChanceNum());
                        map1.put("place", "2");
                        map1.put("end", withdrawaChancelSum.getEndTime());
                        map1.put("status", withdrawaChancelSum.getStatus());
                        break;
                    case "3":
                        map1.put("num", withdrawaChancelSum.getChanceNum());
                        map1.put("place", "3");
                        map1.put("end", withdrawaChancelSum.getEndTime());
                        map1.put("status", withdrawaChancelSum.getStatus());
                        break;
                    case "4":
                        map1.put("num", withdrawaChancelSum.getChanceNum());
                        map1.put("place", "4");
                        map1.put("end", withdrawaChancelSum.getEndTime());
                        map1.put("status", withdrawaChancelSum.getStatus());
                        break;
                }
            }

        }


        datamap.put("status", map);

        if (null != map1) {
            datamap.put("chance", map1);
        } else {
            datamap.put("chance", 0);
        }

        return ResponseUtil.ok(datamap);
    }

}
