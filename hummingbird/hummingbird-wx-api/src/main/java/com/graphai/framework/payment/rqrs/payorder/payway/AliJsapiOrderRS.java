package com.graphai.framework.payment.rqrs.payorder.payway;

import com.graphai.commons.constant.CS;
import com.graphai.commons.util.json.JacksonUtils;
import com.graphai.framework.payment.rqrs.payorder.UnifiedOrderRS;
import lombok.Data;

/*
 * 支付方式： ALI_JSAPI
 *
 * @author terrfly
 * 
 * @site https://www.jeepay.vip
 * 
 * @date 2021/6/8 17:34
 */
@Data
public class AliJsapiOrderRS extends UnifiedOrderRS {

    /** 调起支付插件的支付宝订单号 **/
    private String alipayTradeNo;

    @Override
    public String buildPayDataType() {
        return CS.PAY_DATA_TYPE.ALI_APP;
    }

    @Override
    public String buildPayData() {
        return JacksonUtils.newJson("alipayTradeNo", alipayTradeNo).toString();
    }

}
