/*
 * Copyright (c) 2021-2031, 河北计全科技有限公司 (https://www.jeequan.com & jeequan@126.com).
 * <p>
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE 3.0;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl.html
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.graphai.framework.payment.channel.alipay.payway;

import com.alipay.api.AlipayApiException;
import com.alipay.api.domain.AlipayTradeWapPayModel;
import com.alipay.api.request.AlipayTradeWapPayRequest;
import com.graphai.commons.constant.CS;
import com.graphai.framework.payment.domain.PayOrder;
import com.graphai.commons.util.money.AmountUtils;
import com.graphai.framework.payment.channel.alipay.AlipayKit;
import com.graphai.framework.payment.channel.alipay.AlipayPaymentService;
import com.graphai.framework.payment.exception.ChannelException;
import com.graphai.framework.payment.model.MchAppConfigContext;
import com.graphai.framework.payment.rqrs.AbstractRS;
import com.graphai.framework.payment.rqrs.msg.ChannelRetMsg;
import com.graphai.framework.payment.rqrs.payorder.UnifiedOrderRQ;
import com.graphai.framework.payment.rqrs.payorder.payway.AliWapOrderRQ;
import com.graphai.framework.payment.rqrs.payorder.payway.AliWapOrderRS;
import com.graphai.framework.payment.util.ApiResBuilder;
import org.springframework.stereotype.Service;

/*
* 支付宝 wap支付
*
* @author terrfly
* @site https://www.jeepay.vip
* @date 2021/6/8 17:21
*/
@Service("alipayPaymentByAliWapService") //Service Name需保持全局唯一性
public class AliWap extends AlipayPaymentService {

    @Override
    public String preCheck(UnifiedOrderRQ rq, PayOrder payOrder) {
        return null;
    }

    @Override
    public AbstractRS pay(UnifiedOrderRQ rq, PayOrder payOrder, MchAppConfigContext mchAppConfigContext){

        AliWapOrderRQ bizRQ = (AliWapOrderRQ)rq;

        AlipayTradeWapPayRequest req = new AlipayTradeWapPayRequest();
        AlipayTradeWapPayModel model = new AlipayTradeWapPayModel();
        model.setOutTradeNo(payOrder.getPayOrderId());
        model.setSubject(payOrder.getSubject()); //订单标题
        model.setBody(payOrder.getBody()); //订单描述信息
        model.setTotalAmount(AmountUtils.convertCent2Dollar(payOrder.getAmount().toString()));  //支付金额
        model.setProductCode("QUICK_WAP_PAY");
        req.setNotifyUrl(getNotifyUrl()); // 设置异步通知地址
        req.setReturnUrl(getReturnUrl()); // 同步跳转地址
        req.setBizModel(model);

        //统一放置 isv接口必传信息
        AlipayKit.putApiIsvInfo(mchAppConfigContext, req, model);

        // 构造函数响应数据
        AliWapOrderRS res = ApiResBuilder.buildSuccess(AliWapOrderRS.class);

        try {
            if(CS.PAY_DATA_TYPE.FORM.equals(bizRQ.getPayDataType())){ //表单方式
                res.setFormContent(mchAppConfigContext.getAlipayClientWrapper().getAlipayClient().pageExecute(req).getBody());

            }else if (CS.PAY_DATA_TYPE.CODE_IMG_URL.equals(bizRQ.getPayDataType())){ //二维码图片地址

                String payUrl = mchAppConfigContext.getAlipayClientWrapper().getAlipayClient().pageExecute(req, "GET").getBody();
                res.setCodeImgUrl(sysConfigService.getDBApplicationConfig().genScanImgUrl(payUrl));
            }else{ // 默认都为 payUrl方式

                res.setPayUrl(mchAppConfigContext.getAlipayClientWrapper().getAlipayClient().pageExecute(req, "GET").getBody());
            }
        }catch (AlipayApiException e) {
            throw ChannelException.sysError(e.getMessage());
        }

        ChannelRetMsg channelRetMsg = new ChannelRetMsg();
        res.setChannelRetMsg(channelRetMsg);

        //放置 响应数据
        channelRetMsg.setChannelState(ChannelRetMsg.ChannelState.WAITING);

        return res;
    }

}
