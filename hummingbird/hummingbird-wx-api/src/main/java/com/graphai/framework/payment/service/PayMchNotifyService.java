/*
 * Copyright (c) 2021-2031, 河北计全科技有限公司 (https://www.jeequan.com & jeequan@126.com). <p> Licensed
 * under the GNU LESSER GENERAL PUBLIC LICENSE 3.0; you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at <p>
 * http://www.gnu.org/licenses/lgpl.html <p> Unless required by applicable law or agreed to in
 * writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific
 * language governing permissions and limitations under the License.
 */
package com.graphai.framework.payment.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.alibaba.fastjson.JSONObject;
import com.graphai.commons.util.lang.StringUtils;
import com.graphai.framework.payment.domain.MchNotifyRecord;
import com.graphai.framework.payment.domain.PayOrder;
import com.graphai.framework.payment.domain.RefundOrder;
import com.graphai.framework.payment.rqrs.payorder.QueryPayOrderRS;
import com.graphai.framework.payment.rqrs.refund.QueryRefundOrderRS;
import com.graphai.payment.utils.JeepayKit;
import lombok.extern.slf4j.Slf4j;

/*
 * 商户通知 service
 *
 * @author terrfly
 * 
 * @site https://www.jeepay.vip
 * 
 * @date 2021/6/8 17:43
 */
@Slf4j
@Service
public class PayMchNotifyService {

    @Autowired
    private IMchNotifyRecordService mchNotifyRecordService;

    @Autowired
    private ConfigContextService configContextService;


    /** 商户通知信息， 只有订单是终态，才会发送通知， 如明确成功和明确失败 **/
    public void payOrderNotify(PayOrder dbPayOrder) {

        try {
            // 通知地址为空
            if (StringUtils.isEmpty(dbPayOrder.getNotifyUrl())) {
                return;
            }

            // 获取到通知对象
            MchNotifyRecord mchNotifyRecord = mchNotifyRecordService.findByPayOrder(dbPayOrder.getPayOrderId());

            if (mchNotifyRecord != null) {

                log.info("当前已存在通知消息， 不再发送。");
                return;
            }

            // 商户app私钥
            String appSecret =
                    configContextService.getMchAppConfigContext(dbPayOrder.getMchNo(), dbPayOrder.getAppId()).getMchApp().getAppSecret();

            // 封装通知url
            String notifyUrl = createNotifyUrl(dbPayOrder, appSecret);
            mchNotifyRecord = new MchNotifyRecord();
            mchNotifyRecord.setOrderId(dbPayOrder.getPayOrderId());
            mchNotifyRecord.setOrderType(MchNotifyRecord.TYPE_PAY_ORDER);
            mchNotifyRecord.setMchNo(dbPayOrder.getMchNo());
            mchNotifyRecord.setMchOrderNo(dbPayOrder.getMchOrderNo()); // 商户订单号
            mchNotifyRecord.setIsvNo(dbPayOrder.getIsvNo());
            mchNotifyRecord.setAppId(dbPayOrder.getAppId());
            mchNotifyRecord.setNotifyUrl(notifyUrl);
            mchNotifyRecord.setResResult("");
            mchNotifyRecord.setNotifyCount(0);
            mchNotifyRecord.setState(MchNotifyRecord.STATE_ING); // 通知中
            mchNotifyRecordService.save(mchNotifyRecord);

            // 推送到MQ
            Long notifyId = mchNotifyRecord.getNotifyId();
            // mqPayOrderMchNotifyQueue.send(notifyId + "");

        } catch (Exception e) {
            log.error("推送失败！", e);
        }
    }

    /** 商户通知信息，退款成功的发送通知 **/
    public void refundOrderNotify(RefundOrder dbRefundOrder) {

        try {
            // 通知地址为空
            if (StringUtils.isEmpty(dbRefundOrder.getNotifyUrl())) {
                return;
            }

            // 获取到通知对象
            MchNotifyRecord mchNotifyRecord = mchNotifyRecordService.findByRefundOrder(dbRefundOrder.getRefundOrderId());

            if (mchNotifyRecord != null) {

                log.info("当前已存在通知消息， 不再发送。");
                return;
            }

            // 商户app私钥
            String appSecret = configContextService.getMchAppConfigContext(dbRefundOrder.getMchNo(), dbRefundOrder.getAppId()).getMchApp()
                    .getAppSecret();

            // 封装通知url
            String notifyUrl = createNotifyUrl(dbRefundOrder, appSecret);
            mchNotifyRecord = new MchNotifyRecord();
            mchNotifyRecord.setOrderId(dbRefundOrder.getRefundOrderId());
            mchNotifyRecord.setOrderType(MchNotifyRecord.TYPE_REFUND_ORDER);
            mchNotifyRecord.setMchNo(dbRefundOrder.getMchNo());
            mchNotifyRecord.setMchOrderNo(dbRefundOrder.getMchRefundNo()); // 商户订单号
            mchNotifyRecord.setIsvNo(dbRefundOrder.getIsvNo());
            mchNotifyRecord.setAppId(dbRefundOrder.getAppId());
            mchNotifyRecord.setNotifyUrl(notifyUrl);
            mchNotifyRecord.setResResult("");
            mchNotifyRecord.setNotifyCount(0);
            mchNotifyRecord.setState(MchNotifyRecord.STATE_ING); // 通知中
            mchNotifyRecordService.save(mchNotifyRecord);

            // 推送到MQ
            Long notifyId = mchNotifyRecord.getNotifyId();
            // mqPayOrderMchNotifyQueue.send(notifyId + "");

        } catch (Exception e) {
            log.error("推送失败！", e);
        }
    }


    /**
     * 创建响应URL
     */
    public String createNotifyUrl(PayOrder payOrder, String appSecret) {

        QueryPayOrderRS queryPayOrderRS = QueryPayOrderRS.buildByPayOrder(payOrder);
        JSONObject jsonObject = (JSONObject) JSONObject.toJSON(queryPayOrderRS);
        jsonObject.put("reqTime", System.currentTimeMillis()); // 添加请求时间

        // 报文签名
        jsonObject.put("sign", JeepayKit.getSign(jsonObject, appSecret));

        // 生成通知
        return StringUtils.appendUrlQuery(payOrder.getNotifyUrl(), jsonObject);
    }


    /**
     * 创建响应URL
     */
    public String createNotifyUrl(RefundOrder refundOrder, String appSecret) {

        QueryRefundOrderRS queryRefundOrderRS = QueryRefundOrderRS.buildByRefundOrder(refundOrder);
        JSONObject jsonObject = (JSONObject) JSONObject.toJSON(queryRefundOrderRS);
        jsonObject.put("reqTime", System.currentTimeMillis()); // 添加请求时间

        // 报文签名
        jsonObject.put("sign", JeepayKit.getSign(jsonObject, appSecret));

        // 生成通知
        return StringUtils.appendUrlQuery(refundOrder.getNotifyUrl(), jsonObject);
    }


    /**
     * 创建响应URL
     */
    public String createReturnUrl(PayOrder payOrder, String appSecret) {

        if (StringUtils.isEmpty(payOrder.getReturnUrl())) {
            return "";
        }

        QueryPayOrderRS queryPayOrderRS = QueryPayOrderRS.buildByPayOrder(payOrder);
        JSONObject jsonObject = (JSONObject) JSONObject.toJSON(queryPayOrderRS);
        jsonObject.put("reqTime", System.currentTimeMillis()); // 添加请求时间

        // 报文签名
        jsonObject.put("sign", JeepayKit.getSign(jsonObject, appSecret)); // 签名

        // 生成跳转地址
        return StringUtils.appendUrlQuery(payOrder.getReturnUrl(), jsonObject);

    }

}
