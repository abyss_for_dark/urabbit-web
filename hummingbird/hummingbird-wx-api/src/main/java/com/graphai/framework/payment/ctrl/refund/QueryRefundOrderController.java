/*
 * Copyright (c) 2021-2031, 河北计全科技有限公司 (https://www.jeequan.com & jeequan@126.com). <p> Licensed
 * under the GNU LESSER GENERAL PUBLIC LICENSE 3.0; you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at <p>
 * http://www.gnu.org/licenses/lgpl.html <p> Unless required by applicable law or agreed to in
 * writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific
 * language governing permissions and limitations under the License.
 */
package com.graphai.framework.payment.ctrl.refund;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.graphai.commons.dto.IResponse;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.exception.CustomException;
import com.graphai.framework.payment.ctrl.ApiController;
import com.graphai.framework.payment.domain.RefundOrder;
import com.graphai.framework.payment.rqrs.refund.QueryRefundOrderRQ;
import com.graphai.framework.payment.rqrs.refund.QueryRefundOrderRS;
import com.graphai.framework.payment.service.ConfigContextService;
import com.graphai.framework.payment.service.IRefundOrderService;
import lombok.extern.slf4j.Slf4j;

/**
 * 商户退款单查询controller
 *
 * @author terrfly
 * @site https://www.jeepay.vip
 * @date 2021/6/17 15:20
 */
@Slf4j
@RestController
public class QueryRefundOrderController extends ApiController {

    @Autowired
    private IRefundOrderService refundOrderService;
    @Autowired
    private ConfigContextService configContextService;

    /**
     * 查单接口
     **/
    @RequestMapping("/api/refund/query")
    public IResponse<Object> queryRefundOrder() {

        // 获取参数 & 验签
        QueryRefundOrderRQ rq = getRQByWithMchSign(QueryRefundOrderRQ.class);

        if (StringUtils.isAllEmpty(rq.getMchRefundNo(), rq.getRefundOrderId())) {
            throw new CustomException("mchRefundNo 和 refundOrderId不能同时为空");
        }

        RefundOrder refundOrder = refundOrderService.queryMchOrder(rq.getMchNo(), rq.getMchRefundNo(), rq.getRefundOrderId());
        if (refundOrder == null) {
            throw new CustomException("订单不存在");
        }

        QueryRefundOrderRS bizRes = QueryRefundOrderRS.buildByRefundOrder(refundOrder);
        return ResponseUtil.okWithSign(bizRes,
                configContextService.getMchAppConfigContext(rq.getMchNo(), rq.getAppId()).getMchApp().getAppSecret());
    }
}
