package com.graphai.framework.payment.ctrl.payorder;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.graphai.commons.dto.IResponse;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.exception.CustomException;
import com.graphai.framework.payment.ctrl.ApiController;
import com.graphai.framework.payment.domain.PayOrder;
import com.graphai.framework.payment.rqrs.payorder.QueryPayOrderRQ;
import com.graphai.framework.payment.rqrs.payorder.QueryPayOrderRS;
import com.graphai.framework.payment.service.ConfigContextService;
import com.graphai.framework.payment.service.IPayOrderService;
import lombok.extern.slf4j.Slf4j;

/*
 * 商户查单controller
 *
 * @author terrfly
 * 
 * @site https://www.jeepay.vip
 * 
 * @date 2021/6/8 17:26
 */
@Slf4j
@RestController
public class QueryOrderController extends ApiController {

    @Autowired
    private IPayOrderService payOrderService;
    @Autowired
    private ConfigContextService configContextService;

    /**
     * 查单接口
     **/
    @RequestMapping("/api/pay/query")
    public IResponse<Object> queryOrder() {

        // 获取参数 & 验签
        QueryPayOrderRQ rq = getRQByWithMchSign(QueryPayOrderRQ.class);

        if (StringUtils.isAllEmpty(rq.getMchOrderNo(), rq.getPayOrderId())) {
            throw new CustomException("mchOrderNo 和 payOrderId不能同时为空");
        }

        PayOrder payOrder = payOrderService.queryMchOrder(rq.getMchNo(), rq.getPayOrderId(), rq.getMchOrderNo());
        if (payOrder == null) {
            throw new CustomException("订单不存在");
        }

        QueryPayOrderRS bizRes = QueryPayOrderRS.buildByPayOrder(payOrder);
        return ResponseUtil.okWithSign(bizRes,
                configContextService.getMchAppConfigContext(rq.getMchNo(), rq.getAppId()).getMchApp().getAppSecret());
    }

}
