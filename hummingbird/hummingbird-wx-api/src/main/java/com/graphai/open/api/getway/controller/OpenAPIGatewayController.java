package com.graphai.open.api.getway.controller;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.mall.wx.annotation.LoginUser;
import com.graphai.open.api.getway.service.*;
import com.graphai.open.utils.RequestSetPersister;
import com.graphai.open.utils.StringHelper;
import com.graphai.open.utils.json.JacksonUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.MapUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@RestController
@Validated
@Slf4j
@AllArgsConstructor
public class OpenAPIGatewayController {

    private OpenAPIGatewayDTKService openAPIGatewayDTKService;

    private OpenAPIGatewayFuluService openAPIGatewayFuluService;

    private OpenAPIGatewayJQService openAPIGatewayJQService;

    private OpenAPIGatewayBlueService openAPIGatewayBlueService;

    private OpenAPIGatewayJDService openAPIGatewayJDService;

    private OpenAPIGatewayPDDService openAPIGatewayPDDService;

    private OpenAPIGatewaySNService openAPIGatewaySNService;

    private OpenAPIGatewayWPHService openAPIGatewayWPHService;

    private OpenAPIGatewayPubService openAPIGatewayPubService;
    
    private OpenAPIGatewayCostService openAPIGatewayCostService;
    
    private OpenAPIGatewayWlkService openAPIGatewayWlkService;

    private OpenAPIGatewayYLService openAPIGatewayYLService;

    @PostMapping("/api/getaway")
    public Object getway(@LoginUser String userId, HttpServletResponse response, HttpServletRequest request, @RequestBody String reqjsn, @RequestHeader HttpHeaders headers) {
        //方法/接口名称
        String methodName = headers.getFirst("method");
        //方法版本
        String version = headers.getFirst("version");
        try {
            //认证请求合法性
            if (!RequestSetPersister.requestLegal(headers)) {
//               return IResponseUtils.fail(FrameworkConstants.ERRNO_NOAU, "无应用网关权限");
            }
            
            if (StringHelper.isNullOrEmpty(headers.getFirst("method"))) {
            	return ResponseUtil.fail(501, "方法名参数不能为空!");
			}
           

            Map<String, Object> reqmap = JacksonUtils.jsn2map(reqjsn, String.class, Object.class);
            log.info("网关请求参数reqjsn : " + reqjsn);
            assert methodName != null;
            switch (MapUtils.getString(reqmap, "plateform")) {
                case "fulu":
                    return openAPIGatewayFuluService.requestMethod(MapUtils.getString(reqmap, "plateform"), methodName, version, reqmap);
                case "zm":
                    return openAPIGatewayJQService.requestMethod(MapUtils.getString(reqmap, "plateform"), methodName, version, reqmap);
                case "lsxd":
                    return openAPIGatewayBlueService.requestMethod(MapUtils.getString(reqmap, "plateform"), methodName, version, reqmap);
                case "dtk":
                    return openAPIGatewayDTKService.requestMethod(MapUtils.getString(reqmap, "plateform"), methodName, version, reqmap);
                case "jd":
                    return openAPIGatewayJDService.requestMethod(MapUtils.getString(reqmap, "plateform"), methodName, version, reqmap);
                case "wph":
                    return openAPIGatewayWPHService.requestMethod(MapUtils.getString(reqmap, "plateform"), methodName, version, reqmap);
                case "pdd":
                    return openAPIGatewayPDDService.requestMethod(MapUtils.getString(reqmap, "plateform"), methodName, version, reqmap);
                case "sn":
                    return openAPIGatewaySNService.requestMethod(MapUtils.getString(reqmap, "plateform"), methodName, version, reqmap);
                case "cost":
                    return openAPIGatewayCostService.requestMethod(MapUtils.getString(reqmap, "plateform"), methodName, version, reqmap);
                case "wlk":
                    reqmap.put("userId",userId);
                	return openAPIGatewayWlkService.requestMethod(MapUtils.getString(reqmap, "plateform"), methodName, version, reqmap);
                case "yl":
                    return openAPIGatewayYLService.requestMethod(MapUtils.getString(reqmap, "plateform"), methodName, version, reqmap);
                case "pub":
                    //公用调用入口
                    return openAPIGatewayPubService.requestMethod(MapUtils.getString(reqmap, "plateform"), methodName, version, reqmap);
            }
        } catch (Exception e) {
            log.error("开放平台接口请求异常"
                    + "methodname：" + methodName
                    + "版本号version：" + version, e);
        }
        return null;
    }


}
