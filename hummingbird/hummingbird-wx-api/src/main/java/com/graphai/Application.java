package com.graphai;

import javax.servlet.MultipartConfigElement;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.util.unit.DataSize;

import com.alicp.jetcache.anno.config.EnableCreateCacheAnnotation;
import com.alicp.jetcache.anno.config.EnableMethodCache;

// @SpringBootApplication(scanBasePackages = {"com.graphai.mall.db", "com.graphai.mall.core",
// "com.graphai.mall.wx"})
@SpringBootApplication(scanBasePackages = {"com.graphai","com.xxl.job.core", "com.xxl.job.executor","com.xxl.job.admin.service"} /* exclude = {DataSourceAutoConfiguration.class} */)
@MapperScan({"com.graphai.*.*.dao", "com.graphai.**.**.mapper", "com.graphai.uid.worker.dao","com.xxl.job.admin.dao", "com.xxl.job.admin.core.conf"})
@EnableTransactionManagement
@EnableScheduling
// 开启jetcache缓存
@EnableCreateCacheAnnotation
@EnableMethodCache(basePackages = "com.graphai")
// @EnableCreateCacheAnnotation
@Configuration
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
        System.out.println("微信前端商城服务启动成功...............");
    }

    @Bean
    public MultipartConfigElement multipartConfigElement() {
        MultipartConfigFactory factory = new MultipartConfigFactory();
        // 允许上传的文件最大值
        factory.setMaxFileSize(DataSize.ofKilobytes(10485760L)); // KB,MB
        /// 设置总上传数据总大小
        factory.setMaxRequestSize(DataSize.ofKilobytes(10485760L));
        return factory.createMultipartConfig();
    }


}
