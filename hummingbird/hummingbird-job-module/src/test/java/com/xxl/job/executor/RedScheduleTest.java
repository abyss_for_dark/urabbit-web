package com.xxl.job.executor;

import com.graphai.mall.admin.domain.MallRedQrcodeRecord;
import com.graphai.mall.admin.service.IMallRedQrcodeRecordService;
import com.graphai.mall.db.service.red.BigRedschedule;
import com.graphai.mall.db.service.red.RedStatus;
import com.xxl.job.executor.service.jobhandler.SampleXxlJob;
import javafx.application.Application;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: jw
 * @Date: 2021/2/4 21:31
 */
//@SpringBootTest(classes = XxlWxJobExecutorApplication.class)
@WebAppConfiguration
public class RedScheduleTest {
    @Autowired
    BigRedschedule bigRedschedule;
    @Autowired
    private IMallRedQrcodeRecordService iMallRedQrcodeRecordService;


      @Autowired
      private SampleXxlJob sampleXxlJob ;

    @Test
    public void test (){
        Map<String, Object> reusMap = new HashMap<String, Object>();
        reusMap.put("result","false");

        bigRedschedule.insertRandom();

        //手气最佳
        MallRedQrcodeRecord luck = iMallRedQrcodeRecordService.luck();
        luck.setIsBestLuck(RedStatus.MRQR_IS_BEST_LUCK_1);

        boolean b = iMallRedQrcodeRecordService.saveOrUpdate(luck);
    }
    @Test
    public void test1 () throws Exception {
        sampleXxlJob.makerRecharge(null);
    }

    @Test
    public void deleteJobLog () throws Exception {
        sampleXxlJob.deleteJobLog(null);
    }
}
