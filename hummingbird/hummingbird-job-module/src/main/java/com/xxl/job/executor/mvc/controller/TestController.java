package com.xxl.job.executor.mvc.controller;

import com.graphai.mall.admin.domain.MallRedQrcodeRecord;
import com.graphai.mall.admin.service.IMallRedQrcodeRecordService;
import com.graphai.mall.db.service.red.BigRedschedule;
import com.graphai.mall.db.service.red.RedStatus;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: jw
 * @Date: 2021/2/4 22:09
 */
//@Component
public class TestController {

    @Autowired
    BigRedschedule bigRedschedule;
    @Autowired
    private IMallRedQrcodeRecordService iMallRedQrcodeRecordService;

//        @Scheduled(cron = "0/5 * * * * ? ")
    public void test (){
        Map<String, Object> reusMap = new HashMap<String, Object>();
        reusMap.put("result","false");

        bigRedschedule.insertRandom();

        //手气最佳
        MallRedQrcodeRecord luck = iMallRedQrcodeRecordService.luck();
        luck.setIsBestLuck(RedStatus.MRQR_IS_BEST_LUCK_1);

        boolean b = iMallRedQrcodeRecordService.saveOrUpdate(luck);

            System.out.println("执行了。。。。。。。。。。。。");
    }
}
