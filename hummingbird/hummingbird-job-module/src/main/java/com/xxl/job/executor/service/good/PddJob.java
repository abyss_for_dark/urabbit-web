package com.xxl.job.executor.service.good;

import com.graphai.open.mallgoods.service.IMallGoodsDataSerice;
import com.graphai.open.mallgoods.service.IMallGoodsPDDService;
import com.graphai.open.mallgoods.service.IMallGoodsSNService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class PddJob {

    @Autowired
    private IMallGoodsPDDService mallGoodsPDDService;

    @Autowired
    private IMallGoodsSNService mallGoodsSNService;

    @Autowired
    private IMallGoodsDataSerice mallGoodsDataSerice;

    /**
     * 拼多多商品、优惠券等信息更新
     */
//    @XxlJob("OpenJobHandler")
//    public ReturnT<String> OpenJobHandler(String param) throws Exception {
//        log.info("进来了OpenJobHandler");
//        mallGoodsPDDService.getAllGoodsList();
//        mallGoodsPDDService.getHotsGoodsList();
//        return ReturnT.SUCCESS;
//    }

    /**
     * 苏宁商品、优惠券等信息更新
     */
    @XxlJob("OpenSNJobHandler")
    public ReturnT<String> OpenSNJobHandler(String param) throws Exception {
        log.info("进来了OpenSNJobHandler--------------------------");
        mallGoodsSNService.getAllGoodsList();
        mallGoodsSNService.getHotsGoodsList();
        return ReturnT.SUCCESS;
    }

    /**
     * 京东商品、优惠券等信息更新
     */
    @XxlJob("OpenJDJobHandler")
    public ReturnT<String> OpenJDJobHandler(String param) throws Exception {
        log.info("进来了OpenJDJobHandler--------------------------");
        List<Integer> list =new ArrayList<>();
        list.add(1);list.add(2);list.add(10);
        list.add(22);list.add(23);list.add(24);
        list.add(25);list.add(26);list.add(27);
        list.add(28);list.add(29);list.add(30);
        list.add(31);list.add(32);list.add(33);
        list.add(34);list.add(40);list.add(41);
        list.add(109);list.add(110);list.add(125);
        list.add(129);
        for(int j=0;j<list.size();j++){

            for (int i =0 ; i <= 200; i++) {
                String page = i + "";
                System.out.println("i-------" + i);
                log.info("进来了OpenJDJobHandler-----------i--"+i+"----------j-----"+j);
                mallGoodsDataSerice.insertMallGoodInfoByJD(page,list.get(j));
            }
        }
        return ReturnT.SUCCESS;
    }


    /**
     * 定时修改任务
     */
    @XxlJob("updateGoodsJobHandler")
    public ReturnT<String> updateGoodsJobHandler(String param) throws Exception {
        log.info("进来了 updateGoodsJobHandler");
        mallGoodsPDDService.updateGoodsCouponSale(null);
        mallGoodsPDDService.updateGoodsSale(null);
        return ReturnT.SUCCESS;
    }


}
