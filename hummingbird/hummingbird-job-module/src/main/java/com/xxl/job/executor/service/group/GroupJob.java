package com.xxl.job.executor.service.group;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.graphai.mall.db.service.groupon.MallGrouponService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;

import lombok.extern.slf4j.Slf4j;

/**
 *
 *团购定时器
 */
@Component
@Slf4j
public class GroupJob {

    private static Logger logger = LoggerFactory.getLogger(GroupJob.class);

    @Autowired
    private MallGrouponService mallGrouponService;

    /**
     * 团购机器人开团
     */
    @XxlJob("startGroup")
    public ReturnT<String> startGroup(String param) {
        log.info("执行定时器----------------团购机器人开团-------------------------------");
        try {
            logger.info("已经关闭机器人自动参团，请手动后台添加机器人");
//            mallGrouponService.generateRobotGroup();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            e.printStackTrace();
        }
        return ReturnT.SUCCESS;
    }


    /**
     * 团购机器人参团
     */
    @XxlJob("joinGroup")
    public ReturnT<String> joinGroup(String param) {
        log.info("执行定时器-------------------------------团购机器人开团-------------------------------");
        try {
            logger.info("已经关闭机器人自动参团，请手动后台添加机器人");
//            mallGrouponService.robotJoinGroup();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            e.printStackTrace();
        }
        return ReturnT.SUCCESS;
    }



    /**
          *拼团到期还没满团的自动退款
     */
    @XxlJob("endGroup")
    public ReturnT<String> endGroup(String param) {
        log.info("执行定时器-------------------------------团购到期还没满团自动退款-------------------------------");
        try {
            mallGrouponService.NoFullGroupToRefund();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            e.printStackTrace();
        }
        return ReturnT.SUCCESS;
    }

    /**
     * 团购2.0取消订单
     * @param param
     * @return
     */
    @XxlJob("cancelOrderForSecondPay")
    public ReturnT<String> cancelOrderForSecondPay(String param) {
        log.info("执行定时器-------------------------------到期为付尾款取消订单-------------------------------");
        try {
            log.info("------------------------------已经取消二次付款功能，请关闭定时任务-------------------------------");
//            mallGrouponService.cancelOrderForSecondPay();

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            e.printStackTrace();
            return ReturnT.FAIL;
        }
        return ReturnT.SUCCESS;
    }
}
