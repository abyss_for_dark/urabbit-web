package com.xxl.job.executor.service.order;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.graphai.mall.admin.domain.MallPurchaseOrder;
import com.graphai.mall.admin.service.IMallPurchaseOrderService;
import com.graphai.mall.core.system.SystemConfig;
import com.graphai.mall.db.domain.MallOrder;
import com.graphai.mall.db.domain.MallOrderGoods;
import com.graphai.mall.db.service.account.FcAccountPrechargeBillService;
import com.graphai.mall.db.util.OrderUtil;
import com.graphai.open.mallorder.entity.IMallOrder;
import com.graphai.open.mallorder.service.IMallOrderService;
import com.graphai.open.mallorder.service.IMallOrderZYService;
import com.graphai.validator.Order;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import com.xxl.job.core.log.XxlJobLogger;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Slf4j
@Component
public class ZyPurchaseOrderJob {

    @Autowired
    private IMallPurchaseOrderService purchaseOrderService;
    @Autowired
    private IMallOrderZYService orderZYService;

    /**
     * <p>
     * 自营采购订单 卖家发货开始算起十天后用户未点击收货 系统确认自动收货
     * <p>
     * 每半小时检查一次
     */
    @XxlJob("zyOrderCheckJobHandler")
    public ReturnT<String> zyOrderCheckJobHandler(String param) {
        log.info("3、系统开启任务检查订单是否已经超期未确认收货 【采购订单】");
        try {
            //采购
            Object re = purchaseOrderService.checkOrder();
            //销售
            Object res = orderZYService.checkOrderStatus();
            HashMap<String, Object> map = new HashMap<>();
            map.put("销售定时任务：",res);
            map.put("采购定时任务",re);
            String msg = JSON.toJSONString(map);
            return new ReturnT(200,msg);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            e.printStackTrace();
        }
        return ReturnT.SUCCESS;
    }
}