package com.xxl.job.executor.service.jobhandler;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.graphai.mall.db.constant.coupon.CouponUserConstant;
import com.graphai.mall.db.domain.MallCouponUser;
import com.graphai.mall.db.service.coupon.MallCouponUserService;
import com.graphai.mall.db.util.account.fangtao.recharge.AccountFangtaoRePurchaseComponent;
import com.graphai.mall.db.util.account.fangtao.recharge.AccountFangtaoReShopComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.graphai.mall.admin.service.IMallRedDStatisticsService;
import com.graphai.mall.db.service.MallRankConfigureService;
import com.graphai.mall.db.service.account.FcAccountPrechargeBillService;
import com.graphai.mall.db.service.game.GameJackpotService;
import com.graphai.mall.db.service.gasoline.MallGasService;
import com.graphai.mall.db.service.order.MallOrderService;
import com.graphai.mall.db.service.red.BigRedschedule;
import com.graphai.mall.db.service.red.WxRedService;
import com.graphai.mall.db.service.wangzhuan.WangzhuanTaskService;
import com.graphai.mall.db.service.wangzhuan.WangzhuanTaskServiceV2;
import com.graphai.open.mallgoods.service.IMallGoodsDataSerice;
import com.graphai.open.mallgoods.service.IMallGoodsJDService;
import com.graphai.open.mallgrouponrules.service.IMallGrouponRulesService;
import com.xxl.job.admin.service.XxlJobService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.XxlJob;
import com.xxl.job.core.log.XxlJobLogger;
import com.xxl.job.core.util.ShardingUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * XxlJob开发示例（Bean模式）
 * <p>
 * 开发步骤： 1、在Spring Bean实例中，开发Job方法，方式格式要求为 "public ReturnT<String> execute(String param)"
 * 2、为Job方法添加注解 "@XxlJob(value="自定义jobhandler名称", init = "JobHandler初始化方法", destroy =
 * "JobHandler销毁方法")"，注解value值对应的是调度中心新建任务的JobHandler属性的值。 3、执行日志：需要通过 "XxlJobLogger.log" 打印执行日志；
 *
 * @author xuxueli 2019-12-11 21:52:51
 */
@Slf4j
@Component
public class SampleXxlJob {

    private static Logger logger = LoggerFactory.getLogger(SampleXxlJob.class);
    @Autowired
    private MallOrderService mallOrderService;
    @Autowired
    private WangzhuanTaskService wangzhuanTaskService;
    @Autowired
    private WangzhuanTaskServiceV2 wangzhuanTaskServiceV2;
    @Autowired
    private MallRankConfigureService mallRankConfigureService;
    @Autowired
    private GameJackpotService gameJackpotService;
    @Autowired
    private MallGasService MallGasService;
    @Autowired
    private IMallRedDStatisticsService mallRedDStatisticsService;
    @Autowired
    private WxRedService wxRedService;

    @Autowired
    private BigRedschedule bigRedschedule;

    @Autowired
    private IMallGrouponRulesService iMallGrouponRulesService;

    @Autowired
    private IMallGoodsJDService iMallGoodsJDService;

    @Autowired
    private FcAccountPrechargeBillService fcAccountPrechargeBillService;

    @Autowired
    private XxlJobService xxlJobService;

    @Autowired
    private AccountFangtaoReShopComponent accountFangtaoReShopComponent;

    @Autowired
    private AccountFangtaoRePurchaseComponent accountFangtaoRePurchaseComponent;

    @Autowired
    private MallCouponUserService couponUserService;

    /**
     * C端购物订单-预充值转换充值
     */
    @XxlJob("fangtaoCustomerPrechargeToCharge")
    public ReturnT<String> fangtaoCustomerPrechargeToCharge(String param) {
        log.info("C端购物订单-预充值转换充值....................");
        try {
            Object iotResult = accountFangtaoReShopComponent.fangtaoCustomerPrechargeToCharge();
            String msg = JSON.toJSONString(iotResult);
            if (msg.length() > 10000) {
                msg = msg.substring(0, 10000);
            }
            return new ReturnT(200, msg);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            e.printStackTrace();
        }
        return ReturnT.SUCCESS;
    }

    /**
     * 采购订单-预充值转换充值
     */
    @XxlJob("fangtaoPurchasePrechargeToCharge")
    public ReturnT<String> fangtaoPurchasePrechargeToCharge(String param) {
        log.info("采购订单-预充值转换充值....................");
        try {
            Object iotResult = accountFangtaoRePurchaseComponent.fangtaoPurchasePrechargeToCharge();
            String msg = JSON.toJSONString(iotResult);
            if (msg.length() > 10000) {
                msg = msg.substring(0, 10000);
            }
            return new ReturnT(200, msg);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            e.printStackTrace();
        }
        return ReturnT.SUCCESS;
    }

    /**
     * 1、简单任务示例（Bean模式）
     */
    @XxlJob("demoJobHandler")
    public ReturnT<String> demoJobHandler(String param) throws Exception {
        XxlJobLogger.log("XXL-JOB, Hello World.");
        for (int i = 0; i < 5; i++) {
            XxlJobLogger.log("beat at:" + i);
            TimeUnit.SECONDS.sleep(2);
        }
        return ReturnT.SUCCESS;
    }


    /**
     * 删除定时任务日志
     */
    @XxlJob("deleteJobLog")
    public ReturnT<String> deleteJobLog(String param) throws Exception {
        return xxlJobService.deleteJobLog();
    }

    /**
     * 普通用户的创客培育金充值  mall_maker_charge
     * 備注：若他的下线升级创客，且当他是普通用户，则在规定时间内若他也升级创客，则把那笔培育金给他
     */
    @XxlJob("makerRecharge")
    public ReturnT<String> makerRecharge(String param) throws Exception {
        return fcAccountPrechargeBillService.makerRecharge();
    }

    /**
     * 统计红包收益
     */
    @XxlJob("redPrevDayStatistics")
    public ReturnT<String> redPrevDayStatistics(String param) throws Exception {

        log.info("执行统计上一天红包收益....................");
        try {
            Object redObject = mallRedDStatisticsService.insertStatistics();
            String msg = JSON.toJSONString(redObject);
            if (msg.length() > 10000) {
                msg = msg.substring(0, 10000);
            }
            return new ReturnT(200, msg);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            e.printStackTrace();
        }

        return ReturnT.SUCCESS;
    }


    /**
     * 每天生成10个拼团红包团购规则
     */
    @XxlJob("generateGroupRule")
    public ReturnT<String> generateGroupRule(String param) throws Exception {

        log.info("生成拼团红包团购规则....................");
        try {
            Object object = iMallGrouponRulesService.insertGroupRule();
            String msg = JSON.toJSONString(object);

            return new ReturnT(200, msg);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            e.printStackTrace();
        }

        return ReturnT.SUCCESS;
    }

    /**
     * 口令红包退款
     */
    @XxlJob("refundRedPwd")
    public ReturnT<String> refundRedPwd(String param) {
        log.info("执行口令红包退款....................");
        try {
            Object redObject = wxRedService.refundRedPacketPwd();
            String msg = JSON.toJSONString(redObject);
            if (msg.length() > 10000) {
                msg = msg.substring(0, 10000);
            }
            return new ReturnT(200, msg);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            e.printStackTrace();
        }
        return ReturnT.SUCCESS;
    }

    /**
     * 大额红包获取随机金额
     */
    @XxlJob("bigRandom")
    public ReturnT<String> bigRandom(String param) {
        log.info("执行口令红包--大额红包获取随机金额....................");
        try {
            Object redObject = bigRedschedule.generatorRandomBigRed();
            String msg = JSON.toJSONString(redObject);
            if (msg.length() > 10000) {
                msg = msg.substring(0, 10000);
            }
            return new ReturnT(200, msg);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            e.printStackTrace();
        }
        return ReturnT.SUCCESS;
    }


    /**
     * 达人到期推送
     */
    @XxlJob("talentExpire")
    public ReturnT<String> talentExpire(String param) {
        log.info("达人到期前5天推送消息....................");
        try {
            Object redObject = bigRedschedule.talentExpire();
            String msg = JSON.toJSONString(redObject);
            if (msg.length() > 10000) {
                msg = msg.substring(0, 10000);
            }
            return new ReturnT(200, msg);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            e.printStackTrace();
        }
        return ReturnT.SUCCESS;
    }

    /**
     * 达人到期推送
     */
    @XxlJob("talentCancel")
    public ReturnT<String> talentCancel(String param) {
        log.info("达人到期取消达人身份....................");
        try {
            Object redObject = bigRedschedule.talentCancel();
            String msg = JSON.toJSONString(redObject);
            if (msg.length() > 10000) {
                msg = msg.substring(0, 10000);
            }
            return new ReturnT(200, msg);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            e.printStackTrace();
        }
        return ReturnT.SUCCESS;
    }

    /**
     * 淘宝订单查询
     */
    @XxlJob("tbOrderDetail")
    public ReturnT<String> tbOrderDetail(String param) throws Exception {

        log.info("执行淘宝订单api调用....................");
        try {
            mallOrderService.getTbOrderDetail();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            e.printStackTrace();
        }

        return ReturnT.SUCCESS;
    }


    /**
     * 结算淘宝订单查询
     */
    @XxlJob("tbJieSuanOrderDetail")
    public ReturnT<String> tbJieSuanOrderDetail(String param) throws Exception {

        log.info("修改淘宝结算订单api调用....................");
        try {

            Object OrderDetail = mallOrderService.getJiesuanTbOrderDetail();
            String msg = JSON.toJSONString(OrderDetail);
            if (msg.length() > 10000) {
                msg = msg.substring(0, 10000);
            }
            return new ReturnT(200, msg);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            e.printStackTrace();
        }

        return ReturnT.SUCCESS;
    }

    /**
     * 修改付款后进行退款或结算的订单状态
     */
    @XxlJob("updateTbOrder")
    public ReturnT<String> updateTbOrder(String param) throws Exception {

        log.info("修改淘宝结算订单api调用....................");
        try {

            Object OrderDetail = mallOrderService.updateTbOrder();
            String msg = JSON.toJSONString(OrderDetail);
            if (msg.length() > 10000) {
                msg = msg.substring(0, 10000);
            }
            return new ReturnT(200, msg);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            e.printStackTrace();
        }

        return ReturnT.SUCCESS;
    }

    /**
     * 拼多多订单查询
     */
    @XxlJob("pddOrderDetail")
    public ReturnT<String> pddOrderDetail(String param) throws Exception {

        log.info("执行拼多多订单api调用....................");
        try {
            Object pddOrderDetail = mallOrderService.getPddOrderDetail();
            String msg = JSON.toJSONString(pddOrderDetail);
            if (msg.length() > 10000) {
                msg = msg.substring(0, 10000);
            }
            return new ReturnT(200, msg);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            e.printStackTrace();
        }

        return ReturnT.SUCCESS;
    }

    /**
     * 京东订单查询
     */
    @XxlJob("jdOrderDetail")
    public ReturnT<String> jdOrderDetail(String param) throws Exception {
        log.info("执行京东订单api调用....................");
        try {
            Object jdOrderDetail = mallOrderService.getJdOrderDetail();
            if (jdOrderDetail == null) {
                return ReturnT.SUCCESS;
            }
            String msg = JSON.toJSONString(jdOrderDetail);
            if (msg.length() > 10000) {
                msg = msg.substring(0, 10000);
            }
            return new ReturnT(200, msg);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            e.printStackTrace();
        }

        return ReturnT.SUCCESS;
    }


    /**
     * 新版京东订单查询
     */
    // @XxlJob("jdNewOrderDetail")
    // public ReturnT<String> jdNewOrderDetail(String param) throws Exception {
    // log.info("执行京东订单api调用....................");
    // try {
    // Object jdOrderDetail = mallOrderService.getJdNewOrderDetail();
    // if (jdOrderDetail == null) {
    // return ReturnT.SUCCESS;
    // }
    // String msg = JSON.toJSONString(jdOrderDetail);
    // if (msg.length() > 10000) {
    // msg = msg.substring(0, 10000);
    // }
    // return new ReturnT(200, msg);
    // } catch (Exception e) {
    // log.error(e.getMessage(), e);
    // e.printStackTrace();
    // }
    //
    // return ReturnT.SUCCESS;
    // }

    /**
     * 苏宁订单查询 业务逻辑: 1、拉取商品 2、点击商品详情，进行商品推广链接转链。目的:标识当前商品的推广者，即用户ID
     * 3、根据时间段（自定义，不超过24小时）拉取订单（获取订单时，会拿到推广链接转链时，标识的用户ID），并录入订单信息，并记录预充值记录
     * 4、进行订单状态更新,如果入库的订单，和返回的订单状态不一致 5、如果退款，预充值订单取消 6、如果已结算，预充值订单修改为待审核 7、人工审核订单，成功、失败 8、进行预估存转余额，并可提现
     */
    @XxlJob("snOrderDetail")
    public ReturnT<String> snOrderDetail(String param) throws Exception {

        log.info("执行苏宁订单api调用....................");
        try {
            Object snOrderDetail = mallOrderService.getSnOrderDetail();
            if (snOrderDetail == null) {
                return ReturnT.SUCCESS;
            }
            String msg = JSON.toJSONString(snOrderDetail);
            if (msg.length() > 10000) {
                msg = msg.substring(0, 10000);
            }
            return new ReturnT(200, msg);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            e.printStackTrace();
        }

        return ReturnT.SUCCESS;
    }

    /**
     * 唯品会订单查询
     */
    @XxlJob("wphOrderDetail")
    public ReturnT<String> wphOrderDetail(String param) throws Exception {

        log.info("执行唯品会订单api调用....................");
        try {
            Object wphOrderDetail = mallOrderService.getWphOrderDetail();
            if (wphOrderDetail == null) {
                return ReturnT.SUCCESS;
            }
            String msg = JSON.toJSONString(wphOrderDetail);
            if (msg.length() > 10000) {
                msg = msg.substring(0, 10000);
            }
            return new ReturnT(200, msg);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            e.printStackTrace();
        }
        return ReturnT.SUCCESS;
    }


    /**
     * 闪电玩订单查询
     */
    @XxlJob("sdwOrderDetail")
    public ReturnT<String> sdwOrderDetail(String param) throws Exception {

        log.info("执行闪电玩订单api调用....................");

        try {
            mallOrderService.getSdwOrderDetail();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            e.printStackTrace();
        }

        return ReturnT.SUCCESS;
    }


    /**
     * 肯德基订单查询
     */
    @XxlJob("kfcOrderDetail")
    public ReturnT<String> kfcOrderDetail(String param) throws Exception {

        log.info("执行肯德基订单api调用....................");
        try {
            Object kfcOrderDetail = mallOrderService.getKfcOrderDetail();
            if (kfcOrderDetail == null) {
                return ReturnT.SUCCESS;
            }
            String msg = JSON.toJSONString(kfcOrderDetail);
            if (msg.length() > 10000) {
                msg = msg.substring(0, 10000);
            }
            return new ReturnT(200, msg);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            e.printStackTrace();
        }
        return ReturnT.SUCCESS;
    }


    /**
     * 拉取小桔加油订单
     */
    @XxlJob("getXiaoJuOrderList")
    public ReturnT<String> getXiaoJuOrderList(String param) throws Exception {

        log.info("执行小桔加油订单api调用....................");
        try {
            Object orderDetail = mallOrderService.getXiaoJuGasOrderDetail();
            if (orderDetail == null) {
                return ReturnT.SUCCESS;
            }
            String msg = JSON.toJSONString(orderDetail);
            if (msg.length() > 10000) {
                msg = msg.substring(0, 10000);
            }
            return new ReturnT(200, msg);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            e.printStackTrace();
        }
        return ReturnT.SUCCESS;
    }


    /**
     * 拉取团油订单
     */
    @XxlJob("gasOrderDetail")
    public ReturnT<String> gasOrderDetail(String param) throws Exception {

        log.info("执行团油订单api调用....................");
        try {
            Object kfcOrderDetail = mallOrderService.getGasOrderDetail();
            if (kfcOrderDetail == null) {
                return ReturnT.SUCCESS;
            }
            String msg = JSON.toJSONString(kfcOrderDetail);
            if (msg.length() > 10000) {
                msg = msg.substring(0, 10000);
            }
            return new ReturnT(200, msg);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            e.printStackTrace();
        }
        return ReturnT.SUCCESS;
    }

    /**
     * 美团订单查询 业务逻辑: 1、拉取商品 2、判断订单存入和修改订单表 3、存入订单明细表 4、平台请求日志 5、说明：美团的话目前商家没有提供订单状态
     */
    @XxlJob("mtOrderDetail")
    public ReturnT<String> mtOrderDetail(String param) throws Exception {

        log.info("执行美团订单api调用....................");
        try {
            Object mtOrderDetail = mallOrderService.getMtOrderDetail();
            if (mtOrderDetail == null) {
                return ReturnT.SUCCESS;
            }
            String msg = JSON.toJSONString(mtOrderDetail);
            if (msg.length() > 10000) {
                msg = msg.substring(0, 10000);
            }
            return new ReturnT(200, msg);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            e.printStackTrace();
        }

        return ReturnT.SUCCESS;
    }


    /**
     * 美团订单查询 业务逻辑: 1、拉取商品 2、判断订单存入和修改订单表 3、存入订单明细表 4、平台请求日志
     */
    @XxlJob("meiTuanOrderDetail")
    public ReturnT<String> meiTuanOrderDetail(String param) throws Exception {

        log.info("执行美团订单api调用....................");
        try {
            Object mtOrderDetail = mallOrderService.getMeiTuanOrderDetail();
            if (mtOrderDetail == null) {
                return ReturnT.SUCCESS;
            }
            String msg = JSON.toJSONString(mtOrderDetail);
            if (msg.length() > 10000) {
                msg = msg.substring(0, 10000);
            }
            return new ReturnT(200, msg);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            e.printStackTrace();
        }

        return ReturnT.SUCCESS;
    }

    /**
     * 定时拉取油站数据
     */
    @XxlJob("getGas")
    public ReturnT<String> getGas(String param) throws Exception {

        log.info("执行拉取团油数据....................");
        try {
            Object gas = MallGasService.getGasData();
            if (gas == null) {
                return ReturnT.SUCCESS;
            }
            String msg = JSON.toJSONString(gas);
            if (msg.length() > 10000) {
                msg = msg.substring(0, 10000);
            }
            return new ReturnT(200, msg);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            e.printStackTrace();
        }
        return ReturnT.SUCCESS;
    }


    /**
     * 任务定时预上架
     */
    @XxlJob("autoTaskPreOnShelf")
    public ReturnT<String> autoTaskPreOnShelf(String param) {
        log.info("执行任务定时预上架api调用....................");
        try {
            wangzhuanTaskService.autoTaskPreOnShelf();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            e.printStackTrace();
        }
        return ReturnT.SUCCESS;
    }

    /**
     * 排行奖金定时发放
     */
    @XxlJob("autoGrantRankAward")
    public ReturnT<String> autoGrantRankAward(String param) {
        log.info("执行排行奖励发放api调用....................");
        try {
            mallRankConfigureService.autoGrantRankAward();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            e.printStackTrace();
        }
        return ReturnT.SUCCESS;
    }

    /**
     * 游戏奖池定时结算
     */
    @XxlJob("autoGameJackPotShare")
    public ReturnT<String> autoGameJackPotShare(String param) {
        log.info("执行游戏奖池结算api调用....................");
        try {
            gameJackpotService.autoGameJackPotShare();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            e.printStackTrace();
        }
        return ReturnT.SUCCESS;
    }

    /**
     * 游戏定时报名
     */
    @XxlJob("autoGameEnroll")
    public ReturnT<String> autoGameEnroll(String param) {
        log.info("执行游戏报名api调用....................");
        try {
            gameJackpotService.autoGameEnroll();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            e.printStackTrace();
        }
        return ReturnT.SUCCESS;
    }

    /**
     * 游戏红包定时发放
     */
    @XxlJob("autoGameJackpotRedpacket")
    public ReturnT<String> autoGameJackpotRedpacket(String param) {
        log.info("执行游戏红包发放api调用....................");
        try {
            gameJackpotService.autoGameJackpotRedpacket();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            e.printStackTrace();
        }
        return ReturnT.SUCCESS;
    }

    /**
     * 预充值定时审核(返佣)
     */
    @XxlJob("autoPrechargeApply")
    public ReturnT<String> autoPrechargeApply(String param) {
        log.info("执行预充值审核api调用....................");
        try {
            // todo 定时通过符合条件的订单(fc_account_precharge_bill中 对应的 购物order状态为已结算)
            log.info("--------预充值业务逻辑执行--------");


        } catch (Exception e) {
            log.error(e.getMessage(), e);
            e.printStackTrace();
        }
        return ReturnT.SUCCESS;
    }


    /**
     * 4、跨平台Http任务 参数示例： "url: http://www.baidu.com\n" + "method: get\n" + "data: content\n";
     */
    @XxlJob("httpJobHandler")
    public ReturnT<String> httpJobHandler(String param) throws Exception {

        // param parse
        if (param == null || param.trim().length() == 0) {
            XxlJobLogger.log("param[" + param + "] invalid.");
            return ReturnT.FAIL;
        }
        String[] httpParams = param.split("\n");
        String url = null;
        String method = null;
        String data = null;
        for (String httpParam : httpParams) {
            if (httpParam.startsWith("url:")) {
                url = httpParam.substring(httpParam.indexOf("url:") + 4).trim();
            }
            if (httpParam.startsWith("method:")) {
                method = httpParam.substring(httpParam.indexOf("method:") + 7).trim().toUpperCase();
            }
            if (httpParam.startsWith("data:")) {
                data = httpParam.substring(httpParam.indexOf("data:") + 5).trim();
            }
        }

        // param valid
        if (url == null || url.trim().length() == 0) {
            XxlJobLogger.log("url[" + url + "] invalid.");
            return ReturnT.FAIL;
        }
        if (method == null || !Arrays.asList("GET", "POST").contains(method)) {
            XxlJobLogger.log("method[" + method + "] invalid.");
            return ReturnT.FAIL;
        }

        // request
        HttpURLConnection connection = null;
        BufferedReader bufferedReader = null;
        try {
            // connection
            URL realUrl = new URL(url);
            connection = (HttpURLConnection) realUrl.openConnection();

            // connection setting
            connection.setRequestMethod(method);
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setReadTimeout(5 * 1000);
            connection.setConnectTimeout(3 * 1000);
            connection.setRequestProperty("connection", "Keep-Alive");
            connection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            connection.setRequestProperty("Accept-Charset", "application/json;charset=UTF-8");

            // do connection
            connection.connect();

            // data
            if (data != null && data.trim().length() > 0) {
                DataOutputStream dataOutputStream = new DataOutputStream(connection.getOutputStream());
                dataOutputStream.write(data.getBytes("UTF-8"));
                dataOutputStream.flush();
                dataOutputStream.close();
            }

            // valid StatusCode
            int statusCode = connection.getResponseCode();
            if (statusCode != 200) {
                throw new RuntimeException("Http Request StatusCode(" + statusCode + ") Invalid.");
            }

            // result
            bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
            StringBuilder result = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                result.append(line);
            }
            String responseMsg = result.toString();

            XxlJobLogger.log(responseMsg);
            return ReturnT.SUCCESS;
        } catch (Exception e) {
            XxlJobLogger.log(e);
            return ReturnT.FAIL;
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
                if (connection != null) {
                    connection.disconnect();
                }
            } catch (Exception e2) {
                XxlJobLogger.log(e2);
            }
        }

    }

    /**
     * 5、生命周期任务示例：任务初始化与销毁时，支持自定义相关逻辑；
     */
    @XxlJob(value = "demoJobHandler2", init = "init", destroy = "destroy")
    public ReturnT<String> demoJobHandler2(String param) throws Exception {
        XxlJobLogger.log("XXL-JOB, Hello World.");
        return ReturnT.SUCCESS;
    }


    @Autowired
    private IMallGoodsDataSerice mallGoodsDataSerice;

    /**
     * 拉取咚咚呛商品
     */
    @XxlJob("dtkDdqGoods")
    public ReturnT<String> dtkDdqGoods(String param) throws Exception {

        logger.info("执行大淘客获取咚咚呛商品api调用....................");
        System.out.println("注入" + mallGoodsDataSerice);
        try {
            mallGoodsDataSerice.updateDdqGoodsList();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            e.printStackTrace();
        }

        return ReturnT.SUCCESS;
    }


    /**
     * 大淘客失效商品
     */
    @XxlJob("getDtkOldGoods")
    public ReturnT<String> getDtkOldGoods(String param) throws Exception {

        logger.info("执行大淘客失效商品api调用....................");
        try {
            mallGoodsDataSerice.updateGoodsNoOnSale();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            e.printStackTrace();
        }
        return ReturnT.SUCCESS;
    }


    /**
     * 更新大淘客商品实时数据
     */
    @XxlJob("updateDtkGoodsData")
    public ReturnT<String> updateDtkGoodsData(String param) throws Exception {

        logger.info("执行更新大淘客商品实时数据api调用....................");
        try {
            mallGoodsDataSerice.updateChangeGoods();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            e.printStackTrace();
        }
        return ReturnT.SUCCESS;
    }


    /**
     * 定时拉取大淘客商品
     */
    @XxlJob("timingPullDtkGoods")
    public ReturnT<String> timingPullDtkGoods(String param) throws Exception {

        logger.info("执行定时拉取大淘客商品数据api调用....................");
        try {
            mallGoodsDataSerice.pullDtkGoods();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            e.printStackTrace();
        }
        return ReturnT.SUCCESS;
    }


    /**
     * 拉取9.9包邮数据
     */
    @XxlJob("pullNineGoods")
    public ReturnT<String> pullNineGoods(String param) throws Exception {

        logger.info("执行定时拉取9.9包邮商品数据api调用....................");
        try {
            mallGoodsDataSerice.DtkNineGoods();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            e.printStackTrace();
        }
        return ReturnT.SUCCESS;
    }


    /**
     * 疯抢榜单
     */
    @XxlJob("pullRankGoods")
    public ReturnT<String> pullRankGoods(String param) throws Exception {

        logger.info("执行定时拉取疯抢榜单商品数据api调用....................");
        try {
            mallGoodsDataSerice.DtkRankGoods();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            e.printStackTrace();
        }
        return ReturnT.SUCCESS;
    }


    /**
     * 商品入库
     */
    @XxlJob("getGoodsList")
    public ReturnT<String> getGoodsList(String param) throws Exception {

        logger.info("执行商品入库数据api调用....................");
        try {
            mallGoodsDataSerice.insertMallGoodsByDaTaoKe();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            e.printStackTrace();
        }
        return ReturnT.SUCCESS;
    }


    /**
     * 京东商品、优惠券等信息更新
     */
    @XxlJob("updateJdGoods")
    public ReturnT<String> updateJdGoods(String param) throws Exception {
        logger.info("更新京东商品优惠券等信息");
        try {
            iMallGoodsJDService.updateGoodsCoupon();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            e.printStackTrace();
        }
        return ReturnT.SUCCESS;
    }


    /**
     * 拼多多商品、优惠券等信息更新
     */
    // @XxlJob("pddJobHandler")
    // public ReturnT<String> pddJobHandler(String param) throws Exception {
    // System.out.println("ssssssssssssssssssssssssssssssssss");
    // logger.info("sssssssssssssss");
    //// mallGoodsPDDService.getAllGoodsList();
    // return ReturnT.SUCCESS;
    // }


    /**
     * 2、分片广播任务
     */
    @XxlJob("shardingJobHandler")
    public ReturnT<String> shardingJobHandler(String param) throws Exception {

        // 分片参数
        ShardingUtil.ShardingVO shardingVO = ShardingUtil.getShardingVo();
        XxlJobLogger.log("分片参数：当前分片序号 = {}, 总分片数 = {}", shardingVO.getIndex(), shardingVO.getTotal());

        // 业务逻辑
        for (int i = 0; i < shardingVO.getTotal(); i++) {
            if (i == shardingVO.getIndex()) {
                XxlJobLogger.log("第 {} 片, 命中分片开始处理", i);
            } else {
                XxlJobLogger.log("第 {} 片, 忽略", i);
            }
        }

        return ReturnT.SUCCESS;
    }


    /**
     * 3、命令行任务
     */
    @XxlJob("commandJobHandler")
    public ReturnT<String> commandJobHandler(String param) throws Exception {
        String command = param;
        int exitValue = -1;

        BufferedReader bufferedReader = null;
        try {
            // command process
            Process process = Runtime.getRuntime().exec(command);
            BufferedInputStream bufferedInputStream = new BufferedInputStream(process.getInputStream());
            bufferedReader = new BufferedReader(new InputStreamReader(bufferedInputStream));

            // command log
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                XxlJobLogger.log(line);
            }

            // command exit
            process.waitFor();
            exitValue = process.exitValue();
        } catch (Exception e) {
            XxlJobLogger.log(e);
        } finally {
            if (bufferedReader != null) {
                bufferedReader.close();
            }
        }

        if (exitValue == 0) {
            return IJobHandler.SUCCESS;
        } else {
            return new ReturnT<String>(IJobHandler.FAIL.getCode(), "command exit value(" + exitValue + ") is failed");
        }
    }

    // 领取任务-  任务时限内 未完成， 直接返回库存。
    @XxlJob("thirdSecondBackStock")
    public ReturnT<String> thirdSecondBackStock(String param) throws Exception {

        logger.info("规定时间内，任务未被完成，直接返回库存....................");
        try {
            wangzhuanTaskService.thirdSecondBackStock();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            e.printStackTrace();
        }
        return ReturnT.SUCCESS;
    }

    @XxlJob("overTimeToExamined")
    public ReturnT<String> overTimeToExamined(String param) throws Exception {
        logger.info("规定时间内，未完成审核，直接审核通过.......................");
        try {
            wangzhuanTaskService.overTimeToExamined();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            e.printStackTrace();
        }
        return ReturnT.SUCCESS;
    }

    @XxlJob("twoFourBackMoney")
    public ReturnT<String> twoFourBackMoney(String param) throws Exception {
        logger.info("二十四时间内，未手动退款，直接退款.......................");
        try {
            wangzhuanTaskService.twoFourBackMoney();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            e.printStackTrace();
        }
        return ReturnT.SUCCESS;
    }

    @XxlJob("overToCycleDate")
    public ReturnT<String> overToCycleDate(String param) throws Exception {
        logger.info("超出任务周期时间，自动下架.......................");
        try {
            wangzhuanTaskService.overToCycleDate();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            e.printStackTrace();
        }
        return ReturnT.SUCCESS;
    }

    @XxlJob("changeBidding")
    public ReturnT<String> changeBidding(String param) throws Exception {
        logger.info("竞拍时间到啦~,需要调用相应的逻辑啦！.......................每天17点左右，运行！");
        try {
            wangzhuanTaskServiceV2.changeBidding();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            e.printStackTrace();
        }
        return ReturnT.SUCCESS;
    }

    @XxlJob("changeTopOrRecommend")
    public ReturnT<String> changeTopOrRecommend(String param) throws Exception {
        logger.info("每三分钟运行一次~--------------------------- 计算哪个任务的置顶/推荐时间过去了！");
        try {
            wangzhuanTaskServiceV2.changeTopOrRecommend();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            e.printStackTrace();
        }
        return ReturnT.SUCCESS;
    }

    @XxlJob("checkCouponExpiredJobHandler")
    public ReturnT<String> checkCouponExpiredJobHandler(String param) {
        logger.info("系统开启任务检查优惠券是否已经过期");
        try {
            List<MallCouponUser> couponUserList = couponUserService.queryExpired();
            for (MallCouponUser couponUser : couponUserList) {
                couponUser.setStatus(CouponUserConstant.STATUS_EXPIRED);
                couponUserService.update(couponUser);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            e.printStackTrace();
        }
        return ReturnT.SUCCESS;
    }


    public void init() {
        log.info("init");
    }

    public void destroy() {
        log.info("destory");
    }


}
