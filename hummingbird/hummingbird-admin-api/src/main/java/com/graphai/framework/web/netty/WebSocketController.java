package com.graphai.framework.web.netty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.SecurityUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.json.JacksonUtils;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.framework.netty.service.PushService;
import com.graphai.mall.db.domain.MallAdmin;
import com.graphai.mall.db.domain.MallNotice;
import com.graphai.mall.db.service.notice.MallNoticeService;


@RestController
@RequestMapping("/admin/webSocket")
@Validated
public class WebSocketController {

    private final Log logger = LogFactory.getLog(WebSocketController.class);

    @Resource
    private PushService pushService;

    @Resource
    private MallNoticeService mallNoticeService;

    /**
     * 列表
     */
    // @RequiresPermissions("admin:webSocket:list")
    @RequiresPermissionsDesc(menu = {"公告管理", "公告通知"}, button = "查询")
    @GetMapping("/list")
    public Object list(@RequestParam(required = false) String title, @RequestParam(required = false) String beginDate, @RequestParam(required = false) String endDate,
                    @RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit) {

        List<MallNotice> list = mallNoticeService.selectByExample(title, beginDate, endDate, 5, null, page, limit);
        long total = PageInfo.of(list).getTotal();
        Map<String, Object> data = new HashMap<>(16);
        data.put("total", total);
        data.put("items", list);
        return ResponseUtil.ok(data);
    }

    /**
     * 添加
     */
    // @RequiresPermissions("admin:webSocket:add")
    @RequiresPermissionsDesc(menu = {"公告管理", "公告通知"}, button = "添加")
    @PostMapping("/add")
    public Object add(@RequestBody MallNotice mallNotice) {
        // 获取当前用户
        MallAdmin adminUser = (MallAdmin) SecurityUtils.getSubject().getPrincipal();
        if (mallNoticeService.insert(mallNotice, adminUser.getAliasName())) {
            return ResponseUtil.ok();
        }
        return ResponseUtil.fail();
    }

    /**
     * 修改
     */
    // @RequiresPermissions("admin:webSocket:update")
    @RequiresPermissionsDesc(menu = {"公告管理", "公告通知"}, button = "修改")
    @PostMapping("/update")
    public Object update(@RequestBody MallNotice mallNotice) {
        // 获取当前用户
        MallAdmin adminUser = (MallAdmin) SecurityUtils.getSubject().getPrincipal();
        if (mallNoticeService.update(mallNotice, adminUser.getAliasName())) {
            return ResponseUtil.ok();
        }
        return ResponseUtil.fail();
    }

    /**
     * 指定发送
     */
    @PostMapping("/sendToUser")
    public Object send(@RequestBody String data) {
        pushService.pushMsgToOne(JacksonUtils.jsn2map(data, String.class, Object.class));
        return ResponseUtil.ok();
    }

    /**
     * 广播
     */
    @GetMapping("/broadcast")
    public Object broadcast(@NotNull String id) {
        pushService.pushMsgToAll(mallNoticeService.getMallNoticeById(id));
        return ResponseUtil.ok();
    }

    /**
     * 广播
     */
    @GetMapping("/msgList")
    public Object msgList(@NotNull String id) {

        List<Map<String, Object>> list = pushService.getMsgList(id);// 全部消息
        List<Map<String, Object>> listUnread = new ArrayList<>();// 未读消息
        List<Map<String, Object>> listRead = new ArrayList<>();// 已读消息
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                if ((boolean) list.get(i).get("successful")) {
                    listRead.add(list.get(i));
                } else {
                    listUnread.add(list.get(i));
                }

            }
        }
        Map<String, Object> map = new HashMap<>();
        map.put("listAll", list);
        map.put("listUnread", listUnread);
        map.put("listRead", listRead);

        return ResponseUtil.ok(map);
    }

    /**
     * 阅读消息
     */
    @GetMapping("/readMsg")
    public Object readMsg(@NotNull String noticeId) {

        pushService.readMsg(noticeId);
        return ResponseUtil.ok();
    }


}
