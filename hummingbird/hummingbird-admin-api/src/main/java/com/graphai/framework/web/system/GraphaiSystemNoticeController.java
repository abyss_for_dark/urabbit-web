package com.graphai.framework.web.system;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSONObject;
import com.gexin.rp.sdk.base.impl.Target;
import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.PushUtil;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.mall.db.constant.PlateFromConstant;
import com.graphai.mall.db.domain.MallAdmin;
import com.graphai.mall.db.domain.MallNotice;
import com.graphai.mall.db.service.goods.MallGoodsService;
import com.graphai.mall.db.service.notice.MallNoticeService;
import com.graphai.mall.db.service.user.MallUserService;

import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * 公告通知
 *
 * @author LaoGF
 * @since 2020-08-08
 */
@RestController
@RequestMapping("/admin/notice")
@Validated
@Slf4j
public class GraphaiSystemNoticeController {

    @Resource
    private MallNoticeService mallNoticeService;

    @Autowired
    private MallUserService mallUserService;

    @Resource
    private MallGoodsService mallGoodsService;


    /**
     * 列表
     */
    @RequiresPermissions("admin:notice:list")
    @RequiresPermissionsDesc(menu = {"公告管理", "公告通知"}, button = "查询")
    @GetMapping("/list")
    public Object list(@RequestParam(required = false) String title,
                       @RequestParam(required = false) Integer noticeType,
                       @RequestParam(required = false) Integer publishState,
                       @RequestParam(required = false) String beginDate,
                       @RequestParam(required = false) String endDate,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit) {

        List<MallNotice> list = mallNoticeService.selectByExample(title, beginDate, endDate, noticeType, publishState, page, limit);
        long total = PageInfo.of(list).getTotal();
        Map<String, Object> data = new HashMap<>(16);
        data.put("total", total);
        data.put("items", list);
        return ResponseUtil.ok(data);
    }


    /**
     * 添加
     */
    @RequiresPermissions("admin:notice:add")
    @RequiresPermissionsDesc(menu = {"公告管理", "公告通知"}, button = "添加")
    @PostMapping("/add")
    public Object add(@RequestBody MallNotice mallNotice) {
        //获取当前用户
        MallAdmin adminUser = (MallAdmin) SecurityUtils.getSubject().getPrincipal();
        String linkUrl = "";
        if (mallNotice.getType() == 0) {
            mallNotice.setPlatform(PlateFromConstant.PLATFORM_DTK);
        }

        if (mallNotice.getType() == 1 && Objects.equals(mallNotice.getNavigatorType(), "navigateTo")) {
            try {
                linkUrl = URLEncoder.encode(mallNotice.getLinkUrl(), "UTF-8");
                mallNotice.setLinkUrl("/pages/web/webview?link=" + linkUrl);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        if (mallNoticeService.insert(mallNotice, adminUser.getAliasName())) {
            return ResponseUtil.ok();
        }
        return ResponseUtil.fail();
    }

    /**
     * 修改
     */
    @RequiresPermissions("admin:notice:update")
    @RequiresPermissionsDesc(menu = {"公告管理", "公告通知"}, button = "修改")
    @PostMapping("/update")
    public Object update(@RequestBody MallNotice mallNotice) {
        //获取当前用户
        MallAdmin adminUser = (MallAdmin) SecurityUtils.getSubject().getPrincipal();

        String linkUrl = "";
        if (mallNotice.getType() == 1 && Objects.equals(mallNotice.getNavigatorType(), "navigateTo")) {
            try {
                linkUrl = URLEncoder.encode(mallNotice.getLinkUrl(), "UTF-8");
                mallNotice.setLinkUrl("/pages/web/webview?link=" + linkUrl);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        if (mallNoticeService.update(mallNotice, adminUser.getAliasName())) {
            return ResponseUtil.ok();
        }
        return ResponseUtil.fail();
    }

    /**
     * 删除
     */
    @RequiresPermissions("admin:notice:delete")
    @RequiresPermissionsDesc(menu = {"公告管理", "公告通知"}, button = "删除")
    @PostMapping("/delete")
    public Object delete(@RequestParam String id) {
        if (mallNoticeService.deleteByID(id)) {
            return ResponseUtil.ok();
        }
        return ResponseUtil.fail();
    }

    /**
     * 发布公告
     */
    @RequiresPermissions("admin:notice:release")
    @RequiresPermissionsDesc(menu = {"公告管理", "公告通知"}, button = "发布")
    @PostMapping("/release")
    public Object release(@RequestParam String id, @RequestParam Integer status) {

        List<String> cList = null;
        String result = "";
        String linkUrl = "";
        String sysUrl = "";
        try {
            // 获取当前用户
            MallAdmin adminUser = (MallAdmin) SecurityUtils.getSubject().getPrincipal();

            MallNotice notice = mallNoticeService.getMallNoticeById(id);
            if (notice == null) {
                return ResponseUtil.fail(508, "查询公告异常");
            }
            // 查询出符合发布对象的所有用户
//			List<Byte> clientIds = Arrays.asList(notice.getUserLevelId()).stream().map(String::)
//					.collect(Collectors.toList());

            List<String> clientIds = Stream.of(notice.getUserLevelId()).collect(Collectors.toList());
            //推送好货和系统通知
            if (notice.getType() == 0 || notice.getType() == 2) {
                // 商品详情链接Url拼接
                linkUrl = URLEncoder.encode(String
                                .format("/pages/product/product?goodsSn=%s&id=&virtualGood=1&industryId=4", notice.getGoodSn()),
                        "UTF-8");
                // 系统通知列表Url
                sysUrl = URLEncoder.encode(
                        String.format("/pages/notice/notice-info?name=系统通知&type=%s", notice.getType()), "UTF-8");
                if (String.valueOf(notice.getUserLevelId()).contains("1")) {
                    if (notice.getType() == 0) {

                        result = PushUtil.pushMessageToApp(notice.getTitle(), notice.getContent(), linkUrl, "switchTab", notice.getPicUrl() != null ? notice.getPicUrl() : "", null);
                    } else {
                        result = PushUtil.pushMessageToApp(notice.getTitle(), notice.getContent(), sysUrl, "switchTab", "https://gxfacepay.oss-cn-shenzhen.aliyuncs.com/hummingbird/push/smallicon.png", null);
                    }

                } else {
                    cList = mallUserService.selectUserByLevel(clientIds);
                    List<Target> targets = new ArrayList<Target>();
                    for (int i = 0; i < cList.size(); i++) {
                        Target target = new Target();
                        target.setAppId(PushUtil.appId);
                        target.setClientId(cList.get(i));
                        targets.add(target);
                    }
                    if (notice.getType() == 0) {
                        result = PushUtil.pushToList(targets, notice.getTitle(), notice.getContent(), linkUrl, "switchTab", notice.getPicUrl() != null ? notice.getPicUrl() : "", null);
                    } else {
                        result = PushUtil.pushToList(targets, notice.getTitle(), notice.getContent(), sysUrl, "switchTab", "https://gxfacepay.oss-cn-shenzhen.aliyuncs.com/hummingbird/push/smallicon.png", null);
                    }

                }
                // 推送活动
            } else {

                linkUrl = URLEncoder.encode(notice.getLinkUrl(), "UTF-8");
                if (String.valueOf(notice.getUserLevelId()).contains("1")) {

                    result = PushUtil.pushMessageToApp(notice.getTitle(), notice.getContent(), linkUrl,
                            notice.getNavigatorType(), notice.getPicUrl() != null ? notice.getPicUrl() : "", notice.getType());
                } else {
                    cList = mallUserService.selectUserByLevel(clientIds);
                    List<Target> targets = new ArrayList<Target>();
                    for (int i = 0; i < cList.size(); i++) {
                        Target target = new Target();
                        target.setAppId(PushUtil.appId);
                        target.setClientId(cList.get(i));
                        targets.add(target);
                    }
                    result = PushUtil.pushToList(targets, notice.getTitle(), notice.getContent(), linkUrl,
                            notice.getNavigatorType(), notice.getPicUrl() != null ? notice.getPicUrl() : "", notice.getType());
                }
            }

            if (Objects.equals("fail", result)) {
                return ResponseUtil.fail(508, "推送服务失败!");
            }

            if (mallNoticeService.release(id, status, adminUser.getAliasName())) {
                return ResponseUtil.ok();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return ResponseUtil.fail();
    }


    /**
     * 添加
     */
    @RequiresPermissions("admin:notice:add")
    @GetMapping("/pullGoods")
    public Object pullGoods(@RequestParam(required = true) String goodsSn) {

        List<String> list = new ArrayList<String>(1);
        list.add(goodsSn);

        boolean flag;
        try {
            flag = mallGoodsService.insertTbZeroGoods(list);
            log.info("源系统商品ID【" + goodsSn + "】调用插入open库淘宝0元购商品结果【" + flag + "】。");
            if (flag) {
                JSONObject object = mallGoodsService.queryGoodsDetailBySrcId(goodsSn);
                log.info("拉取的商品信息【" + object);
                if (ObjectUtil.isNotNull(object)) {
                    return ResponseUtil.ok(object);
                }
                //return ResponseUtil.ok();
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return ResponseUtil.fail();
    }


}
