package com.graphai.framework.util;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.reflect.MethodUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.util.ClassUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.graphai.commons.constant.UserConstants;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.framework.dto.Permission;
import com.graphai.framework.system.domain.GraphaiSystemMenu;
import com.graphai.framework.system.util.MenuUtil;
import com.graphai.framework.system.vo.MetaVo;
import com.graphai.framework.system.vo.RouterVo;
import com.graphai.mall.admin.vo.PermVo;

@Component
public class PermissionUtil {

	private static PermissionUtil me;

	public PermissionUtil() {
		me = this;
	}

	@Autowired
	private ApplicationContext context;

	public static Collection<String> toAPI(Set<String> permissions) {
		HashMap<String, String> systemPermissionsMap = new HashMap<>();
		// if (systemPermissionsMap == null) {}
		final String basicPackage = "com.graphai";
		List<Permission> systemPermissions = PermissionUtil.listPermission(me.context, basicPackage);
		for (Permission permission : systemPermissions) {
			String perm = permission.getRequiresPermissions().value()[0];
			String api = permission.getApi();
			if (StringUtils.isNotEmpty(api)) {
				systemPermissionsMap.put(perm, api);
			}
		}

		Collection<String> apis = new HashSet<>();
		for (String perm : permissions) {
			String api = systemPermissionsMap.get(perm);
			apis.add(api);

			if (perm.equals("*")) {
				apis.clear();
				apis.add("*");
				return apis;
//                return systemPermissionsMap.values();

			}
		}
		return apis;
	}

	public static List<RouterVo> buildMenus(List<GraphaiSystemMenu> menus) {
		List<RouterVo> routers = new LinkedList<RouterVo>();
		for (GraphaiSystemMenu menu : menus) {
			RouterVo router = new RouterVo();
			router.setHidden("1".equals(menu.getVisible()));
			router.setName(MenuUtil.getRouteName(menu));
			router.setPath(MenuUtil.getRouterPath(menu));
			router.setComponent(MenuUtil.getComponent(menu));
			MetaVo pMetaVo = new MetaVo(menu.getMenuName(), menu.getIcon());

			Set<String> pperms = new HashSet<>();
			/** 如果菜单下挂了按钮权限，这里有一个问题前端需要控制菜单必须挂载在目录下面，不然会有BUG **/
			List<GraphaiSystemMenu> pbuttonMenus = menu.getChildren();
			if (CollectionUtils.isNotEmpty(pbuttonMenus)) {
				for (int i = 0; i < pbuttonMenus.size(); i++) {
					GraphaiSystemMenu bMenu = pbuttonMenus.get(i);
					if (UserConstants.TYPE_BUTTON.equals(bMenu.getMenuType())) {
						pperms.add(bMenu.getPerms());
					}
				}
			}

			if (StringUtils.isNotEmpty(menu.getPerms())) {
				pperms.add(menu.getPerms());
				pMetaVo.setPerms(toAPI(pperms));
			}
			pMetaVo.setMenuId(menu.getMenuId());
			router.setMeta(pMetaVo);
			List<GraphaiSystemMenu> cMenus = menu.getChildren();
			if (!cMenus.isEmpty() && cMenus.size() > 0 && UserConstants.TYPE_DIR.equals(menu.getMenuType())) {
				router.setAlwaysShow(true);
				router.setRedirect("noRedirect");
				router.setChildren(buildMenus(cMenus));
			} else if (UserConstants.TYPE_NPAGE.equals(menu.getMenuType())) {
				router.setHidden(true);
			} else if (MenuUtil.isMeunFrame(menu)) {
				List<RouterVo> childrenList = new ArrayList<RouterVo>();
				RouterVo children = new RouterVo();
				children.setPath(menu.getPath());
				children.setComponent(MenuUtil.getComponent(menu));
				children.setName(StringUtils.capitalize(menu.getPath()));
				MetaVo cMetaVo = new MetaVo(menu.getMenuName(), menu.getIcon());
				cMetaVo.setMenuId(menu.getMenuId());

				Set<String> perms = new HashSet<String>();

				/** 如果菜单下挂了按钮权限，这里有一个问题前端需要控制菜单必须挂载在目录下面，不然会有BUG **/
				List<GraphaiSystemMenu> buttonMenus = menu.getChildren();

				if (CollectionUtils.isNotEmpty(buttonMenus)) {
					for (int i = 0; i < buttonMenus.size(); i++) {
						GraphaiSystemMenu bMenu = buttonMenus.get(i);
						if (UserConstants.TYPE_BUTTON.equals(bMenu.getMenuType())) {
							perms.add(bMenu.getPerms());
						}
					}
				}

				/** 把自身菜单的权限挂载上去 **/
				if (StringUtils.isNotEmpty(menu.getPerms())) {
					perms.add(menu.getPerms());
					cMetaVo.setPerms(toAPI(perms));
				}

				children.setMeta(cMetaVo);
				childrenList.add(children);
				router.setChildren(childrenList);
			}
			routers.add(router);
		}
		return routers;
	}

	public static List<PermVo> listPermVo(List<Permission> permissions) {
		List<PermVo> root = new ArrayList<>();
		for (Permission permission : permissions) {
			RequiresPermissions requiresPermissions = permission.getRequiresPermissions();
			RequiresPermissionsDesc requiresPermissionsDesc = permission.getRequiresPermissionsDesc();
			String api = permission.getApi();

			String[] menus = requiresPermissionsDesc.menu();
			if (menus.length != 2) {
				throw new RuntimeException("目前只支持两级菜单");
			}
			String menu1 = menus[0];
			PermVo perm1 = null;
			for (PermVo permVo : root) {
				if (permVo.getLabel().equals(menu1)) {
					perm1 = permVo;
					break;
				}
			}
			if (perm1 == null) {
				perm1 = new PermVo();
				perm1.setId(menu1);
				perm1.setLabel(menu1);
				perm1.setChildren(new ArrayList<>());
				root.add(perm1);
			}
			String menu2 = menus[1];
			PermVo perm2 = null;
			for (PermVo permVo : perm1.getChildren()) {
				if (permVo.getLabel().equals(menu2)) {
					perm2 = permVo;
					break;
				}
			}
			if (perm2 == null) {
				perm2 = new PermVo();
				perm2.setId(menu2);
				perm2.setLabel(menu2);
				perm2.setChildren(new ArrayList<>());
				perm1.getChildren().add(perm2);
			}

			String button = requiresPermissionsDesc.button();
			PermVo leftPerm = null;
			for (PermVo permVo : perm2.getChildren()) {
				if (permVo.getLabel().equals(button)) {
					leftPerm = permVo;
					break;
				}
			}
			if (leftPerm == null) {
				leftPerm = new PermVo();
				leftPerm.setId(requiresPermissions.value()[0]);
				leftPerm.setLabel(requiresPermissionsDesc.button());
				leftPerm.setApi(api);
				perm2.getChildren().add(leftPerm);
			} else {
				// TODO
				// 目前限制Controller里面每个方法的RequiresPermissionsDesc注解是唯一的
				// 如果允许相同，可能会造成内部权限不一致。
				throw new RuntimeException("权限已经存在，不能添加新权限");
			}

		}
		return root;
	}
	
	/**
	 * 对path进行转换，主要用于在path上传递参数的情况下将{} 处理成和权限标志对应
	 * @param path
	 * @return
	 */
	public static String pathTransformation(String path){
		if(StringUtils.isNotBlank(path) && path.contains("{") && path.contains("}")) {
			return path.substring(0,path.indexOf("{")-1) ;
		}
		return path;
	}
	
	public static List<Permission> listPermission(ApplicationContext context, String basicPackage) {
		Map<String, Object> map = context.getBeansWithAnnotation(Controller.class);
		List<Permission> permissions = new ArrayList<>();
		for (Map.Entry<String, Object> entry : map.entrySet()) {
			Object bean = entry.getValue();
			if (!StringUtils.contains(ClassUtils.getPackageName(bean.getClass()), basicPackage)) {
				continue;
			}

			Class<?> clz = bean.getClass();
			Class controllerClz = clz.getSuperclass();
			RequestMapping clazzRequestMapping = AnnotationUtils.findAnnotation(controllerClz, RequestMapping.class);
			List<Method> methods = MethodUtils.getMethodsListWithAnnotation(controllerClz, RequiresPermissions.class);
			for (Method method : methods) {
				RequiresPermissions requiresPermissions = AnnotationUtils.getAnnotation(method,
						RequiresPermissions.class);
				RequiresPermissionsDesc requiresPermissionsDesc = AnnotationUtils.getAnnotation(method,
						RequiresPermissionsDesc.class);

				if (requiresPermissions == null || requiresPermissionsDesc == null) {
					continue;
				}

				String api = "";
				if (clazzRequestMapping != null) {
					api = clazzRequestMapping.value()[0];
				}

				PostMapping postMapping = AnnotationUtils.getAnnotation(method, PostMapping.class);
				if (postMapping != null) {
					if (ArrayUtils.isNotEmpty(postMapping.value())) {
						api = "POST " + api + pathTransformation(postMapping.value()[0]);
						Permission permission = new Permission();
						permission.setRequiresPermissions(requiresPermissions);
						permission.setRequiresPermissionsDesc(requiresPermissionsDesc);
						permission.setApi(api);
						permissions.add(permission);
					}
					continue;
				}
				GetMapping getMapping = AnnotationUtils.getAnnotation(method, GetMapping.class);
				if (getMapping != null) {
					if (ArrayUtils.isNotEmpty(getMapping.value())) {
						api = "GET " + api + pathTransformation(getMapping.value()[0]);
						Permission permission = new Permission();
						permission.setRequiresPermissions(requiresPermissions);
						permission.setRequiresPermissionsDesc(requiresPermissionsDesc);
						permission.setApi(api);
						permissions.add(permission);
					}
					continue;
				}

				PutMapping putMapping = AnnotationUtils.getAnnotation(method, PutMapping.class);
				if (putMapping != null) {
					if (ArrayUtils.isNotEmpty(putMapping.value())) {
						api = "PUT " + api + pathTransformation(putMapping.value()[0]);
						Permission permission = new Permission();
						permission.setRequiresPermissions(requiresPermissions);
						permission.setRequiresPermissionsDesc(requiresPermissionsDesc);
						permission.setApi(api);
						permissions.add(permission);
					}
					continue;
				}

				DeleteMapping deleteMapping = AnnotationUtils.getAnnotation(method, DeleteMapping.class);
				if (deleteMapping != null) {
					if (ArrayUtils.isNotEmpty(deleteMapping.value())) {
						api = "DELETE " + api + pathTransformation(deleteMapping.value()[0]);
						Permission permission = new Permission();
						permission.setRequiresPermissions(requiresPermissions);
						permission.setRequiresPermissionsDesc(requiresPermissionsDesc);
						permission.setApi(api);
						permissions.add(permission);
					}
					continue;
				}

				// TODO
				// 这里只支持GetMapping注解或者PostMapping注解，应该进一步提供灵活性
				throw new RuntimeException(
						"目前权限管理应该在method的前面使用GetMapping注解或者PostMapping注解或者PutMapping注解或者DeleteMapping注解");
			}
		}
		return permissions;
	}

	public static Set<String> listPermissionString(List<Permission> permissions) {
		Set<String> permissionsString = new HashSet<>();
		for (Permission permission : permissions) {
			permissionsString.add(permission.getRequiresPermissions().value()[0]);
		}
		return permissionsString;
	}
}
