package com.graphai.framework.netty.support;

import static com.graphai.framework.netty.pojo.PojoEndpointServer.SESSION_KEY;

import org.springframework.core.MethodParameter;

import com.graphai.framework.netty.pojo.Session;

import io.netty.channel.Channel;

public class SessionMethodArgumentResolver implements MethodArgumentResolver {
    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return Session.class.isAssignableFrom(parameter.getParameterType());
    }

    @Override
    public Object resolveArgument(MethodParameter parameter, Channel channel, Object object) throws Exception {
        Session session = channel.attr(SESSION_KEY).get();
        return session;
    }
}
