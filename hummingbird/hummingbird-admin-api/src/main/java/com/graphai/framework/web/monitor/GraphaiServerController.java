package com.graphai.framework.web.monitor;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.server.Server;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/admin/monitor")
public class GraphaiServerController {
    /*
     * @RequiresPermissions("admin:monitor:server")
     *
     * @RequiresPermissionsDesc(menu={"系统监控", "服务器监控"}, button="查询")
     */
    @GetMapping("/server")
    public Object getInfo() throws Exception {
        Server server = new Server();
        server.copyTo();
        return ResponseUtil.ok(server);
    }
}
