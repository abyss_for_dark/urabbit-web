package com.graphai.framework.web.system;

import com.github.pagehelper.PageInfo;
import com.graphai.mall.db.domain.MallSystem;
import com.graphai.mall.db.service.sysconfig.MallSystemConfigService;
import com.graphai.validator.Order;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.lang.StringUtils;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.mr.util.HttpClientUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 系统参数
 */
@RestController
@RequestMapping("/admin/system")
@Validated
public class GraphaiSystemConfigController {

    private final Log logger = LogFactory.getLog(GraphaiSystemConfigController.class);

    @Autowired
    private MallSystemConfigService mallSystemConfigService;

    @Value("${weiRed.qrcodeUrl}")
    private String qrcodeUrl;

    /**
     * 查询系统参数列表
     */
    @GetMapping("/list")
    public Object list(@RequestParam(required = false) String keyName,
                       @RequestParam(required = false) String remarks,
                       @RequestParam(required = false) String type,
                       @RequestParam(required = false) String typeCode,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit,
                       @RequestParam(defaultValue = "id") String sort,
                       @Order @RequestParam(defaultValue = "asc") String order) {
        MallSystem mallSystem = new MallSystem();
        mallSystem.setKeyName(keyName);
        mallSystem.setRemarks(remarks);
        mallSystem.setType(type);
        mallSystem.setTypeCode(typeCode);
        List<MallSystem> list = mallSystemConfigService.list(mallSystem, page, limit, sort, order);
        long total = PageInfo.of(list).getTotal();
        Map<String, Object> data = new HashMap<>();
        data.put("total", total);
        data.put("items", list);
        return ResponseUtil.ok(data);
    }

    /**
     * 添加系统参数
     */
    @PostMapping("/add")
    public Object addSave(@RequestBody MallSystem mallSystem) {
        return ResponseUtil.ok(mallSystemConfigService.insertMallSystem(mallSystem));
    }

    /**
     * 查看系统参数详情
     */
    @GetMapping("/edit/{id}")
    public Object edit(@PathVariable("id") String id) {
        MallSystem mallSystem = mallSystemConfigService.selectMallSystemById(id);
        return ResponseUtil.ok(mallSystem);
    }

    /**
     * 修改系统参数
     */
    @PostMapping("/edit")
    public Object editSave(@RequestBody MallSystem mallSystem) {
        return ResponseUtil.ok(mallSystemConfigService.updateMallSystem(mallSystem));
    }



    /**
     * app审核状态
     */
    @RequiresPermissions("admin:system:appApply")
    @RequiresPermissionsDesc(menu = {"系统管理", "APP审核状态"}, button = "查询")
    @GetMapping("/appApply")
    public Object appApply() {
        Map<String, String> map = mallSystemConfigService.listAppApply();
        return ResponseUtil.ok(map);
    }
    /**
     * 修改app审核状态
     */
    @RequiresPermissions("admin:system:applyUpdate")
    @RequiresPermissionsDesc(menu = {"系统管理", "APP审核状态"}, button = "修改")
    @PostMapping("/applyUpdate")
    public Object applyUpdate(@RequestBody MallSystem mallSystem) {
        try {
            Map<String, String> map = new HashMap<>();
            map.put(mallSystem.getKeyName(), mallSystem.getKeyValue());
            mallSystemConfigService.updateConfig(map);

            // 清理缓存
            String status = "1";
            // APP是否审核(1审核状态 0正常使用)  mallSystem.getKeyValue()=1开启 mallSystem.getKeyValue()=1关闭
            if (StringUtils.isNotBlank(mallSystem.getKeyValue())) {
                if ("1".equals((mallSystem.getKeyValue()))) {
                    status = "1"; // 开启
                } else {
                    status = "0"; // 关闭
                }
            }
            if ("1".equals(status)) {
                if (StringUtils.isBlank(qrcodeUrl))
                    qrcodeUrl = "http://app.weilaylife.com";
                String reqUrl = qrcodeUrl + "/wx/home/clearCacheByKey?key=indexcate";
                logger.info("清理缓存请求地址："+ reqUrl);
                String rspStr = HttpClientUtil.doGet(reqUrl,"UTF-8");
                logger.info("清理缓存结果："+rspStr);
            }

            return ResponseUtil.ok();
        } catch (Exception ex) {
            logger.error("修改app审核状态出现异常"+ex.getMessage(),ex);
        }
        return ResponseUtil.ok();
    }

}
