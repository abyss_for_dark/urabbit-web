package com.graphai.framework.web.sms;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.lang.StringUtils;
import com.graphai.framework.generator.controller.IBaseController;
import com.graphai.framework.sms.entity.SmsTemplate;
import com.graphai.framework.sms.service.ISmsTemplateService;


/**
 * @version V1.0
 * @package com.graphai.framework.sms.controller
 * @title: 短信模板控制器
 * @description: 短信模板控制器
 * @author: biteam
 * @date: 2018-09-14 09:47:35
 */

@RestController
@RequestMapping("/admin/sms/template")
public class SmsTemplateController extends IBaseController<SmsTemplate,String> {

    @Autowired
    private ISmsTemplateService smsTemplateService;


    @GetMapping(value = "list")
    public void list( HttpServletRequest request) throws IOException {
        //加入条件
        QueryWrapper<SmsTemplate> entityWrapper = new QueryWrapper<>( );
        entityWrapper.orderByDesc("createDate");
        String code= request.getParameter("code");
        if (!StringUtils.isEmpty(code)){
            entityWrapper.like("code",code);
        }
        String name=request.getParameter("name");
        if (!StringUtils.isEmpty(name)){
            entityWrapper.like("name",name);
        }
        // 预处理
        IPage<SmsTemplate> ipage = smsTemplateService.page(getPlusPage(),entityWrapper);
        //FastJsonUtils.print(pageBean,SmsTemplate.class,"id,name,code,businessType,templateContent");
    }

    @PostMapping("add")
    public Object add(SmsTemplate entity, BindingResult result,
                        HttpServletRequest request, HttpServletResponse response) {
        // 验证错误
        this.checkError(entity,result);
        String templateCode = StringUtils.getRandomNum(10);
        entity.setCode(templateCode);
        smsTemplateService.save(entity);
        return ResponseUtil.ok("添加成功");
    }

    @PostMapping("{id}/update")
    public Object update(SmsTemplate entity, BindingResult result,
                           HttpServletRequest request, HttpServletResponse response) {
        // 验证错误
        this.checkError(entity,result);
        smsTemplateService.saveOrUpdate(entity);
        return ResponseUtil.ok("更新成功");
    }

    @PostMapping("{id}/delete")
    public Object delete(@PathVariable("id") String id) {
        smsTemplateService.removeById(id);
        return ResponseUtil.ok("删除成功");
    }

    
    @PostMapping("batch/delete")
    public Object batchDelete(@RequestParam("ids") String[] ids) {
        List<String> idList = java.util.Arrays.asList(ids);
        smsTemplateService.removeByIds(idList);
        return ResponseUtil.ok("删除成功");
    }
}