package com.graphai.framework.web.system;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.graphai.framework.system.vo.RegionVO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.system.domain.GraphaiSystemRegion;
import com.graphai.framework.system.service.IGraphaiSystemRegionService;
import com.graphai.mall.admin.vo.RegionVo;
import com.graphai.mall.db.domain.MallRegion;
import com.graphai.mall.db.service.common.MallRegionService;

@RestController
@RequestMapping("/admin/region")
@Validated
public class GraphaiSystemRegionController {
    private final Log logger = LogFactory.getLog(GraphaiSystemRegionController.class);

    @Autowired
    private MallRegionService mallRegionService;
    @Autowired
    private IGraphaiSystemRegionService regionService;

    @GetMapping("/clist")
    public Object clist(@NotNull String id) {
        List<MallRegion> regionList = mallRegionService.queryByPid(id);
        return ResponseUtil.ok(regionList);
    }

    @GetMapping("/list")
    public Object list() {
        List<RegionVo> regionVoList = new ArrayList<>();

        List<MallRegion> provinceList = mallRegionService.queryByPid("0");
        for (MallRegion province : provinceList) {
            RegionVo provinceVO = new RegionVo();
            provinceVO.setId(province.getId());
            provinceVO.setName(province.getName());
            provinceVO.setCode(province.getCode());
            provinceVO.setType(province.getType());

            List<MallRegion> cityList = mallRegionService.queryByPid(province.getId());
            List<RegionVo> cityVOList = new ArrayList<>();
            for (MallRegion city : cityList) {
                RegionVo cityVO = new RegionVo();
                cityVO.setId(city.getId());
                cityVO.setName(city.getName());
                cityVO.setCode(city.getCode());
                cityVO.setType(city.getType());

                List<MallRegion> areaList = mallRegionService.queryByPid(city.getId());
                List<RegionVo> areaVOList = new ArrayList<>();
                for (MallRegion area : areaList) {
                    RegionVo areaVO = new RegionVo();
                    areaVO.setId(area.getId());
                    areaVO.setName(area.getName());
                    areaVO.setCode(area.getCode());
                    areaVO.setType(area.getType());
                    areaVOList.add(areaVO);
                }

                cityVO.setChildren(areaVOList);
                cityVOList.add(cityVO);
            }
            provinceVO.setChildren(cityVOList);
            regionVoList.add(provinceVO);
        }

        return ResponseUtil.ok(regionVoList);
    }
    
   
    @PostMapping(value = "/sync")
    public void synchronizationData(String url) {
        regionService.synchronizationData(url);
    }

    @GetMapping(value = "/{id}")
    public Object get(@PathVariable String id) {
        return ResponseUtil.ok(regionService.getById(id));
    }

    @GetMapping(value = "/item/{id}")
    public Object getItem(@PathVariable String id) {
        return ResponseUtil.ok(regionService.getItem(id));
    }

    @PutMapping(value = "/{id}")
    public Object update(@PathVariable String id, @Valid GraphaiSystemRegion region) {
        region.setId(id);
        regionService.updateById(region);
        return ResponseUtil.ok(region);
    }


    @PostMapping
    public Object save(@Valid GraphaiSystemRegion region) {
        regionService.save(region);
        return ResponseUtil.ok(region);
    }

    @DeleteMapping(value = "{ids}")
    public Object delAllByIds(@PathVariable List<String> ids) {
        regionService.removeByIds(ids);
        return ResponseUtil.ok();
    }

    @GetMapping(value = "/allCity")
    public Object getAllCity() {
        return ResponseUtil.ok(regionService.getAllCity());
    }


}
