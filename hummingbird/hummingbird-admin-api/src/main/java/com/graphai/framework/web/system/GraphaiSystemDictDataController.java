package com.graphai.framework.web.system;

import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.mall.db.domain.MallDictData;
import com.graphai.mall.db.service.distribution.MallDictDataService;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 字典管理
 *
 * @author LaoGF
 * @since 2020-07-21
 */

@RestController
@RequestMapping("/admin/dict/data")
@Validated
public class GraphaiSystemDictDataController {
    @Resource
    private MallDictDataService mallDictDataService;

    /**
     * 列表
     */
    @RequiresPermissions("admin:dict:data:list")
    @RequiresPermissionsDesc(menu = {"系统管理", "字典管理"}, button = "查询")
    @GetMapping("/list")
    public Object list(@RequestParam(required = false) String dictName,
                       @RequestParam(required = false) String status,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit) {
        List<MallDictData> list = mallDictDataService.selectList(dictName, status, page, limit);
        long total = PageInfo.of(list).getTotal();
        Map<String, Object> data = new HashMap<>(16);
        data.put("total", total);
        data.put("items", list);
        return ResponseUtil.ok(data);
    }


    /**
     * 添加
     */
    @RequiresPermissions("admin:dict:data:add")
    @RequiresPermissionsDesc(menu = {"系统管理", "字典管理"}, button = "添加")
    @PostMapping("/add")
    public Object add(@RequestBody MallDictData mallDictData) {
        if (mallDictDataService.insert(mallDictData)) {
            return ResponseUtil.ok();
        }
        return ResponseUtil.fail();
    }

    /**
     * 修改
     */
    @RequiresPermissions("admin:dict:data:update")
    @RequiresPermissionsDesc(menu = {"系统管理", "字典管理"}, button = "修改")
    @PostMapping("/update")
    public Object update(@RequestBody MallDictData mallDictData) {
        if (mallDictDataService.update(mallDictData)) {
            return ResponseUtil.ok();
        }
        return ResponseUtil.fail();
    }

    /**
     * 删除
     */
    @RequiresPermissions("admin:dict:data:delete")
    @RequiresPermissionsDesc(menu = {"系统管理", "字典管理"}, button = "删除")
    @PostMapping("/delete")
    public Object delete(@RequestParam String id) {
        if (mallDictDataService.deleteByID(id)) {
            return ResponseUtil.ok();
        }
        return ResponseUtil.fail();
    }

    /**
     * 状态修改
     */
    @RequiresPermissions("admin:dict:data:updateStatus")
    @RequiresPermissionsDesc(menu = {"系统管理", "字典管理"}, button = "状态修改")
    @PostMapping("/updateStatus")
    public Object updateStatus(@RequestParam String id,
                               @RequestParam boolean state) {
        if (mallDictDataService.updateStatus(id, state)) {
            return ResponseUtil.ok();
        }
        return ResponseUtil.fail();
    }


    /**
     * 获取字典树
     */
    @GetMapping("/getDictTree")
    public Object getDictTree(@RequestParam(value = "dictName", required = false) String dictName,
                              @RequestParam(value = "dictType", required = false) String dictType) {
        return ResponseUtil.ok(mallDictDataService.getDictTree(dictName, dictType));
    }

    /**
     * 获取字典名称
     */
    @GetMapping("/getDictValue")
    public Object getDictValue(@RequestParam(value = "dictType", required = false) String dictType,
                               @RequestParam String dictKey) {
        return ResponseUtil.ok(mallDictDataService.getDictValue(dictType, dictKey));
    }


}
