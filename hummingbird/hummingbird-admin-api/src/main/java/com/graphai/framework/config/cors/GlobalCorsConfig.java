package com.graphai.framework.config.cors;

import java.util.Collections;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

/**
 * 使用CORS，用于解决ajax跨域访问问题
 */
@Configuration
public class GlobalCorsConfig {
    @Bean
    public FilterRegistrationBean<CorsFilter> corsFilter() {
        //1.添加CORS配置信息
        CorsConfiguration config = new CorsConfiguration();
        //1) 允许的域,不要写*，否则cookie就无法使用了
//        config.addAllowedOrigin("http://localhost:9527");
//        config.addAllowedOrigin("http://kmi.gxflow.cn");
//        config.addAllowedOrigin("https://kmi.gxflow.cn");
//        config.addAllowedOrigin("http://app.weilaylife.com");
//        config.addAllowedOrigin("https://app.weilaylife.com");
//        config.addAllowedOrigin("http://app2.weilaylife.com");
//        config.addAllowedOrigin("https://app2.weilaylife.com");
//        config.addAllowedOrigin("http://test.weilaylife.com");
//        config.addAllowedOrigin("https://test.weilaylife.com");
//        //2) 是否发送Cookie信息
//        config.setAllowCredentials(true);
//        //3) 允许的请求方式
//        config.addAllowedMethod("OPTIONS");
//        config.addAllowedMethod("HEAD");
//        config.addAllowedMethod("GET");
//        config.addAllowedMethod("PUT");
//        config.addAllowedMethod("POST");
//        config.addAllowedMethod("DELETE");
//        config.addAllowedMethod("PATCH");
        //1,允许任何来源
        config.setAllowedOriginPatterns(Collections.singletonList("*"));
        //2,允许任何请求头
        config.addAllowedHeader(CorsConfiguration.ALL);
        //3,允许任何方法
        config.addAllowedMethod(CorsConfiguration.ALL);
        //4,允许凭证
        config.setAllowCredentials(true);
        config.setMaxAge(3600L);
        // 4）允许的头信息
        config.addAllowedHeader("*");

        //2.添加映射路径，我们拦截一切请求
        UrlBasedCorsConfigurationSource configSource = new UrlBasedCorsConfigurationSource();
        configSource.registerCorsConfiguration("/**", config);

        //3.返回新的CorsFilter.
        //return new CorsFilter(configSource);

        FilterRegistrationBean<CorsFilter> bean = new FilterRegistrationBean<CorsFilter>(new CorsFilter(configSource));
        bean.setOrder(0);
        return bean;
    }
    
}
