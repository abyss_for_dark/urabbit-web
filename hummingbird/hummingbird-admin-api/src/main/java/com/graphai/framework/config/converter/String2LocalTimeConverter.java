package com.graphai.framework.config.converter;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import org.springframework.core.convert.converter.Converter;

import com.graphai.commons.util.lang.StringUtils;

public class String2LocalTimeConverter implements Converter<String, LocalTime> {
    @Override
    public LocalTime convert(String s) {
        if (StringUtils.isNotEmpty(s)) {
            DateTimeFormatter fmt = DateTimeFormatter.ofPattern("HH:mm:ss");
            return LocalTime.parse(s, fmt);
        }
        return null;
    }
}
