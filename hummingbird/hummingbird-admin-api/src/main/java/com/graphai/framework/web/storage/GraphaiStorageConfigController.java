package com.graphai.framework.web.storage;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.lang.StringUtils;
import com.graphai.commons.util.servlet.ServletUtils;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.framework.generator.controller.IBaseController;
import com.graphai.framework.netty.annotation.RequestParam;
import com.graphai.framework.storage.domain.GraphaiStorageConfig;
import com.graphai.framework.storage.service.IGraphaiStorageConfigService;

import lombok.extern.slf4j.Slf4j;



/**
 * <p>
 * 云存储配置 前端控制器
 * </p>
 *
 * @author biteam
 * @since 2021-05-25
 */
@Slf4j
@RestController
@RequestMapping("/admin/storage/config")
public class GraphaiStorageConfigController extends IBaseController<GraphaiStorageConfig,  String >  {

	@Autowired
    private IGraphaiStorageConfigService serviceImpl;
    
    @RequiresPermissions("admin:storage:config:single")
    @RequiresPermissionsDesc(menu = {"单独查询"}, button = "单独查询")
	@GetMapping("/single/{id}")
    public Object getEntityById(@PathVariable  String  id) throws Exception{
        GraphaiStorageConfig entityVName = serviceImpl.getById(id);
        return ResponseUtil.ok(entityVName);
    }
    
    @GetMapping("/single/cloudtype")
    public Object getEntityByCouldType(@RequestParam("cloudType")  String  cloudType) throws Exception{
        return ResponseUtil.ok( serviceImpl.getOne(new QueryWrapper<GraphaiStorageConfig>().eq("cloud_type", cloudType)));
    }
    
//    @RequiresPermissions("admin:storage:config:addOrUpdate")
//    @RequiresPermissionsDesc(menu = {"新增"}, button = "新增")
    @PostMapping("/addOrUpdate")
    public Object addOrUpdate(GraphaiStorageConfig graphaiStorageConfig) throws Exception{
        
        /**只允许存在一个默认的配置**/
        if(graphaiStorageConfig.isDefaultUse()) {
            GraphaiStorageConfig config = new GraphaiStorageConfig();
            config.setDefaultUse(false);
            serviceImpl.update(config,new UpdateWrapper<GraphaiStorageConfig>().eq("default_use", true));
        }
        if(serviceImpl.saveOrUpdate(graphaiStorageConfig)){
            return ResponseUtil.ok();
        }else{
            return ResponseUtil.fail();
        }
    }
	
    /**
     * 获取默认的配置
     * @return
     * @throws Exception
     */
    @GetMapping("/default")
    public Object getDefaultStorageConfig() throws Exception {
        return ResponseUtil.ok( serviceImpl.getOne(new QueryWrapper<GraphaiStorageConfig>().eq("default_use", true)));
    }
    
	
	@RequiresPermissions("admin:storage:config:melist")
	@RequiresPermissionsDesc(menu = {"云存储配置查询"}, button = "查询")
    @GetMapping("/melist")
    public Object getMeList() throws Exception {
        IPage<GraphaiStorageConfig> ipage = serviceImpl.page(getPlusPage(),listQueryWrapper());
        return ResponseUtil.ok(ipage);
    }
 	
 	private QueryWrapper<GraphaiStorageConfig> listQueryWrapper() {
 		QueryWrapper<GraphaiStorageConfig> meq = new QueryWrapper<>();
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("domain"))) {
		      meq.eq("domain", ServletUtils.getParameter("domain"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("prefix"))) {
		      meq.eq("prefix", ServletUtils.getParameter("prefix"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("accessKey"))) {
		      meq.eq("access_key", ServletUtils.getParameter("accessKey"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("secretKey"))) {
		      meq.eq("secret_key", ServletUtils.getParameter("secretKey"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("bucketName"))) {
		      meq.eq("bucket_name", ServletUtils.getParameter("bucketName"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("appid"))) {
		      meq.eq("appid", ServletUtils.getParameter("appid"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("region"))) {
		      meq.eq("region", ServletUtils.getParameter("region"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("storageStatus"))) {
		      meq.eq("storage_status", ServletUtils.getParameter("storageStatus"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("cloudType"))) {
		      meq.eq("cloud_type", ServletUtils.getParameter("cloudType"));
		}
		
        return meq;
    }
 	@RequiresPermissions("admin:storage:config:save")
 	@RequiresPermissionsDesc(menu = {"云存储配置新增"}, button = "新增")
    @PostMapping("/save")
    public Object save(@ModelAttribute GraphaiStorageConfig graphaiStorageConfig) throws Exception {
        serviceImpl.save(graphaiStorageConfig);
        return ResponseUtil.ok();
    }
	
	@RequiresPermissions("admin:storage:config:modify")
	@RequiresPermissionsDesc(menu = {"云存储配置修改"}, button = "修改")
    @PutMapping("/modify")
    public Object modify(@ModelAttribute  GraphaiStorageConfig graphaiStorageConfig){
        serviceImpl.updateById(graphaiStorageConfig);
        return  ResponseUtil.ok();
    }

    @RequiresPermissions("admin:storage:config:del")
    @RequiresPermissionsDesc(menu = {"云存储配置删除"}, button = "删除")
    @DeleteMapping("/del/{id}")
    public Object deleteById(@PathVariable  String  id){
        serviceImpl.removeById(id);
        return  ResponseUtil.ok();
    }

}

