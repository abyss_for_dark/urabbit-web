package com.graphai.framework.config;

import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

/**
 * 系统未运行完成前初始化部分数据
 *
 * @author biteam
 * @ClassName: FrameworkRunInitConfig
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date 2020-10-12
 */
@Component
public class FrameworkRunInitConfig {
    private final Log logger = LogFactory.getLog(FrameworkRunInitConfig.class);

    private static AtomicInteger counter = new AtomicInteger(0);

    public static String nextVal(String tableName) {
        if (counter.get() > 999999) {
            counter.set(1);
        }
        long time = System.currentTimeMillis();
        long returnValue = time * 100 + counter.incrementAndGet();
        return String.valueOf(returnValue);
    }

}
