package com.graphai.framework.netty.service;

import java.util.List;
import java.util.Map;

import com.graphai.mall.db.domain.MallNotice;

public interface PushService {

    /**
     * 推送给指定用户
     * 
     * @param userId 用户ID
     * @param msg 消息信息
     */
    void pushMsgToOne(Map<String, Object> msg);

    /**
     * 推送给所有用户
     * 
     * @param msg 消息信息
     */
    void pushMsgToAll(MallNotice msg);

    /**
     * 获取当前连接数
     * 
     * @return 连接数
     */
    int getConnectCount();

    /**
     */
    List<Map<String, Object>> getMsgList(String userId);

    void readMsg(String noticeId);

}
