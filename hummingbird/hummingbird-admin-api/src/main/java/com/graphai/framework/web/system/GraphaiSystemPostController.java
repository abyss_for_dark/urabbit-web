package com.graphai.framework.web.system;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.framework.system.domain.MallSysPost;
import com.graphai.framework.system.service.IGraphaiSystemPostService;
import com.graphai.mall.admin.util.AdminUserUtils;
import com.graphai.validator.Order;

/**
 * 岗位信息
 *
 */
@RestController
@RequestMapping("/admin/system/post")
public class GraphaiSystemPostController {

	@Autowired
	private IGraphaiSystemPostService mallSysPostService;

	/**
	 * 获取岗位列表
	 */
	@RequiresPermissions("admin:system:post:list")
	@RequiresPermissionsDesc(menu = { "岗位管理", "岗位管理" }, button = "查询")
	@GetMapping("/list")
	public Object list(@RequestParam(required = false) String postCode, @RequestParam(required = false) String postName,
			@RequestParam(required = false) String status, @RequestParam(defaultValue = "1") Integer page,
			@RequestParam(defaultValue = "10") Integer limit, @RequestParam(defaultValue = "post_id") String sort,
			@Order @RequestParam(defaultValue = "desc") String order) {
		List<MallSysPost> list = mallSysPostService.selectPostAll(postCode, postName, status, page, limit, sort, order);
		Map<String, Object> dataMap = new HashMap<>();
		long total = PageInfo.of(list).getTotal();
		dataMap.put("total", total);
		dataMap.put("items", list);
		return ResponseUtil.ok(dataMap);
	}

	/**
	 * 根据岗位编号获取详细信息
	 */
	@RequiresPermissionsDesc(menu = { "岗位管理", "岗位管理" }, button = "详情")
	@GetMapping(value = "/{postId}")
	public Object getInfo(@PathVariable String postId) {
		MallSysPost mallSysPost = mallSysPostService.selectPostById(postId);
		return ResponseUtil.ok(mallSysPost);
	}

	/**
	 * 新增岗位
	 */
	@RequiresPermissions("admin:system:post:add")
	@RequiresPermissionsDesc(menu = { "岗位管理", "岗位管理" }, button = "新增")
	@PostMapping("/add")
	public Object add(@Validated @RequestBody MallSysPost post) {
		post.setCreateBy(AdminUserUtils.getLoginUserName());
		return ResponseUtil.ok(mallSysPostService.addPost(post));
	}

	/**
	 * 修改岗位
	 */
	@RequiresPermissions("admin:system:post:edit")
	@RequiresPermissionsDesc(menu = { "岗位管理", "岗位管理" }, button = "修改")
	@PutMapping("/edit")
	public Object edit(@Validated @RequestBody MallSysPost post) {
		post.setUpdateBy(AdminUserUtils.getLoginUserName());
		return ResponseUtil.ok(mallSysPostService.updatePost(post));
	}

	/**
	 * 删除岗位
	 */
	@RequiresPermissions("admin:system:post:remove")
	@RequiresPermissionsDesc(menu = { "岗位管理", "岗位管理" }, button = "删除")
	@DeleteMapping("/remove/{postIds}")
	public Object remove(@PathVariable String[] postIds) {
		return ResponseUtil.ok(mallSysPostService.deletePost(postIds));
	}

	@GetMapping("/export")
	public Object export(@RequestParam(defaultValue = "1") Integer page,
			@RequestParam(defaultValue = "10") Integer limit, @RequestParam(defaultValue = "id") String sort,
			@Order @RequestParam(defaultValue = "desc") String order) {

		return ResponseUtil.ok();
	}
}
