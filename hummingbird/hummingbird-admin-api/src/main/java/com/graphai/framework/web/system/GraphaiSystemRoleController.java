package com.graphai.framework.web.system;

import static com.graphai.framework.constant.AdminResponseCode.ROLE_NAME_EXIST;
import static com.graphai.framework.constant.AdminResponseCode.ROLE_USER_EXIST;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.constraints.NotNull;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.json.JacksonUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.framework.constant.AdminResponseCode;
import com.graphai.framework.dto.Permission;
import com.graphai.framework.system.service.IGraphaiSystemRoleService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.github.pagehelper.PageInfo;
import com.graphai.framework.system.service.MallRoleService;
import com.graphai.framework.util.PermissionUtil;
import com.graphai.mall.admin.vo.PermVo;
import com.graphai.mall.db.domain.MallAdmin;
import com.graphai.mall.db.domain.MallPermission;
import com.graphai.mall.db.domain.MallRole;
import com.graphai.mall.db.service.MallAdminService;
import com.graphai.mall.db.service.MallPermissionService;
import com.graphai.validator.Order;
import com.graphai.validator.Sort;

@RestController
@RequestMapping("/admin/role")
@Validated
public class GraphaiSystemRoleController {
	private final Log logger = LogFactory.getLog(GraphaiSystemRoleController.class);

	@Autowired
	private MallRoleService roleService;
	@Autowired
	private MallPermissionService permissionService;
	@Autowired
	private MallAdminService adminService;
	@Autowired
	private IGraphaiSystemRoleService iGraphaiSystemRoleService;

	@RequiresPermissions("admin:role:list")
	@RequiresPermissionsDesc(menu = { "系统管理", "角色管理" }, button = "角色查询")
	@GetMapping("/list")
	public Object list(String name, @RequestParam(defaultValue = "1") Integer page,
			@RequestParam(defaultValue = "10") Integer limit,
			@Sort @RequestParam(defaultValue = "add_time") String sort,
			@Order @RequestParam(defaultValue = "desc") String order) {
		List<MallRole> roleList = roleService.querySelective(name, page, limit, sort, order);
		long total = PageInfo.of(roleList).getTotal();
		Map<String, Object> data = new HashMap<>();
		data.put("total", total);
		data.put("items", roleList);

		return ResponseUtil.ok(data);
	}

	@GetMapping("/options")
	public Object options() {

		Subject currentUser = SecurityUtils.getSubject();
		MallAdmin admin = (MallAdmin) currentUser.getPrincipal();

		List<MallRole> roleList = roleService.queryAllExitAdmin(admin);

		List<Map<String, Object>> options = new ArrayList<>(roleList.size());
		for (MallRole role : roleList) {
			Map<String, Object> option = new HashMap<>(2);
			option.put("value", role.getId());
			option.put("label", role.getName());
			options.add(option);
		}

		return ResponseUtil.ok(options);
	}

	@GetMapping("/optionsList")
	public Object optionsList() {
		List<MallRole> roleList = roleService.queryAll();

		List<Map<String, Object>> options = new ArrayList<>(roleList.size());
		for (MallRole role : roleList) {
			Map<String, Object> option = new HashMap<>(2);
			option.put("value", role.getId());
			option.put("label", role.getName());
			options.add(option);
		}

		return ResponseUtil.ok(options);
	}

	@RequiresPermissions("admin:role:read")
	@RequiresPermissionsDesc(menu = { "系统管理", "角色管理" }, button = "角色详情")
	@GetMapping("/read")
	public Object read(@NotNull String id) {
		MallRole role = roleService.findById(id);
		return ResponseUtil.ok(role);
	}

	private Object validate(MallRole role) {
		String name = role.getName();
		if (StringUtils.isEmpty(name)) {
			return ResponseUtil.badArgument();
		}

		return null;
	}

	@RequiresPermissions("admin:role:create")
	@RequiresPermissionsDesc(menu = { "系统管理", "角色管理" }, button = "角色添加")
	@PostMapping("/create")
	public Object create(@RequestBody MallRole role) {
		Object error = validate(role);
		if (error != null) {
			return error;
		}

		if (roleService.checkExist(role.getName())) {
			return ResponseUtil.fail(ROLE_NAME_EXIST, "角色已经存在");
		}

		roleService.add(role);

		return ResponseUtil.ok(role);
	}

	@RequiresPermissions("admin:role:update")
	@RequiresPermissionsDesc(menu = { "系统管理", "角色管理" }, button = "角色编辑")
	@PostMapping("/update")
	public Object update(@RequestBody MallRole role) {
		Object error = validate(role);
		if (error != null) {
			return error;
		}

		roleService.updateRole(role);
		return ResponseUtil.ok();
	}

	@RequiresPermissions("admin:role:delete")
	@RequiresPermissionsDesc(menu = { "系统管理", "角色管理" }, button = "角色删除")
	@PostMapping("/delete")
	public Object delete(@RequestBody MallRole role) {
		String id = role.getId();
		if (id == null) {
			return ResponseUtil.badArgument();
		}

		// 如果当前角色所对应管理员仍存在，则拒绝删除角色。
		List<MallAdmin> adminList = adminService.all();
		for (MallAdmin admin : adminList) {
			String[] roleIds = admin.getRoleIds();
			for (String roleId : roleIds) {
				if (id.equals(roleId)) {
					return ResponseUtil.fail(ROLE_USER_EXIST, "当前角色存在管理员，不能删除");
				}
			}
		}

		roleService.deleteById(id);
		return ResponseUtil.ok();
	}

	@Autowired
	private ApplicationContext context;
	private List<PermVo> systemPermissions = null;
	private Set<String> systemPermissionsString = null;

	private List<PermVo> getSystemPermissions() {
		final String basicPackage = "com.graphai.mall.admin";
		if (systemPermissions == null) {
			List<Permission> permissions = PermissionUtil.listPermission(context, basicPackage);
			systemPermissions = PermissionUtil.listPermVo(permissions);
			systemPermissionsString = PermissionUtil.listPermissionString(permissions);
		}
		return systemPermissions;
	}

	private Set<String> getAssignedPermissions(String roleId) {
		// 这里需要注意的是，如果存在超级权限*，那么这里需要转化成当前所有系统权限。
		// 之所以这么做，是因为前端不能识别超级权限，所以这里需要转换一下。
		Set<String> assignedPermissions = null;
		if (permissionService.checkSuperPermission(roleId)) {
			getSystemPermissions();
			assignedPermissions = systemPermissionsString;
		} else {
			assignedPermissions = permissionService.queryByRoleId(roleId);
		}

		return assignedPermissions;
	}

	/**
	 * 管理员的权限情况
	 *
	 * @return 系统所有权限列表和管理员已分配权限
	 */
	@RequiresPermissions("admin:role:permission:get")
	@RequiresPermissionsDesc(menu = { "系统管理", "角色管理" }, button = "权限详情")
	@GetMapping("/permissions")
	public Object getPermissions(String roleId) {
		List<PermVo> systemPermissions = getSystemPermissions();
		Set<String> assignedPermissions = getAssignedPermissions(roleId);

		Map<String, Object> data = new HashMap<>();
		data.put("systemPermissions", systemPermissions);
		data.put("assignedPermissions", assignedPermissions);
		return ResponseUtil.ok(data);
	}

	/**
	 * 更新管理员的权限
	 *
	 * @param body
	 * @return
	 */
	@RequiresPermissions("admin:role:permission:update")
	@RequiresPermissionsDesc(menu = { "系统管理", "角色管理" }, button = "权限变更")
	@PostMapping("/permissions")
	public Object updatePermissions(@RequestBody String body) {
		String roleId = JacksonUtil.parseString(body, "roleId");
		List<String> permissions = JacksonUtil.parseStringList(body, "permissions");
		if (roleId == null || permissions == null) {
			return ResponseUtil.badArgument();
		}

		// 如果修改的角色是超级权限，则拒绝修改。
		if (permissionService.checkSuperPermission(roleId)) {
			return ResponseUtil.fail(AdminResponseCode.ROLE_SUPER_SUPERMISSION, "当前角色的超级权限不能变更");
		}

		// 先删除旧的权限，再更新新的权限
		permissionService.deleteByRoleId(roleId);
		for (String permission : permissions) {
			MallPermission MallPermission = new MallPermission();
			MallPermission.setRoleId(roleId);
			MallPermission.setPermission(permission);
			permissionService.add(MallPermission);
		}
		return ResponseUtil.ok();
	}

	/**
	 * 修改保存数据权限
	 *
	 * @param role
	 * @return
	 */
	@RequiresPermissions("admin:role:datascope")
	@RequiresPermissionsDesc(menu = { "系统管理", "角色管理" }, button = "数据权限")
	@PostMapping("/datascope")
	public Object updateDataScope(@RequestBody MallRole role) {
		iGraphaiSystemRoleService.checkRoleAllowed(role);
		return ResponseUtil.ok(iGraphaiSystemRoleService.authDataScope(role));
	}



}
