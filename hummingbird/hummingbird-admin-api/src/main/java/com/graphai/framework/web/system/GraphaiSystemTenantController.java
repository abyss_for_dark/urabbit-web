package com.graphai.framework.web.system;

import cn.afterturn.easypoi.excel.entity.ExportParams;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.lang.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.graphai.commons.util.servlet.ServletUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import org.springframework.web.bind.annotation.RestController;
import com.graphai.framework.generator.controller.IBaseController;

import com.graphai.framework.system.domain.GraphaiSystemTenant;
import com.graphai.framework.system.service.IGraphaiSystemTenantService;
import java.util.List;



/**
 * <p>
 * 多租户信息表 前端控制器
 * </p>
 *
 * @author biteam
 * @since 2021-04-09
 */
@RestController
@RequestMapping("/admin/tenant")
public class GraphaiSystemTenantController extends IBaseController<GraphaiSystemTenant,  Long >  {

	@Autowired
    private IGraphaiSystemTenantService serviceImpl;
    
    @RequiresPermissions("admin:tenant:single")
    @RequiresPermissionsDesc(menu = {"单独查询"}, button = "单独查询")
	@GetMapping("/single/{id}")
    public Object getEntityById(@PathVariable  Long  id) throws Exception{
        GraphaiSystemTenant entityVName = serviceImpl.getById(id);
        return ResponseUtil.ok(entityVName);
    }
    
    @RequiresPermissions("admin:tenant:addOrUpdate")
    @RequiresPermissionsDesc(menu = {"新增"}, button = "新增")
    @PostMapping("/addOrUpdate")
    public Object addOrUpdate(GraphaiSystemTenant graphaiSystemTenant) throws Exception{
        if(serviceImpl.saveOrUpdate(graphaiSystemTenant)){
            return ResponseUtil.ok();
        }else{
            return ResponseUtil.fail();
        }
    } 
	
	
	@RequiresPermissions("admin:tenant:melist")
	@RequiresPermissionsDesc(menu = {"多租户信息表查询"}, button = "查询")
    @GetMapping("/melist")
    public Object getMeList() throws Exception {
        IPage<GraphaiSystemTenant> ipage = serviceImpl.page(getPlusPage(),listQueryWrapper());
        return ResponseUtil.ok(ipage);
    }
 	
 	private QueryWrapper<GraphaiSystemTenant> listQueryWrapper() {
 		QueryWrapper<GraphaiSystemTenant> meq = new QueryWrapper<>();
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("name"))) {
		      meq.eq("name", ServletUtils.getParameter("name"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("beginDate"))) {
		      meq.eq("begin_date", ServletUtils.getLocalDateTime("beginDate"));  
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("endDate"))) {
		      meq.eq("end_date", ServletUtils.getLocalDateTime("endDate"));  
		}
		
        return meq;
    }
 	@RequiresPermissions("admin:tenant:save")
 	@RequiresPermissionsDesc(menu = {"多租户信息表新增"}, button = "新增")
    @PostMapping("/save")
    public Object save(@ModelAttribute GraphaiSystemTenant graphaiSystemTenant) throws Exception {
        serviceImpl.save(graphaiSystemTenant);
        return ResponseUtil.ok();
    }
	
	@RequiresPermissions("admin:tenant:modify")
	@RequiresPermissionsDesc(menu = {"多租户信息表修改"}, button = "修改")
    @PutMapping("/modify")
    public Object modify(@ModelAttribute  GraphaiSystemTenant graphaiSystemTenant){
        serviceImpl.updateById(graphaiSystemTenant);
        return  ResponseUtil.ok();
    }

    @RequiresPermissions("admin:tenant:del")
    @RequiresPermissionsDesc(menu = {"多租户信息表删除"}, button = "删除")
    @DeleteMapping("/del/{id}")
    public Object deleteById(@PathVariable  Long  id){
        serviceImpl.removeById(id);
        return  ResponseUtil.ok();
    }

}

