package com.graphai.framework.web.system;

import static com.graphai.framework.constant.AdminResponseCode.ADMIN_DELETE_NOT_ALLOWED;
import static com.graphai.framework.constant.AdminResponseCode.ADMIN_INVALID_NAME;
import static com.graphai.framework.constant.AdminResponseCode.ADMIN_INVALID_PASSWORD;
import static com.graphai.framework.constant.AdminResponseCode.ADMIN_NAME_EXIST;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.RegexUtil;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.bcrypt.BCryptPasswordEncoder;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.framework.system.service.MallRoleService;
import com.graphai.mall.admin.service.LogHelper;
import com.graphai.mall.db.domain.MallAdmin;
import com.graphai.mall.db.domain.MallRole;
import com.graphai.mall.db.service.MallAdminService;
import com.graphai.mall.db.vo.MallAdminVo;
import com.graphai.validator.Order;
import com.graphai.validator.Sort;

@RestController
@RequestMapping("/admin/admin")
@Validated
public class GraphaiSystemAdminController {
    private final Log logger = LogFactory.getLog(GraphaiSystemAdminController.class);

    @Autowired
    private MallAdminService adminService;
    @Autowired
    private LogHelper logHelper;
    @Resource
    private MallRoleService mallRoleService;

    @RequiresPermissions("admin:admin:list")
    @RequiresPermissionsDesc(menu = {"系统管理", "管理员管理"}, button = "查询")
    @GetMapping("/list")
    public Object list(String username, String role, String mobile,String deptId,String deptName,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit,
                       @Sort @RequestParam(defaultValue = "id") String sort,
                       @Order @RequestParam(defaultValue = "desc") String order) {

//        List<MallAdmin> adminList = adminService.querySelective(mobile, role, username, page, limit, sort, order);
        List<MallAdminVo> adminList = adminService.querySelectivePlus(mobile, role, username, page, limit, sort, order, deptId, deptName);
        long total = PageInfo.of(adminList).getTotal();
        Map<String, Object> data = new HashMap<>();
        data.put("total", total);
        data.put("items", adminList);

        return ResponseUtil.ok(data);
    }

    private Object validate(MallAdmin admin) {
        String name = admin.getUsername();
        String mobile = admin.getMobile();
        if (StringUtils.isEmpty(name)) {
            return ResponseUtil.badArgument();
        }
        if (!RegexUtil.isUsername(name)) {
            return ResponseUtil.fail(ADMIN_INVALID_NAME, "管理员名称不符合规定");
        }
        if (!RegexUtil.isMobileExact(mobile)) {
            return ResponseUtil.fail(403, "手机号码格式不符合");
        }
        String password = admin.getPassword();
        if (StringUtils.isEmpty(password) || password.length() < 6) {
            return ResponseUtil.fail(ADMIN_INVALID_PASSWORD, "管理员密码长度不能小于6");
        }
        return null;
    }

    @RequiresPermissions("admin:admin:create")
    @RequiresPermissionsDesc(menu = {"系统管理", "管理员管理"}, button = "添加")
    @PostMapping("/create")
    public Object create(@RequestBody MallAdmin admin) {
        Object error = validate(admin);
        if (error != null) {
            return error;
        }

        String username = admin.getUsername();
        List<MallAdmin> adminList = adminService.findAdminByUserName(username);
        if (adminList.size() > 0) {
            return ResponseUtil.fail(ADMIN_NAME_EXIST, "管理员已经存在");
        }
        MallAdmin mallAdmin = adminService.findAdminByAliasName(admin.getAliasName());
        if (mallAdmin != null) {
            return ResponseUtil.fail(ADMIN_NAME_EXIST, "管理员别名已存在");
        }

        String roleIds[] = admin.getRoleIds();
        if (roleIds.length > 1) {
            return ResponseUtil.fail(403, "一位管理员只能绑定一个角色");
        }

        //超级系统管理员只能存在一个，不能创建多个
        String currentRoleId = roleIds[0];
        MallRole role = mallRoleService.findById(currentRoleId);
        if (role != null) {
            if (role.getType().equals(false)) {
                return ResponseUtil.fail(ADMIN_DELETE_NOT_ALLOWED, "超级系统管理员已存在，只能有一个，不能创建多个");
            }
        }

        String rawPassword = admin.getPassword();
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String encodedPassword = encoder.encode(rawPassword);
        admin.setPassword(encodedPassword);
        // 新增系统用户
        adminService.add(admin);
        // 新增系统用户与角色关联
        adminService.insertUserRole(admin);
        logHelper.logAuthSucceed("添加管理员", username);
        return ResponseUtil.ok(admin);
    }
    
    public static void main(String[] args) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String encodedPassword = encoder.encode("123456");
        System.out.println(encodedPassword);
    }
    
    @RequiresPermissions("admin:admin:read")
    @RequiresPermissionsDesc(menu = {"系统管理", "管理员管理"}, button = "详情")
    @GetMapping("/read")
    public Object read(@NotNull String id) {
        MallAdmin admin = adminService.findById(id);
        return ResponseUtil.ok(admin);
    }

    @RequiresPermissions("admin:admin:update")
    @RequiresPermissionsDesc(menu = {"系统管理", "管理员管理"}, button = "编辑")
    @PostMapping("/update")
    public Object update(@RequestBody MallAdmin admin) {
        Object error = validate(admin);
        if (error != null) {
            return error;
        }

        MallAdmin mallAdmin = adminService.findAdminByAliasName(admin.getAliasName());
        if (mallAdmin != null) {
            if (!admin.getId().equals(mallAdmin.getId())) {
                return ResponseUtil.fail(ADMIN_NAME_EXIST, "管理员别名已存在");
            }
        }

        String anotherAdminId = admin.getId();
        if (anotherAdminId == null) {
            return ResponseUtil.badArgument();
        }

        // 不允许管理员通过编辑接口修改密码
        admin.setPassword(null);

        String roleIds[] = admin.getRoleIds();
        if (roleIds.length > 1) {
            return ResponseUtil.fail(403, "一位管理员只能绑定一个角色");
        }
        
        //超级系统管理员只能存在一个，不能创建多个
        String currentRoleId = roleIds[0];
        MallRole role = mallRoleService.findById(currentRoleId);
        if (role != null) {
            if (role.getType().equals(false)) {
                return ResponseUtil.fail(ADMIN_DELETE_NOT_ALLOWED, "超级系统管理员已存在，只能有一个，不能创建多个");
            }
        }

        if (adminService.updateAdminPerms(admin) == 0) {
            return ResponseUtil.updatedDataFailed();
        }

        logHelper.logAuthSucceed("编辑管理员", admin.getUsername());
        return ResponseUtil.ok(admin);
    }

    @RequiresPermissions("admin:admin:delete")
    @RequiresPermissionsDesc(menu = {"系统管理", "管理员管理"}, button = "删除")
    @PostMapping("/delete")
    public Object delete(@RequestBody MallAdmin admin) {
        String anotherAdminId = admin.getId();
        if (anotherAdminId == null) {
            return ResponseUtil.badArgument();
        }
        
        // 管理员不能删除自身账号
        Subject currentUser = SecurityUtils.getSubject();
        MallAdmin currentAdmin = (MallAdmin) currentUser.getPrincipal();
        if (currentAdmin.getId().equals(anotherAdminId)) {
            return ResponseUtil.fail(ADMIN_DELETE_NOT_ALLOWED, "管理员不能删除自己账号");
        }

        String roleIds[] = admin.getRoleIds();
        String roleId = roleIds[0];
        MallRole role = mallRoleService.findById(roleId);
        if (role != null) {
            if (role.getType().equals(false)) {
                return ResponseUtil.fail(ADMIN_DELETE_NOT_ALLOWED, "超级系统管理员不能删除");
            }
        }

        adminService.deleteById(anotherAdminId);
        logHelper.logAuthSucceed("删除管理员", admin.getUsername());
        return ResponseUtil.ok();
    }
}
