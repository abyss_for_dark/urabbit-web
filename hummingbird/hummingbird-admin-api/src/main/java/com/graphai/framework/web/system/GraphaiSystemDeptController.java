package com.graphai.framework.web.system;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.graphai.commons.constant.UserConstants;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.framework.system.domain.GraphaiSystemDept;
import com.graphai.framework.system.service.IGraphaiSystemDeptService;
import com.graphai.id.GraphaiIdGenerator;
import com.graphai.mall.admin.util.AdminUserUtils;
import com.graphai.mall.db.util.StringUtils;

/**
 * 部门信息
 */
@RestController
@RequestMapping("/admin/system/dept")
public class GraphaiSystemDeptController {
	@Autowired
	private IGraphaiSystemDeptService deptService;

	/**
	 * 获取部门列表
	 */
	@RequiresPermissions("admin:system:dept:list")
	@RequiresPermissionsDesc(menu = { "部门管理", "部门管理" }, button = "查询")
	@GetMapping("/list")
	public Object list(GraphaiSystemDept dept) {
		List<GraphaiSystemDept> depts = deptService.selectDeptList(dept);
		return ResponseUtil.ok(depts);
	}

	/**
	 * 查询部门列表（排除节点）
	 */
	@GetMapping("/list/exclude/{deptId}")
	public Object excludeChild(@PathVariable(value = "deptId", required = false) String deptId) {
		List<GraphaiSystemDept> depts = deptService.selectDeptList(new GraphaiSystemDept());
		Iterator<GraphaiSystemDept> it = depts.iterator();
		while (it.hasNext()) {
			GraphaiSystemDept d = (GraphaiSystemDept) it.next();
			if (d.getDeptId() == deptId || ArrayUtils.contains(StringUtils.split(d.getAncestors(), ","), deptId + "")) {
				it.remove();
			}
		}
		return ResponseUtil.ok(depts);
	}

	/**
	 * 根据部门编号获取详细信息
	 */
	@GetMapping(value = "/{deptId}")
	public Object getInfo(@PathVariable String deptId) {
		return ResponseUtil.ok(deptService.selectDeptById(deptId));
	}

	/**
	 * 获取部门下拉树列表
	 */
	@GetMapping("/treeselect")
	public Object treeselect(GraphaiSystemDept dept) {
		List<GraphaiSystemDept> depts = deptService.selectDeptList(dept);
		return ResponseUtil.ok(deptService.buildDeptTreeSelect(depts));
	}

	/**
	 * 加载对应角色部门列表树
	 */
	@GetMapping(value = "/roleDeptTreeselect/{roleId}")
	public Object roleDeptTreeselect(@PathVariable("roleId") String roleId) {
		List<GraphaiSystemDept> depts = deptService.selectDeptList(new GraphaiSystemDept());
		Map<String, Object> map = new HashMap<>();
		map.put("checkedKeys", deptService.selectDeptListByRoleId(roleId));
		map.put("depts", deptService.buildDeptTreeSelect(depts));
		return ResponseUtil.ok(map);
	}

	/**
	 * 新增部门
	 */
	@RequiresPermissions("admin:system:dept:add")
	@RequiresPermissionsDesc(menu = { "部门管理", "部门管理" }, button = "新增")
	@PostMapping("/add")
	public Object add(@Validated @RequestBody GraphaiSystemDept dept) {
		if (UserConstants.NOT_UNIQUE.equals(deptService.checkDeptNameUnique(dept))) {
			return ResponseUtil.fail("新增部门'" + dept.getDeptName() + "'失败，部门名称已存在");
		}
		dept.setDeptId(GraphaiIdGenerator.nextId("GraphaiSystemDept"));
		dept.setCreateBy(AdminUserUtils.getLoginUserName());
		dept.setCreateTime(new Date());
		return ResponseUtil.ok(deptService.insertDept(dept));
	}

	/**
	 * 修改部门
	 */
	@RequiresPermissions("admin:system:dept:edit")
	@RequiresPermissionsDesc(menu = { "部门管理", "部门管理" }, button = "编辑")
	@PutMapping("/edit")
	public Object edit(@Validated @RequestBody GraphaiSystemDept dept) {
		if (UserConstants.NOT_UNIQUE.equals(deptService.checkDeptNameUnique(dept))) {
			return ResponseUtil.fail(403, "修改部门'" + dept.getDeptName() + "'失败，部门名称已存在");
		} else if (dept.getParentId().equals(dept.getDeptId())) {
			return ResponseUtil.fail(403, "修改部门'" + dept.getDeptName() + "'失败，上级部门不能是自己");
		} else if (StringUtils.equals(UserConstants.DEPT_DISABLE, dept.getStatus())
				&& deptService.selectNormalChildrenDeptById(dept.getDeptId()) > 0) {
			return ResponseUtil.fail(403, "该部门包含未停用的子部门！");
		}
		dept.setUpdateBy(AdminUserUtils.getLoginUserName());
		dept.setUpdateTime(new Date());
		return ResponseUtil.ok(deptService.updateDept(dept));
	}

	/**
	 * 删除部门
	 */
	@RequiresPermissions("admin:system:dept:remove")
	@RequiresPermissionsDesc(menu = { "部门管理", "部门管理" }, button = "删除")
	@DeleteMapping("/remove/{deptId}")
	public Object remove(@PathVariable String deptId) {
		if (deptService.hasChildByDeptId(deptId)) {
			return ResponseUtil.fail("存在下级部门,不允许删除");
		}
		if (deptService.checkDeptExistUser(deptId)) {
			return ResponseUtil.fail("部门存在用户,不允许删除");
		}
		return ResponseUtil.ok(deptService.deleteDeptById(deptId));
	}
}
