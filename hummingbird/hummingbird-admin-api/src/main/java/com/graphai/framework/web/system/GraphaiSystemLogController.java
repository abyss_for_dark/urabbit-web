package com.graphai.framework.web.system;

import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.mall.db.domain.MallLog;
import com.graphai.mall.db.service.common.MallLogService;
import com.graphai.validator.Order;
import com.graphai.validator.Sort;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/admin/log")
@Validated
public class GraphaiSystemLogController {
    private final Log logger = LogFactory.getLog(GraphaiSystemLogController.class);

    @Autowired
    private MallLogService logService;

    @RequiresPermissions("admin:log:list")
    @RequiresPermissionsDesc(menu = {"系统管理", "操作日志"}, button = "查询")
    @GetMapping("/list")
    public Object list(String name,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit,
                       @Sort @RequestParam(defaultValue = "id") String sort,
                       @Order @RequestParam(defaultValue = "desc") String order) {
        List<MallLog> logList = logService.querySelective(name, page, limit, sort, order);
        long total = PageInfo.of(logList).getTotal();
        Map<String, Object> data = new HashMap<>();
        data.put("total", total);
        data.put("items", logList);

        return ResponseUtil.ok(data);
    }
}
