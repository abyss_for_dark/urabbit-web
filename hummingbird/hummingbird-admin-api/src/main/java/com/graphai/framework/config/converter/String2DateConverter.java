package com.graphai.framework.config.converter;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.core.convert.converter.Converter;

import com.graphai.commons.util.lang.StringUtils;

import lombok.SneakyThrows;

public class String2DateConverter implements Converter<String, Date> {
    @SneakyThrows
    @Override
    public Date convert(String s) {
        if (StringUtils.isNotEmpty(s)) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            return sdf.parse(s);
        }
        return null;
    }
}
