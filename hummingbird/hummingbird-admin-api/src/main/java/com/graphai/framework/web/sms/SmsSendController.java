package com.graphai.framework.web.sms;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Maps;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.lang.StringUtils;
import com.graphai.framework.sms.service.ISmsSendService;

/**
 * @title 短信发送
 * @description 短信发送
 * @author biteam
 * @date 2017-06-08 12:56:37
 * @version V1.0
 *
 */
@RestController
@RequestMapping("/admin/sms/send")
public class SmsSendController  {
	@Autowired
	private ISmsSendService smsSendService;

	@PostMapping(value = "/sendSmsByCode")
	public Object sendSmsByCode(HttpServletRequest request,
								  @RequestParam("phone") String phone,
								  @RequestParam("code") String code,
								  @RequestParam("data") String data) {
		try {
			String[] phones=phone.split(",");
			if (!StringUtils.isEmpty(data)) {
				smsSendService.send(phones, code, JSON.parseObject(StringEscapeUtils.unescapeHtml4(data),Map.class));
			} else {
				smsSendService.send(phones, code, Maps.newHashMap());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseUtil.fail("短信发送失败");
		}
		return  ResponseUtil.ok("短信发送成功");
	}

	@PostMapping(value = "/massSendSmsByCode")
	public Object massSendSmsByCode(HttpServletRequest request, HttpServletResponse response) {
		try {
			String phone = request.getParameter("phone");
			String code = request.getParameter("code");
			String data = request.getParameter("data");
			String[] phones=phone.split(",");
			if (!StringUtils.isEmpty(data)) {
				smsSendService.send(phones, code, JSON.parseObject(StringEscapeUtils.unescapeHtml4(data),Map.class));
			} else {
				smsSendService.send(phones, code, Maps.newHashMap());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseUtil.fail("短信发送失败");
		}
		return ResponseUtil.ok("短信发送成功");
	}
}