package com.graphai.framework.security.redis.serializer;

import com.graphai.framework.security.redis.exception.SerializationException;

public interface RedisSerializer<T> {

    byte[] serialize(T t) throws SerializationException;

    T deserialize(byte[] bytes) throws SerializationException;
}
