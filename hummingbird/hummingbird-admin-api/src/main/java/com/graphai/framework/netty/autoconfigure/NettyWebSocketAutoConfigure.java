package com.graphai.framework.netty.autoconfigure;

import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

// @Configuration
// @EnableWebSocketMessageBroker
//// @EnableWebSocket
// @EnableCaching
// @CrossOrigin("*")
public class NettyWebSocketAutoConfigure implements WebSocketMessageBrokerConfigurer {
    @Override
    public void configureMessageBroker(MessageBrokerRegistry config) {
        /**
         * 配置消息代理 启动简单Broker，消息的发送的地址符合配置的前缀来的消息才发送到这个broker
         */
        config.enableSimpleBroker("/topic");
        // config.setUserDestinationPrefix("/user");
        // config.setApplicationDestinationPrefixes("/admin");
    }

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        /**
         * 注册 Stomp的端点 addEndpoint：添加STOMP协议的端点。这个HTTP URL是供WebSocket或SockJS客户端访问的地址 setAllowedOrigins("*")
         * 允许跨域 withSockJS：指定端点使用SockJS协议
         */
        registry.addEndpoint("*").setAllowedOrigins("*").withSockJS();
    }
}
