package com.graphai.framework.web.system;

import static com.graphai.framework.constant.AdminResponseCode.ADMIN_INVALID_ACCOUNT;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.management.relation.Role;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.graphai.mall.db.domain.MallRole;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.anji.captcha.service.CaptchaService;
import com.graphai.commons.util.IpUtil;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.json.JacksonUtil;
import com.graphai.commons.util.json.JacksonUtils;
import com.graphai.commons.util.lang.StringUtils;
import com.graphai.framework.system.domain.GraphaiSystemMenu;
import com.graphai.framework.system.service.IGraphaiSystemMenuService;
import com.graphai.framework.system.service.IGraphaiSystemPermissionService;
import com.graphai.framework.system.service.MallRoleService;
import com.graphai.framework.system.vo.RouterVo;
import com.graphai.framework.util.PermissionUtil;
import com.graphai.mall.admin.service.LogHelper;
import com.graphai.mall.db.domain.MallAdmin;
import com.graphai.mall.db.service.MallAdminService;
import com.graphai.mall.db.service.MallPermissionService;

@RestController
@RequestMapping("/admin/auth")
@Validated
public class GraphaiSystemAuthController {
    private final Log logger = LogFactory.getLog(GraphaiSystemAuthController.class);

    @Autowired
    private IGraphaiSystemMenuService menuService;
    @Autowired
    private MallAdminService adminService;
    @Autowired
    private MallRoleService roleService;
    @Autowired
    private MallPermissionService permissionService;

    @Autowired
    private CaptchaService captchaService;

    @Autowired
    private IGraphaiSystemPermissionService ipermissionService;
    @Autowired
    private LogHelper logHelper;

    /*
     * { username : value, password : value }
     */
    @PostMapping("/login")
    public Object login(@RequestBody String body, HttpServletRequest request, HttpSession session) {
        String username = JacksonUtil.parseString(body, "username");
        String password = JacksonUtil.parseString(body, "password");
        // 图形验证码
        /*
         * String captchaVerification = JacksonUtil.parseString(body, "captchaVerification"); CaptchaVO
         * captchaVO = new CaptchaVO(); captchaVO.setCaptchaVerification(captchaVerification); ResponseModel
         * verification = captchaService.verification(captchaVO); if (! verification.isSuccess()) { return
         * ResponseUtil.fail(ADMIN_INVALID_ACCOUNT, verification.getRepMsg()); }
         */
        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password)) {
            return ResponseUtil.badArgument();
        }

        Subject currentUser = SecurityUtils.getSubject();
        try {
            currentUser.login(new UsernamePasswordToken(username, password));
        } catch (UnknownAccountException uae) {
            logHelper.logAuthFail("登录", "用户帐号或密码不正确");
            return ResponseUtil.fail(ADMIN_INVALID_ACCOUNT, "用户帐号或密码不正确");
        } catch (LockedAccountException lae) {
            logHelper.logAuthFail("登录", "用户帐号已锁定不可用");
            return ResponseUtil.fail(ADMIN_INVALID_ACCOUNT, "用户帐号已锁定不可用");

        } catch (AuthenticationException ae) {
            logger.error("登录失败：" + ae.getMessage(), ae);
            logHelper.logAuthFail("登录", "认证失败");
            return ResponseUtil.fail(ADMIN_INVALID_ACCOUNT, "认证失败");
        }

        currentUser = SecurityUtils.getSubject();
        MallAdmin admin = (MallAdmin) currentUser.getPrincipal();
        admin.setLastLoginIp(IpUtil.getIpAddr(request));
        admin.setLastLoginTime(LocalDateTime.now());
        adminService.updateById(admin);

        logHelper.logAuthSucceed("登录");
        /** 设置session的过期时间 **/
        session.setMaxInactiveInterval(3600);

        String[] roleIds = admin.getRoleIds();
        MallRole mallRole = new MallRole();
        if(roleIds.length > 0) {
            mallRole = roleService.findById(roleIds[0]);
        }

        Map<String, Object> adminMap = new HashMap<>();
        adminMap.put("name", admin.getUsername());
        adminMap.put("avatar", admin.getAvatar());
        adminMap.put("userId", admin.getId());
        adminMap.put("isItSet", admin.getIsItSet());
        adminMap.put("mobile", admin.getMobile());
        adminMap.put("roleKey", mallRole.getRoleKey());

        Map<String, Object> infomap = new HashMap<>();
        infomap.put("admin", adminMap);
        infomap.put("token", currentUser.getSession().getId());
        return ResponseUtil.ok(infomap);
    }

    /*
     *
     */
    @RequiresAuthentication
    @PostMapping("/logout")
    public Object logout() {
        Subject currentUser = SecurityUtils.getSubject();

        logHelper.logAuthSucceed("退出");
        currentUser.logout();
        return ResponseUtil.ok();
    }

    @RequiresAuthentication
    @GetMapping("/info")
    public Object info() {
        Subject currentUser = SecurityUtils.getSubject();
        MallAdmin admin = (MallAdmin) currentUser.getPrincipal();
        //
        Map<String, Object> data = new HashMap<>();
        data.put("name", admin.getUsername());
        data.put("avatar", admin.getAvatar());
        data.put("userId", admin.getId());
        String[] roleIds = admin.getRoleIds();
        // Set<String> roles1 = roleService.queryByIds(roleIds);
        // Set<String> permissions1 = permissionService.queryByRoleIds(roleIds);
        // 角色集合
        Set<String> roles = ipermissionService.getRolePermission(admin);
        Set<String> permissions = ipermissionService.getMenuPermission(admin);
        data.put("roles", roles);
        // data.put("roles", null);
        // NOTE
        // 这里需要转换perms结构，因为对于前端而已API形式的权限更容易理解
        data.put("perms", PermissionUtil.toAPI(permissions));
        logger.info("查询出来的角色相关的数据permissions: " + JacksonUtils.bean2Jsn(data));
        // data.put("perms", null);
        return ResponseUtil.ok(data);
    }


    @GetMapping("/401")
    public Object page401() {
        return ResponseUtil.unlogin();
    }

    @GetMapping("/index")
    public Object pageIndex() {
        return ResponseUtil.ok();
    }

    @GetMapping("/403")
    public Object page403() {
        return ResponseUtil.unauthz();
    }

    /**
     * 测试获取session信息
     */
    @GetMapping("/getSession")
    public Object getSession(HttpSession session) {
        int maxInactiveInterval = session.getMaxInactiveInterval();
        long creationTime = session.getCreationTime();
        long lastAccessedTime = session.getLastAccessedTime();

        logger.info(maxInactiveInterval);
        logger.info(creationTime);
        logger.info(lastAccessedTime);
        return ResponseUtil.ok(session);

    }

    /**
     * 获取路由信息
     *
     * @return 路由信息
     */
    @GetMapping("getRouters")
    public Object getRouters(@RequestParam(value = "parenMenuId", required = false) String parenMenuId) {
        // LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        MallAdmin admin = (MallAdmin) SecurityUtils.getSubject().getPrincipal();
        // 用户信息
        List<GraphaiSystemMenu> gMenus = null;
        if (StringUtils.isEmpty(parenMenuId)) {
            gMenus = menuService.selectMenuTreeByUserId(admin.getId());
        } else {
            GraphaiSystemMenu qMenu = new GraphaiSystemMenu();
            qMenu.setParentId(parenMenuId);
            List<GraphaiSystemMenu> menus = menuService.selectMenuList(qMenu, admin.getId());
            gMenus = menuService.getChildPerms(menus, parenMenuId);
        }
        return ResponseUtil.ok(PermissionUtil.buildMenus(gMenus));
    }

    @GetMapping("getTopRouters")
    public Object getTopRouters() {
        // LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        MallAdmin admin = (MallAdmin) SecurityUtils.getSubject().getPrincipal();
        // 用户信息
        List<GraphaiSystemMenu> menus = menuService.selectTopMenuByUserId(admin.getId());
        return ResponseUtil.ok(PermissionUtil.buildMenus(menus));
    }

    /**
     * 获取顶部至底部子菜单的树形结构菜单
     * 
     * @return
     */
    @GetMapping("getTop2ChildTreeRouters")
    public Object getTop2ChildTreeRouters() {
        // LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        MallAdmin admin = (MallAdmin) SecurityUtils.getSubject().getPrincipal();

        List<Map<String, Object>> mpsRouters = new ArrayList<>();
        List<GraphaiSystemMenu> menus = menuService.selectTopMenuByUserId(admin.getId());

        logger.info("查询数据结构 :" + JacksonUtils.bean2Jsn(menus));
        List<RouterVo> routers = PermissionUtil.buildMenus(menus);
        /** 查询出所有的菜单 **/
        GraphaiSystemMenu qMenu = new GraphaiSystemMenu();
        List<GraphaiSystemMenu> cmenus = menuService.selectMenuList(qMenu, admin.getId());
        for (int i = 0; i < routers.size(); i++) {
            RouterVo vo = routers.get(i);
            if (vo != null) {
                // 通过父菜单ID进行归纳
                List<GraphaiSystemMenu> gMenus = menuService.getChildPerms(cmenus, vo.getMeta().getMenuId());
                List<RouterVo> crouters = PermissionUtil.buildMenus(gMenus);
                Map<String, Object> mpsRouter = new HashMap<String, Object>();
                mpsRouter.put("trouters", vo);
                mpsRouter.put("crouters", crouters);
                mpsRouters.add(mpsRouter);
            }
        }
        return ResponseUtil.ok(mpsRouters);
    }
}
