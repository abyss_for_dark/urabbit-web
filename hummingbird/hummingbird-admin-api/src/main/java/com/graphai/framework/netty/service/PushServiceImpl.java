package com.graphai.framework.netty.service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.Resource;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.graphai.commons.util.json.JacksonUtils;
import com.graphai.commons.util.lang.StringUtils;
import com.graphai.framework.netty.autoconfigure.NettyConfig;
import com.graphai.id.GraphaiIdGenerator;
import com.graphai.mall.db.domain.MallAdmin;
import com.graphai.mall.db.domain.MallNotice;
import com.graphai.mall.db.domain.MallNoticeRelation;
import com.graphai.mall.db.service.MallAdminService;
import com.graphai.mall.db.service.notice.MallNoticeRelationService;
import com.graphai.mall.db.service.notice.MallNoticeService;

import cn.hutool.json.JSONObject;
import io.netty.channel.Channel;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;


@Service
public class PushServiceImpl implements PushService {

    @Resource
    private MallNoticeRelationService mallNoticeRelation;
    @Resource
    private MallAdminService mallAdminService;
    @Resource
    private MallNoticeService mallNoticeService;
    private final Log logger = LogFactory.getLog(PushServiceImpl.class);

    /**
     * 推送给指定用户
     * 
     * @param userId 用户ID
     * @param msg 消息信息
     */
    @Override
    public void pushMsgToOne(Map<String, Object> msg) {
        ConcurrentHashMap<String, Channel> userChannelMap = NettyConfig.getUserChannelMap();
        logger.info("传过来的userId:" + MapUtils.getString(msg, "toUserId"));
        Channel channel = userChannelMap.get(MapUtils.getString(msg, "toUserId"));

        String title = StringUtils.isNotBlank(MapUtils.getString(msg, "title")) ? MapUtils.getString(msg, "title") : "您有新的消息";
        String content = MapUtils.getString(msg, "msg");
        String fromUserId = MapUtils.getString(msg, "fromUserId");
        Integer bizType = MapUtils.getInteger(msg, "bizType");
        String linkUrl = MapUtils.getString(msg, "linkUrl");
        String bizId = MapUtils.getString(msg, "bizId");
        String toUserId = MapUtils.getString(msg, "toUserId");


        Map<String, Object> msgMap = new HashMap<String, Object>();
        // 文本消息
        msgMap.put("msg", content);
        msgMap.put("title", title);
        // 业务类型
        msgMap.put("bizType", bizType);
        // 业务主键ID
        msgMap.put("bizId", bizId);
        // 消息发送人
        msgMap.put("fromUserId", fromUserId);
        // 消息接收人
        msgMap.put("toUserId", toUserId);
        // 跳转URL
        msgMap.put("linkUrl", linkUrl);

        msgMap.put("event", "msg");

        String message = "{\"msg\":" + content + "," + "\"event\":" + "\"msg\"}";

        logger.info("有没有对象:" + JacksonUtils.bean2Jsn(channel));
        MallNotice msgNotice = new MallNotice();
        String noticeId = GraphaiIdGenerator.nextId("MallNotice");
        msgNotice.setId(noticeId);
        msgNotice.setType(2);
        msgNotice.setRemark("三方接口调用发送业务消息");
        msgNotice.setUserId(toUserId);
        msgNotice.setTitle(title);
        msgNotice.setContent(content);
        msgNotice.setStatus(1);
        msgNotice.setReleaseUser(fromUserId);
        msgNotice.setCreateTime(LocalDateTime.now());
        msgNotice.setUpdateTime(LocalDateTime.now());
        msgNotice.setSecondType(bizType);
        msgNotice.setGoodSn(bizId);
        msgNotice.setLinkUrl(linkUrl);
        msgNotice.setDeleted(false);
        mallNoticeService.insert(msgNotice, "SysApi");

        MallNoticeRelation mallNotice = new MallNoticeRelation();
        mallNotice.setId(GraphaiIdGenerator.nextId("MallNotice"));
        mallNotice.setUserId(fromUserId);
        mallNotice.setPushTime(LocalDateTime.now());
        mallNotice.setDeleted(false);
        mallNotice.setNoticeId(noticeId);
        mallNotice.setSuccessful(false);
        mallNoticeRelation.addNotice(mallNotice);

        // JacksonUtils.bean2Jsn(msgMap)
        channel.writeAndFlush(new TextWebSocketFrame(JacksonUtils.bean2Jsn(msgMap)));
    }

    /**
     * 推送给所有用户
     * 
     * @param msg 消息信息
     */
    @Override
    public void pushMsgToAll(MallNotice msg) {
        JSONObject jsonObject = new JSONObject();
        Map<String, Object> map = new HashMap<>();
        map.put("isRead", 1);
        map.put("event", "msg");
        jsonObject.putAll(map);
        NettyConfig.getChannelGroup().writeAndFlush(new TextWebSocketFrame(jsonObject.toString()));
        List<MallAdmin> list = mallAdminService.querySelective(null, null, null, 1, 1000, null, null);
        for (MallAdmin admin : list) {
            MallNoticeRelation mallNotice = new MallNoticeRelation();
            mallNotice.setId(GraphaiIdGenerator.nextId("MallNotice"));
            mallNotice.setUserId(admin.getId());
            mallNotice.setPushTime(LocalDateTime.now());
            mallNotice.setDeleted(false);
            mallNotice.setNoticeId(msg.getId());
            mallNotice.setSuccessful(false);
            mallNoticeRelation.addNotice(mallNotice);
        }

    }

    /**
     * 获取当前连接数
     * 
     * @return 连接数
     */
    @Override
    public int getConnectCount() {
        return NettyConfig.getChannelGroup().size();
    }

    /**
     *
     *
     */
    @Override
    public List<Map<String, Object>> getMsgList(String userId) {
        List<Map<String, Object>> myNoticeList = mallNoticeRelation.getMyNoticeList(userId);
        for (int i = 0; i < myNoticeList.size(); i++) {
            MallNotice notice = mallNoticeService.getMallNoticeById(myNoticeList.get(i).get("noticeId").toString());
            myNoticeList.get(i).put("createName", notice.getCreateUser());
        }
        return myNoticeList;
    }


    public void readMsg(String noticeId) {
        MallNoticeRelation mallNotice = mallNoticeRelation.getNoticeById(noticeId);
        mallNotice.setSuccessful(true);
        mallNoticeRelation.updateNotice(mallNotice);
    }

}
