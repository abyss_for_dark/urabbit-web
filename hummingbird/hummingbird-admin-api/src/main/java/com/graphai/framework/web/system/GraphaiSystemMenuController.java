package com.graphai.framework.web.system;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.graphai.commons.constant.Constants;
import com.graphai.commons.constant.UserConstants;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.date.DateUtils;
import com.graphai.commons.util.lang.StringUtils;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.framework.system.domain.GraphaiSystemMenu;
import com.graphai.framework.system.service.IGraphaiSystemMenuService;
import com.graphai.framework.system.service.MallRoleService;
import com.graphai.id.GraphaiIdGenerator;
import com.graphai.mall.admin.util.AdminUserUtils;
import com.graphai.mall.db.domain.MallRole;

/**
 * 菜单信息
 *
 * @author biteam
 * @ClassName: GraphaiSystemMenuController
 * @date 2020-10-17
 */
@RestController
@RequestMapping("/admin/menu")
public class GraphaiSystemMenuController {
	@Autowired
	private IGraphaiSystemMenuService menuService;
	
	@Autowired
	private MallRoleService roleService;
	
	private static final String ADD_MENU = "新增菜单";

	/**
	 * 获取菜单列表
	 */
	@RequiresPermissions("admin:menu:list")
	@RequiresPermissionsDesc(menu = { "菜单管理" }, button = "查询")
	@GetMapping("/list")
	public Object list(GraphaiSystemMenu menu) {
		List<GraphaiSystemMenu> menus = menuService.selectMenuList(menu, AdminUserUtils.getLoginUserId());
		return ResponseUtil.ok(menus);
	}

	/**
	 * 根据菜单编号获取详细信息
	 */
	@GetMapping(value = "/{menuId}")
	public Object getInfo(@PathVariable String menuId) {
		return ResponseUtil.ok(menuService.selectMenuById(menuId));
	}

	/**
	 * 获取菜单下拉树列表
	 */
	@GetMapping("/treeselect")
	public Object treeselect(GraphaiSystemMenu menu) {
		List<GraphaiSystemMenu> menus = menuService.selectMenuList(menu, AdminUserUtils.getLoginUserId());
		return ResponseUtil.ok(menuService.buildMenuTreeSelect(menus));
	}

	/**
	 * 加载对应角色菜单列表树
	 */
	@GetMapping(value = "/roleMenuTreeselect/{roleId}")
	public Object roleMenuTreeselect(@PathVariable("roleId") String roleId) {
//		AdminUserUtils.getLoginUserId()
		List<GraphaiSystemMenu> menus = menuService.selectMenuList(null);
		Map<String, Object> ajax = new HashMap<>();
		ajax.put("checkedKeys", menuService.selectMenuListByRoleId(roleId));
		ajax.put("menus", menuService.buildMenuTreeSelect(menus));
		return ResponseUtil.ok(ajax);
	}

	/**
	 * 新增菜单
	 */
	@RequiresPermissions("admin:menu:add")
	@RequiresPermissionsDesc(menu = { "菜单管理" }, button = "新增")
	@PostMapping("/add")
	public Object add(@Validated @RequestBody GraphaiSystemMenu menu) {
		if (UserConstants.NOT_UNIQUE.equals(menuService.checkMenuNameUnique(menu))) {
			return ResponseUtil.fail("" + menu.getMenuName() + "'失败，菜单名称已存在");
		} else if (UserConstants.YES_FRAME.equals(menu.getIsFrame())
				&& !StringUtils.startsWithAny(menu.getPath(), Constants.HTTP, Constants.HTTPS)) {
			return ResponseUtil.fail(ADD_MENU + menu.getMenuName() + "'失败，地址必须以http(s)://开头");
		}
		menu.setCreateBy(AdminUserUtils.getLoginUserName());
		menu.setCreateTime(DateUtils.getNow());
		menu.setMenuId(GraphaiIdGenerator.nextId("GraphaiSystemMenu"));
		return ResponseUtil.ok(menuService.insertMenu(menu));
	}
	
	
	/**
	 * 新增菜单并且直接授权给当前用户
	 */
	@RequiresPermissions("admin:menu:add:grant")
	@RequiresPermissionsDesc(menu = { "菜单管理" }, button = "新增")
	@PostMapping("/add/grant")
	public Object addGrant(@Validated @RequestBody GraphaiSystemMenu menu) {
		if (UserConstants.NOT_UNIQUE.equals(menuService.checkMenuNameUnique(menu))) {
			return ResponseUtil.fail("" + menu.getMenuName() + "'失败，菜单名称已存在");
		} else if (UserConstants.YES_FRAME.equals(menu.getIsFrame())
				&& !StringUtils.startsWithAny(menu.getPath(), Constants.HTTP, Constants.HTTPS)) {
			return ResponseUtil.fail(ADD_MENU + menu.getMenuName() + "'失败，地址必须以http(s)://开头");
		}
		menu.setCreateBy(AdminUserUtils.getLoginUserName());
		menu.setCreateTime(DateUtils.getNow());
		menu.setMenuId(GraphaiIdGenerator.nextId("GraphaiSystemMenu"));
		
		/**获取当前用户的所有角色信息**/
		List<MallRole> roleList =  roleService.queryAllExitAdmin(AdminUserUtils.getLoginUser());
		/**授权**/
		menuService.insertMenu(menu);
		if(CollectionUtils.isNotEmpty(roleList)) {
			for (int i = 0; i < roleList.size(); i++) {
				MallRole role = roleList.get(i);
				String[] strs = new String[] {menu.getMenuId()};
				role.setMenuIds(strs);
				roleService.insertRoleMenu(role);
			}
		}
		
		return ResponseUtil.ok();
	}
	
	

	/**
	 * 修改菜单
	 */
	@RequiresPermissions("admin:menu:edit")
	@RequiresPermissionsDesc(menu = { "菜单管理" }, button = "编辑")
	@PutMapping("/edit")
	public Object edit(@Validated @RequestBody GraphaiSystemMenu menu) {
		if (UserConstants.YES_FRAME.equals(menu.getIsFrame())
				&& !StringUtils.startsWithAny(menu.getPath(), Constants.HTTP, Constants.HTTPS)) {
			return ResponseUtil.fail(ADD_MENU + menu.getMenuName() + "'失败，地址必须以http(s)://开头");
		} else if (menu.getMenuId().equals(menu.getParentId())) {
			return ResponseUtil.fail(ADD_MENU + menu.getMenuName() + "'失败，上级菜单不能选择自己");
		}
		menu.setUpdateBy(AdminUserUtils.getLoginUserName());
		menu.setUpdateTime(DateUtils.getNow());
		return ResponseUtil.ok(menuService.updateMenu(menu));
	}

	/**
	 * 删除菜单
	 */
	@RequiresPermissions("admin:menu:del")
	@RequiresPermissionsDesc(menu = { "菜单管理" }, button = "删除")
	@PostMapping("/del/{menuId}")
	public Object remove(@PathVariable("menuId") String menuId) {
		if (menuService.hasChildByMenuId(menuId)) {
			return ResponseUtil.fail("存在子菜单,不允许删除");
		}
		if (menuService.checkMenuExistRole(menuId)) {
			return ResponseUtil.fail("菜单已分配,不允许删除");
		}
		return ResponseUtil.ok(menuService.deleteMenuById(menuId));
	}
}
