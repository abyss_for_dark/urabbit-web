/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.graphai.framework.interceptor;

import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.graphai.commons.util.lang.StringUtils;
import com.graphai.framework.config.converter.String2DateConverter;
import com.graphai.framework.config.converter.String2LocalDateConverter;
import com.graphai.framework.config.converter.String2LocalDateTimeConverter;
import com.graphai.framework.config.converter.String2LocalTimeConverter;

/**
 * @author biteam
 * @ClassName: LogInterceptorConfig
 * @Description: TODO(后台管理日志记录拦截器)
 * @date 2020-10-8
 */
@Configuration
// @ConditionalOnProperty(name = "true", havingValue = "true", matchIfMissing = true)
public class LogInterceptorConfig implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        InterceptorRegistration registration = registry.addInterceptor(new LogInterceptor());
        String apps = "/**";// Global.getProperty("web.interceptor.log.addPathPatterns");
        String epps = "/login";// Global.getProperty("web.interceptor.log.excludePathPatterns");
        for (String uri : StringUtils.split(apps, ",")) {
            if (StringUtils.isNotBlank(uri)) {
                registration.addPathPatterns(StringUtils.trim(uri));
            }
        }
        for (String uri : StringUtils.split(epps, ",")) {
            if (StringUtils.isNotBlank(uri)) {
                registration.excludePathPatterns(StringUtils.trim(uri));
            }
        }
    }


    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(new String2LocalDateConverter());
        registry.addConverter(new String2LocalDateTimeConverter());
        registry.addConverter(new String2LocalTimeConverter());
        registry.addConverter(new String2DateConverter());
    }

}
