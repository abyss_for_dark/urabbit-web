package com.graphai.framework.web.sms;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.lang.StringUtils;
import com.graphai.framework.generator.controller.IBaseController;
import com.graphai.framework.payment.domain.GraphaiPayProduct;
import com.graphai.framework.sms.entity.SmsSendLog;
import com.graphai.framework.sms.service.ISmsSendLogService;

import cn.hutool.db.Page;


/**
 * All rights Reserved, Designed By www.jeeweb.cn
 *
 * @version V1.0
 * @package com.graphai.framework.sms.controller
 * @title: 发送日志控制器
 * @description: 发送日志控制器
 * @author: 王存见
 * @copyright: 2018 www.jeeweb.cn Inc. All rights reserved.
 */

@RestController
@RequestMapping("/admin/sms/sendlog")
public class SmsSendLogController extends IBaseController<SmsSendLog, String>  {

    @Autowired
    private ISmsSendLogService smsSendLogService;
    
    @GetMapping(value = "list")
    public void list( HttpServletRequest request) throws IOException {
        //加入条件
        QueryWrapper<SmsSendLog> entityWrapper = new QueryWrapper<>();
        entityWrapper.orderByDesc("responseDate");
        String sendCode= request.getParameter("sendCode");
        if (!StringUtils.isEmpty(sendCode)){
            entityWrapper.eq("sendCode",sendCode);
        }
        String phone= request.getParameter("phone");
        if (!StringUtils.isEmpty(phone)){
            entityWrapper.eq("phone",phone);
        }
        String status=request.getParameter("status");
        if (!StringUtils.isEmpty(status)){
            entityWrapper.eq("status",status);
        }
        // 预处理
        IPage<SmsSendLog> ipage = smsSendLogService.page(getPlusPage(),entityWrapper);
        //FastJsonUtils.print(pageBean,SmsSendLog.class,"id,phone,templateName,sendData,sendCode,tryNum,status,smsid,code,msg,delFlag,responseDate");
    }

    @PostMapping("{id}/delete")
    public Object delete(@PathVariable("id") String id) {
        smsSendLogService.getById(id);
        return ResponseUtil.ok("删除成功");
    }

    @PostMapping("batch/delete")
    public Object batchDelete(@RequestParam("ids") String[] ids) {
        List<String> idList = java.util.Arrays.asList(ids);
        smsSendLogService.removeByIds(idList);
        return ResponseUtil.ok("删除成功");
    }

    @PostMapping(value = "retrySend")
    @ResponseBody
    public Object retrySend(@RequestParam(value = "ids", required = false) String[] ids) {
        try {
            List<String> idList = java.util.Arrays.asList(ids);
            smsSendLogService.retrySend(idList);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseUtil.fail("重发队列添加失败");
        }
        return ResponseUtil.ok("重发队列添加成功");
    }
}