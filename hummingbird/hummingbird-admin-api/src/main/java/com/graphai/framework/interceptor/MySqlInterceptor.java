package com.graphai.framework.interceptor;

import java.lang.reflect.Field;
import java.sql.Statement;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.ibatis.executor.resultset.ResultSetHandler;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlCommandType;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Plugin;
import org.apache.ibatis.plugin.Signature;

import com.graphai.commons.util.lang.StringHelper;


/**
 * @author yangxj
 * @date 2020-08-10 11:31
 *       <p>
 *       数据脱敏拦截器
 */
// @Intercepts({@Signature(type = Executor.class, method = "query",
// args = {MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class})})
@Intercepts({@Signature(type = ResultSetHandler.class, method = "handleResultSets", args = {Statement.class})})
public class MySqlInterceptor implements Interceptor {


    private final Log logger = LogFactory.getLog(MySqlInterceptor.class);


    private static final String mobile = "mobilephone";

    private String getOperateType(Invocation invocation) {
        final Object[] args = invocation.getArgs();
        MappedStatement ms = (MappedStatement) args[0];
        SqlCommandType commondType = ms.getSqlCommandType();
        if (commondType.compareTo(SqlCommandType.SELECT) == 0) {
            return "select";
        }

        return null;
    }

    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        Object result = invocation.proceed(); // 执行请求方法，并将所得结果保存到result中


        // 如果需要对结果脱敏，则执行
        // 先对Map进行处理
        // if (result != null && result instanceof Map) {
        // return this.desensitizationMap(result);
        // }
        //
        // // 处理集合
        // if (result instanceof ArrayList<?>) {
        // List<?> list = (ArrayList<?>) result;
        // return this.desensitization(list);
        // }

        // 处理单个bean
        // return this.desensitization(result);

        return result;
    }



    @Override
    public Object plugin(Object target) {
        return Plugin.wrap(target, this);
    }



    // 手机号码前三后四脱敏
    public static String mobileEncrypt(String mobile) {
        if (StringHelper.isNullOrEmpty(mobile) || (mobile.length() != 11)) {
            return mobile;
        }
        return mobile.replaceAll("(\\d{3})\\d{4}(\\d{4})", "$1****$2");
    }


    /*
     * 对map脱敏
     */
    private Object desensitizationMap(Object result) {
        Map mapResult = (Map) result;
        if (MapUtils.isEmpty(mapResult)) {
            return mapResult;
        }

        Set<String> keySet = mapResult.keySet();
        for (String key : keySet) {
            if (mobile.contains(key)) {
                String value = null;
                if (StringUtils.isBlank(String.valueOf(mapResult.get(key)))) {
                    continue;
                }
                value = mobileEncrypt(String.valueOf(mapResult.get(key)));
                mapResult.put(key, value);
            }
        }
        return result;
    }

    private List desensitization(List list) {
        if (CollectionUtils.isEmpty(list)) {
            return Collections.emptyList();
        }

        Class cls = null;
        for (Object o : list) {
            // 脱敏map，改变引用地址(根据静态配置脱敏)
            if (o != null && o instanceof Map) {
                o = desensitizationMap(o);
                continue;
            }

            // 脱敏bean(根据注解脱敏)
            if (cls == null) {
                cls = o.getClass();
            }
            o = desensitization(o);
        }
        return list;
    }



    private Object desensitization(Object obj) {
        if (obj == null) {
            return obj;
        }
        String value = null;
        Class cls = obj.getClass();
        Field[] objFields = cls.getDeclaredFields();
        if (Objects.equals(null, objFields)) {
            return obj;
        }

        for (Field field : objFields) {

            try {
                // value = getReplacedVal(desensitization.type(), value, desensitization.attach());
                field.setAccessible(true);
                if ("mobile".equals(field.getName()) || "phone".equals(field.getName())) {
                    value = mobileEncrypt(String.valueOf(field.get(obj)));
                    field.set(obj, value);
                }

            } catch (Exception ignore) {
                ignore.printStackTrace();
            }
        }

        return obj;
    }


    @Override
    public void setProperties(Properties properties) {
        String mobile = properties.getProperty("mobile");

    }

}
