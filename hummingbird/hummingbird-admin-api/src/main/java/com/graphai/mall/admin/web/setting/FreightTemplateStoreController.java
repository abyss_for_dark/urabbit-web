package com.graphai.mall.admin.web.setting;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.generator.controller.IBaseController;
import com.graphai.mall.admin.domain.MallShipCompanyV2;
import com.graphai.mall.admin.domain.MallUserMerchant;
import com.graphai.mall.admin.service.impl.MallUserMerchantServiceImpl;
import com.graphai.mall.admin.util.AdminUserUtils;
import com.graphai.mall.db.constant.account.AccountStatus;
import com.graphai.mall.db.domain.MallAdmin;
import com.graphai.mall.setting.domain.FreightTemplate;
import com.graphai.mall.setting.service.FreightTemplateService;
import com.graphai.mall.setting.vo.FreightTemplateVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 店铺端,运费模板接口
 *
 * @author paulG
 * @date 2020/8/26
 **/
@RestController
@RequestMapping("/admin/store/freightTemplate")
public class FreightTemplateStoreController extends IBaseController<FreightTemplate,  String > {
    @Autowired
    private FreightTemplateService freightTemplateService;
    @Autowired
    private MallUserMerchantServiceImpl userMerchantService;

    @GetMapping
    public Object list(String tag) {
        if (StrUtil.isEmpty(tag)){
            Map<String,Object> map = freightTemplateService.getFreightTemplateList(getPlusPage());
            return ResponseUtil.ok(map);
        }
        List<FreightTemplate> list = null;
        MallAdmin loginUser = AdminUserUtils.getLoginUser();
        String roleId = Arrays.toString(loginUser.getRoleIds());
        if (roleId.contains(AccountStatus.LOGIN_ROLE_ADMIN)){
            list = freightTemplateService.list();
        }else{
            MallUserMerchant userMerchant = userMerchantService.getOne(new QueryWrapper<MallUserMerchant>().lambda().eq(MallUserMerchant::getUserId, loginUser.getUserId()));
            if (ObjectUtil.isNotEmpty(userMerchant)){
                list = freightTemplateService.list(new QueryWrapper<FreightTemplate>().lambda().eq(FreightTemplate::getMerchantId,userMerchant.getMerchantId()));
            }
        }
       return ResponseUtil.ok(list);
    }

    @GetMapping("/queryMerchantTemplate")
    public Object queryMerchantTemplate(String merchantId) {
        return ResponseUtil.ok(freightTemplateService.list(new QueryWrapper<FreightTemplate>().lambda().eq(FreightTemplate::getMerchantId,merchantId)));
    }

    @PostMapping
    public Object add(@Valid @RequestBody FreightTemplateVO freightTemplateVO) {
        return ResponseUtil.ok(freightTemplateService.addFreightTemplate(freightTemplateVO));
    }

    @PutMapping("/{id}")
    public Object edit(@PathVariable String id, @RequestBody @Valid FreightTemplateVO freightTemplateVO) {
        return ResponseUtil.ok(freightTemplateService.editFreightTemplate(freightTemplateVO));
    }

    @DeleteMapping("/{id}")
    public Object edit(@PathVariable String id) {
        freightTemplateService.removeFreightTemplate(id);
        return ResponseUtil.ok();
    }
}
