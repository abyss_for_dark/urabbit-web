package com.graphai.mall.admin.web.distribution;

import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.mall.db.domain.MallAdmin;
import com.graphai.mall.db.domain.MallDistribution;
import com.graphai.mall.db.service.distribution.MallDistributionService;
import com.graphai.validator.Order;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 分销配置
 */
@RestController
@RequestMapping("/admin/mallDistribution")
@Validated
public class AdminMallDistributionController {
    private final Log logger = LogFactory.getLog(AdminMallDistributionController.class);

    @Autowired
    private MallDistributionService mallDistributionService;

    /**
     * 查询分销配置
     */
    @GetMapping("/list")
    public Object list(@RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit,
                       @RequestParam(defaultValue = "id") String sort,
                       @Order @RequestParam(defaultValue = "desc") String order) {
        MallDistribution mallDistribution = new MallDistribution();
        List<MallDistribution> list = mallDistributionService.selectMallDistributionList(mallDistribution, page, limit, sort, order);
        long total = PageInfo.of(list).getTotal();
        Map<String, Object> data = new HashMap<>();
        data.put("total", total);
        data.put("items", list);

        return ResponseUtil.ok(data);
    }

    /**
     * 添加分销配置
     */
    @PostMapping("/add")
    public Object addSave(@RequestBody MallDistribution mallDistribution) {
        Object error = validate(mallDistribution);
        if (error != null) {
            return error;
        }
        Subject currentUser = SecurityUtils.getSubject();
        MallAdmin admin = (MallAdmin) currentUser.getPrincipal();
        mallDistribution.setUserId(admin.getId());
        mallDistribution.setUserName(admin.getUsername());
        mallDistributionService.insertMallDistribution(mallDistribution);
        return ResponseUtil.ok();
    }


    /**
     * 查看分销配置详情
     */
    @GetMapping("/edit/{id}")
    public Object edit(@PathVariable("id") String id) {
        MallDistribution mallDistribution = mallDistributionService.selectMallDistributionById(id);
        return ResponseUtil.ok(mallDistribution);
    }

    /**
     * 修改分销配置
     */
    @PostMapping("/edit")
    public Object editSave(@RequestBody MallDistribution mallDistribution) {
        //等级主键id
        if (StringUtils.isEmpty(mallDistribution.getId())) {
            return ResponseUtil.badArgumentValue("没有获取到id！");
        }
        Object error = validate(mallDistribution);
        if (error != null) {
            return error;
        }
        mallDistributionService.updateMallDistribution(mallDistribution);
        return ResponseUtil.ok();
    }


    /**
     * 校验数据(分销配置)
     */
    private Object validate(MallDistribution mallDistribution) {
        if (StringUtils.isEmpty(mallDistribution.getGoodsId())) {
            return ResponseUtil.badArgument();
        }
        if (StringUtils.isEmpty(mallDistribution.getGoodsName())) {
            return ResponseUtil.badArgument();
        }
        if (StringUtils.isEmpty(mallDistribution.getCategoryId())) {
            return ResponseUtil.badArgument();
        }
        if (StringUtils.isEmpty(mallDistribution.getBrandId())) {
            return ResponseUtil.badArgument();
        }

        return null;
    }
}
