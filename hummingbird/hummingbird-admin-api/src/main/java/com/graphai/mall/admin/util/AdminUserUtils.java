package com.graphai.mall.admin.util;

import com.graphai.mall.db.constant.account.AccountStatus;
import com.graphai.mall.db.domain.MallAdmin;
import org.apache.shiro.SecurityUtils;

import java.util.Arrays;

/**
 * 用户信息类
 *
 * @author biteam
 * @ClassName: AdminUserUtils
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date 2020-10-17
 */
public class AdminUserUtils {

    /**
     * 获取当前登录用户信息
     *
     * @return
     */
    public static MallAdmin getLoginUser() {
        MallAdmin admin = (MallAdmin) SecurityUtils.getSubject().getPrincipal();
        return admin;
    }

    /**
     * 获取用户ID
     *
     * @return
     */
    public static String getLoginUserId() {
        return getLoginUser().getId();
    }

    /**
     * 获取用户ID
     *
     * @return
     */
    public static String getLoginUserName() {
        return getLoginUser().getUsername();
    }

    /**
     *  判断是否是admin角色
     */
    public static boolean isAdminRole(){
        MallAdmin adminUser = (MallAdmin) SecurityUtils.getSubject().getPrincipal();
        String roleIds = Arrays.toString(adminUser.getRoleIds());
        return roleIds.contains(AccountStatus.LOGIN_ROLE_ADMIN);
    }

    /**
     *  判断是否是工厂角色
     */
    public static boolean isFactoryRole(){
        MallAdmin adminUser = (MallAdmin) SecurityUtils.getSubject().getPrincipal();
        String roleIds = Arrays.toString(adminUser.getRoleIds());
        return roleIds.contains(AccountStatus.LOGIN_ROLE_FACTORY);
    }

    /**
     *  判断是否是商户角色
     */
    public static boolean isMerchantRole(){
        MallAdmin adminUser = (MallAdmin) SecurityUtils.getSubject().getPrincipal();
        String roleIds = Arrays.toString(adminUser.getRoleIds());
        return roleIds.contains(AccountStatus.LOGIN_ROLE_MERCHANT);
    }
}
