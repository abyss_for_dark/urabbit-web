package com.graphai.mall.admin.web.promotion;

import cn.hutool.json.JSONUtil;
import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.mall.db.domain.MallSubMsg;
import com.graphai.mall.db.service.promotion.MallSubMsgService;
import com.graphai.validator.Order;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 招商管理-商务合作
 */
@RestController
@RequestMapping("/admin/subMsg")
@Validated
public class AdminMallSubMsgController {
    private final Log logger = LogFactory.getLog(AdminMallSubMsgController.class);

    @Autowired
    private MallSubMsgService mallSubMsgService;

    @RequiresPermissions("admin:subMsg:list")
    @RequiresPermissionsDesc(menu = {"招商管理", "商务合作"}, button = "查询")
    @GetMapping("/list")
    public Object list(MallSubMsg mallSubMsg,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit,
                       @RequestParam(defaultValue = "id") String sort,
                       @Order @RequestParam(defaultValue = "desc") String order) {
        logger.info(JSONUtil.toJsonStr(mallSubMsg));
        List<MallSubMsg> mallSubMsgList = mallSubMsgService.selectMallSubMsgList(mallSubMsg, page, limit, sort, order);
        long total = PageInfo.of(mallSubMsgList).getTotal();
        Map<String, Object> data = new HashMap<>();
        data.put("total", total);
        data.put("items", mallSubMsgList);
        return ResponseUtil.ok(data);
    }


}