package com.graphai.mall.admin.web.seckill;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.lang.StringUtils;
import com.graphai.commons.util.servlet.ServletUtils;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.framework.generator.controller.IBaseController;
import com.graphai.mall.admin.domain.MallActivityGoodsProduct;
import com.graphai.mall.admin.domain.MallSeckillGoods;
import com.graphai.mall.admin.service.IMallActivityGoodsProductService;
import com.graphai.mall.admin.service.IMallSeckillGoodsService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * <p>
 * 秒杀商品表  前端控制器
 * </p>
 *
 * @author biteam
 * @since 2021-07-05
 */
@RestController
@RequestMapping("/admin/admin/goods")
public class MallSeckillGoodsController extends IBaseController<MallSeckillGoods,  String >  {

	@Autowired
    private IMallSeckillGoodsService serviceImpl;
    @Autowired
    private IMallActivityGoodsProductService mallActivityGoodsProductService;
    
    @RequiresPermissions("admin:admin:goods:single")
    @RequiresPermissionsDesc(menu = {"单独查询"}, button = "单独查询")
	@GetMapping("/single/{id}")
    public Object getEntityById(@PathVariable  String  id) throws Exception{
        MallSeckillGoods entityVName = serviceImpl.getById(id);
        return ResponseUtil.ok(entityVName);
    }
    
    @RequiresPermissions("admin:admin:goods:addOrUpdate")
    @RequiresPermissionsDesc(menu = {"新增"}, button = "新增")
    @PostMapping("/addOrUpdate")
    public Object addOrUpdate(MallSeckillGoods mallSeckillGoods) throws Exception{
        if(serviceImpl.saveOrUpdate(mallSeckillGoods)){
            return ResponseUtil.ok();
        }else{
            return ResponseUtil.fail();
        }
    } 
	
	
	@RequiresPermissions("admin:admin:goods:melist")
	@RequiresPermissionsDesc(menu = {"秒杀商品表 查询"}, button = "查询")
    @GetMapping("/melist")
    public Object getMeList() throws Exception {
        IPage<MallSeckillGoods> ipage = serviceImpl.page(getPlusPage(),listQueryWrapper());
        return ResponseUtil.ok(ipage);
    }
 	
 	private QueryWrapper<MallSeckillGoods> listQueryWrapper() {
 		QueryWrapper<MallSeckillGoods> meq = new QueryWrapper<>();
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("seckillId"))) {
		      meq.eq("seckill_id", ServletUtils.getParameter("seckillId"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("number"))) {
		      meq.eq("number", ServletUtils.getParameter("number"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("goodsName"))) {
		      meq.eq("goods_name", ServletUtils.getParameter("goodsName"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("startsNumber"))) {
		      meq.eq("starts_number", ServletUtils.getParameter("startsNumber"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("seckillPrice"))) {
		      meq.eq("seckill_price", ServletUtils.getParameter("seckillPrice"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("goodsId"))) {
		      meq.eq("goods_id", ServletUtils.getParameter("goodsId"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("auditStatus"))) {
		      meq.eq("audit_status", ServletUtils.getParameter("auditStatus"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("reason"))) {
		      meq.eq("reason", ServletUtils.getParameter("reason"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("isOnSale"))) {
		      meq.eq("is_on_sale", ServletUtils.getParameter("isOnSale"));
		}
		
        return meq;
    }
 	@RequiresPermissions("admin:admin:goods:save")
 	@RequiresPermissionsDesc(menu = {"秒杀商品表 新增"}, button = "新增")
    @PostMapping("/save")
    public Object save(@ModelAttribute MallSeckillGoods mallSeckillGoods) throws Exception {
        serviceImpl.save(mallSeckillGoods);
        return ResponseUtil.ok();
    }
	
	@RequiresPermissions("admin:admin:goods:modify")
	@RequiresPermissionsDesc(menu = {"秒杀商品表 修改"}, button = "修改")
    @PutMapping("/modify")
    public Object modify(@ModelAttribute  MallSeckillGoods mallSeckillGoods){
        MallActivityGoodsProduct mallActivityGoodsProduct = mallActivityGoodsProductService.getById(mallSeckillGoods.getId());


        QueryWrapper<MallSeckillGoods> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("goods_id",mallSeckillGoods.getGoodsId());
        queryWrapper.eq("seckill_time_id",mallActivityGoodsProduct.getActivityRuleId());

        MallSeckillGoods goods = serviceImpl.getOne(queryWrapper);
        goods.setIsOnSale(0);
        goods.setAuditStatus(0);
        serviceImpl.updateById(goods);
        return  ResponseUtil.ok();
    }

    @RequiresPermissions("admin:admin:goods:del")
    @RequiresPermissionsDesc(menu = {"秒杀商品表 删除"}, button = "删除")
    @DeleteMapping("/del/{id}")
    public Object deleteById(@PathVariable  String  id){
        mallActivityGoodsProductService.removeById(id);
        serviceImpl.removeById(id);
        return  ResponseUtil.ok();
    }

    /**
     * 根据秒杀商品id集合，批量删除
     *
     * @return
     */
    @PostMapping("/deleteByIds")
    public Object deleteByIds(@RequestBody List<String> ids) {
        serviceImpl.removeByIds(ids);
        //通过goodsId和timeId删除活动商品规格表数据
        return ResponseUtil.ok();
    }

}

