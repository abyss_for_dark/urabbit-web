package com.graphai.mall.admin.service;


import cn.hutool.core.util.StrUtil;
import com.beust.jcommander.internal.Lists;
import com.github.pagehelper.PageHelper;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.system.service.IGraphaiSystemDeptService;
import com.graphai.framework.system.service.IGraphaiSystemRoleService;
import com.graphai.id.GraphaiIdGenerator;
import com.graphai.mall.admin.dto.MallSelectionManageMentImport;
import com.graphai.mall.admin.util.ExcelUtil;
import com.graphai.mall.db.dao.*;
import com.graphai.mall.db.domain.*;
import com.graphai.mall.db.service.goods.MallGoodsService;

import com.graphai.mall.db.util.StringHelper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;

@Service
public class MallSelectionManageService {

    private final Log logger = LogFactory.getLog(MallSelectionManageService.class);

    @Resource
    private MallGoodsSelectionMapper mallGoodsSelectionMapper;
    @Resource
    private MallSelectionManagementMapper mallSelectionManagementMapper;
    @Resource
    private MallGoodsService mallGoodsService;
    @Autowired
    private MallBrandMapper brandMapper;
    @Autowired
    private MallCategoryMapper categoryMapper;
    @Autowired
    private IGraphaiSystemDeptService iGraphaiSystemDeptService;
    @Resource
    private MallGoodsMapper goodsMapper;
    @Resource
    private CustomGoodsMapper customGoodsMapper;
    @Autowired
    private MallProductCouponMapper mallProductCouponMapper;
    @Autowired
    private MallCouponMapper mallCouponMapper;
    @Autowired
    private IGraphaiSystemRoleService iGraphaiSystemRoleService;

    /**
     * 选品活动-导入excel
     *
     * @param file 文件
     * @return Object
     * @throws Exception 异常
     */
    public Object importData(MultipartFile file) throws Exception {
        ExcelUtil<MallSelectionManageMentImport> util = new ExcelUtil<MallSelectionManageMentImport>(MallSelectionManageMentImport.class);
        List<MallSelectionManageMentImport> mallSelectionManageMentList = util.importExcel(file.getInputStream());
        //导入
        if (null == mallSelectionManageMentList || mallSelectionManageMentList.size() == 0) {
            logger.error("导入源系统商品ID数据不能为空！");
            return ResponseUtil.fail(402, "导入源系统商品ID数据不能为空！");
        }

        int oldListCount = mallSelectionManageMentList.size();//导入总条数
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder(); //定义接收成功信息
        StringBuilder failureMsg = new StringBuilder(); //定义接收失败信息
        StringBuilder removalMsg = new StringBuilder();//定义重复数据

        //去重
        HashSet<MallSelectionManageMentImport> listToSet = new HashSet(mallSelectionManageMentList);//list集合转HashSet集合
        HashSet<MallSelectionManageMentImport> hashSet = new HashSet<>();
        List<String> getProductIdList = mallGoodsSelectionMapper.getProductIdList();//获取选品管理的源系统商品ID
        //遍历循环，获取不重复的数据存入hashSet集合
        for (MallSelectionManageMentImport mallSelectionManageMentImport : listToSet) {
            if (!getProductIdList.contains(mallSelectionManageMentImport.getSrcGoodId())) {
                hashSet.add(mallSelectionManageMentImport);
            }
        }
        //获取重复条数
        mallSelectionManageMentList.clear();
        mallSelectionManageMentList.addAll(hashSet);
        if (mallSelectionManageMentList.size() < oldListCount) {
            Integer removalNum = oldListCount - mallSelectionManageMentList.size(); //导入总条数 - 没有重复数据 = 重复条数
            logger.error("共 " + removalNum + " 条数据去重！");
            removalMsg.insert(0, "共 " + removalNum + " 条数据去重！");
        }

        //获取结果字符串信息
        List<String> resultStr = Lists.newArrayList();
        for (MallSelectionManageMentImport mallSelectionManageMentImport : mallSelectionManageMentList) {
            if (StrUtil.isBlank(mallSelectionManageMentImport.getSrcGoodId()) || null == mallSelectionManageMentImport.getSrcGoodId()) {
                failureNum++;
            } else {
                resultStr.add(mallSelectionManageMentImport.getSrcGoodId());
                successNum++;
            }
        }
        logger.error("failureNum：" + failureNum);
        logger.info("successNum：" + successNum);
        logger.info("结果字符串：" + resultStr);

        //调用大淘客接口验证源系统商品ID
        boolean flag = mallGoodsService.insertTbZeroGoods(resultStr);
        logger.info("导入excel源系统商品ID【" + resultStr + "】调用插入open库淘宝0元购商品结果【" + flag + "】。");

        Map<String, Object> dataMap = new HashMap<>();
        dataMap.put("successNum", successNum);
        dataMap.put("failureNum", failureNum);
        dataMap.put("resultStr", resultStr);

        return ResponseUtil.ok(dataMap);
    }


    public Object addGoods(Map<String,Object> map) {
        List<Map<String,String>> goodsList = (List<Map<String, String>>) map.get("goodsList");
//        List<MallSelectionManagement> list = new ArrayList<>();
        for (Map<String,String> goodsMap : goodsList ){
            MallSelectionManagement mallSelectionManagement = new MallSelectionManagement();
            mallSelectionManagement.setId(GraphaiIdGenerator.nextId("MallSelectionManagement"));
            mallSelectionManagement.setMallSelectionId(map.get("selectionId").toString());
            mallSelectionManagement.setProductId(goodsMap.get("goodsId"));
            mallSelectionManagement.setBak3(goodsMap.get("index"));
            mallSelectionManagement.setSource((byte) 1);
            mallSelectionManagement.setProductType((byte) 1);
            mallSelectionManagement.setState((byte) 1);
            mallSelectionManagementMapper.insert(mallSelectionManagement);
//            list.add(mallSelectionManagement);
        }

        return ResponseUtil.ok();
    }

    public Object getGoodsList(String selectionId,String goodsSn, String goodsName, Integer page, Integer size, String sort,
                               String order, String brandName, Integer priceMin, Integer priceMax, Integer platform, Boolean isHot,
                               Boolean isNew, Boolean isOnSale, Integer categorypid, Integer industryId, Integer goodsType,
                               Boolean isRightGoods) {
        MallAdmin adminUser = (MallAdmin) SecurityUtils.getSubject().getPrincipal();
        String deptId = "";
        if (adminUser != null) {
            deptId = adminUser.getDeptId();
        }
        List<MallRole> mallRoleList = iGraphaiSystemRoleService.selectRoleListMMByUserId(adminUser.getId());
        MallRole mallRole = null;
        if (mallRoleList.size() > 0) {
            mallRole = mallRoleList.get(0);
        }
        MallGoodsExample example = new MallGoodsExample();
        MallGoodsExample.Criteria criteria = example.createCriteria();

        if (!StringUtils.isEmpty(goodsSn)) {
            criteria.andGoodsSnEqualTo(goodsSn);
        }

        if (StringHelper.isNotNullAndEmpty(platform)) {
            criteria.andPlatformEqualTo(platform);
        }
        // type:0 自营商品、1:第三方商品 2:优惠券商品 3:权益商品 *
        List<String> ids = new ArrayList<>();
        if (!StringUtils.isEmpty(goodsType)) {
            if (goodsType == 3) {
                MallCategoryExample categoryExample = new MallCategoryExample();
                MallCategoryExample.Criteria criteria1 = categoryExample.createCriteria();
                criteria1.andPidEqualTo("1252056603188809730");
                criteria1.andDeletedEqualTo(false);
                criteria1.andStatusEqualTo("1");
                categoryExample.setDistinct(true);
                List<MallCategory> categories =
                        categoryMapper.selectByExampleSelective(categoryExample, MallCategory.Column.id);
                if (categories.size() > 0) {
                    // List<String> ids = new ArrayList<>();
                    for (MallCategory category : categories) {
                        ids.add(category.getId());
                    }
                    if (ids.size() > 0) {
                        criteria.andCategoryIdIn(ids);
                    }
                }
            } else {
                criteria.andVirtualGoodEqualTo(goodsType);
            }
        }
        criteria.andDeletedEqualTo(false);


        if (!StringUtils.isEmpty(goodsName)) {
            criteria.andNameLike("%" + goodsName + "%");
        }
        // * isRightGoods：权益商品 *
        // * ------------------------------------------------ *
        if (null != isRightGoods && isRightGoods) {
            MallCategoryExample categoryExample = new MallCategoryExample();
            MallCategoryExample.Criteria criteria1 = categoryExample.createCriteria();
            criteria1.andPidEqualTo("1252056603188809730");
            criteria1.andDeletedEqualTo(false);
            criteria1.andStatusEqualTo("1");
            categoryExample.setDistinct(true);
            List<MallCategory> categories =
                    categoryMapper.selectByExampleSelective(categoryExample, MallCategory.Column.id);
            if (categories.size() > 0) {
                // List<String> ids = new ArrayList<>();
                for (MallCategory category : categories) {
                    ids.add(category.getId());
                }
                if (ids.size() > 0) {
                    criteria.andCategoryIdIn(ids);
                }
            }
        }
        // * ------------------------------------------------ *

        if (isHot != null) {
            criteria.andIsHotEqualTo(isHot);
        }
        if (isNew != null) {
            criteria.andIsNewEqualTo(isNew);
        }
        if (isOnSale != null) {
            criteria.andIsOnSaleEqualTo(isOnSale);
        }
        if (priceMin != null) {
            criteria.andRetailPriceGreaterThanOrEqualTo(new BigDecimal(priceMin));
        }
        if (priceMax != null) {
            criteria.andRetailPriceLessThanOrEqualTo(new BigDecimal(priceMax));
        }

        if (!StringUtils.isEmpty(brandName)) {
            MallBrandExample brandExample = new MallBrandExample();
            MallBrandExample.Criteria brandcriteria = brandExample.createCriteria();

            brandcriteria.andDeletedEqualTo(false);
            brandcriteria.andNameLike("%" + brandName + "%");
            List<MallBrand> brandList = brandMapper.selectByExample(brandExample);
            List<String> brandIdList = new ArrayList<String>();
            if (brandList != null && brandList.size() > 0) {
                for (MallBrand brand : brandList) {
                    brandIdList.add(brand.getId());
                }
                criteria.andBrandIdIn(brandIdList);
            } else {
                criteria.andBrandIdEqualTo("-19223"); // 设置一个无法查询的ID
            }
        }

        // 数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限 5：仅本人数据权限）
        String dataScope = "1"; // 默认 1：全部数据权限
        if (mallRole != null) {
            if (!"".equals(mallRole.getDataScope())) {
                dataScope = mallRole.getDataScope();
            }
        }
        if ("2".equals(dataScope)) {
            // 2：自定数据权限
            List<String> roleIdList = iGraphaiSystemDeptService.selectDeptListByRoleId(mallRole.getId());
            criteria.andDeptIdIn(roleIdList);
        } else if ("3".equals(dataScope)) {
            // 3：本部门数据权限
            criteria.andDeptIdEqualTo(deptId);
        } else if ("4".equals(dataScope)) {
            // 4：本部门及以下数据权限
            List<String> deptIdList = iGraphaiSystemDeptService.selectDeptListByDeptId(deptId);
            criteria.andDeptIdIn(deptIdList);
        } else if ("5".equals(dataScope)) {
            // 5：仅本人数据权限
            criteria.andDeptIdEqualTo(adminUser.getMobile());
        } else {
            // 1：全部数据权限
        }
        criteria.andVirtualGoodNotEqualTo(3);// 积分商城商品过滤掉

        if (!StringUtils.isEmpty(sort) && !StringUtils.isEmpty(order)) {
            if (sort.equals("rebateAmount"))
                example.setOrderByClause("commission_rate " + order + ", retail_price " + order);
            else
                example.setOrderByClause(sort + " " + order);
        }
        MallSelectionManagementExample exampleSel = new MallSelectionManagementExample();
        MallSelectionManagementExample.Criteria criteriaSel = exampleSel.createCriteria();
        criteriaSel.andMallSelectionIdEqualTo(selectionId);
        criteriaSel.andDeletedEqualTo(false);
        List<MallSelectionManagement> goodsIdSel = mallSelectionManagementMapper.selectByExample(exampleSel);
        List<String> selGoodsId = new ArrayList<>();
        for (MallSelectionManagement selectionMana : goodsIdSel){
            selGoodsId.add(selectionMana.getProductId());
        }
        if (selGoodsId.size() == 0)
            return ResponseUtil.ok();
        criteria.andIdIn(selGoodsId);
        PageHelper.startPage(page, size);
        List<MallGoods> goodsList = goodsMapper.selectByExampleWithBLOBs(example);
        List<String> goodsIdList = new ArrayList<String>();
        List<String> thirdGoodsIdList = new ArrayList<>();
        for (MallGoods mallGoods : goodsList) {
            // 自营商品、权益商品
            if (mallGoods.getVirtualGood() == 0 || ids.contains(mallGoods.getCategoryId())) {
                goodsIdList.add(mallGoods.getId());
                mallGoods.setCommissionType(0);// 借用字段
            } else {
                // 第三方或其他
                thirdGoodsIdList.add(mallGoods.getId());
                mallGoods.setCommissionType(1);// 借用字段
            }
        }

        // 自营商品、权益商品
        if (!goodsIdList.isEmpty()) {
            List<Map<String, Object>> mallGoodsProductList = customGoodsMapper.queryGoodsProductInGoodsId(goodsIdList);
            for (Map<String, Object> goodsInfo : mallGoodsProductList) {
                for (MallGoods mallGoods : goodsList) {
                    if (goodsInfo.get("goodsId").equals(mallGoods.getId()) && goodsInfo.containsKey("pubSharePreFee")) {
                        mallGoods.setRebateAmount(new BigDecimal(goodsInfo.get("pubSharePreFee").toString()));// 返佣
                        mallGoods.setFactoryPrice(new BigDecimal(goodsInfo.get("factoryPrice").toString()));// 出厂价
                    }
                }
            }
        }

        // 第三方或其他
        if (!thirdGoodsIdList.isEmpty()) {
            // 计算第三方商品返佣
            for (String goodsId : thirdGoodsIdList) {
                for (MallGoods mallGoods : goodsList) {
                    if (goodsId.equals(mallGoods.getId())) {
                        if (mallGoods.getRebateAmount() == null && mallGoods.getCommissionRate() != null) {
                            BigDecimal commissionRate = new BigDecimal(mallGoods.getCommissionRate());
                            BigDecimal rate = commissionRate.divide(new BigDecimal(100));
                            BigDecimal rebateAmount = mallGoods.getRetailPrice().multiply(rate)
                                    .multiply(new BigDecimal("0.9")).setScale(2, BigDecimal.ROUND_HALF_DOWN);
                            mallGoods.setRebateAmount(rebateAmount);// 返佣
                        }
                    }
                }
            }
            MallProductCouponExample mallProductCouponExample = new MallProductCouponExample();
            MallProductCouponExample.Criteria productCouponCriteria = mallProductCouponExample.createCriteria();

            productCouponCriteria.andDeletedEqualTo(false);
            productCouponCriteria.andGoodsIdIn(thirdGoodsIdList);
            // 查询优惠券id
            List<MallProductCoupon> productCouponList =
                    mallProductCouponMapper.selectByExample(mallProductCouponExample);
            List<String> couponIds = new ArrayList<>();
            for (MallProductCoupon list : productCouponList) {
                couponIds.add(list.getCouponId());
            }
            if (!couponIds.isEmpty()) {
                MallCouponExample mallCouponExample = new MallCouponExample();
                MallCouponExample.Criteria couponCriteria = mallCouponExample.createCriteria();
                couponCriteria.andDeletedEqualTo(false);
                couponCriteria.andIdIn(couponIds);
                // 查询优惠券详细数据
                List<MallCoupon> couponList = mallCouponMapper.selectByExample(mallCouponExample);
                // 优惠券信息
                List<Map<String, Object>> couponListMap = new ArrayList<>();
                for (MallCoupon couList : couponList) {
                    for (MallProductCoupon list : productCouponList) {
                        if (couList.getId().equals(list.getCouponId())) {
                            Map<String, Object> couponMap = new HashMap<>();
                            couponMap.put("goodsId", list.getGoodsId());
                            couponMap.put("discount", couList.getDiscount());
                            couponListMap.add(couponMap);
                        }
                    }
                }
                if (!couponListMap.isEmpty()) {
                    for (MallGoods mallGoods : goodsList) {
                        for (Map<String, Object> couponInfo : couponListMap) {
                            if (couponInfo.get("goodsId").equals(mallGoods.getId())) {
                                BigDecimal factoryPrice = mallGoods.getRetailPrice()
                                        .subtract(new BigDecimal(couponInfo.get("discount").toString()))
                                        .setScale(2, BigDecimal.ROUND_HALF_DOWN);
                                mallGoods.setFactoryPrice(factoryPrice);// 卷后价，借用字段出厂价
                                mallGoods.setCouponDiscount(new BigDecimal(couponInfo.get("discount").toString()));// 优惠券金额
                            }
                        }
                    }
                }
            }
        }
        return goodsList;
    }

    @Transactional
    public Object delete(MallSelectionManagement selectionManagement) {
        String id = selectionManagement.getId();
        if (id == null) {
            return ResponseUtil.badArgument();
        }
        mallSelectionManagementMapper.logicalDeleteByPrimaryKey(id);
        return ResponseUtil.ok();
    }
}
