package com.graphai.mall.admin.web.promotion;

import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.mall.db.domain.MallSelection;
import com.graphai.mall.db.service.promotion.MallSelectionService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 选品活动
 *
 * @author qinxiang
 * @time 2020-07-20 10:10:26
 */
@RestController
@RequestMapping("/admin/selection")
@Validated
public class AdminSelectionController {

    private final Log logger = LogFactory.getLog(AdminSelectionController.class);

    @Resource
    private MallSelectionService mallSelectionService;

    @RequiresPermissions("admin:selection:list")
    @RequiresPermissionsDesc(menu = {"选品活动", "选品活动"}, button = "查询")
    @GetMapping("/list")
    public Object list(String activityName,
                       @RequestParam(required = false) String entranceLocation,
                       @RequestParam(required = false) Byte purchaseAmountDisplay,
                       @RequestParam(required = false) Byte recurrenceType,
                       @RequestParam(required = false) Byte status,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit) {
        List<MallSelection> list = mallSelectionService.selectActivityList(activityName, entranceLocation, purchaseAmountDisplay, recurrenceType, status, page, limit);
        long total = PageInfo.of(list).getTotal();
        Map<String, Object> dataMap = new HashMap<>();
        dataMap.put("total", total);
        dataMap.put("items", list);
        return ResponseUtil.ok(dataMap);
    }

    @RequiresPermissions("admin:selection:update")
    @RequiresPermissionsDesc(menu = {"选品活动", "选品活动"}, button = "编辑")
    @PostMapping("/update")
    public Object update(@RequestBody MallSelection mallSelection) {
        Object error = validateUpdate(mallSelection);
        if (error != null) {
            return error;
        }
        if (mallSelectionService.updateSelection(mallSelection) == 0) {
            return ResponseUtil.updatedDataFailed();
        }
        return ResponseUtil.ok(mallSelection);
    }

    @RequiresPermissions("admin:selection:create")
    @RequiresPermissionsDesc(menu = {"选品活动", "选品活动"}, button = "创建")
    @PostMapping("/create")
    public Object create(@RequestBody MallSelection mallSelection) {
        Object error = validateCreate(mallSelection);
        if (error != null) {
            return error;
        }
        if (mallSelectionService.createSelection(mallSelection) > 0) {
            return ResponseUtil.ok();
        }
        return ResponseUtil.fail();
    }

    @RequiresPermissions("admin:selection:isDisable")
    @RequiresPermissionsDesc(menu = {"选品活动", "选品活动"}, button = "禁用")
    @PostMapping("/isDisable")
    public Object isDisable(@RequestBody MallSelection mallSelection) {
        Object error = validateIsDisable(mallSelection);
        if (error != null) {
            return error;
        }
        if (mallSelectionService.updateSelection(mallSelection) > 0) {
            return ResponseUtil.ok();
        }
        return ResponseUtil.fail();
    }

    @RequiresPermissions("admin:selection:deleted")
    @RequiresPermissionsDesc(menu = {"选品活动", "选品活动"}, button = "删除")
    @PostMapping("/deleted")
    public Object deleted(@RequestBody MallSelection mallSelection) {
        Object error = validateDeleted(mallSelection);
        if (error != null) {
            return error;
        }
        mallSelection.setDeleted(true);
        if (mallSelectionService.updateSelection(mallSelection) > 0) {
            return ResponseUtil.ok();
        }
        return ResponseUtil.fail();
    }

    @RequiresPermissions("admin:selection:detail")
    @RequiresPermissionsDesc(menu = {"选品活动", "选品活动"}, button = "活动详情")
    @GetMapping("/detail")
    public Object detail(@NotNull String id) {
        return ResponseUtil.ok(mallSelectionService.getActivityDetail(id));
    }

    /**
     * 参数校验-validate
     *
     * @param mallSelection 选品对象
     * @return Object
     */
    private Object validate(MallSelection mallSelection) {
        String id = mallSelection.getId();
        if (StringUtils.isEmpty(id)) {
            return ResponseUtil.badArgument();
        }
        return null;
    }

    /**
     * 参数校验-update
     *
     * @param mallSelection 选品对象
     * @return Object
     */
    private Object validateUpdate(MallSelection mallSelection) {
        String id = mallSelection.getId();
        if (StringUtils.isEmpty(id)) {
            return ResponseUtil.badArgument();
        }
        return null;
    }

    /**
     * 参数校验-create
     *
     * @param mallSelection 选品对象
     * @return Object
     */
    private Object validateCreate(MallSelection mallSelection) {
        String name = mallSelection.getActivityName();
        if (StringUtils.isEmpty(name)) {
            return ResponseUtil.fail(402, "活动名称不能为空。");
        }
        String entranceLocation = mallSelection.getEntranceLocation();
        if (StringUtils.isEmpty(entranceLocation)) {
            return ResponseUtil.fail(402, "入口位置不能为空。");
        }
        Byte purchaseAmountDisplay = mallSelection.getPurchaseAmountDisplay();
        if (purchaseAmountDisplay == null) {
            return ResponseUtil.fail(402, "抢购金额显示不能为空。");
        }
        Byte recurrenceType = mallSelection.getRecurrenceType();
        if (recurrenceType == null) {
            return ResponseUtil.fail(402, "返现类型不能为空。");
        }
        return null;
    }

    /**
     * 参数校验-deleted
     *
     * @param mallSelection 选品对象
     * @return Object
     */
    private Object validateDeleted(MallSelection mallSelection) {
        String id = mallSelection.getId();
        if (StringUtils.isEmpty(id)) {
            return ResponseUtil.badArgument();
        }
        return null;
    }

    /**
     * 参数校验-isDisable
     *
     * @param mallSelection 选品对象
     * @return Object
     */
    private Object validateIsDisable(MallSelection mallSelection) {
        String id = mallSelection.getId();
        if (StringUtils.isEmpty(id)) {
            return ResponseUtil.fail(402, "id不能为空。");
        }
        //活动状态（0-可用, 1-禁用）
        String status = String.valueOf(mallSelection.getStatus());
        if (StringUtils.isEmpty(status)) {
            return ResponseUtil.fail(402, "活动状态不能为空。");
        }
        return null;
    }

}