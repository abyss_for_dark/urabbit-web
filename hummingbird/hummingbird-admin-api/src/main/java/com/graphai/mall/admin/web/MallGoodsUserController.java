package com.graphai.mall.admin.web;

import cn.afterturn.easypoi.excel.entity.ExportParams;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.lang.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.graphai.commons.util.servlet.ServletUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import org.springframework.web.bind.annotation.RestController;
import com.graphai.framework.generator.controller.IBaseController;

import com.graphai.mall.admin.domain.MallGoodsUser;
import com.graphai.mall.admin.service.IMallGoodsUserService;
import java.util.List;



/**
 * <p>
 * 商品商家中间表 前端控制器
 * </p>
 *
 * @author biteam
 * @since 2021-03-27
 */
@RestController
@RequestMapping("/admin/user")
public class MallGoodsUserController extends IBaseController<MallGoodsUser,  String >  {

	@Autowired
    private IMallGoodsUserService serviceImpl;
    
    @RequiresPermissions("admin:user:single")
    @RequiresPermissionsDesc(menu = {"单独查询"}, button = "单独查询")
	@GetMapping("/single/{id}")
    public Object getEntityById(@PathVariable  String  id) throws Exception{
        MallGoodsUser entityVName = serviceImpl.getById(id);
        return ResponseUtil.ok(entityVName);
    }
    
    @RequiresPermissions("admin:user:addOrUpdate")
    @RequiresPermissionsDesc(menu = {"新增"}, button = "新增")
    @PostMapping("/addOrUpdate")
    public Object addOrUpdate(MallGoodsUser mallGoodsUser) throws Exception{
        if(serviceImpl.saveOrUpdate(mallGoodsUser)){
            return ResponseUtil.ok();
        }else{
            return ResponseUtil.fail();
        }
    } 
	
	
	@RequiresPermissions("admin:user:melist")
	@RequiresPermissionsDesc(menu = {"商品商家中间表查询"}, button = "查询")
    @GetMapping("/melist")
    public Object getMeList() throws Exception {
        IPage<MallGoodsUser> ipage = serviceImpl.page(getPlusPage());
        return ResponseUtil.ok(ipage);
    }
 	
 	@RequiresPermissions("admin:user:save")
 	@RequiresPermissionsDesc(menu = {"商品商家中间表新增"}, button = "新增")
    @PostMapping("/save")
    public Object save(@ModelAttribute MallGoodsUser mallGoodsUser) throws Exception {
        serviceImpl.save(mallGoodsUser);
        return ResponseUtil.ok();
    }
	
	@RequiresPermissions("admin:user:modify")
	@RequiresPermissionsDesc(menu = {"商品商家中间表修改"}, button = "修改")
    @PutMapping("/modify")
    public Object modify(@ModelAttribute  MallGoodsUser mallGoodsUser){
        serviceImpl.updateById(mallGoodsUser);
        return  ResponseUtil.ok();
    }

    @RequiresPermissions("admin:user:del")
    @RequiresPermissionsDesc(menu = {"商品商家中间表删除"}, button = "删除")
    @DeleteMapping("/del/{id}")
    public Object deleteById(@PathVariable  String  id){
        serviceImpl.removeById(id);
        return  ResponseUtil.ok();
    }

}

