package com.graphai.mall.admin.web.game;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.mall.db.service.game.GameStatisticsService;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;


/**
 * 游戏统计
 *
 * @author LaoGF
 * @since 2020-07-02
 */
@RestController
@RequestMapping("/admin/gameStatistics")
@Validated
public class AdminGameStatisticsController {

    @Resource
    private GameStatisticsService gameStatisticsService;

    /**
     * 统计收益
     */
    @RequiresPermissions("admin:gameStatistics:reward")
    @RequiresPermissionsDesc(menu = {"数据统计", "游戏统计"}, button = "查询")
    @GetMapping("/reward")
    public Object statisticsReward() {
        return ResponseUtil.ok(gameStatisticsService.statisticsReward());
    }

    /**
     * 根据最近天数统计收益
     */
    @GetMapping("/rewardGroupByDay")
    public Object statisticsRewardGroupByDay(@RequestParam Integer day) {
        return ResponseUtil.ok(gameStatisticsService.statisticsRewardGroupByDay(day == null ? 30 : day));
    }

}
