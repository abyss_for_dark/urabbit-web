package com.graphai.mall.admin.web.game;

import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.mall.db.domain.GameDefinition;
import com.graphai.mall.db.service.game.GameDefinitionService;
import com.graphai.validator.Order;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 游戏定义
 */
@RestController
@RequestMapping("/admin/gameDefinition")
@Validated
public class AdminGameDefinitionController {

    @Autowired
    private GameDefinitionService gameDefinitionService;

    /**
     * 查询游戏定义
     */
    @GetMapping("/list")
    public Object list(@RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit,
                       @RequestParam(defaultValue = "id") String sort,
                       @Order @RequestParam(defaultValue = "desc") String order) {
        GameDefinition gameDefinition = new GameDefinition();
        List<GameDefinition> list = gameDefinitionService.selectGameDefinitionList(gameDefinition, page, limit, sort, order);
        long total = PageInfo.of(list).getTotal();
        Map<String, Object> data = new HashMap<>();
        data.put("total", total);
        data.put("items", list);

        return ResponseUtil.ok(data);
    }

    /**
     * 添加游戏定义
     */
    @PostMapping("/add")
    public Object addSave(@RequestBody GameDefinition gameDefinition) {
        Object error = validate(gameDefinition);
        if (error != null) {
            return error;
        }
        gameDefinitionService.insertGameDefinition(gameDefinition);
        return ResponseUtil.ok();
    }


    /**
     * 查看游戏定义详情
     */
    @GetMapping("/edit/{id}")
    public Object edit(@PathVariable("id") String id) {
        GameDefinition gameDefinition = gameDefinitionService.selectGameDefinitionById(id);
        return ResponseUtil.ok(gameDefinition);
    }

    /**
     * 修改游戏定义
     */
    @PostMapping("/edit")
    public Object editSave(@RequestBody GameDefinition gameDefinition) {
        //等级主键id
        if (StringUtils.isEmpty(gameDefinition.getId())) {
            return ResponseUtil.badArgumentValue("没有获取到id！");
        }
        Object error = validate(gameDefinition);
        if (error != null) {
            return error;
        }
        gameDefinitionService.updateGameDefinition(gameDefinition);
        return ResponseUtil.ok();
    }


    /**
     * 校验数据(游戏定义)
     */
    private Object validate(GameDefinition gameDefinition) {
        if (StringUtils.isEmpty(gameDefinition.getGameName())) {
            return ResponseUtil.badArgument();
        }
        if (StringUtils.isEmpty(gameDefinition.getDescription())) {
            return ResponseUtil.badArgument();
        }
        if (StringUtils.isEmpty(gameDefinition.getBanner())) {
            return ResponseUtil.badArgument();
        }
        if (StringUtils.isEmpty(gameDefinition.getGameType())) {
            return ResponseUtil.badArgument();
        }

        return null;
    }
}
