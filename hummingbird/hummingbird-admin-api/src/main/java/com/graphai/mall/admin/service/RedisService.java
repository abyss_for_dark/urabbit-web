package com.graphai.mall.admin.service;

import com.graphai.mall.db.domain.redis.RedisInfo;
//import org.springframework.web.servlet.HandlerExecutionChain;

import java.util.List;
import java.util.Map;
/**
 * redis - 服务层
 * @author Qxian
 * @date 2020-11-30
 */
public interface RedisService {

	/**
	 * 获取 redis 的详细信息
	 *
	 * @return List
	 */
	List<RedisInfo> getRedisInfo() throws Exception;

	/**
	 * 获取 redis key 数量
	 *
	 * @return Map
	 */
	Map<String, Object> getKeysSize() throws Exception;

	/**
	 * 获取 redis 内存信息
	 *
	 * @return Map
	 */
	Map<String, Object> getMemoryInfo() throws Exception;

}
