package com.graphai.mall.admin.web.promotion;


import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.mall.db.domain.FcAccountWithdrawBill;
import com.graphai.mall.db.domain.MallAdmin;
import com.graphai.mall.db.service.promotion.PromotionChannelService;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * 提现记录
 *
 * @author LaoGF
 * @since 2020-07-13
 */
@RestController
@RequestMapping("/admin/promotionChannelWithdraw")
@Validated
public class PromotionChannelWithdrawController {

    @Resource
    private PromotionChannelService promotionChannelService;


    /**
     * 提现记录列表
     */
    @RequiresPermissions("admin:promotionChannelWithdraw:list")
    @RequiresPermissionsDesc(menu = {"推广渠道后台", "提现记录"}, button = "查询")
    @GetMapping("/list")
    public Object selectWithdraw(@RequestParam(value = "beginCreateDate", required = false) String beginCreateDate,
                                 @RequestParam(value = "endCreateDate", required = false) String endCreateDate,
                                 @RequestParam(value = "beginUpdateDate", required = false) String beginUpdateDate,
                                 @RequestParam(value = "endUpdateDate", required = false) String endUpdateDate) {
        //获取当前用户
        MallAdmin adminUser = (MallAdmin) SecurityUtils.getSubject().getPrincipal();
        List<FcAccountWithdrawBill> list = promotionChannelService.selectWithdraw(adminUser.getId(), beginCreateDate, endCreateDate, beginUpdateDate, endUpdateDate);
        return ResponseUtil.ok(list);
    }


}
