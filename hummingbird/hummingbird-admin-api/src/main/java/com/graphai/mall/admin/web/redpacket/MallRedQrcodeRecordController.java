package com.graphai.mall.admin.web.redpacket;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.graphai.mall.admin.vo.GameRedpacketRecordVo;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.framework.generator.controller.IBaseController;
import com.graphai.mall.admin.domain.MallRedQrcodeRecord;
import com.graphai.mall.admin.service.IMallRedQrcodeRecordService;
import com.graphai.mall.admin.vo.MallRedQrcodeRecordVo;


/**
 * <p>
 * 红包码领取记录 前端控制器
 * </p>
 *
 * @author biteam
 * @since 2020-12-10
 */
@RestController
@RequestMapping("/admin/record")
public class MallRedQrcodeRecordController extends IBaseController<MallRedQrcodeRecord, String> {

    @Autowired
    private IMallRedQrcodeRecordService serviceImpl;

    @RequiresPermissions("admin:record:single")
    @RequiresPermissionsDesc(menu = {"单独查询"}, button = "单独查询")
    @GetMapping("/single/{id}")
    public Object getEntityById(@PathVariable String id) throws Exception {
        MallRedQrcodeRecord entityVName = serviceImpl.getById(id);
        return ResponseUtil.ok(entityVName);
    }

    @RequiresPermissions("admin:record:addOrUpdate")
    @RequiresPermissionsDesc(menu = {"新增"}, button = "新增")
    @PostMapping("/addOrUpdate")
    public Object addOrUpdate(MallRedQrcodeRecord mallRedQrcodeRecord) throws Exception {
        if (serviceImpl.saveOrUpdate(mallRedQrcodeRecord)) {
            return ResponseUtil.ok();
        } else {
            return ResponseUtil.fail();
        }
    }

    @RequiresPermissions("admin:record:melist")
    @RequiresPermissionsDesc(menu = {"红包码领取记录查询"}, button = "查询")
    @GetMapping("/melist")
    public Object getMeList(String code, String userName, String redpacketId, String toUserId,
                    @RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit) {
        List<MallRedQrcodeRecordVo> recodeList = serviceImpl.selectVoList(code, userName, redpacketId, toUserId, page, limit);
        Integer total = serviceImpl.getRecordTotal(code, userName, redpacketId, toUserId);
        Map<String, Object> data = new HashMap<>();
        data.put("total", total);
        data.put("items", recodeList);
        return ResponseUtil.ok(data);

    }

    @RequiresPermissions("admin:record:melist")
    @RequiresPermissionsDesc(menu = {"整点领取记录查询"}, button = "查询")
    @GetMapping("/melistGame")
    public Object getMeListGame(String platformResource, String adName, String toUserName, String redpacketId, String toUserId,
                            @RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit) {
        List<GameRedpacketRecordVo> recodeList = serviceImpl.selectListGame(platformResource, adName, toUserName, redpacketId, toUserId, page, limit);
        Integer total = serviceImpl.getRecordTotalGame(platformResource, adName, toUserName, redpacketId, toUserId);
        Map<String, Object> data = new HashMap<>();
        data.put("total", total);
        data.put("items", recodeList);
        return ResponseUtil.ok(data);

    }

    @RequiresPermissions("admin:record:melist")
    @RequiresPermissionsDesc(menu = {"现金红包和整点红包合拼记录查询"}, button = "查询")
    @GetMapping("/melistRed")
    public Object getMeListRed(@ModelAttribute MallRedQrcodeRecordVo mallRedQrcodeRecordVo, @RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit) {
        mallRedQrcodeRecordVo.setPage((page - 1) * limit);
        mallRedQrcodeRecordVo.setLimit(limit);
        List<MallRedQrcodeRecordVo> recodeList = serviceImpl.selectListRed(mallRedQrcodeRecordVo);
        Integer total = serviceImpl.getRecordTotalRed(mallRedQrcodeRecordVo); // 总记录数

        BigDecimal zeroBigDecimal = new BigDecimal("0.00");
        BigDecimal platformTotalAmountUser = zeroBigDecimal;
        BigDecimal platformTotalAmountPro = zeroBigDecimal;
        BigDecimal platformTotalAmount = zeroBigDecimal;
        mallRedQrcodeRecordVo.setTypeOfClaim("01");
        platformTotalAmountUser = serviceImpl.getGlatformTotalAmountByType(mallRedQrcodeRecordVo); // 用户收益（元）
        mallRedQrcodeRecordVo.setTypeOfClaim("02");
        platformTotalAmountPro = serviceImpl.getGlatformTotalAmountByType(mallRedQrcodeRecordVo);  // 推荐收益（元）
        mallRedQrcodeRecordVo.setTypeOfClaim("");
        platformTotalAmount = serviceImpl.getGlatformTotalAmountByType(mallRedQrcodeRecordVo);     // 平台发放金额（元）
        Map<String, Object> data = new HashMap<>();
        data.put("platformTotalAmountUser", platformTotalAmountUser);
        data.put("platformTotalAmountPro", platformTotalAmountPro);
        data.put("platformTotalAmount", platformTotalAmount);
        data.put("total", total);
        data.put("items", recodeList);
        return ResponseUtil.ok(data);

    }

    @RequiresPermissions("admin:record:save")
    @RequiresPermissionsDesc(menu = {"红包码领取记录新增"}, button = "新增")
    @PostMapping("/save")
    public Object save(@ModelAttribute MallRedQrcodeRecord mallRedQrcodeRecord) throws Exception {
        serviceImpl.save(mallRedQrcodeRecord);
        return ResponseUtil.ok();
    }

    @RequiresPermissions("admin:record:modify")
    @RequiresPermissionsDesc(menu = {"红包码领取记录修改"}, button = "修改")
    @PutMapping("/modify")
    public Object modify(@ModelAttribute MallRedQrcodeRecord mallRedQrcodeRecord) {
        serviceImpl.updateById(mallRedQrcodeRecord);
        return ResponseUtil.ok();
    }

    @RequiresPermissions("admin:record:del")
    @RequiresPermissionsDesc(menu = {"红包码领取记录删除"}, button = "删除")
    @DeleteMapping("/del/{id}")
    public Object deleteById(@PathVariable String id) {
        serviceImpl.removeById(id);
        return ResponseUtil.ok();
    }

}

