package com.graphai.mall.admin.web.promotion;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.mall.db.domain.MallAd;
import com.graphai.mall.db.service.advertisement.MallAdService;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/admin/promoteImgs")
public class AdminPromoteImgsController {

    @Autowired
    private MallAdService adService;

    @RequiresPermissions("admin:promoteImgs:list")
    @RequiresPermissionsDesc(menu = {"推广渠道", "推广素材"}, button = "查询")
    @GetMapping("/list")
    public Object list(@RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit) {
        PageHelper.startPage(page, limit);
        List<MallAd> ads = adService.queryIndex((byte) 112);
        long total = PageInfo.of(ads).getTotal();

        HashMap<String, Object> data = new HashMap<>();
        data.put("total", total);
        data.put("items", ads);
        return ResponseUtil.ok(data);
    }

    @RequiresPermissions("admin:promoteImgs:delete")
    @RequiresPermissionsDesc(menu = {"推广渠道", "推广素材"}, button = "删除")
    @PostMapping("/delete")
    public Object delete(@RequestParam String ids) {
        adService.deleteByIds(ids);
        return ResponseUtil.ok();
    }

    @RequiresPermissions("admin:promoteImgs:add")
    @RequiresPermissionsDesc(menu = {"推广渠道", "推广素材"}, button = "新增")
    @PostMapping("/add")
    public Object add(@RequestParam String url, String url2) {
        MallAd ad = new MallAd();
        ad.setDeleted(false);
        ad.setEnabled(true);
        ad.setIndustryId("0");
        ad.setPosition((byte) 112);
        ad.setUrl(url);
        ad.setBanner(url2);//缩略图
        adService.add(ad);
        return ResponseUtil.ok();
    }
}
