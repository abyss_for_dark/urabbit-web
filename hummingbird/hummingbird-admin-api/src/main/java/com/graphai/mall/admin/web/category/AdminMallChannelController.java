package com.graphai.mall.admin.web.category;

import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.RegexUtil;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.bcrypt.BCryptPasswordEncoder;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.mall.db.domain.MallAdmin;
import com.graphai.mall.db.domain.MallChannel;
import com.graphai.mall.db.service.MallAdminService;
import com.graphai.mall.db.service.account.AccountService;
import com.graphai.mall.db.service.category.MallChannelService;
import com.graphai.mall.db.vo.MallChannelStatisticVo;
import com.graphai.mall.db.vo.MallChannelVo;
import com.graphai.validator.Order;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import static com.graphai.framework.constant.AdminResponseCode.*;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 商城渠道商链接注册次数记录
 */
@RestController
@RequestMapping("/admin/channel")
@Validated
public class AdminMallChannelController {
    private final Log logger = LogFactory.getLog(AdminMallChannelController.class);

    @Autowired
    private MallChannelService mallChannelService;
    @Autowired
    private MallAdminService mallAdminService;
    @Autowired
    private AccountService accountService;

    @RequiresPermissions("admin:channel:list")
    @RequiresPermissionsDesc(menu = {"推广渠道", "渠道管理"}, button = "查询")
    @GetMapping("/list")
    public Object list(@RequestParam(required = false) String channelName,
                       @RequestParam(required = false) String settlementMethod,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit,
                       @RequestParam(defaultValue = "id") String sort,
                       @Order @RequestParam(defaultValue = "desc") String order) {
        //todo 判断登录用户的角色，若角色为系统管理员则可以查看所有，若角色为渠道商则只能看自己的
        String mallAdminId = null;


        List<MallChannelVo> mallChannelList = mallChannelService.selectMallChannelVoList(mallAdminId, channelName, settlementMethod, page, limit, sort, order);
        long total = PageInfo.of(mallChannelList).getTotal();
        Map<String, Object> data = new HashMap<>();
        data.put("total", total);
        data.put("items", mallChannelList);
        return ResponseUtil.ok(data);
    }


    @RequiresPermissions("admin:channel:create")
    @RequiresPermissionsDesc(menu = {"推广渠道", "渠道管理"}, button = "添加")
    @PostMapping("/add")
    public Object add(@RequestBody MallChannelVo mallChannelVo) throws Exception {
        Object error = validate(mallChannelVo);
        if (error != null) {
            return error;
        }
        String username = mallChannelVo.getUsername();
        String password = mallChannelVo.getPassword();
        Object userError = validateUser(username, password);
        if (userError != null) {
            return userError;
        }

        //创建渠道商账号
        List<MallAdmin> adminList = mallAdminService.findAdminByUserName(username);
        if (adminList.size() > 0) {
            return ResponseUtil.fail(ADMIN_NAME_EXIST, "账号已经存在");
        }
        //密码加密
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String encodedPassword = encoder.encode(password);

        MallAdmin admin = new MallAdmin();
        admin.setUsername(username);
        admin.setPassword(encodedPassword);
        //todo 用户赋予角色

        mallAdminService.add(admin);
        /** 渠道商账户开户 **/
        if (admin.getId() != null) {
            logger.info("开户成功..............");
            accountService.accountTradeCreateAccount(admin.getId().toString());
        }

        mallChannelVo.setMallAdminId(admin.getId());
        mallChannelService.insertMallChannel(mallChannelVo);
        return ResponseUtil.ok(admin);
    }


    @RequiresPermissions("admin:channel:update")
    @RequiresPermissionsDesc(menu = {"推广渠道", "渠道管理"}, button = "修改")
    @PostMapping("/update")
    public Object update(@RequestBody MallChannelVo mallChannelVo) {
        //校验参数
        Object error = validate(mallChannelVo);
        if (error != null) {
            return error;
        }
        mallChannelService.updateMallChannel(mallChannelVo);

//        //修改渠道商账号
//        String username = mallChannelVo.getUsername();
//        List<MallAdmin> adminList = mallAdminService.findAdminByUserName(username);
//        if (adminList.size() == 1) {
//            //密码加密
//            String password = mallChannelVo.getPassword();
//            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
//            String encodedPassword = encoder.encode(password);
//
//            MallAdmin admin = adminList.get(0);
//            admin.setUsername(username);
//            admin.setPassword(encodedPassword);
//            mallAdminService.updateById(admin);
//        }
        return ResponseUtil.ok();
    }


    @RequiresPermissions("admin:channel:update")
//    @RequiresPermissionsDesc(menu={"推广渠道" , "渠道管理"}, button="修改状态")
    @PostMapping("/updateStatus")
    public Object updateStatus(@RequestParam String id,
                               @RequestParam Boolean deleted) {
        MallChannel channel = mallChannelService.selectMallChannelById(id);
        if (channel == null) {
            return ResponseUtil.badArgumentValue("请选择正确的渠道！");
        }
        MallChannel mallChannel = new MallChannel();
        mallChannel.setId(id);
        mallChannel.setDeleted(deleted);
        mallChannelService.updateMallChannel(mallChannel);

        //若关闭渠道，则直接删除账号
        MallAdmin admin = new MallAdmin();
        admin.setId(channel.getMallAdminId());
        admin.setDeleted(deleted);
        mallAdminService.updateById(admin);
        return ResponseUtil.ok();
    }


    @RequiresPermissions("admin:channel:update")
//    @RequiresPermissionsDesc(menu={"推广渠道" , "渠道管理"}, button="重置密码")
    @PostMapping("/resetPassword")
    public Object resetPassword(@RequestParam String id,
                                @RequestParam String newPassword,
                                @RequestParam String rePassword) {
        if (StringUtils.isEmpty(id) || StringUtils.isEmpty(newPassword) || StringUtils.isEmpty(rePassword)) {
            return ResponseUtil.badArgument();
        }
        if (!newPassword.equals(rePassword)) {
            return ResponseUtil.badArgumentValue("两次输入的密码不一致！");
        }
        if (StringUtils.isEmpty(newPassword) || newPassword.length() < 6) {
            return ResponseUtil.fail(ADMIN_INVALID_PASSWORD, "管理员密码长度不能小于6");
        }
        MallChannel channel = mallChannelService.selectMallChannelById(id);
        if (channel == null) {
            return ResponseUtil.badArgumentValue("请选择正确的渠道！");
        }
        if (StringUtils.isEmpty(channel.getMallAdminId())) {
            return ResponseUtil.badArgumentValue("该渠道不存在账号！");
        }
        //密码加密
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String encodedPassword = encoder.encode(newPassword);

        //若关闭渠道，则直接删除账号
        MallAdmin admin = new MallAdmin();
        admin.setId(channel.getMallAdminId());
        admin.setPassword(encodedPassword);
        mallAdminService.updateById(admin);
        return ResponseUtil.ok();
    }


    @RequiresPermissions("admin:channel:statisticList")
    @RequiresPermissionsDesc(menu = {"推广渠道", "推广效果"}, button = "查询")
    @GetMapping("/statisticList")
    public Object statisticList(@RequestParam String beginDate,
                                @RequestParam String endDate,
                                @RequestParam(defaultValue = "1") Integer page,
                                @RequestParam(defaultValue = "10") Integer limit,
                                @RequestParam(defaultValue = "id") String sort,
                                @Order @RequestParam(defaultValue = "desc") String order) {
        //todo 判断登录用户的角色，若角色为系统管理员则可以查看所有，若角色为渠道商则只能看自己的
        String mallAdminId = null;


        List<MallChannelStatisticVo> mallChannelStatisticList = mallChannelService.selectChannelStatisticVoList(mallAdminId, beginDate, endDate, page, limit, sort, order);
        long total = PageInfo.of(mallChannelStatisticList).getTotal();
        Map<String, Object> data = new HashMap<>();
        for (MallChannelStatisticVo channel : mallChannelStatisticList) {
            BigDecimal money = new BigDecimal(0.00);
            //安卓、ios总的激活量
            Integer andriodCount = channel.getAndriodCount();
            Integer iosCount = channel.getIosCount();
            Integer count = andriodCount + iosCount;
            if (count > 0) {
                if (channel.getSettlementValue() != null) {
                    money = channel.getSettlementValue().multiply(new BigDecimal(count));
                }
            }
            channel.setMoney(money);
        }

        data.put("total", total);
        data.put("items", mallChannelStatisticList);
        return ResponseUtil.ok(data);
    }


    //参数验证
    private Object validate(MallChannelVo mallChannelVo) {
        if (StringUtils.isEmpty(mallChannelVo.getChannelName())) {
            return ResponseUtil.badArgument();
        }
        if (StringUtils.isEmpty(mallChannelVo.getCompany())) {
            return ResponseUtil.badArgument();
        }
        if (StringUtils.isEmpty(mallChannelVo.getPerson())) {
            return ResponseUtil.badArgument();
        }
        if (StringUtils.isEmpty(mallChannelVo.getMobile())) {
            return ResponseUtil.badArgument();
        }
        if (StringUtils.isEmpty(mallChannelVo.getSettlementMethod())) {
            return ResponseUtil.badArgument();
        }
        if (mallChannelVo.getSettlementValue() == null) {
            return ResponseUtil.badArgument();
        }
        if (StringUtils.isEmpty(mallChannelVo.getSettlementCycle())) {
            return ResponseUtil.badArgument();
        }
        return null;
    }

    private Object validateUser(String name, String password) {
        if (StringUtils.isEmpty(name)) {
            return ResponseUtil.badArgument();
        }
        if (!RegexUtil.isUsername(name)) {
            return ResponseUtil.fail(ADMIN_INVALID_NAME, "账号名称不符合规定");
        }
        if (StringUtils.isEmpty(password) || password.length() < 6) {
            return ResponseUtil.fail(ADMIN_INVALID_PASSWORD, "账号密码长度不能小于6");
        }
        return null;
    }
}
