package com.graphai.mall.admin.web.statistics;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.lang.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.graphai.commons.util.servlet.ServletUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.baomidou.mybatisplus.core.metadata.IPage;

import org.springframework.web.bind.annotation.RestController;

import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.framework.generator.controller.IBaseController;

import com.graphai.mall.admin.domain.MallPageView;
import com.graphai.mall.admin.service.IMallPageViewService;



/**
 * <p>
 * 广告渠道点击明细 前端控制器
 * </p>
 *
 * @author biteam
 * @since 2020-12-30
 */
@RestController
@RequestMapping("/admin/view")
public class MallPageViewController extends IBaseController<MallPageView,  String >  {

	@Autowired
    private IMallPageViewService serviceImpl;
    
    @RequiresPermissions("admin:view:single")
    @RequiresPermissionsDesc(menu = {"单独查询"}, button = "单独查询")
	@GetMapping("/single/{id}")
    public Object getEntityById(@PathVariable  String  id) throws Exception{
        MallPageView entityVName = serviceImpl.getById(id);
        return ResponseUtil.ok(entityVName);
    }
    
    @RequiresPermissions("admin:view:addOrUpdate")
    @RequiresPermissionsDesc(menu = {"新增"}, button = "新增")
    @PostMapping("/addOrUpdate")
    public Object addOrUpdate(MallPageView mallPageView) throws Exception{
        if(serviceImpl.saveOrUpdate(mallPageView)){
            return ResponseUtil.ok();
        }else{
            return ResponseUtil.fail();
        }
    } 
	
	
	@RequiresPermissions("admin:view:melist")
	@RequiresPermissionsDesc(menu = {"广告渠道点击明细查询"}, button = "查询")
    @GetMapping("/melist")
    public Object getMeList() throws Exception {
        IPage<MallPageView> ipage = serviceImpl.page(getPlusPage(),listQueryWrapper());
        return ResponseUtil.ok(ipage);
    }
 	
 	private QueryWrapper<MallPageView> listQueryWrapper() {
 		QueryWrapper<MallPageView> meq = new QueryWrapper<>();
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("pvName"))) {
		      meq.eq("pv_name", ServletUtils.getParameter("pvName"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("pvUrl"))) {
		      meq.eq("pv_url", ServletUtils.getParameter("pvUrl"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("count"))) {
		      meq.eq("count", ServletUtils.getParameter("count"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("mobileType"))) {
		      meq.eq("mobile_type", ServletUtils.getParameter("mobileType"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("outUserId"))) {
		      meq.eq("out_user_id", ServletUtils.getParameter("outUserId"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("type"))) {
		      meq.eq("type", ServletUtils.getParameter("type"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("clientIp"))) {
		      meq.eq("client_ip", ServletUtils.getParameter("clientIp"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("platform"))) {
		      meq.eq("platform", ServletUtils.getParameter("platform"));
		}
		
        return meq;
    }
 	@RequiresPermissions("admin:view:save")
 	@RequiresPermissionsDesc(menu = {"广告渠道点击明细新增"}, button = "新增")
    @PostMapping("/save")
    public Object save(@ModelAttribute MallPageView mallPageView) throws Exception {
        serviceImpl.save(mallPageView);
        return ResponseUtil.ok();
    }
	
	@RequiresPermissions("admin:view:modify")
	@RequiresPermissionsDesc(menu = {"广告渠道点击明细修改"}, button = "修改")
    @PutMapping("/modify")
    public Object modify(@ModelAttribute  MallPageView mallPageView){
        serviceImpl.updateById(mallPageView);
        return  ResponseUtil.ok();
    }
    
    @RequiresPermissions("admin:view:del")
    @RequiresPermissionsDesc(menu = {"广告渠道点击明细删除"}, button = "删除")
    @DeleteMapping("/del/{id}")
    public Object deleteById(@PathVariable  String  id){
        serviceImpl.removeById(id);
        return  ResponseUtil.ok();
    }
    
}

