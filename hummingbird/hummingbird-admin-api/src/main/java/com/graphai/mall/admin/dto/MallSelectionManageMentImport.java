package com.graphai.mall.admin.dto;

import com.graphai.framework.annotation.Excel;

/**
 * 选品活动-添加选品-导入商品ID
 * Created by qinxiang on 2020-07-27 15:06
 */
public class MallSelectionManageMentImport {

    @Excel(name = "商品ID")
    private String srcGoodId;

    public String getSrcGoodId() {
        return srcGoodId;
    }

    public void setSrcGoodId(String srcGoodId) {
        this.srcGoodId = srcGoodId;
    }
}
