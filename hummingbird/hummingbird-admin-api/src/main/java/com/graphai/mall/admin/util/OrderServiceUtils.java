package com.graphai.mall.admin.util;

import java.math.BigDecimal;
import java.util.List;

import com.graphai.mall.admin.dao.MallMerchantMapper;
import com.graphai.mall.admin.domain.MallMerchant;
import com.graphai.mall.admin.domain.MallPurchaseOrder;
import com.graphai.mall.admin.domain.MallPurchaseOrderGoods;
import com.graphai.mall.admin.domain.MallUserMerchant;
import com.graphai.mall.db.constant.account.AccountStatus;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.lang3.ArrayUtils;

import com.graphai.commons.util.lang.StringUtils;
import com.graphai.mall.admin.vo.OrderVo;
import com.graphai.mall.db.domain.MallOrder;
import com.graphai.mall.db.domain.MallOrderGoods;
import com.graphai.mall.db.domain.MallUser;
import org.nustaq.offheap.bytez.malloc.MallocBytez;
import org.springframework.beans.factory.annotation.Autowired;

public class OrderServiceUtils {

    public static void setOrderVoPlatform(MallOrder order2, OrderVo orderVo) {
        switch (order2.getHuid()) {
            case "dtk":
                orderVo.setPlatform("淘宝");
                break;
            case "pdd":
                orderVo.setPlatform("拼多多");
                break;
            case "sn":
                orderVo.setPlatform("苏宁易购");
                break;
            case "jd":
                orderVo.setPlatform("京东");
                break;
            case "wph":
                orderVo.setPlatform("唯品会");
                break;
            case "zmGoods":
                orderVo.setPlatform("中闵");
                break;
            case "fuluGoods":
                orderVo.setPlatform("福禄");
                break;
            case "lsxdGoods":
                orderVo.setPlatform("蓝色兄弟");
                break;
            case "mt":
                orderVo.setPlatform("美团");
                break;
            case "elm":
                orderVo.setPlatform("饿了么");
                break;
            case "kfc":
                orderVo.setPlatform("肯德基");
                break;
            case "groupgas":
                orderVo.setPlatform("团油");
                order2.setBid(AccountStatus.BID_25);
                break;
            case "sdw":
                orderVo.setPlatform("闪电玩");
                break;
            case "wx":
                orderVo.setPlatform("微信");
                break;
            case "huifu":
                orderVo.setPlatform("汇付");
                break;
        }
    }
    public static void setOrderSource(MallOrder order2, OrderVo orderVo) {
        if (com.graphai.mall.admin.util.StringUtils.isNotEmpty(order2.getSource())){
        switch (order2.getSource()) {
            case "alipay":
                orderVo.setSource("支付宝支付");
                break;
            case "xcx":
                orderVo.setSource("小程序支付");
                break;
            case "test":
                orderVo.setSource("测试支付");
                break;
            case "wx":
                orderVo.setSource("微信支付");
                break;
            case "huifu":
                orderVo.setSource("汇付支付");
                break;
        }}
    }

    public static void setOrderVo(MallOrder order2, OrderVo orderVo, MallUser mallUser, String type, List<MallOrderGoods> orderGoodsList, List<OrderVo> ordervoList) {
        try {
            BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
            BeanUtils.copyProperties(orderVo, order2);
            orderVo.setAddTime(order2.getAddTime());
            orderVo.setPayTime(order2.getPayTime());

            setOrderSource(order2, orderVo);
            orderVo.setOrderId(order2.getId());
            orderVo.setOrderSn(order2.getOrderSn());
            orderVo.setShipSn(order2.getShipSn());
            if (null != mallUser && StringUtils.isNotEmpty(mallUser.getChannelId())) {
                orderVo.setChannelId(mallUser.getChannelId());
            } else {
                orderVo.setChannelId("");
            }

            if (null != mallUser && !StringUtils.isEmpty(mallUser.getMobile())) {
                orderVo.setMobile(mallUser.getMobile());
            } else {
                orderVo.setMobile("");
            }
            if (!StringUtils.isEmpty(order2.getMobile())) {
                orderVo.setReceivingMobile(order2.getMobile());
            } else {
                orderVo.setReceivingMobile("");
            }
            if (!StringUtils.isEmpty(order2.getTicket())) {
                orderVo.setTicket(order2.getTicket());
            } else {
                orderVo.setTicket("");
            }
            if (null != order2.getActualPrice()) {
                orderVo.setActualPrice(order2.getActualPrice());
            }
            if (null != order2.getCouponPrice() || null != order2.getIntegralPrice()) {
                BigDecimal specialPrice = order2.getCouponPrice().add(order2.getIntegralPrice());
                orderVo.setSpecialPrice(specialPrice);
            }
            if (null != order2.getCouponPrice()) {
                orderVo.setCouponPrice(order2.getCouponPrice());
            }
            if (null != mallUser && !StringUtils.isEmpty(mallUser.getNickname())) {
                orderVo.setUserName(mallUser.getNickname());
            } else {
                orderVo.setUserName("");
                if(null != mallUser && !StringUtils.isEmpty(mallUser.getMobile()))
                    orderVo.setUserName(mallUser.getMobile());
            }
            if (null != mallUser && !StringUtils.isEmpty(mallUser.getAvatar())) {
                orderVo.setAvatar(mallUser.getAvatar());
            } else {
                orderVo.setAvatar("");
            }
            if (!StringUtils.isEmpty(order2.getAddress())) {
                orderVo.setAddress(order2.getAddress());
            } else {
                orderVo.setAddress("");
            }
            if (order2.getSettleTime() != null) {
                orderVo.setSettleTime(order2.getSettleTime());
            }
            if (!StringUtils.isEmpty(order2.getLevelId())) {
                orderVo.setLevelId(order2.getLevelId());
            } else {
                orderVo.setLevelId("");
            }
            if (order2.getSupplementTime() != null) {
                orderVo.setSupplementTime(order2.getSupplementTime());
            }
            if(!StringUtils.isEmpty(order2.getActivityType())){
                orderVo.setActivityType(order2.getActivityType());
            } else {
                orderVo.setActivityType("");
            }
            if(!StringUtils.isEmpty(order2.getChannelCode())){
                orderVo.setChannelCode(order2.getChannelCode());
            } else {
                orderVo.setChannelCode("");
            }
            if (null != order2.getOrderThreeStatus()) {
                if (order2.getOrderThreeStatus() == 200) {
                    orderVo.setOrderThreeStatus("成功");// 充值成功
                } else if (order2.getOrderThreeStatus() == 500 || order2.getOrderThreeStatus() == 501) {
                    orderVo.setOrderThreeStatus("失败");// 充值失败
                }
            } else {
                orderVo.setOrderThreeStatus("失败");// 充值失败
            }

            // 权益商品 或 自营商品
            if ("2".equals(type) || "7".equals(type) || "zy".equals(type)) {
                if (orderGoodsList != null && orderGoodsList.size() > 0) {
                    StringBuffer orderGoodsStr = new StringBuffer();
                    for (MallOrderGoods orderGoods : orderGoodsList) {
                        if (orderGoods.getOrderId().equals(order2.getId())) {

                            orderVo.setGoodsId(orderGoods.getGoodsId());
                            orderVo.setComment(orderGoods.getComment());
                            orderGoodsStr.append("【" + orderGoods.getGoodsName());
                            orderGoodsStr.append(";规格:" + ArrayUtils.toString(orderGoods.getSpecifications()));
                            orderGoodsStr.append(";数量:" + orderGoods.getNumber());
                            // MallGoods goods = goodsService.selectOneById(orderGoods.getGoodsSn());
                            // if(goods!=null&&goods.getSrcGoodId()!=null){
                            // orderGoodsStr.append(";").append(goods.getSrcGoodId());
                            // }
                            orderGoodsStr.append("】");
                            orderVo.setOrderGoodsStr(orderGoodsStr.toString());
                        }
                        orderVo.setComment(orderGoods.getComment());
                        orderVo.setCardPwd(orderGoods.getCardPwd());
                        orderVo.setCardNumber(orderGoods.getCardNumber());

                    }
                    ordervoList.add(orderVo);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void setpurchaseOrderSource(MallPurchaseOrder purchaseOrder, OrderVo orderVo) {
        if (com.graphai.mall.admin.util.StringUtils.isNotEmpty(purchaseOrder.getSource())){
            switch (purchaseOrder.getSource()) {
                case "alipay":
                    orderVo.setSource("支付宝支付");
                    break;
                case "xcx":
                    orderVo.setSource("小程序支付");
                    break;
                case "test":
                    orderVo.setSource("测试支付");
                    break;
                case "wx":
                    orderVo.setSource("微信支付");
                    break;
                case "huifu":
                    orderVo.setSource("汇付支付");
                    break;
            }}
    }

    public static void setOrderPurchaseVo(MallPurchaseOrder purchaseOrder, OrderVo orderVo, MallMerchant mallMerchant, String type, List<MallPurchaseOrderGoods> purchaseOrderGoodsList, List<OrderVo> orderVoList) {
        try {
            BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
            BeanUtils.copyProperties(orderVo, purchaseOrder);
            orderVo.setAddTime(purchaseOrder.getAddTime());
            orderVo.setPayTime(purchaseOrder.getPayTime());

            orderVo.setOrderSn(purchaseOrder.getOrderSn());
            setpurchaseOrderSource(purchaseOrder, orderVo);
            orderVo.setShipSn(purchaseOrder.getShipSn());
            /*if (null != mallUser && StringUtils.isNotEmpty(mallUser.getChannelId())) {
                orderVo.setChannelId(mallUser.getChannelId());
            } else {
                orderVo.setChannelId("");
            }*/

            if (null != mallMerchant && !StringUtils.isEmpty(mallMerchant.getPhone())) {
                orderVo.setMobile(mallMerchant.getPhone());
            } else {
                orderVo.setMobile("");
            }
            if (!StringUtils.isEmpty(purchaseOrder.getMobile())) {
                orderVo.setReceivingMobile(purchaseOrder.getMobile());
            } else {
                orderVo.setReceivingMobile("");
            }

            if (null != purchaseOrder.getActualPrice()) {
                orderVo.setActualPrice(purchaseOrder.getActualPrice());
            }
            if (null != purchaseOrder.getCouponPrice() || null != purchaseOrder.getIntegralPrice()) {
                BigDecimal specialPrice = purchaseOrder.getCouponPrice().add(purchaseOrder.getIntegralPrice());
                orderVo.setSpecialPrice(specialPrice);
            }
            if (null != purchaseOrder.getCouponPrice()) {
                orderVo.setCouponPrice(purchaseOrder.getCouponPrice());
            }
            if (null != mallMerchant && !StringUtils.isEmpty(mallMerchant.getName())) {
                orderVo.setUserName(mallMerchant.getName());
            } else {
                orderVo.setUserName("");
                if(null != mallMerchant && !StringUtils.isEmpty(mallMerchant.getPhone()))
                    orderVo.setUserName(mallMerchant.getPhone());
            }
            if (null != mallMerchant && !StringUtils.isEmpty(mallMerchant.getImg())) {
                orderVo.setAvatar(mallMerchant.getImg());
            } else {
                orderVo.setAvatar("");
            }
            if (!StringUtils.isEmpty(purchaseOrder.getAddress())) {
                orderVo.setAddress(purchaseOrder.getAddress());
            } else {
                orderVo.setAddress("");
            }
            if (purchaseOrder.getSettleTime() != null) {
                orderVo.setSettleTime(purchaseOrder.getSettleTime());
            }
            if (!StringUtils.isEmpty(purchaseOrder.getLevelId())) {
                orderVo.setLevelId(purchaseOrder.getLevelId());
            } else {
                orderVo.setLevelId("");
            }
            if (purchaseOrder.getSupplementTime() != null) {
                orderVo.setSupplementTime(purchaseOrder.getSupplementTime());
            }
            if(!StringUtils.isEmpty(purchaseOrder.getChannelCode())){
                orderVo.setChannelCode(purchaseOrder.getChannelCode());
            } else {
                orderVo.setChannelCode("");
            }
            if (null != purchaseOrder.getOrderThreeStatus()) {
                if (purchaseOrder.getOrderThreeStatus() == 200) {
                    orderVo.setOrderThreeStatus("成功");// 充值成功
                } else if (purchaseOrder.getOrderThreeStatus() == 500 || purchaseOrder.getOrderThreeStatus() == 501) {
                    orderVo.setOrderThreeStatus("失败");// 充值失败
                }
            } else {
                orderVo.setOrderThreeStatus("失败");// 充值失败
            }
            if ("zy".equals(type)) {
                if (purchaseOrderGoodsList != null && purchaseOrderGoodsList.size() > 0) {
                    StringBuffer purchaseOrderGoodsStr = new StringBuffer();
                    for (MallPurchaseOrderGoods mallPurchaseOrderGoods : purchaseOrderGoodsList) {
                        if (mallPurchaseOrderGoods.getOrderId().equals(purchaseOrder.getId())) {
                            orderVo.setComment(mallPurchaseOrderGoods.getComment());
                            purchaseOrderGoodsStr.append("【" + mallPurchaseOrderGoods.getGoodsName());
                            purchaseOrderGoodsStr.append(";规格:" + ArrayUtils.toString(mallPurchaseOrderGoods.getSpecifications()));
                            purchaseOrderGoodsStr.append(";数量:" + mallPurchaseOrderGoods.getNumber());
                            purchaseOrderGoodsStr.append("】");
                            orderVo.setOrderGoodsStr(purchaseOrderGoodsStr.toString());
                        }
                        orderVo.setComment(mallPurchaseOrderGoods.getComment());
                        orderVo.setCardPwd(mallPurchaseOrderGoods.getCardPwd());
                        orderVo.setCardNumber(mallPurchaseOrderGoods.getCardNumber());
                    }
                    orderVoList.add(orderVo);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



}
