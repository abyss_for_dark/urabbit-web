package com.graphai.mall.admin.service;

import cn.hutool.core.util.StrUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.id.GraphaiIdGenerator;
import com.graphai.mall.admin.dto.MallGoodsCouponCodeImport;
import com.graphai.mall.admin.util.ExcelUtil;
import com.graphai.mall.db.dao.CustomerMallGoodsCouponCodeMapper;
import com.graphai.mall.db.dao.MallGoodsCouponCodeMapper;
import com.graphai.mall.db.dao.MallProductCouponMapper;
import com.graphai.mall.db.dao.MallUserMapper;
import com.graphai.mall.db.domain.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.*;

/**
 * Created by wyd on 2020-06-28 09:06
 */
@Service
public class AdminGoodsCouponCodeService {

    private final Log logger = LogFactory.getLog(AdminGoodsCouponCodeService.class);

    @Resource
    private MallGoodsCouponCodeMapper mallGoodsCouponCodeMapper;
    @Resource
    private MallUserMapper mallUserMapper;
    @Resource
    private MallProductCouponMapper mallProductCouponMapper;
    @Resource
    private CustomerMallGoodsCouponCodeMapper customerMallGoodsCouponCodeMapper;

    public Object goodsCouponCodeList(String goodsId, Boolean saleStatus, Boolean useStatus, String couponCard, String name,
                                      Integer page, Integer size, String sort, String order) {
        MallGoodsCouponCodeExample example = new MallGoodsCouponCodeExample();
        MallGoodsCouponCodeExample.Criteria criteria = example.createCriteria();

        List<String> userIdList = new ArrayList<String>();
        if (!StringUtils.isEmpty(name)) {
            MallUserExample userExample = new MallUserExample();
            userExample.or().andUsernameLike("%" + name + "%").andDeletedEqualTo(false);
            List<MallUser> userList = mallUserMapper.selectByExample(userExample);
            for (MallUser mallUser : userList) {
                userIdList.add(mallUser.getId());
            }
        }

        if (!StringUtils.isEmpty(couponCard)) {
            criteria.andCouponCardEqualTo(couponCard);
        }
        if (null != saleStatus) {
            criteria.andSaleStatusEqualTo(saleStatus);
        }
        if (null != useStatus) {
            criteria.andUseStatusEqualTo(useStatus);
        }
        if (0 < userIdList.size()) {
            criteria.andUserIdIn(userIdList);
        }

        criteria.andGoodsIdEqualTo(goodsId).andDeletedEqualTo(false);

        if (!StringUtils.isEmpty(sort) && !StringUtils.isEmpty(order)) {
            example.setOrderByClause(sort + " " + order);
        }
        PageHelper.startPage(page, size);
        List<MallGoodsCouponCode> mallGoodsCouponCodes = mallGoodsCouponCodeMapper.selectByExample(example);
        long total = PageInfo.of(mallGoodsCouponCodes).getTotal();
        Map<String, Object> data = new HashMap<>();
        data.put("total", total);
        data.put("items", mallGoodsCouponCodes);
        return ResponseUtil.ok(data);
    }

    public Object create(MallGoodsCouponCode mallGoodsCouponCode) {
        if (null == mallGoodsCouponCode || null == mallGoodsCouponCode.getGoodsId() || null == mallGoodsCouponCode.getCouponCard()) {
            return ResponseUtil.badArgument();
        }
        MallGoodsCouponCodeExample mallGoodsCouponCodeExample = new MallGoodsCouponCodeExample();
        mallGoodsCouponCodeExample.or().andCouponCardEqualTo(mallGoodsCouponCode.getCouponCard()).andDeletedEqualTo(false);
        List<MallGoodsCouponCode> list = mallGoodsCouponCodeMapper.selectByExample(mallGoodsCouponCodeExample);
        if (0 < list.size()) {
            return ResponseUtil.fail(402, "有重复卡号值，请重新输入");
        }

        MallProductCouponExample example = new MallProductCouponExample();
        example.or().andGoodsIdEqualTo(mallGoodsCouponCode.getGoodsId()).andDeletedEqualTo(false);
        List<MallProductCoupon> mallProductCouponList = mallProductCouponMapper.selectByExample(example);
        if (0 == mallProductCouponList.size()) {
            return ResponseUtil.badArgument();
        }
        String couponId = mallProductCouponList.get(0).getCouponId();
        mallGoodsCouponCode.setId(GraphaiIdGenerator.nextId("MallGoodsCouponCode"));
        mallGoodsCouponCode.setCouponId(couponId);
        mallGoodsCouponCode.setSaleStatus(false);
        mallGoodsCouponCode.setUseStatus(false);
        mallGoodsCouponCode.setCreateTime(LocalDateTime.now());
        mallGoodsCouponCode.setUpdateTime(LocalDateTime.now());
        mallGoodsCouponCode.setDeleted(false);
        mallGoodsCouponCodeMapper.insert(mallGoodsCouponCode);
        return ResponseUtil.ok();
    }

    public Object detail(String id) {
        MallGoodsCouponCode mallGoodsCouponCode = mallGoodsCouponCodeMapper.selectByPrimaryKey(id);
        return ResponseUtil.ok(mallGoodsCouponCode);
    }

    public Object update(MallGoodsCouponCode mallGoodsCouponCode) {
        if (null == mallGoodsCouponCode || null == mallGoodsCouponCode.getId()) {
            return ResponseUtil.badArgument();
        }
        mallGoodsCouponCode.setCreateTime(LocalDateTime.now());
        if (mallGoodsCouponCodeMapper.updateByPrimaryKey(mallGoodsCouponCode) == 0) {
            throw new RuntimeException("更新数据失败");
        }
        return ResponseUtil.ok();
    }

    public Object importData(MultipartFile file, String goodsId) throws Exception {
        ExcelUtil<MallGoodsCouponCodeImport> util = new ExcelUtil<MallGoodsCouponCodeImport>(MallGoodsCouponCodeImport.class);
        List<MallGoodsCouponCodeImport> mallGoodsCouponCodeList = util.importExcel(file.getInputStream());
        //导入
        if (null == mallGoodsCouponCodeList || mallGoodsCouponCodeList.size() == 0) {
            return ResponseUtil.fail(402, "导入券码数据不能为空！");
        }

        MallProductCouponExample example = new MallProductCouponExample();
        example.or().andGoodsIdEqualTo(goodsId).andDeletedEqualTo(false);
        MallProductCoupon mallProductCoupon = mallProductCouponMapper.selectOneByExample(example);
        if (null == mallProductCoupon) {
            return ResponseUtil.badArgument();
        }

        int oldListCount = mallGoodsCouponCodeList.size();
        int successNum = 0;
        int failureNum = 0;

        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        StringBuilder removalMsg = new StringBuilder();

        //去重
        HashSet<MallGoodsCouponCodeImport> listToSet = new HashSet(mallGoodsCouponCodeList);
        HashSet<MallGoodsCouponCodeImport> hashSet = new HashSet<>();
        List<String> couponCardList = customerMallGoodsCouponCodeMapper.getCouponCardList();
        for (MallGoodsCouponCodeImport mallGoodsCouponCodeImport : listToSet) {
            if (!couponCardList.contains(mallGoodsCouponCodeImport.getCouponCard())) {
                hashSet.add(mallGoodsCouponCodeImport);
            }
        }
        mallGoodsCouponCodeList.clear();
        mallGoodsCouponCodeList.addAll(hashSet);
        if (mallGoodsCouponCodeList.size() < oldListCount) {
            Integer removalNum = oldListCount - mallGoodsCouponCodeList.size();
            removalMsg.insert(0, "共 " + removalNum + " 条数据去重！");
        }

        List<MallGoodsCouponCode> list = new ArrayList<MallGoodsCouponCode>();
        for (MallGoodsCouponCodeImport mallGoodsCouponCodeImport : mallGoodsCouponCodeList) {
            MallGoodsCouponCode mallGoodsCouponCode = new MallGoodsCouponCode();
            mallGoodsCouponCode.setGoodsId(goodsId);
            mallGoodsCouponCode.setCouponId(mallProductCoupon.getCouponId());
            mallGoodsCouponCode.setSaleStatus(false);
            mallGoodsCouponCode.setUseStatus(false);
            mallGoodsCouponCode.setCreateTime(LocalDateTime.now());
            mallGoodsCouponCode.setUpdateTime(LocalDateTime.now());
            mallGoodsCouponCode.setDeleted(false);

            mallGoodsCouponCode.setId(GraphaiIdGenerator.nextId("MallGoodsCouponCode"));
            mallGoodsCouponCode.setCouponCard(mallGoodsCouponCodeImport.getCouponCard());
            mallGoodsCouponCode.setCouponCode(mallGoodsCouponCodeImport.getCouponCode());
            mallGoodsCouponCode.setLink(mallGoodsCouponCodeImport.getLink());

            if (StrUtil.isBlank(mallGoodsCouponCode.getCouponCard()) || null == mallGoodsCouponCode.getCouponCard()) {
                failureNum++;
            } else {
                list.add(mallGoodsCouponCode);
                successNum++;
            }
        }
        if (0 < list.size()) {
            Integer num = customerMallGoodsCouponCodeMapper.insertForeach(list);
            if (0 == num) {
                return ResponseUtil.fail();
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "共 " + failureNum + " 条数据格式不正确，导入失败！");
            if (successNum > 0) {
                successMsg.insert(0, "恭喜您，共 " + successNum + " 条数据格式导入成功！");
            }
            return ResponseUtil.ok(successMsg.toString() + failureMsg.toString() + removalMsg.toString());
        }
        successMsg.insert(0, "恭喜您，共 " + successNum + " 条数据格式导入成功！");

        return ResponseUtil.ok(successMsg.toString() + removalMsg.toString());
    }
}
