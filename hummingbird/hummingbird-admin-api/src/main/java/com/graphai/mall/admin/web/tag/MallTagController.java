package com.graphai.mall.admin.web.tag;

import cn.afterturn.easypoi.excel.entity.ExportParams;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.lang.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.graphai.commons.util.servlet.ServletUtils;
import com.graphai.mall.admin.service.IMallTagMallUserService;
import com.graphai.mall.admin.util.AdminUserUtils;
import com.graphai.mall.db.service.tag.IMallTagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import org.springframework.web.bind.annotation.RestController;
import com.graphai.framework.generator.controller.IBaseController;

import com.graphai.mall.admin.domain.MallTag;

import java.util.List;


/**
 * <p>
 * 标签表 前端控制器
 * </p>
 *
 * @author biteam
 * @since 2021-05-19
 */
@RestController
@RequestMapping("/admin/tag")
public class MallTagController extends IBaseController<MallTag, String> {

    @Autowired
    private IMallTagService serviceImpl;

    @Autowired
    private IMallTagMallUserService mallTagMallUserService;

    @RequiresPermissions("admin:tag:single")
    @RequiresPermissionsDesc(menu = {"单独查询"}, button = "单独查询")
    @GetMapping("/single/{id}")
    public Object getEntityById(@PathVariable String id) throws Exception {
        MallTag entityVName = serviceImpl.getById(id);
        return ResponseUtil.ok(entityVName);
    }

    @RequiresPermissions("admin:tag:addOrUpdate")
    @RequiresPermissionsDesc(menu = {"新增"}, button = "新增")
    @PostMapping("/addOrUpdate")
    public Object addOrUpdate(MallTag mallTag) throws Exception {
        if (serviceImpl.saveOrUpdate(mallTag)) {
            return ResponseUtil.ok();
        } else {
            return ResponseUtil.fail();
        }
    }


    @RequiresPermissions("admin:tag:melist")
    @RequiresPermissionsDesc(menu = {"标签表查询"}, button = "查询")
    @GetMapping("/melist")
    public Object getMeList() throws Exception {
        IPage<MallTag> ipage=serviceImpl.queryList(getPlusPage());
        return ResponseUtil.ok(ipage);
    }



    @RequiresPermissions("admin:tag:save")
    @RequiresPermissionsDesc(menu = {"标签表新增"}, button = "新增")
    @PostMapping("/save")
    public Object save(@ModelAttribute MallTag mallTag) throws Exception {
        String userId = AdminUserUtils.getLoginUserId();
        serviceImpl.mallTagAdd(userId,mallTag);
        return ResponseUtil.ok();
    }

    @RequiresPermissions("admin:tag:modify")
    @RequiresPermissionsDesc(menu = {"标签表修改"}, button = "修改")
    @PutMapping("/modify")
    public Object modify(@ModelAttribute MallTag mallTag) {
        String userId = AdminUserUtils.getLoginUserId();
        serviceImpl.mallTagUpdate(userId,mallTag);
        return ResponseUtil.ok();
    }

    @RequiresPermissions("admin:tag:del")
    @RequiresPermissionsDesc(menu = {"标签表删除"}, button = "删除")
    @DeleteMapping("/del/{id}")
    public Object deleteById(@PathVariable String id) {
        serviceImpl.removeById(id);
        return ResponseUtil.ok();
    }

    @RequiresPermissions("admin:tag:disable")
    @RequiresPermissionsDesc(menu = {"标签表禁用"}, button = "禁用")
    @PutMapping("/disable/{id}")
    public Object disableById(@PathVariable String id) {
        List<MallTag> mallTags = serviceImpl.disableById(id);
        if (mallTags ==null || mallTags.size()<1){
            return ResponseUtil.fail();
        }else{
            for (MallTag mallTag : mallTags) {
                if (mallTag.getStatus()){
                    mallTagMallUserService.disableByTagId(mallTag.getId(),true);
                }else{
                    mallTagMallUserService.disableByTagId(mallTag.getId(),false);
                }
            }
            return ResponseUtil.ok();
        }
    }
    /**
     * 获取一级标签
     * @return
     */
    @GetMapping("/getTopTag")
    public Object getTopTag()   {
        List<MallTag> mallTag =serviceImpl.getTopTag();
        return ResponseUtil.ok(mallTag);
    }

}

