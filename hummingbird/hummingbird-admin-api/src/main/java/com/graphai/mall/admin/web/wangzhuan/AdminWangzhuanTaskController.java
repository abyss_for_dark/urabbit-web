package com.graphai.mall.admin.web.wangzhuan;


import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.json.JacksonUtil;
import com.graphai.commons.util.obj.ObjAndMapUtil;
import com.graphai.commons.util.obj.ObjectUtils;
import com.graphai.commons.util.validate.ValidateUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.id.GraphaiIdGenerator;
import com.graphai.mall.admin.dto.WangzhuanTaskDto;
import com.graphai.mall.admin.vo.WangzhuanTaskVo;
import com.graphai.mall.db.constant.wangzhuan.WangzhuanTaskEnum;
import com.graphai.mall.db.dao.WangzhuanTaskMapper;
import com.graphai.mall.db.dao.WangzhuanTaskTypeMapper;
import com.graphai.mall.db.domain.*;
import com.graphai.mall.db.service.common.MallRegionService;
import com.graphai.mall.db.service.promotion.ActivityService;
import com.graphai.mall.db.service.sysconfig.MallSystemConfigService;
import com.graphai.mall.db.service.wangzhuan.WangzhuanTaskService;
import com.graphai.mall.db.service.wangzhuan.WangzhuanTaskStepService;
import com.graphai.mall.db.service.wangzhuan.WangzhuanTaskTypeService;
import com.graphai.mall.db.util.account.task.AccountRWUtils;
import com.graphai.mall.db.util.phone.PushUtils;
import com.graphai.mall.db.vo.MallRegionVo;
import com.graphai.open.utils.lock.MutexLock;
import com.graphai.open.utils.lock.MutexLockUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * (网赚)任务Controller
 *
 * @author ruoyi
 * @date 2020-04-08
 */
@RestController
@RequestMapping("/admin/task")
@Validated
public class AdminWangzhuanTaskController {
    private final Log logger = LogFactory.getLog(AdminWangzhuanTaskController.class);

    @Autowired
    private WangzhuanTaskService wangzhuanTaskService;
    @Autowired
    private WangzhuanTaskStepService wangzhuanTaskStepService;
    @Resource
    private WangzhuanTaskTypeMapper wangzhuanTaskTypeMapper;

    @Autowired
    private MallSystemConfigService mallSystemConfigService;

    @Autowired
    private MallRegionService mallRegionService;
    @Autowired
    private WangzhuanTaskMapper wangzhuanTaskMapper;
    @Autowired
    private AccountRWUtils accountRWUtils;
    @Autowired
    private ActivityService activityService;
    @Resource
    private WangzhuanTaskTypeService wangzhuanTaskTypeService;

    /**
     * 查询网赚任务
     *
     * @param taskName      任务名称
     * @param taskType      投放类型
     * @param tstatus       任务状态
     * @param taskCategory  任务类目
     * @param page          页码
     * @param limit         每页数量
     * @param sort          排序
     * @return
     */
    @RequiresPermissions("admin:task:list")
    @RequiresPermissionsDesc(menu = {"任务管理", "任务管理"}, button = "查询")
    @GetMapping("/list")
    public Object list(@RequestParam String taskName, @RequestParam String taskType,
                       @RequestParam String tstatus, @RequestParam String taskCategory,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit,
                       @RequestParam(defaultValue = "id") String sort,LocalDateTime begin,
                       LocalDateTime end) {

        List<WangzhuanTaskVo> list = wangzhuanTaskService.selectWangZhuanTaskStep(sort, taskName, taskType, tstatus, taskCategory, (page-1)*limit, limit,begin,end);

        Long total = wangzhuanTaskService.selectWangZhuanTaskStepCount(sort, taskName, taskType, tstatus, taskCategory,begin,end);

        Map<String, Object> data = new HashMap<>();
        data.put("total", total);
        data.put("items", list);

        return ResponseUtil.ok(data);
    }

    /**
     *
     * @param id
     * @return
     */
    @RequiresPermissions("admin:task:detail")
    @RequiresPermissionsDesc(menu = {"任务管理", "任务管理"}, button = "查询步骤")
    @GetMapping("/detail")
    public Object detail(@RequestParam String id) {
        return  wangzhuanTaskStepService.getTaskStepDetails(id);
    }


    @RequiresPermissions("admin:task:approve")
    @RequiresPermissionsDesc(menu = {"任务管理", "任务管理"}, button = "审核通过")
    @GetMapping("/approve")
    public Object approve(@RequestParam String id) throws Exception {
        System.out.println("审核通过"+id);
        WangzhuanTask wangzhuanTask = new WangzhuanTask();
        wangzhuanTask.setId(id);
        wangzhuanTask.setTstatus(WangzhuanTaskEnum.TSTATUS_ST101.getCode());
        wangzhuanTask.setUpdateTime(LocalDateTime.now());
        int result = wangzhuanTaskMapper.updateByPrimaryKeySelective(wangzhuanTask);
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime time = LocalDateTime.now();
        String localTime = df.format(time);
        // 任务 - 通知 push
        String content = "恭喜您~ 您申请上架的网赚任务："+wangzhuanTask.getTaskName()+"审核通过了！--- " + localTime;
        PushUtils.pushMessage(wangzhuanTask.getUserId(), "系统通知", content, 2, 0);

        return ResponseUtil.ok();
    }
    @RequiresPermissions("admin:task:downTaskApprove")
    @RequiresPermissionsDesc(menu = {"任务管理", "任务管理"}, button = "任务下架")
    @GetMapping("/downTaskApprove")
    public Object downTaskApprove(@RequestParam String id) throws Exception {
        WangzhuanTask record = new WangzhuanTask();
        record.setId(id);
        record.setTstatus(WangzhuanTaskEnum.TSTATUS_ST102.getCode());
        WangzhuanTask wangzhuanTask = wangzhuanTaskMapper.selectByPrimaryKey(id);
        int result = wangzhuanTaskService.updateWangzhuanTask(record);
        //5. 往用户账户钱包打钱
        Map<String, String> map1 = new HashMap<>();
        map1.put("taskId",id);
        map1.put("userId", wangzhuanTask.getUserId());
        map1.put("investOrDrawback","drawback");
        accountRWUtils.reChargeWZV2(map1);
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime time = LocalDateTime.now();
        String localTime = df.format(time);
        // 任务 - 通知 push
        String content = "很抱歉~ 您上架的网赚任务："+wangzhuanTask.getTaskName()+"被下架了。原因是："+wangzhuanTask.getRemarks()+" --- " + localTime;
        PushUtils.pushMessage(wangzhuanTask.getUserId(), "系统通知", content, 2, 0);
        return ResponseUtil.ok();
    }

    @RequiresPermissions("admin:task:reject")
    @RequiresPermissionsDesc(menu = {"任务管理", "任务管理"}, button = "驳回")
    @GetMapping("/reject")
    public Object reject(@RequestParam String id, @RequestParam(required = false) String reasonsForRejection) throws Exception {
        System.out.println("驳回"+id);
        WangzhuanTask wangzhuanTask = new WangzhuanTask();
        wangzhuanTask.setId(id);
        wangzhuanTask.setReasonsForRejection(reasonsForRejection);
        wangzhuanTask.setTstatus(WangzhuanTaskEnum.TSTATUS_ST103.getCode());
        wangzhuanTask.setUpdateTime(LocalDateTime.now());
        wangzhuanTask.setRejectionTime(LocalDateTime.now());
        int result = wangzhuanTaskMapper.updateByPrimaryKeySelective(wangzhuanTask);
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime time = LocalDateTime.now();
        String localTime = df.format(time);
        // 任务 - 通知 push
        String content = "很抱歉~ 您申请上架的网赚任务："+wangzhuanTask.getTaskName()+"被驳回了。原因是："+wangzhuanTask.getRemarks()+" --- " + localTime;
        PushUtils.pushMessage(wangzhuanTask.getUserId(), "系统通知", content, 2, 0);

        return ResponseUtil.ok();
    }

    @RequiresPermissions("admin:task:createTask")
    @RequiresPermissionsDesc(menu = {"任务管理", "任务管理"}, button = "创建")
    @PostMapping("/createTask")
    public Object addSave(@RequestBody String data) {
        MutexLock mutexLock = MutexLockUtils.getMutexLock(data);
        synchronized (mutexLock) {
            // 参数验证
            Object error = validatereateTask(data);
            if (error != null) {
                return error;
            }
            JSONObject jsonObject = JSONUtil.parseObj(data);
            WangzhuanTask wangzhuanTask = new WangzhuanTask();
            wangzhuanTask.setAmount(jsonObject.getBigDecimal("amount"));//任务单价
            wangzhuanTask.setTaskType(jsonObject.getStr("taskType"));//投放类型
            wangzhuanTask.setRemarks(jsonObject.getStr("remark"));
            wangzhuanTask.setTstatus(WangzhuanTaskEnum.TSTATUS_ST100.getCode()); //任务状态
            wangzhuanTask.setTaskName(jsonObject.getStr("taskName"));//任务名称
            wangzhuanTask.setSubName(jsonObject.getStr("subName"));//副标题



            wangzhuanTask.setProductKey(jsonObject.getStr("productKey")); // 产品名称
            wangzhuanTask.setProductName(jsonObject.getStr("productName")); // 产品名称
            wangzhuanTask.setSex(jsonObject.getStr("sex")); // 性别
            wangzhuanTask.setBeginAge(jsonObject.getStr("beginAge")); // 限制开始年龄
            wangzhuanTask.setEndAge(jsonObject.getStr("endAge")); // 限制结束年龄.



            wangzhuanTask.setDeleted(false);//是否禁用
            wangzhuanTask.setTaskCategory(jsonObject.getStr("taskCategory"));//任务类型
            String imgUrl = jsonObject.getStr("imageUrl");
            if (com.graphai.commons.util.lang.StringUtils.isBlank(imgUrl)) {
                Byte aByte = new Byte("27");
                List<MallActivity> list = activityService.getUrlByNum(aByte);
                if (CollectionUtils.isNotEmpty(list)) {
                    for (int i = 0; list.size() > i; i++) {
                        if (list.get(i).getTitle().equals("DY") && jsonObject.getStr("taskCategory").equals(WangzhuanTaskEnum.TASK_CATEGORY_T03.getCode())) {
                            wangzhuanTask.setImageUrl(list.get(i).getUrl());
                            break;
                        } else if (list.get(i).getTitle().equals("SPH") && jsonObject.getStr("taskCategory").equals(WangzhuanTaskEnum.TASK_CATEGORY_T02.getCode())) {
                            wangzhuanTask.setImageUrl(list.get(i).getUrl());
                            break;
                        } else if (list.get(i).getTitle().equals("GY") && jsonObject.getStr("taskCategory").equals(WangzhuanTaskEnum.TASK_CATEGORY_T01.getCode())) {
                            wangzhuanTask.setImageUrl(list.get(i).getUrl());
                            break;
                        }
                    }
                }
            } else {
                wangzhuanTask.setImageUrl(imgUrl);
            }
            wangzhuanTask.setUnit(WangzhuanTaskEnum.UNIT_US102.getCode());//完成时间单位
            wangzhuanTask.setDuration(jsonObject.getStr("duration"));//任务完成时长
            wangzhuanTask.setAuditDuration(jsonObject.getStr("auditDuration")); // 审核时长，单位：小时
            wangzhuanTask.setStockNumber(Integer.parseInt(jsonObject.get("stockNumber").toString()));//任务数量--这个是会变化的。
            wangzhuanTask.setSumStockNumber(Integer.parseInt(jsonObject.get("stockNumber").toString()));//任务数量--这个是一直不变化的
            String id = GraphaiIdGenerator.nextId("WangzhuanTask");
            wangzhuanTask.setId(id);//id
            wangzhuanTask.setUserId("00"); // 发布者用户ID
            // 省市区
            cn.hutool.json.JSONArray regin = new cn.hutool.json.JSONArray(jsonObject.getStr("region"));
            if (CollectionUtils.isNotEmpty(regin)) {
                String provinceCode = "";
                String cityCode = "";
                String areaCode = "";
                String province = "";
                String city = "";
                String county = "";
                for (int i = 0; regin.size() > i; i++) {
                    JSONObject regionObject = JSONUtil.parseObj(regin.get(i));
                    if (ObjectUtils.isNotNull(regionObject.get("provinceCode"))) {
                        provinceCode = provinceCode.concat(com.graphai.commons.util.lang.StringUtils.isNotBlank(provinceCode) ? "," + regionObject.get("provinceCode").toString() : regionObject.get("provinceCode").toString());
                    }
                    if (ObjectUtils.isNotNull(regionObject.get("cityCode"))) {
                        cityCode = cityCode.concat(com.graphai.commons.util.lang.StringUtils.isNotBlank(cityCode) ? "," + regionObject.get("cityCode").toString() : regionObject.get("cityCode").toString());
                    }
                    if (ObjectUtils.isNotNull(regionObject.get("province"))) {
                        province = province.concat(com.graphai.commons.util.lang.StringUtils.isNotBlank(province) ? "," + regionObject.get("province").toString() : regionObject.get("province").toString());
                    }
                    if (ObjectUtils.isNotNull(regionObject.get("city"))) {
                        city = city.concat(com.graphai.commons.util.lang.StringUtils.isNotBlank(city) ? "," + regionObject.get("city").toString() : regionObject.get("city").toString());
                    }
                    if (ObjectUtils.isNotNull(regionObject.get("areaCode"))) {
                        areaCode = areaCode.concat(com.graphai.commons.util.lang.StringUtils.isNotBlank(areaCode) ? "," + regionObject.get("areaCode").toString() : regionObject.get("areaCode").toString());
                    }
                    if (ObjectUtils.isNotNull(regionObject.get("county"))) {
                        county = county.concat(com.graphai.commons.util.lang.StringUtils.isNotBlank(county) ? "," + regionObject.get("county").toString() : regionObject.get("county").toString());
                    }
                }
                wangzhuanTask.setProvinceCode(provinceCode);
                wangzhuanTask.setCityCode(cityCode);
                wangzhuanTask.setAreaCode(areaCode);
                wangzhuanTask.setProvince(province);
                wangzhuanTask.setCity(city);
                wangzhuanTask.setCounty(county);
            }

            wangzhuanTaskService.insertWangzhuanTask(wangzhuanTask);
            List<Map> stepList = ObjAndMapUtil.castList(jsonObject.get("stepList"), Map.class);
            int i = 1;
            if (CollectionUtils.isNotEmpty(stepList)) {
                for (Map wangzhuanTaskStepDto : stepList) {
                    Map<String, String> wangzhuanTaskStepMap = ObjAndMapUtil.mapToStringMap(wangzhuanTaskStepDto);
                    WangzhuanTaskStepWithBLOBs wangzhuanTaskStep = new WangzhuanTaskStepWithBLOBs();
                    wangzhuanTaskStep.setTaskId(wangzhuanTask.getId());
                    wangzhuanTaskStep.setStepOrder(i++);
                    wangzhuanTaskStep.setSetpType(wangzhuanTaskStepMap.get("type"));
                    wangzhuanTaskStep.setRemarks(wangzhuanTaskStepMap.get("remark"));
                    switch (Integer.parseInt(wangzhuanTaskStepMap.get("type"))) {
                        case 1:
                            List<Map> imgList1 = ObjAndMapUtil.castList(wangzhuanTaskStepDto.get("imageUrls"), Map.class);
                            if(CollectionUtils.isNotEmpty(imgList1)){
                                wangzhuanTaskStep.setUrls(imgList1.get(0).get("url").toString());
                            }
                            break;
                        case 3:
                            List<Map> imgList2 = ObjAndMapUtil.castList(wangzhuanTaskStepDto.get("imageUrls"), Map.class);
                            if(CollectionUtils.isNotEmpty(imgList2)) {
                                wangzhuanTaskStep.setUrls(imgList2.get(0).get("url").toString());
                            }
                            break;
                        case 5:
                            List<Map> imgList = ObjAndMapUtil.castList(wangzhuanTaskStepDto.get("imageUrls"), Map.class);
                            if(CollectionUtils.isNotEmpty(imgList)) {
                                wangzhuanTaskStep.setUrls(imgList.get(0).get("url").toString());
                            }
                            break;
                        case 2:
                            wangzhuanTaskStep.setUrls(wangzhuanTaskStepMap.get("url"));
                            break;
                        case 4:
                            wangzhuanTaskStep.setUrls(wangzhuanTaskStepMap.get("data"));
                            break;
                    }
                    wangzhuanTaskStepService.insertWangzhuanTaskStep(wangzhuanTaskStep);
                }
            }
            //创建步骤
        }
        return ResponseUtil.ok();
    }
    private Object validatereateTask(String data) {


        if (com.graphai.commons.util.lang.StringUtils.isBlank(data)) {
            return ResponseUtil.fail("参数为空");
        }
        JSONObject jsonObject = JSONUtil.parseObj(data);
        String taskCategory = jsonObject.getStr("taskCategory");
        if (com.graphai.commons.util.lang.StringUtils.isBlank(taskCategory)) {
            return ResponseUtil.fail("任务类型不能为空");
        }
        BigDecimal amount = jsonObject.getBigDecimal("amount");

        Map<String,String> map = mallSystemConfigService.getMallSystemCommonSingle("wangzhuan_task_limit_amount");
        BigDecimal bigDecimal = new BigDecimal(map.get("wangzhuan_task_limit_amount"));
        if (amount == null) {
            if(!taskCategory.equals(WangzhuanTaskEnum.TASK_CATEGORY_T01.getCode())) {
                return ResponseUtil.fail("任务单价不能为空");
            }
        }else if(!ValidateUtil.isNumber(amount.toString())){
            if(!taskCategory.equals(WangzhuanTaskEnum.TASK_CATEGORY_T01.getCode())) {
                return ResponseUtil.fail("任务单价只能为数字类型(允许到小数点后两位)");
            }
        }else if(amount.compareTo(bigDecimal) == -1){
            if(!taskCategory.equals(WangzhuanTaskEnum.TASK_CATEGORY_T01.getCode())) {
                return ResponseUtil.fail("任务单价，最低为" + bigDecimal + "元");
            }
        }


        String taskType = jsonObject.getStr("taskType");
        if (com.graphai.commons.util.lang.StringUtils.isBlank(taskType)) {
            return ResponseUtil.fail("投放类型不能为空");
        }

        String taskName = jsonObject.getStr("taskName");
        if (com.graphai.commons.util.lang.StringUtils.isBlank(taskName)) {
            return ResponseUtil.fail("任务名称不能为空");
        }


        String duration = jsonObject.getStr("duration");
        if (com.graphai.commons.util.lang.StringUtils.isBlank(duration)) {
            return ResponseUtil.fail("任务完成时长不能为空");
        }

        Map<String,String> map1 = mallSystemConfigService.getMallSystemCommonSingle("wangzhuan_task_limit_number");
        Integer stock=Integer.parseInt(map1.get("wangzhuan_task_limit_number"));
        String stockNumber = jsonObject.getStr("stockNumber");
        if (com.graphai.commons.util.lang.StringUtils.isBlank(stockNumber)) {
            return ResponseUtil.fail("任务数量不能为空");
        }else if(!ValidateUtil.isNumeric(stockNumber)){
            return ResponseUtil.fail("任务数量只能为正整数");
        }else if(Integer.parseInt(stockNumber) < stock){
            return ResponseUtil.fail("任务数量，最少为"+stock+"个");
        }

        String remark = jsonObject.getStr("remark");
        if (com.graphai.commons.util.lang.StringUtils.isBlank(remark)) {
            return ResponseUtil.fail("任务备注不能为空");
        }

        String auditDuration = jsonObject.getStr("auditDuration");
        if (com.graphai.commons.util.lang.StringUtils.isBlank(auditDuration)) {
            return ResponseUtil.fail("审核时长不能为空");
        }

        if (WangzhuanTaskEnum.TASK_TYPE_02.getCode().equals(taskType)) {
            String launchSettings = jsonObject.getStr("launchSettings");
            if (com.graphai.commons.util.lang.StringUtils.isBlank(launchSettings)) {
                return ResponseUtil.fail("投放设置不能为空");
            }

        }

        return null;
    }

    /**
     * 获取投放设置
     * @return
     */
    @GetMapping("/getLaunchSettings")
    public Object getLaunchSettings() {
        Map<String,String> launchSettings = mallSystemConfigService.getTaskLaunchSettingsMallSystem();
        return ResponseUtil.ok(launchSettings);
    }

    /**
     * 获取任务完成时长
     * @return
     */
    @GetMapping("/getTaskTime")
    public Object getTaskTime() {
        List<MallSystem> data = mallSystemConfigService.getTaskTimeMallSystem();
        return ResponseUtil.ok(data);
    }

    /**
     *  获取产品名称
     *  @return
     */
    @GetMapping("/getTaskProductName")
    public Object getTaskProductName() {
        List<MallSystem> data = mallSystemConfigService.getTaskProductName();
        return ResponseUtil.ok(data);
    }


    /**
     * 获取投放类型
     * @return
     */
    @GetMapping("/getTaskType")
    public Object getTaskType() {
        Map<String,String> taskType = mallSystemConfigService.getTaskTypeMallSystem();
        return ResponseUtil.ok(taskType);
    }

    /**
     * 获取任务类目
     * @return
     */
    @GetMapping("/getTaskCategory")
    public Object getTaskCategory() {
        List<WangzhuanTaskType> taskCategory = wangzhuanTaskTypeService.getTaskCategory();
        return ResponseUtil.ok(taskCategory);
    }

    /**
     * 获取任务审核时长
     * @return
     */
    @GetMapping("/getTaskAuditTime")
    public Object getTaskAuditTime() {
        List<MallSystem> taskAuditTime = mallSystemConfigService.getTaskAuditMallSystem();
        return ResponseUtil.ok(taskAuditTime);
    }


    /**
     * 新增保存(网赚)任务
     */
    @RequiresPermissions("admin:task:createTaskCopy")
    @RequiresPermissionsDesc(menu = {"任务管理", "任务管理(暂停使用)"}, button = "创建")
    @PostMapping("/createTaskCopy")
    public Object addSaveCopy(@RequestBody WangzhuanTaskDto wangzhuanTaskDto) {
        WangzhuanTask wangzhuanTask = new WangzhuanTask();
        wangzhuanTask.setAmount(new BigDecimal(wangzhuanTaskDto.getAmount()));
        wangzhuanTask.setSort(wangzhuanTaskDto.getSort());
        wangzhuanTask.setTaskType(wangzhuanTaskDto.getTaskType());
        wangzhuanTask.setImageUrl(wangzhuanTaskDto.getImageUrl());
        wangzhuanTask.setTstatus(wangzhuanTaskDto.getTstatus()); //状态( ST100 未上架、ST101 已上架、ST102 预上架)
        wangzhuanTask.setTaskName(wangzhuanTaskDto.getTaskName());
        wangzhuanTask.setSubName(wangzhuanTaskDto.getSubName());
        wangzhuanTask.setProductId(wangzhuanTaskDto.getProductId());
        wangzhuanTask.setDeleted(false);
        wangzhuanTask.setTaskCategory(wangzhuanTaskDto.getTaskCategory());
        wangzhuanTask.setUnit("UM101");//完成时间单位（分）
        wangzhuanTask.setDuration(60+"");
        wangzhuanTask.setStockNumber(1000);
        Object error = validateTask(wangzhuanTask);
        if (error != null) {
            return error;
        }
        wangzhuanTask.setId(GraphaiIdGenerator.nextId("WangzhuanTask"));
        wangzhuanTaskService.insertWangzhuanTask(wangzhuanTask);

        //添加 网赚任务步骤
        //应用类任务
        if ("02".equals(wangzhuanTaskDto.getTaskType())) {
            WangzhuanTaskStepWithBLOBs wangzhuanTaskStep = new WangzhuanTaskStepWithBLOBs();
            wangzhuanTaskStep.setIosDownloadUrl(wangzhuanTaskDto.getIosDownloadUrl());
            wangzhuanTaskStep.setAndroidDownloadUrl(wangzhuanTaskDto.getAndroidDownloadUrl());
            wangzhuanTaskStep.setTaskNotice2(wangzhuanTaskDto.getTaskNotice2());
            wangzhuanTaskStep.setCreateTime(LocalDateTime.now());
            wangzhuanTaskStep.setUpdateTime(LocalDateTime.now());
            wangzhuanTaskStep.setDeleted(false);
            wangzhuanTaskStep.setSetpType("2");
            wangzhuanTaskStep.setSuccessLv(wangzhuanTaskDto.getSuccessLv());
            wangzhuanTaskStep.setStepOrder(1);
            if (!StringUtils.isEmpty(wangzhuanTaskDto.getStep1()) && !StringUtils.isEmpty(wangzhuanTaskDto.getStep2())) {
                String step1 = '"' + wangzhuanTaskDto.getStep1() + '"' + ",";
                String step2 = '"' + wangzhuanTaskDto.getStep2() + '"';
                String step = "[" + step1 + step2 + "]";
                wangzhuanTaskStep.setTaskStep(step);
            }
            if (!StringUtils.isEmpty(wangzhuanTaskDto.getStep1()) && !StringUtils.isEmpty(wangzhuanTaskDto.getStep2())
                    && !StringUtils.isEmpty(wangzhuanTaskDto.getStep3())) {
                String step1 = '"' + wangzhuanTaskDto.getStep1() + '"' + ",";
                String step2 = '"' + wangzhuanTaskDto.getStep2() + '"' + ",";
                String step3 = '"' + wangzhuanTaskDto.getStep3() + '"';
                String step = "[" + step1 + step2 + step3 + "]";
                wangzhuanTaskStep.setTaskStep(step);
            }

            List<String> list = new ArrayList<>();

            for (int i = 0; i < wangzhuanTaskDto.getPicUrls().length; i++) {
                Map map = (Map) wangzhuanTaskDto.getPicUrls()[i];
                String url = (String) map.get("url");
                list.add(url);
            }
            if (list.size() == 1) {
                String url1 = '"' + list.get(0) + '"';
                String url3 = "[" + url1 + "]";
                wangzhuanTaskStep.setPicUrls(url3);
            } else if (list.size() == 2) {
                String url1 = '"' + list.get(0) + '"' + ",";
                String url2 = '"' + list.get(1) + '"';
                String url3 = "[" + url1 + url2 + "]";
                wangzhuanTaskStep.setPicUrls(url3);
            } else if (list.size() == 3) {
                String url1 = '"' + list.get(0) + '"' + ",";
                String url2 = '"' + list.get(1) + '"' + ",";
                String url3 = '"' + list.get(2) + '"';
                String url4 = "[" + url1 + url2 + url3 + "]";
                wangzhuanTaskStep.setPicUrls(url4);
            }
            List<String> listu = new ArrayList<>();

            for (int i = 0; i < wangzhuanTaskDto.getUrls().length; i++) {
                Map map = (Map) wangzhuanTaskDto.getUrls()[i];
                String url = (String) map.get("url");
                listu.add(url);
            }
            if (listu.size() == 1) {
                String url1 = '"' + list.get(0) + '"';
                String url3 = "[" + url1 + "]";
                wangzhuanTaskStep.setUrls(url3);
            } else if (listu.size() == 2) {
                String url1 = '"' + list.get(0) + '"' + ",";
                String url2 = '"' + list.get(1) + '"';
                String url3 = "[" + url1 + url2 + "]";
                wangzhuanTaskStep.setUrls(url3);
            } else if (listu.size() == 3) {
                String url1 = '"' + list.get(0) + '"' + ",";
                String url2 = '"' + list.get(1) + '"' + ",";
                String url3 = '"' + list.get(2) + '"';
                String url4 = "[" + url1 + url2 + url3 + "]";
                wangzhuanTaskStep.setUrls(url4);
            }
            String[] strs = wangzhuanTaskDto.getTaskNotice1().split("，");
            if (strs.length == 1) {
                String url1 = '"' + strs[0] + '"';
                String url3 = "[" + url1 + "]";
                wangzhuanTaskStep.setTaskNotice1(url3);
            } else if (strs.length == 2) {
                String url1 = '"' + strs[0] + '"' + ",";
                String url2 = '"' + strs[1] + '"';
                String url3 = "[" + url1 + url2 + "]";
                wangzhuanTaskStep.setTaskNotice1(url3);
            } else if (strs.length == 3) {
                String url1 = '"' + strs[0] + '"' + ",";
                String url2 = '"' + strs[1] + '"' + ",";
                String url3 = '"' + strs[2] + '"';
                String url4 = "[" + url1 + url2 + url3 + "]";
                wangzhuanTaskStep.setTaskNotice1(url4);
            }


            Object stepError = validateTaskStep(wangzhuanTaskStep);
            if (stepError != null) {
                return error;
            }
            wangzhuanTaskStep.setTaskId(wangzhuanTask.getId());
            wangzhuanTaskStep.setDeleted(false);
            wangzhuanTaskStepService.insertWangzhuanTaskStep(wangzhuanTaskStep);

        }
        //信用卡类任务
        else if ("01".equals(wangzhuanTaskDto.getTaskType())) {
            WangzhuanTaskStepWithBLOBs wangzhuanTaskStep = new WangzhuanTaskStepWithBLOBs();
            wangzhuanTaskStep.setIosDownloadUrl(wangzhuanTaskDto.getIosDownloadUrl());
            wangzhuanTaskStep.setAndroidDownloadUrl(wangzhuanTaskDto.getAndroidDownloadUrl());
//            wangzhuanTaskStep.setTaskNotice2(wangzhuanTaskDto.getTaskNotice2());
            wangzhuanTaskStep.setCreateTime(LocalDateTime.now());
            wangzhuanTaskStep.setUpdateTime(LocalDateTime.now());
            wangzhuanTaskStep.setDeleted(false);
            wangzhuanTaskStep.setSetpType("2");
            wangzhuanTaskStep.setSuccessLv(wangzhuanTaskDto.getSuccessLv());
            wangzhuanTaskStep.setProductId(wangzhuanTaskDto.getProductId());
            wangzhuanTaskStep.setStepOrder(1);
            if (!StringUtils.isEmpty(wangzhuanTaskDto.getStep1()) && !StringUtils.isEmpty(wangzhuanTaskDto.getStep2())) {
                String step = "[" + '"' + wangzhuanTaskDto.getStep1() + '"' + ',' + '"' + wangzhuanTaskDto.getStep2() + '"' + ']';
                wangzhuanTaskStep.setTaskStep(step);
            }
            if (!StringUtils.isEmpty(wangzhuanTaskDto.getStep1()) && !StringUtils.isEmpty(wangzhuanTaskDto.getStep2())
                    && !StringUtils.isEmpty(wangzhuanTaskDto.getStep3())) {
                String step = "[" + '"' + wangzhuanTaskDto.getStep1() + '"' + ',' + '"' + wangzhuanTaskDto.getStep2() + '"' + ',' + '"' + wangzhuanTaskDto.getStep3() + '"' + ']';
                wangzhuanTaskStep.setTaskStep(step);
            }

            List<String> list = new ArrayList<>();

            for (int i = 0; i < wangzhuanTaskDto.getPicUrls().length; i++) {
                Map map = (Map) wangzhuanTaskDto.getPicUrls()[i];
                String url = (String) map.get("url");
                list.add(url);
            }
            if (list.size() == 1) {
                String url3 = "[" + '"' + list.get(0) + '"' + ']';
                wangzhuanTaskStep.setPicUrls(url3);
            } else if (list.size() == 2) {
                String url3 = "[" + '"' + list.get(0) + '"' + ',' + '"' + list.get(1) + '"' + ']';
                wangzhuanTaskStep.setPicUrls(url3);
            } else if (list.size() == 3) {
                String url4 = "[" + '"' + list.get(0) + '"' + ',' + '"' + list.get(1) + '"' + ',' + '"' + list.get(2) + '"' + ']';
                wangzhuanTaskStep.setPicUrls(url4);
            }
            List<String> listu = new ArrayList<>();

            for (int i = 0; i < wangzhuanTaskDto.getUrls().length; i++) {
                Map map = (Map) wangzhuanTaskDto.getUrls()[i];
                String url = (String) map.get("url");
                listu.add(url);
            }
            if (listu.size() == 1) {
                String url3 = "[" + '"' + listu.get(0) + '"' + ']';
                wangzhuanTaskStep.setUrls(url3);
            } else if (listu.size() == 2) {
                String url3 = "[" + '"' + listu.get(0) + '"' + ',' + '"' + listu.get(1) + '"' + ']';
                wangzhuanTaskStep.setUrls(url3);
            } else if (listu.size() == 3) {
                String url4 = "[" + '"' + listu.get(0) + '"' + ',' + '"' + listu.get(1) + '"' + ',' + '"' + listu.get(2) + '"' + ']';
                wangzhuanTaskStep.setUrls(url4);
            }
            String[] strs = wangzhuanTaskDto.getTaskNotice1().split("，");
            if (strs.length == 1) {
                String url3 = "[" + '"' + strs[0] + '"' + ']';
                wangzhuanTaskStep.setTaskNotice1(url3);
            } else if (strs.length == 2) {
                String url3 = "[" + '"' + strs[0] + '"' + ',' + '"' + strs[1] + '"' + ']';
                wangzhuanTaskStep.setTaskNotice1(url3);
            } else if (strs.length == 3) {
                String url4 = "[" + '"' + strs[0] + '"' + ',' + '"' + strs[1] + '"' + ',' + '"' + strs[2] + '"' + ']';
                wangzhuanTaskStep.setTaskNotice1(url4);
            }
            String[] strs1 = wangzhuanTaskDto.getTaskNotice2().split("，");
            if (strs1.length == 1) {
                String url3 = "[" + '"' + strs1[0] + '"' + ']';
                wangzhuanTaskStep.setTaskNotice2(url3);
            } else if (strs1.length == 2) {
                String url3 = "[" + '"' + strs1[0] + '"' + ',' + '"' + strs1[1] + '"' + ']';
                wangzhuanTaskStep.setTaskNotice2(url3);
            } else if (strs1.length == 3) {
                String url4 = "[" + '"' + strs1[0] + '"' + ',' + '"' + strs1[1] + '"' + ',' + '"' + strs1[2] + '"' + ']';
                wangzhuanTaskStep.setTaskNotice2(url4);
            }

            Object stepError = validateTaskStep(wangzhuanTaskStep);
            if (stepError != null) {
                return error;
            }
            wangzhuanTaskStep.setTaskId(wangzhuanTask.getId());
            wangzhuanTaskStep.setDeleted(false);
            wangzhuanTaskStepService.insertWangzhuanTaskStep(wangzhuanTaskStep);
        }


        return ResponseUtil.ok();
    }

    /**
     * 修改(网赚)任务
     */
    @GetMapping("/edit/{id}")
    public Object edit(@PathVariable("id") String id) {
        //任务
        WangzhuanTask wangzhuanTask = wangzhuanTaskService.selectWangzhuanTaskById(id);
        //任务步骤
        WangzhuanTaskStepWithBLOBs wangzhuanTaskStep = new WangzhuanTaskStepWithBLOBs();
        wangzhuanTaskStep.setTaskId(id);
        wangzhuanTaskStep.setDeleted(false);
        List<WangzhuanTaskStepWithBLOBs> wangzhuanTaskStepList = wangzhuanTaskStepService.selectWangzhuanTaskStepList(wangzhuanTaskStep);

        Map<String, Object> data = new HashMap<>();
        WangzhuanTaskDto wangzhuanTaskDto = new WangzhuanTaskDto();
        wangzhuanTaskDto.setWangzhuanTask(wangzhuanTask);
        wangzhuanTaskDto.setWangzhuanTaskStepList(wangzhuanTaskStepList);

        data.put("items", wangzhuanTaskDto);
        return ResponseUtil.ok(data);
    }

    /**
     * 禁用/启用(网赚)任务
     */
    @RequiresPermissions("admin:task:disableTask")
    @RequiresPermissionsDesc(menu = {"任务管理", "任务管理"}, button = "禁用")
    @PostMapping("/disableTask")
    public Object disableITask(@RequestBody WangzhuanTask task) {


        if ("ST101".equals(task.getTstatus())) {
            task.setTstatus("ST100");
        } else if ("ST100".equals(task.getTstatus())) {
            task.setTstatus("ST101");
        }
        if (!StringUtils.isEmpty(task.getTaskCategory())) {
            if ("高佣任务".equals(task.getTaskCategory())) {
                task.setTaskCategory("T03");
            } else if ("每日任务".equals(task.getTaskCategory())) {
                task.setTaskCategory("T02");
            } else if ("新人任务".equals(task.getTaskCategory())) {
                task.setTaskCategory("T01");
            }
        }
        wangzhuanTaskService.updateWangzhuanTask(task);
        return ResponseUtil.ok();
    }

    /**
     * 修改保存(网赚)任务
     */
    @RequiresPermissions("admin:task:updateTask")
    @RequiresPermissionsDesc(menu = {"任务管理", "任务管理"}, button = "编辑")
    @PostMapping("/updateTask")
    public Object editSave(@RequestBody String wangzhuan) {
        String id = JacksonUtil.parseString(wangzhuan, "id");
        String taskName = JacksonUtil.parseString(wangzhuan, "taskName");
        String subName = JacksonUtil.parseString(wangzhuan, "subName");
        String amount = JacksonUtil.parseString(wangzhuan, "amount");
        String imageUrl = JacksonUtil.parseString(wangzhuan, "imageUrl");
        String tstatus = JacksonUtil.parseString(wangzhuan, "tstatus");
        String taskType = JacksonUtil.parseString(wangzhuan, "taskType");
        String sort = JacksonUtil.parseString(wangzhuan, "sort");
        String androidDownloadUrl = JacksonUtil.parseString(wangzhuan, "androidDownloadUrl");
        String iosDownloadUrl = JacksonUtil.parseString(wangzhuan, "iosDownloadUrl");
        String taskNotice1 = JacksonUtil.parseString(wangzhuan, "taskNotice1");
        String taskNotice2 = JacksonUtil.parseString(wangzhuan, "taskNotice2");
        String taskStep = JacksonUtil.parseString(wangzhuan, "taskStep");
        String urls = JacksonUtil.parseString(wangzhuan, "urls");
        String picUrls = JacksonUtil.parseString(wangzhuan, "picUrls");
        JSONArray objar = new JSONArray(urls);
        List<Object> list = objar.toList();
        JSONArray objr = new JSONArray(picUrls);
        List<Object> list1 = objr.toList();
        List<String> listUrl = new ArrayList();
        List<String> listPic = new ArrayList();
        for (Object obj : list) {
            Map<String, Object> map = (HashMap<String, Object>) obj;
            Set<String> sets = map.keySet();
            for (String key : sets) {
                if ("url".equals(key)) {
                    listUrl.add((String) map.get(key));
                }
            }
        }
        for (Object obj : list1) {
            Map<String, Object> map = (HashMap<String, Object>) obj;
            Set<String> sets = map.keySet();
            for (String key : sets) {
                if ("url".equals(key)) {
                    listPic.add((String) map.get(key));
                }
            }
        }
        String url = "";
        if (listUrl.size() == 1) {
            url = "[" + listUrl.get(0) + "]";
        } else if (listUrl.size() == 2) {
            url = "[" + listUrl.get(0) + "," + listUrl.get(1) + "]";
        } else if (listUrl.size() == 3) {
            url = "[" + listUrl.get(0) + "," + listUrl.get(1) + "," + listUrl.get(2) + "]";
        } else {
            url = "[]";
        }
        String urlPic = "";
        if (listPic.size() == 1) {
            urlPic = "[" + listUrl.get(0) + "]";
        } else if (listPic.size() == 2) {
            urlPic = "[" + listUrl.get(0) + "," + listPic.get(1) + "]";
        } else if (listPic.size() == 3) {
            urlPic = "[" + listUrl.get(0) + "," + listPic.get(1) + "," + listPic.get(2) + "]";
        } else {
            urlPic = "[]";
        }


        WangzhuanTask wangzhuanTask = new WangzhuanTask();
        wangzhuanTask.setId(id);
        wangzhuanTask.setTaskName(taskName);
        wangzhuanTask.setSubName(subName);
        wangzhuanTask.setTstatus(tstatus);
        wangzhuanTask.setImageUrl(imageUrl);
        wangzhuanTask.setAmount(new BigDecimal(amount));
        wangzhuanTask.setTaskType(taskType);
        wangzhuanTask.setSort(sort);
        Object error = validateTask(wangzhuanTask);
        if (error != null) {
            return error;
        }
        if (wangzhuanTaskService.updateWangzhuanTask(wangzhuanTask) == 0) {
            return ResponseUtil.updatedDataFailed();
        }
        //修改任务步骤
        if (!StringUtils.isEmpty(taskStep)) {
            WangzhuanTaskStepWithBLOBs taskstep = new WangzhuanTaskStepWithBLOBs();
            taskstep.setTaskId(wangzhuanTask.getId());
            taskstep.setTaskNotice1(taskNotice1);
            taskstep.setTaskNotice2(taskNotice2);
            taskstep.setAndroidDownloadUrl(androidDownloadUrl);
            taskstep.setIosDownloadUrl(iosDownloadUrl);
            taskstep.setUrls(url);
            taskstep.setPicUrls(urlPic);
            wangzhuanTaskStepService.updateWangzhuanTaskStep(taskstep);

        }

        return ResponseUtil.ok();
    }

    /**
     * 删除(网赚)任务
     */
    @RequiresPermissions("admin:task:deleteTask")
    @RequiresPermissionsDesc(menu = {"任务管理", "任务管理"}, button = "删除")
    @PostMapping("/deleteTask")
    public Object remove(@RequestBody WangzhuanTask task) {

        task.setDeleted(true);
        if (!StringUtils.isEmpty(task.getTaskCategory())) {
            if ("高佣任务".equals(task.getTaskCategory())) {
                task.setTaskCategory("T03");
            } else if ("每日任务".equals(task.getTaskCategory())) {
                task.setTaskCategory("T02");
            } else if ("新人任务".equals(task.getTaskCategory())) {
                task.setTaskCategory("T01");
            }
        }
        wangzhuanTaskService.updateWangzhuanTask(task);


        return ResponseUtil.ok();
    }

    /**
     * 上、下架(网赚)任务
     */
    @PostMapping("/isOnShelf")
    public Object isOnShelf(@RequestParam("id") String id, @RequestParam("status") String status) {
        WangzhuanTask wangzhuanTask = new WangzhuanTask();
        wangzhuanTask.setId(id);
        wangzhuanTask.setTstatus(status);
        wangzhuanTaskService.updateWangzhuanTask(wangzhuanTask);
        return ResponseUtil.ok();
    }

    /**
     * 任务类目
     */
    @RequiresPermissions("admin:task:taskCategory")
    @RequiresPermissionsDesc(menu = {"任务管理", "任务类目"}, button = "查询")
    @GetMapping("/taskCategory")
    public Object taskCategory(String categoryName, String type) {

        WangzhuanTaskTypeExample example = new WangzhuanTaskTypeExample();
        WangzhuanTaskTypeExample.Criteria criteria = example.createCriteria();
        criteria.andIsDeleteEqualTo("0");
        if (!StringUtils.isEmpty(categoryName)) {
            criteria.andTaskCategoryLike("%" + categoryName + "%");
        }
        if (!StringUtils.isEmpty(type)) {
            if (type.equals("3")) {
            } else {
                criteria.andStatusEqualTo(Integer.parseInt(type));
            }
        }
        List<WangzhuanTaskType> list = wangzhuanTaskTypeMapper.selectByExample(example);
        Map<String, Object> map = new HashMap<>();
        long total = PageInfo.of(list).getTotal();
        map.put("total", total);
        map.put("items", list);
        return ResponseUtil.ok(map);
    }

    @PostMapping("/saveTaskCategory")
    public Object saveTaskCategory(WangzhuanTaskType wangzhuanTaskType){
        wangzhuanTaskType.setId(GraphaiIdGenerator.nextId("WangzhuanTaskCategory"));
        wangzhuanTaskType.setStatus(1);
        wangzhuanTaskType.setType("wangzhuanRenWuLeiXing");
        wangzhuanTaskType.setCreateTime(LocalDateTime.now());
        wangzhuanTaskType.setIsDelete("0");
        wangzhuanTaskTypeMapper.insert(wangzhuanTaskType);
        return ResponseUtil.ok();
    }
    @PostMapping("/updateTaskCategory")
    public Object updateTaskCategory(WangzhuanTaskType wangzhuanTaskType){
        wangzhuanTaskType.setUpdateTime(LocalDateTime.now());
        wangzhuanTaskTypeMapper.updateByPrimaryKeySelective(wangzhuanTaskType);
        return ResponseUtil.ok();
    }


    /**
     * 禁用/启用(网赚)任务
     */
    @RequiresPermissions("admin:task:disableCategory")
    @RequiresPermissionsDesc(menu = {"任务管理", "任务类目"}, button = "禁用")
    @PostMapping("/disableCategory")
    public Object disableCategory(@RequestBody WangzhuanTaskType task) {


        if (task.getStatus() == 1) {
            task.setStatus(2);
        } else if (task.getStatus() == 2) {
            task.setStatus(1);
        }
        WangzhuanTaskTypeExample example = new WangzhuanTaskTypeExample();


        wangzhuanTaskTypeMapper.updateByPrimaryKeySelective(task);
        return ResponseUtil.ok();
    }


    /**
     * 通过任务id 查询任务步骤
     */
    @GetMapping("/getTaskStepById/{taskId}")
    public Object getTaskStepById(@PathVariable("taskId") String taskId) {
        WangzhuanTaskStep wangzhuanTaskStep = new WangzhuanTaskStep();
        wangzhuanTaskStep.setTaskId(taskId);
        wangzhuanTaskStep.setDeleted(false);
        List<WangzhuanTaskStepWithBLOBs> wangzhuanTaskStepList = wangzhuanTaskStepService.selectWangzhuanTaskStepList(wangzhuanTaskStep);
        Map<String, Object> data = new HashMap<>();
        data.put("items", wangzhuanTaskStepList);

        return ResponseUtil.ok(data);
    }


    /**
     * 校验数据(网赚任务)
     */
    private Object validateTask(WangzhuanTask wangzhuanTask) {
        if (StringUtils.isEmpty(wangzhuanTask.getTaskName())) {
            return ResponseUtil.badArgument();
        }
//        if (StringUtils.isEmpty(wangzhuanTask.getStockNumber())) {
//            return ResponseUtil.badArgument();
//        }
//        if (StringUtils.isEmpty(wangzhuanTask.getSubName())) {
//            return ResponseUtil.badArgument();
//        }
//
//        if (StringUtils.isEmpty(wangzhuanTask.getAndroidDownloadUrl())) {
//            return ResponseUtil.badArgument();
//        }
//        if (wangzhuanTask.getAmount() == null) {
//            return ResponseUtil.badArgument();
//        }
        return null;
    }

    /**
     * 校验数据(网赚任务步骤)
     */
    private Object validateTaskStep(WangzhuanTaskStep wangzhuanTaskStep) {
//        if (StringUtils.isEmpty(wangzhuanTaskStep.getStepTitle())) {
//            return ResponseUtil.badArgument();
//        }
//        if (StringUtils.isEmpty(wangzhuanTaskStep.getStepSubTitle())) {
//            return ResponseUtil.badArgument();
//        }
//        if (StringUtils.isEmpty(wangzhuanTaskStep.getRemarks())) {
//            return ResponseUtil.badArgument();
//        }
//
        if (StringUtils.isEmpty(wangzhuanTaskStep.getSetpType())) {
            return ResponseUtil.badArgument();
        }

        return null;
    }

    /**
     * 获取省市行政区域
     */
    @GetMapping("/getAllRegion")
    public Object getAllRegion() {

        List<MallRegionVo> fiList = new ArrayList<MallRegionVo>();

        try {
            List<MallRegionVo> regions = mallRegionService.selectRegions();
            if (CollectionUtils.isNotEmpty(regions)) {

                for (MallRegionVo mallRegionVo : regions) {
                    if (Objects.equals("0", mallRegionVo.getPid())) {
                        fiList.add(mallRegionVo);
                    }
                }

            }

            if (CollectionUtils.isNotEmpty(fiList)) {
                for (MallRegionVo mallRegionVo : fiList) {
                    List<MallRegionVo> seList = new ArrayList<MallRegionVo>();
                    for (int i = 0; i < regions.size(); i++) {
                        if (Objects.equals(mallRegionVo.getId(), regions.get(i).getPid())) {
                            seList.add(regions.get(i));
                        }
                    }
                    mallRegionVo.setChilds(seList);
                }

            }


            return ResponseUtil.ok(fiList);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw e;
        }

    }

}
