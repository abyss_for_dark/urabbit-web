package com.graphai.mall.admin.web.user;

import com.github.pagehelper.PageHelper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.graphai.mall.admin.domain.MallSystemRule;
import com.graphai.mall.admin.service.IGameRedHourRuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.framework.generator.controller.IBaseController;

import com.graphai.mall.admin.domain.MallUserProfitNew;
import com.graphai.mall.admin.service.IMallUserProfitNewService;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;


/**
 * <p>
 * 新模式波比设置 前端控制器
 * </p>
 *
 * @author biteam
 * @since 2020-12-23
 */
@RestController
@RequestMapping("/admin/new")
public class MallUserProfitNewController extends IBaseController<MallUserProfitNew,  String >  {

	@Autowired
    private IMallUserProfitNewService serviceImpl;

	@Autowired
	private IGameRedHourRuleService gameRedHourRuleService;

    @RequiresPermissions("admin:new:single")
    @RequiresPermissionsDesc(menu = {"单独查询"}, button = "单独查询")
	@GetMapping("/single/{id}")
    public Object getEntityById(@PathVariable  String  id) throws Exception{
        MallUserProfitNew entityVName = serviceImpl.getById(id);
        return ResponseUtil.ok(entityVName);
    }

    @RequiresPermissions("admin:new:addOrUpdate")
    @RequiresPermissionsDesc(menu = {"新增"}, button = "新增")
    @PostMapping("/addOrUpdate")
    public Object addOrUpdate(MallUserProfitNew mallUserProfitNew) throws Exception{
        if(serviceImpl.saveOrUpdate(mallUserProfitNew)){
            return ResponseUtil.ok();
        }else{
            return ResponseUtil.fail();
        }
    }


	@RequiresPermissions("admin:new:melist")
	@RequiresPermissionsDesc(menu = {"新模式波比设置查询"}, button = "查询")
    @GetMapping("/melist")
    public Object getMeList()  {
//        IPage<MallUserProfitNew> ipage = serviceImpl.page(getPlusPage(),listQueryWrapper());
		try {
			List<MallUserProfitNew> list = serviceImpl.list(new QueryWrapper<MallUserProfitNew>().eq("deleted", 0));
			for (MallUserProfitNew mallUserProfitNew : list) {
				BigDecimal pub = new BigDecimal("100");
				mallUserProfitNew.setVipProfit(mallUserProfitNew.getVipProfit().multiply(pub));
				mallUserProfitNew.setOrdinaryUsersProfit(mallUserProfitNew.getOrdinaryUsersProfit().multiply(pub));
				mallUserProfitNew.setUserProfit(mallUserProfitNew.getUserProfit().multiply(pub));
				mallUserProfitNew.setVipUsersProfit(mallUserProfitNew.getVipUsersProfit().multiply(pub));
				mallUserProfitNew.setOneMerProfit(mallUserProfitNew.getOneMerProfit().multiply(pub));
				mallUserProfitNew.setTwoMerProfit(mallUserProfitNew.getTwoMerProfit().multiply(pub));
				mallUserProfitNew.setThreeMerProfit(mallUserProfitNew.getThreeMerProfit().multiply(pub));
				mallUserProfitNew.setFourMerProfit(mallUserProfitNew.getFourMerProfit().multiply(pub));
				mallUserProfitNew.setFiveMerProfit(mallUserProfitNew.getFiveMerProfit().multiply(pub));
				mallUserProfitNew.setSixMerProfit(mallUserProfitNew.getSixMerProfit().multiply(pub));
				mallUserProfitNew.setSevenMerProfit(mallUserProfitNew.getSevenMerProfit().multiply(pub));
				mallUserProfitNew.setEightMerProfit(mallUserProfitNew.getEightMerProfit().multiply(pub));
				mallUserProfitNew.setNineMerProfit(mallUserProfitNew.getNineMerProfit().multiply(pub));
				mallUserProfitNew.setTenMerProfit(mallUserProfitNew.getTenMerProfit().multiply(pub));
				if (mallUserProfitNew.getProfitType() == 2) {
					mallUserProfitNew.setPackageAmount(mallUserProfitNew.getPackageAmount().multiply(pub));
				}
			}
			return ResponseUtil.ok(list);
		}catch (Exception e){
			e.printStackTrace();
			System.out.println("-------------------"+e.getMessage());
			return ResponseUtil.fail("报错了");
		}
    }




	@RequiresPermissions("admin:new:melist")
	@RequiresPermissionsDesc(menu = {"分润变更记录查询"}, button = "查询")
	@GetMapping("/changeList")
	public Object changeList(@RequestParam Integer profitType,
							 @RequestParam(defaultValue = "1") Integer pageNum,
							 @RequestParam(defaultValue = "20") Integer pageSize) throws Exception {
		BigDecimal pub = new BigDecimal("100");
		PageHelper.startPage(pageNum, pageSize);
		 // 分类：0-未知 1-第三方购物 2-自营商品 3-权益商品 4-创客套餐 5-红包




		if (5 == profitType){
			List<MallSystemRule> list = gameRedHourRuleService.list(new QueryWrapper<MallSystemRule>().eq("status", 1)
					.orderByDesc("update_time"));

			for (MallSystemRule rule : list) {
				rule.setProportion(rule.getProportion().multiply(pub));
				rule.setShareProportion(rule.getShareProportion().multiply(pub));
			}

			HashMap<String, Object> map = new HashMap<>();
			map.put("profitType",profitType);
			map.put("data",list);
			return ResponseUtil.ok(map);
		}



		List<MallUserProfitNew> list = serviceImpl.list(
				new QueryWrapper<MallUserProfitNew>().eq("deleted", 1)
						.eq("profit_type", profitType)
						.orderByDesc("update_time"));

		for(MallUserProfitNew mallUserProfitNew: list){
			mallUserProfitNew.setVipProfit(mallUserProfitNew.getVipProfit().multiply(pub));
			mallUserProfitNew.setOrdinaryUsersProfit(mallUserProfitNew.getOrdinaryUsersProfit().multiply(pub));
			mallUserProfitNew.setUserProfit(mallUserProfitNew.getUserProfit().multiply(pub));
			mallUserProfitNew.setVipUsersProfit(mallUserProfitNew.getVipUsersProfit().multiply(pub));
			mallUserProfitNew.setOneMerProfit(mallUserProfitNew.getOneMerProfit().multiply(pub));
			mallUserProfitNew.setTwoMerProfit(mallUserProfitNew.getTwoMerProfit().multiply(pub));
			mallUserProfitNew.setThreeMerProfit(mallUserProfitNew.getThreeMerProfit().multiply(pub));
			mallUserProfitNew.setFourMerProfit(mallUserProfitNew.getFourMerProfit().multiply(pub));
			mallUserProfitNew.setFiveMerProfit(mallUserProfitNew.getFiveMerProfit().multiply(pub));
			mallUserProfitNew.setSixMerProfit(mallUserProfitNew.getSixMerProfit().multiply(pub));
			mallUserProfitNew.setSevenMerProfit(mallUserProfitNew.getSevenMerProfit().multiply(pub));
			mallUserProfitNew.setEightMerProfit(mallUserProfitNew.getEightMerProfit().multiply(pub));
			mallUserProfitNew.setNineMerProfit(mallUserProfitNew.getNineMerProfit().multiply(pub));
			mallUserProfitNew.setTenMerProfit(mallUserProfitNew.getTenMerProfit().multiply(pub));
			if(mallUserProfitNew.getProfitType() == 2){
				mallUserProfitNew.setPackageAmount(mallUserProfitNew.getPackageAmount().multiply(pub));
			}
		}

		HashMap<String, Object> map = new HashMap<>();
		map.put("profitType",profitType);
		map.put("data",list);

		return ResponseUtil.ok(map);
    }
 	@RequiresPermissions("admin:new:save")
 	@RequiresPermissionsDesc(menu = {"新模式波比设置新增"}, button = "新增")
    @PostMapping("/save")
    public Object save(@ModelAttribute MallUserProfitNew mallUserProfitNew) throws Exception {
        serviceImpl.save(mallUserProfitNew);
        return ResponseUtil.ok();
    }

	@RequiresPermissions("admin:new:modify")
	@RequiresPermissionsDesc(menu = {"新模式波比设置修改"}, button = "修改")
    @PutMapping("/modify")
    public Object modify(@ModelAttribute  MallUserProfitNew mallUserProfitNew){
		mallUserProfitNew.setId(null);
    	serviceImpl.saveProfit(mallUserProfitNew);

        return  ResponseUtil.ok();
    }

    @RequiresPermissions("admin:new:del")
    @RequiresPermissionsDesc(menu = {"新模式波比设置删除"}, button = "删除")
    @DeleteMapping("/del/{id}")
    public Object deleteById(@PathVariable  String  id){
        serviceImpl.removeById(id);
        return  ResponseUtil.ok();
    }

}

