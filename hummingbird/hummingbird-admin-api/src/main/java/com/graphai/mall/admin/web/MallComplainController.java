package com.graphai.mall.admin.web;

import cn.afterturn.easypoi.excel.entity.ExportParams;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.lang.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.graphai.commons.util.servlet.ServletUtils;
import com.graphai.mall.admin.util.AdminUserUtils;
import com.graphai.mall.db.constant.account.AccountStatus;
import com.graphai.mall.db.constant.wangzhuan.ComplainEnum;
import com.graphai.mall.db.domain.MallComplain;
import com.graphai.mall.db.domain.MallUser;
import com.graphai.mall.db.service.complain.IMallComplainService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.constant.CommonConstant;
import com.graphai.mall.db.util.account.AccountUtils;
import com.graphai.mall.db.util.phone.PushUtils;
import com.graphai.mall.db.vo.ComplainImagesVo;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.framework.generator.controller.IBaseController;


import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * <p>
 * 投诉表 前端控制器
 * </p>
 *
 * @author biteam
 * @since 2021-05-12
 */
@RestController
@RequestMapping("/admin/complain")
public class MallComplainController extends IBaseController<MallComplain,  String >  {

    private final Log logger = LogFactory.getLog(MallComplainController.class);

	@Autowired
    private IMallComplainService serviceImpl;

	@Autowired
    private MallUserService mallUserService;
    
    @RequiresPermissions("admin:complain:single")
    @RequiresPermissionsDesc(menu = {"单独查询"}, button = "单独查询")
	@GetMapping("/single/{id}")
    public Object getEntityById(@PathVariable  String  id) throws Exception{
        MallComplain entityVName = serviceImpl.getById(id);
        return ResponseUtil.ok(entityVName);
    }


	@GetMapping("/selectImages/{id}")
    public Object selectImages(@PathVariable  String  id) throws Exception{
        List<ComplainImagesVo> complainImagesVos = serviceImpl.selectImages(id);
        Map<String, Object> map = new HashMap<>();
        map.put("items",complainImagesVos);
        return ResponseUtil.ok(map);
    }


    @RequiresPermissions("admin:complain:addOrUpdate")
    @RequiresPermissionsDesc(menu = {"新增"}, button = "新增")
    @PostMapping("/addOrUpdate")
    public Object addOrUpdate(MallComplain mallComplain) throws Exception{
        if(serviceImpl.saveOrUpdate(mallComplain)){
            return ResponseUtil.ok();
        }else{
            return ResponseUtil.fail();
        }
    } 
	
	
	@RequiresPermissions("admin:complain:melist")
	@RequiresPermissionsDesc(menu = {"投诉表查询"}, button = "查询")
    @GetMapping("/melist")
    public Object getMeList() throws Exception {
        IPage<MallComplain> ipage = serviceImpl.page(getPlusPage(),listQueryWrapper());
        List<MallComplain> mallComplains = ipage.getRecords();
        for (MallComplain mallComplain : mallComplains) {
            MallUser complainUser = mallUserService.findById(mallComplain.getComplainUser());
            MallUser complainCreateUser = mallUserService.findById(mallComplain.getComplainCreateUser());
            mallComplain.setComplainUser(complainUser.getNickname()+complainUser.getMobile());
            mallComplain.setComplainCreateUser(complainCreateUser.getNickname()+complainCreateUser.getMobile());
        }
        return ResponseUtil.ok(ipage);
    }
 	
 	private QueryWrapper<MallComplain> listQueryWrapper() {
 		QueryWrapper<MallComplain> meq = new QueryWrapper<>();
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("title"))) {
		      meq.like("title", ServletUtils.getParameter("title"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("complainStatus"))) {
		      meq.eq("complain_status", ServletUtils.getParameter("complainStatus"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("complainDealStatus"))) {
		      meq.eq("complain_deal_status", ServletUtils.getParameter("complainDealStatus"));
		}
		meq.orderByDesc("create_time");
        return meq;
    }

 	@RequiresPermissions("admin:complain:save")
 	@RequiresPermissionsDesc(menu = {"投诉表新增"}, button = "新增")
    @PostMapping("/save")
    public Object save(@ModelAttribute MallComplain mallComplain) throws Exception {
        serviceImpl.save(mallComplain);
        return ResponseUtil.ok();
    }
	
	@RequiresPermissions("admin:complain:modify")
	@RequiresPermissionsDesc(menu = {"投诉表修改"}, button = "审核")
    @PostMapping("/modify")
    public Object modify(@RequestBody MallComplain mallComplain){

        try {
            String content ="";
            mallComplain.setComplainUser(null);
            mallComplain.setComplainCreateUser(null);

            mallComplain.setDealTime(LocalDateTime.now());
            mallComplain.setComplainPunishType(ComplainEnum.PUNISH_TYPE_1.getCode());
            mallComplain.setComplainDealUser(AdminUserUtils.getLoginUserId());
            mallComplain.setComplainStatus(ComplainEnum.PROCESSED_1.getCode());
            serviceImpl.updateById(mallComplain);

            MallComplain mallComplainUser = serviceImpl.getById(mallComplain.getId());

            if (mallComplain.getComplainDealStatus().equals(ComplainEnum.DEAL_STATUS_1.getCode())){
                BigDecimal creditscore = mallComplain.getComplainPunishAmount();
                Map<String, Object> paramsMap1 = new HashMap();
                paramsMap1.put("dType", AccountStatus.DISTRIBUTION_TYPE_00); // 直接分润
                paramsMap1.put("message", "被投诉处罚信用分");
                paramsMap1.put("capitalTrend", AccountStatus.CAPITAL_TREND_02);
                paramsMap1.put("changeType", AccountStatus.CHANGE_TYPE_02);
                paramsMap1.put("changeAccountType", AccountStatus.CHANGE_ACCOUNT_TYPE_4);

                AccountUtils.accountChange(mallComplainUser.getComplainUser(), creditscore, CommonConstant.TASK_REWARD_SCORE,
                        AccountStatus.BID_6, paramsMap1);
                 content = "您的投诉已审核通过,投诉结果为扣除被投诉者信用分:"+mallComplainUser.getComplainPunishAmount();
                 String content2="您被投诉的审核已通过,投诉结果为扣除您信用分:"+mallComplainUser.getComplainPunishAmount();
                PushUtils.pushMessage(mallComplainUser.getComplainCreateUser(), "投诉审核通过", content, 4, null);
                PushUtils.pushMessage(mallComplainUser.getComplainUser(), "投诉审核通过", content2, 4, null);

            }else{
                content = "您的投诉已审核通过,投诉结果为不进行处罚,原因为:"+mallComplain.getComplainDealText();
                PushUtils.pushMessage(mallComplainUser.getComplainCreateUser(), "投诉审核通过", content, 4, null);
                String content2="您被投诉的审核已通过,投诉结果为不对您进行处罚";
                PushUtils.pushMessage(mallComplainUser.getComplainUser(), "投诉审核通过", content2, 4, null);
            }
        } catch (Exception e) {
            logger.info( "投诉审核出错了:"+e.getMessage());
            return ResponseUtil.fail();
        }
        return  ResponseUtil.ok();
    }

    @RequiresPermissions("admin:complain:del")
    @RequiresPermissionsDesc(menu = {"投诉表删除"}, button = "删除")
    @DeleteMapping("/del/{id}")
    public Object deleteById(@PathVariable  String  id){
        serviceImpl.removeById(id);
        return  ResponseUtil.ok();
    }

	@RequiresPermissions("admin:complain:export")
	@RequiresPermissionsDesc(menu = {"投诉表导出"}, button = "导出")
	@GetMapping("/export")
	public Object export() throws Exception {
		List<MallComplain> list = serviceImpl.list(listQueryWrapper());
        return ResponseUtil.export(new ExportParams("投诉表","", ""),MallComplain.class,list);
	}
}

