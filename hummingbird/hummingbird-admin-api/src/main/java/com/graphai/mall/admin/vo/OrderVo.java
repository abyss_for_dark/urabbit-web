package com.graphai.mall.admin.vo;

import com.graphai.framework.annotation.Excel;
import com.graphai.mall.db.domain.MallOrderGoods;
import com.graphai.mall.db.vo.MallAfterSaleVo;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Data
public class OrderVo {


    private String id;

    private String huid;

    @Excel(name = "用户名称")
    private String userName;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    private String platform;

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    private String pubShareFee;


    public String getPubShareFee() {
        return pubShareFee;
    }

    public void setPubShareFee(String pubShareFee) {
        this.pubShareFee = pubShareFee;
    }

    private BigDecimal userProfit;
    private BigDecimal platProfit;
    private BigDecimal agentProfit;


    private String orderId;
    private String goodsId;

    private String userId;

    @Excel(name = "订单号")
    private String orderSn;

    @Excel(name = "订单状态", readConverterExp = "101=未付款,201=已付款,301=已发货,203=已退款,111=已失效,102=用户取消,103=系统取消,401=用户收货,402=系统收货,107=审核失败,202=申请退款")
    private Short orderStatus;

    @Excel(name = "收货人")
    private String consignee;

    @Excel(name = "联系电话")
    private String mobile;

    @Excel(name = "收货人手机号")
    private String receivingMobile;

    private String source;

    @Excel(name = "收货地址")
    private String address;

    private String message;

    private BigDecimal goodsPrice;

    private BigDecimal freightPrice;

    private BigDecimal couponPrice;

    private BigDecimal integralPrice;

    private BigDecimal grouponPrice;

    private BigDecimal specialPrice;

    private BigDecimal turnover;

    @Excel(name = "订单金额")
    private BigDecimal orderPrice;

    @Excel(name = "实付金额")
    private BigDecimal actualPrice;

    private String payId;

    @Excel(name = "支付时间")
    private LocalDateTime payTime;

    @Excel(name = "运单号")
    private String shipSn;

    @Excel(name = "物流公司")
    private String shipChannel;

    @Excel(name = "发货时间")
    private LocalDateTime shipTime;

    private LocalDateTime confirmTime;
    private LocalDateTime settleTime;

    private Short comments;
    private String comment;

    private LocalDateTime endTime;

    private LocalDateTime addTime;

    private LocalDateTime updateTime;

    private Boolean deleted;

    private String tpid;

    private String tpName;

    private List<MallOrderGoods> orderGoods;

    private String orderGoodsStr;
    private String orderThreeStatus;
    private String ticket;
    private String cardPwd;
    private String cardNumber;
    private String avatar;
    private String levelId;
    private LocalDateTime supplementTime;

    private String activityType;
    private String channelCode;

    private String channelId;

    private List<MallAfterSaleVo> mallAftersales;

}
