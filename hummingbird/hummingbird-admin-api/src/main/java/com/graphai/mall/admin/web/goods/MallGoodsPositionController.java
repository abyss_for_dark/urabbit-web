package com.graphai.mall.admin.web.goods;

import cn.afterturn.easypoi.excel.entity.ExportParams;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.lang.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.graphai.commons.util.servlet.ServletUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import org.springframework.web.bind.annotation.RestController;
import com.graphai.framework.generator.controller.IBaseController;

import com.graphai.mall.admin.domain.MallGoodsPosition;
import com.graphai.mall.admin.service.IMallGoodsPositionService;
import java.util.List;



/**
 * <p>
 * 商品位置关系表 前端控制器
 * </p>
 *
 * @author biteam
 * @since 2021-05-19
 */
@RestController
@RequestMapping("/admin/goodsPosition")
public class MallGoodsPositionController extends IBaseController<MallGoodsPosition,  String >  {

	@Autowired
    private IMallGoodsPositionService serviceImpl;

    @RequiresPermissions("admin:goodsPosition:single")
    @RequiresPermissionsDesc(menu = {"单独查询"}, button = "单独查询")
	@GetMapping("/single/{id}")
    public Object getEntityById(@PathVariable  String  id) throws Exception{
        MallGoodsPosition entityVName = serviceImpl.getById(id);
        return ResponseUtil.ok(entityVName);
    }

    @RequiresPermissions("admin:goodsPosition:addOrUpdate")
    @RequiresPermissionsDesc(menu = {"新增"}, button = "新增")
    @PostMapping("/addOrUpdate")
    public Object addOrUpdate(MallGoodsPosition mallGoodsPosition) throws Exception{
        if(serviceImpl.saveOrUpdate(mallGoodsPosition)){
            return ResponseUtil.ok();
        }else{
            return ResponseUtil.fail();
        }
    }


	@RequiresPermissions("admin:goodsPosition:melist")
	@RequiresPermissionsDesc(menu = {"商品位置关系表查询"}, button = "查询")
    @GetMapping("/melist")
    public Object getMeList() throws Exception {
        IPage<MallGoodsPosition> ipage = serviceImpl.page(getPlusPage(),listQueryWrapper());
        return ResponseUtil.ok(ipage);
    }

 	private QueryWrapper<MallGoodsPosition> listQueryWrapper() {
 		QueryWrapper<MallGoodsPosition> meq = new QueryWrapper<>();
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("goodsId"))) {
		      meq.eq("goods_id", ServletUtils.getParameter("goodsId"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("type"))) {
		      meq.eq("type", ServletUtils.getParameter("type"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("deleted"))) {
		      meq.eq("deleted", ServletUtils.getParameter("deleted"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("createId"))) {
		      meq.eq("create_id", ServletUtils.getParameter("createId"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("updateId"))) {
		      meq.eq("update_id", ServletUtils.getParameter("updateId"));
		}

        return meq;
    }
 	@RequiresPermissions("admin:goodsPosition:save")
 	@RequiresPermissionsDesc(menu = {"商品位置关系表新增"}, button = "新增")
    @PostMapping("/save")
    public Object save(@ModelAttribute MallGoodsPosition mallGoodsPosition) throws Exception {
        serviceImpl.save(mallGoodsPosition);
        return ResponseUtil.ok();
    }

	@RequiresPermissions("admin:goodsPosition:modify")
	@RequiresPermissionsDesc(menu = {"商品位置关系表修改"}, button = "修改")
    @PutMapping("/modify")
    public Object modify(@ModelAttribute  MallGoodsPosition mallGoodsPosition){
        serviceImpl.updateById(mallGoodsPosition);
        return  ResponseUtil.ok();
    }

    @RequiresPermissions("admin:goodsPosition:del")
    @RequiresPermissionsDesc(menu = {"商品位置关系表删除"}, button = "删除")
    @DeleteMapping("/del/{id}")
    public Object deleteById(@PathVariable  String  id){
        serviceImpl.removeById(id);
        return  ResponseUtil.ok();
    }

	@RequiresPermissions("admin:goodsPosition:export")
	@RequiresPermissionsDesc(menu = {"商品位置关系表导出"}, button = "导出")
	@GetMapping("/export")
	public Object export() throws Exception {
		List<MallGoodsPosition> list = serviceImpl.list(listQueryWrapper());
        return ResponseUtil.export(new ExportParams("商品位置关系表","", ""),MallGoodsPosition.class,list);
	}
}

