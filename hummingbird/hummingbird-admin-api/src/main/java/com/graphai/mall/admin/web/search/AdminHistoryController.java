package com.graphai.mall.admin.web.search;

import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.mall.db.domain.MallSearchHistory;
import com.graphai.mall.db.domain.MallUser;
import com.graphai.mall.db.domain.MallUserSearchHistoryVo;
import com.graphai.mall.db.service.goods.MallGoodsService;
import com.graphai.mall.db.service.search.MallSearchHistoryService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.validator.Order;
import com.graphai.validator.Sort;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/admin/history")
public class AdminHistoryController {
    private final Log logger = LogFactory.getLog(AdminHistoryController.class);

    @Autowired
    private MallSearchHistoryService searchHistoryService;

    @Autowired
    private MallUserService mallUserService;



    @RequiresPermissions("admin:history:list")
    @RequiresPermissionsDesc(menu = {"用户管理", "搜索历史"}, button = "查询")
    @GetMapping("/list")
    public Object list(String userId, String keyword,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit,
                       @Sort @RequestParam(defaultValue = "id") String sort,
                       @Order @RequestParam(defaultValue = "desc") String order) {
        List<MallSearchHistory> footprintList = searchHistoryService.querySelective(userId, keyword, page, limit, sort, order);
        //long total = PageInfo.of(footprintList).getTotal();
        Map<String, Object> data = new HashMap<>();
        List<MallUserSearchHistoryVo> list = new ArrayList();
        List<String> ids = new ArrayList();
        MallUserSearchHistoryVo mallUserSearchHistoryVo = null;
        List<MallUser> mallUsers = null;

        for (MallSearchHistory searchHistory : footprintList) {
            ids.add(searchHistory.getUserId());
        }

        if (ids.size() != 0) {
            mallUsers = mallUserService.findByIds(ids);
        }

        for (MallSearchHistory mallSearchHistory : footprintList) {
            mallUserSearchHistoryVo = new MallUserSearchHistoryVo();
            mallUserSearchHistoryVo.setId(mallSearchHistory.getId());
            for (MallUser mallUser : mallUsers) {
                if (mallSearchHistory.getUserId().equals(mallUser.getId())) {
                    mallUserSearchHistoryVo.setUserId(mallSearchHistory.getUserId());
                    mallUserSearchHistoryVo.setUserName(mallUser.getUsername());
                    mallUserSearchHistoryVo.setMobile(mallUser.getMobile());
                    mallUserSearchHistoryVo.setAvatar(mallUser.getAvatar());
                }
            }
            mallUserSearchHistoryVo.setKeyword(mallSearchHistory.getKeyword());
            mallUserSearchHistoryVo.setAddTime(mallSearchHistory.getAddTime());
            list.add(mallUserSearchHistoryVo);
        }

        long total = PageInfo.of(footprintList).getTotal();
        data.put("total", total);
        data.put("items", list);
        return ResponseUtil.ok(data);
    }
}
