package com.graphai.mall.admin.service;

import com.alibaba.druid.support.json.JSONUtils;
import com.graphai.commons.util.httprequest.HttpUtils;
import com.graphai.mall.admin.util.LocalDateTimeUtil;
import com.graphai.mall.db.config.TbkapiProperties;
import com.graphai.mall.db.domain.MallGoods;
import com.graphai.mall.db.service.goods.MallGoodsService;

import jodd.util.StringUtil;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TbkGoodsInvalidService {

    private final Log logger = LogFactory.getLog(TbkGoodsInvalidService.class);

    @Autowired
    private MallGoodsService goodsService;

    @Autowired
    private TbkapiProperties properties;

    public void syscTbkGoodsInvalid(int start, int end) {
        Map<String, String> paramsMap = new HashMap<String, String>();

        String url = "http://v2.api.haodanku.com/get_down_items/apikey/" + properties.getApiKey() + "/start/"
                + start + "/end/" + end;

        String retVal = HttpUtils.get(url, paramsMap, null);
//    	logger.info("retVal:"+retVal);

        if (retVal != null) {
            Map<String, Object> retMap = (Map<String, Object>) JSONUtils.parse(retVal);
            if (retMap != null) {
                if (1 == MapUtils.getIntValue(retMap, "code", 0)) {
                    List<Map<String, Object>> goodsList = (List) retMap.get("data");
                    if (goodsList != null) {
                        logger.error("goodsList size:" + goodsList.size());
                        for (Map<String, Object> goodsMap : goodsList) {
                            try {
                                this.syscGoodsInvalid(goodsMap);
                            } catch (Exception e) {
                                logger.error("", e);
                            }
                        }
                    }
                }
            }
        }
    }

    public void syscGoodsInvalid(Map<String, Object> goodsMap)
            throws Exception {

        String itemid = MapUtils.getString(goodsMap, "itemid");
//		String activityid = MapUtils.getString(goodsMap, "activityid");
        MallGoods goodDto = new MallGoods();
        goodDto.setSrcGoodId(itemid);
        goodDto.setDeleted(false);
        goodDto.setChannelCode(properties.getChannelCode());

        MallGoods oldGoodsDto = goodsService.selectOneBySrcGoodIdAndChannelCode(goodDto);
        if (oldGoodsDto != null && oldGoodsDto.getIsOnSale()) {
            oldGoodsDto.setIsOnSale(false);
            oldGoodsDto.setDownType(MapUtils.getString(goodsMap, "down_type"));
            if (StringUtil.isNotBlank(MapUtils.getString(goodsMap, "down_time"))) {
                oldGoodsDto.setDownTime(LocalDateTimeUtil.getLongTimestampToLocalDateTime(MapUtils.getLong(goodsMap, "down_time") * 1000));
            }
            goodsService.updateById(oldGoodsDto);
        }
    }


}
