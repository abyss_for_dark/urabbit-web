package com.graphai.mall.admin.web.order;


import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.mall.admin.domain.MallPurchaseOrder;
import com.graphai.mall.admin.domain.MallUserMerchant;
import com.graphai.mall.admin.service.IMallPurchaseOrderService;
import com.graphai.mall.admin.service.IMallUserMerchantService;
import com.graphai.mall.admin.util.AdminUserUtils;
import com.graphai.mall.db.constant.account.AccountStatus;
import com.graphai.mall.db.domain.MallAdmin;
import com.graphai.mall.db.domain.MallAftersale;
import com.graphai.mall.db.service.order.MallAftersaleService;
import com.graphai.mall.db.service.sysconfig.MallSystemConfigService;
import com.graphai.mall.db.util.OrderUtil;
import com.graphai.open.mallgoods.entity.IMallGoods;
import com.graphai.open.mallgoods.service.IMallGoodsService;
import com.graphai.open.mallorder.service.impl.MallOrderZYServiceImpl;
import com.graphai.validator.Order;
import com.graphai.validator.Sort;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/admin/aftersale")
@Validated
public class AdminAftersaleController {

    @Autowired
    private MallAftersaleService aftersaleService;

    @Autowired
    private IMallGoodsService goodsService;

    @Resource
    private IMallPurchaseOrderService purchaseOrderService;

    @Autowired
    private IMallUserMerchantService userMerchantService;

    @Autowired
    private MallSystemConfigService systemConfigService;

    @Autowired
    private MallOrderZYServiceImpl zyService;


    @RequiresPermissions("admin:aftersale:list")
    @RequiresPermissionsDesc(menu = {"商城管理", "售后管理"}, button = "查询")
    @GetMapping("/list")
    public Object list(String orderId, String aftersaleSn, String method,String status,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit,
                       @Sort @RequestParam(defaultValue = "add_time") String sort,
                       @Order @RequestParam(defaultValue = "desc") String order) {

        MallAdmin loginUser = AdminUserUtils.getLoginUser();
        String roleIds = Arrays.toString(loginUser.getRoleIds());
        List<MallAftersale> aftersaleList = null;
        if (roleIds.contains(AccountStatus.LOGIN_ROLE_ADMIN)) {
            aftersaleList = aftersaleService.querySelective(orderId, aftersaleSn, status,null, page, limit, sort, order);
        } else {
            MallUserMerchant userMerchant = userMerchantService.getOne(new QueryWrapper<MallUserMerchant>().lambda()
                    .eq(MallUserMerchant::getUserId, loginUser.getUserId()));
            aftersaleList = aftersaleService.querySelective(orderId, aftersaleSn, status,userMerchant.getMerchantId(), page, limit, sort, order);
        }
        long total = PageInfo.of(aftersaleList).getTotal();
        HashMap<String, Object> map = new HashMap<>();
        if (StrUtil.isNotBlank(method)) {
            MallAftersale mallAftersale = aftersaleList.get(0);
            IMallGoods goods = goodsService.getById(mallAftersale.getGoodsId());
            mallAftersale.setGoodsUrl(goods.getPicUrl());
            mallAftersale.setGoodsName(goods.getName());
        }
        map.put("total", total);
        map.put("list", aftersaleList);
        return ResponseUtil.ok(map);
    }

    @RequiresPermissions("admin:aftersale:refund")
    @RequiresPermissionsDesc(menu = {"售后管理"}, button = "退款")
    @PostMapping("/refund")
    public Object refund(@RequestBody MallAftersale aftersale) {
        return aftersaleService.handleRefund(aftersale);
    }

    @RequiresPermissions("admin:aftersale:save")
    @RequiresPermissionsDesc(menu = {"售后管理"}, button = "自营商品售后订单插入")
    @PostMapping("/save")
    public Object saveZyGoodsPurchaseAfterSale(@RequestBody MallAftersale mallAftersale) {
        //TODO 是否是已完成状态的订单状态 售后状态
        Short orderType = mallAftersale.getOrderType();
        String orderId = mallAftersale.getOrderId();
        //从配置参数中获取有效时间 未设置默认是7天
        Map<String, String> map = systemConfigService.getMallSystemCommon("afterSale");
        String  applyAfterSaleValid = map.get("apply_aftersale_valid");
        Integer validTime = 7;
        if (StrUtil.isNotBlank(applyAfterSaleValid)){
            validTime= Integer.parseInt(applyAfterSaleValid);
        }
        if ((Short.valueOf("0")).equals(orderType)) {
            MallPurchaseOrder purchaseOrder = purchaseOrderService.getOne(new QueryWrapper<MallPurchaseOrder>()
                    .lambda().eq(MallPurchaseOrder::getId, orderId));
            //售后
            if (OrderUtil.STATUS_AFTERSALE_REFUND.equals(purchaseOrder.getOrderStatus().shortValue())) {
                //判断用户是否已收货
                if ( ObjectUtil.isNotNull(purchaseOrder.getConfirmTime())){
                    if (!purchaseOrder.getConfirmTime().plusDays(validTime).isAfter(LocalDateTime.now())){
                        return ResponseUtil.fail("售后已超过有效时间,无法申请售后!");
                    }
                }
                
            }else if (OrderUtil.STATUS_CONFIRM.equals(purchaseOrder.getOrderStatus().shortValue())){
                //用户已收货
                if (!purchaseOrder.getConfirmTime().plusDays(validTime).isAfter(LocalDateTime.now())){
                    return ResponseUtil.fail("售后已超过有效时间,无法申请售后!");
                }
                purchaseOrder.setOrderStatus(OrderUtil.STATUS_AFTERSALE_REFUND.intValue());
                purchaseOrderService.updateById(purchaseOrder);
            }
        }
        aftersaleService.add(mallAftersale);
        return ResponseUtil.ok();
    }

}
