package com.graphai.mall.admin.job;

import com.alibaba.druid.support.json.JSONUtils;
import com.graphai.commons.util.httprequest.HttpUtils;
import com.graphai.mall.db.config.TbkapiProperties;
import com.graphai.mall.db.dao.MallCategoryMapper;
import com.graphai.mall.db.domain.MallCategory;
import com.graphai.mall.db.domain.MallCategoryExample;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//@Component
public class TbkGoodsCategorySyncJob {

    private final Log logger = LogFactory.getLog(TbkGoodsCategorySyncJob.class);

//    @Autowired
//    private AdminAdSyncService adminAdSyncService;

//    @Autowired
//    private AdminTbkSyncInstService adminTbkSyncInstService;

    @Autowired
    private MallCategoryMapper MallCategoryMapper;

    @Autowired
    private TbkapiProperties properties;

    private final static String apiKey = "zRuJVVE2TadyVHXGCIFsngM7jucBXaTJ";

    //每隔10分钟(10*60*1000ms)执行一次
    @Scheduled(fixedRate = 6 * 60 * 60 * 1000)
    public void syscAd() {
        this.syscTbkGoodsCate();
    }

    public void syscTbkGoodsCate() {
        Map<String, String> paramsMap = new HashMap<String, String>();
        paramsMap.put("apikey", apiKey);

        String url = "http://api.tbk.dingdanxia.com/spk/cate";
        String retVal = HttpUtils.post(url, paramsMap, null);

        logger.info("retVal:" + retVal);

        if (retVal != null) {
            Map<String, Object> retMap = (Map<String, Object>) JSONUtils.parse(retVal);
//    		System.out.println(retMap);
            if (retMap != null && 200 == MapUtils.getIntValue(retMap, "code", 0)) {
                List<Map<String, Object>> cateList = (List) retMap.get("data");
                if (cateList != null) {
                    for (Map<String, Object> cateMap : cateList) {
                        try {
                            logger.info("cateMap:" + cateMap);
                            this.syncTbkGoodsCate(cateMap);
                        } catch (Exception e) {
                            logger.error("", e);
                        }

                    }
                }

            }
        }
    }


    private void syncTbkGoodsCate(Map<String, Object> cateMap)
            throws Exception {

//    	cateMap:{id=171, name=健身装备 , cid=13, pid=170, 
//    	icon=https://img.alicdn.com/imgextra/i2/2053469401/TB27Bmltv9TBuNjy1zbXXXpepXa-2053469401.png, tb_cid=50010728}
        MallCategory cate = new MallCategory();
        cate.setName(MapUtils.getString(cateMap, "name"));
        cate.setKeywords(MapUtils.getString(cateMap, "name"));
        cate.setPid(10000 + MapUtils.getString(cateMap, "pid"));
        cate.setIconUrl(MapUtils.getString(cateMap, "icon"));
        cate.setPicUrl(MapUtils.getString(cateMap, "icon"));
        if (MapUtils.getInteger(cateMap, "pid") != 0) {
            cate.setLevel("L2");
        } else {
            cate.setLevel("L1");
        }
        cate.setPositionCode("categoryHome");
        cate.setDeleted(false);
        LocalDateTime now = LocalDateTime.now();
        cate.setAddTime(now);
        cate.setUpdateTime(now);
        cate.setSortOrder(66);
        cate.setChannelCode(properties.getChannelCode());
        cate.setTbCateId(MapUtils.getString(cateMap, "tb_cid"));
        cate.setInfCateId(MapUtils.getString(cateMap, "id"));

        String infPid = MapUtils.getString(cateMap, "pid");
        cate.setInfPid(infPid);
        if ("0".equals(infPid)) {
            cate.setPid("0");
        } else {
            cate.setPid(this.getParentCateId(infPid));
        }

        MallCategoryMapper.insert(cate);
    }

    private String getParentCateId(String infPid) {
        MallCategoryExample example = new MallCategoryExample();
        MallCategoryExample.Criteria criteria = example.createCriteria();
        criteria.andChannelCodeEqualTo(properties.getChannelCode());
        criteria.andInfCateIdEqualTo(infPid);

        MallCategory cate = MallCategoryMapper.selectOneByExample(example);
        if (cate != null) {
            return cate.getId();
        } else {
            return "-3";
        }
    }

}
