package com.graphai.mall.admin.vo;

import lombok.Data;

import java.util.List;

@Data
public class CategoryVo {
    private String id;
    private String name;
    private String keywords;
    private String desc;
    private String iconUrl;
    private String picUrl;
    private String level;
    private String pid;
    private String isHost;
    private String hostUrl;
    private String hostSort;
    private String industry;
    private String status;
    private Integer sortOrder;
    private String positionCode;
    private String linkUrl;
    private String smallIconUrl;
    private String navigatorType;
    private String chainChange;
    private List<CategoryVo> children;

}
