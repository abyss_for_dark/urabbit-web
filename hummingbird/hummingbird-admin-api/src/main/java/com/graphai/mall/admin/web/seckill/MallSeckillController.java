package com.graphai.mall.admin.web.seckill;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.github.pagehelper.PageHelper;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.lang.StringUtils;
import com.graphai.commons.util.servlet.ServletUtils;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.framework.generator.controller.IBaseController;
import com.graphai.framework.system.service.MallRoleService;
import com.graphai.id.GraphaiIdGenerator;
import com.graphai.mall.admin.domain.*;
import com.graphai.mall.admin.service.*;
import com.graphai.mall.db.domain.MallAdmin;
import com.graphai.mall.db.domain.MallRole;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * <p>
 * 商品秒杀活动表  前端控制器
 * </p>
 *
 * @author biteam
 * @since 2021-07-05
 */
@RestController
@RequestMapping("/admin/seckill/seckill")
public class MallSeckillController extends IBaseController<MallSeckill, String> {

    @Autowired
    private IMallSeckillService serviceImpl;
    @Autowired
    private IMallSeckillTimeService seckillTimeService;
    @Autowired
    private IMallSeckillGoodsService seckillGoodsService;
    @Autowired
    private IMallUserMerchantService userMerchantService;
    @Autowired
    private MallRoleService mallRoleService;


    @GetMapping("/single/{id}")
    public Object getEntityById(@PathVariable String id) throws Exception {
        MallSeckill entityVName = serviceImpl.getById(id);

        QueryWrapper<MallSeckillTime> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("seckill_id", entityVName.getId());
        List<MallSeckillTime> times = seckillTimeService.list(queryWrapper);
        List<String> strings = new ArrayList<>();
        for (MallSeckillTime time : times) {
            int hour = time.getSeckillTime().getHour();
            strings.add(String.valueOf(hour));
        }
        entityVName.setSeckillTime(strings);

        return ResponseUtil.ok(entityVName);
    }

    @GetMapping("/audit/{id}")
    public Object audit(@PathVariable String id) throws Exception {

        MallSeckillGoods mallSeckillGoods = seckillGoodsService.getById(id);
        mallSeckillGoods.setAuditStatus(1);
        mallSeckillGoods.setIsOnSale(1);
        seckillGoodsService.updateById(mallSeckillGoods);

        return ResponseUtil.ok();
    }

    @GetMapping("/reject")
    public Object reject(@RequestParam String id, @RequestParam String reason) throws Exception {

        MallSeckillGoods mallSeckillGoods = seckillGoodsService.getById(id);
        mallSeckillGoods.setAuditStatus(2);
        mallSeckillGoods.setIsOnSale(0);
        mallSeckillGoods.setReason(reason);
        seckillGoodsService.updateById(mallSeckillGoods);

        return ResponseUtil.ok();
    }

    @RequiresPermissions("admin:admin:seckill:addOrUpdate")
    @RequiresPermissionsDesc(menu = {"新增"}, button = "新增")
    @PostMapping("/addOrUpdate")
    public Object addOrUpdate(MallSeckill mallSeckill) throws Exception {
        if (serviceImpl.saveOrUpdate(mallSeckill)) {
            return ResponseUtil.ok();
        } else {
            return ResponseUtil.fail();
        }
    }


    @RequiresPermissions("admin:admin:seckill:melist")
    @RequiresPermissionsDesc(menu = {"商品秒杀活动表 查询"}, button = "查询")
    @GetMapping("/melist")
    public Object getMeList() throws Exception {
        IPage<MallSeckill> ipage = serviceImpl.page(getPlusPage(), listQueryWrapper());
        List<MallSeckill> list = ipage.getRecords();
        LocalDateTime now = LocalDateTime.now();
        for (MallSeckill item : list) {
            QueryWrapper<MallSeckillTime> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("seckill_id", item.getId());
            List<MallSeckillTime> times = seckillTimeService.list(queryWrapper);
            List<String> strings = new ArrayList<>();
            for (MallSeckillTime time : times) {
                int hour = time.getSeckillTime().getHour();
                if (hour < 10) {
                    strings.add("0" + hour + ":00");
                } else {
                    strings.add(hour + ":00");
                }
            }
            item.setSeckillTime(strings);

//            if (now.isBefore(item.getApplyTime())) {
                item.setApplyFlag(true);
//            }

        }

        return ResponseUtil.ok(ipage);
    }

    @RequiresPermissions("admin:admin:seckill:melist:plant")
    @RequiresPermissionsDesc(menu = {"商品秒杀活动表 查询"}, button = "查询")
    @GetMapping("/melist/plant")
    public Object getMeListPlant() throws Exception {
        IPage<MallSeckill> ipage = serviceImpl.page(getPlusPage(), listQueryWrapper());
        List<MallSeckill> list = ipage.getRecords();
        LocalDateTime now = LocalDateTime.now();
        for (MallSeckill item : list) {
            QueryWrapper<MallSeckillTime> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("seckill_id", item.getId());
            List<MallSeckillTime> times = seckillTimeService.list(queryWrapper);
            List<String> strings = new ArrayList<>();
            for (MallSeckillTime time : times) {
                int hour = time.getSeckillTime().getHour();
                if (hour < 10) {
                    strings.add("0" + hour + ":00");
                } else {
                    strings.add(hour + ":00");
                }
            }
            item.setSeckillTime(strings);

//            if (now.isBefore(item.getApplyTime())) {
            item.setApplyFlag(true);
//            }

        }

        return ResponseUtil.ok(ipage);
    }

    private QueryWrapper<MallSeckill> listQueryWrapper() {
        QueryWrapper<MallSeckill> meq = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(ServletUtils.getParameter("seckillName"))) {
            meq.like("seckill_name", ServletUtils.getParameter("seckillName"));
        }
        if (StringUtils.isNotEmpty(ServletUtils.getParameter("startTime"))) {
            meq.ge("start_time", ServletUtils.getLocalDateTime("startTime"));
        }
        if (StringUtils.isNotEmpty(ServletUtils.getParameter("endTime"))) {
            meq.le("start_time", ServletUtils.getLocalDateTime("endTime"));
        }
        if (StringUtils.isNotEmpty(ServletUtils.getParameter("version"))) {
            meq.eq("version", ServletUtils.getParameter("version"));
        }
        if (StringUtils.isNotEmpty(ServletUtils.getParameter("seckillStatus"))) {
            meq.eq("seckill_status", ServletUtils.getParameter("seckillStatus"));
        }
        if (StringUtils.isNotEmpty(ServletUtils.getParameter("remarks"))) {
            meq.eq("remarks", ServletUtils.getParameter("remarks"));
        }

        return meq;
    }

    @RequiresPermissions("admin:admin:seckill:save")
    @RequiresPermissionsDesc(menu = {"商品秒杀活动表 新增"}, button = "新增")
    @PostMapping("/save")
    public Object save(@ModelAttribute MallSeckill mallSeckill) throws Exception {
//        mallSeckill.setCreateTime(LocalDateTime.of(LocalDate.now(), LocalTime.now()));
//
//        LocalDateTime time = mallSeckill.getStartTime();
        LocalDateTime time = LocalDateTime.now();
//        LocalDateTime startTime = LocalDateTime.of(time.getYear(), time.getMonth(), time.getDayOfMonth(), 0, 0, 0);
//        mallSeckill.setStartTime(startTime);
//
//        LocalDateTime endTime = LocalDateTime.of(startTime.getYear(), startTime.getMonth(), startTime.getDayOfMonth(), 23, 59, 59);
//        mallSeckill.setEndTime(endTime);
//        if (mallSeckill.getStartTime().isBefore(mallSeckill.getEndTime())) {
//
//        } else {
//            throw new RuntimeException("活动开始时间大于结束时间");
//        }
        QueryWrapper<MallSeckill> meq = new QueryWrapper<>();
        meq.eq("start_time", mallSeckill.getStartTime());
        List<MallSeckill> result = serviceImpl.list(meq);
        if (ObjectUtil.isNotNull(result) && ObjectUtil.isNotEmpty(result)) {
            throw new RuntimeException("活动开始时间已存在");
        }
        serviceImpl.save(mallSeckill);

        //新增时间段
        List<String> seckillTimes = mallSeckill.getSeckillTime();
        List<MallSeckillTime> timeList = new ArrayList<>();
        for (String str : seckillTimes) {
            MallSeckillTime mallSeckillTime = new MallSeckillTime();
            mallSeckillTime.setId(GraphaiIdGenerator.nextId("MallSeckillTime"));

            LocalDateTime localDateTime = LocalDateTime.of(time.getYear(), time.getMonth(), time.getDayOfMonth(), Integer.parseInt(str), 0, 0);

            mallSeckillTime.setSeckillTime(localDateTime);
            mallSeckillTime.setSeckillId(mallSeckill.getId());
            timeList.add(mallSeckillTime);
        }
        seckillTimeService.saveBatch(timeList);


        return ResponseUtil.ok();
    }

    @RequiresPermissions("admin:admin:seckill:modify")
    @RequiresPermissionsDesc(menu = {"商品秒杀活动表 修改"}, button = "修改")
    @PutMapping("/modify")
    public Object modify(@ModelAttribute MallSeckill mallSeckill) {
        LocalDateTime time = mallSeckill.getStartTime();
        LocalDateTime startTime = LocalDateTime.of(time.getYear(), time.getMonth(), time.getDayOfMonth(), 0, 0, 0);
        mallSeckill.setStartTime(startTime);

        LocalDateTime endTime = LocalDateTime.of(startTime.getYear(), startTime.getMonth(), startTime.getDayOfMonth(), 23, 59, 59);
        mallSeckill.setEndTime(endTime);

        serviceImpl.updateById(mallSeckill);
        //删除时间段
        QueryWrapper<MallSeckillTime> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("seckill_id", mallSeckill.getId());

        //新增时间段
        List<String> seckillTimes = mallSeckill.getSeckillTime();
        List<LocalDateTime> times = new ArrayList<>();

        for (String str : seckillTimes) {
            LocalDateTime localDateTime = LocalDateTime.of(time.getYear(), time.getMonth(), time.getDayOfMonth(), Integer.parseInt(str), 0, 0);
            times.add(localDateTime);
        }

        queryWrapper.notIn("seckill_time", times);
        List<MallSeckillTime> MallSeckillTimes = seckillTimeService.list(queryWrapper);
        if (ObjectUtil.isNotEmpty(MallSeckillTimes) && ObjectUtil.isNotNull(MallSeckillTimes)) {
            List<String> ids = MallSeckillTimes.stream().map(MallSeckillTime::getId).collect(Collectors.toList());
            seckillTimeService.removeByIds(ids);
        }

        //新增时间段
        QueryWrapper<MallSeckillTime> queryWrapper1 = new QueryWrapper<>();
        queryWrapper1.eq("seckill_id", mallSeckill.getId());
        List<MallSeckillTime> mallSeckillTimes = seckillTimeService.list(queryWrapper1);

        List<MallSeckillTime> timeList = new ArrayList<>();
        for (int i = times.size() - 1; i >= 0; i--) {
            LocalDateTime it = times.get(i);
            for (MallSeckillTime item : mallSeckillTimes) {
                if (it.equals(item.getSeckillTime())) {
                    times.remove(it);
                    break;
                }
            }
        }

        if (times.size() > 0) {
            for (int i = times.size() - 1; i >= 0; i--) {
                MallSeckillTime mallSeckillTime = new MallSeckillTime();
                mallSeckillTime.setId(GraphaiIdGenerator.nextId("MallSeckillTime"));
                mallSeckillTime.setSeckillTime(times.get(i));
                mallSeckillTime.setSeckillId(mallSeckill.getId());
                timeList.add(mallSeckillTime);
            }
        }
        seckillTimeService.saveBatch(timeList);

        return ResponseUtil.ok();
    }


    @RequiresPermissions("admin:admin:seckill:del")
    @RequiresPermissionsDesc(menu = {"商品秒杀活动表 删除"}, button = "删除")
    @DeleteMapping("/del/{id}")
    public Object deleteById(@PathVariable String id) {
        serviceImpl.removeById(id);
        return ResponseUtil.ok();
    }


    /**
     * 根据活动id查询对应时间段，及其时间段下参与活动的商品
     *
     * @return
     */
    @GetMapping("/timeAndGoods/{id}")
    public Object timeAndGoods(@PathVariable String id) {
        // 获取当前登陆人
        MallAdmin adminUser = (MallAdmin) SecurityUtils.getSubject().getPrincipal();
        Set<MallRole> roles = mallRoleService.findByIds(adminUser.getRoleIds());
        String role = null;
        for (MallRole item : roles) {
            role = item.getRoleKey();
        }

        // 获取当前登陆人关联商户
        QueryWrapper<MallUserMerchant> mallUserMerchantQueryWrapper = new QueryWrapper<>();
        mallUserMerchantQueryWrapper.eq("user_id", adminUser.getUserId());
        List<MallUserMerchant> userMerchants = userMerchantService.list(mallUserMerchantQueryWrapper);
        List<String> merchantIds = userMerchants.stream().map(MallUserMerchant::getMerchantId).collect(Collectors.toList());

        // 获取该秒杀活动时间段，及该时间段该登录人上架商品
        QueryWrapper<MallSeckillTime> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("seckill_id", id);
        List<MallSeckillTime> mallSeckillTimes = seckillTimeService.list(queryWrapper);

        for (MallSeckillTime mallSeckillTime : mallSeckillTimes) {
            QueryWrapper<MallSeckillGoods> mallSeckillGoodsQueryWrapper = new QueryWrapper<>();
            mallSeckillGoodsQueryWrapper.eq("seckill_time_id", mallSeckillTime.getId());
            if ("plant".equals(role)) {
                mallSeckillGoodsQueryWrapper.in("merchant_id", merchantIds);
            }
            List<MallSeckillGoods> list = seckillGoodsService.list(mallSeckillGoodsQueryWrapper);
            if (ObjectUtil.isNotEmpty(list) && ObjectUtil.isNotNull(list)) {

                List<String> goodsId = list.stream().map(MallSeckillGoods::getGoodsId).collect(Collectors.toList());

                QueryWrapper<MallActivityGoodsProduct> mallActivityGoodsProductQueryWrapper = new QueryWrapper<>();
                mallActivityGoodsProductQueryWrapper.eq("activity_rule_id", mallSeckillTime.getId());
                mallActivityGoodsProductQueryWrapper.in("goods_id", goodsId);
                mallActivityGoodsProductQueryWrapper.orderByDesc("add_time");
                List<MallActivityGoodsProduct> result = mallActivityGoodsProductService.list(mallActivityGoodsProductQueryWrapper);
                for (MallActivityGoodsProduct item : result) {
                    for (MallSeckillGoods it : list) {
                        if (item.getGoodsId().equals(it.getGoodsId())) {
                            item.setGoodsName(it.getGoodsName());
                            item.setMerchantId(it.getMerchantId());
                            item.setSeckillId(it.getSeckillId());
                            item.setSeckillGoodsId(it.getId());
                            item.setIsOnSale(it.getIsOnSale());
                            item.setAuditStatus(it.getAuditStatus());
                            item.setStartsNumber(it.getStartsNumber());
                        }
                    }
                }
                mallSeckillTime.setSeckillGoods(result);
            } else {
                mallSeckillTime.setSeckillGoods(new ArrayList<>());
            }


            int hour = mallSeckillTime.getSeckillTime().getHour();
            if (hour < 10) {
                mallSeckillTime.setTime("0" + hour + ":00");
            } else {
                mallSeckillTime.setTime(hour + ":00");
            }
        }


        return ResponseUtil.ok(mallSeckillTimes);
    }

    /**
     * 根据秒杀商品id集合，批量删除
     *
     * @return
     */
    @PostMapping("/delSeckillGoods")
    public Object delSeckillGoods(@RequestBody List<String> ids) {

        MallActivityGoodsProduct mallActivityGoodsProduct = mallActivityGoodsProductService.getById(ids.get(0));
        mallActivityGoodsProductService.removeByIds(ids);

        //删除已不存在规格的商品
        QueryWrapper<MallActivityGoodsProduct> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("activity_rule_id", mallActivityGoodsProduct.getActivityRuleId());
        queryWrapper.eq("deleted", 0);
        List<MallActivityGoodsProduct> result = mallActivityGoodsProductService.list(queryWrapper);
        List<String> goodsList = result.stream().map(MallActivityGoodsProduct::getGoodsId).collect(Collectors.toList());
        Set<String> goodsId = new HashSet<>();
        goodsId.addAll(goodsList);
        goodsList = new ArrayList<>();
        for (String id : goodsId) {
            goodsList.add(id);
        }

        if (ObjectUtil.isNotNull(goodsList) && ObjectUtil.isNotEmpty(goodsList)) {
            QueryWrapper<MallSeckillGoods> queryWrapper1 = new QueryWrapper<>();
            queryWrapper1.eq("seckill_time_id", mallActivityGoodsProduct.getActivityRuleId());
            queryWrapper1.notIn("goods_id", goodsList);
            List<MallSeckillGoods> goods = seckillGoodsService.list(queryWrapper1);

            ids = goods.stream().map(MallSeckillGoods::getId).collect(Collectors.toList());
            seckillGoodsService.removeByIds(ids);
        } else {
            //如果没有规格了，则删除该秒杀时间段中所有商品
            QueryWrapper<MallSeckillGoods> queryWrapper1 = new QueryWrapper<>();
            queryWrapper1.eq("seckill_time_id", mallActivityGoodsProduct.getActivityRuleId());
            seckillGoodsService.remove(queryWrapper1);
        }
        return ResponseUtil.ok();
    }

    /**
     * 根据秒杀商品id集合，批量过审
     *
     * @return
     */
    @PostMapping("/auditSeckillGoods")
    public Object auditSeckillGoods(@RequestBody List<String> ids) {
        List<MallSeckillGoods> result = seckillGoodsService.listByIds(ids);
        for (MallSeckillGoods item : result) {
            item.setAuditStatus(1);
            item.setIsOnSale(1);
        }
        seckillGoodsService.updateBatchById(result);
        return ResponseUtil.ok();
    }

    @Autowired
    private IMallActivityGoodsProductService mallActivityGoodsProductService;

    /**
     * 根据活动时间id，当前登录人删除参与活动的商品
     *
     * @return
     */
    @PostMapping("/saveSeckillGoods")
    public Object saveSeckillGoods(@RequestBody JSONArray jsonArray) {
        List<JSONObject> result = jsonArray.toList(JSONObject.class);
        List<MallActivityGoodsProduct> list = null;
        List<MallSeckillGoods> goods = new ArrayList<>();
        for (JSONObject item : result) {
            String goodsIds = "";
            if (JSONUtil.isJsonArray(item.getStr("seckillGoods"))) {
                JSONArray array = item.getJSONArray("seckillGoods");
                list = array.toList(MallActivityGoodsProduct.class);
                for (MallActivityGoodsProduct it : list) {
                    //不是0的商品不必新增秒杀商品
                    if ("0".equals(it.getId())) {
                        it.setId(GraphaiIdGenerator.nextId("MallActivityGoodsProduct"));
                        it.setSurplusNumber(it.getNumber());
                        it.setAddTime(LocalDateTime.now());


                        if (goodsIds.indexOf(it.getGoodsId()) == -1) {
                            MallSeckillGoods mallSeckillGoods = new MallSeckillGoods();
                            String id = GraphaiIdGenerator.nextId("MallSeckillGoods");
                            mallSeckillGoods.setId(id);
                            mallSeckillGoods.setIsOnSale(it.getIsOnSale());
                            mallSeckillGoods.setSeckillTimeId(it.getActivityRuleId());
                            mallSeckillGoods.setSeckillId(it.getSeckillId());
                            mallSeckillGoods.setGoodsName(it.getGoodsName());
                            mallSeckillGoods.setStartsNumber(it.getStartsNumber());
                            mallSeckillGoods.setMerchantId(it.getMerchantId());
                            mallSeckillGoods.setGoodsId(it.getGoodsId());
                            mallSeckillGoods.setCreateTime(LocalDateTime.now());
                            mallSeckillGoods.setAuditStatus(it.getAuditStatus());
                            goods.add(mallSeckillGoods);
                            goodsIds += it.getGoodsId();
                        }
                    }
                }
                if (list.size() > 0) {
                    seckillGoodsService.saveOrUpdateBatch(goods);
                    mallActivityGoodsProductService.saveOrUpdateBatch(list);
                }
            }
        }

        return ResponseUtil.ok();
    }


}

