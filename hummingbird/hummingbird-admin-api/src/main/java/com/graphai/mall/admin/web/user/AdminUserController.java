package com.graphai.mall.admin.web.user;

import static com.graphai.framework.constant.AdminResponseCode.ADMIN_INVALID_ACCOUNT;

import java.awt.Color;
import java.awt.Font;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.bcrypt.BCryptPasswordEncoder;
import com.graphai.commons.util.httprequest.HttpUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.framework.storage.StorageService;
import com.graphai.id.GraphaiIdGenerator;
import com.graphai.mall.admin.domain.MallRedQrcode;
import com.graphai.mall.admin.service.IMallRedQrcodeService;
import com.graphai.mall.db.domain.MallAdmin;
import com.graphai.mall.db.domain.MallStorage;
import com.graphai.mall.db.domain.MallUser;
import com.graphai.mall.db.domain.MallUserCard;
import com.graphai.mall.db.domain.MallUserLevelChange;
import com.graphai.mall.db.service.MallAdminService;
import com.graphai.mall.db.service.pay.DoPayService;
import com.graphai.mall.db.service.promotion.MarketingSignRecordService;
import com.graphai.mall.db.service.sysconfig.MallSystemConfigService;
import com.graphai.mall.db.service.user.MallUserLevelChangeService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.service.user.behavior.MallUserCardService;
import com.graphai.mall.db.util.RandomUtils;

import cn.hutool.core.img.ImgUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.extra.qrcode.QrCodeUtil;
import cn.hutool.extra.qrcode.QrConfig;
import cn.hutool.json.JSONUtil;


@RestController
@RequestMapping("/admin/user")
@Validated
public class AdminUserController {
    private final Log logger = LogFactory.getLog(AdminUserController.class);

    @Autowired
    private MallUserService userService;
    @Autowired
    private MallUserLevelChangeService mallUserLevelChangeService;
    @Autowired
    private StorageService storageService;
    @Autowired
    private MallUserService mallUserService;
    @Autowired
    private IMallRedQrcodeService serviceImpl;
    @Value("${weiRed.qrcodeUrl}")
    private String requestUrl;
    @Autowired
    private MallSystemConfigService mallSystemConfigService;
    @Resource
    private MallAdminService mallAdminService;
    @Value("${weiRed.wlsqUrl}")
    private String wlsqUrl;
    @Autowired
    private DoPayService doPayService;
    @Autowired
    private MarketingSignRecordService marketingSignRecordService;
    @Autowired
    private MallUserCardService mallUserCardService;

    @RequiresPermissions("admin:user:list")
    @RequiresPermissionsDesc(menu = {"用户管理", "会员管理"}, button = "查询")
    @GetMapping("/list")
    public Object list(String mobile, String channelId, String levelId, String beginTime, String endTime,
                    Boolean getSeachParamList, String firstMobile, String version, String plat,String lastBeginTime,String lastEndTime,
                    @RequestParam(required = false) List<Integer> region,
                    @RequestParam(required = false) String customerId,@RequestParam(required = false) String pUserId,
                    @RequestParam(required = false) String firstLevel, @RequestParam(defaultValue = "1") Integer page,
                    @RequestParam(defaultValue = "20") Integer limit,@RequestParam(required = false) Integer fansNum) {


        // (page - 1) * limit
        HashMap<String, Object> userList =
                        userService.getCommonUserListV2(plat, version, firstLevel, customerId, firstMobile, mobile,
                                        channelId, levelId, beginTime, endTime, page, limit, getSeachParamList, region,pUserId,lastBeginTime,lastEndTime,fansNum);
        return ResponseUtil.ok(userList);
    }

    @RequiresPermissions("admin:user:changeUserState")
    @RequiresPermissionsDesc(menu = {"用户管理", "会员管理"}, button = "修改会员状态")
    @PostMapping("/changeUserState")
    public Object changeUserState(@RequestParam String status, @RequestParam String id) {
        if (!status.equalsIgnoreCase("0") && !status.equalsIgnoreCase("1")) {
            return ResponseUtil.badArgument();
        }
        MallUser user = new MallUser();
        user.setId(id);
        user.setStatus(new Byte(status));
        userService.updateById(user);
        return ResponseUtil.ok();
    }


    /**
     * 获取用户等级变更记录
     */
    @RequiresPermissions("admin:user:getUserLevelChange")
    @RequiresPermissionsDesc(menu = {"用户管理", "会员管理"}, button = "查询用户变更记录")
    @GetMapping("/getUserLevelChange")
    public Object getUserLevelChange(@RequestParam String userId) throws IOException {
        List<MallUserLevelChange> list = mallUserLevelChangeService.selectUserLevelChangeByUserId(userId);
        Map<String, Object> map = new HashMap<>();
        map.put("items", list);
        map.put("total", list.size());
        return ResponseUtil.ok(map);

    }

    /**
     * 后台升级创客
     */
    @RequiresPermissions("admin:user:createMaker")
    @RequiresPermissionsDesc(menu = {"用户管理", "会员管理"}, button = "升级创客")
    @GetMapping("/createMaker")
    public Object createMaker(@RequestParam String id, @RequestParam String operationPWd, @RequestParam(defaultValue = "") String isCent) throws IOException {

        System.out.println("是否分傭"+isCent);

        MallUser mallUser2 = mallUserService.findById(id);
        Object error = validate(operationPWd);
        if (error != null) {
            return error;
        }

        String qrCodeUrl = "";
        {
            // 生成注册邀请码
            String qrcode = RandomUtils.genRandomNum(6);
            logger.info("后台管理系统升级创客-------qrcode-----------" + qrcode);
            MallRedQrcode one = serviceImpl.getOne(new QueryWrapper<MallRedQrcode>().eq("code", qrcode));
            logger.info("后台管理系统升级创客-------查询二维码-----------" + one);


            Map<String, String> stringStringMap = mallSystemConfigService.queryMaker();

            String number = "";
            if (null != one) {
                number = one.getCode();
            } else {
                MallRedQrcode mallRedQrcode = new MallRedQrcode();
                mallRedQrcode.setCode(qrcode);
                mallRedQrcode.setUsersId(mallUser2.getId());
                mallRedQrcode.setName("后台管理系统升级创客");
                mallRedQrcode.setTotalNumber(900000000); // 总次数（这个码总共能扫多少次） 功能做好后搞成前端变量转进来，不能写死
                mallRedQrcode.setSurplusNumber(900000000); // 剩余次数（这个码剩余能扫多少次） 功能做好后搞成前端变量转进来，不能写死
                mallRedQrcode.setStatus("0"); // 红包状态（0，有效，1 无效，2,禁用, 3 启用）
                mallRedQrcode.setUserNumber(5); // 个人领取记录 功能做好后搞成前端变量转进来，不能写死
                String mallMakerPacket1 = stringStringMap.get("mall_maker_packet1");// 区间1
                String mallMakerPacket2 = stringStringMap.get("mall_maker_packet2");// 区间2
                String mallMakerBl = stringStringMap.get("mall_maker_bl");
                String qJ = mallMakerPacket1 + "," + mallMakerPacket2;// 区间
                mallRedQrcode.setOneNum(qJ); // 第一次区间 功能做好后搞成前端变量转进来，不能写死
                mallRedQrcode.setTwoNum(qJ);
                mallRedQrcode.setThreeNum(qJ);
                mallRedQrcode.setFourNum(qJ);
                mallRedQrcode.setFiveNum(qJ);
                mallRedQrcode.setSixNum(qJ);
                mallRedQrcode.setSevenNum(mallMakerBl); // 创客所得比例 功能做好后搞成前端变量转进来，不能写死
                mallRedQrcode.setCreateTime(LocalDateTime.now());
                mallRedQrcode.setIsPrinted("0"); // 是否已印刷 默认0-否 1-是
                mallRedQrcode.setIsBind("0"); // 是否已绑定 默认0-否 1-是
                mallRedQrcode.setType("percent"); // 默认 0-创客使用
                mallRedQrcode.setEndTime(LocalDateTime.of(2100, 01, 01, 23, 59, 59));

                // mallRedQrcode.setRemark("商"); // 二维码备注
                serviceImpl.save(mallRedQrcode);
                // QueryWrapper queryWrapper1 = new QueryWrapper();
                // queryWrapper1.eq("mall_red_qrcode.code",qrcode);
                MallRedQrcode one2 = serviceImpl.getOne(new QueryWrapper<MallRedQrcode>().eq("code", qrcode));
                logger.info("后台管理系统升级创客-------二维码-----------" + one2);
                number = one2.getCode();
            }
            QrConfig qrConfig = new QrConfig();
            qrConfig.setWidth(640); // 设置二维码宽度
            qrConfig.setHeight(640);// 设置二维码高度
            qrConfig.setMargin(1);// 设置边距
            File generate = QrCodeUtil.generate(requestUrl + "/wx/gamejackpot/qrCode?makerCode=" + qrcode, qrConfig,
                            new File("/home/gxkj/redCode/" + qrcode + ".png"));// 生成二维码存放地址
            /**
             * @param srcImageFile 源图像文件
             * @param destImageFile 目标图像文件
             * @param pressImg 水印图片
             * @param x 修正值。 默认在中间，偏移量相对于中间偏移
             * @param y 修正值。 默认在中间，偏移量相对于中间偏移
             * @param alpha 透明度：alpha 必须是范围 [0.0, 1.0] 之内（包含边界值）的一个浮点数字
             */
            ImgUtil.pressImage(FileUtil.file("/home/gxkj/redCode/red.png"), // 源图像文件
                            // FileUtil.file("C:\\Users\\Administrator.SC-202011302240\\Desktop\\红包码\\red.png"),//源图像文件
                            FileUtil.file("/home/gxkj/redCode1/" + number + "." + qrcode + ".png"), // 目标图像文件，保存生成图片地址
                            ImgUtil.read(generate), -8, // x坐标修正值。 默认在中间，偏移量相对于中间偏移
                            30, // y坐标修正值。 默认在中间，偏移量相对于中间偏移
                            1f);

            String saveUrl = "/home/gxkj/redCode2/" + number + "." + qrcode + ".png";
            ImgUtil.pressText(FileUtil.file("/home/gxkj/redCode1/" + number + "." + qrcode + ".png"), /// 源图像文件，就是需要添加文字的源文件
                            FileUtil.file(saveUrl), // 目标图像文件，就是生成添加文字存放地址
                            qrcode, Color.BLACK, // 文字
                            new Font("黑体", Font.BOLD, 38), // 字体
                            -5, // x坐标修正值。 默认在中间，偏移量相对于中间偏移
                            383, // y坐标修正值。 默认在中间，偏移量相对于中间偏移
                            1f);// 透明度：alpha 必须是范围 [0.0, 1.0] 之内（包含边界值）的一个浮点数字


            File file = new File(saveUrl);
            InputStream inputStream = new FileInputStream(file);
            MultipartFile multipartFile = new MockMultipartFile(file.getName(), inputStream);

            String originalFilename = number + "." + qrcode + ".png";
            MallStorage MallStorage = storageService.store(multipartFile.getInputStream(), multipartFile.getSize(),
                            multipartFile.getContentType(), originalFilename);
            qrCodeUrl = MallStorage.getUrl();

        }



        /** 购买成功给用户赋予创客权限 **/
        MallUser updateUser = new MallUser();
        updateUser.setId(mallUser2.getId());
        updateUser.setIsMaker("1");
        updateUser.setSource("后台管理系统升级创客");
        updateUser.setIsActivate((byte) 1);
        if ("0".equals(mallUser2.getLevelId())) {
            updateUser.setLevelId("1");
        }
        updateUser.setSessionKey(qrCodeUrl);

        // 用户升级创客送一个月达人
        Map<String, String> stringStringMap = mallSystemConfigService.queryTalent();
        /** 是否送达人 1 送 其它不送 */
        if (stringStringMap.get("mall_talent_is_on").equals("1")) {
            /** 购买创客默认送达人月卡 **/
            Map<String, Object> dataMap = marketingSignRecordService.queryUserTalentCard(mallUser2.getId());
            MallUserCard mallUserCard = new MallUserCard();
            // 有卡 就续期
            if (null != dataMap) {
                long end = ((Timestamp) dataMap.get("endTime")).getTime();
                LocalDateTime endTime = Instant.ofEpochMilli(end).atZone(ZoneOffset.ofHours(8)).toLocalDateTime();
                LocalDateTime now = LocalDateTime.now();
                String cid = (String) dataMap.get("id");
                mallUserCard.setId(cid);
                // 月卡
                /** 已过期 **/
                if (endTime.isBefore(now)) {
                    // 月卡
                    mallUserCard.setEndTime(now.plusMonths(1L));
                } else {
                    // 月卡
                    mallUserCard.setEndTime(endTime.plusMonths(1L));
                }
                // 更新卡的信息
                mallUserCardService.updateMallUserCard(mallUserCard);
            }
            // 没有卡
            else {
                // 月卡时间 开始时间 mstart - 结束时间 mend
                LocalDateTime mstart = LocalDateTime.now();
                LocalDateTime mend = LocalDateTime.now().plus(1, ChronoUnit.MONTHS);
                mallUserCard.setId(GraphaiIdGenerator.nextId("mallCard"));
                mallUserCard.setIsActivate((byte) 1);
                mallUserCard.setPhone(mallUser2.getMobile());
                mallUserCard.setUserId(mallUser2.getId());
                mallUserCard.setCardType((byte) 2);
                // 月卡
                mallUserCard.setStartTime(mstart);
                mallUserCard.setEndTime(mend);

                // 插入卡记录
                mallUserCardService.insertMallUserCard(mallUserCard);
            }
            /** 激活达人身份 **/
            updateUser.setIsTalent("1");

        }


        // 修改用户信息，用户已经购购买创客身份
        if (mallUserService.updateById(updateUser) > 0) {

            // 记录用户升级创客
            MallUserLevelChange mallUserLevelChange = new MallUserLevelChange();
            mallUserLevelChange.setUserId(updateUser.getId());
            mallUserLevelChange.setNickname(updateUser.getNickname());
            mallUserLevelChange.setMobile(updateUser.getMobile());
            mallUserLevelChange.setOldLevelId(updateUser.getLevelId());
            mallUserLevelChange.setLevelId("9");// 创客标识不是具体等级
            mallUserLevelChange.setOldDirectLeader(
                            null == updateUser.getDirectLeader() ? "" : updateUser.getDirectLeader());
            mallUserLevelChange
                            .setOldFirstLeader(null == updateUser.getFirstLeader() ? "" : updateUser.getFirstLeader());
            mallUserLevelChange.setOldSecondLeader(
                            null == updateUser.getSecondLeader() ? "" : updateUser.getSecondLeader());
            mallUserLevelChange
                            .setOldThirdLeader(null == updateUser.getThirdLeader() ? "" : updateUser.getThirdLeader());
            mallUserLevelChange
                            .setDirectLeader(null == updateUser.getDirectLeader() ? "" : updateUser.getDirectLeader());
            mallUserLevelChange.setFirstLeader(null == updateUser.getFirstLeader() ? "" : updateUser.getFirstLeader());
            mallUserLevelChange
                            .setSecondLeader(null == updateUser.getSecondLeader() ? "" : updateUser.getSecondLeader());
            mallUserLevelChange.setThirdLeader(null == updateUser.getThirdLeader() ? "" : updateUser.getThirdLeader());
            mallUserLevelChange.setChangeApi("/createMaker : 管理后台升级创客----levelId:9 为创客标识用，不作具体等级 ");
            mallUserLevelChangeService.insertMallUserLevelChange(mallUserLevelChange);

            // todo 判断升级用户推荐人达标3位创客
            doPayService.doUserEligible(updateUser.getDirectLeader());

            // TODO 同步用户信息给支付系统
            Map map = new HashMap<>();
            String url = wlsqUrl + "/api/wlsh/addUserInfo";
            logger.info("后台管理系统升级创客-------userId-----------" + mallUser2.getId());
            MallUser user = mallUserService.findById(mallUser2.getId());
            logger.info("后台管理系统升级创客-------user-----------" + user);
            // name str 名称
            if (StringUtils.isBlank(user.getNickname())) {
                map.put("name", user.getMobile());
            } else {
                map.put("name", user.getNickname());
            }
            // phone str手机号
            map.put("phone", user.getMobile());
            // address str 地区

            map.put("address", user.getProvince() + user.getCity() + user.getCounty());
            // userType str 用户类型 7
            if ("1".equals(user.getIsMaker())) {
                map.put("userType", "7");
            }
            // 推荐人
            if (StringUtils.isNotBlank(user.getDirectLeader())) {
                MallUser mallUserD = userService.findById(user.getDirectLeader());
                // referrerPhone
                if (null != mallUserD) {
                    map.put("referrerPhone", mallUserD.getMobile());
                }
            }



            try {
                String s = HttpUtil.sendPostJson(url, JSONUtil.toJsonStr(map));
                logger.info("后台创客身份同步支付系统------------------" + s);
            } catch (Exception e) {
                e.printStackTrace();
                logger.info("后台升级创客同步用户信息给支付系统失败：" + e.getMessage(), e);
            }

            return ResponseUtil.ok();
        } else {
            return ResponseUtil.fail(403, "升级失败！");
        }

    }

    // 审核和打款要验证操作密码
    private Object validate(String operationPWd) {
        // 获取当前用户
        MallAdmin adminUser = (MallAdmin) SecurityUtils.getSubject().getPrincipal();
        // 防止用户刚设置操作密码，没有重新登录，获取不到新的操作密码，所以再去数据库查询一次，确保能获取
        MallAdmin user = mallAdminService.findOperationPwdById(adminUser.getId());

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        if (!encoder.matches(operationPWd, user.getOperationPassword())) {
            return ResponseUtil.fail(ADMIN_INVALID_ACCOUNT, "操作密码有误");
        }
        return null;
    }



}
