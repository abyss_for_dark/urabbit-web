package com.graphai.mall.admin.dto;

import com.graphai.mall.db.domain.WangzhuanTask;
import com.graphai.mall.db.domain.WangzhuanTaskStep;
import com.graphai.mall.db.domain.WangzhuanTaskStepWithBLOBs;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class WangzhuanTaskStepDto {

    //步骤类型
    private String type;
    //步骤说明
    private String remark;
    //图片URL
    private Map<String,String>[] imageUrls;
    //网址URL
    private String url;
    //数据
    private String data;
}
