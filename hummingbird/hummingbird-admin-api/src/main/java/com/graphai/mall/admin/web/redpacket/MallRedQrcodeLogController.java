package com.graphai.mall.admin.web.redpacket;

import org.springframework.beans.factory.annotation.Autowired;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.baomidou.mybatisplus.core.metadata.IPage;

import org.springframework.web.bind.annotation.RestController;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.framework.generator.controller.IBaseController;

import com.graphai.mall.admin.domain.MallRedQrcodeLog;
import com.graphai.mall.admin.service.IMallRedQrcodeLogService;

import com.baomidou.mybatisplus.extension.service.IService;


/**
 * <p>
 * 红包码日志表 前端控制器
 * </p>
 *
 * @author biteam
 * @since 2020-12-10
 */
@RestController
@RequestMapping("/admin/log")
public class MallRedQrcodeLogController extends IBaseController<MallRedQrcodeLog,  String >  {

	@Autowired
    private IMallRedQrcodeLogService serviceImpl;
    
    @RequiresPermissions("admin:log:single")
    @RequiresPermissionsDesc(menu = {"单独查询"}, button = "单独查询")
	@GetMapping("/single/{id}")
    public Object getEntityById(@PathVariable  String  id) throws Exception{
        MallRedQrcodeLog entityVName = serviceImpl.getById(id);
        return ResponseUtil.ok(entityVName);
    }
    
    @RequiresPermissions("admin:log:addOrUpdate")
    @RequiresPermissionsDesc(menu = {"新增"}, button = "新增")
    @PostMapping("/addOrUpdate")
    public Object addOrUpdate(MallRedQrcodeLog mallRedQrcodeLog) throws Exception{
        if(serviceImpl.saveOrUpdate(mallRedQrcodeLog)){
            return ResponseUtil.ok();
        }else{
            return ResponseUtil.fail();
        }
    } 

	@RequiresPermissions("admin:log:melist")
	@RequiresPermissionsDesc(menu = {"红包码日志表查询"}, button = "查询")
    @GetMapping("/melist")
    public Object getMeList() throws Exception {
        IPage<MallRedQrcodeLog> ipage = serviceImpl.page(getPlusPage());
        return ResponseUtil.ok(ipage);
    }
 	
 	@RequiresPermissions("admin:log:save")
 	@RequiresPermissionsDesc(menu = {"红包码日志表新增"}, button = "新增")
    @PostMapping("/save")
    public Object save(@ModelAttribute MallRedQrcodeLog mallRedQrcodeLog) throws Exception {
        serviceImpl.save(mallRedQrcodeLog);
        return ResponseUtil.ok();
    }
	
	@RequiresPermissions("admin:log:modify")
	@RequiresPermissionsDesc(menu = {"红包码日志表修改"}, button = "修改")
    @PutMapping("/modify")
    public Object modify(@ModelAttribute  MallRedQrcodeLog mallRedQrcodeLog){
        serviceImpl.updateById(mallRedQrcodeLog);
        return  ResponseUtil.ok();
    }
    
    @RequiresPermissions("admin:log:del")
    @RequiresPermissionsDesc(menu = {"红包码日志表删除"}, button = "删除")
    @DeleteMapping("/del/{id}")
    public Object deleteById(@PathVariable  String  id){
        serviceImpl.removeById(id);
        return  ResponseUtil.ok();
    }
    
}

