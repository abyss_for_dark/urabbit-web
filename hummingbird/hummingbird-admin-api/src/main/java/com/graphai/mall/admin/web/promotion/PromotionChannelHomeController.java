package com.graphai.mall.admin.web.promotion;


import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.mall.db.domain.MallAdmin;
import com.graphai.mall.db.service.promotion.PromotionChannelService;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 推广渠道后台首页
 *
 * @author LaoGF
 * @since 2020-07-13
 */
@RestController
@RequestMapping("/admin/promotionChannelHome")
@Validated
public class PromotionChannelHomeController {

    @Resource
    private PromotionChannelService promotionChannelService;

    /**
     * 统计用户量(同渠道)
     */
    @RequiresPermissions("admin:promotionChannelHome:statistics")
    @RequiresPermissionsDesc(menu = {"推广渠道后台", "推广渠道首页"}, button = "首页统计")
    @GetMapping("/statistics")
    public Object statistics(@RequestParam(value = "day", required = false) Integer day) {
        //获取当前用户
        MallAdmin adminUser = (MallAdmin) SecurityUtils.getSubject().getPrincipal();
        return ResponseUtil.ok(promotionChannelService.statistics(adminUser.getId(), day));
    }

    /**
     * 统计注册用户量、Android、IOS激活用户量、uv用户量(同渠道)
     */
    @RequiresPermissions("admin:promotionChannelHome:statisticsGroupByDay")
    @RequiresPermissionsDesc(menu = {"推广渠道后台", "推广渠道首页"}, button = "统计趋势")
    @GetMapping("/statisticsGroupByDay")
    public Object statisticsGroupByDay(@RequestParam(value = "day", required = false) Integer day) {
        //获取当前用户
        MallAdmin adminUser = (MallAdmin) SecurityUtils.getSubject().getPrincipal();
        return ResponseUtil.ok(promotionChannelService.statisticsUserCountGroupByDay(adminUser.getId(), day));
    }

}
