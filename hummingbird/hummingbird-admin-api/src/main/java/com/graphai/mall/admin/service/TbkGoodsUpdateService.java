package com.graphai.mall.admin.service;

import com.alibaba.druid.support.json.JSONUtils;
import com.graphai.commons.util.httprequest.HttpUtils;
import com.graphai.mall.db.config.TbkapiProperties;
import com.graphai.mall.db.domain.MallCoupon;
import com.graphai.mall.db.domain.MallGoods;
import com.graphai.mall.db.service.coupon.MallCouponService;
import com.graphai.mall.db.service.goods.MallGoodsService;

import jodd.util.StringUtil;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TbkGoodsUpdateService {

    private final Log logger = LogFactory.getLog(TbkGoodsUpdateService.class);

    @Autowired
    private MallGoodsService goodsService;

    @Autowired
    private MallCouponService MallCouponService;

    @Autowired
    private TbkapiProperties properties;

    public void syscTbkGoodsUpdate(int minId) {
        Map<String, String> paramsMap = new HashMap<String, String>();
        int pageSize = 10000;

        String url = "http://v2.api.haodanku.com/update_item/apikey/" + properties.getApiKey()
                + "/sort/1/back/" + pageSize + "/min_id/" + minId;

        String retVal = HttpUtils.get(url, paramsMap, null);
//    	logger.info("retVal:"+retVal);

        if (retVal != null) {
            Map<String, Object> retMap = (Map<String, Object>) JSONUtils.parse(retVal);
            if (retMap != null) {
                if (1 == MapUtils.getIntValue(retMap, "code", 0)) {
                    int listSize = 0;
                    List<Map<String, Object>> goodsList = (List) retMap.get("data");
                    if (goodsList != null) {
                        listSize = goodsList.size();
                        logger.error("goodsList size:" + goodsList.size());
                        logger.error("minId:" + minId);
                        for (Map<String, Object> goodsMap : goodsList) {
                            try {
                                this.syscGoodsUpdate(goodsMap);
                            } catch (Exception e) {
                                logger.error("", e);
                            }
                        }
                    }
                    if (minId != MapUtils.getIntValue(retMap, "min_id")
                            && listSize == pageSize
                            && 0 != MapUtils.getIntValue(retMap, "min_id")) {
                        this.syscTbkGoodsUpdate(MapUtils.getIntValue(retMap, "min_id"));
                    }
                }
            }
        }
    }

    public void syscGoodsUpdate(Map<String, Object> goodsMap)
            throws Exception {

        String itemid = MapUtils.getString(goodsMap, "itemid");
        MallGoods goodDto = new MallGoods();
        goodDto.setSrcGoodId(itemid);
        goodDto.setDeleted(false);
        goodDto.setChannelCode(properties.getChannelCode());

        MallGoods oldGoodsDto = goodsService.selectOneBySrcGoodIdAndChannelCode(goodDto);
        if (oldGoodsDto != null && oldGoodsDto.getIsOnSale()) {
//			logger.info("oldGoodsDto："+JacksonUtils.bean2Jsn(oldGoodsDto)); //pxhtest

            oldGoodsDto.setTodaysale(MapUtils.getInteger(goodsMap, "todaysale"));
            oldGoodsDto.setItemsale(MapUtils.getInteger(goodsMap, "itemsale"));
            oldGoodsDto.setItemsale2(MapUtils.getInteger(goodsMap, "itemsale2"));
            oldGoodsDto.setGeneralIndex(MapUtils.getInteger(goodsMap, "general_index"));

            if (StringUtil.isNotBlank(MapUtils.getString(goodsMap, "itemendprice"))) {
                goodDto.setRetailPrice(new BigDecimal(MapUtils.getString(goodsMap, "itemendprice")));
                if (goodDto.getRetailPrice().compareTo(oldGoodsDto.getRetailPrice()) != 0) {
                    //价格不相等,直接下架
                    oldGoodsDto.setIsOnSale(false);
                    goodsService.updateById(oldGoodsDto);
                } else {
                    MallCoupon coupon = new MallCoupon();
                    coupon.setChannelCode(properties.getChannelCode());
                    coupon.setGoodsValue(new String[]{oldGoodsDto.getId()});
                    MallCoupon oldCoupon =
                            MallCouponService.selectOneByGoodsValue(coupon);
                    if (oldCoupon != null) {
                        coupon.setReceive(MapUtils.getInteger(goodsMap, "couponreceive"));
                        coupon.setSurplus(MapUtils.getInteger(goodsMap, "couponsurplus"));
                        coupon.setId(oldCoupon.getId());

                        goodsService.updateById(oldGoodsDto);
                        MallCouponService.updateById(coupon);
                    }
                }
            } else {
                //没有取到价格,直接下架
                oldGoodsDto.setIsOnSale(false);
                goodsService.updateById(oldGoodsDto);
            }

        }
    }

}
