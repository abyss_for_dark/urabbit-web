package com.graphai.mall.admin.web;

import java.util.Map;

import javax.validation.constraints.NotNull;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.graphai.commons.constant.FrameworkConstants;
import com.graphai.commons.util.RegexUtil;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.mall.db.domain.MallUser;
import com.graphai.mall.db.service.MallAgentService;
import com.graphai.mall.db.service.user.MallUserService;

@RestController
@RequestMapping("/admin/agent")
@Validated
public class AdminAgentController {

    @Autowired
    private MallUserService userService;
    @Autowired
    private MallAgentService mallAgentService;

    @RequiresPermissions("admin:agent:list")
    @RequiresPermissionsDesc(menu = {"用户管理", "代理管理"}, button = "查询")
    @GetMapping("/list")
    public Object list(@RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "20") Integer limit,
                       @NotNull String levelId, String firstMobile,
                       String keyword, String region, String beginTime, String endTime,
                       @RequestParam(required = false) String customerId,
                       @RequestParam(required = false) String level) {
        
        
        Map<String,Object> mp = mallAgentService.querySelective(customerId, level, firstMobile, keyword, region, beginTime, endTime, levelId, page, limit);
        /**独家代理模式**/
        if("single".equals(FrameworkConstants.PROXY_PATTERN)) {
            
            
        }else {
            
        }
        return ResponseUtil.ok(mp);
    }

    /**
     * 修改代理人信息
     *
     * @param mallUser
     * @return
     */
    @RequiresPermissions("admin:agent:update")
    @RequiresPermissionsDesc(menu = {"用户管理", "代理管理"}, button = "修改")
    @PostMapping("/update")
    public Object update(@RequestBody MallUser mallUser) {
        userService.updateById(mallUser);
        return ResponseUtil.ok();
    }

    /**
     * 新增代理人
     *
     * @param mallUser
     * @return
     */
    @RequiresPermissions("admin:agent:add")
    @RequiresPermissionsDesc(menu = {"用户管理", "代理管理"}, button = "添加")
    @PostMapping("/add")
    public Object add(@RequestBody MallUser mallUser) throws Exception {
        Object error = validate(mallUser);
        if (error != null) {
            return error;
        }
        return mallAgentService.createAgent(mallUser);
    }


    @GetMapping("/queryMobile")
    public Object queryMobile(@RequestParam String mobile) {
        return mallAgentService.checkMobile(mobile);
    }

    /**
     * 删除代理人
     *
     * @param id
     * @return
     */
    @RequiresPermissions("admin:agent:delete")
    @RequiresPermissionsDesc(menu = {"用户管理", "代理管理"}, button = "删除")
    @PostMapping("/delete")
    public Object delete(@RequestParam String id) {
        MallUser user = new MallUser();
        user.setDeleted(true);
        user.setId(id);
        userService.updateById(user);
        return ResponseUtil.ok();
    }

    /**
     * 获取设备管理 左侧的树列表
     * @param agentId
     * @return
     */
    @GetMapping("agentListTree")
    public Object agentListTree(@RequestParam(required = false) String agentId){
        return mallAgentService.getAgentListTree(agentId);
    }

    /**
     * 校验参数
     */
    private Object validate(MallUser user) {
        if (StringUtils.isEmpty(user.getLevelId())) {
            return ResponseUtil.badArgumentValue("请选择等级！");
        }
        if (StringUtils.isEmpty(user.getNickname())) {
            return ResponseUtil.badArgumentValue("请填写姓名！");
        }
        if (StringUtils.isEmpty(user.getMobile())) {
            return ResponseUtil.badArgumentValue("请填写手机号码！");
        }
        if (!RegexUtil.isMobileExact(user.getMobile())) {
            return ResponseUtil.badArgumentValue("请填写正确的手机号码！");
        }
//        if (StringUtils.isEmpty(user.getAvatar())) {
//            return ResponseUtil.badArgumentValue("请上传头像！");
//        }
        if (StringUtils.isEmpty(user.getProvince())) {
            return ResponseUtil.badArgumentValue("请选择省！");
        }
        if (StringUtils.isEmpty(user.getCity())) {
            return ResponseUtil.badArgumentValue("请选择市！");
        }
        if (StringUtils.isEmpty(user.getCounty())) {
            return ResponseUtil.badArgumentValue("请选择区！");
        }
        if (user.getStatus() == null) {
            return ResponseUtil.badArgumentValue("请选择状态！");
        }
        return null;
    }

}
