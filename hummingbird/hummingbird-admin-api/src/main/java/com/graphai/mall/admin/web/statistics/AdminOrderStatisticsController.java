package com.graphai.mall.admin.web.statistics;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.mall.admin.service.AdminOrderService;
import com.graphai.mall.db.service.order.MallOrderStatisticsService;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;


/**
 * 订单统计
 *
 * @author LaoGF
 * @since 2020-07-02
 */
@RestController
@RequestMapping("/admin/orderStatistics")
@Validated
public class AdminOrderStatisticsController {

    @Resource
    private MallOrderStatisticsService mallOrderStatisticsService;

    @Autowired
    private AdminOrderService adminOrderService;

    /**
     * 统计订单所有
     */
    @RequiresPermissions("admin:orderStatistics:statistics")
    @RequiresPermissionsDesc(menu = {"数据统计", "订单统计"}, button = "查询")
    @ResponseBody
    @GetMapping("/statistics")
    public Object statistics(@RequestParam Integer day) {
        return ResponseUtil.ok(mallOrderStatisticsService.statistics(day == null ? 7 : day));
    }

    /**
     * 统计订单笔数、金额
     */
    @RequiresPermissions("admin:orderStatistics:statistics")
    @RequiresPermissionsDesc(menu = {"数据统计", "订单统计"}, button = "查询V2")
    @ResponseBody
    @GetMapping("/statisticsV2")
    public Object statistics(@RequestParam String param) {
        return ResponseUtil.ok(mallOrderStatisticsService.statisticsV2(param));
    }


    /**
     * 订单统计每日汇总、饼图、折线图
     */
    @ResponseBody
    @GetMapping("/statisticsOrderDetail")
    public Object statisticsOrderDetail(@RequestParam Integer day) {
        return ResponseUtil.ok(mallOrderStatisticsService.statisticsOrderDetail(day == null ? 7 : day));
    }

    /**
     * 统计收益趋势
     */
    @ResponseBody
    @GetMapping("/statisticsTrend")
    public Object statisticsTrend() {
        return ResponseUtil.ok(mallOrderStatisticsService.statisticsTrend());
    }


    /**
     * 后台总收益查询
     * @param date  时间筛选（今日，昨日，历史）
     * @return
     */
    @GetMapping("/totalEarning")
    public Object totalEarning(@RequestParam(defaultValue = "today") String  date) throws Exception {


        return adminOrderService.getTotalEarning(date);
    }
    /**
     * 后台交易统计
     * @param date  时间筛选（今日，昨日，近30天，历史）
     * @return
     */
    @GetMapping("/tradeStatistic")
    public Object tradeStatistic(@RequestParam(defaultValue = "today") String  date) {


        return adminOrderService.getTradeStatistic(date);
    }


    /**
     * 各类订单待处理事项
     * @param
     * @return
     */
    @GetMapping("/StatisticOrderStatus")
    public Object StatisticOrderStatus() {

        return adminOrderService.StatisticOrderStatus();
    }
}
