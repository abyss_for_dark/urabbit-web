package com.graphai.mall.admin.web.seckill;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.lang.StringUtils;
import com.graphai.commons.util.servlet.ServletUtils;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.framework.generator.controller.IBaseController;
import com.graphai.mall.admin.domain.MallSeckillTime;
import com.graphai.mall.admin.service.IMallSeckillTimeService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;



/**
 * <p>
 * 秒杀时间表  前端控制器
 * </p>
 *
 * @author biteam
 * @since 2021-07-13
 */
@RestController
@RequestMapping("/admin/admin/time")
public class MallSeckillTimeController extends IBaseController<MallSeckillTime,  String >  {

	@Autowired
    private IMallSeckillTimeService serviceImpl;
    
    @RequiresPermissions("admin:admin:time:single")
    @RequiresPermissionsDesc(menu = {"单独查询"}, button = "单独查询")
	@GetMapping("/single/{id}")
    public Object getEntityById(@PathVariable  String  id) throws Exception{
        MallSeckillTime entityVName = serviceImpl.getById(id);
        return ResponseUtil.ok(entityVName);
    }
    
    @RequiresPermissions("admin:admin:time:addOrUpdate")
    @RequiresPermissionsDesc(menu = {"新增"}, button = "新增")
    @PostMapping("/addOrUpdate")
    public Object addOrUpdate(MallSeckillTime mallSeckillTime) throws Exception{
        if(serviceImpl.saveOrUpdate(mallSeckillTime)){
            return ResponseUtil.ok();
        }else{
            return ResponseUtil.fail();
        }
    } 
	
	
	@RequiresPermissions("admin:admin:time:melist")
	@RequiresPermissionsDesc(menu = {"秒杀时间表 查询"}, button = "查询")
    @GetMapping("/melist")
    public Object getMeList() throws Exception {
        IPage<MallSeckillTime> ipage = serviceImpl.page(getPlusPage(),listQueryWrapper());
        return ResponseUtil.ok(ipage);
    }
 	
 	private QueryWrapper<MallSeckillTime> listQueryWrapper() {
 		QueryWrapper<MallSeckillTime> meq = new QueryWrapper<>();
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("seckillId"))) {
		      meq.eq("seckill_id", ServletUtils.getParameter("seckillId"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("seckillTime"))) {
		      meq.eq("seckill_time", ServletUtils.getLocalDateTime("seckillTime"));  
		}
		
        return meq;
    }
 	@RequiresPermissions("admin:admin:time:save")
 	@RequiresPermissionsDesc(menu = {"秒杀时间表 新增"}, button = "新增")
    @PostMapping("/save")
    public Object save(@ModelAttribute MallSeckillTime mallSeckillTime) throws Exception {
        serviceImpl.save(mallSeckillTime);
        return ResponseUtil.ok();
    }
	
	@RequiresPermissions("admin:admin:time:modify")
	@RequiresPermissionsDesc(menu = {"秒杀时间表 修改"}, button = "修改")
    @PutMapping("/modify")
    public Object modify(@ModelAttribute  MallSeckillTime mallSeckillTime){
        serviceImpl.updateById(mallSeckillTime);
        return  ResponseUtil.ok();
    }

    @RequiresPermissions("admin:admin:time:del")
    @RequiresPermissionsDesc(menu = {"秒杀时间表 删除"}, button = "删除")
    @DeleteMapping("/del/{id}")
    public Object deleteById(@PathVariable  String  id){
        serviceImpl.removeById(id);
        return  ResponseUtil.ok();
    }

}

