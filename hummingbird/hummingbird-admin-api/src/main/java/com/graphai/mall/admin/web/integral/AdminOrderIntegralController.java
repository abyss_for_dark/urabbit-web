package com.graphai.mall.admin.web.integral;

import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.mall.admin.service.AdminOrderIntegralService;
import com.graphai.mall.db.service.common.ShipService;
import com.graphai.validator.Order;
import com.graphai.validator.Sort;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 积分商城-订单管理-controller
 *
 * @author Qxian
 * @version V0.0.1
 * @date 2020-10-22
 */
@RestController
@RequestMapping("/admin/order/integral")
@Validated
public class AdminOrderIntegralController {
    private final Log logger = LogFactory.getLog(AdminOrderIntegralController.class);

    @Autowired
    private AdminOrderIntegralService adminOrderIntegralService;

    @Autowired
    private ShipService shipService;

    /**
     * 后台管理-积分订单列表查询
     *
     * @param userId
     * @param orderSn
     * @param goodsName
     * @param tpId
     * @param orderStatusArray
     * @param page
     * @param limit
     * @param sort
     * @param order
     * @return
     */
    @RequiresPermissions("admin:order:integral:list")
    @RequiresPermissionsDesc(menu = {"订单管理", "积分订单"}, button = "查询")
    @GetMapping("/list")
    public Object list(String userId, String orderSn, String goodsName, String tpId, String tpUserName,
                       @RequestParam(required = false) List<Short> orderStatusArray,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit,
                       @Sort @RequestParam(defaultValue = "id") String sort,
                       @Order @RequestParam(defaultValue = "desc") String order) {
        return adminOrderIntegralService.list(userId, orderSn,
                goodsName, tpId, tpUserName, orderStatusArray, page, limit, sort, order);
    }

    /**
     * 订单详情
     *
     * @param id
     * @return
     */
    @RequiresPermissions("admin:order:integral:read")
    @RequiresPermissionsDesc(menu = {"订单管理", "积分订单"}, button = "详情")
    @GetMapping("/detail")
    public Object detail(@NotNull String id) {
        return adminOrderIntegralService.detail(id);
    }

    /**
     * 订单退款
     *
     * @param body 订单信息，{ orderId：xxx }
     * @return 订单退款操作结果
     */
    @RequiresPermissions("admin:order:integral:refund")
    @RequiresPermissionsDesc(menu = {"订单管理", "积分订单"}, button = "订单退款")
    @PostMapping("/refund")
    public Object refund(@RequestBody String body) {
        return adminOrderIntegralService.refund(body);
    }

    /**
     * 发货
     *
     * @param body 订单信息，{ orderId：xxx, shipSn: xxx, shipChannel: xxx }
     * @return 订单操作结果
     */
    @RequiresPermissions("admin:order:integral:ship")
    @RequiresPermissionsDesc(menu = {"订单管理", "积分订单"}, button = "订单发货")
    @PostMapping("/ship")
    public Object ship(@RequestBody String body) {
        return adminOrderIntegralService.ship(body);
    }


    /**
     * 回复订单商品
     *
     * @param body 订单信息，{ orderId：xxx }
     * @return 订单操作结果
     */
    @RequiresPermissions("admin:order:integral:reply")
    @RequiresPermissionsDesc(menu = {"订单管理", "积分订单"}, button = "订单商品回复")
    @PostMapping("/reply")
    public Object reply(@RequestBody String body) {
        return adminOrderIntegralService.reply(body);
    }


    /**
     * 发货
     *
     * @return
     */
    @GetMapping("/shipCompanys")
    public Object getShipCompanys() {
        return shipService.getShipCompanys();
    }
}
