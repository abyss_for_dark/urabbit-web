package com.graphai.mall.admin.web.finance;

import com.alibaba.druid.sql.dialect.h2.visitor.H2ASTVisitor;
import com.aliyun.openservices.shade.org.apache.commons.lang3.time.DateFormatUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.lang.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.graphai.commons.util.servlet.ServletUtils;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.server.dto.Sys;
import org.springframework.beans.factory.annotation.Autowired;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.graphai.framework.generator.controller.IBaseController;

import com.graphai.mall.admin.domain.FcAccountPaymentBill;
import com.graphai.mall.admin.service.IFcAccountPaymentBillService;

import java.math.BigDecimal;
import java.util.*;


/**
 * <p>
 * 提现打款流水 前端控制器
 * </p>
 *
 * @author biteam
 * @since 2021-02-21
 */
@RestController
@RequestMapping("/admin/account/payment/bill")
public class FcAccountPaymentBillController extends IBaseController<FcAccountPaymentBill,  String >  {

	@Autowired
    private IFcAccountPaymentBillService serviceImpl;
    
    @RequiresPermissions("admin:bill:single")
    @RequiresPermissionsDesc(menu = {"单独查询"}, button = "单独查询")
	@GetMapping("/single/{id}")
    public Object getEntityById(@PathVariable  String  id) throws Exception{
        FcAccountPaymentBill entityVName = serviceImpl.getById(id);
        return ResponseUtil.ok(entityVName);
    }
    
    @RequiresPermissions("admin:bill:addOrUpdate")
    @RequiresPermissionsDesc(menu = {"新增"}, button = "新增")
    @PostMapping("/addOrUpdate")
    public Object addOrUpdate(FcAccountPaymentBill fcAccountPaymentBill) throws Exception{
        if(serviceImpl.saveOrUpdate(fcAccountPaymentBill)){
            return ResponseUtil.ok();
        }else{
            return ResponseUtil.fail();
        }
    } 
	
	
//	@RequiresPermissions("admin:bill:melist")
	@RequiresPermissionsDesc(menu = {"提现打款流水查询"}, button = "查询")
    @GetMapping("/melist")
    public Object getMeList() throws Exception {
        IPage<FcAccountPaymentBill> ipage = serviceImpl.page(getPlusPage(),listQueryWrapper());
        return ResponseUtil.ok(ipage);
    }




    @RequiresPermissions("admin:bill:list")
	@RequiresPermissionsDesc(menu = {"打款记录查询"}, button = "查询")
	@GetMapping("/list")
	public Object getList(@RequestParam(defaultValue = "1") Integer pageNum,
						  @RequestParam(defaultValue = "20") Integer pageSize,
						  String withdrawBillId,
						  String userName,
						  String mobile,
						  String auditUserName,
						  String beginTime,
						  String endTime) {

		Page<FcAccountPaymentBill> page = new Page<>(pageNum, pageSize);


		QueryWrapper<FcAccountPaymentBill> queryWrapper = new QueryWrapper<>();
		queryWrapper.like(StringUtils.isNotEmpty(withdrawBillId),"withdraw_bill_id",withdrawBillId);
		queryWrapper.like(StringUtils.isNotEmpty(mobile),"mobile",mobile);
		queryWrapper.like(StringUtils.isNotEmpty(auditUserName),"audit_user_name",auditUserName);
		queryWrapper.like(StringUtils.isNotEmpty(userName),"user_name",userName);

		if(StringUtils.isNotEmpty(beginTime)) {
			System.out.println(beginTime);
			queryWrapper.apply(StringUtils.isNotEmpty(beginTime), "UNIX_TIMESTAMP(create_date) >= UNIX_TIMESTAMP('" +beginTime  + "')");
		}
		if(StringUtils.isNotEmpty(endTime)) {
			System.out.println(endTime);
			queryWrapper.apply(StringUtils.isNotEmpty(endTime), "UNIX_TIMESTAMP(create_date) < UNIX_TIMESTAMP('" +endTime  + "')");
		}

		IPage<FcAccountPaymentBill> ipage = serviceImpl.page(page, queryWrapper);


		List<FcAccountPaymentBill> list = serviceImpl.list(queryWrapper);

		BigDecimal sum = new BigDecimal(0);
		for(FcAccountPaymentBill fcAccountPaymentBill : list){
			if(null != fcAccountPaymentBill.getAmt()) {
				sum = sum.add(fcAccountPaymentBill.getAmt());
			}
		}


		Map map = new HashMap();
		map.put("sum",sum);
		map.put("count",list.size());
		map.put("data",ipage);


		return ResponseUtil.ok(map);

	}

	@RequiresPermissions("admin:bill:list")
	@RequiresPermissionsDesc(menu = {"打款记录统计"}, button = "查询")
	@GetMapping("/title")
	public Object getTitle() {
		Map map = new HashMap();
		List<FcAccountPaymentBill> list = serviceImpl.list();
		BigDecimal sum = new BigDecimal(0);
		for(FcAccountPaymentBill fcAccountPaymentBill : list){
			if(null != fcAccountPaymentBill.getAmt()){
				sum = sum.add(fcAccountPaymentBill.getAmt());
			}
		}

		map.put("sum",sum);
		map.put("count",list.size());


		QueryWrapper<FcAccountPaymentBill> queryWrapper = new QueryWrapper<>();

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		Date beginDate = calendar.getTime();
		String beginStr = DateFormatUtils.format(beginDate, "yyyy-MM-dd HH:mm:ss");

		queryWrapper.apply( "UNIX_TIMESTAMP(create_date) >= UNIX_TIMESTAMP('" +beginStr  + "')");


		List<FcAccountPaymentBill> todayList = serviceImpl.list(queryWrapper);
		BigDecimal todaySum = new BigDecimal(0);
		for(FcAccountPaymentBill fcAccountPaymentBill : todayList){
			todaySum = todaySum.add(fcAccountPaymentBill.getAmt());
		}


		map.put("todaySum",todaySum);
		map.put("todayCount",todayList.size());

		return ResponseUtil.ok(map);
	}










 	
 	private QueryWrapper<FcAccountPaymentBill> listQueryWrapper() {
 		QueryWrapper<FcAccountPaymentBill> meq = new QueryWrapper<>();
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("accountId"))) {
		      meq.eq("account_id", ServletUtils.getParameter("accountId"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("customerId"))) {
		      meq.eq("customer_id", ServletUtils.getParameter("customerId"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("userName"))) {
		      meq.eq("user_name", ServletUtils.getParameter("userName"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("mobile"))) {
		      meq.eq("mobile", ServletUtils.getParameter("mobile"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("custName"))) {
		      meq.eq("cust_name", ServletUtils.getParameter("custName"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("amt"))) {
		      meq.eq("amt", ServletUtils.getParameter("amt"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("paymentNo"))) {
		      meq.eq("payment_no", ServletUtils.getParameter("paymentNo"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("paymentTime"))) {
		      meq.eq("payment_time", ServletUtils.getParameter("paymentTime"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("paymentAccountType"))) {
		      meq.eq("payment_account_type", ServletUtils.getParameter("paymentAccountType"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("withdrawBillId"))) {
		      meq.eq("withdraw_bill_id", ServletUtils.getParameter("withdrawBillId"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("status"))) {
		      meq.eq("status", ServletUtils.getParameter("status"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("createDate"))) {
		      meq.eq("create_date", ServletUtils.getLocalDateTime("createDate"));  
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("updateDate"))) {
		      meq.eq("update_date", ServletUtils.getLocalDateTime("updateDate"));  
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("note"))) {
		      meq.eq("note", ServletUtils.getParameter("note"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("checkStatus"))) {
		      meq.eq("check_status", ServletUtils.getParameter("checkStatus"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("checkDate"))) {
		      meq.eq("check_date", ServletUtils.getLocalDateTime("checkDate"));  
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("auditUserId"))) {
		      meq.eq("audit_user_id", ServletUtils.getParameter("auditUserId"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("auditUserName"))) {
		      meq.eq("audit_user_name", ServletUtils.getParameter("auditUserName"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("bankType"))) {
		      meq.eq("bank_type", ServletUtils.getParameter("bankType"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("idCard"))) {
		      meq.eq("id_card", ServletUtils.getParameter("idCard"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("valCode"))) {
		      meq.eq("val_code", ServletUtils.getParameter("valCode"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("bankCode"))) {
		      meq.eq("bank_code", ServletUtils.getParameter("bankCode"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("bankCardName"))) {
		      meq.eq("bank_card_name", ServletUtils.getParameter("bankCardName"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("bankCardNo"))) {
		      meq.eq("bank_card_no", ServletUtils.getParameter("bankCardNo"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("receiveBranchName"))) {
		      meq.eq("receive_branch_name", ServletUtils.getParameter("receiveBranchName"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("receiveBankCustname"))) {
		      meq.eq("receive_bank_custname", ServletUtils.getParameter("receiveBankCustname"));
		}
		
        return meq;
    }
 	@RequiresPermissions("admin:bill:save")
 	@RequiresPermissionsDesc(menu = {"提现打款流水新增"}, button = "新增")
    @PostMapping("/save")
    public Object save(@ModelAttribute FcAccountPaymentBill fcAccountPaymentBill) throws Exception {
        serviceImpl.save(fcAccountPaymentBill);
        return ResponseUtil.ok();
    }
	
	@RequiresPermissions("admin:bill:modify")
	@RequiresPermissionsDesc(menu = {"提现打款流水修改"}, button = "修改")
    @PutMapping("/modify")
    public Object modify(@ModelAttribute  FcAccountPaymentBill fcAccountPaymentBill){
        serviceImpl.updateById(fcAccountPaymentBill);
        return  ResponseUtil.ok();
    }
    
    @RequiresPermissions("admin:bill:del")
    @RequiresPermissionsDesc(menu = {"提现打款流水删除"}, button = "删除")
    @DeleteMapping("/del/{id}")
    public Object deleteById(@PathVariable  String  id){
        serviceImpl.removeById(id);
        return  ResponseUtil.ok();
    }
    
}

