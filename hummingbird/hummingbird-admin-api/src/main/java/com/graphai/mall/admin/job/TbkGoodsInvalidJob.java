package com.graphai.mall.admin.job;

import com.graphai.mall.admin.service.TbkGoodsInvalidService;
import com.graphai.mall.db.config.TbkapiProperties;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 失效商品列表API
 *
 * @author panxianhuan
 */
//@Component
public class TbkGoodsInvalidJob {

    private final Log logger = LogFactory.getLog(TbkGoodsInvalidJob.class);

    @Autowired
    private TbkGoodsInvalidService tbkGoodsInvalidService;

    @Autowired
    private TbkapiProperties properties;

    //每隔10分钟(10*60*1000ms)执行一次
    @Scheduled(fixedRate = 58 * 60 * 1000)
    public void syscTbkGoodsInvalid() {
        logger.error("TbkGoodsInvalidJob start!");
        int start = 1;
        int end = 16;

        Date now = new Date();
        SimpleDateFormat f = new SimpleDateFormat("HH");
        String hour = f.format(now);
        logger.info("hour:" + hour);
        if (Integer.valueOf(hour) == 0) {
            start = 0;
            end = 1;
        } else {
            start = Integer.valueOf(hour) - 1;
            end = Integer.valueOf(hour);
        }
        tbkGoodsInvalidService.syscTbkGoodsInvalid(start, end);
        logger.error("TbkGoodsInvalidJob end!");
    }

}

