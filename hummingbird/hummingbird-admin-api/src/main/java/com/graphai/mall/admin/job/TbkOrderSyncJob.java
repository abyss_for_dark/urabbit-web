package com.graphai.mall.admin.job;

import com.alibaba.druid.support.json.JSONUtils;
import com.graphai.commons.util.httprequest.HttpUtils;
import com.graphai.mall.admin.util.LocalDateTimeUtil;
import com.graphai.mall.db.config.TbkapiProperties;
import com.graphai.mall.db.dao.MallDistributionSettingsMapper;
import com.graphai.mall.db.domain.*;
import com.graphai.mall.db.service.goods.MallGoodsService;
import com.graphai.mall.db.service.order.MallOrderService;
import com.graphai.mall.db.service.user.MallUserService;

import jodd.util.StringUtil;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//@Component
public class TbkOrderSyncJob {

    private final Log logger = LogFactory.getLog(TbkOrderSyncJob.class);

    private final static Byte USER_LEVEL_MEMBER = 0; //会员

    private final static Byte USER_LEVEL_OPER = 2; //运营商

    @Autowired
    private MallGoodsService goodsService;

    @Autowired
    private MallOrderService MallOrderService;

    @Autowired
    private MallUserService mallUserService;

    @Autowired
    private MallDistributionSettingsMapper distSettingsMapper;

    @Autowired
    private TbkapiProperties properties;

    private final static String apiKey = "zRuJVVE2TadyVHXGCIFsngM7jucBXaTJ";

    //每隔10分钟(10*60*1000ms)执行一次
    @Scheduled(fixedRate = 2 * 60 * 60 * 1000)
    public void syscAd() {
        logger.error("TbkGoodsUpdateJob start!");
        this.syscTbkOrder();
        logger.error("TbkGoodsUpdateJob end!");
    }


    public void syscTbkOrder() {
        Map<String, String> paramsMap = new HashMap<String, String>();
        paramsMap.put("apikey", apiKey);
        paramsMap.put("start_time", "2019-08-11 19:00:00");
        paramsMap.put("end_time", "2019-08-11 21:00:00");

        String url = "http://api.tbk.dingdanxia.com/tbk/order_details";
        String retVal = HttpUtils.post(url, paramsMap, null);

        logger.info("retVal:" + retVal);

        if (retVal != null) {
            Map<String, Object> retMap = (Map<String, Object>) JSONUtils.parse(retVal);
//    		System.out.println(retMap);
            if (retMap != null && 200 == MapUtils.getIntValue(retMap, "code", 0)) {
                List<Map<String, Object>> orderList = (List) retMap.get("data");
                if (orderList != null) {
                    for (Map<String, Object> orderMap : orderList) {
                        try {
                            this.syncOrder(orderMap);
                        } catch (Exception e) {
                            logger.error("", e);
                        }

                    }
                }

            }
        }
    }

    private void syncOrder(Map<String, Object> orderMap)
            throws Exception {
        logger.info("orderMap:" + orderMap);
        String orderSn = MapUtils.getString(orderMap, "trade_id");
        MallOrder oldOrder = MallOrderService.findBySn(orderSn);
        if (oldOrder != null) {
            //已经存在,不再计算
            return;
        }

        MallOrder order = new MallOrder();
        order.setUserId("-3"); //pxhtest
        order.setChannelCode("TBK");
        order.setDeleted(false);

        order.setOrderSn(orderSn);

        order.setSrcOrderIds(MapUtils.getString(orderMap, "trade_id"));
//    	已付款：指订单已付款，但还未确认收货 已收货：指订单已确认收货，但商家佣金未支付
//    	已结算：指订单已确认收货，且商家佣金已支付成功
//    	已失效：指订单关闭/订单佣金小于0.01元，订单关闭主要有：1）买家超时未付款； 2）买家付款前，买家/卖家取消了订单；3）订单付款后发起售中退款成功；
//    	tk_status字典值:3：订单结算，
//    	12：订单付款， 
//    	13：订单失效，
//    	14：订单成功
        // 603 612 613 614
        short srcOrderStatus = MapUtils.getShort(orderMap, "tk_status");
        if (srcOrderStatus == 12) {
            order.setOrderStatus((short) (201));
        } else if (srcOrderStatus == 13) {
            order.setOrderStatus((short) (103));
        } else if (srcOrderStatus == 14) {
            order.setOrderStatus((short) (614));
        } else if (srcOrderStatus == 3) {
            order.setOrderStatus((short) (603));
        }

        BigDecimal orderPrice = new BigDecimal(MapUtils.getString(orderMap, "alipay_total_price"));
        order.setOrderPrice(orderPrice);
        order.setGoodsPrice(orderPrice);
        order.setActualPrice(orderPrice);

        LocalDateTime payTime = LocalDateTimeUtil.getDateStrToLocalDateTime(
                MapUtils.getString(orderMap, "tk_paid_time"));
        order.setPayTime(payTime);
        order.setSource(MapUtils.getString(orderMap, "order_type"));

        LocalDateTime addTime = LocalDateTimeUtil.getDateStrToLocalDateTime(
                MapUtils.getString(orderMap, "tk_create_time"));
        order.setAddTime(addTime);
        order.setUpdateTime(payTime);

        if (StringUtil.isNotBlank(MapUtils.getString(orderMap, "tk_earning_time"))) {
            LocalDateTime endTime = LocalDateTimeUtil.getDateStrToLocalDateTime(
                    MapUtils.getString(orderMap, "tk_earning_time"));
            order.setConfirmTime(endTime);
        }

        String pubId = MapUtils.getString(orderMap, "pub_id");
        String siteId = MapUtils.getString(orderMap, "site_id");
        String adzoneId = MapUtils.getString(orderMap, "adzone_id");
        order.setTbkPubId(pubId);
        order.setTbkSiteId(siteId);
        order.setTbkSiteName(MapUtils.getString(orderMap, "site_name"));
        order.setTbkAdzoneId(adzoneId);
        order.setTbkAdzoneName(MapUtils.getString(orderMap, "adzone_name"));
        order.setTbkSpecialId(MapUtils.getString(orderMap, "special_id"));
        order.setTbkRelationId(MapUtils.getString(orderMap, "relation_id"));
        order.setTkTotalRate(MapUtils.getString(orderMap, "tk_total_rate"));
        order.setPubSharePreFee(MapUtils.getString(orderMap, "pub_share_pre_fee"));
        order.setPubShareRate(MapUtils.getString(orderMap, "pub_share_rate"));
        order.setTotalCommissionFee(MapUtils.getString(orderMap, "total_commission_fee"));
        order.setTotalCommissionRate(MapUtils.getString(orderMap, "total_commission_rate"));

        //根据渠道ID来获取销售人员ID
        String relationId = MapUtils.getString(orderMap, "relation_id");
        MallUser MallUser = null;
        String tbkPubId = "mm_" + pubId + "_" + siteId + "_" + adzoneId;
        if (relationId != null) {
            MallUser = mallUserService.queryByTbkRelationId(relationId);
        }
        if (MallUser == null) {
            MallUser = mallUserService.queryByTbkPubId(tbkPubId);
        }
        if (MallUser != null) {
            order.setTpid("" + MallUser.getId());
            order.setTpUserName(MallUser.getNickname());
        }

        BigDecimal sharePreFee = new BigDecimal(MapUtils.getString(orderMap, "pub_share_pre_fee"));//平台总分润
        //计算一二级分润
        BigDecimal primaryDividend = null;
        BigDecimal secondaryDividend = new BigDecimal(0);
        BigDecimal thirdDividend = new BigDecimal(0);
        String thirdLeaderUserId = "-1";

        //计算一级分润
        String disUserType = MallUser.getUserLevel().toString();
        String disProLevel = "D00";
        MallDistributionSettings distSettings =
                this.selectDistributionSettings(disUserType, disProLevel);
        primaryDividend = sharePreFee.
                multiply(new BigDecimal(distSettings.getDisIntegralValue()))
                .divide(new BigDecimal(100)).setScale(2, BigDecimal.ROUND_DOWN);
        //直接分润
        //1、运营商 60% 2、会员 40%

        if (MallUser.getFirstLeader() != null) {
            //有上级存在
            MallUser parentUser =
                    mallUserService.findById(MallUser.getFirstLeader());

            if (MallUser.getUserLevel() == USER_LEVEL_MEMBER) {
                //普通会员

                //计算二级分润
                if (parentUser.getUserLevel() == USER_LEVEL_MEMBER) {
                    //上级是普通会员
                    secondaryDividend = this.getDistCommission(sharePreFee, USER_LEVEL_MEMBER, "D10");

                    //计算三级分润
                    //判断上上级的身份
                    if (MallUser.getSecondLeader() != null) {
                        MallUser grandParentUser =
                                mallUserService.findById(MallUser.getSecondLeader());
                        if (grandParentUser != null) {
                            if (grandParentUser.getUserLevel() == USER_LEVEL_OPER) {
                                //上上级是运营商
                                thirdDividend = this.getDistCommission(sharePreFee, USER_LEVEL_OPER, "D20");
                                thirdLeaderUserId = grandParentUser.getId();
                            } else if (grandParentUser.getUserLevel() == USER_LEVEL_MEMBER) {
                                //上上级是普通会员
                                //三级都是普通会员
                                //判断上上级的上级是运营商,如果是则需要结算
                                if (grandParentUser.getFirstLeader() != null) {
                                    MallUser parentGrandParentUser =
                                            mallUserService.findById(grandParentUser.getFirstLeader());
                                    if (parentGrandParentUser != null &&
                                            parentGrandParentUser.getUserLevel() == USER_LEVEL_OPER) {
                                        thirdDividend = this.getDistCommission(sharePreFee, USER_LEVEL_OPER, "D20");
                                        thirdLeaderUserId = parentGrandParentUser.getId();
                                    } else {
                                        thirdDividend = new BigDecimal(0);
                                    }
                                }
                            }
                        }
                    }
                } else if (parentUser.getUserLevel() == USER_LEVEL_OPER) {
                    //二级分润
                    //自己是会员
                    //上级是运营商, 不再给上上级结算
                    secondaryDividend = this.getDistCommission(sharePreFee, USER_LEVEL_OPER, "D10");
                    thirdDividend = new BigDecimal(0);
                }


            } else if (MallUser.getUserLevel() == USER_LEVEL_OPER) {
                //运营商
                if (parentUser.getUserLevel() == USER_LEVEL_OPER) {
                    //上级是运营商并且自己是运营商
                    secondaryDividend = this.getDistCommission(sharePreFee, USER_LEVEL_OPER, "D11");
                    thirdDividend = new BigDecimal(0);
                } else if (parentUser.getUserLevel() == USER_LEVEL_MEMBER) {
                    //上级是会员,自己是运营商,无需结算
                    secondaryDividend = new BigDecimal(0);
                    thirdDividend = new BigDecimal(0);
                }


            }
        }

        order.setPrimaryDividend(primaryDividend);
        order.setSecondaryDividend(secondaryDividend);
        order.setTripleDividend(thirdDividend);


        LocalDateTime click_time = LocalDateTimeUtil.getDateStrToLocalDateTime(
                MapUtils.getString(orderMap, "click_time"));
        order.setAddTime(click_time);

        MallGoods goodDto = new MallGoods();
        goodDto.setSrcGoodId(MapUtils.getString(orderMap, "item_id"));
        goodDto.setChannelCode(properties.getChannelCode());
        MallGoods oldGoods = goodsService.selectOneBySrcGoodIdAndChannelCode(goodDto);

        MallOrderGoods orderGoods = new MallOrderGoods();
        orderGoods.setGoodsName(MapUtils.getString(orderMap, "item_title"));
        orderGoods.setGoodsSn(MapUtils.getString(orderMap, "item_id"));
        orderGoods.setSrcGoodId(MapUtils.getString(orderMap, "item_id"));

        String picUrl = "https:" + MapUtils.getString(orderMap, "item_img");
        orderGoods.setPicUrl(picUrl);
        orderGoods.setNumber(MapUtils.getShort(orderMap, "item_num"));
        orderGoods.setPrice(orderPrice);

        if (oldGoods != null) {
            orderGoods.setGoodsId(oldGoods.getId());
            orderGoods.setGoodsSn(oldGoods.getGoodsSn());
        }
        orderGoods.setAddTime(addTime);
        orderGoods.setUpdateTime(addTime);
        orderGoods.setDeleted(false);
        orderGoods.setComment("0");

        MallOrderService.addForTbk(order, orderGoods);
    }

    private BigDecimal getDistCommission(BigDecimal sharePreFee, Byte disUserType, String disProLevel) {
        MallDistributionSettings distSettings =
                this.selectDistributionSettings(disUserType.toString(), disProLevel);
        if (distSettings == null) {
            return new BigDecimal(0);
        } else {
            return sharePreFee.
                    multiply(new BigDecimal(distSettings.getDisIntegralValue()))
                    .divide(new BigDecimal(100)).setScale(2, BigDecimal.ROUND_DOWN);
        }
    }

    private MallDistributionSettings selectDistributionSettings(String disUserType, String disProLevel) {
        MallDistributionSettingsExample example = new MallDistributionSettingsExample();
        example.or().andDisUserTypeEqualTo(disUserType)
                .andDisProLevelEqualTo(disProLevel)
                .andDeletedEqualTo("0");
        return distSettingsMapper.selectOneByExample(example);
    }
    //未同步字段
//	"alimama_rate": "0.00",
//	"alimama_share_fee": "0.00",
//	"flow_source": "--",
//	"income_rate": "9.00",
//	"pub_share_fee": "0.00",
//	"refund_tag": 0,
//	"seller_nick": "家居生活居士",
//	"seller_shop_title": "尚品纸杯工厂店",
//	"subsidy_fee": "0.00",
//	"subsidy_rate": "0.00",
//	"subsidy_type": "--",
//	"tb_paid_time": "2019-08-11 20:37:55",
//	"terminal_type": "无线",
//	"tk_commission_fee_for_media_platform": "0.00",
//	"tk_commission_pre_fee_for_media_platform": "0.00",
//	"tk_commission_rate_for_media_platform": "0.00",
//	"tk_order_role": 2,
//	"trade_parent_id": "575938467281045351"
//	"item_price": "14.97",
//	"item_category_name": "餐饮具",
//	"item_link": "http:\/\/item.taobao.com\/item.htm?id=43379803590",

}
