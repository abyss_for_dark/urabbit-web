package com.graphai.mall.admin.web.user.behavior;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.dataokutil.HttpUtils;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.mall.admin.domain.MallMerchant;
import com.graphai.mall.db.domain.MallAddress;
import com.graphai.mall.db.domain.MallAdmin;
import com.graphai.mall.db.domain.MallRegion;
import com.graphai.mall.db.service.common.MallRegionService;
import com.graphai.mall.db.service.user.behavior.MallAddressService;
import com.graphai.validator.Order;
import com.graphai.validator.Sort;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/admin/address")
@Validated
public class AdminAddressController {
    private final Log logger = LogFactory.getLog(AdminAddressController.class);

    @Autowired
    private MallAddressService addressService;
    @Autowired
    private MallRegionService regionService;

    @RequiresPermissions("admin:address:list")
    @RequiresPermissionsDesc(menu = {"用户管理", "收货地址"}, button = "查询")
    @GetMapping("/list")
    public Object list(String userId, String name,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit,
                       @Sort @RequestParam(defaultValue = "id") String sort,
                       @Order @RequestParam(defaultValue = "desc") String order) {

        List<MallAddress> addressList = addressService.querySelective(userId, name, page, limit, sort, order);
        long total = PageInfo.of(addressList).getTotal();


        Map<String, Object> data = new HashMap<>();
        data.put("total", total);
        data.put("items", addressList);

        return ResponseUtil.ok(data);
    }

    private static final String key = "0adaff1dc4e2e4193d6403a64cfce19e";

    @RequiresPermissions("admin:address:addOrUpdate")
    @RequiresPermissionsDesc(menu = {"新增"}, button = "新增")
    @PostMapping("/addOrUpdate")
    public Object addOrUpdate(@RequestBody MallAddress mallAddress) throws Exception {
        MallAdmin adminUser = (MallAdmin) SecurityUtils.getSubject().getPrincipal();
        mallAddress.setUserId(adminUser.getUserId());
        mallAddress.setIsDefault(false);
        mallAddress.setType(mallAddress.getType());
        mallAddress.setDeleted(false);


        String url = "https://restapi.amap.com/v3/geocode/geo?output=JSON&address=" + mallAddress.getAddressDetail() + "&key=" + key + "&city=" + mallAddress.getCity();
        JSONObject jsonObject = new JSONObject(HttpUtils.sendGet(url, null));

        Object str = jsonObject.get("status");

        if (ObjectUtil.isNotNull(str)) {
            if ("1".equals(str.toString())) {
                JSONObject regeocode = new JSONObject(jsonObject.get("geocodes"));
                if (regeocode.isEmpty()) {
                    JSONArray jsonArray = regeocode.toJSONArray(new ArrayList<>());
                    JSONObject jsonMap = new JSONObject(jsonArray.get(0));
                    Object location = jsonMap.get("location");
                    if (ObjectUtil.isNotNull(location) && ObjectUtil.isNotEmpty(location)) {
                        String[] strs = location.toString().split(",");
                        mallAddress.setLongitude(strs[0]);
                        mallAddress.setLatitude(strs[1]);
                    }
                }
            } else {
                //如果请求高德地图失败，则使用区经纬度
                // 区经纬度
                MallRegion mallRegion = regionService.queryByCode(Integer.parseInt(mallAddress.getCountyCode()));
                if (mallRegion != null) {
                    mallAddress.setLongitude(mallRegion.getLongitude());
                    mallAddress.setLatitude(mallRegion.getLatitude());
                }
            }
        }

        if (ObjectUtil.isEmpty(mallAddress.getId()) || ObjectUtil.isNull(mallAddress.getId())) {
            addressService.add(mallAddress);
        } else {
            addressService.update(mallAddress);
        }
        return ResponseUtil.ok();
    }


}
