package com.graphai.mall.admin.web.coupon;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.framework.generator.controller.IBaseController;
import com.graphai.mall.admin.domain.MallUserMerchant;
import com.graphai.mall.admin.service.IMallUserMerchantService;
import com.graphai.mall.admin.util.AdminUserUtils;
import com.graphai.mall.db.constant.account.AccountStatus;
import com.graphai.mall.db.constant.coupon.CouponConstant;
import com.graphai.mall.db.constant.coupon.CouponUserConstant;
import com.graphai.mall.db.domain.MallAdmin;
import com.graphai.mall.db.domain.MallCoupon;
import com.graphai.mall.db.domain.MallCouponUser;
import com.graphai.mall.db.service.coupon.MallCouponService;
import com.graphai.mall.db.service.coupon.MallCouponUserService;
import com.graphai.open.mallcoupon.entity.IMallCoupon;
import com.graphai.open.mallcoupon.service.IMallCouponService;
import com.graphai.validator.Order;
import com.graphai.validator.Sort;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/admin/coupon")
@Validated
@Slf4j
public class AdminCouponController extends IBaseController<IMallCoupon, String> {

    @Autowired
    private MallCouponService couponService;
    @Autowired
    private MallCouponUserService couponUserService;
    @Autowired
    private IMallCouponService iMallCouponService;
    @Autowired
    private IMallUserMerchantService mallUserMerchantService;

    @RequiresPermissions("admin:coupon:list")
    @RequiresPermissionsDesc(menu = {"推广管理", "优惠券管理"}, button = "查询")
    @GetMapping("/list")
    public Object list(String name, Short type, Short status,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit,
                       @Sort @RequestParam(defaultValue = "id") String sort,
                       @Order @RequestParam(defaultValue = "desc") String order) {
        List<MallCoupon> couponList = couponService.querySelective(name, type, status, page, limit, sort, order);
        long total = PageInfo.of(couponList).getTotal();
        Map<String, Object> data = new HashMap<>();
        data.put("total", total);
        data.put("items", couponList);

        return ResponseUtil.ok(data);
    }


    @RequiresPermissions("admin:couponV2:list")
    @RequiresPermissionsDesc(menu = {"营销管理", "卡劵营销"}, button = "查询")
    @GetMapping("/listV2")
    public Object listV2(@RequestParam(required = false) String name, @RequestParam(required = false) Short status,
                         @RequestParam(required = false) String createStartTime,
                         @RequestParam(required = false) String endStartTime) {
        Map<String, Object> data = new HashMap<>();
        LambdaQueryWrapper<IMallCoupon> queryWrapper = new QueryWrapper<IMallCoupon>().lambda();
        MallAdmin loginUser = AdminUserUtils.getLoginUser();
        String roleId = Arrays.toString(loginUser.getRoleIds());
        if (!roleId.contains(AccountStatus.LOGIN_ROLE_ADMIN)) {
            MallUserMerchant mallUserMerchant = mallUserMerchantService.getOne(
                    new QueryWrapper<MallUserMerchant>().lambda().eq(MallUserMerchant::getUserId, loginUser.getUserId()));
            queryWrapper.eq(IMallCoupon::getMerchantId, mallUserMerchant.getMerchantId());
        }

        if (StrUtil.isNotBlank(name)) {
            queryWrapper.like(IMallCoupon::getName, name);
        }
        if (status != null) {
            queryWrapper.eq(IMallCoupon::getStatus, status);
        }
        if (StrUtil.isNotBlank(createStartTime) && StrUtil.isNotBlank(endStartTime)) {
            queryWrapper.between(IMallCoupon::getAddTime, createStartTime, endStartTime);
        }
        queryWrapper.orderByDesc(IMallCoupon::getAddTime);
        queryWrapper.eq(IMallCoupon::getDeleted, false);
        IPage<IMallCoupon> page = iMallCouponService.page(getPlusPage(), queryWrapper);
        data.put("total", page.getTotal());
        data.put("items", page.getRecords());
        return ResponseUtil.ok(data);
    }

    @RequiresPermissions("admin:coupon:listuser")
    @RequiresPermissionsDesc(menu = {"推广管理", "优惠券管理"}, button = "查询用户")
    @GetMapping("/listuser")
    public Object listuser(String userId, String couponId, Short status,
                           @RequestParam(defaultValue = "1") Integer page,
                           @RequestParam(defaultValue = "10") Integer limit,
                           @Sort @RequestParam(defaultValue = "id") String sort,
                           @Order @RequestParam(defaultValue = "desc") String order) {
        List<MallCouponUser> couponList = couponUserService.queryList(userId, couponId, status, page, limit, sort, order);
        long total = PageInfo.of(couponList).getTotal();
        Map<String, Object> data = new HashMap<>();
        data.put("total", total);
        data.put("items", couponList);
        return ResponseUtil.ok(data);
    }

    private Object validate(MallCoupon coupon) {
        String name = coupon.getName();
        if (StringUtils.isEmpty(name)) {
            return ResponseUtil.badArgument();
        }
        return null;
    }

    @RequiresPermissions("admin:coupon:create")
    @RequiresPermissionsDesc(menu = {"推广管理", "优惠券管理"}, button = "添加")
    @PostMapping("/create")
    public Object create(@RequestBody MallCoupon coupon) {
        MallAdmin adminUser = (MallAdmin) SecurityUtils.getSubject().getPrincipal();
        String userId = adminUser.getUserId();
        if (!AdminUserUtils.isAdminRole()) {
            MallUserMerchant mallUserMerchant = mallUserMerchantService.getOne(
                    new QueryWrapper<MallUserMerchant>().lambda().eq(MallUserMerchant::getUserId, userId));
            if (ObjectUtil.isNotEmpty(mallUserMerchant))
                coupon.setMerchantId(mallUserMerchant.getMerchantId());
        }
        Object error = validate(coupon);
        if (error != null) {
            return error;
        }

        // 如果是兑换码类型，则这里需要生存一个兑换码
        if (coupon.getType().equals(CouponConstant.TYPE_CODE)) {
            String code = couponService.generateCode();
            coupon.setCode(code);
        } else if (coupon.getType().equals(new Short((short) 4))) {
            if (coupon.getDiscount() != null &&
                    !coupon.getDiscount().equals("") &&
                    coupon.getDiscount().compareTo(new BigDecimal(1)) == 1) {
                return ResponseUtil.fail("请输入对应格式折扣！");
            }
        } else if ((coupon.getType().equals(new Short((short) 5)) || coupon.getType().equals((short) 0)) && coupon.getCouponType().equals(new Short((short) 1))) {
            if (coupon.getDiscount() != null &&
                    !coupon.getDiscount().equals("") &&
                    coupon.getDiscount().compareTo(new BigDecimal(1)) == 1) {
                return ResponseUtil.fail("请输入对应格式折扣！");
            }
        }
        couponService.add(coupon);
        return ResponseUtil.ok(coupon);
    }

    @RequiresPermissions("admin:coupon:read")
    @RequiresPermissionsDesc(menu = {"推广管理", "优惠券管理"}, button = "详情")
    @GetMapping("/read")
    public Object read(@NotNull String id) {
        MallCoupon coupon = couponService.findById(id);
        return ResponseUtil.ok(coupon);
    }

    @RequiresPermissions("admin:coupon:update")
    @RequiresPermissionsDesc(menu = {"推广管理", "优惠券管理"}, button = "编辑")
    @PostMapping("/update")
    public Object update(@RequestBody MallCoupon coupon) {
//        Object error = validate(coupon);
//        if (error != null) {
//            return error;
//        }
        if (couponService.updateById(coupon) == 0) {
            return ResponseUtil.updatedDataFailed();
        }
        //优惠劵下架需要更改用户 领取并未使用 的优惠劵状态为过期
        changStatus(coupon);
        return ResponseUtil.ok(coupon);
    }

    private void changStatus(MallCoupon coupon) {
        //下架
        if (CouponUserConstant.STATUS_EXPIRED.equals(coupon.getStatus())) {
            couponUserService.updateByStatus(CouponUserConstant.STATUS_EXPIRED, CouponUserConstant.STATUS_USABLE, coupon.getId(), null);
            //上架
        } else if (CouponUserConstant.STATUS_USABLE.equals(coupon.getStatus())) {
            couponUserService.updateByStatus(CouponUserConstant.STATUS_USABLE, CouponUserConstant.STATUS_EXPIRED, coupon.getId(), LocalDateTime.now().withNano(0));
        }
    }

    @RequiresPermissions("admin:coupon:delete")
    @RequiresPermissionsDesc(menu = {"推广管理", "优惠券管理"}, button = "删除")
    @PostMapping("/delete")
    public Object delete(@RequestBody MallCoupon coupon) {
        couponService.deleteById(coupon.getId());
        couponUserService.deleteByCouponId(coupon.getId());
        return ResponseUtil.ok();
    }

    @GetMapping("/getUserInfo")
    public Object getUserInfo() {
        MallAdmin loginUser = AdminUserUtils.getLoginUser();
        String roleIds = Arrays.toString(loginUser.getRoleIds());
        HashMap<String, Object> map = new HashMap<>();
        if (roleIds.contains(AccountStatus.LOGIN_ROLE_ADMIN)) {
            map.put("tag", 1);
            map.put("isAdmin", false);
        } else {
            if (roleIds.contains(AccountStatus.LOGIN_ROLE_FACTORY)) {
                map.put("tag", 1);
            } else if (roleIds.contains(AccountStatus.LOGIN_ROLE_MERCHANT)) {
                map.put("tag", 2);
            }
            map.put("isAdmin", true);
        }

        return ResponseUtil.ok(map);
    }

}
