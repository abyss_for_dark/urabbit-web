package com.graphai.mall.admin.web.goods;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.framework.system.service.IGraphaiSystemRoleService;
import com.graphai.framework.system.service.MallRoleService;
import com.graphai.id.GraphaiIdGenerator;
import com.graphai.mall.admin.domain.MallGoodsCategory;
import com.graphai.mall.admin.domain.MallMerchant;
import com.graphai.mall.admin.domain.MallUserMerchant;
import com.graphai.mall.admin.dto.GoodsAllinone;
import com.graphai.mall.admin.service.AdminGoodsService;
import com.graphai.mall.admin.service.IMallGoodsCategoryService;
import com.graphai.mall.admin.service.IMallMerchantService;
import com.graphai.mall.admin.service.IMallUserMerchantService;
import com.graphai.mall.core.lucene.service.DeleteIndexAboutService;
import com.graphai.mall.db.domain.*;
import com.graphai.mall.db.service.advertisement.MallBrandService;
import com.graphai.mall.db.service.goods.MallGoodsAttributeService;
import com.graphai.mall.db.service.goods.MallGoodsProductService;
import com.graphai.mall.db.service.goods.MallGoodsService;
import com.graphai.mall.db.service.goods.MallGoodsSpecificationService;
import com.graphai.mall.db.service.sysconfig.MallSystemConfigService;
import com.graphai.mall.db.vo.MallCardVo;
import com.graphai.mall.setting.domain.FreightTemplate;
import com.graphai.mall.setting.service.FreightTemplateService;
import com.graphai.open.utils.HttpRequestUtils;
import com.graphai.validator.Order;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.*;

@RestController
@RequestMapping("/admin/goods")
@Validated
public class AdminGoodsController {
    private final Log logger = LogFactory.getLog(AdminGoodsController.class);
    private static final String URL = "http://web.51fangtao.com/wx/all/search/createOrUpdateIndexById?id=";
    private static final String DELETE_URL = "http://web.51fangtao.com/wx/all/search/deleteIndexById?id=";

    @Autowired
    private AdminGoodsService adminGoodsService;
    @Autowired
    private MallBrandService mallBrandService;
    @Autowired
    private MallRoleService mallRoleService;
    @Autowired
    private MallSystemConfigService mallSystemConfigService;
    @Autowired
    private DeleteIndexAboutService deleteIndexAboutService;
    @Autowired
    private IGraphaiSystemRoleService iGraphaiSystemRoleService;
    @Autowired
    private MallGoodsService goodsService;
    @Autowired
    private IMallMerchantService mallMerchantService;
    @Autowired
    private IMallUserMerchantService mallUserMerchantService;
    @Autowired
    private MallGoodsAttributeService mallGoodsAttributeService;
    @Autowired
    private IMallGoodsCategoryService mallGoodsCategoryService;
    @Autowired
    private MallGoodsProductService mallGoodsProductService;
    @Autowired
    private MallGoodsSpecificationService mallGoodsSpecificationService;

    /**
     * 查询商品(goodsType: 0 自营商品、1:第三方商品 2:优惠券商品 3:权益商品)
     */
    @RequiresPermissions("admin:goods:list")
    @RequiresPermissionsDesc(menu = {"商品管理", "商品列表"}, button = "查询")
    @GetMapping("/list")
    public Object list(String goodsSn, String name, Integer goodsType,
                       String brandName, Integer priceMin, Integer priceMax,
                       Boolean isHot, Boolean isNew, Integer isOnSale, String merchantNoSale,
                       Integer categorypid, Integer industryId, Boolean isRightGoods,
                       Integer platform,
                       Integer placement,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit,
                       @RequestParam(defaultValue = "id") String sort,
                       @Order @RequestParam(defaultValue = "desc") String order) {
        //isOnSale：0:下架，1:上架，2:审核中（审核中也就是下架）,3:（驳回）
        if (ObjectUtil.isNull(isOnSale)) {
            return adminGoodsService.list(merchantNoSale, goodsSn, name, page, limit, sort, order,
                    brandName, priceMin, platform, priceMax, isHot, isNew, null, categorypid, industryId, goodsType, isRightGoods, placement);
        }

        if (isOnSale == 2 || isOnSale == 0) {
            return adminGoodsService.list(merchantNoSale, goodsSn, name, page, limit, sort, order,
                    brandName, priceMin, platform, priceMax, isHot, isNew, false, categorypid, industryId, goodsType, isRightGoods, placement);
        } else if (isOnSale == 1) {
            return adminGoodsService.list(merchantNoSale, goodsSn, name, page, limit, sort, order,
                    brandName, priceMin, platform, priceMax, isHot, isNew, true, categorypid, industryId, goodsType, isRightGoods, placement);
        } else if (isOnSale == 3) {
            return adminGoodsService.list("2", goodsSn, name, page, limit, sort, order,
                    brandName, priceMin, platform, priceMax, isHot, isNew, null, categorypid, industryId, goodsType, isRightGoods, placement);
        }
        return adminGoodsService.list(merchantNoSale, goodsSn, name, page, limit, sort, order,
                brandName, priceMin, platform, priceMax, isHot, isNew, true, categorypid, industryId, goodsType, isRightGoods, placement);
    }


    /**
     * 活动查询商品接口(查询当前登录用户的商品及商品规格)
     */
    @GetMapping("/list/activity")
    public Object listActivity(@RequestParam(defaultValue = "1") Integer page,
                               @RequestParam(defaultValue = "10") Integer limit,
                               @RequestParam(defaultValue = "id") String sort,
                               @Order @RequestParam(defaultValue = "desc") String order) {

        // 获取当前用户
        MallAdmin adminUser = (MallAdmin) SecurityUtils.getSubject().getPrincipal();

        List<MallRole> mallRoleList = iGraphaiSystemRoleService.selectRoleListMMByUserId(adminUser.getId());
        MallRole mallRole = null;
        if (mallRoleList.size() > 0) {
            mallRole = mallRoleList.get(0);
        }

        Map<String, Object> data = goodsService.listActivity(page, limit, sort, order, mallRole, adminUser);


        if (ObjectUtil.isNotNull(mallRole) && ObjectUtil.isNotEmpty(mallRole)) {
            data.put("login", mallRole.getRoleKey());
        } else {
            data.put("login", "admin");
        }
        return ResponseUtil.ok(data);
    }


    /**
     * 活动查询商品接口(查询当前登录用户的商品及商品规格)
     */
    @GetMapping("/list/activity/seckill")
    public Object listActivitySeckill(@RequestParam(defaultValue = "1") Integer page,
                                      @RequestParam(defaultValue = "10") Integer limit,
                                      @RequestParam(defaultValue = "id") String sort,
                                      @Order @RequestParam(defaultValue = "desc") String order) {

        // 获取当前用户
        MallAdmin adminUser = (MallAdmin) SecurityUtils.getSubject().getPrincipal();

        List<MallRole> mallRoleList = iGraphaiSystemRoleService.selectRoleListMMByUserId(adminUser.getId());
        MallRole mallRole = null;
        if (mallRoleList.size() > 0) {
            mallRole = mallRoleList.get(0);
        }

        Map<String, Object> data = goodsService.listActivity1(page, limit, sort, order, mallRole, adminUser);


        if (ObjectUtil.isNotNull(mallRole) && ObjectUtil.isNotEmpty(mallRole)) {
            data.put("login", mallRole.getRoleKey());
        } else {
            data.put("login", "admin");
        }
        return ResponseUtil.ok(data);
    }


    @GetMapping("/plant/list")
    public Object list(@RequestParam(required = false) String merchantId,
                       @RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit,
                       @RequestParam(defaultValue = "id") String sort,
                       @Order @RequestParam(defaultValue = "desc") String order) {
        return adminGoodsService.list(merchantId, page, limit, sort, order);
    }


    /**
     * 查询分类
     */
    @GetMapping("/categoryList")
    public Object categoryList() {
        Object object = adminGoodsService.selectCategoryList();
        return ResponseUtil.ok(object);
    }


    /**
     * 查询 所属品牌商(默认50条数据)
     */
    @GetMapping("/brandList")
    public Object brandList(@RequestParam("name") String name,
                            @RequestParam(defaultValue = "1") Integer page,
                            @RequestParam(defaultValue = "50") Integer limit) {
        MallBrand mallBrand = new MallBrand();
        mallBrand.setName(name);
        List<MallBrand> mallBrandList = mallBrandService.selectBrandList(mallBrand, page, limit);
        return ResponseUtil.ok(mallBrandList);
    }


    /**
     * 商品编辑
     */
    @RequiresPermissions("admin:goods:update")
    @RequiresPermissionsDesc(menu = {"商品管理", "商品列表"}, button = "编辑")
    @PostMapping("/update")
    public Object update(@RequestBody GoodsAllinone goodsAllinone) {
        adminGoodsService.update(goodsAllinone);
        MallGoods mallGoods = goodsAllinone.getGoods();
        if(mallGoods.getIsOnSale()) {
            String url = URL + goodsAllinone.getGoods().getId();
            HttpRequestUtils.get(url, null, null);
        }
        return "success";
    }

    /**
     * 商品添加
     */
    @RequiresPermissions("admin:goods:create")
    @RequiresPermissionsDesc(menu = {"商品管理", "商品列表"}, button = "添加")
    @PostMapping("/create")
    public Object create(@RequestBody GoodsAllinone goodsAllinone) {
        try {
            adminGoodsService.create(goodsAllinone);
            MallGoods mallGoods = goodsAllinone.getGoods();
            if(mallGoods.getIsOnSale()) {
                String url = URL + goodsAllinone.getGoods().getId();
                HttpRequestUtils.get(url, null, null);
            }
            return "success";
        } catch (Exception e) {
            return ResponseUtil.fail(-1, e.getMessage());
        }
    }

    @Autowired
    private FreightTemplateService freightTemplateService;

    /**
     * 商品导入
     */
    @RequiresPermissions("admin:goods:copy")
    @RequiresPermissionsDesc(menu = {"商品管理", "商品列表"}, button = "导入")
    @PostMapping("/copy")
    public Object copy(@RequestBody List<MallGoods> mallGoodsList) {
        if(ObjectUtil.isEmpty(mallGoodsList)){
            return ResponseUtil.fail("未选择商品");
        }
        MallAdmin adminUser = (MallAdmin) SecurityUtils.getSubject().getPrincipal();
        QueryWrapper<MallUserMerchant> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", adminUser.getUserId());
        MallUserMerchant mallUserMerchant = mallUserMerchantService.getOne(queryWrapper);

        //导入商品验证，判断是否存在已导入过的商品
        List<String> ids = new ArrayList<>();
        for(MallGoods item : mallGoodsList){
            ids.add(item.getId());
        }
        MallGoodsExample example = new MallGoodsExample();
        MallGoodsExample.Criteria criteria = example.or();
        criteria.andSrcGoodIdIn(ids);
        criteria.andMerchantIdEqualTo(mallUserMerchant.getMerchantId());
        List<MallGoods> data = goodsService.selectByExample(example);
        if(data.size() > 0){
            return ResponseUtil.fail("存在已导入过的商品");
        }

        List<FreightTemplate> freightTemplates = freightTemplateService.list(new QueryWrapper<FreightTemplate>().lambda().eq(FreightTemplate::getMerchantId, mallUserMerchant.getMerchantId()));
        try {
            for (MallGoods mallGoods : mallGoodsList) {
                String oldGoodsId = mallGoods.getId();
                MallGoodsExample example1 = new MallGoodsExample();
                MallGoodsExample.Criteria criteria1 = example1.or();
                criteria1.andIdEqualTo(oldGoodsId);
                List<MallGoods> data1 = goodsService.selectByExample(example1);
                if(ObjectUtil.isNotEmpty(data1)){
                    mallGoods.setGallery(data1.get(0).getGallery());
                }
                String goodsId = GraphaiIdGenerator.nextId("MallGoods");
                mallGoods.setId(goodsId);
                mallGoods.setSrcGoodId(oldGoodsId);
                mallGoods.setSellerId(mallGoods.getMerchantId());
                mallGoods.setMerchantId(mallUserMerchant.getMerchantId());
                mallGoods.setItemsale(0);
                mallGoods.setItemsale2(0);
                mallGoods.setTodaysale(0);

                if (freightTemplates.size() != 0) {
                    mallGoods.setTemplateId(freightTemplates.get(0).getId());
                } else {
                    mallGoods.setTemplateId(null);
                }
                //复制商品参数
                List<MallGoodsAttribute> mallGoodsAttributes = mallGoodsAttributeService.queryByGid(oldGoodsId);
                for (MallGoodsAttribute item : mallGoodsAttributes) {
                    item.setId(GraphaiIdGenerator.nextId("MallGoodsAttribute"));
                    item.setGoodsId(goodsId);
                    mallGoodsAttributeService.add(item);
                }
                //复制商品类目
                List<MallGoodsCategory> mallGoodsCategories = mallGoodsCategoryService.queryByGid(oldGoodsId);
                for (MallGoodsCategory item : mallGoodsCategories) {
                    item.setId(GraphaiIdGenerator.nextId("MallGoodsCategory"));
                    item.setGoodsId(goodsId);
                    mallGoodsCategoryService.add(item);
                }
                //复制商品货品
                List<MallGoodsProduct> mallGoodsProducts = mallGoodsProductService.queryByGid(oldGoodsId);
                for (MallGoodsProduct item : mallGoodsProducts) {
                    item.setId(GraphaiIdGenerator.nextId("MallGoodsProduct"));
                    item.setGoodsId(goodsId);
                    mallGoodsProductService.add(item);
                }
                //复制商品规格
                List<MallGoodsSpecification> mallGoodsSpecifications = mallGoodsSpecificationService.queryByGid(oldGoodsId);
                for (MallGoodsSpecification item : mallGoodsSpecifications) {
                    item.setId(GraphaiIdGenerator.nextId("MallGoodsSpecification"));
                    item.setGoodsId(goodsId);
                    mallGoodsSpecificationService.add(item);
                }
            }
            goodsService.batchInsert(mallGoodsList);

            for (MallGoods mallGoods : mallGoodsList) {
                String url = URL + mallGoods.getId();
                HttpRequestUtils.get(url, null, null);
            }
            return ResponseUtil.ok();
        } catch (Exception e) {
            return ResponseUtil.fail(-1, e.getMessage());
        }
    }

    /**
     * 商品详情
     */
    @RequiresPermissions("admin:goods:read")
    @RequiresPermissionsDesc(menu = {"商品管理", "商品列表"}, button = "详情")
    @GetMapping("/detail")
    public Object detail(@NotNull String id) {
        return adminGoodsService.detail(id);

    }

    /**
     * 商品删除
     */
    @RequiresPermissions("admin:goods:delete")
    @RequiresPermissionsDesc(menu = {"商品管理", "商品列表"}, button = "删除")
    @PostMapping("/delete")
    public Object delete(@RequestBody MallGoods goods) {
        //原商品id为当前正在删除的商品id需要一同删除
        MallGoodsExample example = new MallGoodsExample();
        MallGoodsExample.Criteria criteria = example.or();
        criteria.andSrcGoodIdEqualTo(goods.getId());
        List<MallGoods> list = goodsService.selectByExample(example);
        for (MallGoods item : list) {
            item.setDeleted(true);
            goodsService.updateById(item);
            String url = DELETE_URL + item.getId();
            HttpRequestUtils.get(url, null, null);
        }
        goods.setDeleted(true);
        goodsService.updateById(goods);
        String url = DELETE_URL + goods.getId();
        HttpRequestUtils.get(url, null, null);
        return "success";
    }


    /**
     * 商品下架
     */
    @RequiresPermissions("admin:goods:disableGoods")
    @RequiresPermissionsDesc(menu = {"商品管理", "商品列表"}, button = "上架/下架,取消审核/申请审核")
    @PostMapping("/disableGoods")
    public Object disableGoods(@RequestBody MallGoods mallGoods) {
        if(mallGoods.getIsOnSale() == true){
            //上架
            String url = URL + mallGoods.getId();
            HttpRequestUtils.get(url, null, null);
        }else{
            //下架
            String url = DELETE_URL + mallGoods.getId();
            HttpRequestUtils.get(url, null, null);
        }

        return adminGoodsService.disableGoods(mallGoods);
    }

    /**
     * 商品下架
     */
    @RequiresPermissions("admin:goods:batchDisableGoods")
    @RequiresPermissionsDesc(menu = {"商品管理", "商品列表"}, button = "下架")
    @PostMapping("/batchDisableGoods")
    public Object batchDisableGoods(@RequestBody JSONArray body) {
        for (Object o : body) {
            String id = JSONUtil.toJsonStr(o);
            MallGoods goods = new MallGoods();
            goods.setId(id);
            goods.setIsOnSale(false);
            //下架
            String url = DELETE_URL + id;
            HttpRequestUtils.get(url, null, null);

            adminGoodsService.disableGoods(goods);
        }
        return ResponseUtil.ok();
    }

    /**
     * 会员卡
     */
    @RequiresPermissions("admin:goods:cardList")
    @RequiresPermissionsDesc(menu = {"商品管理", "购卡管理"}, button = "查询")
    @GetMapping("/cardList")
    public Object getCardList(@RequestParam(defaultValue = "0") String type,
                              @RequestParam(defaultValue = " ") String name,
                              @RequestParam(defaultValue = "1") Integer page,
                              @RequestParam(defaultValue = "10") Integer limit,
                              @RequestParam(defaultValue = "") String sort,
                              @Order @RequestParam(defaultValue = "desc") String order) {
        Map mapData = new HashMap();
        Map<String, String> map = mallSystemConfigService.listCard();
        List<MallCardVo> list = new ArrayList<>();
        MallCardVo cardVo = new MallCardVo();
        MallCardVo cardVo1 = new MallCardVo();


        cardVo1.setcPrice(map.get("Mall_card_electron_cprice"));
        cardVo1.setyPrice(map.get("Mall_card_electron_yprice"));
        cardVo1.setDesc(map.get("Mall_card_electron_desc"));
        cardVo1.setType("2");

        cardVo.setcPrice(map.get("Mall_card_cprice"));
        cardVo.setyPrice(map.get("Mall_card_yprice"));
        cardVo.setDesc(map.get("Mall_card_desc"));
        cardVo.setType("1");

        if ("1".equals(type)) {
            list.add(cardVo);
        } else if ("2".equals(type)) {
            list.add(cardVo1);
        } else if ("0".equals(type)) {
            list.add(cardVo1);
            list.add(cardVo);
        }


        mapData.put("item", list);
        mapData.put("total", list.size());
        return ResponseUtil.ok(mapData);

    }

    /**
     * 更新会员卡
     */
//    @RequiresPermissions("admin:goods:read")
    @RequiresPermissionsDesc(menu = {"商品管理", "购卡管理"}, button = "编辑")
    @PostMapping("/updateCard")
    public Object updateCard(@RequestBody MallCardVo cardVo) {

        Map mapData = new HashMap();
        if ("1".equals(cardVo.getType())) {
            mapData.put("Mall_card_cprice", cardVo.getcPrice());
            mapData.put("Mall_card_yprice", cardVo.getyPrice());
            mapData.put("Mall_card_desc", cardVo.getDesc());
            mallSystemConfigService.updateConfig(mapData);
        }
        if ("2".equals(cardVo.getType())) {
            mapData.put("Mall_card_electron_cprice", cardVo.getcPrice());
            mapData.put("Mall_card_electron_yprice", cardVo.getyPrice());
            mapData.put("Mall_card_electron_desc", cardVo.getDesc());
            mallSystemConfigService.updateConfig(mapData);
        }


        return ResponseUtil.ok();

    }


    @GetMapping("/getAgentProportion/{name}")
    public Object getAgentProportion(@PathVariable String name) {
        String value = mallSystemConfigService.getMallSystemCommonKeyValue(name);
        return ResponseUtil.ok(value);
    }

    @GetMapping("/getMallUserMerchant")
    public Object getMallUserMerchant(@RequestParam(required = false) String name) {
        try {
            // 获取当前用户
            MallAdmin adminUser = (MallAdmin) SecurityUtils.getSubject().getPrincipal();
            QueryWrapper<MallMerchant> umWrapper = new QueryWrapper<>();
            String[] roleIds = adminUser.getRoleIds();
            MallRole merchantRole = mallRoleService.queryByRoleKey("merchant");
            MallRole plantRole = mallRoleService.queryByRoleKey("plant");


            //如果当前登录用户的角色里面有商户角色
            if (Arrays.asList(roleIds).contains(merchantRole.getId()) || Arrays.asList(roleIds).contains(plantRole.getId())) {
                QueryWrapper<MallUserMerchant> mallWrapper = new QueryWrapper<>();
                mallWrapper.eq("user_id", adminUser.getUserId());
                List<MallUserMerchant> merchants = mallUserMerchantService.list(mallWrapper);
                List<String> merchantIds = new ArrayList<>();
                for (MallUserMerchant item : merchants) {
                    merchantIds.add(item.getMerchantId());
                }
                umWrapper.in("id", merchantIds);
            }


            if (StrUtil.isNotEmpty(name)) {
                umWrapper.like("merchant_name", name);
            }
            List<MallMerchant> list = mallMerchantService.list(umWrapper);
            return ResponseUtil.ok(list);
        } catch (Exception e) {
            return ResponseUtil.fail(-1, "所属商家查询失败！");
        }
    }


}
