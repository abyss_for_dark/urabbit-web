package com.graphai.mall.admin.web.user.behavior;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.framework.system.service.MallRoleService;
import com.graphai.mall.admin.dao.MallMerchantMapper;
import com.graphai.mall.admin.domain.MallMerchant;
import com.graphai.mall.admin.domain.MallUserMerchant;
import com.graphai.mall.admin.service.impl.MallMerchantServiceImpl;
import com.graphai.mall.admin.service.impl.MallUserMerchantServiceImpl;
import com.graphai.mall.db.domain.*;
import com.graphai.mall.db.service.goods.MallGoodsService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.service.user.behavior.MallFootprintService;
import com.graphai.mall.db.vo.MallFootprintVo;
import com.graphai.validator.Order;
import com.graphai.validator.Sort;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

import static org.apache.shiro.SecurityUtils.*;

@RestController
@RequestMapping("/admin/footprint")
@Validated
public class AdminFootprintController {

    private final Log logger = LogFactory.getLog(AdminFootprintController.class);

    @Autowired
    private MallFootprintService footprintService;

    @Autowired
    private MallGoodsService mallGoodsService;

    @Autowired
    private MallUserService mallUserService;

    @Autowired
    private MallMerchantMapper merchantMapper;

    @Autowired
    private MallRoleService mallRoleService;

    @Autowired
    private MallUserMerchantServiceImpl mallUserMerchantService;



    @RequiresPermissions("admin:footprint:list")
    @RequiresPermissionsDesc(menu = {"用户管理", "用户足迹"}, button = "查询")
    @GetMapping("/list")
    public Object list(String userId, String goodsId, String userNickname, String goodsName,
                       String beginTime, String endTime, String parentNickname,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit,
                       @Sort @RequestParam(defaultValue = "id") String sort,
                       @Order @RequestParam(defaultValue = "desc") String order) {

        MallFootprintVo footprint = new MallFootprintVo();
        footprint.setUserId(userId);
        footprint.setGoodsId(goodsId);
        footprint.setGoodsName(goodsName);


        if (StringUtils.isNotEmpty(parentNickname)) {
            footprint.setParentNickname(parentNickname);
        }
        if (StringUtils.isNotEmpty(userNickname)) {
            footprint.setUserNickname(userNickname);
        }

        try {
            if (beginTime != null) {
                footprint.setBeginTime(getDateStrToLocalDateTime(beginTime));
            }
            if (endTime != null) {
                footprint.setEndTime(getDateStrToLocalDateTime(endTime));
            }
        } catch (Exception e) {
            logger.error("", e);
        }

        List<MallFootprint> footprintList = null;

        MallAdmin adminUser = (MallAdmin) getSubject().getPrincipal();
        Set<MallRole> mallRoleSet = mallRoleService.findByIds(adminUser.getRoleIds());
        for (MallRole mallRole : mallRoleSet) {
            if ("plant".equals(mallRole.getRoleKey())) {
                QueryWrapper<MallUserMerchant> wrapper=new QueryWrapper<>();
                wrapper.eq("user_id",adminUser.getUserId());
                MallUserMerchant mallUserMerchant = mallUserMerchantService.getOne(wrapper);
//                footprint.setMerchantId(mallUserMerchant.getMerchantId());
//                footprintList = footprintService.selectByUserRole(footprint, page, limit);


                footprintList = footprintService.findByMerchantIdAndName(mallUserMerchant.getMerchantId(),adminUser.getUserId(),goodsId, goodsName, page, limit);
            }
            if ("merchant".equals(mallRole.getRoleKey())) {
                QueryWrapper<MallUserMerchant> wrapper=new QueryWrapper<>();
                wrapper.eq("user_id",adminUser.getUserId());
                MallUserMerchant mallUserMerchant = mallUserMerchantService.getOne(wrapper);
//                footprint.setMerchantId(mallUserMerchant.getMerchantId());
//                footprintList = footprintService.selectByUserRole(footprint, page, limit);
                footprintList = footprintService.findByFactoryIdAndName(mallUserMerchant.getMerchantId(),adminUser.getUserId(),goodsId, goodsName, page, limit);
            }
            if ("admin".equals(mallRole.getRoleKey())) {
                footprintList = footprintService.findAll(page,limit);
            }
        }



//        		(userId, goodsId, page, limit, sort, order);
        //long total = PageInfo.of(footprintList).getTotal();
        Map<String, Object> data = new HashMap<>();


        List<MallFootprintListVo> list = new ArrayList();
        List<String> userIds = new ArrayList();
        List<String> goodsIds = new ArrayList();
        List<String> merchantIds = new ArrayList();
        MallFootprintListVo mallFootprintListVo = null;
        MallGoods mallGoods = null;
        MallUser mallUser = null;
        List<MallGoods> mallGoodsList = null;
        List<MallUser> mallUserList = null;
        List<MallMerchant> mallMerchants = null;

        if (footprintList != null) {
            for (MallFootprint footprintVo : footprintList) {
                userIds.add(footprintVo.getUserId());
                goodsIds.add(footprintVo.getGoodsId());
                merchantIds.add(footprintVo.getMerchantId());
            }
        }
        if (userIds.size() != 0) {
            mallUserList = mallUserService.findByIds(userIds);
        }
        if (goodsIds.size() != 0) {
            mallGoodsList = mallGoodsService.findByIdList(goodsIds);
        }
        if (merchantIds.size() != 0) {
             mallMerchants = merchantMapper.selectBatchIds(merchantIds);
        }

        if (footprintList != null) {
            for (MallFootprint footprintVo : footprintList) {
                mallFootprintListVo = new MallFootprintListVo();
                //mallFootprintListVo.setId(footprintVo.getId());
                for (MallUser user : mallUserList) {
                    if (footprintVo.getUserId().equals(user.getId())) {
                        mallFootprintListVo.setUserId(user.getId());
                        mallFootprintListVo.setNickName(user.getNickname());
                        mallFootprintListVo.setAvatar(user.getAvatar());
                        mallFootprintListVo.setMobile(user.getMobile());
                    }
                }
                for (MallGoods goods : mallGoodsList) {
                    if (footprintVo.getGoodsId().equals(goods.getId())) {
                        mallFootprintListVo.setValueId(footprintVo.getGoodsId());
                        mallFootprintListVo.setName(goods.getName());
                        mallFootprintListVo.setShopName(goods.getShopname());
                        mallFootprintListVo.setShopImg(goods.getPicUrl());
                    }
                }
                for (MallMerchant mallMerchant : mallMerchants) {
                    mallFootprintListVo.setMerchantId(footprintVo.getMerchantId());
                    if (footprintVo.getMerchantId().equals(mallMerchant.getId())){
                        mallFootprintListVo.setMerchantName(mallMerchant.getName());
                        mallFootprintListVo.setMerchantImg(mallMerchant.getImg());
                    }
                }
                mallFootprintListVo.setAddTime(footprintVo.getAddTime());

                list.add(mallFootprintListVo);
            }
            long total = PageInfo.of(footprintList).getTotal();
            data.put("total", total);
            data.put("items", list);
        }
        return ResponseUtil.ok(data);
    }

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static LocalDateTime getDateStrToLocalDateTime(String dateStr) throws Exception {
        java.util.Date date = dateFormat.parse(dateStr);
        Instant instant = date.toInstant();
        ZoneId zone = ZoneId.systemDefault();
        LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, zone);
        return localDateTime;
    }

}
