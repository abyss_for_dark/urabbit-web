package com.graphai.mall.admin.job;

import com.graphai.mall.admin.service.TbkGoodsAddTimingService;
import com.graphai.mall.db.config.TbkapiProperties;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 定时拉取API
 *
 * @author panxianhuan
 */
//@Component
public class TbkGoodsAddTimingJob {

    private final Log logger = LogFactory.getLog(TbkGoodsAddTimingJob.class);

    @Autowired
    private TbkGoodsAddTimingService tbkGoodsAddTimingService;

    @Autowired
    private TbkapiProperties properties;

    //每隔10分钟(10*60*1000ms)执行一次
    @Scheduled(fixedRate = 30 * 60 * 1000)
    public void syscTbkGoodsInvalid() {
        logger.error("TbkGoodsAddTimingJob start!");
        int start = 0;
        int end = 17;
        String dataBatchId = "1001";

        Date now = new Date();
        SimpleDateFormat f = new SimpleDateFormat("HH");
        String hour = f.format(now);
        logger.info("hour:" + hour);
        if (Integer.valueOf(hour) == 0) {
        } else {
            start = Integer.valueOf(hour) - 1;
            end = Integer.valueOf(hour);
        }

        int minId = 1;
        tbkGoodsAddTimingService.syscTbkGoodsTiming(start, end, dataBatchId, minId);
        logger.error("TbkGoodsAddTimingJob end!");
    }

}

