package com.graphai.mall.admin.web.order;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.mall.db.service.order.MallOrderService;
import com.graphai.mall.db.service.order.mallOrderXxlJob.MallOrderXxlJobService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@RestController
@RequestMapping("/admin/order/XxlJob")
@Validated
public class AdminOrderAboutXxlJobController {
    private static Logger log = LoggerFactory.getLogger(AdminOrderAboutXxlJobController.class);
    @Resource
    private MallOrderService mallOrderService;
    @Resource
    private MallOrderXxlJobService mallOrderXxlJobService;

    @GetMapping("zero")
    public Object zero(String closeTime, String type){
        try{
            DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            LocalDateTime time = LocalDateTime.now();
            String localTime = df.format(time);
            LocalDateTime ldt = LocalDateTime.parse(closeTime,df);
            String startTime=df.format(ldt.minusSeconds(10L));
            String endTime=df.format(ldt.plusSeconds(10L));;
            return mallOrderXxlJobService.getOrderDetailByType(startTime, endTime, type);
        }catch(Exception e){
            e.printStackTrace();
            return ResponseUtil.ok(e.getMessage());
        }
    }
}
