package com.graphai.mall.admin.util;

//import com.ruoyi.common.config.Global;
//import com.sun.image.codec.jpeg.JPEGCodec;
//import com.sun.image.codec.jpeg.JPEGImageEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/**
 * 文件处理工具类
 *
 * @author Qxian
 * @date 2020-12-12
 */
public class FileUtils {

    private static final Logger log = LoggerFactory.getLogger(FileUtils.class);


    public static String FILENAME_PATTERN = "[a-zA-Z0-9_\\-\\|\\.\\u4e00-\\u9fa5]+";

    /**
     * 输出指定文件的byte数组
     *
     * @param filePath 文件路径
     * @param os       输出流
     * @return
     */
    public static void writeBytes(String filePath, OutputStream os) throws IOException {
        FileInputStream fis = null;
        try {
            File file = new File(filePath);
            if (!file.exists()) {
                throw new FileNotFoundException(filePath);
            }
            fis = new FileInputStream(file);
            byte[] b = new byte[1024];
            int length;
            while ((length = fis.read(b)) > 0) {
                os.write(b, 0, length);
            }
        } catch (IOException e) {
            throw e;
        } finally {
            if (os != null) {
                try {
                    os.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }
    
    
    /**
    * 根据地址获得数据的字节流
    * @param strUrl 网络连接地址
    * @return
    */
    public static byte[] getImageFromNetByUrl(String strUrl){
    try {
    URL url = new URL(strUrl);
    HttpURLConnection conn = (HttpURLConnection)url.openConnection();
    conn.setRequestMethod("GET");
    conn.setConnectTimeout(5 * 1000);
    InputStream inStream = conn.getInputStream();//通过输入流获取图片数据
    byte[] btImg = readInputStream(inStream);//得到图片的二进制数据
    return btImg;
    } catch (Exception e) {
    e.printStackTrace();
    }
    return null;
    }
    
    
    public static byte[] readInputStream(InputStream inStream) throws Exception{  
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();  
        byte[] buffer = new byte[1024];  
        int len = 0;  
        while( (len=inStream.read(buffer)) != -1 ){    
            outStream.write(buffer, 0, len);  
        }  
        inStream.close();   
        return outStream.toByteArray();  
    } 


    /**
     * 打印输出的txt字符串
     *
     * @param txtPath
     * @param content
     */
    public static void writeTxt(String txtPath, String content) {
        FileOutputStream fileOutputStream = null;
        File file = new File(txtPath);
        try {
            if (file.exists()) {
                //判断文件是否存在，如果不存在就新建一个txt
                file.createNewFile();
            }
            fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(content.getBytes());
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 递归删除文件夹下的所有文件夹和文件
     *
     * @param file
     */
    public static void del(File file) {

        if (file.exists()) {
            @SuppressWarnings("unused")
            File[] f = file.listFiles(new FilenameFilter() {

                @Override
                public boolean accept(File dir, String name) {
                    File currFile = new File(dir, name);
                    if (currFile.isDirectory()) {
                        del(currFile);
                    } else {
                        System.out.println("即将删除：" + currFile.getName());
                        currFile.delete();
                    }
                    System.out.println("即将删除：" + currFile.getName());
                    currFile.delete();
                    return false;
                }
            });
        }
        file.delete();
    }

    /**
     * 删除文件
     *
     * @param filePath 文件
     * @return
     */
    public static boolean deleteFile(String filePath) {
        boolean flag = false;
        File file = new File(filePath);
        // 路径为文件且不为空则进行删除
        if (file.isFile() && file.exists()) {
            file.delete();
            flag = true;
        }
        return flag;
    }

    /**
     * 文件名称验证
     *
     * @param filename 文件名称
     * @return true 正常 false 非法
     */
    public static boolean isValidFilename(String filename) {
        return filename.matches(FILENAME_PATTERN);
    }

    /**
     * 下载文件名重新编码
     *
     * @param request  请求对象
     * @param fileName 文件名
     * @return 编码后的文件名
     */
    public static String setFileDownloadHeader(HttpServletRequest request, String fileName)
            throws UnsupportedEncodingException {
        final String agent = request.getHeader("USER-AGENT");
        String filename = fileName;
        if (agent.contains("MSIE")) {
            // IE浏览器
            filename = URLEncoder.encode(filename, "utf-8");
            filename = filename.replace("+", " ");
        } else if (agent.contains("Firefox")) {
            // 火狐浏览器
            filename = new String(fileName.getBytes(), "ISO8859-1");
        } else if (agent.contains("Chrome")) {
            // google浏览器
            filename = URLEncoder.encode(filename, "utf-8");
        } else {
            // 其它浏览器
            filename = URLEncoder.encode(filename, "utf-8");
        }
        return filename;
    }

//    /**
//     * 指定图片宽度和高度和压缩比例对图片进行压缩
//     *
//     * @param imgsrc     源图片地址
//     * @param imgdist    目标图片地址
//     * @param widthdist  压缩后图片的宽度
//     * @param heightdist 压缩后图片的高度
//     * @param rate       压缩的比例
//     */
//    public static void reduceImg(String imgsrc, String imgdist, int widthdist, int heightdist, Float rate) {
//        try {
//            File srcfile = new File(imgsrc);
//            // 检查图片文件是否存在
//            if (!srcfile.exists()) {
//                System.out.println("文件不存在");
//            }
//            // 如果比例不为空则说明是按比例压缩
//            if (rate != null && rate > 0) {
//                //获得源图片的宽高存入数组中
//                int[] results = getImgWidthHeight(srcfile);
//                if (results == null || results[0] == 0 || results[1] == 0) {
//                    return;
//                } else {
//                    //按比例缩放或扩大图片大小，将浮点型转为整型
//                    widthdist = (int) (results[0] * rate);
//                    heightdist = (int) (results[1] * rate);
//                }
//            }
//            // 开始读取文件并进行压缩
//            Image src = ImageIO.read(srcfile);
//
//            // 构造一个类型为预定义图像类型之一的 BufferedImage
//            BufferedImage tag = new BufferedImage(widthdist, heightdist, BufferedImage.TYPE_INT_RGB);
//
//            //绘制图像  getScaledInstance表示创建此图像的缩放版本，返回一个新的缩放版本Image,按指定的width,height呈现图像
//            //Image.SCALE_SMOOTH,选择图像平滑度比缩放速度具有更高优先级的图像缩放算法。
//            tag.getGraphics().drawImage(src.getScaledInstance(widthdist, heightdist, Image.SCALE_SMOOTH), 0, 0, null);
//
//            //创建文件输出流
//            FileOutputStream out = new FileOutputStream(imgdist);
//            //将图片按JPEG压缩，保存到out中
//            JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
//            encoder.encode(tag);
//            //关闭文件输出流
//            out.close();
//        } catch (Exception ef) {
//            ef.printStackTrace();
//        }
//    }

    /**
     * 获取图片宽度和高度
     *
     * @param file
     * @return 返回图片的宽度
     */
    private static int[] getImgWidthHeight(File file) {
        InputStream is = null;
        BufferedImage src = null;
        int[] result = {0, 0};
        try {
            // 获得文件输入流
            is = new FileInputStream(file);
            // 从流里将图片写入缓冲图片区
            src = ImageIO.read(is);
            result[0] = src.getWidth(null); // 得到源图片宽
            result[1] = src.getHeight(null);// 得到源图片高
            is.close();  //关闭输入流
        } catch (Exception ef) {
            ef.printStackTrace();
        }

        return result;
    }

    /**
     * 文件重命名
     *
     * @param path
     * @param newname
     * @return
     */
    public File reName(String path, String newname) {
        //Scanner scanner=new Scanner(System.in);
        File file = new File(path);
        if (file.exists()) {
            File newfile = new File(file.getParent() + File.separator + newname);//创建新名字的抽象文件
            if (file.renameTo(newfile)) {
                System.out.println("重命名成功！");
                return newfile;
            } else {
                System.out.println("重命名失败！新文件名已存在");
                return file;
            }
        } else {
            System.out.println("重命名文件不存在！");
            return file;
        }

    }


    /**
     * 复制文件到另一个路径
     *
     * @param fileFrom 源文件
     * @param fileTo   拷贝后的文件
     * @return
     */
    public static boolean copeFileWithPath(File fileFrom, File fileTo) {
        BufferedInputStream bis = null;
        BufferedOutputStream bos = null;
        try {
            bis = new BufferedInputStream(
                    new FileInputStream(fileFrom));
            bos = new BufferedOutputStream(
                    new FileOutputStream(fileTo));

            byte[] byts = new byte[1024];
            int len = 0;
            while ((len = bis.read(byts, 0, byts.length)) != -1) {
                bos.write(byts, 0, len);
            }
        } catch (FileNotFoundException e) {
            log.error("复制文件到另一个路径-FileNotFoundException{}", e);
            return false;
        } catch (IOException e) {
            log.error("复制文件到另一个路径-IOException{}", e);
            return false;
        } finally {
            if (bis != null) {
                try {
                    bis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            try {
                bos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return true;
        }
    }


    /**
     * 复制文件到另一个路径 网络流文件
     *
     * @param fileFrom 源文件
     * @param fileTo   拷贝后的文件
     * @return
     */
    public static boolean copeFileWithPathNetUrl(String fileFrom, File fileTo) {

        FileOutputStream fos = null;
        BufferedInputStream bis = null;
        HttpURLConnection httpUrl = null;
        URL url = null;
        int BUFFER_SIZE = 1024;
        byte[] buf = new byte[BUFFER_SIZE];
        int size = 0;
        try {
            url = new URL(fileFrom);
            httpUrl = (HttpURLConnection) url.openConnection();
            httpUrl.connect();
            bis = new BufferedInputStream(httpUrl.getInputStream());
            fos = new FileOutputStream(fileTo);
            while ((size = bis.read(buf)) != -1) {
                fos.write(buf, 0, size);
            }
            fos.flush();
        } catch (FileNotFoundException e) {
            log.error("复制文件到另一个路径-FileNotFoundException{}", e);
            return false;
        } catch (IOException e) {
            log.error("复制文件到另一个路径-IOException{}", e);
            return false;
        } catch (ClassCastException e) {
            log.error("复制文件到另一个路径-ClassCastException{}", e);
            return false;
        } finally {
            try {
                fos.close();
                bis.close();
                httpUrl.disconnect();
            } catch (IOException e) {
            } catch (NullPointerException e) {
            }

            return true;
        }

    }

//    /**
//     * 截取文件券路径的基本路径 https://gxfacepay.oss-cn-shenzhen.aliyuncs.com/merchant
//     * @param url
//     * @param baseUrl
//     * @return
//     */
//    public static String replaceDomain2(String url,String baseUrl) {
//        if(isNetUrl(url)){
//            if(url.contains(Global.getAdPath())){
//                return url.replace(baseUrl+Global.getAdPath().replace("/", ""), "");
//            }else if(url.contains(Global.getAvatarPath())){
//                return url.replace(baseUrl+Global.getAvatarPath().replace("/", ""), "");
//            }else if(url.contains(Global.getDownloadPath())){
//                return url.replace(baseUrl+Global.getDownloadPath().replace("/", ""), "");
//            }else if(url.contains(Global.getTenantPath())){
//                return url.replace(baseUrl+Global.getTenantPath().replace("/", ""), "");
//            }else if(url.contains(Global.getUploadPath())){
//                return url.replace(baseUrl+Global.getUploadPath().replace("/", ""), "");
//            }else if(url.contains(Global.getMerchantPath())){
//                return url.replace(baseUrl+Global.getMerchantPath().replace("/", ""), "");
//            }else if(url.contains(Global.getStorePath())){
//                return url.replace(baseUrl+Global.getStorePath().replace("/", ""), "");
//            }else if(url.contains(Global.getNoticePath())){
//                return url.replace(baseUrl+Global.getNoticePath().replace("/", ""), "");
//            }else if(url.contains(Global.getMerchantAdPath())){
//                return url.replace(baseUrl+Global.getMerchantAdPath().replace("/", ""), "");
//            }else if(url.contains(Global.getMemberLogoPath())){
//                return url.replace(baseUrl+Global.getMemberLogoPath().replace("/", ""), "");
//            }else if(url.contains(Global.getDevicePath())){
//                return url.replace(baseUrl+Global.getDevicePath().replace("/", ""), "");
//            }
//        }
//        return url;
//    }

    public static boolean isNetUrl(String url) {
        boolean reault = false;
        if (url != null) {
            if (url.toLowerCase().startsWith("http") || url.toLowerCase().startsWith("https") || url.toLowerCase().startsWith("rtsp") || url.toLowerCase().startsWith("mms")) {
                reault = true;
            }
        }
        return reault;
    }

}
