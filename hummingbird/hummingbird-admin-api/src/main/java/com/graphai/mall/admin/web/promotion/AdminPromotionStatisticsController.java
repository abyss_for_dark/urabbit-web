package com.graphai.mall.admin.web.promotion;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.mall.admin.util.StringUtils;
import com.graphai.mall.db.service.statistics.MallPromotionStatisticsService;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;


/**
 * 推广统计
 *
 * @author LaoGF
 * @since 2020-07-20
 */
@RestController
@RequestMapping("/admin/promotionStatistics")
@Validated
public class AdminPromotionStatisticsController {

    @Resource
    private MallPromotionStatisticsService pallPromotionStatisticsService;

    /**
     * 统计注册、Android、IOS激活、购卡量、收益
     */
    @RequiresPermissions("admin:promotionStatistics:statisticsAll")
    @RequiresPermissionsDesc(menu = {"数据统计", "推广统计"}, button = "查询")
    @GetMapping("/statisticsAll")
    public Object statisticsAll(@RequestParam(value = "channelId", required = false) String channelId,
                                @RequestParam(value = "channelName", required = false) String channelName,
                                @RequestParam(value = "beginDate", required = false) String beginDate,
                                @RequestParam(value = "endDate", required = false) String endDate,
                                @RequestParam(value = "day", required = false) Integer day) {
        if(!StringUtils.isEmpty(beginDate)){
            day = null;
        }
        return ResponseUtil.ok(pallPromotionStatisticsService.statisticsAllCount(channelId, channelName, beginDate, endDate, day));
    }

    /**
     * 统计注册、Android、IOS激活、购卡量（按天数）
     */
    @GetMapping("/statisticsAllTrend")
    public Object statisticsTotalUserCount(@RequestParam(value = "channelId", required = false) String channelId) {
        return ResponseUtil.ok(pallPromotionStatisticsService.statisticsAllCountGroupByDay(channelId));
    }

    /**
     * 统计注册用户量(按渠道)
     */
    @GetMapping("/registerUserCount")
    public Object statisticsRegisterUserCount() {
        return ResponseUtil.ok(pallPromotionStatisticsService.statisticsRegisterUserCount(null, null, null, null, null));
    }

    /**
     * 统计Android激活用户量(按渠道)
     */
    @GetMapping("/androidUserCount")
    public Object statisticsAndroidUserCount() {
        return ResponseUtil.ok(pallPromotionStatisticsService.statisticsAndroidUserCount(null, null, null, null, null));
    }

    /**
     * 统计IOS激活用户量(按渠道)
     */
    @GetMapping("/iosUserCount")
    public Object statisticsIosUserCount() {
        return ResponseUtil.ok(pallPromotionStatisticsService.statisticsIosUserCount(null, null, null, null, null));
    }

    /**
     * 查询所有渠道
     */
    @GetMapping("/selectChannel")
    public Object selectChannel() {
        return ResponseUtil.ok(pallPromotionStatisticsService.selectChannel());
    }


}
