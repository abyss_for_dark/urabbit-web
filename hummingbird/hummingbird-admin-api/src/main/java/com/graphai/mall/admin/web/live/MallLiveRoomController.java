package com.graphai.mall.admin.web.live;

import cn.afterturn.easypoi.excel.entity.ExportParams;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.lang.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.graphai.commons.util.servlet.ServletUtils;
import com.graphai.framework.system.service.IGraphaiSystemRoleService;
import com.graphai.framework.system.service.MallRoleService;
import com.graphai.mall.admin.domain.MallActivityGoodsProduct;
import com.graphai.mall.admin.domain.MallLiveRoomProduct;
import com.graphai.mall.admin.domain.MallMerchant;
import com.graphai.mall.admin.domain.MallUserMerchant;
import com.graphai.mall.admin.service.IMallActivityGoodsProductService;
import com.graphai.mall.admin.service.IMallLiveRoomProductService;
import com.graphai.mall.admin.service.IMallUserMerchantService;
import com.graphai.mall.db.domain.MallAdmin;
import com.graphai.mall.db.domain.MallRole;
import com.graphai.mall.db.util.LiveUtil;
import com.graphai.mall.live.domain.MallLiveRoomUser;
import com.graphai.mall.live.service.IMallLiveRoomUserService;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import org.springframework.web.bind.annotation.RestController;
import com.graphai.framework.generator.controller.IBaseController;

import com.graphai.mall.live.domain.MallLiveRoom;
import com.graphai.mall.live.service.IMallLiveRoomService;

import java.time.LocalDateTime;
import java.util.*;


/**
 * <p>
 * 直播间 表 前端控制器
 * </p>
 *
 * @author biteam
 * @since 2021-06-29
 */
@RestController
@RequestMapping("/admin/live/live")
public class MallLiveRoomController extends IBaseController<MallLiveRoom,  String >  {
	@Autowired
    private IMallLiveRoomService serviceImpl;
	@Autowired
    private IMallLiveRoomProductService mallLiveRoomProductService;
    @Autowired
    private IMallActivityGoodsProductService mallActivityGoodsProductService;
    @Autowired
    private MallRoleService mallRoleService;
    @Autowired
    private IMallLiveRoomUserService mallLiveRoomUserService;
    
    @RequiresPermissions("admin:live:live:single")
    @RequiresPermissionsDesc(menu = {"单独查询"}, button = "单独查询")
	@GetMapping("/single/{id}")
    public Object getEntityById(@PathVariable  String  id) throws Exception{
        MallLiveRoom entityVName = serviceImpl.getById(id);

        // 当前登录角色
        MallAdmin adminUser = (MallAdmin) SecurityUtils.getSubject().getPrincipal();
        MallRole role = (MallRole)mallRoleService.findByIds(adminUser.getRoleIds()).toArray()[0];

        // 商品列表
        QueryWrapper<MallLiveRoomProduct> mallLiveRoomProductQueryWrapper = new QueryWrapper<>();
        mallLiveRoomProductQueryWrapper.eq("room_id", id);
        List<MallLiveRoomProduct> mallLiveRoomProduct = mallLiveRoomProductService.list(mallLiveRoomProductQueryWrapper);

        // 商品规格列表
        List<MallActivityGoodsProduct> mallActivityGoodsProduct = new ArrayList<>();
        for (MallLiveRoomProduct temp: mallLiveRoomProduct) {
            QueryWrapper<MallActivityGoodsProduct> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("activity_type", 1);
            queryWrapper.eq("activity_rule_id", temp.getId());
            List<MallActivityGoodsProduct> tempMallActivityGoodsProduct = mallActivityGoodsProductService.list(queryWrapper);

            for (MallActivityGoodsProduct temp2:tempMallActivityGoodsProduct) {
                temp2.setGoodsName(temp.getName());
            }
            mallActivityGoodsProduct.addAll(tempMallActivityGoodsProduct);
        }

        Map<String,Object> map = new HashMap<>();

        Boolean isAdmin = true;
        if(!role.getRoleKey().equals("admin")){
            isAdmin = false;
        }
        map.put("isAdmin",isAdmin);
        map.put("entityVName",entityVName);
        map.put("mallActivityGoodsProduct",mallActivityGoodsProduct);
        return ResponseUtil.ok(map);
    }
    
    @RequiresPermissions("admin:live:live:addOrUpdate")
    @RequiresPermissionsDesc(menu = {"新增"}, button = "新增")
    @PostMapping("/addOrUpdate")
    public Object addOrUpdate(MallLiveRoom mallLiveRoom) throws Exception{
        if(serviceImpl.saveOrUpdate(mallLiveRoom)){
            return ResponseUtil.ok();
        }else{
            return ResponseUtil.fail();
        }
    } 
	
	
	@RequiresPermissions("admin:live:live:melist")
	@RequiresPermissionsDesc(menu = {"直播间 表查询"}, button = "查询")
    @GetMapping("/melist")
    public Object getMeList() throws Exception {
        IPage<MallLiveRoom> ipage = serviceImpl.page(getPlusPage(),listQueryWrapper());
        return ResponseUtil.ok(ipage);
    }
 	
 	private QueryWrapper<MallLiveRoom> listQueryWrapper() {
        MallAdmin adminUser = (MallAdmin) SecurityUtils.getSubject().getPrincipal();
        MallRole role = (MallRole)mallRoleService.findByIds(adminUser.getRoleIds()).toArray()[0];
 		QueryWrapper<MallLiveRoom> meq = new QueryWrapper<>();
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("id"))) {
		      meq.like("id", ServletUtils.getParameter("id"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("roomStatus"))) {
		      meq.eq("room_status", ServletUtils.getParameter("roomStatus"));
		}
        if(!role.getRoleKey().equals("admin")){
            meq.eq("add_id", adminUser.getUserId());
        }
		
        return meq;
    }
 	@RequiresPermissions("admin:live:live:save")
 	@RequiresPermissionsDesc(menu = {"直播间 表新增"}, button = "新增")
    @PostMapping("/save")
    public Object save(@ModelAttribute MallLiveRoom mallLiveRoom) throws Exception {
        MallAdmin adminUser = (MallAdmin) SecurityUtils.getSubject().getPrincipal();

        mallLiveRoom.setAddId(adminUser.getId());
        mallLiveRoom.setAddTime(LocalDateTime.now());
        serviceImpl.save(mallLiveRoom);
        return ResponseUtil.ok();
    }

    /**
     * 目前用来查看详情
     * @param mallLiveRoom
     * @return
     */
	@RequiresPermissions("admin:live:live:modify")
	@RequiresPermissionsDesc(menu = {"直播间 表修改"}, button = "修改")
    @PutMapping("/modify")
    public Object modify(@ModelAttribute  MallLiveRoom mallLiveRoom){
        serviceImpl.updateById(mallLiveRoom);
        return  ResponseUtil.ok();
    }

    @RequiresPermissions("admin:live:live:updateGoods")
    @RequiresPermissionsDesc(menu = {"直播间 商品修改"}, button = "修改")
    @PutMapping("/updateGoods")
    public Object updateGoods(@ModelAttribute  MallActivityGoodsProduct mallActivityGoodsProduct){
        MallActivityGoodsProduct temp = mallActivityGoodsProductService.getById(mallActivityGoodsProduct.getId());
        temp.setNumber(mallActivityGoodsProduct.getNumber());
        temp.setActivityPrice(mallActivityGoodsProduct.getActivityPrice());
        mallActivityGoodsProductService.updateById(temp);
        return  ResponseUtil.ok();
    }

    @RequiresPermissions("admin:live:live:del")
    @RequiresPermissionsDesc(menu = {"直播间 表删除"}, button = "删除")
    @DeleteMapping("/del/{id}")
    public Object deleteById(@PathVariable  String  id){
        serviceImpl.removeById(id);
        return  ResponseUtil.ok();
    }

    @RequiresPermissions("admin:live:live:stopLive")
    @RequiresPermissionsDesc(menu = {"直播间 强制停播"}, button = "修改")
    @PutMapping("/stopLive/{id}")
    public Object stopLive(@PathVariable String id){
        MallLiveRoom mallLiveRoom = serviceImpl.getById(id);

        // 停止推流
        LiveUtil.stopPush(id);

        // 修改直播间下播信息
        mallLiveRoom.setRoomStatus("3");
        mallLiveRoom.setOpenTotal(mallLiveRoom.getOpenTotal()+1);
        mallLiveRoom.setPushUrl("");
        mallLiveRoom.setPlayUrl("");
        mallLiveRoom.setUpdateTime(LocalDateTime.now());
        mallLiveRoom.setEndLiveAt(LocalDateTime.now());
        serviceImpl.updateById(mallLiveRoom);

        QueryWrapper<MallLiveRoomUser> mallLiveRoomUserQueryWrapper = new QueryWrapper<MallLiveRoomUser>();
        mallLiveRoomUserQueryWrapper.eq("room_id", mallLiveRoom.getId());
        mallLiveRoomUserService.remove(mallLiveRoomUserQueryWrapper);
        return  ResponseUtil.ok();
    }

}

