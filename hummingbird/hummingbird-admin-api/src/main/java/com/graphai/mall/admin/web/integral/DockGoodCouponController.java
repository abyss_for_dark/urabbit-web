package com.graphai.mall.admin.web.integral;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.httprequest.HttpUtil;
import com.graphai.mall.db.domain.MallAdmin;
import com.graphai.mall.db.domain.MallGoods;
import com.graphai.mall.db.domain.mall.GoodsAllinone;
import com.graphai.mall.db.service.integral.DockGoodCouponService;
import com.graphai.mall.db.service.integral.MallGoodsIntegralService;

import net.sf.json.JSONArray;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

//import com.graphai.mall.db.service.mall.LitemmallMerchantConfigService;

/**
 * Created by Qxian on 2020-10-20 10:05
 */
@RestController
@RequestMapping("/admin/dockPayWithGoodCoupon")
public class DockGoodCouponController extends BaseIntegralController {

    @Autowired
    private DockGoodCouponService dockGoodCouponService;
    @Autowired
    private MallGoodsIntegralService goodsIntegralService;
//    @Autowired
//    private LitemmallMerchantConfigService litemmallMerchantConfigService;


    /**
     * 百变科技商户（id、名称）列表
     *
     * @param tenantId
     * @return
     */
    @PostMapping("/merchantList")
    public Object merchantList(@RequestParam(defaultValue = "BBKJ") String tenantId) {
        String url = "http://8.129.216.154:9022/api/expose/mall/merchantList";
        Map<String, String> params = new HashMap<String, String>();
        params.put("tenantId", tenantId);
        String result = HttpUtil.sendPost(url, params);
        JSONArray jsonArray = JSONArray.fromObject(result);//String转为json数组  
        return ResponseUtil.ok(jsonArray);
    }

    /**
     * 上下架商品
     *
     * @param MallGoods
     * @return
     */
    @PostMapping("/updateGoods")
    public Object updateGoods(@RequestBody MallGoods MallGoods) {
        Object error = validateGoods(MallGoods);
        if (error != null) {
            return error;
        }
        if (goodsIntegralService.updateById(MallGoods) == 0) {
            return ResponseUtil.updatedDataFailed();
        }
        return ResponseUtil.ok(MallGoods);
    }

    /**
     * 更新商品/优惠券信息
     *
     * @param goodsAllinone
     * @return
     */
    @ResponseBody
    @PostMapping("/updateGoodsCoupon")
    public Object updateGoodsCoupon(@RequestBody GoodsAllinone goodsAllinone) {
        return dockGoodCouponService.updateGoodsCoupon(goodsAllinone);
    }

    /**
     * 优惠券详情 (包含对应商品信息)
     *
     * @param MallGoods
     * @return
     */
    @PostMapping("/couponDetail")
    public Object couponDetail(@RequestBody MallGoods MallGoods) {
        return dockGoodCouponService.couponDetail(MallGoods);
    }

    /**
     * 添加商品/优惠券信息
     *
     * @param goodsAllinone
     * @return
     */
    @ResponseBody
    @PostMapping("/createGoodsCoupon")
    public Object createGoodsCoupon(@RequestBody GoodsAllinone goodsAllinone) {
        //获取当前用户
        MallAdmin adminUser = (MallAdmin) SecurityUtils.getSubject().getPrincipal();
        String deptId = "";
        if (adminUser != null) {
            deptId = adminUser.getDeptId();
        }
        return dockGoodCouponService.createGoodsCoupon(goodsAllinone,deptId);
    }


    private Object validateGoods(MallGoods MallGoods) {
        String goodsId = MallGoods.getId();
        if ("0".equals(goodsId) || null == goodsId) {
            return ResponseUtil.badArgument();
        }
        return null;
    }


}

