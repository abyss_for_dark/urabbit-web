package com.graphai.mall.admin.dto;

import com.graphai.framework.annotation.Excel;

/**
 * Created by Administrator on 2020-06-30 15:06
 */
public class MallGoodsCouponCodeImport {

    @Excel(name = "券码卡号")
    private String couponCard;

    @Excel(name = "券码卡密")
    private String couponCode;

    @Excel(name = "电子码链接")
    private String link;

    public String getCouponCard() {
        return couponCard;
    }

    public void setCouponCard(String couponCard) {
        this.couponCard = couponCard;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
