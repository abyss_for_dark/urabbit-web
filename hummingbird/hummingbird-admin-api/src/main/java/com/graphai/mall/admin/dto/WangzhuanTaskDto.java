package com.graphai.mall.admin.dto;

import com.graphai.mall.db.domain.WangzhuanTask;
import com.graphai.mall.db.domain.WangzhuanTaskStep;
import com.graphai.mall.db.domain.WangzhuanTaskStepWithBLOBs;
import lombok.Data;

import java.util.List;

@Data
public class WangzhuanTaskDto {

    private String id;
    private String taskName;
    private String taskCategory;
    private String remark;
    private String duration;
    private String auditDuration;
    private String amount;
    private String stockNumber;
    private String taskType;
    private String launchSettings;
    private Region region ;
    private List<WangzhuanTaskStepDto> stepList;

    private List<WangzhuanTaskStep> wangzhuanTaskSteps;

    private String step1;
    private String step2;
    private String step3;
    private String subName;
    private String imageUrl;
    private String tstatus;
    private String sort;
    private String androidDownloadUrl;
    private String iosDownloadUrl;
    private String taskNotice1;
    private String taskNotice2;
    private String taskStep;
    private String successLv;
    private Object[] picUrls;
    private Object[] Urls;

    private String productId;

    private WangzhuanTask wangzhuanTask;

    private List<WangzhuanTaskStepWithBLOBs> wangzhuanTaskStepList;

    @Data
    public class Region {
        String provinceCode;
        String cityCode;
        String areaCode;
        String province;
        String city;
        String county;

    }
}
