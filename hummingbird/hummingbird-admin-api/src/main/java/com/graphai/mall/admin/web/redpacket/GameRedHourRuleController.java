package com.graphai.mall.admin.web.redpacket;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.lang.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.graphai.commons.util.servlet.ServletUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.framework.generator.controller.IBaseController;

import com.graphai.mall.admin.domain.MallSystemRule;
import com.graphai.mall.admin.service.IGameRedHourRuleService;

import java.time.LocalDateTime;
import java.util.List;


/**
 * <p>
 * 整点红包规则表 前端控制器
 * </p>
 *
 * @author biteam
 * @since 2021-01-13
 */
@RestController
@RequestMapping("/admin/rule")
public class GameRedHourRuleController extends IBaseController<MallSystemRule,  String >  {

	@Autowired
    private IGameRedHourRuleService serviceImpl;
    
    @RequiresPermissions("admin:rule:single")
    @RequiresPermissionsDesc(menu = {"单独查询"}, button = "单独查询")
	@GetMapping("/single/{id}")
    public Object getEntityById(@PathVariable  String  id) throws Exception{
        MallSystemRule entityVName = serviceImpl.getById(id);
        return ResponseUtil.ok(entityVName);
    }
    
    @RequiresPermissions("admin:rule:addOrUpdate")
    @RequiresPermissionsDesc(menu = {"新增"}, button = "新增")
    @PostMapping("/addOrUpdate")
    public Object addOrUpdate(MallSystemRule mallSystemRule) throws Exception{
        if(serviceImpl.saveOrUpdate(mallSystemRule)){
            return ResponseUtil.ok();
        }else{
            return ResponseUtil.fail();
        }
    } 
	
	
	@RequiresPermissions("admin:rule:melist")
	@RequiresPermissionsDesc(menu = {"整点红包规则表查询"}, button = "查询")
    @GetMapping("/melist")
    public Object getMeList() throws Exception {
        IPage<MallSystemRule> ipage = serviceImpl.page(getPlusPage(),listQueryWrapper());
        return ResponseUtil.ok(ipage);
    }
 	
 	private QueryWrapper<MallSystemRule> listQueryWrapper() {
 		QueryWrapper<MallSystemRule> meq = new QueryWrapper<>();
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("ruleId"))) {
		      meq.eq("rule_id", ServletUtils.getParameter("ruleId"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("status"))) {
		      meq.eq("status", ServletUtils.getParameter("status"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("addTime"))) {
		      meq.eq("add_time", ServletUtils.getLocalDateTime("addTime"));  
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("startRed"))) {
		      meq.eq("start_red", ServletUtils.getParameter("startRed"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("endRed"))) {
		      meq.eq("end_red", ServletUtils.getParameter("endRed"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("proportion"))) {
		      meq.eq("proportion", ServletUtils.getParameter("proportion"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("shareProportion"))) {
		      meq.eq("share_proportion", ServletUtils.getParameter("shareProportion"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("reserveOne"))) {
		      meq.eq("reserve_one", ServletUtils.getParameter("reserveOne"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("reserveTwo"))) {
		      meq.eq("reserve_two", ServletUtils.getParameter("reserveTwo"));
		}
		
        return meq;
    }
 	@RequiresPermissions("admin:rule:save")
 	@RequiresPermissionsDesc(menu = {"整点红包规则表新增"}, button = "新增")
    @PostMapping("/save")
    public Object save(@ModelAttribute MallSystemRule mallSystemRule) throws Exception {
        serviceImpl.save(mallSystemRule);
        return ResponseUtil.ok();
    }
	
	@RequiresPermissions("admin:rule:modify")
	@RequiresPermissionsDesc(menu = {"整点红包规则表修改"}, button = "修改")
    @PostMapping("/modify")
    public Object modify(@RequestBody List<MallSystemRule> mallSystemRule){

        for(MallSystemRule mallSystemRule1 : mallSystemRule){
            if(StringUtils.isEmpty(mallSystemRule1.getId())){
                mallSystemRule1.setAddTime(LocalDateTime.now());
                mallSystemRule1.setUpdateTime(LocalDateTime.now());
                mallSystemRule1.setStatus("0");
                mallSystemRule1.setRuleId(1);
                serviceImpl.saveOrUpdate(mallSystemRule1);
            }else{
                serviceImpl.updateById(mallSystemRule1);
            }
        }
        return  ResponseUtil.ok();
    }
    
    @RequiresPermissions("admin:rule:del")
    @RequiresPermissionsDesc(menu = {"整点红包规则表删除"}, button = "删除")
    @DeleteMapping("/del/{id}")
    public Object deleteById(@PathVariable  String  id){
        serviceImpl.removeById(id);
        return  ResponseUtil.ok();
    }
    
}

