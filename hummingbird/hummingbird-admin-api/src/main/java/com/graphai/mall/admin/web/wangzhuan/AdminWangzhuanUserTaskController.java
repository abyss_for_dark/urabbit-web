package com.graphai.mall.admin.web.wangzhuan;

import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.mall.admin.vo.WangzhuanUserTaskVo;
import com.graphai.mall.db.constant.wangzhuan.WangzhuanTaskEnum;
import com.graphai.mall.db.constant.wangzhuan.WangzhuanUserTaskEnum;
import com.graphai.mall.db.dao.WangzhuanUserTaskMapper;
import com.graphai.mall.db.domain.*;
import com.graphai.mall.db.service.account.FcAccountRechargeBillService;
import com.graphai.mall.db.service.wangzhuan.WangzhuanTaskService;
import com.graphai.mall.db.service.wangzhuan.WangzhuanUserTaskService;
import com.graphai.mall.db.service.wangzhuan.WxWangzhuanTaskRedisService;
import com.graphai.mall.db.util.account.task.AccountRWUtils;
import com.graphai.mall.db.constant.CommonConstant;

import com.graphai.mall.db.util.phone.PushUtils;
import com.graphai.validator.Order;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * (网赚)领取个人任务记录Controller
 *
 * @author ruoyi
 * @date 2020-04-08
 */
@RestController
@RequestMapping("/admin/userTask")
@Validated
public class AdminWangzhuanUserTaskController {
    private final Log logger = LogFactory.getLog(AdminWangzhuanUserTaskController.class);

    @Autowired
    private WangzhuanTaskService wangzhuanTaskService;
    @Autowired
    private WangzhuanUserTaskService wangzhuanUserTaskService;

    @Autowired
    private FcAccountRechargeBillService fcAccountRechargeBillService;

    @Autowired
    private WxWangzhuanTaskRedisService wxWangzhuanTaskRedisService;

    @Resource
    private WangzhuanUserTaskMapper wangzhuanUserTaskMapper;
    /**
     *
     *
     * @param name      任务名称或用户名称
     * @param taskType  投放类型
     * @param status    任务状态
     * @param begin     创建时间起始
     * @param end       创建时间结束
     * @param huid      平台ID
     * @param page      页码
     * @param limit     每页数量
     * @param sort      排序
     * @return
     */
    @RequiresPermissions("admin:userTask:selectList")
    @RequiresPermissionsDesc(menu = {"任务管理", "任务审核"}, button = "查询")
    @GetMapping("/selectList")
    public Object selectWangZhuanUserTaskList(String name, String taskType,
                                              String status, LocalDateTime begin,
                                              LocalDateTime end, String huid,
                                              String taskCategory,String taskId,
                                              @RequestParam(defaultValue = "1") Integer page,
                                              @RequestParam(defaultValue = "10") Integer limit,
                                              @RequestParam(defaultValue = "id") String sort) {

        List<WangzhuanUserTaskVo> list = wangzhuanTaskService.selectWangZhuanUserTaskList(huid, name, taskType, status, taskCategory, begin, end, (page-1)* limit, limit, sort,taskId);

        Long total = wangzhuanTaskService.selectWangZhuanUserTaskCount(huid, name, taskType, status, taskCategory, begin, end,taskId);

        Map<String, Object> data = new HashMap<>();
        data.put("total", total);
        data.put("items", list);

        return ResponseUtil.ok(data);

    }


    /**
     * 用户完成任务审核
     *
     * @param wangzhuanUserTask
     * @return
     * @throws Exception
     */
    @RequiresPermissions("admin:userTask:Adopt")
    @RequiresPermissionsDesc(menu = {"任务管理", "任务审核"}, button = "审核")
    @PostMapping("/Adopt")
    public Object editSave(@RequestBody WangzhuanUserTask wangzhuanUserTask) throws Exception {
        Object error = validate(wangzhuanUserTask);
        if (error != null) {
            return error;
        }
        WangzhuanUserTask task = new WangzhuanUserTask();
        //设置状态
        task.setTstatus(wangzhuanUserTask.getTstatus());
        task.setUpdateTime(LocalDateTime.now());
        //获取任务详情
        WangzhuanTask wangzhuanTask = wangzhuanTaskService.selectWangzhuanTaskById(wangzhuanUserTask.getTaskId());
        //驳回
        if (task.getTstatus().equals(CommonConstant.TASK_STATUS_NO_AUDIT)) {
            //更新驳回理由
            task.setRemarks(wangzhuanUserTask.getRemarks());
            //驳回，库存加一
            int stockNumber = wangzhuanTask.getStockNumber();
            wangzhuanTask.setStockNumber(stockNumber + 1);
            DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            LocalDateTime time = LocalDateTime.now();
            String localTime = df.format(time);
            // 任务 - 通知 push
            String content = "很抱歉~ 您领取的公益任务："+wangzhuanTask.getTaskName()+"被驳回了。原因是："+wangzhuanUserTask.getRemarks()+" --- " + localTime;
            PushUtils.pushMessage(wangzhuanUserTask.getUserId(), "系统通知", content, 2, 0);
        }
        //通过
        else if (task.getTstatus().equals(CommonConstant.TASK_STATUS_AUDIT)) {
            //这里先不处理，即，后台先不管钱。
            //通过，完成数量加一
            int completedQuantity = wangzhuanTask.getCompletedQuantity();
            wangzhuanTask.setCompletedQuantity(completedQuantity + 1);
            ///这里要加redis
            Map<String, String> map = new HashMap<>();
            map.put("id",wangzhuanUserTask.getId());
            map.put("userId", wangzhuanUserTask.getUserId());
            map.put("nickName", wangzhuanUserTask.getNickname());
            map.put("taskId", wangzhuanUserTask.getTaskId());
            map.put("taskCategory", wangzhuanUserTask.getTaskCategory());
            map.put("reward", wangzhuanUserTask.getReward().toString());
            map.put("avatar", wangzhuanUserTask.getAvatar());
            wxWangzhuanTaskRedisService.setTaskRandingRedis(map);
            DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            LocalDateTime time = LocalDateTime.now();
            String localTime = df.format(time);
            // 任务 - 通知 push
            String content = "恭喜您~  您领取的公益任务："+wangzhuanTask.getTaskName()+"审核通过了。  ---  "+ localTime;
            PushUtils.pushMessage(wangzhuanUserTask.getUserId(), "系统通知", content, 2, 0);

            // 信用分充值方法。
            AccountRWUtils.addCreditScoreByWZGYTask(wangzhuanUserTask.getUserId(),wangzhuanUserTask.getReward());
        }
        task.setId(wangzhuanUserTask.getId());
        int result = wangzhuanUserTaskMapper.updateByPrimaryKeySelective(task);
        if(result == 1) {
            wangzhuanTaskService.updateWangzhuanTask(wangzhuanTask);
        }
        return ResponseUtil.ok();
    }

    /**
     * 批量通过任务
     * @return
     * @throws Exception
     */
    @RequiresPermissions("admin:userTask:examineList")
    @RequiresPermissionsDesc(menu = {"任务管理", "任务审核"}, button = "批量通过")
    @PostMapping("/examineList")
    public Object examineList(@RequestBody List<WangzhuanUserTask> wangzhuanUserTasks ) throws Exception {

        for (WangzhuanUserTask wangzhuanUserTask : wangzhuanUserTasks) {
            Object error = validate(wangzhuanUserTask);
            if (error != null) {
                return error;
            }
            WangzhuanUserTask task = new WangzhuanUserTask();
            //设置状态
            task.setTstatus(wangzhuanUserTask.getTstatus());
            task.setUpdateTime(LocalDateTime.now());
            //获取任务详情
            WangzhuanTask wangzhuanTask = wangzhuanTaskService.selectWangzhuanTaskById(wangzhuanUserTask.getTaskId());
            //通过
                //这里先不处理，即，后台先不管钱。
                //通过，完成数量加一
                int completedQuantity = wangzhuanTask.getCompletedQuantity();
                wangzhuanTask.setCompletedQuantity(completedQuantity + 1);
                ///这里要加redis
                Map<String, String> map = new HashMap<>();
                map.put("id",wangzhuanUserTask.getId());
                map.put("userId", wangzhuanUserTask.getUserId());
                map.put("nickName", wangzhuanUserTask.getNickname());
                map.put("taskId", wangzhuanUserTask.getTaskId());
                map.put("taskCategory", wangzhuanUserTask.getTaskCategory());
                map.put("reward", wangzhuanUserTask.getReward().toString());
                map.put("avatar", wangzhuanUserTask.getAvatar());
                wxWangzhuanTaskRedisService.setTaskRandingRedis(map);
            DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            LocalDateTime time = LocalDateTime.now();
            String localTime = df.format(time);
                // 任务 - 通知 push
                String content = "恭喜您~  您领取的公益任务："+wangzhuanTask.getTaskName()+"审核通过了。  ---  "+ localTime;
                PushUtils.pushMessage(wangzhuanUserTask.getUserId(), "系统通知", content, 2, 0);

                // 信用分充值方法。
                AccountRWUtils.addCreditScoreByWZGYTask(wangzhuanUserTask.getUserId(),wangzhuanUserTask.getReward());
             WangzhuanUserTaskExample example = new WangzhuanUserTaskExample();
             example.createCriteria().andTstatusEqualTo(WangzhuanUserTaskEnum.TSTATUS_UST102.getCode()).andDeletedEqualTo(false).andIdEqualTo(wangzhuanUserTask.getId()).andTaskCategoryEqualTo(WangzhuanTaskEnum.TASK_CATEGORY_T01.getCode());
            int result = wangzhuanUserTaskMapper.updateByExampleSelective(task,example);
            if(result == 1) {
                wangzhuanTaskService.updateWangzhuanTask(wangzhuanTask);
            }
        }

        return ResponseUtil.ok();
    }

    /**
     * 查询(网赚)领取个人任务记录列表
     */

    @GetMapping("/list")
    public Object list(@RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit,
                       @RequestParam(defaultValue = "id") String sort,
                       @Order @RequestParam(defaultValue = "desc") String order) {
        WangzhuanUserTask wangzhuanUserTask = new WangzhuanUserTask();
        List<WangzhuanUserTask> list = wangzhuanUserTaskService.selectWangzhuanUserTaskList(wangzhuanUserTask, page, limit, sort, order);
        long total = PageInfo.of(list).getTotal();
        Map<String, Object> data = new HashMap<>();
        data.put("total", total);
        data.put("items", list);

        return ResponseUtil.ok(data);
    }



    /**
     * 查询领取个人任务记录
     */
    @GetMapping("/edit/{id}")
    public Object edit(@PathVariable("id") String id) {
        //todo 需要显示用户信息

        WangzhuanUserTask wangzhuanUserTask = wangzhuanUserTaskService.selectWangzhuanUserTaskById(id);
        return ResponseUtil.ok(wangzhuanUserTask);
    }



    /**
     * 校验数据(审核通过、不通过)
     */
    private Object validate(WangzhuanUserTask wangzhuanUserTask) {
        //主键
        if (StringUtils.isEmpty(wangzhuanUserTask.getId())) {
            return ResponseUtil.badArgument();
        }
        //状态
        if (StringUtils.isEmpty(wangzhuanUserTask.getTstatus())) {
            return ResponseUtil.badArgument();
        }
        //审核备注
//        if (StringUtils.isEmpty(wangzhuanUserTask.getRemarks())) {
//            return ResponseUtil.badArgument();
//        }
        return null;
    }

}
