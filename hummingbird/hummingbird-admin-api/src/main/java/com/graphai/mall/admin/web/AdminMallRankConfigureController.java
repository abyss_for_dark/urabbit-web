package com.graphai.mall.admin.web;

import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.mall.db.constant.account.AccountStatus;
import com.graphai.mall.db.domain.MallRankConfigure;
import com.graphai.mall.db.service.MallRankConfigureService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.util.account.AccountUtils;
import com.graphai.mall.db.constant.CommonConstant;
import com.graphai.mall.db.vo.MallUserRankVo;
import com.graphai.validator.Order;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 排行奖励金额配置Controller
 */
@RestController
@RequestMapping("/admin/mallRankConfigure")
@Validated
public class AdminMallRankConfigureController {
    private final Log logger = LogFactory.getLog(AdminMallRankConfigureController.class);

    @Autowired
    private MallRankConfigureService mallRankConfigureService;
    @Autowired
    private MallUserService mallUserService;

    /**
     * 查询 排行奖励金额配置
     */
    @GetMapping("/list")
    public Object list(@RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit,
                       @RequestParam(defaultValue = "order_num") String sort,
                       @Order @RequestParam(defaultValue = "asc") String order) {
        MallRankConfigure mallRankConfigure = new MallRankConfigure();
        List<MallRankConfigure> list = mallRankConfigureService.selectMallRankConfigureList(mallRankConfigure, page, limit, sort, order);
        long total = PageInfo.of(list).getTotal();
        Map<String, Object> data = new HashMap<>();
        data.put("total", total);
        data.put("items", list);

        return ResponseUtil.ok(data);
    }

    /**
     * 添加 排行奖励金额配置
     */
    @PostMapping("/add")
    public Object addSave(@RequestBody MallRankConfigure mallRankConfigure) {
        Object error = validate(mallRankConfigure);
        if (error != null) {
            return error;
        }
        mallRankConfigureService.insertMallRankConfigure(mallRankConfigure);
        return ResponseUtil.ok();
    }

    /**
     * 查询 排行奖励金额配置
     */
    @GetMapping("/edit/{id}")
    public Object edit(@PathVariable("id") String id) {
        MallRankConfigure mallRankConfigure = mallRankConfigureService.selectMallRankConfigureById(id);
        return ResponseUtil.ok(mallRankConfigure);
    }

    /**
     * 修改保存 排行奖励金额配置
     */
    @PostMapping("/edit")
    public Object editSave(@RequestBody MallRankConfigure mallRankConfigure) {
        //任务主键id
        if (StringUtils.isEmpty(mallRankConfigure.getId())) {
            return ResponseUtil.badArgument();
        }
        Object error = validate(mallRankConfigure);
        if (error != null) {
            return error;
        }

        mallRankConfigureService.updateMallRankConfigure(mallRankConfigure);
        return ResponseUtil.ok();
    }


    /**
     * 校验数据
     */
    private Object validate(MallRankConfigure mallRankConfigure) {
        if (mallRankConfigure.getAwardAmount() == null) {
            return ResponseUtil.badArgument();
        }
        return null;
    }


    /**
     * 查看上周排行list    todo 应该查询未发放的信息
     */
    @GetMapping("/rankList")
    public Object rankList() {
        LocalDate now = LocalDate.now();
        int value = now.getDayOfWeek().getValue();
        LocalDate endDate = now.minusDays(value);   //结束时间
        LocalDate beginDate = endDate.minusDays(6); //开始时间
        logger.info("开始时间： " + beginDate + ",结束时间： " + endDate);
        //上周排行榜
        List<MallUserRankVo> lastWeekRankList = mallUserService.rankList(null, beginDate, endDate);

        return ResponseUtil.ok(lastWeekRankList);
    }

    /**
     * 发放排行奖金   id:用户id
     */
    @PostMapping("/grantRankAward/{id}")
    public Object grantRankAward(@PathVariable("id") String id) throws Exception {
        LocalDate now = LocalDate.now();
        int value = now.getDayOfWeek().getValue();
        LocalDate endDate = now.minusDays(value);   //结束时间
        LocalDate beginDate = endDate.minusDays(6); //开始时间
        logger.info("开始时间： " + beginDate + ",结束时间： " + endDate);
        //上周排行榜
        List<MallUserRankVo> lastWeekRankList = mallUserService.rankList(null, beginDate, endDate);

        //判断用户是否符合 本周的奖金发放规则
        Boolean flag = true;
        BigDecimal awardAmount = new BigDecimal(0.00);  //排行奖励金额
        for (MallUserRankVo mallUserRankVo : lastWeekRankList) {
            if (mallUserRankVo.getId().contains(id)) {
                flag = false;
                awardAmount = mallUserRankVo.getAwardAmount();
            }
        }
        if (flag) {
            return ResponseUtil.badArgumentValue("该用户不符合奖金发放规则！");
        }
        //todo 判断用户本周的奖金是否已经发放


        //符合发放条件
        Map<String, Object> paramsMap = new HashMap<>();    //todo 暂时不传参数
        paramsMap.put("dType", AccountStatus.DISTRIBUTION_TYPE_00);  //直接分润
        paramsMap.put("message", "排行奖励");
        paramsMap.put("capitalTrend", AccountStatus.CAPITAL_TREND_01);
        paramsMap.put("changeType", AccountStatus.CHANGE_TYPE_01);
        paramsMap.put("changeAccountType", AccountStatus.CHANGE_ACCOUNT_TYPE_1);
        if (awardAmount.compareTo(new BigDecimal(0.00)) == 1) {
            AccountUtils.accountChange(id, awardAmount, CommonConstant.TASK_REWARD_CASH, AccountStatus.BID_13, paramsMap);
        }

        return ResponseUtil.ok();
    }

}
