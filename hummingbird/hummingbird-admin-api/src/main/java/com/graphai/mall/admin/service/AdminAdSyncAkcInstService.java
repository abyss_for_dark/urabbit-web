package com.graphai.mall.admin.service;

import com.alibaba.druid.support.json.JSONUtils;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.httprequest.HttpUtils;
import com.graphai.mall.admin.dto.GoodsAllinone;
import com.graphai.mall.core.qcode.QCodeService;
import com.graphai.mall.db.config.AkcApiProperties;
import com.graphai.mall.db.dao.MallApiSyncConfigMapper;
import com.graphai.mall.db.domain.*;
import com.graphai.mall.db.service.*;
import com.graphai.mall.db.service.advertisement.AdminAdSyncService;
import com.graphai.mall.db.service.advertisement.MallAdBrandService;
import com.graphai.mall.db.service.advertisement.MallAdService;
import com.graphai.mall.db.service.advertisement.MallBrandService;
import com.graphai.mall.db.service.category.MallCategoryService;
import com.graphai.mall.db.service.goods.MallGoodsAttributeService;
import com.graphai.mall.db.service.goods.MallGoodsProductService;
import com.graphai.mall.db.service.goods.MallGoodsService;
import com.graphai.mall.db.service.goods.MallGoodsSpecificationService;
import com.qcloud.cos.utils.Md5Utils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Header;
import org.apache.http.message.BasicHeader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

@Service
public class AdminAdSyncAkcInstService {
    private final Log logger = LogFactory.getLog(AdminAdSyncAkcInstService.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Autowired
    private MallAdService adService;

    @Autowired
    private AdminAdSyncService adminAdSyncService;

    @Autowired
    private MallGoodsService goodsService;
    @Autowired
    private MallGoodsSpecificationService specificationService;
    @Autowired
    private MallGoodsAttributeService attributeService;
    @Autowired
    private MallGoodsProductService productService;
    @Autowired
    private MallCategoryService categoryService;
    @Autowired
    private MallBrandService brandService;

    @Autowired
    private QCodeService qCodeService;

    @Autowired
    private MallAdBrandService adBrandService;

    @Autowired
    private AkcApiProperties properties;

    @Autowired
    private MallApiSyncConfigMapper MallApiSyncConfigMapper;

    public void clearData(String dataBatchId, String channelCode) {
        //所有数据执行完毕,
        //如果最新的接口查不出来的数据
        //从品牌活动表做逻辑删除，所有商品下架。
        adService.updateByDataBatchId(dataBatchId, channelCode);
        goodsService.updateByDataBatchId(dataBatchId, channelCode);
    }

    public void syscBrand(Integer index, Integer count, String dataBatchId,
                          String sessionId, Integer page, Integer pageSize) {
//    	if(true){
//    		return;
//    	}

//    	List<String> adNameList = new ArrayList<String>();

        MallApiSyncConfigExample example = new MallApiSyncConfigExample();
        MallApiSyncConfigExample.Criteria criteria = example.createCriteria();
        criteria.andChannelCodeEqualTo(properties.getChannelCode());
        criteria.andSrcDataTypeEqualTo("BRAND_SEARCH_KEYWORD");

        List<MallApiSyncConfig> list =
                MallApiSyncConfigMapper.selectByExample(example);

        if (list != null && list.size() > 0
                && list.get(0) != null
                && list.get(0).getSrcDataId() != null) {
            String[] adNameArr = list.get(0).getSrcDataId().split(",");
            for (String adName : adNameArr) {
                this.syscBrand(index, count, dataBatchId, sessionId, page, pageSize, adName);
            }
        }

//    	adNameList.add("暇步士HushPuppies童装0701");
//    	adNameList.add("老席匠凉席0701");
//    	adNameList.add("乐赟贝童装0701");
//    	adNameList.add("雅莹EP KIDS童装0701");
//
//    	adNameList.add("【反季清仓】妃图FEITU女装0629");
//    	adNameList.add("唐狮TONLION男装0627");
//    	adNameList.add("精典泰迪Classic Teddy&欧淘otao母婴用品0629");
//    	
//    	adNameList.add("ELLE拉杆箱0630");
//    	adNameList.add("卡米KUMIKIWA女鞋箱包0630");
//    	adNameList.add("老爷车LAOYECHE皮带0630");

//    	for(String adName:adNameList){
//        	this.syscBrand(index, count, dataBatchId, sessionId, page, pageSize, adName);
//    	}

    }

    public void syscBrand(Integer index, Integer count, String dataBatchId,
                          String sessionId, Integer page, Integer pageSize, String keyWord) {

//    	if(true){
//    		return; //pxhtest
//    	}
        // 1.appKey放到配置文件中
        // 2.批次号找个地方存放
        // 3.查找原本是否存在某个品牌档期,如果有则更新,没有则新增 ok

        Map<String, String> paramsMap = new HashMap<String, String>();

        paramsMap.put("cateStr", "pzGwZ08S04,pzGwZ08S0u,pzGwZ08S0t,pzGwZ08S0w,pzGwZ08S00,pzGwZ08S0y,pzGwZ08S0G,p1pbcdN5N1,pzGwZ08S0L,pzGwZ08S1V");
        try {
            keyWord = URLEncoder.encode(keyWord, "UTF-8");
//			paramsMap.put("keyWord", URLEncoder.encode(keyWord, "UTF-8"));
        } catch (UnsupportedEncodingException e1) {
            logger.error("", e1);
        }
        paramsMap.put("forecast", "false");
        paramsMap.put("underWay", "true");
        paramsMap.put("pageNum", page.toString());
        paramsMap.put("pageSize", pageSize.toString());

        Header[] headers = {
                new BasicHeader(":authority", "console.xiangtuan.xyz")
                , new BasicHeader(":method", "GET")
//				    ,new BasicHeader(":path","/api/commodity/search/activity?cateStr=&keyWord=&forecast=false&underWay=true&pageNum=1&pageSize=48")
                , new BasicHeader(":scheme", "https")
                , new BasicHeader("accept", "application/json, text/plain, */*")
                , new BasicHeader("accept-encoding", "gzip, deflate, br")
                , new BasicHeader("accept-language", "zh-CN,zh;q=0.9")
                , new BasicHeader("cache-control", "no-cache")
//					    ,new BasicHeader("cookie","_bl_uid=gIjjFw6kvnXkpmm0bdRCfh2w3tm9; SESSION=ab84e16e-8fef-45b8-9878-949b052b05fd")
                , new BasicHeader("cookie", "SESSION=" + sessionId)
                , new BasicHeader("pragma", "no-cache")
                , new BasicHeader("referer", "https://console.xiangtuan.xyz/selectCenter")
                , new BasicHeader("user-agent", "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36")
        };

        String requestUrl = "https://console.xiangtuan.xyz/api/commodity/search/activity?keyWord=" + keyWord + "&";

        String retVal = HttpUtils.get(requestUrl, paramsMap, headers);
//    	System.out.println("retVal:"+retVal);

        if (retVal != null) {
            Map<String, Object> retMap = (Map<String, Object>) JSONUtils.parse(retVal);
//    		System.out.println(retMap);
            if (retMap != null && 200 == MapUtils.getIntValue(retMap, "code", 0)) {
                Map<String, Object> pageMap = (Map<String, Object>) retMap.get("data");

                if (pageMap != null && MapUtils.getIntValue(pageMap, "total", 0) > 0) {
                    List<Map<String, Object>> adList = (List) pageMap.get("data");
                    if (adList != null) {
                        int adIndex = 0;
                        for (Map<String, Object> adMap : adList) {
                            String srcAdId = MapUtils.getString(adMap, "id");
                            adIndex++;
                            if (adIndex % count != index) {
                                continue;
                            }
//            				if(!"596757270997057796".equals(srcAdId)){ //pxhtest
//            					continue;
//            				}
                            logger.info("sync adId:" + srcAdId);

                            MallAd ad = new MallAd();
                            ad.setName(MapUtils.getString(adMap, "name"));
                            ad.setUrl(MapUtils.getString(adMap, "brandLogoUrl"));
                            ad.setPosition((byte) 1);
                            ad.setContent(MapUtils.getString(adMap, "description"));

                            try {
                                ad.setStartTime(getLongTimestampToLocalDateTime(MapUtils.getLong(adMap, "beginTime")));
                                ad.setEndTime(getLongTimestampToLocalDateTime(MapUtils.getLong(adMap, "endTime")));
                            } catch (Exception e) {
                                continue;
                            }

                            ad.setEnabled(true);
                            ad.setOnlineProductCount(MapUtils.getInteger(adMap, "onlineProductCount"));
                            ad.setTagName(MapUtils.getString(adMap, "tagName"));
                            ad.setBanner(MapUtils.getString(adMap, "banner"));

                            boolean addPriceFlag = false;
                            int commissionRatio = MapUtils.getIntValue(adMap, "commissionRatio", -1);
                            if (commissionRatio == 0) {
                                addPriceFlag = true;
                            }

                            ad.setChannelCode(properties.getChannelCode());
                            ad.setSrcAdId(srcAdId);
                            ad.setDataBatchId(dataBatchId);

                            MallBrand brand = this.syncBrandInfo(adMap, dataBatchId);

                            MallAd oldAd = adminAdSyncService.findByChannelCodeAndSrcAdId(ad);
                            if (oldAd != null) {
                                ad.setId(oldAd.getId());
                                oldAd.setDataBatchId(dataBatchId);


                                try {
                                    oldAd.setStartTime(getLongTimestampToLocalDateTime(MapUtils.getLong(adMap, "beginTime")));
                                    oldAd.setEndTime(getLongTimestampToLocalDateTime(MapUtils.getLong(adMap, "endTime")));
                                } catch (Exception e) {
                                    continue;
                                }

                                adService.updateById(oldAd);
                            } else {
                                ad.setDeleted(false);
                                ad.setPosition((byte) 2);
                                ad.setSort(59);
                                adService.add(ad);

                                //同步活动品牌关联表
                                MallAdBrand adBrand = new MallAdBrand();
                                adBrand.setAdId(ad.getId());
                                adBrand.setBrandId(brand.getId());
                                adBrand.setDataBatchId(dataBatchId);
                                adBrand.setChannelCode(properties.getChannelCode());
                                adBrand.setSrcAdId(srcAdId);
                                MallAdBrand oldAdBrand = adBrandService.selectByAdIdAndBrandId(adBrand);
                                if (oldAdBrand != null) {
                                    if (dataBatchId != oldAdBrand.getDataBatchId()) {
                                        oldAdBrand.setDataBatchId(dataBatchId);
                                        adBrandService.updateById(oldAdBrand);
                                    }
                                } else {
                                    adBrandService.add(adBrand);
                                }
                            }

                            this.syscGoods(ad.getId(), srcAdId, dataBatchId,
                                    brand.getId(), brand.getName(), addPriceFlag, sessionId);
                        }
                    }

                    if (MapUtils.getBooleanValue(pageMap, "hasNextPage", false)) {
                        //递归调用
                        this.syscBrand(index, count, dataBatchId, sessionId, page + 1, pageSize);
                    }
                }
            }
        }
    }

    public MallBrand syncBrandInfo(Map<String, Object> adMap, String dataBatchId) {
        //同步品牌信息
        String brandCnName = MapUtils.getString(adMap, "brandName");
        String brandLogo = MapUtils.getString(adMap, "brandLogoUrl");
        MallBrand brand = new MallBrand();
        brand.setName(brandCnName);
        brand.setPicUrl(brandLogo);
        brand.setDataBatchId(dataBatchId);
        brand.setChannelCode(properties.getChannelCode());
        brand.setDesc(MapUtils.getString(adMap, "description"));

        MallBrand oldBrand = adminAdSyncService.findByChannelCodeAndName(brand);
        if (oldBrand != null) {
            brand.setId(oldBrand.getId());
            brandService.updateById(brand);
        } else {
            brandService.add(brand);
        }
        return brand;
    }

    public static String getWpcSignStr(Map<String, String> paramsMap, String apiSecret) {
        List<String> list = new ArrayList<String>();
        for (String key : paramsMap.keySet()) {
            list.add(key);
        }
        Collections.sort(list);

        StringBuilder stringBuilder = new StringBuilder();
        for (String key : list) {
            stringBuilder.append("&").append(key).append("=").append(paramsMap.get(key));
        }

        String paramStr = stringBuilder.substring(1) + apiSecret;

        String sign = Md5Utils.md5Hex(paramStr.getBytes());
        return sign.toLowerCase();
    }

    public static LocalDateTime getDateStrToLocalDateTime(String dateStr) throws Exception {
        java.util.Date date = dateFormat.parse(dateStr);
        Instant instant = date.toInstant();
        ZoneId zone = ZoneId.systemDefault();
        LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, zone);
        return localDateTime;
    }

    public static LocalDateTime getLongTimestampToLocalDateTime(Long lt) throws Exception {
        java.util.Date date = new Date(lt);
        Instant instant = date.toInstant();
        ZoneId zone = ZoneId.systemDefault();
        LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, zone);
        return localDateTime;
    }

    public static String getNonceStr() {
        String str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < 8; i++) {

            int number = random.nextInt(str.length());
            sb.append(str.charAt(number));
        }
        return sb.toString();
    }

    public void syscGoods(String newAdId, String oldAdId, String dataBatchId,
                          String brandId, String brandName,
                          boolean addPriceFlag, String sessionId) {

        syscGoods(newAdId, oldAdId, 1, 30, dataBatchId, brandId, brandName, addPriceFlag, sessionId);
    }

    public void syscGoods(String newAdId, String adId,
                          Integer page, Integer pageSize, String dataBatchId,
                          String brandId, String brandName, boolean addPriceFlag, String sessionId) {
//    	int totalNum = 0;
        Map<String, String> paramsMap = new HashMap<String, String>();

        paramsMap.put("pageNum", page + "");
        paramsMap.put("pageSize", pageSize + "");

        String requestUrl = "https://console.xiangtuan.xyz/api/commodity/product/list/saIe7oe1fZ/"
                + adId;

        Header[] headers = {
                new BasicHeader(":authority", "console.xiangtuan.xyz")
                , new BasicHeader(":method", "GET")
                , new BasicHeader(":scheme", "https")
                , new BasicHeader("accept", "application/json, text/plain, */*")
                , new BasicHeader("accept-encoding", "gzip, deflate, br")
                , new BasicHeader("accept-language", "zh-CN,zh;q=0.9")
                , new BasicHeader("cache-control", "no-cache")
                , new BasicHeader("cookie", "SESSION=" + sessionId)
                , new BasicHeader("pragma", "no-cache")
                , new BasicHeader("referer", "https://console.xiangtuan.xyz/productList?activityNo=" + adId)
                , new BasicHeader("user-agent", "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36")
        };

        String retVal = HttpUtils.post(requestUrl, paramsMap, headers);
//    	System.out.println("retVal:"+retVal);

        if (retVal != null) {
            Map<String, Object> retMap = (Map<String, Object>) JSONUtils.parse(retVal);
//    		System.out.println(retMap);
            if (retMap != null && 200 == MapUtils.getIntValue(retMap, "code", 0)
                    && retMap.get("data") != null) {
                Map<String, Object> dataMap = (Map<String, Object>) retMap.get("data");

                Map<String, Object> pageMap = (Map<String, Object>) dataMap.get("productPage");
                if (pageMap == null || pageMap.get("list") == null) {
                    return;
                }
                Boolean hasNextPage = MapUtils.getBoolean(pageMap, "hasNextPage", false);

                List<Map<String, Object>> goods = (List) pageMap.get("list");
                if (goods != null) {
                    for (Map<String, Object> goodMap : goods) {
//    					if(!"597019009760182558".equals(MapUtils.getString(goodMap, "productNo"))){//pxhtest
//    						continue;
//    					}
//    		    		logger.debug(goodMap); //pxhtest
                        MallGoods goodDto = new MallGoods();

                        List<String> pictureUrls = (List) goodMap.get("pictureUrls");

                        String goodImage = pictureUrls.get(0);
                        //同步sku信息
                        List<MallGoodsSpecification> specList = new ArrayList<MallGoodsSpecification>();

                        goodDto.setIsOnSale(true);


                        //pxhtest 库存同步时间较长,库存大于8才将sku数据同步过来
                        int inventory_limit = 8;

                        List<MallGoodsProduct> productList = new ArrayList<MallGoodsProduct>();
                        if (goodMap.get("skuVOS") != null) {
                            List<Map<String, Object>> sizes = (List<Map<String, Object>>) goodMap.get("skuVOS");
                            if (sizes.size() > 0) {
                                boolean isExistsStock = false; //商品是否有库存

                                for (Map<String, Object> sizeMap : sizes) {
//                					{
//                						"sizeName": "28",
//                						"vipshopPrice": "94",
//                						"marketPrice": "699",
//                						"stock": true
//                						}

                                    String sizeName = MapUtils.getString(sizeMap, "properties");
                                    //marketPrice 没有写入到SKU表中
                                    MallGoodsSpecification specification2 = new MallGoodsSpecification();
                                    specification2.setSpecification("尺码");
                                    specification2.setValue(sizeName);
                                    if (StringUtils.isEmpty(goodImage)) {
                                        specification2.setPicUrl("http://");
                                    } else {
                                        specification2.setPicUrl(goodImage);
                                    }
                                    specification2.setDeleted(false);
                                    specList.add(specification2);

                                    BigDecimal skuPriceRes = null;

                                    BigDecimal skuPrice = new BigDecimal(MapUtils.getString(sizeMap, "price"));

                                    BigDecimal prodPrice = new BigDecimal(MapUtils.getString(goodMap, "price"));
                                    BigDecimal profit = new BigDecimal(MapUtils.getString(goodMap, "profit"));
                                    BigDecimal temp = new BigDecimal("0.2").subtract(profit.divide(prodPrice, 2, BigDecimal.ROUND_DOWN)); // 0.2 - profit/price

                                    if (prodPrice.compareTo(skuPrice) < 0) {
                                        //sku价格大于商品价格,用sku价格来除
                                        skuPriceRes = new BigDecimal(MapUtils.getString(goodMap, "settlementPrice")).
                                                divide(new BigDecimal("0.8"), 0, BigDecimal.ROUND_CEILING);
                                    } else {
                                        if (temp.compareTo(BigDecimal.ZERO) > 0) {
//                        					temp = temp.add(new BigDecimal("1"));
                                            skuPriceRes = new BigDecimal(MapUtils.getString(goodMap, "settlementPrice")).
                                                    divide(new BigDecimal("0.8"), 0, BigDecimal.ROUND_CEILING);
                                        } else {
                                            skuPriceRes = skuPrice;
                                        }
                                    }

                                    MallGoodsProduct product = new MallGoodsProduct();
                                    product.setPrice(skuPriceRes);

                                    if (MapUtils.getInteger(sizeMap, "inventory") > inventory_limit) {
                                        //pxhtest 库存同步时间较长,库存大于8才将sku数据同步过来
//                    					if(MapUtils.getInteger(sizeMap, "inventory")>0){
                                        //是否有库存
                                        product.setNumber(MapUtils.getInteger(sizeMap, "inventory"));
                                        isExistsStock = true;
                                    } else {
                                        product.setNumber(0);
                                    }
                                    product.setSrcSizeId(MapUtils.getString(sizeMap, "skuNo"));
                                    product.setDataBatchId(dataBatchId);
                                    product.setChannelCode(properties.getChannelCode());
                                    product.setSrcGoodId(MapUtils.getString(goodMap, "productNo"));

                                    product.setSpecifications(new String[]{sizeName});

                                    product.setDeleted(false);
                                    productList.add(product);

                                }

                                if (!isExistsStock) {
                                    //商品没有库存,直接下架
                                    goodDto.setIsOnSale(false);
                                }
                            } else {
                                continue;
                            }
                        } else {
                            continue;
                        }

                        BigDecimal prodPrice = new BigDecimal(MapUtils.getString(goodMap, "price"));
                        BigDecimal profit = new BigDecimal(MapUtils.getString(goodMap, "profit"));
                        BigDecimal temp = new BigDecimal("0.2").subtract(profit.divide(prodPrice, 2, BigDecimal.ROUND_DOWN)); // 0.2 - profit/price
                        if (temp.compareTo(BigDecimal.ZERO) > 0) {

//        					logger.info("goodMap:"+goodMap);
//    						logger.info("prodPrice:"+prodPrice);
//    						logger.info("rate:"+profit.divide(prodPrice,2, BigDecimal.ROUND_DOWN));
//        					logger.info("temp:"+temp);
                            temp = temp.add(new BigDecimal("1"));
//    						prodPrice = prodPrice.multiply(temp).setScale(0,BigDecimal.ROUND_CEILING);
                            prodPrice = new BigDecimal(MapUtils.getString(goodMap, "settlementPrice")).
                                    divide(new BigDecimal("0.8"), 0, BigDecimal.ROUND_CEILING);
//        					logger.info("prodPrice:"+prodPrice);
                        }

                        goodDto.setCounterPrice(new BigDecimal(MapUtils.getString(goodMap, "tagPrice")));
                        goodDto.setRetailPrice(prodPrice);
                        goodDto.setFactoryPrice(new BigDecimal(MapUtils.getString(goodMap, "settlementPrice")));
                        goodDto.setCommissionType(1);
                        goodDto.setCommissionValue(MapUtils.getInteger(goodMap, "profit"));

                        // dcImageURLs
                        if (goodMap.get("pictureUrls") != null) {
                            List<String> dcImageURLs = (List<String>) goodMap.get("pictureUrls");
                            goodDto.setDetail(JSONUtils.toJSONString(dcImageURLs));
                        }

                        goodDto.setBrandId(brandId);
                        goodDto.setGallery(new String[]{goodImage});

                        // goodId
                        goodDto.setChannelCode(properties.getChannelCode());
                        goodDto.setDataBatchId(dataBatchId);
                        goodDto.setSrcAdId(adId);
                        goodDto.setSrcGoodId(MapUtils.getString(goodMap, "productNo"));

                        goodDto.setSrcGoodName(MapUtils.getString(goodMap, "productName"));

                        String brief = MapUtils.getString(goodMap, "productDescription");

                        //goodImage goodName sn
                        String goodName = MapUtils.getString(goodMap, "productName");

                        String akcGoodId = "";
                        if (brief != null && brief.contains("、")) {
                            akcGoodId = brief.split("、")[0];
                        }

                        goodName = brandName + " " + akcGoodId + "、" + goodName;
                        goodDto.setGoodsSn(MapUtils.getString(goodMap, "productNo"));
                        goodDto.setName(goodName);
                        goodDto.setPicUrl(goodImage);

                        //这里还需要处理
//    					goodDto.setBrief(brief);
                        goodDto.setBrief(goodName);

                        GoodsAllinone goodsAllinone = new GoodsAllinone();
                        goodsAllinone.setGoods(goodDto);
                        goodsAllinone.setProducts(productList.toArray(new MallGoodsProduct[]{}));
                        goodsAllinone.setSpecifications(specList.toArray(new MallGoodsSpecification[]{}));
    					
    					/*String material = MapUtils.getString(goodMap, "material");
    					MallGoodsAttribute goodAttribute = new MallGoodsAttribute();
    					goodAttribute.setAttribute("材质");
    					goodAttribute.setDeleted(false);
    					if(StringUtils.isEmpty(material)){
        					goodAttribute.setValue("");
    					}else{
        					goodAttribute.setValue(material);
        					
        					MallGoodsAttribute[] attributes = new MallGoodsAttribute[1];
        					attributes[0] = goodAttribute;
        					
        					goodsAllinone.setAttributes(attributes);
    					}*/

                        try {
                            boolean isExists = goodsService.
                                    checkExistBySrcGoodNameAndSn(goodDto.getSrcGoodName(), goodDto.getGoodsSn());
                            if (isExists) {
                                //已经存在该产品
                                //更新库存
                                MallGoods oldGoods = goodsService.selectOneBySrcGoodNameAndSn(goodDto);
                                if (oldGoods != null) {
                                    oldGoods.setDataBatchId(dataBatchId);
//        				    		logger.info("updateByNameAndGoodsSn:"+oldGoods.getId()); //pxhtest

                                    goodDto.setUpdateTime(LocalDateTime.now());

                                    if (productList != null && productList.size() > 0) {
                                        for (MallGoodsProduct prod : productList) {
//        				    				if(prod.getNumber()==0){
                                            prod.setGoodsId(oldGoods.getId());

                                            MallGoodsProduct oldProd = productService.selectOneByExample(prod);
//        		    				    		if(oldProd!=null && oldProd.getNumber()>0){
//        		        				    		logger.info("oldProd.getNumber():"+oldProd.getNumber()); //pxhtest
                                            prod.setGoodsId(oldProd.getGoodsId());
                                            prod.setUpdateTime(LocalDateTime.now());
                                            productService.updateByExampleSelective(prod);
//        		    				    		}
//        				    				}
                                        }

                                    }

                                    goodDto.setDetail(null); //详情图片不更新
                                    goodDto.setName(null); //名称不更新
                                    //更新商品批次号
                                    goodsService.updateByNameAndGoodsSn(goodDto);
                                }
//    				    		logger.error("oldGoods:");
                            } else {
                                this.create(goodsAllinone);
                            }
                        } catch (Exception e) {
                            logger.error("", e);
//							logger.error(JSONUtils.toJSONString(goodsAllinone));
                        }
                    }
                }

                if (hasNextPage) {
                    try {
                        Thread.sleep(10 * 1000L);//操作不能太快,每次分页间隔10s pxhtest
                    } catch (InterruptedException e) {
                        logger.error("", e);
                    }
                    //递归调用
                    this.syscGoods(newAdId, adId, page + 1, pageSize, dataBatchId,
                            brandId, brandName, addPriceFlag, sessionId);
                }
            }
        }
    }

    //    @Autowired
//	private MallGoodsMapper goodsMapper;
//
//    @Autowired
//	private MallGoodsProductMapper goodsProductMapper;
//	
    public Object create(GoodsAllinone goodsAllinone) {
        Object error = validate(goodsAllinone);
        if (error != null) {
            return error;
        }

        MallGoods goods = goodsAllinone.getGoods();
        MallGoodsAttribute[] attributes = goodsAllinone.getAttributes();
        MallGoodsSpecification[] specifications = goodsAllinone.getSpecifications();
        MallGoodsProduct[] products = goodsAllinone.getProducts();

        String name = goods.getName();

        // 商品基本信息表Mall_goods
        goodsService.add(goods);

        //将生成的分享图片地址写入数据库
        String url = qCodeService.createGoodShareImage(goods.getId().toString(), goods.getPicUrl(), goods.getName());
        if (!StringUtils.isEmpty(url)) {
            goods.setShareUrl(url);
            if (goodsService.updateById(goods) == 0) {
                throw new RuntimeException("更新数据失败");
            }
        }

        // 商品规格表Mall_goods_specification
        List<MallGoodsSpecification> specList = new ArrayList<MallGoodsSpecification>();
        for (MallGoodsSpecification specification : specifications) {
            specification.setGoodsId(goods.getId());

            specList.add(specification);
//            specificationService.add(specification);
        }
        if (specList.size() > 0) {
            adminAdSyncService.addGoodsSpecBatch(specList);
        }

        // 商品参数表Mall_goods_attribute
        if (attributes != null && attributes.length > 0) {
            List<MallGoodsAttribute> attrList = new ArrayList<MallGoodsAttribute>();
            for (MallGoodsAttribute attribute : attributes) {
                attribute.setGoodsId(goods.getId());

                attrList.add(attribute);
//                attributeService.add(attribute);
            }
            if (attrList.size() > 0) {
                adminAdSyncService.addGoodsAttrBatch(attrList);
            }
        }

        // 商品货品表Mall_product
        List<MallGoodsProduct> productList = new ArrayList<MallGoodsProduct>();
        for (MallGoodsProduct product : products) {
            product.setGoodsId(goods.getId());

            product.setAddTime(LocalDateTime.now());
            product.setUpdateTime(LocalDateTime.now());
            productList.add(product);
//            productService.add(product);
        }
        if (productList.size() > 0) {
            adminAdSyncService.addGoodsProductsBatch(productList);
        }
        return ResponseUtil.ok();
    }


    private Object validate(GoodsAllinone goodsAllinone) {
        MallGoods goods = goodsAllinone.getGoods();
        String name = goods.getName();
        if (StringUtils.isEmpty(name)) {
            return ResponseUtil.badArgument();
        }
        String goodsSn = goods.getGoodsSn();
        if (StringUtils.isEmpty(goodsSn)) {
            return ResponseUtil.badArgument();
        }
        // 品牌商可以不设置，如果设置则需要验证品牌商存在
        /*String brandId = goods.getBrandId();
        if (brandId != null && brandId != 0) {
            if (brandService.findById(brandId) == null) {
                return ResponseUtil.badArgumentValue();
            }
        }*/
        // 分类可以不设置，如果设置则需要验证分类存在
        String categoryId = goods.getCategoryId();
        if (categoryId != null && !"0".equals(categoryId)) {
            if (categoryService.findById(categoryId) == null) {
                return ResponseUtil.badArgumentValue();
            }
        }

        /*
        MallGoodsAttribute[] attributes = goodsAllinone.getAttributes();
        for (MallGoodsAttribute attribute : attributes) {
            String attr = attribute.getAttribute();
            if (StringUtils.isEmpty(attr)) {
                return ResponseUtil.badArgument();
            }
            String value = attribute.getValue();
            if (StringUtils.isEmpty(value)) {
                return ResponseUtil.badArgument();
            }
        }*/

        MallGoodsSpecification[] specifications = goodsAllinone.getSpecifications();
        for (MallGoodsSpecification specification : specifications) {
            String spec = specification.getSpecification();
            if (StringUtils.isEmpty(spec)) {
                return ResponseUtil.badArgument();
            }
            String value = specification.getValue();
            if (StringUtils.isEmpty(value)) {
                return ResponseUtil.badArgument();
            }
        }

        MallGoodsProduct[] products = goodsAllinone.getProducts();
        for (MallGoodsProduct product : products) {
            Integer number = product.getNumber();
            if (number == null || number < 0) {
                return ResponseUtil.badArgument();
            }

            BigDecimal price = product.getPrice();
            if (price == null) {
                return ResponseUtil.badArgument();
            }

            String[] productSpecifications = product.getSpecifications();
            if (productSpecifications.length == 0) {
                return ResponseUtil.badArgument();
            }
        }

        return null;
    }

}
