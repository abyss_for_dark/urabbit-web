package com.graphai.mall.admin.web.finance;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.mall.db.service.MallApplyService;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController
@RequestMapping("/admin/apply")
@Validated
public class AdminApplyController {

    @Autowired
    private MallApplyService mallApplyService;

    @RequiresPermissions("admin:apply:list")
    @RequiresPermissionsDesc(menu = {"用户审核", "用户审核"}, button = "查询")
    @GetMapping("/list")
    public Object auditList(@RequestParam(defaultValue = "1") Integer page,
                            @RequestParam(defaultValue = "20") Integer limit,
                            String keyword, String region, String beginTime, String endTime, String status) {
        HashMap<String, Object> data = mallApplyService.queryAuditList(keyword, region, status, beginTime, endTime, page, limit);
        return ResponseUtil.ok(data);
    }

    @RequiresPermissions("admin:apply:audit")
    @RequiresPermissionsDesc(menu = {"用户审核", "用户审核"}, button = "审核驳回/通过")
    @PostMapping("/audit")
    public Object audit(@RequestParam String id, @RequestParam String status, String actualReward, String reason) {
        try {
            if (!status.equalsIgnoreCase("3") && !status.equalsIgnoreCase("5")) {
                return ResponseUtil.badArgument();
            }
            int errno = mallApplyService.auditAgent(id, actualReward, status, reason);
            if (errno == -1) {
                return ResponseUtil.fail(503, "已存在对应地区的代理,无法修改");
            }
            if (errno == -2) {
                return ResponseUtil.fail(503, "手机号已被其他代理注册");
            }
            if (errno == -3) {
                return ResponseUtil.fail(503, "非法操作");
            }
            return ResponseUtil.ok();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseUtil.fail(-1, "修改失败");
    }
}
