package com.graphai.mall.admin.web.promotion;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.mall.db.domain.MallActivityTopic;
import com.graphai.mall.db.service.bmdesign.BmdesignBusinessFormService;
import com.graphai.mall.db.service.promotion.MallActivityTopicService;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/admin/activity/topic")
@Validated
public class AdminActivityTopicController {


    @Autowired
    private MallActivityTopicService activityTopicService;

    @Autowired
    private BmdesignBusinessFormService bmdesignBusinessFormService;

    @RequiresPermissions("admin:activity:topic:list")
    @RequiresPermissionsDesc(menu = {"主题活动", "主题管理"}, button = "查询")
    @GetMapping("/list")
    public Object list(@RequestParam(required = false) String topicName,
                       @RequestParam(required = false) String showPlace,
                       @RequestParam(required = false) String state,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit) {
        return ResponseUtil.ok(activityTopicService.selectTopicList(topicName, showPlace, state, page, limit));
    }

    @PostMapping("/delete")
    public Object delete(@NotNull String id) {
        activityTopicService.deleteTopicById(id);
        return ResponseUtil.ok();
    }

    @RequiresPermissions("admin:activity:topic:update")
    @RequiresPermissionsDesc(menu = {"主题活动", "主题管理"}, button = "修改")
    @PostMapping("/update")
    public Object update(@RequestBody MallActivityTopic topic) {
        if (StringUtils.isEmpty(topic.getId())) return ResponseUtil.badArgumentValue("id");
        activityTopicService.updateTopic(topic);
        return ResponseUtil.ok();
    }

    @RequiresPermissions("admin:activity:topic:add")
    @RequiresPermissionsDesc(menu = {"主题活动", "主题管理"}, button = "添加")
    @PostMapping("/add")
    public Object add(@RequestBody MallActivityTopic topic) {
        if (checkIsNull(topic.getTopicName(), topic.getState())) {
            return ResponseUtil.badArgument();
        }
        activityTopicService.addTopic(topic);
        return ResponseUtil.ok();
    }

    @RequiresPermissions("admin:activity:topic:simpleList")
    @RequiresPermissionsDesc(menu = {"主题活动", "主题管理"}, button = "简单主题列表")
    @GetMapping("/simpleList")
    public Object selectAll() {
        return ResponseUtil.ok(activityTopicService.selectSimpleTopics());
    }

    @GetMapping("/bmDesignList")
    public Object bmDesignList() {
        return ResponseUtil.ok(bmdesignBusinessFormService.selectBmdesignBusinessFormListByType("BF10002"));
    }

    private boolean checkIsNull(String... args) {
        for (String arg : args) {
            if (StringUtils.isEmpty(arg)) return true;
        }
        return false;
    }

}
