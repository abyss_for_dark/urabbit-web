package com.graphai.mall.admin.job;

import com.graphai.mall.admin.service.TbkGoodsAllSyncInstService;
import com.graphai.mall.db.config.TbkapiProperties;
import com.graphai.mall.db.service.advertisement.AdminAdSyncService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.concurrent.CountDownLatch;

//@Component
public class TbkGoodsAllSyncJob {

    private final Log logger = LogFactory.getLog(TbkGoodsAllSyncJob.class);

    @Autowired
    private AdminAdSyncService adminAdSyncService;

    @Autowired
    private TbkGoodsAllSyncInstService tbkGoodsAllSyncInstService;

    @Autowired
    private TbkapiProperties properties;

//    private int SYSC_THREAD_CNT = 8;

    //每隔10分钟(10*60*1000ms)执行一次
    @Scheduled(fixedRate = 30 * 60 * 60 * 1000)
    public void syscAd() {

        int index = 0;
        logger.info("系统开启任务:同步淘宝客商品数据" + index);

        String maxBatchId = "0";
        try {
            maxBatchId = adminAdSyncService.selMaxBatchId(properties.getChannelCode());
        } catch (Exception e) {

        }
        Long x = (Long.parseLong(maxBatchId) + 1); //数据更新批次号
        final String dataBatchId = x.toString();
//        adminAdSyncService.syscBrand(index, SYSC_THREAD_CNT);

        final int threadCount = Integer.valueOf(properties.getThreadCount());

        final CountDownLatch cd = new CountDownLatch(threadCount);

        for (int i = 0; i < threadCount; i++) {
            final int threadIndex = i;
            new Thread() {
                public void run() {

                    tbkGoodsAllSyncInstService.syscTbkGoods(threadIndex, threadCount, dataBatchId, 1);
                    logger.error("同步数据完成");
                    //还记得之前构造方法的参数吗,10,每调用一个countDown()方法，都会使得这个数值减1
                    cd.countDown();
                }
            }.start();
        }

//    	adminTbkSyncInstService.syscTbkTQGGoods(dataBatchId);

        //这个方法，会使得所有的线程暂停，只有当cd构造方法里面的值为0的时候，才能走通，调用一个countDown()方法，都会使得这个数值减1
        try {
            cd.await();
//			adminAdSyncInstService.clearData(dataBatchId,properties.getChannelCode());
        } catch (Exception e) {
            logger.error("", e);
        }
        //或者使用这个方法，这个方法也会等待数值到0才会往下面走，但是如果达到指定的时间，还没有达到0，它也会走过
//        cd.await(1000, TimeUnit.MINUTES);
        //开始合并文件
        logger.error("所有线程同步数据完成");
    }

}
