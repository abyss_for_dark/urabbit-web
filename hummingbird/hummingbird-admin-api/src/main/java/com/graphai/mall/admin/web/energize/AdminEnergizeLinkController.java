package com.graphai.mall.admin.web.energize;

import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.mall.db.domain.MallAdmin;
import com.graphai.mall.db.domain.MallEnergizeRecord;
import com.graphai.mall.db.service.funeng.MallEnergizeService;
import com.graphai.mall.db.vo.MallEnergizeRecordVo;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 赋能链接
 *
 * @author LaoGF
 * @since 2020-07-28
 */
@RestController
@RequestMapping("/admin/energizeLink")
@Validated
public class AdminEnergizeLinkController {

    @Resource
    private MallEnergizeService mallEnergizeService;


    /**
     * 生成赋能链接
     */
//    @RequiresPermissions("admin:energizeLink:expend")
//    @RequiresPermissionsDesc(menu = {"广告管理", "生成记录"}, button = "生成链接")
    @PostMapping("/expend")
    public Object expend(@RequestBody MallEnergizeRecord mallEnergizeRecord) {
        String energize = mallEnergizeService.createLink(mallEnergizeRecord);
        if (energize == null) {
            return ResponseUtil.ok("生成失败，请确认链接和生成次数！");
        }
        return ResponseUtil.ok(energize);
    }


    /**
     * 查询所有会员用户
     */
    @GetMapping("/getUser")
    public Object getUser() {
        return ResponseUtil.ok(mallEnergizeService.getUser());
    }

    /**
     * 列表
     */
//    @RequiresPermissions("admin:energizeLink:list")
//    @RequiresPermissionsDesc(menu = {"广告管理", "生成记录"}, button = "查询")
    @GetMapping("/list")
    public Object list(@RequestParam(required = false) String nickName,
                       @RequestParam(required = false) Integer showType,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit) {
        //获取当前用户
        MallAdmin adminUser = (MallAdmin) SecurityUtils.getSubject().getPrincipal();
        List<MallEnergizeRecordVo> list = mallEnergizeService.selectRecordList(nickName, showType, adminUser, page, limit);
        long total = PageInfo.of(list).getTotal();
        Map<String, Object> data = new HashMap<>(16);
        data.put("total", total);
        data.put("items", list);
        return ResponseUtil.ok(data);
    }


    /**
     * 删除
     */
//    @RequiresPermissions("admin:energizeLink:delete")
//    @RequiresPermissionsDesc(menu = {"广告管理", "生成记录"}, button = "删除")
    @PostMapping("/delete")
    public Object delete(@RequestParam String id) {
        if (mallEnergizeService.deleteByID(id)) {
            return ResponseUtil.ok();
        }
        return ResponseUtil.fail();
    }

}
