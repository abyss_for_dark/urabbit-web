package com.graphai.mall.admin.web.promotion;


import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.mall.db.domain.MallAdmin;
import com.graphai.mall.db.service.promotion.PromotionChannelService;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 效果统计
 *
 * @author LaoGF
 * @since 2020-07-13
 */
@RestController
@RequestMapping("/admin/promotionChannelStatistics")
@Validated
public class PromotionChannelStatisticsController {

    @Resource
    private PromotionChannelService promotionChannelService;

    /**
     * 统计收益(用户)
     */
    @RequiresPermissions("admin:promotionChannelStatistics:statistics")
    @RequiresPermissionsDesc(menu = {"推广渠道后台", "效果统计"}, button = "统计收益")
    @GetMapping("/statisticsReward")
    public Object statisticsReward() {
        //获取当前用户
        MallAdmin adminUser = (MallAdmin) SecurityUtils.getSubject().getPrincipal();
        return ResponseUtil.ok(promotionChannelService.statisticsReward(adminUser.getId()));
    }

    /**
     * 统计注册、Android、IOS激活、uv、购卡量、收益(同渠道)
     */
    @RequiresPermissions("admin:promotionChannelStatistics:statisticsAllCount")
    @RequiresPermissionsDesc(menu = {"推广渠道后台", "效果统计"}, button = "统计趋势")
    @GetMapping("/statisticsAllCount")
    public Object statisticsAllCountGroupByDay(@RequestParam(value = "beginDate", required = false) String beginDate,
                                               @RequestParam(value = "endDate", required = false) String endDate) {
        //获取当前用户
        MallAdmin adminUser = (MallAdmin) SecurityUtils.getSubject().getPrincipal();
        return ResponseUtil.ok(promotionChannelService.statisticsAllCountGroupByDay("9", beginDate, endDate));
    }

}
