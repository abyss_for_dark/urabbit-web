package com.graphai.mall.admin.web.statistics;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.lang.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.graphai.commons.util.servlet.ServletUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.baomidou.mybatisplus.core.metadata.IPage;

import org.springframework.web.bind.annotation.RestController;

import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.framework.generator.controller.IBaseController;

import com.graphai.mall.admin.domain.MallRedDStatistics;
import com.graphai.mall.admin.service.IMallRedDStatisticsService;

import javax.websocket.server.PathParam;


/**
 * <p>
 * 整点、现金红包每日统计 前端控制器
 * </p>
 *
 * @author Qxian
 * @since 2021-01-16
 */
@RestController
@RequestMapping("/admin/statistics")
public class MallRedDStatisticsController extends IBaseController<MallRedDStatistics,  String >  {

	@Autowired
    private IMallRedDStatisticsService serviceImpl;
    
    @RequiresPermissions("admin:statistics:single")
    @RequiresPermissionsDesc(menu = {"单独查询"}, button = "单独查询")
	@GetMapping("/single/{id}")
    public Object getEntityById(@PathVariable  String  id) throws Exception{
        MallRedDStatistics entityVName = serviceImpl.getById(id);
        return ResponseUtil.ok(entityVName);
    }
    
    @RequiresPermissions("admin:statistics:addOrUpdate")
    @RequiresPermissionsDesc(menu = {"新增"}, button = "新增")
    @PostMapping("/addOrUpdate")
    public Object addOrUpdate(MallRedDStatistics mallRedDStatistics) throws Exception{
        if(serviceImpl.saveOrUpdate(mallRedDStatistics)){
            return ResponseUtil.ok();
        }else{
            return ResponseUtil.fail();
        }
    }

    /**
     * 1、红包统计-每日统计列表查询
     * @return Object
     */
	@RequiresPermissions("admin:statistics:melist")
	@RequiresPermissionsDesc(menu = {"每日统计列表查询"}, button = "查询")
    @GetMapping("/melist")
    public Object getMeList() {
        IPage<MallRedDStatistics> ipage = serviceImpl.page(getPlusPage(),listQueryWrapper());
        return ResponseUtil.ok(ipage);
    }
 	
 	private QueryWrapper<MallRedDStatistics> listQueryWrapper() {
 		QueryWrapper<MallRedDStatistics> meq = new QueryWrapper<>();
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("dateDayStart")) && StringUtils.isNotEmpty(ServletUtils.getParameter("dateDayEnd"))) {
		      meq.between("date_day", ServletUtils.getParameter("dateDayStart"),ServletUtils.getParameter("dateDayEnd"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("adId"))) {
		      meq.eq("ad_id", ServletUtils.getParameter("adId"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("title"))) {
		      meq.eq("title", ServletUtils.getParameter("title"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("platform"))) {
		      meq.eq("platform", ServletUtils.getParameter("platform"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("type"))) {
		      meq.eq("type", ServletUtils.getParameter("type"));
		}
        meq.orderByDesc("date_day");
        return meq;
    }
 	@RequiresPermissions("admin:statistics:save")
 	@RequiresPermissionsDesc(menu = {"整点、现金红包每日统计新增"}, button = "新增")
    @PostMapping("/save")
    public Object save(@ModelAttribute MallRedDStatistics mallRedDStatistics) throws Exception {
        serviceImpl.save(mallRedDStatistics);
        return ResponseUtil.ok();
    }
	
	@RequiresPermissions("admin:statistics:modify")
	@RequiresPermissionsDesc(menu = {"整点、现金红包每日统计修改"}, button = "修改")
    @PutMapping("/modify")
    public Object modify(@ModelAttribute  MallRedDStatistics mallRedDStatistics){
        serviceImpl.updateById(mallRedDStatistics);
        return  ResponseUtil.ok();
    }
    
    @RequiresPermissions("admin:statistics:del")
    @RequiresPermissionsDesc(menu = {"整点、现金红包每日统计删除"}, button = "删除")
    @DeleteMapping("/del/{id}")
    public Object deleteById(@PathVariable  String  id){
        serviceImpl.removeById(id);
        return  ResponseUtil.ok();
    }

    /**
     * 2、后台管理-红包统计-顶部总金额统计
     *
     * @return Object
     */
    @RequiresPermissions("admin:statistics:melist")
    @RequiresPermissionsDesc(menu = {"顶部总金额统计"}, button = "查询")
    @GetMapping("/topTotalAmount")
    public Object getTopTotalAmount() {
        return ResponseUtil.ok(serviceImpl.getTopTotalAmount());
    }

    /**
     * 3、后台管理-红包统计-昨日、近7日、近30天、本月、全部
     * @param param prevDay、day、week、thirty、month
     * @return Object
     */
    @RequiresPermissions("admin:statistics:melist")
    @RequiresPermissionsDesc(menu = {"顶部总金额统计"}, button = "查询")
    @GetMapping("/groupStatistics/{param}")
    public Object getGroupStatistics(@PathVariable String param) {
        return ResponseUtil.ok(serviceImpl.getGroupStatistics(param));
    }

    /**
     * 4、后台管理-红包统计-近30天曲线图
     * @param param prevDay、day、week、thirty、month
     * @return Object
     */
    @RequiresPermissions("admin:statistics:melist")
    @RequiresPermissionsDesc(menu = {"近30天曲线图"}, button = "查询")
    @GetMapping("/thirtyDayStatistics/{param}")
    public Object getThirtyDayStatistics(@PathVariable String param) throws Exception {
        return serviceImpl.getThirtyDayStatistics(param);
    }
    
}

