package com.graphai.mall.admin.web.common;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.mall.db.domain.MallAppVersion;
import com.graphai.mall.db.service.common.AppVersionService;

@RestController
@RequestMapping("/admin/version")
@Validated
public class AdminAppVersionController {

    @Autowired
    private AppVersionService appVersionService;

    /**
     * 版本列表
     */
    @RequiresPermissions("admin:version:list")
    @RequiresPermissionsDesc(menu = {"平台管理", "版本管理"}, button = "查询")
    @GetMapping("/list")
    public Object list(String number, String type, String method, String beginTime, String endTime,
                    @RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "17") Integer limit) {
        Map<String, Object> data = new HashMap<>();
        try {
            List<MallAppVersion> list = appVersionService.queryAppVersions(number, type, method, beginTime, endTime,
                            (page - 1) * limit, limit);
            long total = PageInfo.of(list).getTotal();
            data.put("total", total);
            data.put("items", list);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseUtil.ok(data);
    }


    /**
     * 添加版本
     */
    @RequiresPermissions("admin:version:add")
    @RequiresPermissionsDesc(menu = {"平台管理", "版本管理"}, button = "添加")
    @PostMapping("/create")
    public Object create(@RequestBody MallAppVersion appVersion) {
        appVersionService.add(appVersion);
        return ResponseUtil.ok(appVersion);
    }


    /**
     * 详情
     */
    @RequiresPermissionsDesc(menu = {"平台管理", "版本管理"}, button = "详情")
    @GetMapping("/single/{id}")
    public Object single(@PathVariable String id) {
        return ResponseUtil.ok(appVersionService.detail(id));
    }


    /**
     * 删除
     */
    @RequiresPermissions("admin:version:detele")
    @RequiresPermissionsDesc(menu = {"平台管理", "版本管理"}, button = "删除")
    @GetMapping("/del/{id}")
    public Object delete(@PathVariable String id) {

        if (appVersionService.delete(id) == 0) {
            return ResponseUtil.updatedDataFailed();
        }
        return ResponseUtil.ok();
    }

    /**
     * 修改类目
     */
    @RequiresPermissions("admin:version:update")
    @RequiresPermissionsDesc(menu = {"平台管理", "版本管理"}, button = "编辑")
    @PostMapping("/update")
    public Object update(@RequestBody MallAppVersion appVersion) {

        if (appVersionService.updateById(appVersion) == 0) {
            return ResponseUtil.updatedDataFailed();
        }
        return ResponseUtil.ok();
    }

}
