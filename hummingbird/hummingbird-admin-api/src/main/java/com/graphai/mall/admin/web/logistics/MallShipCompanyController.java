package com.graphai.mall.admin.web.logistics;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.framework.generator.controller.IBaseController;
import com.graphai.mall.admin.domain.MallShipCompanyV2;
import com.graphai.mall.admin.service.IMallShipCompanyService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;



/**
 * <p>
 * 快递公司 前端控制器
 * </p>
 *
 * @author biteam
 * @since 2021-07-19
 */
@RestController
@RequestMapping("/admin/logistics")
public class MallShipCompanyController extends IBaseController<MallShipCompanyV2,  String >  {

	@Autowired
    private IMallShipCompanyService serviceImpl;
    
    @RequiresPermissions("admin:logistics:single")
    @RequiresPermissionsDesc(menu = {"单独查询"}, button = "单独查询")
	@GetMapping("/single/{id}")
    public Object getEntityById(@PathVariable  String  id) throws Exception{
        MallShipCompanyV2 entityVName = serviceImpl.getById(id);
        return ResponseUtil.ok(entityVName);
    }
    
    @RequiresPermissions("admin:logistics:addOrUpdate")
    @RequiresPermissionsDesc(menu = {"新增"}, button = "新增")
    @PostMapping("/addOrUpdate")
    public Object addOrUpdate(MallShipCompanyV2 mallShipCompany) throws Exception{
        if(serviceImpl.saveOrUpdate(mallShipCompany)){
            return ResponseUtil.ok();
        }else{
            return ResponseUtil.fail();
        }
    } 
	
	
	@RequiresPermissions("admin:logistics:melist")
	@RequiresPermissionsDesc(menu = {"快递公司查询"}, button = "查询")
    @GetMapping("/melist")
    public Object getMeList() throws Exception {
        IPage<MallShipCompanyV2> ipage = serviceImpl.page(getPlusPage(),new QueryWrapper<MallShipCompanyV2>().lambda().orderByDesc(MallShipCompanyV2::getCreateTime));
        return ResponseUtil.ok(ipage);
    }
 	
 	@RequiresPermissions("admin:logistics:save")
 	@RequiresPermissionsDesc(menu = {"快递公司新增"}, button = "新增")
    @PostMapping("/save")
    public Object save(@RequestBody MallShipCompanyV2 mallShipCompany) throws Exception {
        serviceImpl.save(mallShipCompany);
        return ResponseUtil.ok();
    }
	
	@RequiresPermissions("admin:logistics:modify")
	@RequiresPermissionsDesc(menu = {"快递公司修改"}, button = "修改")
    @PutMapping("/modify")
    public Object modify(@RequestBody MallShipCompanyV2 mallShipCompany){
        serviceImpl.updateById(mallShipCompany);
        return  ResponseUtil.ok();
    }

    @RequiresPermissions("admin:logistics:del")
    @RequiresPermissionsDesc(menu = {"快递公司删除"}, button = "删除")
    @DeleteMapping("/del/{id}")
    public Object deleteById(@PathVariable String id){
        serviceImpl.removeById(id);
        return  ResponseUtil.ok();
    }
    
}

