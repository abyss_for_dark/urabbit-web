package com.graphai.mall.admin.job;

import com.graphai.mall.db.constant.coupon.CouponUserConstant;
import com.graphai.mall.db.domain.MallCouponUser;
import com.graphai.mall.db.service.coupon.MallCouponService;
import com.graphai.mall.db.service.coupon.MallCouponUserService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 检测优惠券过期情况
 */
//@Component
public class CouponJob {
    private final Log logger = LogFactory.getLog(CouponJob.class);

    @Autowired
    private MallCouponService couponService;
    @Autowired
    private MallCouponUserService couponUserService;

    /**
     * 每隔一个小时检查
     * TODO
     * 注意，因为是相隔一个小时检查，因此导致优惠券真正超时时间可能比设定时间延迟1个小时
     */
//    @Scheduled(fixedDelay = 60 * 60 * 1000)
    public void checkCouponExpired() {
        logger.info("系统开启任务检查优惠券是否已经过期");
/*        List<MallCoupon> couponList = couponService.queryExpired();
        for (MallCoupon coupon : couponList) {
            coupon.setStatus(CouponConstant.STATUS_EXPIRED);
            couponService.updateById(coupon);
        }*/

        List<MallCouponUser> couponUserList = couponUserService.queryExpired();
        for (MallCouponUser couponUser : couponUserList) {
            couponUser.setStatus(CouponUserConstant.STATUS_EXPIRED);
            couponUserService.update(couponUser);
        }
    }

}
