package com.graphai.mall.admin.web.bmdesign;

import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.mall.db.domain.BmdesignBusinessForm;
import com.graphai.mall.db.service.bmdesign.BmdesignBusinessFormService;
import com.graphai.validator.Order;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 业务形态及规则配置
 */
@RestController
@RequestMapping("/admin/bmdesignBusinessForm")
@Validated
public class AdminBmdesignBusinessFormController {
    private final Log logger = LogFactory.getLog(AdminBmdesignBusinessFormController.class);

    @Autowired
    private BmdesignBusinessFormService bmdesignBusinessFormService;

    /**
     * 查询业务形态及规则配置
     */
    @RequiresPermissions("admin:bmdesignBusinessForm:list")
    @RequiresPermissionsDesc(menu = {"平台管理", "平台管理"}, button = "查询")
    @GetMapping("/list")
    public Object list(BmdesignBusinessForm bmdesignBusinessForm,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit,
                       @RequestParam(defaultValue = "id") String sort,
                       @Order @RequestParam(defaultValue = "desc") String order) {
        List<BmdesignBusinessForm> list = bmdesignBusinessFormService.selectBmdesignBusinessFormList(bmdesignBusinessForm, page, limit, sort, order);
        long total = PageInfo.of(list).getTotal();
        Map<String, Object> data = new HashMap<>();
        data.put("total", total);
        data.put("items", list);

        return ResponseUtil.ok(data);
    }

    /**
     * 添加业务形态及规则配置
     */
    @RequiresPermissions("admin:bmdesignBusinessForm:add")
    @RequiresPermissionsDesc(menu = {"平台管理", "平台管理"}, button = "新增")
    @PostMapping("/add")
    public Object addSave(@RequestBody BmdesignBusinessForm bmdesignBusinessForm) {
        Object error = validate(bmdesignBusinessForm);
        if (error != null) {
            return error;
        }
        bmdesignBusinessFormService.insertBmdesignBusinessForm(bmdesignBusinessForm);
        return ResponseUtil.ok();
    }


    /**
     * 查看业务形态及规则配置详情
     */
    @RequiresPermissions("admin:bmdesignBusinessForm:detail")
    @RequiresPermissionsDesc(menu = {"平台管理", "平台管理"}, button = "详情")
    @GetMapping("/edit/{id}")
    public Object edit(@PathVariable("id") String id) {
        BmdesignBusinessForm bmdesignBusinessForm = bmdesignBusinessFormService.selectBmdesignBusinessFormById(id);
        return ResponseUtil.ok(bmdesignBusinessForm);
    }

    /**
     * 修改业务形态及规则配置
     */
    @RequiresPermissions("admin:bmdesignBusinessForm:edit")
    @RequiresPermissionsDesc(menu = {"平台管理", "平台管理"}, button = "编辑")
    @PostMapping("/edit")
    public Object editSave(@RequestBody BmdesignBusinessForm bmdesignBusinessForm) {
        //等级主键id
        if (StringUtils.isEmpty(bmdesignBusinessForm.getId())) {
            return ResponseUtil.badArgumentValue("没有获取到id！");
        }
        Object error = validate(bmdesignBusinessForm);
        if (error != null) {
            return error;
        }
        bmdesignBusinessFormService.updateBmdesignBusinessForm(bmdesignBusinessForm);
        return ResponseUtil.ok();
    }


    /**
     * 校验数据(业务形态及规则配置)
     */
    private Object validate(BmdesignBusinessForm bmdesignBusinessForm) {
        if (StringUtils.isEmpty(bmdesignBusinessForm.getPid())) {
            return ResponseUtil.badArgument();
        }
        if (StringUtils.isEmpty(bmdesignBusinessForm.getFormName())) {
            return ResponseUtil.badArgument();
        }
        if (StringUtils.isEmpty(bmdesignBusinessForm.getFormType())) {
            return ResponseUtil.badArgument();
        }
//        if (bmdesignBusinessForm.getProportion() == null) {
//            return ResponseUtil.badArgument();
//        }
//        if (bmdesignBusinessForm.getReward() == null) {
//            return ResponseUtil.badArgument();
//        }
//        if (bmdesignBusinessForm.getNumber() == null) {
//            return ResponseUtil.badArgument();
//        }
//        if (bmdesignBusinessForm.getIsContinuity() == null) {
//            return ResponseUtil.badArgument();
//        }
//        if (bmdesignBusinessForm.getCycle() == null) {
//            return ResponseUtil.badArgument();
//        }
//        if (StringUtils.isEmpty(bmdesignBusinessForm.getCycleUnit())) {
//            return ResponseUtil.badArgument();
//        }
//        if (StringUtils.isEmpty(bmdesignBusinessForm.getRewardType())) {
//            return ResponseUtil.badArgument();
//        }

        return null;
    }
}
