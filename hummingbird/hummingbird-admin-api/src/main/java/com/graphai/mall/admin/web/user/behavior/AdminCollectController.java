package com.graphai.mall.admin.web.user.behavior;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.framework.system.service.MallRoleService;
import com.graphai.mall.admin.dao.MallMerchantMapper;
import com.graphai.mall.admin.domain.MallMerchant;
import com.graphai.mall.admin.domain.MallUserMerchant;
import com.graphai.mall.admin.service.impl.MallUserMerchantServiceImpl;
import com.graphai.mall.db.constant.collect.MallCollectEnum;
import com.graphai.mall.db.domain.*;
import com.graphai.mall.db.service.goods.MallGoodsService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.service.user.behavior.MallCollectService;
import com.graphai.validator.Order;
import com.graphai.validator.Sort;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;
import java.util.stream.Collectors;

import static org.apache.shiro.SecurityUtils.getSubject;

@RestController
@RequestMapping("/admin/collect")
@Validated
public class AdminCollectController {
    private final Log logger = LogFactory.getLog(AdminCollectController.class);

    @Autowired
    private MallCollectService collectService;

    @Autowired
    private MallGoodsService mallGoodsService;

    @Autowired
    private MallUserService mallUserService;

    @Autowired
    private MallMerchantMapper merchantMapper;

    @Autowired
    private MallRoleService mallRoleService;

    @Autowired
    private MallUserMerchantServiceImpl mallUserMerchantService;


    @RequiresPermissions("admin:collect:list")
    @RequiresPermissionsDesc(menu = {"用户管理", "用户收藏"}, button = "查询")
    @GetMapping("/list")
    public Object list(String userId, String valueId,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit,
                       @Sort @RequestParam(defaultValue = "id") String sort,
                       @Order @RequestParam(defaultValue = "desc") String order) {

        MallCollectListVo collect = new MallCollectListVo();
        collect.setUserId(userId);
        collect.setValueId(valueId);

        List<MallCollect> collectList = null;
        MallAdmin adminUser = (MallAdmin) getSubject().getPrincipal();
        Set<MallRole> mallRoleSet = mallRoleService.findByIds(adminUser.getRoleIds());
        for (MallRole mallRole : mallRoleSet) {
            if ("merchant".equals(mallRole.getRoleKey())) {
                QueryWrapper<MallUserMerchant> wrapper=new QueryWrapper<>();
                wrapper.eq("user_id",adminUser.getUserId());
                MallUserMerchant mallUserMerchant = mallUserMerchantService.getOne(wrapper);
                collect.setMerchantId(mallUserMerchant.getMerchantId());
                collectList = collectService.selectByCollectRole(collect, page, limit);
            }
            if ("plant".equals(mallRole.getRoleKey())) {
                QueryWrapper<MallUserMerchant> wrapper=new QueryWrapper<>();
                wrapper.eq("user_id",adminUser.getUserId());
                MallUserMerchant mallUserMerchant = mallUserMerchantService.getOne(wrapper);
                collect.setMerchantId(mallUserMerchant.getMerchantId());
                collectList = collectService.selectByCollectRole(collect, page, limit);
            }
            if ("admin".equals(mallRole.getRoleKey())) {
                collectList = collectService.querySelective(userId, valueId , page, limit,sort ,order);

            }
        }


        //long total = PageInfo.of(collectList).getTotal();
        Map<String, Object> data = new HashMap<>();


        List<MallCollectListVo> list = new ArrayList();
        List<String> userIds = new ArrayList();
        List<String> goodsIds = new ArrayList();
        List<String> merchantIds = new ArrayList();


        MallCollectListVo collectListVo = null;
        MallGoods mallGoods = null;
        MallUser mallUser = null;
        List<MallGoods> mallGoodsList = null;
        List<MallUser> mallUserList = null;
        List<MallMerchant> mallMerchants = null;

        if (collectList != null) {
            for (MallCollect mallCollect : collectList) {
                if (mallCollect.getType().toString().equals(MallCollectEnum.TYPE_0.getCode())) {
                    userIds.add(mallCollect.getUserId());
                    goodsIds.add(mallCollect.getValueId());
                    merchantIds.add(mallCollect.getMerchantId());
                }

            }
        }
        if (userIds.size() != 0) {
            mallUserList = mallUserService.findByIds(userIds);
        }
        if (goodsIds.size() != 0) {
            mallGoodsList = mallGoodsService.findByIdList(goodsIds);
        }
        if (merchantIds.size() != 0) {
            mallMerchants = merchantMapper.selectBatchIds(merchantIds);
        }

        if (collectList != null) {
            for (MallCollect mallCollect : collectList) {
                collectListVo = new MallCollectListVo();
                collectListVo.setId(mallCollect.getId());
                for (MallUser user : mallUserList) {
                    if (mallCollect.getUserId().equals(user.getId())) {
                        collectListVo.setUserId(user.getId());
                        collectListVo.setNickName(user.getNickname()
                        );
                        collectListVo.setMobile(user.getMobile());
                        collectListVo.setAvatar(user.getAvatar());
                    }
                }
                for (MallGoods goods : mallGoodsList) {
                    if (mallCollect.getValueId().equals(goods.getId())) {
                        collectListVo.setValueId(goods.getId());
                        collectListVo.setName(goods.getName());
                        collectListVo.setShopName(goods.getShopname());
                        collectListVo.setShopImg(goods.getPicUrl());
                    }
                }
                for (MallMerchant mallMerchant : mallMerchants) {
                    if (StrUtil.isNotBlank(mallCollect.getMerchantId())&& mallCollect.getMerchantId().equals(mallMerchant.getId())){
                        //mallCollectListVo.setMerchantId(mallMerchant.getId());
                        collectListVo.setMerchantName(mallMerchant.getName());
                        collectListVo.setMerchantImg(mallMerchant.getImg());
                    }
                }
                collectListVo.setAddTime(mallCollect.getAddTime());
                list.add(collectListVo);
            }

            long total = PageInfo.of(collectList).getTotal();
            data.put("total", total);
            data.put("items", list);
        }

        return ResponseUtil.ok(data);
    }
}
