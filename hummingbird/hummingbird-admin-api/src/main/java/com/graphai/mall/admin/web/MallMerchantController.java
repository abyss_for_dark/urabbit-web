package com.graphai.mall.admin.web;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.bcrypt.BCryptPasswordEncoder;
import com.graphai.commons.util.lang.StringUtils;
import com.graphai.commons.util.servlet.ServletUtils;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.framework.generator.controller.IBaseController;
import com.graphai.framework.system.domain.GraphaiSystemUserRole;
import com.graphai.framework.system.service.IGraphaiSystemRoleService;
import com.graphai.framework.system.service.IGraphaiSystemUserRoleService;
import com.graphai.framework.system.service.MallRoleService;
import com.graphai.id.GraphaiIdGenerator;
import com.graphai.mall.admin.domain.MallMerchant;
import com.graphai.mall.admin.domain.MallUserMerchant;
import com.graphai.mall.admin.service.IMallMerchantService;
import com.graphai.mall.admin.service.IMallUserMerchantService;
import com.graphai.mall.db.constant.user.MallUserEnum;
import com.graphai.mall.db.domain.*;
import com.graphai.mall.db.service.MallAdminService;
import com.graphai.mall.db.service.account.AccountStatus;
import com.graphai.mall.db.service.category.MallCategoryService;
import com.graphai.mall.db.service.goods.MallGoodsService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.service.user.behavior.MallAddressService;
import com.graphai.mall.db.vo.MallMerchantVo;
import com.xxl.job.admin.core.alarm.JobAlarmer;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.*;


/**
 * <p>
 * 店铺表  前端控制器
 * </p>
 *
 * @author biteam
 * @since 2021-06-25
 */
@RestController
@RequestMapping("/admin/admin/merchant")
public class MallMerchantController extends IBaseController<MallMerchant, String> {
    private static Logger logger = LoggerFactory.getLogger(JobAlarmer.class);
    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");

    @Autowired
    private IMallMerchantService serviceImpl;
    @Autowired
    private IGraphaiSystemRoleService graphaiSystemUserRoleService;
    @Autowired
    private MallUserService mallUserService;
    @Autowired
    private MallAdminService mallAdminService;
    @Autowired
    private IMallUserMerchantService mallUserMerchantService;
    @Autowired
    private MallRoleService mallRoleService;
    @Autowired
    private MallCategoryService mallCategoryService;
    @Autowired
    private MallAddressService mallAddressService;

    @RequiresPermissions("admin:admin:merchant:single")
    @RequiresPermissionsDesc(menu = {"单独查询"}, button = "单独查询")
    @GetMapping("/single/{id}")
    public Object getEntityById(@PathVariable String id) throws Exception {
        System.out.println("============================" + id);
        MallMerchant entityVName = serviceImpl.getById(id);

        MallMerchantVo mallMerchantVo = new MallMerchantVo(entityVName);
        MallCategory categories = mallCategoryService.findById(mallMerchantVo.getCategory());

        mallMerchantVo.setCategoryTwoName(categories.getName());
        mallMerchantVo.setCategoryOneId(categories.getPid());
        if (mallMerchantVo.getType() == 0) {
            mallMerchantVo.setCategoryOneId(mallMerchantVo.getCategory());
        }

        if (categories.getId().equals(mallMerchantVo.getCategoryOneId())) {
            mallMerchantVo.setCategoryOneName(categories.getName());
        }


        return ResponseUtil.ok(mallMerchantVo);
    }

    @RequiresPermissions("admin:admin:merchant:login")
    @RequiresPermissionsDesc(menu = {"根据登录人获取商户"}, button = "根据登录人获取商户")
    @GetMapping("/single/login")
    public Object getEntityByLogin() throws Exception {
        MallAdmin adminUser = (MallAdmin) SecurityUtils.getSubject().getPrincipal();
        QueryWrapper<MallUserMerchant> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", adminUser.getUserId());
        MallUserMerchant mallUserMerchant = mallUserMerchantService.getOne(queryWrapper);
        MallMerchant entityVName = serviceImpl.getById(mallUserMerchant.getMerchantId());
        List<Map> fileList = new LinkedList<>();
        if (ObjectUtil.isNotEmpty(entityVName.getImg()) && ObjectUtil.isNotNull(entityVName.getImg())) {
            String[] files = entityVName.getImg().split(",");
            for (int i = 0; i < files.length; i++) {
                Map<String, String> file = new HashMap<>();
                file.put("name", "图" + (i + 1));
                file.put("url", files[i]);
                fileList.add(file);
            }
        }

        entityVName.setOrderId(mallCategoryService.findById(entityVName.getCategory()).getName());


        HashMap<String, Object> result = new HashMap<>();
        result.put("files", fileList);
        result.put("item", entityVName);

        return ResponseUtil.ok(result);
    }


    @GetMapping("/plants")
    public Object getPlants() throws Exception {
        QueryWrapper<MallMerchant> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("type", 0);
        queryWrapper.eq("status", 0);
        queryWrapper.eq("audit_status", 1);
        List<MallMerchant> result = serviceImpl.list(queryWrapper);
        return ResponseUtil.ok(result);
    }


    @RequiresPermissions("admin:admin:merchant:address")
    @RequiresPermissionsDesc(menu = {"根据登录人获取退货地址"}, button = "根据登录人获取退货地址")
    @GetMapping("/single/address/{type}")
    public Object singleAddress(@PathVariable("type") String type) throws Exception {
        System.out.println("***************************" + type);
        MallAdmin adminUser = (MallAdmin) SecurityUtils.getSubject().getPrincipal();
        MallAddress mallAddress = mallAddressService.queryByUidAndIsDefault(adminUser.getUserId(), type);
        return ResponseUtil.ok(mallAddress);
    }

    /**
     * @param id
     * @param auditStatus 1：通过，2：驳回
     * @param auditMsg    驳回原因（可不传）
     * @return 审核通过创建对应的admin和role
     * @throws Exception
     */

    @GetMapping("/audit/{id}")
    public Object audit(@PathVariable String id, @RequestParam Integer auditStatus, @RequestParam(required = false) Integer level, @RequestParam(required = false) String auditMsg) throws Exception {
        MallMerchant entityVName = serviceImpl.getById(id);

        if (ObjectUtil.isNotEmpty(entityVName) && ObjectUtil.isNotNull(entityVName)) {
            if (auditStatus == 1) {
                entityVName.setStatus(0);//审核通过将商户状态改为正常营业
            }
            entityVName.setAuditStatus(auditStatus);
            entityVName.setAuditMsg(auditMsg);
            entityVName.setEnterTime(simpleDateFormat.format(new Date()));
            switch (entityVName.getType()) {
                //工厂
                case 0:
                    //如果审核通过则创建管理员账号
                    if (auditStatus.equals(AccountStatus.AUDIT_STATUS_1)) {
                        QueryWrapper<MallUserMerchant> queryWrapper = new QueryWrapper<>();
                        queryWrapper.eq("merchant_id", entityVName.getId());
                        MallUserMerchant mallUserMerchant = mallUserMerchantService.getOne(queryWrapper);
                        MallAdmin mallAdmin = new MallAdmin();
                        mallAdmin.setUserId(mallUserMerchant.getUserId());
                        mallAdmin.setUsername(entityVName.getPhone());
                        mallAdmin.setMobile(entityVName.getPhone());
                        mallAdmin.setAliasName(entityVName.getContacts());
                        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
                        String encodedPassword = encoder.encode(entityVName.getPhone());
                        mallAdmin.setPassword(encodedPassword);
                        String[] roleIds = new String[1];
                        roleIds[0] = mallRoleService.queryByRoleKey("plant").getId();

                        mallAdmin.setRoleIds(roleIds);
                        mallAdminService.add(mallAdmin);

                        GraphaiSystemUserRole systemUserRole = new GraphaiSystemUserRole();
                        systemUserRole.setUserId(mallAdmin.getId());
                        systemUserRole.setRoleId(roleIds[0]);
                        systemUserRole.setRelaId(GraphaiIdGenerator.nextId("GraphaiSystemUserRole"));
                        List<GraphaiSystemUserRole> adminRole = new ArrayList<>();
                        adminRole.add(systemUserRole);
                        graphaiSystemUserRoleService.batchUserRole(adminRole);
                        //审核通过修改用户等级
                        MallUser mallUser = mallUserService.findById(mallUserMerchant.getUserId());
                        mallUser.setLevelId(MallUserEnum.LEVEL_ID_98.getCode());
                        mallUser.setFirstLeader("");
                        mallUserService.updateById(mallUser);
                    }
                    break;
                //商户（无论审核通过还是驳回都不会创建账号）
                case 1:
                    entityVName.setLevel(level);
                    break;
            }
        } else {
            return ResponseUtil.fail("未查询到商户");
        }
        logger.info("工厂审核！！！！！！！！！");
        logger.info(entityVName.getEnterTime());
        if (serviceImpl.saveOrUpdate(entityVName)) {
            return ResponseUtil.ok();
        } else {
            return ResponseUtil.fail();
        }
    }

    @RequiresPermissions("admin:admin:merchant:addOrUpdate")
    @RequiresPermissionsDesc(menu = {"新增"}, button = "新增")
    @PostMapping("/addOrUpdate")
    public Object addOrUpdate(@RequestBody MallMerchant mallMerchant) throws Exception {
        if (serviceImpl.saveOrUpdate(mallMerchant)) {
            return ResponseUtil.ok();
        } else {
            return ResponseUtil.fail();
        }
    }


    @RequiresPermissions("admin:admin:merchant:melist")
    @RequiresPermissionsDesc(menu = {"店铺表 查询"}, button = "查询")
    @GetMapping("/melist")
    public Object getMeList() throws Exception {
        IPage<MallMerchant> ipage = serviceImpl.page(getPlusPage(), listQueryWrapper());

        IPage<MallMerchantVo> ipage1 = new Page<>();
        List<MallMerchant> records = ipage.getRecords();
        List<MallMerchantVo> mallMerchantVos = new ArrayList<>();
        List<String> ids = new ArrayList<>();
        if (ObjectUtil.isNotEmpty(records)) {
            for (MallMerchant item : records) {
                MallMerchantVo it = new MallMerchantVo(item);
                ids.add(it.getCategory());
                mallMerchantVos.add(it);
            }
            List<MallCategory> categories = mallCategoryService.queryByIds(ids);

            for (MallMerchantVo item : mallMerchantVos) {
                for (MallCategory it : categories) {
                    if (item.getCategory().equals(it.getId())) {
                        item.setCategoryTwoName(it.getName());
                        item.setCategoryOneId(it.getPid());
                        if (item.getType() == 0) {
                            item.setCategoryOneId(item.getCategory());
                        }
                    }
                }
                for (MallCategory it : categories) {
                    if (it.getId().equals(item.getCategoryOneId())) {
                        item.setCategoryOneName(it.getName());
                    }
                }
            }
        }


        ipage1.setRecords(mallMerchantVos);
        ipage1.setCurrent(ipage.getCurrent());
        ipage1.setPages(ipage.getPages());
        ipage1.setSize(ipage.getSize());
        ipage1.setTotal(ipage.getTotal());
        //统计总数
        List<MallMerchant> list = serviceImpl.list(listQueryWrapper());

        Integer factory = 0;
        Integer store = 0;
        Integer merchant = 0;
        Integer neighbourhood = 0;

        Integer onLine = 0;
        Integer offLine = 0;
        Integer o2o = 0;

        Integer high = 0;
        Integer middle = 0;
        Integer low = 0;

        Integer enter = 0;
        Integer audit = 0;
        Integer reject = 0;

        if (ObjectUtil.isNotEmpty(list)) {
            for (MallMerchant mallMerchant : list) {
                if (ObjectUtil.isNotNull(mallMerchant.getLevel()) && ObjectUtil.isNotEmpty(mallMerchant.getLevel())) {
                    switch (mallMerchant.getLevel()) {
                        case 0:
                            low++;
                            break;
                        case 1:
                            middle++;
                            break;
                        case 2:
                            high++;
                            break;
                    }
                }
                switch (mallMerchant.getType()) {
                    case 0:
                        factory++;
                        break;
                    case 1:
                        store++;
                        break;
                    case 2:
                        merchant++;
                        break;
                    case 3:
                        neighbourhood++;
                        break;
                }
                if (ObjectUtil.isNotNull(mallMerchant.getPattern()) && ObjectUtil.isNotEmpty(mallMerchant.getPattern())) {
                    switch (mallMerchant.getPattern()) {
                        case 0:
                            onLine++;
                            break;
                        case 1:
                            offLine++;
                            break;
                        case 2:
                            o2o++;
                            break;
                    }
                }
                switch (mallMerchant.getAuditStatus()) {
                    case 0:
                        audit++;
                        break;
                    case 1:
                        enter++;
                        break;
                    case 2:
                        reject++;
                        break;
                }

            }
        }
        Map<String, Object> result = new HashMap<>();
        Map<String, Integer> statistics = new HashMap<>();

        statistics.put("factory", factory);
        statistics.put("store", store);
        statistics.put("merchant", merchant);
        statistics.put("neighbourhood", neighbourhood);
        statistics.put("onLine", onLine);
        statistics.put("offLine", offLine);
        statistics.put("o2o", o2o);
        statistics.put("high", high);
        statistics.put("middle", middle);
        statistics.put("low", low);
        statistics.put("enter", enter);
        statistics.put("audit", audit);
        statistics.put("reject", reject);


        result.put("statistics", statistics);
        result.put("ipage", ipage1);
        return ResponseUtil.ok(result);
    }

    private QueryWrapper<MallMerchant> listQueryWrapper() {
        QueryWrapper<MallMerchant> meq = new QueryWrapper<>();

        if (StringUtils.isNotEmpty(ServletUtils.getParameter("level"))) {
            meq.eq("level", ServletUtils.getParameter("level"));
        }
        if (StringUtils.isNotEmpty(ServletUtils.getParameter("type"))) {
            meq.eq("type", ServletUtils.getParameter("type"));
        }
        if (StringUtils.isNotEmpty(ServletUtils.getParameter("pattern"))) {
            meq.eq("pattern", ServletUtils.getParameter("pattern"));
        }
        if (StringUtils.isNotEmpty(ServletUtils.getParameter("category"))) {
            List<String> categoryIds = new ArrayList<>();
            categoryIds.add(ServletUtils.getParameter("category"));
            if (StringUtils.isNotEmpty(ServletUtils.getParameter("categoryOneId"))) {
                categoryIds.add(ServletUtils.getParameter("categoryOneId"));
            }
            meq.in("category", categoryIds);
        }
        if (StringUtils.isNotEmpty(ServletUtils.getParameter("title"))) {
            meq.eq("title", ServletUtils.getParameter("title"));
        }
        if (StringUtils.isNotEmpty(ServletUtils.getParameter("name"))) {
            meq.and(wrapper -> wrapper.like("name", ServletUtils.getParameter("name"))
                    .or().like("contacts", ServletUtils.getParameter("name"))
                    .or().like("phone", ServletUtils.getParameter("name")));
        }
        if (StringUtils.isNotEmpty(ServletUtils.getParameter("log"))) {
            meq.eq("log", ServletUtils.getParameter("log"));
        }
        if (StringUtils.isNotEmpty(ServletUtils.getParameter("starLevel"))) {
            meq.eq("star_level", ServletUtils.getParameter("starLevel"));
        }
        if (StringUtils.isNotEmpty(ServletUtils.getParameter("browseNum"))) {
            meq.eq("browse_num", ServletUtils.getParameter("browseNum"));
        }
        if (StringUtils.isNotEmpty(ServletUtils.getParameter("info"))) {
            meq.eq("info", ServletUtils.getParameter("info"));
        }
        if (StringUtils.isNotEmpty(ServletUtils.getParameter("img"))) {
            meq.eq("img", ServletUtils.getParameter("img"));
        }
        if (StringUtils.isNotEmpty(ServletUtils.getParameter("businessBegin"))) {
            meq.eq("business_begin", ServletUtils.getParameter("businessBegin"));
        }
        if (StringUtils.isNotEmpty(ServletUtils.getParameter("businessEnd"))) {
            meq.eq("business_end", ServletUtils.getParameter("businessEnd"));
        }
        if (StringUtils.isNotEmpty(ServletUtils.getParameter("businessHoursBegin"))) {
            meq.eq("business_hours_begin", ServletUtils.getParameter("businessHoursBegin"));
        }
        if (StringUtils.isNotEmpty(ServletUtils.getParameter("businessHoursEnd"))) {
            meq.eq("business_hours_end", ServletUtils.getParameter("businessHoursEnd"));
        }
        if (StringUtils.isNotEmpty(ServletUtils.getParameter("provinceId"))) {
            meq.eq("province_id", ServletUtils.getParameter("provinceId"));
        }
        if (StringUtils.isNotEmpty(ServletUtils.getParameter("cityId"))) {
            meq.eq("city_id", ServletUtils.getParameter("cityId"));
        }
        if (StringUtils.isNotEmpty(ServletUtils.getParameter("countyId"))) {
            meq.eq("county_id", ServletUtils.getParameter("countyId"));
        }
        if (StringUtils.isNotEmpty(ServletUtils.getParameter("province"))) {
            meq.eq("province", ServletUtils.getParameter("province"));
        }
        if (StringUtils.isNotEmpty(ServletUtils.getParameter("city"))) {
            meq.eq("city", ServletUtils.getParameter("city"));
        }
        if (StringUtils.isNotEmpty(ServletUtils.getParameter("county"))) {
            meq.eq("county", ServletUtils.getParameter("county"));
        }
        if (StringUtils.isNotEmpty(ServletUtils.getParameter("addressDetail"))) {
            meq.like("address_detail", ServletUtils.getParameter("addressDetail"));
        }
        if (StringUtils.isNotEmpty(ServletUtils.getParameter("longitude"))) {
            meq.eq("longitude", ServletUtils.getParameter("longitude"));
        }
        if (StringUtils.isNotEmpty(ServletUtils.getParameter("latitude"))) {
            meq.eq("latitude", ServletUtils.getParameter("latitude"));
        }
        if (StringUtils.isNotEmpty(ServletUtils.getParameter("remarks"))) {
            meq.eq("remarks", ServletUtils.getParameter("remarks"));
        }
        if (StringUtils.isNotEmpty(ServletUtils.getParameter("tenantId"))) {
            meq.eq("tenant_id", ServletUtils.getParameter("tenantId"));
        }
        if (StringUtils.isNotEmpty(ServletUtils.getParameter("businessLicense"))) {
            meq.eq("business_license", ServletUtils.getParameter("businessLicense"));
        }
        if (StringUtils.isNotEmpty(ServletUtils.getParameter("code"))) {
            meq.eq("code", ServletUtils.getParameter("code"));
        }
        if (StringUtils.isNotEmpty(ServletUtils.getParameter("status"))) {
            meq.eq("status", ServletUtils.getParameter("status"));
        }
        if (StringUtils.isNotEmpty(ServletUtils.getParameter("auditStatus"))) {
            meq.eq("audit_status", ServletUtils.getParameter("auditStatus"));
        }
        if (StringUtils.isNotEmpty(ServletUtils.getParameter("auditMsg"))) {
            meq.eq("audit_msg", ServletUtils.getParameter("auditMsg"));
        }
        if (StringUtils.isNotEmpty(ServletUtils.getParameter("createTime")) && StringUtils.isNotEmpty(ServletUtils.getParameter("updateTime"))) {
            meq.and(wrapper -> wrapper
                    .ge("create_time", ServletUtils.getParameter("createTime"))
                    .le("create_time", ServletUtils.getParameter("updateTime"))
                    .or(wrapper1 -> wrapper1.ge("enter_time", ServletUtils.getParameter("createTime"))
                            .le("enter_time", ServletUtils.getParameter("updateTime"))));
        }
        if (StringUtils.isNotEmpty(ServletUtils.getParameter("createId"))) {
            meq.eq("create_id", ServletUtils.getParameter("createId"));
        }
        if (StringUtils.isNotEmpty(ServletUtils.getParameter("updateId"))) {
            meq.eq("update_id", ServletUtils.getParameter("updateId"));
        }
        meq.orderByDesc("create_time");
        return meq;
    }


    @RequiresPermissions("admin:admin:merchant:save")
    @RequiresPermissionsDesc(menu = {"店铺表 新增"}, button = "新增")
    @PostMapping("/save")
    public Object save(@RequestBody MallMerchant mallMerchant) throws Exception {
        List<MallUser> mallUsers = mallUserService.queryByMobile(mallMerchant.getPhone());
        MallUser mallUser = new MallUser();
        Boolean flag = true;
        if (mallUsers.size() > 0) {
            mallUser = mallUsers.get(0);
            mallUser.setFirstLeader("");
            if (!"0".equals(mallUser.getLevelId())) {
                return ResponseUtil.fail("该手机号已是商家用户");
            } else {
                flag = false;
            }
        } else {
            mallUser.setId(GraphaiIdGenerator.nextId("MallUser"));
            mallUser.setUsername(mallMerchant.getPhone());
            mallUser.setPassword(mallMerchant.getPhone());
            mallUser.setMobile(mallMerchant.getPhone());
            mallUser.setNickname(mallMerchant.getContacts());
        }


        if (mallMerchant.getType() == 1) {
            switch (mallMerchant.getLevel()) {
                case 0:
                    mallUser.setLevelId(AccountStatus.LEVEL_9);
                    break;
                case 1:
                    mallUser.setLevelId(AccountStatus.LEVEL_10);
                    break;
                case 2:
                    mallUser.setLevelId(AccountStatus.LEVEL_11);
                    break;
            }
        } else if (mallMerchant.getType() == 0) {
            mallMerchant.setCategory(mallMerchant.getCategoryOneId());
            mallUser.setLevelId(AccountStatus.LEVEL_98);
        }

        mallUser.setProvinceCode(Integer.parseInt(mallMerchant.getProvinceId()));
        mallUser.setCityCode(Integer.parseInt(mallMerchant.getCityId()));
        mallUser.setAreaCode(Integer.parseInt(mallMerchant.getCountyId()));
        mallUser.setProvince(mallMerchant.getProvince());
        mallUser.setCity(mallMerchant.getCity());
        mallUser.setCounty(mallMerchant.getCounty());

        if (flag) {
            mallUserService.addUser(mallUser);
        } else {
            mallUserService.updateById(mallUser);
        }
        if (mallMerchant.getAuditStatus() == 1) {
            MallAdmin mallAdmin = new MallAdmin();
            mallAdmin.setAvatar("https://gxkj-mall.oss-cn-shenzhen.aliyuncs.com/mall/upload/1629289906765ka85wpa2rk3z3f4vufzf.jpg");
            mallAdmin.setUserId(mallUser.getId());
            mallAdmin.setUsername(mallMerchant.getPhone());
            mallAdmin.setMobile(mallMerchant.getPhone());
            mallAdmin.setAliasName(mallMerchant.getContacts());
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
            String encodedPassword = encoder.encode(mallMerchant.getPhone());
            mallAdmin.setPassword(encodedPassword);
            String[] roleIds = new String[1];
            if (mallMerchant.getType() == 1) {
                roleIds[0] = mallRoleService.queryByRoleKey("merchant").getId();
            } else if (mallMerchant.getType() == 0) {
                roleIds[0] = mallRoleService.queryByRoleKey("plant").getId();
            }
            mallAdmin.setRoleIds(roleIds);
            mallAdminService.add(mallAdmin);

            GraphaiSystemUserRole systemUserRole = new GraphaiSystemUserRole();
            systemUserRole.setUserId(mallAdmin.getId());
            systemUserRole.setRoleId(roleIds[0]);
            systemUserRole.setRelaId(GraphaiIdGenerator.nextId("GraphaiSystemUserRole"));
            List<GraphaiSystemUserRole> adminRole = new ArrayList<>();
            adminRole.add(systemUserRole);
            graphaiSystemUserRoleService.batchUserRole(adminRole);
        }

        mallMerchant.setId(GraphaiIdGenerator.nextId("MallMerchant"));
        mallMerchant.setStatus(0);
        mallMerchant.setEnterTime(simpleDateFormat.format(new Date()));
        mallMerchant.setCreateTime(simpleDateFormat.format(new Date()));
        mallMerchant.setUpdateTime(simpleDateFormat.format(new Date()));
        serviceImpl.save(mallMerchant);

        MallUserMerchant mallUserMerchant = new MallUserMerchant();
        mallUserMerchant.setId(GraphaiIdGenerator.nextId("MallUserMerchant"));
        mallUserMerchant.setUserId(mallUser.getId());
        mallUserMerchant.setMerchantId(mallMerchant.getId());
        mallUserMerchantService.save(mallUserMerchant);


        return ResponseUtil.ok();
    }

    @RequiresPermissions("admin:admin:merchant:modify")
    @RequiresPermissionsDesc(menu = {"店铺表 修改"}, button = "修改")
    @PostMapping("/modify")
    public Object modify(@RequestBody MallMerchant mallMerchant) {

        MallMerchant mallMerchant1 = serviceImpl.getById(mallMerchant.getId());

        //如果商户类型变了,则给该商户管理员更换角色
        if (!mallMerchant1.getType().equals(mallMerchant.getType())) {
            //根据merchantId查询商户用户关联表，得到userId
            QueryWrapper<MallUserMerchant> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("merchant_id", mallMerchant.getId());
            MallUserMerchant mallUserMerchant = mallUserMerchantService.getOne(queryWrapper);

            //根据userId获取该商户管理员
            MallAdmin mallAdmin = mallAdminService.findByUserId(mallUserMerchant.getUserId());

            //给该管理员替换角色
            String[] roleIds = new String[1];
            if (mallMerchant.getType() == 1 || mallMerchant.getType() == 2) {
                roleIds[0] = mallRoleService.queryByRoleKey("merchant").getId();
            } else if (mallMerchant.getType() == 0) {
                roleIds[0] = mallRoleService.queryByRoleKey("plant").getId();
            }
            mallAdmin.setRoleIds(roleIds);
            mallAdminService.updateById(mallAdmin);
        }

        if (mallMerchant.getType() == 0) {
            mallMerchant.setCategory(mallMerchant.getCategoryOneId());
        }
        serviceImpl.updateById(mallMerchant);
        return ResponseUtil.ok();
    }

    @Autowired
    private MallGoodsService mallGoodsService;
    private static final String DELETE_URL = "http://web.51fangtao.com/wx/all/search/deleteIndexById?id=";

    @RequiresPermissions("admin:admin:merchant:del")
    @RequiresPermissionsDesc(menu = {"店铺表 删除"}, button = "删除")
    @DeleteMapping("/del/{id}")
    public Object deleteById(@PathVariable String id) {
        serviceImpl.removeById(id);
        MallGoodsExample mallGoodsExample = new MallGoodsExample();
        mallGoodsExample.or().andMerchantIdEqualTo(id);
        List<MallGoods> goods = mallGoodsService.selectByExample(mallGoodsExample);
        for(MallGoods item : goods){
            item.setDeleted(true);
            mallGoodsService.updateById(item);
            String url = DELETE_URL + item.getId();
            com.graphai.open.utils.HttpRequestUtils.get(url, null, null);
        }

        return ResponseUtil.ok();
    }

    @RequiresPermissions("admin:admin:merchant:del")
    @RequiresPermissionsDesc(menu = {"店铺表 删除"}, button = "删除")
    @DeleteMapping("/dels/{ids}")
    public Object deleteByIds(@PathVariable String[] ids) {
        if (ids.length > 0) {
            List<String> resultList = new ArrayList<>(Arrays.asList(ids));
            serviceImpl.removeByIds(resultList);
        }
        return ResponseUtil.ok();
    }

}

