package com.graphai.mall.admin.service;

import com.graphai.mall.db.dao.*;
import com.graphai.mall.db.domain.*;
import com.graphai.mall.db.service.goods.MallGoodsService;
import com.graphai.mall.db.vo.MallCommentVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class MallInfoService {

    @Resource
    private MallOrderMapper mallOrderMapper;
    @Resource
    private MallOrderQryMapper mallOrderQryMapper;
    @Resource
    private MallCommentQryMapper mallCommentQryMapper;
    @Resource
    private MallGoodsMapper mallGoodsMapper;
    @Resource
    private MallGoodsQryMapper mallGoodsQryMapper;



    /**
     * 跟据merchantId查询 服务、评价得分
     * **/
    public List<Map<String,Object>> getScoreByMerchantId(String merchantId) {
        return mallCommentQryMapper.getScoreByMerchantId(merchantId);
    }

    /**
     * 根据merchantId查询该门店所属所有订单
     * **/
    public List<Map<String,Object>> queryOrderByMerchantId(String merchantId) {
        return mallOrderQryMapper.queryOrderByMerchantId(merchantId);
    }

    /**
     * 根据factoryMerchantId查询该门店所属所有订单
     * **/
    public List<Map<String,Object>> queryOrderByfactoryMerchantId(String merchantId) {
        return mallOrderQryMapper.queryOrderByfactoryMerchantId(merchantId);
    }


    /**
     * 根据merchantId查询该门店所属所有订单
     * **/
    public List<MallOrder> getOrderByMerchantId(String merchantId) {
        MallOrderExample orderExample = new MallOrderExample();
        orderExample.or().andMerchantIdEqualTo(merchantId);
        return mallOrderMapper.selectByExample(orderExample);
    }

    /**
     * 根据merchantId查询该门店所属所有商品
     * **/
    public List<MallGoods> getGoodsByMerchantId(String merchantId) {
        MallGoodsExample mallGoodsExample = new MallGoodsExample();
        mallGoodsExample.or().andMerchantIdEqualTo(merchantId);
        return mallGoodsMapper.selectByExample(mallGoodsExample);
    }

    /**
     * 根据merchantId查询该门店所属商品数量
     * **/

    public List<Integer> getGoodsNumByMerchantId(String merchantId) {
        List<Integer> goodsList = new ArrayList();
        Integer goodsNum = mallGoodsQryMapper.queryGoodsNumByMerchantId(merchantId);
        Integer reject = mallGoodsQryMapper.queryGoodsStatusByMerchantId(merchantId, 0, 2);
        Integer waitAuth = mallGoodsQryMapper.queryGoodsStatusByMerchantId(merchantId, 0, 1);
        goodsList.add(goodsNum);
        goodsList.add(reject);
        goodsList.add(waitAuth);
        return goodsList;
    }



}
