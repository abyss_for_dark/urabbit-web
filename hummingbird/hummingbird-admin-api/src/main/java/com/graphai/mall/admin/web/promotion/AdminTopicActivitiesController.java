package com.graphai.mall.admin.web.promotion;


import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.mall.db.domain.MallTopicActivities;
import com.graphai.mall.db.service.promotion.MallTopicActivitiesService;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/admin/topic/activities")
@Validated
public class AdminTopicActivitiesController {


    @Autowired
    MallTopicActivitiesService topicActivitiesService;

    @RequiresPermissions("admin:topic:activities:list")
    @RequiresPermissionsDesc(menu = {"主题活动", "活动管理"}, button = "查询")
    @GetMapping("/list")
    public Object list(@RequestParam(required = false) String actName,
                       @RequestParam(required = false) String topicId,
                       @RequestParam(required = false) String actType,
                       @RequestParam(required = false) String linkType,
                       @RequestParam(required = false) String state,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit) {
        return ResponseUtil.ok(topicActivitiesService.selectTopicList(actName, topicId, actType, linkType, state, page, limit));
    }


    @RequiresPermissions("admin:topic:activities:delete")
    @RequiresPermissionsDesc(menu = {"主题活动", "活动管理"}, button = "删除")
    @PostMapping("/delete")
    public Object delete(@NotNull String id) {
        topicActivitiesService.deleteActivityById(id);
        return ResponseUtil.ok();
    }


    @RequiresPermissions("admin:topic:activities:update")
    @RequiresPermissionsDesc(menu = {"主题活动", "活动管理"}, button = "修改")
    @PostMapping("/update")
    public Object update(@RequestBody MallTopicActivities activities) {
        if (StringUtils.isEmpty(activities.getId())) return ResponseUtil.badArgumentValue("id");
        int num = topicActivitiesService.updateActivity(activities);
        if (num > 0) {
            return ResponseUtil.ok();
        }
        if (num == -1) {
            return ResponseUtil.fail(503, "所属主题对应活动类型启用数量超限");
        }
        return ResponseUtil.fail();
    }


    @RequiresPermissions("admin:topic:activities:add")
    @RequiresPermissionsDesc(menu = {"主题活动", "活动管理"}, button = "添加")
    @PostMapping("/add")
    public Object add(@RequestBody MallTopicActivities acts) {
        if (checkIsNull(acts.getActImg(), acts.getTopicId(), acts.getActUrl(), acts.getState())) {
            return ResponseUtil.badArgument();
        }
        int num = topicActivitiesService.addActivity(acts);
        if (num > 0) {
            return ResponseUtil.ok();
        }
        if (num == -1) {
            return ResponseUtil.fail(503, "所属主题对应活动类型启用数量超限");
        }
        return ResponseUtil.fail();
    }


    @RequiresPermissions("admin:topic:activities:detail")
    @RequiresPermissionsDesc(menu = {"主题活动", "活动管理"}, button = "活动详情")
    @GetMapping("/detail")
    public Object detail(@NotNull String id) {
        return ResponseUtil.ok(topicActivitiesService.getActivityDetail(id));
    }


    private boolean checkIsNull(String... args) {
        for (String arg : args) {
            if (StringUtils.isEmpty(arg)) return true;
        }
        return false;
    }


}
