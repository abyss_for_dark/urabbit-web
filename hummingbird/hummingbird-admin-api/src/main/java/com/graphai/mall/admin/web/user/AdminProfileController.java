package com.graphai.mall.admin.web.user;

import static com.graphai.framework.constant.AdminResponseCode.ADMIN_INVALID_ACCOUNT;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.subject.Subject;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.graphai.commons.util.NameUtil;
import com.graphai.commons.util.RegexUtil;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.bcrypt.BCryptPasswordEncoder;
import com.graphai.commons.util.json.JacksonUtil;
import com.graphai.commons.util.json.JacksonUtils;
import com.graphai.commons.util.sms.AliSmsUtils;
import com.graphai.id.GraphaiIdGenerator;
import com.graphai.mall.db.domain.MallAdmin;
import com.graphai.mall.db.domain.MallPhoneValidation;
import com.graphai.mall.db.service.MallAdminService;
import com.graphai.mall.db.service.account.AccountStatus;
import com.graphai.mall.db.service.common.MallPhoneValidationService;

@RestController
@RequestMapping("/admin/profile")
@Validated
public class AdminProfileController {
    private final Log logger = LogFactory.getLog(AdminProfileController.class);

    @Resource
    private MallAdminService adminService;
    @Resource
    private MallAdminService mallAdminService;
    @Resource
    private MallPhoneValidationService phoneService;

    @RequiresAuthentication
    @PostMapping("/password")
    public Object create(@RequestBody String body) {
        String oldPassword = JacksonUtil.parseString(body, "oldPassword");
        String newPassword = JacksonUtil.parseString(body, "newPassword");
        if (StringUtils.isEmpty(oldPassword)) {
            return ResponseUtil.badArgument();
        }
        if (StringUtils.isEmpty(newPassword)) {
            return ResponseUtil.badArgument();
        }

        Subject currentUser = SecurityUtils.getSubject();
        MallAdmin admin = (MallAdmin) currentUser.getPrincipal();

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        if (!encoder.matches(oldPassword, admin.getPassword())) {
            return ResponseUtil.fail(ADMIN_INVALID_ACCOUNT, "账号密码不对");
        }

        String encodedNewPassword = encoder.encode(newPassword);
        admin.setPassword(encodedNewPassword);

        adminService.updateById(admin);
        return ResponseUtil.ok();
    }

    /**
     * 个人中心-详情
     *
     * @return Object
     */
    @PostMapping("/personal/detail")
    public Object getDetail() {
        //获取当前用户
        MallAdmin adminUser = (MallAdmin) SecurityUtils.getSubject().getPrincipal();
        MallAdmin user = mallAdminService.findById(adminUser.getId());
        if (StringUtils.isEmpty(user)) {
            return ResponseUtil.fail();
        }
        //登录密码和操作密码高度隐私，要隐藏掉，设置为null
        user.setPassword(null);
        user.setOperationPassword(null);
        //手机号码脱敏
        user.setMobile(NameUtil.mobileEncrypt(user.getMobile()));
        return ResponseUtil.ok(user);
    }

    /**
     * 个人中心-发送验证码（修改操作密码专用）
     *
     * @return Object
     */
    @PostMapping("/personal/sendcode")
    public Object sendCode() {

        try {
            //获取当前用户
            MallAdmin adminUser = (MallAdmin) SecurityUtils.getSubject().getPrincipal();
            MallAdmin user = mallAdminService.findById(adminUser.getId());
            if ("".equals(user.getMobile())) {
                return ResponseUtil.fail(403, "未绑定手机号码，请先去绑定");
            }

            String code = String.valueOf(new Random().nextInt(899999) + 100000);//生成6位数字短信验证码
            String templateParam = "{\"code\":\"" + code + "\"}";
            String signName = AccountStatus.SIGNNAME_AGENT;
            String templateCode = AccountStatus.TEMPLATE_CODE_OPERATION;
            String phone = user.getMobile();
            String smsType = "PC_PWD";

            //验证5分钟内的短信不用重复发
            List<MallPhoneValidation> vlist = phoneService.getByMoblieAndvalType(user.getMobile(), smsType);
            if (CollectionUtils.isNotEmpty(vlist)) {
                MallPhoneValidation val = vlist.get(0);
                LocalDateTime localDate = LocalDateTime.now();
                if (val != null && val.getCreateTime().isAfter(localDate.minus(60, ChronoUnit.SECONDS))) {
                    logger.info("说明没有过期val.getCreateTime()......." + val.getCreateTime());
                    logger.info("说明没有过期localDate.minus(600, ChronoUnit.SECONDS)......." + localDate.minus(60, ChronoUnit.SECONDS));
                    return ResponseUtil.ok("验证码未失效，请输入刚刚收到的验证码");
                }
            }

            //发短信
            SendSmsResponse response = AliSmsUtils.sendSms(phone, signName, templateCode, templateParam);
            logger.info("sendSmsResponse结果----- " + JacksonUtils.bean2Jsn(response));
            logger.info("短信接口返回的数据----------------");
            logger.info("Code=" + response.getCode());
            logger.info("Message=" + response.getMessage());
            logger.info("RequestId=" + response.getRequestId());
            logger.info("BizId=" + response.getBizId());
            if ("OK".equals(response.getCode())) {

                //保存到数据库
                MallPhoneValidation record = new MallPhoneValidation();
                record.setId(GraphaiIdGenerator.nextId("MallPhoneValidation"));
                record.setCreateTime(LocalDateTime.now());
                record.setUpdateTime(LocalDateTime.now());
                record.setValCode(code);
                record.setMobile(user.getMobile());
                record.setOvertimeThreshold(300);    //默认过期时间为300秒=5分钟
                record.setValState("01");
                record.setValType(smsType);
                phoneService.insertSelective(record);

                return ResponseUtil.ok("验证码发送成功");
            }

        } catch (Exception e) {
            logger.error("", e);
        }
        return ResponseUtil.ok();
    }

    /**
     * 个人中心-修改操作密码
     *
     * @return Object
     */
    @PostMapping("/personal/updoperationpwd")
    public Object updOperationPwd(@RequestParam String operationPassword, @RequestParam String code) {

        //验证基本信息
        if (StringUtils.isEmpty(operationPassword)) {
            return ResponseUtil.fail(403, "操作失败，操作密码为空");
        }
        if (StringUtils.isEmpty(code)) {
            return ResponseUtil.fail(403, "操作失败，短信验证码为空");
        }

        //获取当前用户
        MallAdmin adminUser = (MallAdmin) SecurityUtils.getSubject().getPrincipal();
        MallAdmin user = mallAdminService.findById(adminUser.getId());
        if ("".equals(user.getMobile())) {
            return ResponseUtil.fail(403, "未绑定手机号码，请先去绑定");
        }

        //验证码校验
        String smsType = "PC_PWD";
        MallPhoneValidation mallPhoneValidation = phoneService.getPhoneValidationByCode(user.getMobile(), code, smsType);
        if (mallPhoneValidation == null) {
            return ResponseUtil.fail(403, "验证码有误");
        }

        //设置操作密码
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String encodedNewPassword = encoder.encode(operationPassword);
        MallAdmin adminUserUpd = new MallAdmin();
        adminUserUpd.setId(adminUser.getId());
        adminUserUpd.setOperationPassword(encodedNewPassword);
        adminUserUpd.setIsItSet(true);
        adminService.updateById(adminUserUpd);

        //修改手机验证码状态-已验证
        MallPhoneValidation updateVal = new MallPhoneValidation();
        updateVal.setId(mallPhoneValidation.getId());
        updateVal.setValState("00");
        phoneService.update(updateVal);

        return ResponseUtil.ok();

    }

    /**
     * 个人中心-修改手机号码
     *
     * @return Object
     */
    @PostMapping("/personal/updmobile")
    public Object updmobile(@RequestParam String newMobile, @RequestParam String operationPWd) {

        //验证基本信息
        if (StringUtils.isEmpty(operationPWd)) {
            return ResponseUtil.fail(403, "操作失败，操作密码为空");
        }
        if (StringUtils.isEmpty(newMobile)) {
            return ResponseUtil.fail(403, "操作失败，手机号码为空");
        }
        if (!RegexUtil.isMobileExact(newMobile)) {
            return ResponseUtil.fail(403, "手机号码格式不符合");
        }

        //获取当前用户
        MallAdmin adminUser = (MallAdmin) SecurityUtils.getSubject().getPrincipal();
        //防止用户刚设置操作密码，没有重新登录，获取不到新的操作密码，所以再去数据库查询一次，确保能获取
        MallAdmin user = mallAdminService.findOperationPwdById(adminUser.getId());

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        if (!encoder.matches(operationPWd, user.getOperationPassword())) {
            return ResponseUtil.fail(ADMIN_INVALID_ACCOUNT, "操作密码不对");
        }

        MallAdmin adminUserUpd = new MallAdmin();
        adminUserUpd.setId(adminUser.getId());
        adminUserUpd.setMobile(newMobile);
        adminService.updateById(adminUserUpd);

        return ResponseUtil.ok();

    }
}
