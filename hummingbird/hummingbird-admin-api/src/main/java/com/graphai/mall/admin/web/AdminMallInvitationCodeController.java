package com.graphai.mall.admin.web;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.json.JacksonUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.mall.db.bo.MallInvitationCodeBo;
import com.graphai.mall.db.domain.BmdesignUserLevel;
import com.graphai.mall.db.domain.MallInvitationCode;
import com.graphai.mall.db.domain.MallUser;
import com.graphai.mall.db.service.MallInvitationCodeService;
import com.graphai.mall.db.service.bmdesign.BmdesignUserLevelService;
import com.graphai.mall.db.service.user.MallUserService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.logging.log4j.util.Strings;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/admin/invitation")
@Validated
public class AdminMallInvitationCodeController {
    private final Log logger = LogFactory.getLog(AdminMallInvitationCodeController.class);

    @Autowired
    private MallInvitationCodeService invitationCodeService;
    @Autowired
    private BmdesignUserLevelService bmdesignUserLevelService;
    @Autowired
    private MallUserService mallUserService;

    /**
     * 1.激活码管理查询
     */
    @RequiresPermissions("admin:invitation:list")
    @RequiresPermissionsDesc(menu = {"激活码", "激活码管理"}, button = "查询")
    @GetMapping("/list")
    public Object list(@RequestParam(required = false) String code,
                       @RequestParam(required = false) Integer isPrint,
                       @RequestParam(required = false) String useStatus,
                       @RequestParam(required = false) String userName,
                       @RequestParam(required = false) String spendUserName,
                       @RequestParam(required = false) String levelId,
                       @RequestParam(required = false) String createTime, @RequestParam(required = false) String createTime2,
                       @RequestParam(required = false) String useTime, @RequestParam(required = false) String useTime2,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit) {
        List<MallInvitationCodeBo> mallInvitationCodeBoList = invitationCodeService.queryinvitationList(code, isPrint, useStatus,
                userName, spendUserName, levelId,
                createTime, createTime2,
                useTime, useTime2, page, limit);
        long total = PageInfo.of(mallInvitationCodeBoList).getTotal();

        Map<String, Object> data = new HashMap<>();
        data.put("total", total);
        data.put("items", mallInvitationCodeBoList);
        return ResponseUtil.ok(data);
    }

    /**
     * 2.查询等级-身份列表
     *
     * @return Object
     */
    @GetMapping("/selectUserLevel")
    public Object selectIndustry() {
        List<BmdesignUserLevel> list = bmdesignUserLevelService.selectUserLevelList();
        Map<String, Object> data = new HashMap<>();
        data.put("items", list);
        return ResponseUtil.ok(data);
    }

    /**
     * 3.批量生成激活码
     */
    @RequiresPermissions("admin:invitation:create")
    @RequiresPermissionsDesc(menu = {"激活码", "激活码管理"}, button = "生成激活码")
    @PostMapping("/createBatchCode")
    public Object createBatchCode(@RequestBody String data) {
        JSONObject jsonObject = JSONObject.parseObject(data);
        String number = (String) jsonObject.get("number");
        String excelFlag = (String) jsonObject.get("excelFlag");
        String use = (String) jsonObject.get("use");
        String levelId = (String) jsonObject.get("levelId");
        String partner = (String) jsonObject.get("partner"); //合作商id -- 目前先存到拥有者id
        Object error = validate(number, excelFlag, use, levelId);
        if (error != null) {
            return error;
        }
        if (StringUtils.isEmpty(number)) {
            number = "1";//生成数量不填默认为1
        }
        Integer num = Integer.parseInt(number);
        Integer useInt = Integer.parseInt(use);
        List<MallInvitationCode> list = new ArrayList<>();
        for (int i = 0; i < num; i++) {
            MallInvitationCode mallInvitationCode = new MallInvitationCode();
            if (useInt.equals(1)) {
                mallInvitationCode.setIsPrint(true);
            } else {
                mallInvitationCode.setIsPrint(false);
            }
            if (Strings.isNotEmpty(partner)) {
                mallInvitationCode.setUserId(partner);
            }
            MallInvitationCode invitation = invitationCodeService.insertMallInvitationCodeBatch(mallInvitationCode);
            //如果需求要导出excel返回list数据给前端导出
            if ("1".equals(excelFlag)) {
                list.add(invitation);
            }
        }
//        System.out.println("导出excel数据="+list);
        Map<String, Object> data2 = new HashMap<>();
        data2.put("items", list);
        return ResponseUtil.ok(data2);
    }

    /**
     * 4.查询用户列表-通过等级id来查询
     *
     * @return Object
     */
    @PostMapping("/selectUserByLevelId")
    public Object selectUserByLevelId(@RequestBody String data) {
        String levelId = JacksonUtil.parseString(data, "levelId");
        logger.info("levelId：" + levelId);
        Map<String, Object> dataResult = new HashMap<>();
        if (!StringUtils.isEmpty(levelId)) {
            List<MallUser> list = mallUserService.queryByIevelId(levelId);
            dataResult.put("items", list);
        }
        return ResponseUtil.ok(dataResult);
    }


    /**
     * 批量生成激活码 验证
     *
     * @param number    生成数量
     * @param excelFlag 是否导出excel 1-是  0-否
     * @param use       1-实体卡 0-电子卡
     * @param levelId   等级id
     * @return Object
     */
    private Object validate(String number, String excelFlag, String use, String levelId) {
        if (StringUtils.isEmpty(number)) {
            number = "1";//生成数量不填默认为1
        }
        if (Integer.parseInt(number) <= 0) {
            return ResponseUtil.badArgumentValue();
        }
        if (StringUtils.isEmpty(use)) {
            return ResponseUtil.badArgument();
        }
        return null;
    }

//    /**
//     * 激活码管理查询-备用
//     * @param code 激活码
//     * @param userId 拥有者id（为空表示未分配，反之）
//     * @param spendUserId 所属合作商
//     * @param isPrint 是否是实体卡(0：否 1：是)
//     * @param useStatus 激活状态：111-已激活 000-未激活
//     * @param createBiginTime 创建开始时间
//     * @param createEndTime 创建结束时间
//     * @param page 页数
//     * @param limit 显示条数
//     * @param sort 排序字段
//     * @param order 排序方式
//     * @param useBeginTime 使用开始时间
//     * @param useEndTime 使用结束时间
//     * @return Object
//     */
//    @RequiresPermissions("admin:invitation:list")
//    @RequiresPermissionsDesc(menu={"激活码" , "激活码管理"}, button="查询")
//    @GetMapping("/list")
//    public Object list(String code, String userId,String spendUserId,
//                       @RequestParam(required = false) List<Boolean> isPrint,
//                       @RequestParam(required = false) List<String> useStatus,
//                       String createBiginTime,String createEndTime,
//                       @RequestParam(defaultValue = "1") Integer page,
//                       @RequestParam(defaultValue = "10") Integer limit,
//                       @RequestParam(defaultValue = "id") String sort,
//                       @Order @RequestParam(defaultValue = "desc") String order,
//                       String useBeginTime,String useEndTime) {
//
//        List<MallInvitationCode> invitationList = invitationCodeService.queryinvitationList(code,userId,spendUserId,isPrint,useStatus, createBiginTime, createEndTime, page, limit, sort, order,useBeginTime,useEndTime);
//        long total = PageInfo.of(invitationList).getTotal();
//        Map<String, Object> data = new HashMap<>();
//        data.put("total", total);
//        data.put("items", invitationList);
//        return ResponseUtil.ok(data);
//    }

}
