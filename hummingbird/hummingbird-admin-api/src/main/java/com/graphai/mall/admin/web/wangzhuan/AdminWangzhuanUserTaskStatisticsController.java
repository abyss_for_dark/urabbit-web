package com.graphai.mall.admin.web.wangzhuan;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.mall.db.service.wangzhuan.WangzhuanUserTaskStatisticsService;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;


/**
 * 任务统计
 *
 * @author LaoGF
 * @since 2020-07-02
 */
@RestController
@RequestMapping("/admin/userTaskStatistics")
@Validated
public class AdminWangzhuanUserTaskStatisticsController {

    @Resource
    private WangzhuanUserTaskStatisticsService wangzhuanUserTaskStatisticsService;

    /**
     * 统计任务量
     */
    @RequiresPermissions("admin:userTaskStatistics:task")
    @RequiresPermissionsDesc(menu = {"数据统计", "任务统计"}, button = "查询")
    @GetMapping("/task")
    public Object statisticsTask(@RequestParam Integer day) {
        return ResponseUtil.ok(wangzhuanUserTaskStatisticsService.statisticsTask(day));
    }

    /**
     * 统计完成任务量
     */
    @GetMapping("/finishTask")
    public Object statisticsFinishTask(@RequestParam Integer day) {
        return ResponseUtil.ok(wangzhuanUserTaskStatisticsService.statisticsFinishTask(day));
    }

    /**
     * 统计待审核任务
     */
    @GetMapping("/approvalTask")
    public Object statisticsApprovalTask(@RequestParam Integer day) {
        return ResponseUtil.ok(wangzhuanUserTaskStatisticsService.statisticsApprovalTask(day));
    }

    /**
     * 统计预估用户总收益
     */
    @GetMapping("reward")
    public Object statisticsReward(@RequestParam Integer day) {
        return ResponseUtil.ok(wangzhuanUserTaskStatisticsService.statisticsReward(day));
    }

    /**
     * 统计任务完成量变化趋势
     */
    @GetMapping("finishTaskGroupByDay")
    public Object statisticsFinishTaskGroupByDay(@RequestParam Integer day) {
        return ResponseUtil.ok(wangzhuanUserTaskStatisticsService.statisticsFinishTaskGroupByDay(day == null ? 30 : day));
    }

}
