package com.graphai.mall.admin.web.promotion;


import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.mall.db.domain.FcAccountBindCard;
import com.graphai.mall.db.domain.MallAdmin;
import com.graphai.mall.db.domain.MallChannel;
import com.graphai.mall.db.service.promotion.PromotionChannelService;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.subject.Subject;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 账户中心
 *
 * @author LaoGF
 * @since 2020-07-13
 */
@RestController
@RequestMapping("/admin/promotionChannelAccount")
@Validated
public class PromotionChannelAccountController {


    @Resource
    private PromotionChannelService promotionChannelService;

    /**
     * 渠道-账户明细
     */
    @RequiresPermissions("admin:promotionChannelAccount:detail")
    @RequiresPermissionsDesc(menu = {"推广渠道后台", "账户中心"}, button = "查询")
    @PostMapping("/detail")
    public Object getDetail() {
        //获取当前用户
        MallAdmin adminUser = (MallAdmin) SecurityUtils.getSubject().getPrincipal();
        return ResponseUtil.ok(promotionChannelService.getDetail(adminUser.getId(), adminUser.getUsername()));
    }

    /**
     * 修改渠道资料
     */
    @RequiresPermissions("admin:promotionChannelAccount:update")
    @RequiresPermissionsDesc(menu = {"推广渠道后台", "账户中心"}, button = "修改")
    @PostMapping("/updateChannel")
    public Object updateChannel(MallChannel mallChannel) {
        return ResponseUtil.ok(promotionChannelService.updateChannel(mallChannel));
    }

    /**
     * 修改admin用户密码
     */
    @RequiresPermissions("admin:promotionChannelAccount:updatePassword")
    @RequiresPermissionsDesc(menu = {"推广渠道后台", "账户中心"}, button = "修改密码")
    @PostMapping("/updatePassword")
    public Object updatePassword(@RequestParam String password,
                                 @RequestParam String newPassword) {
        //获取当前用户
        Subject subject = SecurityUtils.getSubject();
        MallAdmin adminUser = (MallAdmin) subject.getPrincipal();
        boolean update = promotionChannelService.updatePassword(adminUser.getId(), password, newPassword);
        if (update) {
            subject.logout();
            return ResponseUtil.ok(true);
        }
        return ResponseUtil.ok(false);
    }

    /**
     * 修改收款账户信息
     */
    @RequiresPermissions("admin:promotionChannelAccount:updateAccount")
    @RequiresPermissionsDesc(menu = {"推广渠道后台", "账户中心"}, button = "修改账户")
    @PostMapping("/updateAccountBindCard")
    public Object updateAccountBindCard(FcAccountBindCard fcAccountBindCard) {
        return ResponseUtil.ok(promotionChannelService.updateAccountBindCard(fcAccountBindCard));
    }

}
