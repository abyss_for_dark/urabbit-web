package com.graphai.mall.admin.web.finance;


import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.mall.db.service.MallAdminService;
import com.graphai.mall.db.service.account.FcAccountPrechargeBillService;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequestMapping("/admin/prechargeBill")
@Validated
public class AdminPrechargeBillController {

    @Autowired
    FcAccountPrechargeBillService prechargeBillService;
    @Autowired
    MallAdminService mallAdminService;

    /**
     * 代理端预充值列表
     *
     * @param page
     * @param limit
     * @param checkState
     * @return
     */
    @RequiresPermissions("admin:prechargeBill:agent")
    @RequiresPermissionsDesc(menu = {"财务管理", "报单管理"}, button = "查询")
    @GetMapping("/agent")
    public Object list(@RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "20") Integer limit,
                       String checkState,
                       String mobile,
                       String beginTime,
                       String endTime) {
        return ResponseUtil.ok(prechargeBillService.selectAgentOrders(checkState, mobile, beginTime, endTime, page, limit));
    }

    /**
     * 非代理预充值列表
     *
     * @param page
     * @param limit
     * @param checkState
     * @return
     */
    @RequiresPermissions("admin:prechargeBill:common")
    @RequiresPermissionsDesc(menu = {"财务管理", "分账管理"}, button = "查询")
    @GetMapping("/common")
    public Object list(@RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "20") Integer limit,
                       String checkState,
                       String orderSn,
                       String bid,
                       String huid,
                       String mobile,
                       Boolean getBidItem,
                       String beginTime,
                       String endTime,
                       @RequestParam(required = false) List<Integer> region) {
        return ResponseUtil.ok(prechargeBillService.selectCommonOrders(huid, orderSn, checkState, bid, mobile, getBidItem, beginTime, endTime, page, limit,region));
    }

    /**
     * 审核订单
     *
     * @param id
     * @param state
     * @return
     */
    @RequiresPermissions("admin:prechargeBill:audit")
    @RequiresPermissionsDesc(menu = {"财务管理", "分账/报单审核"}, button = "审核")
    @PostMapping("/audit")
    public Object audit(@NotNull String id, @NotNull String state) {
        if (!state.equals("2") && !state.equals("3") && !state.equals("4")) {
            return ResponseUtil.fail(-1, "非法操作");
        }
        return prechargeBillService.audit(id, state);
    }

}
