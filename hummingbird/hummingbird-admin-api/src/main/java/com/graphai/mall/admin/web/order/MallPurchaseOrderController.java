package com.graphai.mall.admin.web.order;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.framework.generator.controller.IBaseController;
import com.graphai.mall.admin.dao.DeviceOrderDRecordMapper;
import com.graphai.mall.admin.domain.MallPurchaseOrder;
import com.graphai.mall.admin.domain.MallPurchaseOrderGoods;
import com.graphai.mall.admin.domain.MallShipCompanyV2;
import com.graphai.mall.admin.domain.MallUserMerchant;
import com.graphai.mall.admin.service.IMallPurchaseOrderGoodsService;
import com.graphai.mall.admin.service.IMallPurchaseOrderService;
import com.graphai.mall.admin.service.IMallShipCompanyService;
import com.graphai.mall.admin.service.impl.MallUserMerchantServiceImpl;
import com.graphai.mall.admin.util.AdminUserUtils;
import com.graphai.mall.db.constant.account.AccountStatus;
import com.graphai.mall.db.constant.account.FcAccountPrechargeBillEnum;
import com.graphai.mall.db.domain.FcAccountPrechargeBill;
import com.graphai.mall.db.domain.MallAdmin;
import com.graphai.mall.db.domain.MallAftersale;
import com.graphai.mall.db.domain.MallUser;
import com.graphai.mall.db.service.order.MallAftersaleService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.util.OrderUtil;
import com.graphai.mall.db.vo.MallPurchaseOrderVo;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;


/**
 * <p>
 * 采购订单表  前端控制器
 * </p>
 *
 * @author biteam
 * @since 2021-07-22
 */
@RestController
@RequestMapping("/admin/purchaseOrder")
public class MallPurchaseOrderController extends IBaseController<MallPurchaseOrder, String> {

    @Autowired
    private IMallPurchaseOrderService serviceImpl;

    @Autowired
    private IMallPurchaseOrderGoodsService purchaseOrderGoodsService;

    @Autowired
    private MallUserMerchantServiceImpl mallUserMerchantService;

    @Autowired
    private IMallShipCompanyService shipCompanyService;

    @Autowired
    private MallAftersaleService aftersaleService;

    @Autowired
    private DeviceOrderDRecordMapper deviceOrderDRecordMapper;

    @Autowired
    private MallUserService mallUserService;

    @RequiresPermissions("admin:purchaseOrder:single")
    @RequiresPermissionsDesc(menu = {"单独查询"}, button = "单独查询")
    @GetMapping("/single/{id}")
    public Object getEntityById(@PathVariable String id) throws Exception {
        MallPurchaseOrder purchaseOrder = serviceImpl.getOne(new QueryWrapper<MallPurchaseOrder>().lambda().eq(MallPurchaseOrder::getId, id));
        Integer orderStatus = purchaseOrder.getOrderStatus();
        LocalDateTime now = LocalDateTime.now();
        boolean isApply = true;
        if (OrderUtil.STATUS_CONFIRM.equals(orderStatus.shortValue())) {
            if (ObjectUtil.isNotEmpty(purchaseOrder.getConfirmTime()) && !purchaseOrder.getConfirmTime().plusDays(7).isAfter(now)) {
                isApply = false;
            }
        } else if (OrderUtil.STATUS_AFTERSALE_REFUND.equals(orderStatus.shortValue())) {
            if (ObjectUtil.isNotEmpty(purchaseOrder.getConfirmTime()) && ObjectUtil.isNotEmpty(purchaseOrder.getConfirmTime())) {
                if (!purchaseOrder.getConfirmTime().plusDays(7).isAfter(now)) {
                    isApply = false;
                }
            }
        }
        //查询订单相关商品
        List<MallPurchaseOrderGoods> purchaseOrderGoods = purchaseOrderGoodsService.list(new QueryWrapper<MallPurchaseOrderGoods>()
                .eq("order_id", id).select("id,pic_url,goods_id,goods_name,specifications,price,number,(price * number)actualPrice"));
        purchaseOrderGoods.forEach(item -> {
            MallAftersale aftersale = aftersaleService.findByOrderGoodsId(item.getId());
            if (ObjectUtil.isNotNull(aftersale)) {
                if ("0".equals(aftersale.getStatus())) {
                    //售后中
                    item.setAfterSaleStatus(1);
                } else {
                    //售后完成
                    item.setAfterSaleStatus(2);
                }
                item.setAfterSaleInfo(aftersale);
            } else {
                //未售后
                item.setAfterSaleStatus(0);
            }
        });
        HashMap<String, Object> map = new HashMap<>();
        map.put("list", purchaseOrderGoods);
        map.put("isMerchant", AdminUserUtils.isMerchantRole());
        map.put("isApply", isApply);

        //查询分润
        List<FcAccountPrechargeBill> fcList= deviceOrderDRecordMapper.selectFcAccountPrechargeBillsByOrderId(id,FcAccountPrechargeBillEnum.ORDER_TYPE_2.getCode());
        List<Object> listLs = new ArrayList<>();
        Map<String,Object> map1 = new HashMap<>();
        fcList.forEach(item ->{
            MallUser user = mallUserService.findById(item.getCustomerId());
            if (ObjectUtil.isNotEmpty(user)) {
                if (FcAccountPrechargeBillEnum.RECHARGE_CLASSIFCATION_5.getCode().equals(item.getRechargeClassification().toString())) {
                    // 高级门店
                    map1.put("pUser", user.getNickname());
                    map1.put("pMobile", user.getMobile());
                    map1.put("pStatus", item.getCheckStatus());
                    map1.put("pAmt", item.getAmt());
                    map1.put("pNote", item.getNote());
                } else if (FcAccountPrechargeBillEnum.RECHARGE_CLASSIFCATION_6.getCode().equals(item.getRechargeClassification().toString())) {
                    // 工厂
                    map1.put("mUser", user.getNickname());
                    map1.put("mMobile", user.getMobile());
                    map1.put("mAmt", item.getAmt());
                    map1.put("mStatus", item.getCheckStatus());
                    map1.put("mNote", item.getNote());
                }
            }
        });
        if(ObjectUtil.isEmpty(map1)){
            map.put("agentLs", listLs);
        }else{
            listLs.add(map1);
        }

        map.put("agentLs", listLs);
        return ResponseUtil.ok(map);
    }


    @RequiresPermissions("admin:purchaseOrder:melist")
    @RequiresPermissionsDesc(menu = {"采购订单表 查询"}, button = "查询")
    @GetMapping("/melist")
    public Object getMeList(Integer orderStatus, String userName, String orderSn, String beginTime, String endTime,
                            @RequestParam(defaultValue = "add_time") String sort, @RequestParam(defaultValue = "desc") String order,
                            @RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "17") Integer limit) throws Exception {
        Page<MallPurchaseOrderVo> orderList = null;
        MallAdmin loginUser = AdminUserUtils.getLoginUser();
        String roleIds = Arrays.toString(loginUser.getRoleIds());
        boolean isFactory = true;

        HashMap<String, Object> map = new HashMap<>();
        if (roleIds.contains(AccountStatus.LOGIN_ROLE_ADMIN)) {
            orderList = serviceImpl.getOrderList(orderStatus, userName, orderSn, sort, order, null, null, beginTime, endTime, page, limit);
            //统计营业额
            Map<String, Object> total = serviceImpl.getMap(new QueryWrapper<MallPurchaseOrder>()
                    .eq("order_status", OrderUtil.STATUS_CONFIRM)
                    .or(item -> {
                        item.eq("order_status", OrderUtil.STATUS_AFTERSALE_REFUND);
                        item.isNotNull("ship_time");
                        item.isNotNull("confirm_time");
                    }).select("count(1) total,sum(remaining_refund_amount) turnover")
            );
            map.put("total", total.get("total"));
            map.put("turnover", total.get("turnover"));
        } else {
            //区别买家（商户）还是卖家 （工厂）登录
            if (roleIds.contains(AccountStatus.LOGIN_ROLE_FACTORY)) {
                MallUserMerchant userMerchant = mallUserMerchantService.getOne(
                        new QueryWrapper<MallUserMerchant>().lambda().eq(MallUserMerchant::getUserId, loginUser.getUserId()));
                orderList = serviceImpl.getOrderList(orderStatus, userName, orderSn, sort, order, null, userMerchant.getMerchantId(), beginTime, endTime, page, limit);
                //统计营业额
                MallPurchaseOrderVo total = serviceImpl.getOrderListTotal(orderStatus, userName, orderSn, null, userMerchant.getMerchantId(), beginTime, endTime);
                map.put("total", total.getTotal());
                map.put("turnover", total.getTurnover());
            } else if (roleIds.contains(AccountStatus.LOGIN_ROLE_MERCHANT)) {
                orderList = serviceImpl.getOrderList(orderStatus, userName, orderSn, sort, order, loginUser.getUserId(), null, beginTime, endTime, page, limit);
                isFactory = false;
            } else {
                return ResponseUtil.fail("用户权限不足!");
            }
        }
        orderList.getRecords().forEach(orderVo -> {
            //售后状态中 发货
            if (ObjectUtil.isNull(orderVo.getShipTime()) && OrderUtil.STATUS_AFTERSALE_REFUND.equals(orderVo.getOrderStatus().shortValue())) {
                check(orderVo, true);
            }
            //售后状态中 收货
            if (OrderUtil.STATUS_AFTERSALE_REFUND.equals(orderVo.getOrderStatus().shortValue()) && ObjectUtil.isNotEmpty(orderVo.getShipTime()) &&
                    ObjectUtil.isEmpty(orderVo.getConfirmTime())) {
                check(orderVo, false);
            }
        });
        map.put("isFactory", isFactory);
        map.put("list", orderList);
        //查询所有物流公司
        List<MallShipCompanyV2> list = shipCompanyService.list(new QueryWrapper<MallShipCompanyV2>().lambda().eq(MallShipCompanyV2::getDeleteFlag, false));
        map.put("shipCompanyList", list);
        return ResponseUtil.ok(map);
    }


    @RequiresPermissions("admin:purchaseOrder:modify")
    @RequiresPermissionsDesc(menu = {"采购订单表 修改"}, button = "修改")
    @PutMapping("/modify")
    public Object modify(@RequestBody MallPurchaseOrderVo mallPurchaseOrder) {
        String flag = mallPurchaseOrder.getFlag();
        if (StrUtil.isNotBlank(flag)) {
            if ("shipments".equals(flag)) {
                //发货的两种状态 （售后 正常的流程）
                if (OrderUtil.STATUS_PAY.equals(mallPurchaseOrder.getOrderStatus().shortValue())) {
                    mallPurchaseOrder.setOrderStatus(OrderUtil.STATUS_SHIP.intValue());
                }
                mallPurchaseOrder.setShipTime(LocalDateTime.now());
            } else if ("receipt".equals(flag)) {
                //收货
                if (OrderUtil.STATUS_SHIP.equals(mallPurchaseOrder.getOrderStatus().shortValue())) {
                    mallPurchaseOrder.setOrderStatus(OrderUtil.STATUS_CONFIRM.intValue());
                }
                mallPurchaseOrder.setConfirmTime(LocalDateTime.now());
            }if ("updatePrice".equals(flag)){
                // 重新计算支付金额
                MallPurchaseOrder purchaseOrder = serviceImpl.getOne(new QueryWrapper<MallPurchaseOrder>().lambda().eq(MallPurchaseOrder::getId, mallPurchaseOrder.getId()));
                purchaseOrder.setModifyPrice(true);
                purchaseOrder.setOrderPrice(mallPurchaseOrder.getOrderPrice());
                BigDecimal actualPrice = mallPurchaseOrder.getOrderPrice().subtract(purchaseOrder.getCouponPrice()).subtract(purchaseOrder.getIntegralPrice());
                purchaseOrder.setActualPrice(actualPrice);
                purchaseOrder.setRemainingRefundAmount(actualPrice);
                mallPurchaseOrder.setUpdateTime(LocalDateTime.now());
                serviceImpl.updateById(purchaseOrder);
                return ResponseUtil.ok();
            }
        }
        mallPurchaseOrder.setUpdateTime(LocalDateTime.now());
        serviceImpl.updateById(mallPurchaseOrder);
        return ResponseUtil.ok();
    }

    @RequiresPermissions("admin:purchaseOrder:del")
    @RequiresPermissionsDesc(menu = {"采购订单表 删除"}, button = "删除")
    @DeleteMapping("/del/{id}")
    public Object deleteById(@PathVariable String id) {
        serviceImpl.removeById(id);
        return ResponseUtil.ok();
    }

    private void check(MallPurchaseOrderVo orderVo, boolean isShipAndReceipt) {
        List<MallPurchaseOrderGoods> goodsList = purchaseOrderGoodsService.list(new QueryWrapper<MallPurchaseOrderGoods>()
                .lambda().eq(MallPurchaseOrderGoods::getOrderId, orderVo.getId()));
        List<MallAftersale> aftersales = aftersaleService.findByOrderId(orderVo.getId());
        if (aftersales.size() < goodsList.size()) {
            //部分商品售后
            if (isShipAndReceipt) {
                orderVo.setNeedToShip(true);
            } else {
                orderVo.setNeedToTake(true);
            }
        } else {
            //售后申请是否全部审核过
            boolean flag = true;
            boolean tag = false;
            for (MallAftersale a : aftersales) {
                for (MallPurchaseOrderGoods g : goodsList) {
                    if (a.getGoodsId().equals(g.getGoodsId())) {
                        //审核过
                        if (!"0".equals(a.getStatus())) {
                            flag = false;
                            if("3".equals(a.getType())){
                                if (isShipAndReceipt){
                                    orderVo.setNeedToShip(true);
                                } else {
                                    orderVo.setNeedToTake(true);
                                }
                                tag = true;
                                break;
                            }else{
                                //是否同意退款(换货)
                                if (a.getIsRefund()) {
                                    //售后数量是否跟购物数量一致
                                    if (!a.getNumber().equals(g.getNumber())) {
                                        if (isShipAndReceipt)
                                            orderVo.setNeedToShip(true);
                                        else orderVo.setNeedToTake(true);
                                        tag = true;
                                        break;
                                    }
                                } else {
                                    //只要一件商品不同意退款就可以发货
                                    if (isShipAndReceipt)
                                        orderVo.setNeedToShip(true);
                                    else
                                        orderVo.setNeedToTake(true);
                                    tag = true;
                                    break;
                                }
                            }

                        }
                    }
                }
                if (tag) {
                    break;
                }
            }
            //都未审核过
            if (flag) {
                if (isShipAndReceipt) {
                    orderVo.setNeedToShip(true);
                } else {
                    orderVo.setNeedToTake(true);
                }
            }

        }
    }
}

