package com.graphai.mall.admin.web;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.mall.db.domain.MallPositionCode;
import com.graphai.mall.db.service.MallPositionCodeService;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 平台管理
 */
@RestController
@RequestMapping("/admin/positionCode")
@Validated
public class AdminMallPositionCodeController {

    @Autowired
    private MallPositionCodeService mallPositionCodeService;

    //    @RequiresPermissions("admin:positionCode:add")
//    @RequiresPermissionsDesc(menu = {"商品管理", "平台管理"}, button = "查询")
    @GetMapping("/list")
    public Object list(@RequestBody MallPositionCode mallPositionCode) {
        //查询商品positionCode列表
//        List<String> typeList = new ArrayList<>();
//        typeList.add("1");  //自营
//        typeList.add("2");  //第三方
//        typeList.add("3");  //权益
//        typeList.add("4");  //卡券商品
        List<MallPositionCode> mallPositionCodeList = mallPositionCodeService.selectMallPositionCodeList(mallPositionCode, "type", "asc");
        Map<String, Object> data = new HashMap<>();
        data.put("mallPositionCodeList", mallPositionCodeList); //平台
        return ResponseUtil.ok(data);
    }


    @RequiresPermissions("admin:positionCode:add")
    @RequiresPermissionsDesc(menu = {"商品管理", "平台管理"}, button = "添加")
    @PostMapping("/add")
    public Object add(@RequestBody MallPositionCode mallPositionCode) {
        Object error = validate(mallPositionCode);
        if (error != null) {
            return error;
        }
        List<MallPositionCode> mallPositionCodeList = mallPositionCodeService.selectMallPositionCodeList(mallPositionCode, null, null);
        if (mallPositionCodeList.size() > 0) {
            return ResponseUtil.badArgumentValue("positionCode已经存在！");
        }
        mallPositionCodeService.insertMallPositionCode(mallPositionCode);
        return ResponseUtil.ok();
    }

    @RequiresPermissions("admin:positionCode:update")
    @RequiresPermissionsDesc(menu = {"商品管理", "平台管理"}, button = "修改")
    @PostMapping("/update")
    public Object update(@RequestBody MallPositionCode mallPositionCode) {
//        Object error = validate(mallPositionCode);
//        if (error != null) {
//            return error;
//        }
        mallPositionCodeService.updateMallPositionCode(mallPositionCode);
        return ResponseUtil.ok();
    }

    @RequiresPermissions("admin:positionCode:delete")
    @RequiresPermissionsDesc(menu = {"商品管理", "平台管理"}, button = "删除")
    @PostMapping("/delete")
    public Object delete(@RequestParam String id) {
        MallPositionCode mallPositionCode = new MallPositionCode();
        mallPositionCode.setId(id);
        mallPositionCode.setDeleted(true);
        mallPositionCodeService.updateMallPositionCode(mallPositionCode);
        return ResponseUtil.ok();
    }

    private Object validate(MallPositionCode mallPositionCode) {
        if (StringUtils.isEmpty(mallPositionCode.getType())) {
            return ResponseUtil.badArgument();
        }
        if (StringUtils.isEmpty(mallPositionCode.getPositionName())) {
            return ResponseUtil.badArgument();
        }
        if (StringUtils.isEmpty(mallPositionCode.getPositionCode())) {
            return ResponseUtil.badArgument();
        }
        return null;
    }
}
