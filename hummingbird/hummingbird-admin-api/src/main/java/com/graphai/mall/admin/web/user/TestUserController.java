package com.graphai.mall.admin.web.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.mall.db.domain.MallUser;
import com.graphai.mall.db.service.user.MallUserService;

@RestController
@RequestMapping("/admin/new")
public class TestUserController {


    @Autowired
    private MallUserService serviceImpl;

    @PostMapping("/addUser")
    public Object addUser() throws Exception {

        MallUser mallUser = new MallUser();
        for (int i = 0; i < 100; i++) {
            mallUser.setCity("13123");
            mallUser.setUsername("1383838438" + i);
            mallUser.setPassword("123131");
            serviceImpl.add(mallUser);
        }

        return ResponseUtil.ok("成功");
    }

}
