package com.graphai.mall.admin.web.order;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.bcrypt.BCryptPasswordEncoder;
import com.graphai.commons.util.json.JacksonUtil;
import com.graphai.commons.util.json.JacksonUtils;
import com.graphai.commons.util.lang.StringUtils;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.mall.admin.dao.DeviceOrderDRecordMapper;
import com.graphai.mall.admin.domain.MallMerchant;
import com.graphai.mall.admin.domain.MallShipCompanyV2;
import com.graphai.mall.admin.domain.MallUserMerchant;
import com.graphai.mall.admin.service.AdminOrderService;
import com.graphai.mall.admin.service.IMallMerchantService;
import com.graphai.mall.admin.service.IMallShipCompanyService;
import com.graphai.mall.admin.service.IMallUserMerchantService;
import com.graphai.mall.admin.util.AdminUserUtils;
import com.graphai.mall.admin.util.ExcelUtil;
import com.graphai.mall.admin.util.LocalDateTimeUtil;
import com.graphai.mall.admin.vo.OrderVo;
import com.graphai.mall.db.constant.CommonConstant;
import com.graphai.mall.db.constant.account.AccountStatus;
import com.graphai.mall.db.constant.account.FcAccountPrechargeBillEnum;
import com.graphai.mall.db.dao.MallOrderMapper;
import com.graphai.mall.db.domain.*;
import com.graphai.mall.db.service.MallAdminService;
import com.graphai.mall.db.service.account.FcAccountRechargeBillService;
import com.graphai.mall.db.service.common.ShipService;
import com.graphai.mall.db.service.goods.MallGoodsService;
import com.graphai.mall.db.service.order.MallAftersaleService;
import com.graphai.mall.db.service.order.MallOrderGoodsService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.service.wangzhuan.WangzhuanTaskService;
import com.graphai.mall.db.util.OrderUtil;
import com.graphai.mall.db.util.account.AccountUtils;
import com.graphai.mall.db.vo.MallZYOrderVo;
import com.graphai.mall.db.vo.ZYOrderGoodsVo;
import com.graphai.open.mallorder.entity.IMallOrder;
import com.graphai.open.mallorder.service.IMallOrderZYService;
import com.graphai.open.mallordergoods.entity.IMallOrderGoods;
import com.graphai.open.mallordergoods.service.IMallOrderGoodsService;
import com.graphai.validator.Order;
import com.graphai.validator.Sort;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static com.graphai.framework.constant.AdminResponseCode.ADMIN_INVALID_ACCOUNT;

@RestController
@RequestMapping("/admin/order")
@Validated
public class AdminOrderController {
    private final Log logger = LogFactory.getLog(AdminOrderController.class);

    @Autowired
    private AdminOrderService adminOrderService;

    @Autowired
    private ShipService shipService;

    @Autowired
    private MallUserService mallUserService;

    @Resource
    private MallAdminService mallAdminService;

    @Resource
    private FcAccountRechargeBillService fcAccountRechargeBillService;

    @Autowired
    private WangzhuanTaskService wangzhuanTaskService;

    @Resource
    private MallOrderMapper mallOrderMapper;

    @Autowired
    private IMallOrderZYService orderZYService;

    @Autowired
    private IMallOrderGoodsService orderGoodsService;

    @Autowired
    private IMallUserMerchantService userMerchantService;

    @Autowired
    private IMallShipCompanyService shipCompanyService;

    @Autowired
    private MallAftersaleService afterSaleService;

    @Autowired
    private MallGoodsService mallGoodsService;

    @Autowired
    private IMallMerchantService mallMerchantService;

    @Autowired
    private DeviceOrderDRecordMapper deviceOrderDRecordMapper;
/*    @RequiresPermissions("admin:order:listZy")
    @RequiresPermissionsDesc(menu = {"订单管理", "自营订单"}, button = "查询")
    @GetMapping("/listZy")
    public Object list1(String userName, String userId, String orderSn, String goodsName, String tpId, String tpUserName, String beginTime, String endTime, String type,
                    boolean getShipCompanyList, @RequestParam(required = false) List<String> platArray, @RequestParam(required = false) List<Short> orderStatusArray,
                    @RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit, @Sort @RequestParam(defaultValue = "id") String sort,
                    @Order @RequestParam(defaultValue = "desc") String order,@RequestParam(required = false)String isGroup, @RequestParam(required = false)String grouponId,
                        String source,String orderStatus,String salesStatus) {
        return this.orderList(userName, userId, orderSn, goodsName, tpId, tpUserName, beginTime, endTime, type, isGroup,getShipCompanyList, platArray, orderStatusArray, page, limit, sort,
                        order, grouponId, source, orderStatus,salesStatus);
    }*/

    @RequiresPermissions("admin:order:listYx")
    @RequiresPermissionsDesc(menu = {"订单管理", "游戏订单"}, button = "查询")
    @GetMapping("/listYx")
    public Object listYx(String userName, String userId, String orderSn, String goodsName, String tpId, String tpUserName, String beginTime, String endTime, String type,
                         boolean getShipCompanyList, @RequestParam(required = false) List<String> platArray, @RequestParam(required = false) List<Short> orderStatusArray,
                         @RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit, @Sort @RequestParam(defaultValue = "id") String sort,
                         @Order @RequestParam(defaultValue = "desc") String order, @RequestParam(required = false) String isGroup, String source, String orderStatus) {
        return this.orderList(userName, userId, orderSn, goodsName, tpId, tpUserName, beginTime, endTime, type, isGroup, getShipCompanyList, platArray, orderStatusArray, page, limit, sort,
                order, "", source, orderStatus, "");
    }

    @RequiresPermissions("admin:order:listMs")
    @RequiresPermissionsDesc(menu = {"订单管理", "美食订单"}, button = "查询")
    @GetMapping("/listMs")
    public Object listMs(String userName, String userId, String orderSn, String goodsName, String tpId, String tpUserName, String beginTime, String endTime, String type,
                         boolean getShipCompanyList, @RequestParam(required = false) List<String> platArray, @RequestParam(required = false) List<Short> orderStatusArray,
                         @RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit, @Sort @RequestParam(defaultValue = "id") String sort,
                         @Order @RequestParam(defaultValue = "desc") String order, @RequestParam(required = false) String isGroup, String source, String orderStatus) {
        return this.orderList(userName, userId, orderSn, goodsName, tpId, tpUserName, beginTime, endTime, type, isGroup, getShipCompanyList, platArray, orderStatusArray, page, limit, sort,
                order, "", source, orderStatus, "");
    }

    @RequiresPermissions("admin:order:listCx")
    @RequiresPermissionsDesc(menu = {"订单管理", "出行订单"}, button = "查询")
    @GetMapping("/listCx")
    public Object listCx(String userName, String userId, String orderSn, String goodsName, String tpId, String tpUserName, String beginTime, String endTime, String type,
                         boolean getShipCompanyList, @RequestParam(required = false) List<String> platArray, @RequestParam(required = false) List<Short> orderStatusArray,
                         @RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit, @Sort @RequestParam(defaultValue = "id") String sort,
                         @Order @RequestParam(defaultValue = "desc") String order, @RequestParam(required = false) String isGroup, String source, String orderStatus) {
        return this.orderList(userName, userId, orderSn, goodsName, tpId, tpUserName, beginTime, endTime, type, isGroup, getShipCompanyList, platArray, orderStatusArray, page, limit, sort,
                order, "", source, orderStatus, "");
    }

    @RequiresPermissions("admin:order:listQy")
    @RequiresPermissionsDesc(menu = {"订单管理", "权益订单"}, button = "查询")
    @GetMapping("/listQy")
    public Object listQy(String userName, String userId, String orderSn, String goodsName, String tpId, String tpUserName, String beginTime, String endTime, String type,
                         boolean getShipCompanyList, @RequestParam(required = false) List<String> platArray, @RequestParam(required = false) List<Short> orderStatusArray,
                         @RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit, @Sort @RequestParam(defaultValue = "id") String sort,
                         @Order @RequestParam(defaultValue = "desc") String order, @RequestParam(required = false) String isGroup, String source, String orderStatus) {
        return this.orderList(userName, userId, orderSn, goodsName, tpId, tpUserName, beginTime, endTime, type, isGroup, getShipCompanyList, platArray, orderStatusArray, page, limit, sort,
                order, "", source, orderStatus, "");
    }

    /**
     * 查询订单(购物、权益)
     *
     * @return
     */
    @RequiresPermissions("admin:order:listShop")
    @RequiresPermissionsDesc(menu = {"订单管理", "订单管理"}, button = "查询")
    @GetMapping("/list")
    public Object list(String userName, String userId, String orderSn, String goodsName, String tpId, String tpUserName, String beginTime, String endTime, String type,
                       boolean getShipCompanyList, @RequestParam(required = false) List<String> platArray, @RequestParam(required = false) List<Short> orderStatusArray,
                       @RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit, @Sort @RequestParam(defaultValue = "id") String sort,
                       @Order @RequestParam(defaultValue = "desc") String order, @RequestParam(required = false) String isGroup, String source, String orderStatus) {

        return this.orderList(userName, userId, orderSn, goodsName, tpId, tpUserName, beginTime, endTime, type, isGroup,
                getShipCompanyList, platArray, orderStatusArray,
                page, limit, sort,
                order, "", source, orderStatus, "");
    }


    /**
     * 游戏订单(闲玩，闪电玩)
     */
    // @RequiresPermissions("admin:order:gameOrder")
    // @RequiresPermissionsDesc(menu = {"订单管理", "游戏订单"}, button = "查询")
    // @GetMapping("/gameOrder")
    // public Object gameOrder(String userName,String userId, String orderSn,
    // String beginTime, String endTime,String type,
    // @RequestParam(required = false) List<String> platArray,
    // @RequestParam(required = false) List<Short> orderStatusArray,
    // @RequestParam(defaultValue = "1") Integer page,
    // @RequestParam(defaultValue = "10") Integer limit,
    // @Sort @RequestParam(defaultValue = "id") String sort,
    // @Order @RequestParam(defaultValue = "desc") String order){
    // String userid ="";
    // if(!StringUtils.isEmpty(userName)){
    // List<MallUser> list = mallUserService.qrueyUser(userName);
    // if(null != list ){
    // userid = list.get(0).getId();
    // }
    // }
    //
    // Map map = new HashMap();
    // map.put("items",adminOrderService.gameOrder(type,userid,page,limit));
    // map.put("total",adminOrderService.gameOrder(type,userid,page,limit).size());
    // return ResponseUtil.ok(map);
    //
    // }

    /**
     * 游戏订单(闲玩，闪电玩)
     */
    @RequiresPermissions("admin:order:detailGameOrder")
    @RequiresPermissionsDesc(menu = {"订单管理", "游戏订单"}, button = "详情")
    @GetMapping("/detailGameOrder")
    public Object detailGameOrder(@NotNull String id) {
        return ResponseUtil.ok(adminOrderService.detailGameOrder(id));
    }


    /**
     * 订单详情
     */
    @RequiresPermissions("admin:order:detail")
    @RequiresPermissionsDesc(menu = {"订单管理", "订单管理"}, button = "订单详情")
    @GetMapping("/detail")
    public Object detail(@NotNull String id, @RequestParam(defaultValue = "1") String type) {
        return adminOrderService.detail(id, type);
    }


    /**
     * 订单找回
     */
    @RequiresPermissions("admin:order:retrieve")
    @RequiresPermissionsDesc(menu = {"订单管理", "订单找回"}, button = "查询")
    @GetMapping("/retrieve")
    public Object retrieve(String orderSn, String userName, String beginTime, String endTime, String type, String value1, @RequestParam(required = false) List<String> platArray,
                           @RequestParam(required = false) List<String> orderStatusArray, @RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit,
                           @Sort @RequestParam(defaultValue = "id") String sort, @Order @RequestParam(defaultValue = "desc") String order)
            throws InvocationTargetException, IllegalAccessException {
        DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime begin1 = null;
        LocalDateTime end1 = null;
        String userid = "";
        if (!StringUtils.isEmpty(userName)) {
            List<MallUser> list = mallUserService.qrueyUser(userName);
            if (null != list) {
                userid = list.get(0).getId();
            }
        }
        try {
            if (!com.aliyuncs.utils.StringUtils.isEmpty(beginTime)) {
                String[] str = beginTime.split(" ");
                begin1 = LocalDateTime.parse(str[0] + " 00:00:00", formatter2);
            }
            if (!com.aliyuncs.utils.StringUtils.isEmpty(endTime)) {
                String[] str = endTime.split(" ");
                end1 = LocalDateTime.parse(str[0] + " 23:59:59", formatter2);
                // end = LocalDateTimeUtil.getDateStrToLocalDateTime(endTime);
            }
        } catch (Exception e) {
            // begin = null;
            // end = null;
            logger.error("", e);
        }


        return adminOrderService.retrieve(userid, orderSn, begin1, end1, type, value1, platArray, orderStatusArray, page, limit, sort, order);
    }

    /**
     * 订单处理
     */
    @RequiresPermissions("admin:order:handle")
    @RequiresPermissionsDesc(menu = {"订单管理", "订单找回"}, button = "订单处理")
    @PostMapping("/handle")
    public Object handle(@RequestBody String body) {
        return adminOrderService.handle(body);
    }


    /**
     * 购卡订单
     */
    @RequiresPermissions("admin:order:buyCardList")
    @RequiresPermissionsDesc(menu = {"订单管理", "购卡订单"}, button = "查询")
    @GetMapping("/buyCardList")
    public Object buyCardList(@RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "20") Integer limit, String orderSn, String mobile, String status,
                              String beginTime, String endTime, boolean getShipCompanyList) {
        return ResponseUtil.ok(adminOrderService.queryBuyCardList(orderSn, mobile, status, page, limit, beginTime, endTime, getShipCompanyList));
    }

    /**
     * 购卡订单发货
     *
     * @param body 订单信息，{ orderId：xxx, shipSn: xxx, shipChannel: xxx }
     */
    @RequiresPermissions("admin:order:ship")
    @RequiresPermissionsDesc(menu = {"订单管理", "购卡订单"}, button = "购卡订单发货")
    @PostMapping("/ship")
    public Object ship(@RequestBody String body) throws IOException {
        return adminOrderService.ship(body);
    }


    /**
     * 订单退款
     *
     * @param body 订单信息，{ orderId：xxx }
     */
    // @RequiresPermissions("admin:order:refund")
    // @RequiresPermissionsDesc(menu = {"订单管理", "订单管理"}, button = "订单退款")
    @PostMapping("/refund")
    public Object refund(@RequestBody String body) {
        return adminOrderService.refund(body);
    }

    /**
     * 回复订单商品
     *
     * @param body 订单信息，{ orderId：xxx }
     */
    // @RequiresPermissions("admin:order:reply")
    // @RequiresPermissionsDesc(menu = {"订单管理", "订单管理"}, button = "订单商品回复")
    @PostMapping("/reply")
    public Object reply(@RequestBody String body) {
        return adminOrderService.reply(body);
    }


    /**
     * 查询快递物流公司信息
     */
    @GetMapping("/shipCompanys")
    public Object getShipCompanys() {
        return shipService.getShipCompanys();
    }

    /**
     * 补录订单分佣
     *
     * @return
     */
    @GetMapping("/suppOrder")
    public Object suppOrder(@RequestParam String data) throws IOException {
        String userId = JacksonUtil.parseString(data, "userId");
        String orderSn = JacksonUtil.parseString(data, "orderSn");
        String operationPWd = JacksonUtil.parseString(data, "operationPWd");
        Object error = validate(operationPWd);
        if (error != null) {
            return error;
        }
        List<MallOrder> list = adminOrderService.getOrderBySn(userId, orderSn);

        if (list.size() > 0) {
            Map<String, String> map = new HashMap<>();
            map.put("userId", userId);
            map.put("bid", list.get(0).getBid());
            map.put("totalMoney", list.get(0).getPubSharePreFee());
            map.put("orderId", list.get(0).getId());
            map.put("rechargeType", CommonConstant.TASK_REWARD_CASH);
            map.put("orderSn", orderSn);
            Object result = null;
            if ("zero".equals(list.get(0).getActivityType())) {// 零元购商品
                list.get(0).setSupplementTime(LocalDateTime.now());
                mallOrderMapper.updateByPrimaryKeySelective(list.get(0));
                result = AccountUtils.newPreCharge(map);
                AccountUtils.applyPrecharge(list.get(0).getId(), "1");
            } else {
                List<FcAccountPrechargeBill> list1 = fcAccountRechargeBillService.selectPrechargeBillByOrderSn(list.get(0).getOrderSn(), list.get(0).getUserId());
                if (list1.size() > 0) {
                    for (FcAccountPrechargeBill bill : list1) {
                        if (userId.equals(bill.getCustomerId()) && !bill.getCheckStatus().equals("2")) {
                            list.get(0).setSupplementTime(LocalDateTime.now());
                            mallOrderMapper.updateByPrimaryKeySelective(list.get(0));
                            result = AccountUtils.preCharge(map);
                            AccountUtils.applyPrecharge(list.get(0).getId(), "1");
                        } else {
                            return ResponseUtil.fail(403, "补录失败,该返佣已经审核！");
                        }
                    }
                } else {
                    list.get(0).setSupplementTime(LocalDateTime.now());
                    mallOrderMapper.updateByPrimaryKeySelective(list.get(0));
                    result = AccountUtils.preCharge(map);
                    AccountUtils.applyPrecharge(list.get(0).getId(), "1");
                }
            }
            if (ObjectUtil.isNotNull(result)) {
                Map<String, Object> resultMap = JacksonUtils.jsn2map(JacksonUtils.bean2Jsn(result), String.class, Object.class);
                if (!Objects.equals(resultMap, null)) {
                    return ResponseUtil.ok();

                }
            }
        }
        return ResponseUtil.fail(403, "补录失败！");
    }

    // 审核和打款要验证操作密码
    private Object validate(String operationPWd) {
        // 获取当前用户
        MallAdmin adminUser = (MallAdmin) SecurityUtils.getSubject().getPrincipal();
        // 防止用户刚设置操作密码，没有重新登录，获取不到新的操作密码，所以再去数据库查询一次，确保能获取
        MallAdmin user = mallAdminService.findOperationPwdById(adminUser.getId());

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        if (!encoder.matches(operationPWd, user.getOperationPassword())) {
            return ResponseUtil.fail(ADMIN_INVALID_ACCOUNT, "操作密码有误");
        }
        return null;
    }

    /**
     * 查询订单(达人)
     *
     * @return
     */
    @RequiresPermissions("admin:order:listDr")
    @RequiresPermissionsDesc(menu = {"订单管理", "达人订单"}, button = "查询")
    @GetMapping("/listDr")
    public Object listDr(String userName, String userId, String orderSn, String goodsName, String tpId, String tpUserName, String beginTime, String endTime, String type,
                         boolean getShipCompanyList, @RequestParam(required = false) List<String> platArray, @RequestParam(required = false) List<Short> orderStatusArray,
                         @RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit, @Sort @RequestParam(defaultValue = "id") String sort,
                         @Order @RequestParam(defaultValue = "desc") String order, @RequestParam(required = false) String isGroup, String source, String orderStatus) {
        return this.orderList(userName, userId, orderSn, goodsName, tpId, tpUserName, beginTime, endTime, type, isGroup, getShipCompanyList, platArray, orderStatusArray, page, limit, sort,
                order, "", source, orderStatus, "");
    }

    /**
     * 查询订单(创客)
     *
     * @return
     */
    @RequiresPermissions("admin:order:listCk")
    @RequiresPermissionsDesc(menu = {"订单管理", "创客订单"}, button = "查询")
    @GetMapping("/listCk")
    public Object listCk(String userName, String userId, String orderSn, String goodsName, String tpId, String tpUserName, String beginTime, String endTime, String type,
                         boolean getShipCompanyList, @RequestParam(required = false) List<String> platArray, @RequestParam(required = false) List<Short> orderStatusArray,
                         @RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit, @Sort @RequestParam(defaultValue = "id") String sort,
                         @Order @RequestParam(defaultValue = "desc") String order, @RequestParam(required = false) String isGroup, String source, String orderStatus) {
        return this.orderList(userName, userId, orderSn, goodsName, tpId, tpUserName, beginTime, endTime, type, isGroup, getShipCompanyList, platArray, orderStatusArray, page, limit, sort,
                order, "", source, orderStatus, "");
    }

    @RequiresPermissions("admin:order:ckDetail")
    @RequiresPermissionsDesc(menu = {"订单管理", "创客订单"}, button = "订单详情")
    @GetMapping("/ckDetail")
    public Object ckDetail(@NotNull String id) {
        return adminOrderService.ckDetail(id);
    }

    /**
     * 创客订单发货
     *
     * @param body 订单信息，{ orderId：xxx, shipSn: xxx, shipChannel: xxx }
     */
    @RequiresPermissions("admin:order:shipCk")
    @RequiresPermissionsDesc(menu = {"订单管理", "创客订单"}, button = "发货")
    @PostMapping("/shipCk")
    public Object shipCk(@RequestBody String body) throws IOException {
        return adminOrderService.ship(body);
    }

    /**
     * 订单查询方法（购物、权益）
     *
     * @param userName
     * @param userId
     * @param orderSn
     * @param goodsName
     * @param tpId
     * @param tpUserName
     * @param beginTime
     * @param endTime
     * @param type
     * @param getShipCompanyList
     * @param platArray
     * @param orderStatusArray
     * @param page
     * @param limit
     * @param sort
     * @param order
     * @param grouponId          团购团长id【mall_groupon表的id字段】
     * @return
     */
    private Object orderList(String userName, String userId, String orderSn, String goodsName, String tpId, String tpUserName, String beginTime, String endTime, String type, String isGroup,
                             boolean getShipCompanyList, List<String> platArray, List<Short> orderStatusArray,
                             Integer page, Integer limit, String sort,
                             String order, String grouponId, String source, String orderStatus, String salesStatus) {

        LocalDateTime begin = null;
        LocalDateTime end = null;

        DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime begin1 = null;
        LocalDateTime end1 = null;
        try {
            if (!com.aliyuncs.utils.StringUtils.isEmpty(beginTime)) {
                String[] str = beginTime.split(" ");
                begin1 = LocalDateTime.parse(str[0] + " 00:00:00", formatter2);
                begin = LocalDateTimeUtil.getDateStrToLocalDateTime(beginTime);
            }
            if (!com.aliyuncs.utils.StringUtils.isEmpty(endTime)) {
                String[] str = endTime.split(" ");
                end1 = LocalDateTime.parse(str[0] + " 23:59:59", formatter2);
                end = LocalDateTimeUtil.getDateStrToLocalDateTime(endTime);
            }
        } catch (Exception e) {
            begin = null;
            end = null;
            logger.error("", e);

        }
        String userid = "";
        if (!StringUtils.isEmpty(userName)) {
            List<MallUser> list = mallUserService.qrueyUser(userName);
            if (null != list && list.size() > 0) {
                userid = list.get(0).getId();
            } else {
                userid = userName;
            }

        }

        return adminOrderService.list(begin1, end1, userName, userid, orderSn, goodsName, tpId, tpUserName, orderStatusArray, page, limit, sort, order, begin, end, type, isGroup, platArray,
                getShipCompanyList, grouponId, source, orderStatus, salesStatus);
    }

    /**
     * 订单导出
     */
    @RequiresPermissions("admin:order:exportOrder")
    @RequiresPermissionsDesc(menu = {"订单管理", "订单导出"}, button = "导出")
    @PostMapping("/exportOrder")
    public Object exportOrder(String userName, String userId, String orderSn, String goodsName, String tpId, String tpUserName, String beginTime, String endTime, String type,
                              boolean getShipCompanyList, @RequestParam(required = false) List<String> platArray, @RequestParam(required = false) List<Short> orderStatusArray,
                              Integer page, Integer limit, @Sort @RequestParam(defaultValue = "id") String sort, String source,
                              @Order @RequestParam(defaultValue = "desc") String order, @RequestParam(required = false) String isGroupr, String orderStatus) {
        page = null;
        limit = null;
        Map<String, Object> data = (Map<String, Object>) orderList(userName, userId, orderSn, goodsName, tpId, tpUserName, beginTime, endTime, type, isGroupr, getShipCompanyList, platArray, orderStatusArray, page, limit, sort,
                order, "", source, orderStatus, "");
        Map<String, Object> mapData = (Map<String, Object>) data.get("data");
        List<OrderVo> ordervoList = (List<OrderVo>) mapData.get("items");
        ExcelUtil<OrderVo> util = new ExcelUtil<OrderVo>(OrderVo.class);
        return util.exportExcel(ordervoList, "创客订单");
    }

    /**
     * @param taskName 任务名称
     * @param orderId  订单编号
     * @param begin    开始时间
     * @param end      结束时间
     * @param userName 发布者名称
     * @param tStatus  任务状态
     * @param page     当前页数
     * @param limit    每页显示数量
     * @return 1 taskName:任务名称，
     * 2 completedQuantity:任务完成数量，
     * 3 stockNumber:任务剩余数量，
     * 4 tStatus:任务当前状态,
     * 5 sumAmount:任务总支付金额，
     * 6 sumStockNumber:任务总量,
     * 7 userInCome = completed_quantity * amount:用户收益，
     * 8 coverCharge = completed_quantity * amount / (1 - tips) * tips:服务费，
     * 9 drawback= stock_number * amount / tips:任务退款金额，
     * 10 surplusAmount = stock_number * amount / tips:任务剩余金额，
     * 11 updateTime:上架时间，
     * 12 orderId:订单编号，
     * 13 userName:发布者名称
     */
    @RequiresPermissions("admin:order:getAboutTaskCount")
    @RequiresPermissionsDesc(menu = {"订单管理", "玩赚订单"}, button = "查询")
    @GetMapping("/getAboutTaskCount")
    public Object getAboutTaskCount(@RequestParam(required = false) String taskName, @RequestParam(required = false) String orderId,
                                    @RequestParam(required = false) LocalDateTime begin, @RequestParam(required = false) LocalDateTime end,
                                    @RequestParam(required = false) String userName, @RequestParam(required = false) String tStatus,
                                    @RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit) {
        return wangzhuanTaskService.getAboutTaskCount(taskName, orderId, begin, end, userName, tStatus, page, limit);
    }

    /**
     * 为了避免爆炸，这里重新写一个方法，用来计算 各类金额的 统计。
     */
    @GetMapping("/getAmountCount")
    public Object getAmountCount(@RequestParam(required = false) String taskName, @RequestParam(required = false) String orderId,
                                 @RequestParam(required = false) LocalDateTime begin, @RequestParam(required = false) LocalDateTime end,
                                 @RequestParam(required = false) String userName, @RequestParam(required = false) String tStatus) {

        return wangzhuanTaskService.getAmountCount(taskName, orderId, begin, end, userName, tStatus);
    }

    /**
     * 自营订单查询
     */
    @RequiresPermissions("admin:order:listZy")
    @RequiresPermissionsDesc(menu = {"销售订单"}, button = "查询")
    @GetMapping("/listZy")
    public Object list1(String nickName, String orderSn, String beginTime, String endTime,
                        @RequestParam(defaultValue = "1") Integer page,@RequestParam(required = false) String grouponId,
                        @RequestParam(defaultValue = "10") Integer limit, @Sort @RequestParam(defaultValue = "id") String sort,
                        @Order @RequestParam(defaultValue = "desc") String order, Integer orderStatus,Integer bid) {
        MallAdmin loginUser = AdminUserUtils.getLoginUser();
        String roles = Arrays.toString(loginUser.getRoleIds());
        Page<MallZYOrderVo> zyOrderList = null;
        HashMap<String, Object> map = new HashMap<>();
        if (roles.contains(AccountStatus.LOGIN_ROLE_ADMIN)) {
            zyOrderList = orderZYService.getZYOrderList(page, limit, orderStatus, nickName, orderSn, null, null, beginTime, endTime, sort, order, bid,grouponId);
            //统计营业额
            Map<String, Object> total = orderZYService.getMap(new QueryWrapper<IMallOrder>()
                    .in("bid","53,56,36,55")
                    .eq("order_status", OrderUtil.STATUS_CONFIRM)
                    .or(item -> {
                        item.eq("order_status", OrderUtil.STATUS_AFTERSALE_REFUND);
                        item.isNotNull("ship_time");
                        item.isNotNull("confirm_time");
                    }).select("count(1) total,sum(remaining_refund_amount) turnover")
            );
            map.put("total", total.get("total"));
            map.put("turnover",total.get("turnover"));
        } else {
            MallUserMerchant userMerchant = userMerchantService.getOne(new QueryWrapper<MallUserMerchant>().lambda()
                    .eq(MallUserMerchant::getUserId, loginUser.getUserId()));
            zyOrderList = orderZYService.getZYOrderList(page, limit, orderStatus, nickName, orderSn, null, userMerchant.getMerchantId(), beginTime, endTime, sort, order,bid,grouponId);
            //统计营业额
            MallZYOrderVo total = orderZYService.getZYOrderListTotal(orderStatus, nickName, orderSn, null, userMerchant.getMerchantId(), beginTime, endTime, bid,grouponId);
            map.put("total", total.getTotal());
            map.put("turnover",total.getTurnover());
        }

        zyOrderList.getRecords().forEach(orderVo -> {
            //订单处于售后状态 发货时间为空 但是订单商品只有一种 买了3个 申请售后2个  还是能够发货的
            if (ObjectUtil.isNull(orderVo.getShipTime()) && OrderUtil.STATUS_AFTERSALE_REFUND.equals(orderVo.getOrderStatus().shortValue())) {
                List<IMallOrderGoods> goodsList = orderGoodsService.list(new QueryWrapper<IMallOrderGoods>()
                        .lambda().eq(IMallOrderGoods::getOrderId, orderVo.getId()));
                List<MallAftersale> aftersales = afterSaleService.findByOrderId(orderVo.getId());
                if (aftersales.size() < goodsList.size()) {
                    //部分商品售后
                    orderVo.setSendShip(true);
                } else {
                    //售后申请是否全部审核过
                    boolean flag = true;
                    boolean tag = false;
                    for (MallAftersale a : aftersales) {
                        for (IMallOrderGoods g : goodsList) {
                            if (a.getGoodsId().equals(g.getGoodsId())){
                                //审核过
                                if (!"0".equals(a.getStatus())) {
                                    flag = false;
                                    if("3".equals(a.getType())){
                                        orderVo.setSendShip(true);
                                        tag =true;
                                        break;
                                    }else{
                                        //是否同意退款(换货)
                                        if (a.getIsRefund()) {
                                            //售后数量是否跟购物数量一致
                                            if (!a.getNumber().equals(g.getNumber())){
                                                orderVo.setSendShip(true);
                                                tag =true;
                                                break;
                                            }
                                        }else{
                                            //只要有一件商品不同意退款就可以发货
                                            orderVo.setSendShip(true);
                                            tag =true;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        if (tag){
                            break;
                        }
                    }
                    //都未审核过
                    if (flag) {
                        orderVo.setSendShip(flag);
                    }
                }
            }
        });

        map.put("orderList", zyOrderList);
        //查询所有物流公司
        List<MallShipCompanyV2> list = shipCompanyService.list(new QueryWrapper<MallShipCompanyV2>().lambda().eq(MallShipCompanyV2::getDeleteFlag, false));
        map.put("shipCompanyList", list);
        return ResponseUtil.ok(map);
    }

    @Autowired
    private MallOrderGoodsService mallOrderGoodsService;
    /**
     * 订单修改
     */
    @RequiresPermissions("admin:order:modify")
    @RequiresPermissionsDesc(menu = {"销售订单"}, button = "修改")
    @PostMapping("/modify")
    public Object modifyOrder(IMallOrder order,String flag) {
        if (StrUtil.isNotBlank(flag) && "updatePrice".equals(flag)){
            //重新计算实付金额
            IMallOrder mallOrder = orderZYService.getById(order.getId());
            BigDecimal actualPrice = order.getOrderPrice().subtract(mallOrder.getCouponPrice()).subtract(mallOrder.getIntegralPrice());
            mallOrder.setActualPrice(actualPrice);

            mallOrder.setTicket("(商家改价)");
            mallOrder.setOrderPrice(order.getOrderPrice());
            order.setRemainingRefundAmount(order.getOrderPrice());//修改退款金额

            MallOrderGoods mallPurchaseOrderGoods = mallOrderGoodsService.queryByOid(order.getId()).get(0);
            MallGoods mallGoods = mallGoodsService.findById(mallPurchaseOrderGoods.getGoodsId());
            //重新计算分润金额
            BigDecimal pubSharePreFee = mallOrder.getActualPrice().multiply(mallGoods.getFirstRatio().divide(BigDecimal.valueOf(100)));
            mallOrder.setPubSharePreFee(pubSharePreFee.toString());

            orderZYService.updateById(mallOrder);
            return ResponseUtil.ok();
        }
        if (ObjectUtil.isNotEmpty(order.getShipCode())) {
            order.setShipTime(LocalDateTime.now());
        }
        order.setUpdateTime(LocalDateTime.now());
        orderZYService.updateById(order);
        return ResponseUtil.ok();
    }

    @RequiresPermissions("admin:order:zyDetail")
    @RequiresPermissionsDesc(menu = {"订单管理, 自营订单管理"}, button = "订单详情")
    @GetMapping("/zyDetail")
    public Object zyDetail(@NotNull String orderId) {
        //查询订单相关商品
        List<IMallOrderGoods> orderGoods = orderGoodsService.list(new QueryWrapper<IMallOrderGoods>()
                .eq("order_id", orderId).select("id,pic_url,goods_id,goods_name,specifications,price,number,(price * number)actualPrice"));
        List<ZYOrderGoodsVo> goodsVos = new ArrayList<ZYOrderGoodsVo>();
        orderGoods.forEach(item -> {
            ZYOrderGoodsVo orderGoodsVo = new ZYOrderGoodsVo();
            BeanUtil.copyProperties(item, orderGoodsVo);
            MallAftersale aftersale = afterSaleService.findByOrderGoodsId(item.getId());
            if (ObjectUtil.isNotNull(aftersale)) {
                if ("0".equals(aftersale.getStatus())) {
                    //售后中
                    orderGoodsVo.setAfterSaleStatus(1);
                } else {
                    //售后完成
                    orderGoodsVo.setAfterSaleStatus(2);
                }
                orderGoodsVo.setMallAftersale(aftersale);
            } else {
                //未售后
                orderGoodsVo.setAfterSaleStatus(0);
            }

            MallGoods mallGoods = mallGoodsService.findById(item.getGoodsId());

            MallMerchant mallMerchant = mallMerchantService.getById(mallGoods.getMerchantId());
            if(mallMerchant != null){
                orderGoodsVo.setMerchantName(mallMerchant.getName());
                orderGoodsVo.setMerchantUrl(mallMerchant.getLog());
            }
            goodsVos.add(orderGoodsVo);
        });

        HashMap<String, Object> map = new HashMap<>();
        map.put("orderGoods", goodsVos);

        //查询分润
        List<FcAccountPrechargeBill> list= deviceOrderDRecordMapper.selectFcAccountPrechargeBillsByOrderId(orderId,FcAccountPrechargeBillEnum.ORDER_TYPE_1.getCode());
        List<Object> listLs = new ArrayList<>();
        Map<String,Object> map1 = new HashMap<>();
        list.forEach(item ->{
            MallUser user = mallUserService.findById(item.getCustomerId());
            if (null != user) {
                if (FcAccountPrechargeBillEnum.RECHARGE_CLASSIFCATION_5.getCode().equals(item.getRechargeClassification().toString())) {
                    // 高级门店
                    map1.put("pUser", user.getNickname());
                    map1.put("pMobile", user.getMobile());
                    map1.put("pStatus", item.getCheckStatus());
                    map1.put("pAmt", item.getAmt());
                    map1.put("pNote", item.getNote());
                } else if (FcAccountPrechargeBillEnum.RECHARGE_CLASSIFCATION_7.getCode().equals(item.getRechargeClassification().toString())) {
                    // 商家
                    map1.put("mUser", user.getNickname());
                    map1.put("mMobile", user.getMobile());
                    map1.put("mAmt", item.getAmt());
                    map1.put("mStatus", item.getCheckStatus());
                    map1.put("mNote", item.getNote());
                } else if (FcAccountPrechargeBillEnum.RECHARGE_CLASSIFCATION_8.getCode().equals(item.getRechargeClassification().toString())) {
                    // 推广员
                    map1.put("tUser", user.getNickname());
                    map1.put("tMobile", user.getMobile());
                    map1.put("tAmt", item.getAmt());
                    map1.put("tStatus", item.getCheckStatus());
                    map1.put("tNote", item.getNote());
                }
            }
        });
        if(ObjectUtil.isEmpty(map1)){
            map.put("agentLs", listLs);
        }else{
            listLs.add(map1);
        }

        map.put("agentLs", listLs);
        return ResponseUtil.ok(map);
    }
}
