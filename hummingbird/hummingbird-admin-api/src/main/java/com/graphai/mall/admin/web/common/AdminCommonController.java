package com.graphai.mall.admin.web.common;

import com.alibaba.fastjson.JSONObject;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.mall.admin.util.FileUtils;
import com.graphai.mall.admin.util.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 通用下载请求处理
 *
 * @author Qxian
 */
@Controller
@RequestMapping("/admin/common")
public class AdminCommonController {
    private static final Logger log = LoggerFactory.getLogger(AdminCommonController.class);

    /**
     * 文件上传訪問路径
     */
    public static final String UPLOAD_PATH = "/storage";

    /**
     * 资源映射路径 前缀
     */
    public static final String RESOURCE_PREFIX = "/storage";

    /**
     * 本地资源红包码下载-方法1
     */
//    @RequiresPermissions("admin:qrcode:modify")
    @RequiresPermissionsDesc(menu = {"单个红包码下载"}, button = "红包码下载")
    @GetMapping("/download/resource")
    public void resourceDownload(String fileNamePath, HttpServletRequest request, HttpServletResponse response) throws Exception {
//        本地资源路径
//        String localPath = Global.getProfile();
//        数据库资源地址
//        String downloadPath = localPath + StringUtils.substringAfter(resource, RESOURCE_PREFIX);
//        JSONObject jsonObject = JSONObject.parseObject(data);
//        String resource = (String) jsonObject.get("fileNamePath");
        String downloadPath = fileNamePath;
//        String downloadPath = "D:\\projects\\idea-workspace-gongxiang\\weilai-life\\storage\\qrcode63725A\\63725A.png";
        log.info("--------------downloadPath--------------------"+downloadPath); // D:\\projects\\idea-workspace-gongxiang\\weilai-life\\storage\\qrcode63725A\\63725A.png
        // 下载名称
        String downloadName = StringUtils.substringAfterLast(downloadPath, "\\");
        log.info("--------------downloadName--------------------"+downloadName); // 63725A.png
        response.setCharacterEncoding("utf-8");
        response.setContentType("multipart/form-data");
        response.setHeader("Content-Disposition",
                "attachment;fileName=" + FileUtils.setFileDownloadHeader(request, downloadName));
        FileUtils.writeBytes(downloadPath, response.getOutputStream());
    }

    /**
     * 本地资源红包码下载-方法2
     */
//    @RequiresPermissions("admin:qrcode:modify")
    @RequiresPermissionsDesc(menu = {"单个红包码下载2"}, button = "红包码下载2")
    @GetMapping("/download/resource2")
    public void resourceDownload2(String fileNamePath, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String downloadPath = fileNamePath;
        log.info("--------------downloadPath2--------------------"+downloadPath); // D:\\projects\\idea-workspace-gongxiang\\weilai-life\\storage\\qrcode63725A\\63725A.png
        // 下载名称
        String downloadName = StringUtils.substringAfterLast(downloadPath, "\\");
        log.info("--------------downloadName2--------------------"+downloadName); // 63725A.png

        String fileUrl = downloadPath;
        if(fileUrl != null){
            response.setCharacterEncoding("utf-8");
            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition","attachment;fileName=" + FileUtils.setFileDownloadHeader(request, downloadName));
            //将文件转换成二进制文件
            FileUtils.writeBytes(fileUrl,response.getOutputStream());
        }
    }


    /**
     * 通用下载请求【压缩文件下载】
     *
     * @param fileName 文件名称
     * @param delete   是否删除
     */
    @GetMapping("/download")
    public void fileDownload(String fileName, Boolean delete, HttpServletResponse response, HttpServletRequest request) {
        try {
            if (!FileUtils.isValidFilename(fileName)) {
                throw new Exception(StringUtils.format("文件名称({})非法，不允许下载。 ", fileName));
            }
            String realFileName = System.currentTimeMillis() + fileName.substring(fileName.indexOf("_") + 1);
            String filePath = RESOURCE_PREFIX + fileName;

            response.setCharacterEncoding("utf-8");
            response.setContentType("multipart/form-data");
            response.setHeader("Content-Disposition",
                    "attachment;fileName=" + FileUtils.setFileDownloadHeader(request, realFileName));
            FileUtils.writeBytes(filePath, response.getOutputStream());
            if (delete) {
                FileUtils.deleteFile(filePath);
            }
        } catch (Exception e) {
            log.error("下载文件失败", e);
        }
    }

//    /**
//     * 通用上传请求
//     */
//    @PostMapping("/upload")
//    @ResponseBody
//    public AjaxResult uploadFile(MultipartFile file) {
//        try {
//            // 上传文件路径
//            String filePath = Global.getUploadPath();
//            // 上传并返回新文件名称
//            String fileName = FileUploadUtils.upload(filePath, file);
//            String url = serverConfig.getUrl() + UPLOAD_PATH + fileName;
//            AjaxResult ajax = AjaxResult.success();
//            ajax.put("fileName", fileName);
//            ajax.put("url", url);
//            return ajax;
//        } catch (Exception e) {
//            return AjaxResult.error(e.getMessage());
//        }
//    }
//
//

}
