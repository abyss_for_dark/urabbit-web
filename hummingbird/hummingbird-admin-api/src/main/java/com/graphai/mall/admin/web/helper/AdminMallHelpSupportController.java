package com.graphai.mall.admin.web.helper;

import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.mall.db.domain.MallHelpCategory;
import com.graphai.mall.db.service.helper.MallHelpSupportService;
import com.graphai.mall.db.vo.MallHelpSupportVo;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 帮助管理
 *
 * @author LaoGF
 * @since 2020-6-27
 */

@RestController
@RequestMapping("/admin/helpSupport")
@Validated
public class AdminMallHelpSupportController {

    @Resource
    private MallHelpSupportService mallHelpSupportService;


    /**
     * 列表
     */
    @RequiresPermissions("admin:helpSupport:list")
    @RequiresPermissionsDesc(menu = {"帮助服务", "帮助管理"}, button = "查询")
    @GetMapping("/list")
    public Object list(
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String question,
            @RequestParam(required = false) String categoryId,
            @RequestParam(required = false) Integer state,
            @RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer limit) {

        List<MallHelpSupportVo> list = mallHelpSupportService.selectList(question,name, categoryId, state, page, limit);
        long total = PageInfo.of(list).getTotal();
        Map<String, Object> data = new HashMap<>(16);
        data.put("total", total);
        data.put("items", list);
        return ResponseUtil.ok(data);
    }


    /**
     * 查询一级类目
     */
    @GetMapping("/selectCategory")
    public Object selectCategory() {
        List<MallHelpCategory> list = mallHelpSupportService.selectCategory();
        long total = PageInfo.of(list).getTotal();
        Map<String, Object> data = new HashMap<>(16);
        data.put("total", total);
        data.put("items", list);
        return ResponseUtil.ok(data);
    }

    /**
     * 查询二级类目
     */
    @GetMapping("/selectCategoryByOne")
    public Object selectCategoryByOne(@RequestParam String id) {
        List<MallHelpCategory> list = mallHelpSupportService.selectCategoryByOne(id);
        long total = PageInfo.of(list).getTotal();
        Map<String, Object> data = new HashMap<>(16);
        data.put("total", total);
        data.put("items", list);
        return ResponseUtil.ok(data);
    }

    /**
     * 查询 分类
     */
    @GetMapping("/selectCategoryList")
    public Object selectCategoryList() {
        return ResponseUtil.ok(mallHelpSupportService.selectCategoryList());
    }


    /**
     * 添加
     */
    @RequiresPermissions("admin:helpSupport:add")
    @RequiresPermissionsDesc(menu = {"帮助服务", "帮助管理"}, button = "添加")
    @PostMapping("/add")
    public Object add(@RequestBody MallHelpSupportVo mallHelpSupportVo) {
        if (mallHelpSupportService.insert(mallHelpSupportVo)) {
            return ResponseUtil.ok();
        }
        return ResponseUtil.fail();
    }

    /**
     * 修改
     */
    @RequiresPermissions("admin:helpSupport:update")
    @RequiresPermissionsDesc(menu = {"帮助服务", "帮助管理"}, button = "修改")
    @PostMapping("/update")
    public Object update(@RequestBody MallHelpSupportVo mallHelpSupportVo) {
        if (mallHelpSupportService.update(mallHelpSupportVo)) {
            return ResponseUtil.ok();
        }
        return ResponseUtil.fail();
    }

    /**
     * 删除
     */
    @RequiresPermissions("admin:helpSupport:delete")
    @RequiresPermissionsDesc(menu = {"帮助服务", "帮助管理"}, button = "删除")
    @PostMapping("/delete")
    public Object delete(@RequestParam String id) {
        if (mallHelpSupportService.deleteByID(id)) {
            return ResponseUtil.ok();
        }
        return ResponseUtil.fail();
    }

    /**
     * 状态修改
     */
    @RequiresPermissions("admin:helpSupport:updateStatus")
    @RequiresPermissionsDesc(menu = {"帮助服务", "帮助管理"}, button = "状态修改")
    @PostMapping("/updateStatus")
    public Object updateStatus(@RequestParam String id,
                               @RequestParam Integer state) {

        if (mallHelpSupportService.updateStatus(id, state)) {
            return ResponseUtil.ok();
        }
        return ResponseUtil.fail();
    }


}
