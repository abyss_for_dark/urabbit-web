package com.graphai.mall.admin.web.integral;

import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.mall.admin.service.AdminGoodsIntegralService;
import com.graphai.mall.admin.web.goods.AdminGoodsController;
import com.graphai.mall.db.domain.MallGoods;
import com.graphai.mall.db.domain.mall.GoodsAllinone;
import com.graphai.validator.Order;
import com.graphai.validator.Sort;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;

/**
 * 商品管理-积分商城
 *
 * @author Qxian
 * @version V0.0.1
 * <p>
 * #所有积分商城的商品
 * select * from mall_goods where virtual_good=3;
 * @date 2020-10-19
 */
@RestController
@RequestMapping("/admin/goods/integral")
@Validated
public class AdminGoodsIntegralController {
    private final Log logger = LogFactory.getLog(AdminGoodsController.class);

    @Autowired
    private AdminGoodsIntegralService adminGoodsIntegralService;

    /**
     * 查询商品
     *
     * @param goodsSn
     * @param name
     * @param page
     * @param limit
     * @param sort
     * @param order
     * @return
     */
    @RequiresPermissions("admin:goods:integral:list")
    @RequiresPermissionsDesc(menu = {"商品管理", "积分商品列表"}, button = "查询")
    @GetMapping("/list")
    public Object list(String goodsSn, String name, String categoryId,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit,
                       @Sort @RequestParam(defaultValue = "id") String sort,
                       @Order @RequestParam(defaultValue = "desc") String order,
                       String brandName, Integer priceMin, Integer priceMax,
                       Boolean isHot, Boolean isNew, Boolean isOnSale,
                       HttpServletRequest request) {

        return adminGoodsIntegralService.list(goodsSn, name, categoryId, page, limit, sort, order,
                brandName, priceMin, priceMax, isHot, isNew, isOnSale);
    }

    @GetMapping("/catAndBrand")
    public Object list2() {
        return adminGoodsIntegralService.list2();
    }

    /**
     * 编辑商品
     *
     * @param goodsAllinone
     * @return
     */
    @RequiresPermissions("admin:goods:integral:update")
    @RequiresPermissionsDesc(menu = {"商品管理", "积分商品列表"}, button = "编辑")
    @PostMapping("/update")
    public Object update(@RequestBody GoodsAllinone goodsAllinone) {
        return adminGoodsIntegralService.update(goodsAllinone);
    }

    /**
     * 删除商品
     *
     * @param goods
     * @return
     */
    @RequiresPermissions("admin:goods:integral:delete")
    @RequiresPermissionsDesc(menu = {"商品管理", "积分商品列表"}, button = "删除")
    @PostMapping("/delete")
    public Object delete(@RequestBody MallGoods goods) {
        return adminGoodsIntegralService.delete(goods);
    }

    /**
     * 添加商品
     *
     * @param goodsAllinone
     * @return
     */
    @RequiresPermissions("admin:goods:integral:create")
    @RequiresPermissionsDesc(menu = {"商品管理", "积分商品列表"}, button = "上架")
    @PostMapping("/create")
    public Object create(@RequestBody GoodsAllinone goodsAllinone) {
        return adminGoodsIntegralService.create(goodsAllinone);
    }

    /**
     * 商品详情
     *
     * @param id
     * @return
     */
    @RequiresPermissions("admin:goods:integral:read")
    @RequiresPermissionsDesc(menu = {"商品管理", "积分商品列表"}, button = "详情")
    @GetMapping("/detail")
    public Object detail(@NotNull String id) {
        return adminGoodsIntegralService.detail(id);

    }

}
