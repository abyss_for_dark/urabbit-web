package com.graphai.mall.admin.web.category;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.mall.admin.vo.CategoryVo;
import com.graphai.mall.db.domain.MallCategory;
import com.graphai.mall.db.service.integral.MallCategoryIntegralService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 积分商品类目-积分商城
 *
 * @author Qxian
 * @version V0.0.1
 * <p>
 * #所有积分商城的类目
 * select * from mall_category where position_code='litemall';
 * @date 2020-10-19
 */
@RestController
@RequestMapping("/admin/category/integral")
@Validated
public class AdminCategoryIntegralController {
    private final Log logger = LogFactory.getLog(AdminCategoryController.class);

    @Autowired
    private MallCategoryIntegralService categoryServiceIntegral;

    @RequiresPermissions("admin:categoryIntegral:list")
    @RequiresPermissionsDesc(menu = {"商品管理", "积分商品类目"}, button = "查询")
    @GetMapping("/list")
    public Object list() {
        List<CategoryVo> categoryVoList = new ArrayList<>();

        List<MallCategory> categoryList = categoryServiceIntegral.queryByPid("0");
        for (MallCategory category : categoryList) {
            CategoryVo categoryVO = new CategoryVo();
            categoryVO.setId(category.getId());
            categoryVO.setDesc(category.getDesc());
            categoryVO.setIconUrl(category.getIconUrl());
            categoryVO.setPicUrl(category.getPicUrl());
            categoryVO.setKeywords(category.getKeywords());
            categoryVO.setName(category.getName());
            categoryVO.setLevel(category.getLevel());

            List<CategoryVo> children = new ArrayList<>();
            List<MallCategory> subCategoryList = categoryServiceIntegral.queryByPid(category.getId());
            for (MallCategory subCategory : subCategoryList) {
                CategoryVo subCategoryVo = new CategoryVo();
                subCategoryVo.setId(subCategory.getId());
                subCategoryVo.setDesc(subCategory.getDesc());
                subCategoryVo.setIconUrl(subCategory.getIconUrl());
                subCategoryVo.setPicUrl(subCategory.getPicUrl());
                subCategoryVo.setKeywords(subCategory.getKeywords());
                subCategoryVo.setName(subCategory.getName());
                subCategoryVo.setLevel(subCategory.getLevel());

                children.add(subCategoryVo);
            }

            categoryVO.setChildren(children);
            categoryVoList.add(categoryVO);
        }

        return ResponseUtil.ok(categoryVoList);
    }

    private Object validate(MallCategory category) {
        String name = category.getName();
        if (StringUtils.isEmpty(name)) {
            return ResponseUtil.badArgument();
        }

        String level = category.getLevel();
        if (StringUtils.isEmpty(level)) {
            return ResponseUtil.badArgument();
        }
        if (!level.equals("L1") && !level.equals("L2")) {
            return ResponseUtil.badArgumentValue();
        }

        String pid = category.getPid();
        if (level.equals("L2") && (pid == null)) {
            return ResponseUtil.badArgument();
        }

        return null;
    }

    @RequiresPermissions("admin:categoryIntegral:create")
    @RequiresPermissionsDesc(menu = {"商品管理", "积分商品类目"}, button = "添加")
    @PostMapping("/create")
    public Object create(@RequestBody MallCategory category) {
        Object error = validate(category);
        if (error != null) {
            return error;
        }
        categoryServiceIntegral.add(category);
        return ResponseUtil.ok(category);
    }

    @RequiresPermissions("admin:categoryIntegral:read")
    @RequiresPermissionsDesc(menu = {"商品管理", "积分商品类目"}, button = "详情")
    @GetMapping("/read")
    public Object read(@NotNull String id) {
        MallCategory category = categoryServiceIntegral.findById(id);
        return ResponseUtil.ok(category);
    }

    @RequiresPermissions("admin:categoryIntegral:update")
    @RequiresPermissionsDesc(menu = {"商品管理", "积分商品类目"}, button = "编辑")
    @PostMapping("/update")
    public Object update(@RequestBody MallCategory category) {
        Object error = validate(category);
        if (error != null) {
            return error;
        }

        if (categoryServiceIntegral.updateById(category) == 0) {
            return ResponseUtil.updatedDataFailed();
        }
        return ResponseUtil.ok();
    }

    @RequiresPermissions("admin:categoryIntegral:delete")
    @RequiresPermissionsDesc(menu = {"商品管理", "积分商品类目"}, button = "删除")
    @PostMapping("/delete")
    public Object delete(@RequestBody MallCategory category) {
        String id = category.getId();
        if (id == null) {
            return ResponseUtil.badArgument();
        }
        categoryServiceIntegral.deleteById(id);
        return ResponseUtil.ok();
    }

    @RequiresPermissions("admin:categoryIntegral:list")
    @GetMapping("/l1")
    public Object catL1() {
        // 所有一级分类目录
        List<MallCategory> l1CatList = categoryServiceIntegral.queryL1("1");
        List<Map<String, Object>> data = new ArrayList<>(l1CatList.size());
        for (MallCategory category : l1CatList) {
            Map<String, Object> d = new HashMap<>(2);
            d.put("value", category.getId());
            d.put("label", category.getName());
            data.add(d);
        }
        return ResponseUtil.ok(data);
    }
}
