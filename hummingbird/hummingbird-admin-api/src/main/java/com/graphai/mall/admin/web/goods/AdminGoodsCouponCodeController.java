package com.graphai.mall.admin.web.goods;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.mall.admin.service.AdminGoodsCouponCodeService;
import com.graphai.mall.db.domain.MallGoodsCouponCode;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;

/**
 * Created by wyd on 2020-06-28 09:06
 */
@RestController
@RequestMapping("/admin/goodsCouponCode")
@Validated
public class AdminGoodsCouponCodeController {

    @Autowired
    private AdminGoodsCouponCodeService adminGoodsCouponCodeService;


    /**
     * 商品管理
     *
     * @param goodsId
     * @param saleStatus
     * @param useStatus
     * @param couponCard
     * @param name
     * @param page
     * @param limit
     * @param sort
     * @param order
     * @return
     */
    @RequiresPermissions("admin:goodsCouponCode:list")
    @RequiresPermissionsDesc(menu = {"商品管理", "券码管理"}, button = "查询")
    @GetMapping("/list")
    public Object list(String goodsId, Boolean saleStatus, Boolean useStatus, String couponCard, String name,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit,
                       @RequestParam(defaultValue = "update_time") String sort,
                       @RequestParam(defaultValue = "desc") String order) {
        if (StringUtils.isEmpty(goodsId)) {
            return ResponseUtil.badArgument();
        }
        return adminGoodsCouponCodeService.goodsCouponCodeList(goodsId, saleStatus, useStatus, couponCard, name, page, limit, sort, order);
    }

    /**
     * 券码新增
     *
     * @param mallGoodsCouponCode
     * @return
     */
    @RequiresPermissions("admin:goodsCouponCode:create")
    @RequiresPermissionsDesc(menu = {"商品管理", "券码管理"}, button = "添加")
    @PostMapping("/create")
    public Object create(MallGoodsCouponCode mallGoodsCouponCode) {
        return adminGoodsCouponCodeService.create(mallGoodsCouponCode);
    }

    /**
     * 券码详情
     *
     * @param id
     * @return
     */
    @RequiresPermissions("admin:goodsCouponCode:detail")
    @RequiresPermissionsDesc(menu = {"商品管理", "券码管理"}, button = "详情")
    @GetMapping("/detail")
    public Object detail(@NotNull String id) {
        return adminGoodsCouponCodeService.detail(id);
    }

    /**
     * 编辑券码
     *
     * @param mallGoodsCouponCode
     * @return
     */
    @RequiresPermissions("admin:goodsCouponCode:update")
    @RequiresPermissionsDesc(menu = {"商品管理", "券码管理"}, button = "编辑")
    @PostMapping("/update")
    public Object update(@RequestBody MallGoodsCouponCode mallGoodsCouponCode) {
        return adminGoodsCouponCodeService.update(mallGoodsCouponCode);
    }

    /**
     * 下架
     *
     * @param mallGoodsCouponCode
     * @return
     */
    @RequiresPermissions("admin:goodsCouponCode:disableGoodsCoupon")
    @RequiresPermissionsDesc(menu = {"商品管理", "券码管理"}, button = "下架")
    @PostMapping("/disableGoodsCoupon")
    public Object disableGoods(@RequestBody MallGoodsCouponCode mallGoodsCouponCode) {
        mallGoodsCouponCode.setDeleted(true);
        return adminGoodsCouponCodeService.update(mallGoodsCouponCode);
    }

    /**
     * 导入
     *
     * @param file
     * @param goodsId
     * @return
     * @throws Exception
     */
    @RequiresPermissions("admin:goodsCouponCode:importData")
    @RequiresPermissionsDesc(menu = {"商品管理", "券码管理"}, button = "导入")
    @PostMapping("/importData")
    @ResponseBody
    public Object importData(MultipartFile file, String goodsId) throws Exception {
        if (null == file || null == goodsId) {
            return ResponseUtil.badArgument();
        }
        return adminGoodsCouponCodeService.importData(file, goodsId);
    }

}
