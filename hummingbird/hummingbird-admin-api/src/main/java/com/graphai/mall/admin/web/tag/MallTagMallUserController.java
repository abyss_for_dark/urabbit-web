package com.graphai.mall.admin.web.tag;

import cn.afterturn.easypoi.excel.entity.ExportParams;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.lang.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.graphai.commons.util.servlet.ServletUtils;
import com.graphai.mall.admin.domain.MallTag;
import com.graphai.mall.db.domain.MallUser;
import com.graphai.mall.db.service.tag.IMallTagService;
import com.graphai.mall.db.service.user.MallUserService;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import org.springframework.web.bind.annotation.RestController;
import com.graphai.framework.generator.controller.IBaseController;

import com.graphai.mall.admin.domain.MallTagMallUser;
import com.graphai.mall.admin.service.IMallTagMallUserService;
import java.util.List;



/**
 * <p>
 * 标签和用户关联表 前端控制器
 * </p>
 *
 * @author biteam
 * @since 2021-05-22
 */
@RestController
@RequestMapping("/admin/tagUser")
public class MallTagMallUserController extends IBaseController<MallTagMallUser,  String >  {

	@Autowired
    private IMallTagMallUserService serviceImpl;

	@Autowired
    private MallUserService mallUserService;

	@Autowired
    private IMallTagService mallTagService;
    
    @RequiresPermissions("admin:tagUser:single")
    @RequiresPermissionsDesc(menu = {"单独查询"}, button = "单独查询")
	@GetMapping("/single/{id}")
    public Object getEntityById(@PathVariable  String  id) throws Exception{
        MallTagMallUser entityVName = serviceImpl.getById(id);
        return ResponseUtil.ok(entityVName);
    }
    
    @RequiresPermissions("admin:tagUser:addOrUpdate")
    @RequiresPermissionsDesc(menu = {"新增"}, button = "新增")
    @PostMapping("/addOrUpdate")
    public Object addOrUpdate(MallTagMallUser mallTagMallUser) throws Exception{
        if(serviceImpl.saveOrUpdate(mallTagMallUser)){
            return ResponseUtil.ok();
        }else{
            return ResponseUtil.fail();
        }
    } 
	
	
	@RequiresPermissions("admin:tagUser:melist")
	@RequiresPermissionsDesc(menu = {"标签和用户关联表查询"}, button = "查询")
    @GetMapping("/melist")
    public Object getMeList() throws Exception {
        IPage<MallTagMallUser> ipage = serviceImpl.page(getPlusPage(),listQueryWrapper());
        List<MallTagMallUser> mallTagMallUsers = ipage.getRecords();
        for (MallTagMallUser mallTagMallUser : mallTagMallUsers) {
            MallUser mallUser = mallUserService.findById(mallTagMallUser.getUserId());
            MallTag mallTag = mallTagService.getById(mallTagMallUser.getTagId());
            MallUser takeTagUser = mallUserService.findById(mallTagMallUser.getTakeTagUserId());


            mallTagMallUser.setTakeTagUserId(mallUser.getNickname()+mallUser.getMobile());
            mallTagMallUser.setTagId(mallTag.getName());
            mallTagMallUser.setUserId(takeTagUser.getNickname()+takeTagUser.getMobile());
        }
        return ResponseUtil.ok(ipage);
    }
 	
 	private QueryWrapper<MallTagMallUser> listQueryWrapper() {
 		QueryWrapper<MallTagMallUser> meq = new QueryWrapper<>();
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("userName"))) {
            String userName = ServletUtils.getParameter("userName");
            MallUser user = mallUserService.getUserByUserName(userName);
            if (user!=null){
                meq.eq("user_id", user.getId());
            }
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("tagName"))) {
            String tagName = ServletUtils.getParameter("tagName");
            MallTag mallTag=mallTagService.getTagByName(tagName);
            if (mallTag!=null){
                meq.eq("tag_id", mallTag.getId());
            }
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("deleted"))) {
		      meq.eq("deleted", ServletUtils.getParameter("deleted"));
		}
        return meq;
    }
 	@RequiresPermissions("admin:tagUser:save")
 	@RequiresPermissionsDesc(menu = {"标签和用户关联表新增"}, button = "新增")
    @PostMapping("/save")
    public Object save(@ModelAttribute MallTagMallUser mallTagMallUser) throws Exception {
        serviceImpl.save(mallTagMallUser);
        return ResponseUtil.ok();
    }
	
	@RequiresPermissions("admin:tagUser:modify")
	@RequiresPermissionsDesc(menu = {"标签和用户关联表修改"}, button = "修改")
    @PutMapping("/modify")
    public Object modify(@ModelAttribute  MallTagMallUser mallTagMallUser){
        serviceImpl.updateById(mallTagMallUser);
        return  ResponseUtil.ok();
    }

    @RequiresPermissions("admin:tagUser:del")
    @RequiresPermissionsDesc(menu = {"标签和用户关联表删除"}, button = "删除")
    @DeleteMapping("/del/{id}")
    public Object deleteById(@PathVariable  String  id){
        serviceImpl.removeById(id);
        return  ResponseUtil.ok();
    }

}

