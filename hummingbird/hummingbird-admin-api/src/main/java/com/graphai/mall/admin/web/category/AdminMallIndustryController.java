package com.graphai.mall.admin.web.category;


import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.json.JacksonUtil;
import com.graphai.mall.db.domain.MallIndustry;
import com.graphai.mall.db.service.category.MallIndustryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 行业管理
 */
@RestController
@RequestMapping("/admin/industry")
@Validated
public class AdminMallIndustryController {

    @Autowired
    private MallIndustryService mallIndustryService;


    /**
     * 行业列表
     *
     * @return
     */
    @RequestMapping("/list")
    public Object getIndustryList() {
        List<MallIndustry> list = mallIndustryService.getIndustryList();
        return ResponseUtil.ok(list);
    }

    /**
     * 修改行业
     */
    @PostMapping("/edit")
    public Object getEditIndustry(@RequestBody String data) {
        String id = JacksonUtil.parseString(data, "id");
        String name = JacksonUtil.parseString(data, "name");
        String stor = JacksonUtil.parseString(data, "stor");
        String status = JacksonUtil.parseString(data, "status");

        MallIndustry mallIndustry = mallIndustryService.getIndustryById(id);
        if (null != mallIndustry) {
            mallIndustry.setSortOrder(Integer.parseInt(stor));
            mallIndustry.setIname(name);
            mallIndustry.setStatus(status);

            mallIndustryService.updateIndustryById(mallIndustry);
        }
        return ResponseUtil.ok();
    }

    /**
     * 删除行业
     */
    @GetMapping("/delete")
    public Object deleteIndustry(@RequestParam String id) {
        MallIndustry mallIndustry = mallIndustryService.getIndustryById(id);
        if (null != mallIndustry) {
            mallIndustry.setDeleted(true);
            mallIndustryService.updateIndustryById(mallIndustry);
        } else {
            return ResponseUtil.fail(403, "删除失败！");
        }

        return ResponseUtil.ok();
    }

    /**
     * 禁 启 用行业
     */
    @GetMapping("/disable")
    public Object disableIndustry(@RequestParam String id, @RequestParam String disable) {
        MallIndustry mallIndustry = mallIndustryService.getIndustryById(id);
        if (null != mallIndustry) {
            if ("0".equals(disable)) {
                mallIndustry.setStatus(disable);
            } else if ("1".equals(disable)) {
                mallIndustry.setStatus(disable);
            }

            mallIndustryService.updateIndustryById(mallIndustry);
        } else {
            return ResponseUtil.fail(403, "失败！");
        }

        return ResponseUtil.ok();
    }

    /**
     * 创建行业
     */
    @PostMapping("/add")
    public Object addIndustry(@RequestBody MallIndustry mallIndustry) {
        Object obj = validateComplete(mallIndustry);
        if (null != obj) {
            return obj;
        }
        mallIndustryService.insertIndustry(mallIndustry);
        return ResponseUtil.ok();
    }

    /**
     * 模糊查询行业
     */
    @RequestMapping("/like")
    public Object getIndustryListByName(@RequestParam String name) {

        List<MallIndustry> list = mallIndustryService.getIndustryListByName(name);
        return ResponseUtil.ok(list);
    }


    /**
     * 校验数据(完成)
     */
    private Object validateComplete(MallIndustry mallIndustry) {
        //validate(wangzhuanUserTask);
        //行业名称
        if (StringUtils.isEmpty(mallIndustry.getIname())) {
            return ResponseUtil.badArgument();
        }
        //行业状态
        if (StringUtils.isEmpty(mallIndustry.getStatus())) {
            return ResponseUtil.badArgument();
        }
        //行业存放部位
        if (StringUtils.isEmpty(mallIndustry.getPosition())) {
            return ResponseUtil.badArgument();
        }
        //行业排序
        if (StringUtils.isEmpty(mallIndustry.getSortOrder())) {
            return ResponseUtil.badArgument();
        }
        return null;
    }

}
