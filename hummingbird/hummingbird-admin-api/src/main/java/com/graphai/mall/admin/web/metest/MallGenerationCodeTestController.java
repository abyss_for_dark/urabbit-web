package com.graphai.mall.admin.web.metest;

import org.springframework.beans.factory.annotation.Autowired;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.baomidou.mybatisplus.core.metadata.IPage;

import org.springframework.web.bind.annotation.RestController;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.framework.generator.controller.IBaseController;

import com.graphai.mall.admin.domain.MallGenerationCodeTest;
import com.graphai.mall.admin.service.IMallGenerationCodeTestService;

import com.baomidou.mybatisplus.extension.service.IService;


/**
 * <p>
 * 订单表 前端控制器
 * </p>
 *
 * @author biteam
 * @since 2020-11-19
 */
@RestController
@RequestMapping("/admin/test")
public class MallGenerationCodeTestController extends IBaseController<MallGenerationCodeTest,  String >  {

	@Autowired
    private IMallGenerationCodeTestService serviceImpl;
    
    @RequiresPermissions("admin:test:single")
    @RequiresPermissionsDesc(menu = {"单独查询"}, button = "单独查询")
	@GetMapping("/single/{id}")
    public Object getEntityById(@PathVariable  String  id) throws Exception{
        MallGenerationCodeTest entityVName = serviceImpl.getById(id);
        return ResponseUtil.ok(entityVName);
    }
    
    @RequiresPermissions("admin:test:addOrUpdate")
    @RequiresPermissionsDesc(menu = {"新增"}, button = "新增")
    @PostMapping("/addOrUpdate")
    public Object addOrUpdate(MallGenerationCodeTest mallGenerationCodeTest) throws Exception{
        if(serviceImpl.saveOrUpdate(mallGenerationCodeTest)){
            return ResponseUtil.ok();
        }else{
            return ResponseUtil.fail();
        }
    } 

	@RequiresPermissions("admin:test:melist")
	@RequiresPermissionsDesc(menu = {"订单表查询"}, button = "查询")
    @GetMapping("/melist")
    public Object getMeList() throws Exception {
        IPage<MallGenerationCodeTest> ipage = serviceImpl.page(getPlusPage());
        return ResponseUtil.ok(ipage);
    }
 	
 	@RequiresPermissions("admin:test:save")
 	@RequiresPermissionsDesc(menu = {"订单表新增"}, button = "新增")
    @PostMapping("/save")
    public Object save(@ModelAttribute MallGenerationCodeTest mallGenerationCodeTest) throws Exception {
        serviceImpl.save(mallGenerationCodeTest);
        return ResponseUtil.ok();
    }
	
	@RequiresPermissions("admin:test:modify")
	@RequiresPermissionsDesc(menu = {"订单表修改"}, button = "修改")
    @PutMapping("/modify")
    public Object modify(@ModelAttribute  MallGenerationCodeTest mallGenerationCodeTest){
        serviceImpl.updateById(mallGenerationCodeTest);
        return  ResponseUtil.ok();
    }
    
    @RequiresPermissions("admin:test:del")
    @RequiresPermissionsDesc(menu = {"订单表删除"}, button = "删除")
    @DeleteMapping("/del/{id}")
    public Object deleteById(@PathVariable  String  id){
        serviceImpl.removeById(id);
        return  ResponseUtil.ok();
    }
    
}

