package com.graphai.mall.admin.web.helper;

import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.mall.db.domain.MallHelpTeachCategory;
import com.graphai.mall.db.service.helper.MallHelpTeachCategoryService;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 教程类目
 *
 * @author LaoGF
 * @since 2020-6-27
 */
@RestController
@RequestMapping("/admin/helpTeachCategory")
@Validated
public class AdminMallHelpTeachCategoryController {

    @Resource
    private MallHelpTeachCategoryService mallHelpTeachCategoryService;


    /**
     * 列表
     */
    @RequiresPermissions("admin:helpTeachCategory:list")
    @RequiresPermissionsDesc(menu = {"帮助服务", "教程类目"}, button = "查询")
    @GetMapping("/list")
    public Object list(
            @RequestParam(required = false) String name,
            @RequestParam(required = false) Integer state,
            @RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer limit) {

        List<MallHelpTeachCategory> list = mallHelpTeachCategoryService.selectByExample(name, state, page, limit);
        long total = PageInfo.of(list).getTotal();
        Map<String, Object> data = new HashMap<>(16);
        data.put("total", total);
        data.put("items", list);
        return ResponseUtil.ok(data);
    }


    /**
     * 添加
     */
    @RequiresPermissions("admin:helpTeachCategory:add")
    @RequiresPermissionsDesc(menu = {"帮助服务", "教程类目"}, button = "添加")
    @PostMapping("/add")
    public Object add(@RequestBody MallHelpTeachCategory mallHelpTeachCategory) {
        if (mallHelpTeachCategoryService.insert(mallHelpTeachCategory)) {
            return ResponseUtil.ok();
        }
        return ResponseUtil.fail();
    }

    /**
     * 修改
     */
    @RequiresPermissions("admin:helpTeachCategory:update")
    @RequiresPermissionsDesc(menu = {"帮助服务", "教程类目"}, button = "修改")
    @PostMapping("/update")
    public Object update(@RequestBody MallHelpTeachCategory mallHelpTeachCategory) {
        if (mallHelpTeachCategoryService.update(mallHelpTeachCategory)) {
            return ResponseUtil.ok();
        }
        return ResponseUtil.fail();
    }

    /**
     * 删除
     */
    @RequiresPermissions("admin:helpTeachCategory:delete")
    @RequiresPermissionsDesc(menu = {"帮助服务", "教程类目"}, button = "删除")
    @PostMapping("/delete")
    public Object delete(@RequestParam String id) {
        if (mallHelpTeachCategoryService.deleteByID(id)) {
            return ResponseUtil.ok();
        }
        return ResponseUtil.fail();
    }

    /**
     * 状态修改
     */
    @RequiresPermissions("admin:helpTeachCategory:updateStatus")
    @RequiresPermissionsDesc(menu = {"帮助服务", "教程类目"}, button = "状态修改")
    @PostMapping("/updateStatus")
    public Object updateStatus(@RequestParam String id,
                               @RequestParam Integer state) {
        if (mallHelpTeachCategoryService.updateStatus(id, state)) {
            return ResponseUtil.ok();
        }
        return ResponseUtil.fail();
    }


}
