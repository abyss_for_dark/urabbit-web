package com.graphai.mall.admin.service;

import com.alibaba.druid.support.json.JSONUtils;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.httprequest.HttpUtils;
import com.graphai.mall.admin.dto.GoodsAllinone;
import com.graphai.mall.admin.util.LocalDateTimeUtil;
import com.graphai.mall.db.config.TbkapiProperties;
import com.graphai.mall.db.dao.MallCategoryMapper;
import com.graphai.mall.db.dao.MallProductCouponMapper;
import com.graphai.mall.db.domain.*;
import com.graphai.mall.db.service.*;
import com.graphai.mall.db.service.advertisement.AdminAdSyncService;
import com.graphai.mall.db.service.advertisement.MallAdService;
import com.graphai.mall.db.service.category.MallCategoryService;
import com.graphai.mall.db.service.coupon.MallCouponService;
import com.graphai.mall.db.service.goods.MallGoodsService;

import jodd.util.StringUtil;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TbkGoodsAllSyncInstService {
    private final Log logger = LogFactory.getLog(TbkGoodsAllSyncInstService.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Autowired
    private MallAdService adService;

    @Autowired
    private AdminAdSyncService adminAdSyncService;

    @Autowired
    private MallCategoryMapper MallCategoryMapper;

    @Autowired
    private MallGoodsService goodsService;
    //    @Autowired
//    private MallGoodsSpecificationService specificationService;
//    @Autowired
//    private MallGoodsAttributeService attributeService;
//    @Autowired
//    private MallGoodsProductService productService;
    @Autowired
    private MallCategoryService categoryService;

    @Autowired
    private MallCouponService couponService;

    @Autowired
    private MallProductCouponMapper MallProductCouponMapper;

    @Autowired
    private TbkapiProperties properties;

    private final static String PXHTEST_pid = "mm_495000094_710300135_109273300405";

//    private final static String apiKey = "JWbiULZkNtEOVnp";

    public void clearData(String dataBatchId, String channelCode) {
        //所有数据执行完毕,
        //如果最新的接口查不出来的数据
        //从品牌活动表做逻辑删除，所有商品下架。
        adService.updateByDataBatchId(dataBatchId, channelCode);
        goodsService.updateByDataBatchId(dataBatchId, channelCode);
    }

    public void syscTbkGoods(int index, int count, String dataBatchId, int minId) {
        Map<String, String> paramsMap = new HashMap<String, String>();

        String url = "http://v2.api.haodanku.com/itemlist/apikey/" + properties.getApiKey() + "/nav/3/cid/0/back/1000/min_id/" + minId;
        url += "?item_type=1";

        String retVal = HttpUtils.get(url, paramsMap, null);
        if (index == 0 && minId == 1) {
//    		logger.info("retVal:"+retVal);
        }

        if (retVal != null) {
            Map<String, Object> retMap = (Map<String, Object>) JSONUtils.parse(retVal);
//    		System.out.println(retMap);
            if (retMap != null) {
                int listSize = 0;
                logger.error("min_id:" + MapUtils.getIntValue(retMap, "min_id"));
                if (1 == MapUtils.getIntValue(retMap, "code", 0)) {
                    List<Map<String, Object>> goodsList = (List) retMap.get("data");
                    if (goodsList != null) {
                        listSize = goodsList.size();
                        logger.error("goodsList size:" + goodsList.size());
                        for (Map<String, Object> goodsMap : goodsList) {
                            try {
                                this.syscGoods(goodsMap, dataBatchId, (byte) 0);
                            } catch (Exception e) {
                                logger.error("", e);
                            }

                        }
                    }
                }
                if (minId != MapUtils.getIntValue(retMap, "min_id")
                        && listSize == 1000
                        && 0 != MapUtils.getIntValue(retMap, "min_id")) {
                    this.syscTbkGoods(index, count, dataBatchId, MapUtils.getIntValue(retMap, "min_id"));
                }
            }
        }
    }

    public void syscTbkTQGGoods(String dataBatchId) {
        Integer[] hourTypeArr = new Integer[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
        for (Integer hourType : hourTypeArr) {
            this.syscTbkTQGGoods(hourType, dataBatchId);
        }
    }

    /**
     * 淘抢购商品同步
     *
     * @param index
     * @param count
     * @param dataBatchId
     */
    public void syscTbkTQGGoods(int hourType, String dataBatchId) {
        Map<String, String> paramsMap = new HashMap<String, String>();


        String url = "http://v2.api.haodanku.com/fastbuy/apikey/" + properties.getApiKey() + "/hour_type/" + hourType + "/min_id/1";

        String retVal = HttpUtils.get(url, paramsMap, null);

        if (retVal != null) {
            Map<String, Object> retMap = (Map<String, Object>) JSONUtils.parse(retVal);
//    		System.out.println(retMap);
            if (retMap != null && 1 == MapUtils.getIntValue(retMap, "code", 0)) {
                List<Map<String, Object>> goodsList = (List) retMap.get("data");
                if (goodsList != null) {
                    for (Map<String, Object> goodsMap : goodsList) {
                        try {
                            this.syscGoods(goodsMap, dataBatchId, (byte) 1);
                        } catch (Exception e) {
                            logger.error("", e);
                        }

                    }
                }

            }
        }
    }

    public void syscGoods(Map<String, Object> goodsMap, String dataBatchId, byte isFastBuy)
            throws Exception {

        MallGoods goodDto = new MallGoods();

        goodDto.setIsFastBuy(isFastBuy);

        goodDto.setDataBatchId(dataBatchId);
        goodDto.setChannelCode(properties.getChannelCode());

        goodDto.setGoodsSn(MapUtils.getString(goodsMap, "product_id"));
        goodDto.setSrcGoodId(MapUtils.getString(goodsMap, "itemid"));
        goodDto.setSellerId(MapUtils.getString(goodsMap, "seller_id"));
        goodDto.setName(MapUtils.getString(goodsMap, "itemtitle"));
        goodDto.setSrcGoodName(MapUtils.getString(goodsMap, "itemshorttitle"));
        goodDto.setBrief(MapUtils.getString(goodsMap, "itemdesc"));
        goodDto.setCounterPrice(new BigDecimal(MapUtils.getString(goodsMap, "itemprice")));
        goodDto.setItemsale(MapUtils.getInteger(goodsMap, "itemsale"));
        goodDto.setItemsale2(MapUtils.getInteger(goodsMap, "itemsale2"));
        goodDto.setTodaysale(MapUtils.getInteger(goodsMap, "todaysale"));
        goodDto.setPicUrl(MapUtils.getString(goodsMap, "itempic"));
        goodDto.setShareUrl(MapUtils.getString(goodsMap, "itempic_copy"));
        goodDto.setTbFirstCategory(MapUtils.getString(goodsMap, "fqcat"));
        goodDto.setRetailPrice(new BigDecimal(MapUtils.getString(goodsMap, "itemendprice")));
        goodDto.setShoptype(MapUtils.getString(goodsMap, "shoptype"));
        goodDto.setTktype(MapUtils.getString(goodsMap, "tktype"));
        goodDto.setTkrates(new BigDecimal(MapUtils.getString(goodsMap, "tkrates")));
        goodDto.setTkmoney(new BigDecimal(MapUtils.getString(goodsMap, "tkmoney")));
        goodDto.setReportStatus(MapUtils.getShort(goodsMap, "report_status", (short) -2));
        goodDto.setIsBrand(MapUtils.getByte(goodsMap, "is_brand"));
        goodDto.setIsBrand(MapUtils.getByte(goodsMap, "is_live"));
        goodDto.setGuideArticle(MapUtils.getString(goodsMap, "guide_article"));
        goodDto.setVideoid(MapUtils.getString(goodsMap, "videoid"));
        goodDto.setActivityType(MapUtils.getString(goodsMap, "activity_type"));
        goodDto.setGeneralIndex(MapUtils.getInteger(goodsMap, "general_index"));

        goodDto.setSellerName(MapUtils.getString(goodsMap, "seller_name"));
        goodDto.setTbUserid(MapUtils.getString(goodsMap, "userid"));
        goodDto.setSellernick(MapUtils.getString(goodsMap, "sellernick"));
        goodDto.setShopname(MapUtils.getString(goodsMap, "shopname"));
        if (StringUtil.isNotBlank(MapUtils.getString(goodsMap, "discount"))) {
            goodDto.setDiscount(new BigDecimal(MapUtils.getString(goodsMap, "discount")));
        }
        goodDto.setSrcAdId(MapUtils.getString(goodsMap, "activityid"));

        String taobaoImage = MapUtils.getString(goodsMap, "taobao_image");
        if (taobaoImage != null && taobaoImage.contains(",")) {
            String[] gallery = taobaoImage.split(",");
            goodDto.setGallery(gallery);
        }
        goodDto.setTbShopid(MapUtils.getString(goodsMap, "shopid"));
        goodDto.setTbSecondCategory(MapUtils.getString(goodsMap, "son_category"));
        goodDto.setCategoryId(this.getCategoryId(MapUtils.getString(goodsMap, "son_category")));

        if (StringUtil.isNotBlank(MapUtils.getString(goodsMap, "start_time"))) {
            goodDto.setFbStartTimeVal(MapUtils.getLong(goodsMap, "start_time"));
            goodDto.setFbStartTime(LocalDateTimeUtil.getLongTimestampToLocalDateTime(MapUtils.getLong(goodsMap, "start_time") * 1000));
        }

//		coupon.setStartTime(getLongTimestampToLocalDateTime(MapUtils.getLong(goodsMap, "couponstarttime")*1000));

        GoodsAllinone goodsAllinone = new GoodsAllinone();
        goodsAllinone.setGoods(goodDto);
        goodsAllinone.setProducts(null);
        goodsAllinone.setSpecifications(null);
        goodsAllinone.setAttributes(null);

//		"cuntao": "0",
//		"start_time": "1565830800",
//		"end_time": "1566489599",
//		"starttime": "1565830800",
//		"isquality": null,
//		"planlink": null,
//		"online_users": "0",
//		"original_img": null,
//		"original_article": null,
//		"is_explosion": "0",
//		"me": null,
//		"down_type": "0"

        MallCoupon coupon = new MallCoupon();
        coupon.setSrcCouponUrl(MapUtils.getString(goodsMap, "couponurl"));
        coupon.setDiscount(new BigDecimal(MapUtils.getString(goodsMap, "couponmoney")));
        coupon.setSurplus(MapUtils.getInteger(goodsMap, "couponsurplus"));
        coupon.setReceive(MapUtils.getInteger(goodsMap, "couponreceive"));
        coupon.setReceive2(MapUtils.getInteger(goodsMap, "couponreceive2"));
        coupon.setReceiveToday(MapUtils.getInteger(goodsMap, "todaycouponreceive"));
        coupon.setTotal(MapUtils.getInteger(goodsMap, "couponnum"));
        coupon.setDesc(MapUtils.getString(goodsMap, "couponexplain"));
        coupon.setStartTime(LocalDateTimeUtil.getLongTimestampToLocalDateTime(MapUtils.getLong(goodsMap, "couponstarttime") * 1000));
        coupon.setEndTime(LocalDateTimeUtil.getLongTimestampToLocalDateTime(MapUtils.getLong(goodsMap, "couponendtime") * 1000));
        if (StringUtil.isNotBlank(MapUtils.getString(goodsMap, "coupon_condition"))) {
            coupon.setMin(new BigDecimal(MapUtils.getString(goodsMap, "coupon_condition")));
        }

        coupon.setChannelCode(properties.getChannelCode());
        coupon.setType((short) 0);
        coupon.setName(MapUtils.getString(goodsMap, "couponmoney") + "元劵");
        coupon.setTag(MapUtils.getString(goodsMap, "couponmoney") + "元劵");
        coupon.setStatus((short) 0);
        coupon.setGoodsType((short) 2); //商品限制类型，如果0则全商品，如果是1则是类目限制，如果是2则是商品限制。
        coupon.setTimeType((short) 1); //有效时间限制，如果是0，则基于领取时间的有效天数days；如果是1，则start_time和end_time是优惠券有效期；
//		coupon.setLimit((short)1);

        String itemid = MapUtils.getString(goodsMap, "itemid");
        String activityid = MapUtils.getString(goodsMap, "activityid");
        String pid = PXHTEST_pid;
        String tbkRateJson = this.getTbkRate(properties.getApiKey(), itemid, pid, activityid);
//		logger.info("tbkRateJson:"+tbkRateJson);
        String couponUrl = null;
        if (tbkRateJson != null) {
//			{"code":1,"msg":"SUCCESS","data":{"coupon_click_url":"https:\/\/uland.taobao.com\/coupon\/edetail?e=CdkyzggTNgAGQASttHIRqVHah%2BGWizhcXFkkystIypHlOjPepplVBAQU2CTtHVBTggWoBz9zjak%2BAl%2BFfVh1xSYan619xCHOY1kWovEdEWM%2Bz74VcXAP5vsnwWZGSCD41ug731VBEQm0m3Ckm6GN2CwynAdGnOngMmnVmWO1H8uh%2BA5RKrMwjVLajOROSSHlJMLaH4QQ4RDfqTZMHsH3KQ%3D%3D&traceId=0be0f55815660262865193593e&union_lens=lensId:0b015dd6_0bc8_16c9e70c5d6_34ad&xId=E2xhqlcl7ek1XvmiHanR4KzsYD4Q2W9BlaHlCQwBqLopWF2Ymt6D1rxH6LAYmn1zdkDh8Qd68bV2XbPzc6qS7V&activityId=d7bf15a4866b4d918a92f2fc0da503d0&thispid=mm_495000094_710300135_109273300405&src=fklm_hltk&from=tool&sight=fklm","max_commission_rate":"9.00","item_url":"https:\/\/s.click.taobao.com\/t?e=m%3D2%26s%3DATnGk4G8liJw4vFB6t2Z2ueEDrYVVa64K7Vc7tFgwiHLWlSKdGSYDsSko49aIiOT1aH1Hk3GeOhhbYDruHmS7tA3MvHuOWf6X2bacVZvZJl26y%2BxsN90g716pcTImRFhzrASQX1ywjcUoTMK0NLo9wtkyqhBb5%2B4x8PUddoNe3DAE3knul3gBgbKYkQYHjG44JOed0Rxa6Mn6rrFNlAnMb4UhYgsvUYSxNJgbZ2tGPQj%2BxF9WdFnzsYl7w3%2FA2kb&union_lens=lensId:0b015dd6_0bc8_16c9e70c5d6_34ad&xId=E2xhqlcl7ek1XvmiHanR4KzsYD4Q2W9BlaHlCQwBqLopWF2Ymt6D1rxH6LAYmn1zdkDh8Qd68bV2XbPzc6qS7V","couponmoney":"30.0","couponstarttime":"1565971200","couponendtime":"1566230399","couponexplain":"满39.0元可用","couponnum":"100000","couponsurplus":"95000","couponreceive":5000}}
            Map<String, Object> rateMap = (Map<String, Object>) JSONUtils.parse(tbkRateJson);
            if ("1".equals(MapUtils.getString(rateMap, "code"))
                    && rateMap.get("data") != null) {
//    				"data": {
//    					"coupon_click_url": "https:\/\/uland.taobao.com\/coupon\/edetail?e=w1Z8kSeMTlQGQASttHIRqTLHvmgRjCEqvQbhw%2Bbp4BflOjPepplVBAQU2CTtHVBTggWoBz9zjak%2BAl%2BFfVh1xSYan619xCHOY1kWovEdEWM%2Bz74VcXAP5vsnwWZGSCD41ug731VBEQm0m3Ckm6GN2CwynAdGnOngMmnVmWO1H8uh%2BA5RKrMwjVLajOROSSHlJMLaH4QQ4RA76GKd7T%2Bn0A%3D%3D&traceId=0b14d5fd15660262910318574e&union_lens=lensId:0b0840e9_0c10_16c9e70d76c_6b6d&xId=U7K4N4oEslsKdjEtJL3VcymwWMF9IhL7rqMJSugLQ4tGhiWhejNpLaCDGo5VTmmxBZqSUNK2dvUn7C6QvIHsYZ&activityId=ab48c4eb9c5c45e49b3f20e76ee46a56&thispid=mm_495000094_710300135_109273300405&src=fklm_hltk&from=tool&sight=fklm",
//    					"max_commission_rate": "6.00",
//    					"item_url": "https:\/\/s.click.taobao.com\/t?e=m%3D2%26s%3DmKknbK4QgRVw4vFB6t2Z2ueEDrYVVa64K7Vc7tFgwiHLWlSKdGSYDu6rS0k4pQN%2FRitN3%2FurF3xhbYDruHmS7tA3MvHuOWf6X2bacVZvZJl26y%2BxsN90g716pcTImRFh3ywG1fovPwHL4Dw938c8SQtkyqhBb5%2B4x8PUddoNe3DAE3knul3gBgbKYkQYHjG44JOed0Rxa6NHOk04I6nOYJa6Ma3WtuPXxNJgbZ2tGPQj%2BxF9WdFnzsYl7w3%2FA2kb&union_lens=lensId:0b0840e9_0c10_16c9e70d76c_6b6d&xId=U7K4N4oEslsKdjEtJL3VcymwWMF9IhL7rqMJSugLQ4tGhiWhejNpLaCDGo5VTmmxBZqSUNK2dvUn7C6QvIHsYZ",
//    					"couponmoney": "25.0",
//    					"couponstarttime": "1565971200",
//    					"couponendtime": "1566575999",
//    					"couponexplain": "满34.0元可用",
//    					"couponnum": "50000",
//    					"couponsurplus": "47000",
//    					"couponreceive": 3000
                Map<String, Object> rateDateMap = (Map<String, Object>) rateMap.get("data");
                couponUrl = MapUtils.getString(rateDateMap, "coupon_click_url");
                couponUrl = couponUrl.replace("/", "");
                coupon.setSrcCouponUrl(MapUtils.getString(rateDateMap, "coupon_click_url"));

                BigDecimal tbkRate = new BigDecimal(MapUtils.getString(rateDateMap, "max_commission_rate"));
                // 佣金分成按照 5:2:3来划分
                BigDecimal firstRatio = goodDto.getRetailPrice().multiply(tbkRate).
                        divide(new BigDecimal(100)).multiply(new BigDecimal(0.5)).setScale(2, BigDecimal.ROUND_DOWN);
                BigDecimal secondRatio = goodDto.getRetailPrice().multiply(tbkRate).
                        divide(new BigDecimal(100)).multiply(new BigDecimal(0.5)).setScale(2, BigDecimal.ROUND_DOWN);
                goodDto.setCalModel("2G");
                goodDto.setFirstRatio(firstRatio);
                goodDto.setSecondRatio(secondRatio);
                goodDto.setThirdRatio(new BigDecimal(MapUtils.getInteger(rateDateMap, "max_commission_rate")));
                goodDto.setCommissionType(1); //淘宝客佣金
                goodDto.setCommissionValue(MapUtils.getInteger(rateDateMap, "max_commission_rate"));
            }
        }


        try {
            boolean isExists = goodsService.
                    checkExistBySrcGoodNameAndSn(goodDto.getSrcGoodName(), goodDto.getGoodsSn());
            if (isExists) {
                //已经存在该产品
                //更新库存
                MallGoods oldGoods = goodsService.selectOneBySrcGoodNameAndSn(goodDto);
                if (oldGoods != null) {
                    oldGoods.setDataBatchId(dataBatchId);
                    goodDto.setUpdateTime(LocalDateTime.now());
                    goodDto.setDetail(null); //详情图片不更新
                    goodDto.setName(null); //名称不更新
                    //更新商品批次号
                    goodsService.updateByNameAndGoodsSn(goodDto);
                }
            } else {
                this.create(goodsAllinone);

                coupon.setGoodsValue(new String[]{goodDto.getId()});
                couponService.add(coupon);

                LocalDateTime now = LocalDateTime.now();
                MallProductCoupon prodCoupon = new MallProductCoupon();
                prodCoupon.setGoodsId(goodDto.getId());
                prodCoupon.setCouponId(coupon.getId());
                prodCoupon.setAddTime(now);
                prodCoupon.setUpdateTime(now);
                prodCoupon.setDeleted(false);

                MallProductCouponMapper.insert(prodCoupon);
            }
        } catch (Exception e) {
            logger.error("", e);
        }
    }

    private String getCategoryId(String tbCateId) {
        MallCategoryExample example = new MallCategoryExample();
        MallCategoryExample.Criteria criteria = example.createCriteria();
        criteria.andChannelCodeEqualTo(properties.getChannelCode());
        criteria.andTbCateIdEqualTo(tbCateId);

        MallCategory cate = MallCategoryMapper.selectOneByExample(example);
        if (cate != null) {
            return cate.getId();
        } else {
            return "0";
        }
    }

    public Object create(GoodsAllinone goodsAllinone) {
        Object error = validate(goodsAllinone);
        if (error != null) {
            return error;
        }

        MallGoods goods = goodsAllinone.getGoods();
        MallGoodsAttribute[] attributes = goodsAllinone.getAttributes();
        MallGoodsSpecification[] specifications = goodsAllinone.getSpecifications();
        MallGoodsProduct[] products = goodsAllinone.getProducts();

        String name = goods.getName();

        // 商品基本信息表Mall_goods
        goodsService.add(goods);

        // 商品规格表Mall_goods_specification
        if (specifications != null) {
            List<MallGoodsSpecification> specList = new ArrayList<MallGoodsSpecification>();
            for (MallGoodsSpecification specification : specifications) {
                specification.setGoodsId(goods.getId());

                specList.add(specification);
            }
            if (specList.size() > 0) {
                adminAdSyncService.addGoodsSpecBatch(specList);
            }
        }

        // 商品参数表Mall_goods_attribute
        if (attributes != null && attributes.length > 0) {
            List<MallGoodsAttribute> attrList = new ArrayList<MallGoodsAttribute>();
            for (MallGoodsAttribute attribute : attributes) {
                attribute.setGoodsId(goods.getId());

                attrList.add(attribute);
//                attributeService.add(attribute);
            }
            if (attrList.size() > 0) {
                adminAdSyncService.addGoodsAttrBatch(attrList);
            }
        }

        // 商品货品表Mall_product
        if (products != null) {
            List<MallGoodsProduct> productList = new ArrayList<MallGoodsProduct>();
            for (MallGoodsProduct product : products) {
                product.setGoodsId(goods.getId());

                product.setAddTime(LocalDateTime.now());
                product.setUpdateTime(LocalDateTime.now());
                productList.add(product);
//                productService.add(product);
            }
            if (productList.size() > 0) {
                adminAdSyncService.addGoodsProductsBatch(productList);
            }
        }
        return ResponseUtil.ok();
    }


    private Object validate(GoodsAllinone goodsAllinone) {
        MallGoods goods = goodsAllinone.getGoods();
        String name = goods.getName();
        if (StringUtils.isEmpty(name)) {
            return ResponseUtil.badArgument();
        }
        String goodsSn = goods.getGoodsSn();
        if (StringUtils.isEmpty(goodsSn)) {
            return ResponseUtil.badArgument();
        }
        // 品牌商可以不设置，如果设置则需要验证品牌商存在
        /*String brandId = goods.getBrandId();
        if (brandId != null && brandId != 0) {
            if (brandService.findById(brandId) == null) {
                return ResponseUtil.badArgumentValue();
            }
        }*/
        // 分类可以不设置，如果设置则需要验证分类存在
        String categoryId = goods.getCategoryId();
        if (categoryId != null && !"0".equals(categoryId)) {
            if (categoryService.findById(categoryId) == null) {
                return ResponseUtil.badArgumentValue();
            }
        }

        /*
        MallGoodsAttribute[] attributes = goodsAllinone.getAttributes();
        for (MallGoodsAttribute attribute : attributes) {
            String attr = attribute.getAttribute();
            if (StringUtils.isEmpty(attr)) {
                return ResponseUtil.badArgument();
            }
            String value = attribute.getValue();
            if (StringUtils.isEmpty(value)) {
                return ResponseUtil.badArgument();
            }
        }*/

        MallGoodsSpecification[] specifications = goodsAllinone.getSpecifications();
        if (specifications != null) {
            for (MallGoodsSpecification specification : specifications) {
                String spec = specification.getSpecification();
                if (StringUtils.isEmpty(spec)) {
                    return ResponseUtil.badArgument();
                }
                String value = specification.getValue();
                if (StringUtils.isEmpty(value)) {
                    return ResponseUtil.badArgument();
                }
            }
        }

        MallGoodsProduct[] products = goodsAllinone.getProducts();
        if (products != null) {
            for (MallGoodsProduct product : products) {
                Integer number = product.getNumber();
                if (number == null || number < 0) {
                    return ResponseUtil.badArgument();
                }

                BigDecimal price = product.getPrice();
                if (price == null) {
                    return ResponseUtil.badArgument();
                }

                String[] productSpecifications = product.getSpecifications();
                if (productSpecifications.length == 0) {
                    return ResponseUtil.badArgument();
                }
            }
        }

        return null;
    }

    public String getTbkRate(String apiKey, String itemid, String pid, String activityid) {
        String result = null;
        String urlString = "http://v2.api.haodanku.com/ratesurl";
        InputStream is = null;
        String param = null;
        StringBuilder sbParams = new StringBuilder();
        sbParams.append("apikey");
        sbParams.append("=");
        sbParams.append(apiKey);
        sbParams.append("&");
        sbParams.append("itemid");
        sbParams.append("=");
        sbParams.append(itemid);
        sbParams.append("&");
        sbParams.append("pid");
        sbParams.append("=");
        sbParams.append(pid);
        sbParams.append("&");
        sbParams.append("activityid");
        sbParams.append("=");
        sbParams.append(activityid);
        sbParams.append("&");

//    	String retVal = HttpUtils.get(urlString+"?"+sbParams.toString() , null, null);
//		System.out.println("retVal:"+retVal);
        try {
            URL url = new URL(urlString);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            //post请求需要设置DoOutput为true
            urlConnection.setDoOutput(true);
            urlConnection.setRequestMethod("POST");
            //设置参数
            param = sbParams.toString();
            urlConnection.getOutputStream().write(param.getBytes());
            urlConnection.getOutputStream().flush();
            urlConnection.setConnectTimeout(5 * 1000);
            urlConnection.setReadTimeout(5 * 1000);
            //连接服务器
            urlConnection.connect();
            StringBuilder stringBuilder = new StringBuilder();
            if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                is = urlConnection.getInputStream();
                int len = 0;
                byte[] buffer = new byte[1024];
                while ((len = is.read(buffer)) != -1) {
                    stringBuilder.append(new String(buffer, 0, len));
                }
                result = stringBuilder.toString();
            }
        } catch (MalformedURLException e) {
            logger.error("", e);
        } catch (IOException e) {
            logger.error("", e);
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    logger.error("", e);
                }
            }
        }
        return result;
    }

}
