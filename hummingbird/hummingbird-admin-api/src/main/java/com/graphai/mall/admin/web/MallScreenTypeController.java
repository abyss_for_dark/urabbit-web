package com.graphai.mall.admin.web;

import cn.hutool.core.util.StrUtil;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.lang.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.graphai.commons.util.servlet.ServletUtils;
import com.graphai.mall.db.constant.screenType.MallScreenTypeEnum;
import com.graphai.mall.db.domain.MallAdmin;
import org.springframework.beans.factory.annotation.Autowired;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.framework.generator.controller.IBaseController;

import com.graphai.mall.admin.domain.MallScreenType;
import com.graphai.mall.admin.service.IMallScreenTypeService;

import java.time.LocalDateTime;
import java.util.*;

import static org.apache.shiro.SecurityUtils.getSubject;


/**
 * <p>
 * 筛选类型表 前端控制器
 * </p>
 *
 * @author biteam
 * @since 2021-07-16
 */
@RestController
@RequestMapping("/admin/screen/screen")
public class MallScreenTypeController extends IBaseController<MallScreenType,  String >  {

	@Autowired
    private IMallScreenTypeService serviceImpl;

    @RequiresPermissions("admin:screen:screen:single")
    @RequiresPermissionsDesc(menu = {"单独查询"}, button = "单独查询")
	@GetMapping("/single/{id}")
    public Object getEntityById(@PathVariable  String  id) throws Exception{
        MallScreenType entityVName = serviceImpl.getById(id);
        return ResponseUtil.ok(entityVName);
    }

    @RequiresPermissions("admin:screen:screen:materialTypeList")
    @RequiresPermissionsDesc(menu = {"单独查询"}, button = "单独查询")
    @GetMapping("/materialTypeList")
    public Object getMaterialTypeByCategoryId(String  CategoryId) throws Exception{
        List<MallScreenType> mallScreenTypes = serviceImpl.selectbyCategoryId(CategoryId);
        List<MallScreenType> materialType = new ArrayList();
        List<MallScreenType> sizeType = new ArrayList();
        List<MallScreenType> craftType = new ArrayList();
        List<MallScreenType> priceType = new ArrayList();
        List<MallScreenType> placeType = new ArrayList();
        Map<String,Object> map = new HashMap();
        // 若CategoryId为空,设置默认筛选
        if (CategoryId == null || CategoryId.equals("") || CategoryId.equals("null")) {
            CategoryId = "5606235792321712128";
        }
        for (MallScreenType mallScreenType : mallScreenTypes) {
            if (mallScreenType.getCategoryId().equals(CategoryId) && mallScreenType.getDisabled().equals(MallScreenTypeEnum.Disabled_0.strCode2Int())) {
                if (mallScreenType.getType().equals(MallScreenTypeEnum.TYPE_0.strCode2Int()) && mallScreenType.getDisabled().equals(MallScreenTypeEnum.Disabled_0.strCode2Int())) {
                    materialType.add(mallScreenType);
                }
                if (mallScreenType.getType().equals(MallScreenTypeEnum.TYPE_1.strCode2Int()) && mallScreenType.getDisabled().equals(MallScreenTypeEnum.Disabled_0.strCode2Int())) {
                    sizeType.add(mallScreenType);
                }
                if (mallScreenType.getType().equals(MallScreenTypeEnum.TYPE_2.strCode2Int()) && mallScreenType.getDisabled().equals(MallScreenTypeEnum.Disabled_0.strCode2Int())) {
                    craftType.add(mallScreenType);
                }
                if (mallScreenType.getType().equals(MallScreenTypeEnum.TYPE_3.strCode2Int()) && mallScreenType.getDisabled().equals(MallScreenTypeEnum.Disabled_0.strCode2Int())) {
                    priceType.add(mallScreenType);
                }
                if (mallScreenType.getType().equals(MallScreenTypeEnum.TYPE_4.strCode2Int()) && mallScreenType.getDisabled().equals(MallScreenTypeEnum.Disabled_0.strCode2Int())) {
                    placeType.add(mallScreenType);
                }
            }
        }
        map.put("materialType",materialType);
        map.put("sizeType",sizeType);
        map.put("craftType",craftType);
        map.put("priceType",priceType);
        map.put("placeType",placeType);
        return ResponseUtil.ok(map);
    }

//    @RequiresPermissions("admin:screen:screen:goods:materialTypeList")
    @RequiresPermissionsDesc(menu = {"添加商品查询"}, button = "添加商品查询")
    @PostMapping("/goods/materialTypeList")
    public Object getMaterialTypeByCategoryIdForGoods(@RequestBody List<String> categoryIds) throws Exception{
        List<MallScreenType> mallScreenTypes = serviceImpl.selectbyCategoryIds(categoryIds);
        Set<String> materialType = new HashSet<>();
        Set<String> sizeType = new HashSet();
        Set<String> craftType = new HashSet();
        Set<String> priceType = new HashSet();
        Set<String> placeType = new HashSet();
        Map<String,Object> map = new HashMap();

        for (MallScreenType mallScreenType : mallScreenTypes) {
                if (mallScreenType.getType().equals(MallScreenTypeEnum.TYPE_0.strCode2Int())) {
                    if (StrUtil.isNotEmpty(mallScreenType.getValue2())){
                        materialType.add(mallScreenType.getValue() + "-" + mallScreenType.getValue2());
                    }else{
                        materialType.add(mallScreenType.getValue());
                    }
                }
                if (mallScreenType.getType().equals(MallScreenTypeEnum.TYPE_1.strCode2Int())) {
                    if (StrUtil.isNotEmpty(mallScreenType.getValue2()) && StrUtil.isNotEmpty(mallScreenType.getValue())){
                        sizeType.add(mallScreenType.getValue() + "-" + mallScreenType.getValue2());
                    }else if(StrUtil.isEmpty(mallScreenType.getValue2()) && StrUtil.isNotEmpty(mallScreenType.getValue())){
                        sizeType.add(mallScreenType.getValue() + "以上");
                    }else if(StrUtil.isEmpty(mallScreenType.getValue()) && StrUtil.isNotEmpty(mallScreenType.getValue2())){
                        sizeType.add(mallScreenType.getValue2() + "以下");
                    }
                }
                if (mallScreenType.getType().equals(MallScreenTypeEnum.TYPE_2.strCode2Int())) {
                    if (StrUtil.isNotEmpty(mallScreenType.getValue2())){
                        craftType.add(mallScreenType.getValue() + "-" + mallScreenType.getValue2());
                    }else{
                        craftType.add(mallScreenType.getValue());
                    }
                }
                if (mallScreenType.getType().equals(MallScreenTypeEnum.TYPE_3.strCode2Int())) {
                    if (StrUtil.isNotEmpty(mallScreenType.getValue2()) && StrUtil.isNotEmpty(mallScreenType.getValue())){
                        priceType.add(mallScreenType.getValue() + "-" + mallScreenType.getValue2());
                    }else if(StrUtil.isEmpty(mallScreenType.getValue2()) && StrUtil.isNotEmpty(mallScreenType.getValue())){
                        priceType.add(mallScreenType.getValue() + "以上");
                    }else if(StrUtil.isEmpty(mallScreenType.getValue()) && StrUtil.isNotEmpty(mallScreenType.getValue2())){
                        priceType.add(mallScreenType.getValue2() + "以下");
                    }
                }
                if (mallScreenType.getType().equals(MallScreenTypeEnum.TYPE_4.strCode2Int())) {
                    if (StrUtil.isNotEmpty(mallScreenType.getValue2())){
                        placeType.add(mallScreenType.getValue() + "-" + mallScreenType.getValue2());
                    }else{
                        placeType.add(mallScreenType.getValue());
                    }
                }

        }
        map.put("materialType",materialType);
        map.put("sizeType",sizeType);
        map.put("craftType",craftType);
        map.put("priceType",priceType);
        map.put("placeType",placeType);
        return ResponseUtil.ok(map);
    }


    @RequiresPermissions("admin:screen:screen:addOrUpdate")
    @RequiresPermissionsDesc(menu = {"新增"}, button = "新增")
    @PostMapping("/addOrUpdate")
    public Object addOrUpdate(MallScreenType mallScreenType) throws Exception{
        if(serviceImpl.saveOrUpdate(mallScreenType)){
            return ResponseUtil.ok();
        }else{
            return ResponseUtil.fail();
        }
    }

	@RequiresPermissions("admin:screen:screen:melist")
	@RequiresPermissionsDesc(menu = {"筛选类型表查询"}, button = "查询")
    @GetMapping("/melist")
    public Object getMeList() throws Exception {
        IPage<MallScreenType> ipage = serviceImpl.page(getPlusPage(),listQueryWrapper());
        return ResponseUtil.ok(ipage);
    }

 	private QueryWrapper<MallScreenType> listQueryWrapper() {
 		QueryWrapper<MallScreenType> meq = new QueryWrapper<>();
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("userId"))) {
		      meq.eq("user_id", ServletUtils.getParameter("userId"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("categoryId"))) {
		      meq.eq("category_id", ServletUtils.getParameter("categoryId"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("pid"))) {
		      meq.eq("pid", ServletUtils.getParameter("pid"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("type"))) {
		      meq.eq("type", ServletUtils.getParameter("type"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("value"))) {
		      meq.eq("value", ServletUtils.getParameter("value"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("disabled"))) {
		      meq.eq("disabled", ServletUtils.getParameter("disabled"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("merchantId"))) {
		      meq.eq("merchant_id", ServletUtils.getParameter("merchantId"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("addTime"))) {
		      meq.eq("add_time", ServletUtils.getLocalDateTime("addTime"));
		}

        return meq;
    }
 	@RequiresPermissions("admin:screen:screen:save")
 	@RequiresPermissionsDesc(menu = {"筛选类型表新增"}, button = "新增")
    @PostMapping("/save")
    public Object save(@ModelAttribute MallScreenType mallScreenType) throws Exception {
        MallAdmin adminUser = (MallAdmin) getSubject().getPrincipal();
        mallScreenType.setUserId(adminUser.getUserId());
        mallScreenType.setAddTime(LocalDateTime.now());
        if(serviceImpl.save(mallScreenType)){
            return ResponseUtil.ok();
        }else{
            return ResponseUtil.fail();
        }
    }

	@RequiresPermissions("admin:screen:screen:modify")
	@RequiresPermissionsDesc(menu = {"筛选类型表修改"}, button = "修改")
    @PutMapping("/modify")
    public Object modify(@ModelAttribute  MallScreenType mallScreenType){
        serviceImpl.updateById(mallScreenType);
        return  ResponseUtil.ok();
    }

    @RequiresPermissions("admin:screen:screen:del")
    @RequiresPermissionsDesc(menu = {"筛选类型表删除"}, button = "删除")
    @DeleteMapping("/del/{id}")
    public Object deleteById(@PathVariable  String  id){
        serviceImpl.removeById(id);
        return  ResponseUtil.ok();
    }

}

