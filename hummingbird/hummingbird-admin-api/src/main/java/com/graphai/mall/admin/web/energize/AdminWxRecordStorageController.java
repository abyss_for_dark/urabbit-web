package com.graphai.mall.admin.web.energize;

import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.mall.db.service.funeng.MallEnergizeService;
import com.graphai.mall.db.vo.MallWxRecordStorageVo;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 公众号文件管理
 *
 * @author LaoGF
 * @since 2020-08-05
 */
@RestController
@RequestMapping("/admin/wxRecordStorage")
@Validated
public class AdminWxRecordStorageController {

    @Resource
    private MallEnergizeService mallEnergizeService;


    /**
     * 列表
     */
//    @RequiresPermissions("admin:wxRecordStorage:list")
//    @RequiresPermissionsDesc(menu = {"广告管理", "公众号文件管理"}, button = "查询")
    @GetMapping("/list")
    public Object list(@RequestParam(required = false) String showTitle,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit) {
        List<MallWxRecordStorageVo> list = mallEnergizeService.selectStorageList(showTitle, page, limit);
        long total = PageInfo.of(list).getTotal();
        Map<String, Object> data = new HashMap<>(16);
        data.put("total", total);
        data.put("items", list);
        return ResponseUtil.ok(data);
    }


}
