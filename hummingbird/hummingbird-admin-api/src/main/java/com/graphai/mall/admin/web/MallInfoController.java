package com.graphai.mall.admin.web;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.system.service.MallRoleService;
import com.graphai.mall.admin.dao.MallMerchantMapper;
import com.graphai.mall.admin.domain.MallMerchant;
import com.graphai.mall.admin.domain.MallUserMerchant;
import com.graphai.mall.admin.service.MallInfoService;
import com.graphai.mall.admin.service.impl.MallMerchantServiceImpl;
import com.graphai.mall.admin.service.impl.MallUserMerchantServiceImpl;
import com.graphai.mall.db.dao.MallGoodsMapper;
import com.graphai.mall.db.dao.MallSystemNoticeMapper;
import com.graphai.mall.db.domain.*;
import com.graphai.mall.db.service.order.MallAftersaleService;
import com.graphai.mall.db.service.user.behavior.MallCommentService;
import com.graphai.mall.db.service.user.behavior.MallFootprintService;
import com.graphai.mall.db.vo.MallZYOrderVo;
import com.graphai.open.mallorder.service.IMallOrderZYService;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.apache.shiro.SecurityUtils.getSubject;

@RestController
@RequestMapping("/admin/admin/mallInfo")
public class MallInfoController {

    @Autowired
    private MallRoleService mallRoleService;
    @Autowired
    private MallUserMerchantServiceImpl mallUserMerchantService;
    @Autowired
    private MallInfoService infoService;
    @Autowired
    private MallMerchantMapper mallMerchantMapper;
    @Autowired
    private MallSystemNoticeMapper mallSystemNoticeMapper;
    @Autowired
    private MallAftersaleService aftersaleService;
    @Autowired
    private MallFootprintService footprintService;
    @Autowired
    private IMallOrderZYService orderZYService;
    @Autowired
    private MallCommentService mallCommentService;


    @RequestMapping("/getAllInfo")
    public Object getAllInfo() {
        MallAdmin adminUser = (MallAdmin) getSubject().getPrincipal();
        Set<MallRole> mallRoleSet = mallRoleService.findByIds(adminUser.getRoleIds());
        MallUserMerchant mallUserMerchant = null;
        List<Map<String, Object>> orderList = null;
        List<MallFootprint> footprintList = null;
        long total = 0;
        for (MallRole mallRole : mallRoleSet) {
            if ("merchant".equals(mallRole.getRoleKey())) {
                QueryWrapper<MallUserMerchant> wrapper=new QueryWrapper<>();
                wrapper.eq("user_id",adminUser.getUserId());
                mallUserMerchant = mallUserMerchantService.getOne(wrapper);
                orderList = infoService.queryOrderByMerchantId(mallUserMerchant.getMerchantId());

                footprintList = footprintService.findByFactoryIdAndName(mallUserMerchant.getMerchantId(),adminUser.getUserId(),null, null, 0, 10);
                total = PageInfo.of(footprintList).getTotal();
            }
            if ("plant".equals(mallRole.getRoleKey())) {
                QueryWrapper<MallUserMerchant> wrapper=new QueryWrapper<>();
                wrapper.eq("user_id",adminUser.getUserId());
                mallUserMerchant = mallUserMerchantService.getOne(wrapper);
                orderList = infoService.queryOrderByfactoryMerchantId(mallUserMerchant.getMerchantId());

                footprintList = footprintService.findByMerchantIdAndName(mallUserMerchant.getMerchantId(),adminUser.getUserId(),null, null, 0, 10);
                total = PageInfo.of(footprintList).getTotal();
            }
        }

        Map map = new HashMap();

        MallMerchant mallMerchant = mallMerchantMapper.selectById(mallUserMerchant.getMerchantId());
        mallMerchant.setBrowseNum((int) total);

        List<Map<String,Object>> scoreList = infoService.getScoreByMerchantId(mallUserMerchant.getMerchantId());
        List<MallSystemNotice> noticeList = mallSystemNoticeMapper.selectByExample(null);
        List<Integer> goodsList = infoService.getGoodsNumByMerchantId(mallUserMerchant.getMerchantId());

        List<MallAftersale> mallAftersales = aftersaleService.findByMerchantId(mallUserMerchant.getMerchantId());
        List<MallAftersale> mallAftersales1 = aftersaleService.findByMerchantIdStatus(mallUserMerchant.getMerchantId());
        Integer comment = mallCommentService.queryByMerchantId(mallUserMerchant.getMerchantId());
        int after1 = 0;
        int after2 = 0;
        int after3 = 0;
        for(MallAftersale item : mallAftersales){
            switch (item.getType()){
                case "1":
                    after1++;
                    break;
                case "2":
                    after2++;
                    break;
                case "3":
                    after3++;
                    break;
            }
        }
//
//        MallZYOrderVo total = orderZYService.getZYOrderListTotal(null, null, null, null, mallUserMerchant.getMerchantId(), null, null, null,null);
//        map.put("total", total.getTotal());
//        map.put("turnover",total.getTurnover());



        map.put("mallMerchant",mallMerchant);
        map.put("orderList",orderList);
        map.put("scoreList",scoreList);
        map.put("notices",noticeList);
        map.put("goodsList",goodsList);

        map.put("comment",comment);
        map.put("after0",mallAftersales1.size());//售后待处理
        map.put("after1",after1);//退款
        map.put("after2",after2);//退货
        map.put("after3",after3);//换货



        return ResponseUtil.ok(map);
    }

}
