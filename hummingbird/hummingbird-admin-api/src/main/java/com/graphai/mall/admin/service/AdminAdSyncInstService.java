package com.graphai.mall.admin.service;

import com.alibaba.druid.support.json.JSONUtils;
import com.graphai.commons.util.AppIdAlgorithm;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.httprequest.HttpUtils;
import com.graphai.commons.util.image.ImageUtils;
import com.graphai.framework.storage.StorageService;
import com.graphai.mall.admin.dto.GoodsAllinone;
import com.graphai.mall.core.qcode.QCodeService;
import com.graphai.mall.db.config.WpcapiProperties;
import com.graphai.mall.db.dao.MallGoodsCodeSeqMapper;
import com.graphai.mall.db.domain.*;
import com.graphai.mall.db.service.*;
import com.graphai.mall.db.service.advertisement.AdminAdSyncService;
import com.graphai.mall.db.service.advertisement.MallAdBrandService;
import com.graphai.mall.db.service.advertisement.MallAdService;
import com.graphai.mall.db.service.advertisement.MallBrandService;
import com.graphai.mall.db.service.category.MallCategoryService;
import com.graphai.mall.db.service.goods.MallGoodsAttributeService;
import com.graphai.mall.db.service.goods.MallGoodsProductService;
import com.graphai.mall.db.service.goods.MallGoodsService;
import com.graphai.mall.db.service.goods.MallGoodsSpecificationService;
import com.graphai.mall.db.util.WpcApiUtil;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AdminAdSyncInstService {
    private final Log logger = LogFactory.getLog(AdminAdSyncInstService.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Autowired
    private MallAdService adService;

    @Autowired
    private AdminAdSyncService adminAdSyncService;

    @Autowired
    private MallGoodsService goodsService;
    @Autowired
    private MallGoodsSpecificationService specificationService;
    @Autowired
    private MallGoodsAttributeService attributeService;
    @Autowired
    private MallGoodsProductService productService;
    @Autowired
    private MallCategoryService categoryService;
    @Autowired
    private MallBrandService brandService;

    @Autowired
    private QCodeService qCodeService;

    @Autowired
    private MallAdBrandService adBrandService;

    @Autowired
    private WpcapiProperties properties;

    public void clearData(String dataBatchId, String channelCode) {
        //所有数据执行完毕,
        //如果最新的接口查不出来的数据
        //从品牌活动表做逻辑删除，所有商品下架。
        adService.updateByDataBatchId(dataBatchId, channelCode);
        goodsService.updateByDataBatchId(dataBatchId, channelCode);
    }

    public void syscBrand(int index, int count, String dataBatchId) {

//    	if(true){
//    		return; //pxhtest
//    	}
        // 1.appKey放到配置文件中
        // 2.批次号找个地方存放
        // 3.查找原本是否存在某个品牌档期,如果有则更新,没有则新增 ok

//    	LOG.info("updateOrderCancelTask 定时任务执行时间：" + dateFormat.format(new Date()));
        Map<String, String> paramsMap = new HashMap<String, String>();

//		String apiSecret = "1bba7ee6df5848949d3cc395e133fb98";
//		String apiKey = "4ffbc0bb73544e07ab5e104ccab3458d";
//		String appId = "190";

        String nonceStr = WpcApiUtil.getNonceStr();
        String timeStamp = System.currentTimeMillis() / 1000 + "";

        paramsMap.put("apiKey", properties.getApiKey());
        paramsMap.put("appId", properties.getAppId());

        paramsMap.put("nonceStr", nonceStr);
        paramsMap.put("timeStamp", timeStamp);
        paramsMap.put("userNumber", properties.getUserNumber());

        String sign = WpcApiUtil.getWpcSignStr(paramsMap, properties.getAppSecret());
        paramsMap.put("apiSign", sign.toLowerCase());

//    	System.out.println("paramsMap:"+paramsMap);

        String retVal = HttpUtils.get("http://wpc-api.vip.com/openapi/brand/list/v1", paramsMap, null);
//    	System.out.println("retVal:"+retVal);
        if (index == 0) {
            logger.info("retVal:" + retVal);
        }

        if (retVal != null) {
            Map<String, Object> retMap = (Map<String, Object>) JSONUtils.parse(retVal);
//    		System.out.println(retMap);
            if (retMap != null && 200 == MapUtils.getIntValue(retMap, "code", 0)) {
                List<Map<String, Object>> adList = (List) retMap.get("data");
                if (adList != null) {
                    int adIndex = 0;
                    for (Map<String, Object> adMap : adList) {
                        String srcAdId = MapUtils.getString(adMap, "adId");
//        				if(Integer.valueOf(srcAdId)%count!=index){
                        adIndex++;
                        if (adIndex % count != index) {
                            continue;
                        }
//        				if(!"5777".equals(srcAdId)){ //pxhtest
//        					continue;
//        				}
                        if ("5793".equals(srcAdId)) {
                            //部分品牌不同步
                            continue;
                        }
                        logger.info("sync adId:" + srcAdId);

                        MallAd ad = new MallAd();
                        ad.setName(MapUtils.getString(adMap, "brandName"));
                        ad.setUrl(MapUtils.getString(adMap, "brandImage"));
                        ad.setPosition((byte) 1);
                        ad.setContent(MapUtils.getString(adMap, "brandDesc"));
//            	    	sellTimeFrom
//                        DateTimeFormatter format = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");  

                        try {
                            ad.setStartTime(getDateStrToLocalDateTime(MapUtils.getString(adMap, "sellTimeFrom")));
                            ad.setEndTime(getDateStrToLocalDateTime(MapUtils.getString(adMap, "sellTimeTo")));
                        } catch (Exception e) {
                            continue;
                        }

                        ad.setEnabled(true);

                        boolean addPriceFlag = false;
                        int addPriceRate = 0;
                        int commissionRatio = MapUtils.getIntValue(adMap, "commissionRatio");
                        if (commissionRatio < 20) {
                            logger.info("sync adId: " + srcAdId + ",commissionRatio:" + commissionRatio);
                            //
                            addPriceFlag = true;
                            addPriceRate = commissionRatio;
                        }

                        ad.setChannelCode(properties.getChannelCode());
                        ad.setSrcAdId(srcAdId);
                        ad.setDataBatchId(dataBatchId);
                        ad.setDeleted(false);

                        MallAd oldAd = adminAdSyncService.findByChannelCodeAndSrcAdId(ad);
//            	    	oldAd = null; //pxhtest
                        if (oldAd != null) {
                            ad.setId(oldAd.getId());
                            ad.setDataBatchId(dataBatchId);
                            ad.setContent(null);
                            ad.setPosition((byte) 2);
                            adService.updateById(ad);
                        } else {
                            ad.setPosition((byte) 2);
                            ad.setSort(59);
                            adService.add(ad);
                        }
                        this.syscGoods(ad.getId(), srcAdId, dataBatchId, MapUtils.getString(adMap, "brandDesc"),
                                addPriceFlag, addPriceRate);

                    }
                }

            }
        }
    }

    public static LocalDateTime getDateStrToLocalDateTime(String dateStr) throws Exception {
        java.util.Date date = dateFormat.parse(dateStr);
        Instant instant = date.toInstant();
        ZoneId zone = ZoneId.systemDefault();
        LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, zone);
        return localDateTime;
    }

    public void syscGoods(String newAdId, String oldAdId, String dataBatchId, String brandDesc,
                          boolean addPriceFlag, int addPriceRate) {

        syscGoods(newAdId, oldAdId, 1, 20, dataBatchId, brandDesc, addPriceFlag, addPriceRate);
    }

    public void syscGoods(String newAdId, String adId, int page, int pageSize, String dataBatchId,
                          String brandDesc, boolean addPriceFlag, int addPriceRate) {
        int totalNum = 0;
        Map<String, String> paramsMap = new HashMap<String, String>();
        String nonceStr = WpcApiUtil.getNonceStr();
        String timeStamp = System.currentTimeMillis() / 1000 + "";

        paramsMap.put("apiKey", properties.getApiKey());
        paramsMap.put("appId", properties.getAppId());
        paramsMap.put("nonceStr", nonceStr);
        paramsMap.put("timeStamp", timeStamp);

        paramsMap.put("adId", adId);

        paramsMap.put("page", page + "");
        paramsMap.put("pageSize", pageSize + "");

        String sign = WpcApiUtil.getWpcSignStr(paramsMap, properties.getAppSecret());
        paramsMap.put("apiSign", sign.toLowerCase());

//    	logger.info("paramsMap:"+paramsMap);

        String retVal = HttpUtils.get("http://wpc-api.vip.com/openapi/good/list/v1", paramsMap, null);
//    	System.out.println("retVal:"+retVal);

        if (retVal != null) {
            Map<String, Object> retMap = (Map<String, Object>) JSONUtils.parse(retVal);
//    		System.out.println(retMap);
            if (retMap != null && 200 == MapUtils.getIntValue(retMap, "code", 0)
                    && retMap.get("data") != null) {
                Map<String, Object> dataMap = (Map<String, Object>) retMap.get("data");
                totalNum = MapUtils.getIntValue(dataMap, "totalNum", 0);

                List<Map<String, Object>> goods = (List) dataMap.get("goods");
                if (goods != null) {
                    for (Map<String, Object> goodMap : goods) {

//        				if(!"085201".equals(MapUtils.getString(goodMap, "goodId"))){ //pxhtest
//        					continue;
//        				}
//    					System.out.println("goodMap:"+goodMap);

                        MallGoods goodDto = new MallGoods();

                        //同步品牌信息
                        String brandCnName = MapUtils.getString(goodMap, "brandCnName");
                        String brandEnName = MapUtils.getString(goodMap, "brandEnName");
                        String brandLogo = MapUtils.getString(goodMap, "logo");
                        if (StringUtils.isEmpty(brandCnName)) {
                            brandCnName = brandEnName;
                        }
                        MallBrand brand = new MallBrand();
                        brand.setName(brandCnName);
                        brand.setPicUrl(brandLogo);
                        brand.setBrandEnName(brandEnName);
                        brand.setDataBatchId(dataBatchId);
                        brand.setChannelCode(properties.getChannelCode());
                        brand.setDesc(brandDesc);

                        MallBrand oldBrand = adminAdSyncService.findByChannelCodeAndName(brand);
                        if (oldBrand != null) {
                            brand.setId(oldBrand.getId());
                            brandService.updateById(brand);
                        } else {
                            brandService.add(brand);
                        }

                        //同步商品信息
                        MallAdBrand adBrand = new MallAdBrand();
                        adBrand.setAdId(newAdId);
                        adBrand.setBrandId(brand.getId());
                        adBrand.setDataBatchId(dataBatchId);
                        adBrand.setChannelCode(properties.getChannelCode());
                        adBrand.setSrcAdId(adId);
                        MallAdBrand oldAdBrand = adBrandService.selectByAdIdAndBrandId(adBrand);
                        if (oldAdBrand != null) {
                            if (dataBatchId != oldAdBrand.getDataBatchId()) {
                                oldAdBrand.setDataBatchId(dataBatchId);
                                adBrandService.updateById(oldAdBrand);
                            }
                        } else {
                            adBrandService.add(adBrand);
                        }

                        String goodImage = MapUtils.getString(goodMap, "goodImage");
                        //同步sku信息
                        List<MallGoodsSpecification> specList = new ArrayList<MallGoodsSpecification>();

                        String color = MapUtils.getString(goodMap, "color");
                        if (!StringUtils.isEmpty(color)) {
                            color = color.replace("颜色：", "");
                            MallGoodsSpecification specification1 = new MallGoodsSpecification();
                            specification1.setSpecification("颜色");
                            specification1.setValue(color);
                            if (StringUtils.isEmpty(goodImage)) {
                                specification1.setPicUrl("http://");
                            } else {
                                specification1.setPicUrl(goodImage);
                            }
                            specification1.setDeleted(false);
                            specList.add(specification1);
                        }

                        goodDto.setIsOnSale(true);

                        List<MallGoodsProduct> productList = new ArrayList<MallGoodsProduct>();
                        if (goodMap.get("sizes") != null) {
                            List<Map<String, Object>> sizes = (List<Map<String, Object>>) goodMap.get("sizes");
                            if (sizes.size() > 0) {
                                boolean isExistsStock = false; //商品是否有库存

                                for (Map<String, Object> sizeMap : sizes) {
//                					{
//                						"sizeName": "28",
//                						"vipshopPrice": "94",
//                						"marketPrice": "699",
//                						"stock": true
//                						}

                                    String sizeName = MapUtils.getString(sizeMap, "sizeName");
                                    //marketPrice 没有写入到SKU表中
                                    MallGoodsSpecification specification2 = new MallGoodsSpecification();
                                    specification2.setSpecification("尺寸");
                                    specification2.setValue(sizeName);
                                    if (StringUtils.isEmpty(goodImage)) {
                                        specification2.setPicUrl("http://");
                                    } else {
                                        specification2.setPicUrl(goodImage);
                                    }
                                    specification2.setDeleted(false);
                                    specList.add(specification2);

                                    BigDecimal vipshopPrice = new BigDecimal(MapUtils.getString(sizeMap, "vipshopPrice"));
                                    if (addPriceFlag) {
                                        BigDecimal addPriceRateBD =
                                                new BigDecimal(100).subtract(new BigDecimal(addPriceRate))
                                                        .divide(new BigDecimal(80), 2, BigDecimal.ROUND_CEILING);
                                        vipshopPrice = vipshopPrice.multiply(addPriceRateBD).
                                                setScale(0, BigDecimal.ROUND_CEILING);
                                    }
                                    MallGoodsProduct product = new MallGoodsProduct();
                                    product.setPrice(vipshopPrice);

                                    if (MapUtils.getBooleanValue(sizeMap, "stock")) {
                                        //是否有库存
                                        product.setNumber(100);
                                        isExistsStock = true;

                                        // 需取到最大或最小的一个价格
                                        goodDto.setCounterPrice(new BigDecimal(MapUtils.getString(sizeMap, "marketPrice")));
                                        goodDto.setRetailPrice(vipshopPrice);
                                    } else {
                                        product.setNumber(0);
                                    }
                                    product.setSrcSizeId(MapUtils.getString(sizeMap, "sizeId"));
                                    product.setDataBatchId(dataBatchId);
                                    product.setChannelCode(properties.getChannelCode());
                                    product.setSrcGoodId(MapUtils.getString(goodMap, "goodId"));

                                    if (!StringUtils.isEmpty(color)) {
                                        product.setSpecifications(new String[]{color, sizeName});
                                    } else {
                                        product.setSpecifications(new String[]{sizeName});
                                    }
                                    product.setDeleted(false);
                                    productList.add(product);

                                }

                                if (!isExistsStock) {
                                    //商品没有库存,直接下架
                                    goodDto.setIsOnSale(false);

                                    // 如果没有库存,取第一个价格
                                    goodDto.setCounterPrice(new BigDecimal(MapUtils.getString(sizes.get(0), "marketPrice")));
                                    goodDto.setRetailPrice(new BigDecimal(MapUtils.getString(sizes.get(0), "vipshopPrice")));
                                }
                            } else {
                                continue;
                            }
                        } else {
                            continue;
                        }

                        // brandCnName brandEnName logo

                        //color

                        // dcImageURLs
                        if (goodMap.get("dcImageURLs") != null) {
                            List<String> dcImageURLs = (List<String>) goodMap.get("dcImageURLs");
//        					StringBuffer detail = new StringBuffer();
//        					for(String detailImg:dcImageURLs){
//        						detail.append(
//        								"<p><img src=\""+detailImg+"\" _src=\""+detailImg+"\" style=\"\"/></p>");
//        					}
//        					goodDto.setDetail(detail.toString());
                            goodDto.setDetail(JSONUtils.toJSONString(dcImageURLs));
                        }

                        goodDto.setBrandId(brand.getId());
                        goodDto.setGallery(new String[]{MapUtils.getString(goodMap, "goodImage")});
                        goodDto.setSrcGoodId(MapUtils.getString(goodMap, "goodId"));

                        // goodId
                        goodDto.setChannelCode(properties.getChannelCode());
                        goodDto.setDataBatchId(dataBatchId);
                        goodDto.setSrcAdId(adId);
                        goodDto.setSrcGoodId(MapUtils.getString(goodMap, "goodId"));

                        //goodImage goodName sn
                        String goodName = MapUtils.getString(goodMap, "goodName");
                        goodDto.setSrcGoodName(goodName);

                        goodDto.setGoodsSn(MapUtils.getString(goodMap, "sn"));
                        goodDto.setPicUrl(goodImage);

                        goodDto.setBrief(goodName);


                        GoodsAllinone goodsAllinone = new GoodsAllinone();
                        goodsAllinone.setGoods(goodDto);
                        goodsAllinone.setProducts(productList.toArray(new MallGoodsProduct[]{}));
                        goodsAllinone.setSpecifications(specList.toArray(new MallGoodsSpecification[]{}));

                        String material = MapUtils.getString(goodMap, "material");
                        MallGoodsAttribute goodAttribute = new MallGoodsAttribute();
                        goodAttribute.setAttribute("材质");
                        goodAttribute.setDeleted(false);
                        if (StringUtils.isEmpty(material)) {
                            goodAttribute.setValue("");
                        } else {
                            goodAttribute.setValue(material);

                            MallGoodsAttribute[] attributes = new MallGoodsAttribute[1];
                            attributes[0] = goodAttribute;

                            goodsAllinone.setAttributes(attributes);
                        }

                        try {
                            boolean isExists = goodsService.
                                    checkExistBySrcGoodNameAndSn(goodDto.getSrcGoodName(), goodDto.getGoodsSn());
                            if (isExists) {
                                //已经存在该产品
                                //更新库存
                                MallGoods oldGoods = goodsService.selectOneBySrcGoodNameAndSn(goodDto);
                                if (oldGoods != null) {
                                    oldGoods.setDataBatchId(dataBatchId);

                                    goodDto.setUpdateTime(LocalDateTime.now());

                                    if (productList != null && productList.size() > 0) {
                                        int skuIndex = 0;
                                        for (MallGoodsProduct prod : productList) {
                                            if (prod.getNumber() > 0 ||
                                                    oldGoods.getRetailPrice().compareTo(goodDto.getRetailPrice()) != 0) {
                                                //价格不相等的时候也更新
                                                prod.setGoodsId(oldGoods.getId());

                                                MallGoodsProduct oldProd = productService.selectOneByExample(prod);
                                                if (oldProd == null) {
                                                    if ((skuIndex++) == 0) {
                                                        //第一次发现sku有变化的时候,更新表数据
                                                        productService.deleteByGoodsId(oldGoods.getId(), dataBatchId);
                                                    }
                                                    //如果原先没有对应sku,则新增
                                                    prod.setUpdateTime(LocalDateTime.now());
                                                    productService.add(prod);
                                                }
                                                if (oldProd != null) {
                                                    if (oldProd.getNumber() > 0 && prod.getNumber() == 0) {
                                                        //原来有库存,现在没库存,更新库存数据
                                                        prod.setGoodsId(oldProd.getGoodsId());
                                                        prod.setUpdateTime(LocalDateTime.now());
                                                        prod.setDataBatchId(dataBatchId);
                                                        productService.updateByExampleSelective(prod);
                                                    } else if (oldProd.getNumber() == 0 && prod.getNumber() > 0) {
                                                        //原来没库存,现在有库存,也更新库存数据
                                                        prod.setGoodsId(oldProd.getGoodsId());
                                                        prod.setUpdateTime(LocalDateTime.now());
                                                        prod.setDataBatchId(dataBatchId);
                                                        productService.updateByExampleSelective(prod);
                                                    }
                                                }
                                            }
                                        }

                                    }

                                    goodDto.setDetail(null); //详情图片不更新
                                    goodDto.setName(null); //名称不更新
                                    //更新商品批次号
                                    goodsService.updateByNameAndGoodsSn(goodDto);

//        							this.syncGoodsImg(oldGoods);//更新图片,将序列号加入到图片中
                                }
//    				    		logger.error("oldGoods:");
                            } else {

//    	    					MallGoodsCodeSeq goodsCodeSeq = new MallGoodsCodeSeq();
//    	    					goodsService.addGoodsCodeSeq(goodsCodeSeq);
//    	    					goodName = brandCnName + " "+goodName+ " "+goodsCodeSeq.getId();
                                goodName = brandCnName + " " + goodName;
                                goodName = goodName.replaceAll("唯品会", "平台").replaceAll("唯品", "平台");
                                goodDto.setName(goodName);
                                this.create(goodsAllinone);

//    							this.syncGoodsImg(goodDto);//更新图片,将序列号加入到图片中
                            }
                        } catch (Exception e) {
                            logger.error("", e);
//							logger.error(JSONUtils.toJSONString(goodsAllinone));
                        }
                    }
                }

                if (totalNum > page * pageSize) {
                    //递归调用
                    this.syscGoods(newAdId, adId, page + 1, pageSize, dataBatchId, brandDesc, addPriceFlag, addPriceRate);
                }
            }
        }
    }

    @Autowired
    private StorageService storageService;
    @Autowired
    private MallGoodsCodeSeqMapper goodsCodeSeqMapper;

    //    private int index = 0;
    private void syncGoodsImg(MallGoods goods) {
//    	if(index++>0){
//    		return;//pxhtest
//    	}
        if (goods == null || goods.getId() == null
                || goods.getName() == null
                || goods.getPicUrl() == null
                && !goods.getName().contains(" ")) {
            return;
        }
        if (!goods.getPicUrl().contains("a.vpimg2.com")) {
            return;
        }
        long beginTime = System.currentTimeMillis();
        String[] goodsNameSplitArr = goods.getName().split(" ");
        if (goodsNameSplitArr == null || goodsNameSplitArr.length < 1) {
            return;
        }

        try {
            String goodsCodeSeq = goodsNameSplitArr[goodsNameSplitArr.length - 1];
            MallGoodsCodeSeq codeSeq =
                    goodsCodeSeqMapper.selectByPrimaryKey(goodsCodeSeq);
            if (codeSeq == null) {
                return;
            }

            ByteArrayOutputStream os = new ByteArrayOutputStream();

            String content = goodsCodeSeq;
            String urlPath = goods.getPicUrl();
//			System.out.println(urlPath);
//			System.out.println(goods.getId());
            int fontSize = 16;
            BufferedImage bg = ImageUtils.drawStringForNetImage(urlPath, content,
                    Color.white, 6, 20,
                    "Microsoft YaHei", fontSize, new Color(220, 99, 119),
                    0, 0, 95, 30);

            ImageIO.write(bg, "jpg", os);

            InputStream ips = new ByteArrayInputStream(os.toByteArray());

            String format = "jpg";
            String fileName = AppIdAlgorithm.generateCode(6).toUpperCase() + System.currentTimeMillis() + ".jpg";
            MallStorage storage = storageService.store(ips, os.toByteArray().length, format, fileName);
//			logger.info("storge:"+JacksonUtils.bean2Jsn(storage));

            MallGoods syncGoodsImgDto = new MallGoods();
            syncGoodsImgDto.setId(goods.getId());
            syncGoodsImgDto.setPicUrl(storage.getUrl());
            goodsService.updateById(syncGoodsImgDto);

            long passtime = System.currentTimeMillis() - beginTime;
            logger.info("passtime:" + passtime);

        } catch (Exception e) {
            logger.error("", e);
        }
    }


    //    @Autowired
//	private MallGoodsMapper goodsMapper;
//
//    @Autowired
//	private MallGoodsProductMapper goodsProductMapper;
//	
    public Object create(GoodsAllinone goodsAllinone) {
        Object error = validate(goodsAllinone);
        if (error != null) {
            return error;
        }

        MallGoods goods = goodsAllinone.getGoods();
        MallGoodsAttribute[] attributes = goodsAllinone.getAttributes();
        MallGoodsSpecification[] specifications = goodsAllinone.getSpecifications();
        MallGoodsProduct[] products = goodsAllinone.getProducts();

        String name = goods.getName();

        // 商品基本信息表Mall_goods
        goodsService.add(goods);

        //将生成的分享图片地址写入数据库
        String url = qCodeService.createGoodShareImage(goods.getId().toString(), goods.getPicUrl(), goods.getName());
        if (!StringUtils.isEmpty(url)) {
            goods.setShareUrl(url);
            if (goodsService.updateById(goods) == 0) {
                throw new RuntimeException("更新数据失败");
            }
        }

        // 商品规格表Mall_goods_specification
        List<MallGoodsSpecification> specList = new ArrayList<MallGoodsSpecification>();
        for (MallGoodsSpecification specification : specifications) {
            specification.setGoodsId(goods.getId());

            specList.add(specification);
//            specificationService.add(specification);
        }
        if (specList.size() > 0) {
            adminAdSyncService.addGoodsSpecBatch(specList);
        }

        // 商品参数表Mall_goods_attribute
        if (attributes != null && attributes.length > 0) {
            List<MallGoodsAttribute> attrList = new ArrayList<MallGoodsAttribute>();
            for (MallGoodsAttribute attribute : attributes) {
                attribute.setGoodsId(goods.getId());

                attrList.add(attribute);
//                attributeService.add(attribute);
            }
            if (attrList.size() > 0) {
                adminAdSyncService.addGoodsAttrBatch(attrList);
            }
        }

        // 商品货品表Mall_product
        List<MallGoodsProduct> productList = new ArrayList<MallGoodsProduct>();
        for (MallGoodsProduct product : products) {
            product.setGoodsId(goods.getId());

            product.setAddTime(LocalDateTime.now());
            product.setUpdateTime(LocalDateTime.now());
            productList.add(product);
//            productService.add(product);
        }
        if (productList.size() > 0) {
            adminAdSyncService.addGoodsProductsBatch(productList);
        }
        return ResponseUtil.ok();
    }


    private Object validate(GoodsAllinone goodsAllinone) {
        MallGoods goods = goodsAllinone.getGoods();
        String name = goods.getName();
        if (StringUtils.isEmpty(name)) {
            return ResponseUtil.badArgument();
        }
        String goodsSn = goods.getGoodsSn();
        if (StringUtils.isEmpty(goodsSn)) {
            return ResponseUtil.badArgument();
        }
        // 品牌商可以不设置，如果设置则需要验证品牌商存在
        /*String brandId = goods.getBrandId();
        if (brandId != null && brandId != 0) {
            if (brandService.findById(brandId) == null) {
                return ResponseUtil.badArgumentValue();
            }
        }*/
        // 分类可以不设置，如果设置则需要验证分类存在
        String categoryId = goods.getCategoryId();
        if (categoryId != null && "0".equals(categoryId)) {
            if (categoryService.findById(categoryId) == null) {
                return ResponseUtil.badArgumentValue();
            }
        }

        /*
        MallGoodsAttribute[] attributes = goodsAllinone.getAttributes();
        for (MallGoodsAttribute attribute : attributes) {
            String attr = attribute.getAttribute();
            if (StringUtils.isEmpty(attr)) {
                return ResponseUtil.badArgument();
            }
            String value = attribute.getValue();
            if (StringUtils.isEmpty(value)) {
                return ResponseUtil.badArgument();
            }
        }*/

        MallGoodsSpecification[] specifications = goodsAllinone.getSpecifications();
        for (MallGoodsSpecification specification : specifications) {
            String spec = specification.getSpecification();
            if (StringUtils.isEmpty(spec)) {
                return ResponseUtil.badArgument();
            }
            String value = specification.getValue();
            if (StringUtils.isEmpty(value)) {
                return ResponseUtil.badArgument();
            }
        }

        MallGoodsProduct[] products = goodsAllinone.getProducts();
        for (MallGoodsProduct product : products) {
            Integer number = product.getNumber();
            if (number == null || number < 0) {
                return ResponseUtil.badArgument();
            }

            BigDecimal price = product.getPrice();
            if (price == null) {
                return ResponseUtil.badArgument();
            }

            String[] productSpecifications = product.getSpecifications();
            if (productSpecifications.length == 0) {
                return ResponseUtil.badArgument();
            }
        }

        return null;
    }

}
