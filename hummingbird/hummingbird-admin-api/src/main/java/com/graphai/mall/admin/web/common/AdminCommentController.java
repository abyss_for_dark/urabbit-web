package com.graphai.mall.admin.web.common;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.framework.system.service.MallRoleService;
import com.graphai.mall.admin.domain.MallUserMerchant;
import com.graphai.mall.admin.service.impl.MallUserMerchantServiceImpl;
import com.graphai.mall.db.domain.MallAdmin;
import com.graphai.mall.db.domain.MallComment;
import com.graphai.mall.db.domain.MallRole;
import com.graphai.mall.db.service.user.behavior.MallCommentService;
import com.graphai.validator.Order;
import com.graphai.validator.Sort;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.apache.shiro.SecurityUtils.getSubject;

@RestController
@RequestMapping("/admin/comment")
@Validated
public class AdminCommentController {
    private final Log logger = LogFactory.getLog(AdminCommentController.class);

    @Autowired
    private MallCommentService commentService;

    @Autowired
    private MallRoleService mallRoleService;

    @Autowired
    private MallUserMerchantServiceImpl mallUserMerchantService;

    @RequiresPermissions("admin:comment:list")
    @RequiresPermissionsDesc(menu = {"商品管理", "评论管理"}, button = "查询")
    @GetMapping("/list")
    public Object list(String userId, String valueId,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit,
                       @Sort @RequestParam(defaultValue = "id") String sort,
                       @Order @RequestParam(defaultValue = "desc") String order) {

        MallAdmin adminUser = (MallAdmin) getSubject().getPrincipal();
        Set<MallRole> mallRoleSet = mallRoleService.findByIds(adminUser.getRoleIds());
        List<MallComment> brandList = null;
        MallComment mallComment = new MallComment();
        mallComment.setUserId(userId);
        mallComment.setValueId(valueId);

        mallComment.setUserId(userId);
        mallComment.setValueId(valueId);
        for (MallRole mallRole : mallRoleSet) {
            if ("merchant".equals(mallRole.getRoleKey()) || "plant".equals(mallRole.getRoleKey())) {
                QueryWrapper<MallUserMerchant> wrapper=new QueryWrapper<>();
                wrapper.eq("user_id",adminUser.getUserId());
                MallUserMerchant mallUserMerchant = mallUserMerchantService.getOne(wrapper);
                mallComment.setMerchantId(mallUserMerchant.getMerchantId());
                brandList = commentService.selectByCollectRole(mallComment, page, limit);
            }
            if ("admin".equals(mallRole.getRoleKey())) {
                brandList = commentService.querySelective(userId, valueId, page, limit, sort, order);
            }
        }

        long total = PageInfo.of(brandList).getTotal();
        Map<String, Object> data = new HashMap<>();
        data.put("total", total);
        data.put("items", brandList);

        return ResponseUtil.ok(data);
    }

    @RequiresPermissions("admin:comment:delete")
    @RequiresPermissionsDesc(menu = {"商品管理", "评论管理"}, button = "删除")
    @PostMapping("/delete")
    public Object delete(@RequestBody MallComment comment) {
        String id = comment.getId();
        if (id == null) {
            return ResponseUtil.badArgument();
        }
        commentService.deleteById(id);
        return ResponseUtil.ok();
    }

}
