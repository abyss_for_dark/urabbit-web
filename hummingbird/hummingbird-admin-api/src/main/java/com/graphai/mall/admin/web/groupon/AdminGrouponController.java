package com.graphai.mall.admin.web.groupon;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.graphai.commons.util.lang.StringUtils;
import com.graphai.mall.admin.domain.MallActivityGoodsProduct;
import com.graphai.mall.admin.domain.MallGoodsPosition;
import com.graphai.mall.admin.domain.MallSeckill;
import com.graphai.mall.admin.domain.MallSeckillTime;
import com.graphai.mall.admin.service.IMallActivityGoodsProductService;
import com.graphai.mall.admin.service.IMallGoodsPositionService;
import com.graphai.mall.db.constant.activity.MallActivityEnum;
import com.graphai.mall.db.constant.group.GroupEnum;
import com.graphai.mall.db.domain.*;
import com.graphai.mall.db.service.groupon.MallGrouponComponent;
import com.graphai.mall.db.service.pay.DoPayService;
import com.graphai.mall.db.vo.ActivityGrouponGoodsVo;
import com.graphai.mall.db.vo.MallGroupRuleDTO;
import com.graphai.mall.db.vo.MallGrouponDTO;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.mall.db.service.goods.MallGoodsProductService;
import com.graphai.mall.db.service.goods.MallGoodsService;
import com.graphai.mall.db.service.groupon.MallGrouponRulesService;
import com.graphai.mall.db.service.groupon.MallGrouponService;
import com.graphai.validator.Order;
import com.graphai.validator.Sort;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/admin/groupon")
@Validated
public class AdminGrouponController {
    private final Log logger = LogFactory.getLog(AdminGrouponController.class);

    @Autowired
    private MallGrouponRulesService rulesService;
    @Autowired
    private MallGoodsService goodsService;
    @Autowired
    private IMallActivityGoodsProductService mallActivityGoodsProductService;
    @Autowired
    private MallGrouponService grouponService;
    @Autowired
    private MallGoodsProductService productService;
    @Autowired
    private MallGrouponComponent mallGrouponComponent;
    @Autowired
    private IMallGoodsPositionService mallGoodsPositionService;

    /**
     * 1、团购-大额红包
     * @param grouponId
     * @param page
     * @param limit
     * @param sort
     * @param order
     * @return
     */
    @RequiresPermissions("admin:groupon:read")
    @RequiresPermissionsDesc(menu = {"推广管理", "红包记录"}, button = "详情")
    @GetMapping("/listRecord")
    public Object listRecord(String grouponId,
                             @RequestParam(defaultValue = "1") Integer page,
                             @RequestParam(defaultValue = "10") Integer limit,
                             @Sort @RequestParam(defaultValue = "id") String sort,
                             @Order @RequestParam(defaultValue = "desc") String order) {
        List<MallGroupon> grouponList = grouponService.querySelective(grouponId, page, limit, sort, order);
        long total = PageInfo.of(grouponList).getTotal();

        List<Map<String, Object>> records = new ArrayList<>();
        for (MallGroupon groupon : grouponList) {
            try {
                Map<String, Object> RecordData = new HashMap<>();
                List<MallGroupon> subGrouponList = grouponService.queryJoinRecord(groupon.getId());
                MallGrouponRules rules = rulesService.queryById(groupon.getRulesId());
                MallGoods goods = goodsService.findById(rules.getGoodsId());

                RecordData.put("groupon", groupon);
                RecordData.put("subGroupons", subGrouponList);
                RecordData.put("rules", rules);
                RecordData.put("goods", goods);

                records.add(RecordData);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        Map<String, Object> data = new HashMap<>();
        data.put("total", total);
        data.put("items", records);

        return ResponseUtil.ok(data);
    }

    /**
     * 2、团购-团购管理
     * @param mallGrouponDTO mallGrouponDTO
     * @return Object
     */
    @RequiresPermissions("admin:groupon:manageList")
    @RequiresPermissionsDesc(menu = {"商品团购", "团购管理"}, button = "查询")
    @GetMapping("/manageList")
    public Object manageList(MallGrouponDTO mallGrouponDTO) {
        MallGrouponDTO mallGrouponDTO1 = defaultParamter(mallGrouponDTO);
        HashMap<String, Object> hashMap = mallGrouponComponent.queryGrouponManage(mallGrouponDTO1);
        return ResponseUtil.ok(hashMap);
    }

    /**
     * 3、团购-团购管理-详情
     */
    @RequiresPermissions("admin:groupon:detail")
    @RequiresPermissionsDesc(menu = {"商品团购", "团购管理"}, button = "详情")
    @GetMapping("/detail")
    public Object detail(@NotNull String id) {
        return mallGrouponComponent.detail(id);
    }

    /**
     * 4、团购规则-查询
     * @param mallGrouponDTO mallGrouponDTO
     * @return
     */
    @RequiresPermissions("admin:groupon:list")
    @RequiresPermissionsDesc(menu = {"商品团购", "团购规则"}, button = "查询")
    @GetMapping("/list")
    public Object list(MallGrouponDTO mallGrouponDTO) {

        MallGrouponDTO mallGrouponDTO1 = defaultParamter(mallGrouponDTO);
        List<MallGrouponRules> rulesList = mallGrouponComponent.queryMallGrouponList(mallGrouponDTO1);
        long total = PageInfo.of(rulesList).getTotal();
        Map<String, Object> data = new HashMap<>();
        data.put("total", total);
        data.put("items", rulesList);

        return ResponseUtil.ok(data);
    }

    /**
     * 5、团购规则-编辑
     * @param grouponRules
     * @return
     */
    @RequiresPermissions("admin:groupon:update")
    @RequiresPermissionsDesc(menu = {"推广管理", "团购管理"}, button = "编辑")
    @PostMapping("/update")
    public Object update(@RequestBody MallGrouponRules grouponRules) {
        try {
            Object error = validate(grouponRules);
            if (error != null) {
                return error;
            }

            String goodsId = grouponRules.getGoodsId();
            MallGoods goods = goodsService.findById(goodsId);
            if (goods == null) {
                return ResponseUtil.badArgumentValue();
            }
            List<MallGoodsProduct> products=productService.queryByGid(goodsId);
            if (CollectionUtils.isNotEmpty(products)) {
                products.get(0).setFaceValue(grouponRules.getGroupPrice());
            }

            grouponRules.setGoodsName(goods.getName());
            grouponRules.setPicUrl(goods.getPicUrl());

            // 因为goods_id跟mall_goods关联，这里不能更新，要赋值为空
            grouponRules.setGoodsId(null);

            if (rulesService.updateById(grouponRules) == 0) {
                return ResponseUtil.updatedDataFailed();
            }

            return ResponseUtil.ok();
        } catch (Exception ex) {
            logger.error("异常信息：" + ex.getMessage(), ex);
            ex.printStackTrace();
            return ResponseUtil.fail("操作失败：" + ex.getMessage());
        }
    }

    /**
     * 6、团购规则-添加
     * @param grouponRules
     * @return
     */
    @RequiresPermissions("admin:groupon:create")
    @RequiresPermissionsDesc(menu = {"推广管理", "团购管理"}, button = "添加")
    @PostMapping("/create")
    public Object create(@RequestBody MallGrouponRules grouponRules) {
        Object error = validate(grouponRules);
        if (error != null) {
            return error;
        }

        String goodsId = grouponRules.getGoodsId();
        MallGoods goods = goodsService.findById(goodsId);
        if (goods == null) {
            return ResponseUtil.badArgumentValue();
        }
        List<MallGoodsProduct> products=productService.queryByGid(goodsId);
        if (CollectionUtils.isNotEmpty(products)) {
            products.get(0).setFaceValue(grouponRules.getGroupPrice());
        }
        grouponRules.setGoodsName(goods.getName());
        grouponRules.setPicUrl(goods.getPicUrl());

        rulesService.createRules(grouponRules);

        return ResponseUtil.ok(grouponRules);
    }

    @RequiresPermissions("admin:groupon:delete")
    @RequiresPermissionsDesc(menu = {"推广管理", "团购管理"}, button = "删除")
    @PostMapping("/delete")
    public Object delete(@RequestBody MallGrouponRules grouponRules) {
        String id = grouponRules.getId();
        if (id == null) {
            return ResponseUtil.badArgument();
        }

        rulesService.delete(id);
        return ResponseUtil.ok();
    }

    /**
     * 7、团购-测试添加机器人
     *
     * @param body  openGrouponId团长ID  num机器人数量
     * @return
     */
    @RequiresPermissions("admin:groupon:addRobot")
    @RequiresPermissionsDesc(menu = {"推广管理", "团购管理"}, button = "添加机器人")
    @PostMapping("addRobot")
    public Object addRobot(@RequestBody JSONObject body) {
        String openGrouponId = body.getStr("openGrouponId");
        Integer num = body.getInt("num");
        return mallGrouponComponent.addRobot(openGrouponId, num);
    }

    /**
     * 8、团购-必中操作
     *
     * @param mallGroupon  id：团ID
     * @return
     */
    @RequiresPermissions("admin:groupon:detail")
    @RequiresPermissionsDesc(menu = {"推广管理", "团购管理"}, button = "必中操作")
    @PostMapping("isWinnerGroupon")
    public Object isWinnerGroupon(@RequestBody MallGroupon mallGroupon) {
        if (Objects.isNull(mallGroupon))
            return ResponseUtil.fail("参数有误");
        if (StringUtils.isNotBlank(mallGroupon.getId())) {
            return mallGrouponComponent.isWinnerGroupon(mallGroupon.getId());
        } else {
            return ResponseUtil.fail("参数ID有误");
        }
    }


    private Object validate(MallGrouponRules grouponRules) {
        String goodsId = grouponRules.getGoodsId();
        if (goodsId == null) {
            return ResponseUtil.badArgument();
        }
//        BigDecimal discount = grouponRules.getDiscount();
//        if (discount == null) {
//            return ResponseUtil.badArgument();
//        }
        Integer discountMember = grouponRules.getDiscountMember();
        if (discountMember == null) {
            return ResponseUtil.badArgument();
        }
        LocalDateTime expireTime = grouponRules.getExpireTime();
        if (expireTime == null) {
            return ResponseUtil.badArgument();
        }

        return null;
    }

    /**
     * 请求参数默认值
     * @param mallGrouponDTO 封装参数对象
     * @return
     */
    private MallGrouponDTO defaultParamter(MallGrouponDTO mallGrouponDTO) {
        // 排序
        String sort = "id";
        String order = "desc";
        if (StringUtils.isNotBlank(mallGrouponDTO.getSort()))
            sort = mallGrouponDTO.getSort();
        if (StringUtils.isNotBlank(mallGrouponDTO.getOrder()))
            order = mallGrouponDTO.getOrder();
        mallGrouponDTO.setSort(sort);
        mallGrouponDTO.setOrder(order);

        // 分页
        Integer page = 1;
        Integer size = 10;
        if (Objects.nonNull(mallGrouponDTO.getPage()))
            page = mallGrouponDTO.getPage();
        if (Objects.nonNull(mallGrouponDTO.getLimit()))
            size = mallGrouponDTO.getLimit();
        mallGrouponDTO.setPage(page);
        mallGrouponDTO.setLimit(size);

        return mallGrouponDTO;
    }


    @RequiresPermissionsDesc(menu = {"单独查询"}, button = "单独查询")
    @GetMapping("/info/{id}")
    public Object info(@PathVariable String id) throws Exception {
        MallGrouponRules mallGrouponRules = rulesService.queryById(id);
        String goodsId = mallGrouponRules.getGoodsId();
        MallGoods mallGoods = goodsService.findById(goodsId);
        List<MallActivityGoodsProduct> mallActivityGoodsProductList = mallActivityGoodsProductService.getListByActivityGoodsId(MallActivityEnum.ACTIVITY_GOODS_2.strCode2Int(), goodsId);
       Map<String,Object> map = new HashMap<>();
        map.put("mallActivityGoodsProductList",mallActivityGoodsProductList);
        map.put("mallGoods",mallGoods);
        map.put("mallGrouponRules",mallGrouponRules);
        return ResponseUtil.ok(map);
    }

    /**
     * 6、团购规则-添加或者修改
     *
     * @param activityGrouponGoodsVo
     * @return
     */
    @RequiresPermissionsDesc(menu = {"推广管理", "团购管理"}, button = "添加")
    @PostMapping("/addOrSave")
    public Object addOrSave(@RequestBody ActivityGrouponGoodsVo activityGrouponGoodsVo) {
        MallAdmin adminUser = (MallAdmin) SecurityUtils.getSubject().getPrincipal();
        MallGrouponRules grouponRules = activityGrouponGoodsVo.getGrouponRules();
        grouponRules.setFactoryUserId(adminUser.getUserId());
        boolean isAdd = false;
        if (StrUtil.isEmpty(grouponRules.getId())) {
            isAdd = true;
        }
        List<MallActivityGoodsProduct> goodsList = activityGrouponGoodsVo.getMallActivityGoodsProductList();
        if (null == goodsList || goodsList.size() < 1) {
            return ResponseUtil.fail("获取商品信息失败");
        }
        for (MallActivityGoodsProduct mallActivityGoodsProduct : goodsList) {
            if (!isAdd && StrUtil.isEmpty(mallActivityGoodsProduct.getId())) {
                return ResponseUtil.fail("获取到产品ID为空");
            }
            if (null == mallActivityGoodsProduct.getActivityPrice() || mallActivityGoodsProduct.getActivityPrice().compareTo(BigDecimal.ZERO) < 1) {
                return ResponseUtil.fail("团购价必须大于0元");
            }
        }
        String goodsId = grouponRules.getGoodsId();
        MallGroupRuleDTO groupRuleDTO = rulesService.queryGroupByGoodsId(goodsId);
        if (isAdd && null != groupRuleDTO && groupRuleDTO.getStatus() != null && groupRuleDTO.getStatus() != GroupEnum.STATUS_2.getIntCode()) {
            return ResponseUtil.fail("该商品已经是团购商品了");
        }
        MallGoods goods = goodsService.findById(goodsId);
        if (goods == null) {
            return ResponseUtil.fail("获取商品信息失败");
        }
        grouponRules.setGoodsName(goods.getName());
        grouponRules.setPicUrl(goods.getPicUrl());
        if (isAdd) {
            grouponRules.setStatus(GroupEnum.STATUS_1.getIntCode());
            grouponRules.setGroupNum(9999);
            grouponRules.setSuccessNum(grouponRules.getDiscountMember());
            rulesService.createRules(grouponRules);
            String id = grouponRules.getId();
            for (MallActivityGoodsProduct mallActivityGoodsProduct : goodsList) {
                mallActivityGoodsProduct.setId(null);
                mallActivityGoodsProduct.setGoodsId(goodsId);
                mallActivityGoodsProduct.setActivityRuleId(id);
                mallActivityGoodsProduct.setSurplusNumber(mallActivityGoodsProduct.getNumber());
                mallActivityGoodsProduct.setActivityType(MallActivityEnum.ACTIVITY_GOODS_2.strCode2Int());
                mallActivityGoodsProduct.setAddTime(LocalDateTime.now());
                mallActivityGoodsProductService.save(mallActivityGoodsProduct);
            }

            MallGoodsPosition mallGoodsPosition = new MallGoodsPosition();
            mallGoodsPosition.setGoodsId(grouponRules.getGoodsId());
            mallGoodsPosition.setType(3);
            mallGoodsPosition.setDeleted(false);
            mallGoodsPosition.setCreateId(adminUser.getUserId());
            mallGoodsPosition.setCreateTime(LocalDateTime.now());
            mallGoodsPositionService.save(mallGoodsPosition);
            return ResponseUtil.ok(grouponRules);
        }
        //修改
        for (MallActivityGoodsProduct mallActivityGoodsProduct : goodsList) {
            mallActivityGoodsProductService.updateById(mallActivityGoodsProduct);
        }
        MallGrouponRules mallGrouponRules = rulesService.queryById(grouponRules.getId());
        if (null != mallGrouponRules && null != mallGrouponRules.getCheckStatus() && GroupEnum.RULE_CHECK_STATUS_2.getIntCode() == mallGrouponRules.getCheckStatus()) {
            grouponRules.setCheckStatus(GroupEnum.RULE_CHECK_STATUS_0.getIntCode());
        }
        rulesService.updateById(grouponRules);
        return ResponseUtil.ok(grouponRules);
    }

    /**
     * 8、团购-审核
     *
     * @param mallGrouponRules  团购规则
     * @return
     */
    @RequiresPermissions("admin:groupon:audit")
    @RequiresPermissionsDesc(menu = {"推广管理", "团购管理"}, button = "审核")
    @PostMapping("/audit")
    public Object audit(@RequestBody MallGrouponRules mallGrouponRules) {
        if (Objects.isNull(mallGrouponRules))
            return ResponseUtil.fail("参数有误");
        if (StringUtils.isNotBlank(mallGrouponRules.getId())) {
            return mallGrouponComponent.auditGroupon(mallGrouponRules);
        } else {
            return ResponseUtil.fail("参数ID有误");
        }
    }
}
