package com.graphai.mall.admin.web.statistics;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.mall.db.service.statistics.MallDashbordService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import retrofit2.http.GET;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * 首页-数据统计
 */
@RestController
@RequestMapping("/admin/dashboard")
@Validated
public class AdminDashbordController {
    private final Log logger = LogFactory.getLog(AdminDashbordController.class);


    @Resource
    private MallDashbordService mallDashbordService;

    /**
     * 1.首页统计-总数量统计
     *
     * @return Object
     */
    @GetMapping("")
    public Object info() {
        return ResponseUtil.ok(mallDashbordService.getStatisticsTotal());
    }


    /**
     * 1.首页统计-总数量统计
     *
     * @return Object
     */
    @GetMapping("/infoV2")
    public Object infoV2(String param) {
        return ResponseUtil.ok(mallDashbordService.getStatisticsTotalV2(param));
    }

    /**
     * 2.首页统计-昨天、近7天、近30天
     *
     * @param day 天数
     * @return Object
     */
    @GetMapping("/statistics")
    public Object statistics(@RequestParam Integer day) {
        return ResponseUtil.ok(mallDashbordService.statistics(day == null ? 7 : day));
    }


    /**
     * 折线图-用户趋势
     */
    @GetMapping("/statisticsUserChange")
    public Object statisticsUserChange(@RequestParam Integer day) {
        return ResponseUtil.ok(mallDashbordService.statisticsUserChange(day == null ? 7 : day));
    }

    /**
     * 3.首页统计-统计收益趋势
     */
    @GetMapping("/statisticsTrend")
    public Object statisticsTrend(@RequestParam Integer day) {
        return ResponseUtil.ok(mallDashbordService.statisticsTrend(day == null ? 7 : day));
    }

    /**
     * 4.首页统计-平台总用户数、当月新增用户数
     */
    @GetMapping("/statisticsUser")
    public Object statisticsUser() {
        return ResponseUtil.ok(mallDashbordService.statisticsUser());
    }


    /**
     * 4.首页统计-用户粉丝排行榜·TOP200
     */
    @GetMapping("/getFansRanking")
    public Object getFansRanking() {
        return ResponseUtil.ok(mallDashbordService.getFansRanking());
    }


    /**
     * 数据概况（首页）
     */
    @GetMapping("/ProfileData")
    public Object ProfileData() {

        List<Map<String, Object>> list = mallDashbordService.ProfileData();
        return ResponseUtil.ok(list);

    }

    /**
     * 实时数据（每日）
     *
     * @return
     */
    @GetMapping("/toDay")
    public Object toDay(@RequestParam(value = "day", defaultValue = "0") String day) {

        List<Map<String, Object>> list = mallDashbordService.realTimeData(day);
        return ResponseUtil.ok(list);

    }

    /**
     * @param day 天数
     *            工厂数据(排行榜)
     */

    @GetMapping("/factoryData")
    public Object factoryData(@RequestParam(value = "day", defaultValue = "") String day, @RequestParam(value = "page", defaultValue = "1") String page, @RequestParam(value = "limit", defaultValue = "10") String pageSize) {
        PageHelper.startPage(Integer.parseInt(page), Integer.parseInt(pageSize));

        List<Map<String, Object>> list = mallDashbordService.factoryData(day);
        PageInfo pageInfo = new PageInfo(list);
        return ResponseUtil.ok(pageInfo);
    }

    /**
     * 商品热销榜
     */
    @GetMapping("/hotList")
    public Object hotList(@RequestParam(value = "day", defaultValue = "") String day, @RequestParam(value = "page", defaultValue = "1") String page, @RequestParam(value = "pageSize", defaultValue = "10") String pageSize) {
        PageHelper.startPage(Integer.parseInt(page), Integer.parseInt(pageSize));
        List<Map<String, Object>> list = mallDashbordService.hotList(day);
        PageInfo pageInfo = new PageInfo(list);
        return ResponseUtil.ok(pageInfo);
    }

    /**
     * 销售商统计
     *
     * @return
     */

    @GetMapping("/storeCount")
    public Object storeCount() {
        Object store = mallDashbordService.storeCount();
        return store;
    }

}
