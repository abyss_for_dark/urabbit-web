package com.graphai.mall.admin.web.metest;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.lang.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.graphai.commons.util.servlet.ServletUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.baomidou.mybatisplus.core.metadata.IPage;

import org.springframework.web.bind.annotation.RestController;

import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.framework.generator.controller.IBaseController;

import com.graphai.mall.admin.domain.MallQinxiang;
import com.graphai.mall.admin.service.IMallQinxiangService;



/**
 * <p>
 * 技术测试 前端控制器
 * </p>
 *
 * @author biteam
 * @since 2020-12-16
 */
@RestController
@RequestMapping("/admin/qinxiang")
public class MallQinxiangController extends IBaseController<MallQinxiang,  String >  {

	@Autowired
    private IMallQinxiangService serviceImpl;
    
    @RequiresPermissions("admin:qinxiang:single")
    @RequiresPermissionsDesc(menu = {"单独查询"}, button = "单独查询")
	@GetMapping("/single/{id}")
    public Object getEntityById(@PathVariable  String  id) throws Exception{
        MallQinxiang entityVName = serviceImpl.getById(id);
        return ResponseUtil.ok(entityVName);
    }
    
    @RequiresPermissions("admin:qinxiang:addOrUpdate")
    @RequiresPermissionsDesc(menu = {"新增"}, button = "新增")
    @PostMapping("/addOrUpdate")
    public Object addOrUpdate(MallQinxiang mallQinxiang) throws Exception{
        if(serviceImpl.saveOrUpdate(mallQinxiang)){
            return ResponseUtil.ok();
        }else{
            return ResponseUtil.fail();
        }
    } 
	
	
	@RequiresPermissions("admin:qinxiang:melist")
	@RequiresPermissionsDesc(menu = {"技术测试查询"}, button = "查询")
    @GetMapping("/melist")
    public Object getMeList() throws Exception {
        IPage<MallQinxiang> ipage = serviceImpl.page(getPlusPage(),listQueryWrapper());
        return ResponseUtil.ok(ipage);
    }
 	
 	private QueryWrapper<MallQinxiang> listQueryWrapper() {
 		QueryWrapper<MallQinxiang> meq = new QueryWrapper<>();
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("activityId"))) {
		      meq.eq("activity_id", ServletUtils.getParameter("activityId"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("brandId"))) {
		      meq.eq("brand_id", ServletUtils.getParameter("brandId"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("addTime"))) {
		      meq.eq("add_time", ServletUtils.getLocalDateTime("addTime"));  
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("deleted"))) {
		      meq.eq("deleted", ServletUtils.getParameter("deleted"));
		}
		
        return meq;
    }
 	@RequiresPermissions("admin:qinxiang:save")
 	@RequiresPermissionsDesc(menu = {"技术测试新增"}, button = "新增")
    @PostMapping("/save")
    public Object save(@ModelAttribute MallQinxiang mallQinxiang) throws Exception {
        serviceImpl.save(mallQinxiang);
        return ResponseUtil.ok();
    }
	
	@RequiresPermissions("admin:qinxiang:modify")
	@RequiresPermissionsDesc(menu = {"技术测试修改"}, button = "修改")
    @PutMapping("/modify")
    public Object modify(@ModelAttribute  MallQinxiang mallQinxiang){
        serviceImpl.updateById(mallQinxiang);
        return  ResponseUtil.ok();
    }
    
    @RequiresPermissions("admin:qinxiang:del")
    @RequiresPermissionsDesc(menu = {"技术测试删除"}, button = "删除")
    @DeleteMapping("/del/{id}")
    public Object deleteById(@PathVariable  String  id){
        serviceImpl.removeById(id);
        return  ResponseUtil.ok();
    }
    
}

