package com.graphai.mall.admin.web.finance;

import static com.graphai.framework.constant.AdminResponseCode.ADMIN_INVALID_ACCOUNT;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.bcrypt.BCryptPasswordEncoder;
import com.graphai.commons.util.json.JacksonUtil;
import com.graphai.commons.util.json.JacksonUtils;
import com.graphai.commons.util.lang.StringUtils;
import com.graphai.commons.util.lock.MutexLock;
import com.graphai.commons.util.lock.MutexLockUtils;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.mall.admin.domain.FcAccountPaymentBill;
import com.graphai.mall.admin.service.IFcAccountPaymentBillService;
import com.graphai.mall.db.domain.FcAccount;
import com.graphai.mall.db.domain.FcAccountWithdrawBill;
import com.graphai.mall.db.domain.MallAdmin;
import com.graphai.mall.db.domain.MallSystem;
import com.graphai.mall.db.domain.MallUser;
import com.graphai.mall.db.domain.MemberPullNew;
import com.graphai.mall.db.service.MallAdminService;
import com.graphai.mall.db.service.WxEnterpriseTransferService;
import com.graphai.mall.db.service.account.AccountService;
import com.graphai.mall.db.service.account.AccountStatus;
import com.graphai.mall.db.service.sysconfig.MallSystemConfigService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.constant.CommonConstant;
import com.graphai.mall.db.util.account.AccountUtils;
import com.graphai.mall.db.util.phone.PushUtils;
import com.graphai.mall.db.vo.FcAccountWithdrawalBillVo;
import com.graphai.mall.db.vo.WithdrawalRuleVo;
import com.graphai.properties.WxProperties;
import com.graphai.validator.Order;

import cn.hutool.core.util.ObjectUtil;

@RestController
@RequestMapping("/admin/account/withdraw")
@Validated
public class AdminAccountController {
    private final Log logger = LogFactory.getLog(AdminAccountController.class);
    @Autowired
    private WxProperties wxProperties;
    @Autowired
    private AccountService accountService;
    @Resource
    private MallAdminService mallAdminService;
    @Resource
    private MallUserService mallUserService;
    @Autowired
    private WxEnterpriseTransferService wxEnterpriseTransferService;
    @Autowired
    private MallSystemConfigService mallSystemConfigService;
    @Autowired
    private IFcAccountPaymentBillService paymentBillService;

    @RequiresPermissions("admin:account:withdraw:list")
    @RequiresPermissionsDesc(menu = {"财务管理", "提现管理"}, button = "查询")
    @GetMapping("/list")
    public Object list(@RequestParam(defaultValue = "0") String type,
                       String withdrawAccount,
                       String superior,
                       @RequestParam(required = false) String mobile,
                       @RequestParam(required = false) List<String> withdrawType,
                       @RequestParam(required = false) List<String> withdrawState,
                       String beginTime, String endTime,String paymentNo,
                       @RequestParam String amtMin, @RequestParam String amtMax,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit,
                       @RequestParam(defaultValue = "WITHDRAW_DATE") String sort,
                       @Order @RequestParam(defaultValue = "desc") String order,
                       String statusBeginTime, String statusEndTime) {

        System.out.println(amtMin+"----------------------"+amtMax);

        String userId = "";//用户
        String userId1 = "";//代理
        //通过手机号查询用户信息
        if (!StringUtils.isEmpty(mobile)) {
            List<MallUser> mallUserList = mallUserService.queryByMobileUserName(mobile);
            MallUser mallUser = null;
            if (mallUserList.size() > 0) {
                mallUser = mallUserList.get(0);
                userId = mallUser.getId();
            } else {
                userId = "-2";
            }
        }
        //通过手机号查询代理用户信息
        if (!StringUtils.isEmpty(superior)) {
            List<MallUser> mallUserList = mallUserService.queryByMobileUserName(superior);
            MallUser mallUser = null;
            if (mallUserList.size() > 0) {
                mallUser = mallUserList.get(0);
                userId1 = mallUser.getId();
            }
        }



        if (!StringUtils.isEmpty(superior)) {
            List<FcAccountWithdrawalBillVo> withdrawList2 =
                    accountService.selectWithdrawBillBySuperior(userId1, amtMin, amtMax, withdrawType, withdrawState, page, limit);
//        List<MallAd> adList = adService.querySelective(name, content, page, limit, sort, order);
            long total = PageInfo.of(withdrawList2).getTotal();
            Map<String, Object> data = new HashMap<>();
            data.put("total", total);
            data.put("items", withdrawList2);

            return ResponseUtil.ok(data);
        } else {
            List<FcAccountWithdrawBill> withdrawList =
                    accountService.queryAccountWithdraw(userId, amtMin, amtMax, type, withdrawAccount, withdrawType, withdrawState,
                            beginTime, endTime, page, limit, sort, order, statusBeginTime, statusEndTime,paymentNo);

            for (FcAccountWithdrawBill list1 : withdrawList) {
                MallUser user = mallUserService.findById(list1.getCustomerId());
                if (user != null) {
                    list1.setMobile(user.getMobile());
                    list1.setValCode(user.getAvatar());
                }
            }

//        List<MallAd> adList = adService.querySelective(name, content, page, limit, sort, order);
            long total = PageInfo.of(withdrawList).getTotal();
            Map<String, Object> data = new HashMap<>();
            data.put("total", total);
            data.put("items", withdrawList);

            return ResponseUtil.ok(data);
        }
    }

    @RequiresPermissions("admin:account:withdraw:list1")
    @RequiresPermissionsDesc(menu = {"财务管理", "提现记录"}, button = "查询")
    @GetMapping("/list1")
    public Object list1(String type,
                        String withdrawAccount,
                        @RequestParam(required = false) String mobile,
                        @RequestParam(required = false) List<String> withdrawType,
                        @RequestParam(required = false) List<String> withdrawState,
                        String beginTime, String endTime,
                        @RequestParam(defaultValue = "1") Integer page,
                        @RequestParam(defaultValue = "10") Integer limit,
                        @RequestParam(defaultValue = "WITHDRAW_DATE") String sort,
                        @Order @RequestParam(defaultValue = "desc") String order,
                        String statusBeginTime, String statusEndTime) {

        String userId = "";
        //通过手机号查询用户信息
        if (!StringUtils.isEmpty(mobile)) {

            List<MallUser> mallUserList = mallUserService.queryByMobile(mobile);
            MallUser mallUser = null;
            if (mallUserList.size() == 1) {
                mallUser = mallUserList.get(0);
                userId = mallUser.getId();
            }
        }


        List<FcAccountWithdrawBill> withdrawList =
                accountService.queryAccountWithdraw(userId, null,null, "2", withdrawAccount, withdrawType, withdrawState,
                        beginTime, endTime, page, limit, sort, order, statusBeginTime, statusEndTime,null);
        List<FcAccountWithdrawBill> withdrawList1 = new ArrayList<>();
        for (FcAccountWithdrawBill list1 : withdrawList) {
            MallUser user = mallUserService.findById(list1.getCustomerId());
            if (user != null) {
                list1.setMobile(user.getMobile());
                withdrawList1.add(list1);
            }
        }


//        List<MallAd> adList = adService.querySelective(name, content, page, limit, sort, order);
        long total = PageInfo.of(withdrawList).getTotal();
        Map<String, Object> data = new HashMap<>();
        data.put("total", total);
        data.put("items", withdrawList1);

        return ResponseUtil.ok(data);
    }

    @RequiresPermissions("admin:account:withdraw:listNew")
    @RequiresPermissionsDesc(menu = {"财务管理", "会员拉新结算"}, button = "查询")
    @GetMapping("/listNew")
    public Object listNew(@RequestParam(required = false) String channelId,
                          @RequestParam(required = false)List<String> levelId,
                          @RequestParam(required = false) String agentId,
                          @RequestParam(required = false) String mobile,
                          @RequestParam(defaultValue = "") String beginTime,
                          @RequestParam(defaultValue = "") String endTime,
                          @RequestParam(defaultValue = "") String beginTime1,
                          @RequestParam(defaultValue = "") String endTime1,
                          @RequestParam(defaultValue = "1") Integer page,
                          @RequestParam(defaultValue = "17") Integer limit,
                          @Order @RequestParam(defaultValue = "desc") String order,
                          @RequestParam(required = false) String isSettlement,
                          @RequestParam(required = false) String isEligible) {


        String userId = "";
        String agentid = "";
        //通过手机号查询用户信息
        if (!StringUtils.isEmpty(mobile)) {
            List<MallUser> mallUserList = mallUserService.queryByMobileUserName(mobile);
            MallUser mallUser = null;
            if (mallUserList.size() == 1) {
                mallUser = mallUserList.get(0);
                userId = mallUser.getId();
            }
        }
        //通过手机号查询用户信息
        if (!StringUtils.isEmpty(agentId)) {
            List<MallUser> mallUserList = mallUserService.queryByMobileUserName(agentId);
            MallUser mallUser = null;
            if (mallUserList.size() == 1) {
                mallUser = mallUserList.get(0);
                agentid = mallUser.getId();
            }
        }

        if(null != levelId && levelId.size() > 0 && "9".equals(levelId.get(0))){
            levelId=null;
        }
        if("9".equals(channelId)){
            channelId = null;
        }

        Map<String, Object> list = mallUserService.selectNewMember(channelId,levelId,isEligible, beginTime, endTime, beginTime1, endTime1,agentid, isSettlement, userId, page, limit);

        return ResponseUtil.ok(list);
    }

    @RequiresPermissions("admin:account:withdraw:listNew")
    @RequiresPermissionsDesc(menu = {"财务管理", "粉丝达标结算"}, button = "查询")
    @GetMapping("/listFans")
    public Object listFans(@RequestParam(required = false) String channelId,
                           @RequestParam(required = false)String levelId,
                           @RequestParam(defaultValue = "") String settlementObj,
                           @RequestParam(required = false) String settlementObjLevel,
                           @RequestParam(required = false) String isWx,
                          @RequestParam(required = false) String mobile,
                          @RequestParam(defaultValue = "") String beginTime,
                          @RequestParam(defaultValue = "") String endTime,
                          @RequestParam(defaultValue = "") String beginTime1,
                          @RequestParam(defaultValue = "") String endTime1,
                          @RequestParam(defaultValue = "1") Integer page,
                          @RequestParam(defaultValue = "17") Integer limit,
                          @Order @RequestParam(defaultValue = "desc") String order,
                          @RequestParam(required = false) String isSettlement,
                          @RequestParam(required = false) String isEligible) {


        String userId = "";

        //通过手机号或昵称查询用户信息
        if (!StringUtils.isEmpty(mobile)) {
            List<MallUser> mallUserList = mallUserService.queryByMobileUserName(mobile);
            MallUser mallUser = null;
            if (mallUserList.size() == 1) {
                mallUser = mallUserList.get(0);
                userId = mallUser.getId();
            }
        }
        //通过手机号或昵称查询用户信息
        if (!StringUtils.isEmpty(settlementObj)) {
            List<MallUser> mallUserList = mallUserService.queryByMobileUserName(settlementObj);
            MallUser mallUser = null;
            if (mallUserList.size() == 1) {
                mallUser = mallUserList.get(0);
                settlementObj = mallUser.getId();
            }
        }
        //结算对象等级
        if (!StringUtils.isEmpty(settlementObjLevel)) {
            if (settlementObjLevel.equals("7")) {
                settlementObjLevel =null;
            }
        }
        //是否微信授权
        if (!StringUtils.isEmpty(isWx)) {
            if (isWx.equals("3")) {
                isWx =null;
            }
        }




        Map<String, Object> list = mallUserService.selectNewFans("3",levelId,isEligible, beginTime, endTime, beginTime1,
                endTime1,settlementObj, isSettlement, userId, page, limit,settlementObjLevel,isWx);

        return ResponseUtil.ok(list);
    }




    //    @RequiresPermissions("admin:account:withdraw:batchSettlement")
    @RequiresPermissionsDesc(menu = {"财务管理", "会员拉新结算"}, button = "批量结算")
    @PostMapping("/batchSettlement")
    public Object batchSettlement(@RequestBody List<Map<String, Object>> memberPullNews) {
        MemberPullNew memberPullNew = new MemberPullNew();
        if (memberPullNews.size() > 0) {
            for (Map<String, Object> map : memberPullNews) {
                memberPullNew.setUserId((String) map.get("userId"));
                memberPullNew.setOrderSn((String) map.get("orderSn"));
                if (!StringUtils.isEmpty(String.valueOf(map.get("thirdId")))) {
                    memberPullNew.setAgentId((String) map.get("thirdId"));
                    memberPullNew.setAgentMobile((String) map.get("thirdLeaderMobile"));
                    memberPullNew.setAgentNickname((String) map.get("thirdLeaderName"));
                    memberPullNew.setLevelId("6");
                    if (mallUserService.addMemberPullNew(memberPullNew) > 0) {
                        if (null != memberPullNew.getUserId() && !ObjectUtil.equal(memberPullNew.getUserId(), null)) {
                            MallUser user = mallUserService.findById(memberPullNew.getUserId());
                            if (null != user && 1 != user.getIsSettlement()) {
                                user.setIsSettlement((byte) 1);
                                user.setSettlementTime(LocalDateTime.now());
                                mallUserService.updateById(user);
                            } else {
                                return ResponseUtil.fail(403, "未查到用户或已经结算！");
                            }
                        }
                    } else {
                        return ResponseUtil.fail(403, "结算失败！");
                    }
                } else if (StringUtils.isEmpty(String.valueOf(map.get("thirdId"))) && !StringUtils.isEmpty(String.valueOf(map.get("secondId")))) {
                    memberPullNew.setAgentId((String) map.get("secondId"));
                    memberPullNew.setAgentMobile((String) map.get("secondLeaderMobile"));
                    memberPullNew.setAgentNickname((String) map.get("secondLeaderName"));
                    memberPullNew.setLevelId("5");
                    if (mallUserService.addMemberPullNew(memberPullNew) > 0) {
                        if (null != memberPullNew.getUserId() && !ObjectUtil.equal(memberPullNew.getUserId(), null)) {
                            MallUser user = mallUserService.findById(memberPullNew.getUserId());
                            if (null != user && 1 != user.getIsSettlement()) {
                                user.setIsSettlement((byte) 1);
                                user.setSettlementTime(LocalDateTime.now());
                                mallUserService.updateById(user);
                            } else {
                                return ResponseUtil.fail(403, "未查到用户或已经结算！");
                            }
                        }
                    } else {
                        return ResponseUtil.fail(403, "结算失败！");
                    }
                } else if (StringUtils.isEmpty(String.valueOf(map.get("thirdId"))) && StringUtils.isEmpty(String.valueOf(map.get("secondId"))) && !StringUtils.isEmpty(String.valueOf(map.get("firstId")))) {
                    memberPullNew.setAgentId((String) map.get("firstId"));
                    memberPullNew.setAgentMobile((String) map.get("firstLeaderMobile"));
                    memberPullNew.setAgentNickname((String) map.get("firstLeaderName"));
                    memberPullNew.setLevelId("4");
                    if (mallUserService.addMemberPullNew(memberPullNew) > 0) {
                        if (null != memberPullNew.getUserId() && !ObjectUtil.equal(memberPullNew.getUserId(), null)) {
                            MallUser user = mallUserService.findById(memberPullNew.getUserId());
                            if (null != user && 1 != user.getIsSettlement()) {
                                user.setIsSettlement((byte) 1);
                                user.setSettlementTime(LocalDateTime.now());
                                mallUserService.updateById(user);
                            } else {
                                return ResponseUtil.fail(403, "未查到用户或已经结算！");
                            }
                        }
                    } else {
                        return ResponseUtil.fail(403, "结算失败！");
                    }
                }

            }
        }

        return ResponseUtil.ok();
    }
    @PostMapping("/selectUser")
    public Object selectUser(@RequestParam String id){
        MallUser user = mallUserService.findById(id);
        return ResponseUtil.ok(user);
    }



    //    @RequiresPermissions("admin:account:withdraw:settlement")
    @RequiresPermissionsDesc(menu = {"财务管理", "会员拉新结算"}, button = "结算")
    @PostMapping("/settlement")
    public Object settlement(@RequestBody MemberPullNew memberPullNew) {


        if (mallUserService.addMemberPullNew(memberPullNew) > 0) {
            if (null != memberPullNew.getUserId() && !ObjectUtil.equal(memberPullNew.getUserId(), null)) {
                MallUser user = mallUserService.findById(memberPullNew.getUserId());
                if (null != user && 1 != user.getIsSettlement()) {
                    user.setIsSettlement((byte) 1);
                    user.setSettlementTime(LocalDateTime.now());
                    mallUserService.updateById(user);
                } else {
                    return ResponseUtil.fail(403, "未查到用户或已经结算！");
                }
            }
        } else {
            return ResponseUtil.fail(403, "结算失败！");
        }
        return ResponseUtil.ok();
    }

    //    @RequiresPermissions("admin:account:withdraw:settlement")
    @RequiresPermissionsDesc(menu = {"财务管理", "粉丝达标结算"}, button = "结算")
    @PostMapping("/settlementFans")
    public Object settlementFans(@RequestBody String body) {

        String settlementObjId = JacksonUtil.parseString(body,"settlementObjId");
        String settlementObjMobile = JacksonUtil.parseString(body,"settlementObjMobile");
        String settlementObj = JacksonUtil.parseString(body,"settlementObj");
        String userId = JacksonUtil.parseString(body,"userId");
        MemberPullNew memberPullNew = new MemberPullNew();
        MallUser userObj = mallUserService.findById(settlementObjId);
        memberPullNew.setAgentNickname(settlementObj);
        memberPullNew.setAgentMobile(settlementObjMobile);
        memberPullNew.setAgentId(settlementObjId);
        if(null != userObj){
            memberPullNew.setLevelId(userObj.getId());
        }
        if (mallUserService.addMemberPullNew(memberPullNew) > 0) {
            if (null != memberPullNew.getUserId() && !ObjectUtil.equal(memberPullNew.getUserId(), null)) {
                MallUser user = mallUserService.findById(userId);
                if (null != user && 1 != user.getIsSettlement()) {
                    user.setIsSettlement((byte) 1);
                    user.setSettlementTime(LocalDateTime.now());
                    mallUserService.updateById(user);
                } else {
                    return ResponseUtil.fail(403, "未查到用户或已经结算！");
                }
            }
        } else {
            return ResponseUtil.fail(403, "结算失败！");
        }
        return ResponseUtil.ok();
    }

    @RequiresPermissions("admin:account:withdraw:examine")
    @RequiresPermissionsDesc(menu = {"财务管理", "提现管理"}, button = "审核")
    @PostMapping("/examine")
    public Object examine(@RequestBody FcAccountWithdrawBill withdrawBill, @RequestParam String operationPWd) throws Exception {

        Object error = validate(operationPWd);
        if (error != null) {
            return error;
        }
        if (null != withdrawBill.getAuditFlag()) {


            //审核通过
            if (withdrawBill.getAuditFlag() == 1) {
                withdrawBill.setWithdrawState("WAIT_PAY");
                withdrawBill.setCheckDate(LocalDateTime.now());
                withdrawBill.setValCode("");
                int result =
                        accountService.updateFcAccountWithdrawBillExamine(withdrawBill);
                if (result > 0) {
                    //消息推送
                    DateTimeFormatter dtf2 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                    String time = dtf2.format(withdrawBill.getWithdrawDate());
                    String content = "您的提现申请（申请时间/金额：" + time + "/¥" + withdrawBill.getAmt() + "）已审核通过,待财务打款，请耐心等待。";

                    PushUtils.pushMessage(withdrawBill.getCustomerId(), "提现审核通过", content, 3, 2);
                    return ResponseUtil.ok("操作成功");
                } else {
                    return ResponseUtil.fail();
                }
            }
            //已驳回
            else if (withdrawBill.getAuditFlag() == 0) {
                withdrawBill.setWithdrawState("REJECT");
                withdrawBill.setCheckDate(LocalDateTime.now());
                withdrawBill.setValCode("");
                FcAccount fcAccount = accountService.getAccountByAccountId(withdrawBill.getAccountId());
                Map paramsMap = new HashMap();

                paramsMap.put("capitalTrend", AccountStatus.CAPITAL_TREND_01);
                paramsMap.put("changeType", AccountStatus.CHANGE_TYPE_01);
                //驳回之后，返回金额
                if (null != fcAccount) {
                    //零钱提现 ，返账户1
                    if ("01".equals(withdrawBill.getWithdrawType())) {
                        paramsMap.put("dType", AccountStatus.DISTRIBUTION_TYPE_00);  //直接分润
                        paramsMap.put("message", "收益零钱提现驳回入账");
                        paramsMap.put("changeAccountType", AccountStatus.CHANGE_ACCOUNT_TYPE_1);
                        paramsMap.put("tradeType", AccountStatus.TRADE_TYPE_06);
                        logger.info("---零钱提现申请驳回----------" + withdrawBill.getAmt());
                        if (withdrawBill.getAmt().compareTo(new BigDecimal(0.00)) == 1) {
                            AccountUtils.accountChange(withdrawBill.getCustomerId(), withdrawBill.getAmt(), CommonConstant.TASK_REWARD_CASH, null, paramsMap);
                        }
                    }
                    //任务提现，返账户2
                    else if ("02".equals(withdrawBill.getWithdrawType())) {
                        paramsMap.put("dType", AccountStatus.DISTRIBUTION_TYPE_00);  //直接分润
                        paramsMap.put("message", "任务提现驳回入账");
                        paramsMap.put("changeAccountType", AccountStatus.CHANGE_ACCOUNT_TYPE_2);
                        paramsMap.put("tradeType", AccountStatus.TRADE_TYPE_05);
                        logger.info("---任务提现申请驳回----------" + withdrawBill.getAmt());
                        if (withdrawBill.getAmt().compareTo(new BigDecimal(0.00)) == 1) {
                            AccountUtils.accountChangeByExchange(withdrawBill.getCustomerId(), withdrawBill.getAmt(), CommonConstant.TASK_REWARD_CASH, null, paramsMap);
                        }
                    }
                    //代理提现，返账户3
                    else if ("03".equals(withdrawBill.getWithdrawType())) {
                        paramsMap.put("dType", AccountStatus.DISTRIBUTION_TYPE_10);  //间接分润
                        paramsMap.put("message", "代理端提现驳回入账");
                        paramsMap.put("changeAccountType", AccountStatus.CHANGE_ACCOUNT_TYPE_3);
                        paramsMap.put("tradeType", AccountStatus.TRADE_TYPE_06);
                        logger.info("---任务提现申请驳回----------" + withdrawBill.getAmt());
                        if (withdrawBill.getAmt().compareTo(new BigDecimal(0.00)) == 1) {
                            AccountUtils.accountChange(withdrawBill.getCustomerId(), withdrawBill.getAmt(), CommonConstant.TASK_REWARD_CASH, null, paramsMap);
                        }
                    }
                    //消息推送
                    DateTimeFormatter dtf2 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                    String time = dtf2.format(withdrawBill.getWithdrawDate());
                    String content = "";
                    if (null != withdrawBill.getAuditResult() && !"".equals(withdrawBill.getAuditResult())) {
                        content = "您的提现申请（申请时间/金额：" + time + "/¥" + withdrawBill.getAmt() + "）已驳回，驳回原因：" + withdrawBill.getAuditResult() + "；对应提现金额已返回您的钱包对应账户。";
                    } else {
                        content = "您的提现申请（申请时间/金额：" + time + "/¥" + withdrawBill.getAmt() + "）已驳回，驳回原因：无；对应提现金额已返回您的钱包对应账户。";
                    }
                    int result =
                            accountService.updateFcAccountWithdrawBillExamine(withdrawBill);
                    if (result > 0) {
                        PushUtils.pushMessage(withdrawBill.getCustomerId(), "提现审核驳回", content, 3, 2);
                    }
                }
            }
        } else {
            return ResponseUtil.fail(403, "请选择审核状态！");
        }
//        withdrawBill.setUpdateDate(LocalDateTime.now());
        return ResponseUtil.ok();
    }

    @RequiresPermissions("admin:account:withdraw:examineList")
    @RequiresPermissionsDesc(menu = {"财务管理", "提现管理"}, button = "批量审核")
    @PostMapping("/examineList")
    public Object examineList(@RequestBody List<FcAccountWithdrawBill> withdrawBillList, @RequestParam String operationPWd) throws Exception {

        Object error = validate(operationPWd);
        if (error != null) {
            return error;
        }
        for(FcAccountWithdrawBill withdrawBill :withdrawBillList){
            if (null != withdrawBill.getAuditFlag()) {

                //审核通过
                if (withdrawBill.getAuditFlag() == 1) {
                    withdrawBill.setWithdrawState("WAIT_PAY");
                    withdrawBill.setCheckDate(LocalDateTime.now());
                    withdrawBill.setValCode("");
                    int result =
                            accountService.updateFcAccountWithdrawBillExamine(withdrawBill);
                    if (result > 0) {
                        //消息推送
                        DateTimeFormatter dtf2 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                        String time = dtf2.format(withdrawBill.getWithdrawDate());
                        String content = "您的提现申请（申请时间/金额：" + time + "/¥" + withdrawBill.getAmt() + "）已审核通过,待财务打款，请耐心等待。";

                        PushUtils.pushMessage(withdrawBill.getCustomerId(), "提现审核通过", content, 3, 2);
                    } else {
                        return ResponseUtil.fail();
                    }
                }
                //已驳回
                else if (withdrawBill.getAuditFlag() == 0) {
                    withdrawBill.setWithdrawState("REJECT");
                    withdrawBill.setCheckDate(LocalDateTime.now());
                    withdrawBill.setValCode("");
                    FcAccount fcAccount = accountService.getAccountByAccountId(withdrawBill.getAccountId());
                    Map paramsMap = new HashMap();

                    paramsMap.put("capitalTrend", AccountStatus.CAPITAL_TREND_01);
                    paramsMap.put("changeType", AccountStatus.CHANGE_TYPE_01);
                    //驳回之后，返回金额
                    if (null != fcAccount) {
                        //零钱提现 ，返账户1
                        if ("01".equals(withdrawBill.getWithdrawType())) {
                            paramsMap.put("dType", AccountStatus.DISTRIBUTION_TYPE_00);  //直接分润
                            paramsMap.put("message", "收益零钱提现驳回入账");
                            paramsMap.put("changeAccountType", AccountStatus.CHANGE_ACCOUNT_TYPE_1);
                            paramsMap.put("tradeType", AccountStatus.TRADE_TYPE_06);
                            logger.info("---零钱提现申请驳回----------" + withdrawBill.getAmt());
                            if (withdrawBill.getAmt().compareTo(new BigDecimal(0.00)) == 1) {
                                AccountUtils.accountChange(withdrawBill.getCustomerId(), withdrawBill.getAmt(), CommonConstant.TASK_REWARD_CASH, null, paramsMap);
                            }
                        }
                        //任务提现，返账户2
                        else if ("02".equals(withdrawBill.getWithdrawType())) {
                            paramsMap.put("dType", AccountStatus.DISTRIBUTION_TYPE_00);  //直接分润
                            paramsMap.put("message", "任务提现驳回入账");
                            paramsMap.put("changeAccountType", AccountStatus.CHANGE_ACCOUNT_TYPE_2);
                            paramsMap.put("tradeType", AccountStatus.TRADE_TYPE_05);
                            logger.info("---任务提现申请驳回----------" + withdrawBill.getAmt());
                            if (withdrawBill.getAmt().compareTo(new BigDecimal(0.00)) == 1) {
                                AccountUtils.accountChangeByExchange(withdrawBill.getCustomerId(), withdrawBill.getAmt(), CommonConstant.TASK_REWARD_CASH, null, paramsMap);
                            }
                        }
                        //代理提现，返账户3
                        else if ("03".equals(withdrawBill.getWithdrawType())) {
                            paramsMap.put("dType", AccountStatus.DISTRIBUTION_TYPE_10);  //间接分润
                            paramsMap.put("message", "代理端提现驳回入账");
                            paramsMap.put("changeAccountType", AccountStatus.CHANGE_ACCOUNT_TYPE_3);
                            paramsMap.put("tradeType", AccountStatus.TRADE_TYPE_06);
                            logger.info("---任务提现申请驳回----------" + withdrawBill.getAmt());
                            if (withdrawBill.getAmt().compareTo(new BigDecimal(0.00)) == 1) {
                                AccountUtils.accountChange(withdrawBill.getCustomerId(), withdrawBill.getAmt(), CommonConstant.TASK_REWARD_CASH, null, paramsMap);
                            }
                        }
                        //消息推送
                        DateTimeFormatter dtf2 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                        String time = dtf2.format(withdrawBill.getWithdrawDate());
                        String content = "";
                        if (null != withdrawBill.getAuditResult() && !"".equals(withdrawBill.getAuditResult())) {
                            content = "您的提现申请（申请时间/金额：" + time + "/¥" + withdrawBill.getAmt() + "）已驳回，驳回原因：" + withdrawBill.getAuditResult() + "；对应提现金额已返回您的钱包对应账户。";
                        } else {
                            content = "您的提现申请（申请时间/金额：" + time + "/¥" + withdrawBill.getAmt() + "）已驳回，驳回原因：无；对应提现金额已返回您的钱包对应账户。";
                        }
                        int result =
                                accountService.updateFcAccountWithdrawBillExamine(withdrawBill);
                        if (result > 0) {
                            PushUtils.pushMessage(withdrawBill.getCustomerId(), "提现审核驳回", content, 3, 2);
                        }
                    }
                }
            } else {
                return ResponseUtil.fail(403, "请选择审核状态！");
            }
        }
//        withdrawBill.setUpdateDate(LocalDateTime.now());
        return ResponseUtil.ok();
    }

    @RequiresPermissions("admin:account:withdraw:payment")
    @RequiresPermissionsDesc(menu = {"财务管理", "提现管理"}, button = "打款")
    @PostMapping("/payment")
    public Object payment(@RequestBody FcAccountWithdrawBill withdrawBill, @RequestParam String operationPWd) {

        try {
            Object error = validate(operationPWd);
            if (error != null) {
                return error;
            }

            String hsAppletsAppId = wxProperties.getMiniActivityAppId();
            String userId = withdrawBill.getCustomerId();
            MallUser user = mallUserService.findById(userId);
            String openid = user.getWeixinOpenid();
            String miOpenId = user.getWeixinMiniOpenid();
            String resson = "微信打款";

//        //打款
//        withdrawBill.setWithdrawState("FINISH");
            withdrawBill.setUpdateDate(LocalDateTime.now());
            withdrawBill.setValCode("");
            MutexLock mutexLock = MutexLockUtils.getMutexLock(userId);

            //获取毫秒数
            Long milliSecond = LocalDateTime.now().toInstant(ZoneOffset.of("+8")).toEpochMilli();
            synchronized (mutexLock) {
//            {"msg":"提现成功","code":0}
                Map map = null;
                //app，微信打款

                if ("3".equals(withdrawBill.getWithdrawAccountType())) {
                    if ("01".equals(withdrawBill.getWithdrawType()) || "02".equals(withdrawBill.getWithdrawType()) ||"03".equals(withdrawBill.getWithdrawType())) {
                        map = wxEnterpriseTransferService.transfer2SmallChange(null, openid, withdrawBill.getBillId(), withdrawBill.getAmt().toString(), resson);
                    }
//                    //代理端，微信打款
//                    else if ("03".equals(withdrawBill.getWithdrawType())) {
//                        map = wxEnterpriseTransferService.transfer2SmallChange(miAppid, miOpenId, withdrawBill.getBillId(), withdrawBill.getAmt().toString(), resson);
//                    }
                    //小程序，微信打款
                    else if ("04".equals(withdrawBill.getWithdrawType()) || "05".equals(withdrawBill.getWithdrawType())) {
                        map = wxEnterpriseTransferService.transfer2SmallChange(hsAppletsAppId, miOpenId, withdrawBill.getBillId(), withdrawBill.getAmt().toString(), resson);
                    }
                }
                //支付宝打款
                else if ("2".equals(withdrawBill.getWithdrawAccountType())) {
                    withdrawBill.setWithdrawState("FINISH");
                    if (accountService.updateFcAccountWithdrawBillExamine(withdrawBill) > 0) {
                        return ResponseUtil.ok("操作成功");
                    } else {
                        return ResponseUtil.fail();
                    }
                }

                logger.error("返回结果+" + map.toString());
                if ((int) map.get("code") == 0) {
                    Map<String,Object> maps = (Map) map.get("errmsg");
                    String paymentNo =(String) maps.get("payment_no");
                    //打款
                    withdrawBill.setWithdrawState("FINISH");
                    /**打款单号*/
                    withdrawBill.setPaymentNo(paymentNo);
                    if (accountService.updateFcAccountWithdrawBillExamine(withdrawBill) > 0) {
                        //消息推送
                        DateTimeFormatter dtf2 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                        String time = dtf2.format(withdrawBill.getWithdrawDate());
                        String content = "您的提现申请（申请时间/金额：" + time + "/¥" + withdrawBill.getAmt() + "）已成功打款，请登录对应提现账户进行查收。";
                        PushUtils.pushMessage(withdrawBill.getCustomerId(), "提现到账", content, 3, 2);
                        return ResponseUtil.ok("操作成功");
                    } else {
                        return ResponseUtil.fail();
                    }
                } else {
                    // {"code":-1,"errmsg":"余额不足"}
                    // 返回结果+{code=-1, errmsg=非实名用户账号不可发放}
                    String str = map.get("errmsg").toString();
                    // 记录操作失败原因
                    /*String note = withdrawBill.getNote();
                    if (note.length() > 100) {
                        note = note.substring(0, 100);
                    }*/
                    withdrawBill.setNote(str);
                    accountService.updateFcAccountWithdrawBillExamine(withdrawBill);

                    if(str.contains("操作异常")){
                        return ResponseUtil.fail(403, "操作异常，"+ str);
                    }else if(str.contains("余额不足")){
                        return ResponseUtil.fail(403, "微信余额不足！");
                    }else{
                        logger.error("失败:" + map.toString());
                        return ResponseUtil.fail(403, "失败！原因："+ str);
                    }
                }
            }
        } catch (Exception ex){
            logger.error("提现管理打款出现异常："+ex.getMessage(), ex);
        }

        return ResponseUtil.fail(403, "操作未知结果！");
    }

    @RequiresPermissions("admin:account:withdraw:paymentList")
    @RequiresPermissionsDesc(menu = {"财务管理", "提现管理"}, button = "批量打款")
    @PostMapping("/paymentList")
    public Object paymentList(@RequestBody List<FcAccountWithdrawBill> withdrawBillList, @RequestParam String operationPWd,@RequestParam String type) {

        try {
            /**打款小计*/
            if(StringUtils.isNotEmpty(type) && type.equals("1")){
                List<Map<String,Object>> list =new ArrayList<>();
                //获取需要打款记录中有多少人，相同人去重
                List<String> customerIds = withdrawBillList.stream().map(FcAccountWithdrawBill :: getCustomerId).distinct().collect(Collectors.toList());
                for(String customerId : customerIds){
                    Map<String,Object> mapList = new HashMap<>();
                    //获取同一人的记录
                    List<FcAccountWithdrawBill> bills = withdrawBillList.stream().filter(withdrawBill -> withdrawBill.getCustomerId().equals(customerId) ).collect(Collectors.toList());
                    //拿到同一人提现金额
                    List<BigDecimal> amts = bills.stream().map(FcAccountWithdrawBill :: getAmt).collect(Collectors.toList());
                    //同一人提现金额合并
                    BigDecimal amtSum = getSum(amts);
                    if(StringUtils.isNotEmpty(bills.get(0).getCustName())){
                        mapList.put("userName",bills.get(0).getCustName());
                    }else{
                        mapList.put("userName",bills.get(0).getUserName());
                    }
                    mapList.put("amount",amtSum);
                    list.add(mapList);
                }
                return ResponseUtil.ok(list);
            }

            /*校验验证码*/
            Object error = validate(operationPWd);
            if (error != null) {
                return error;
            }
        if(withdrawBillList.size()>0){
            List<Map<String,Object>> listMap = new ArrayList<>();
            //获取需要打款记录中有多少人，相同人去重
            List<String> accountIds = withdrawBillList.stream().map(FcAccountWithdrawBill :: getAccountId).distinct().collect(Collectors.toList());
            for(String accountId : accountIds){
                Map<String,Object> callMap = new HashMap<>();
                //获取同一人的记录
                  List<FcAccountWithdrawBill> bills = withdrawBillList.stream().filter(withdrawBill -> withdrawBill.getAccountId().equals(accountId) ).collect(Collectors.toList());
                  //拿到同一人提现金额
                  List<BigDecimal> amts = bills.stream().map(FcAccountWithdrawBill :: getAmt).collect(Collectors.toList());
                  //同一人提现金额合并
                  BigDecimal amtSum = getSum(amts);
                  callMap.put("amount",amtSum);
                  if(StringUtils.isNotEmpty(bills.get(0).getCustName())){
                      callMap.put("userName",bills.get(0).getCustName());
                  }else{
                      callMap.put("userName",bills.get(0).getUserName());
                  }
                  //小程序appid
                  String hsAppletsAppId = wxProperties.getHsAppletsAppId();
                  MallUser user = mallUserService.findById(bills.get(0).getCustomerId());
                  if(null == user){
                      return  ResponseUtil.fail("该用户不存在，请确定用户是否被删除！");
                  }
                  String openid = user.getWeixinOpenid();
                  String miOpenId = user.getWeixinMiniOpenid();
                  String resson = "微信打款";
                  MutexLock mutexLock = MutexLockUtils.getMutexLock(bills.get(0).getCustomerId());
                  synchronized (mutexLock) {
                      //{"msg":"提现成功","code":0}
                      Map map = null;
                      String orderId = bills.get(0).getBillId();
                      //加个随机数防第一次打款失败第二次不能打
                      int i = (int) (Math.random()*99+1);
                      /**app微信有授权就用微信打款，没有就用小程序打款*/
                      if(!StringUtils.isEmpty(openid) ){
                          //app，微信打款
                          map = wxEnterpriseTransferService.transfer2SmallChange(null, openid, orderId+i, amtSum.toString(), resson);
                      }else if(StringUtils.isEmpty(openid) && !StringUtils.isEmpty(miOpenId)){
                          map = wxEnterpriseTransferService.transfer2SmallChange(hsAppletsAppId, miOpenId, orderId+i, amtSum.toString(), resson);
                      }

                      //todo 支付宝打款暂不支持，待开发

                      logger.error("返回结果+" + map.toString());
                      if ((int) map.get("code") == 0) {
                          Map<String,Object> maps = (Map) map.get("errmsg");
                          String paymentNo =(String) maps.get("payment_no");
                          String paymentTime =(String) maps.get("payment_time");
                          Map<String,String> payment = new HashMap<>();
                          payment.put("payment_no",paymentNo);
                          payment.put("payment_time",paymentTime);
                          //打款成功
                          updateBill(bills,payment,null);
                          FcAccountPaymentBill paymentBill = new FcAccountPaymentBill();
                          paymentBill.setAccountId(bills.get(0).getAccountId());
                          paymentBill.setPaymentNo(paymentNo);
                          paymentBill.setPaymentTime(paymentTime);
                          paymentBill.setCreateDate(LocalDateTime.now());
                          paymentBill.setUpdateDate(LocalDateTime.now());
                          paymentBill.setAmt(amtSum);
                          paymentBill.setCustomerId(bills.get(0).getCustomerId());
                          paymentBill.setUserName(bills.get(0).getUserName());
                          paymentBill.setCustName(bills.get(0).getCustName());
                          paymentBill.setMobile(bills.get(0).getMobile());
                          paymentBill.setNote(bills.get(0).getNote());
                          /** 1:银行卡 2:支付宝 3:微信 */
                          paymentBill.setPaymentAccountType("3");
                          //拿到同一人提现金额
                          List<String> bill = bills.stream().map(FcAccountWithdrawBill :: getBillId).collect(Collectors.toList());
                          paymentBill.setWithdrawBillId(bill.toString());
                          paymentBillService.save(paymentBill);
                          //打款状态
                          callMap.put("status",1);
                          callMap.put("result","打款成功");
                      } else {
                          // {"code":-1,"errmsg":"余额不足"}
                          // 返回结果+{code=-1, errmsg=非实名用户账号不可发放}
                          String str = map.get("errmsg").toString();

//                          accountService.updateFcAccountWithdrawBillExamine(withdrawBill);
                          //打款失败
                          updateBill(bills,null,str);
                          //打款状态
                          callMap.put("status",0);
                          if(str.contains("操作异常")){
                              callMap.put("result","打款失败："+str);
                          }else if(str.contains("余额不足")){
                              callMap.put("result","打款失败：微信余额不足！");
                          }else{
                              logger.error("失败:" + map.toString());
                              callMap.put("result","打款失败："+str);
                          }
                      }

                  }
                  listMap.add(callMap);
              }
            return  ResponseUtil.ok(listMap);
            }
        } catch (Exception ex){
            logger.error("提现管理打款出现异常："+ex.getMessage(), ex);
        }

        return ResponseUtil.fail(403, "操作未知结果！");
    }

    @RequiresPermissions("admin:account:withdraw:stream:list")
    @RequiresPermissionsDesc(menu = {"财务管理", "账户流水"}, button = "查询")
    @GetMapping("/stream/list")
    public Object listStream(@RequestParam(required = false) List<String> tradeType, String billId,String rechangeType, String orderId, @RequestParam(required = false) String customerId,
                             @RequestParam(required = false) String withdrawType, String account, String mobile, @RequestParam(required = false) List<String> streamType,
                             @RequestParam(required = false) List<String> streamState, String beginTime, String endTime, @RequestParam(defaultValue = "1") Integer page,
                             @RequestParam(defaultValue = "17") Integer limit, @RequestParam(defaultValue = "") String sort, @Order @RequestParam(defaultValue = "desc") String order) {



        String accountType = "";
        if ("01".equals(withdrawType)) {
            accountType = "1"; // 零钱收益账户
        } else if ("02".equals(withdrawType)) {
            accountType = "2"; // 任务赚钱账户
        } else if ("03".equals(withdrawType)) {
            accountType = "3"; // 代理端账户
        } else if ("05".equals(withdrawType)) {
            accountType = "";// 其它公用
        }

        List<MallUser> userList = new ArrayList<MallUser>();
        if (!StringUtils.isEmpty(account)) {
            userList = mallUserService.qrueyUser(account);
            if (userList.size() > 0) {
                customerId = userList.get(0).getId();
            }
        }

        //Map<String, Object> streamList = accountService.queryAccountStream(orderId,billId, tradeType, accountType, customerId, "1", account, streamType, streamState, beginTime, endTime, page, limit, sort, order);
        Map<String, Object> data = new HashMap<>();
        long total = 0;
        try {
            PageHelper.startPage(page, limit);
            List<Map<String, Object>> streamList = accountService.selectStreamBillV2(orderId, billId,rechangeType, tradeType, accountType, customerId, "1", streamType, streamState, beginTime,
                    endTime, (page - 1) * limit, limit, sort, order);

            if (CollectionUtils.isEmpty(streamList)) {
                data.put("total", total);
                data.put("items", streamList);
                return data;
            }

            long pnow = System.currentTimeMillis();
            logger.info("查询开始时间pnow " + pnow);
            if (CollectionUtils.isNotEmpty(streamList)) {
                total = PageInfo.of(streamList).getTotal();
            }

            logger.info("查询开始时间结束now " + (System.currentTimeMillis() - pnow));
            if (CollectionUtils.isEmpty(userList) && CollectionUtils.isNotEmpty(streamList)) {
                List<String> userIdList = streamList.stream().map(mp -> MapUtils.getString(mp, "userId")).filter(Objects::nonNull).collect(Collectors.toList());
                if (CollectionUtils.isNotEmpty(userIdList)) {
                    logger.info("获取userId: " + JacksonUtils.bean2Jsn(userIdList));
                    // userList = userIWorkerService.getMallUserList(userIdList);
                    userList = mallUserService.findByIds(userIdList);
                }
            }

            for (int i = 0; i < streamList.size(); i++) {
                Map<String, Object> map = streamList.get(i);
                String userId = MapUtils.getString(map, "userId");
                Optional<MallUser> cartOptional = userList.stream().filter(item -> item.getId().equals(userId)).findFirst();
                if (cartOptional.isPresent()) {
                    MallUser malluser = cartOptional.get();

                    map.put("nickname", malluser.getNickname());
                    map.put("avatar", malluser.getAvatar());
                    map.put("leveId", malluser.getLevelId());
                    map.put("id", malluser.getId());
                    map.put("accountNo", malluser.getMobile());
                }
            }

            data.put("total", total);
            data.put("items", streamList);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            PageHelper.clearPage();
        }
        return ResponseUtil.ok(data);
    }


    @RequiresPermissions("admin:account:withdraw:stream:list")
    @RequiresPermissionsDesc(menu = {"财务管理", "公司账户流水"}, button = "查询")
    @GetMapping("/platform/list")
    public Object platformListStream(@RequestParam(required = false) List<String> tradeType,
                                     String billId,
                                     String orderId,
                                     @RequestParam(required = false) String customerId,
                                     @RequestParam(required = false) String withdrawType,
                                     String account,
                                     @RequestParam(required = false) List<String> streamType,
                                     @RequestParam(required = false) List<String> streamState,
                                     String beginTime, String endTime,
                                     @RequestParam(defaultValue = "1") Integer page,
                                     @RequestParam(defaultValue = "10") Integer limit,
                                     @RequestParam(defaultValue = "") String sort,
                                     @Order @RequestParam(defaultValue = "desc") String order) {


        String accountType = "";
        if ("01".equals(withdrawType)) {
            accountType = "00"; //公司
        } else if ("02".equals(withdrawType)) {
            accountType = "08"; //共同体
        }
        List<String> reType = new ArrayList<>();
                if( null != tradeType && tradeType.size()>0){
                    for(String type :tradeType){
                        if ("01".equals(type)) {
                            reType.add("27");
                            reType.add("36");
                        } else if ("02".equals(type)) {
                            reType.add("4");
                            reType.add("5");
                            reType.add("8");
                            reType.add("15");
                            reType.add("16");
                            reType.add("17");
                            reType.add("18");
                        } else if ("03".equals(type)) {
                            reType.add("9");
                        } else if ("04".equals(type)) {
                            reType.add("38");
                        } else if ("05".equals(type)) {
                           //
                        }
                    }
                }

        List<Map> streamList =
                accountService.queryPlatFormAccountStream(orderId,billId, reType, accountType, customerId,  account, streamType, streamState,
                        beginTime, endTime, page, limit);
        long total = PageInfo.of(streamList).getTotal();

        Map<String, Object> data = new HashMap<>();
        data.put("total", total);
        data.put("items", streamList);

        return ResponseUtil.ok(data);
    }


    @RequiresPermissions("admin:account:withdraw:rule:list")
    @RequiresPermissionsDesc(menu = {"财务管理", "提现规则"}, button = "查询")
    @GetMapping("/rule/list")
    public Object listRule(@RequestParam(required = false) String ruleType,
                           @RequestParam(defaultValue = " ") String name,
                           @RequestParam(defaultValue = "1") Integer page,
                           @RequestParam(defaultValue = "10") Integer limit,
                           @RequestParam(defaultValue = "") String sort,
                           @Order @RequestParam(defaultValue = "desc") String order) {

        WithdrawalRuleVo ruleVo = new WithdrawalRuleVo();
        WithdrawalRuleVo ruleVo1 = new WithdrawalRuleVo();
        MallSystem mallSystem = new MallSystem();
        mallSystem.setKeyName("Mall_withdrawal_%");
        List<MallSystem> list = mallSystemConfigService.getSystemList(mallSystem);
        for (MallSystem system : list) {
            String key = system.getKeyName();
            String value = system.getKeyValue();
            if (key.contains("Mall_withdrawal_task_rule_")) {
                switch (key) {
                    case "Mall_withdrawal_task_rule_number":
                        ruleVo.setNumber(value);
                        break;
                    case "Mall_withdrawal_task_rule_ruleType":
                        ruleVo.setRuleType(value);
                        break;
                    case "Mall_withdrawal_task_rule_amount":
                        ruleVo.setAmount(value);
                        break;
                    case "Mall_withdrawal_task_rule_amount1":
                        ruleVo.setAmount1(value);
                        break;
                    case "Mall_withdrawal_task_rule_examine":
                        ruleVo.setExamine(value);
                        break;
                    case "Mall_withdrawal_task_rule_minAmount":
                        ruleVo.setMinAmount(value);
                        break;
                    case "Mall_withdrawal_task_rule_maxAmount":
                        ruleVo.setMaxAmount(value);
                        break;
                    case "Mall_withdrawal_task_rule_commission":
                        ruleVo.setCommission(value);
                        break;
                    case "Mall_withdrawal_task_rule_commType":
                        ruleVo.setCommType(value);
                        break;
                    case "Mall_withdrawal_task_rule_detail":
                        ruleVo.setDetail(value);
                        break;
                }
            } else if (key.contains("Mall_withdrawal_profit_rule_")) {
                switch (key) {
                    case "Mall_withdrawal_profit_rule_number":
                        ruleVo1.setNumber(value);
                        break;
                    case "Mall_withdrawal_profit_rule_ruleType":
                        ruleVo1.setRuleType(value);
                        break;
                    case "Mall_withdrawal_profit_rule_amount":
                        ruleVo1.setAmount(value);
                        break;
                    case "Mall_withdrawal_profit_rule_minAmount":
                        ruleVo1.setMinAmount(value);
                        break;
                    case "Mall_withdrawal_profit_rule_maxAmount":
                        ruleVo1.setMaxAmount(value);
                        break;
                    case "Mall_withdrawal_profit_rule_commission":
                        ruleVo1.setCommission(value);
                        break;
                    case "Mall_withdrawal_profit_rule_commType":
                        ruleVo1.setCommType(value);
                        break;
                    case "Mall_withdrawal_profit_rule_detail":
                        ruleVo1.setDetail(value);
                        break;
                }
            }
        }

        List list1 = new ArrayList();
        if (null != ruleVo1.getRuleType()) {
            list1.add(ruleVo1);
        }
        if (null != ruleVo.getRuleType()) {
            list1.add(ruleVo);
        }
        long total = list1.size();
        Map<String, Object> map = new HashMap<>();
        map.put("items", list1);
        map.put("total", total);
        List list2 = new ArrayList();
        if (StringUtils.isEmpty(ruleType) && " ".equals(name)) {
            return ResponseUtil.ok(map);
        } else if ("1".equals(ruleType) || "收益零钱提现".contains(name)) {
            list2.add(ruleVo1);
            Map<String, Object> map1 = new HashMap<>();
            map1.put("items", list2);
            map1.put("total", list2.size());
            return ResponseUtil.ok(map1);
        } else if ("2".equals(ruleType) || "任务兑换提现".contains(name)) {
            list2.add(ruleVo);
            Map<String, Object> map2 = new HashMap<>();
            map2.put("items", list2);
            map2.put("total", list2.size());
            return ResponseUtil.ok(map2);
        }
        return ResponseUtil.ok();
    }

    @RequiresPermissions("admin:account:withdraw:rule:createRule")
    @RequiresPermissionsDesc(menu = {"财务管理", "提现规则"}, button = "创建")
    @PostMapping("/rule/createRule")
    public Object createRule(@RequestBody WithdrawalRuleVo withdrawalRuleVo) {

        Map<String, String> map = new HashMap<>();
        MallSystem mallSystem = new MallSystem();
        mallSystem.setKeyName("Mall_withdrawal_%");
        List<MallSystem> list = mallSystemConfigService.getSystemList(mallSystem);
        for (MallSystem system : list) {
            String key = system.getKeyName();
            String value = system.getKeyValue();

            switch (key) {
                case "Mall_withdrawal_profit_rule_ruleType":
                    if (withdrawalRuleVo.getRuleType().equals(value))
                        return ResponseUtil.fail(402, "该类型规则已存在，请修改之前规则即可！");
                    break;
                case "Mall_withdrawal_task_rule_ruleType":
                    if (withdrawalRuleVo.getRuleType().equals(value))
                        return ResponseUtil.fail(402, "该类型规则已存在，请修改之前规则即可！");
                    break;
            }

        }
        //收益零钱提现
        if ("1".equals(withdrawalRuleVo.getRuleType())) {
            map.put("Mall_withdrawal_profit_rule_number", StringUtils.isEmpty(withdrawalRuleVo.getNumber()) ? "1" : withdrawalRuleVo.getNumber());
            map.put("Mall_withdrawal_profit_rule_ruleType", withdrawalRuleVo.getRuleType());
            map.put("Mall_withdrawal_profit_rule_amount", StringUtils.isEmpty(withdrawalRuleVo.getAmount()) ? "0.1" : withdrawalRuleVo.getAmount());
            map.put("Mall_withdrawal_profit_rule_minAmount", StringUtils.isEmpty(withdrawalRuleVo.getMinAmount()) ? "0.01" : withdrawalRuleVo.getMinAmount());
            map.put("Mall_withdrawal_profit_rule_maxAmount", StringUtils.isEmpty(withdrawalRuleVo.getMaxAmount()) ? "5000" : withdrawalRuleVo.getMaxAmount());
            map.put("Mall_withdrawal_profit_rule_commission", StringUtils.isEmpty(withdrawalRuleVo.getCommission()) ? "0" : withdrawalRuleVo.getCommission());
            map.put("Mall_withdrawal_profit_rule_commType", withdrawalRuleVo.getCommType());
            map.put("Mall_withdrawal_profit_rule_detail", withdrawalRuleVo.getDetail());
            mallSystemConfigService.insertConfig(map);

        }
        //任务兑换提现
        else if ("2".equals(withdrawalRuleVo.getRuleType())) {
            map.put("Mall_withdrawal_task_rule_number", withdrawalRuleVo.getNumber());
            map.put("Mall_withdrawal_task_rule_ruleType", withdrawalRuleVo.getRuleType());
            map.put("Mall_withdrawal_task_rule_amount", withdrawalRuleVo.getAmount());
            map.put("Mall_withdrawal_task_rule_amount1", withdrawalRuleVo.getAmount1());
            map.put("Mall_withdrawal_task_rule_examine", withdrawalRuleVo.getExamine());
            map.put("Mall_withdrawal_task_rule_minAmount", withdrawalRuleVo.getMinAmount());
            map.put("Mall_withdrawal_task_rule_maxAmount", withdrawalRuleVo.getMaxAmount());
            map.put("Mall_withdrawal_task_rule_commission", withdrawalRuleVo.getCommission());
            map.put("Mall_withdrawal_task_rule_commType", withdrawalRuleVo.getCommType());
            map.put("Mall_withdrawal_task_rule_detail", withdrawalRuleVo.getDetail());
            mallSystemConfigService.insertConfig(map);

        }
        //代理端提现(暂时不用)
        else if ("3".equals(withdrawalRuleVo.getRuleType())) {
            map.put("Mall_withdrawal_agent_rule_number", withdrawalRuleVo.getNumber());
            map.put("Mall_withdrawal_agent_rule_ruleType", withdrawalRuleVo.getRuleType());
            map.put("Mall_withdrawal_agent_rule_amount", withdrawalRuleVo.getAmount());
            map.put("Mall_withdrawal_agent_rule_minAmount", withdrawalRuleVo.getMinAmount());
            map.put("Mall_withdrawal_agent_rule_maxAmount", withdrawalRuleVo.getMaxAmount());
            map.put("Mall_withdrawal_agent_rule_commission", withdrawalRuleVo.getCommission());
            map.put("Mall_withdrawal_agent_rule_commType", withdrawalRuleVo.getCommType());
            map.put("Mall_withdrawal_agent_rule_detail", withdrawalRuleVo.getDetail());
            mallSystemConfigService.insertConfig(map);

        }


        return ResponseUtil.ok();
    }

    @RequiresPermissions("admin:account:withdraw:rule:updateRule")
    @RequiresPermissionsDesc(menu = {"财务管理", "提现规则"}, button = "编辑")
    @PostMapping("/rule/updateRule")
    public Object updateRule(@RequestBody WithdrawalRuleVo withdrawalRuleVo) {

        Map<String, String> map = new HashMap<>();
        //收益零钱提现
        if ("1".equals(withdrawalRuleVo.getRuleType())) {
            map.put("Mall_withdrawal_profit_rule_number", StringUtils.isEmpty(withdrawalRuleVo.getNumber()) ? "1" : withdrawalRuleVo.getNumber());
            map.put("Mall_withdrawal_profit_rule_ruleType", withdrawalRuleVo.getRuleType());
            map.put("Mall_withdrawal_profit_rule_amount", StringUtils.isEmpty(withdrawalRuleVo.getAmount()) ? "0.3" : withdrawalRuleVo.getAmount());
            map.put("Mall_withdrawal_profit_rule_minAmount", StringUtils.isEmpty(withdrawalRuleVo.getMinAmount()) ? "0.3" : withdrawalRuleVo.getMinAmount());
            map.put("Mall_withdrawal_profit_rule_maxAmount", StringUtils.isEmpty(withdrawalRuleVo.getMaxAmount()) ? "5000" : withdrawalRuleVo.getMaxAmount());
            map.put("Mall_withdrawal_profit_rule_commission", StringUtils.isEmpty(withdrawalRuleVo.getCommission()) ? "0" : withdrawalRuleVo.getCommission());
            map.put("Mall_withdrawal_profit_rule_commType", withdrawalRuleVo.getCommType());
            map.put("Mall_withdrawal_profit_rule_detail", withdrawalRuleVo.getDetail());
            mallSystemConfigService.updateConfig(map);

        }
        //任务兑换提现
        else if ("2".equals(withdrawalRuleVo.getRuleType())) {
            map.put("Mall_withdrawal_task_rule_number", withdrawalRuleVo.getNumber());
            map.put("Mall_withdrawal_task_rule_ruleType", withdrawalRuleVo.getRuleType());
            map.put("Mall_withdrawal_task_rule_amount", withdrawalRuleVo.getAmount());
            map.put("Mall_withdrawal_task_rule_amount1", withdrawalRuleVo.getAmount1());
            map.put("Mall_withdrawal_task_rule_examine", withdrawalRuleVo.getExamine());
            map.put("Mall_withdrawal_task_rule_minAmount", withdrawalRuleVo.getMinAmount());
            map.put("Mall_withdrawal_task_rule_maxAmount", withdrawalRuleVo.getMaxAmount());
            map.put("Mall_withdrawal_task_rule_commission", withdrawalRuleVo.getCommission());
            map.put("Mall_withdrawal_task_rule_commType", withdrawalRuleVo.getCommType());
            map.put("Mall_withdrawal_task_rule_detail", withdrawalRuleVo.getDetail());
            mallSystemConfigService.updateConfig(map);

        }
        return ResponseUtil.ok();
    }

    //审核和打款要验证操作密码
    private Object validate(String operationPWd) {
        //获取当前用户
        MallAdmin adminUser = (MallAdmin) SecurityUtils.getSubject().getPrincipal();
        //防止用户刚设置操作密码，没有重新登录，获取不到新的操作密码，所以再去数据库查询一次，确保能获取
        MallAdmin user = mallAdminService.findOperationPwdById(adminUser.getId());

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        if (!encoder.matches(operationPWd, user.getOperationPassword())) {
            return ResponseUtil.fail(ADMIN_INVALID_ACCOUNT, "操作密码有误");
        }
        return null;
    }

    private static BigDecimal getSum(List<BigDecimal> numbers){
        BigDecimal sum = numbers.get(0);

        for(int i=1;i < numbers.size();i++){
            sum = sum.add(numbers.get(i));
        }
        return sum;
    }
    /**打款成功更新流水*/
    public void updateBill(List<FcAccountWithdrawBill> bills,Map<String,String> payment,String essage) throws Exception {
        for(FcAccountWithdrawBill bill : bills){
            if(!MapUtils.isEmpty(payment)){
                bill.setWithdrawState("FINISH");
                bill.setPaymentNo(payment.get("payment_no"));
                DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                LocalDateTime ldt = LocalDateTime.parse(payment.get("payment_time"),df);
                bill.setUpdateDate(ldt);
                bill.setNote("微信打款成功");
                bill.setValCode("");
                if (accountService.updateFcAccountWithdrawBillExamine(bill) > 0) {
                    //消息推送
                    DateTimeFormatter dtf2 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                    String time = dtf2.format(bill.getWithdrawDate());
                    String content = "您的提现申请（申请时间/金额：" + time + "/¥" + bill.getAmt() + "）已成功打款，请登录对应提现账户进行查收。";
                    PushUtils.pushMessage(bill.getCustomerId(), "提现到账", content, 3, 2);
                }
            }else if(!StringUtils.isEmpty(essage)){
                // 记录操作失败原因
//                String note = bill.getNote();
//                if (note.length() > 200) {
//                    note = note.substring(0, 200);
//                }
                bill.setNote("微信打款失败："+essage);
                bill.setValCode("");
                accountService.updateFcAccountWithdrawBillExamine(bill);
            }
        }
    }



}
