package com.graphai.mall.admin.web.user;

import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.mall.db.service.user.MallUserStatisticsService;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 用户统计
 *
 * @author LaoGF
 * @since 2020-07-02
 */
@RestController
@RequestMapping("/admin/userStatistics")
@Validated
public class AdminUserStatisticsController {

    @Resource
    private MallUserStatisticsService mallUserStatisticsService;

    /**
     * 统计用户量
     */
    @RequiresPermissions("admin:userStatistics:userCount")
    @RequiresPermissionsDesc(menu = {"数据统计", "用户统计"}, button = "查询")
    @GetMapping("/userCount")
    public Object statisticsUserCount() {
        return ResponseUtil.ok(mallUserStatisticsService.statisticsUserCountV2());
    }

    /**
     * 统计总用户量
     */
    @GetMapping("/totalUserCount")
    public Object statisticsTotalUserCount() {
        return ResponseUtil.ok(mallUserStatisticsService.statisticsTotalUserCount());
    }

    /**
     * 统计普通用户量
     */
    @GetMapping("/generalUserCount")
    public Object statisticsGeneralUserCount() {
        return ResponseUtil.ok(mallUserStatisticsService.statisticsGeneralUserCount(null));
    }

    /**
     * 统计激活用户量
     */
    @GetMapping("/activateUserCount")
    public Object statisticsActivateUserCount() {
        return ResponseUtil.ok(mallUserStatisticsService.statisticsActivateUserCount(null));
    }

    /**
     * 统计市级代理
     */
    @GetMapping("/cityAgentCount")
    public Object statisticsCityAgentCount() {
        return ResponseUtil.ok(mallUserStatisticsService.statisticsCityAgentCount(null));
    }

    /**
     * 统计区县代理
     */
    @GetMapping("/countyAgentCount")
    public Object statisticsCountyAgentCount() {
        return ResponseUtil.ok(mallUserStatisticsService.statisticsCountyAgentCount(null));
    }

    /**
     * 统计服务商
     */
    @GetMapping("/serviceProviderCount")
    public Object statisticsServiceProviderCount() {
        return ResponseUtil.ok(mallUserStatisticsService.statisticsServiceProviderCount(null));
    }

    /**
     * 统计各渠道用户量
     */
    @GetMapping("/userCountGroupByChannel")
    public Object statisticsUserCountGroupByChannel() {
        return ResponseUtil.ok(mallUserStatisticsService.statisticsUserCountGroupByChannel());
    }

    /**
     * 统计各等级用户量
     */
    @GetMapping("/userCountGroupByLevel")
    public Object statisticsUserCountGroupByLevel() {
        return ResponseUtil.ok(mallUserStatisticsService.statisticsUserCountGroupByLevel());
    }

    /**
     * 数据统计  每日汇总：日期（YYYY-MM-dd）、今日注册量、会员注册量、注册量增速（较上一天）、市级代理新增量，区级代理新增量，服务商新增量
     */
    @GetMapping("/userCountGroupByDay")
    public Object statisticsUserCountGroupByDay(@RequestParam Integer day) {
        return ResponseUtil.ok(mallUserStatisticsService.statisticsUserCountGroupByDayV2(day != null ? day : 7));
    }

    /**
     * 统计用户签到天数排行
     */
    @GetMapping("/signGroupByUser")
    public Object statisticsSignGroupByUser(@RequestParam Integer page, @RequestParam Integer limit) {
        return ResponseUtil.ok(mallUserStatisticsService.statisticsSignGroupByUser(page, limit));
    }

    /**
     * 统计用户日签到量
     */
    @GetMapping("/signGroupByDay")
    public Object statisticsSignGroupByDay(@RequestParam Integer day) {
        return ResponseUtil.ok(mallUserStatisticsService.statisticsSignGroupByDay(day));
    }

    /**
     * 查询用户的签到数据
     */
    @GetMapping("/queryUserSign")
    public Object queryUserSign(@RequestParam(required = false) String nickName,@RequestParam(required = false) String mobile,
            @RequestParam(defaultValue = "1") Integer page,@RequestParam(defaultValue = "20") Integer limit){
        List<Map<String, Object>> maps = mallUserStatisticsService.queryUserSign(nickName, mobile, page, limit);
        long total = PageInfo.of(maps).getTotal();
        HashMap<String, Object>  m = new HashMap<>();
        m.put("list",maps);
        m.put("total",total);
        return ResponseUtil.ok(m);
    }

}
