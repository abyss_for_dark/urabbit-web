package com.graphai.mall.admin.web.promotion;

import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.mall.db.domain.MallActivity;
import com.graphai.mall.db.service.promotion.ActivityService;
import com.graphai.mall.db.util.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/admin/activity")
@Validated
public class AdminActivityController {
    private final Log logger = LogFactory.getLog(AdminActivityController.class);

    @Autowired
    private ActivityService activityService;


    @RequiresPermissions("admin:activity:list")
    @RequiresPermissionsDesc(menu = {"弹窗活动", "活动管理"}, button = "查询")
    @GetMapping("/list")
    public Object list(String activityName,
                       @RequestParam(required = false) Byte place,
                       @RequestParam(required = false) String jumpType,
                       @RequestParam(required = false) String status,
                       @RequestParam(required = false) Boolean enabled,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit) {

        List<MallActivity> list = activityService.selectActivityList(activityName, place, jumpType, status, enabled, page, limit);

        Map<String, Object> map = new HashMap<>();
        map.put("total", PageInfo.of(list).getTotal());
        map.put("item", list);

        return ResponseUtil.ok(map);
    }

    @RequiresPermissions("admin:activity:update")
    @RequiresPermissionsDesc(menu = {"弹窗活动", "活动管理"}, button = "编辑")
    @PostMapping("/update")
    public Object update(@RequestBody MallActivity mallActivity) {
        if (!validate(mallActivity)) {
            return ResponseUtil.badArgument();
        }
        return activityService.updateActivity(mallActivity) > 0 ? ResponseUtil.ok() : ResponseUtil.fail();
    }

    @RequiresPermissions("admin:activity:create")
    @RequiresPermissionsDesc(menu = {"弹窗活动", "活动管理"}, button = "创建")
    @PostMapping("/create")
    public Object create(@RequestBody MallActivity mallActivity) {
        if (!validate(mallActivity)) {
            return ResponseUtil.badArgument();
        }
        return activityService.createActivity(mallActivity) > 0 ? ResponseUtil.ok() : ResponseUtil.fail();
    }

    @RequiresPermissions("admin:activity:isDisable")
    @RequiresPermissionsDesc(menu = {"弹窗活动", "活动管理"}, button = "禁用")
    @PostMapping("/isDisable")
    public Object isDisable(@RequestBody MallActivity mallActivity) {
        if (!validate(mallActivity)) {
            return ResponseUtil.badArgument();
        }
        mallActivity.setEnabled(!mallActivity.getEnabled());
        return activityService.updateActivity(mallActivity) > 0 ? ResponseUtil.ok() : ResponseUtil.fail();
    }

    @RequiresPermissions("admin:activity:deleted")
    @RequiresPermissionsDesc(menu = {"弹窗活动", "活动管理"}, button = "删除")
    @PostMapping("/deleted")
    public Object deleted(@RequestBody MallActivity mallActivity) {
        if (!validate(mallActivity)) {
            return ResponseUtil.badArgument();
        }
        mallActivity.setDeleted(true);
        if (activityService.updateActivity(mallActivity) > 0) {
            return ResponseUtil.ok();
        }
        return ResponseUtil.fail();
    }


    private boolean validate(MallActivity act) {
        if (
                StringUtils.isEmpty(act.getTitle()) ||
                StringUtils.isEmpty(act.getUrl()) ||
                StringUtils.isEmpty(act.getLink()) ||
                StringUtils.isEmpty(act.getStatus()) ||
                act.getPosition() == null ||
                act.getStartTime() == null ||
                act.getEndTime() == null
        ) return false;

        return true;
    }
}