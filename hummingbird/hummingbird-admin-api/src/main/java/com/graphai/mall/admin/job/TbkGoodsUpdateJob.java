package com.graphai.mall.admin.job;

import com.graphai.mall.admin.service.TbkGoodsUpdateService;
import com.graphai.mall.db.config.TbkapiProperties;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;

/**
 * 商品更新API
 * 更新销量等数据
 *
 * @author panxianhuan
 */
//@Component
public class TbkGoodsUpdateJob {

    private final Log logger = LogFactory.getLog(TbkGoodsUpdateJob.class);

    @Autowired
    private TbkGoodsUpdateService tbkGoodsUpdateService;

    @Autowired
    private TbkapiProperties properties;

    //每隔10分钟(10*60*1000ms)执行一次
    @Scheduled(fixedRate = 57 * 60 * 1000)
    public void syscTbkGoodsInvalid() {
        logger.error("TbkGoodsUpdateJob start!");
        int minId = 1;
        tbkGoodsUpdateService.syscTbkGoodsUpdate(minId);
        logger.error("TbkGoodsUpdateJob end!");
    }

}

