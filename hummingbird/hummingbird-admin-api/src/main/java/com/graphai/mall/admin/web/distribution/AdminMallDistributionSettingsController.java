package com.graphai.mall.admin.web.distribution;

import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.mall.db.domain.MallDistributionSettings;
import com.graphai.mall.db.service.distribution.MallDistributionSettingsService;
import com.graphai.validator.Order;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 分销分润设置
 */
@RestController
@RequestMapping("/admin/mallDistributionSettings")
@Validated
public class AdminMallDistributionSettingsController {
    private final Log logger = LogFactory.getLog(AdminMallDistributionSettingsController.class);

    @Autowired
    private MallDistributionSettingsService mallDistributionSettingsService;

    /**
     * 查询分销分润设置
     */
    @GetMapping("/list")
    public Object list(@RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit,
                       @RequestParam(defaultValue = "id") String sort,
                       @Order @RequestParam(defaultValue = "desc") String order) {
        MallDistributionSettings mallDistributionSettings = new MallDistributionSettings();
        List<MallDistributionSettings> list = mallDistributionSettingsService.selectMallDistributionSettingsList(mallDistributionSettings, page, limit, sort, order);
        long total = PageInfo.of(list).getTotal();
        Map<String, Object> data = new HashMap<>();
        data.put("total", total);
        data.put("items", list);

        return ResponseUtil.ok(data);
    }

    /**
     * 添加分销分润设置
     */
    @PostMapping("/add")
    public Object addSave(@RequestBody MallDistributionSettings mallDistributionSettings) {
        Object error = validate(mallDistributionSettings);
        if (error != null) {
            return error;
        }
        mallDistributionSettingsService.insertMallDistributionSettings(mallDistributionSettings);
        return ResponseUtil.ok();
    }


    /**
     * 查看分销分润设置详情
     */
    @GetMapping("/edit/{id}")
    public Object edit(@PathVariable("id") String id) {
        MallDistributionSettings mallDistributionSettings = mallDistributionSettingsService.selectMallDistributionSettingsById(id);
        return ResponseUtil.ok(mallDistributionSettings);
    }

    /**
     * 修改分销分润设置
     */
    @PostMapping("/edit")
    public Object editSave(@RequestBody MallDistributionSettings mallDistributionSettings) {
        //等级主键id
        if (StringUtils.isEmpty(mallDistributionSettings.getId())) {
            return ResponseUtil.badArgumentValue("没有获取到id！");
        }
        Object error = validate(mallDistributionSettings);
        if (error != null) {
            return error;
        }
        mallDistributionSettingsService.updateMallDistributionSettings(mallDistributionSettings);
        return ResponseUtil.ok();
    }


    /**
     * 校验数据(分销分润设置)
     */
    private Object validate(MallDistributionSettings mallDistributionSettings) {
        if (StringUtils.isEmpty(mallDistributionSettings.getCalModel())) {
            return ResponseUtil.badArgument();
        }
        if (StringUtils.isEmpty(mallDistributionSettings.getAccountType())) {
            return ResponseUtil.badArgument();
        }
        if (StringUtils.isEmpty(mallDistributionSettings.getDisIntegralValue())) {
            return ResponseUtil.badArgument();
        }
        if (StringUtils.isEmpty(mallDistributionSettings.getDisProLevel())) {
            return ResponseUtil.badArgument();
        }
        if (StringUtils.isEmpty(mallDistributionSettings.getDisUserType())) {
            return ResponseUtil.badArgument();
        }

        return null;
    }
}
