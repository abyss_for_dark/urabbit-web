package com.graphai.mall.admin.web.promotion;

import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.json.JacksonUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.mall.admin.service.MallSelectionManageService;
import com.graphai.mall.db.domain.MallAdmin;
import com.graphai.mall.db.domain.MallGoods;
import com.graphai.mall.db.domain.MallRole;
import com.graphai.mall.db.domain.MallSelectionManagement;
import com.graphai.mall.db.service.promotion.MallSelectionManagementService;
import com.graphai.mall.db.service.promotion.MallSelectionService;
import com.graphai.validator.Order;
import com.graphai.validator.Sort;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 选品活动-选品管理
 *
 * @author qinxiang
 * @time 2020-07-21 10:10:26
 */
@RestController
@RequestMapping("/admin/selection/management")
@Validated
public class AdminSelectionManagementController {

    private final Log logger = LogFactory.getLog(AdminSelectionManagementController.class);

    @Resource
    private MallSelectionManagementService mallSelectionManagementService;
    @Resource
    private MallSelectionService mallSelectionService;
    @Resource
    private MallSelectionManageService mallSelectionManageService;

    @RequiresPermissions("admin:selection:management:list")
    @RequiresPermissionsDesc(menu = {"选品活动", "选品管理"}, button = "查询")
    @GetMapping("/list")
    public Object list(@RequestParam(required = false) String mallSelectionId,
                       @RequestParam(required = false) Byte source,
                       @RequestParam(required = false) Byte productType,
                       @RequestParam(required = false) Byte state,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit) {
        List<MallSelectionManagement> list = mallSelectionManagementService.selectSelectionManageList(mallSelectionId, source, productType, state, page, limit);
        long total = PageInfo.of(list).getTotal();
        Map<String, Object> dataMap = new HashMap<>();
        dataMap.put("total", total);
        dataMap.put("items", list);
        return ResponseUtil.ok(dataMap);
    }

    @RequiresPermissions("admin:selection:management:update")
    @RequiresPermissionsDesc(menu = {"选品活动", "选品管理"}, button = "编辑")
    @PostMapping("/update")
    public Object update(@RequestBody MallSelectionManagement mallSelectionManagement) {
        Object error = validateUpdate(mallSelectionManagement);
        if (error != null) {
            return error;
        }
        Map<String, String> resMap = mallSelectionManagementService.updateSelectionManagementCommon(mallSelectionManagement);
        if ("0000".equals(resMap.get("code"))) {
            return ResponseUtil.ok();
        }
        return ResponseUtil.fail(402, resMap.get("message"));
    }

    @RequiresPermissions("admin:selection:management:create")
    @RequiresPermissionsDesc(menu = {"选品活动", "选品管理"}, button = "创建")
    @PostMapping("/create")
    public Object create(@RequestBody MallSelectionManagement mallSelectionManagement) {
        Object error = validateCreate(mallSelectionManagement);
        if (error != null) {
            return error;
        }
        Map<String, String> resMap = mallSelectionManagementService.createSelectionManagement(mallSelectionManagement);
        if ("0000".equals(resMap.get("code"))) {
            return ResponseUtil.ok();
        }
        return ResponseUtil.fail(402, resMap.get("message"));
    }

    @RequiresPermissions("admin:selection:management:isDisable")
    @RequiresPermissionsDesc(menu = {"选品活动", "选品管理"}, button = "禁用")
    @PostMapping("/isDisable")
    public Object isDisable(@RequestBody MallSelectionManagement mallSelectionManagement) {
        Object error = validateIsDisable(mallSelectionManagement);
        if (error != null) {
            return error;
        }
        if (mallSelectionManagementService.updateSelectionManagement(mallSelectionManagement) > 0) {
            return ResponseUtil.ok();
        }
        return ResponseUtil.fail();
    }

    @RequiresPermissions("admin:selection:management:deleted")
    @RequiresPermissionsDesc(menu = {"选品活动", "选品管理"}, button = "删除")
    @PostMapping("/deleted")
    public Object deleted(@RequestBody MallSelectionManagement mallSelectionManagement) {
        Object error = validateDeleted(mallSelectionManagement);
        if (error != null) {
            return error;
        }
        mallSelectionManagement.setDeleted(true);
        if (mallSelectionManagementService.updateSelectionManagement(mallSelectionManagement) > 0) {
            return ResponseUtil.ok();
        }
        return ResponseUtil.fail();
    }

    @RequiresPermissions("admin:selection:management:detail")
    @RequiresPermissionsDesc(menu = {"选品活动", "选品管理"}, button = "活动详情")
    @GetMapping("/detail")
    public Object detail(@NotNull String id) {
        return ResponseUtil.ok(mallSelectionManagementService.getActivityManagementDetail(id));
    }

    /**
     * 查询下拉列表-选品活动
     *
     * @return Object
     */
    @GetMapping("/getSelectInList")
    public Object selectInList() {
        return ResponseUtil.ok(mallSelectionService.selectActivityList());
    }

    /**
     * 导入
     *
     * @param file excel文件
     * @return Object
     * @throws Exception 异常
     */
    @PostMapping("/importData")
    @ResponseBody
    public Object importData(MultipartFile file) throws Exception {
        if (null == file) {
            return ResponseUtil.fail(402, "文件不能为空。");
        }
        return mallSelectionManageService.importData(file);
    }


    /**
     * 参数校验-update
     *
     * @param mallSelectionManagement 选品对象
     * @return Object
     */
    private Object validateUpdate(MallSelectionManagement mallSelectionManagement) {
        String id = mallSelectionManagement.getId();
        if (StringUtils.isEmpty(id)) {
            return ResponseUtil.fail(402, "id不能为空。");
        }
        String productId = mallSelectionManagement.getProductId();
        if (StringUtils.isEmpty(productId)) {
            return ResponseUtil.fail(402, "源系统商品id不能为空。");
        }
        return null;
    }

    /**
     * 参数校验-create
     *
     * @param mallSelectionManagement 选品管理
     * @return Object
     */
    private Object validateCreate(MallSelectionManagement mallSelectionManagement) {
        String getMallSelectionId = mallSelectionManagement.getMallSelectionId();
        if (StringUtils.isEmpty(getMallSelectionId)) {
            return ResponseUtil.fail(402, "选品活动ID不能为空。");
        }
        Byte getSource = mallSelectionManagement.getSource();
        if (StringUtils.isEmpty(getSource)) {
            return ResponseUtil.fail(402, "来源不能为空。");
        }
        Byte getProductType = mallSelectionManagement.getProductType();
        if (getProductType == null) {
            return ResponseUtil.fail(402, "商品类型不能为空。");
        }
        String getProductId = mallSelectionManagement.getProductId();
        if (StringUtils.isEmpty(getProductId)) {
            return ResponseUtil.fail(402, "商品ID不能为空。");
        }
        return null;
    }

    /**
     * 参数校验-deleted
     *
     * @param mallSelectionManagement 选品对象
     * @return Object
     */
    private Object validateDeleted(MallSelectionManagement mallSelectionManagement) {
        String id = mallSelectionManagement.getId();
        if (StringUtils.isEmpty(id)) {
            return ResponseUtil.fail(402, "id不能为空。");
        }
        return null;
    }

    /**
     * 参数校验-isDisable
     *
     * @param mallSelectionManagement 选品管理对象
     * @return Object
     */
    private Object validateIsDisable(MallSelectionManagement mallSelectionManagement) {
        String id = mallSelectionManagement.getId();
        if (StringUtils.isEmpty(id)) {
            return ResponseUtil.fail(402, "id不能为空。");
        }
        //状态（0-下架, 1-上架）
        String status = String.valueOf(mallSelectionManagement.getState());
        if (StringUtils.isEmpty(status)) {
            return ResponseUtil.fail(402, "状态不能为空。");
        }
        return null;
    }

    @PostMapping("/addGoods")
    public Object addGoods(@RequestBody Map<String,Object> map) {
        return mallSelectionManageService.addGoods(map);
    }

    @GetMapping("/getGoodsList")
    public Object getGoodsList(String selectionId,String goodsSn, String name, Integer goodsType,
                               String brandName, Integer priceMin, Integer priceMax,
                               Boolean isHot, Boolean isNew, Boolean isOnSale,
                               Integer categorypid, Integer industryId, Boolean isRightGoods,
                               Integer platform,
                               @RequestParam(defaultValue = "1") Integer page,
                               @RequestParam(defaultValue = "10") Integer limit,
                               @Sort @RequestParam(defaultValue = "id") String sort,
                               @Order @RequestParam(defaultValue = "desc") String order) {
        return mallSelectionManageService.getGoodsList(selectionId,goodsSn, name, page, limit, sort, order, brandName,
                priceMin, priceMax, platform, isHot, isNew, isOnSale, categorypid, industryId, goodsType,
                isRightGoods);
    }

    @PostMapping("/delete")
    public Object delete(@RequestBody MallSelectionManagement selectionManagement) {
        return mallSelectionManageService.delete(selectionManagement);
    }

}