package com.graphai.mall.admin.service;

import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.system.service.IGraphaiSystemRoleService;
import com.graphai.mall.admin.vo.CatVo;
import com.graphai.mall.core.qcode.QCodeService;
import com.graphai.mall.db.domain.*;
import com.graphai.mall.db.domain.mall.GoodsAllinone;
import com.graphai.mall.db.service.*;
import com.graphai.mall.db.service.advertisement.MallBrandService;
import com.graphai.mall.db.service.coupon.MallCouponService;
import com.graphai.mall.db.service.goods.MallGoodsAttributeService;
import com.graphai.mall.db.service.goods.MallGoodsProductService;
import com.graphai.mall.db.service.goods.MallGoodsSpecificationService;
import com.graphai.mall.db.service.integral.MallCategoryIntegralService;
import com.graphai.mall.db.service.integral.MallGoodsIntegralService;
import com.graphai.mall.db.service.order.MallOrderGoodsService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import static com.graphai.framework.constant.AdminResponseCode.GOODS_NAME_EXIST;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import com.graphai.mall.admin.dto.GoodsAllinone;

@Service
public class AdminGoodsIntegralService {
    private final Log logger = LogFactory.getLog(AdminGoodsService.class);

    @Autowired
    private MallGoodsIntegralService goodsIntegralService;
    @Autowired
    private MallGoodsSpecificationService specificationService;
    @Autowired
    private MallGoodsAttributeService attributeService;
    @Autowired
    private MallGoodsProductService productService;
    @Autowired
    private MallCategoryIntegralService categoryIntegralService;
    @Autowired
    private MallBrandService brandService;
    @Autowired
    private MallCartService cartService;
    @Autowired
    private MallOrderGoodsService orderGoodsService;

    @Autowired
    private QCodeService qCodeService;
    @Autowired
    private MallCouponService couponService;
    @Autowired
    private IGraphaiSystemRoleService iGraphaiSystemRoleService;

    public Object list(String goodsSn, String name,
                       Integer page, Integer limit, String sort, String order) {
        List<MallGoods> goodsList = goodsIntegralService.querySelective(goodsSn, name, page, limit, sort, order);
        long total = PageInfo.of(goodsList).getTotal();
        Map<String, Object> data = new HashMap<>();
        data.put("total", total);
        data.put("items", goodsList);

        return ResponseUtil.ok(data);
    }

    public Object list(String goodsSn, String name, String categoryId,
                       Integer page, Integer limit, String sort, String order,
                       String brandName, Integer priceMin, Integer priceMax,
                       Boolean isHot, Boolean isNew, Boolean isOnSale) {
        //获取当前用户
        MallAdmin adminUser = (MallAdmin) SecurityUtils.getSubject().getPrincipal();
        String deptId = "";
        if (adminUser != null) {
            deptId = adminUser.getDeptId();
        }
        List<MallRole> mallRoleList = iGraphaiSystemRoleService.selectRoleListMMByUserId(adminUser.getId());
        MallRole mallRole = null;
        if(mallRoleList.size() > 0){
            mallRole = mallRoleList.get(0);
        }

        List<MallGoods> goodsList = goodsIntegralService.querySelective(goodsSn, name, categoryId, page, limit, sort, order, brandName, priceMin, priceMax, isHot, isNew, isOnSale, mallRole, deptId, adminUser);
//        for (MallGoods goods : goodsList){
//            MallCoupon coupon = couponService.findById(goods.getCouponId());
//            goods.setKeywords(coupon.getMerchantInfo());  暂时先注释掉
//        }
        long total = PageInfo.of(goodsList).getTotal();
        Map<String, Object> data = new HashMap<>();
        data.put("total", total);
        data.put("items", goodsList);

        return ResponseUtil.ok(data);
    }

    private Object validate(GoodsAllinone goodsAllinone) {
        MallGoods goods = goodsAllinone.getGoods();
        String name = goods.getName();
        if (StringUtils.isEmpty(name)) {
            return ResponseUtil.badArgument();
        }
        String goodsSn = goods.getGoodsSn();
        if (StringUtils.isEmpty(goodsSn)) {
            return ResponseUtil.badArgument();
        }
        // 品牌商可以不设置，如果设置则需要验证品牌商存在
        String brandId = goods.getBrandId();
        if (brandId != null && !"0".equals(brandId)) {
            if (brandService.findById(brandId) == null) {
                return ResponseUtil.badArgumentValue();
            }
        }
        // 分类可以不设置，如果设置则需要验证分类存在
        String categoryId = goods.getCategoryId();
        if (categoryId != null && !"0".equals(categoryId)) {
            if (categoryIntegralService.findById(categoryId) == null) {
                return ResponseUtil.badArgumentValue();
            }
        }

        MallGoodsAttribute[] attributes = goodsAllinone.getAttributes();
        for (MallGoodsAttribute attribute : attributes) {
            String attr = attribute.getAttribute();
            if (StringUtils.isEmpty(attr)) {
                return ResponseUtil.badArgument();
            }
            String value = attribute.getValue();
            if (StringUtils.isEmpty(value)) {
                return ResponseUtil.badArgument();
            }
        }

        MallGoodsSpecification[] specifications = goodsAllinone.getSpecifications();
        for (MallGoodsSpecification specification : specifications) {
            String spec = specification.getSpecification();
            if (StringUtils.isEmpty(spec)) {
                return ResponseUtil.badArgument();
            }
            String value = specification.getValue();
            if (StringUtils.isEmpty(value)) {
                return ResponseUtil.badArgument();
            }
        }

        MallGoodsProduct[] products = goodsAllinone.getProducts();
        for (MallGoodsProduct product : products) {
            Integer number = product.getNumber();
            if (number == null || number < 0) {
                return ResponseUtil.badArgument();
            }

            BigDecimal price = product.getPrice();
            if (price == null) {
                return ResponseUtil.badArgument();
            }

            String[] productSpecifications = product.getSpecifications();
            if (productSpecifications.length == 0) {
                return ResponseUtil.badArgument();
            }
        }

        return null;
    }

    /**
     * 编辑商品
     * <p>
     * TODO
     * 目前商品修改的逻辑是
     * 1. 更新Mall_goods表
     * 2. 逻辑删除Mall_goods_specification、Mall_goods_attribute、Mall_goods_product
     * 3. 添加Mall_goods_specification、Mall_goods_attribute、Mall_goods_product
     * <p>
     * 这里商品三个表的数据采用删除再添加的策略是因为
     * 商品编辑页面，支持管理员添加删除商品规格、添加删除商品属性，因此这里仅仅更新是不可能的，
     * 只能删除三个表旧的数据，然后添加新的数据。
     * 但是这里又会引入新的问题，就是存在订单商品货品ID指向了失效的商品货品表。
     * 因此这里会拒绝管理员编辑商品，如果订单或购物车中存在商品。
     * 所以这里可能需要重新设计。
     */
    @Transactional
    public Object update(GoodsAllinone goodsAllinone) {
        Object error = validate(goodsAllinone);
        if (error != null) {
            return error;
        }

        MallGoods goods = goodsAllinone.getGoods();
        MallGoodsAttribute[] attributes = goodsAllinone.getAttributes();
        MallGoodsSpecification[] specifications = goodsAllinone.getSpecifications();
        MallGoodsProduct[] products = goodsAllinone.getProducts();

        String id = goods.getId();

        //将生成的分享图片地址写入数据库
        String url = qCodeService.createGoodShareImage(goods.getId(), goods.getPicUrl(), goods.getName());
        goods.setShareUrl(url);

        // 商品基本信息表Mall_goods
        if (goodsIntegralService.updateById(goods) == 0) {
            throw new RuntimeException("更新数据失败");
        }

        String gid = goods.getId();
        specificationService.deleteByGid(gid);
        attributeService.deleteByGid(gid);
        productService.deleteByGid(gid);

        // 商品规格表Mall_goods_specification
        for (MallGoodsSpecification specification : specifications) {
            specification.setGoodsId(goods.getId());
            specificationService.add(specification);
        }

        // 商品参数表Mall_goods_attribute
        for (MallGoodsAttribute attribute : attributes) {
            attribute.setGoodsId(goods.getId());
            attributeService.add(attribute);
        }

        // 商品货品表Mall_product
        for (MallGoodsProduct product : products) {
            product.setGoodsId(goods.getId());
            productService.add(product);
        }

        return ResponseUtil.ok();
    }

    @Transactional
    public Object delete(MallGoods goods) {
        String id = goods.getId();
        if (id == null) {
            return ResponseUtil.badArgument();
        }

        String gid = goods.getId();
        goodsIntegralService.deleteById(gid);
        specificationService.deleteByGid(gid);
        attributeService.deleteByGid(gid);
        productService.deleteByGid(gid);
        return ResponseUtil.ok();
    }

    @Transactional
    public Object create(GoodsAllinone goodsAllinone) {
        //获取当前用户
        MallAdmin adminUser = (MallAdmin) SecurityUtils.getSubject().getPrincipal();
        String deptId = "";
        if (adminUser != null) {
            deptId = adminUser.getDeptId();
        }

        Object error = validate(goodsAllinone);
        if (error != null) {
            return error;
        }

        MallGoods goods = goodsAllinone.getGoods();
        //如果前端传入的C端价格为空那么将B端价格直接写入C端
//        goods.setcRetailPrice(null == goods.getcRetailPrice()?new BigDecimal(0):goods.getcRetailPrice());

        MallGoodsAttribute[] attributes = goodsAllinone.getAttributes();
        MallGoodsSpecification[] specifications = goodsAllinone.getSpecifications();
        MallGoodsProduct[] products = goodsAllinone.getProducts();

        String name = goods.getName();
        if (goodsIntegralService.checkExistByName(name)) {
            return ResponseUtil.fail(GOODS_NAME_EXIST, "商品名已经存在");
        }

        goods.setDeptId(deptId);//绑定归属部门
        // 商品基本信息表Mall_goods
        goodsIntegralService.add(goods);

        //将生成的分享图片地址写入数据库
        String url = qCodeService.createGoodShareImage(goods.getId(), goods.getPicUrl(), goods.getName());
        if (!StringUtils.isEmpty(url)) {
            goods.setShareUrl(url);
            if (goodsIntegralService.updateById(goods) == 0) {
                throw new RuntimeException("更新数据失败");
            }
        }

        // 商品规格表Mall_goods_specification
        for (MallGoodsSpecification specification : specifications) {
            specification.setGoodsId(goods.getId());
            specificationService.add(specification);
        }

        // 商品参数表Mall_goods_attribute
        for (MallGoodsAttribute attribute : attributes) {
            attribute.setGoodsId(goods.getId());
            attributeService.add(attribute);
        }

        // 商品货品表Mall_product
        for (MallGoodsProduct product : products) {
            product.setGoodsId(goods.getId());
            productService.add(product);
        }
        return ResponseUtil.ok();
    }

    public Object list2() {
        // http://element-cn.eleme.io/#/zh-CN/component/cascader
        // 管理员设置“所属分类”
        List<MallCategory> l1CatList = categoryIntegralService.queryL1("1");
        List<CatVo> categoryList = new ArrayList<>(l1CatList.size());

        for (MallCategory l1 : l1CatList) {
            CatVo l1CatVo = new CatVo();
            l1CatVo.setValue(l1.getId());
            l1CatVo.setLabel(l1.getName());

            List<MallCategory> l2CatList = categoryIntegralService.queryByPid(l1.getId());
            List<CatVo> children = new ArrayList<>(l2CatList.size());
            for (MallCategory l2 : l2CatList) {
                CatVo l2CatVo = new CatVo();
                l2CatVo.setValue(l2.getId());
                l2CatVo.setLabel(l2.getName());
                children.add(l2CatVo);
            }
            l1CatVo.setChildren(children);

            categoryList.add(l1CatVo);
        }

        // http://element-cn.eleme.io/#/zh-CN/component/select
        // 管理员设置“所属品牌商”
        List<MallBrand> list = brandService.all();
        List<Map<String, Object>> brandList = new ArrayList<>(l1CatList.size());
        for (MallBrand brand : list) {
            Map<String, Object> b = new HashMap<>(2);
            b.put("value", brand.getId());
            b.put("label", brand.getName());
            brandList.add(b);
        }

        Map<String, Object> data = new HashMap<>();
        data.put("categoryList", categoryList);
        data.put("brandList", brandList);
        return ResponseUtil.ok(data);
    }

    public Object detail(String id) {
        MallGoods goods = goodsIntegralService.findById(id);
        List<MallGoodsProduct> products = productService.queryByGid(id);
        List<MallGoodsSpecification> specifications = specificationService.queryByGid(id);
        List<MallGoodsAttribute> attributes = attributeService.queryByGid(id);

        String categoryId = goods.getCategoryId();
        MallCategory category = categoryIntegralService.findById(categoryId);
        String[] categoryIds = new String[]{};
        if (category != null) {
            String parentCategoryId = category.getPid();
            categoryIds = new String[]{parentCategoryId, categoryId};
        }

        Map<String, Object> data = new HashMap<>();
        data.put("goods", goods);
        data.put("specifications", specifications);
        data.put("products", products);
        data.put("attributes", attributes);
        data.put("categoryIds", categoryIds);

        return ResponseUtil.ok(data);
    }

}
