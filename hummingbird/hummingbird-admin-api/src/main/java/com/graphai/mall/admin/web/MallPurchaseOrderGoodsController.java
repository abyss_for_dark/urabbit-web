package com.graphai.mall.admin.web;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.lang.StringUtils;
import com.graphai.commons.util.servlet.ServletUtils;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.framework.generator.controller.IBaseController;
import com.graphai.mall.admin.domain.MallPurchaseOrderGoods;
import com.graphai.mall.admin.service.IMallPurchaseOrderGoodsService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;



/**
 * <p>
 * 采购订单商品表  前端控制器
 * </p>
 *
 * @author biteam
 * @since 2021-07-22
 */
@RestController
@RequestMapping("/admin/admin/goods")
public class MallPurchaseOrderGoodsController extends IBaseController<MallPurchaseOrderGoods,  String >  {

	@Autowired
    private IMallPurchaseOrderGoodsService serviceImpl;

    @RequiresPermissions("admin:admin:goods:single")
    @RequiresPermissionsDesc(menu = {"单独查询"}, button = "单独查询")
	@GetMapping("/singleGoods/{id}")
    public Object getEntityById(@PathVariable  String  id) throws Exception{
        MallPurchaseOrderGoods entityVName = serviceImpl.getById(id);
        return ResponseUtil.ok(entityVName);
    }

    @RequiresPermissions("admin:admin:goods:addOrUpdate")
    @RequiresPermissionsDesc(menu = {"新增"}, button = "新增")
    @PostMapping("/addOrUpdateGoods")
    public Object addOrUpdate(MallPurchaseOrderGoods mallPurchaseOrderGoods) throws Exception{
        if(serviceImpl.saveOrUpdate(mallPurchaseOrderGoods)){
            return ResponseUtil.ok();
        }else{
            return ResponseUtil.fail();
        }
    }


	@RequiresPermissions("admin:admin:goods:melist")
	@RequiresPermissionsDesc(menu = {"采购订单商品表 查询"}, button = "查询")
    @GetMapping("/melistGoods")
    public Object getMeList() throws Exception {
        IPage<MallPurchaseOrderGoods> ipage = serviceImpl.page(getPlusPage(),listQueryWrapper());
        return ResponseUtil.ok(ipage);
    }

 	private QueryWrapper<MallPurchaseOrderGoods> listQueryWrapper() {
 		QueryWrapper<MallPurchaseOrderGoods> meq = new QueryWrapper<>();
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("orderId"))) {
		      meq.eq("order_id", ServletUtils.getParameter("orderId"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("goodsId"))) {
		      meq.eq("goods_id", ServletUtils.getParameter("goodsId"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("goodsName"))) {
		      meq.eq("goods_name", ServletUtils.getParameter("goodsName"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("goodsSn"))) {
		      meq.eq("goods_sn", ServletUtils.getParameter("goodsSn"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("productId"))) {
		      meq.eq("product_id", ServletUtils.getParameter("productId"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("number"))) {
		      meq.eq("number", ServletUtils.getParameter("number"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("price"))) {
		      meq.eq("price", ServletUtils.getParameter("price"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("specifications"))) {
		      meq.eq("specifications", ServletUtils.getParameter("specifications"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("couponPrice"))) {
		      meq.eq("coupon_price", ServletUtils.getParameter("couponPrice"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("couponUserId"))) {
		      meq.eq("coupon_user_id", ServletUtils.getParameter("couponUserId"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("picUrl"))) {
		      meq.eq("pic_url", ServletUtils.getParameter("picUrl"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("comment"))) {
		      meq.eq("comment", ServletUtils.getParameter("comment"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("addTime"))) {
		      meq.eq("add_time", ServletUtils.getLocalDateTime("addTime"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("deleted"))) {
		      meq.eq("deleted", ServletUtils.getParameter("deleted"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("srcGoodId"))) {
		      meq.eq("src_good_id", ServletUtils.getParameter("srcGoodId"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("cardPwd"))) {
		      meq.eq("card_pwd", ServletUtils.getParameter("cardPwd"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("cardNumber"))) {
		      meq.eq("card_number", ServletUtils.getParameter("cardNumber"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("cardDeadline"))) {
		      meq.eq("card_deadline", ServletUtils.getParameter("cardDeadline"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("chargeType"))) {
		      meq.eq("charge_type", ServletUtils.getParameter("chargeType"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("goodsShortUrl"))) {
		      meq.eq("goods_short_url", ServletUtils.getParameter("goodsShortUrl"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("commissionRate"))) {
		      meq.eq("commission_rate", ServletUtils.getParameter("commissionRate"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("serviceFee"))) {
		      meq.eq("service_fee", ServletUtils.getParameter("serviceFee"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("actualPrice"))) {
		      meq.eq("actual_price", ServletUtils.getParameter("actualPrice"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("serviceRate"))) {
		      meq.eq("service_rate", ServletUtils.getParameter("serviceRate"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("commissionBeforeFee"))) {
		      meq.eq("commission_before_fee", ServletUtils.getParameter("commissionBeforeFee"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("commissionFee"))) {
		      meq.eq("commission_fee", ServletUtils.getParameter("commissionFee"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("userId"))) {
		      meq.eq("user_id", ServletUtils.getParameter("userId"));
		}
		if(StringUtils.isNotEmpty(ServletUtils.getParameter("tenantId"))) {
		      meq.eq("tenant_id", ServletUtils.getParameter("tenantId"));
		}

        return meq;
    }
 	@RequiresPermissions("admin:admin:goods:save")
 	@RequiresPermissionsDesc(menu = {"采购订单商品表 新增"}, button = "新增")
    @PostMapping("/saveGoods")
    public Object save(@ModelAttribute MallPurchaseOrderGoods mallPurchaseOrderGoods) throws Exception {
        serviceImpl.save(mallPurchaseOrderGoods);
        return ResponseUtil.ok();
    }

	@RequiresPermissions("admin:admin:goods:modify")
	@RequiresPermissionsDesc(menu = {"采购订单商品表 修改"}, button = "修改")
    @PutMapping("/modifyGoods")
    public Object modify(@ModelAttribute  MallPurchaseOrderGoods mallPurchaseOrderGoods){
        serviceImpl.updateById(mallPurchaseOrderGoods);
        return  ResponseUtil.ok();
    }

    @RequiresPermissions("admin:admin:goods:del")
    @RequiresPermissionsDesc(menu = {"采购订单商品表 删除"}, button = "删除")
    @DeleteMapping("/delGoods/{id}")
    public Object deleteById(@PathVariable  String  id){
        serviceImpl.removeById(id);
        return  ResponseUtil.ok();
    }

}

