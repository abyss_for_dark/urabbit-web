package com.graphai.mall.admin.web.bmdesign;

import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.mall.db.domain.BmdesignUserLevel;
import com.graphai.mall.db.service.bmdesign.BmdesignUserLevelService;
import com.graphai.validator.Order;

import jodd.util.StringUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 用户等级/段位
 */
@RestController
@RequestMapping("/admin/bmdesignUserLevel")
@Validated
public class AdminBmdesignUserLevelController {
    private final Log logger = LogFactory.getLog(AdminBmdesignUserLevelController.class);

    @Autowired
    private BmdesignUserLevelService bmdesignUserLevelService;


    /**
     * 查询等级/段位列表
     */
    @RequiresPermissions("admin:bmdesignUserLevel:list")
    @RequiresPermissionsDesc(menu = {"系统管理", "等级角色管理"}, button = "查询")
    @GetMapping("/list")
    public Object list(BmdesignUserLevel bmdesignUserLevel,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit,
                       @RequestParam(defaultValue = "level_num") String sort,
                       @Order @RequestParam(defaultValue = "asc") String order) {
        List<BmdesignUserLevel> list = bmdesignUserLevelService.queryBmdesignUserLevelList(bmdesignUserLevel, page, limit, sort, order);
        long total = PageInfo.of(list).getTotal();
        Map<String, Object> data = new HashMap<>();
        data.put("total", total);
        data.put("items", list);

        return ResponseUtil.ok(data);
    }

    /**
     * 添加等级/段位
     */
    @RequiresPermissions("admin:bmdesignUserLevel:add")
    @RequiresPermissionsDesc(menu = {"系统管理", "等级角色管理"}, button = "新增")
    @PostMapping("/add")
    public Object addSave(@RequestBody BmdesignUserLevel bmdesignUserLevel) {
        Object error = validate(bmdesignUserLevel);
        if (error != null) {
            return error;
        }
        bmdesignUserLevelService.insertBmdesignUserLevel(bmdesignUserLevel);
        return ResponseUtil.ok();
    }


    /**
     * 查看等级/段位详情
     */
    @RequiresPermissions("admin:bmdesignUserLevel:detail")
    @RequiresPermissionsDesc(menu = {"系统管理", "等级角色管理"}, button = "详情")
    @GetMapping("/edit/{id}")
    public Object edit(@PathVariable("id") String id) {
        BmdesignUserLevel bmdesignUserLevel = bmdesignUserLevelService.selectBmdesignUserLevelById(id);
        return ResponseUtil.ok(bmdesignUserLevel);
    }

    /**
     * 修改等级/段位
     */
    @RequiresPermissions("admin:bmdesignUserLevel:edit")
    @RequiresPermissionsDesc(menu = {"系统管理", "等级角色管理"}, button = "编辑")
    @PostMapping("/edit")
    public Object editSave(@RequestBody BmdesignUserLevel bmdesignUserLevel) {
        //等级主键id
        if (StringUtils.isEmpty(bmdesignUserLevel.getId())) {
            return ResponseUtil.badArgumentValue("没有获取到id！");
        }
        if (StringUtil.isEmpty(bmdesignUserLevel.getLevelName())){
            return ResponseUtil.badArgumentValue();
        }
        bmdesignUserLevelService.updateBmdesignUserLevel(bmdesignUserLevel);
        return ResponseUtil.ok();
    }


    /**
     * 校验数据(等级/段位)
     */
    private Object validate(BmdesignUserLevel bmdesignUserLevel) {
        if (StringUtils.isEmpty(bmdesignUserLevel.getLevelCode())) {
            return ResponseUtil.badArgument();
        }
        if (StringUtils.isEmpty(bmdesignUserLevel.getLevelName())) {
            return ResponseUtil.badArgument();
        }
        if (bmdesignUserLevel.getPrice() == null) {
            return ResponseUtil.badArgument();
        }

        if (StringUtils.isEmpty(bmdesignUserLevel.getCalModel())) {
            return ResponseUtil.badArgument();
        }
        if (bmdesignUserLevel.getLevelNum() == null) {
            return ResponseUtil.badArgument();
        }
        return null;
    }
}
