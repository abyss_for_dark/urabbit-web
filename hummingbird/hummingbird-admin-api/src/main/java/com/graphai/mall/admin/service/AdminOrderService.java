package com.graphai.mall.admin.service;

import static com.graphai.framework.constant.AdminResponseCode.ORDER_CONFIRM_NOT_ALLOWED;
import static com.graphai.framework.constant.AdminResponseCode.ORDER_REFUND_FAILED;
import static com.graphai.framework.constant.AdminResponseCode.ORDER_REPLY_EXIST;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.graphai.mall.admin.dao.MallMerchantMapper;
import com.graphai.mall.admin.dao.MallPurchaseOrderGoodsMapper;
import com.graphai.mall.admin.dao.MallPurchaseOrderMapper;
import com.graphai.mall.admin.domain.*;
import com.graphai.mall.admin.service.impl.MallUserMerchantServiceImpl;
import com.graphai.mall.db.dao.*;
import com.graphai.mall.db.domain.*;
import com.graphai.mall.db.service.order.*;
import com.graphai.mall.db.vo.MallAfterSaleVo;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.github.binarywang.wxpay.bean.request.WxPayRefundRequest;
import com.github.binarywang.wxpay.bean.result.WxPayRefundResult;
import com.github.binarywang.wxpay.exception.WxPayException;
import com.github.binarywang.wxpay.service.WxPayService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.json.JacksonUtil;
import com.graphai.commons.util.json.JacksonUtils;
import com.graphai.commons.util.lang.StringUtils;
import com.graphai.commons.util.obj.ObjectUtils;
import com.graphai.commons.util.sms.AliSmsUtils;
import com.graphai.commons.util.sms.TecentSmsUtil;
import com.graphai.config.JDiworkerConfig;
import com.graphai.framework.system.service.IGraphaiSystemRoleService;
import com.graphai.mall.admin.util.OrderServiceUtils;
import com.graphai.mall.admin.vo.OrderVo;
import com.graphai.mall.core.notify.NotifyService;
import com.graphai.mall.core.notify.NotifyType;
import com.graphai.mall.db.constant.account.AccountStatus;
import com.graphai.mall.db.service.account.FcAccountPrechargeBillService;
import com.graphai.mall.db.service.account.FcAccountRechargeBillService;
import com.graphai.mall.db.service.common.MallShipCompanyService;
import com.graphai.mall.db.service.goods.MallGoodsProductService;
import com.graphai.mall.db.service.user.MallUserIWorkerService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.service.user.behavior.MallCommentService;
import com.graphai.mall.db.util.OrderUtil;
import com.graphai.mall.db.vo.MallLoseOrderVo;
import com.graphai.mall.db.vo.MallOrderGoodsVo;

import cn.hutool.core.date.LocalDateTimeUtil;

@Service

public class AdminOrderService {
    private final Log logger = LogFactory.getLog(AdminOrderService.class);

    @Autowired
    private MallOrderGoodsService orderGoodsService;
    @Autowired
    private MallOrderService orderService;
    @Autowired
    private MallGoodsProductService productService;
    @Autowired
    private MallUserService userService;
    @Autowired
    private MallUserIWorkerService userIWorkerService;
    @Autowired
    private MallCommentService commentService;
    @Autowired
    private WxPayService wxPayService;
    @Autowired
    private NotifyService notifyService;
    @Autowired
    private LogHelper logHelper;
    @Autowired
    private FcAccountRechargeBillService fcAccountRechargeBillService;
    @Autowired
    private IMallUserProfitNewService mallUserProfitNewService;
    @Autowired
    private MallLoseOrderMapper mallLoseOrderMapper;
    @Autowired
    private MallOrderPurchaseService mallOrderPurchaseService;
    @Autowired
    private MallOrderPurchaseGoodsService mallOrderPurchaseGoodsService;
    @Autowired
    private MallPurchaseOrderMapper mallPurchaseOrderMapper;
    @Autowired
    private MallPurchaseOrderGoodsMapper mallPurchaseOrderGoodsMapper;
    @Autowired
    private MallMerchantMapper mallMerchantMapper;




    @Autowired
    private CustomMallLoseOrderMapper customMallLoseOrderMapper;

//    @Autowired
//    private  MallLoseOrder;

    @Autowired
    private MallOrderMapper mallOrderMapper;
    @Autowired
    CustomMallOrderMapper customMallOrderMapper;
    @Autowired
    private MallShipCompanyService mallShipCompanyService;
    @Autowired
    private FcAccountPrechargeBillService fcAccountPrechargeBillService;

    @Resource
    private FcAccountPrechargeBillMapper fcAccountPrechargeBillMapper;

    @Resource
    CustomFcAccountPrechargeBillMapper customFcAccountPrechargeBillMapper;



    @Resource
    private MallOrderGoodsService mallOrderGoodsService;
    @Resource
    private MallGrouponMapper mapper;
    @Resource
    private CustomMallGroupMapper customMallGroupMapper;
    @Autowired
    private IGraphaiSystemRoleService iGraphaiSystemRoleService;
    @Autowired
    private IMallUserProfitNewService serviceImpl;
    @Autowired
    private MallUserMerchantServiceImpl mallUserMerchantService;
    @Autowired
    private AdminAfterSaleMapper adminAfterSaleMapper;

    @Autowired
    private CustomFcAccountWithdrawBillMapper customFcAccountWithdrawBillMapper;

    public Object list(LocalDateTime begin1, LocalDateTime end1, String userName, String userid, String orderSn, String goodsName, String tpId, String tpUserName,
                    List<Short> orderStatusArray, Integer page, Integer limit, String sort, String order, LocalDateTime beginTime, LocalDateTime endTime, String type,String isGroup,
                    List<String> platArray, boolean getShipCompanyList, String grouponId, String source,String orderStatus,String salesStatus) {

        // 获取当前用户
        MallAdmin adminUser = (MallAdmin) SecurityUtils.getSubject().getPrincipal();
        String deptId = "";
        if (adminUser != null) {
            deptId = adminUser.getDeptId();
        }
        List<MallRole> mallRoleList = iGraphaiSystemRoleService.selectRoleListMMByUserId(adminUser.getId());
        MallRole mallRole = null;
        if (mallRoleList.size() > 0) {
            mallRole = mallRoleList.get(0);
        }
        QueryWrapper<MallUserMerchant> wrapper=new QueryWrapper<>();
        wrapper.eq("user_id",adminUser.getUserId());
        MallUserMerchant mallUserMerchant = mallUserMerchantService.getOne(wrapper);
        String merchantId = "";
        if ("merchant".equals(mallRole.getRoleKey())) {
            merchantId = mallUserMerchant.getMerchantId();
        }
        if ("plant".equals(mallRole.getRoleKey())) {
            merchantId = mallUserMerchant.getMerchantId();
        }

        // 管理员查询所有门店的zy销售订单
        List<MallOrder> orderList =  orderService.querySelective(begin1, end1, userName, userid, orderSn, goodsName, tpId, tpUserName, orderStatusArray, page, limit, sort, order,
                beginTime, endTime, type,isGroup, platArray, mallRole, deptId, adminUser, grouponId, source, orderStatus,salesStatus,merchantId);

        /*// 管理员查询所有工厂的zy销售订单
        List<MallPurchaseOrder> mallPurchaseOrderList = mallOrderPurchaseService.queryOrderPurchaseList(orderSn, source, begin1, end1, type,userid, orderStatus);*/

        /*List<MallOrder> mallMerchantOrderList = new ArrayList();
        // 当前商家用户查询该商家的全部zy销售订单
        if ("merchant".equals(mallRole.getRoleKey())) {
            QueryWrapper<MallUserMerchant> wrapper=new QueryWrapper<>();
            wrapper.eq("user_id",adminUser.getUserId());
            MallUserMerchant mallUserMerchant = mallUserMerchantService.getOne(wrapper);
            if (mallUserMerchant != null && orderList != null && orderList.size() > 0) {
                for (MallOrder mallOrder : orderList) {
                    if ((StringUtils.isNotEmpty(mallOrder.getMerchantId()))&&mallOrder.getMerchantId().equals(mallUserMerchant.getMerchantId())) {
                        mallMerchantOrderList.add(mallOrder);
                    }
                }
            }
        }

        // 当前工厂用户查询该工厂的全部zy销售订单
        if ("plant".equals(mallRole.getRoleKey())) {
            QueryWrapper<MallUserMerchant> wrapper=new QueryWrapper<>();
            wrapper.eq("user_id",adminUser.getUserId());
            MallUserMerchant mallUserMerchant = mallUserMerchantService.getOne(wrapper);
            if (mallUserMerchant != null && orderList != null && orderList.size() > 0) {
                for (MallOrder mallOrder : orderList) {
                    if ((StringUtils.isNotEmpty(mallOrder.getMerchantId()))&&mallOrder.getMerchantId().equals(mallUserMerchant.getMerchantId())) {
                        mallMerchantOrderList.add(mallOrder);
                    }
                }
            }
        }*/


        BigDecimal UserProfitSum = new BigDecimal("0");// 用户有效总收益
        BigDecimal PlatProfitSum = new BigDecimal("0");// 平台有效总收益
        BigDecimal PubShareFeeSum = new BigDecimal("0");// 订单有效总收益
        List<OrderVo> ordervoList = new ArrayList<OrderVo>();
        List<String> list1 = new ArrayList<>();// 用户id user_id
        List<String> list2 = new ArrayList<>();// 订单号 order_sn
        List<String> list6 = new ArrayList<>();// 预充值用户id user_id
        List<String> directList = new ArrayList<>();// 推荐人id

        List<String> orderIdList = new ArrayList<>(); // 订单id
        // 根据订单id查询订单商品表该id下所属所有商品信息
        List<MallOrderGoods> mallOrderGoodsList = null;



        List<MallOrderGoods> orderGoodsList = null;
        List<String> userIdList = new ArrayList<>();


        if (orderList != null && orderList.size() > 0) {
            for (MallOrder order2 : orderList) {
                list1.add(order2.getUserId());
                list2.add(order2.getOrderSn());
                list6.add(order2.getId());
                if (order2.getTpid() != null && !order2.getTpid().equals(""))
                    directList.add(order2.getTpid());

                userIdList.add(order2.getUserId());

                orderIdList.add(order2.getId());



            }

            if (orderIdList != null && orderIdList.size() > 0) {
                mallOrderGoodsList = mallOrderGoodsService.queryByOidList(orderIdList);
            }

            List<MallUser> list3 = userIWorkerService.getMallUserList(userIdList);
            List<FcAccountPrechargeBill> list4 = fcAccountRechargeBillService.selectRechargeBillByOrderSnList(list2);


            if ("2".equals(type) || "7".equals(type)) {
                orderGoodsList = orderGoodsService.queryByOidListGroup(list6);
            }
            for (MallOrder order2 : orderList) {
                MallUser mallUser = null;
                for (MallUser mallUser1 : list3) {
                    if (mallUser1.getId().equals(order2.getUserId())) {
                        mallUser = mallUser1;
                    }
                }
                OrderVo orderVo = new OrderVo();

                OrderServiceUtils.setOrderVo(order2, orderVo, mallUser, type, mallOrderGoodsList, ordervoList);



                if (!"8".equals(type) && !"9".equals(type))
                    OrderServiceUtils.setOrderVoPlatform(order2, orderVo);

                BigDecimal userProfit = new BigDecimal("0.00");// 用户收益
                BigDecimal agentProfit = new BigDecimal("0.00");// 代理商收益
                BigDecimal platProfit = new BigDecimal("0.00");// 平台收益
                // 订单收益
                orderVo.setPubShareFee(order2.getPubSharePreFee());
                for (FcAccountPrechargeBill bill : list4) {
                    if (bill.getOrderSn().equals(order2.getOrderSn())) {

                        if (null != mallUser) {
                            if (mallUser.getId().equals(bill.getCustomerId()) && bill.getDistributionType().equals(AccountStatus.DISTRIBUTION_TYPE_00)) {
                                userProfit = bill.getAmt();
                            } else if ("平台分润".equals(bill.getNote()) || "平台额外分润".equals(bill.getNote())) {
                                platProfit = bill.getAmt();
                            } else {
                                agentProfit = agentProfit.add(bill.getAmt());

                            }
                        }
                    }
                }

                orderVo.setPlatProfit(platProfit);
                orderVo.setAgentProfit(agentProfit);
                orderVo.setUserProfit(userProfit);
                //OrderServiceUtils.setOrderVo(order2, orderVo, mallUser, type, orderGoodsList, ordervoList);


                /*if (order2.getHuid().equals(type))
                    OrderServiceUtils.setOrderVo(order2, orderVo, mallUser, type, mallOrderGoodsList, ordervoList);*/
            }



        }

        List<String> userIds = new ArrayList<>();


        if (("8".equals(type) || "9".equals(type)) && !directList.isEmpty()) {
            List<MallUser> userList = userService.findByIds(directList);
            for (MallUser user : userList) {
                for (OrderVo orderVo : ordervoList) {
                    if (user.getId().equals(orderVo.getTpid())) {
                        orderVo.setTpName(user.getNickname() == null || user.getNickname().equals("") ? user.getMobile() : user.getNickname());
                    }
                }
            }
        }

        if ("9".equals(type) && !list1.isEmpty()) {
            this.setTpName(list1, ordervoList);
        }


        List<MallAfterSaleVo> mallAfterSaleVos = new ArrayList<>();
        for (OrderVo vo : ordervoList) {
            for (MallOrderGoods orderGoods : mallOrderGoodsList) {
                if (orderGoods.getOrderId().equals(vo.getOrderId())) {
                    MallAfterSaleVo aftersale = adminAfterSaleMapper.getAfterSaleBy(vo.getOrderId(), orderGoods.getGoodsId());
                    if (aftersale != null && aftersale.getOrderId().equals(vo.getOrderId()) && aftersale.getOrderGoodsId().equals(aftersale.getOrderGoodsId())) {
                        mallAfterSaleVos.add(aftersale);
                        vo.setMallAftersales(mallAfterSaleVos);
                    }
                }
            }
        }

        long total = PageInfo.of(orderList).getTotal();
        Map<String, Object> data = new HashMap<>();
        BigDecimal totalAmount = new BigDecimal(0);
        for (OrderVo orderVo : ordervoList) {
            if (108 == orderVo.getOrderStatus() || 603 == orderVo.getOrderStatus()) {
                totalAmount = totalAmount.add(orderVo.getOrderPrice());
            }
        }
        data.put("turnover", totalAmount);
        data.put("total", total);
        data.put("items", ordervoList);
        if ("merchant".equals(mallRole.getRoleKey())) {

            long merchantTotal = PageInfo.of(ordervoList).getTotal();
            data.put("total", merchantTotal);
            data.put("items", ordervoList);
        }
        if ("plant".equals(mallRole.getRoleKey())) {
            long plantTotal = PageInfo.of(ordervoList).getTotal();
            data.put("total", plantTotal);
            data.put("items", ordervoList);
        }
        Map map = list1(begin1, end1, userName, userid, orderSn, goodsName, tpId, tpUserName, orderStatusArray, page, limit, sort, order, LocalDateTime.now(), endTime, type,isGroup,
                        platArray, source,orderStatus);

        data.put("PlatProfitSum", map.get("PlatProfitSum"));
        data.put("UserProfitSum", map.get("UserProfitSum"));
        data.put("PubShareFeeSum", map.get("PubShareFeeSum"));
        data.put("orderSum", map.get("orderSum"));
        if (getShipCompanyList) {
            data.put("shipCompanyList", mallShipCompanyService.list());
        }

        return ResponseUtil.ok(data);
    }

    public void setTpName(List<String> list1, List<OrderVo> ordervoList) {
        try {
            // 达人订单
            List<Map<String, String>> directLeaderMap = new ArrayList<>();// 用户直推人id，直推人id
            List<String> directLeader = new ArrayList<>();// 用户直推人id
            List<MallUser> userList = userService.findByIds(list1);
            for (MallUser user : userList) {
                Map<String, String> map = new HashMap<>();
                map.put("userId", user.getId());
                map.put("directLeaderId", user.getDirectLeader());
                directLeaderMap.add(map);
                directLeader.add(user.getDirectLeader());
            }
            List<MallUser> directUserList = userService.findByIds(directLeader);
            List<Map<String, String>> userInfoMap = new ArrayList<>();// 用户直推人id
            // 通过直推人id绑定订单用户与用户直推人姓名
            for (MallUser user : directUserList) {
                for (Map<String, String> dirMap : directLeaderMap) {
                    if (dirMap.get("directLeaderId") != null && dirMap.get("directLeaderId").equals(user.getId())) {
                        Map<String, String> map = new HashMap<>();
                        map.put("userId", dirMap.get("userId"));
                        map.put("userName", user.getNickname() == null || user.getNickname().equals("") ? user.getMobile() : user.getNickname());
                        userInfoMap.add(map);
                    }
                }
            }
            // 通过用户id获取绑定好的直推人姓名
            for (Map<String, String> userInfo : userInfoMap) {
                for (OrderVo orderVo : ordervoList) {
                    if (userInfo.get("userId").equals(orderVo.getUserId())) {
                        orderVo.setTpName(userInfo.get("userName"));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public Map list1(LocalDateTime begin1, LocalDateTime end1, String userName, String userid, String orderSn, String goodsName, String tpId, String tpUserName,
                    List<Short> orderStatusArray, Integer page, Integer limit, String sort, String order, LocalDateTime beginTime, LocalDateTime endTime, String type,String isGroup,
                    List<String> platArray, String source,String orderStatus) {

        // 获取当前用户
        MallAdmin adminUser = (MallAdmin) SecurityUtils.getSubject().getPrincipal();
        String deptId = "";
        if (adminUser != null) {
            deptId = adminUser.getDeptId();
        }
        List<MallRole> mallRoleList = iGraphaiSystemRoleService.selectRoleListMMByUserId(adminUser.getId());
        MallRole mallRole = null;
        if (mallRoleList.size() > 0) {
            mallRole = mallRoleList.get(0);
        }

        List<Short> list = new ArrayList<>();
//        list.add((short) 201);
//        list.add((short) 108);
//        list.add((short) 105);
//        list.add((short) 106);
//        list.add((short) 603);
//        list.add((short) 301);
//        list.add((short) 401);
//        list.add((short) 402);
        orderStatusArray = list;
        List<MallOrder> orderList = orderService.querySelective(begin1, end1, userName, userid, orderSn, goodsName, tpId, tpUserName, orderStatusArray, page, limit, sort, order,
                        beginTime, endTime, type,isGroup, platArray, mallRole, deptId, adminUser, "", source,orderStatus,"","");

        BigDecimal UserProfitSum = new BigDecimal("0");// 用户有效总收益
        BigDecimal PlatProfitSum = new BigDecimal("0");// 平台有效总收益
        BigDecimal PubShareFeeSum = new BigDecimal("0");// 订单有效总收益
        BigDecimal orderSum = new BigDecimal("0.00");// 订单总金额
        List<String> list2 = new ArrayList<>();// 订单号 order_sn
        for (MallOrder order2 : orderList) {
            list2.add(order2.getOrderSn());
        }
        List<FcAccountPrechargeBill> list4 = null;
        if (list2.size() > 0) {

            list4 = fcAccountRechargeBillService.selectRechargeBillByOrderSnList(list2);
        }
        if (orderList != null && orderList.size() > 0) {
            for (MallOrder order2 : orderList) {

                // List<FcAccountPrechargeBill> list =
                // fcAccountRechargeBillService.selectRechargeBillByOrderSn(order2.getOrderSn());

                BigDecimal userProfit = new BigDecimal("0.00");// 用户收益
                BigDecimal agentProfit = new BigDecimal("0.00");// 代理商收益
                BigDecimal platProfit = new BigDecimal("0.00");// 平台收益
                if (null != order2.getActualPrice() && order2.getActualPrice().compareTo(new BigDecimal("0.00")) == 1) {
                    orderSum = orderSum.add(order2.getActualPrice());
                }
                if (!StringUtils.isEmpty(order2.getPubSharePreFee())) {
                    PubShareFeeSum = PubShareFeeSum.add(new BigDecimal(order2.getPubSharePreFee()));
                }
                for (FcAccountPrechargeBill bill : list4) {
                    if (bill.getOrderSn().equals(order2.getOrderSn())) {
                        if (bill.getDistributionType().equals(AccountStatus.DISTRIBUTION_TYPE_00)) {
                            userProfit = bill.getAmt();
                            UserProfitSum = UserProfitSum.add(bill.getAmt());
                        } else if (bill.getCustomerId().equals("00") && bill.getDistributionType().equals(AccountStatus.DISTRIBUTION_TYPE_10)) {
                        } else {
                            agentProfit = agentProfit.add(bill.getAmt());
                        }
                    }
                }


                if (agentProfit.compareTo(new BigDecimal("0.00")) == 1) {
                    if (order2.getOrderStatus() == 108 || order2.getOrderStatus() == 201 || order2.getOrderStatus() == 105 || order2.getOrderStatus() == 603
                                    || order2.getOrderStatus() == 401) {
                        if (!StringUtils.isEmpty(order2.getPubSharePreFee())) {
                            PlatProfitSum = PlatProfitSum.add(new BigDecimal(order2.getPubSharePreFee()).subtract(userProfit).subtract(agentProfit));
                        }
                    }
                } else {
                    // 没有代理
                    if (!StringUtils.isEmpty(order2.getBid()) && order2.getBid().equals(AccountStatus.BID_9)) {
                        // 权益商品
                    } else {
                        if (!StringUtils.isEmpty(order2.getPubSharePreFee())) {
                            PlatProfitSum = PlatProfitSum.add(new BigDecimal(order2.getPubSharePreFee()).subtract(userProfit));
                        }
                    }
                }


            }

        }

        Map<String, Object> data = new HashMap<>();
        if (PlatProfitSum.compareTo(new BigDecimal("0.00")) == 1) {
            data.put("PlatProfitSum", PlatProfitSum.setScale(2, BigDecimal.ROUND_DOWN));
        } else {
            data.put("PlatProfitSum", 0);
        }
        if (UserProfitSum.compareTo(new BigDecimal("0.00")) == 1) {
            data.put("UserProfitSum", UserProfitSum.setScale(2, BigDecimal.ROUND_DOWN));
        } else {
            data.put("UserProfitSum", 0);
        }
        if (PubShareFeeSum.compareTo(new BigDecimal("0.00")) == 1) {
            data.put("PubShareFeeSum", PubShareFeeSum.setScale(2, BigDecimal.ROUND_DOWN));
        } else {
            data.put("PubShareFeeSum", 0);
        }
        if (orderSum.compareTo(new BigDecimal("0.00")) == 1) {
            data.put("orderSum", orderSum.setScale(2, BigDecimal.ROUND_DOWN));
        } else {
            data.put("orderSum", 0);
        }

        return data;
    }

    public Object detail(String id, String type) {
        Map<String, Object> data = new HashMap<>();
        if ("1".equals(type)) {
            data = (Map) detailRe(id);
        } else {
            data = (Map) detailPre(id);
        }
        return ResponseUtil.ok(data);
    }

    public Object detailRe(String id) {
        MallOrder order = orderService.findById(id);
        List<MallOrderGoods> orderGoods = orderGoodsService.queryByOid(id);
        UserVo user = userService.findUserVoById(order.getUserId());

        List<FcAccountPrechargeBill> list = fcAccountRechargeBillService.selectRechargeBillByOrderSn(order.getOrderSn(), 1);// 当前数据
        List<FcAccountPrechargeBill> listL = fcAccountRechargeBillService.selectRechargeBillByOrderSn(order.getOrderSn(), 0);// 历史数据
        Map<String, Object> data = new HashMap<>();
        // BigDecimal sumPrice= new BigDecimal("0.00");//商品总价格
        //
        // BigDecimal platProfitSum = new BigDecimal("0");//平台有效总收益
        // BigDecimal PubShareFeeSum = new BigDecimal("0");//订单有效总收益
        // BigDecimal userProfitSum = new BigDecimal("0");//用户有效总收益
        //
        // BigDecimal fuserSum = new BigDecimal("0");//服务商有效收益
        // BigDecimal quserSum = new BigDecimal("0");//区代有效收益
        // BigDecimal quserSume = new BigDecimal("0");//区代额外分润
        // BigDecimal suserSum = new BigDecimal("0");//市代分润
        // BigDecimal suserSume = new BigDecimal("0");//市代额外分润


        List listLs = new ArrayList();
        Map<String, Object> map = new HashMap();// 当前记录
        for (FcAccountPrechargeBill bill : list) {
            MallUser user1 = userService.findById(bill.getCustomerId());

            map.put("userProfitSumTime", bill.getCreateDate());
            if (null != user1 && user1.getId().equals(bill.getCustomerId()) && bill.getDistributionType().equals(AccountStatus.DISTRIBUTION_TYPE_00)) {
                map.put("userProfitSum", bill.getAmt());
                map.put("checkStatus", bill.getCheckStatus());
                map.put("status", bill.getStatus());
                map.put("uproportion", bill.getProfitProportion());
            } else if (null != user1 && user1.getLevelId().equals("4")) {
                map.put("fuser", user1.getNickname());
                map.put("fmobile", user1.getMobile());
                if ("服务商额外分润".equals(bill.getNote())) {
                    map.put("fuserSume", bill.getAmt());
                    map.put("fuserSumTime", bill.getCreateDate());
                    map.put("fuserSumMobile", user1.getMobile());
                    map.put("fproportione", bill.getProfitProportion());
                } else if ("服务商分润".equals(bill.getNote())) {
                    map.put("fuserSum", bill.getAmt());
                    map.put("fuserSumTime", bill.getCreateDate());
                    map.put("fuserSumMobile", user1.getMobile());
                    map.put("fproportion", bill.getProfitProportion());
                }
            } else if (null != user1 && user1.getLevelId().equals("5")) {
                map.put("quser", user1.getNickname());
                map.put("qmobile", user1.getMobile());
                if ("区代额外分润".equals(bill.getNote())) {
                    map.put("quserSume", bill.getAmt());// 当前记录
                    map.put("quserSumeTime", bill.getCreateDate());// 当前记录
                    map.put("quserSumMobile", user1.getMobile());
                    map.put("qproportione", bill.getProfitProportion());
                } else if ("区代分润".equals(bill.getNote())) {
                    map.put("quserSum", bill.getAmt());// 当前记录
                    map.put("quserSumTime", bill.getCreateDate());// 当前记录时间
                    map.put("quserSumMobile", user1.getMobile());
                    map.put("qproportion", bill.getProfitProportion());
                }
            } else if (null != user1 && user1.getLevelId().equals("6")) {

                if ("市代额外分润".equals(bill.getNote())) {
                    map.put("suser", user1.getNickname());
                    map.put("smobile", user1.getMobile());
                    map.put("suserSume", bill.getAmt());// 当前记录
                    map.put("suserSumeTime", bill.getCreateDate());// 当前记录时间
                    map.put("suserSumMobile", user1.getMobile());
                    map.put("sproportione", bill.getProfitProportion());
                } else if ("市代分润".equals(bill.getNote())) {
                    map.put("suser", user1.getNickname());
                    map.put("smobile", user1.getMobile());
                    map.put("suserSum", bill.getAmt());// 当前记录
                    map.put("suserSumTime", bill.getCreateDate());// 当前记录时间
                    map.put("suserSumMobile", user1.getMobile());
                    map.put("sproportion", bill.getProfitProportion());
                } else if ("平台分润".equals(bill.getNote()) || "平台额外分润".equals(bill.getNote())) {
                    // 平台
                    map.put("platProfitSum", bill.getAmt());
                    map.put("pproportion", bill.getProfitProportion());
                }
            } else if (null != user1 && user1.getId().equals("00")) {
                // 平台
                map.put("platProfitSum", bill.getAmt());
                map.put("pproportion", bill.getProfitProportion());

            }


        }
        TreeSet<Integer> arr = new TreeSet();
        for (FcAccountPrechargeBill bill : listL) {
            // 获取当天时间的年月日时分秒
            int year = bill.getCreateDate().getYear();
            int month = bill.getCreateDate().getMonthValue();
            int day = bill.getCreateDate().getDayOfMonth();
            int hour = bill.getCreateDate().getHour();
            int minute = bill.getCreateDate().getMinute();
            int str = year + month + day + hour + minute;
            arr.add(str);

        }
        arr = (TreeSet) arr.descendingSet();
        for (Integer ss : arr) {
            Map map1 = new HashMap();
            for (int j = 0; j < listL.size(); j++) {
                int str = listL.get(j).getCreateDate().getYear() + listL.get(j).getCreateDate().getMonthValue() + listL.get(j).getCreateDate().getDayOfMonth()
                                + listL.get(j).getCreateDate().getHour() + listL.get(j).getCreateDate().getMinute();

                MallUser user1 = userService.findById(listL.get(j).getCustomerId());
                map1.put("userProfitSumTime", listL.get(j).getCreateDate());
                if (null != user1 && user1.getId().equals(listL.get(j).getCustomerId()) && listL.get(j).getDistributionType().equals(AccountStatus.DISTRIBUTION_TYPE_00)
                                && ss == str) {
                    // 用户本身
                    map1.put("checkStatus", listL.get(j).getCheckStatus());
                    map1.put("status", listL.get(j).getStatus());
                    map1.put("userProfitSum", listL.get(j).getAmt());
                    map1.put("uproportion", listL.get(j).getProfitProportion());
                } else if (null != user1 && user1.getLevelId().equals("4") && ss == str) {
                    // 服务商
                    map1.put("fuser", user1.getNickname());
                    map1.put("fmobile", user1.getMobile());
                    if ("服务商额外分润".equals(listL.get(j).getNote())) {
                        map1.put("fuserSume", listL.get(j).getAmt());
                        map1.put("fuserSumTime", listL.get(j).getCreateDate());
                        map1.put("fuserSumMobile", user1.getMobile());
                        map1.put("fproportione", listL.get(j).getProfitProportion());
                    } else if ("服务商分润".equals(listL.get(j).getNote())) {
                        map1.put("fuserSum", listL.get(j).getAmt());
                        map1.put("fuserSumTime", listL.get(j).getCreateDate());
                        map1.put("fuserSumMobile", user1.getMobile());
                        map1.put("fproportion", listL.get(j).getProfitProportion());
                    }
                } else if (null != user1 && user1.getLevelId().equals("5") && ss == str) {
                    // 区代
                    map1.put("quser", user1.getNickname());
                    map1.put("qmobile", user1.getMobile());
                    if ("区代额外分润".equals(listL.get(j).getNote())) {
                        map1.put("quserSume", listL.get(j).getAmt());// 当前记录
                        map1.put("quserSumeTime", listL.get(j).getCreateDate());// 当前记录
                        map1.put("quserSumMobile", user1.getMobile());
                        map1.put("qproportione", listL.get(j).getProfitProportion());
                    } else if ("区代分润".equals(listL.get(j).getNote())) {
                        map1.put("quserSum", listL.get(j).getAmt());// 当前记录
                        map1.put("quserSumTime", listL.get(j).getCreateDate());// 当前记录时间
                        map1.put("quserSumMobile", user1.getMobile());
                        map1.put("qproportion", listL.get(j).getProfitProportion());
                    }
                } else if (null != user1 && user1.getLevelId().equals("6") && ss == str) {
                    // 市代
                    if ("市代额外分润".equals(listL.get(j).getNote())) {
                        map1.put("suser", user1.getNickname());
                        map1.put("smobile", user1.getMobile());
                        map1.put("suserSume", listL.get(j).getAmt());// 当前记录
                        map1.put("suserSumeTime", listL.get(j).getCreateDate());// 当前记录时间
                        map1.put("suserSumMobile", user1.getMobile());
                        map1.put("sproportione", listL.get(j).getProfitProportion());
                    } else if ("市代分润".equals(listL.get(j).getNote())) {
                        map1.put("suser", user1.getNickname());
                        map1.put("smobile", user1.getMobile());
                        map1.put("suserSum", listL.get(j).getAmt());// 当前记录
                        map1.put("suserSumTime", listL.get(j).getCreateDate());// 当前记录时间
                        map1.put("suserSumMobile", user1.getMobile());
                        map1.put("sproportion", listL.get(j).getProfitProportion());
                    } else if ("平台分润".equals(listL.get(j).getNote()) || "平台额外分润".equals(listL.get(j).getNote())) {
                        // 平台
                        map1.put("platProfitSum", listL.get(j).getAmt());
                        map1.put("pproportion", listL.get(j).getProfitProportion());
                    }
                } else if (null != user1 && user1.getId().equals("00") && ss == str) {
                    // 平台
                    map1.put("platProfitSum", listL.get(j).getAmt());
                    map1.put("pproportion", listL.get(j).getProfitProportion());

                }
            }
            listLs.add(map1);
        }


        data.put("order", order);
        data.put("orderGoods", orderGoods);
        data.put("user", user);
        data.put("total", orderGoods.size());
        data.put("agent", map);
        data.put("agentLs", listLs);
        return data;
    }

    public Object detailPre(String id) {
        MallOrder order = orderService.findById(id);
        List<MallOrderGoods> orderGoods = orderGoodsService.queryByOid(id);
        UserVo user = userService.findUserVoById(order.getUserId());

        // List<FcAccountPrechargeBill> list =
        // fcAccountRechargeBillService.selectRechargeBillByOrderSn(order.getOrderSn(), 1);//当前数据
        List<FcAccountPrechargeBill> listL = fcAccountRechargeBillService.selectRechargeBillByOrderSn(order.getOrderSn(), 0);// 历史数据
        Map<String, Object> data = new HashMap<>();
        Map<String, Object> map = new HashMap();// 当前记录
        List listLs = new ArrayList();

        TreeSet<Integer> arr = new TreeSet();
        for (FcAccountPrechargeBill bill : listL) {
            // 获取当天时间的年月日时分秒
            int year = bill.getCreateDate().getYear();
            int month = bill.getCreateDate().getMonthValue();
            int day = bill.getCreateDate().getDayOfMonth();
            int hour = bill.getCreateDate().getHour();
            int minute = bill.getCreateDate().getMinute();
            int str = year + month + day + hour + minute;
            arr.add(str);

        }
        arr = (TreeSet) arr.descendingSet();
        for (Integer ss : arr) {
            Map map1 = new HashMap();
            for (int j = 0; j < listL.size(); j++) {
                int str = listL.get(j).getCreateDate().getYear() + listL.get(j).getCreateDate().getMonthValue() + listL.get(j).getCreateDate().getDayOfMonth()
                                + listL.get(j).getCreateDate().getHour() + listL.get(j).getCreateDate().getMinute();

                MallUser user1 = userService.findById(listL.get(j).getCustomerId());

                if (null != user1) {
                    map1.put("userProfitSumTime", listL.get(j).getCreateDate());
                    if (listL.get(j).getNote().contains("用户返佣") && ss == str) {
                        // 用户本身
                        map1.put("checkStatus", listL.get(j).getCheckStatus());
                        map1.put("status", listL.get(j).getStatus());
                        map1.put("userProfitSum", listL.get(j).getAmt());
                        map1.put("uproportion", listL.get(j).getProfitProportion());
                        map1.put("uNote", listL.get(j).getNote());
                        map1.put("uProp", listL.get(j).getProfitProportion());
                    } else if (listL.get(j).getNote().contains("锁客者推荐") && ss == str) {
                        // 服务商 培育奖-联盟商家
                        map1.put("fuser", user1.getNickname());
                        map1.put("fmobile", user1.getMobile());
                        map1.put("frecomAmt", listL.get(j).getAmt());
                        map1.put("fNote", listL.get(j).getNote());
                        map1.put("fProp", listL.get(j).getProfitProportion());
                    } else if ((listL.get(j).getNote().contains("创客/商家分润") || listL.get(j).getNote().contains("直堆人分润"))  && ss == str) {
                        // 服务商 都给锁客者
                        map1.put("fuserSupply", user1.getNickname());
                        map1.put("fmobileSupply", user1.getMobile());
                        map1.put("frecomSupplyAmt", listL.get(j).getAmt());
                        map1.put("fSupplyNote", listL.get(j).getNote());
                        map1.put("fSupplyProp", listL.get(j).getProfitProportion());
                    } else if (listL.get(j).getNote().contains("区代推荐分红") && ss == str) {
                        // 区代
                        map1.put("quserSupply", user1.getNickname());
                        map1.put("qmobileSupply", user1.getMobile());
                        map1.put("quserSupplyAmt", listL.get(j).getAmt());// 当前记录
                        map1.put("qSupplyNote", listL.get(j).getNote());
                        map1.put("qSupplyProp", listL.get(j).getProfitProportion());
                    } else if (listL.get(j).getNote().contains("区代分润") && ss == str) {
                        map1.put("quser", user1.getNickname());
                        map1.put("qmobile", user1.getMobile());
                        map1.put("quserAmt", listL.get(j).getAmt());// 当前记录
                        map1.put("qNote", listL.get(j).getNote());
                        map1.put("qProp", listL.get(j).getProfitProportion());
                    } else if (listL.get(j).getNote().contains("市代推荐分红") && ss == str) {
                        // 市代
                        map1.put("suserSupply", user1.getNickname());
                        map1.put("smobileSupply", user1.getMobile());
                        map1.put("suserSupplyAmt", listL.get(j).getAmt());// 当前记录
                        map1.put("sSupplyNote", listL.get(j).getNote());// 当前记录
                        map1.put("sSupplyProp", listL.get(j).getProfitProportion());
                    } else if (listL.get(j).getNote().contains("市代分润") && ss == str) {
                        map1.put("suser", user1.getNickname());
                        map1.put("smobile", user1.getMobile());
                        map1.put("suserAmt", listL.get(j).getAmt());// 当前记录
                        map1.put("sNote", listL.get(j).getNote());
                        map1.put("sProp", listL.get(j).getProfitProportion());
                    } else if (user1.getId().equals("00") && listL.get(j).getNote().contains("公司分润")) {
                        // 平台（公司）
                        map1.put("platName", "未莱生活（公司）");
                        map1.put("platProfitSum", listL.get(j).getAmt());
                        map1.put("platNote", listL.get(j).getNote());
                        map1.put("platProp", listL.get(j).getProfitProportion());
                    } else if (user1.getId().equals("08") && listL.get(j).getNote().contains("共同体分润")) {
                        // 平台（共同体）
                        map1.put("platNameG", "未莱生活（共同体）");
                        map1.put("platProfitSumG", listL.get(j).getAmt());
                        map1.put("platNoteG", listL.get(j).getNote());
                        map1.put("platPropG", listL.get(j).getProfitProportion());
                    } else if (listL.get(j).getNote().contains("分享赚")) {
                        map1.put("userShare", user1.getNickname());
                        map1.put("mobileShare", user1.getMobile());
                        map1.put("shareAmt", listL.get(j).getAmt());// 当前记录
                        map1.put("shareNote", listL.get(j).getNote());// 当前记录
                        map1.put("shareProp", listL.get(j).getProfitProportion());
                    }

                }
            }
            listLs.add(map1);
        }


        data.put("order", order);
        data.put("orderGoods", orderGoods);
        data.put("user", user);
        data.put("total", orderGoods.size());
        data.put("agent", map);
        data.put("agentLs", listLs);
        return data;
    }


    public Object zyDetail(String id) {
        Map<String, Object> data = new HashMap<>();
        MallUser User = null;
        MallOrder order = orderService.findById(id);
        List<MallOrderGoods> orderGoods = orderGoodsService.queryByOid(id);
        if (order != null) {
            User = userService.findById(order.getUserId());
            List<String> list1 = new ArrayList<>();
            List<Map<String, Object>> list2 = new ArrayList<>();
            // 普通订单
            String type = "0";
            List<FcAccountPrechargeBill> listL = fcAccountRechargeBillService.selectRechargeBillByOrderSn(order.getOrderSn(), 0);// 历史数据
            List listLs = new ArrayList();

            TreeSet<Integer> arr = new TreeSet();
            for (FcAccountPrechargeBill bill : listL) {
                // 获取当天时间的年月日时分秒
                int year = bill.getCreateDate().getYear();
                int month = bill.getCreateDate().getMonthValue();
                int day = bill.getCreateDate().getDayOfMonth();
                int hour = bill.getCreateDate().getHour();
                int minute = bill.getCreateDate().getMinute();
                int str = year + month + day + hour + minute;
                arr.add(str);

            }
            arr = (TreeSet) arr.descendingSet();
            for (Integer ss : arr) {
                Map map1 = new HashMap();
                for (int j = 0; j < listL.size(); j++) {
                    int str = listL.get(j).getCreateDate().getYear() + listL.get(j).getCreateDate().getMonthValue() + listL.get(j).getCreateDate().getDayOfMonth()
                            + listL.get(j).getCreateDate().getHour() + listL.get(j).getCreateDate().getMinute();

                    MallUser user1 = userService.findById(listL.get(j).getCustomerId());

                    if (null != user1) {
                        map1.put("userProfitSumTime", listL.get(j).getCreateDate());
                        if (listL.get(j).getNote().contains("用户返佣") && ss == str) {
                            // 用户本身
                            map1.put("checkStatus", listL.get(j).getCheckStatus());
                            map1.put("status", listL.get(j).getStatus());
                            map1.put("userProfitSum", listL.get(j).getAmt());
                            map1.put("uproportion", listL.get(j).getProfitProportion());
                            map1.put("uNote", listL.get(j).getNote());
                            map1.put("uProp", listL.get(j).getProfitProportion());
                        } else if (listL.get(j).getNote().contains("服务商推荐分红") && ss == str) {
                            // 服务商 培育奖-联盟商家
                            map1.put("fuser", user1.getNickname());
                            map1.put("fmobile", user1.getMobile());
                            map1.put("frecomAmt", listL.get(j).getAmt());
                            map1.put("fNote", listL.get(j).getNote());
                            map1.put("fProp", listL.get(j).getProfitProportion());
                        } else if (listL.get(j).getNote().contains("直堆人分润") && ss == str) {
                            // 服务商 都给锁客者
                            map1.put("fuserSupply", user1.getNickname());
                            map1.put("fmobileSupply", user1.getMobile());
                            map1.put("frecomSupplyAmt", listL.get(j).getAmt());
                            map1.put("fSupplyNote", listL.get(j).getNote());
                            map1.put("fSupplyProp", listL.get(j).getProfitProportion());
                        } else if (listL.get(j).getNote().contains("区代推荐分红") && ss == str) {
                            // 区代
                            map1.put("quserSupply", user1.getNickname());
                            map1.put("qmobileSupply", user1.getMobile());
                            map1.put("quserSupplyAmt", listL.get(j).getAmt());// 当前记录
                            map1.put("qSupplyNote", listL.get(j).getNote());
                            map1.put("qSupplyProp", listL.get(j).getProfitProportion());
                        } else if (listL.get(j).getNote().contains("区代分润") && ss == str) {
                            map1.put("quser", user1.getNickname());
                            map1.put("qmobile", user1.getMobile());
                            map1.put("quserAmt", listL.get(j).getAmt());// 当前记录
                            map1.put("qNote", listL.get(j).getNote());
                            map1.put("qProp", listL.get(j).getProfitProportion());
                        } else if (listL.get(j).getNote().contains("市代推荐分红") && ss == str) {
                            // 市代
                            map1.put("suserSupply", user1.getNickname());
                            map1.put("smobileSupply", user1.getMobile());
                            map1.put("suserSupplyAmt", listL.get(j).getAmt());// 当前记录
                            map1.put("sSupplyNote", listL.get(j).getNote());// 当前记录
                            map1.put("sSupplyProp", listL.get(j).getProfitProportion());
                        } else if (listL.get(j).getNote().contains("市代分润") && ss == str) {
                            map1.put("suser", user1.getNickname());
                            map1.put("smobile", user1.getMobile());
                            map1.put("suserAmt", listL.get(j).getAmt());// 当前记录
                            map1.put("sNote", listL.get(j).getNote());
                            map1.put("sProp", listL.get(j).getProfitProportion());
                        } else if (user1.getId().equals("00") && listL.get(j).getNote().contains("公司分润")) {
                            // 平台（公司）
                            map1.put("platName", "未莱生活（公司）");
                            map1.put("platProfitSum", listL.get(j).getAmt());
                            map1.put("platNote", listL.get(j).getNote());
                            map1.put("platProp", listL.get(j).getProfitProportion());
                        } else if (user1.getId().equals("08") && listL.get(j).getNote().contains("共同体分润")) {
                            // 平台（共同体）
                            map1.put("platNameG", "未莱生活（共同体）");
                            map1.put("status", listL.get(j).getStatus());
                            map1.put("checkStatus", listL.get(j).getCheckStatus());
                            map1.put("platProfitSumG", listL.get(j).getAmt());
                            map1.put("platNoteG", listL.get(j).getNote());
                            map1.put("platPropG", listL.get(j).getProfitProportion());
                        } else if (listL.get(j).getNote().contains("分享赚")) {
                            map1.put("userShare", user1.getNickname());
                            map1.put("mobileShare", user1.getMobile());
                            map1.put("shareAmt", listL.get(j).getAmt());// 当前记录
                            map1.put("shareNote", listL.get(j).getNote());// 当前记录
                            map1.put("shareProp", listL.get(j).getProfitProportion());
                        }

                    }
                }
                listLs.add(map1);
            }

            if (null != order.getGrouponPrice() && order.getGrouponPrice().compareTo(new BigDecimal("0.00")) > 0) {
                // 团购订单
                type = "1";
                MallGrouponExample example = new MallGrouponExample();
                example.createCriteria().andOrderIdEqualTo(order.getParentOrderOn()).andDeletedEqualTo(false);//这里要传父级订单号！！！
                MallGroupon mallGroupon = mapper.selectOneByExample(example);
                MallGrouponExample example1 = new MallGrouponExample();
                MallGrouponExample.Criteria criteria = example1.createCriteria();
                criteria.andDeletedEqualTo(false);
                if (mallGroupon.getCreatorUserId().equals(order.getUserId())) {
                    // 团长
                    criteria.andIdEqualTo(mallGroupon.getId());
                } else {
                    // 团员
                    criteria.andGrouponIdEqualTo(mallGroupon.getGrouponId());
                }
                List<MallGroupon> mallGroupons = mapper.selectByExample(example1);
                List<String> stringList = new ArrayList<>();
                String teamLeader = "";
                for (MallGroupon groupon : mallGroupons) {
                    teamLeader = groupon.getCreatorUserId();
                    stringList.add(groupon.getUserId());
                }
                List<MallUser> list3 = new ArrayList<>();
                UserVo team = new UserVo();
                if (stringList.size() > 0) {
                    team = userService.findUserVoById(teamLeader);
                    list3 = userService.findByIds(stringList);
                }
                data.put("orderUser", list3);
                data.put("orderTeam", team);
            } else {
                for (MallOrderGoods goods : orderGoods) {
                    Map<String, Object> map = new HashMap<>();
                    if (null != goods.getCashbackType() && "1".equals(goods.getCashbackType())) {
                        type = "2";// 队列返订单
                        list1.add(goods.getProductId());
                        MallOrderGoodsVo vo = new MallOrderGoodsVo();
                        vo.setProductId(goods.getProductId());
                        List<MallOrderGoodsVo> zyGoodsList = mallOrderGoodsService.selectCashbackListByGoodsId(vo);
                        if (zyGoodsList.size() > 0) {
                            for (MallOrderGoodsVo goodsVo : zyGoodsList) {
                                if (goodsVo.getUserId().equals(User.getId())) {
                                    map.put("user", goodsVo);
                                }
                            }
                        }
                        map.put("list", zyGoodsList);
                        list2.add(map);
                    }
                }
            }

            BigDecimal aPrice = new BigDecimal("0");
            // 实际支付
            BigDecimal actualPrice = new BigDecimal("0");
            for (MallOrderGoods goods : orderGoods) {
                if (null != goods.getPrice()) {
                    aPrice = aPrice.add(goods.getPrice());
                }
                if (null != goods.getActualPrice()) {
                    actualPrice = actualPrice.add(goods.getActualPrice());
                } else {
                    actualPrice = aPrice;
                }
            }

            data.put("queue", list2);
            data.put("type", type);
            data.put("aPrice", aPrice);
            data.put("order", order);
            data.put("user", User);
            data.put("orderGoods", orderGoods);
            data.put("total", orderGoods.size());
            data.put("actualPrice", actualPrice);
            data.put("agentLs", listLs);
        }


        if (order == null && orderGoods.size() == 0) {
            MallPurchaseOrder mallPurchaseOrder = mallPurchaseOrderMapper.selectById(id);
            Map<String,Object> map = new HashMap();
            MallUserMerchant mallUserMerchant = null;
            map.put("order_id",id);
            List<MallPurchaseOrderGoods> mallPurchaseOrderGoodsList = mallPurchaseOrderGoodsMapper.selectByMap(map);
            MallUser mallUser = userService.findById(mallPurchaseOrder.getUserId());
            if (mallUser.getId().equals(mallPurchaseOrder.getUserId())) {
                //mallUser = mallUser1;
                QueryWrapper<MallUserMerchant> wrapper=new QueryWrapper<>();
                wrapper.eq("user_id",mallUser.getId());
                mallUserMerchant = mallUserMerchantService.getOne(wrapper);
            }
            MallMerchant mallMerchant = mallMerchantMapper.selectById(mallUserMerchant.getMerchantId());
            mallUser.setNickname(mallMerchant.getName());
            mallUser.setMobile(mallMerchant.getPhone());
            BigDecimal aPrice = new BigDecimal("0");
            // 实际支付
            BigDecimal actualPrice = new BigDecimal("0");
            for (MallPurchaseOrderGoods goods : mallPurchaseOrderGoodsList) {
                if (null != goods.getPrice()) {
                    aPrice = aPrice.add(goods.getPrice());
                }
                if (null != goods.getActualPrice()) {
                    actualPrice = actualPrice.add(goods.getActualPrice());
                } else {
                    actualPrice = aPrice;
                }

            }
            data.put("aPrice", aPrice);
            data.put("order", mallPurchaseOrder);
            data.put("user", mallUser);
            data.put("orderGoods", mallPurchaseOrderGoodsList);
            data.put("total", mallPurchaseOrderGoodsList.size());
            data.put("actualPrice", actualPrice);
        }

        return ResponseUtil.ok(data);
    }

    public Object detail2(String id) {
        MallOrder order = orderService.findById(id);
        List<MallOrderGoods> orderGoods = orderGoodsService.queryByOid(id);
        UserVo user = userService.findUserVoById(order.getUserId());
        MallUser User = userService.findById(order.getUserId());
        MallUser fuser = null;// 4 服务商
        MallUser quser = null;// 5 区代
        MallUser suser = null;// 6 市代
        String levelId = "";

        // if(null != User && !StringUtils.isEmpty(User.getFirstLeader())) {
        // fuser = userService.findById(User.getFirstLeader()); //第一个上级
        // if(null != fuser){
        // levelId = fuser.getLevelId();//等级ID 0 普通用户 1 vip 2 高级VIP 4 服务商 5 区代 6 市代
        // }
        // }

        List<FcAccountPrechargeBill> list = fcAccountRechargeBillService.selectRechargeBillByOrderSn(order.getOrderSn(), 1);
        Map<String, Object> data = new HashMap<>();
        BigDecimal sumPrice = new BigDecimal("0.00");// 商品总价格

        BigDecimal PlatProfitSum = new BigDecimal("0");// 平台有效总收益
        BigDecimal PubShareFeeSum = new BigDecimal("0");// 订单有效总收益

        BigDecimal UserProfitSum = new BigDecimal("0");// 用户有效总收益
        BigDecimal fuserSum = new BigDecimal("0");// 服务商有效收益
        BigDecimal quserSum = new BigDecimal("0");// 区代有效收益
        BigDecimal suserSum = new BigDecimal("0");// 市代有效收益


        Map map = new HashMap();


        for (FcAccountPrechargeBill bill : list) {
            MallUser user1 = userService.findById(bill.getCustomerId());
            if (null != user1 && user1.getId().equals(bill.getCustomerId()) && bill.getDistributionType().equals(AccountStatus.DISTRIBUTION_TYPE_00)) {
                data.put("UserProfitSum", bill.getAmt());
                UserProfitSum = bill.getAmt();
            } else if (null != user1 && user1.getLevelId().equals("4")) {
                fuserSum = bill.getAmt();
                map.put("fuser", user1.getNickname());
                map.put("fmobile", user1.getMobile());
                map.put("fuserSum", bill.getAmt().setScale(2, BigDecimal.ROUND_DOWN));
            } else if (null != user1 && user1.getLevelId().equals("5")) {
                quserSum = bill.getAmt();
                map.put("quser", user1.getNickname());
                map.put("qmobile", user1.getMobile());
                map.put("quserSum", bill.getAmt().setScale(2, BigDecimal.ROUND_DOWN));
            } else if (null != user1 && user1.getLevelId().equals("6") && !"00".equals(user1.getId())) {
                suserSum = bill.getAmt();
                map.put("suser", user1.getNickname());
                map.put("smobile", user1.getMobile());
                map.put("suserSum", bill.getAmt().setScale(2, BigDecimal.ROUND_DOWN));
            } else if (null != user1 && user1.getId().equals("00")) {
                PlatProfitSum = bill.getAmt();
            }
        }
        if (!StringUtils.isEmpty(order.getActivityType()) && order.getActivityType().equals("zero")) {

        } else if (!StringUtils.isEmpty(order.getBid()) && order.getBid().equals("9")) {
        } else {
            if (fuserSum.compareTo(new BigDecimal("0")) == 0 && quserSum.compareTo(new BigDecimal("0")) == 0 && suserSum.compareTo(new BigDecimal("0")) == 0
                            && !StringUtils.isEmpty(order.getPubSharePreFee())) {
                data.put("PlatProfitSum", new BigDecimal(order.getPubSharePreFee()).subtract(UserProfitSum));
            } else if (fuserSum.compareTo(new BigDecimal("0")) == 0 && quserSum.compareTo(new BigDecimal("0")) == 1 && suserSum.compareTo(new BigDecimal("0")) == 1
                            && !StringUtils.isEmpty(order.getPubSharePreFee())) {
                // 没有服务商
                PlatProfitSum = new BigDecimal(order.getPubSharePreFee()).subtract(UserProfitSum).subtract(quserSum.add(suserSum));
                data.put("PlatProfitSum", PlatProfitSum);
            } else if (fuserSum.compareTo(new BigDecimal("0")) == 1 && quserSum.compareTo(new BigDecimal("0")) == 0 && suserSum.compareTo(new BigDecimal("0")) == 1
                            && !StringUtils.isEmpty(order.getPubSharePreFee())) {
                // 没有区代
                PlatProfitSum = new BigDecimal(order.getPubSharePreFee()).subtract(UserProfitSum).subtract(fuserSum.add(suserSum));
                data.put("PlatProfitSum", PlatProfitSum);
            } else if (fuserSum.compareTo(new BigDecimal("0")) == 1 && quserSum.compareTo(new BigDecimal("0")) == 1 && suserSum.compareTo(new BigDecimal("0")) == 0
                            && !StringUtils.isEmpty(order.getPubSharePreFee())) {
                // 没有市代
                PlatProfitSum = new BigDecimal(order.getPubSharePreFee()).subtract(UserProfitSum).subtract(fuserSum.add(quserSum));
                data.put("PlatProfitSum", PlatProfitSum);
            } else if (fuserSum.compareTo(new BigDecimal("0")) == 1 && quserSum.compareTo(new BigDecimal("0")) == 0 && suserSum.compareTo(new BigDecimal("0")) == 0
                            && !StringUtils.isEmpty(order.getPubSharePreFee())) {
                // 只有服务商
                PlatProfitSum = new BigDecimal(order.getPubSharePreFee()).subtract(UserProfitSum).subtract(fuserSum);
                data.put("PlatProfitSum", PlatProfitSum);
            } else if (fuserSum.compareTo(new BigDecimal("0")) == 0 && quserSum.compareTo(new BigDecimal("0")) == 1 && suserSum.compareTo(new BigDecimal("0")) == 0
                            && !StringUtils.isEmpty(order.getPubSharePreFee())) {
                // 只有区代
                PlatProfitSum = new BigDecimal(order.getPubSharePreFee()).subtract(UserProfitSum).subtract(quserSum);
                data.put("PlatProfitSum", PlatProfitSum);
            } else if (fuserSum.compareTo(new BigDecimal("0")) == 0 && quserSum.compareTo(new BigDecimal("0")) == 0 && suserSum.compareTo(new BigDecimal("0")) == 1
                            && !StringUtils.isEmpty(order.getPubSharePreFee())) {
                // 只有市代
                PlatProfitSum = new BigDecimal(order.getPubSharePreFee()).subtract(UserProfitSum).subtract(suserSum);
                data.put("PlatProfitSum", PlatProfitSum);
            }
        }

        data.put("order", order);
        data.put("orderGoods", orderGoods);
        data.put("user", user);
        data.put("total", orderGoods.size());
        data.put("sumP", order.getActualPrice());
        if (!StringUtils.isEmpty(order.getPubSharePreFee())) {
            data.put("PubShareFeeSum", order.getPubSharePreFee());
        } else {
            data.put("PubShareFeeSum", "0.00");
        }
        data.put("agent", map);
        return ResponseUtil.ok(data);
    }

    public Object ckDetail(String id) {
        MallOrder order = orderService.findById(id);
        UserVo user = userService.findUserVoById(order.getUserId());

        List<FcAccountRechargeBill> list = fcAccountRechargeBillService.selectCkRechargeBillByOrderSn(order.getOrderSn(), 0);// 所有数据
        Map<String, Object> data = new HashMap<>();
        Map<String, Object> map = new HashMap();// 当前记录
        String pubSharePreFeeId = null;
        for (FcAccountRechargeBill bill : list) {
            MallUser user1 = userService.findById(bill.getCustomerId());
            if (pubSharePreFeeId == null)
                pubSharePreFeeId = bill.getUserProfitNewId();
            if (!map.containsKey("checkStatus"))
                map.put("checkStatus", bill.getCheckStatus());
            if (!map.containsKey("status"))
                map.put("status", bill.getStatus());
            if (null != user1) {
                map.put("userProfitSumTime", bill.getCreateDate());
                if (bill.getMessage().contains("服务商推荐分红")) {
                    // 服务商 培育奖-联盟商家
                    map.put("fuser", user1.getNickname());
                    map.put("fmobile", user1.getMobile());
                    map.put("frecomAmt", bill.getAmt());
                    map.put("fNote", bill.getMessage());
                    // map.put("fProp", mallUserProfitNewService.getById(bill.getUserProfitNewId()).getSixMerProfit());
                    // map.put("fProp", bill.getProfitProportion());
                } else if (bill.getMessage().contains("直堆人分润") || bill.getMessage().contains("创客分润")) {
                    // 服务商 都给锁客者
                    map.put("fuserSupply", user1.getNickname());
                    map.put("fmobileSupply", user1.getMobile());
                    map.put("frecomSupplyAmt", bill.getAmt());
                    map.put("fSupplyNote", bill.getMessage());
                    // map.put("fSupplyProp",
                    // mallUserProfitNewService.getById(bill.getUserProfitNewId()).getTenMerProfit());
                    // map.put("fSupplyProp", bill.getProfitProportion());
                } else if (bill.getMessage().contains("区代推荐分红")) {
                    // 区代
                    map.put("quserSupply", user1.getNickname());
                    map.put("qmobileSupply", user1.getMobile());
                    map.put("quserSupplyAmt", bill.getAmt());// 当前记录
                    map.put("qSupplyNote", bill.getMessage());
                    // map.put("qSupplyProp",
                    // mallUserProfitNewService.getById(bill.getUserProfitNewId()).getFiveMerProfit());
                    // map.put("qSupplyProp", bill.getProfitProportion());
                } else if (bill.getMessage().contains("区代得分润")) {
                    map.put("quser", user1.getNickname());
                    map.put("qmobile", user1.getMobile());
                    map.put("quserAmt", bill.getAmt());// 当前记录
                    map.put("qNote", bill.getMessage());
                    // map.put("qProp",
                    // mallUserProfitNewService.getById(bill.getUserProfitNewId()).getEightMerProfit());
                    // map.put("qProp", bill.getProfitProportion());
                } else if (bill.getMessage().contains("市代推荐分红")) {
                    // 市代
                    map.put("suserSupply", user1.getNickname());
                    map.put("smobileSupply", user1.getMobile());
                    map.put("suserSupplyAmt", bill.getAmt());// 当前记录
                    map.put("sSupplyNote", bill.getMessage());// 当前记录
                    // map.put("sSupplyProp",
                    // mallUserProfitNewService.getById(bill.getUserProfitNewId()).getFourMerProfit());
                    // map.put("sSupplyProp", bill.getProfitProportion());
                } else if (bill.getMessage().contains("市代得分润")) {
                    map.put("suser", user1.getNickname());
                    map.put("smobile", user1.getMobile());
                    map.put("suserAmt", bill.getAmt());// 当前记录
                    map.put("sNote", bill.getMessage());
                    // map.put("sProp",
                    // mallUserProfitNewService.getById(bill.getUserProfitNewId()).getSevenMerProfit());
                    // map.put("sProp", bill.getProfitProportion());
                } else if (user1.getId().equals("00") && bill.getMessage().contains("公司分润")) {
                    // 平台（公司）
                    map.put("platName", "未莱生活（公司）");
                    map.put("platProfitSum", bill.getAmt());
                    map.put("platNote", bill.getMessage());
                    // map.put("platProp",
                    // mallUserProfitNewService.getById(bill.getUserProfitNewId()).getOneMerProfit());
                    // map.put("platProp", bill.getProfitProportion());
                } else if (user1.getId().equals("08") && bill.getMessage().contains("共同体分润")) {
                    // 平台（共同体）
                    map.put("platNameG", "未莱生活（共同体）");
                    map.put("platProfitSumG", bill.getAmt());
                    map.put("platNoteG", bill.getMessage());
                    // map.put("platPropG",
                    // mallUserProfitNewService.getById(bill.getUserProfitNewId()).getTwoMerProfit());
                    // map.put("platPropG",bill.getProfitProportion());
                }

            }
        }



        if (CollectionUtils.isNotEmpty(list)) {
            MallUserProfitNew mallUserProfitNew = mallUserProfitNewService.getById(list.get(0).getUserProfitNewId());
            map.put("fProp", mallUserProfitNew.getSixMerProfit());
            map.put("fSupplyProp", mallUserProfitNew.getTenMerProfit());
            map.put("qSupplyProp", mallUserProfitNew.getFiveMerProfit());
            map.put("qProp", mallUserProfitNew.getEightMerProfit());
            map.put("sSupplyProp", mallUserProfitNew.getFourMerProfit());
            map.put("sProp", mallUserProfitNew.getSevenMerProfit());
            map.put("platProp", mallUserProfitNew.getOneMerProfit());
            map.put("platPropG", mallUserProfitNew.getTwoMerProfit());
        }
        if (pubSharePreFeeId != null)
            data.put("pubSharePreFee", mallUserProfitNewService.getById(pubSharePreFeeId).getDistributableAmount());
        data.put("order", order);
        data.put("user", user);
        data.put("agent", map);
        return ResponseUtil.ok(data);
    }

    /**
     * 订单退款
     * <p>
     * 1. 检测当前订单是否能够退款; 2. 微信退款操作; 3. 设置订单退款确认状态； 4. 订单商品库存回库。
     * <p>
     * TODO 虽然接入了微信退款API，但是从安全角度考虑，建议开发者删除这里微信退款代码，采用以下两步走步骤： 1. 管理员登录微信官方支付平台点击退款操作进行退款 2.
     * 管理员登录Mall管理后台点击退款操作进行订单状态修改和商品库存回库
     *
     * @param body 订单信息，{ orderId：xxx }
     * @return 订单退款操作结果
     */
    @Transactional
    public Object refund(String body) {
        String orderId = JacksonUtil.parseString(body, "orderId");
        String refundMoney = JacksonUtil.parseString(body, "refundMoney");
        if (orderId == null) {
            return ResponseUtil.badArgument();
        }
        if (StringUtils.isEmpty(refundMoney)) {
            return ResponseUtil.badArgument();
        }

        MallOrder order = orderService.findById(orderId);
        if (order == null) {
            return ResponseUtil.badArgument();
        }

        if (order.getActualPrice().compareTo(new BigDecimal(refundMoney)) != 0) {
            return ResponseUtil.badArgumentValue();
        }

        // 如果订单不是退款状态，则不能退款
        if (!order.getOrderStatus().equals(OrderUtil.STATUS_REFUND)) {
            return ResponseUtil.fail(ORDER_CONFIRM_NOT_ALLOWED, "订单不能确认收货");
        }

        // 微信退款
        WxPayRefundRequest wxPayRefundRequest = new WxPayRefundRequest();
        wxPayRefundRequest.setOutTradeNo(order.getOrderSn());
        wxPayRefundRequest.setOutRefundNo("refund_" + order.getOrderSn());
        // 元转成分
        Integer totalFee = order.getActualPrice().multiply(new BigDecimal(100)).intValue();
        wxPayRefundRequest.setTotalFee(totalFee);
        wxPayRefundRequest.setRefundFee(totalFee);

        WxPayRefundResult wxPayRefundResult = null;
        try {
            wxPayRefundResult = wxPayService.refund(wxPayRefundRequest);
        } catch (WxPayException e) {
            e.printStackTrace();
            return ResponseUtil.fail(ORDER_REFUND_FAILED, "订单退款失败");
        }
        if (!wxPayRefundResult.getReturnCode().equals("SUCCESS")) {
            logger.warn("refund fail: " + wxPayRefundResult.getReturnMsg());
            return ResponseUtil.fail(ORDER_REFUND_FAILED, "订单退款失败");
        }
        if (!wxPayRefundResult.getResultCode().equals("SUCCESS")) {
            logger.warn("refund fail: " + wxPayRefundResult.getReturnMsg());
            return ResponseUtil.fail(ORDER_REFUND_FAILED, "订单退款失败");
        }

        // 设置订单取消状态
        order.setOrderStatus(OrderUtil.STATUS_REFUND_CONFIRM);
        if (orderService.updateWithOptimisticLocker(order) == 0) {
            throw new RuntimeException("更新数据已失效");
        }

        // 商品货品数量增加
        List<MallOrderGoods> orderGoodsList = orderGoodsService.queryByOid(orderId);
        for (MallOrderGoods orderGoods : orderGoodsList) {
            String productId = orderGoods.getProductId();
            Short number = orderGoods.getNumber();
            if (productService.addStock(productId, number) == 0) {
                throw new RuntimeException("商品货品库存增加失败");
            }
        }

        // TODO 发送邮件和短信通知，这里采用异步发送
        // 退款成功通知用户, 例如“您申请的订单退款 [ 单号:{1} ] 已成功，请耐心等待到账。”
        // 注意订单号只发后6位
        notifyService.notifySmsTemplate(order.getMobile(), NotifyType.REFUND, new String[] {order.getOrderSn().substring(8, 14)});

        logHelper.logOrderSucceed("退款", "订单编号 " + orderId);
        return ResponseUtil.ok();
    }

    /**
     * 发货 1. 检测当前订单是否能够发货 2. 设置订单发货状态
     *
     * @param body 订单信息，{ orderId：xxx, shipSn: xxx, shipChannel: xxx }
     * @return 订单操作结果 成功则 { errno: 0, errmsg: '成功' } 失败则 { errno: XXX, errmsg: XXX }
     */
    public Object ship(String body) throws IOException {
        String orderId = JacksonUtil.parseString(body, "orderId");
        String shipSn = JacksonUtil.parseString(body, "shipSn");
        String shipCode = JacksonUtil.parseString(body, "shipCode");
        String shipChannel = JacksonUtil.parseString(body, "shipChannel");
        if (orderId == null || shipSn == null || shipChannel == null) {
            return ResponseUtil.badArgument();
        }

        MallOrder order = orderService.findById(orderId);
        if (order == null) {
            return ResponseUtil.badArgument();
        }

        // 如果订单不是已付款状态，则不能发货
        if (!order.getOrderStatus().equals(OrderUtil.STATUS_PAY)) {
            return ResponseUtil.fail(ORDER_CONFIRM_NOT_ALLOWED, "订单不能确认收货");
        }

        order.setOrderStatus(OrderUtil.STATUS_SHIP);
        order.setShipSn(shipSn);
        order.setShipChannel(shipChannel);
        order.setShipTime(LocalDateTime.now());
        order.setShipCode(shipCode);
        if (orderService.updateWithOptimisticLocker(order) == 0) {
            return ResponseUtil.updatedDateExpired();
        }

        // TODO 发送邮件和短信通知，这里采用异步发送
        // 发货会发送通知短信给用户: *
        // "尊敬的用户，您购买的${name}已发货（物流公司：${company}；物流单号：${code}）请注意查收，感谢您对未莱生活的支持"
        // notifyService.notifySmsTemplate(order.getMobile(), NotifyType.SHIP, new String[]{shipChannel,
        // shipSn});
        // try {
        // //发送短信给代理
        // this.sendSms(order, shipChannel, shipSn);
        // } catch (Exception e) {
        // logger.error("",e);
        // }


        try {
            // 发送短信通知（快递名、快递公司、快递编号）
            String name = "未莱生活特权卡";
            String company = shipChannel;
            String code = shipSn;
            String templateParam = "{\"name\":\"" + name + "\",\"company\":\"" + company + "\",\"code\":\"" + code + "\"}";
            String signName = AccountStatus.SIGNNAME_AGENT;
            SendSmsResponse sendSmsResponse = AliSmsUtils.sendSms(order.getMobile(), signName, AccountStatus.TEMPLATE_CODE_SHIP, templateParam);
            logger.info("sendSmsResponse结果----- " + JacksonUtils.bean2Jsn(sendSmsResponse));
        } catch (Exception e) {
            logger.error("", e);
        }

        logHelper.logOrderSucceed("发货", "订单编号 " + orderId);
        return ResponseUtil.ok();
    }

    /**
     * 找回
     */
    public Object retrieve(String userid, String orderSn, LocalDateTime begin1, LocalDateTime end1, String type, String value1, List<String> platArray,
                    List<String> orderStatusArray, Integer page, Integer limit, String sort, String order1) throws InvocationTargetException, IllegalAccessException {

        MallLoseOrderExample example = new MallLoseOrderExample();
        MallLoseOrderExample.Criteria criteria = example.createCriteria();

        if (platArray != null && platArray.size() != 0) {
            criteria.andPlatformIn(platArray);
        }
        if (!StringUtils.isEmpty(orderSn)) {
            criteria.andOrderIdLike("%" + orderSn + "%");
        }
        if (!StringUtils.isEmpty(userid)) {
            criteria.andUserIdEqualTo(userid);
        }
        if (orderStatusArray != null && orderStatusArray.size() != 0) {
            criteria.andStatusIn(orderStatusArray);
        }
        if (begin1 != null && end1 != null) {
            criteria.andAddTimeBetween(begin1, end1);
        }
        if (!StringUtils.isEmpty(sort) && !StringUtils.isEmpty(order1)) {
            example.setOrderByClause(sort + " " + order1);
        }
        criteria.andDeletedEqualTo(false);
        PageHelper.startPage(page, limit);


        List<MallLoseOrder> loseOrderr = mallLoseOrderMapper.selectByExample(example);
        long total = PageInfo.of(loseOrderr).getTotal();
        List<MallLoseOrder> loseOrderr1 = new ArrayList<>();
        for (MallLoseOrder order : loseOrderr) {
            MallUser mallUser = userService.findById(order.getUserId());
            MallLoseOrderVo orderVo = new MallLoseOrderVo();

            switch (order.getPlatform()) {
                case "dtk":
                    order.setPlatform("dtk");
                    break;
                case "pdd":
                    order.setPlatform("pdd");
                    break;
                case "sn":
                    order.setPlatform("sn");
                    break;
                case "jd":
                    order.setPlatform("jd");
                    break;
                case "zmGoods":
                    order.setPlatform("zmGoods");
                    break;
                case "fuluGoods":
                    order.setPlatform("fuluGoods");
                    break;
                case "lsxdGoods":
                    order.setPlatform("lsxdGoods");
                    break;
            }
            if (null != mallUser && !StringUtils.isEmpty(mallUser.getNickname())) {
                order.setUserId(mallUser.getNickname());
            } else {
                order.setUserId("");
            }
            if (order.getPrice() == null) {
                order.setPrice(new BigDecimal("0.00"));
            }
            BeanUtils.copyProperties(orderVo, order);
            if (null != mallUser && !StringUtils.isEmpty(mallUser.getMobile())) {
                orderVo.setMobile(mallUser.getMobile());
                orderVo.setAvatar(mallUser.getAvatar());
            }
            loseOrderr1.add(orderVo);
        }


        Map<String, Object> datamap = new HashMap();
        datamap.put("item", loseOrderr1);
        datamap.put("total", total);
        return ResponseUtil.ok(datamap);
    }

    /**
     * 处理
     */
    public Object handle(String body) {


        String orderId = JacksonUtil.parseString(body, "orderId");
        String orderStatus = JacksonUtil.parseString(body, "status");
        String remarks = JacksonUtil.parseString(body, "remarks");
        MallLoseOrderExample example = new MallLoseOrderExample();
        example.createCriteria().andDeletedEqualTo(false).andOrderIdEqualTo(orderId);
        MallLoseOrder mallLoseOrder = mallLoseOrderMapper.selectOneByExampleSelective(example);
        if ("02".equals(orderStatus)) {
            mallLoseOrder.setStatus(orderStatus);
            mallLoseOrder.setRemarks(remarks);
        } else if ("03".equals(orderStatus)) {
            mallLoseOrder.setStatus(orderStatus);
            mallLoseOrder.setRemarks(remarks);
        }


        if (mallLoseOrderMapper.updateByPrimaryKeySelective(mallLoseOrder) > 0) {
            return ResponseUtil.ok();
        }
        return ResponseUtil.fail(403, "处理失败！");

    }


    private void sendSms(MallOrder order, String shipChannel, String shipSn) {
        // 短信通知代理
        if (order.getTpid() != null) {
            MallUser fenXiaoUser = userService.findById(order.getTpid());

            if (fenXiaoUser != null) {

                String smsContent2fenxiao = "";
                /** 先查询商品 **/
                List<MallOrderGoods> orderGoods = orderGoodsService.queryByOid(order.getId());
                int loop = CollectionUtils.isNotEmpty(orderGoods) ? orderGoods.size() : 0;
                for (int i = 0; i < loop; i++) {
                    MallOrderGoods orderGood = orderGoods.get(i);

                    smsContent2fenxiao += "【商品: " + orderGood.getGoodsName() + ",数量: " + orderGood.getNumber() + ",规格: " + Arrays.toString(orderGood.getSpecifications()) + "】";
                }
                String[] fenxiaoPhoneNumbers = {fenXiaoUser.getMobile()};
                String[] params2 = {shipChannel, shipSn, order.getConsignee(), order.getOrderSn(), smsContent2fenxiao};
                int noteSaleMantemplateId = 364274;
                // 364274 发货通知
                // 您的订单已发货! 物流公司：{1}快递单号：{2} 收件人: {3} 订单号：{4} 商品：{5}
                TecentSmsUtil.sendForTemplate(fenxiaoPhoneNumbers, noteSaleMantemplateId, params2);
            }
        }
    }

    /**
     * 回复订单商品
     *
     * @param body 订单信息，{ orderId：xxx }
     * @return 订单操作结果 成功则 { errno: 0, errmsg: '成功' } 失败则 { errno: XXX, errmsg: XXX }
     */
    public Object reply(String body) {
        String commentId = JacksonUtil.parseString(body, "commentId");
        if (commentId == null || "0".equals(commentId)) {
            return ResponseUtil.badArgument();
        }
        // 目前只支持回复一次
        if (commentService.findById(commentId) != null) {
            return ResponseUtil.fail(ORDER_REPLY_EXIST, "订单商品已回复！");
        }
        String content = JacksonUtil.parseString(body, "content");
        if (StringUtils.isEmpty(content)) {
            return ResponseUtil.badArgument();
        }
        // 创建评价回复
        MallComment comment = new MallComment();
        comment.setType((byte) 2);
        comment.setValueId(commentId);
        comment.setContent(content);
        comment.setUserId("0"); // 评价回复没有用
        comment.setStar((byte) 0); // 评价回复没有用
        comment.setHasPicture(false); // 评价回复没有用
        comment.setPicUrls(new String[] {}); // 评价回复没有用
        commentService.save(comment);

        return ResponseUtil.ok();
    }

    public Object queryBuyCardList(String orderSn, String mobile, String status, Integer page, Integer limit, String beginTime, String endTime, boolean getShipCompanyList) {
        MallOrderExample example = new MallOrderExample();
        example.setOrderByClause(MallOrder.Column.id.value() + " desc");
        MallOrderExample.Criteria criteria = example.createCriteria();
        criteria.andBidEqualTo(AccountStatus.BID_14);
        if (!StringUtils.isEmpty(orderSn)) {
            criteria.andOrderSnEqualTo(orderSn);
        }
        if (!StringUtils.isEmpty(mobile)) {
            criteria.andMobileEqualTo(mobile);
        }
        if (!StringUtils.isEmpty(status)) {
            criteria.andOrderStatusEqualTo(new Short(status));
        }
        if (!StringUtils.isEmpty(beginTime)) {
            criteria.andAddTimeGreaterThan(formatTime(beginTime + " 00:00:00"));
        }
        if (!StringUtils.isEmpty(endTime)) {
            criteria.andAddTimeLessThan(formatTime(endTime + " 23:59:59"));
        }
        PageHelper.startPage(page, limit);
        List<MallOrder> mallOrders = mallOrderMapper.selectByExampleSelective(example);
        long total = PageInfo.of(mallOrders).getTotal();

        HashMap<String, Object> data = new HashMap<>();
        data.put("total", total);
        data.put("items", mallOrders);
        if (getShipCompanyList) {
            data.put("shipCompanyList", mallShipCompanyService.list());
        }
        return data;
    }

    private LocalDateTime formatTime(String time) {
        if (StringUtils.isEmpty(time))
            return null;
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");// HH:mm:ss
        return LocalDateTime.parse(time, dtf);
    }

    public List<OrderVo> gameOrder(String type, String userid, int page, int limit) {

        List<MallOrder> orderList = orderService.gameOrder(type, userid, page, limit);
        List<OrderVo> orderList1 = new ArrayList<>();

        BigDecimal UserProfitSum = new BigDecimal("0");// 用户有效总收益
        BigDecimal PlatProfitSum = new BigDecimal("0");// 平台有效总收益
        BigDecimal PubShareFeeSum = new BigDecimal("0");// 订单有效总收益

        for (MallOrder order : orderList) {

            MallUser mallUser = userService.findById(order.getUserId());
            OrderVo orderVo = new OrderVo();
            if (null != mallUser && !StringUtils.isEmpty(mallUser.getNickname())) {
                orderVo.setUserName(mallUser.getNickname());
            } else {
                orderVo.setUserName("");
            }
            if (null != mallUser && !StringUtils.isEmpty(mallUser.getMobile())) {
                orderVo.setMobile(mallUser.getMobile());
            } else {
                orderVo.setMobile("");
            }
            String orderId = order.getId();
            FcAccountPrechargeBill fcAccountPrechargeBill = new FcAccountPrechargeBill();
            fcAccountPrechargeBill.setOrderId(orderId);
            List<FcAccountPrechargeBill> list = fcAccountPrechargeBillService.selectByCondition(fcAccountPrechargeBill);
            Map map = new HashMap();
            BigDecimal userProfit = new BigDecimal("0.00");// 用户收益
            BigDecimal agentProfit = new BigDecimal("0.00");// 代理商收益
            BigDecimal platProfit = new BigDecimal("0.00");// 平台收益
            for (FcAccountPrechargeBill fcp : list) {

                MallUser user = userService.findById(fcp.getCustomerId());
                if (user.getId().equals(mallUser.getId()) && fcp.getDistributionType().equals(AccountStatus.DISTRIBUTION_TYPE_00)) {
                    userProfit = fcp.getAmt();
                    UserProfitSum = UserProfitSum.add(fcp.getAmt());
                } else if (user.getId().equals("00") && fcp.getDistributionType().equals(AccountStatus.DISTRIBUTION_TYPE_10)) {
                    platProfit = fcp.getAmt();
                    PlatProfitSum = PlatProfitSum.add(fcp.getAmt());
                } else {
                    agentProfit = agentProfit.add(fcp.getAmt());
                }

            }
            orderVo.setUserProfit(userProfit);
            orderVo.setAgentProfit(agentProfit);
            orderVo.setPlatProfit(platProfit);
            switch (order.getHuid()) {
                case "sdw":
                    orderVo.setPlatform("闪电玩");
                    break;

            }
            orderVo.setPayTime(order.getPayTime());
            orderVo.setOrderSn(order.getOrderSn());
            orderVo.setOrderPrice(order.getActualPrice());
            orderVo.setPubShareFee(order.getPubSharePreFee());
            orderVo.setAddTime(order.getAddTime());
            orderVo.setOrderStatus(order.getOrderStatus());
            orderVo.setId(order.getId());
            orderList1.add(orderVo);
        }


        return orderList1;
    }

    public Object detailGameOrder(String id) {
        MallOrder order = orderService.findById(id);
        MallUser user1 = userService.findById(order.getUserId());
        FcAccountPrechargeBill fcAccountPrechargeBill = new FcAccountPrechargeBill();
        fcAccountPrechargeBill.setOrderId(id);
        List<FcAccountPrechargeBill> list = fcAccountPrechargeBillService.selectByCondition(fcAccountPrechargeBill);
        BigDecimal agentProfit = new BigDecimal("0.00");// 服务商分佣
        BigDecimal agent1Profit = new BigDecimal("0.00");// 区代分佣
        BigDecimal agent2Profit = new BigDecimal("0.00");// 市代分佣
        String agent = "";// 服务商
        String agent1 = "";// 区代
        String agent2 = "";// 市代
        for (FcAccountPrechargeBill bill : list) {
            MallUser user = userService.findById(bill.getCustomerId());
            if (user.getLevelId().equals("4")) {
                agentProfit = bill.getAmt();
                agent = user.getNickname();
            } else if (user.getLevelId().equals("5")) {
                agent1Profit = bill.getAmt();
                agent1 = user.getNickname();
            } else if (user.getLevelId().equals("6")) {
                agent2Profit = bill.getAmt();
                agent2 = user.getNickname();
            }
        }

        Map map = new HashMap();
        map.put("agent", agent);
        map.put("agent1", agent1);
        map.put("agent2", agent2);
        map.put("agentProfit", agentProfit);
        map.put("agent1Profit", agent1Profit);
        map.put("agent2Profit", agent2Profit);
        map.put("orderSn", order.getOrderSn());
        map.put("orderUser", user1.getNickname());
        map.put("orderStatus", order.getOrderStatus());
        return map;
    }

    public List<MallOrder> getOrderBySn(String userId, String orderSn) {
        FcAccountPrechargeBillExample example = new FcAccountPrechargeBillExample();
        example.createCriteria().andStatusEqualTo((short) 1).andOrderSnEqualTo(orderSn);
        List<FcAccountPrechargeBill> bill = fcAccountPrechargeBillMapper.selectByExample(example);
        if (bill.size() > 0) {
            for (FcAccountPrechargeBill bill1 : bill) {
                if (!"2".equals(bill1.getCheckStatus())) {
                    bill1.setStatus((short) 0);
                    bill1.setBillStatus(AccountStatus.BILL_STATUS_02);
                    fcAccountPrechargeBillMapper.updateByPrimaryKeySelective(bill1);
                }
            }
        }
        List<MallOrder> order = orderService.getByOrderSn(userId, orderSn);
        return order;
    }

    public Object getTotalEarning(String date) throws ExecutionException, InterruptedException {

        LocalDateTime start;
        LocalDateTime end;
        if (date.equals("today")){
            start = LocalDateTimeUtil.beginOfDay(LocalDateTime.now());
            end = LocalDateTimeUtil.endOfDay(LocalDateTime.now());
        } else if (date.equals("yesterday")){
            start = LocalDateTimeUtil.beginOfDay(LocalDateTime.now().plusDays(-1));
            end = LocalDateTimeUtil.endOfDay(LocalDateTime.now().plusDays(-1));
            //历史记录
        } else {
            start = null;
            end = LocalDateTimeUtil.endOfDay(LocalDateTime.now());
        }


                            //创客  达人
        CompletableFuture<List<Map<String, Object>>> future1 = CompletableFuture.supplyAsync(() -> {
            List<String> list = Arrays.asList(AccountStatus.BID_38, AccountStatus.BID_44);
            List<Map<String, Object>> TotalEarning = customMallOrderMapper.getTotalEarning(start, end, list, "201", null);
            return TotalEarning;
        }, JDiworkerConfig.CUSTOMER_COMMON_POOL);


                            //三方电商平台
        CompletableFuture<List<Map<String, Object>>> future2 = CompletableFuture.supplyAsync(() -> {
            List<String> list2 = Arrays.asList(AccountStatus.BID_4, AccountStatus.BID_5, AccountStatus.BID_8, AccountStatus.BID_17, AccountStatus.BID_19);
            List<Map<String, Object>> TotalEarning = customMallOrderMapper.getTotalEarning(start, end, list2, "108", null);
            return TotalEarning;
        }, JDiworkerConfig.CUSTOMER_COMMON_POOL);




                            //自营
        CompletableFuture<List<Map<String, Object>>> future3 = CompletableFuture.supplyAsync(() -> {
            List<String> list3 = Arrays.asList(AccountStatus.BID_27);
            List<Map<String, Object>> TotalEarning= customMallOrderMapper.getTotalEarning(start,end,list3,"301",null);

            return TotalEarning;
        }, JDiworkerConfig.CUSTOMER_COMMON_POOL);
        List<String> list3 = Arrays.asList(AccountStatus.BID_27);




                            //权益
        CompletableFuture<List<Map<String, Object>>> future4 = CompletableFuture.supplyAsync(() -> {
            List<String> list4 = Arrays.asList(AccountStatus.BID_9);
            List<Map<String, Object>> TotalEarning= customMallOrderMapper.getTotalEarning(start,end,list4,null,"200");
            return TotalEarning;
        }, JDiworkerConfig.CUSTOMER_COMMON_POOL);




        BigDecimal ckMoney = new BigDecimal(0.00);
        BigDecimal talentMoney = new BigDecimal(0.00);
        BigDecimal thirdMoney = new BigDecimal(0.00);
        BigDecimal zyMoney = new BigDecimal(0.00);
        BigDecimal qyMoney = new BigDecimal(0.00);

        List<Map<String, Object>> ckTalent = future1.get();

        if (ckTalent != null && ckTalent.size() >0){
            for (Map<String, Object> map : ckTalent) {

                if (map.get("money") != null) {
                    if (map.get("bid").equals("44")) {
                        talentMoney = (BigDecimal) map.get("money");
                    } else {
                        ckMoney = (BigDecimal) map.get("money");
                    }
                }
            }
        }

        List<Map<String, Object>> third = future2.get();
        if (third != null && third.size() >0){
            for (Map<String, Object> map : third) {
                if (map.get("money") != null) {
                    thirdMoney = thirdMoney.add((BigDecimal) map.get("money"));
                }
            }
        }
        List<Map<String, Object>> zy = future3.get();
        if (zy != null && zy.size() >0){
            for (Map<String, Object> map : zy) {
                if (map.get("money") != null) {
                    zyMoney = (BigDecimal) map.get("money");
                }
            }
        }

        List<Map<String, Object>> qy = future4.get();

        if (qy != null && qy.size() >0){
            for (Map<String, Object> map : qy) {
                if (map.get("money") != null) {
                    qyMoney = (BigDecimal) map.get("money");
                }
            }
        }




        HashMap<String, Object> map = new HashMap<>(6);

        BigDecimal total = new BigDecimal(0.00);
        total = total.add(ckMoney).add(talentMoney).add(zyMoney).add(qyMoney).add(thirdMoney);

        map.put("ck",ckMoney);
        map.put("talent",talentMoney);
        map.put("zy",zyMoney);
        map.put("qy",qyMoney);
        map.put("third",thirdMoney);
        map.put("total",total);

        return ResponseUtil.ok(map);

    }


    public Object getTradeStatistic(String date) {
        LocalDateTime start;
        LocalDateTime end;
        if (date.equals("today")){
            start = LocalDateTimeUtil.beginOfDay(LocalDateTime.now());
            end = LocalDateTimeUtil.endOfDay(LocalDateTime.now());
        } else if (date.equals("yesterday")){
            start = LocalDateTimeUtil.beginOfDay(LocalDateTime.now().plusDays(-1));
            end = LocalDateTimeUtil.endOfDay(LocalDateTime.now().plusDays(-1));
            //近30天
        } else if (date.equals("month")){
            start = LocalDateTimeUtil.beginOfDay(LocalDateTime.now().plusDays(-29));
            end = LocalDateTimeUtil.endOfDay(LocalDateTime.now());
        } else {
            start = null;
            end = LocalDateTimeUtil.endOfDay(LocalDateTime.now());
        }

//        QueryWrapper<MallOrder> wrapper1 = new QueryWrapper<>();
//        wrapper1.select("sum(actual_price) actualPrice"," count(1) count"," sum(pub_share_pre_fee) pubShareFee","bid");
//        if (start !=null){
//            wrapper1.between("pay_time",start,end). in("bid",AccountStatus.BID_38,AccountStatus.BID_44,AccountStatus.BID_4,AccountStatus.BID_5,
//                    AccountStatus.BID_8,AccountStatus.BID_17,AccountStatus.BID_19,AccountStatus.BID_27,AccountStatus.BID_9).
//                    eq("order_status", "201").groupBy("bid");
//        }else {
//            //历史查询
//            wrapper1.le("pay_time", end).
//                    in("bid",AccountStatus.BID_38,AccountStatus.BID_44,AccountStatus.BID_4,AccountStatus.BID_5,
//                            AccountStatus.BID_8,AccountStatus.BID_17,AccountStatus.BID_19,AccountStatus.BID_27,AccountStatus.BID_9).
//                    eq("order_status", "201").groupBy("bid");
//        }
//        List<Map<String, Object>> maps = mallOrderMapper.selectMaps(wrapper1);


        List<Map<String, Object>> maps ;
        List<String>   bids = Arrays.asList(AccountStatus.BID_38, AccountStatus.BID_44, AccountStatus.BID_4, AccountStatus.BID_5,
                AccountStatus.BID_8, AccountStatus.BID_17, AccountStatus.BID_19, AccountStatus.BID_27, AccountStatus.BID_9);
        if (start !=null){

            maps =  customMallOrderMapper.getTradeStatistic(start,end,bids);
        }else {
            //历史查询
            maps =  customMallOrderMapper.getTradeStatistic(null,end,bids);

        }







        //接收多类型第三方
        BigDecimal thirdActualPrice = new BigDecimal(0);
        BigDecimal thirdPubShareFee = new BigDecimal(0);
        Long thirdCount = 0L;

        Map<String, Object> ck = new HashMap<>();
        ck.put("actualPrice","0.00");
        ck.put("pubShareFee","0.00");
        ck.put("count","0");
        Map<String, Object> talent = new HashMap<>();
        talent.put("actualPrice","0.00");
        talent.put("pubShareFee","0.00");
        talent.put("count","0");
        Map<String, Object> zy = new HashMap<>();
        zy.put("actualPrice","0.00");
        zy.put("pubShareFee","0.00");
        zy.put("count","0");
        Map<String, Object> qy = new HashMap<>();
        qy.put("actualPrice","0.00");
        qy.put("pubShareFee","0.00");
        qy.put("count","0");
        Map<String, Object> third = new HashMap<>();
        third.put("actualPrice","0.00");
        third.put("pubShareFee","0.00");
        third.put("count","0");

        if (maps != null && maps.size() >0){

        for (Map<String, Object> map : maps) {
            if (((String)map.get("bid")).equals(AccountStatus.BID_38)){
                Object actualPrice = map.get("actualPrice");
                Object pubShareFee = map.get("pubShareFee");
                Object count = map.get("count");
                if (ObjectUtils.isNotEmpty(actualPrice)){
                    ck.put("actualPrice",actualPrice);
                }
                if (ObjectUtils.isNotEmpty(pubShareFee)){
                    ck.put("pubShareFee",new BigDecimal((Double)pubShareFee).setScale(2,BigDecimal.ROUND_DOWN));
                }
                if (ObjectUtils.isNotEmpty(count)){
                    ck.put("count",count);
                }
            }
           else if (((String)map.get("bid")).equals(AccountStatus.BID_44)){
                Object actualPrice = map.get("actualPrice");
                Object pubShareFee = map.get("pubShareFee");
                Object count = map.get("count");
                if (ObjectUtils.isNotEmpty(actualPrice)){
                    talent.put("actualPrice",actualPrice);
                }
                if (ObjectUtils.isNotEmpty(pubShareFee)){
                    talent.put("pubShareFee",new BigDecimal((Double)pubShareFee).setScale(2,BigDecimal.ROUND_DOWN));
                }
                if (ObjectUtils.isNotEmpty(count)){
                    talent.put("count",count);
                }
            }
            else  if (((String)map.get("bid")).equals(AccountStatus.BID_27)){
                Object actualPrice = map.get("actualPrice");
                Object pubShareFee = map.get("pubShareFee");
                Object count = map.get("count");
                if (ObjectUtils.isNotEmpty(actualPrice)){
                    zy.put("actualPrice",actualPrice);
                }
                if (ObjectUtils.isNotEmpty(pubShareFee)){
                    zy.put("pubShareFee",new BigDecimal((Double)pubShareFee).setScale(2,BigDecimal.ROUND_DOWN));
                }
                if (ObjectUtils.isNotEmpty(count)){
                    zy.put("count",count);
                }
            }
            else   if (((String)map.get("bid")).equals(AccountStatus.BID_9)){
                Object actualPrice = map.get("actualPrice");
                Object pubShareFee = map.get("pubShareFee");
                Object count = map.get("count");
                if (ObjectUtils.isNotEmpty(actualPrice)){
                    qy.put("actualPrice",actualPrice);
                }
                if (ObjectUtils.isNotEmpty(pubShareFee)){
                    qy.put("pubShareFee",new BigDecimal((Double)pubShareFee).setScale(2,BigDecimal.ROUND_DOWN));
                }
                if (ObjectUtils.isNotEmpty(count)){
                    qy.put("count",count);
                }
            }
            else {
                Object actualPrice = map.get("actualPrice");
                Object pubShareFee = map.get("pubShareFee");
                Object count = map.get("count");
                if (ObjectUtils.isNotEmpty(actualPrice)){
                    thirdActualPrice =  thirdActualPrice.add((BigDecimal)actualPrice);
                    third.put("actualPrice", thirdActualPrice);
                }
                if (ObjectUtils.isNotEmpty(pubShareFee)){
                    thirdPubShareFee = thirdPubShareFee.add(new BigDecimal((Double) pubShareFee)).setScale(2,BigDecimal.ROUND_DOWN);
                    third.put("pubShareFee",thirdPubShareFee);
                }
                if (ObjectUtils.isNotEmpty(count)){
                    thirdCount += (Long) count;
                    third.put("count",thirdCount);
                }
             }
            }
        }

        HashMap<String, Object> map = new HashMap<>(5);
        map.put("ck",ck);
        map.put("talent",talent);
        map.put("zy",zy);
        map.put("qy",qy);
        map.put("third",third);
        return ResponseUtil.ok(map);
    }

    public Object StatisticOrderStatus() {

        //1,自营，创客订单待发货数量
        List<Map<String, Object>> TotalEarning = customMallOrderMapper.StatisticOrderStatus(AccountStatus.BID_27,AccountStatus.BID_38);

        long waitzy = 0;
        long waitMark = 0;
        if (CollectionUtils.isNotEmpty(TotalEarning)){
            for (Map<String, Object> map : TotalEarning) {
                if ("27".equals(map.get("bid"))){
                    waitzy = (long) map.get("count");
                }else {
                    waitMark = (long) map.get("count");
                }
            }
        }



        //2.自营订单待售后数量 （order_status 退款中203，已退款203  ，换货中，已换货）目前还没有换货退货
        long zyAfterSale =  customMallOrderMapper.getAfterSale(AccountStatus.BID_27,"202","203");
        //3.获取 返佣审核+待审核数量
        long checkCount = customFcAccountPrechargeBillMapper.getCheckCount("1");

        //4.提现待审核与待打款
        long waitPay = 0;
        long waitAudit = 0;
        List<Map<String, Object>>  state  = customFcAccountWithdrawBillMapper.getState();
        if (CollectionUtils.isNotEmpty(state)){
            for (Map<String, Object> map : state) {
                if ("WAIT_PAY".equals(map.get("state"))){
                    waitPay = (long) map.get("count");
                }else {
                    waitAudit = (long) map.get("count");
                }
            }
        }

       // mall_lose_order
            //找回订单 待处理
        long num = customMallLoseOrderMapper.getwatiCheck();

        HashMap<String, Object> map = new HashMap<>();

        map.put("waitzy",waitzy);       //自营订单待发货
        map.put("waitMark",waitMark);   //创客订单待发货
        map.put("zyAfterSale",zyAfterSale); //自营订单售后
        map.put("checkCount",checkCount); //返佣审核
        map.put("waitPay",waitzy);      //提现打款
        map.put("waitAudit",waitAudit); //提现审核
        map.put("loseOrder",num); //找回订单

        return map;
    }
}
