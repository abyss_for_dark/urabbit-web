package com.graphai.mall.admin.web.promotion;

import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.mall.db.domain.MallUniqueVisitor;
import com.graphai.mall.db.service.promotion.MallUniqueVisitorService;
import com.graphai.validator.Order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/admin/mallUniqueVisitor")
@Validated
public class AdminMallUniqueVisitorController {

    @Autowired
    private MallUniqueVisitorService mallUniqueVisitorService;

    @GetMapping("/list")
    public Object list(@RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit,
                       @RequestParam(defaultValue = "id") String sort,
                       @Order @RequestParam(defaultValue = "desc") String order) {
        MallUniqueVisitor mallUniqueVisitor = new MallUniqueVisitor();
        List<MallUniqueVisitor> mallUniqueVisitorList = mallUniqueVisitorService.selectMallUniqueVisitorList(mallUniqueVisitor, page, limit, sort, order);
        long total = PageInfo.of(mallUniqueVisitorList).getTotal();
        Map<String, Object> data = new HashMap<>();
        data.put("total", total);
        data.put("items", mallUniqueVisitorList);

        return ResponseUtil.ok(data);
    }
}
