package com.graphai.mall.admin.web.helper;

import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.mall.db.domain.MallHelpCategory;
import com.graphai.mall.db.service.helper.MallHelpCategoryService;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 帮助类目
 *
 * @author LaoGF
 * @since 2020-6-24
 */
@RestController
@RequestMapping("/admin/helpCategory")
@Validated
public class AdminMallHelpCategoryController {

    @Resource
    private MallHelpCategoryService mallHelpCategoryService;


    /**
     * 列表
     */
    @RequiresPermissions("admin:helpCategory:list")
    @RequiresPermissionsDesc(menu = {"帮助服务", "帮助类目"}, button = "查询")
    @GetMapping("/list")
    public Object list(
            @RequestParam(required = false) String name,
            @RequestParam(required = false) Integer level,
            @RequestParam(required = false) Integer state,
            @RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer limit) {

        List<MallHelpCategory> list = mallHelpCategoryService.selectByExample(name, level, state, page, limit);
        List<MallHelpCategory> options = mallHelpCategoryService.selectAllByExample(null, 1, null);
        long total = PageInfo.of(list).getTotal();
        Map<String, Object> data = new HashMap<>(16);
        data.put("total", total);
        data.put("items", list);
        data.put("options", options);
        return ResponseUtil.ok(data);
    }


    /**
     * 添加一级类目
     */
    @RequiresPermissions("admin:helpCategory:add")
    @RequiresPermissionsDesc(menu = {"帮助服务", "帮助类目"}, button = "添加一级类目")
    @PostMapping("/add")
    public Object add(@RequestBody MallHelpCategory mallHelpCategory) {
        if (mallHelpCategoryService.insert(mallHelpCategory)) {
            return ResponseUtil.ok();
        }
        return ResponseUtil.fail();
    }

    /**
     * 添加二级类目
     */
    @RequiresPermissions("admin:helpCategory:add2")
    @RequiresPermissionsDesc(menu = {"帮助服务", "帮助类目"}, button = "添加二级类目")
    @PostMapping("/add2")
    public Object add2(@RequestBody MallHelpCategory mallHelpCategory) {
        if (mallHelpCategoryService.insert2(mallHelpCategory)) {
            return ResponseUtil.ok();
        }
        return ResponseUtil.fail();
    }

    /**
     * 修改
     */
    @RequiresPermissions("admin:helpCategory:update")
    @RequiresPermissionsDesc(menu = {"帮助服务", "帮助类目"}, button = "修改")
    @PostMapping("/update")
    public Object update(@RequestBody MallHelpCategory mallHelpCategory) {
        if (mallHelpCategoryService.update(mallHelpCategory)) {
            return ResponseUtil.ok();
        }
        return ResponseUtil.fail();
    }

    /**
     * 删除
     */
    @RequiresPermissions("admin:helpCategory:delete")
    @RequiresPermissionsDesc(menu = {"帮助服务", "帮助类目"}, button = "删除")
    @PostMapping("/delete")
    public Object delete(@RequestParam String id) {
        if (mallHelpCategoryService.deleteByID(id)) {
            return ResponseUtil.ok();
        }
        return ResponseUtil.fail();
    }

    /**
     * 状态修改
     */
    @RequiresPermissions("admin:helpCategory:updateStatus")
    @RequiresPermissionsDesc(menu = {"帮助服务", "帮助类目"}, button = "状态修改")
    @PostMapping("/updateStatus")
    public Object updateStatus(@RequestParam String id,
                               @RequestParam Integer state) {
        if (mallHelpCategoryService.updateStatus(id, state)) {
            return ResponseUtil.ok();
        }
        return ResponseUtil.fail();
    }


}
