package com.graphai.mall.admin.web.category;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

import cn.hutool.core.util.StrUtil;
import com.graphai.mall.db.dao.CustomCategoryMapper;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.checkerframework.checker.units.qual.A;
import org.hibernate.validator.internal.util.StringHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.json.JacksonUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.mall.admin.vo.CategoryVo;
import com.graphai.mall.db.domain.MallCategory;
import com.graphai.mall.db.domain.MallIndustry;
import com.graphai.mall.db.domain.MallPositionCode;
import com.graphai.mall.db.service.MallPositionCodeService;
import com.graphai.mall.db.service.category.MallCategoryService;
import com.graphai.validator.Order;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/admin/category")
@Validated
@Slf4j
public class AdminCategoryController {
    private final Log logger = LogFactory.getLog(AdminCategoryController.class);

    @Autowired
    private MallCategoryService categoryService;
    @Autowired
    private MallPositionCodeService mallPositionCodeService;

    @RequiresPermissions("admin:category:list")
    @RequiresPermissionsDesc(menu = {"商场管理", "类目管理"}, button = "查询")
    @GetMapping("/list")
    public Object list(String positionCode) {
        log.info("进来了---------------------------");
        List<CategoryVo> categoryVoList = new ArrayList<>();
        List<MallCategory> allCategory = null;
        // 获取所有类目
        if ("zyCategory".equals(positionCode)) {
            allCategory = categoryService.getAllZyCategory();
        } else {
            allCategory = categoryService.getAllCategory();
        }

        // 存放子类的集合
        List<MallCategory> subList = new ArrayList<>();

        // pidMaps 用于存放所有pid的值，用于通过 cd /home/qsmall-admin-8080/logs
        // tail -f console.logmap的数据寻找下面的值
        Map<String, List<MallCategory>> pidMaps = new HashMap<>();

        for (MallCategory mallCategory : allCategory) {

            // 实现，通过id 找到id下面的所有子类目
            if (!pidMaps.containsKey(mallCategory.getPid())) {
                subList = new ArrayList<>();
            } else {
                // 获取有pid的key，进行继续追加
                if (pidMaps.get(mallCategory.getPid()) != null) {
                    subList = pidMaps.get(mallCategory.getPid());
                }
            }
            subList.add(mallCategory);
            pidMaps.put(mallCategory.getPid(), subList);
        }
        log.info("pidMaps{},categoryVoList{},pidMaps.get(\"0\"){}", pidMaps, categoryVoList, pidMaps.get("0"));
        getCategoryList(pidMaps, categoryVoList, null, pidMaps.get("0"));

        return ResponseUtil.ok(categoryVoList);
    }

    /** ------------类目------------ **/
    /**
     * 类目管理_查询类目
     */
    @RequiresPermissions("admin:category:categoryList")
    @RequiresPermissionsDesc(menu = {"商品管理", "类目管理"}, button = "查询")
    @GetMapping("/goodsCategoryList")
    public Object goodsCategoryList(MallCategory mallCategory, @RequestParam(defaultValue = "1") Integer page,
                    @RequestParam(defaultValue = "10") Integer limit,
                    @RequestParam(defaultValue = "sort_order") String sort,
                    @Order @RequestParam(defaultValue = "asc") String order) {
        // 查询商品positionCode列表
        // List<String> typeList = new ArrayList<>();
        // typeList.add("1"); //自营
        // typeList.add("2"); //第三方
        // typeList.add("3"); //权益
        // typeList.add("4"); //卡券商品
        List<MallPositionCode> mallPositionCodeList =
                        mallPositionCodeService.selectMallPositionCodeList(null, "type", "asc");
        List<String> positionCodeList = new ArrayList<>();
        if (mallPositionCodeList.size() > 0) {
            for (MallPositionCode mallPositionCode : mallPositionCodeList) {
                if (!StringUtils.isEmpty(mallPositionCode.getPositionCode())) {
                    positionCodeList.add(mallPositionCode.getPositionCode());
                }
            }
        }

        // 获取所有商品类目
        List<MallCategory> mallCategoryList =
                        categoryService.goodsCategory(mallCategory, positionCodeList, page, limit, sort, order);
        long total = PageInfo.of(mallCategoryList).getTotal();
        Map<String, Object> data = new HashMap<>();
        data.put("total", total);
        data.put("items", mallCategoryList);
        data.put("mallPositionCodeList", mallPositionCodeList); // 平台
        return ResponseUtil.ok(data);
    }


    /**
     * 自定义类目
     */
    @RequiresPermissions("admin:category:zyCategoryList")
    @RequiresPermissionsDesc(menu = {"商品管理", "自定义类目"}, button = "查询")
    @GetMapping("/zyCategoryList")
    public Object zyCategoryList(String status, String name, String positionCode, String level) {
        List<MallCategory> mallCategoryList = categoryService.queryByPositionCode(status, name, positionCode,level);

        return ResponseUtil.ok(mallCategoryList);
    }

    /**
     * 自定义类目
     */
    @RequiresPermissions("admin:category:merchant")
    @RequiresPermissionsDesc(menu = {"商品管理", "自定义类目"}, button = "查询")
    @GetMapping("/merchantCategoryList")
    public Object merchantCategoryList(String status, String name, String positionCode) {
        List<MallCategory> mallCategoryList = new ArrayList<>();
        if(StrUtil.isNotEmpty(positionCode)) {
            mallCategoryList = categoryService.queryByPositionCode(status, name,positionCode,null);
        }else{
            mallCategoryList = categoryService.queryByPositionCode(status, name,"fangtao",null);
        }
        return ResponseUtil.ok(mallCategoryList);
    }


    /**
     * 类目管理_通过一级类目查询平台
     */
    @GetMapping("/positionCodeList")
    public Object positionCodeList(@RequestParam(required = false) String type) {
        // 查询商品positionCode列表
        // List<String> typeList = new ArrayList<>();
        // if(!StringUtils.isEmpty(type)){
        // typeList.add(type);
        // }
        MallPositionCode mallPositionCode = new MallPositionCode();
        mallPositionCode.setType(type);
        List<MallPositionCode> mallPositionCodeList =
                        mallPositionCodeService.selectMallPositionCodeList(mallPositionCode, "type", "asc");
        return ResponseUtil.ok(mallPositionCodeList);
    }

    /**
     * 类目管理_添加类目
     */
    @RequiresPermissions("admin:category:categoryAdd")
    @RequiresPermissionsDesc(menu = {"商品管理", "类目管理"}, button = "添加")
    @PostMapping("/create")
    public Object create(@RequestBody MallCategory category) {
        Object error = validate(category);
        if (error != null) {
            return error;
        }
        categoryService.add(category);
        return ResponseUtil.ok(category);
    }

    /**
     * 类目管理_修改类目
     */
    @RequiresPermissions("admin:category:update")
    @RequiresPermissionsDesc(menu = {"商品管理", "类目管理"}, button = "编辑")
    @PostMapping("/update")
    public Object update(@RequestBody MallCategory category) {
        // Object error = validate(category);
        // if (error != null) {
        // return error;
        // }
        if (categoryService.updateById(category) == 0) {
            return ResponseUtil.updatedDataFailed();
        }
        return ResponseUtil.ok();
    }

    /**
     * 类目管理_删除类目
     */
    @RequiresPermissions("admin:category:delete")
    @RequiresPermissionsDesc(menu = {"商品管理", "类目管理"}, button = "删除")
    @PostMapping("/delete")
    public Object delete(@NotNull String id) {
        MallCategory mallCategory = new MallCategory();
        mallCategory.setId(id);
        mallCategory.setDeleted(true);
        if (categoryService.updateById(mallCategory) == 0) {
            return ResponseUtil.updatedDataFailed();
        }
        return ResponseUtil.ok();
    }


    /** ------------频道------------ **/
    /**
     * 首页频道
     */
    @RequiresPermissions("admin:category:listCategory")
    @RequiresPermissionsDesc(menu = {"频道管理", "首页频道"}, button = "查询")
    @GetMapping("/listCategory")
    public Object listCategory(@RequestParam(defaultValue = "") String name,
                    @RequestParam(defaultValue = "2") String isHost, @RequestParam(defaultValue = "") String industryId,
                    @RequestParam(defaultValue = "") String status,
                    @RequestParam(defaultValue = "") String navigatorType,
                    @RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit) {
        log.info("进来了---------------------------");
        List<CategoryVo> categoryVoList = new ArrayList<>();

        // 获取所有频道
        List<MallCategory> allCategory = null;
        if (isHost.equals("2")) {
            allCategory = categoryService.getAllCategoryList(status, navigatorType, name, "", industryId, page, limit);
        } else {
            allCategory = categoryService.getAllCategoryList(status, navigatorType, name, isHost, industryId, page,
                            limit);
        }

        // 存放子类的集合
        List<MallCategory> subList = new ArrayList<>();

        // pidMaps 用于存放所有pid的值，用于通过 cd /home/qsmall-admin-8080/logs
        // tail -f console.logmap的数据寻找下面的值
        Map<String, List<MallCategory>> pidMaps = new HashMap<>();
        List<CategoryVo> list = null;
        if (allCategory.size() > 0) {
            for (MallCategory mallCategory : allCategory) {

                // 实现，通过id 找到id下面的所有子类目
                if (!pidMaps.containsKey(mallCategory.getPid())) {
                    subList = new ArrayList<>();
                } else {
                    // 获取有pid的key，进行继续追加
                    if (pidMaps.get(mallCategory.getPid()) != null) {
                        subList = pidMaps.get(mallCategory.getPid());
                    }
                }
                subList.add(mallCategory);
                pidMaps.put(mallCategory.getPid(), subList);
            }
            log.info("pidMaps{},categoryVoList{},pidMaps.get(\"0\"){}", pidMaps, categoryVoList, pidMaps.get("0"));
            list = getCategoryList1(pidMaps, categoryVoList, null, pidMaps.get("0"));
        }
        long total = PageInfo.of(allCategory).getTotal();
        Map<String, Object> data = new HashMap<>();
        data.put("total", total);
        data.put("items", list);
        return ResponseUtil.ok(data);
    }

    /**
     * 获取所有类目
     *
     * @param pidMaps key=pid value=MallCategory 第一次: level=L1 代表一级目录
     * @param finallyList 返回最终的结果list
     * @param pidCategory 存放当前父类，用于pidCategory.setChildren
     * @param categoryList 获取当前类目所有的子类
     * @return
     */
    private List<CategoryVo> getCategoryList1(Map<String, List<MallCategory>> pidMaps, List<CategoryVo> finallyList,
                    CategoryVo pidCategory, List<MallCategory> categoryList) {
        // 引用对象，减少内存开支
        CategoryVo categoryVO;
        List<CategoryVo> childrenList = null;
        List<MallCategory> mallCategoriesList;

        for (MallCategory category : categoryList) {
            categoryVO = new CategoryVo();
            categoryVO.setId(category.getId());
            categoryVO.setDesc(category.getDesc());
            categoryVO.setIconUrl(category.getIconUrl());
            categoryVO.setPicUrl(category.getPicUrl());
            categoryVO.setKeywords(category.getKeywords());
            categoryVO.setName(category.getName());
            categoryVO.setLevel(category.getLevel());
            categoryVO.setPid(category.getPid());
            categoryVO.setPositionCode(category.getPositionCode());
            categoryVO.setSortOrder(category.getSortOrder());
            // 新加字段
            categoryVO.setIsHost(category.getIsHost());
            categoryVO.setSmallIconUrl(category.getSmallIconUrl());
            categoryVO.setHostUrl(category.getHostUrl());
            categoryVO.setNavigatorType(category.getNavigatorType());
            categoryVO.setChainChange(category.getChainChange());
            if (null != category.getHostSort() && !"".equals(category.getHostSort())) {
                categoryVO.setHostSort(category.getHostSort().toString());
            }
            categoryVO.setStatus(category.getStatus());
            if (null != category.getIndustryId() && !"".equals(category.getIndustryId())) {
                MallIndustry mallIndustry = categoryService.selectIndustry(category.getIndustryId());
                if (null != mallIndustry) {
                    categoryVO.setIndustry(mallIndustry.getId());
                }
            }
            categoryVO.setSortOrder(category.getSortOrder());
            categoryVO.setLinkUrl(category.getLinkUrl());
            // 获取当前类目的子类
            mallCategoriesList = pidMaps.get(categoryVO.getId());
            if (CollectionUtils.isNotEmpty(mallCategoriesList)) {
                // 进行递归调用，返回子类的结果集，赋值给父类的children 属性
                childrenList = getCategoryList1(pidMaps, new ArrayList<>(), categoryVO, mallCategoriesList);
            } else {
                childrenList = new ArrayList<>();

            }

            // 添加当前类目的子类 = 空/childrenList
            categoryVO.setChildren(childrenList);
            finallyList.add(categoryVO);
        }
        return finallyList;
    }

    @RequiresPermissions("admin:category:addCategory")
    @RequiresPermissionsDesc(menu = {"频道管理", "首页频道"}, button = "添加")
    @PostMapping("/addCategory")
    public Object createCategory(@RequestBody MallCategory category) {
        Object error = validate(category);
        if (error != null) {
            return error;
        }
        category.setPositionCode("home");
        List<MallCategory> list = categoryService.queryIndustryId(category.getIndustryId());
        if (list.size() < 15) {
            categoryService.add(category);
        } else {
            return ResponseUtil.fail(403, "该行业下已有15个启用状态的频道，若想再启用新的频道，请先禁用其它频道！");
        }
        return ResponseUtil.ok(category);
    }


    @RequiresPermissions("admin:category:disableCategory")
    @RequiresPermissionsDesc(menu = {"频道管理", "首页频道"}, button = "启用/禁用")
    @PostMapping("/disableCategory")
    public Object disableCategory(@RequestBody MallCategory category) {
        Object error = validate(category);
        if (error != null) {
            return error;
        }
        if ("0".equals(category.getStatus())) {
            category.setStatus("1");
        } else if ("1".equals(category.getStatus())) {
            category.setStatus("0");
        } else {
            category.setStatus("1");
        }

        if (categoryService.updateById(category) == 0) {
            return ResponseUtil.updatedDataFailed();
        }
        return ResponseUtil.ok();
    }

    @RequiresPermissions("admin:category:deleteCategory")
    @RequiresPermissionsDesc(menu = {"频道管理", "首页频道"}, button = "删除")
    @PostMapping("/deleteCategory")
    public Object deleteCategory(@RequestBody MallCategory category) {
//        Object error = validate(category);
//        if (error != null) {
//            return error;
//        }
        category.setDeleted(true);
        if (categoryService.updateById(category) == 0) {
            return ResponseUtil.updatedDataFailed();
        }
        return ResponseUtil.ok();
    }

    @RequiresPermissions("admin:category:updateCategory")
    @RequiresPermissionsDesc(menu = {"频道管理", "首页频道"}, button = "编辑")
    @PostMapping("/updateCategory")
    public Object updateCategory(@RequestBody MallCategory category) {
        Object error = validate(category);
        if (error != null) {
            return error;
        }
        category.setPositionCode("home");
        if (categoryService.updateById(category) == 0) {
            return ResponseUtil.updatedDataFailed();
        }
        return ResponseUtil.ok();
    }

    /**
     * 查询行业
     */
    @RequiresPermissions("admin:category:selectIndustry")
    @RequiresPermissionsDesc(menu = {"频道管理", "行业管理"}, button = "查询")
    @GetMapping("/selectIndustry")
    public Object selectIndustry(@RequestParam(required = false) String name,
                    @RequestParam(required = false) String position, @RequestParam(required = false) String remark,
                    @RequestParam(required = false) String status, @RequestParam(defaultValue = "1") Integer page,
                    @RequestParam(defaultValue = "10") Integer limit) {
        List<MallIndustry> list = categoryService.selectIndustryList(name, position, remark, status, page, limit);
        long total = PageInfo.of(list).getTotal();
        Map<String, Object> data = new HashMap<>();
        data.put("total", total);
        data.put("items", list);
        return ResponseUtil.ok(data);
    }


    @RequiresPermissions("admin:category:createIndustry")
    @RequiresPermissionsDesc(menu = {"频道管理", "行业管理"}, button = "添加")
    @PostMapping("/createIndustry")
    public Object createIndustry(@RequestBody MallIndustry industry) {
        Object error = validateIndustry(industry);
        if (error != null) {
            return error;
        }
        industry.setType("home");
        categoryService.addIndustry(industry);
        return ResponseUtil.ok(industry);
    }

    @RequiresPermissions("admin:category:updateIndustry")
    @RequiresPermissionsDesc(menu = {"频道管理", "行业管理"}, button = "编辑")
    @PostMapping("/updateIndustry")
    public Object updateIndustry(@RequestBody MallIndustry industry) {
        Object error = validateIndustry(industry);
        if (error != null) {
            return error;
        }

        if (categoryService.updateIndustryById(industry) == 0) {
            return ResponseUtil.updatedDataFailed();
        }
        return ResponseUtil.ok();
    }

    @RequiresPermissions("admin:category:disableIndustry")
    @RequiresPermissionsDesc(menu = {"频道管理", "行业管理"}, button = "启用/禁用")
    @PostMapping("/disableIndustry")
    public Object disableIndustry(@RequestBody MallIndustry industry) {
        Object error = validateIndustry(industry);
        if (error != null) {
            return error;
        }
        if ("0".equals(industry.getStatus())) {
            industry.setStatus("1");
        } else {
            industry.setStatus("0");
        }
        if (categoryService.updateIndustryById(industry) == 0) {
            return ResponseUtil.updatedDataFailed();
        }
        return ResponseUtil.ok();
    }

    @RequiresPermissions("admin:category:deleteIndustry")
    @RequiresPermissionsDesc(menu = {"频道管理", "行业管理"}, button = "删除")
    @PostMapping("/deleteIndustry")
    public Object deleteIndustry(@RequestBody MallIndustry industry) {
        Object error = validateIndustry(industry);
        if (error != null) {
            return error;
        }
        industry.setDeleted(true);
        if (categoryService.updateIndustryById(industry) == 0) {
            return ResponseUtil.updatedDataFailed();
        }
        return ResponseUtil.ok();
    }

    /**
     * 获取所有类目
     *
     * @param pidMaps key=pid value=MallCategory 第一次: level=L1 代表一级目录
     * @param finallyList 返回最终的结果list
     * @param pidCategory 存放当前父类，用于pidCategory.setChildren
     * @param categoryList 获取当前类目所有的子类
     * @return
     */
    private List<CategoryVo> getCategoryList(Map<String, List<MallCategory>> pidMaps, List<CategoryVo> finallyList,
                    CategoryVo pidCategory, List<MallCategory> categoryList) {
        // 引用对象，减少内存开支
        CategoryVo categoryVO;
        List<CategoryVo> childrenList = null;
        List<MallCategory> mallCategoriesList;

        for (MallCategory category : categoryList) {
            categoryVO = new CategoryVo();
            categoryVO.setId(category.getId());
            categoryVO.setDesc(category.getDesc());
            categoryVO.setIconUrl(category.getIconUrl());
            categoryVO.setPicUrl(category.getPicUrl());
            categoryVO.setKeywords(category.getKeywords());
            categoryVO.setName(category.getName());
            categoryVO.setLevel(category.getLevel());
            categoryVO.setPid(category.getPid());
            categoryVO.setPositionCode(category.getPositionCode());
            categoryVO.setSortOrder(category.getSortOrder());
            // 新加字段
            categoryVO.setIsHost(category.getIsHost());
            categoryVO.setHostUrl(category.getHostUrl());
            categoryVO.setNavigatorType(category.getNavigatorType());
            if (null != category.getHostSort() && !"".equals(category.getHostSort())) {
                categoryVO.setHostSort(category.getHostSort().toString());
            }
            categoryVO.setStatus(category.getStatus());
            if (null != category.getIndustryId() && !"".equals(category.getIndustryId())) {
                MallIndustry mallIndustry = categoryService.selectIndustry(category.getIndustryId());
                if (null != mallIndustry) {
                    categoryVO.setIndustry(mallIndustry.getIname());
                }
            }
            categoryVO.setSortOrder(category.getSortOrder());
            categoryVO.setLinkUrl(category.getLinkUrl());
            // 获取当前类目的子类
            mallCategoriesList = pidMaps.get(categoryVO.getId());
            if (CollectionUtils.isNotEmpty(mallCategoriesList)) {
                // 进行递归调用，返回子类的结果集，赋值给父类的children 属性
                childrenList = getCategoryList(pidMaps, new ArrayList<>(), categoryVO, mallCategoriesList);
            } else {
                childrenList = new ArrayList<>();
            }

            // 添加当前类目的子类 = 空/childrenList
            categoryVO.setChildren(childrenList);
            finallyList.add(categoryVO);
        }
        return finallyList;
    }

    /*
     * public Object list() { List<CategoryVo> categoryVoList = new ArrayList<>();
     *
     * List<MallCategory> allCategory = categoryService.getAllCategory();
     *
     * List<MallCategory> list = new ArrayList<>(); List<MallCategory> list2 = new ArrayList<>();
     *
     * //map 存放level = L1的数据 Map<String, List<MallCategory>> map = new HashMap<>(); //map2
     * 用于存放所有pid的值，用于通过 map的数据寻找下面的值 Map<String, List<MallCategory>> map2 = new HashMap<>();
     *
     * for (MallCategory mallCategory : allCategory) {
     *
     * //只获取L1的类别 if (Objects.equals(mallCategory.getLevel(), "L1")) { if
     * (!map.containsKey(mallCategory.getLevel())) { list = new ArrayList<>(); } list.add(mallCategory);
     * map.put(mallCategory.getLevel(), list); }
     *
     * //实现，通过id 找到id下面的所有子类目 if (!map2.containsKey(mallCategory.getPid())) { list2 = new ArrayList<>();
     * } else { //获取有pid的key，进行继续追加 if (map2.get(mallCategory.getPid()) != null) { list2 =
     * map2.get(mallCategory.getPid()); } } list2.add(mallCategory); map2.put(mallCategory.getPid(),
     * list2); }
     *
     * //进行对象引用 CategoryVo categoryVO = null; CategoryVo subCategoryVo = null; CategoryVo
     * thirdCategoryVo = null; CategoryVo fourCategoryVo = null;
     *
     * List<CategoryVo> children = null; List<CategoryVo> thirdChildren = null; List<CategoryVo>
     * fourChildren = null;
     *
     * List<MallCategory> thirdCategoryList = null; //暂时只支持四级类目 //循环逻辑：
     * 拿到所有一级类目，下面的二级类目，下面的三级类目，下面的四级类目，合并返回给前端 List<MallCategory> categoryList = map.get("L1"); for
     * (MallCategory category : categoryList) { categoryVO = new CategoryVo();
     * categoryVO.setId(category.getId()); categoryVO.setDesc(category.getDesc());
     * categoryVO.setIconUrl(category.getIconUrl()); categoryVO.setPicUrl(category.getPicUrl());
     * categoryVO.setKeywords(category.getKeywords()); categoryVO.setName(category.getName());
     * categoryVO.setLevel(category.getLevel()); categoryVO.setPid(category.getPid());
     *
     * children = new ArrayList<>();
     *
     * List<MallCategory> subCategoryList = map2.get(category.getId()); if
     * (CollectionUtils.isEmpty(subCategoryList)) { continue; } for (MallCategory subCategory :
     * subCategoryList) { subCategoryVo = new CategoryVo(); subCategoryVo.setId(subCategory.getId());
     * subCategoryVo.setDesc(subCategory.getDesc()); subCategoryVo.setIconUrl(subCategory.getIconUrl());
     * subCategoryVo.setPicUrl(subCategory.getPicUrl());
     * subCategoryVo.setKeywords(subCategory.getKeywords());
     * subCategoryVo.setName(subCategory.getName()); subCategoryVo.setLevel(subCategory.getLevel());
     * subCategoryVo.setPid(subCategory.getPid());
     *
     * children.add(subCategoryVo);
     *
     *
     * thirdCategoryList = map2.get(subCategory.getId()); if
     * (CollectionUtils.isEmpty(thirdCategoryList)) { continue; }
     *
     * thirdChildren = new ArrayList<>(); for (MallCategory mallCategory : thirdCategoryList) {
     * thirdCategoryVo = new CategoryVo(); thirdCategoryVo.setId(mallCategory.getId());
     * thirdCategoryVo.setDesc(mallCategory.getDesc());
     * thirdCategoryVo.setIconUrl(mallCategory.getIconUrl());
     * thirdCategoryVo.setPicUrl(mallCategory.getPicUrl());
     * thirdCategoryVo.setKeywords(mallCategory.getKeywords());
     * thirdCategoryVo.setName(mallCategory.getName());
     * thirdCategoryVo.setLevel(mallCategory.getLevel()); thirdCategoryVo.setPid(mallCategory.getPid());
     *
     * thirdChildren.add(thirdCategoryVo);
     *
     *
     * List<MallCategory> fourCategoryList = map2.get(thirdCategoryVo.getId()); if
     * (CollectionUtils.isEmpty(fourCategoryList)) { continue; } fourChildren = new ArrayList<>();
     *
     * for (MallCategory mallCategory1 : fourCategoryList) { fourCategoryVo = new CategoryVo();
     * fourCategoryVo.setId(mallCategory1.getId()); fourCategoryVo.setDesc(mallCategory1.getDesc());
     * fourCategoryVo.setIconUrl(mallCategory1.getIconUrl());
     * fourCategoryVo.setPicUrl(mallCategory1.getPicUrl());
     * fourCategoryVo.setKeywords(mallCategory1.getKeywords());
     * fourCategoryVo.setName(mallCategory1.getName());
     * fourCategoryVo.setLevel(mallCategory1.getLevel()); fourCategoryVo.setPid(mallCategory1.getPid());
     * fourChildren.add(fourCategoryVo); }
     *
     * thirdCategoryVo.setChildren(fourChildren);
     *
     * } subCategoryVo.setChildren(thirdChildren);
     *
     * }
     *
     * categoryVO.setChildren(children); categoryVoList.add(categoryVO); }
     *
     * return ResponseUtil.ok(categoryVoList); }
     */

    /*
     * @RequiresPermissions("admin:category:list")
     *
     * @RequiresPermissionsDesc(menu={"商场管理" , "类目管理"}, button="查询")
     *
     * @GetMapping("/list") public Object list() { List<CategoryVo> categoryVoList = new ArrayList<>();
     *
     * List<MallCategory> categoryList = categoryService.queryByPid("0"); for(MallCategory category :
     * categoryList){ CategoryVo categoryVO = new CategoryVo(); categoryVO.setId(category.getId());
     * categoryVO.setDesc(category.getDesc()); categoryVO.setIconUrl(category.getIconUrl());
     * categoryVO.setPicUrl(category.getPicUrl()); categoryVO.setKeywords(category.getKeywords());
     * categoryVO.setName(category.getName()); categoryVO.setLevel(category.getLevel());
     *
     * List<CategoryVo> children = new ArrayList<>(); List<MallCategory> subCategoryList =
     * categoryService.queryByPid(category.getId()); for(MallCategory subCategory : subCategoryList){
     * CategoryVo subCategoryVo = new CategoryVo(); subCategoryVo.setId(subCategory.getId());
     * subCategoryVo.setDesc(subCategory.getDesc()); subCategoryVo.setIconUrl(subCategory.getIconUrl());
     * subCategoryVo.setPicUrl(subCategory.getPicUrl());
     * subCategoryVo.setKeywords(subCategory.getKeywords());
     * subCategoryVo.setName(subCategory.getName()); subCategoryVo.setLevel(subCategory.getLevel());
     *
     * children.add(subCategoryVo); }
     *
     * categoryVO.setChildren(children); categoryVoList.add(categoryVO); }
     *
     * return ResponseUtil.ok(categoryVoList); }
     */

    private Object validate(MallCategory category) {
        if (StringUtils.isEmpty(category.getName())) {
            return ResponseUtil.badArgument();
        }
        if (StringUtils.isEmpty(category.getLevel())) {
            return ResponseUtil.badArgument();
        }

        return null;
    }

    private Object validateIndustry(MallIndustry industry) {
        String name = industry.getIname();
        if (StringUtils.isEmpty(name)) {
            return ResponseUtil.badArgument();
        }


        return null;
    }


    @RequiresPermissions("admin:category:read")
    @RequiresPermissionsDesc(menu = {"商场管理", "类目管理"}, button = "详情")
    @GetMapping("/read")
    public Object read(@NotNull String id) {
        MallCategory category = categoryService.findById(id);
        return ResponseUtil.ok(category);
    }


    @RequiresPermissions("admin:category:list")
    @GetMapping("/l1")
    public Object catL1(String positionCode) {
        // 所有一级分类目录
        List<MallCategory> l1CatList = categoryService.queryL1(positionCode);
        List<Map<String, Object>> data = new ArrayList<>(l1CatList.size());
        for (MallCategory category : l1CatList) {
            Map<String, Object> d = new HashMap<>(2);
            d.put("value", category.getId());
            d.put("label", category.getName());
            data.add(d);
        }
        return ResponseUtil.ok(data);
    }


    @GetMapping("/queryByPid")
    public Object queryByPid(String pid) {
        // 所有一级分类目录
        List<MallCategory> CatList = categoryService.queryByPid(pid);
        return ResponseUtil.ok(CatList);
    }

    @RequiresPermissions("admin:category:list")
    @GetMapping("/query")
    public Object query(@RequestParam(required = false) String name, String positionCode, String level) {
        // 所有一级分类目录
        List<MallCategory> categoryList = categoryService.query(name, positionCode, level);
        List<Map<String, Object>> data = new ArrayList<>(categoryList.size());
        for (MallCategory category : categoryList) {
            Map<String, Object> d = new HashMap<>(2);
            d.put("value", category.getId());
            d.put("label", category.getName());
            data.add(d);
        }
        return ResponseUtil.ok(data);
    }



    @GetMapping("/fangtao/query")
    public Object fangtaoQuery() {
        List<MallCategory> mallCategories = categoryService.getFangtaoCategory("zyCategory");
        return ResponseUtil.ok(mallCategories);
    }

    @RequiresPermissions("admin:category:list")
    @GetMapping("/level")
    public Object catLevel(String level) {
        if (StringHelper.isNullOrEmptyString(level)) {
            level = null;
        }
        // 所有一级分类目录
        List<MallCategory> l1CatList = categoryService.queryLevel(level, "categoryRights");
        List<Map<String, Object>> data = new ArrayList<>(l1CatList.size());
        for (MallCategory category : l1CatList) {
            Map<String, Object> d = new HashMap<>(2);
            d.put("value", category.getId());
            d.put("label", category.getName());
            data.add(d);
        }
        return ResponseUtil.ok(data);
    }

    /**
     * 单页管理
     */
    @RequiresPermissions("admin:category:listSingePage")
    @RequiresPermissionsDesc(menu = {"频道管理", "单页管理"}, button = "查询")
    @GetMapping("/listSingePage")
    public Object listSinglePage(@RequestParam(defaultValue = "") String name,
                    @RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit) {
        log.info("-------进来了--------------------");
        List<MallIndustry> list = categoryService.selectSinglePageList(name, page, limit);
        log.info("----结束了-----------------------");
        long total = PageInfo.of(list).getTotal();
        Map<String, Object> data = new HashMap<>();
        data.put("total", total);
        data.put("items", list);


        return ResponseUtil.ok(data);
    }


    /**
     * 单页管理
     */
    @RequiresPermissions("admin:category:createSinglePage")
    @RequiresPermissionsDesc(menu = {"频道管理", "单页管理"}, button = "添加")
    @PostMapping("/createSinglePage")
    public Object createSinglePage(@RequestBody String data) {
        log.info("-------进来了--------------------");
        String categoryNum = JacksonUtil.parseString(data, "categoryNum");
        String tempType = JacksonUtil.parseString(data, "tempType");
        String tempName = JacksonUtil.parseString(data, "tempName");
        String iname = JacksonUtil.parseString(data, "iname");
        String icode = JacksonUtil.parseString(data, "icode");
        if (categoryService.insertSinglePage(icode, iname, categoryNum, tempType, tempName) > 0) {

            return ResponseUtil.ok();
        }

        return ResponseUtil.badArgumentValue();


    }

    /**
     * 单页管理
     */
    @RequiresPermissions("admin:category:updateSinglePage")
    @RequiresPermissionsDesc(menu = {"频道管理", "单页管理"}, button = "编辑")
    @PostMapping("/updateSinglePage")
    public Object updateSinglePage(@RequestBody MallIndustry mallIndustry) {
        log.info("-------进来了--------------------");
        Object error = validateIndustry(mallIndustry);
        if (error != null) {
            return error;
        }

        if (categoryService.updateIndustryById(mallIndustry) == 0) {
            return ResponseUtil.updatedDataFailed();
        }
        return ResponseUtil.ok();


    }

    /**
     * 单页管理
     */
    @RequiresPermissions("admin:category:deleteSinglePage")
    @RequiresPermissionsDesc(menu = {"频道管理", "单页管理"}, button = "删除")
    @PostMapping("/deleteSinglePage")
    public Object deleteSinglePage(@RequestBody MallIndustry mallIndustry) {
        log.info("-------进来了--------------------");
        Object error = validateIndustry(mallIndustry);
        if (error != null) {
            return error;
        }
        mallIndustry.setDeleted(true);
        if (categoryService.updateIndustryById(mallIndustry) == 0) {
            return ResponseUtil.updatedDataFailed();
        }
        return ResponseUtil.ok();


    }

    /**
     * 单页管理
     */
    @RequiresPermissions("admin:category:updateStatus")
    @RequiresPermissionsDesc(menu = {"频道管理", "单页管理"}, button = "启用/禁用")
    @PostMapping("/updateStatus")
    public Object updateStatus(@RequestBody String data) {
        log.info("-------进来了--------------------");
        String id = JacksonUtil.parseString(data, "id");
        String status = JacksonUtil.parseString(data, "status");
        MallIndustry mallIndustry = categoryService.selectIndustry(id);
        if (null != mallIndustry) {
            if ("0".equals(status)) {
                mallIndustry.setStatus("1");
            } else if ("1".equals(status)) {
                mallIndustry.setStatus("0");
            }
        }
        if (categoryService.updateIndustryById(mallIndustry) == 0) {
            return ResponseUtil.updatedDataFailed();
        }
        return ResponseUtil.ok();


    }

    /**
     * 单页频道
     */
    @RequiresPermissions("admin:category:listSingleChannel")
    @RequiresPermissionsDesc(menu = {"频道管理", "单页频道"}, button = "查询")
    @GetMapping("/listSingleChannel")
    public Object listSingleChannel(@RequestParam String industryId, @RequestParam String name,
                    @RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit) {
        log.info("-------进来了--------------------");

        List<MallCategory> list = categoryService.querySingleChannel(industryId, name, page, limit);
        long total = PageInfo.of(list).getTotal();
        Map<String, Object> data = new HashMap<>();
        data.put("total", total);
        data.put("items", list);
        return ResponseUtil.ok(data);


    }

    /**
     * 单页频道
     */
    @RequiresPermissions("admin:category:createSinpageChannel")
    @RequiresPermissionsDesc(menu = {"频道管理", "单页频道"}, button = "添加")
    @PostMapping("/createSinpageChannel")
    public Object createSinpageChannel(@RequestBody MallCategory category) {
        log.info("-------进来了--------------------");

        category.setPositionCode("singlePage");
        category.setLevel("L1");
        MallIndustry mallIndustry = categoryService.selectIndustry(category.getIndustryId());

        List<MallCategory> list = categoryService.queryIndustryId(category.getIndustryId());
        if (!StringUtils.isEmpty(mallIndustry.getCategoryNum())) {
            if (list.size() < Integer.parseInt(mallIndustry.getCategoryNum())) {
                categoryService.add(category);
            } else {
                return ResponseUtil.fail(403, "已达到该单页下启用状态的频道的最大值，若想再启用新的频道，请先禁用其它频道！");
            }
        } else {
            categoryService.add(category);
        }
        return ResponseUtil.ok();

    }

    /**
     * 单页频道
     */
    @RequiresPermissions("admin:category:updateSinpageChannel")
    @RequiresPermissionsDesc(menu = {"频道管理", "单页频道"}, button = "编辑")
    @PostMapping("/updateSinpageChannel")
    public Object updateSinpageChannel(@RequestBody MallCategory category) {
        log.info("-------进来了--------------------");
        category.setPositionCode("singlePage");
        category.setLevel("L1");
        Object error = validate(category);
        if (error != null) {
            return error;
        }
        if (categoryService.updateById(category) == 0) {
            return ResponseUtil.updatedDataFailed();
        }
        return ResponseUtil.ok();

    }

    /**
     * 单页频道
     */
    @RequiresPermissions("admin:category:deleteSingleChannel")
    @RequiresPermissionsDesc(menu = {"频道管理", "单页频道"}, button = "删除")
    @GetMapping("/deleteSingleChannel")
    public Object deleteSingleChannel(@RequestParam String id) {
        log.info("-------进来了--------------------");
        MallCategory category = categoryService.findById(id);
        category.setDeleted(true);
        if (categoryService.updateById(category) == 0) {
            return ResponseUtil.updatedDataFailed();
        }
        return ResponseUtil.ok();

    }

    /**
     * 单页频道
     */
    @RequiresPermissions("admin:category:updateChannelStatus")
    @RequiresPermissionsDesc(menu = {"频道管理", "单页频道"}, button = "启用/禁用")
    @PostMapping("/updateChannelStatus")
    public Object updateChannelStatus(@RequestBody MallCategory category) {
        log.info("-------进来了--------------------");
        if ("0".equals(category.getStatus())) {
            category.setStatus("1");
        } else if ("1".equals(category.getStatus())) {
            category.setStatus("0");
        }
        if (categoryService.updateById(category) == 0) {
            return ResponseUtil.updatedDataFailed();
        }
        return ResponseUtil.ok();

    }

}
