package com.graphai.mall.admin.service;

import com.github.binarywang.wxpay.bean.request.WxPayRefundRequest;
import com.github.binarywang.wxpay.bean.result.WxPayRefundResult;
import com.github.binarywang.wxpay.exception.WxPayException;
import com.github.binarywang.wxpay.service.WxPayService;
import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.json.JacksonUtil;
import com.graphai.commons.util.sms.TecentSmsUtil;
import com.graphai.framework.system.service.IGraphaiSystemRoleService;
import com.graphai.framework.system.service.MallRoleService;
import com.graphai.mall.admin.vo.OrderVo;
import com.graphai.mall.core.notify.NotifyService;
import com.graphai.mall.core.notify.NotifyType;
import com.graphai.mall.db.dao.FcAccountMapper;
import com.graphai.mall.db.domain.*;
import com.graphai.mall.db.service.*;
import com.graphai.mall.db.service.goods.MallGoodsProductService;
import com.graphai.mall.db.service.goods.MallGoodsService;
import com.graphai.mall.db.service.integral.MallOrderIntegralService;
import com.graphai.mall.db.service.order.MallOrderGoodsService;
import com.graphai.mall.db.service.user.MallUserService;
import com.graphai.mall.db.service.user.behavior.MallCommentService;
import com.graphai.mall.db.util.OrderUtil;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;

import static com.graphai.framework.constant.AdminResponseCode.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;

//import org.apache.commons.lang.ArrayUtils;

/**
 * 积分商城-订单管理-admin-service
 *
 * @author Qxian
 * @version V0.0.1
 * @date 2020-10-22
 */
@Service
public class AdminOrderIntegralService {
    private final Log logger = LogFactory.getLog(AdminOrderIntegralService.class);

    @Autowired
    private MallOrderGoodsService orderGoodsService;
    @Autowired
    private MallOrderIntegralService orderIntegralService;
    @Autowired
    private MallGoodsProductService productService;
    @Autowired
    private MallUserService userService;
    @Autowired
    private MallCommentService commentService;
    @Autowired
    private WxPayService wxPayService;
    @Autowired
    private NotifyService notifyService;
    @Autowired
    private LogHelper logHelper;

    @Autowired
    private MallGoodsService goodsService;
    @Autowired
    private MallRoleService roleService;
    @Autowired
    private IGraphaiSystemRoleService iGraphaiSystemRoleService;
    @Resource
    private FcAccountMapper fcAccountMapper;

    /**
     * 后台管理-积分订单列表查询
     *
     * @param userId           用户ID
     * @param orderSn          订单号
     * @param goodsName        商品名称
     * @param tpId             分销员ID
     * @param tpUserName       分销员名称
     * @param orderStatusArray 订单状态
     * @param page             页数
     * @param limit            显示行数
     * @param sort             排序字段
     * @param order            排序方式
     * @return
     */
    public Object list(String userId, String orderSn, String goodsName, String tpId, String tpUserName,
                       List<Short> orderStatusArray,
                       Integer page, Integer limit, String sort, String order) {
        //获取当前用户
        MallAdmin adminUser = (MallAdmin) SecurityUtils.getSubject().getPrincipal();
        String deptId = "";
        if (adminUser != null) {
            deptId = adminUser.getDeptId();
        }
        List<MallRole> mallRoleList = iGraphaiSystemRoleService.selectRoleListMMByUserId(adminUser.getId());
        MallRole mallRole = null;
        if(mallRoleList.size() > 0){
            mallRole = mallRoleList.get(0);
        }

        List<MallOrder> orderList = orderIntegralService.querySelective(userId, orderSn, goodsName, tpId, tpUserName, orderStatusArray, page, limit, sort, order, null, null, mallRole, deptId, adminUser);

        List<OrderVo> ordervoList = new ArrayList<OrderVo>();
        if (orderList != null && orderList.size() > 0) {
            for (MallOrder order2 : orderList) {
                OrderVo orderVo = new OrderVo();
                try {
                    BeanUtils.copyProperties(orderVo, order2);
                    List<MallOrderGoods> orderGoodsList = orderGoodsService.queryByOid(order2.getId());
                    if (orderGoodsList != null && orderGoodsList.size() > 0) {
                        StringBuffer orderGoodsStr = new StringBuffer();
                        for (MallOrderGoods orderGoods : orderGoodsList) {
                            orderGoodsStr.append("【" + orderGoods.getGoodsName());
                            orderGoodsStr.append(";规格:" + ArrayUtils.toString(orderGoods.getSpecifications()));
                            orderGoodsStr.append(";数量:" + orderGoods.getNumber());
                            MallGoods goods = goodsService.selectOneById(orderGoods.getGoodsId());
                            if (goods != null && goods.getSrcGoodId() != null) {
                                orderGoodsStr.append(";").append(goods.getSrcGoodId());
                            }
                            orderGoodsStr.append("】");
                        }
                        orderVo.setOrderGoodsStr(orderGoodsStr.toString());
                        ordervoList.add(orderVo);
                    }
                } catch (Exception e) {
                    logger.error("", e);
                }
            }
        }
        long total = PageInfo.of(orderList).getTotal();

        Map<String, Object> data = new HashMap<>();
        data.put("total", total);
        data.put("items", ordervoList);

        return ResponseUtil.ok(data);
    }

    public Object detail(String id) {
        MallOrder order = orderIntegralService.findById(id);
        List<MallOrderGoods> orderGoods = orderGoodsService.queryByOid(id);
        UserVo user = userService.findUserVoById(order.getUserId());
        Map<String, Object> data = new HashMap<>();
        data.put("order", order);
        data.put("orderGoods", orderGoods);
        data.put("user", user);

        return ResponseUtil.ok(data);
    }

    /**
     * 订单退款
     * <p>
     * 1. 检测当前订单是否能够退款;
     * 2. 微信退款操作;
     * 3. 设置订单退款确认状态；
     * 4. 订单商品库存回库。
     * <p>
     * TODO
     * 虽然接入了微信退款API，但是从安全角度考虑，建议开发者删除这里微信退款代码，采用以下两步走步骤：
     * 1. 管理员登录微信官方支付平台点击退款操作进行退款
     * 2. 管理员登录Mall管理后台点击退款操作进行订单状态修改和商品库存回库
     *
     * @param body 订单信息，{ orderId：xxx }
     * @return 订单退款操作结果
     */
    @Transactional
    public Object refund(String body) {
        String orderId = JacksonUtil.parseString(body, "orderId");
        String refundMoney = JacksonUtil.parseString(body, "refundMoney");
        if (orderId == null) {
            return ResponseUtil.badArgument();
        }
        if (StringUtils.isEmpty(refundMoney)) {
            return ResponseUtil.badArgument();
        }

        MallOrder order = orderIntegralService.findById(orderId);
        if (order == null) {
            return ResponseUtil.badArgument();
        }

        if (order.getActualPrice().compareTo(new BigDecimal(refundMoney)) != 0) {
            return ResponseUtil.badArgumentValue();
        }

        // 如果订单不是退款状态，则不能退款
        if (!order.getOrderStatus().equals(OrderUtil.STATUS_REFUND)) {
            return ResponseUtil.fail(ORDER_CONFIRM_NOT_ALLOWED, "订单不能确认收货");
        }

        // 回退积分
        FcAccountExample fcAccountMallExample = new FcAccountExample();
        fcAccountMallExample.or().andCustomerIdEqualTo(order.getUserId()).andStatusEqualTo(new Short("1"));
        FcAccount fcAccountMall=  fcAccountMapper.selectOneByExample(fcAccountMallExample);
        fcAccountMall.setIntegralAmount(fcAccountMall.getIntegralAmount().add(order.getActualPrice()));
        fcAccountMapper.updateByPrimaryKey(fcAccountMall);

        // 设置订单取消状态
        order.setOrderStatus(OrderUtil.STATUS_REFUND_CONFIRM);
        if (orderIntegralService.updateWithOptimisticLocker(order) == 0) {
            throw new RuntimeException("更新数据已失效");
        }

        // 商品货品数量增加
        List<MallOrderGoods> orderGoodsList = orderGoodsService.queryByOid(orderId);
        for (MallOrderGoods orderGoods : orderGoodsList) {
            String productId = orderGoods.getProductId();
            Short number = orderGoods.getNumber();
            if (productService.addStock(productId, number) == 0) {
                throw new RuntimeException("商品货品库存增加失败");
            }
        }

        //TODO 发送邮件和短信通知，这里采用异步发送
        // 退款成功通知用户, 例如“您申请的订单退款 [ 单号:{1} ] 已成功，请耐心等待到账。”
        // 注意订单号只发后6位
        notifyService.notifySmsTemplate(order.getMobile(), NotifyType.REFUND, new String[]{order.getOrderSn().substring(8, 14)});

        logHelper.logOrderSucceed("退款", "订单编号 " + orderId);
        return ResponseUtil.ok();
    }

    /**
     * 发货
     * 1. 检测当前订单是否能够发货
     * 2. 设置订单发货状态
     *
     * @param body 订单信息，{ orderId：xxx, shipSn: xxx, shipChannel: xxx }
     * @return 订单操作结果
     * 成功则 { errno: 0, errmsg: '成功' }
     * 失败则 { errno: XXX, errmsg: XXX }
     */
    public Object ship(String body) {
        String orderId = JacksonUtil.parseString(body, "orderId");
        String shipSn = JacksonUtil.parseString(body, "shipSn");
        String shipCode = JacksonUtil.parseString(body, "shipCode");
        String shipChannel = JacksonUtil.parseString(body, "shipChannel");
        if (orderId == null || shipSn == null || shipChannel == null) {
            return ResponseUtil.badArgument();
        }

        MallOrder order = orderIntegralService.findById(orderId);
        if (order == null) {
            return ResponseUtil.badArgument();
        }

        // 如果订单不是已付款状态，则不能发货
        if (!order.getOrderStatus().equals(OrderUtil.STATUS_PAY)) {
            return ResponseUtil.fail(ORDER_CONFIRM_NOT_ALLOWED, "订单不能确认收货");
        }

        order.setOrderStatus(OrderUtil.STATUS_SHIP);
        order.setShipSn(shipSn);
        order.setShipChannel(shipChannel);
        order.setShipTime(LocalDateTime.now());
        order.setShipCode(shipCode);
        if (orderIntegralService.updateWithOptimisticLocker(order) == 0) {
            return ResponseUtil.updatedDateExpired();
        }

        //TODO 发送邮件和短信通知，这里采用异步发送
        // 发货会发送通知短信给用户:          *
        // "您的订单已经发货，快递公司 {1}，快递单 {2} ，请注意查收"
        notifyService.notifySmsTemplate(order.getMobile(), NotifyType.SHIP, new String[]{shipChannel, shipSn});

        try {
            //发送短信给代理
            this.sendSms(order, shipChannel, shipSn);
        } catch (Exception e) {
            logger.error("", e);
        }

        logHelper.logOrderSucceed("发货", "订单编号 " + orderId);
        return ResponseUtil.ok();
    }

    private void sendSms(MallOrder order, String shipChannel, String shipSn) {
        //短信通知代理
        if (order.getTpid() != null) {
            MallUser fenXiaoUser =
                    userService.findById(order.getTpid());

            if (fenXiaoUser != null) {

                String smsContent2fenxiao = "";
                /** 先查询商品 **/
                List<MallOrderGoods> orderGoods = orderGoodsService.queryByOid(order.getId());
                int loop = CollectionUtils.isNotEmpty(orderGoods) ? orderGoods.size() : 0;
                for (int i = 0; i < loop; i++) {
                    MallOrderGoods orderGood = orderGoods.get(i);

                    smsContent2fenxiao += "【商品: " + orderGood.getGoodsName() + ",数量: " + orderGood.getNumber() +
                            ",规格: " + Arrays.toString(orderGood.getSpecifications()) + "】";
                }
                String[] fenxiaoPhoneNumbers = {fenXiaoUser.getMobile()};
                String[] params2 = {shipChannel, shipSn, order.getConsignee(),
                        order.getOrderSn(), smsContent2fenxiao};
                int noteSaleMantemplateId = 364274;
                //364274 发货通知	
                //您的订单已发货! 物流公司：{1}快递单号：{2} 收件人: {3} 订单号：{4} 商品：{5}
                TecentSmsUtil.sendForTemplate(fenxiaoPhoneNumbers, noteSaleMantemplateId, params2);
            }
        }
    }

    /**
     * 回复订单商品
     *
     * @param body 订单信息，{ orderId：xxx }
     * @return 订单操作结果
     * 成功则 { errno: 0, errmsg: '成功' }
     * 失败则 { errno: XXX, errmsg: XXX }
     */
    public Object reply(String body) {
        String commentId = JacksonUtil.parseString(body, "commentId");
        if (commentId == null || "0".equals(commentId)) {
            return ResponseUtil.badArgument();
        }
        // 目前只支持回复一次
        if (commentService.findById(commentId) != null) {
            return ResponseUtil.fail(ORDER_REPLY_EXIST, "订单商品已回复！");
        }
        String content = JacksonUtil.parseString(body, "content");
        if (StringUtils.isEmpty(content)) {
            return ResponseUtil.badArgument();
        }
        // 创建评价回复
        MallComment comment = new MallComment();
        comment.setType((byte) 2);
        comment.setValueId(commentId);
        comment.setContent(content);
        comment.setUserId("0");               // 评价回复没有用
        comment.setStar((byte) 0);           // 评价回复没有用
        comment.setHasPicture(false);        // 评价回复没有用
        comment.setPicUrls(new String[]{});  // 评价回复没有用
        commentService.save(comment);

        return ResponseUtil.ok();
    }


}
