package com.graphai.mall.admin.vo;

import lombok.Data;

@Data
public class OrderModifyVo {
    private String id;
    private String orderStatus;
    private String orderPrice;
    private String consignee;
    private String mobile;
    private String address;
    private String ticket;
}
