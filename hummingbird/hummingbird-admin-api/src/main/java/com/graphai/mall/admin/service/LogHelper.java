package com.graphai.mall.admin.service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.graphai.commons.constant.FrameworkConstants;
import com.graphai.commons.util.IpUtil;
import com.graphai.commons.util.httprequest.UserAgentUtils;
import com.graphai.commons.util.ip.IpUtils;
import com.graphai.commons.util.lang.ExceptionUtils;
import com.graphai.commons.util.lang.StringUtils;
import com.graphai.commons.util.obj.ObjectUtils;
import com.graphai.commons.util.json.JacksonUtils;
import com.graphai.mall.db.domain.MallAdmin;
import com.graphai.mall.db.domain.MallLogWithBLOBs;
import com.graphai.mall.db.service.common.MallLogService;

import eu.bitwalker.useragentutils.UserAgent;
import io.netty.util.concurrent.DefaultThreadFactory;
import org.apache.ibatis.mapping.SqlCommandType;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.DefaultParameterNameDiscoverer;
import org.springframework.core.ParameterNameDiscoverer;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.method.HandlerMethod;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 这里的日志类型设计成四种（当然开发者需要可以自己扩展）
 * 一般日志：用户觉得需要查看的一般操作日志，建议是默认的日志级别
 * 安全日志：用户安全相关的操作日志，例如登录、删除管理员
 * 订单日志：用户交易相关的操作日志，例如订单发货、退款
 * 其他日志：如果以上三种不合适，可以选择其他日志，建议是优先级最低的日志级别
 * <p>
 * 当然可能很多操作是不需要记录到数据库的，例如编辑商品、编辑广告品之类。
 */
@Component
public class LogHelper {


    // 采用线程池优化性能
    private static ExecutorService logThreadPool = new ThreadPoolExecutor(5, 20,
            60L, TimeUnit.SECONDS, new LinkedBlockingQueue<>(),
            new DefaultThreadFactory("log-save"));


    public final static Integer LOG_TYPE_GENERAL = 0;
    public final static Integer LOG_TYPE_AUTH = 1;
    public final static Integer LOG_TYPE_ORDER = 2;
    public final static Integer LOG_TYPE_OTHER = 3;

    @Autowired
    private MallLogService logService;

    private static LogHelper me;


    public LogHelper() {
        me = this;
    }

    /**
     * 静态内部类，延迟加载，懒汉式，线程安全的单例模式
     */
    private static final class Static {
        private static MallLogService logService = me.logService;
    }


    public void logGeneralSucceed(String action) {
        logAdmin(LOG_TYPE_GENERAL, action, true, "", "");
    }

    public void logGeneralSucceed(String action, String result) {
        logAdmin(LOG_TYPE_GENERAL, action, true, result, "");
    }

    public void logGeneralFail(String action, String error) {
        logAdmin(LOG_TYPE_GENERAL, action, false, error, "");
    }

    public void logAuthSucceed(String action) {
        logAdmin(LOG_TYPE_AUTH, action, true, "", "");
    }

    public void logAuthSucceed(String action, String result) {
        logAdmin(LOG_TYPE_AUTH, action, true, result, "");
    }

    public void logAuthFail(String action, String error) {
        logAdmin(LOG_TYPE_AUTH, action, false, error, "");
    }

    public void logOrderSucceed(String action) {
        logAdmin(LOG_TYPE_ORDER, action, true, "", "");
    }

    public void logOrderSucceed(String action, String result) {
        logAdmin(LOG_TYPE_ORDER, action, true, result, "");
    }

    public void logOrderFail(String action, String error) {
        logAdmin(LOG_TYPE_ORDER, action, false, error, "");
    }

    public void logOtherSucceed(String action) {
        logAdmin(LOG_TYPE_OTHER, action, true, "", "");
    }

    public void logOtherSucceed(String action, String result) {
        logAdmin(LOG_TYPE_OTHER, action, true, result, "");
    }


    public void logOtherFail(String action, String error) {
        logAdmin(LOG_TYPE_OTHER, action, false, error, "");
    }

    public void logAdmin(Integer type, String action, Boolean succeed, String result, String comment) {
        MallLogWithBLOBs log = new MallLogWithBLOBs();

        Subject currentUser = SecurityUtils.getSubject();
        if (currentUser != null) {
            MallAdmin admin = (MallAdmin) currentUser.getPrincipal();
            if (admin != null) {
                log.setAdmin(admin.getUsername());
            } else {
                log.setAdmin("匿名用户");
            }
        } else {
            log.setAdmin("匿名用户");
        }

        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        if (request != null) {
            log.setIp(IpUtil.getIpAddr(request));
        }

        log.setType(type);
        log.setAction(action);
        log.setStatus(succeed);
        log.setResult(result);
        log.setComment(comment);
        logService.add(log);
    }


//	/**
//	 * 静态内部类，延迟加载，懒汉式，线程安全的单例模式
//	 */
//	private static final class Static {
//		private static LogService logService = SpringUtils.getBean(LogService.class);
//		private static MenuService menuService = SpringUtils.getBean(MenuService.class);
//	}

    // 参数名获取工具（尝试获取标注为@ModelAttribute注解的方法，第一个参数名一般为主键名）
    private static ParameterNameDiscoverer pnd = new DefaultParameterNameDiscoverer();

    /**
     * 保存日志
     */
    public static void saveLog(MallAdmin user, HttpServletRequest request, String logTitle, String logType) {
        saveLog(user, request, null, null, logTitle, logType, 0);
    }

    /**
     * 保存日志
     *
     * @param executeTime
     */
    public static void saveLog(MallAdmin user, HttpServletRequest request, Object handler, Exception ex, String logTitle, String logType, long executeTime) {
        if (user == null || StringUtils.isBlank(user.getUsername()) || request == null) {
//				|| !Global.getPropertyToBoolean("web.interceptor.log.enabled", "true")){
            return;
        }
        MallLogWithBLOBs log = new MallLogWithBLOBs();
        log.setLogTitle(logTitle);
        log.setLogType(logType);
        if (StringUtils.isBlank(log.getLogType())) {
            String sqlCommandTypes = ObjectUtils.toString(request.getAttribute(SqlCommandType.class.getName()));
            if (StringUtils.containsAny("," + sqlCommandTypes + ",", ",INSERT,", ",UPDATE,", ",DELETE,")) {
                log.setLogType(FrameworkConstants.TYPE_UPDATE);
            } else if (StringUtils.contains("," + sqlCommandTypes + ",", ",SELECT,")) {
                log.setLogType(FrameworkConstants.TYPE_SELECT);
            } else {
                log.setLogType(FrameworkConstants.TYPE_ACCESS);
            }
        }
        log.setServerAddr(request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort());
        log.setRemoteAddr(IpUtils.getRemoteAddr(request));
        log.setIp(IpUtil.getIpAddr(request));
        UserAgent userAgent = UserAgentUtils.getUserAgent(request);
        log.setDeviceName(userAgent.getOperatingSystem().getName());
        log.setBrowserName(userAgent.getBrowser().getName());
        log.setUserAgent(request.getHeader("User-Agent"));
        log.setRequestUri(StringUtils.abbr(request.getRequestURI(), 255));
        log.setRequestParams(JacksonUtils.bean2Jsn(request.getParameterMap()));
         
        log.setRequestMethod(request.getMethod());
        log.setAdmin(user.getUsername());
        BigDecimal executeTimeB = new BigDecimal(executeTime);
        log.setExecuteTime(executeTimeB);

        // 获取异常对象
        Throwable throwable = ex;
        if (throwable == null) {
            throwable = ExceptionUtils.getThrowable(request);
        }

        // 异步保存日志
        logThreadPool.submit(new SaveLogThread(log, handler, request.getContextPath(), throwable));
    }

    /**
     * 保存日志线程
     */
    public static class SaveLogThread extends Thread {

        private MallLogWithBLOBs log;
        private Object handler;
        private String contextPath;
        private Throwable throwable;

        public SaveLogThread(MallLogWithBLOBs log, Object handler, String contextPath, Throwable throwable) {
            this.log = log;
            this.handler = handler;
            this.contextPath = contextPath;
            this.throwable = throwable;
        }

        @Override
        public void run() {
            // 获取日志标题
            if (StringUtils.isBlank(log.getLogTitle())) {
                String permission = "";
                if (handler instanceof HandlerMethod) {
                    HandlerMethod hm = ((HandlerMethod) handler);
                    Method m = hm.getMethod();
                    // 获取权限字符串
                    RequiresPermissions rp = m.getAnnotation(RequiresPermissions.class);
                    permission = (rp != null ? StringUtils.join(rp.value(), ",") : "");

                    // 尝试获取BaseEntity的设置的主键值
//					for (Class<?> type : m.getParameterTypes()){
//						try {
//							// 判断是否是BaseEntity的子类
//							Class<?> superClass = type.getSuperclass();
//							while(superClass != null && superClass != BaseEntity.class){
//								superClass = superClass.getSuperclass();
//							};
//							// 如果是BaseEntity的子类，则获取主键名
//							if (superClass != null){
//								Table t = type.getAnnotation(Table.class);
//								for (Column c : t.columns()){
//									if (c.isPK()){
//										try {
//											String attrName = MapperHelper.getAttrName(c);
//											if (attrName != null){
//												log.setBizKey(log.getRequestParam(attrName));
//												log.setBizType(type.getSimpleName());
//											}
//										} catch (Exception e) {
//											break;
//										}
//									}
//								}
//							}
//						} catch (Exception e) {
//							break;
//						}
//					}

                    // 尝试获取标注为@ModelAttribute注解的方法，第一个参数名一般为主键名
//					if (StringUtils.isBlank(log.getBizKey())){
//						for (Method me : hm.getBeanType().getMethods()){
//							ModelAttribute ma = AnnotationUtils.findAnnotation(me, ModelAttribute.class);
//							if(ma != null){
//								String[] ps = pnd.getParameterNames(me);
//								if(ps != null && ps.length > 0){
//									log.setBizKey(StringUtils.abbr(log.getRequestParam(ps[0]), 64));
//									log.setBizType(me.getReturnType().getSimpleName());
//									break;
//								}
//							}
//						}
//					}
                }
                String href = log.getRequestUri();
                if (StringUtils.startsWith(href, contextPath)) {
                    href = StringUtils.substringAfter(href, contextPath);
                }


//				if (StringUtils.startsWith(href, Global.getAdminPath())){
//					href = StringUtils.substringAfter(href, Global.getAdminPath());
//				}
//				if (StringUtils.startsWith(href, Global.getFrontPath())){
//					href = StringUtils.substringAfter(href, Global.getFrontPath());
//				}
//				log.setLogTitle(Static.menuService.getMenuNamePath(href, permission));


            }
            if (StringUtils.isBlank(log.getLogTitle())) {
                log.setLogTitle("未知操作");
            }
            // 如果有异常，设置异常信息（将异常对象转换为字符串）
            log.setIsException(throwable != null ? "1" : "0");
            String message = ExceptionUtils.getExceptionMessage(throwable);
            if (message != null) {
                log.setExceptionInfo(message);
            } else {
                log.setExceptionInfo(ExceptionUtils.getStackTraceAsString(throwable));
            }
            // 如果无地址并无异常日志，则不保存信息
            if (StringUtils.isBlank(log.getRequestUri()) && StringUtils.isBlank(log.getExceptionInfo())) {
                return;
            }
            // 保存日志信息
            //Static.logService.insertLog(log);

            Static.logService.add(log);
        }
    }


}
