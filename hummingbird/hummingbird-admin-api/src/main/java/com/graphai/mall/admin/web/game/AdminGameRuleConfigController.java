package com.graphai.mall.admin.web.game;

import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.mall.db.domain.GameDefinition;
import com.graphai.mall.db.domain.GameRuleConfig;
import com.graphai.mall.db.service.game.GameDefinitionService;
import com.graphai.mall.db.service.game.GameRuleConfigService;
import com.graphai.mall.db.constant.CommonConstant;
import com.graphai.validator.Order;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 游戏规则配置
 */
@RestController
@RequestMapping("/admin/gameRuleConfig")
@Validated
public class AdminGameRuleConfigController {

    @Autowired
    private GameRuleConfigService gameRuleConfigService;
    @Autowired
    private GameDefinitionService gameDefinitionService;

    /**
     * 查询游戏规则配置
     */
    @GetMapping("/list")
    public Object list(@RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit,
                       @RequestParam(defaultValue = "id") String sort,
                       @Order @RequestParam(defaultValue = "desc") String order) {
        GameRuleConfig gameRuleConfig = new GameRuleConfig();
        List<GameRuleConfig> list = gameRuleConfigService.selectGameRuleConfigList(gameRuleConfig, page, limit, sort, order);
        long total = PageInfo.of(list).getTotal();
        Map<String, Object> data = new HashMap<>();
        data.put("total", total);
        data.put("items", list);

        return ResponseUtil.ok(data);
    }

    /**
     * 添加游戏规则配置
     */
    @PostMapping("/add")
    public Object addSave(@RequestBody GameRuleConfig gameRuleConfig) {
        if (validate(gameRuleConfig) != null) {
            return validate(gameRuleConfig);
        }
        //todo 判断占比设置比例是否合理

        //判断正、负比例占比
        BigDecimal total = gameRuleConfig.getNegativeProportion().add(gameRuleConfig.getPositiveProportion());
        if (total.compareTo(new BigDecimal(1.00)) != 0) {
            return ResponseUtil.badArgumentValue("配置参数出现错误！");
        }
        //判断游戏类型
        GameDefinition gameDefinition = gameDefinitionService.selectGameDefinitionById(gameRuleConfig.getGameId());
        if (gameDefinition == null) {
            return ResponseUtil.badArgumentValue("请选择正确的游戏！");
        }
        if (gameDefinition.getGameType().equals(CommonConstant.GAME_TYPE_CLOCK)) {
            //打卡
            if (validateClock(gameRuleConfig) != null) {
                return validateClock(gameRuleConfig);
            }
        } else if (gameDefinition.getGameType().equals(CommonConstant.GAME_TYPE_STEP)) {
            //走路
            if (validateStep(gameRuleConfig) != null) {
                return validateStep(gameRuleConfig);
            }
        } else {
            return ResponseUtil.badArgumentValue("请选择正确的游戏类型！");
        }

        gameRuleConfigService.insertGameRuleConfig(gameRuleConfig);
        return ResponseUtil.ok();
    }


    /**
     * 查看游戏规则配置详情
     */
    @GetMapping("/edit/{id}")
    public Object edit(@PathVariable("id") String id) {
        GameRuleConfig gameRuleConfig = gameRuleConfigService.selectGameRuleConfigById(id);
        return ResponseUtil.ok(gameRuleConfig);
    }

    /**
     * 修改游戏规则配置
     */
    @PostMapping("/edit")
    public Object editSave(@RequestBody GameRuleConfig gameRuleConfig) {
        //等级主键id
        if (StringUtils.isEmpty(gameRuleConfig.getId())) {
            return ResponseUtil.badArgumentValue("没有获取到id！");
        }

        if (validate(gameRuleConfig) != null) {
            return validate(gameRuleConfig);
        }
        //判断正、负比例占比
        BigDecimal total = gameRuleConfig.getNegativeProportion().add(gameRuleConfig.getPositiveProportion());
        if (total.compareTo(new BigDecimal(1.00)) != 0) {
            return ResponseUtil.badArgumentValue("配置参数出现错误！");
        }
        //判断游戏类型
        GameDefinition gameDefinition = gameDefinitionService.selectGameDefinitionById(gameRuleConfig.getGameId());
        if (gameDefinition == null) {
            return ResponseUtil.badArgumentValue("请选择正确的游戏！");
        }
        if (gameDefinition.getGameType().equals(CommonConstant.GAME_TYPE_CLOCK)) {
            //打卡
            if (validateClock(gameRuleConfig) != null) {
                return validateClock(gameRuleConfig);
            }
        } else if (gameDefinition.getGameType().equals(CommonConstant.GAME_TYPE_STEP)) {
            //走路
            if (validateStep(gameRuleConfig) != null) {
                return validateStep(gameRuleConfig);
            }
        } else {
            return ResponseUtil.badArgumentValue("请选择正确的游戏类型！");
        }

        gameRuleConfigService.updateGameRuleConfig(gameRuleConfig);
        return ResponseUtil.ok();
    }


    /**
     * 校验数据(游戏规则配置)
     */
    private Object validate(GameRuleConfig gameRuleConfig) {
        if (StringUtils.isEmpty(gameRuleConfig.getGameId())) {
            return ResponseUtil.badArgument();
        }
        if (StringUtils.isEmpty(gameRuleConfig.getRuleName())) {
            return ResponseUtil.badArgument();
        }
        if (StringUtils.isEmpty(gameRuleConfig.getRuleDescribe())) {
            return ResponseUtil.badArgument();
        }
        if (StringUtils.isEmpty(gameRuleConfig.getRuleBeginTime())) {
            return ResponseUtil.badArgument();
        }
        if (StringUtils.isEmpty(gameRuleConfig.getRuleEndTime())) {
            return ResponseUtil.badArgument();
        }
        if (gameRuleConfig.getNegativeProportion() == null) {
            return ResponseUtil.badArgument();
        }
        if (gameRuleConfig.getPositiveProportion() == null) {
            return ResponseUtil.badArgument();
        }
        if (gameRuleConfig.getSubRewardProportion() == null) {
            return ResponseUtil.badArgument();
        }
        if (gameRuleConfig.getAllRankProportion() == null) {
            return ResponseUtil.badArgument();
        }
        if (gameRuleConfig.getAllServiceProportion() == null) {
            return ResponseUtil.badArgument();
        }
        if (gameRuleConfig.getAllEnvelopesProportion() == null) {
            return ResponseUtil.badArgument();
        }
        if (gameRuleConfig.getAllNextJackpotProportion() == null) {
            return ResponseUtil.badArgument();
        }
        if (gameRuleConfig.getPartRankProportion() == null) {
            return ResponseUtil.badArgument();
        }
        if (gameRuleConfig.getPartServiceProportion() == null) {
            return ResponseUtil.badArgument();
        }
        if (gameRuleConfig.getPartEnvelopesProportion() == null) {
            return ResponseUtil.badArgument();
        }
        if (gameRuleConfig.getPartNextJackpotProportion() == null) {
            return ResponseUtil.badArgument();
        }

        //todo 需计算 d 值
        //奖励最高倍位(an)
        if (gameRuleConfig.getMaxRewardMultiple() == null) {
            return ResponseUtil.badArgument();
        }
        //奖励最低倍位(a1)
        if (gameRuleConfig.getMinRewardMultiple() == null) {
            return ResponseUtil.badArgument();
        }
        //奖励倍位下降幅度(d)
        if (gameRuleConfig.getSubRewardMultiple() == null) {
            return ResponseUtil.badArgument();
        }

        //游戏金额类型
        if (StringUtils.isEmpty(gameRuleConfig.getAmountType())) {
            return ResponseUtil.badArgument();
        }
        if (gameRuleConfig.getSignAmount() == null) {
            return ResponseUtil.badArgument();
        }
        return null;
    }


    private Object validateClock(GameRuleConfig gameRuleConfig) {
        //间隔段数(n,例如：20段)
        if (gameRuleConfig.getIntervalNumber() < 1) {
            return ResponseUtil.badArgument();
        }
        return null;
    }

    private Object validateStep(GameRuleConfig gameRuleConfig) {
        //步数
        if (gameRuleConfig.getStepCount() == null) {
            return ResponseUtil.badArgument();
        }
        return null;
    }
}
