package com.graphai.mall.admin.web.order;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.graphai.commons.util.lang.StringUtils;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.mall.admin.domain.FcAccountPaymentBill;
import com.graphai.mall.admin.util.LocalDateTimeUtil;
import com.graphai.mall.db.domain.MallOrderNotifyLog;
import com.graphai.mall.db.service.order.MallOrderNotifyLogService;
import com.graphai.validator.Order;
import com.graphai.validator.Sort;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/admin/order/notifylog")
@AllArgsConstructor
@Slf4j
public class AdminOrderNotifyLogController {

    private MallOrderNotifyLogService mallOrderNotifyLogService;


    @RequiresPermissions("admin:order:notifylog:list")
    @RequiresPermissionsDesc(menu = {"打款记录查询"}, button = "查询")
    @GetMapping("/list")
    public Object list(String orderId, String orderSn, String platform, String beginTime, String endTime,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit) {
        System.out.println(orderId + "orderSn:"+orderSn+"platform:"+platform + "beginTime:"+beginTime+"endTime:"+endTime);

        return mallOrderNotifyLogService.selectList(orderId, orderSn, platform, beginTime, endTime, page, limit);
    }

}
