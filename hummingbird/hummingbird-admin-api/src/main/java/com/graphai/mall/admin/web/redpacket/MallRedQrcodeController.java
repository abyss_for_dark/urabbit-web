package com.graphai.mall.admin.web.redpacket;

import java.awt.Color;
import java.awt.Font;
import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.logging.log4j.util.Strings;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.commons.util.lang.StringUtils;
import com.graphai.commons.util.servlet.ServletUtils;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.framework.generator.controller.IBaseController;
import com.graphai.mall.admin.domain.MallRedQrcode;
import com.graphai.mall.admin.service.IMallRedQrcodeService;

import cn.hutool.core.img.ImgUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.extra.qrcode.QrCodeUtil;
import cn.hutool.extra.qrcode.QrConfig;


/**
 * <p>
 * 红包码 前端控制器
 * </p>
 *
 * @author biteam
 * @since 2020-12-10
 */
@RestController
@RequestMapping("/admin/qrcode")
public class MallRedQrcodeController extends IBaseController<MallRedQrcode,  String >  {

    private final Log log = LogFactory.getLog(MallRedQrcodeController.class);

    DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

	@Autowired
    private IMallRedQrcodeService serviceImpl;

    @Value("${weiRed.qrcodeTemplate}")
    private String qrcodeTemplate2;

    @Value("${weiRed.commonDownloadAddres}")
    private String commonDownloadAddres;

    @Value("${weiRed.qrcodeUrl}")
    private String qrcodeUrl;
    
    @RequiresPermissions("admin:qrcode:single")
    @RequiresPermissionsDesc(menu = {"单独查询"}, button = "单独查询")
	@GetMapping("/single/{id}")
    public Object getEntityById(@PathVariable  String  id) throws Exception{
        MallRedQrcode entityVName = serviceImpl.getById(id);
        return ResponseUtil.ok(entityVName);
    }
    
    @RequiresPermissions("admin:qrcode:addOrUpdate")
    @RequiresPermissionsDesc(menu = {"新增"}, button = "新增")
    @PostMapping("/addOrUpdate")
    public Object addOrUpdate(MallRedQrcode mallRedQrcode) throws Exception{
        if(serviceImpl.saveOrUpdate(mallRedQrcode)){
            return ResponseUtil.ok();
        }else{
            return ResponseUtil.fail();
        }
    } 

	@RequiresPermissions("admin:qrcode:melist")
	@RequiresPermissionsDesc(menu = {"红包码查询"}, button = "查询")
    @GetMapping("/melist")
    public Object getMeList() throws Exception {
        IPage<MallRedQrcode> ipage = serviceImpl.page(getPlusPage(),listQueryWrapper());
        Map<String, Object> data = new HashMap<>();
        data.put("qrcodeUrl", qrcodeUrl);
        data.put("items", ipage);
        return ResponseUtil.ok(data);
    }
    private QueryWrapper<MallRedQrcode> listQueryWrapper() {
        QueryWrapper<MallRedQrcode> meq = new QueryWrapper<>();
        if(StringUtils.isNotEmpty(ServletUtils.getParameter("name"))) {
            meq.like("name", ServletUtils.getParameter("name"));
        }
        if(StringUtils.isNotEmpty(ServletUtils.getParameter("code"))) {
            meq.eq("code", ServletUtils.getParameter("code"));
        }
        if(StringUtils.isNotEmpty(ServletUtils.getParameter("lastDownloadTime"))) {
            meq.eq("last_download_time", ServletUtils.getLocalDateTime("lastDownloadTime"));
        }
        if(StringUtils.isNotEmpty(ServletUtils.getParameter("ownerId"))) {
            meq.eq("owner_id", ServletUtils.getParameter("ownerId"));
        }
        if(StringUtils.isNotEmpty(ServletUtils.getParameter("usersId"))) {
            meq.eq("users_id", ServletUtils.getParameter("usersId"));
        }
        if(StringUtils.isNotEmpty(ServletUtils.getParameter("remark"))) {
            meq.eq("remark", ServletUtils.getParameter("remark"));
        }
        if(StringUtils.isNotEmpty(ServletUtils.getParameter("id"))) {
            meq.eq("id", ServletUtils.getParameter("id"));
        }
        meq.eq("deleted",0);
        meq.orderByDesc("id");
        return meq;
    }
 	
 	@RequiresPermissions("admin:qrcode:save")
 	@RequiresPermissionsDesc(menu = {"红包码新增"}, button = "新增")
    @PostMapping("/save")
    public Object save(@ModelAttribute MallRedQrcode mallRedQrcode) throws Exception {
        serviceImpl.save(mallRedQrcode);
        return ResponseUtil.ok();
    }

    /**
     * 批量生成红包码
     */
    @RequiresPermissions("admin:qrcode:save")
    @RequiresPermissionsDesc(menu = {"红包码批量生成"}, button = "批量生成")
    @PostMapping("/createQrcodeCode")
    public Object createBatchCode(@RequestBody String data) {
        JSONObject jsonObject = JSONObject.parseObject(data);
        String number = (String) jsonObject.get("number");
        String partner = (String) jsonObject.get("partner"); //合作商id -- 目前先存到拥有者id
        String createRemark = (String) jsonObject.get("createRemark"); // 备注(姓名,拼音首字母)
        String createTotalNumberStr = (String) jsonObject.get("createTotalNumber"); // 总共能领取次数
        String createUserNumberStr = (String) jsonObject.get("createUserNumber");   // 个人限领次数
        String createOneNum = (String) jsonObject.get("createOneNum");     // 第一次区间(K1)
        String createTwoNum = (String) jsonObject.get("createTwoNum");     // 第二次区间(K2)
        String createThreeNum = (String) jsonObject.get("createThreeNum"); // 第三次区间(K3)
        String createFourNum = (String) jsonObject.get("createFourNum");   // 第四次区间(K4)
        String createFiveNum = (String) jsonObject.get("createFiveNum");   // 第五次区间(KA)
        String createSixeNum = (String) jsonObject.get("createSixeNum");   // 第五次区间(KA)
        String createUsersId = (String) jsonObject.get("createUsersId");   // 使用者ID
        String createStartTime = (String) jsonObject.get("createStartTime");   // 选择红包开始时间  yyyy-MM-dd HH:MM:ss
        String createEndTime = (String) jsonObject.get("createEndTime");       // 选择红包结束时间  yyyy-MM-dd HH:MM:ss
        Object error = validate(number,createStartTime,createEndTime);
        if (error != null) {
            return error;
        }
        if (Strings.isEmpty(number)) {
            number = "1";//生成数量不填默认为1
        }

        if (createTotalNumberStr == null) {
            createTotalNumberStr = "5"; // 总共能领取次数不填默认为5
        }
        if (createUserNumberStr == null) {
            createUserNumberStr = "5"; // 个人限领次数不填默认为5
        }
        Integer num = Integer.parseInt(number);
        Integer createTotalNumber = Integer.parseInt(createTotalNumberStr);
        Integer createUserNumber = Integer.parseInt(createUserNumberStr);

        // 日期
        LocalDateTime createStartTimeDate = LocalDateTime.parse(createStartTime,df);
        LocalDateTime createEndTimeDate = LocalDateTime.parse(createEndTime,df);

        // 循环生成二维码
        for (int i = 0; i < num; i++) {
            MallRedQrcode mallRedQrcode = new MallRedQrcode();
            if (Strings.isNotEmpty(partner)) {
                mallRedQrcode.setOwnerId(partner); //拥有者ID
            }
            if (Strings.isNotEmpty(createRemark)) {
                mallRedQrcode.setRemark(createRemark);            // 备注(姓名,拼音首字母)
            }
            mallRedQrcode.setTotalNumber(createTotalNumber);   // 总次数
            mallRedQrcode.setSurplusNumber(createTotalNumber); // 剩余次数
            mallRedQrcode.setUserNumber(createUserNumber);     // 个人限领次数
            mallRedQrcode.setOneNum(createOneNum);
            mallRedQrcode.setTwoNum(createTwoNum);
            mallRedQrcode.setThreeNum(createThreeNum);
            mallRedQrcode.setFourNum(createFourNum);
            mallRedQrcode.setFiveNum(createFiveNum);
            mallRedQrcode.setSixNum(createSixeNum);
            mallRedQrcode.setUsersId(createUsersId);
            mallRedQrcode.setStatrTime(createStartTimeDate);
            mallRedQrcode.setEndTime(createEndTimeDate);
            mallRedQrcode.setDeleted(0);
            serviceImpl.insertMallRedQrcodeBatch(mallRedQrcode);
        }

        Map<String, Object> dataResult = new HashMap<>();
        return ResponseUtil.ok(dataResult);
    }
	
	@RequiresPermissions("admin:qrcode:modify")
	@RequiresPermissionsDesc(menu = {"红包码修改"}, button = "修改")
    @PutMapping("/modify")
    public Object modify(@ModelAttribute  MallRedQrcode mallRedQrcode){
        serviceImpl.updateById(mallRedQrcode);
        return  ResponseUtil.ok();
    }
    
    @RequiresPermissions("admin:qrcode:del")
    @RequiresPermissionsDesc(menu = {"红包码删除"}, button = "删除")
    @DeleteMapping("/del/{id}")
    public Object deleteById(@PathVariable  String  id){
        //serviceImpl.removeById(id);
        MallRedQrcode entityVName = serviceImpl.getById(id);
        entityVName.setDeleted(1);  // 逻辑删除
        entityVName.setStatus("1"); // `status` varchar(5) DEFAULT NULL COMMENT '红包状态（0，有效，1 无效，2,禁用, 3 启用）'
        serviceImpl.updateById(entityVName);
        return  ResponseUtil.ok();
    }

    /**
     * 单个红包码下载
     */
    @RequiresPermissions("admin:qrcode:modify")
    @RequiresPermissionsDesc(menu = {"单个红包码下载"}, button = "红包码下载")
    @GetMapping("/singleDownLoad/{id}")
    public Object singleDownLoad(@PathVariable  String  id) {
        if (Strings.isEmpty(id)) {
            return ResponseUtil.fail("下载红包码ID不能为空，请稍后重试！");
        }
        MallRedQrcode entityVName = serviceImpl.getById(id);
        if (entityVName == null){
            return ResponseUtil.fail("未找到相关二维码，请稍后重试！");
        }

        String lastPackage = "storage" + File.separator  + "qrcode" + entityVName.getCode();
        String downloadPath = lastPackage + ".zip"; // 图片文件夹，压缩包zip
        String downloadName = lastPackage + ".zip"; // 下载文件名zip
        String packagePathSrcDir = lastPackage;   // 图片文件夹
        log.info("-------2.1、downloadPath---------"+downloadPath); // storage\qrcode63725A.zip
        log.info("-------2.2、downloadName---------"+downloadName); // storage\qrcode63725A.zip
        log.info("-------2.3、packagePathSrcDir---------"+packagePathSrcDir);// storage\qrcode63725A

        String commonSaveUrl = commonDownloadAddres; // "/home/mall/";
        String commonSaveUrlNew = commonSaveUrl + "storage" + File.separator;// /home/mall/storage/
        String storeImg = "";

        //创建文件夹，已存在就不用再建
        String packagePath =  lastPackage + File.separator;
        log.info("----------3、packagePath--------------"+packagePath);// storage\qrcode63725A\
        File filePackage = new File(packagePath);
        if (! filePackage.exists()) {
            filePackage.mkdirs();
        }
        String saveUrl = commonSaveUrl+packagePath;
        log.info("----------4.2、saveUrl模板2--------------"+saveUrl); // /home/mall/storage/qrcode63725A/
        log.info("----------4.2、commonSaveUrlNew--------------"+commonSaveUrlNew); // /home/mall/storage/
        String resultUrl = createQrcode2(entityVName.getUsersId(),entityVName.getCode(),qrcodeUrl,saveUrl,qrcodeTemplate2,storeImg,commonSaveUrlNew,entityVName.getId(),entityVName.getRemark());
        log.info("生成二维码地址resultUrl="+resultUrl);// 生成二维码地址 \weilai-life\storage\qrcode63725A\\63725A.png

        //可选是否包含被打包的目录。比如我们打包一个照片的目录，打开这个压缩包有可能是带目录的，也有可能是打开压缩包直接看到的是文件。
        //zip方法增加一个boolean参数可选这两种模式，以应对众多需求。
        //ZipUtil.zip(commonSaveUrl+packagePathSrcDir, commonSaveUrl+downloadPath, true);

        return ResponseUtil.ok(resultUrl);
    }

    /**
     * 生成二维码：模板1【2020-12-12】
     * http://kmi.gxflow.cn/wx/gamejackpot/qrCode?makerId=336830247180644352&makerCode=3C29PX
     * @param code 生成二维码图片名称
     * @param qrcodeUrl 二维码链接前缀地址，例如：http://kmi.gxflown.cn
     * @param saveUrl   生成二维码图片存放路径,例如：/home/mall/storage/qrcode63725A/
     * @param sourceUrl 源图像模板文件,例如：/home/mall/storage/red.png
     * @param storeImg 二维码中心图片，即门店图片
     * @param commonSaveUrlNew 二维码临时存放地址 /home/mall/storage/
     * @return 返回生成图片地址
     */
    private static String createQrcode1(String makerId, String code,String qrcodeUrl,String saveUrl,String sourceUrl,String storeImg,String commonSaveUrlNew,String qrcodeId, String remark){

        QrConfig qrConfig = new QrConfig();
        if(!"".equals(storeImg)){
            qrConfig.setImg(storeImg);//二维码中心图片
        }
        qrConfig.setWidth(430); //设置二维码宽度
        qrConfig.setHeight(430);//设置二维码高度
        qrConfig.setMargin(1);//设置边距
        File generate = QrCodeUtil.generate(qrcodeUrl + "/wx/gamejackpot/qrCode?makerId=" + makerId + "&makerCode=" + code,
                qrConfig, new File(saveUrl + code + ".png"));//生成二维码存放地址

        /**
         *
         * @param srcImageFile 源图像文件
         * @param destImageFile 目标图像文件
         * @param pressImg 水印图片
         * @param x 修正值。 默认在中间，偏移量相对于中间偏移
         * @param y 修正值。 默认在中间，偏移量相对于中间偏移
         * @param alpha 透明度：alpha 必须是范围 [0.0, 1.0] 之内（包含边界值）的一个浮点数字
         * @param qrcodeId 红包码ID
         */
        ImgUtil.pressImage(
                FileUtil.file(sourceUrl),//源图像文件
                FileUtil.file(saveUrl+code+".png"),//目标图像文件，保存生成图片地址
                ImgUtil.read(FileUtil.file(saveUrl + code + ".png")), //水印图片，就是上面生成二维码存放地址
                3, //x坐标修正值。 默认在中间，偏移量相对于中间偏移
                116, //y坐标修正值。 默认在中间，偏移量相对于中间偏移
                1f);
        ImgUtil.pressText(
                FileUtil.file(saveUrl+code+".png"), //源图像文件，就是需要添加文字的源文件
                FileUtil.file(saveUrl+code+".png"), //目标图像文件，就是生成添加文字存放地址
                qrcodeId+"("+remark+")", Color.BLACK, //文字
                new Font("黑体", Font.BOLD, 20), //字体
                0, //x坐标修正值。 默认在中间，偏移量相对于中间偏移
                350, //y坐标修正值。 默认在中间，偏移量相对于中间偏移
                1f);//透明度：alpha 必须是范围 [0.0, 1.0] 之内（包含边界值）的一个浮点数字
        return saveUrl+code+".png";
    }

    /**
     * 生成二维码：模板2【2020-12-16】
     * http://kmi.gxflow.cn/wx/gamejackpot/qrCode?makerId=336830247180644352&makerCode=3C29PX
     * @param code 生成二维码图片名称
     * @param qrcodeUrl 二维码链接前缀地址，例如：http://kmi.gxflown.cn
     * @param saveUrl   生成二维码图片存放路径,例如：/home/mall/storage/qrcode63725A/
     * @param sourceUrl 源图像模板文件,例如：/home/mall/storage/red.png
     * @param storeImg 二维码中心图片，即门店图片
     * @param commonSaveUrlNew 二维码临时存放地址 /home/mall/storage/
     * @param qrcodeId 红包码ID
     * @return 返回生成图片地址
     */
    private static String createQrcode2(String makerId, String code,String qrcodeUrl,String saveUrl,String sourceUrl,String storeImg,String commonSaveUrlNew,String qrcodeId, String remark){
        if (!"".equals(remark)) {
            qrcodeId = qrcodeId+"("+remark+")";
        }
        QrConfig qrConfig = new QrConfig();
        if(!"".equals(storeImg)){
            qrConfig.setImg(storeImg);//二维码中心图片
        }
        qrConfig.setWidth(630); //设置二维码宽度
        qrConfig.setHeight(630);//设置二维码高度
        qrConfig.setMargin(1);//设置边距
        File generate = QrCodeUtil.generate(qrcodeUrl + "/wx/gamejackpot/qrCode?makerId=" + makerId + "&makerCode=" + code,
                qrConfig, new File(commonSaveUrlNew + code + "temp2.png"));//生成二维码存放地址

        /**
         *
         * @param srcImageFile 源图像文件
         * @param destImageFile 目标图像文件
         * @param pressImg 水印图片
         * @param x 修正值。 默认在中间，偏移量相对于中间偏移
         * @param y 修正值。 默认在中间，偏移量相对于中间偏移
         * @param alpha 透明度：alpha 必须是范围 [0.0, 1.0] 之内（包含边界值）的一个浮点数字
         */
        ImgUtil.pressImage(
                FileUtil.file(sourceUrl),//源图像文件
                FileUtil.file(commonSaveUrlNew+code+"temp2.png"),//目标图像文件，保存生成图片地址
                ImgUtil.read(FileUtil.file(commonSaveUrlNew + code + "temp2.png")), //水印图片，就是上面生成二维码存放地址
                -10, //x坐标修正值。 默认在中间，偏移量相对于中间偏移
                36, //y坐标修正值。 默认在中间，偏移量相对于中间偏移
                1f);
        ImgUtil.pressText(
                FileUtil.file(commonSaveUrlNew+code+"temp2.png"), ///源图像文件，就是需要添加文字的源文件
                FileUtil.file(saveUrl+code+".png"), //目标图像文件，就是生成添加文字存放地址
                qrcodeId, Color.BLACK, //文字
                new Font("黑体", Font.BOLD, 40), //字体
                0, //x坐标修正值。 默认在中间，偏移量相对于中间偏移
                400, //y坐标修正值。 默认在中间，偏移量相对于中间偏移
                1f);//透明度：alpha 必须是范围 [0.0, 1.0] 之内（包含边界值）的一个浮点数字
        return saveUrl+code+".png";
    }

    /**
     * 批量生成激活码 验证
     *
     * @param number    生成数量
     * @param createStartTime  红包有效开始日期
     * @param createEndTime    红包有效结束日期
     * @return Object
     */
    private Object validate(String number,String createStartTime,String createEndTime) {
        if (StringUtils.isEmpty(number)) {
            number = "1";//生成数量不填默认为1
        }
        if (Integer.parseInt(number) <= 0) {
            return ResponseUtil.fail("红包生成数量为空");
        }
        if (StringUtils.isEmpty(createStartTime)) {
            return ResponseUtil.fail("红包时间范围不能为空");
        }
        if (StringUtils.isEmpty(createEndTime)) {
            return ResponseUtil.fail("红包时间范围不能为空");
        }
        return null;
    }

}

