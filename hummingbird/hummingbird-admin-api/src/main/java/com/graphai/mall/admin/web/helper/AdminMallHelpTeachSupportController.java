package com.graphai.mall.admin.web.helper;

import com.github.pagehelper.PageInfo;
import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.mall.db.domain.MallDictData;
import com.graphai.mall.db.domain.MallHelpTeachCategory;
import com.graphai.mall.db.domain.MallHelpTeachSupport;
import com.graphai.mall.db.service.distribution.MallDictDataService;
import com.graphai.mall.db.service.helper.MallHelpTeachSupportService;
import com.graphai.mall.db.vo.MallDictTreeVo;
import com.graphai.mall.db.vo.MallHelpTeachSupportVo;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 教程管理
 *
 * @author LaoGF
 * @since 2020-6-24
 */
@RestController
@RequestMapping("/admin/helpTeachSupport")
@Validated
public class AdminMallHelpTeachSupportController {

    @Resource
    private MallHelpTeachSupportService mallHelpTeachSupportService;
    @Resource
    private MallDictDataService mallDictDataService;


    /**
     * 列表
     */
    @RequiresPermissions("admin:helpTeachSupport:list")
    @RequiresPermissionsDesc(menu = {"帮助服务", "教程管理"}, button = "查询")
    @GetMapping("/list")
    public Object list(String id,
                       @RequestParam(required = false) String title,
                       @RequestParam(required = false) String categoryId,
                       @RequestParam(required = false) Integer state,
                       @RequestParam(required = false) Integer type,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit) {

        List<MallHelpTeachSupportVo> list = mallHelpTeachSupportService.selectList(id,title, categoryId, state, type, page, limit);
        List<MallHelpTeachCategory> options = mallHelpTeachSupportService.selectCategory();
        long total = PageInfo.of(list).getTotal();
        Map<String, Object> data = new HashMap<>(16);
        data.put("total", total);
        data.put("items", list);
        data.put("options", options);
        return ResponseUtil.ok(data);
    }


    /**
     * 添加
     */
    @RequiresPermissions("admin:helpTeachSupport:add")
    @RequiresPermissionsDesc(menu = {"帮助服务", "教程管理"}, button = "添加")
    @PostMapping("/add")
    public Object add(@RequestBody MallHelpTeachSupport mallHelpTeachSupport) {
        if (mallHelpTeachSupportService.insert(mallHelpTeachSupport)) {
            return ResponseUtil.ok();
        }
        return ResponseUtil.fail();
    }

    /**
     * 修改
     */
    @RequiresPermissions("admin:helpTeachSupport:update")
    @RequiresPermissionsDesc(menu = {"帮助服务", "教程管理"}, button = "修改")
    @PostMapping("/update")
    public Object update(@RequestBody MallHelpTeachSupport mallHelpTeachSupport) {
        if (mallHelpTeachSupportService.update(mallHelpTeachSupport)) {
            return ResponseUtil.ok();
        }
        return ResponseUtil.fail();
    }

    /**
     * 删除
     */
    @RequiresPermissions("admin:helpTeachSupport:delete")
    @RequiresPermissionsDesc(menu = {"帮助服务", "教程管理"}, button = "删除")
    @PostMapping("/delete")
    public Object delete(@RequestParam String id) {
        if (mallHelpTeachSupportService.deleteByID(id)) {
            return ResponseUtil.ok();
        }
        return ResponseUtil.fail();
    }

    /**
     * 状态修改
     */
    @RequiresPermissions("admin:helpTeachSupport:updateStatus")
    @RequiresPermissionsDesc(menu = {"帮助服务", "教程管理"}, button = "状态修改")
    @PostMapping("/updateStatus")
    public Object updateStatus(@RequestParam String id,
                               @RequestParam Integer state) {
        if (mallHelpTeachSupportService.updateStatus(id, state)) {
            return ResponseUtil.ok();
        }
        return ResponseUtil.fail();
    }

    /**
     * 获取树形类型列表
     * @return
     */
    @RequiresPermissions("admin:helpTeachSupport:treeselect")
    @RequiresPermissionsDesc(menu = {"帮助服务", "教程类目"}, button = "查询")
    @GetMapping("/treeselect")
    public Object treeList() {
        List<MallDictTreeVo> list = (List<MallDictTreeVo>) mallDictDataService.getDictTree(null, "help_teach_category");
        return ResponseUtil.ok(mallHelpTeachSupportService.buildDeptTreeSelect(list));
    }
}
