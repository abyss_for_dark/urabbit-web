package com.graphai.mall.admin.web.user;

import com.graphai.commons.util.ResponseUtil;
import com.graphai.framework.annotation.RequiresPermissionsDesc;
import com.graphai.mall.db.service.bmdesign.BmdesignBusinessFormService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/admin/profit")
@Validated
public class AdminProfitManageController {

    private final Log logger = LogFactory.getLog(AdminProfitManageController.class);

    @Autowired
    private BmdesignBusinessFormService bmdesignBusinessFormService;

    /**
     * 用户，代理 (返佣/分润) 列表
     *
     * @param page
     * @param limit
     * @param
     * @return
     */
    @RequiresPermissions("admin:profit:list")
    @RequiresPermissionsDesc(menu = {"系统管理", "返佣管理"}, button = "查询")
    @GetMapping("/list")
    public Object list(@RequestParam(defaultValue = "") Integer page,
                       @RequestParam(defaultValue = "") Integer limit) {

        List<Map<String, String>> list = bmdesignBusinessFormService.selectBmdesignList(page, limit);
        List<Map<String, Object>> list1 = new ArrayList<>();

        HashSet<String> map1 = new HashSet<>();

        for (int a = 0; a < list.size(); a++) {
            map1.add(list.get(a).get("id"));
        }


        for (String str : map1) {
            List list3 = new ArrayList<>();
            Map<String, Object> map = new HashMap<>();
            for (int i = 0; i < list.size(); i++) {
                if (str.equals(list.get(i).get("id"))) {
                    Map<String, String> map2 = new HashMap<>();
                    map2.put("calModel", list.get(i).get("calModel"));
                    map2.put("deleted", list.get(i).get("deleted"));
                    map2.put("id", list.get(i).get("id") + i);
                    map2.put("levelId", list.get(i).get("levelId"));
                    map2.put("levelName", list.get(i).get("levelName"));
                    map2.put("comPro", list.get(i).get("comPro"));
                    map2.put("profitPro", list.get(i).get("profitPro"));
                    map2.put("formName", list.get(i).get("formName"));
                    list3.add(map2);
                }
            }
            map.put("id", str);

            switch (str) {
                case "4":
                    map.put("name", "淘宝");
                    break;
                case "8":
                    map.put("name", "拼多多");
                    break;
                case "19":
                    map.put("name", "苏宁易购");
                    break;
                case "5":
                    map.put("name", "京东");
                    break;
                case "17":
                    map.put("name", "唯品会");
                    break;
                case "9":
                    map.put("name", "权益类");
                    break;
                case "23":
                    map.put("name", "美团");
                    break;
                case "26":
                    map.put("name", "出行");
                    break;
                case "24":
                    map.put("name", "肯德基");
                    break;
                case "25":
                    map.put("name", "团油");
                    break;
                case "22":
                    map.put("name", "闪电玩");
                    break;
                case "27":
                    map.put("name", "优惠券商品");
                    break;
            }
            map.put("children", list3);

            list1.add(map);
        }
        Map<String, Object> dataMap = new HashMap<>();
        dataMap.put("item", list1);
        dataMap.put("total", list1.size());
        return ResponseUtil.ok(dataMap);
    }


}
