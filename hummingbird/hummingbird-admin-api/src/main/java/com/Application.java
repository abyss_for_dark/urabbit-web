package com;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.alicp.jetcache.anno.config.EnableCreateCacheAnnotation;
import com.alicp.jetcache.anno.config.EnableMethodCache;
import com.graphai.commons.util.json.JacksonUtils;

@SpringBootApplication(scanBasePackages = {"com.graphai", "com.xxl"})
@MapperScan({"com.graphai.*.*.dao", "com.graphai.uid.worker.dao", "com.graphai.*.*.mapper", "com.xxl.*.*.dao", "com.xxl.job.admin.core.conf"})
@EnableTransactionManagement
@EnableScheduling
@EnableMethodCache(basePackages = "com.graphai.mall.*")
@EnableCreateCacheAnnotation
public class Application {
    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(Application.class);
        ConfigurableApplicationContext context = application.run(args);
        System.out.println("~~~~商城后台管理系统启动成功: " + JacksonUtils.bean2Jsn(context.getEnvironment().getActiveProfiles()));
    }

}
