
-- 1、用户信息
alter table mall_user add column `is_identity` tinyint(2) DEFAULT '0' COMMENT '是否已实名认证：0-未认证，1-已认证';

-- 2、网赚任务主表 wangzhuan_task
alter table wangzhuan_task add column completed_quantity int(11) DEFAULT '0' COMMENT '完成数量';
alter table wangzhuan_task add column audit_duration varchar(16) DEFAULT '' COMMENT '审核时长';
alter table wangzhuan_task add column `launch_settings` tinyint(2) DEFAULT '0' COMMENT '投放设置：0-区域 1-性别 2-年龄 3-其他';
alter table wangzhuan_task add column `province` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '省份';
alter table wangzhuan_task add column `city` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '城市';
alter table wangzhuan_task add column `county` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '县/区';
alter table wangzhuan_task add column `province_code` varchar(12) DEFAULT '' COMMENT '省编码';
alter table wangzhuan_task add column `city_code` varchar(12) DEFAULT '' COMMENT '市编码';
alter table wangzhuan_task add column `area_code` varchar(12) DEFAULT '' COMMENT '区编码';
-- 修改前：`task_type` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '任务类型(01信用卡，02 应用)', 
alter table wangzhuan_task modify column `task_type` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '投放类型(01-广泛投放，02-精准投放)';
-- 修改前：`tstatus` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '状态( ST100 未上架、ST101 已上架、ST102 预上架)
alter table wangzhuan_task modify column `tstatus` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '状态( ST100-未上架(审核中)、ST101-已上架、ST102-已下架、ST103-已驳回)';
alter table wangzhuan_task add column `reasons_for_rejection` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '驳回原因';
-- 修改前：`task_category` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '任务类目（T01新人任务, T02每日任务, T03高佣任务, T04阅读任务）',
alter table wangzhuan_task modify column `task_category` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '任务类目（T01-公益任务, T02-视频号, T03-抖音, T04-其他）';


-- 3、网赚用户领取
alter table wangzhuan_user_task add column `nickname` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '用户昵称';
alter table wangzhuan_user_task add column `avatar` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'https://gxfacepay.oss-cn-shenzhen.aliyuncs.com/hummingbird/app/static/img/config/she.png' COMMENT '领取用户头像';
alter table wangzhuan_user_task add column `mobile` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '用户手机号码';


-- 4、用户与标签 【分片表】
CREATE TABLE `mall_tag_mall_user` (
  `rela_id` varchar(32) NOT NULL COMMENT '主键ID',
  `user_id` varchar(32) NOT NULL COMMENT '用户ID',
  `tag_id` varchar(32) NOT NULL COMMENT '标签ID',
  PRIMARY KEY (`rela_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='标签和用户关联表';

-- 5、任务与标签 【广播-主表】
CREATE TABLE `mall_tag_wangzhuan_task` (
  `rela_id` varchar(32) NOT NULL COMMENT '主键ID',
  `task_id` varchar(32) NOT NULL COMMENT '任务ID',
  `tag_id` varchar(32) NOT NULL COMMENT '标签ID',
  PRIMARY KEY (`rela_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='标签和任务关联表';

-- 6、标签表【广播-主表】
CREATE TABLE `mall_tag` (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '标签名称',
  `sort_order` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '排序',
  `add_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `deleted` tinyint(1) DEFAULT '0' COMMENT '逻辑删除: 0-正常 1-逻辑删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='标签表';

