import request from '@/utils/request'

// 查询订单列表
export function listTest(query) {
  return request({
    url: '/admin/test/melist',
    method: 'get',
    params: query
  })
}

// 查询订单详细
export function getTest(id) {
  return request({
    url: '/admin/test/single/' + id,
    method: 'get'
  })
}

// 新增订单
export function addTest(data) {
  return request({
    url: '/admin/test/save',
    method: 'post',
    data: data
  })
}

// 修改订单
export function updateTest(data) {
  return request({
    url: '/admin/test/modify',
    method: 'put',
    data: data
  })
}

// 删除订单
export function delTest(id) {
  return request({
    url: '/admin/test/del/' + id,
    method: 'delete'
  })
}

// 导出订单
export function exportTest(query) {
  return request({
    url: '/admin/test/export',
    method: 'get',
    params: query
  })
}