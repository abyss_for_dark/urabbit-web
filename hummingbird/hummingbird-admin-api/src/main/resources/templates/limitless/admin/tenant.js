import request from '@/utils/request'
import qs from 'qs'

// 查询多租户信息列表
export function listTenant(query) {
  return request({
    url: '/tenant/melist',
    method: 'get',
    params: query
  })
}

// 查询多租户信息详细
export function getTenant(id) {
  return request({
    url: '/tenant/single/' + id,
    method: 'get'
  })
}

// 新增多租户信息
export function addTenant(data) {
  return request({
    url: '/tenant/save',
    method: 'post',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    data: qs.stringify(data)
  })
}

// 修改多租户信息
export function updateTenant(data) {
  return request({
    url: '/tenant/modify',
    method: 'put',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    data: qs.stringify(data)
  })
}

// 删除多租户信息
export function delTenant(id) {
  return request({
    url: '/tenant/del/' + id,
    method: 'delete'
  })
}

// 导出多租户信息
export function exportTenant(query) {
  return request({
    url: '/tenant/export',
    method: 'get',
    params: query
  })
}