package com.graphai.mall.db.service.search;

import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

import com.graphai.mall.db.domain.MallGoods;
import com.graphai.mall.db.dto.SearchEngineKeywordDTO;

/**
 * graphai:
 *     search:
 *      engine:
 *          service:
 *            impl: elastic
 * @author biteam
 * @date 2021-05-24
 *
 */
@Service
@Configuration
@ConditionalOnProperty(prefix = "graphai.search.engine.service",name = "impl",havingValue = "elastic" )
public class ISearchEngineServiceElasticImpl implements ISearchEngineService {

    @Override
    public List<MallGoods> queryGoodsByKeyWord(SearchEngineKeywordDTO searchDTO) {
        return null;
    }

}
