package com.graphai.mall.es.goods.domain;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class ESMallGoods {

    private String id;

    private Integer platform;

    private String bussinessId;

    private String goodsSn;

    private String name;

    private String categoryId;

    private String categoryIdTwo;

    private String secondCategoryId;

    private Integer rankNo;

    private Integer rankType;

    private String goodsClass;

    private String brandId;

    private String[] gallery;

    private String keywords;

    private String brief;

    private Boolean isOnSale;

    private Short sortOrder;

    private String jumpUrl;

    private BigDecimal actualPrice;

    private String couponCondition;

    private LocalDateTime upTime;

    private Boolean isCatshop;

    private Boolean isHaitao;

    private Boolean isGoldSeller;

    private Integer hotPush;

    private BigDecimal earnestMoney;

    private BigDecimal reduce;

    private Boolean isFreePostage;

    private BigDecimal estimateAmount;

    private String shopLogo;

    private String picUrl;

    private String marketingmainPic;

    private String shareUrl;

    private String utility;

    private Boolean isNew;

    private Boolean isDouyin;

    private Boolean isHot;

    private Boolean isMerber;

    private String unit;

    private BigDecimal counterPrice;

    private BigDecimal retailPrice;

    private BigDecimal factoryPrice;

    private String commissionRate;

    private Integer commissionType;

    private Integer commissionValue;

    private LocalDateTime addTime;

    private LocalDateTime updateTime;

    private Boolean deleted;

    private BigDecimal spreads;

    private BigDecimal rebateAmount;

    private BigDecimal firstRatio;

    private BigDecimal secondRatio;

    private BigDecimal thirdRatio;

    private String calModel;

    private BigDecimal cRetailPrice;

    private String srcGoodId;

    private String srcAdId;

    private String srcGoodName;

    private String dataBatchId;

    private String mediaUrl;

    private String industryId;

    private Integer restrictionQuantity;

    private String couponId;

    private Boolean merchantNoSale;

    private String cause;

    private Integer itemsale;

    private Integer itemsale2;

    private Integer todaysale;

    private String shoptype;

    private Byte isBrand;

    private Byte isLive;

    private String guideArticle;

    private String videoid;

    private String activityType;

    private String sellerId;

    private String sellerName;

    private String tbUserid;

    private String sellernick;

    private String shopname;

    private String tktype;

    private BigDecimal tkrates;

    private BigDecimal tkmoney;

    private Short reportStatus;

    private Integer generalIndex;

    private BigDecimal discount;

    private String tbShopid;

    private String tbFirstCategory;

    private String tbSecondCategory;

    private Byte isFastBuy;

    private Long fbStartTimeVal;

    private LocalDateTime fbStartTime;

    private LocalDateTime fbEndTime;

    private String downType;

    private LocalDateTime downTime;

    private String topicId;

    private String templateId;

    private String productType;

    private String stockStatus;

    private String chargeAccount;

    private Integer virtualGood;

    private String imgUrl;

    private String channelCode;

    private String salesTip;

    private Integer stock;

    private String cashbackType;

    private BigDecimal cashbackRate;

    private BigDecimal platformRatio;

    private BigDecimal userRatio;

    private BigDecimal shareRatio;

    private BigDecimal min;

    private BigDecimal couponDiscount;

    private String deptId;

    private BigDecimal cashbackQuota;

    private Integer cashbackLength;

    private LocalDate cashbackEffective;

    private Boolean shareAccelerate;

    private LocalDate cashbackEndTime;

    private LocalDateTime groupUpTime;

    private LocalDateTime groupDownTime;

    private LocalDateTime shopUpTime;

    private LocalDateTime shopDownTime;

    private String merchantId;

    private String merchantName;

    private String detail;


}
