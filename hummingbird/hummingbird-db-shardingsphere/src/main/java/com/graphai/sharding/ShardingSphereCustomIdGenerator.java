package com.graphai.sharding;

import java.util.Properties;

import org.apache.shardingsphere.spi.keygen.ShardingKeyGenerator;

import com.graphai.id.GraphaiIdGenerator;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ShardingSphereCustomIdGenerator implements ShardingKeyGenerator {

    private Properties properties = new Properties();

    // @Autowired
    // private UidGenerator uidGenerator;

    @Override
    public String getType() {
        return "BAIDU_UID";
    }

    @Override
    public Comparable<?> generateKey() {
        return GraphaiIdGenerator.nextId("");
        // return uidGenerator.getUID();
    }

}
