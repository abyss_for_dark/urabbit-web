package com.graphai.sharding;

import java.util.Collection;

import org.apache.shardingsphere.api.sharding.standard.PreciseShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingValue;

import com.graphai.commons.util.lang.StringUtils;


public class MyPreciseShardingAlgorithm implements PreciseShardingAlgorithm<String> {

    private static final String DS = "ds-master-";

    @Override
    public String doSharding(Collection<String> availableTargetNames, PreciseShardingValue<String> shardingValue) {

        if (StringUtils.isNotEmpty(shardingValue.getValue())) {
            String value = String.valueOf(Long.valueOf(shardingValue.getValue()) % 11);
            String str = DS + value;
            System.out.println("****************str:" + str); // 552864958602280961
            return str;
        }

        return DS + "0";
    }

}
