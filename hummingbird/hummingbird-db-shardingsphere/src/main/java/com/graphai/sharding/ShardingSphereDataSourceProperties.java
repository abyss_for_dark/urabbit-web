package com.graphai.sharding;

import java.util.Properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Getter;
import lombok.Setter;



@ConfigurationProperties(prefix = "spring.shardingsphere.datasource")
@Getter
@Setter
public class ShardingSphereDataSourceProperties {

    private Properties props = new Properties();
}
