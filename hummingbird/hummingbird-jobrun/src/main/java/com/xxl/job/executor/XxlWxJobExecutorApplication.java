package com.xxl.job.executor;

import com.alicp.jetcache.anno.config.EnableCreateCacheAnnotation;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication(scanBasePackages = {"com.graphai", "com.xxl.job.core", "com.xxl.job.executor","com.xxl.job.admin.service"})
@MapperScan({"com.graphai.*.*.dao", "com.graphai.**.**.mapper", "com.graphai.uid.worker.dao","com.xxl.job.admin.dao"})
@EnableCreateCacheAnnotation
//@EnableScheduling
public class XxlWxJobExecutorApplication {

    public static void main(String[] args) {
        SpringApplication.run(XxlWxJobExecutorApplication.class, args);
        System.out.println("xxl-job执行器启动成功,该工程完全引用开源xxl-jbo非必要情况下不要修改core包源代码......");
    }

}
