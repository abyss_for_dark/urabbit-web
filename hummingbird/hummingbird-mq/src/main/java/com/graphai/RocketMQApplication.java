package com.graphai;


import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.alicp.jetcache.anno.config.EnableCreateCacheAnnotation;
import com.alicp.jetcache.anno.config.EnableMethodCache;


@SpringBootApplication(scanBasePackages = {"com.graphai"})
// @SpringBootApplication(scanBasePackages = {"com"})
@MapperScan({"com.graphai.*.*.dao", "com.graphai.uid.worker.dao", "com.graphai.*.*.mapper", "com.xxl.*.*.dao", "com.xxl.job.admin.core.conf"})
@EnableTransactionManagement
@EnableScheduling
@EnableMethodCache(basePackages = "com.graphai")
// 开启jetcache缓存
@EnableCreateCacheAnnotation
@Configuration
public class RocketMQApplication {

    public static void main(String[] args) {
        SpringApplication.run(RocketMQApplication.class, args);
        System.out.println("RocketMQApplication启动成功..........");
    }

}

