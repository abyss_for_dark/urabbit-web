import com.graphai.RocketMQApplication;
import com.graphai.mall.db.service.game.component.GameRedComponent;
import com.graphai.open.utils.json.JacksonUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: jw
 * @Date: 2021/2/5 16:43
 */
@SpringBootTest(classes = RocketMQApplication.class)
@WebAppConfiguration
public class MqTest {
    @Autowired
    GameRedComponent gameRedComponent;

    @Test
    public void test() throws Exception {
        Map<String, Object> map = new HashMap<String, Object>(5);
        map.put("groupId", "88888");
        map.put("ruleId","12314");
        gameRedComponent.generateGroupRed(map);
//        gameRedComponent.insertRandom("88888","12314");

    }
}
